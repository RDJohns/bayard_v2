﻿<?php 
/* ----
	# Suivi des réception mensuel : les Traités, en cours , les KO, les non traités
	# Suivi des nombre de BDC payés par CHQ, CB, Mandat facture et Espèces, BDP
	# Suivi des KO par date de réception mensuelle
	ment
	
--- */ 	

		function getConnBayard() {
			
				$conn = pg_connect("host=srv-bdd-ged-bayard.madcom.local port=5432 dbname=v2_ged_bayard user=si password=51P@vGD24$")
				or die("Impossible de se connecter à v2_ged_bayard ");				
				return $conn;
		}		
		
		$conn  = getConnBayard(); 		
		
		
		function tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn){
			
			$sql = "SELECT id_stat_h FROM stat_hebdo 
					WHERE semaine = '".$semaine."' 
					AND annee = '".$annee."'
					AND titre = '".$titre."' 					
					AND rubrique = '".$rubrique."'
					AND societe  = '".$id_soc."' ";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			
			return $i;			
		}
		
		
								
		function tcheck_traitement_en_cours($date_fin,$conn){
			$list = array();
			$sql = "SELECT
						min (daty) as date_deb_sem, 
						max (daty) as date_fin_sem,
						sum(coalesce(nb_nontraite,0)) as nb_nontraite,
						semaine
					FROM
					(   SELECT 
							data_pli.date_courrier,
							daty,
							semaine,
							nb_nontraite
						FROM
						(
							SELECT daty,'S'||week as semaine  FROM liste_weeks (('".$date_fin."'::date - interval '2 months')::date,'".$date_fin."')
						) as data_daty
						LEFT JOIN(
							SELECT count(id_pli) as nb_nontraite, date_courrier::date as date_courrier 
							FROM pli_stat
							where (pli_cloture = 0 or pli_en_cours = 1 or pli_anomalie = 1 or pli_non_traite = 1) 
							and date_courrier  between ('".$date_fin."'::date - interval '3 months')::date and '".$date_fin."' 
							GROUP BY date_courrier ORDER BY date_courrier asc
						) data_pli on data_daty.daty = data_pli.date_courrier
					) as tab
					group by semaine
					order by semaine asc
					";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			if ($i > 0){				
					while ($res_rec = pg_fetch_assoc($res)) {
					$daty = $res_rec["date_deb_sem"];
					
						if($daty != "" && !empty($daty) && $res_rec['nb_nontraite'] > 0 ){
							$list[$daty]["date_deb_sem"]   = $res_rec['date_deb_sem'];	
							$list[$daty]["date_fin_sem"]   = $res_rec['date_fin_sem'];	
							$list[$daty]["semaine"]        = $res_rec['semaine'];
							$list[$daty]["nb_nontraite"]   = $res_rec['nb_nontraite'];
						}				
					}
				}
			return $list;			
		}
		
		function get_num_date($date,$conn){
			$num_date = 0;
			$sql = "SELECT EXTRACT(DOW FROM TIMESTAMP '".$date."'::date) as num_date";		
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			if ($i > 0){				
					while ($res_rec = pg_fetch_assoc($res)) {				
					$num_date   = $res_rec['num_date'];	
					}
				}
			return $num_date;

		}
	
		function get_end_date_week($date,$conn){
			$sql = "SELECT to_char( ('".$date."'::date + INTERVAL '7 day')::date, 'DD/MM/YYYY') as date_end";		
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			if ($i > 0){				
					while ($res_rec = pg_fetch_assoc($res)) {				
					$list['date_end']   = $res_rec['date_end'];	
					}
				}
			return $list;

		}
		
				
		
		setlocale (LC_TIME, 'fr_FR');	

		$array_tlmc   = array();
		$date_du_jour = date("Y-m-d");
		
		/*
		$date_du_jour = "2019-01-10";
		$mois_courant = "2019-01";
		$semaine   = "2019-01-01";		
		$annee        = "2019";
		$semaine 		   = "01";
		*/
		
		//#--------------------------------- Récuperer la date du fin de semaine -- --------
		$d_date_fin = "";
		$list       = array();
		$sql_date = " SELECT (date_trunc('MONTH', '".$date_du_jour."'::date) + INTERVAL '1 MONTH - 1 day')::DATE as date_fin";
		$res_date = @pg_query( $conn,$sql_date) OR die("erreur date de fin de semaine !!");
		$ifindate = pg_num_rows($res_date);
		if ($ifindate > 0){				
			while ($ores_date = pg_fetch_assoc($res_date)) {				
			$d_date_fin    = $ores_date['date_fin'];	
			}
		}
		
		$date_fin  = ($ifindate > 0) ? $d_date_fin : date("Y-m-d");
		
		
		// ################################################
		$module_nom  = "CRON DATA - GED BAYARD V2 - RECEPTION HEBDO" ;
		$nom_fichier = "10.90\GED\bayard_v2\crons\cron_data_hebdo.php" ;
		$date_jour   = date("Y-m-d");
		$couple 	 = date("YmdHis");
		$statut      = "debut" ;

		$sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple) 
		VALUES ('$module_nom','$nom_fichier','$date_jour','$statut','$couple') " ;
		$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;

		// ################################################  
		//#--------------------------------- Fin nombre de lot par date de courrier - --------
		$list 	   = tcheck_traitement_en_cours($date_fin,$conn);
		echo "<pre>";print_r($list); echo "</pre>";
		foreach($list as $klist => $vlist){
			
			$nb_nontraite      = $vlist["nb_nontraite"];
			$date_deb      	   = $vlist["date_deb_sem"];
			$date_fin          = $vlist["date_fin_sem"];
			$semaine    	   = $vlist["semaine"]; 
			$annee 			   = substr($vlist["date_deb_sem"],0,4);
			// récupérer (lundi,dimanche) pour un numéro de semaine donnée
			//echo $num_date         = get_num_date($date_courrier,$conn);
			//$date_fin = ($num_date == 1) ? get_end_date_week($date_courrier,$conn) : get_end_date_week($date_courrier,$conn) ;
			
			if ($nb_nontraite > 0 )
			{
					echo "Reste des plis à traiter : <b>".$nb_nontraite." </b>pour les courriers de <b>".$date_deb."</b> au <b>".$date_fin."</b></br>";
			
					$clauseWhere = $_clauseWhere = "";
					if($date_deb != '' && $date_fin != ''){
						
						$clauseWhere 	= "'".$date_deb."' , '".$date_fin."'";
						$_clauseWhere 	= " date_courrier between '".$date_deb."' and '".$date_fin."'";
					}		
					//#---------- AFFICHAGE GLOBAL : Nouveau, Encours, Traité, Anomalie ,---------------
					//#---------------------------------------------------------------------------------		
					
					$sql = "SELECT
								 id_soc, data_societe.nom_societe,
								 mouvement_total , mouvement_non_traite, mvt_cloture , mouvement_en_cours, 
								 pli_total, nb_pli_non_traite, pli_cloture, nb_pli_en_cours  , nb_pli_anomalie, nb_pli_ferme,
								 nb_pli_divise, ci_traite,mvt_anomalie ,mvt_ferme ,	mvt_divise, mvt_ci_traite,
								 case when nb_pli_ok is null then '0' else nb_pli_ok end as nb_pli_ok , 
								case when nb_pli_ano is null then '0' else nb_pli_ano end as nb_pli_ano , 
								case when nb_pli_ci is null then '0' else nb_pli_ci end as nb_pli_ci , 
								case when nb_pli_ks is null then '0' else nb_pli_ks end as nb_pli_ks , 
								case when nb_pli_kd is null then '0' else nb_pli_kd end as nb_pli_kd , 
								case when pli_ci_editee is null then '0' else pli_ci_editee end as nb_pli_ci_editee , 
								case when nb_pli_ci_envoyee is null then '0' else nb_pli_ci_envoyee end as nb_pli_ci_envoyee , 
								case when nb_pli_kd is null then '0' else nb_pli_kd end as nb_pli_kd , 
								case when mouvement_ok is null then '0' else mouvement_ok end as mouvement_ok , 
								case when mouvement_ano is null then '0' else mouvement_ano end as mouvement_ano , 
								case when mouvement_ci is null then '0' else mouvement_ci end as mouvement_ci , 
								case when mvt_ci_editee is null then '0' else mvt_ci_editee end as mvt_ci_editee ,
								case when mouvement_ci_envoyee is null then '0' else mouvement_ci_envoyee end as mouvement_ci_envoyee , 
								case when mouvement_ks is null then '0' else mouvement_ks end as mouvement_ks , 
								case when mouvement_kd is null then '0' else mouvement_kd end as mouvement_kd,
								case when mouvement_ferme is null then '0' else mouvement_ferme end as mouvement_ferme,
								case when mouvement_divise is null then '0' else mouvement_divise end as mouvement_divise
							FROM	(
								SELECT 
									societe.id as id_soc, nom_societe,1 flag
									FROM societe
								)as data_societe
								LEFT JOIN
								(
									SELECT 
										coalesce(mouvement_total::integer,0) mouvement_total , 
										coalesce(pli_total::integer,0)pli_total, 
										coalesce(nb_pli_non_traite::integer,0) nb_pli_non_traite , 
										coalesce(mouvement_non_traite::integer,0) mouvement_non_traite , 
										coalesce(pli_cloture::integer,0) pli_cloture, 
										coalesce(mvt_cloture::integer,0)mvt_cloture , 
										coalesce(nb_pli_en_cours::integer,0) nb_pli_en_cours ,
										coalesce(mouvement_en_cours::integer,0) mouvement_en_cours, 
										coalesce(nb_pli_anomalie::integer,0) nb_pli_anomalie , 
										coalesce(nb_pli_ferme::integer,0) nb_pli_ferme , 
										coalesce(nb_pli_divise::integer,0) nb_pli_divise, 
										coalesce(ci_traite::integer,0) ci_traite ,
										coalesce(mvt_anomalie::integer,0) mvt_anomalie,	
										coalesce(mvt_ferme::integer,0) mvt_ferme,	
										coalesce(mvt_divise::integer,0)mvt_divise, 
										coalesce(mvt_ci_traite::integer,0) mvt_ci_traite,
										coalesce(pli_ci_editee::integer,0)pli_ci_editee,
										coalesce(mvt_ci_editee::integer,0) mvt_ci_editee,
										societe
									FROM (
										SELECT   
											pli_total , 
											nb_pli_non_traite , 											 
											nb_pli_en_cours , 											
											nb_pli_anomalie  ,
											nb_pli_ferme ,
											nb_pli_divise, 
											pli_ci_editee,
											nb_pli_ci_traite as ci_traite,
											case when mouvement_en_cours is null then '0' else mouvement_en_cours end as mouvement_en_cours, 
											case when mouvement_non_traite is null then '0' else mouvement_non_traite end as mouvement_non_traite,  
											pli_cloture , 
											case when mvt_cloture is null then '0' else mvt_cloture end as mvt_cloture,
											case when mouvement_anomalie is null then '0' else mouvement_anomalie end as mvt_anomalie,
											case when mvt_ferme is null then '0' else mvt_ferme end as mvt_ferme,
											case when mvt_divise is null then '0' else mvt_divise end as mvt_divise,
											case when mvt_ci_editee is null then '0' else mvt_ci_editee end as mvt_ci_editee,
											case when mvt_ci_traite is null then '0' else mvt_ci_traite end as mvt_ci_traite,
											case when mouvement_total is null then '0' else mouvement_total end as mouvement_total,											
											2 societe 
										from f_reception_globale( ".$clauseWhere.", 2)
										UNION 
										SELECT pli_total , 
											nb_pli_non_traite , 											 
											nb_pli_en_cours , 											
											nb_pli_anomalie  ,
											nb_pli_ferme ,
											nb_pli_divise, 
											pli_ci_editee,
											nb_pli_ci_traite as ci_traite,
											case when mouvement_en_cours is null then '0' else mouvement_en_cours end as mouvement_en_cours, 
											case when mouvement_non_traite is null then '0' else mouvement_non_traite end as mouvement_non_traite,  
											pli_cloture , 
											case when mvt_cloture is null then '0' else mvt_cloture end as mvt_cloture,
											case when mouvement_anomalie is null then '0' else mouvement_anomalie end as mvt_anomalie,
											case when mvt_ferme is null then '0' else mvt_ferme end as mvt_ferme,
											case when mvt_divise is null then '0' else mvt_divise end as mvt_divise,
											case when mvt_ci_editee is null then '0' else mvt_ci_editee end as mvt_ci_editee,
											case when mvt_ci_traite is null then '0' else mvt_ci_traite end as mvt_ci_traite,
											case when mouvement_total is null then '0' else mouvement_total end as mouvement_total,											
											1 societe 
											from f_reception_globale( ".$clauseWhere.", 1)
									) as data_pli
								)as data on data.societe = data_societe.id_soc
								LEFT JOIN(
										SELECT 2 as societe_id,
										case when nb_pli_ok is null then '0' else nb_pli_ok end as nb_pli_ok , 
										case when nb_pli_ano is null then '0' else nb_pli_ano end as nb_pli_ano , 
										case when nb_pli_ci is null then '0' else nb_pli_ci end as nb_pli_ci , 
										case when nb_pli_ci_envoyee is null then '0' else nb_pli_ci_envoyee end as nb_pli_ci_envoyee , 
										case when nb_pli_ks is null then '0' else nb_pli_ks end as nb_pli_ks , 
										case when nb_pli_kd is null then '0' else nb_pli_kd end as nb_pli_kd ,  
										case when mouvement_ok is null then '0' else mouvement_ok end as mouvement_ok , 
										case when mouvement_ano is null then '0' else mouvement_ano end as mouvement_ano , 
										case when mouvement_ci is null then '0' else mouvement_ci end as mouvement_ci , 
										case when mouvement_ci_envoyee is null then '0' else mouvement_ci_envoyee end as mouvement_ci_envoyee ,
										case when mouvement_ks is null then '0' else mouvement_ks end as mouvement_ks , 
										case when mouvement_kd is null then '0' else mouvement_kd end as mouvement_kd ,										
										case when mouvement_ferme is null then '0' else mouvement_ferme end as mouvement_ferme , 
										case when mouvement_divise is null then '0' else mouvement_divise end as mouvement_divise 
										from f_reception_globale_cloture(  ".$clauseWhere.",2)
									union
										SELECT 1 as societe_id,
										case when nb_pli_ok is null then '0' else nb_pli_ok end as nb_pli_ok , 
										case when nb_pli_ano is null then '0' else nb_pli_ano end as nb_pli_ano , 
										case when nb_pli_ci is null then '0' else nb_pli_ci end as nb_pli_ci , 
										case when nb_pli_ci_envoyee is null then '0' else nb_pli_ci_envoyee end as nb_pli_ci_envoyee , 
										case when nb_pli_ks is null then '0' else nb_pli_ks end as nb_pli_ks , 
										case when nb_pli_kd is null then '0' else nb_pli_kd end as nb_pli_kd ,   										
										case when mouvement_ok is null then '0' else mouvement_ok end as mouvement_ok , 
										case when mouvement_ano is null then '0' else mouvement_ano end as mouvement_ano , 
										case when mouvement_ci is null then '0' else mouvement_ci end as mouvement_ci , 
										case when mouvement_ci_envoyee is null then '0' else mouvement_ci_envoyee end as mouvement_ci_envoyee , 
										case when mouvement_ks is null then '0' else mouvement_ks end as mouvement_ks , 
										case when mouvement_kd is null then '0' else mouvement_kd end as mouvement_kd ,																			
										case when mouvement_ferme is null then '0' else mouvement_ferme end as mouvement_ferme , 
										case when mouvement_divise is null then '0' else mouvement_divise end as mouvement_divise 
										from f_reception_globale_cloture(".$clauseWhere.",1)
								) as data_cloture on data_cloture.societe_id= data_societe.id_soc
							ORDER BY data_societe.id_soc  asc"	;
					/*echo "<pre>";
					print_r($sql);
					echo "</pre>"; exit;*/
					$res 		= pg_query($sql) OR die(" erreur dans la requete AFFICHAGE GLOBAL");	
					
					$iNbr       = pg_num_rows($res);
					$total_global = 0;
					if ( $iNbr > 0){	
					
						while ($ores = pg_fetch_assoc($res)) {
							
						$id_soc     = $ores['id_soc'];			 
						$nouveau    = $ores['nb_pli_non_traite'];			 
						$encours    = $ores['nb_pli_en_cours'];			 
						$traite     = $ores['pli_cloture'];			 
						$anomalie   = $ores['nb_pli_anomalie'];
						$ferme      = $ores['nb_pli_ferme'];
						$divise     = $ores['nb_pli_divise'];
						$ci_traite  = $ores['ci_traite'];
						
                         $total   	 = $ores['pli_total'];

						$mvt_nouveau    = $ores['mouvement_non_traite'];			 
						$mvt_encours    = $ores['mouvement_en_cours'];			 
						$mvt_traite     = $ores['mvt_cloture'];			 
						$mvt_anomalie   = $ores['mvt_anomalie'];  	
						$mvt_ferme      = $ores['mvt_ferme'];  	
						$mvt_divise     = $ores['mvt_divise'];  	
						$mvt_ci_traite  = $ores['mvt_ci_traite'];
						$mvt_ci_editee    = $ores['mvt_ci_editee'];						
						$mvt_total   	= $ores['mouvement_total'];	
						
						$nb_pli_ok      = $ores['nb_pli_ok'];
						$nb_pli_ano     = $ores['nb_pli_ano'];
						$nb_pli_ci      = $ores['nb_pli_ci'];
						$nb_pli_ks      = $ores['nb_pli_ks'];
						$nb_pli_kd      = $ores['nb_pli_kd'];
						$nb_pli_ko_inconnu  = $ores['nb_pli_ko_inconnu'];
						$nb_pli_ko_att  	= $ores['nb_pli_ko_att'];
						$nb_pli_ci_editee 	= $ores['nb_pli_ci_editee'];
						$nb_pli_ci_envoyee  = $ores['nb_pli_ci_envoyee'];
						$nb_pli_ferme  		= $ores['nb_pli_ferme'];
						$nb_pli_divise  	= $ores['nb_pli_divise'];
						
						$mouvement_ok      = $ores['mouvement_ok'];
						$mouvement_ano     = $ores['mouvement_ano'];
						$mouvement_ci      = $ores['mouvement_ci'];
						$mouvement_ks      = $ores['mouvement_ks'];
						$mouvement_kd      = $ores['mouvement_kd'];
						$mouvement_ko_inconnu  	= $ores['mouvement_ko_inconnu']; // ajouter nouveau
						$mouvement_ko_att  		= $ores['mouvement_ko_att'];
						$mouvement_ci_envoyee  	= $ores['mouvement_ci_envoyee']; 
						$mouvement_ferme  		= $ores['mouvement_ferme']; 
						$mouvement_divise  		= $ores['mouvement_divise']; 

						 echo "******************** Pli global - Début : ".$semaine." *******************************</br>";
						 echo "</br>GLOBAL</br>";		 
						 echo  "--------------</br> Nouveau : ".$nouveau." ,</br>Encours : ".$encours.", </br>Traite : ".$traite.",</br> Anomalie : ".$anomalie.",</br> Total : ".$total."</br>";
						 $titre    = "GLOBAL";
						 $rubrique = "Non traité";
						 $cpt_data_nv  	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 
						 //--- Non Traité
						 if($cpt_data_nv  == 0){					
								$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nouveau."','".$mvt_nouveau."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_hebdo SET nb_pli = ".$nouveau." , nb_mvt = ".$mvt_nouveau." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Encours
						 $rubrique 		= "En cours";
						 $cpt_data_enc  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_enc  == 0){
								$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$encours."','".$mvt_encours." ','".$id_soc."','".$date_deb."' , '".$date_fin."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_hebdo SET nb_pli = ".$encours." ,nb_mvt = ".$mvt_encours." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Cloturé
						$rubrique = "Cloturé";
						$cpt_data_cloture  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_cloture  == 0){
								$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$traite."','".$mvt_traite."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_hebdo SET nb_pli = ".$traite." ,nb_mvt = ".$mvt_traite." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Anomalie
						$rubrique = "Anomalie";
						$cpt_data_ano  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_ano  == 0){
								echo $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$anomalie."','".$mvt_anomalie."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								echo $update = "UPDATE stat_hebdo SET nb_pli = ".$anomalie." , nb_mvt = ".$mvt_anomalie."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }

                   
						 //--- ok
						$rubrique = "OK";
						$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ok."','".$mouvement_ok."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_ok." , nb_mvt = ".$mouvement_ok."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
					      //--- Total reçu
						$rubrique = "Total";
						$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$total."','".$mvt_total."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_hebdo SET nb_pli = ".$total." , nb_mvt = ".$mvt_total." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
											
						/*
						
						 echo "******************** Mouvement global - Début : ".$semaine." , Fin : ".$date_fin."*******************************</br>";
						 echo "</br>GLOBAL</br>";		 
						 echo  "--------------</br> Mvt Nouveau : ".$mvt_nouveau." ,</br>Mvt Encours : ".$mvt_encours.", </br>Mvt raite : ".$traite.",</br> Mvt Anomalie : ".$mvt_anomalie.",</br> Mvt Total : ".$mvt_total."</br>";
						 $mvt_traite    = "GLOBAL";
						 $rubrique = "Non traité";
						 //$mvt_rubrique = "Mvt Non traité";
						 
						 $cpt_data_nv  	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 //--- Non Traité
						 if($cpt_data_nv  > 0){					
								$update = "UPDATE stat_hebdo SET nb_mvt = ".$mvt_nouveau." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Encours
						 $rubrique 		= "En cours";
						 $cpt_data_enc  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_enc  > 0){
							
								$update = "UPDATE stat_hebdo SET nb_mvt = ".$mvt_encours." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Cloturé
						$rubrique = "Cloturé";
						$cpt_data_cloture  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_cloture  > 0){
								$update = "UPDATE stat_hebdo SET nb_mvt = ".$mvt_traite." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Anomalie
						$rubrique = "Anomalie";
						$cpt_data_ano  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_ano  > 0){
							$update = "UPDATE stat_hebdo SET nb_mvt = ".$mvt_anomalie." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
					
						
						 //--- OK
						$rubrique = "OK";
						$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ok."','".$mouvement_ok."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_hebdo SET nb_mvt = ".$mouvement_ok."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						//--- Total Mouvement
						$rubrique = "Total";
						$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total >0)
						 {
								$update = "UPDATE stat_hebdo SET nb_mvt = ".$mvt_total." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }	*/		 
						
						
						
						}
						
						
					}
					//#-----------------------------Nouveau tableau  ------------------------
					//#-------------------------- Suivi des plis KO ------------------------
					 $sql = "SELECT
								 id_soc,
								 coalesce(SUM(pli_ko),0) as pli_ko,
								 coalesce(SUM(mvt_ko),0) as mvt_ko ,
								case when SUM(ko_scan) is null then 0 else SUM(ko_scan) end as ko_scan,
								case when SUM(ko_def) is null then 0 else SUM(ko_def) end as ko_def,
								case when SUM(ko_inconnu) is null then 0 else SUM(ko_inconnu) end as ko_inconnu,
								case when SUM(ko_ks) is null then 0 else SUM(ko_ks) end as ko_ks,
								case when SUM(ko_ke) is null then 0 else SUM(ko_ke) end as ko_ke,
								case when SUM(ko_abondonne) is null then 0 else SUM(ko_abondonne) end as ko_abondonne,
								case when SUM(divise) is null then 0 else SUM(divise) end as divise,
								case when SUM(ok_ci) is null then 0 else SUM(ok_ci) end as ok_ci,
								case when SUM(ko_encours) is null then 0 else SUM(ko_encours) end as ko_encours,
								case when SUM(ko_circulaire) is null then 0 else SUM(ko_circulaire) end as ko_circulaire,
								case when SUM(ci_editee) is null then 0 else SUM(ci_editee) end as ci_editee,
								case when SUM(ci_envoyee) is null then 0 else SUM(ci_envoyee) end as ci_envoyee, 
								case when SUM(mvt_ko_scan) is null then 0 else SUM(mvt_ko_scan) end as mvt_ko_scan,
								case when SUM(mvt_ko_def) is null then 0 else SUM(mvt_ko_def) end as mvt_ko_def,
								case when SUM(mvt_ko_inconnu) is null then 0 else SUM(mvt_ko_inconnu) end as mvt_ko_inconnu,
								case when SUM(mvt_ko_ks) is null then 0 else SUM(mvt_ko_ks) end as mvt_ko_ks,
								case when SUM(mvt_ko_ke) is null then 0 else SUM(mvt_ko_ke) end as mvt_ko_ke,
								case when SUM(mvt_ko_abondonne) is null then 0 else SUM(mvt_ko_abondonne) end as mvt_ko_abondonne,
								case when SUM(mvt_ko_circulaire) is null then 0 else SUM(mvt_ko_circulaire) end as mvt_ko_circulaire,
								case when SUM(mvt_ci_editee) is null then 0 else SUM(mvt_ci_editee) end as mvt_ci_editee,
								case when SUM(mvt_ci_envoyee) is null then 0 else SUM(mvt_ci_envoyee) end as mvt_ci_envoyee,
								case when SUM(mvt_divise) is null then 0 else SUM(mvt_divise) end as mvt_divise,
								case when SUM(mvt_ok_ci) is null then 0 else SUM(mvt_ok_ci) end as mvt_ok_ci,
								case when SUM(mvt_ko_encours) is null then 0 else SUM(mvt_ko_encours) end as mvt_ko_encours
							FROM	(
								SELECT 
									societe.id as id_soc, nom_societe,1 flag
									FROM societe
								)as data_societe
								
								LEFT JOIN(
										SELECT 1 as societe_id,
										 case when f_pli.statut_saisie = 2   then count(distinct f_pli.id_pli) else 0 end as ko_scan,					 
										 case when f_pli.statut_saisie = 3  then count( distinct f_pli.id_pli) else 0 end as ko_def,
										 case when f_pli.statut_saisie = 4  then count( distinct f_pli.id_pli) else 0 end as ko_inconnu,
										 case when f_pli.statut_saisie = 5 then count(distinct f_pli.id_pli) else 0 end as ko_ks,
										 case when f_pli.statut_saisie = 6 then count(distinct f_pli.id_pli) else 0 end as ko_ke,
										 case when f_pli.statut_saisie = 7  then count(distinct f_pli.id_pli) else 0 end as ko_circulaire,
										 case when f_pli.statut_saisie = 8  then count( distinct f_pli.id_pli) else 0 end as ko_abondonne,						 
										 case when f_pli.statut_saisie = 9  then count(distinct f_pli.id_pli) else 0 end as ci_editee,
										 case when f_pli.statut_saisie = 10  then count(distinct f_pli.id_pli) else 0 end as ci_envoyee,
										 case when f_pli.statut_saisie = 24  then count(distinct f_pli.id_pli) else 0 end as divise,
										 case when f_pli.statut_saisie = 11  then  count(distinct f_pli.id_pli) else 0 end as ok_ci,
										  case when f_pli.statut_saisie = 12  then  count(distinct f_pli.id_pli) else 0 end as ko_encours,
										 case when f_pli.statut_saisie = ANY (ARRAY[2,3,4,5,6,7,9,12])  then count(distinct f_pli.id_pli) else 0 end as pli_ko,
										 case when f_pli.statut_saisie = 2   then  sum(mvt_nbr) else 0 end as mvt_ko_scan,					 
										 case when f_pli.statut_saisie = 3  then  sum(mvt_nbr)else 0 end as mvt_ko_def,
										 case when f_pli.statut_saisie = 4  then  sum(mvt_nbr) else 0 end as mvt_ko_inconnu,
										 case when f_pli.statut_saisie = 5 then  sum(mvt_nbr) else 0 end as mvt_ko_ks,
										 case when f_pli.statut_saisie = 6 then  sum(mvt_nbr) else 0 end as mvt_ko_ke,
										 case when  f_pli.statut_saisie = 7  then  sum(mvt_nbr) else 0 end as mvt_ko_circulaire,
										 case when f_pli.statut_saisie = 8  then  sum(mvt_nbr) else 0 end as mvt_ko_abondonne,						 
										 case when f_pli.statut_saisie = 9  then  sum(mvt_nbr) else 0 end as mvt_ci_editee,
										 case when f_pli.statut_saisie = 10  then  sum(mvt_nbr)else 0 end as mvt_ci_envoyee,
										  case when f_pli.statut_saisie = 24  then  sum(mvt_nbr)else 0 end as mvt_divise,
										  case when f_pli.statut_saisie = 11  then  sum(mvt_nbr)else 0 end as mvt_ok_ci,
										  case when f_pli.statut_saisie = 12  then  sum(mvt_nbr)else 0 end as mvt_ko_encours,
										 case when f_pli.statut_saisie = ANY (ARRAY[2,3,4,5,6,7,9,12])   then  sum(mvt_nbr)else 0 end as mvt_ko
										 FROM  f_pli
											INNER JOIN f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
											INNER JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
											INNER JOIN societe ON societe.id = f_pli.societe	
											WHERE  ".$_clauseWhere." AND f_pli.societe = 1											
										GROUP BY f_pli.statut_saisie
											
									union
										SELECT 2 as societe_id,
										 case when f_pli.statut_saisie = 2   then count(distinct f_pli.id_pli) else 0 end as ko_scan,					 
										 case when f_pli.statut_saisie = 3  then count( distinct f_pli.id_pli) else 0 end as ko_def,
										 case when f_pli.statut_saisie = 4  then count( distinct f_pli.id_pli) else 0 end as ko_inconnu,
										 case when f_pli.statut_saisie = 5 then count(distinct f_pli.id_pli) else 0 end as ko_ks,
										 case when f_pli.statut_saisie = 6 then count(distinct f_pli.id_pli) else 0 end as ko_ke,
										 case when f_pli.statut_saisie = 7  then count(distinct f_pli.id_pli) else 0 end as ko_circulaire,
										 case when f_pli.statut_saisie = 8  then count( distinct f_pli.id_pli) else 0 end as ko_abondonne,						 
										 case when f_pli.statut_saisie = 9  then count(distinct f_pli.id_pli) else 0 end as ci_editee,
										 case when f_pli.statut_saisie = 10  then count(distinct f_pli.id_pli) else 0 end as ci_envoyee,
										 case when f_pli.statut_saisie = 24  then count(distinct f_pli.id_pli) else 0 end as divise,
										 case when f_pli.statut_saisie = 11  then count(distinct f_pli.id_pli) else 0 end as ok_ci,
										 case when f_pli.statut_saisie = 12  then count(distinct f_pli.id_pli) else 0 end as ko_encours,
										 case when f_pli.statut_saisie = ANY (ARRAY[2,3,4,5,6,7,9,12])  then count(distinct f_pli.id_pli) else 0 end as pli_ko,
										 case when f_pli.statut_saisie = 2   then  sum(mvt_nbr) else 0 end as mvt_ko_scan,					 
										 case when f_pli.statut_saisie = 3  then  sum(mvt_nbr)else 0 end as mvt_ko_def,
										 case when f_pli.statut_saisie = 4  then  sum(mvt_nbr) else 0 end as mvt_ko_inconnu,
										 case when f_pli.statut_saisie = 5 then  sum(mvt_nbr) else 0 end as mvt_ko_ks,
										 case when f_pli.statut_saisie = 6 then  sum(mvt_nbr) else 0 end as mvt_ko_ke,
										 case when  f_pli.statut_saisie = 7  then  sum(mvt_nbr) else 0 end as mvt_ko_circulaire,
										 case when f_pli.statut_saisie = 8  then  sum(mvt_nbr) else 0 end as mvt_ko_abondonne,						 
										 case when f_pli.statut_saisie = 9  then  sum(mvt_nbr) else 0 end as mvt_ci_editee,
										 case when f_pli.statut_saisie = 10  then  sum(mvt_nbr)else 0 end as mvtci_envoyee,
										 case when f_pli.statut_saisie = 24  then  sum(mvt_nbr)else 0 end as mvt_divise,
										 case when f_pli.statut_saisie = 11  then  sum(mvt_nbr)else 0 end as mvt_ok_ci,
										 case when f_pli.statut_saisie = 12  then  sum(mvt_nbr)else 0 end as mvt_ko_encours,
										 case when f_pli.statut_saisie = ANY (ARRAY[2,3,4,5,6,7,9,12]) then  sum(mvt_nbr)else 0 end as mvt_ko
										 FROM  f_pli
											INNER JOIN f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
											INNER JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
											INNER JOIN societe ON societe.id = f_pli.societe	
											WHERE ".$_clauseWhere." AND f_pli.societe = 2											
										GROUP BY f_pli.statut_saisie
								) as data_KO on data_KO.societe_id= data_societe.id_soc
							group by id_soc
							ORDER BY data_societe.id_soc  asc"	;
								//echo "<pre>"; print_r($sql);	echo "</pre>";	exit;
									$res 		= pg_query($conn,$sql) OR die(" erreur dans la requete AFFICHAGE GLOBAL");	
									
									$iNbr   = pg_num_rows($res) or die("Erreur sur la requete => plis KO");
					
									$total_global = 0;
									if ( $iNbr > 0){	
									
										while ($ores = pg_fetch_assoc($res)) {
											
										$id_soc     		= $ores['id_soc'];			 
										$nb_pli_scan    	= $ores['ko_scan'];
										$nb_pli_ks      	= $ores['ko_ks'];
										$nb_pli_kd      	= $ores['ko_def'];
										$nb_pli_ko_inconnu  = $ores['ko_inconnu'];
										$nb_pli_ko_att  	= $ores['ko_ke'];
										$nb_pli_ci      	= $ores['ko_circulaire'];
										$nb_pli_ci_editee 	= $ores['ci_editee'];
										$nb_pli_ci_envoyee  = $ores['ci_envoyee'];
										$nb_pli_abandonne   = $ores['ko_abondonne'];
										$nb_divise  		= $ores['divise'];
										$nb_ok_ci  		    = $ores['ok_ci'];
										$nb_pli_ko_encours  = $ores['ko_encours'];
										$nb_pli_ko  		= $ores['pli_ko'];
										$mouvement_scan    	= $ores['mvt_ko_scan'];
										$mouvement_ks      	= $ores['mvt_ko_ks'];
										$mouvement_kd      	= $ores['mvt_ko_def'];
										$mouvement_ko_inconnu  	= $ores['mvt_ko_inconnu']; // ajouter nouveau
										$mouvement_ko_att  		= $ores['mvt_ko_ke'];
										$mouvement_ci      		= $ores['mvt_ko_circulaire'];
										$mouvement_ci_editee 	= $ores['mvt_ci_editee'];
										$mouvement_ci_envoyee  	= $ores['mvt_ci_envoyee']; // ajouter nouveau
										$mouvement_abandonne  	= $ores['mvt_ko_abondonne']; 
										$mouvement_divise  		= $ores['mvt_divise']; 
										$mouvement_ok_ci  		= $ores['mvt_ok_ci'];
										$mouvement_ko_encours  	= $ores['mvt_ko_encours'];
										$mouvement_ko  			= $ores['mvt_ko'];
										
										echo "******************** Pli KO - Début : ".$semaine." , Fin : ".$date_fin."*******************************</br>";
										echo "</br>Bloc KO</br>";		 
										echo  "--------------</br> KO SCAN : ".$nb_pli_scan." ,</br>KO SRC : ".$nb_pli_ks.", </br>KO DEFINITIF : ".$nb_pli_kd.",</br> KO INCONNU : ".$nb_pli_ko_inconnu."
										 ,</br> KO EN ATTENTE : ".$nb_pli_ko_att.",</br> KO CI : ".$nb_pli_ci.",</br> CI EDITEE : ".$nb_pli_ci_editee.",</br> CI ENVOYEE : ".$nb_pli_ci_envoyee.",</br> KO Abandonne : ".$nb_pli_abandonne.",</br> Total : ".$total."</br>";
										// $titre = 'PLI KO';
										 $titre = 'GLOBAL';
										//--- KO scan
										$rubrique = "ko scan";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_scan."','".$mouvement_scan."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_scan." , nb_mvt = ".$mouvement_scan."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										//--- KO _ci
										$rubrique = "ko circualire";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci."','".$mouvement_ci."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_ci." , nb_mvt = ".$mouvement_ci."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										 
										   //--- KO_ks
										$rubrique = "ko src";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ks."','".$mouvement_ks."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_ks." , nb_mvt = ".$mouvement_ks."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										 
										   //--- KO_kd
										$rubrique = "ko definitif";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_kd."','".$mouvement_kd."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_kd." , nb_mvt = ".$mouvement_kd."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										 
										   //--- KO_inconnu
										$rubrique = "ko inconnu";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ko_inconnu."','".$mouvement_ko_inconnu."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_ko_inconnu." , nb_mvt = ".$mouvement_ko_inconnu."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										  //--- KO_EN ATTENTE
										$rubrique = "ko en attente";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ko_att."','".$mouvement_ko_att."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_ko_att." , nb_mvt = ".$mouvement_ko_att."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										   //--- KO _ci_editee
										$rubrique = "ci éditée";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci_editee."','".$mouvement_ci_editee."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_ci_editee." , nb_mvt = ".$mouvement_ci_editee."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										  //--- KO _ci_envoyee
										$rubrique = "ci envoyée";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci_envoyee."','".$mouvement_ci_envoyee."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_ci_envoyee." , nb_mvt = ".$mouvement_ci_envoyee."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										 
										   //--- KO abandonnee
										$rubrique = "ko abandonné";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_abandonne."','".$mouvement_abandonne."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_abandonne." , nb_mvt = ".$mouvement_abandonne."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										   //--- Divise
										$rubrique = "divise";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												echo "************123*************".$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_divise."','".$mouvement_divise."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												echo "***********456**************".$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_divise." , nb_mvt = ".$mouvement_divise."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }

                                        $rubrique = "ok ci";
                                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
                                        if($cpt_data_total  == 0){
                                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
                                                       VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ok_ci."','".$mouvement_ok_ci."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
                                            $result_insert = pg_query($conn,$insert);
                                        } else
                                        {
                                            $update = "UPDATE stat_hebdo SET nb_pli = ".$nb_ok_ci." , nb_mvt = ".$mouvement_ok_ci."
                                            WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
                                            $result_update = pg_query($conn,$update);
                                        }

                                        $rubrique = "ko en cours";
                                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
                                        if($cpt_data_total  == 0){
                                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
                                                       VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ko_encours."','".$mouvement_ko_encours."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
                                            $result_insert = pg_query($conn,$insert);
                                        } else
                                        {
                                            $update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_ko_encours." , nb_mvt = ".$mouvement_ko_encours."
                                            WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
                                            $result_update = pg_query($conn,$update);
                                        }
										 
										  //--- TOTAL PLI KO 
										$rubrique = "Total KO";
										$cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_pli, nb_mvt, societe,date_debut,date_fin)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ko."','".$mouvement_ko."','".$id_soc."','".$date_deb."' , '".$date_fin."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_hebdo SET nb_pli = ".$nb_pli_ko." , nb_mvt = ".$mouvement_ko."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
																				 
							}
						}
				//#------------------------------------------ FIN STAT MENSUEL ---------------------------------
				//#------------------------------------------ FIN STAT MENSUEL ---------------------------------					
		
				
		}	
	}
		
	// ################################################
	$module_nom  = "CRON DATA - GED BAYARD V2 - RECEPTION HEBDO" ;
	$nom_fichier = "10.90\GED\bayard_v2\crons\cron_data_hebdo.php" ;
	$date_jour   = date("Y-m-d");
	$statut      = "termine" ;

	echo $sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple) 
	VALUES ('$module_nom','$nom_fichier','$date_jour','$statut','$couple') " ;
	$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;

	// ################################################  
?>