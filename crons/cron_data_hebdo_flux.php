<?php
/* ----
	# Suivi des réception mensuel : les Traités, en cours , les KO, les non traités
	# Suivi des nombre de BDC payés par CHQ, CB, Mandat facture et Espèces, BDP
	# Suivi des KO par date de réception mensuelle
	ment
--- */


function getConnBayard() {

    $conn = pg_connect("host=srv-bdd-ged-bayard.madcom.local port=5432 dbname=v2_ged_bayard_push user=si password=51P@vGD24$")
    or die("Impossible de se connecter à ged_bayard ");
    return $conn;
}

$conn  = getConnBayard();


function tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn){

    $sql = "SELECT id_stat FROM stat_hebdo 
					WHERE semaine = '".$semaine."' 
					AND annee = '".$annee."'
					AND titre = '".$titre."' 					
					AND rubrique = '".$rubrique."'
					AND societe  = '".$id_soc."' 
					AND source = '".$id_source."' ";
    $res = @pg_query( $conn,$sql);
    $i   = pg_num_rows($res);

    return $i;
}

function tcheck_data_lot($date_courrier,$id_soc,$id_source,$conn){
    $i = 0;
    $sql = "SELECT nb_flux FROM lot_mensuel 
					WHERE date_reception = '".$date_courrier."' and societe = '".$id_soc."' and source = '".$id_source."' ";
    $res = @pg_query( $conn,$sql);
    $i   = pg_num_rows($res);

    return $i;
}

function update_data_lot($date_courrier,$id_soc,$id_source,$nb,$conn){
    $i = 0;
    $sql = "UPDATE lot_mensuel SET  nb_flux = ".$nb."
					WHERE date_reception = '".$date_courrier."' and societe = '".$id_soc."' and source = '".$id_source."' ";
    $res = @pg_query( $conn,$sql);
}

function insert_data_lot($date_courrier,$id_soc,$id_source,$nb,$conn){
    $insert   = "INSERT INTO lot_mensuel (date_reception, societe,source, nb_flux)
				   VALUES ('".$date_courrier."','".$id_soc."','".$id_source."',".$nb.")";
    $result_insert = pg_query($conn,$insert);

}

function tcheck_traitement_en_cours($date_fin,$conn){
    $list = array();
    $sql = "SELECT 
            MIN(daty) as date_deb_sem,
            MAX(daty) as date_fin_sem,
            sum(coalesce(nb_non_type,0)) as nb_non_type,
            semaine 
            FROM 
            (
                SELECT date_reception,
                daty,
                semaine,nb_non_type 
                FROM 
                (
                    SELECT daty,'S'||week as semaine  FROM liste_weeks (('".$date_fin."'::date - interval '2 months')::date,'".$date_fin."')
                ) as data_daty 
                LEFT JOIN (
                SELECT count(id_flux)  as nb_non_type,date_reception::date as date_reception  
                                      FROM view_stat_flux 
                                      where flux_cloture = 0
                                      and date_reception between ('".$date_fin."'::date  - interval '3 months')::date and '".$date_fin."'
                                     GROUP BY date_reception 
                                     ORDER BY date_reception asc ) data_flux on data_daty.daty = data_flux.date_reception 
            ) tab 
            GROUP BY semaine 
            ORDER BY semaine ASC";
    $res = @pg_query( $conn,$sql);
    $i   = pg_num_rows($res);
    if ($i > 0){
        while ($res_rec = pg_fetch_assoc($res)) {
            $daty = $res_rec['date_deb_sem'];
            $list[$daty]["date_deb_sem"]   = $res_rec['date_deb_sem'];
            $list[$daty]["date_fin_sem"]   = $res_rec['date_fin_sem'];
            $list[$daty]["nb_non_type"]    = $res_rec['nb_non_type'];
            $list[$daty]["semaine"]        = $res_rec['semaine'];
        }
    }
    return $list;
}

function get_lot_by_date($conn,$date_debut,$date_fin,$id_societe,$id_source){
    $list = array();
    $sql = "SELECT date_reception::date AS date_reception, count(distinct flux.id_flux) as nb_flux
					FROM flux 
					WHERE date_reception::date >= '".$date_debut."' and date_reception::date < '".$date_fin."' 
					and societe = '".$id_societe."' and id_source = '".$id_source."' 
					GROUP BY date_reception::date
					ORDER BY  date_reception::date asc
				";
    $res = @pg_query( $conn,$sql);
    $i   = pg_num_rows($res);
    if ($i > 0){
        while ($res_rec = pg_fetch_assoc($res)) {
            $list[$res_rec['date_reception']]["date"]    = $res_rec['date_reception'];
            $list[$res_rec['date_reception']]["flux"]     = $res_rec['nb_flux'];
        }
    }
    return $list;
}


setlocale (LC_TIME, 'fr_FR');

$array_tlmc   = array();
$date_du_jour = date("Y-m-d");

/*
$date_du_jour = "2019-01-10";
$semaine_courant = "2019-01";
$date_debut   = "2019-01-01";
$annee        = "2019";
$semaine 		   = "01";
*/

//#--------------------------------- Récuperer la date du fin de mois -- --------
$d_date_fin = "";
$list       = array();
$sql_date = " SELECT (date_trunc('MONTH', '".$date_du_jour."'::date) + INTERVAL '1 MONTH - 1 day')::DATE as date_fin";
$res_date = @pg_query( $conn,$sql_date) OR die("erreur date de fin de mois !!");
$ifindate = pg_num_rows($res_date);
if ($ifindate > 0){
    while ($ores_date = pg_fetch_assoc($res_date)) {
        $d_date_fin    = $ores_date['date_fin'];
    }
}

$d_date_fin  = ($ifindate > 0) ? $d_date_fin : date("Y-m-d");

//#--------------------------------- Récuperer du nombre de lot par date de courrier - --

// ################################################
$module_nom  = "CRON DATA FLUX - GED BAYARD" ;
$nom_fichier = "10.90\GED\bayard\crons\cron_data_hebdo_flux_.php" ;
$date_jour   = date("Y-m-d");
$couple 	 = date("YmdHis");
$statut      = "debut" ;

$sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple)
		VALUES ('$module_nom','$nom_fichier','$date_jour','$statut','$couple') " ;
$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;

// ################################################
//#--------------------------------- Fin nombre de lot par date de courrier - --------
$list 	   = tcheck_traitement_en_cours($d_date_fin,$conn);
/*echo "<pre>";
print_r($list);
echo "</pre>";
exit;
*/
foreach($list as $klist => $vlist){

    $nb_non_type    = $vlist["nb_non_type"];
    $date_debut     = $vlist["date_deb_sem"];
    $date_fin       = $vlist["date_fin_sem"];
    $semaine        = $vlist["semaine"];
    $annee          = substr($vlist["date_deb_sem"],0,4);

    echo "Reste des flux en cours de traitement ==> non cloturé:".$nb_non_type."</br>";

    if ($nb_non_type > 0 )
    {
                //#------------------------------------------------------- -------------------------
                //#------------------------------------------------------- -------------------------
                //#------------------------------------------------------- -------------------------
                $clauseWhere = $_clauseWhere = "";
                if($date_debut != '' && $date_fin != ''){

                    $clauseWhere 	= "'".$date_debut."' , '".$date_fin."'";
                    $_clauseWhere 	= " date_reception::date between '".$date_debut."' and '".$date_fin."'";
                }
                //#---------- AFFICHAGE GLOBAL : Nouveau, Encours, Traité, Anomalie ,---------------
                //#---------------------------------------------------------------------------------

                $sql = "SELECT
								 id_soc, data_societe.nom_societe,data_societe.id_source, data_societe.source, 
								 abonnement_total , abonnement_non_traite, abo_cloture , abonnement_en_cours, 0 as abonnement_anomalie, 
								 flux_total, nb_flux_non_traite, flux_cloture, nb_flux_en_cours  , nb_flux_anomalie,
								 case when nb_flux_ok is null then '0' else nb_flux_ok end as nb_flux_ok , 
								case when nb_flux_ano is null then '0' else nb_flux_ano end as nb_flux_ano , 
								case when nb_flux_hp is null then '0' else nb_flux_hp end as nb_flux_hp , 
								case when nb_flux_rejete is null then '0' else nb_flux_rejete end as nb_flux_rejete , 
								case when nb_flux_sans_traitement is null then '0' else nb_flux_sans_traitement end as nb_flux_sans_traitement ,  
								case when abonnement_ok is null then '0' else abonnement_ok end as abonnement_ok , 
								case when abonnement_ano is null then '0' else abonnement_ano end as abonnement_ano , 
								case when abonnement_hp is null then '0' else abonnement_hp end as abonnement_hp , 
								case when abonnement_rejete is null then '0' else abonnement_rejete end as abonnement_rejete,
								case when abonnement_sans_traitement is null then '0' else abonnement_sans_traitement end as abonnement_sans_traitement 
							FROM	(
								SELECT 
									societe.id as id_soc, nom_societe, id_source, source, 1 flag
									FROM societe 
								LEFT JOIN (
								SELECT source.id_source, source
									FROM source) as source on 1 = 1 
								)as data_societe
								LEFT JOIN
								(
									SELECT abonnement_total , flux_total , nb_flux_non_traite , abonnement_non_traite , flux_cloture , abo_cloture , 
										nb_flux_en_cours , abonnement_en_cours , nb_flux_anomalie  ,societe, source 
									FROM (
										SELECT   
											case when abonnement_total is null then '0' else abonnement_total end as abonnement_total, 
											flux_total , 
											nb_flux_non_traite , 
											case when abonnement_non_traite is null then '0' else abonnement_non_traite end as abonnement_non_traite,  
											flux_cloture , 
											case when abnmt_cloture is null then '0' else abnmt_cloture end as abo_cloture,  
											nb_flux_en_cours , 
											case when abonnement_en_cours is null then '0' else abonnement_en_cours end as abonnement_en_cours, 
											nb_flux_anomalie  ,
											1 societe ,
											1 source 
										from f_reception_globale( ".$clauseWhere.", 1, 1)
										UNION 
										SELECT  case when abonnement_total is null then '0' else abonnement_total end as abonnement_total, 
											flux_total , 
											nb_flux_non_traite , 
											case when abonnement_non_traite is null then '0' else abonnement_non_traite end as abonnement_non_traite,  
											flux_cloture , 
											case when abnmt_cloture is null then '0' else abnmt_cloture end as abo_cloture,  
											nb_flux_en_cours , 
											case when abonnement_en_cours is null then '0' else abonnement_en_cours end as abonnement_en_cours, 
											nb_flux_anomalie  ,
											1 societe,  
											2 source
											from f_reception_globale( ".$clauseWhere.", 1, 2)
										UNION 
										SELECT  case when abonnement_total is null then '0' else abonnement_total end as abonnement_total, 
											flux_total , 
											nb_flux_non_traite , 
											case when abonnement_non_traite is null then '0' else abonnement_non_traite end as abonnement_non_traite,  
											flux_cloture , 
											case when abnmt_cloture is null then '0' else abnmt_cloture end as abo_cloture,  
											nb_flux_en_cours , 
											case when abonnement_en_cours is null then '0' else abonnement_en_cours end as abonnement_en_cours, 
											nb_flux_anomalie  ,
											2 societe, 
											1 source 
											from f_reception_globale( ".$clauseWhere.", 2, 1)
										UNION 
										SELECT  case when abonnement_total is null then '0' else abonnement_total end as abonnement_total, 
											flux_total , 
											nb_flux_non_traite , 
											case when abonnement_non_traite is null then '0' else abonnement_non_traite end as abonnement_non_traite,  
											flux_cloture , 
											case when abnmt_cloture is null then '0' else abnmt_cloture end as abo_cloture,  
											nb_flux_en_cours , 
											case when abonnement_en_cours is null then '0' else abonnement_en_cours end as abonnement_en_cours, 
											nb_flux_anomalie  ,
											2 societe, 
											2 source 
											from f_reception_globale( ".$clauseWhere.", 2, 2)
									) as data_flux
								)as data on data.societe = data_societe.id_soc and data.source = data_societe.id_source 
								LEFT JOIN(
										SELECT 2 as societe_id,
										1 as source_id, 
										case when nb_flux_ok is null then '0' else nb_flux_ok end as nb_flux_ok , 
										case when nb_flux_ano is null then '0' else nb_flux_ano end as nb_flux_ano , 
										case when nb_flux_hp is null then '0' else nb_flux_hp end as nb_flux_hp , 
										case when nb_flux_rejete is null then '0' else nb_flux_rejete end as nb_flux_rejete ,
										case when nb_flux_sans_traitement is null then '0' else nb_flux_sans_traitement end as nb_flux_sans_traitement ,
										case when abonnement_ok is null then '0' else abonnement_ok end as abonnement_ok , 
										case when abonnement_ano is null then '0' else abonnement_ano end as abonnement_ano , 
										case when abonnement_hp is null then '0' else abonnement_hp end as abonnement_hp , 
										case when abonnement_rejete is null then '0' else abonnement_rejete end as abonnement_rejete,
										case when abonnement_sans_traitement is null then '0' else abonnement_sans_traitement end as abonnement_sans_traitement   
										from f_reception_globale_cloture(  ".$clauseWhere.", 2, 1)
									union
										SELECT 1 as societe_id,
										1 as source_id, 
										case when nb_flux_ok is null then '0' else nb_flux_ok end as nb_flux_ok , 
										case when nb_flux_ano is null then '0' else nb_flux_ano end as nb_flux_ano , 
										case when nb_flux_hp is null then '0' else nb_flux_hp end as nb_flux_hp , 
										case when nb_flux_rejete is null then '0' else nb_flux_rejete end as nb_flux_rejete , 
										case when nb_flux_sans_traitement is null then '0' else nb_flux_sans_traitement end as nb_flux_sans_traitement ,
										case when abonnement_ok is null then '0' else abonnement_ok end as abonnement_ok , 
										case when abonnement_ano is null then '0' else abonnement_ano end as abonnement_ano , 
										case when abonnement_hp is null then '0' else abonnement_hp end as abonnement_hp , 
										case when abonnement_rejete is null then '0' else abonnement_rejete end as abonnement_rejete  ,
										case when abonnement_sans_traitement is null then '0' else abonnement_sans_traitement end as abonnement_sans_traitement   
										from f_reception_globale_cloture(".$clauseWhere.", 1, 1)
									union
										SELECT 2 as societe_id,
										2 as source_id, 
										case when nb_flux_ok is null then '0' else nb_flux_ok end as nb_flux_ok , 
										case when nb_flux_ano is null then '0' else nb_flux_ano end as nb_flux_ano , 
										case when nb_flux_hp is null then '0' else nb_flux_hp end as nb_flux_hp , 
										case when nb_flux_rejete is null then '0' else nb_flux_rejete end as nb_flux_rejete , 
										case when nb_flux_sans_traitement is null then '0' else nb_flux_sans_traitement end as nb_flux_sans_traitement ,
										case when abonnement_ok is null then '0' else abonnement_ok end as abonnement_ok , 
										case when abonnement_ano is null then '0' else abonnement_ano end as abonnement_ano , 
										case when abonnement_hp is null then '0' else abonnement_hp end as abonnement_hp , 
										case when abonnement_rejete is null then '0' else abonnement_rejete end as abonnement_rejete ,
										case when abonnement_sans_traitement is null then '0' else abonnement_sans_traitement end as abonnement_sans_traitement    
										from f_reception_globale_cloture(".$clauseWhere.", 2, 2)
									union
										SELECT 1 as societe_id,
										2 as source_id, 
										case when nb_flux_ok is null then '0' else nb_flux_ok end as nb_flux_ok , 
										case when nb_flux_ano is null then '0' else nb_flux_ano end as nb_flux_ano , 
										case when nb_flux_hp is null then '0' else nb_flux_hp end as nb_flux_hp , 
										case when nb_flux_rejete is null then '0' else nb_flux_rejete end as nb_flux_rejete , 
										case when nb_flux_sans_traitement is null then '0' else nb_flux_sans_traitement end as nb_flux_sans_traitement ,
										case when abonnement_ok is null then '0' else abonnement_ok end as abonnement_ok , 
										case when abonnement_ano is null then '0' else abonnement_ano end as abonnement_ano , 
										case when abonnement_hp is null then '0' else abonnement_hp end as abonnement_hp , 
										case when abonnement_rejete is null then '0' else abonnement_rejete end as abonnement_rejete ,
										case when abonnement_sans_traitement is null then '0' else abonnement_sans_traitement end as abonnement_sans_traitement    
										from f_reception_globale_cloture(".$clauseWhere.", 1, 2)
								) as data_cloture on data_cloture.societe_id= data_societe.id_soc and data_cloture.source_id = data_societe.id_source 
							ORDER BY data_societe.id_soc, data_societe.id_source  asc"	;

                $res 		= pg_query($sql) OR die(" erreur dans la requete AFFICHAGE GLOBAL ");

                $iNbr       = pg_num_rows($res);
                $total_global = 0;
                if ( $iNbr > 0){

                    while ($ores = pg_fetch_assoc($res)) {

                        $id_soc     = $ores['id_soc'];
                        $id_source  = $ores['id_source'];
                        $nouveau    = $ores['nb_flux_non_traite'];
                        $encours    = $ores['nb_flux_en_cours'];
                        $traite     = $ores['flux_cloture'];
                        $anomalie   = $ores['nb_flux_anomalie'];
                        $total   	= $ores['flux_total'];

                        $abo_nouveau    = $ores['abonnement_non_traite'];
                        $abo_encours    = $ores['abonnement_en_cours'];
                        $abo_traite     = $ores['abo_cloture'];
                        $abo_anomalie   = $ores['abonnement_anomalie'];
                        $abo_total   	= $ores['abonnement_total'];

                        $nb_flux_ok      = $ores['nb_flux_ok'];
                        $nb_flux_ano     = $ores['nb_flux_ano'];
                        $nb_flux_hp      = $ores['nb_flux_hp'];
                        $nb_flux_rejete  = $ores['nb_flux_rejete'];
                        $nb_flux_sans_traitement  = $ores['nb_flux_sans_traitement'];
                        //$nb_pli_kd      = $ores['nb_pli_kd'];

                        $abonnement_ok      = $ores['abonnement_ok'];
                        $abonnement_ano     = $ores['abonnement_ano'];
                        $abonnement_hp      = $ores['abonnement_hp'];
                        $abonnement_rejete  = $ores['abonnement_rejete'];
                        $abonnement_sans_trait = $ores['abonnement_sans_traitement'];
                        //$mouvement_kd      = $ores['mouvement_kd'];

                        echo "******************** Flux global - Début : ".$date_debut." , Fin : ".$date_fin."*******************************</br>";
                        echo "</br>GLOBAL</br>";
                        echo  "--------------</br> Nouveau : ".$nouveau." ,</br>Encours : ".$encours.", </br>Traite : ".$traite.",</br> Anomalie : ".$anomalie.",</br> Total : ".$total."</br>";
                        $titre    = "GLOBAL";
                        $rubrique = "Non traité";
                        $cpt_data_nv  	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        //--- Non Traité
                        if($cpt_data_nv  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nouveau."','0','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        }else{
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nouveau." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }
                        //--- Encours
                        $rubrique 		= "Encours";
                        $cpt_data_enc  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_enc  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$encours."','0','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        }else{
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$encours." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }
                        //--- Cloturé
                        $rubrique = "Cloturé";
                        $cpt_data_cloture  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_cloture  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$traite."','0','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        }else{
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$traite." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }
                        //--- Anomalie
                        $rubrique = "Anomalie";
                        $cpt_data_ano  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_ano  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$anomalie."','0','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$anomalie." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }

                        //--- Total reçu
                        $rubrique = "Total";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$total."','0','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$total." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        //--- traité_ok
                        $rubrique = "Traité OK";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_flux_ok."','".$abonnement_ok."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_flux_ok." , nb_abo = ".$abonnement_ok."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }

                        //--- Anomalie
                        $rubrique = "Anomalie";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_flux_ano."','".$abonnement_ano."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_flux_ano." , nb_abo = ".$abonnement_ano."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }

                        //--- traité_hors_perimetre
                        $rubrique = "Traité HP";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_flux_hp."','".$abonnement_hp."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_flux_hp." , nb_abo = ".$abonnement_hp."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }

                        //--- traité_rejeté
                        $rubrique = "Traité Rejeté";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_flux_rejete."','".$abonnement_rejete."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_flux_rejete." , nb_abo = ".$abonnement_rejete."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }

                        //--- traité_rejeté
                        $rubrique = "Sans traitement";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_flux_sans_traitement."','".$abonnement_sans_trait."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_flux_sans_traitement." , nb_abo = ".$abonnement_sans_trait."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }


                        echo "******************** Abonnement global - Début : ".$date_debut." , Fin : ".$date_fin."*******************************</br>";
                        echo "</br>GLOBAL</br>";
                        echo  "--------------</br> Abo Nouveau : ".$abo_nouveau." ,</br>Abo Encours : ".$abo_encours.", </br>Abo traite : ".$traite.",</br> Abo Anomalie : ".$abo_anomalie.",</br> Abo Total : ".$abo_total."</br>";
                        $titre    = "GLOBAL";
                        $rubrique = "Non traité";
                        //$mvt_rubrique = "Mvt Non traité";

                        $cpt_data_nv  	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        //--- Non Traité
                        if($cpt_data_nv  > 0){
                            $update = "UPDATE stat_hebdo SET nb_abo = ".$abo_nouveau." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }
                        //--- Encours
                        $rubrique 		= "Encours";
                        $cpt_data_enc  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_enc  > 0){

                            $update = "UPDATE stat_hebdo SET nb_abo = ".$abo_encours." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }
                        //--- Cloturé
                        $rubrique = "Cloturé";
                        $cpt_data_cloture  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_cloture  > 0){
                            $update = "UPDATE stat_hebdo SET nb_abo = ".$abo_traite." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }
                        //--- Anomalie
                        $rubrique = "Anomalie";
                        $cpt_data_ano  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_ano  > 0){
                            $update = "UPDATE stat_hebdo SET nb_abo = ".$abo_anomalie." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }

                        //--- Total Mouvement reçu
                        $rubrique = "Total";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total >0)
                        {
                            $update = "UPDATE stat_hebdo SET nb_abo = ".$abo_total." 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }



                    }


                }
                //#------------------------------------------------------- -------------------------
                //#-------------------- Nombre de flux par typologie ,---------------------
                //#------------------------------------------------------------------------
                /*$conn  = getConnBayard();
                $sql_typo = "SELECT
									 id_soc,id_source,id_typo, 
									 case when nb_flux_par_typo is null then 0 else nb_flux_par_typo end as nb_flux_par_typo,
									 case when nb_abo_par_typo is null then 0 else nb_abo_par_typo end as nb_abo_par_typo,
									 data_typologie_societe.typologie, data_typologie_societe.nom_societe
								FROM	(
									SELECT data_typologie.id_typologie as id_typo, data_typologie.typologie, data_societe.id as id_soc, data_societe.nom_societe, id_source, source 
									FROM
										(
										SELECT 
											id_typologie, typologie,1 flag
										FROM typologie
										) as data_typologie
										LEFT JOIN
										(
										SELECT 
											id, nom_societe,1 flag
										FROM societe
										) as data_societe on data_societe.flag = data_typologie.flag 
										LEFT JOIN 
										(
											SELECT id_source, source, 1 flag 
											FROM source 
										) as data_source on data_source.flag = data_societe.flag 
									)as data_typologie_societe
									LEFT JOIN
									(
										SELECT case when typologie is null then 'Non définie' else typologie end as typologie , 
										case when nb_flux_par_typo is null then 0 else nb_flux_par_typo end as nb_flux_par_typo,
										case when nb_abnmt_par_typo is null then 0 else nb_abnmt_par_typo end as nb_abo_par_typo,
										societe, 
										source, 
										case when id_typologie is null then 0 else id_typologie end as id_typologie
										FROM (
											SELECT typologie, nb_flux_par_typo, nb_abnmt_par_typo,1 societe,1 source, id_typologie from f_flux_par_typologie( ".$clauseWhere.", 1,1)
											UNION 
											SELECT typologie, nb_flux_par_typo, nb_abnmt_par_typo,1 societe,2 source, id_typologie from f_flux_par_typologie( ".$clauseWhere.", 1,2)
											UNION 
											SELECT typologie, nb_flux_par_typo, nb_abnmt_par_typo,2 societe,1 source, id_typologie from f_flux_par_typologie( ".$clauseWhere.", 2,1)
											UNION 
											SELECT typologie, nb_flux_par_typo, nb_abnmt_par_typo,2 societe,2 source, id_typologie from f_flux_par_typologie( ".$clauseWhere.", 2,2)
										) as data_flux
									)as data on data.id_typologie = data_typologie_societe.id_typo and data.societe = data_typologie_societe.id_soc and data_typologie_societe.id_source = data.source 
								WHERE nb_flux_par_typo >0 or nb_flux_par_typo > 0 
								ORDER BY data_typologie_societe.id_soc, data_typologie_societe.id_typo asc 
								";

                $res_typo  = pg_query( $conn,$sql_typo) OR die(pg_last_error());
                $iNbr_typo = (pg_num_rows($res_typo) == null) ? 0 : pg_num_rows($res_typo);// OR die("Nombre de flux par typologie");

                if ( $iNbr_typo > 0){

                    echo "</br>Flux par typologie</br>";
                    echo "------------------------------";
                    $total_flux = 0;
                    $total_abo = 0;
                    while ($ores_typo = pg_fetch_assoc($res_typo)) {

                        $id_soc     = $ores_typo['id_soc'];
                        $id_source  = $ores_typo['id_source'];
                        $nb_flux_par_typo       = $ores_typo['nb_flux_par_typo'];
                        $nb_abo_par_typo       = $ores_typo['nb_abo_par_typo'];
                        $id_typo   			= $ores_typo['id_typo']." - ".$ores_typo['typologie'];
                        $total_flux += $nb_flux_par_typo;
                        $total_abo  += $nb_abo_par_typo;

                        echo  " </br>id_typo : ".$id_typo.",</br>nb_flux_par_typo : ".$nb_flux_par_typo." ,</br>nb_abo_par_typo : ".$nb_abo_par_typo.",  </br>id_soc : ".$id_soc."</br>";

                        $titre    = "Flux par typologie";
                        $rubrique = $id_typo;
                        $cpt_data  	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);


                        //--- Flux avec typologie
                        if($cpt_data  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_flux_par_typo."','".$nb_abo_par_typo."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        }else{
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_flux_par_typo." , nb_abo = '".$nb_abo_par_typo."'
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }


                    }
                    $rubrique = 'Total';
                    if($cpt_data  == 0){
                        $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$total_flux."','".$total_abo."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                        $result_insert = pg_query($conn,$insert);
                    }else{
                        $update = "UPDATE stat_hebdo SET nb_flux = ".$total_flux." , nb_abo = '".$total_abo."'
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                        $result_update = pg_query($conn,$update);
                    }



                }
                //#---------- FIN Nombre de flux par typologie ---------------------
                //#---------------- Flux cloturés   -----------------------------
                $sql_plis_cltr = "SELECT
											 id_soc,id_source,id_typo,
											 case when nb_flux_par_typo is null then 0 else nb_flux_par_typo end as nb_flux_par_typo,
											 case when nb_abo_par_typo is null then 0 else nb_abo_par_typo end as nb_abo_par_typo,
											 data_typologie_societe.typologie, data_typologie_societe.nom_societe
										FROM	(
											SELECT data_typologie.id_typologie as id_typo, data_typologie.typologie, data_societe.id as id_soc, data_societe.nom_societe, data_source.id_source 
											FROM
												(
												SELECT 
													id_typologie, typologie,1 flag
												FROM typologie
												) as data_typologie
												LEFT JOIN
												(
												SELECT 
													id, nom_societe,1 flag
												FROM societe
												) as data_societe on data_societe.flag = data_typologie.flag 
												LEFT JOIN 
												(
													SELECT id_source, source,1 flag 
													FROM source 
												) as data_source on data_source.flag = data_societe.flag 
											)as data_typologie_societe
											LEFT JOIN
											(
												SELECT case when typologie is null then 'Non définie' else typologie end as typologie , 
												case when nb_flux_par_typo is null then 0 else nb_flux_par_typo end as nb_flux_par_typo,
												case when nb_abnmt_par_typo is null then 0 else nb_abnmt_par_typo end as nb_abo_par_typo,
												societe, 
												source, 
												case when id_typologie is null then 0 else id_typologie end as id_typologie
												FROM (
													SELECT typologie, nb_flux_par_typo, nb_abnmt_par_typo,1 societe,1 source, id_typologie from f_flux_cloture( ".$clauseWhere.", 1, 1)
													UNION 
													SELECT typologie, nb_flux_par_typo, nb_abnmt_par_typo,1 societe,2 source, id_typologie from f_flux_cloture( ".$clauseWhere.", 1, 2)
													UNION 
													SELECT typologie, nb_flux_par_typo, nb_abnmt_par_typo,2 societe,1 source, id_typologie from f_flux_cloture( ".$clauseWhere.", 2, 1)
													UNION 
													SELECT typologie, nb_flux_par_typo, nb_abnmt_par_typo,2 societe,2 source, id_typologie from f_flux_cloture( ".$clauseWhere.", 2, 2)
												) as data_flux
											)as data on data.id_typologie = data_typologie_societe.id_typo and data.societe = data_typologie_societe.id_soc and data.source = data_typologie_societe.id_source 
										WHERE nb_flux_par_typo >0 or nb_abo_par_typo > 0 
										ORDER BY data_typologie_societe.id_soc, data_typologie_societe.id_typo asc
									";

                $res_cltr  = pg_query( $conn,$sql_plis_cltr) OR die(pg_last_error());
                $iNbr_cltr = (pg_num_rows($res_cltr) == null) ? 0 : pg_num_rows($res_cltr);

                if ( $iNbr_cltr > 0){

                    $total_flux = $total_abo = 0;
                    echo "</br>Nombre de flux CLOTURES</br>";
                    echo "------------------------------";
                    while ($ores_cltr = pg_fetch_assoc($res_cltr)) {

                        $id_soc     			  = $ores_cltr['id_soc'];
                        $id_source                = $ores_cltr['id_source'];
                        $id_typo     			  = $ores_cltr['id_typo']." - ".$ores_cltr['typologie'];
                        $nb_flux_par_typo     	  = $ores_cltr['nb_flux_par_typo'];
                        $nb_abo_par_typo     	  = $ores_cltr['nb_abo_par_typo'];
                        $total_flux += $nb_flux_par_typo;
                        $total_abo += $nb_abo_par_typo;

                        echo  " </br>id_soc : ".$id_soc." ,</br>id_typo : ".$id_typo.", </br>nb_flux_par_typo : ".$nb_flux_par_typo.", </br>nb_abo_par_typo : ".$nb_abo_par_typo."</br>";

                        $titre    = "Flux cloturés";
                        $rubrique = $id_typo;
                        $cpt_data  	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);

                        //--- Flux avec typologie
                        if($cpt_data  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_flux_par_typo."','".$nb_abo_par_typo."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        }else{
                            $update = "UPDATE stat_hebdo SET  nb_flux = ".$nb_flux_par_typo." , nb_abo = '".$nb_abo_par_typo."' 
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."'  and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }

                    }
                    $rubrique = 'Total';
                    if($cpt_data  == 0){
                        $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$total_flux."','".$total_abo."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                        $result_insert = pg_query($conn,$insert);
                    }else{
                        $update = "UPDATE stat_hebdo SET nb_flux = ".$total_flux." , nb_abo = '".$total_abo."'
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                        $result_update = pg_query($conn,$update);
                    }
                }
                //#------------------------------------------------------- -------------------------
                //#----------------------- Nombre de flux en cours ------------------------------------
                $sql_plis_encours = "SELECT
											 id_soc,id_source, libelle,abo, flux, data_societe.nom_societe
										FROM	(
											SELECT 
												societe.id as id_soc, nom_societe,id_source, 1 flag
												FROM societe 
												LEFT JOIN 
												(
													SELECT id_source, source 
													FROM source 
												) as data_source on 1 = 1 
											)as data_societe
											LEFT JOIN
											(
												SELECT libelle, abnmt as abo, flux, societe, source 
												FROM (
													SELECT  libelle,abnmt, flux ,1 societe,1 source from f_flux_en_cours( ".$clauseWhere.", 1,1)
													UNION 
													SELECT  libelle,abnmt, flux ,1 societe,2 source from f_flux_en_cours( ".$clauseWhere.", 1,2)
													UNION 
													SELECT  libelle,abnmt, flux ,2 societe,1 source from f_flux_en_cours( ".$clauseWhere.", 2,1)
													UNION 
													SELECT  libelle,abnmt, flux ,2 societe,2 source from f_flux_en_cours( ".$clauseWhere.", 2,2)
												) as data_pli
											)as data on data.societe = data_societe.id_soc and data.source = data_societe.id_source 
										ORDER BY data_societe.id_soc ,libelle asc";

                $res_encours  = pg_query( $conn,$sql_plis_encours) OR die(pg_last_error());
                $iNbr_encours = pg_num_rows($res_encours) or die("Erreur sur la requete => Nombre de flux en cours ");

                if ( $iNbr_encours > 0){
                    $total_flux = $total_abo = 0;
                    echo "</br>Flux en cours </br>";
                    echo "------------------------------";
                    while ($ores_encours = pg_fetch_assoc($res_encours)) {

                        $id_soc     	= $ores_encours['id_soc'];
                        $id_source     	= $ores_encours['id_source'];
                        $libelle     	= $ores_encours['libelle'];
                        $flux     		= $ores_encours['flux'];
                        $abo     		= $ores_encours['abo'];
                        $total_flux 	+= $ores_encours['flux'];
                        $total_abo 	+= $ores_encours['abo'];
                        echo  " </br>id_soc : ".$id_soc." ,</br>libelle : ".$libelle.", </br>flux : ".$flux.", </br>abo : ".$abo.",</br>";

                        $titre    = "Flux en cours";
                        $rubrique = $libelle;
                        $cpt_data_koinc  	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);

                        //
                        if($cpt_data_koinc  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$flux."','".$abo."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        }else{
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$flux." , nb_abo = ".$abo."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }

                    }
                    //--- Total KO
                    $rubrique = "Total";
                    $cpt_data_totalko 	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);

                    if($cpt_data_totalko  == 0){
                        $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$total_flux."','".$total_abo."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                        $result_insert = pg_query($conn,$insert);
                    }else{
                        $update = "UPDATE stat_hebdo SET nb_flux = '".$total_flux."', nb_abo = '".$total_abo."'
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                        $result_update = pg_query($conn,$update);
                    }
                }

                //#------------------------------------------------------- -------------------------
                //#----------------------- Suivi des flux restant à traiter ------------------
                $sql_plis_encours ="SELECT
										 id_soc,id_source,flux_encours,flux_non_traite, abo_encours, abo_non_traite, data_societe.nom_societe
									FROM	(
											SELECT 
											societe.id as id_soc, nom_societe,1 flag, id_source 
											FROM societe 
											LEFT JOIN 
											( 
												SELECT id_source, source 
												FROM source 
											) as data_source on 1 = 1 
										)as data_societe
										LEFT JOIN
										(
											SELECT sum(flux_encours) as flux_encours,sum(flux_non_traite) as flux_non_traite, sum(abo_encours) as abo_encours, sum(abo_non_traite) as abo_non_traite, societe, source 
											FROM (
												SELECT  
													case when flux_en_cours = 1 then count (view_stat_flux.id_flux) else 0 end as flux_encours, 
													case when flux_non_traite = 1 then count (view_stat_flux.id_flux) else 0 end as flux_non_traite,
													case when flux_en_cours = 1 then sum (nb_abo) else 0 end as abo_encours, 
													0 as abo_non_traite,				
													1 as societe,
													1 as source 
												from view_stat_flux 
												left join  view_nb_abonnement on view_nb_abonnement.id_flux = view_stat_flux.id_flux 
												WHERE ".$_clauseWhere." 
												and societe = '1' and id_source = '1' and (flux_en_cours = 1 or flux_non_traite = 1 )
												GROUP BY flux_en_cours,flux_non_traite
												union 
												SELECT  
													case when flux_en_cours = 1 then count (view_stat_flux.id_flux) else 0 end as flux_encours, 
													case when flux_non_traite = 1 then count (view_stat_flux.id_flux) else 0 end as flux_non_traite,
													case when flux_en_cours = 1 then sum (nb_abo) else 0 end as abo_encours, 
													0 as abo_non_traite,				
													1 as societe, 
												    2 as source 
												from view_stat_flux 
												left join  view_nb_abonnement on view_nb_abonnement.id_flux = view_stat_flux.id_flux 
												WHERE ".$_clauseWhere."
												and societe = '1' and id_source = '2'  and (flux_en_cours = 1 or flux_non_traite = 1 )
												GROUP BY flux_en_cours,flux_non_traite
												union 
												SELECT  
													case when flux_en_cours = 1 then count (view_stat_flux.id_flux) else 0 end as flux_encours, 
													case when flux_non_traite = 1 then count (view_stat_flux.id_flux) else 0 end as flux_non_traite,
													case when flux_en_cours = 1 then sum (nb_abo) else 0 end as abo_encours, 
													0 as abo_non_traite,				
													2 as societe,
													1 as source 
												from view_stat_flux 
												left join  view_nb_abonnement on view_nb_abonnement.id_flux = view_stat_flux.id_flux 
												WHERE ".$_clauseWhere."
												and societe = '2' and id_source = '1' and (flux_en_cours = 1 or flux_non_traite = 1 )
												GROUP BY flux_en_cours,flux_non_traite
												union 
												SELECT  
													case when flux_en_cours = 1 then count (view_stat_flux.id_flux) else 0 end as flux_encours, 
													case when flux_non_traite = 1 then count (view_stat_flux.id_flux) else 0 end as flux_non_traite,
													case when flux_en_cours = 1 then sum (nb_abo) else 0 end as abo_encours, 
													0 as abo_non_traite,				
													2 as societe, 
													2 as source 
												from view_stat_flux 
												left join  view_nb_abonnement on view_nb_abonnement.id_flux = view_stat_flux.id_flux 
												WHERE ".$_clauseWhere."
												and societe = '2' and id_source = '2' and (flux_en_cours = 1 or flux_non_traite = 1 )
												GROUP BY flux_en_cours,flux_non_traite
											) as data_flux
											GROUP BY societe,source 
										)as data on data.societe = data_societe.id_soc and data.source = data_societe.id_source 
									ORDER BY data_societe.id_soc asc";

                $res_encours  = pg_query( $conn,$sql_plis_encours) OR die(pg_last_error());
                $iNbr_encours = pg_num_rows($res_encours) or die("Erreur sur la requete => Restant à traiter ");
                $total_encours = 0;
                if ( $iNbr_encours > 0){

                    echo "</br>Nombre de flux en cours </br>";
                    echo "------------------------------";
                    while ($ores_encours = pg_fetch_assoc($res_encours)) {

                        $id_soc     		= $ores_encours['id_soc'];
                        $id_source     		= $ores_encours['id_source'];
                        $nouveau			= ($ores_encours['flux_non_traite'] == null) ? 0 : $ores_encours['flux_non_traite'];
                        $encours			= ($ores_encours['flux_encours'] == null) ? 0 : $ores_encours['flux_encours'];
                        $abo_encours		= ($ores_encours['abo_encours'] == null) ? 0 : $ores_encours['abo_encours'];
                        $abo_nouveau		= ($ores_encours['abo_non_traite'] == null) ? 0 : $ores_encours['abo_non_traite'];

                        echo  " </br>id_soc : ".$id_soc." ,</br>nouveau : ".$nouveau.", </br>abo_nouveau : ".$abo_nouveau.", </br>encours : ".$encours.",</br> abo_encours : ".$abo_encours."</br>";
                        //--- non traité
                        $titre    = "Restant à traiter";
                        $rubrique = "Non traité";
                        $cpt_data_ko_nv 	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);

                        if($cpt_data_ko_nv  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nouveau."','".$abo_nouveau."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        }else{
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nouveau." , nb_abo = ".$abo_nouveau."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }

                        //--- En cours
                        $rubrique = "En cours";
                        $cpt_data_encours 	  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);

                        if($cpt_data_encours  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
										   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$encours."','".$abo_encours."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        }else{
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$encours." , nb_abo = ".$abo_encours."
								WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."' ";
                            $result_update = pg_query($conn,$update);
                        }
                    }
                }
                */
                //#-----------------------------Nouveau tableau  ------------------------
                //#-------------------------- Suivi des flux KO ------------------------
                $sql = "SELECT
								 id_soc,
								 id_source,
								 coalesce(SUM(flux_ko),0) as flux_ko,
								 coalesce(SUM(abnmt_ko),0) as abnmt_ko ,
								case when SUM(ko_mail) is null then 0 else SUM(ko_mail) end as ko_mail,
								case when SUM(ko_pj) is null then 0 else SUM(ko_pj) end as ko_pj,
								case when SUM(ko_fichier) is null then 0 else SUM(ko_fichier) end as ko_fichier,
								case when SUM(ko_src) is null then 0 else SUM(ko_src) end as ko_src,
								case when SUM(ko_definitif) is null then 0 else SUM(ko_definitif) end as ko_definitif,
								case when SUM(ko_inconnu) is null then 0 else SUM(ko_inconnu) end as ko_inconnu,
								case when SUM(ko_attente) is null then 0 else SUM(ko_attente) end as ko_attente,
								case when SUM(ko_rejete) is null then 0 else SUM(ko_rejete) end as ko_rejete,
								case when SUM(ko_hp) is null then 0 else SUM(ko_hp) end as ko_hp, 
								case when SUM(abnmt_ko_mail) is null then 0 else SUM(abnmt_ko_mail) end as abnmt_ko_mail,
								case when SUM(abnmt_ko_pj) is null then 0 else SUM(abnmt_ko_pj) end as abnmt_ko_pj,
								case when SUM(abnmt_ko_fichier) is null then 0 else SUM(abnmt_ko_fichier) end as abnmt_ko_fichier,
								case when SUM(abnmt_ko_src) is null then 0 else SUM(abnmt_ko_src) end as abnmt_ko_src,
								case when SUM(abnmt_ko_definitif) is null then 0 else SUM(abnmt_ko_definitif) end as abnmt_ko_definitif,
								case when SUM(abnmt_ko_inconnu) is null then 0 else SUM(abnmt_ko_inconnu) end as abnmt_ko_inconnu,
								case when SUM(abnmt_ko_attente) is null then 0 else SUM(abnmt_ko_attente) end as abnmt_ko_attente,
								case when SUM(abnmt_ko_rejete) is null then 0 else SUM(abnmt_ko_rejete) end as abnmt_ko_rejete,
								case when SUM(abnmt_ko_hp) is null then 0 else SUM(abnmt_ko_hp) end as abnmt_ko_hp
							FROM	(
								SELECT 
									societe.id as id_soc, nom_societe,1 flag, id_source 
									FROM societe 
									LEFT JOIN 
									( 
										SELECT id_source, source 
										FROM source 
									) as data_source on 1 = 1 
								)as data_societe 
								
								LEFT JOIN(
										SELECT 1 as societe,
										1 as source,
										 case when flux.etat_pli_id = 11   then count(distinct flux.id_flux) else 0 end as ko_mail,					 
										 case when flux.etat_pli_id = 12  then count( distinct flux.id_flux) else 0 end as ko_pj,
										 case when flux.etat_pli_id = 14  then count( distinct flux.id_flux) else 0 end as ko_fichier,
										 case when flux.etat_pli_id = 19 then count(distinct flux.id_flux) else 0 end as ko_src,
										 case when flux.etat_pli_id = 21 then count(distinct flux.id_flux) else 0 end as ko_definitif,
										 case when flux.etat_pli_id = 22  then count(distinct flux.id_flux) else 0 end as ko_inconnu,
										 case when flux.etat_pli_id = 23  then count( distinct flux.id_flux) else 0 end as ko_attente,						 
										 case when flux.etat_pli_id = 15  then count(distinct flux.id_flux) else 0 end as ko_rejete,
										 case when flux.etat_pli_id = 13  then count(distinct flux.id_flux) else 0 end as ko_hp,
										 case when flux.etat_pli_id = ANY (ARRAY[11,12,14,19,21,22,23,15,13])  then count(distinct flux.id_flux) else 0 end as flux_ko,
										 case when flux.etat_pli_id = 11   then  sum(nb_abo) else 0 end as abnmt_ko_mail,					 
										 case when flux.etat_pli_id = 12  then  sum(nb_abo)else 0 end as abnmt_ko_pj,
										 case when flux.etat_pli_id = 14  then  sum(nb_abo) else 0 end as abnmt_ko_fichier,
										 case when flux.etat_pli_id = 19 then  sum(nb_abo) else 0 end as abnmt_ko_src,
										 case when flux.etat_pli_id = 21 then  sum(nb_abo) else 0 end as abnmt_ko_definitif,
										 case when  flux.etat_pli_id = 22  then  sum(nb_abo) else 0 end as abnmt_ko_inconnu,
										 case when flux.etat_pli_id = 23  then  sum(nb_abo) else 0 end as abnmt_ko_attente,						 
										 case when flux.etat_pli_id = 15  then  sum(nb_abo) else 0 end as abnmt_ko_rejete,
										 case when flux.etat_pli_id = 13  then  sum(nb_abo)else 0 end as abnmt_ko_hp,
										 case when flux.etat_pli_id = ANY (ARRAY[11,12,14,19,21,22,23,15,13])   then  sum(nb_abo)else 0 end as abnmt_ko
										 FROM  flux
											LEFT JOIN view_nb_abonnement va ON va.id_flux = flux.id_flux 	
											WHERE   ".$_clauseWhere."  AND flux.societe = 1 AND id_source = 1										
										GROUP BY flux.etat_pli_id
											
									union
										SELECT 2 as societe,
										1 as source,
										 case when flux.etat_pli_id = 11   then count(distinct flux.id_flux) else 0 end as ko_mail,					 
										 case when flux.etat_pli_id = 12  then count( distinct flux.id_flux) else 0 end as ko_pj,
										 case when flux.etat_pli_id = 14  then count( distinct flux.id_flux) else 0 end as ko_fichier,
										 case when flux.etat_pli_id = 19 then count(distinct flux.id_flux) else 0 end as ko_src,
										 case when flux.etat_pli_id = 21 then count(distinct flux.id_flux) else 0 end as ko_definitif,
										 case when flux.etat_pli_id = 22  then count(distinct flux.id_flux) else 0 end as ko_inconnu,
										 case when flux.etat_pli_id = 23  then count( distinct flux.id_flux) else 0 end as ko_attente,						 
										 case when flux.etat_pli_id = 15  then count(distinct flux.id_flux) else 0 end as ko_rejete,
										 case when flux.etat_pli_id = 13  then count(distinct flux.id_flux) else 0 end as ko_hp,
										 case when flux.etat_pli_id = ANY (ARRAY[11,12,14,19,21,22,23,15,13])  then count(distinct flux.id_flux) else 0 end as flux_ko,
										 case when flux.etat_pli_id = 11   then  sum(nb_abo) else 0 end as abnmt_ko_mail,					 
										 case when flux.etat_pli_id = 12  then  sum(nb_abo)else 0 end as abnmt_ko_pj,
										 case when flux.etat_pli_id = 14  then  sum(nb_abo) else 0 end as abnmt_ko_fichier,
										 case when flux.etat_pli_id = 19 then  sum(nb_abo) else 0 end as abnmt_ko_src,
										 case when flux.etat_pli_id = 21 then  sum(nb_abo) else 0 end as abnmt_ko_definitif,
										 case when  flux.etat_pli_id = 22  then  sum(nb_abo) else 0 end as abnmt_ko_inconnu,
										 case when flux.etat_pli_id = 23  then  sum(nb_abo) else 0 end as abnmt_ko_attente,						 
										 case when flux.etat_pli_id = 15  then  sum(nb_abo) else 0 end as abnmt_ko_rejete,
										 case when flux.etat_pli_id = 13  then  sum(nb_abo)else 0 end as abnmt_ko_hp,
										 case when flux.etat_pli_id = ANY (ARRAY[11,12,14,19,21,22,23,15,13])   then  sum(nb_abo)else 0 end as abnmt_ko
										 FROM  flux
											LEFT JOIN view_nb_abonnement va ON va.id_flux = flux.id_flux 	
											WHERE   ".$_clauseWhere."  AND flux.societe = 2 and id_source = 1											
										GROUP BY flux.etat_pli_id
									union 
									SELECT 1 as societe,
									2 as source,
										 case when flux.etat_pli_id = 11   then count(distinct flux.id_flux) else 0 end as ko_mail,					 
										 case when flux.etat_pli_id = 12  then count( distinct flux.id_flux) else 0 end as ko_pj,
										 case when flux.etat_pli_id = 14  then count( distinct flux.id_flux) else 0 end as ko_fichier,
										 case when flux.etat_pli_id = 19 then count(distinct flux.id_flux) else 0 end as ko_src,
										 case when flux.etat_pli_id = 21 then count(distinct flux.id_flux) else 0 end as ko_definitif,
										 case when flux.etat_pli_id = 22  then count(distinct flux.id_flux) else 0 end as ko_inconnu,
										 case when flux.etat_pli_id = 23  then count( distinct flux.id_flux) else 0 end as ko_attente,						 
										 case when flux.etat_pli_id = 15  then count(distinct flux.id_flux) else 0 end as ko_rejete,
										 case when flux.etat_pli_id = 13  then count(distinct flux.id_flux) else 0 end as ko_hp,
										 case when flux.etat_pli_id = ANY (ARRAY[11,12,14,19,21,22,23,15,13])  then count(distinct flux.id_flux) else 0 end as flux_ko,
										 case when flux.etat_pli_id = 11   then  sum(nb_abo) else 0 end as abnmt_ko_mail,					 
										 case when flux.etat_pli_id = 12  then  sum(nb_abo)else 0 end as abnmt_ko_pj,
										 case when flux.etat_pli_id = 14  then  sum(nb_abo) else 0 end as abnmt_ko_fichier,
										 case when flux.etat_pli_id = 19 then  sum(nb_abo) else 0 end as abnmt_ko_src,
										 case when flux.etat_pli_id = 21 then  sum(nb_abo) else 0 end as abnmt_ko_definitif,
										 case when  flux.etat_pli_id = 22  then  sum(nb_abo) else 0 end as abnmt_ko_inconnu,
										 case when flux.etat_pli_id = 23  then  sum(nb_abo) else 0 end as abnmt_ko_attente,						 
										 case when flux.etat_pli_id = 15  then  sum(nb_abo) else 0 end as abnmt_ko_rejete,
										 case when flux.etat_pli_id = 13  then  sum(nb_abo)else 0 end as abnmt_ko_hp,
										 case when flux.etat_pli_id = ANY (ARRAY[11,12,14,19,21,22,23,15,13])   then  sum(nb_abo)else 0 end as abnmt_ko
										 FROM  flux
											LEFT JOIN view_nb_abonnement va ON va.id_flux = flux.id_flux 
											WHERE   ".$_clauseWhere."  AND flux.societe = 1 and id_source = 2											
										GROUP BY flux.etat_pli_id
											
									union
										SELECT 2 as societe,
									2 as source,
										 case when flux.etat_pli_id = 11   then count(distinct flux.id_flux) else 0 end as ko_mail,					 
										 case when flux.etat_pli_id = 12  then count( distinct flux.id_flux) else 0 end as ko_pj,
										 case when flux.etat_pli_id = 14  then count( distinct flux.id_flux) else 0 end as ko_fichier,
										 case when flux.etat_pli_id = 19 then count(distinct flux.id_flux) else 0 end as ko_src,
										 case when flux.etat_pli_id = 21 then count(distinct flux.id_flux) else 0 end as ko_definitif,
										 case when flux.etat_pli_id = 22  then count(distinct flux.id_flux) else 0 end as ko_inconnu,
										 case when flux.etat_pli_id = 23  then count( distinct flux.id_flux) else 0 end as ko_attente,						 
										 case when flux.etat_pli_id = 15  then count(distinct flux.id_flux) else 0 end as ko_rejete,
										 case when flux.etat_pli_id = 13  then count(distinct flux.id_flux) else 0 end as ko_hp,
										 case when flux.etat_pli_id = ANY (ARRAY[11,12,14,19,21,22,23,15,13])  then count(distinct flux.id_flux) else 0 end as flux_ko,
										 case when flux.etat_pli_id = 11   then  sum(nb_abo) else 0 end as abnmt_ko_mail,					 
										 case when flux.etat_pli_id = 12  then  sum(nb_abo)else 0 end as abnmt_ko_pj,
										 case when flux.etat_pli_id = 14  then  sum(nb_abo) else 0 end as abnmt_ko_fichier,
										 case when flux.etat_pli_id = 19 then  sum(nb_abo) else 0 end as abnmt_ko_src,
										 case when flux.etat_pli_id = 21 then  sum(nb_abo) else 0 end as abnmt_ko_definitif,
										 case when  flux.etat_pli_id = 22  then  sum(nb_abo) else 0 end as abnmt_ko_inconnu,
										 case when flux.etat_pli_id = 23  then  sum(nb_abo) else 0 end as abnmt_ko_attente,						 
										 case when flux.etat_pli_id = 15  then  sum(nb_abo) else 0 end as abnmt_ko_rejete,
										 case when flux.etat_pli_id = 13  then  sum(nb_abo)else 0 end as abnmt_ko_hp,
										 case when flux.etat_pli_id = ANY (ARRAY[11,12,14,19,21,22,23,15,13])   then  sum(nb_abo)else 0 end as abnmt_ko
										 FROM  flux
											LEFT JOIN view_nb_abonnement va ON va.id_flux = flux.id_flux 
	
											WHERE ".$_clauseWhere."  AND flux.societe = 2 and id_source = 2											
										GROUP BY flux.etat_pli_id
								) as data_KO on data_KO.societe= data_societe.id_soc AND data_KO.source = data_societe.id_source 
							group by id_soc,id_source 
							ORDER BY data_societe.id_soc  asc"	;
                //echo "<pre>"; print_r($sql);	echo "</pre>";	exit;
                $res 		= pg_query($conn,$sql) OR die(" erreur dans la requete AFFICHAGE GLOBAL");

                $iNbr   = pg_num_rows($res) or die("Erreur sur la requete => flux KO");

                $total_global = 0;
                if ( $iNbr > 0){

                    while ($ores = pg_fetch_assoc($res)) {

                        $id_soc             = $ores['id_soc'];
                        $id_source          = $ores['id_source'];
                        $nb_flux_ko         = $ores['flux_ko'];
                        $nb_ko_mail         = $ores['ko_mail'];
                        $nb_ko_pj           = $ores['ko_pj'];
                        $nb_ko_fichier      = $ores['ko_fichier'];
                        $nb_ko_src          = $ores['ko_src'];
                        $nb_ko_definitif  	= $ores['ko_definitif'];
                        $nb_ko_inconnu      = $ores['ko_inconnu'];
                        $nb_ko_attente 	    = $ores['ko_attente'];
                        $nb_ko_rejete       = $ores['ko_rejete'];
                        $nb_ko_hp           = $ores['ko_hp'];
                        $abo_ko             = $ores['abnmt_ko'];
                        $abo_ko_mail        = $ores['abnmt_ko_mail'];
                        $abo_ko_pj          = $ores['abnmt_ko_pj'];
                        $abo_ko_fichier     = $ores['abnmt_ko_fichier'];
                        $abo_ko_src         = $ores['abnmt_ko_src'];
                        $abo_ko_definitif   = $ores['abnmt_ko_definitif'];
                        $abo_ko_inconnu  	= $ores['abnmt_ko_inconnu']; // ajouter nouveau
                        $abo_ko_attente  	= $ores['abnmt_ko_attente'];
                        $abo_ko_rejete      = $ores['abnmt_ko_rejete'];
                        $abo_ko_hp 	        = $ores['abnmt_ko_hp'];

                        echo "******************** Flux KO - Début : ".$date_debut." , Fin : ".$date_fin."*******************************</br>";
                        echo "</br>Bloc KO</br>";
                        echo  "--------------</br> ANOMALIE MAIL : ".$nb_ko_mail." ,</br>ANOMALIE PJ : ".$nb_ko_pj.", </br>ANOMALIE FICHIER : ".$nb_ko_fichier.",</br> KO TANSFERT SRC : ".$nb_ko_src."
										 ,</br> KO DEFINITIF : ".$nb_ko_definitif.",</br> KO INCONNU : ".$nb_ko_definitif.",</br> KO INCONNU : ".$nb_ko_inconnu.",</br> KO EN ATTENTE : ".$nb_ko_attente.",</br> REJETE : ".$nb_ko_rejete.",</br> Total : ".$nb_flux_ko."</br>";
                        $titre = 'FLUX KO';
                        //--- KO scan
                        $rubrique = "KO MAIL";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_mail."','".$abo_ko_mail."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_ko_mail." , nb_abo = ".$abo_ko_mail."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        $rubrique = "KO PJ";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_pj."','".$abo_ko_pj."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_ko_pj." , nb_abo = ".$abo_ko_pj."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        $rubrique = "KO FICHIER";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_fichier."','".$abo_ko_fichier."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_ko_fichier." , nb_abo = ".$abo_ko_fichier."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        $rubrique = "KO SRC";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_src."','".$abo_ko_src."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_ko_src." , nb_abo = ".$abo_ko_src."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        $rubrique = "KO DEFINITIF";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_definitif."','".$abo_ko_definitif."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_ko_definitif." , nb_abo = ".$abo_ko_definitif."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        $rubrique = "KO INCONNU";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_inconnu."','".$abo_ko_inconnu."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_ko_inconnu." , nb_abo = ".$abo_ko_inconnu."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        $rubrique = "KO EN ATTENTE";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_attente."','".$abo_ko_attente."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_ko_attente." , nb_abo = ".$abo_ko_attente."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        $rubrique = "KO REJETE";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_rejete."','".$abo_ko_rejete."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_ko_rejete." , nb_abo = ".$abo_ko_rejete."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        $rubrique = "KO HP";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_hp."','".$abo_ko_hp."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_ko_hp." , nb_abo = ".$abo_ko_hp."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                        //--- TOTAL PLI KO
                        $rubrique = "TOTAL FLUX KO";
                        $cpt_data_total  = tcheck_data($semaine, $annee, $titre,$rubrique,$id_soc,$id_source,$conn);
                        if($cpt_data_total  == 0){
                            $insert   = "INSERT INTO stat_hebdo (semaine, annee, titre,rubrique, nb_flux, nb_abo, societe,date_debut,date_fin,source)
														   VALUES ('".$semaine."','".$annee."','".$titre."','".$rubrique."','".$nb_flux_ko."','".$abo_ko."','".$id_soc."','".$date_debut."','".$date_fin."','".$id_source."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_hebdo SET nb_flux = ".$nb_flux_ko." , nb_abo = ".$abo_ko."
												WHERE semaine = '".$semaine."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' and source = '".$id_source."'";
                            $result_update = pg_query($conn,$update);
                        }

                    }
                }
                //#------------------------------------------ FIN STAT MENSUEL ---------------------------------
                //#------------------------------------------ FIN STAT MENSUEL ---------------------------------
                //#------------------------------------------ FIN STAT MENSUEL ---------------------------------


            }

}

// ################################################
$module_nom  = "CRON DATA FLUX - GED BAYARD" ;
$nom_fichier = "10.90\GED\bayard\crons\cron_data_flux.php" ;
$date_jour   = date("Y-m-d");
$statut      = "termine" ;

echo $sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple)
	VALUES ('$module_nom','$nom_fichier','$date_jour','$statut','$couple') " ;
$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;

// ################################################
?>