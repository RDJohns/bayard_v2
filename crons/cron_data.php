﻿<?php 
/* ----
	# Suivi des réception mensuel : les Traités, en cours , les KO, les non traités
	# Suivi des nombre de BDC payés par CHQ, CB, Mandat facture et Espèces, BDP
	# Suivi des KO par date de réception mensuelle
	ment
	#Lancer toutes les 30 min 
--- */ 	


		function getConnBayard() {
			
				$conn = pg_connect("host=srv-bdd-ged-bayard.madcom.local port=5432 dbname=v2_ged_bayard user=si password=51P@vGD24$")
				or die("Impossible de se connecter à v2_ged_bayard ");				
				return $conn;
		}		
		
		$conn  = getConnBayard(); 		
		
		
		function tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn){
			
			$sql = "SELECT id_stat FROM stat_mensuel 
					WHERE mois = '".$mois."' 
					AND annee = '".$annee."'
					AND titre = '".$titre."' 					
					AND rubrique = '".$rubrique."'
					AND societe  = '".$id_soc."' ";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			
			return $i;			
		}
		
		function tcheck_data_lot($date_courrier,$id_soc,$conn){
			$i = 0;
			$sql = "SELECT nb_pli FROM lot_mensuel 
					WHERE date_courrier = '".$date_courrier."' and societe = '".$id_soc."' ";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);
			
			return $i;			
		}
		
		function update_data_lot($date_courrier,$id_soc,$nb,$conn){
			$i = 0;
			$sql = "UPDATE lot_mensuel SET  nb_pli = ".$nb."
					WHERE date_courrier = '".$date_courrier."' and societe = '".$id_soc."'";
			$res = @pg_query( $conn,$sql);						
		}
		
		function insert_data_lot($date_courrier,$id_soc,$nb,$conn){
			$insert   = "INSERT INTO lot_mensuel (date_courrier, societe, nb_pli)
				   VALUES ('".$date_courrier."','".$id_soc."',".$nb.")";
			$result_insert = pg_query($conn,$insert);
			
		}
								
		function tcheck_traitement_en_cours($date_fin,$conn){
			$list = array();
			$sql = "SELECT count(id_pli)  as nb_non_type,
					substr(date_courrier::text,1,7) as mois 
					  FROM pli_stat
					  where pli_cloture = 0
					  and date_courrier between ('".$date_fin."'::date  - interval '3 months')::date and '".$date_fin."'
					 GROUP BY substr(date_courrier::text,1,7)
					 ORDER BY mois asc
					";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			if ($i > 0){				
					while ($res_rec = pg_fetch_assoc($res)) {				
					$list[$res_rec['mois']]["nb_non_type"]    = $res_rec['nb_non_type'];	
					$list[$res_rec['mois']]["mois"]           = $res_rec['mois'];	
					}
				}
			return $list;			
		}
		
		function get_lot_by_date($conn,$date_debut,$date_fin,$id_societe){
			$list = array();
			$sql = "SELECT date_courrier::date AS date_courrier, count(distinct view_pli.id_pli) as nb_pli
					FROM view_pli 
					LEFT JOIN view_lot on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation 
					WHERE date_courrier::date >= '".$date_debut."' and date_courrier::date < '".$date_fin."' 
					and societe = '".$id_societe."'/* and view_pli.flag_traitement != 24*/
					GROUP BY date_courrier::date
					ORDER BY  date_courrier::date asc
				";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			if ($i > 0){				
					while ($res_rec = pg_fetch_assoc($res)) {				
					$list[$res_rec['date_courrier']]["date"]    = $res_rec['date_courrier'];	
					$list[$res_rec['date_courrier']]["pli"]     = $res_rec['nb_pli'];	
					}
				}
			return $list;
		}
		
		
		setlocale (LC_TIME, 'fr_FR');	

		$array_tlmc   = array();
		$date_du_jour = date("Y-m-d");
		
		/*
		$date_du_jour = "2019-01-10";
		$mois_courant = "2019-01";
		$date_debut   = "2019-01-01";		
		$annee        = "2019";
		$mois 		   = "01";
		*/
		
		//#--------------------------------- Récuperer la date du fin de mois -- --------
		$d_date_fin = "";
		$list       = array();
		$sql_date = " SELECT (date_trunc('MONTH', '".$date_du_jour."'::date) + INTERVAL '1 MONTH - 1 day')::DATE as date_fin";
		$res_date = @pg_query( $conn,$sql_date) OR die("erreur date de fin de mois !!");
		$ifindate = pg_num_rows($res_date);
		if ($ifindate > 0){				
			while ($ores_date = pg_fetch_assoc($res_date)) {				
			$d_date_fin    = $ores_date['date_fin'];	
			}
		}
		
		$date_fin  = ($ifindate > 0) ? $d_date_fin : date("Y-m-d");
		
		//#--------------------------------- Récuperer du nombre de lot par date de courrier - --
		
		$list_societe = array('1','2'); // bayard et milan
		foreach($list_societe as $key_soc){
			
			$list_lot     = get_lot_by_date($conn,date("Y-m")."-01",$date_fin,$key_soc);
			$cpt_pli_date = 0;		
			
			foreach($list_lot as $kdate=>$vdate){ 
			
				$daty_courrier = $vdate["date"];
				 $pli          = $vdate["pli"];
				 
				 $i_nb_pli  = tcheck_data_lot($daty_courrier, $key_soc,$conn);
				if($pli > 0 && $i_nb_pli != $pli && $i_nb_pli > 0 ){
					 update_data_lot($daty_courrier,$key_soc,$pli,$conn);
				}
				
				if($pli > 0 && $i_nb_pli != $pli && $i_nb_pli == 0 ){
					insert_data_lot($daty_courrier,$key_soc,$pli,$conn);
				}
				
			}
		}	
		
		
		// ################################################
		$module_nom  = "CRON DATA - GED BAYARD V2" ;
		$nom_fichier = "10.90\GED\bayard_v2\crons\cron_data.php" ;
		$date_jour   = date("Y-m-d");
		$couple 	 = date("YmdHis");
		$statut      = "debut" ;

		$sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple) 
		VALUES ('$module_nom','$nom_fichier','$date_jour','$statut','$couple') " ;
		$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;

		// ################################################  
		//#--------------------------------- Fin nombre de lot par date de courrier - --------
		$list 	   = tcheck_traitement_en_cours($date_fin,$conn);
		/*echo "<pre>";
		print_r($list);
		echo "</pre>";
		exit;
		*/
		foreach($list as $klist => $vlist){
			
			$nb_non_type = $vlist["nb_non_type"];
			$mois_annee  = $vlist["mois"];
						
			echo "Reste des plis en cours de traitement ==> non cloturé:".$nb_non_type."</br>";
			
			if ($nb_non_type > 0 )
			{
				$mois_courant = $mois_annee;
				$date_debut   = $mois_annee.'-01';		
				$annee   	  = substr($mois_annee,0,4);
				$mois 	      = substr($mois_annee,5,2);
				//$date_du_jour = "2019-05-16";
				
				$sql_date 	  = " SELECT (date_trunc('MONTH', '".$date_debut."'::date) + INTERVAL '1 MONTH - 1 day')::DATE as date_fin";
				$res_date 	  = @pg_query( $conn,$sql_date) OR die("erreur date de fin de mois !!");
				$ifindate 	  = pg_num_rows($res_date);
				
				if ($ifindate > 0){	
				
					while ($ores_date = pg_fetch_assoc($res_date)) {				
							$date_fin = $ores_date['date_fin'];	
							$list_lot = array();
					//#------------------------------------------------------- -------------------------
					//#------------------------------------------------------- -------------------------
					//#------------------------------------------------------- -------------------------		
					$clauseWhere = $_clauseWhere = "";
					if($date_debut != '' && $date_fin != ''){
						
						$clauseWhere 	= "'".$date_debut."' , '".$date_fin."'";
						$_clauseWhere 	= " date_courrier between '".$date_debut."' and '".$date_fin."'";
					}		
					//#---------- AFFICHAGE GLOBAL : Nouveau, Encours, Traité, Anomalie ,---------------
					//#---------------------------------------------------------------------------------		
					
					$sql = "SELECT
								 id_soc, data_societe.nom_societe,
								 mouvement_total , mouvement_non_traite, mvt_cloture , mouvement_en_cours, 
								 pli_total, nb_pli_non_traite, pli_cloture, nb_pli_en_cours  , nb_pli_anomalie, nb_pli_ferme,
								 nb_pli_divise, ci_traite,mvt_anomalie ,mvt_ferme ,	mvt_divise, mvt_ci_traite,
								 case when nb_pli_ok is null then '0' else nb_pli_ok end as nb_pli_ok , 
								case when nb_pli_ano is null then '0' else nb_pli_ano end as nb_pli_ano , 
								case when nb_pli_ci is null then '0' else nb_pli_ci end as nb_pli_ci , 
								case when nb_pli_ks is null then '0' else nb_pli_ks end as nb_pli_ks , 
								case when nb_pli_kd is null then '0' else nb_pli_kd end as nb_pli_kd , 
								case when pli_ci_editee is null then '0' else pli_ci_editee end as nb_pli_ci_editee , 
								case when nb_pli_ci_envoyee is null then '0' else nb_pli_ci_envoyee end as nb_pli_ci_envoyee , 
								case when nb_pli_kd is null then '0' else nb_pli_kd end as nb_pli_kd ,
								case when mouvement_ok is null then '0' else mouvement_ok end as mouvement_ok , 
								case when mouvement_ano is null then '0' else mouvement_ano end as mouvement_ano , 
								case when mouvement_ci is null then '0' else mouvement_ci end as mouvement_ci , 
								case when mvt_ci_editee is null then '0' else mvt_ci_editee end as mvt_ci_editee ,
								case when mouvement_ci_envoyee is null then '0' else mouvement_ci_envoyee end as mouvement_ci_envoyee , 
								case when mouvement_ks is null then '0' else mouvement_ks end as mouvement_ks , 
								case when mouvement_kd is null then '0' else mouvement_kd end as mouvement_kd,
								case when mouvement_ferme is null then '0' else mouvement_ferme end as mouvement_ferme,
								case when mouvement_divise is null then '0' else mouvement_divise end as mouvement_divise
							FROM	(
								SELECT 
									societe.id as id_soc, nom_societe,1 flag
									FROM societe
								)as data_societe
								LEFT JOIN
								(
									SELECT 
										coalesce(mouvement_total::integer,0) mouvement_total , 
										coalesce(pli_total::integer,0)pli_total, 
										coalesce(nb_pli_non_traite::integer,0) nb_pli_non_traite , 
										coalesce(mouvement_non_traite::integer,0) mouvement_non_traite , 
										coalesce(pli_cloture::integer,0) pli_cloture, 
										coalesce(mvt_cloture::integer,0)mvt_cloture , 
										coalesce(nb_pli_en_cours::integer,0) nb_pli_en_cours ,
										coalesce(mouvement_en_cours::integer,0) mouvement_en_cours, 
										coalesce(nb_pli_anomalie::integer,0) nb_pli_anomalie , 
										coalesce(nb_pli_ferme::integer,0) nb_pli_ferme , 
										coalesce(nb_pli_divise::integer,0) nb_pli_divise, 
										coalesce(ci_traite::integer,0) ci_traite ,
										coalesce(mvt_anomalie::integer,0) mvt_anomalie,	
										coalesce(mvt_ferme::integer,0) mvt_ferme,	
										coalesce(mvt_divise::integer,0)mvt_divise, 
										coalesce(mvt_ci_traite::integer,0) mvt_ci_traite,
										coalesce(pli_ci_editee::integer,0)pli_ci_editee,
										coalesce(mvt_ci_editee::integer,0) mvt_ci_editee,
										societe
									FROM (
										SELECT   
											pli_total , 
											nb_pli_non_traite , 											 
											nb_pli_en_cours , 											
											nb_pli_anomalie  ,
											nb_pli_ferme ,
											nb_pli_divise, 
											pli_ci_editee,
											nb_pli_ci_traite as ci_traite, 
											case when mouvement_en_cours is null then '0' else mouvement_en_cours end as mouvement_en_cours, 
											case when mouvement_non_traite is null then '0' else mouvement_non_traite end as mouvement_non_traite,  
											pli_cloture , 
											case when mvt_cloture is null then '0' else mvt_cloture end as mvt_cloture,
											case when mouvement_anomalie is null then '0' else mouvement_anomalie end as mvt_anomalie,
											case when mvt_ferme is null then '0' else mvt_ferme end as mvt_ferme,
											case when mvt_divise is null then '0' else mvt_divise end as mvt_divise,
											case when mvt_ci_editee is null then '0' else mvt_ci_editee end as mvt_ci_editee,
											case when mvt_ci_traite is null then '0' else mvt_ci_traite end as mvt_ci_traite,
											case when mouvement_total is null then '0' else mouvement_total end as mouvement_total,											
											2 societe 
										from f_reception_globale( ".$clauseWhere.", 2)
										UNION 
										SELECT pli_total , 
											nb_pli_non_traite , 											 
											nb_pli_en_cours , 											
											nb_pli_anomalie  ,
											nb_pli_ferme ,
											nb_pli_divise, 
											pli_ci_editee,
											nb_pli_ci_traite as ci_traite,
											case when mouvement_en_cours is null then '0' else mouvement_en_cours end as mouvement_en_cours, 
											case when mouvement_non_traite is null then '0' else mouvement_non_traite end as mouvement_non_traite,  
											pli_cloture , 
											case when mvt_cloture is null then '0' else mvt_cloture end as mvt_cloture,
											case when mouvement_anomalie is null then '0' else mouvement_anomalie end as mvt_anomalie,
											case when mvt_ferme is null then '0' else mvt_ferme end as mvt_ferme,
											case when mvt_divise is null then '0' else mvt_divise end as mvt_divise,
											case when mvt_ci_editee is null then '0' else mvt_ci_editee end as mvt_ci_editee,
											case when mvt_ci_traite is null then '0' else mvt_ci_traite end as mvt_ci_traite,
											case when mouvement_total is null then '0' else mouvement_total end as mouvement_total,											
											1 societe 
											from f_reception_globale( ".$clauseWhere.", 1)
									) as data_pli
								)as data on data.societe = data_societe.id_soc
								LEFT JOIN(
										SELECT 2 as societe_id,
										case when nb_pli_ok is null then '0' else nb_pli_ok end as nb_pli_ok , 
										case when nb_pli_ano is null then '0' else nb_pli_ano end as nb_pli_ano , 
										case when nb_pli_ci is null then '0' else nb_pli_ci end as nb_pli_ci , 
										case when nb_pli_ci_envoyee is null then '0' else nb_pli_ci_envoyee end as nb_pli_ci_envoyee , 
										case when nb_pli_ks is null then '0' else nb_pli_ks end as nb_pli_ks , 
										case when nb_pli_kd is null then '0' else nb_pli_kd end as nb_pli_kd ,  
										case when mouvement_ok is null then '0' else mouvement_ok end as mouvement_ok , 
										case when mouvement_ano is null then '0' else mouvement_ano end as mouvement_ano , 
										case when mouvement_ci is null then '0' else mouvement_ci end as mouvement_ci , 
										case when mouvement_ci_envoyee is null then '0' else mouvement_ci_envoyee end as mouvement_ci_envoyee ,
										case when mouvement_ks is null then '0' else mouvement_ks end as mouvement_ks , 
										case when mouvement_kd is null then '0' else mouvement_kd end as mouvement_kd ,										
										case when mouvement_ferme is null then '0' else mouvement_ferme end as mouvement_ferme , 
										case when mouvement_divise is null then '0' else mouvement_divise end as mouvement_divise 
										from f_reception_globale_cloture(  ".$clauseWhere.",2)
									union
										SELECT 1 as societe_id,
										case when nb_pli_ok is null then '0' else nb_pli_ok end as nb_pli_ok , 
										case when nb_pli_ano is null then '0' else nb_pli_ano end as nb_pli_ano , 
										case when nb_pli_ci is null then '0' else nb_pli_ci end as nb_pli_ci , 
										case when nb_pli_ci_envoyee is null then '0' else nb_pli_ci_envoyee end as nb_pli_ci_envoyee , 
										case when nb_pli_ks is null then '0' else nb_pli_ks end as nb_pli_ks , 
										case when nb_pli_kd is null then '0' else nb_pli_kd end as nb_pli_kd ,   										
										case when mouvement_ok is null then '0' else mouvement_ok end as mouvement_ok , 
										case when mouvement_ano is null then '0' else mouvement_ano end as mouvement_ano , 
										case when mouvement_ci is null then '0' else mouvement_ci end as mouvement_ci , 
										case when mouvement_ci_envoyee is null then '0' else mouvement_ci_envoyee end as mouvement_ci_envoyee , 
										case when mouvement_ks is null then '0' else mouvement_ks end as mouvement_ks , 
										case when mouvement_kd is null then '0' else mouvement_kd end as mouvement_kd ,																			
										case when mouvement_ferme is null then '0' else mouvement_ferme end as mouvement_ferme , 
										case when mouvement_divise is null then '0' else mouvement_divise end as mouvement_divise 
										from f_reception_globale_cloture(".$clauseWhere.",1)
								) as data_cloture on data_cloture.societe_id= data_societe.id_soc
							ORDER BY data_societe.id_soc  asc"	;
					/*echo "<pre>";
					print_r($sql);
					echo "</pre>";*/
					$res 		= pg_query($sql) OR die(" erreur dans la requete AFFICHAGE GLOBAL");	
					
					$iNbr       = pg_num_rows($res);
					$total_global = 0;
					if ( $iNbr > 0){	
					
						while ($ores = pg_fetch_assoc($res)) {
							
						$id_soc     = $ores['id_soc'];			 
						$nouveau    = $ores['nb_pli_non_traite'];			 
						$encours    = $ores['nb_pli_en_cours'];			 
						$traite     = $ores['pli_cloture'];			 
						$anomalie   = $ores['nb_pli_anomalie'];
						$ferme      = $ores['nb_pli_ferme'];
						$divise     = $ores['nb_pli_divise'];
						$ci_traite  = $ores['ci_traite'];
						//$ok_ci  = $ores['nb_pli_ok_ci'];

                         $total   	 = $ores['pli_total'];

						$mvt_nouveau    = $ores['mouvement_non_traite'];			 
						$mvt_encours    = $ores['mouvement_en_cours'];			 
						$mvt_traite     = $ores['mvt_cloture'];			 
						$mvt_anomalie   = $ores['mvt_anomalie'];  	
						$mvt_ferme      = $ores['mvt_ferme'];  	
						$mvt_divise     = $ores['mvt_divise'];  	
						$mvt_ci_traite  = $ores['mvt_ci_traite'];
						$mvt_ci_editee    = $ores['mvt_ci_editee'];						
						//$mvt_ok_ci    = $ores['mvt_ok_ci'];
						$mvt_total   	= $ores['mouvement_total'];
						
						$nb_pli_ok      = $ores['nb_pli_ok'];
						$nb_pli_ano     = $ores['nb_pli_ano'];
						$nb_pli_ci      = $ores['nb_pli_ci'];
						$nb_pli_ks      = $ores['nb_pli_ks'];
						$nb_pli_kd      = $ores['nb_pli_kd'];
						$nb_pli_ko_inconnu  = $ores['nb_pli_ko_inconnu'];
						$nb_pli_ko_att  	= $ores['nb_pli_ko_att'];
						$nb_pli_ci_editee 	= $ores['nb_pli_ci_editee'];
						$nb_pli_ci_envoyee  = $ores['nb_pli_ci_envoyee'];
						$nb_pli_ferme  		= $ores['nb_pli_ferme'];
						$nb_pli_divise  	= $ores['nb_pli_divise'];
						
						$mouvement_ok      = $ores['mouvement_ok'];
						$mouvement_ano     = $ores['mouvement_ano'];
						$mouvement_ci      = $ores['mouvement_ci'];
						$mouvement_ks      = $ores['mouvement_ks'];
						$mouvement_kd      = $ores['mouvement_kd'];
						$mouvement_ko_inconnu  	= $ores['mouvement_ko_inconnu']; // ajouter nouveau
						$mouvement_ko_att  		= $ores['mouvement_ko_att'];
						$mouvement_ci_envoyee  	= $ores['mouvement_ci_envoyee']; 
						$mouvement_ferme  		= $ores['mouvement_ferme']; 
						$mouvement_divise  		= $ores['mouvement_divise']; 

						 echo "******************** Pli global - Début : ".$date_debut." , Fin : ".$date_fin."*******************************</br>";
						 echo "</br>GLOBAL</br>";		 
						 echo  "--------------</br> Nouveau : ".$nouveau." ,</br>Encours : ".$encours.", </br>Traite : ".$traite.",</br> Anomalie : ".$anomalie.",</br> Total : ".$total."</br>";
						 $titre    = "GLOBAL";
						 $rubrique = "Non traité";
						 $cpt_data_nv  	  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 //--- Non Traité
						 if($cpt_data_nv  == 0){					
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nouveau."','0','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_mensuel SET nb_pli = ".$nouveau." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Encours
						 $rubrique 		= "Encours";
						 $cpt_data_enc  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_enc  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$encours."','0','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_mensuel SET nb_pli = ".$encours." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Cloturé
						$rubrique = "Cloturé";
						$cpt_data_cloture  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_cloture  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$traite."','0','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_mensuel SET nb_pli = ".$traite." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Anomalie
						$rubrique = "Anomalie";
						$cpt_data_ano  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_ano  == 0){
								echo $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$anomalie."','".$mvt_anomalie."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								echo $update = "UPDATE stat_mensuel SET nb_pli = ".$anomalie." , nb_mvt = ".$mvt_anomalie."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }

                        //--- Fermé
                        $rubrique = "Fermé";
                        $cpt_data_ferme  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
                        if($cpt_data_ferme  == 0){
                            $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
                                       VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$ferme."','".$mvt_ferme."','".$id_soc."','".$date_debut."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_mensuel SET nb_pli = ".$ferme." , nb_mvt ='".$mvt_ferme."'
                            WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
                            $result_update = pg_query($conn,$update);
                        }
						
					//--- Divisé
                        $rubrique = "Divisé";
                        $cpt_data_divise  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
                        if($cpt_data_divise  == 0){
                            $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
                                       VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$divise."','".$mvt_divise."','".$id_soc."','".$date_debut."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_mensuel SET nb_pli = ".$divise." , nb_mvt = '".$mvt_divise."'
                            WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
                            $result_update = pg_query($conn,$update);
						}
					//--- Total reçu
						$rubrique = "Total";
						$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$total."','0','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_mensuel SET nb_pli = ".$total." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
						//--- CI Traité
                        $rubrique = "CI traité";
                       $cpt_data_traite  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
                        if($cpt_data_traite  == 0){
                            $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
                                       VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$ci_traite."','".$mvt_ci_traite."','".$id_soc."','".$date_debut."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_mensuel SET nb_pli = ".$ci_traite." , nb_mvt = '".$mvt_ci_traite."'
                            WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
                            $result_update = pg_query($conn,$update);
						}
						//--- CI Editée
                        $rubrique = "CI editée";
                        $cpt_data_editee  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
                        if($cpt_data_editee  == 0){
                            $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
                                       VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci_editee."','".$mvt_ci_editee."','".$id_soc."','".$date_debut."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                             $update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ci_editee." , nb_mvt = '".$mvt_ci_editee."'
                            WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
                            $result_update = pg_query($conn,$update);
						}

						//--- CI Envoyée
                        $rubrique = "CI envoyée";
                        $cpt_data_envoyee  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
                        if($cpt_data_envoyee  == 0){
                            $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
                                       VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci_envoyee."','".$mouvement_ci_envoyee."','".$id_soc."','".$date_debut."')";
                            $result_insert = pg_query($conn,$insert);
                        } else
                        {
                            $update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ci_envoyee." , nb_mvt = '".$mouvement_ci_envoyee."'
                            WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
                            $result_update = pg_query($conn,$update);
						}
						
						 //--- traité_ok
						$rubrique = "Traité OK";
						$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ok."','".$mouvement_ok."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ok." , nb_mvt = ".$mouvement_ok."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
						 //--- traité_ko
						$rubrique = "KO";
						$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ano."','".$mouvement_ano."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ano." , nb_mvt = ".$mouvement_ano."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
						  //--- traité_ci
						$rubrique = "Traité CI";
						$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci."','".$mouvement_ci."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ci." , nb_mvt = ".$mouvement_ci."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
						   //--- traité_ks
						$rubrique = "Traité KS";
						$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ks."','".$mouvement_ks."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ks." , nb_mvt = ".$mouvement_ks."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
						   //--- traité_kd
						$rubrique = "Traité KD";
						$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_kd."','".$mouvement_kd."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_kd." , nb_mvt = ".$mouvement_kd."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
						   //--- traité_ko_inconnu
						$rubrique = "Traité KO INCONNU";
						$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ko_inconnu."','".$mouvement_ko_inconnu."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ko_inconnu." , nb_mvt = ".$mouvement_ko_inconnu."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						  //--- traité_ci_envoyee
						$rubrique = "Traité CI ENVOYEE";
						$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total  == 0){
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci_envoyee."','".$mouvement_ci_envoyee."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 } else 
						 {
								$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ci_envoyee." , nb_mvt = ".$mouvement_ci_envoyee."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
						echo "******************** Mouvement global - Début : ".$date_debut." , Fin : ".$date_fin."*******************************</br>";
						 echo "</br>GLOBAL</br>";		 
						 echo  "--------------</br> Mvt Nouveau : ".$mvt_nouveau." ,</br>Mvt Encours : ".$mvt_encours.", </br>Mvt raite : ".$traite.",</br> Mvt Anomalie : ".$mvt_anomalie.",</br> Mvt Total : ".$mvt_total."</br>";
						 $mvt_traite    = "GLOBAL";
						 $rubrique = "Non traité";
						 //$mvt_rubrique = "Mvt Non traité";
						 
						 $cpt_data_nv  	  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 //--- Non Traité
						 if($cpt_data_nv  > 0){					
								$update = "UPDATE stat_mensuel SET nb_mvt = ".$mvt_nouveau." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Encours
						 $rubrique 		= "Encours";
						 $cpt_data_enc  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_enc  > 0){
							
								$update = "UPDATE stat_mensuel SET nb_mvt = ".$mvt_encours." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Cloturé
						$rubrique = "Cloturé";
						$cpt_data_cloture  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_cloture  > 0){
								$update = "UPDATE stat_mensuel SET nb_mvt = ".$mvt_traite." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						 //--- Anomalie
						$rubrique = "Anomalie";
						$cpt_data_ano  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_ano  > 0){
							$update = "UPDATE stat_mensuel SET nb_mvt = ".$mvt_anomalie." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }

						//--- Total Mouvement reçu
						$rubrique = "Total";
						$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 if($cpt_data_total >0)
						 {
								$update = "UPDATE stat_mensuel SET nb_mvt = ".$mvt_total." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }			 
						
						
						
						}
						
						
					}
					//#------------------------------------------------------- -------------------------
					//#-------------------- Nombre de plis par typologie ,---------------------
					//#------------------------------------------------------------------------	
					$conn  = getConnBayard(); 		
					$sql_typo = "SELECT
									 id_soc,id_typo, 
									 case when nb_pli_par_typo is null then 0 else nb_pli_par_typo end as nb_pli_par_typo,
									 case when nb_mvt_par_typo is null then 0 else nb_mvt_par_typo end as nb_mvt_par_typo,
									 data_typologie_societe.typologie, data_typologie_societe.nom_societe
								FROM	(
									SELECT data_typologie.id as id_typo, data_typologie.typologie, data_societe.id as id_soc, data_societe.nom_societe
									FROM
										(
										SELECT 
											id, typologie,1 flag
										FROM typologie
										) as data_typologie
										LEFT JOIN
										(
										SELECT 
											id, nom_societe,1 flag
										FROM societe
										) as data_societe on data_societe.flag = data_typologie.flag
									)as data_typologie_societe
									LEFT JOIN
									(
										SELECT case when typologie is null then 'Non définie' else typologie end as typologie , 
										case when nb_pli_par_typo is null then 0 else nb_pli_par_typo end as nb_pli_par_typo,
										case when nb_mvt_par_typo is null then 0 else nb_mvt_par_typo end as nb_mvt_par_typo,
										societe,
										case when id_typologie is null then 0 else id_typologie end as id_typologie
										FROM (
											SELECT typologie, nb_pli_par_typo, nb_mvt_par_typo,2 societe,id_typologie from f_pli_par_typologie( ".$clauseWhere.", 2)
											UNION 
											SELECT typologie, nb_pli_par_typo, nb_mvt_par_typo,1 societe,id_typologie from f_pli_par_typologie( ".$clauseWhere.", 1)
										) as data_pli
									)as data on data.id_typologie = data_typologie_societe.id_typo and data.societe = data_typologie_societe.id_soc
								WHERE nb_pli_par_typo >0 or nb_mvt_par_typo > 0 
								ORDER BY data_typologie_societe.id_soc, data_typologie_societe.id_typo asc
								";	
						/*echo "<pre>";
						print_r($sql_typo);
						echo "</pre>";*/
					$res_typo  = pg_query( $conn,$sql_typo) OR die(pg_last_error());
					$iNbr_typo = (pg_num_rows($res_typo) == null) ? 0 : pg_num_rows($res_typo);// OR die("Nombre de plis par typologie");
										
					if ( $iNbr_typo > 0){	
						
						echo "</br>Pli par typologie</br>";
						echo "------------------------------";
						$total_pli = 0;
						$total_mvt = 0;
						while ($ores_typo = pg_fetch_assoc($res_typo)) {
							
						 $id_soc     = $ores_typo['id_soc'];	
						 $nb_pli_par_typo       = $ores_typo['nb_pli_par_typo'];	
						 $nb_mvt_par_typo       = $ores_typo['nb_mvt_par_typo'];	
						 $id_typo   			= $ores_typo['id_typo']." - ".$ores_typo['typologie'];	
						 $total_pli += $nb_pli_par_typo;
						 $total_mvt += $nb_mvt_par_typo;
					   
						 echo  " </br>id_typo : ".$id_typo.",</br>nb_pli_par_typo : ".$nb_pli_par_typo." ,</br>nb_mvt_par_typo : ".$nb_mvt_par_typo.",  </br>id_soc : ".$id_soc."</br>";
						 
						 $titre    = "Plis par typologie";
						 $rubrique = pg_escape_string($id_typo);
						 $cpt_data  	  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 
						 
						 //--- Pli avec typologie
						 if($cpt_data  == 0){					
								echo $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_par_typo."','".$nb_mvt_par_typo."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								echo $update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_par_typo." , nb_mvt = '".$nb_mvt_par_typo."'
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
						
						}
						 $rubrique = 'Total';
						 if($cpt_data  == 0){
						 $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$total_pli."','".$total_mvt."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_mensuel SET nb_pli = ".$total_pli." , nb_mvt = '".$total_mvt."'
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						 
						
						
					}
					//#---------- FIN Nombre de plis par typologie ---------------------
					//#---------------- Plis cloturés   -----------------------------	
					 $sql_plis_cltr = "SELECT
											 id_soc,id_typo,
											 case when nb_pli_par_typo is null then 0 else nb_pli_par_typo end as nb_pli_par_typo,
											 case when nb_mvt_par_typo is null then 0 else nb_mvt_par_typo end as nb_mvt_par_typo,
											 data_typologie_societe.typologie, data_typologie_societe.nom_societe
										FROM	(
											SELECT data_typologie.id as id_typo, data_typologie.typologie, data_societe.id as id_soc, data_societe.nom_societe
											FROM
												(
												SELECT 
													id, typologie,1 flag
												FROM typologie
												) as data_typologie
												LEFT JOIN
												(
												SELECT 
													id, nom_societe,1 flag
												FROM societe
												) as data_societe on data_societe.flag = data_typologie.flag
											)as data_typologie_societe
											LEFT JOIN
											(
												SELECT case when typologie is null then 'Non définie' else typologie end as typologie , 
												case when nb_pli_par_typo is null then 0 else nb_pli_par_typo end as nb_pli_par_typo,
												case when nb_mvt_par_typo is null then 0 else nb_mvt_par_typo end as nb_mvt_par_typo,
												societe,
												case when id_typologie is null then 0 else id_typologie end as id_typologie
												FROM (
													SELECT typologie, nb_pli_par_typo, nb_mvt_par_typo,2 societe,id_typologie from f_pli_cloture( ".$clauseWhere.", 2)
													UNION 
													SELECT typologie, nb_pli_par_typo, nb_mvt_par_typo,1 societe,id_typologie from f_pli_cloture( ".$clauseWhere.", 1)
												) as data_pli
											)as data on data.id_typologie = data_typologie_societe.id_typo and data.societe = data_typologie_societe.id_soc
										WHERE nb_pli_par_typo >0 or nb_mvt_par_typo > 0 
										ORDER BY data_typologie_societe.id_soc, data_typologie_societe.id_typo asc
									";	
						
					$res_cltr  = pg_query( $conn,$sql_plis_cltr) OR die(pg_last_error());			
					$iNbr_cltr = (pg_num_rows($res_cltr) == null) ? 0 : pg_num_rows($res_cltr); 
					
					
					/*echo "<pre>";
					print_r($sql_plis_cltr);
					echo "<pre>";*/
					if ( $iNbr_cltr > 0){	
					
						$total_pli = $total_mvt = 0;
						echo "</br>Nombre de plis CLOTURES</br>";
						echo "------------------------------";
						while ($ores_cltr = pg_fetch_assoc($res_cltr)) {
							
						 $id_soc     			  = $ores_cltr['id_soc'];
						 $id_typo     			  = $ores_cltr['id_typo']." - ".$ores_cltr['typologie'];
						 $nb_pli_par_typo     	  = $ores_cltr['nb_pli_par_typo'];
						 $nb_mvt_par_typo     	  = $ores_cltr['nb_mvt_par_typo'];
						 $total_pli += $nb_pli_par_typo;
						 $total_mvt += $nb_mvt_par_typo;
						 
						  echo  " </br>id_soc : ".$id_soc." ,</br>id_typo : ".$id_typo.", </br>nb_pli_par_typo : ".$nb_pli_par_typo.", </br>nb_mvt_par_typo : ".$nb_mvt_par_typo."</br>";
						 
						 $titre    = "Plis cloturés";
						 $rubrique = $id_typo;
						 $cpt_data  	  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 
						 //--- Pli avec typologie
						 if($cpt_data  == 0){					
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_par_typo."','".$nb_mvt_par_typo."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_mensuel SET  nb_pli = ".$nb_pli_par_typo." , nb_mvt = '".$nb_mvt_par_typo." 
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."'  and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
						
						}
						 $rubrique = 'Total';
						 if($cpt_data  == 0){
						 $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$total_pli."','".$total_mvt."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_mensuel SET nb_pli = ".$total_pli." , nb_mvt = '".$total_mvt."'
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
								$result_update = pg_query($conn,$update);
						 }
					}
					//#------------------------------------------------------- -------------------------
					//#----------------------- Nombre de plis en cours ------------------------------------	
					$sql_plis_encours = "SELECT
											 id_soc,libelle,mvt, pli, data_societe.nom_societe
										FROM	(
											SELECT 
												societe.id as id_soc, nom_societe,1 flag
												FROM societe
											)as data_societe
											LEFT JOIN
											(
												SELECT libelle, mvt, pli, societe
												FROM (
													SELECT  libelle,mvt, pli ,2 societe from f_pli_en_cours( ".$clauseWhere.", 2)
													UNION 
													SELECT  libelle,mvt, pli ,1 societe from f_pli_en_cours( ".$clauseWhere.", 1)
													UNION
													SELECT  'Total',sum(coalesce(mvt,0))as mvt, sum(coalesce(pli,0)) pli ,2 societe from f_pli_en_cours(".$clauseWhere.", 2)
													UNION 
													SELECT  'Total',sum(coalesce(mvt,0))as mvt, sum(coalesce(pli,0)) pli ,1 societe from f_pli_en_cours(".$clauseWhere.", 1)
												) as data_pli
											)as data on data.societe = data_societe.id_soc
										ORDER BY data_societe.id_soc ,libelle asc";	
					
					
					$res_encours  = pg_query( $conn,$sql_plis_encours) OR die(pg_last_error());			
					$iNbr_encours = pg_num_rows($res_encours) or die("Erreur sur la requete => Nombre de plis en cours ");
					
					if ( $iNbr_encours > 0){	
						
						echo "</br>Plis en cours </br>";
						echo "------------------------------";
						while ($ores_encours = pg_fetch_assoc($res_encours)) {
							
						 $id_soc     	= $ores_encours['id_soc'];
						 $libelle     	= $ores_encours['libelle'];
						 $pli     		= $ores_encours['pli'];
						 $mvt     		= $ores_encours['mvt'];
						 
						 echo  " </br>id_soc : ".$id_soc." ,</br>libelle : ".$libelle.", </br>pli : ".$pli.", </br>mvt : ".$mvt.",</br>";
						 
						 $titre    = "Plis en cours";
						 
						 if($libelle != 'Total') list($num_rub,$rubrique) = explode("-",$libelle);	
						 else $rubrique = $libelle;
						 $cpt_data_koinc  	  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 
						 //
						 if($cpt_data_koinc  == 0){					
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$pli."','".$mvt."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_mensuel SET nb_pli = ".$pli." , nb_mvt = ".$mvt."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						
						 }
						
				 }
				 
				 //#------------------------------------------------------- -------------------------
				 //#----------------------- Suivi des plis restant à traiter ------------------
				 $sql_plis_encours ="SELECT
										 id_soc,pli_encours,pli_non_traite, mvt_encours, mvt_non_traite, data_societe.nom_societe
									FROM	(
											SELECT 
											societe.id as id_soc, nom_societe,1 flag
											FROM societe
										)as data_societe
										LEFT JOIN
										(
											SELECT sum(pli_encours) as pli_encours,sum(pli_non_traite) as pli_non_traite, sum(mvt_encours) as mvt_encours, sum(mvt_non_traite) as mvt_non_traite, societe
											FROM (
												SELECT  
													case when pli_en_cours = 1 then count (pli_stat.id_pli) else 0 end as pli_encours, 
													case when pli_non_traite = 1 then count (pli_stat.id_pli) else 0 end as pli_non_traite,
													case when pli_en_cours = 1 then sum (nb_mvt) else 0 end as mvt_encours, 
													0 as mvt_non_traite,				
													2 as societe 
												from pli_stat 
												left join  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = pli_stat.id_pli 
												WHERE ".$_clauseWhere."
												and societe = '2'  and (pli_en_cours = 1 or pli_non_traite = 1 )
												GROUP BY pli_en_cours,pli_non_traite
												union 
												SELECT  
													case when pli_en_cours = 1 then count (pli_stat.id_pli) else 0 end as pli_encours, 
													case when pli_non_traite = 1 then count (pli_stat.id_pli) else 0 end as pli_non_traite,
													case when pli_en_cours = 1 then sum (nb_mvt) else 0 end as mvt_encours, 
													0 as mvt_non_traite,				
													1 as societe 
												from pli_stat 
												left join  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = pli_stat.id_pli 
												WHERE ".$_clauseWhere."
												and societe = '1'  and (pli_en_cours = 1 or pli_non_traite = 1 )
												GROUP BY pli_en_cours,pli_non_traite
											) as data_pli
											GROUP BY societe
										)as data on data.societe = data_societe.id_soc
									ORDER BY data_societe.id_soc asc";	
					
					$res_encours  = pg_query( $conn,$sql_plis_encours) OR die(pg_last_error());			
					$iNbr_encours = pg_num_rows($res_encours) or die("Erreur sur la requete => Restant à traiter ");
					$total_encours = 0;
					if ( $iNbr_encours > 0){	
						
						 echo "</br>Nombre de plis en cours </br>";
						 echo "------------------------------";
						 while ($ores_encours = pg_fetch_assoc($res_encours)) {
							
						 $id_soc     		= $ores_encours['id_soc'];						 
						 $nouveau			= $ores_encours['pli_non_traite'];
						 $encours			= $ores_encours['pli_encours'];
						 $mvt_encours			= $ores_encours['mvt_encours'];
						 $mvt_nouveau			= $ores_encours['mvt_non_traite'];
						 
						  echo  " </br>id_soc : ".$id_soc." ,</br>nouveau : ".$nouveau.", </br>mvt_nouveau : ".$mvt_nouveau.", </br>encours : ".$encours.",</br> mvt_encours : ".$mvt_encours."</br>";
					//--- non traité
						 $titre    = "Restant à traiter";
						 $rubrique = "Non traité";
						 $cpt_data_ko_nv 	  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 
						 if($cpt_data_ko_nv  == 0){					
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nouveau."','".$mvt_nouveau."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_mensuel SET nb_pli = ".$nouveau." , nb_mvt = ".$mvt_nouveau."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						 
						   //--- En cours
						 $rubrique = "En cours";
						 $cpt_data_encours 	  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
						 
						 if($cpt_data_encours  == 0){					
								$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
										   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$encours."','".$mvt_encours."','".$id_soc."','".$date_debut."')";
								$result_insert = pg_query($conn,$insert);
						 }else{
								$update = "UPDATE stat_mensuel SET nb_pli = ".$encours." , nb_mvt = ".$mvt_encours."
								WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."'";
								$result_update = pg_query($conn,$update);
						 }
						}
					}
					
					//#-----------------------------Nouveau tableau  ------------------------
					//#-------------------------- Suivi des plis KO ------------------------
					 $sql = "SELECT
								 id_soc,
								 coalesce(SUM(pli_ko),0) as pli_ko,
								 coalesce(SUM(mvt_ko),0) as mvt_ko ,
								case when SUM(ko_scan) is null then 0 else SUM(ko_scan) end as ko_scan,
								case when SUM(ko_def) is null then 0 else SUM(ko_def) end as ko_def,
								case when SUM(ko_inconnu) is null then 0 else SUM(ko_inconnu) end as ko_inconnu,
								case when SUM(ko_ks) is null then 0 else SUM(ko_ks) end as ko_ks,
								case when SUM(ko_ke) is null then 0 else SUM(ko_ke) end as ko_ke,
								case when SUM(ko_abondonne) is null then 0 else SUM(ko_abondonne) end as ko_abondonne,
								case when SUM(ko_circulaire) is null then 0 else SUM(ko_circulaire) end as ko_circulaire,
								case when SUM(ci_editee) is null then 0 else SUM(ci_editee) end as ci_editee,
								case when SUM(ci_envoyee) is null then 0 else SUM(ci_envoyee) end as ci_envoyee, 
								case when SUM(ok_ci) is null then 0 else SUM(ok_ci) end as ok_ci, 
								case when SUM(ko_encours) is null then 0 else SUM(ko_encours) end as ko_encours, 
								case when SUM(mvt_ko_scan) is null then 0 else SUM(mvt_ko_scan) end as mvt_ko_scan,
								case when SUM(mvt_ko_def) is null then 0 else SUM(mvt_ko_def) end as mvt_ko_def,
								case when SUM(mvt_ko_inconnu) is null then 0 else SUM(mvt_ko_inconnu) end as mvt_ko_inconnu,
								case when SUM(mvt_ko_ks) is null then 0 else SUM(mvt_ko_ks) end as mvt_ko_ks,
								case when SUM(mvt_ko_ke) is null then 0 else SUM(mvt_ko_ke) end as mvt_ko_ke,
								case when SUM(mvt_ko_abondonne) is null then 0 else SUM(mvt_ko_abondonne) end as mvt_ko_abondonne,
								case when SUM(mvt_ko_circulaire) is null then 0 else SUM(mvt_ko_circulaire) end as mvt_ko_circulaire,
								case when SUM(mvt_ci_editee) is null then 0 else SUM(mvt_ci_editee) end as mvt_ci_editee,
								case when SUM(mvt_ci_envoyee) is null then 0 else SUM(mvt_ci_envoyee) end as mvt_ci_envoyee,
								case when SUM(mvt_ok_ci) is null then 0 else SUM(mvt_ok_ci) end as mvt_ok_ci,
								case when SUM(mvt_ko_encours) is null then 0 else SUM(mvt_ko_encours) end as mvt_ko_encours
							FROM	(
								SELECT 
									societe.id as id_soc, nom_societe,1 flag
									FROM societe
								)as data_societe
								
								LEFT JOIN(
										SELECT 1 as societe_id,
										 case when f_pli.statut_saisie = 2   then count(distinct f_pli.id_pli) else 0 end as ko_scan,					 
										 case when f_pli.statut_saisie = 3  then count( distinct f_pli.id_pli) else 0 end as ko_def,
										 case when f_pli.statut_saisie = 4  then count( distinct f_pli.id_pli) else 0 end as ko_inconnu,
										 case when f_pli.statut_saisie = 5 then count(distinct f_pli.id_pli) else 0 end as ko_ks,
										 case when f_pli.statut_saisie = 6 then count(distinct f_pli.id_pli) else 0 end as ko_ke,
										 case when f_pli.statut_saisie = 7  then count(distinct f_pli.id_pli) else 0 end as ko_circulaire,
										 case when f_pli.statut_saisie = 8  then count( distinct f_pli.id_pli) else 0 end as ko_abondonne,						 
										 case when f_pli.statut_saisie = 9  then count(distinct f_pli.id_pli) else 0 end as ci_editee,
										 case when f_pli.statut_saisie = 10  then count(distinct f_pli.id_pli) else 0 end as ci_envoyee,
										 case when f_pli.statut_saisie = 11  then count(distinct f_pli.id_pli) else 0 end as ok_ci,
										 case when f_pli.statut_saisie = 12  then count(distinct f_pli.id_pli) else 0 end as ko_encours,
										 case when f_pli.statut_saisie = ANY (ARRAY[2,3,4,5,6,7,9,12])  then count(distinct f_pli.id_pli) else 0 end as pli_ko,
										 case when f_pli.statut_saisie = 2   then  sum(mvt_nbr) else 0 end as mvt_ko_scan,					 
										 case when f_pli.statut_saisie = 3  then  sum(mvt_nbr)else 0 end as mvt_ko_def,
										 case when f_pli.statut_saisie = 4  then  sum(mvt_nbr) else 0 end as mvt_ko_inconnu,
										 case when f_pli.statut_saisie = 5 then  sum(mvt_nbr) else 0 end as mvt_ko_ks,
										 case when f_pli.statut_saisie = 6 then  sum(mvt_nbr) else 0 end as mvt_ko_ke,
										 case when  f_pli.statut_saisie = 7  then  sum(mvt_nbr) else 0 end as mvt_ko_circulaire,
										 case when f_pli.statut_saisie = 8  then  sum(mvt_nbr) else 0 end as mvt_ko_abondonne,						 
										 case when f_pli.statut_saisie = 9  then  sum(mvt_nbr) else 0 end as mvt_ci_editee,
										 case when f_pli.statut_saisie = 10  then  sum(mvt_nbr)else 0 end as mvt_ci_envoyee,
										 case when f_pli.statut_saisie = 11  then  sum(mvt_nbr)else 0 end as mvt_ok_ci,
										 case when f_pli.statut_saisie = 12  then  sum(mvt_nbr)else 0 end as mvt_ko_encours,
										 case when f_pli.statut_saisie = ANY (ARRAY[2,3,4,5,6,7,9,12])   then  sum(mvt_nbr)else 0 end as mvt_ko
										 FROM  f_pli
											INNER JOIN f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
											INNER JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
											INNER JOIN societe ON societe.id = f_pli.societe	
											WHERE  ".$_clauseWhere." AND f_pli.societe = 1											
										GROUP BY f_pli.statut_saisie
											
									union
										SELECT 2 as societe_id,
										 case when f_pli.statut_saisie = 2   then count(distinct f_pli.id_pli) else 0 end as ko_scan,					 
										 case when f_pli.statut_saisie = 3  then count( distinct f_pli.id_pli) else 0 end as ko_def,
										 case when f_pli.statut_saisie = 4  then count( distinct f_pli.id_pli) else 0 end as ko_inconnu,
										 case when f_pli.statut_saisie = 5 then count(distinct f_pli.id_pli) else 0 end as ko_ks,
										 case when f_pli.statut_saisie = 6 then count(distinct f_pli.id_pli) else 0 end as ko_ke,
										 case when f_pli.statut_saisie = 7  then count(distinct f_pli.id_pli) else 0 end as ko_circulaire,
										 case when f_pli.statut_saisie = 8  then count( distinct f_pli.id_pli) else 0 end as ko_abondonne,						 
										 case when f_pli.statut_saisie = 9  then count(distinct f_pli.id_pli) else 0 end as ci_editee,
										 case when f_pli.statut_saisie = 10  then count(distinct f_pli.id_pli) else 0 end as ci_envoyee,
										 case when f_pli.statut_saisie = 11  then count(distinct f_pli.id_pli) else 0 end as ok_ci,
										 case when f_pli.statut_saisie = 12  then count(distinct f_pli.id_pli) else 0 end as ko_encours,
										 case when f_pli.statut_saisie = ANY (ARRAY[2,3,4,5,6,7,9,12])  then count(distinct f_pli.id_pli) else 0 end as pli_ko,
										 case when f_pli.statut_saisie = 2   then  sum(mvt_nbr) else 0 end as mvt_ko_scan,					 
										 case when f_pli.statut_saisie = 3  then  sum(mvt_nbr)else 0 end as mvt_ko_def,
										 case when f_pli.statut_saisie = 4  then  sum(mvt_nbr) else 0 end as mvt_ko_inconnu,
										 case when f_pli.statut_saisie = 5 then  sum(mvt_nbr) else 0 end as mvt_ko_ks,
										 case when f_pli.statut_saisie = 6 then  sum(mvt_nbr) else 0 end as mvt_ko_ke,
										 case when  f_pli.statut_saisie = 7  then  sum(mvt_nbr) else 0 end as mvt_ko_circulaire,
										 case when f_pli.statut_saisie = 8  then  sum(mvt_nbr) else 0 end as mvt_ko_abondonne,						 
										 case when f_pli.statut_saisie = 9  then  sum(mvt_nbr) else 0 end as mvt_ci_editee,
										 case when f_pli.statut_saisie = 10  then  sum(mvt_nbr)else 0 end as mvtci_envoyee,
										 case when f_pli.statut_saisie = 11  then  sum(mvt_nbr)else 0 end as mvt_ok_ci,
										 case when f_pli.statut_saisie = 12  then  sum(mvt_nbr)else 0 end as mvt_ko_encours,
										 case when f_pli.statut_saisie = ANY (ARRAY[2,3,4,5,6,7,9,12]) then  sum(mvt_nbr)else 0 end as mvt_ko
										 FROM  f_pli
											INNER JOIN f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
											INNER JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
											INNER JOIN societe ON societe.id = f_pli.societe	
											WHERE ".$_clauseWhere." AND f_pli.societe = 2											
										GROUP BY f_pli.statut_saisie
								) as data_KO on data_KO.societe_id= data_societe.id_soc
							group by id_soc
							ORDER BY data_societe.id_soc  asc"	;
								//echo "<pre>"; print_r($sql);	echo "</pre>";	exit;
									$res 		= pg_query($conn,$sql) OR die(" erreur dans la requete AFFICHAGE GLOBAL");	
									
									$iNbr   = pg_num_rows($res) or die("Erreur sur la requete => plis KO");
					
									$total_global = 0;
									if ( $iNbr > 0){	
									
										while ($ores = pg_fetch_assoc($res)) {
											
										$id_soc     = $ores['id_soc'];			 
										$nb_pli_scan    = $ores['ko_scan'];
										$nb_pli_ks      = $ores['ko_ks'];
										$nb_pli_kd      = $ores['ko_def'];
										$nb_pli_ko_inconnu  = $ores['ko_inconnu'];
										$nb_pli_ko_att  	= $ores['ko_ke'];
										$nb_pli_ci      	= $ores['ko_circulaire'];
										$nb_pli_ci_editee 	= $ores['ci_editee'];
										$nb_pli_ci_envoyee  = $ores['ci_envoyee'];
										$nb_pli_abandonne   = $ores['ko_abondonne'];
										$nb_ok_ci           = $ores['ok_ci'];
										$nb_ko_encours      = $ores['ko_encours'];
										$nb_pli_ko  		= $ores['pli_ko'];
										$mouvement_scan    = $ores['mvt_ko_scan'];
										$mouvement_ks      = $ores['mvt_ko_ks'];
										$mouvement_kd      = $ores['mvt_ko_def'];
										$mouvement_ko_inconnu  	= $ores['mvt_ko_inconnu']; // ajouter nouveau
										$mouvement_ko_att  		= $ores['mvt_ko_ke'];
										$mouvement_ci      		= $ores['mvt_ko_circulaire'];
										$mouvement_ci_editee 	= $ores['mvt_ci_editee'];
										$mouvement_ci_envoyee  	= $ores['mvt_ci_envoyee']; // ajouter nouveau
										$mouvement_abandonne  	= $ores['mvt_ko_abondonne']; 
										$mouvement_ok_ci  	    = $ores['mvt_ok_ci'];
										$mouvement_ko_encours  	= $ores['mvt_ko_encours'];
										$mouvement_ko  			= $ores['mvt_ko'];
										
										echo "******************** Pli KO - Début : ".$date_debut." , Fin : ".$date_fin."*******************************</br>";
										echo "</br>Bloc KO</br>";		 
										echo  "--------------</br> KO SCAN : ".$nb_pli_scan." ,</br>KO SRC : ".$nb_pli_ks.", </br>KO DEFINITIF : ".$nb_pli_kd.",</br> KO INCONNU : ".$nb_pli_ko_inconnu."
										 ,</br> KO EN ATTENTE : ".$nb_pli_ko_att.",</br> KO CI : ".$nb_pli_ci.",</br> CI EDITEE : ".$nb_pli_ci_editee.",</br> CI ENVOYEE : ".$nb_pli_ci_envoyee.",</br> KO Abandonne : ".$nb_pli_abandonne.",</br> Total : ".$total."</br>";
										 $titre = 'PLI KO';
										//--- KO scan
										$rubrique = "KO SCAN";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_scan."','".$mouvement_scan."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_scan." , nb_mvt = ".$mouvement_scan."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										//--- KO _ci
										$rubrique = "KO CI";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci."','".$mouvement_ci."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ci." , nb_mvt = ".$mouvement_ci."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										 
										   //--- KO_ks
										$rubrique = "KO KS";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ks."','".$mouvement_ks."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ks." , nb_mvt = ".$mouvement_ks."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										 
										   //--- KO_kd
										$rubrique = "KO KD";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_kd."','".$mouvement_kd."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_kd." , nb_mvt = ".$mouvement_kd."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										 
										   //--- KO_inconnu
										$rubrique = "KO INCONNU";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ko_inconnu."','".$mouvement_ko_inconnu."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ko_inconnu." , nb_mvt = ".$mouvement_ko_inconnu."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										  //--- KO_EN ATTENTE
										$rubrique = "KO EN ATTENTE";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ko_att."','".$mouvement_ko_att."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ko_att." , nb_mvt = ".$mouvement_ko_att."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										   //--- KO _ci_editee
										$rubrique = "CI EDITEE";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci_editee."','".$mouvement_ci_editee."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ci_editee." , nb_mvt = ".$mouvement_ci_editee."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										  //--- KO _ci_envoyee
										$rubrique = "CI ENVOYEE";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ci_envoyee."','".$mouvement_ci_envoyee."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ci_envoyee." , nb_mvt = ".$mouvement_ci_envoyee."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
										 
										   //--- KO abandonnee
										$rubrique = "KO ABANDONNE";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_abandonne."','".$mouvement_abandonne."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_abandonne." , nb_mvt = ".$mouvement_abandonne."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }

                                        //--- OK CI
                                        $rubrique = "OK CI";
                                        $cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
                                        if($cpt_data_total  == 0){
                                            $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
                                                       VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_ok_ci."','".$mouvement_ok_ci."','".$id_soc."','".$date_debut."')";
                                            $result_insert = pg_query($conn,$insert);
                                        } else
                                        {
                                            $update = "UPDATE stat_mensuel SET nb_pli = ".$nb_ok_ci." , nb_mvt = ".$mouvement_ok_ci."
                                            WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
                                            $result_update = pg_query($conn,$update);
                                        }

                                        //--- KO en cours
                                        $rubrique = "KO EN COURS BAYARD";
                                        $cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
                                        if($cpt_data_total  == 0){
                                            $insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
                                                       VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_ko_encours."','".$mouvement_ko_encours."','".$id_soc."','".$date_debut."')";
                                            $result_insert = pg_query($conn,$insert);
                                        } else
                                        {
                                            $update = "UPDATE stat_mensuel SET nb_pli = ".$nb_ko_encours." , nb_mvt = ".$mouvement_ko_encours."
                                            WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
                                            $result_update = pg_query($conn,$update);
                                        }
										 
										  //--- TOTAL PLI KO 
										$rubrique = "TOTAL PLI KO";
										$cpt_data_total  = tcheck_data($mois, $annee, $titre,$rubrique,$id_soc,$conn);
										 if($cpt_data_total  == 0){
												$insert   = "INSERT INTO stat_mensuel (mois, annee, titre,rubrique, nb_pli, nb_mvt, societe,daty)
														   VALUES ('".$mois."','".$annee."','".$titre."','".$rubrique."','".$nb_pli_ko."','".$mouvement_ko."','".$id_soc."','".$date_debut."')";
												$result_insert = pg_query($conn,$insert);
										 } else 
										 {
												$update = "UPDATE stat_mensuel SET nb_pli = ".$nb_pli_ko." , nb_mvt = ".$mouvement_ko."
												WHERE mois = '".$mois."' AND annee = '".$annee."' AND titre = '".$titre."' AND rubrique ='".$rubrique."' and societe = '".$id_soc."' ";
												$result_update = pg_query($conn,$update);
										 }
																				 
							}
						}
				//#------------------------------------------ FIN STAT MENSUEL ---------------------------------
				//#------------------------------------------ FIN STAT MENSUEL ---------------------------------
				//#------------------------------------------ FIN STAT MENSUEL ---------------------------------
					
							
				}	
			}	
				
		}	
	}
	
	// ################################################
	$module_nom  = "CRON DATA - GED BAYARD V2" ;
	$nom_fichier = "10.90\GED\bayard_v2\crons\cron_data.php" ;
	$date_jour   = date("Y-m-d");
	$statut      = "termine" ;

	echo $sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple) 
	VALUES ('$module_nom','$nom_fichier','$date_jour','$statut','$couple') " ;
	$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;

	// ################################################  
	
	exec('php ../crons/cron_data_hebdo.php');
	exec('php ../crons/cron_data_flux.php');
	exec('php ../crons/cron_data_hebdo_flux.php');
	echo "*************************************LANCEMENET DU CRON HEBDO*******************************";
?>