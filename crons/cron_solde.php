﻿<?php 
/* ----
	# Calcul du reliquat J
	# Reception J
	# Solde J
--- */ 	
		function getConnBayard() {
			
				$conn = pg_connect("host=srv-bdd-ged-bayard.madcom.local port=5432 dbname=ged_bayard user=si password=51P@vGD24$")
				or die("Impossible de se connecter à ged_bayard ");
				
				return $conn;
		}
		$conn  = getConnBayard(); 	
		
		setlocale (LC_TIME, 'fr_FR');	

		$array_tlmc   = array();
		//$date_du_jour = '2019-07-03';		
		$date_du_jour = date("Y-m-d");
		$mois_courant = date("Y-m");
		$date_debut   = date("Y-m").'-01';		
		$annee        = date("Y");
		$mois 		  = date("m");
		
				
				
		//#---------- Reception J : traité à J ? non traité à J ? -----------------------
		//#------------Reception J moins : traité à J ? non traité à J ? Solde final ?---
		
		$date_reception = date("Y-d-m");
		
		$list_soc = $list_reception       = $list_reception_j = $list_reception_jmoins = array();
		$list_reception_j_mvt = $list_reception_jmoins_mvt = $tab_reliquat = array();
		
		$list_soc 				  = get_societe($conn);
		
		$tab_reception_j 	      = get_reception_j($date_du_jour,$conn);
		$tab_reception_jmoins     = get_reception_jmoins_traitement_j($date_du_jour,$conn);
		
		$tab_reception_j_mvt      = get_reception_j_mouvement($date_du_jour,$conn);
		$tab_reception_jmoins_mvt = get_reception_jmoins_mouvement_j($date_du_jour,$conn);
		
		$tab_reliquat = get_reliquat($date_du_jour,$conn);
		
		
		echo "</br>*************************************************</br>";
		echo "</br>********** Solde journalier des plis ************</br>";
		echo "</br>---------------------- J -----------------------------</br>";
		echo "<pre>";
		print_r($tab_reception_j);
		echo "</pre>";
		echo "</br>---------------------- J MOINS -----------------------</br>";
		echo "<pre>";
		print_r($tab_reception_jmoins);
		echo "</pre>";
		echo "</br>---------------------- Reliquat courrier anterieur----</br>";
		echo "<pre>";
		print_r($tab_reliquat);
		echo "</pre>";
		
		// ################################################

		$module_nom  = "CRON SOLDE - GED Bayard" ;

		$nom_fichier = "10.90\GED\bayard\crons\cron_solde.php" ;
		$_date_jour   = date("Y-m-d");
		$couple    = date("YmdHis");
		$etat      = "debut" ;

		$sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple) 
		VALUES ('$module_nom','$nom_fichier','$_date_jour','$etat','$couple') " ;
		$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;

		// ################################################ 
		$count_reliq = count($tab_reliquat);
		if($count_reliq > 0){
			foreach($tab_reliquat as $k_rel =>$tab_reliq){
				$id_soc_rel 	   =  $tab_reliq['id_soc'];
				$date_courrier_rel =  $tab_reliq['date_courrier'];
				$pli_rel 		   = $tab_reliq['pli'];
				
				if($pli_rel > 0) maj_reliquat_existant($date_courrier_rel,$id_soc_rel,$pli_rel,$conn);
			}
			
		}
		
		$count_rec_j 	  = count($tab_reception_j);
		$count_rec_jmoins = count($tab_reception_jmoins);
		
		$compteur = $count_rec_j + $count_rec_jmoins; 
		
		$list_dates = $list_traitement = $reception_jmoins = $result = array();
		if ( $compteur > 0){	
			
		
			if($count_rec_j > 0){
				
				foreach($tab_reception_j as $j => $list_reception_j) {
							
						$societe     = $list_reception_j["societe_vf"];						
						$date_j 	 = $list_reception_j["date_courrier"];
						$list_traitement[$societe]["societe"]            = $list_reception_j["societe_vf"];
						$list_traitement[$societe]["date_j"]             = $list_reception_j["date_courrier"];
						$list_traitement[$societe]["pli_recep_j"]        += $list_reception_j["pli_total_j"];
						$list_traitement[$societe]["pli_cloture_j"] 	 += $list_reception_j["pli_cloture_j"];
						$list_traitement[$societe]["pli_non_traite_j"]   += $list_reception_j["pli_non_traite_j"];
						$list_traitement[$societe]["pli_ok"] 	 		 += $list_reception_j["pli_ok"];
						$list_traitement[$societe]["pli_ko_scan"] 	 	 += $list_reception_j["pli_ko_scan"];
						$list_traitement[$societe]["pli_hors_perimetre"] += $list_reception_j["pli_hors_perimetre"];
						$list_traitement[$societe]["pli_ko_ks"] 	     += $list_reception_j["pli_ko_ks"];
						$list_traitement[$societe]["pli_ko_ke"]	         += $list_reception_j["pli_ko_ke"];
						$list_traitement[$societe]["pli_ko_kd"]	     	 += $list_reception_j["pli_ko_kd"];
						$list_traitement[$societe]["pli_ci"] 	         += $list_reception_j["pli_ci"];
						$list_traitement[$societe]["plis_j"] 	         = $list_reception_j["plis"];
						
						
					}						
			}
			if($count_rec_jmoins> 0 || $count_rec_j > 0 ){
				//echo count($tab_reception_jmoins);
				foreach($tab_reception_jmoins as $j1 =>$list_reception_jmoins) {
						    $societe = $j1;
							$list_traitement[$societe]["societe"]            = $societe;
							$list_traitement[$societe]["date_courrier"]      =  ($list_traitement[$societe]["date_courrier"] == null) ? $date_du_jour : $list_traitement[$societe]["date_courrier"];
							$dt_event 	 = $list_reception_jmoins["dt_event"];
							
							$list_traitement[$societe]["pli_non_traite_recep_jmoins"]   += $list_reception_jmoins["pli_non_traite_j"];
							$list_traitement[$societe]["pli_cloture_recep_jmoins"] 	    += $list_reception_jmoins["pli_traite_j"];
							$list_traitement[$societe]["pli_ok"] 			 += $list_reception_jmoins["pli_ok"];
							$list_traitement[$societe]["pli_ko_scan"] 	     += $list_reception_jmoins["pli_ko_scan"];
							$list_traitement[$societe]["pli_hors_perimetre"] += $list_reception_jmoins["pli_hors_perimetre"];
							$list_traitement[$societe]["pli_ko_ks"] 		 += $list_reception_jmoins["pli_ko_ks"];
							$list_traitement[$societe]["pli_ko_ke"] 		 += $list_reception_jmoins["pli_ko_ke"];
							$list_traitement[$societe]["pli_ko_kd"] 		 += $list_reception_jmoins["pli_ko_kd"];
							$list_traitement[$societe]["pli_ci"] 			 += $list_reception_jmoins["pli_ci"];
							$list_traitement[$societe]["plis_jmoins"] 		 = $list_reception_jmoins["plis"];
					}	
				
			}
			echo "</br>---------------------- UNION -----------------------</br>";
			echo "+++<pre>";
			print_r($list_traitement);
			echo "</pre>";
			
			foreach ($list_traitement as $k => $list){		
			
			
				$id_societe          = $list["societe"];
				$date_j 			 = ($list["date_j"] == null) ? $date_du_jour : $list["date_j"];
				$societe 	 	 	 = $list["societe"];
				$pli_total_j 	 	 = ($list["pli_recep_j"] == null) ? 0 : $list["pli_recep_j"];
				$pli_cloture_j 	 	 = ($list["pli_cloture_j"] == null) ? 0 : $list["pli_cloture_j"];
				$pli_non_traite_j 	 = ($list["pli_non_traite_j"] == null) ? 0 : $list["pli_non_traite_j"];
				$pli_non_traite_recep_jmoins 	 = $list["pli_non_traite_recep_jmoins"];
				$pli_cloture_recep_jmoins 	     = $list["pli_cloture_recep_jmoins"];
				$pli_ok 	 		 = $list["pli_ok"];
				$pli_ko_scan 	 	 = $list["pli_ko_scan"];
				$pli_hors_perimetre  = $list["pli_hors_perimetre"];
				$pli_ko_ks 	         = $list["pli_ko_ks"];
				$pli_ko_ke 	         = $list["pli_ko_ke"];
				$pli_ko_kd 	         = $list["pli_ko_kd"];
				$pli_ci 	         = $list["pli_ci"];
				
				$pli_pli_j 	         = ($list["plis_j"] == null || $list["plis_j"] == '' || $list["plis_j"] == ',') ? '' : $list["plis_j"];
				$pli_pli_jmoins 	 = ($list["plis_jmoins"] == null || $list["plis_jmoins"] == '' || $list["plis_jmoins"] == ',') ? '' : $list["plis_jmoins"]; 
				
				 echo "</br>SOLDE DES PLIS</br>";		
				 $pli_non_traite_recep_jmoins = ($pli_non_traite_recep_jmoins == null) ? 0 : $pli_non_traite_recep_jmoins ;
				 $pli_cloture_recep_jmoins    = ($pli_cloture_recep_jmoins == null) ? 0 : $pli_cloture_recep_jmoins ;
				 echo  "--------------</br> Date : ".$date_j." ,</br>Reception J : ".$pli_total_j.", </br>Réception J - Cloturé : ".$pli_cloture_j.", </br>Réception J - Non traité : ".$pli_non_traite_j.", </br>Réception J Moins - Cloturé : ".$pli_cloture_recep_jmoins.", </br>Réception J Moins - Non traité : ".$pli_non_traite_recep_jmoins.",</br> OK : ".$pli_ok.",</br>KO Scan : ".$pli_ko_scan."</br>";
				 echo "--------------</br>";
				 $rubrique = 1;
				 $cpt_data  	  = tcheck_data($date_j, $id_societe, $rubrique, $conn);
				
				 $col_date_ttt    = $val_date_ttt = $where_date_ttt = "";
				
			
				 if($cpt_data  == 0){
								  
				 $insert   = "INSERT INTO 
								solde_plis_mvt_journalier 
								(societe, date_j, pli_total_reception_j, pli_traite_reception_j,pli_non_traite_reception_j, pli_traite_reception_jmoins, pli_non_traite_reception_jmoins,ok, ko_scan, hors_perimetre, ks, ke, kd, ci,rubrique)
					           VALUES ('".$id_societe."','".$date_j."','".$pli_total_j."','".$pli_cloture_j."','".$pli_non_traite_j."','".$pli_cloture_recep_jmoins."','".$pli_non_traite_recep_jmoins."','".$pli_ok."','".$pli_ko_scan."','".$pli_hors_perimetre."','".$pli_ko_ks."','".$pli_ko_ke."','".$pli_ko_kd."','".$pli_ci."',".$rubrique.")";
					
				 $result_insert = pg_query($conn,$insert)or die($insert);
				 echo '</br>Result_insert:'.$result_insert;
				 if($result_insert) echo " insert ok.</br>"; else echo " insert échoué.</br>";
				 
				 echo "</br>'Table ==> solde_plis_chgmt_etat' </br>";
				 echo "Plis & flag_traitement & statut_saisie ==> date de traitement : ".$date_j." plis J : ".$pli_pli_j." plis J Moins : ".$pli_pli_jmoins."</br>";
				 echo "--------------</br>";
				 
				 }else{
					$update   = "UPDATE 
								solde_plis_mvt_journalier 
							 SET
								pli_total_reception_j = '".$pli_total_j."', 
								pli_traite_reception_j = '".$pli_cloture_j."',
								pli_non_traite_reception_j = '".$pli_non_traite_j."', 
								pli_traite_reception_jmoins = '".$pli_cloture_recep_jmoins."', 
								pli_non_traite_reception_jmoins = '".$pli_non_traite_recep_jmoins."',
								ok = '".$pli_ok."' , 
								ko_scan = '".$pli_ko_scan."', 
								hors_perimetre = '".$pli_hors_perimetre."', 
								ks = '".$pli_ko_ks."', 
								ke = '".$pli_ko_ke."', 
								kd = '".$pli_ko_kd."', 
								ci = '".$pli_ci."'
					           WHERE 
									societe = '".$id_societe."'
									and date_j = '".$date_j."'
									and rubrique = ".$rubrique." 
								";					
				 $result_update = pg_query($conn,$update)or die($update);
					 
				 }
				
				if($pli_pli_j != '') {
					
					 $list = explode(",",$pli_pli_j);
					
					 foreach($list as $klist => $vlist){
						 
						 $cpt_pli = 0;
						 
						 list($id_pli, $flag_traitement, $statut_saisie) = explode("|",$vlist);
						 echo "+++date de traitement :".$date_j.", id_pli : ".$id_pli.", flag_traitement :".$flag_traitement.", statut_saisie : ".$statut_saisie."</br>";
						 $cpt_pli = tcheck_data_plis_chgmt_etat($date_j, $id_pli, $conn);

						 
							 if($cpt_pli == 0) {
									 if($id_pli > 0 && $flag_traitement != '' && $statut_saisie != ''){
									$insert_pli   = "INSERT INTO 
									solde_plis_chgmt_etat 
									(id_pli, date_traitement, flag_traitement, statut_saisie)
									VALUES ('".$id_pli."','".$date_j."','".$flag_traitement."','".$statut_saisie."')";
									$result_insert_pli = pg_query($conn,$insert_pli)or die($insert_pli);
									 }
							 }else{
								  if($id_pli > 0 && $flag_traitement != '' && $statut_saisie != ''){
									$upldate_pli   = "UPDATE 
									solde_plis_chgmt_etat SET flag_traitement = '".$flag_traitement."', statut_saisie = '".$statut_saisie."'
									WHERE id_pli = '".$id_pli."' AND date_traitement = '".$date_j."' ";
									$result_update = pg_query($conn,$upldate_pli)or die($upldate_pli);
								  }
							 }
						
					 }
					 
				 }

				 if($pli_pli_jmoins != '') {
					 
					 $list = explode(",",$pli_pli_jmoins);
					 
					 foreach($list as $klist => $vlist){
						 
						 $cpt_pli = 0;
						 
						 list($id_pli, $flag_traitement, $statut_saisie) = explode("|",$vlist);
						 echo "---date de traitement :".$date_j.", id_pli :".$id_pli.", flag_traitement :".$flag_traitement.", statut_saisie : ".$statut_saisie."</br>";
						 $cpt_pli = tcheck_data_plis_chgmt_etat($date_j, $id_pli, $conn);
						 
						 if($cpt_pli == 0) {
								if($id_pli > 0 && $flag_traitement != '' && $statut_saisie != ''){
								$insert_pli   = "INSERT INTO 
								solde_plis_chgmt_etat 
								(id_pli, date_traitement, flag_traitement, statut_saisie)
								VALUES ('".$id_pli."','".$date_j."','".$flag_traitement."','".$statut_saisie."')";
								$result_insert_pli = pg_query($conn,$insert_pli)or die($insert_pli);
							  }
						 }else{
							  if($id_pli > 0 && $flag_traitement != '' && $statut_saisie != ''){
								$upldate_pli   = "UPDATE 
								solde_plis_chgmt_etat SET flag_traitement = '".$flag_traitement."', statut_saisie = '".$statut_saisie."'
								WHERE id_pli = '".$id_pli."' AND date_traitement = '".$date_j."' ";
								$result_update = pg_query($conn,$upldate_pli)or die($upldate_pli);
							 }
							
						 }
						 
					 }
					 
				 }
				 
				 
			
				
				
			/*
			echo "<br>****************************************</br>";
			echo "<pre>";
			print_r($list);
			echo "</pre>";
			*/
				
		}

	}

	/*
exit;		
exit;		
exit;		
exit;		
exit;		
exit;		
exit;
*/
		
		echo "</br>*************************************************</br>";
		echo "</br>********** Solde des mouvements *****************</br>";
		echo "</br>*************************************************</br>";
		echo "<pre>";
		print_r($tab_reception_j_mvt);
		echo "</pre>";
		
		echo "<pre>";
		print_r($tab_reception_jmoins_mvt);
		echo "</pre>";
			
		$count_rec_j_mvt 	  = count($tab_reception_j_mvt);
		$count_rec_jmoins_mvt = count($tab_reception_jmoins_mvt);
		
		$compteur_mvt = $count_rec_j_mvt + $count_rec_jmoins_mvt; 
		
		$list_mouvement = array();
		if ( $compteur_mvt > 0){
			
			if($count_rec_j_mvt > 0){
				foreach($tab_reception_j_mvt as $j=> $list_reception_j_mvt) {
					
					$societe     = $list_reception_j_mvt["societe_vf"];
					$date_j 	 = $list_reception_j_mvt["date_courrier"];
					$list_mouvement[$societe]["societe"]             = $list_reception_j_mvt["societe_vf"];
					$list_mouvement[$societe]["date_j"]              = $list_reception_j_mvt["date_courrier"];
					$list_mouvement[$societe]["mvt_recep_j"]         += $list_reception_j_mvt["mvt_total_j"];
					$list_mouvement[$societe]["mvt_cloture_j"] 	     += $list_reception_j_mvt["mvt_cloture_j"];
					$list_mouvement[$societe]["mvt_non_traite_j"]    += $list_reception_j_mvt["mvt_non_traite_j"];
					$list_mouvement[$societe]["mvt_ok"] 	 		 += $list_reception_j_mvt["mvt_ok"];
					$list_mouvement[$societe]["mvt_ko_scan"] 	 	 += $list_reception_j_mvt["mvt_ko_scan"];
					$list_mouvement[$societe]["mvt_hors_perimetre"]  += $list_reception_j_mvt["mvt_hors_perimetre"];
					$list_mouvement[$societe]["mvt_ko_ks"] 	         += $list_reception_j_mvt["mvt_ko_ks"];
					$list_mouvement[$societe]["mvt_ko_ke"]	         += $list_reception_j_mvt["mvt_ko_ke"];
					$list_mouvement[$societe]["mvt_ko_kd"]	     	 += $list_reception_j_mvt["mvt_ko_kd"];
					$list_mouvement[$societe]["mvt_ci"] 	         += $list_reception_j_mvt["mvt_ci"];
				
			      
				   }
				
			}
			
			if($count_rec_j_mvt> 0 || $count_rec_jmoins_mvt > 0 ){
				
						
					foreach($tab_reception_jmoins_mvt as $j=> $list_reception_jmoins_mvt) {
						$societe = $list_reception_jmoins_mvt["societe"];
						
						
						$dt_event 	 = empty($list_reception_jmoins_mvt["dt_event"])? "-" : $list_reception_jmoins_mvt["dt_event"];
						$list_mouvement[$societe]["mvt_non_traite_recep_jmoins"]+= $list_reception_jmoins_mvt["mvt_non_traite_j"];
						$list_mouvement[$societe]["mvt_cloture_recep_jmoins"]   += $list_reception_jmoins_mvt["mvt_cloture_j"];
						$list_mouvement[$societe]["mvt_ok"] 			 		+= $list_reception_jmoins_mvt["mvt_ok"];
						$list_mouvement[$societe]["mvt_ko_scan"] 	     		+= $list_reception_jmoins_mvt["mvt_ko_scan"];
						$list_mouvement[$societe]["mvt_hors_perimetre"] 		+= $list_reception_jmoins_mvt["mvt_hors_perimetre"];
						$list_mouvement[$societe]["mvt_ko_ks"] 		 		    += $list_reception_jmoins_mvt["mvt_ko_ks"];
						$list_mouvement[$societe]["mvt_ko_ke"] 		 		    += $list_reception_jmoins_mvt["mvt_ko_ke"];
						$list_mouvement[$societe]["mvt_ko_kd"] 		 		    += $list_reception_jmoins_mvt["mvt_ko_kd"];
						$list_mouvement[$societe]["mvt_ci"] 			 		+= $list_reception_jmoins_mvt["mvt_ci"];
						
					}
				
			}

			echo "<pre>";
			print_r($list_mouvement);
			echo "</pre>";
			
			foreach ($list_mouvement as $k => $list){		
			
			
				$id_societe          = $list["societe"];
				$date_j 			 = ($list["date_j"] == null) ? $date_du_jour : $list["date_j"];
				$mvt_total_j 		 = ($list["mvt_recep_j"] == null) ? 0 : $list["mvt_recep_j"];
				$mvt_cloture_j 		 = ($list["mvt_cloture_j"] == null) ? 0 : $list["mvt_cloture_j"];
				$mvt_non_traite_j 	 = ($list["mvt_non_traite_j"] == null) ? 0 : $list["mvt_non_traite_j"];
				$mvt_non_traite_recep_jmoins 	 = $list["mvt_non_traite_recep_jmoins"];
				$mvt_cloture_recep_jmoins 	     = $list["mvt_cloture_recep_jmoins"];
				$mvt_ok 	 		 = $list["mvt_ok"];
				$mvt_ko_scan 	 	 = $list["mvt_ko_scan"];
				$mvt_hors_perimetre  = $list["mvt_hors_perimetre"];
				$mvt_ko_ks 	         = $list["mvt_ko_ks"];
				$mvt_ko_ke 	         = $list["mvt_ko_ke"];
				$mvt_ko_kd 	         = $list["mvt_ko_kd"];
				$mvt_ci 	         = $list["mvt_ci"];
				
				 $mvt_cloture_recep_jmoins     = ($mvt_cloture_recep_jmoins == null) ? 0 : $mvt_cloture_recep_jmoins ;
				 $mvt_non_traite_recep_jmoins  = ($mvt_non_traite_recep_jmoins == null) ? 0 : $mvt_non_traite_recep_jmoins ;
				 echo "</br>SOLDE MOUVEMENTS</br>";		
				 
				 echo  "--------------</br> Date : ".$date_j." ,</br>Reception J : ".$mvt_total_j.", </br>Réception J - Cloturé : ".$mvt_cloture_j.", </br>Réception J - Non traité : ".$mvt_non_traite_j.", </br>Réception J Moins - Cloturé : ".$mvt_cloture_recep_jmoins.", </br>Réception J Moins - Non traité : ".$mvt_non_traite_recep_jmoins.",</br> OK : ".$mvt_ok.",</br>KO Scan : ".$mvt_ko_scan."</br>";
				 
				 $rubrique = 2;
				 $cpt_data  	  = tcheck_data($date_j, $id_societe, $rubrique,$conn);
				
				 $col_date_ttt    = $val_date_ttt = $where_date_ttt = "";
				
				 //--- update data si màj de données
				 if($cpt_data  == 0){
								  
				 $insert   = "INSERT INTO 
									solde_plis_mvt_journalier 
									(societe, date_j, pli_total_reception_j, pli_traite_reception_j, pli_non_traite_reception_j, pli_traite_reception_jmoins, pli_non_traite_reception_jmoins,ok, ko_scan, hors_perimetre, ks, ke, kd, ci,rubrique)
					           VALUES ('".$id_societe."','".$date_j."','".$mvt_total_j."','".$mvt_cloture_j."','".$mvt_non_traite_j."','".$mvt_cloture_recep_jmoins."','".$mvt_non_traite_recep_jmoins."','".$mvt_ok."','".$mvt_ko_scan."','".$mvt_hors_perimetre."','".$mvt_ko_ks."','".$mvt_ko_ke."','".$mvt_ko_kd."','".$mvt_ci."',".$rubrique.")";
					
				 $result_insert = pg_query($conn,$insert)or die($insert);
				 echo '</br>-------------------------------</br>';
				 echo '</br>Result_insert:'.$result_insert."</br>";
				 if($result_insert)
				 echo "</br>------------- insert ok---------------</br>";
				 else echo "</br>------------- insert échoué---------------</br>";
			
			}
				
		}
		}
	
		get_update_flag_traite($date_du_jour,$conn) ;
		
	function get_reception_societe($date_reception,$conn){
			$list_reception = array();				
			 $sql="select count(view_pli_stat.id_pli) pli 
					,case when 
					view_pli_stat.societe is null 
					then view_pli.societe::character varying  
					else case when view_pli.societe::character varying <> view_pli_stat.societe then view_pli_stat.societe
					else view_pli_stat.societe
					end end as societe_vf
				from view_pli
				inner join view_lot on view_lot.id_lot_numerisation =view_pli.id_lot_numerisation
				left join view_pli_stat on view_pli_stat.id_pli = view_pli.id_pli    
				where view_lot.date_courrier::date = '".$date_reception."' 
				group by societe_vf";
				$res 		= @pg_query( $conn,$sql) OR die("erreur function global() !!");			
				$iNbr       = pg_num_rows($res);
				$i = 0;
				if ( $iNbr > 0){	
					
					while ($ores = pg_fetch_assoc($res)) {
						
					 $list_reception[$i]['pli']  		= $ores['pli'];			 
					 $list_reception[$i]['societe_vf']  = $ores['societe_vf'];
					$i++;					 
					}
				}
				return $list_reception;
		}
		
		function tcheck_data($date_j, $d_societe, $rubrique,$conn){
			
			 $where_date_ttt = "";
			 
			 $sql = "SELECT id_solde FROM solde_plis_mvt_journalier 
					WHERE date_j = '".$date_j."' and societe = '".$d_societe."' and rubrique = ".$rubrique." 	
					";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			
			return $i;			
		}
		function tcheck_data_plis_chgmt_etat($date_j, $id_pli, $conn){
			
			 $where_date_ttt = "";
			 
			 $sql = "SELECT id FROM solde_plis_chgmt_etat 
					WHERE date_traitement = '".$date_j."' and id_pli = '".$id_pli."' ";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			
			return $i;			
		}
		function get_societe($conn){
			$list_soc = array();
			$sql = "SELECT id, nom_societe FROM societe order by id asc";			
			$res = @pg_query( $conn,$sql);		
			$j   = pg_num_rows($res);
			$i = 0;
			if ( $j > 0){			
				while ($ores = pg_fetch_assoc($res)) {				
				    
					$list_soc[$i]['id']       = $ores['id'];
					$list_soc[$i]['societe']  = $ores['nom_societe'];			
					$i++;
				}
			}
			return $list_soc;
		}
		
		function maj_reliquat_existant($date_courrier,$societe,$nb_pli,$conn){ 
			$reliquat = 0;
			$sql = "SELECT
						case when reception_posterieure is null or reception_posterieure = 0 
						then ".$nb_pli." else 
						reception_posterieure+".$nb_pli."
						end as nb_pli
					FROM
					solde_plis_mvt_journalier
					Where date_j = '".$date_courrier."'
					and  societe = '".$societe."'
					and rubrique = 1";
			$res = pg_query( $conn,$sql);		
			$j   = pg_num_rows($res);
			if ( $j > 0){			
				while ($rel = pg_fetch_assoc($res)) {				
				   $reliquat = $rel['nb_pli'];
				   if($reliquat > 0){
					   $update   = "UPDATE solde_plis_mvt_journalier 
										SET reception_posterieure = ".$reliquat."
									WHERE date_j = '".$date_courrier."'
									and  societe = '".$societe."'
									and rubrique = 1";
										
						pg_query($conn,$update);
				   }
				}
			}
			
		}
		
		function get_reliquat($date_reception,$conn){
			
			$list_reliquat = array();
			$sql = "SELECT
						sum(pli) as pli, societe, date_courrier
					FROM
					(SELECT 
						case when lot_numerisation.date_creation::date>date_courrier::date then count(id_pli) else 0 end as pli, societe,date_courrier::date as date_courrier 
						FROM f_pli as pli
						inner join f_lot_numerisation as lot_numerisation on lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						where lot_numerisation.date_creation::date = '".$date_reception."'
						and  lot_numerisation.date_creation::date != date_courrier::date
						group by societe,lot_numerisation.date_creation,date_courrier
					) as res group by societe,date_courrier";
			$res = pg_query( $conn,$sql);		
			$j   = pg_num_rows($res);
			if ( $j > 0){			
				while ($rel = pg_fetch_assoc($res)) {				
				   $id_soc = $rel['societe'];
				   $daty   = $rel['date_courrier'];
				   $list_reliquat[$id_soc]['id_soc']  		= $rel['societe'];
				   $list_reliquat[$id_soc]['date_courrier']  = $rel['date_courrier'];
				   $list_reliquat[$id_soc]['pli']  	  	    = $rel['pli'];
				}
			}
			return $list_reliquat;
			
		}
		function get_reception_j($date_reception,$conn){
			// reception j ==> total ? cloturé ? et restant ?
			 $list_traitement_j = array();
			 $sql = "SELECT
						tab1.id as societe_vf,
						case when date_courrier is null then '".$date_reception."' else date_courrier end as date_courrier,
						case when pli_total_j is null then 0 else pli_total_j end as pli_total_j ,
						case when pli_ok is null then 0 else pli_ok end as pli_ok ,
						case when pli_ko_scan is null then 0 else pli_ko_scan end as pli_ko_scan ,
						case when pli_hors_perimetre is null then 0 else pli_hors_perimetre end as pli_hors_perimetre ,
						case when pli_ko_ks is null then 0 else pli_ko_ks end as pli_ko_ks ,
						case when pli_ko_ke is null then 0 else pli_ko_ke end as pli_ko_ke ,
						case when pli_ko_kd is null then 0 else pli_ko_kd end as pli_ko_kd ,
						case when pli_ci is null then 0 else pli_ci end as pli_ci ,
						case when pli_cloture_j is null then 0 else pli_cloture_j end as pli_cloture_j ,
						case when (pli_total_j is null)  then 0 else pli_total_j - pli_cloture_j end as non_traite_j,
						plis					
					FROM
					(
						SELECT societe.id from societe
					)as tab1	
					left join 
					(
						SELECT
							societe_vf,date_courrier,
							count(id_pli) pli_total_j,
							sum(pli_ok) pli_ok,
							sum(pli_ko_scan) pli_ko_scan,
							sum(pli_hors_perimetre) pli_hors_perimetre,
							sum(pli_ko_ks) pli_ko_ks,
							sum( pli_ko_ke) pli_ko_ke,
							sum(pli_ko_kd) pli_ko_kd,
							sum(pli_ci) pli_ci,
							sum(pli_cloture) as pli_cloture_j,
							case when  (pli_cloture = 1 or pli_ok = 1 or pli_ko_scan = 1 or pli_hors_perimetre = 1 or pli_ko_ks = 1 or pli_ko_ke = 1 or pli_ko_kd = 1 or pli_ci = 1 ) then string_agg(id_pli::character varying||'|'||flag_traitement||'|'||statut_saisie,',') 
							else null end as plis
						FROM 
						(
							  select view_pli_stat.id_pli ,view_lot.date_courrier::date as date_courrier
									,case when 
									view_pli_stat.societe is null 
									then view_pli.societe 
									else case when view_pli.societe != view_pli_stat.societe then view_pli_stat.societe
									else view_pli_stat.societe
									end end as societe_vf,
									case when pli_ok = 1 and dt_event::date ='".$date_reception."' then 1 else 0 end as pli_ok,
									case when pli_ko_scan = 1 and dt_event::date ='".$date_reception."' then 1 else 0 end as pli_ko_scan,
									case when pli_hors_perimetre = 1 and dt_event::date ='".$date_reception."' then 1 else 0 end as pli_hors_perimetre,
									case when pli_ko_ks = 1 and dt_event::date = '".$date_reception."' then 1 else 0 end as pli_ko_ks,
									case when pli_ko_ke = 1 and dt_event::date ='".$date_reception."' then 1 else 0 end as pli_ko_ke,
									case when pli_ko_kd = 1 and dt_event::date ='".$date_reception."' then 1 else 0 end as pli_ko_kd,
									case when pli_ci = 1 and dt_event::date = '".$date_reception."' then 1 else 0 end as pli_ci,
									case when ( pli_cloture = 1 and dt_event::date ='".$date_reception."') 
									or (pli_ko_scan = 1 and dt_event::date ='".$date_reception."')
									or (pli_hors_perimetre = 1 and dt_event::date ='".$date_reception."')	
									then 1 else 0 end as pli_cloture,
									view_pli_stat.flag_traitement,
									view_pli_stat.statut_saisie
									
								from view_pli
								inner join view_lot on view_lot.id_lot_numerisation =view_pli.id_lot_numerisation
								left join view_pli_stat on view_pli_stat.id_pli = view_pli.id_pli    
								where view_lot.date_courrier::date = '".$date_reception."'
							
						) as res
						group by societe_vf,date_courrier,pli_ok,pli_ko_scan,pli_hors_perimetre,pli_ko_ks, pli_ko_ke,pli_ko_kd,pli_ci,pli_cloture
					)as tab on tab1.id = tab.societe_vf::integer
					order by tab1.id asc
					";
			/*echo "<pre>";
			print_r($sql);
			echo "</pre>"; exit;
			*/
			
			$res = @pg_query( $conn,$sql);		
			$j   = pg_num_rows($res);
			//$i = 0;
			if ( $j > 0){			
				while ($ores = pg_fetch_assoc($res)) {				
				    $id_soc = $ores['societe_vf'];
					$list_traitement_j[$id_soc]['societe_vf']     = $ores['societe_vf'];
					$list_traitement_j[$id_soc]['date_courrier']  = $ores['date_courrier'];			
					$list_traitement_j[$id_soc]['pli_total_j']    += $ores['pli_total_j'];			
					$list_traitement_j[$id_soc]['pli_cloture_j']  += $ores['pli_cloture_j'];			
					$list_traitement_j[$id_soc]['pli_non_traite_j']  += $ores['non_traite_j'];			
					$list_traitement_j[$id_soc]['pli_ok']  		 += $ores['pli_ok'];			
					$list_traitement_j[$id_soc]['pli_ko_scan']    += $ores['pli_ko_scan'];			
					$list_traitement_j[$id_soc]['pli_hors_perimetre']  += $ores['pli_hors_perimetre'];			
					$list_traitement_j[$id_soc]['pli_ko_ks']      += $ores['pli_ko_ks'];			
					$list_traitement_j[$id_soc]['pli_ko_ke']      += $ores['pli_ko_ke'];			
					$list_traitement_j[$id_soc]['pli_ko_kd']      += $ores['pli_ko_kd'];			
					$list_traitement_j[$id_soc]['pli_ci']         += $ores['pli_ci'];					
					$list_traitement_j[$id_soc]['plis']           = $ores['plis'];					
					
					if($ores['plis'] != '' && $ores['plis'] != ',')
					$list_traitement_j[$id_soc]['plis']         	= ($list_traitement_j[$id_soc]['plis'] == '') ? $ores['plis'] : ','.$ores['plis'];	
					
				}
			}
			return $list_traitement_j;
			
		}
		
		function get_reception_jmoins_traitement_j($date_reception,$conn){
			// reception j moins ==> cloturé à j ? et restant à j ?
			 $list_mvt_recep_jmoins = array();
			 $sql = "SELECT 
					societe_vf,
					dt_event,
					sum(pli_ok) as pli_ok,
					sum(pli_ko_scan) as pli_ko_scan,
					sum(pli_hors_perimetre) as pli_hors_perimetre,
					sum(pli_ko_ks) as pli_ko_ks,
					sum(pli_ko_ke) as pli_ko_ke,
					sum(pli_ko_kd) as pli_ko_kd,
					sum(pli_ci) as pli_ci,
					sum(traite_j) as traite_j,
					sum(non_traite_j) as non_traite_j,
					string_agg(plis::character varying,',') as plis
				FROM 
				(SELECT 
						tab1.id as societe_vf,
						case when dt_event is null then '2200-01-01' else dt_event end as dt_event,
						case when pli_ok is null then 0 else pli_ok end as pli_ok ,
						case when pli_ko_scan is null then 0 else pli_ko_scan end as pli_ko_scan ,
						case when pli_hors_perimetre is null then 0 else pli_hors_perimetre end as pli_hors_perimetre ,
						case when pli_ko_ks is null then 0 else pli_ko_ks end as pli_ko_ks ,
						case when pli_ko_ke is null then 0 else pli_ko_ke end as pli_ko_ke ,
						case when pli_ko_kd is null then 0 else pli_ko_kd end as pli_ko_kd ,
						case when pli_ci is null then 0 else pli_ci end as pli_ci ,
						case when traite_j is null then 0 else traite_j end as traite_j ,
						pli_total - traite_j as non_traite_j,
						plis
					FROM
					(	SELECT societe.id from societe
					)as tab1	
					left join 
					(   select
							case when (pli_cloture = 1 and dt_event::date = '".$date_reception."') or 
								(pli_ko_scan = 1 and dt_event::date = '".$date_reception."') or 
								(pli_hors_perimetre = 1 and dt_event::date = '".$date_reception."')
								then sum(pli_cloture)+sum(pli_ko_scan)+sum(pli_hors_perimetre) else 0 end as traite_j,
							case when (pli_ok = 1 and dt_event::date = '".$date_reception."') then sum(pli_ok) else 0 end as pli_ok,
							case when (pli_ko_scan = 1 and dt_event::date = '".$date_reception."') then sum(pli_ko_scan) else 0 end as pli_ko_scan,
							case when (pli_hors_perimetre = 1 and dt_event::date = '".$date_reception."') then sum(pli_hors_perimetre) else 0 end as pli_hors_perimetre,
							case when (pli_ko_ks = 1 and dt_event::date = '".$date_reception."') then sum(pli_ko_ks) else 0 end as pli_ko_ks,
							case when (pli_ko_ke = 1 and dt_event::date = '".$date_reception."') then sum(pli_ko_ke) else 0 end as pli_ko_ke,
							case when (pli_ko_kd = 1 and dt_event::date = '".$date_reception."') then sum(pli_ko_kd) else 0 end as pli_ko_kd,
							case when (pli_ci = 1 and dt_event::date = '".$date_reception."') then sum(pli_ci) else 0 end as pli_ci,
							case when dt_event is null then '2200-01-01' else dt_event end as dt_event,
							count(id_pli) as pli_total,
							societe_vf,
							case when ( (pli_cloture = 1 or pli_ok = 1 or pli_ko_scan = 1 or pli_hors_perimetre = 1 or pli_ko_ks = 1 or pli_ko_ke = 1 or pli_ko_kd = 1 or pli_ci = 1 ) and dt_event::date = '".$date_reception."') then string_agg(id_pli::character varying||'|'||flag_traitement||'|'||statut_saisie,',') 
							else null end as plis
						from

						(
							select view_pli_stat.id_pli ,case when 
								view_pli_stat.societe is null 
								then view_pli.societe  
								else case when view_pli.societe != view_pli_stat.societe then view_pli_stat.societe
								else view_pli_stat.societe
								end end as societe_vf,pli_cloture,
								pli_ok,pli_ko_scan,pli_hors_perimetre,pli_ko_ks, pli_ko_ke,pli_ko_kd,pli_ci,dt_event::date,
								view_pli_stat.flag_traitement,
								view_pli_stat.statut_saisie
							from view_pli
							inner join view_lot on view_lot.id_lot_numerisation =view_pli.id_lot_numerisation
							left join view_pli_stat on view_pli_stat.id_pli = view_pli.id_pli    
							where (pli_cloture = 0 and pli_ko_scan = 0 and pli_hors_perimetre = 0 and view_lot.date_courrier::date < '".$date_reception."')  
							or ((pli_cloture = 1 or pli_ko_scan = 1 or pli_hors_perimetre = 1 )and view_lot.date_courrier::date <'".$date_reception."' and dt_event::date = '".$date_reception."')
							or ((pli_cloture = 1 or pli_ko_scan = 1 or pli_hors_perimetre = 1 )and view_lot.date_courrier::date <'".$date_reception."' and dt_event::date > '".$date_reception."')
							order by view_pli_stat.id_pli asc
						) as tab
						group by societe_vf,pli_cloture,pli_ko_scan,pli_hors_perimetre,pli_ok,pli_ko_scan,pli_hors_perimetre,pli_ko_ks, pli_ko_ke,pli_ko_kd,pli_ci,dt_event
					) as tab on tab1.id = tab.societe_vf::integer
				) as res
				group by societe_vf,dt_event	
				order by societe_vf asc
				";
			
			
			
			$res = pg_query( $conn,$sql)or die(pg_last_error().'-----');		
			$j   = pg_num_rows($res) or die('die');
			if ( $j > 0){			
				while ($ores = pg_fetch_assoc($res)) {	

					$id_soc = $ores['societe_vf'];
				    $list_non_traite_jmoins[$id_soc]['pli_non_traite_j']  	+= $ores['non_traite_j'];			 
				    $list_non_traite_jmoins[$id_soc]['pli_traite_j']  	    += $ores['traite_j'];			 
					$list_non_traite_jmoins[$id_soc]['societe_vf']       	= $ores['societe_vf'];
					$list_non_traite_jmoins[$id_soc]['dt_event']       	    = $ores['dt_event'];
					$list_non_traite_jmoins[$id_soc]['pli_ok']  		 	+= $ores['pli_ok'];			
					$list_non_traite_jmoins[$id_soc]['pli_ko_scan']    		+= $ores['pli_ko_scan'];			
					$list_non_traite_jmoins[$id_soc]['pli_hors_perimetre']  += $ores['pli_hors_perimetre'];			
					$list_non_traite_jmoins[$id_soc]['pli_ko_ks']      		+= $ores['pli_ko_ks'];			
					$list_non_traite_jmoins[$id_soc]['pli_ko_ke']     		+= $ores['pli_ko_ke'];			
					$list_non_traite_jmoins[$id_soc]['pli_ko_kd']      		+= $ores['pli_ko_kd'];			
					$list_non_traite_jmoins[$id_soc]['pli_ci']         		+= $ores['pli_ci'];	
					
					if($ores['plis'] != '' && $ores['plis'] != ',')
					$list_non_traite_jmoins[$id_soc]['plis']         	= ($list_non_traite_jmoins[$id_soc]['plis'] == '') ? $ores['plis'] : ','.$ores['plis'];	
					
				}
			}
			return $list_non_traite_jmoins;
			
		}
	
				
		function get_reception_j_mouvement($date_reception,$conn){
			// reception j ==> total ? cloturé ? et restant ?
			 $list_mvt_recep_j = array();
			 $sql = "SELECT tab1.id as societe_vf,
						case when date_courrier is null then '".$date_reception."' else date_courrier end as date_courrier,
						case when mvt_total_j is null then 0 else mvt_total_j end as mvt_total_j ,
						case when mvt_cloture_j is null then 0 else mvt_cloture_j end as mvt_cloture_j ,
						case when mvt_non_traite_j is null then 0 else mvt_non_traite_j end as mvt_non_traite_j ,
						case when mvt_ok is null then 0 else mvt_ok end as mvt_ok ,
						case when mvt_ko_scan is null then 0 else mvt_ko_scan end as mvt_ko_scan ,
						case when mvt_hors_perimetre is null then 0 else mvt_hors_perimetre end as mvt_hors_perimetre ,
						case when mvt_ko_ks is null then 0 else mvt_ko_ks end as mvt_ko_ks ,
						case when mvt_ko_ke is null then 0 else mvt_ko_ke end as mvt_ko_ke ,
						case when mvt_ko_kd is null then 0 else mvt_ko_kd end as mvt_ko_kd ,
						case when mvt_ci is null then 0 else mvt_ci end as mvt_ci 
					FROM
					(
						SELECT societe.id from societe
					)as tab1	
					left join 
					(
						SELECT
							societe_vf,date_courrier,
							mvt_total_j,
							mvt_cloture_j,
							mvt_total_j - mvt_cloture_j as mvt_non_traite_j ,
							mvt_ok,
							mvt_ko_scan,
							mvt_hors_perimetre,
							mvt_ko_ks,
							mvt_ko_ke,
							mvt_ko_kd,
							mvt_ci
						FROM
						(
							SELECT
								societe_vf,date_courrier,
								sum(mvt_ok) as mvt_ok,
								sum(mvt_ko_scan) as mvt_ko_scan,
								sum(mvt_hors_perimetre) as mvt_hors_perimetre,
								sum(mvt_ko_ks) as mvt_ko_ks,
								sum(mvt_ko_ke) as mvt_ko_ke,
								sum(mvt_ko_kd) as mvt_ko_kd,
								sum(mvt_ci) as mvt_ci,
								sum(mvt_cloture) as mvt_cloture_j,
								sum(nb_mvt) mvt_total_j				
								
								
							FROM 
							(
								  select view_pli_stat.id_pli ,view_lot.date_courrier::date
										,case when 
										view_pli_stat.societe is null 
										then view_pli.societe  
										else case when view_pli.societe != view_pli_stat.societe then view_pli_stat.societe
										else view_pli_stat.societe
										end end as societe_vf,
										case when nb_mvt is null then 0 else nb_mvt end as nb_mvt,
										case when pli_ok = 1 and dt_event::date ='".$date_reception."' then nb_mvt else 0 end as mvt_ok,
										case when pli_ko_scan = 1 and dt_event::date ='".$date_reception."' then nb_mvt else 0 end as mvt_ko_scan,
										case when pli_hors_perimetre = 1 and dt_event::date ='".$date_reception."' then nb_mvt else 0 end as mvt_hors_perimetre,
										case when pli_ko_ks = 1 and dt_event::date = '".$date_reception."' then nb_mvt else 0 end as mvt_ko_ks,
										case when pli_ko_ke = 1 and dt_event::date = '".$date_reception."' then nb_mvt else 0 end as mvt_ko_ke,
										case when pli_ko_kd = 1 and dt_event::date = '".$date_reception."' then nb_mvt else 0 end as mvt_ko_kd,
										case when pli_ci = 1 and dt_event::date = '".$date_reception."' then nb_mvt else 0 end as mvt_ci,
										case when (pli_cloture = 1 and dt_event::date = '".$date_reception."') or
											(pli_ko_scan = 1 and dt_event::date = '".$date_reception."') or
											(pli_hors_perimetre = 1 and dt_event::date = '".$date_reception."')										
										then nb_mvt else 0 end as mvt_cloture,					
										pli_ok,pli_ko_scan,pli_hors_perimetre,pli_ko_ks, pli_ko_ke,pli_ko_kd,pli_ci,pli_cloture,
										case when pli_ko_scan = 1 then 0 else nb_mvt end as nb_mvt_koscan,
										case when pli_hors_perimetre = 1 then 0 else nb_mvt end as nb_mvt_hp,
										dt_event::date
									from view_pli
									inner join view_lot on view_lot.id_lot_numerisation =view_pli.id_lot_numerisation
									left join view_pli_stat on view_pli_stat.id_pli = view_pli.id_pli    
									left join view_nb_mouvement_pli on view_pli_stat.id_pli = view_nb_mouvement_pli.id_pli    
									where view_lot.date_courrier::date = '".$date_reception."'
								
							) as res
							group by societe_vf,date_courrier
						) as resultat
					)as tab on tab1.id = tab.societe_vf::integer
					order by tab1.id asc";
			
			/*echo "<pre>";
			print_r($sql);
			echo "</pre>";exit;
			*/
			
			$res = @pg_query( $conn,$sql);		
			$j   = pg_num_rows($res);
			$i = 0;
			if ( $j > 0){			
				while ($ores = pg_fetch_assoc($res)) {				
				    $id_soc = $ores['societe_vf'];
					$list_mvt_recep_j[$id_soc]['societe_vf']     = $ores['societe_vf'];
					$list_mvt_recep_j[$id_soc]['date_courrier']  = $ores['date_courrier'];			
					$list_mvt_recep_j[$id_soc]['mvt_total_j']    += $ores['mvt_total_j'];			
					$list_mvt_recep_j[$id_soc]['mvt_cloture_j']  += $ores['mvt_cloture_j'];			
					$list_mvt_recep_j[$id_soc]['mvt_non_traite_j']  += $ores['mvt_non_traite_j'];			
					$list_mvt_recep_j[$id_soc]['mvt_ok']  		 += $ores['mvt_ok'];			
					$list_mvt_recep_j[$id_soc]['mvt_ko_scan']    += $ores['mvt_ko_scan'];			
					$list_mvt_recep_j[$id_soc]['mvt_hors_perimetre']  += $ores['mvt_hors_perimetre'];			
					$list_mvt_recep_j[$id_soc]['mvt_ko_ks']      += $ores['mvt_ko_ks'];			
					$list_mvt_recep_j[$id_soc]['mvt_ko_ke']      += $ores['mvt_ko_ke'];			
					$list_mvt_recep_j[$id_soc]['mvt_ko_kd']      += $ores['mvt_ko_kd'];			
					$list_mvt_recep_j[$id_soc]['mvt_ci']         += $ores['mvt_ci'];					
					//$i++;
				}
			}
			return $list_mvt_recep_j;
			
		}
		
		function get_reception_jmoins_mouvement_j($date_reception,$conn){
			// reception j moins ==> cloturé à j ? et restant à j ?
			 $list_mvt_recep_jmoins = array();
			 $sql = "SELECT tab1.id as societe_vf,
						case when mvt_total_j is null then 0 else mvt_total_j end as mvt_total_j ,
						case when mvt_cloture_j is null then 0 else mvt_cloture_j end as mvt_cloture_j ,
						case when mvt_non_traite_j is null then 0 else mvt_non_traite_j end as mvt_non_traite_j ,
						case when mvt_ok is null then 0 else mvt_ok end as mvt_ok ,
						case when mvt_ko_scan is null then 0 else mvt_ko_scan end as mvt_ko_scan ,
						case when mvt_hors_perimetre is null then 0 else mvt_hors_perimetre end as mvt_hors_perimetre ,
						case when mvt_ko_ks is null then 0 else mvt_ko_ks end as mvt_ko_ks ,
						case when mvt_ko_ke is null then 0 else mvt_ko_ke end as mvt_ko_ke ,
						case when mvt_ko_kd is null then 0 else mvt_ko_kd end as mvt_ko_kd ,
						case when mvt_ci is null then 0 else mvt_ci end as mvt_ci 
					FROM
					(
						SELECT societe.id from societe
					)as tab1	
					left join 
					(
						
						SELECT
							societe_vf,
							sum(mvt_total_j) mvt_total_j,
							sum(mvt_cloture_j) mvt_cloture_j,
							sum(mvt_total_j) - sum(mvt_cloture_j) as mvt_non_traite_j ,
							sum(mvt_ok) mvt_ok,
							sum(mvt_ko_scan) mvt_ko_scan,
							sum(mvt_hors_perimetre) mvt_hors_perimetre,
							sum(mvt_ko_ks) mvt_ko_ks,
							sum(mvt_ko_ke) mvt_ko_ke,
							sum(mvt_ko_kd) mvt_ko_kd,
							sum(mvt_ci) mvt_ci
						FROM
						(
							select
								dt_event,
								societe_vf,
								case when (pli_cloture = 1 and dt_event::date = '".$date_reception."' )
									or (pli_ko_scan = 1 and dt_event::date = '".$date_reception."' )
									or (pli_hors_perimetre = 1 and dt_event::date = '".$date_reception."' )
								then sum(nb_mvt) else 0 end mvt_cloture_j,
								case when pli_ok = 1  and dt_event::date = '".$date_reception."' then sum(nb_mvt) else 0 end mvt_ok,
								case when pli_ko_scan = 1  and dt_event::date = '".$date_reception."' then sum(nb_mvt) else 0 end mvt_ko_scan,
								case when pli_hors_perimetre = 1  and dt_event::date = '".$date_reception."' then sum(nb_mvt) else 0 end mvt_hors_perimetre,
								case when pli_ko_ks = 1  and dt_event::date = '".$date_reception."' then sum(nb_mvt) else 0 end mvt_ko_ks,
								case when pli_ko_ke = 1  and dt_event::date = '".$date_reception."' then sum(nb_mvt) else 0 end mvt_ko_ke,
								case when pli_ko_kd = 1  and dt_event::date = '".$date_reception."' then sum(nb_mvt) else 0 end mvt_ko_kd,
								case when pli_ci = 1  and dt_event::date = '".$date_reception."' then sum(nb_mvt) else 0 end mvt_ci,
								sum(nb_mvt) mvt_total_j
								
								
							from

							(
								select view_pli_stat.id_pli ,case when 
									view_pli_stat.societe is null 
									then view_pli.societe 
									else case when view_pli.societe != view_pli_stat.societe then view_pli_stat.societe
									else view_pli_stat.societe
									end end as societe_vf,
									case when nb_mvt is null then 0 else nb_mvt end as nb_mvt,
									pli_cloture,
									pli_ok,pli_ko_scan,pli_hors_perimetre,pli_ko_ks, pli_ko_ke,pli_ko_kd,pli_ci,dt_event::date
								from view_pli
								inner join view_lot on view_lot.id_lot_numerisation =view_pli.id_lot_numerisation
								left join view_pli_stat on view_pli_stat.id_pli = view_pli.id_pli
								left join view_nb_mouvement_pli on view_pli_stat.id_pli = view_nb_mouvement_pli.id_pli       
								where (pli_cloture = 0  and pli_ko_scan = 0 and pli_hors_perimetre = 0 and view_lot.date_courrier::date < '".$date_reception."')  
								or ((pli_cloture = 1 or pli_ko_scan = 1 or pli_hors_perimetre = 1) and view_lot.date_courrier::date <'".$date_reception."' and dt_event::date = '".$date_reception."')
								or ((pli_cloture = 1 or pli_ko_scan = 1 or pli_hors_perimetre = 1) and view_lot.date_courrier::date <'".$date_reception."' and dt_event::date > '".$date_reception."')
							) as tab
							group by societe_vf,pli_cloture,dt_event,pli_ok,pli_ko_scan,pli_hors_perimetre,pli_ko_ks, pli_ko_ke,pli_ko_kd,pli_ci,pli_cloture
						) as resultat group by 	societe_vf
					)as tab on tab1.id = tab.societe_vf::integer
					order by tab1.id asc
					";
			
			
			/*echo "<pre>";
			print_r($sql);
			echo "</pre>";exit;
			*/
			
			$res = @pg_query( $conn,$sql);		
			$j   = pg_num_rows($res);
			$i = 0;
			if ( $j > 0){			
				while ($ores = pg_fetch_assoc($res)) {
					 $id_soc = $ores['societe_vf'];
				    $list_mvt_recep_jmoins[$id_soc]['societe']  		= $ores['societe_vf'];			 
				    $list_mvt_recep_jmoins[$id_soc]['mvt_total_j']  	+= $ores['mvt_total_j'];			 
				    $list_mvt_recep_jmoins[$id_soc]['mvt_non_traite_j']  	+= $ores['mvt_non_traite_j'];			 
				    $list_mvt_recep_jmoins[$id_soc]['mvt_cloture_j']  	    += $ores['mvt_cloture_j'];			 
					$list_mvt_recep_jmoins[$id_soc]['mvt_ok']  		  += $ores['mvt_ok'];			
					$list_mvt_recep_jmoins[$id_soc]['mvt_ko_scan']    += $ores['mvt_ko_scan'];			
					$list_mvt_recep_jmoins[$id_soc]['mvt_hors_perimetre']  += $ores['mvt_hors_perimetre'];			
					$list_mvt_recep_jmoins[$id_soc]['mvt_ko_ks']      += $ores['mvt_ko_ks'];			
					$list_mvt_recep_jmoins[$id_soc]['mvt_ko_ke']      += $ores['mvt_ko_ke'];			
					$list_mvt_recep_jmoins[$id_soc]['mvt_ko_kd']      += $ores['mvt_ko_kd'];			
					$list_mvt_recep_jmoins[$id_soc]['mvt_ci']         += $ores['mvt_ci'];			
					//$i++;
				}
			}
			return $list_mvt_recep_jmoins;
			
		}
		
		
		function get_update_flag_traite($date_reception,$conn){
				$cpt_solde = 0;
				$sql_last_id_solde = "SELECT 
						flag_solde_cloture
					FROM solde_plis_mvt_journalier
					where date_j::date ='".$date_reception."'
					and flag_solde_cloture > 0";
				$res_ids = @pg_query( $conn,$sql_last_id_solde);
				$cpt_solde   = pg_num_rows($res_ids);
				if($cpt_solde > 0){
					while ($ores_ids = pg_fetch_assoc($res_ids)) {
						$idx = $ores_ids["flag_solde_cloture"];
					}
				}else 
				{	
					$sql_nb = "
					SELECT 
						max(flag_solde_cloture)+1 as idx
					FROM data_pli ";
					$res_idx = @pg_query( $conn,$sql_nb);
					while ($ores_idx = pg_fetch_assoc($res_idx)) {
						$idx = $ores_idx["idx"];
					}
				}
				
				 $sql = "
					select view_pli_stat.id_pli 
					from view_pli
					inner join view_lot on view_lot.id_lot_numerisation =view_pli.id_lot_numerisation
					left join view_pli_stat on view_pli_stat.id_pli = view_pli.id_pli    
					Where ((pli_cloture = 1  or pli_ko_scan = 1 or pli_hors_perimetre = 1 ) and view_lot.date_courrier::date ='".$date_reception."' and dt_event::date = '".$date_reception."')

					union

					select view_pli_stat.id_pli
					from view_pli
					inner join view_lot on view_lot.id_lot_numerisation =view_pli.id_lot_numerisation
					left join view_pli_stat on view_pli_stat.id_pli = view_pli.id_pli    
					Where   ((pli_cloture = 1  or pli_ko_scan = 1 or pli_hors_perimetre = 1) and view_lot.date_courrier::date <'".$date_reception."' and dt_event::date = '".$date_reception."')
				";
				
			
				$res = @pg_query( $conn,$sql);		
				$j   = pg_num_rows($res);
				$i = 0;
				if ( $j > 0){			
					while ($ores = pg_fetch_assoc($res)) {
					 $id_pli = $ores['id_pli'];
					 if ($idx > 0)
					 {	 
						$update   = "UPDATE data_pli SET flag_solde_cloture = ".$idx."
									WHERE id_pli = ".$id_pli."";
										
						$result_update = pg_query($conn,$update)or die($update);
						
						$update_solde   = "UPDATE solde_plis_mvt_journalier SET flag_solde_cloture = ".$idx."
									WHERE date_j = '".$date_reception."'";
										
						pg_query($conn,$update_solde)or die($update_solde);
					 }
					/*
					 echo '</br>-------------------------------</br>';
					 echo '</br>Result_update:'.$result_update."</br>";
					 if($result_update)
					 echo "</br>------------- update ok---------------</br>";
					 else echo "</br>------------- update échoué---------------</br>";
					*/					 
					}
				}
								
			}
			
		
	//#------------------------------------------ FIN STAT MENSUEL ---------------------------------
   // ################################################
	$module_nom  = "CRON SOLDE - GED BAYARD" ;
	$nom_fichier = "10.90\GED\bayard\crons\cron_solde.php" ;
	$date_jour   = date("Y-m-d");
	$statut      = "termine" ;

	$sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple) 
	VALUES ('$module_nom','$nom_fichier','$date_jour','$statut','$couple') " ;
	$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;

	// ################################################ 

?>