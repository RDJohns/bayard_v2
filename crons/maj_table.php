﻿<?php 
		
		$conn  = getConnBayard(); 
		
		setlocale (LC_TIME, 'fr_FR');	

		$array_tlmc   = array();
		$date_du_jour = date("Y-m-d");
		
		
		// ################################################
		$module_nom  = "CRON UPD - GED BAYARD" ;
		$nom_fichier = "10.90\GED\bayard\crons\maj_table.php" ;
		$date_jour   = date("Y-m-d");
		$couple 	 = date("YmdHis");
		$statut      = "debut" ;
		echo "***************".$statut."*************</br>";

		$sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple) 
		VALUES ('$module_nom','$nom_fichier','$date_jour','$statut','$couple') " ;
		$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;

		// ################################################  
		
		$sql  = "SELECT id, id_pli FROM mouvement WHERE (societe is null or societe = 0) order by id_pli asc"	;
		$res  = pg_query($sql) OR die(" Erreur sql au niveau de la requete de selection de la table mouvement ");	
		
		$iNbr = pg_num_rows($res);
		
		if ( $iNbr > 0){	
		
			while ($ores = pg_fetch_assoc($res)) {
				
			$id		    = $ores['id'];			 
			$id_pli     = $ores['id_pli'];			 
			
			$sql_ 	= " SELECT societe from f_pli where id_pli = ".$id_pli."";
			$res_	= @pg_query( $conn,$sql_);
			$i 	    = pg_num_rows($res_);
				
				if ($i > 0){					
					while ($ores_mvt = pg_fetch_assoc($res_)) {				
						$id_soc     = $ores_mvt['societe'];
						echo "Mouvement : ".$cpt_data." ".$id_pli." ".$id_soc."</br>";
						$cpt_data  	= tcheck_data_mouvement($id_soc,$id_pli,$conn);
						if (/*$cpt_data == 0 &&*/ $id_soc > 0 && $id_pli > 0) update_data_mouvement($id_soc,$id_pli,$conn);	
											
					}
				}
			
			}
		}
		
		
		
		$sql_trace  = "SELECT  id_pli,societe FROM trace WHERE (societe is null or societe = 0) order by id_pli asc"	;
		$res_trace  = pg_query($sql_trace) OR die(" Erreur sql au niveau de la requete de selection de la table trace ");	
		
		$iNbr_trace = pg_num_rows($res_trace);
		
		if ( $iNbr_trace > 0){	
		
			while ($ores_trace = pg_fetch_assoc($res_trace)) {
				
			$id_pli     = $ores_trace['id_pli'];			 
			 
			$sql_ 	= " SELECT societe from f_pli where id_pli = ".$id_pli."";
			$res_	= @pg_query( $conn,$sql_);
			$i 	    = pg_num_rows($res_);
				
				if ($i > 0){					
					while ($ores_ttrace = pg_fetch_assoc($res_)) {				
						$id_soc     = $ores_ttrace['societe'];
						echo "Trace : ".$cpt_data." ".$id_pli." ".$id_soc."</br>"; 
						$cpt_data  	= tcheck_data_trace($id_soc,$id_pli,$conn);
						if (/*$cpt_data == 0 &&*/ $id_soc > 0 && $id_pli > 0) update_data_trace($id_soc,$id_pli,$conn);						
					}
				}
			}
		}
		
		$sql_histo  = "SELECT  id_pli,societe FROM histo_pli WHERE (societe is null or societe = 0 ) order by id_pli asc"	;
		$res_histo  = pg_query($sql_histo) OR die(" Erreur sql au niveau de la requete de selection de la table histo_pli ");	
		
		$iNbr_histo = pg_num_rows($res_histo);
		
		if ( $iNbr_histo > 0){	
		
			while ($ores_histo = pg_fetch_assoc($res_histo)) {
				
			$id_pli     = $ores_histo['id_pli'];			 
			
			$sql_ 	= " SELECT societe from f_pli where id_pli = ".$id_pli."";
			$res_	= @pg_query( $conn,$sql_);
			$i 	    = pg_num_rows($res_);
				
				if ($i > 0){					
					while ($ores_hhisto = pg_fetch_assoc($res_)) {				
						$id_soc     = $ores_hhisto['societe'];
						echo "Histo_pli : ".$cpt_data." ".$id_pli." ".$id_soc."</br>"; 
						$cpt_data  	= tcheck_data_histo($id_soc,$id_pli,$conn);
						if (/*$cpt_data == 0 && */ $id_soc > 0 && $id_pli > 0) update_data_histo($id_soc,$id_pli,$conn);						
					}
				}
			}
		}
		
		
		function getConnBayard() {
			
				$conn = pg_connect("host=srv-bdd-ged-bayard.madcom.local port=5432 dbname=ged_bayard user=si password=51P@vGD24$")
				or die("Impossible de se connecter à ged_bayard ");				
				return $conn;
		}		
		
		function tcheck_data_mouvement($id_soc,$id_pli,$conn){
			
			$sql = "SELECT id_pli FROM mouvement 
					WHERE id_pli = '".$id_pli."' 
					AND societe  = '".$id_soc."' ";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			
			return $i;			
		}
		
		function update_data_mouvement($id_soc,$id_pli,$conn){
			$i = 0;
			$sql = "UPDATE mouvement SET  societe = ".$id_soc."
					WHERE id_pli = '".$id_pli."' and (societe = 0 or societe is null) ";
			$res = @pg_query( $conn,$sql);	
			
			
		}
		
		function tcheck_data_histo($id_soc,$id_pli,$conn){
			
			$sql = "SELECT id_pli FROM histo_pli 
					WHERE id_pli = '".$id_pli."' 
					AND societe  = '".$id_soc."' ";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			
			return $i;			
		}
		
		function update_data_histo($id_soc,$id_pli,$conn){
			$i = 0;
			$sql = "UPDATE histo_pli SET  societe = ".$id_soc."
					WHERE id_pli = '".$id_pli."' and (societe = 0 or societe is null) ";
			$res = @pg_query( $conn,$sql);						
		}
		
		function tcheck_data_trace($id_soc,$id_pli,$conn){
			
			$sql = "SELECT id_pli FROM trace 
					WHERE id_pli = '".$id_pli."' 
					AND societe  = '".$id_soc."' ";
			$res = @pg_query( $conn,$sql);		
			$i   = pg_num_rows($res);	
			
			return $i;			
		}
		
		function update_data_trace($id_soc,$id_pli,$conn){
			$i = 0;
			$sql = "UPDATE trace SET  societe = ".$id_soc."
					WHERE id_pli = '".$id_pli."' and (societe = 0 or societe is null) ";
			$res = @pg_query( $conn,$sql);						
		}
		
	// ################################################
	$module_nom  = "CRON UPD - GED BAYARD" ;
	$nom_fichier = "10.90\GED\bayard\crons\maj_table.php" ;
	$date_jour   = date("Y-m-d");
	$couple 	 = date("YmdHis");
	$statut      = "termine" ;


	$sqlInsertCron = "INSERT INTO etat_crons (module_nom,fichier_nom,date_creation,statut,couple) 
	VALUES ('$module_nom','$nom_fichier','$date_jour','$statut','$couple') " ;
	$resInsertCron = @pg_query($conn,$sqlInsertCron) or die (pg_last_error($conn)) ;
	echo "***************".$statut."*************</br>";
	// ################################################  
?>