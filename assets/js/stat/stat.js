var dateDebut;
var dateFin;
var societe;
var tableListePlis;
var myPost;
var table;
var postData;
var chargementSpinner = '<div class="preloader pl-size-xs"><div class="spinner-layer pl-red-grey"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>';
$(".stat-ged").html("-");
init_nsprogress();
//$(".stat-ged").html("-");
/*
getStatCourrier();
getStatCourrier("ce");
getStatMailSftp();
getStatMailSftp("sftp");*/
function init_nsprogress(){
    $( document ).ajaxStart(function() {
        $(".preloader-reception").show();
        
        NProgress.start();
    });
    $(document).ajaxStop(function() {
        NProgress.done();
        $(".preloader-reception").hide();
    });
}

function getStatCourrier(lieu = "mada" )
{
    
    $.ajax({
        url : s_url+"statistics/Stat/GetStatCourrier/"+lieu,
        type : 'POST',
        dataType : 'JSON',
        data : postData,
        success : function(ret, statut){
            $.each(ret, function(k, v) {
                $("#"+k).text(v);
            });
        },
        error : function(resultat, statut, erreur){
            alert_("Errueur dans la base");
        }
    });
}
function getStatMailSftp(source="mail")
{
     
    $.ajax({
        url : s_url+"statistics/Stat/GetSftpMail/"+source,
        type : 'POST',
        dataType : 'JSON',
        data : postData,
        success : function(ret, statut){
            $.each(ret, function(k, v) {
                $("#"+k).text(v);
            });
        },
        error : function(resultat, statut, erreur){
            alert_("Errueur dans la base");
        }
    });
}


$('.recpt-date').datepicker({
    todayBtn: "linked",
    language: "fr",
    autoclose: true,
    todayHighlight: true,
    toggleActive: true
    
});


function charger_plis() 
{
 

    var dateDebut;
    var dateFin;
    var societe;
    var typeFlux;
    var locationGed;

    var go      = 1 ;
    
    societe     = $("#filtre-societe").val();

    typeFlux    = $("#filtre-source").val();
    locationGed = $("#filtre-location-ged").val();

    dateDebut   = $("#date-debut").val() == null ? 0 : $("#date-debut").val();
    dateFin     = $("#date-fin").val() == null ? 0 : $("#date-fin").val();
    
    if(dateDebut == 0 || dateFin == 0)
    {
        $("#date-debut").focus();
        alert_("Erreur sur la date !");
        go = 0; 
        return 0;
    }
    
    if(getDate(dateDebut) > getDate(dateFin))
    {
        alert_("Merci de rectifier votre filtre date !");
        $("#date-debut").focus();
        go = 0; 
        return 0;
    }

    if(!societe || societe == null || societe == "null" || societe == undefined || societe == "undefined")
    {
        alert_("Merci de renseigner la société !");
        go = 0; 
        return 0;
    }

    if(!typeFlux || typeFlux == null || typeFlux == "null" || typeFlux == undefined || typeFlux == "undefined")
    {
        alert_("Merci de renseigner la source (courrier et/ou sftp et/ou mail) !");
        go = 0; 
        return 0;
    }

    if(!locationGed || locationGed == null || locationGed == "null" || locationGed == undefined || locationGed == "undefined")
    {
        alert_("Merci de préciser si GED CE et/ou GED Mada !");
        go = 0; 
        return 0;
    }
    
    postData = {dateDebut:dateDebut,dateFin:dateFin,societe:societe};
    
    $(".stat-ged").html("0");
    $.ajax({
        url : s_url+"statistics/Stat/setSession",
        type : 'POST',
        data:postData,
        dataType : 'html',
        success : function(code_html, statut)
        {
            if(in_array(1,typeFlux))
            {
                if(in_array(1,locationGed)) /*mada*/
                {
                    getStatCourrier();
                }
                
                if(in_array(2,locationGed)) /*ce*/
                {
                    getStatCourrier("ce");
                }
            }
            if(in_array(1,locationGed)) 
            {
                if(in_array(2,typeFlux))
                {
                    getStatMailSftp();
                }

                if(in_array(3,typeFlux))
                {
                    getStatMailSftp("sftp");
                }
            }
           
        },
        error : function(resultat, statut, erreur){
            alert_("Errueur dans la base");
        }
        });
}

function in_array(what, where) {
    var a=false;
    for (var i=0; i<where.length; i++) {
        if(what == where[i]) {
            a=true;
            break;
        }
    }
    return a;
}
/*
function init_nsprogress(){
    $( document ).ajaxStart(function() {
        $(".preloader-reception").show();
        NProgress.start();
    });
    $(document).ajaxStop(function() {
        NProgress.done();
        $(".preloader-reception").hide();
    });
}



function exportID()
{
    window.location =  s_url+'statistics/Stat/exportID';
}

function afficherDonnees()
{
    $("#table-stat-ged-ce-mada").html("");
    $.ajax({
        url : s_url+"statistics/Stat/rechecher",
        type : 'POST',
        dataType : 'html',
        success : function(code_html, statut){
            $("#table-stat-ged-ce-mada").html(code_html);
        },
        error : function(resultat, statut, erreur){
            alert_("Errueur dans la base");
        }
    });
}




function differenceDate(a,b)
{
    return a-b;
}*/


/*****************************
 * TODO V2
 * 
 */

function alert_(msg)
{
    $.alert({
        type:"red",
        theme:'material',
        icon: 'glyphicon glyphicon-info-sign',
        title: 'Information',
        content: msg,
        buttons:
        {
            fermer: {
                text: 'Fermer',
                btnClass:"btn bg-light-blue waves-effect"
            }
        }
    });
}
if ( $.fn.DataTable.isDataTable('#table-ged-stat')) {
    $('#table-ged-stat').DataTable().destroy();
  }

  table = $('#table-ged-stat').DataTable();