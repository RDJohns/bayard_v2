var table;
var etat;

var recupererCoche              = 0;
var tempIntSourceTraitement     = 0 ; 
var koAvantEncours              = 0 ; 
 var statutSaisieSelected       = 0 ;  
var idMotif                     = 0 ;  
var objPliAverifier             = [];

/* variable pour recupérer cnotenu colonne */
var colIdPli                    = 0;
var colEtape                    = 6;
var colEtat                     = 7; /* colEtat == colOldStae*/
var colOldStae                  = 7;
var colSource                   = 4;
var isMailSftp                  = 0;
var  commentUrl                 = "/visual/visual/comments_pli";
initDate();

var newStatut = "0" ; /**ko en attente */
var sIdPli    = [0];
var sEtape = "0";
init_nsprogress(); 
var isBoutonReset = "non";
var utilisateurType = $("#type-utilisateur").val();
var chargementSpinner = '<center><div class="preloader"><div class="spinner-layer pl-black"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>';
var strStatut = {
    "a_traiter":"A traiter",
    "a_traiter_coche":"A traiter",
    "ci_editee":"CI EDITEE",
    "ci_editee_coche":"CI EDITEE",
    "ci_envoyee":"CI ENVOYEE",
    "ci_envoyee_coche":"CI ENVOYEE",
    "cloture":"Clôture sans traitement",
    "cloture_src":"Clôturé",
    "cloture_src_coche":"Clôturé",
    "cloture_coche":"Clôture sans traitement",
    "kd":"KO DEFINITIF",
    "kd_coche":"KO DEFINITIF",
    "ko_ci":"KO CIRCULAIRE",
    "ko_ci_coche":"KO CIRCULAIRE",
    "ko_en_attente":"KO EN ATTENTE",
    "ko_en_attente_coche":"KO EN ATTENTE",
    "ko_en_cours_by":"KO EN COURS BAYARD"
};



$(document).ready(function(){

  init_nsprogress(); 
  listeTypologieBySource();

  /* $('.elem_filtre').keypress(function(e){
    if(e.keyCode == 13){
      afficherPliKO();
    }}); */

    $('#traitement-ko tbody').on( 'contextmenu click', 'tr', function () {
      table.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
  });

  
/*
  var heightScreen = ($(window).height()) - 200;
  $(".contenu-liste-anomalie").css("height", heightScreen);*/

      $('#all').change(function() {
        if(this.checked) 
        {
          $('input[type="checkbox"]').prop('checked', 'checked');
        }
        else
        {
          $('input[type="checkbox"]').prop('checked', '');
        }
        
    });
      $('.data-title').attr('data-menutitle', "Some JS Titlec");
      $.contextMenu({
        selector: "tr ",
        className: 'css-title',  
        build: function($trigger) {
          var options = {
            callback: function(key, options) {
              var idPli = $trigger.context.childNodes[colIdPli].innerText;
              

              var aListePli = [];
              
              var etape = $trigger.context.childNodes[colEtape].innerText;
              var oldState = $trigger.context.childNodes[colOldStae].innerText;
                if(key == "visualiser")
                {
                  $("#id_pli_docs").text(idPli);
                  
                  $('#link_download_docs_zip').attr('href', s_url+"visual/visual/download_all_docs/" + idPli);
                  detail_pli(idPli);
                  /*$('#modal-ttmt-ko-img-content').modal('show');*/
                }
                else if(key == "courrier_comment")
                {
                  comment_pli(idPli);
                } 
                else if(key =="comment_mail_sftp")
                {
                  comment_pli(idPli,1); /*telecharger_ci*/
                }
                else if(key == "telecharger")
                {
                  
                  var valFileDownloadPath = s_url+"recherche/avancee/download/"+idPli;
                  window.open(valFileDownloadPath , '_blank');
                }
                else if(key == "telecharger_ci")
                {
                  
                  
                  var valFileDownloadPath = s_url+"control/Ajax/download_circulaire/"+idPli;
                  window.open(valFileDownloadPath , '_blank');
                }
                else
                {
                  
                  var bufferStrStatut     =  strStatut[key];
                  var msgModal            = "";
                  var multi               = 0;
                  var sourceTraitement    = $trigger.context.childNodes[colSource].innerText; /*courrier, mail et sftp */
                      sourceTraitement    = sourceTraitement.toLowerCase().trim();
                  var typeSourceGed       = "";
                  var intSourceTraitement = 0 ;
                  if(sourceTraitement == "courrier")
                  {
                      typeSourceGed       = " pli(s) ";
                      intSourceTraitement = 2 ;
                  }
                  else if(sourceTraitement == "mail")
                  {
                      typeSourceGed            = " flux ";
                      intSourceTraitement      = 1 ;
                  }
                  else if( sourceTraitement == "sftp")
                  {
                      typeSourceGed            = " flux ";
                      intSourceTraitement      = 3 ;
                  }

                  var etapeSelected            = $("#"+idPli).attr('attrib-etape'); 
                      statutSaisieSelected     = $("#"+idPli).attr('attrib-statut-saisie'); 
                  
                  var pliAverifier = [0];
                 /*Multiple : if(key.includes("coche"))
                  {
                    msgModal     = "Voulez-vous mettre le(s) "+typeSourceGed+" cochés ("+oldState+") en \""+bufferStrStatut+"\" ?";
                    multi        = 1;
                    

                   
                    
                    aListePli = [];
                    
                    $(".c-id-pli").each(function () {
                      if($(this).is(':checked'))
                      {
                        var tempStatutSaisie = $(this).attr("attrib-statut-saisie");
                        var tempEtape = $(this).attr("attrib-etape"); 
                        var tempIdpli = $(this).attr("id");
                        
                        
                        if(etapeSelected == tempEtape && tempStatutSaisie == statutSaisieSelected)
                        {
                            aListePli.push(tempIdpli); 
                        }
                        else
                        {
                          $($(this)).prop('checked', '');
                          $("#all").prop('checked', '');
                          
                        }
                        
                      }
                    }); 

                    pliAverifier = aListePli;
                  } 
                  else
                  {
                     msgModal     = "Voulez-vous mettre le(s) "+typeSourceGed+" #"+idPli+" ("+oldState+") en \""+bufferStrStatut+"\" ?" ;
                     pliAverifier = [idPli];
                  }*/

                  msgModal     = "Voulez-vous mettre le(s) "+sourceTraitement+" #"+idPli+" ("+oldState+") en \""+bufferStrStatut+"\" ?" ;
                  pliAverifier = [idPli];
                  
                 objPliAverifier = {"pliAverifier" :pliAverifier, "intSourceTraitement" :intSourceTraitement,"etapeSelected" : etapeSelected, "intStatutSaisieSelected" :statutSaisieSelected};  
                
               
                var msg = "";
                
                $.ajax(
                  {
                    url :s_url +"traitement/KO/verificationAvantValidation",
                    type :'POST',
                    data:objPliAverifier,
                    dataType :'JSON',
                    async: true,
                    success :function(r,statut) 
                    {
                        if(parseInt(r.retour) > 0) 
                        {
                          
                          listeDiff = r.id_diff;
                          idNoIn    = r.liste_diff;
                          msg       = "<i><u><b>Information</u></b>, d'autre utilisateur a déjà changé le statut de l'ID : \""+r.id_diff+"\" avant vous.</i><br/><br/>";

                          var listeDiff = r.liste_diff.split(",");
                        
                          $.each(listeDiff,function(i){$("#"+listeDiff[i]).prop('checked', '');});

                             aListePli = [0];
                             aListePli = [];
                    
                            $(".c-id-pli").each(function () {
                              if($(this).is(':checked'))
                              {
                                    var tempStatutSaisie = $(this).attr("attrib-statut-saisie");
                                    var tempEtape = $(this).attr("attrib-etape"); 
                                    var tempIdpli = $(this).attr("id");
                              
                                    if(etapeSelected == tempEtape && tempStatutSaisie == statutSaisieSelected)
                                    {
                                      aListePli.push(tempIdpli); 
                                    }
                              }
                            });

                        }
                        
                      
                        console.log(r);
                        if( parseInt(r.retour) <= 0 ) /*if(parseInt(r.i_total) != parseInt(r.retour))*/
                        {
                            if(key != "ci_envoyee" && key != "ci_envoyee_coche")
                            {
                                swal({
                                  html:true,
                                  title: "Confirmation",
                                  text: msg+msgModal,
                                  type: "warning",
                                  showCancelButton: true,
                                  confirmButtonColor: "#ff0000",
                                  confirmButtonText: "OUI",
                                  cancelButtonText: "Fermer",
                                  showLoaderOnConfirm: true,
                                  closeOnConfirm: true
                                }, function () {
            
                                  setEtatPli(idPli,etape,key,multi,aListePli,oldState);
                                });
                            }
                            else
                            {
                                swal({
                                    html:true,  
                                    title: "Confirmation",
                                    text: msg+msgModal +"<br/><br/>Date envoi CI (obligatoire) : ",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#ff0000",
                                    confirmButtonText: "OUI",
                                    cancelButtonText: "Fermer",
                                    closeOnConfirm: false,
                                    type: "input",
                                    inputIdName:"daty-input",
                                    inputClassName:"datypicker"
                                  
                                  
                                }, function (inputValue) {
                                  
                                  if (inputValue === false) return false;
                                  if (inputValue === "") {
                                    swal.showInputError("Merci de choisir une date");
                                    return false
                                  }
                                  setEtatPli(idPli,etape,key,multi,aListePli,"none");
                                });
                            } /*FIN : (key != "ci_envoyee" && key != "ci_envoyee_coche") */
                        }
                        else
                        {
                          swal("Information", msg, "warning");
                          swal({
                              html:true,
                              title: "Confirmation",
                              text: msg,
                              type: "warning",
                          });
                         
                        }
                       

                          
                       
                    },
                    error:function(retour,statut,erreur){
                      return "";
                    }
                  });

                
                }
                
            },
            items: {}
          };
          /*console.log($trigger.context);*/
          var idPli = $trigger.context.childNodes[colIdPli].innerText;
          var etape = $trigger.context.childNodes[colEtape].innerText; /**typage ou saisie */
          var etat  = $trigger.context.childNodes[colEtat].innerText; 
          
          var strEtape = etape.toLowerCase().trim();
          var strEtat  = etat.toLowerCase().trim();
          var sourceTraitement = $trigger.context.childNodes[colSource].innerText; /*courrier, mail et sftp */
              sourceTraitement = sourceTraitement.toLowerCase().trim();
          
          isCocher(); /** Detecte si une ligne de table est cochee */
         /*asiana test oe sao de multi statut léiz*/
         
         
         koAvantEncours              = $("#"+idPli).attr('attrib-ko-avant');   /** etat du id_ged avant "KO EN COURS BAYARD" */
         
         if(sourceTraitement == "courrier")
         {
           statutSaisieSelected = $("#"+idPli).attr('attrib-statut-saisie');
          if(utilisateurType == "client")
          {
            var arrayEntreeBayard = [3,4,5,6];
            var arrayAtraiter = [3,4,6];
            
            if(strEtat == "ko en cours bayard")
            {
              if(arrayEntreeBayard.indexOf(parseInt(koAvantEncours)) >= 0)
              {
                if(arrayAtraiter.indexOf(parseInt(koAvantEncours)) >= 0)
                {
                    options.items.a_traiter  = {name: "\"A traiter\" le pli #"+idPli+" ", icon: "fa-chevron-left"};
                    options.items.cloture    = {name: "CLORE  sans traitement le pli #"+idPli+"", icon: "fa-check-square"};
                    options.items.cloture_src  = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                }
                switch(parseInt(koAvantEncours))
                {
                    case 3:
                      options.items.ko_ci       = {name: "Mettre l' ID #"+idPli+" en \"KO CIRCULAIRE\" le pli", icon: "fa-th-list"};
                      break;

                    case 4:
                      options.items.ko_en_attente   = {name: "Mettre en \"KO EN ATTENTE\" le pli #"+idPli+"", icon: "fa-pause"};
                      break;

                    /*case 5:
                      options.items.cloture_src       = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                      break;*/
                }
              }
              
            }
            else if(parseInt(koAvantEncours) == 0 && arrayEntreeBayard.indexOf(parseInt(statutSaisieSelected)) >= 0)
            {
              options.items.ko_en_cours_by       = {name: "Mettre en \"KO EN COURS BAYARD\"	 le pli #"+idPli+"", icon: "fa-pause-circle"};
              options.items.sep0 = "---------"; 
            }   
          }
            
          
              switch(strEtape)
                {
                  
                  case "typage": /**typage */
                      switch(strEtat)
                      {
                        case "ko scan": /**ko scan*/

                          if(utilisateurType == "cdn")
                          {
                            
                            if(recupererCoche > 1 )
                            {
                              /* options.items.sep0 = "---------"; */ 
                              options.items.a_traiter_coche  = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés \"A traiter\" ", icon: "fa-chevron-left"};
                              options.items.kd_coche         = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"KO DEFINITIF\"", icon: "fa-ban"};
                              options.items.ko_ci_coche      = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"KO CIRCULAIRE\"", icon: "fa-th-list"};
                              options.items.cloture_coche    = {name: "CLORE  sans traitement les plis \""+strEtat.toUpperCase()+"\" cochés", icon: "fa-check-square"};
                            }
                            else
                            {
                              options.items.a_traiter   = {name: "\"A traiter\" le pli #"+idPli+"", icon: "fa-chevron-left"};
                              options.items.kd         = {name: "Mettre l' ID #"+idPli+" en \"KO DEFINITIF\"", icon: "fa-ban"};
                              options.items.ko_ci      = {name: "Mettre l' ID #"+idPli+" en \"KO CIRCULAIRE\"", icon: "fa-th-list"};
                              options.items.cloture_src  = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                              options.items.cloture    = {name: "CLORE  sans traitement le pli #"+idPli+"", icon: "fa-check-square"};
                            }
                          }
                        
                        break;
                        
                        case "ko definitif":/**KO DEFINITIF */
                          if(utilisateurType == "client" || utilisateurType == "cdt")
                          {
                            

                            if(recupererCoche > 1 )
                            {
                              
                              options.items.a_traiter_coche  = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés \"A traiter\" ", icon: "fa-chevron-left"};
                              options.items.ko_ci_coche      = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"KO CIRCULAIRE\"", icon: "fa-th-list"};
                              options.items.cloture_coche    = {name: "CLORE  sans traitement les plis \""+strEtat.toUpperCase()+"\" cochés", icon: "fa-check-square"};
                            }
                            else
                            {
                              options.items.a_traiter   = {name: "\"A traiter\" le pli #"+idPli+"", icon: "fa-chevron-left"};
                              options.items.ko_ci       = {name: "Mettre l' ID #"+idPli+" en \"KO CIRCULAIRE\" le pli", icon: "fa-th-list"};
                              options.items.cloture_src  = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                              options.items.cloture     = {name: "CLORE  sans traitement le pli #"+idPli+"", icon: "fa-check-square"};
                            }
                          }
                          break;
                        
                        case "ko en attente" : /**KO EN ATTENTE */
                          if(utilisateurType == "client" || utilisateurType == "cdt")
                          {
                          

                            if(recupererCoche > 1 )
                            {
                              
                              options.items.a_traiter_coche  = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés \"A traiter\" ", icon: "fa-chevron-left"};
                              options.items.cloture_coche    = {name: "CLORE  sans traitement les plis \""+strEtat.toUpperCase()+"\" cochés", icon: "fa-check-square"};
                              
                            }
                            else
                            {
                              options.items.a_traiter  = {name: "\"A traiter\" le pli #"+idPli+" ", icon: "fa-chevron-left"};
                              options.items.cloture_src  = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                              options.items.cloture    = {name: "CLORE  sans traitement le pli #"+idPli+"", icon: "fa-check-square"};
                            }
                          }
                          break;
                        
                        case "ko inconnu" : /** KO INCONNU*/
                          if(utilisateurType == "client" || utilisateurType == "cdt")
                          {
                            

                            if(recupererCoche > 1 )
                            {
                              /* options.items.sep0 = "---------"; */ 
                              options.items.a_traiter_coche     = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés \"A traiter\" ", icon: "fa-chevron-left"};
                              options.items.ko_en_attente_coche = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"KO EN ATTENTE\"", icon: "fa-pause"};
                              options.items.cloture_coche       = {name: "CLORE  sans traitement les plis \""+strEtat.toUpperCase()+"\" cochés", icon: "fa-check-square"};
                            }
                            else
                            {
                              options.items.a_traiter     = {name: "\"A traiter\" le pli #"+idPli+"", icon: "fa-chevron-left"};
                            options.items.ko_en_attente   = {name: "Mettre en \"KO EN ATTENTE\" le pli #"+idPli+"", icon: "fa-pause"};
                            options.items.cloture_src  = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                            options.items.cloture         = {name: "CLORE  sans traitement le pli #"+idPli+"", icon: "fa-check-square"};
                            }
                          }
                          break;
                          case "ko circulaire" : 
                          case "ok+circulaire" : 
                          if(utilisateurType == "cdn")
                          {
                            
        
                            if(recupererCoche > 1 )
                            {
                              /* options.items.sep0 = "---------"; */ 
                              options.items.ci_editee_coche   = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"CIRCULAIRE EDITEE\"", icon: "fa-edit"};
                              options.items.ci_envoyee_coche  = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"CIRCULAIRE ENVOYEE\" ", icon: "fa-send"};
                            }
                            else
                            {
                              options.items.ci_editee   = {name: "Mettre l' ID #"+idPli+" en \"CIRCULAIRE EDITEE\"", icon: "fa-edit"};
                              options.items.ci_envoyee  = {name: "Mettre l' ID #"+idPli+"  en \"CIRCULAIRE ENVOYEE\"", icon: "fa-send"};
                            }
                            
                            options.items.telecharger_ci    = {name: "Télécharger le fichier circulaire de l'ID  #"+idPli+"", icon: "fa-download"};
                          }
                          break;

                          case "ci editee" : 
                          if(utilisateurType == "cdn")
                          {
                            
                            
        
                            if(recupererCoche > 1 )
                            {
                              /* options.items.sep0 = "---------"; */ 
                              options.items.ci_envoyee_coche = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"CIRCULAIRE ENVOYEE\" ", icon: "fa-send"};
                            }
                            else
                            {
                              options.items.ci_envoyee       = {name: "Mettre l' ID #"+idPli+"  en \"CIRCULAIRE ENVOYEE\"", icon: "fa-send"};
                            }
                          }
                          break;
                      }
                    break;
                  
                  case "saisie": /*saisie */
                    switch(strEtat)
                      {
                        case "ko scan": /**ko scan*/
                        if(utilisateurType == "cdn")
                        {
                          

                          if(recupererCoche > 1 )
                          {
                            /* options.items.sep0 = "---------"; */ 
                            options.items.a_traiter_coche = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés \"A traiter\" ", icon: "fa-chevron-left"};
                            options.items.kd_coche        = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"KO DEFINITIF\"", icon: "fa-ban"};
                          }
                          else
                          {
                            options.items.a_traiter   = {name: "\"A traiter\" le pli #"+idPli+"", icon: "fa-chevron-left"};
                            options.items.kd          = {name: "Mettre en \"KO DEFINITIF\" le pli #"+idPli+"", icon: "fa-ban"};
                            options.items.cloture_src  = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                            options.items.cloture    = {name: "CLORE  sans traitement le pli #"+idPli+"", icon: "fa-check-square"};
                          }
                        }
                        break;

                       /* case "ko src":
                          if(utilisateurType == "client")
                          {
                            

                            if(recupererCoche > 1 )
                            {
                              
                              options.items.cloture_src_coche = {name: "CLORE les plis \""+strEtat.toUpperCase()+"\" cochés", icon: "fa-check"};
                              
                            }
                            else
                            {
                              options.items.cloture_src       = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                            }
                          }
                          break;  */

                        case "ko definitif":/**KO DEFINITIF */
                          if(utilisateurType == "cdt" || utilisateurType == "client")
                          {
                            

                            if(recupererCoche > 1 )
                            {
                              /* options.items.sep0 = "---------"; */ 
                              options.items.a_traiter_coche   = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés \"A traiter\" ", icon: "fa-chevron-left"};
                              options.items.ko_ci_coche       = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"KO CIRCULAIRE\"", icon: "fa-th-list"};
                              options.items.cloture_coche     = {name: "CLORE  sans traitement les plis \""+strEtat.toUpperCase()+"\" cochés", icon: "fa-check-square"};
                            }
                            else
                            {
                              options.items.a_traiter   = {name: "\"A traiter\" le pli #"+idPli+"", icon: "fa-chevron-left"};
                              options.items.ko_ci       = {name: "Mettre en \"KO CIRCULAIRE\" le pli #"+idPli+"", icon: "fa-th-list"};
                              options.items.cloture_src  = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                              options.items.cloture    = {name: "CLORE  sans traitement le pli #"+idPli+"", icon: "fa-check-square"};
                            }
                          }
                          
                          break;
                        
                        case "ko en attente" : /**KO EN ATTENTE */
                          if(utilisateurType == "cdt" || utilisateurType == "client")
                          {
                            

                            if(recupererCoche > 1 )
                            {
                              /* options.items.sep0 = "---------"; */ 
                              options.items.a_traiter_coche   = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés \"A traiter\" ", icon: "fa-chevron-left"};
                              options.items.cloture_coche     = {name: "CLORE  sans traitement les plis \""+strEtat.toUpperCase()+"\" cochés", icon: "fa-check-square"};
                            }
                            else
                            {
                              options.items.a_traiter   = {name: "\"A traiter\" le pli #"+idPli+"", icon: "fa-chevron-left"};
                              options.items.cloture_src  = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                              options.items.cloture    = {name: "CLORE  sans traitement le pli #"+idPli+"", icon: "fa-check-square"};
                            }
                          }
                          break;
                        
                        case "ko inconnu" : /** KO INCONNU*/
                          if(utilisateurType == "cdt" || utilisateurType == "client") 
                          {
                            

                            if(recupererCoche > 1 )
                            {
                              /* options.items.sep0 = "---------"; */ 
                              options.items.ko_en_attente_coche = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"KO EN ATTENTE\"", icon: "fa-pause"};
                              options.items.cloture_coche       = {name: "CLORE  sans traitement les plis \""+strEtat.toUpperCase()+"\" cochés", icon: "fa-check-square"};
                            }
                            else
                            {
                              options.items.a_traiter           = {name: "\"A traiter\" le pli #"+idPli+"", icon: "fa-chevron-left"};
                              options.items.ko_en_attente       = {name: "Mettre en \"KO EN ATTENTE\" le pli #"+idPli+"", icon: "fa-pause"};
                              options.items.cloture_src  = {name: "CLORE le pli #"+idPli+"", icon: "fa-check"};
                              options.items.cloture    = {name: "CLORE  sans traitement le pli #"+idPli+"", icon: "fa-check-square"};
                            }
                          }
                          break;
                        
                        case "ko circulaire" : 
                        case "ok+circulaire" : 
                        if(utilisateurType == "cdn")
                        {
                          

                          if(recupererCoche > 1 )
                          {
                            /* options.items.sep0 = "---------"; */ 
                            /*options.items.a_traiter_coche = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés \"A traiter\" ", icon: "fa-chevron-left"};*/
                            options.items.ci_editee_coche  = {name: "Mettre en \"CIRCULAIRE EDITEE\" le pli", icon: "fa-edit"};
                            options.items.ci_envoyee_coche = {name: "Mettre en \"CIRCULAIRE ENVOYEE\" le pli", icon: "fa-send"};
                          }
                          else
                          {
                           // options.items.a_traiter  = {name: "\"A traiter\" le pli #"+idPli+"", icon: "fa-chevron-left"};
                            options.items.ci_editee  = {name: "Mettre en \"CIRCULAIRE EDITEE\" le pli", icon: "fa-edit"};
                            options.items.ci_envoyee = {name: "Mettre en \"CIRCULAIRE ENVOYEE\" le pli", icon: "fa-send"};
                          }
                          options.items.telecharger_ci    = {name: "Télécharger le fichier circulaire de l'ID  #"+idPli+"", icon: "fa-download"};
                        }
                        break;

                        case "ci editee" : 
                          if(utilisateurType == "cdn")
                          {
                            
                          
                            if(recupererCoche > 1 )
                            {
                              /* options.items.sep0 = "---------"; */ 
                              options.items.ci_envoyee_coche = {name: "Mettre les plis \""+strEtat.toUpperCase()+"\" cochés en \"CIRCULAIRE ENVOYEE\" ", icon: "fa-send"};
                            }
                            else
                            {
                              options.items.ci_envoyee = {name: "Mettre l' ID #"+idPli+"  en \"CIRCULAIRE ENVOYEE\"", icon: "fa-send"};
        
                            }
                          }
                          break;
                      }
                    break;


                }

                /* TODO KO EN COURS BAYARD */
                
                options.items.sep2                = "---------";
                options.items.visualiser          = {name: "Visualiser les images du pli #"+idPli+"", icon: "fa-search-plus"};
                options.items.courrier_comment    = {name: "Voir / ajouter un commentaire sur le pli #"+idPli+"", icon: "fa-commenting"};
         }
         else if(sourceTraitement == "mail" || sourceTraitement == "sftp")
         {
            
          if(utilisateurType == "cdt" || utilisateurType == "client")
          {
            

            if(strEtat == "ko en cours bayard" && utilisateurType == "client")
            {
              options.items.a_traiter  = {name: "\"A traiter\" le flux  "+sourceTraitement+" #"+idPli+"", icon: "fa-chevron-left"};
              options.items.cloture_src  = {name: "CLORE  le flux #"+idPli+"", icon: "fa-check"};
              options.items.cloture    = {name: "CLORE  sans traitement le flux #"+idPli+"", icon: "fa-check-square"};
              
            }
           else if(utilisateurType == "client") 
            {
              options.items.ko_en_cours_by       = {name: "Mettre en \"KO EN COURS BAYARD\"	 le flux #"+idPli+"", icon: "fa-pause-circle"};
              options.items.sep0 = "---------"; 
            }   

            if(recupererCoche > 1 )
            {
              
              options.items.a_traiter_coche = {name: "Mettre les flux "+sourceTraitement+" \""+strEtat.toUpperCase()+"\" cochés \"A traiter\" ", icon: "fa-chevron-left"};
              options.items.cloture_flux_coche    = {name: "CLORE  sans traitement les flux  "+sourceTraitement+" \""+strEtat.toUpperCase()+"\" cochés", icon: "fa-check-square"};
            }
            else
            {
              options.items.a_traiter  = {name: "\"A traiter\" le flux  "+sourceTraitement+" #"+idPli+"", icon: "fa-chevron-left"};
              options.items.cloture_src  = {name: "CLORE  le flux #"+idPli+"", icon: "fa-check"};
              options.items.cloture    = {name: "CLORE  sans traitement le flux #"+idPli+"", icon: "fa-check-square"};
            }
            var etat  = $trigger.context.childNodes[colEtat].innerText; 
            var strEtat  = etat.toLowerCase().trim();
           
            
          }

            options.items.sep2 = "---------";
            options.items.comment_mail_sftp    = {name: "Voir / ajouter un commentaire sur le flux #"+idPli+"", icon: "fa-commenting"};
            options.items.telecharger    = {name: "Télécharger le flux "+sourceTraitement+" #"+idPli+"", icon: "fa-download"};
         }
           
           
          return options;
        }
      });   
      
  /* traitement-ko_wrapper*/     

});

function testa()
{
  
  var strItems = {
    "a_traiter": {name: "Pli \"A traiter\" ", icon: "fa-print",disabled: false},
    "ko_scan": {name: "Mettre en \"KO SCAN\"   le pli", icon: "fa-print",disabled: true},
    "sep1": "---------",
    "abadon": {name: "CLORE  sans traitement", icon: "fa-check-square"},
    "sep2": "---------",
    "visualiser": {name: "Visualiser le pli", icon: "fa-search-plus"}          
  };
  
   return strItems;
} 


function initDate()
{
      var start = moment();
      
      var end = moment();
      $('input[name="daterange"]').daterangepicker({
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          ranges: {
            'Aujourd\'hui': [moment(), moment()],
             'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             '7 jours précédents': [moment().subtract(6, 'days'), moment()],
             '30 derniers jours': [moment().subtract(29, 'days'), moment()],
             'Ce mois': [moment().startOf('month'), moment().endOf('month')],
             'Mois précedent': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          locale:{
            format: 'YYYY/MM/DD',
            daysOfWeek: ["Dim","Lun","Mar","Mer","Jeu","Ven","Sam"],
            monthNames: ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"],
            firstDay: 1},
      }, cb);
  
      cb(start, end);
}

function cb(start, end) {
    $('input[name="daterange"] span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}

function afficherPliKO()
{
  /*reset checkbox*/
  /*$('#all').prop('checked', '');
  $("#all").removeAttr('disabled');*/
 // $(".preloader-reception").show();
  if ( $.fn.DataTable.isDataTable('#traitement-ko')) {
    $('#traitement-ko').DataTable().destroy();
  }
  
  $('#traitement-ko tbody').empty();
/*document.documentElement.getBoundingClientRect().height */
var heightScreen = ($(window).height()) - 0;
var etatPli = $("#etat-pli").val();
var filtreEtape = $("#filtre-etape").val();
var societe = $("#filtre-societe").val(); 
var typologie = $("#filtre-typologie").val(); 
var dataKo = {
                "etatPli":etatPli,
                "source": $("#filtre-source").val(),
                "filtreEtape":filtreEtape,
                "daty":$("#date-courrier").val(),
                "societe":societe,
                "typologie":typologie
              };
/* $(".contenu-liste-anomalie").css("height", heightScreen); */
  var centrerCell = 10;
   if(utilisateurType == "cdn")
   {
    centrerCell = 12;
   }
   $.fn.dataTable.moment( 'DD/MM/YYYY' );
   
  table = $('#traitement-ko').DataTable({
    "bFilter": false,
    "language": {
      processing:     "Traitement en cours...",
      search:         "Rechercher&nbsp;:",
      lengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
      info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur <span class='row-total'> _TOTAL_ </span> &eacute;l&eacute;ments",
      infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
      infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
      infoPostFix:    "",
      loadingRecords: "Chargement en cours...",
      zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
      emptyTable:     "Aucune donnée disponible dans le tableau",
      paginate: {
          first:      "Premier",
          previous:   "Pr&eacute;c&eacute;dent",
          next:       "Suivant",
          last:       "Dernier"
      },
      aria: {
          sortAscending:  ": activer pour trier la colonne par ordre croissant",
          sortDescending: ": activer pour trier la colonne par ordre décroissant"
      }
  },
  
  
  "columnDefs": [
    {
        "targets": [centrerCell ], 
        "className": "text-center",
        "orderable": false
       
    }
], 
    "ajax":{
        "url" : s_url+"traitement/KO/listePliKo",
        "type": "POST",
        data:dataKo
    },
    "fnInitComplete":function()
		{
/*       $("#thead-table").click(); */
      
    },
    "scrollY"  : "60vh" ,
    "scrollCollapse": true,
    "paging": false,
    
    "drawCallback": function( settings ) {
      $.AdminBSB.dropdownMenu.activate();
      $.AdminBSB.select.activate();
      $.AdminBSB.input.activate();
      $('[data-toggle="tooltip"]').tooltip({container: 'body'}); 
      
     /* $('.dataTables_scrollBody').slimscroll({
        height: '60vh',
        alwaysVisible: true
      });*/
  }
  });
  
  paveTraitementKO();
}


function load_img_docs(id_pli) {

 
  $('#ttmt-ko-img-content').html("");
  $('#ttmt-ko-img-content').html(chargementSpinner+"<br/><center>Chargement image ...</center>");
  $.get(s_url + '/visual/visual/get_img_docs/' + id_pli, {}, function(reponse) {
      try {
          var data = $.parseJSON(reponse);
      } catch (error) {
          notifUSer(" Erreur serveur ");
          return '';
      }
      if (data.code == '1') 
      {
        console.log("XXXXX : "+data) ;
        /*$('#ttmt-ko-img-content').html("");
         $('#ttmt-ko-img-content').html(data.img); */
         $("#id_pli_docs").text(id_pli);
                  
         $('#link_download_docs_zip').attr('href', s_url+"visual/visual/download_all_docs/" + id_pli);
         jsPanel.create({
          theme: 'dark',
          headerTitle: 'Pli #'+id_pli,
          headerToolbar: '<span class="body ">Typologie : '+data.typo+' </span>',
          content: data.img,
          contentSize: {
            width:  window.innerWidth * 0.9,
            height: window.innerHeight * 0.8
        },
          callback: function() {
              this.content.style.padding = '10px';
          }
          });

         $('#typo_pli_docs').html(data.typo);
          $('[data-magnify=gallery]').magnify({
              footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
          });
         
      }else {
          notifUSer(" Erreur serveur ");
      }
  });

  
}

function reinitialiser()
{
  initDate();
  $("#filtre-societe").val([1,2]);
  $("#filtre-source").val(2);
  $("#filtre-etape").val(0);
  $("#filtre-societe").selectpicker('refresh');
  $("#filtre-source").selectpicker('refresh');
  $("#filtre-etape").selectpicker('refresh');
  listeTypologieBySource();
  setSource(1,"oui");
}


/*setEtatPli("+idPli+","+etape+",a_traiter)")*/
/**
 * 
 * @param
 * @param {*typage ou saisie} etape 
 * @param {* nouvel etat du pli} nouvlEtat 
 * setEtatPli($idPli,$etape,$nouvlEtat)
 */
function setEtatPli(idPli,etape,nouvlEtat,multi,aListePli,oldState="none")
{

  
  sEtape = etape;
  sIdPli = aListePli;
  var messageComplement = "";


  var source = $("#filtre-source").val();
  
	if(multi != 1 )
      {
        aListePli = [idPli];
        sIdPli = [idPli];
		
      }
    var prochainEtat = nouvlEtat.replace("_coche","");
    //
    if(oldState.toLowerCase().trim()  != "ko en attente" && prochainEtat.toLowerCase().trim() != "a_traiter" 
            ||  prochainEtat.toLowerCase().trim() == "cloture" ||  prochainEtat.toLowerCase().trim() == "cloture_src"
                  ||  prochainEtat.toLowerCase().trim() == "ko_en_cours_by" ) 
    {
     
      var myPost = [];
          myPost = {
                    "etape":etape,
                    "pli":idPli,
                    "multi":multi,
                    "nouvelEtat":nouvlEtat.replace("_coche",""),
                    "listePli":aListePli,
                    "currentEtatSaisie":statutSaisieSelected,
                    "source":source,
                    "dateEnvoieCi":($("#daty-input").val() == "undefined" || $("#daty-input").val() == undefined || $("#daty-input").val() == null)?"":$("#daty-input").val()
                  }
  
            $.ajax(
              {
                url :s_url +"traitement/KO/verificationAvantValidation",
                type :'POST',
                data:objPliAverifier,
                dataType :'JSON',
                async: true,
                success :function(r,statut) 
                {
                  if(parseInt(r.retour) > 0) 
                  {
                    myPost["liste_diff"]  = r.liste_diff;
                    myPost["not_in"]      = 1;

                    showNotification('alert-warning', "<i><u><b>Information</u></b>, d'autre utilisateur a déjà changé le statut de l'ID : \""+r.id_diff+"\" avant vous.</i><br/><br/>", 'top', 'right', null, null);
                  }
                  else
                  {
                    myPost["liste_diff"]  = 0;
                    myPost["not_in"]      = 0;
                  }

                  $.ajax(
                    {
                        url :s_url +"traitement/KO/setEtatPli",
                        type :'POST',
                        dataType :'json',
                        data:myPost,
                        success :function(retour,statut) {
                          afficherPliKO();
                            swal.close();
                        },
                        error:function(retour,statut,erreur){
                          afficherPliKO();
                            swal.close();
                            swal("Erreur inattendue", "Merci de réessayer plus tard", "error");
                    }}); 

                  
                },
                error:function(retour,statut,erreur){
                  return "";
                }
              });
    }
    else/* if(oldState.toLowerCase().trim() == "ko en attente")*/
    {
      
	
	  $("#confirmation-ttmt-ko-label").text("");
      newStatut = "0";
      /*sIdPli = idPli;*/
      var titleHeader = "";
      if(nouvlEtat == "a_traiter" || nouvlEtat == "a_traiter_coche")
      {
        titleHeader = "Mettre l' ID #"+aListePli.join(',')+" \"A traiter\"";
		
        if(multi == 1 )
        {
          titleHeader = "Mettre les plis cochés (KO EN ATTENTE) \"A traiter\"";
        
        }
	  
        newStatut = "a_traiter";
      }
      else if(nouvlEtat == "cloture" || nouvlEtat == "cloture_coche")
      {
        titleHeader = "Clôturer le pli #"+aListePli.join(',')+" sans traitement";
		
        if(multi == 1 )
        {
          titleHeader = "Clôturer les plis (KO EN ATTENTE) cochés  ";
        
        }
            newStatut = "cloture";
      }
	   
	  
      $("#confirmation-ttmt-ko-label").text(titleHeader);
      
      /*motif-operateur*/
      /*getMotifConsigne */
        idMotif = 0 ;  

       $("#consigne-pli").html("");
      $("#consigne-pli").text("");
      $("#consigne-pli").val("");
	   var myPli = {"listePli":aListePli,"source":source};
      $.ajax(
        {
          url : s_url+"traitement/KO/getMotifConsigne",
          type :"POST",
		      data:myPli,
          dataType:"json",
          success : function(r,e){
            $("#motif-operateur").html(r.motif);
            idMotif = parseInt(r.id_motif)
            $("#confirmation-ttmt-ko").modal("show");
            
            $("#consigne-pli").html("");
            
          }
        }
      )
    }
    
}
function setStatutPli() 
{
  
  var consigne = $("#consigne-pli").val();
  var traiter = 0;
  var source = $("#filtre-source").val();
  if(consigne == null || consigne == "null" || consigne == undefined || consigne == "undefined" || consigne.trim() == "")
  {
    notifUSer(" Merci de donner une consigne pour le pli ");
    traiter = 0;
  }
  else
  {
    traiter = 1;
  }

  var myPost = {
                  "listePli":sIdPli,
                  "nouvelEtat":newStatut,
                  "consigne":consigne,
                  "etape":sEtape,
                  "source":source,
                  "id_motif":idMotif,
                  "en_attente":1
                };
  if(traiter == 1 )
  {
      $.ajax(
        {
        url :s_url +"traitement/KO/verificationAvantValidation",
        type :'POST',
        data:objPliAverifier,
        dataType :'JSON',
        async: true,
        success :function(r,statut) 
        {
          if(parseInt(r.retour) > 0) 
          {
              if(parseInt(r.retour) > 0) 
              {
                  myPost["liste_diff"]  = r.liste_diff;
                  myPost["not_in"]      = 1;
                  showNotification('alert-warning', "<i><u><b>Information</u></b>, d'autre utilisateur a déjà changé le statut de l'ID : \""+r.id_diff+"\" avant vous.</i><br/><br/>", 'top', 'right', null, null);
              }
              else
              {
                  myPost["liste_diff"]  = 0;
                  myPost["not_in"]      = 0;
              }
          }

          $.ajax(
            {
              url : s_url+"traitement/KO/setEtatPli",
              type :"POST",
              dataType:"text",
              data:myPost,
              success : function(r,e){
                afficherPliKO();
                    $("#confirmation-ttmt-ko").modal("hide");
                }
            }
          );   
          
        },
        error:function(retour,statut,erreur){
          swal("Erreur inattendue", "Merci de réessayer plus tard", "error");
          return "";
        }
        });
  }
}
function isCocher()
{
  recupererCoche = 0;
  $(".c-id-pli").each(function () {
    if($(this).is(':checked'))
    {
      recupererCoche += 1;
      if(recupererCoche > 1)
        return false;
    }
  });

 
}

function uncheckLigne(aListePli)
{
  $('input[type="checkbox"]').prop('checked', '');
  console.log("**** : "+aListePli[i]);
  for(var i = 0 ; i < aListePli.length; i++)
  {
    //$("#"+aListePli[i]).prop('checked', 'checked');
    console.log("**** : "+aListePli[i]);
  }
}


function init_nsprogress(){
  $( document ).ajaxStart(function() {
      $(".preloader-reception").show();
      $(':button').prop('disabled', true);
      NProgress.start();
  });
  
  $(document).ajaxStop(function() {
      NProgress.done();
      $(".preloader-reception").hide();
      $(':button').prop('disabled', false);
  });
}

function reInit()
{
  $('select').each( function() {
    $(this).val( $(this).find("option[selected]").val() );
  });
  initDate();
}


function setFiltreStatut(defaut = 0)
{
  
  var statutPli  = $("#etat-pli").val();
  var filtreSource = $("#filtre-source").val();
      filtreSource  = parseInt(filtreSource);
  if(jQuery.inArray("all", statutPli) != -1)
  {
    statutPli = "all";
  }
  else if(jQuery.inArray("none", statutPli) != -1)
  {
    statutPli = "none";
  }

  if(defaut == 1)
  {
    statutPli = "none";
  }
  else
  {
    isBoutonReset = "oui";
  }
/*
  if(jQuery.inArray("none", statutPli) != -1)
  {
    statutPli = "none";
  }*/
   
  if(statutPli == "all" || statutPli == "none" )
  {
       $(".c-statut-pli").html("");
    $.ajax(
      {
        url :s_url +"traitement/KO/etatSaisieGedBySource/"+filtreSource+"/"+statutPli,
        type :'POST',
        data : {"isBoutonReset":isBoutonReset},
        dataType :'html',
        
        success :function(retour,statut) {
          $(".c-statut-pli").html(retour);
          $.AdminBSB.dropdownMenu.activate();
          $.AdminBSB.select.activate();
          isBoutonReset = "non";
        },
        error:function(retour,statut,erreur){
          
        }
      });
  }
   
      console.log("statut : "+statutPli);
  
    
}


function setFiltreStatutBySource()
{
  var filtreSource = $("#filtre-source").val();
  //alert(filtreSource);
}
function setSource(source,reset="non")
{
  isBoutonReset = reset;
  setFiltreStatut(source);
  var filtreSource = $("#filtre-source").val();
  listeTypologieBySource(filtreSource);
	if ( $.fn.DataTable.isDataTable('#traitement-ko')) {
    $('#traitement-ko').DataTable().destroy();
  }
  
  $('#traitement-ko tbody').empty();
  
}


function verificationAvantValidation(ob)
{
  $.ajax(
    {
      url :s_url +"traitement/KO/verificationAvantValidation",
      type :'POST',
      data:ob,
      dataType :'JSON',
      async: true,
      success :function(retour,statut) 
      {
         return retour;
      },
      error:function(retour,statut,erreur){
        return "";
      }
    });
}

function comment_pli(id_pli,isMail = 0) {
      
      var data = {id_pli:id_pli,isMail:isMail};
      
      commentUrl = "/visual/visual/comments_pli";
      if(isMail > 0)
      {
        commentUrl = "traitement/KO/comments_flux";
        isMailSftp = isMail;
      }
      $.post(s_url + commentUrl, data, function(reponse) {
      $('#mdl_comment_pli_id_pli').html(id_pli);
      $('#mdl_comment_pli_body').html(reponse);
      $('#new_comment_pli').val("");
      $('#mdl_comment_pli').modal('show');
    });
  }
  
  function add_comment_pli() {
   var data = {
    id_pli: Number.parseInt($.trim($('#mdl_comment_pli_id_pli').html()))
    ,comment:$.trim($('#new_comment_pli').val())
  };
    var commentStr = $.trim($('#new_comment_pli').val());
  
    if(commentStr == "" || commentStr == null || commentStr == "null" || commentStr == undefined || commentStr == "undefined")
    {
      showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Merci de renseigner le commentaire' , 'top', 'right', null, null);
      return false;
    }
    if(Number.isInteger(data.id_pli)) {

         
          $.post(s_url + commentUrl, data, function(reponse) {
          $('#new_comment_pli').val('');
          $('#new_comment_pli').trigger('autosize.resize');
          $('#mdl_comment_pli_body').html(reponse);
          afficherPliKO();
      });
    }
  }

function paveTraitementKO()
{
  var etatPli     = $("#etat-pli").val();
  var filtreEtape = $("#filtre-etape").val();
  var societe     = $("#filtre-societe").val(); 
  var typologie     = $("#filtre-typologie").val(); 
  var dataKo      = {
                  "etatPli":etatPli,
                  "source": $("#filtre-source").val(),
                  "filtreEtape":filtreEtape,
                  "daty":$("#date-courrier").val(),
                  "societe":societe,
                  "typologie":typologie
                };
  
    $.ajax({
      url :s_url +"traitement/KO/paveTraitementKO",
      type : 'POST',
      data:dataKo,
      dataType : 'json',
      beforeSend: function() {
          GedGoToPageLogin();
      },
      success : function(r, statut){
        console.log(r);
        if(r.source == "courrier")
        {
           $("#pli-ko-scan").text(coalesce(r.pli_ko_scan));
           $("#pli-ko-definitif").text(coalesce(r.pli_ko_definitif));
           $("#pli-ko-inconnu").text(coalesce(r.pli_ko_iconnu));
           $("#pli-ko-src").text(coalesce(r.pli_ko_src));
           $("#pli-ko-en-attente").text(coalesce(r.pli_ko_ko_en_attente));
           $("#pli-ko-circulaire").text(coalesce(r.pli_ko_circulaire));
           $("#pli-ko-en-cours-by").text(coalesce(r.pli_ko_en_cours_by));
           $("#pli-ok-circulaire").text(coalesce(r.pli_ok_ci));
           $("#pli-ci-editee").text(coalesce(r.pli_ci_editee));
           $("#pli-ci-envoyee").text(coalesce(r.pli_ko_ci_envoyee));
          
          $(".bloc-courrier").show();
           $(".bloc-ms").hide();
        }
        else
        {
          $(".bloc-courrier").hide();
          $(".bloc-ms").show();

          $("#ko-mail-anomalie").text(coalesce(r.ko_mail_anomalie));
          $("#ko-anomalie-pj").text(coalesce(r.ko_anomalie_pj));
          $("#ko-hors-perimetre").text(coalesce(r.hors_perim));
          $("#ko-anomalie-fichier").text(coalesce(r.ko_anomalie_fichier));
          $("#ko-rejeter").text(coalesce(r.rejeter));
          $("#ko-transfert-src-ms").text(coalesce(r.ko_transfert_src));
          $("#ko-definitif-ms").text(coalesce(r.ko_definitif));
          $("#ko-inconnu-ms").text(coalesce(r.ko_inconnu));
          $("#ko-en-attente-ms").text(coalesce(r.ko_en_attente));
          $("#ko-en-cours-bayard-ms").text(coalesce(r.ko_en_cours_bayard));
            
        }

      },
      error : function(r, statut, erreur){
         
      }
  });
}

function coalesce( inte)
{
  return (inte != null ? inte : "0");
}

function listeTypologieBySource(source = 2)
{
    //filtre-source
    /* c-typologie-pli */
    /* alert(source); */
    $(".c-typologie-pli").html("");
    $.ajax(
      {
        url : s_url+"traitement/KO/listeTypologieBySource/"+source,
        type :"POST",
        dataType:"html",
        success : function(r,e){
          $(".c-typologie-pli").html(r);
        
          $.AdminBSB.dropdownMenu.activate();
          $.AdminBSB.select.activate();
        }
      });
}

function detail_pli(id_pli) {

  $('#ttmt-ko-img-content').html("");
  $('#ttmt-ko-img-content').html(chargementSpinner+"<br/><center>Chargement image ...</center>");
	$.get(s_url + '/visual/visual/detail_pli/' + id_pli, {}, function (reponse) {
		try {
			var data = $.parseJSON(reponse);
		} catch (error) {
			showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
			return '';
		}
		if (data.code == '1') {
			jsPanel.create({
				headerTitle: data.titre,
				headerToolbar: data.sous_titre,
				footerToolbar: data.footer,
				content: data.body,
				contentSize: {
          width:  window.innerWidth * 0.9,
          height: window.innerHeight * 0.8
				},
				theme: 'light',
				onbeforemaximize: function (panel) {
					$('html,body').scrollTop(0);
					return true;
        },
          callback: function() {
          this.content.style.padding = '10px';
        }
			});
			$('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover();
      
		} else {
			showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
		}
	}).fail(function () {
		showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération échouée!.', 'top', 'right', null, null);
		// swal("Serveur", "Erreur serveur!", "error");
	});
}

function load_docs_offset(id_pli, offset, id_dom) {
	$.get(s_url + '/visual/visual/view_docs_offset/' + id_pli + '/' + offset + '/' + id_dom, {}, function (reponse) {
		try {
			var data = $.parseJSON(reponse);
		} catch (error) {
			showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
			return '';
		}
		if (data.code == '1') {
			console.log(id_dom);
			$('#' + id_dom).html(data.ihm);
			$('[data-toggle="tooltip"]').tooltip();
			$('[data-toggle="popover"]').popover();
		} else {
			showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
		}
	});
}

function load_img_doc_face(id_doc, face = 0) {
	$.get(s_url + '/visual/visual/get_img_doc/' + id_doc, {}, function (reponse) {
		try {
			var data = $.parseJSON(reponse);
		} catch (error) {
			showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
			return '';
		}
		if (data.code == '1') {
			$('#zone_img_temp').html(data.img);
			$('[data-magnify=gallery]').magnify({
				footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
			});
			if (face == 0) {
				$('#img_temp_recto').trigger('click');
			} else {
				$('#img_temp_verso').trigger('click');
			}
		} else {
			showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
		}
	});
}




