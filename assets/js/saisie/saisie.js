var first_modif_mvmnt = true;
var keys = {};
onkeydown = onkeyup = function(e) {
    e = e || event;
    e.which = e.which || e.keyCode;
    keys[e.which] = e.type === 'keydown';
    if (keys[17] && keys[13]) { //ctrl+Enter: go
        if ($('[onclick="get_lot_saisie_pli();"]').length == 1) {
            get_lot_saisie_pli();
        } else if ($('[onclick="get_pli();"]').length == 1) {
            get_pli();
        } else if ($('[onclick="enregistrer();"]').length == 1) {
            enregistrer();
            e.preventDefault();
            e.stopPropagation();
        }
    }
    if (keys[17] && keys[8]) { //ctrl+back: annuler traitement
        if ($('[onclick="annuler();"]').length == 1) {
            annuler();
        }
    }
    if (keys[17] && keys[16]) { //ctrl+Shft: afficher apercu document
        if ($('.img_b64').length > 0) {
            $($('.img_b64')[0]).trigger('click');
        }
    }
    if (keys[18] && keys[106]) { //alt+*: retourner document
        var is_magnify = false;
        if ($('.magnify-foot-toolbar .magnify-button-next').length > 0) {
            $('.magnify-foot-toolbar .magnify-button-next').trigger('click');
            is_magnify = true;
        }
        if ($('.tab_doc_ap').length > 0 && !is_magnify) {
            var cible = null;
            $(".tab_doc_ap").each(function() {
                if (!($(this).hasClass('active'))) {
                    cible = ($(this).find('a[data-toggle="tab"]'))[0];
                }
            });
            if (cible !== null) {
                $(cible).trigger('click');
            }
        }
    }
    if (keys[18] && keys[34]) { //alt+page_down: next document
        var is_magnify = $('.magnify-modal').length > 0;
        if ($('#sel_view_doc').length > 0) {
            var opts = $('option.opt_doc');
            var val_selected = $('#sel_view_doc').val();
            if ($(opts[(opts.length - 1)]).attr('value') == val_selected) {
                $('#sel_view_doc').selectpicker('val', $(opts[0]).attr('value'));
            } else {
                var i_opt = 0;
                $('option.opt_doc').each(function() {
                    if ($(this).attr('value') == val_selected) {
                        $('#sel_view_doc').selectpicker('val', $(opts[i_opt + 1]).attr('value'));
                    }
                    i_opt++;
                });
            }
            show_doc();
            if (is_magnify) {
                if ($('.img_b64').length > 0) {
                    $($('.img_b64')[0]).trigger('click');
                }
            }
        }
    }
    if (keys[18] && keys[33]) { //alt+page_up: previous document
        var is_magnify = $('.magnify-modal').length > 0;
        if ($('#sel_view_doc').length > 0) {
            var opts = $('option.opt_doc');
            var val_selected = $('#sel_view_doc').val();
            if ($(opts[0]).attr('value') == val_selected) {
                $('#sel_view_doc').selectpicker('val', $(opts[(opts.length - 1)]).attr('value'));
            } else {
                var i_opt = 0;
                $('option.opt_doc').each(function() {
                    if ($(this).attr('value') == val_selected) {
                        $('#sel_view_doc').selectpicker('val', $(opts[i_opt - 1]).attr('value'));
                    }
                    i_opt++;
                });
            }
            show_doc();
            if (is_magnify) {
                if ($('.img_b64').length > 0) {
                    $($('.img_b64')[0]).trigger('click');
                }
            }
        }
    }
    if (keys[27]) { //Echap: fermer apercu document
        if ($('.magnify-head-toolbar .magnify-button-close').length > 0) {
            $('.magnify-head-toolbar .magnify-button-close').trigger('click');
        }
        if ($('.modal.in a.close').length > 0) {
            $('.modal.in a.close').trigger('click');
        }
    }
    if (keys[18] && keys[107]) { //alt++: zoom apercu
        if ($('.magnify-foot-toolbar .magnify-button-zoom-in').length > 0) {
            $('.magnify-foot-toolbar .magnify-button-zoom-in').trigger('click');
        }
    }
    if (keys[18] && keys[109]) { //alt+-: de-zoom apercu
        if ($('.magnify-foot-toolbar .magnify-button-zoom-in').length > 0) {
            $('.magnify-foot-toolbar .magnify-button-zoom-out').trigger('click');
        }
    }
}

$(function() {
    $('.sel_field').selectpicker();
    $('#mdl_display_lot_saisie').on('shown.bs.modal', function() {
        load_list_lot();
    });
    $('#mdl_display_group_pli').on('shown.bs.modal', function() {
        load_list_grp_pli();
    });
    $('#mdl_display_group_pli').on('hidden.bs.modal', function() {
        $('#mdl_display_group_pli_body').html($('#html_loader').html());
    });
    $('#mdl_display_group_pli_body').html($('#html_loader').html());
    $('#mdl_display_group_pli_by_older').on('shown.bs.modal', function() {
        load_list_grp_pli_by_older();
    });
    $('#mdl_display_group_pli_by_older').on('hidden.bs.modal', function() {
        $('#mdl_display_group_pli_by_older_body').html($('#html_loader').html());
    });
    $('#mdl_display_group_pli_by_older_body').html($('#html_loader').html());
    first_modif_mvmnt = true;
    interface_mode();
    $('body').on('mouseenter', '.modal.fade.in .modal-dialog .modal-content', function(e){
        $('.modal.fade.in').css('z-index', '1091');
    });
    $('body').on('mouseleave', '.modal.fade.in .modal-dialog .modal-content', function(e){
        $('.modal.fade.in').css('z-index', '1050');
    });
    /* Menu */
    $('.visible_menu').show();
    $('#menu_lot_de_saisie').show();
    $('#menu_regroupement_pli').show();
    $('#menu_regroupement_pli_par_date').show();
});
function raccourci()
{
    $("#mdl_display_raccourci").modal("show");
}
function get_lot_saisie_pli() {
    if ($('#sel_id_societe').val() == '') {
        showNotification('alert-danger', '<i class="material-icons">error</i> Erreur: Le champ "soci&eacute;t&eacute;" est requis!.', 'bottom', 'right', null, null);
        return '';
    }
    if ($('#sel_id_typologie').val() == '') {
        showNotification('alert-danger', '<i class="material-icons">error</i> Erreur: Le champ "Typologie" est requis!.', 'bottom', 'right', null, null);
        return '';
    }
    if ($('#sel_id_gr_payement').val() == '') {
        showNotification('alert-danger', '<i class="material-icons">error</i> Erreur: Le champ "Type de paiement" est requis!.', 'bottom', 'right', null, null);
        return '';
    }
    var data_to_server = {
        id_societe: $('#sel_id_societe').val(),
        id_typologie: $('#sel_id_typologie').val(),
        id_gr_paiement: $('#sel_id_gr_payement').val(),
        rejet_batch: $('#rejet_batch_only').prop('checked') ? '1' : '0'
    };
    $('#div_loader').removeClass('cacher');
    in_load_pli();
    $.post(chemin_site + '/saisie/saisie/set_lot_saisie_pli', data_to_server, function(reponse) {
        $('#contenu').html(reponse);
        init_compo_field();
        $('#mdl_ko_scan').on('shown.bs.modal', function() {
            $('#sel_motif_ko_scan').selectpicker();
            $('#sel_motif_ko_scan').selectpicker('val', '1');
            $('#comment_ko_scan').val('');
        });
    });
}

function in_load_pli(){
    $('#contenu').html('<div id="div_loader" class="row m-t-25">'
                +'<div class="col-xs-12 align-center">'
                +'<div class="preloader pl-size-xl">'
                +'<div class="spinner-layer pl-light-blue">'
                +'<div class="circle-clipper left">'
                +'<div class="circle"></div>'
                +'</div>'
                +'<div class="circle-clipper right">'
                +'<div class="circle"></div>'
                +'</div>'
                +'</div>'
                +'</div>'
                +'</div>'
                +'</div>');
}

function get_pli() {
    var rejet_batch = $('#rejet_batch_only').prop('checked') ? '1' : '0';
    in_load_pli();
    $.post(chemin_site + '/saisie/saisie/get_pli', {rejet_batch: rejet_batch}, function(reponse) {
        $('#contenu').html(reponse);
        $('.sel_field').selectpicker();
    });
}

function init_compo_field() {
    $('.sel_field').selectpicker();
    autosize($('textarea.auto-growth'));
    $.AdminBSB.input.activate();
    interface_mode();
    if($('#sel_view_doc').length > 0){
        numerotation_mvmnt();
        numerotation_cheque();
        analyse_paiement();
        init_etat();
    }
}

function init_etat() {
    //$('#field_motif_ko').selectpicker('val', '');
    $('.if_ko').hide();
    //$('#field_autre_motif_ko').val('');
    $('.if_ko_autre').hide();
    $('#parti_field').slimscroll({
        height: '85vh',
        color: 'rgba(0,0,0,0.5)',
        size: '8px',
        alwaysVisible: true,
    });
    $('[data-trigger="spinner"]').spinner();
}

function manage_etat() {
    var etat = $('#field_statut_saisie').val();
    if ($.inArray(1*etat, [1]) == -1) {//etat != 1
        $('.if_ko').show();
        $('.if_ko_autre').show();
    } else {
        init_etat();
    }
}

function show_doc() {
    if($('#sel_view_doc').length > 0){
        $.get(chemin_site + '/saisie/saisie/view_doc/' + $('#sel_view_doc').val(), {}, function(reponse) {
            $('#view_doc').html(reponse);
            //$('[data-magnify=gallery]').magnify({
            $('.img_b64').magnify({
                multiInstances: false,
                modalWidth: '74.5%',
                modalHeight: '100%',
                fixedModalPos: true,
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next'],
                fixedModalSize: true
            });
        });
    }
}

function reset_filtre_pli() {
    $('#sel_id_societe').selectpicker('val', '');
    $('#sel_id_typologie').selectpicker('val', '');
    $('#sel_id_gr_payement').selectpicker('val', '');
}

function new_lot_saisie() {
    $('#div_loader').removeClass('cacher');
    var data = {
        no_pli: 0,
        have_lot_saisie: 1
    }
    in_load_pli();
    $.post(chemin_site + '/saisie/saisie/lot_saisie', data, function(reponse) {
        $('#contenu').html(reponse);
        $('.sel_field').selectpicker();
    });
}

function set_titre() {
    var id_soc = $('#field_soc').val();
    $.get(chemin_site + '/saisie/saisie/option_titres/' + id_soc, {}, function(reponse) {
        $('#field_main_titre').selectpicker('destroy');
        $('#field_main_titre').html(reponse);
        $('#field_main_titre').selectpicker();
        $.get(chemin_site + '/saisie/saisie/option_titres/' + id_soc + '/1', {}, function(reponse) {
            $('select.sel_field_mvmnt').selectpicker('destroy');
            $('select.sel_field_mvmnt').html(reponse);
            $('select.sel_field_mvmnt').selectpicker();
            numerotation_mvmnt();
        });
    });
}

function get_typologie() {
    var id_soc = $('#field_soc').val();
    $.get(chemin_site + '/saisie/saisie/option_typologie/' + id_soc, {}, function(reponse) {
        var field_typo = '#field_typologie';
        if ($(field_typo)) {
            $(field_typo).selectpicker('destroy');
            $(field_typo).html(reponse);
            $(field_typo).selectpicker();
        }
    });
}

function analyse_paiement() {
    appli_chq();
    appli_chq_kd();
    appli_espece();
}

function appli_chq() {
    var avec_grp_field_chq = ($('.grp_field_cheque')).length > 0;
    var modes = $('#field_mode_paiement').val() === null ? [] : $('#field_mode_paiement').val();
    var mode_avec_cheque = modes.indexOf('1') > -1;
    if (mode_avec_cheque && !avec_grp_field_chq) {
        get_view_cheque();
    }
    if (!mode_avec_cheque && avec_grp_field_chq) {
        swal({
            title: "Confirmation!",
            text: "La suppression du mode de paiement par cheque entrainera la suppression des données des cheques de ce pli.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(ok) {
            if (ok) {
                $('.grp_field_cheque').remove();
            } else {
                modes.push('1');
                $('#field_mode_paiement').selectpicker('val', modes);
            }
        });
    }
}

function appli_chq_kd() {
    var avec_grp_field_chq_kd = ($('.grp_field_cheque_kd')).length > 0;
    var modes = $('#field_mode_paiement').val() === null ? [] : $('#field_mode_paiement').val();
    var mode_avec_cheque_kd = modes.indexOf('6') > -1;
    if (mode_avec_cheque_kd && !avec_grp_field_chq_kd) {
        get_view_cheque_kd();
    }
    if (!mode_avec_cheque_kd && avec_grp_field_chq_kd) {
        swal({
            title: "Confirmation!",
            text: "La suppression du mode de paiement par cheque cadeau entrainera la suppression des données des cheques cadeaux de ce pli.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(ok) {
            if (ok) {
                $('.grp_field_cheque_kd').remove();
            } else {
                modes.push('6');
                $('#field_mode_paiement').selectpicker('val', modes);
            }
        });
    }
}

function appli_espece() {
    var avec_esp = $("#row_montant_espece").is(":visible");
    var modes = $('#field_mode_paiement').val() === null ? [] : $('#field_mode_paiement').val();
    var mode_avec_esp = modes.indexOf('5') > -1;
    if(mode_avec_esp && !avec_esp){
        $("#row_montant_espece").show();
    }else if (!mode_avec_esp && avec_esp) {
        swal({
            title: "Confirmation!",
            text: "La suppression du mode de paiement par esp\350ce entrainera la suppression du montant esp\350ce de ce pli.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(ok) {
            if (ok) {
                $("#field_montant_espece").val('0');
                $("#row_montant_espece").hide();
            } else {
                modes.push('5');
                $('#field_mode_paiement').selectpicker('val', modes);
            }
        });
    }
}

function get_view_cheque() {
    $.get(chemin_site + '/saisie/saisie/get_cheque_view/' + ID_PLI, {}, function(reponse) {
        $('#section_paiement').append(reponse);
        $('select.sel_field_cheque').selectpicker();
        $.AdminBSB.input.activate();
        numerotation_cheque();
    });
}

function get_view_cheque_kd() {
    $.get(chemin_site + '/saisie/saisie/get_cheque_kd_view/' + ID_PLI, {}, function(reponse) {
        $('#section_paiement').append(reponse);
        $('select.sel_field_cheque_kd').selectpicker();
        $.AdminBSB.input.activate();
        numerotation_cheque_kd();
    });
}

function suppr_gr_field_cheque(id_grp) {
    swal({
        title: "Confirmation!",
        text: "La suppression de ce cheque entrainera la suppression de ses données.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Supprimer",
        cancelButtonText: "Annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        $(id_grp).remove();
        numerotation_cheque();
    });
}

function suppr_gr_field_cheque_kd(id_grp) {
    swal({
        title: "Confirmation!",
        text: "La suppression de ce cheque cadeau entrainera la suppression de ses données.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Supprimer",
        cancelButtonText: "Annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        $(id_grp).remove();
        numerotation_cheque_kd();
    });
}

function numerotation_cheque() {
    var num = 0;
    $(".grp_field_cheque").each(function() {
        num++;
        $($(this).find('.numerotation_cheque')[0]).html(num);
        $($(this).find('.clear_cheque')[0]).show();
        if ($(".grp_field_cheque").length == 1) {
            $($(this).find('.clear_cheque')[0]).hide();
        }
    });
}

function numerotation_cheque_kd() {
    var num = 0;
    $(".grp_field_cheque_kd").each(function() {
        num++;
        $($(this).find('.numerotation_cheque_kd')[0]).html(num);
        $($(this).find('.clear_cheque_kd')[0]).show();
        if ($(".grp_field_cheque_kd").length == 1) {
            $($(this).find('.clear_cheque_kd')[0]).hide();
        }
    });
}

function controle_cheque() {
    var tab_cmc7 = [];
    var etat = $('#field_statut_saisie').val();
    var is_ci = $.inArray(1*etat, [7, 11]) > -1;
    var is_ko_must_valid_chq = $.inArray(1*etat, [6]) > -1;
    var reponse = true;
    $(".grp_field_cheque").each(function() {
        var ident = $(this).attr('my_ident');
        var cmc7 = $.trim($('#field_cmc7_' + ident).val());
        var etranger = $('#field_chq_etr_' + ident).prop('checked') ? '1' : '0';
        var controler = true;
        if(is_ko_must_valid_chq){
            var anomalie = $('#field_anomalie_' + ident).val();
            anomalie = anomalie === null ? [] : anomalie;
            var intersect = anomalie.filter(function(x) {
                if(IDS_ANOM_CHQ_BLOQUANT.indexOf(x) != -1)
                    return true;
                else
                    return false;
            });
            if(intersect.length > 0){
                controler = false;
            }
        }
        if(controler) {
            field_ok($('#field_cmc7_' + ident));
            field_ok($('#field_rlmc_' + ident));
            field_ok($('#field_nom_client_' + ident));
            var reg = /^\d{31}$/;
            if(etranger == '1'){
                var reg_cmc7_etr = /^\d+$/;
                if(!reg_cmc7_etr.test(cmc7)){
                    if(reponse) {
                        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: CMC7 non num&eacute;rique!', 'bottom', 'right', null, null);
                    }
                    field_error($('#field_cmc7_' + ident));
                    reponse = false;
                }
            }else if (reg.test(cmc7)) {
                var rlmc = $('#field_rlmc_' + ident).val();
                var reg_rlmc = /^\d{1,2}$/;
                if(reg_rlmc.test(rlmc)){
                    var valeur = cmc7.toString() + rlmc.toString();
                    var increm = 0;
                    for (var i_valeur = 0; i_valeur < valeur.length; i_valeur++) {
                        increm = parseInt(increm.toString() + valeur[i_valeur]) % 97;
                    }
                    if(increm != 0){
                        if(reponse) {
                            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: CMC7 ou RLMC invalide!', 'bottom', 'right', null, null);
                        }
                        field_error($('#field_cmc7_' + ident));
                        field_error($('#field_rlmc_' + ident));
                        reponse = false;
                    }
                }else{
                    if(reponse) {
                        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: RLMC invalide!', 'bottom', 'right', null, null);
                    }
                    field_error($('#field_rlmc_' + ident));
                    reponse = false;
                }
            }else{
                if(reponse) {
                    showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: CMC7 invalide!', 'bottom', 'right', null, null);
                }
                field_error($('#field_cmc7_' + ident));
                reponse = false;
            }
            if ($.inArray(cmc7, tab_cmc7) == -1) {
                tab_cmc7.push(cmc7);
            } else {
                if(reponse) {
                    showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: CMC7 en doublon.', 'bottom', 'right', null, null);
                }
                field_error($('#field_cmc7_' + ident));
                reponse = false;
            }
            var id_doc = $.trim($('#field_doc_' + ident).val());
            field_ok($('#field_doc_' + ident));
            var reg_nb = /^\d+$/;
            if (!(reg_nb.test(id_doc))) {
                if(reponse) {
                    showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Les documents correspondants aux cheques sont obligatoires.', 'bottom', 'right', null, null);
                }
                field_error($('#field_doc_' + ident));
                reponse = false;
            }
            var montant = $('#field_montant_' + ident).val();
            field_ok($('#field_montant_' + ident));
            var reg = /^\d+([.,]\d+)?$/;
            if (!(reg.test($.trim(montant)))) {
                if(reponse) {
                    showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Montant invalide!', 'bottom', 'right', null, null);
                }
                field_error($('#field_montant_' + ident));
                reponse = false;
            }
            if(is_ci){
                field_ok($('#field_nom_client_' + ident));
                if($.trim($('#field_nom_client_' + ident).val()) == ''){
                    if(reponse) {
                        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Nom client du ch&egrave;que est obligatoire pour un pli en circulaire!', 'bottom', 'right', null, null);
                    }
                    field_error($('#field_nom_client_' + ident));
                    reponse = false;
                }
            }
        }
    });
    return reponse;
}

function ko_controle_cheque() {
    var etat = $('#field_statut_saisie').val();
    var is_ci = $.inArray(1*etat, [7, 11]) > -1;
    var is_ko_must_valid_chq = $.inArray(1*etat, [6]) > -1;
    var reponse = true;
    $(".grp_field_cheque").each(function() {
        var ident = $(this).attr('my_ident');
        var cmc7 = $.trim($('#field_cmc7_' + ident).val());
        var etranger = $('#field_chq_etr_' + ident).prop('checked') ? '1' : '0';
        field_ok($('#field_cmc7_' + ident));
        field_ok($('#field_rlmc_' + ident));
        field_ok($('#field_nom_client_' + ident));
        var id_doc = $.trim($('#field_doc_' + ident).val());
        field_ok($('#field_doc_' + ident));
        var reg_nb = /^\d+$/;
        if (!(reg_nb.test(id_doc))) {
            if(reponse) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Les documents correspondants aux cheques sont obligatoires.', 'bottom', 'right', null, null);
            }
            field_error($('#field_doc_' + ident));
            reponse = false;
        }
        if(is_ci){
            field_ok($('#field_nom_client_' + ident));
            if($.trim($('#field_nom_client_' + ident).val()) == ''){
                if(reponse) {
                    showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Nom client du ch&egrave;que est obligatoire pour un pli en circulaire!', 'bottom', 'right', null, null);
                }
                field_error($('#field_nom_client_' + ident));
                reponse = false;
            }
        }
    });
    if(is_ci && reponse){
        reponse = controle_cheque();
    }
    if(is_ko_must_valid_chq && reponse){
        reponse = controle_cheque();
    }
    return reponse;
}

function controle_cheque_kd() {
    var reponse = true;
    $(".grp_field_cheque_kd").each(function() {
        var ident = $(this).attr('my_ident');
        var id_doc = $.trim($('#field_doc_chq_kd_' + ident).val());
        field_ok($('#field_doc_chq_kd_' + ident));
        var reg_nb = /^\d+$/;
        if (!(reg_nb.test(id_doc))) {
            if(reponse) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Les documents correspondants aux cheques cadeaux sont obligatoires.', 'bottom', 'right', null, null);
            }
            field_error($('#field_doc_chq_kd_' + ident));
            reponse = false;
        }
        var montant = $('#field_montant_kd_' + ident).val();
        field_ok($('#field_montant_kd_' + ident));
        var reg = /^\d+([.,]\d+)?$/;
        if (!(reg.test($.trim(montant)))) {
            if(reponse) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Montant invalide!', 'bottom', 'right', null, null);
            }
            field_error($('#field_montant_kd_' + ident));
            reponse = false;
        }
    });
    return reponse;
}

function get_view_mvmnt() {
    if (first_modif_mvmnt) {
        swal({
            title: "Confirmation!",
            text: "Vous allez modifier le nombre d\'abonnement qui a \350t\350 defini en typage.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function() {
            first_modif_mvmnt = false;
            add_view_mvmnt();
        });
    } else {
        add_view_mvmnt();
    }
}

function add_view_mvmnt() {
    var id_soc = $('#field_soc').val();
    $.get(chemin_site + '/saisie/saisie/get_mvmnt_view/' + id_soc, {}, function(reponse) {
        $('#main_card_mvmnt').append(reponse);
        $('select.sel_field_mvmnt').selectpicker();
        $('select.sel_field_mvmnt_mdl').selectpicker();
        $.AdminBSB.input.activate();
        interface_mode();
        numerotation_mvmnt();
    });
}

function suppr_gr_field_mvmnt(id_grp) {
    var msg_cnf_plus = '.';
    if (first_modif_mvmnt) {
        msg_cnf_plus = " et la modification du nombre d\'abonnement qui a \350t\350 defini en typage.";
    }
    swal({
        title: "Confirmation!",
        text: "La suppression de ce mouvement entrainera la suppression de ses données" + msg_cnf_plus,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Supprimer",
        cancelButtonText: "Annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        $(id_grp).remove();
        numerotation_mvmnt();
        first_modif_mvmnt = false;
    });
}

function numerotation_mvmnt() {
    var nb_grp_field_mvmnt = ($('.grp_field_mvmnt')).length;
    if (nb_grp_field_mvmnt > 1) {
        var id_soc = $('#field_soc').val();
        $('#field_main_titre').selectpicker('val', id_soc + '000');
    } else {
        var ttr1 = $($("select.sel_field_mvmnt")[0]).val();
        $('#field_main_titre').selectpicker('val', ttr1);
    }
    var num = 0;
    $(".grp_field_mvmnt").each(function() {
        num++;
        $($(this).find('.numerotation_mvmnt')[0]).html(num);
        $($(this).find('.clear_mvmnt')[0]).show();
        if (nb_grp_field_mvmnt == 1) {
            $($(this).find('.clear_mvmnt')[0]).hide();
        }
    });
    $('[data-trigger="spinner"]').spinner();
}

function controle_mvmnt(groupe) {
    var ident = groupe.attr('my_ident');
    var rep = true;
    var num_a = $('#field_num_abon_' + ident).val();
    field_ok($('#field_num_abon_' + ident));
    if ($.trim(num_a) == '') {
        var nom_a = MODE_SAISIE_BATCH ? $('#field_mvmnt_mdl_abonne_' + ident+'_atn_end').val() : $('#field_nom_abon_' + ident).val();
        //var cp_a = $('#field_cp_abon_' + ident).val();
        if ($.trim(nom_a) == ''/* || $.trim(cp_a) == ''*/) {
            if(!notif_nom_ab) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Les informations relatives &agrave; l\'abonn&eacute; d\'un mouvement sont imcompletes.', 'bottom', 'right', null, null);
            }            
            field_error($('#field_num_abon_' + ident));
            rep = false;
            notif_nom_ab = true;
        }
    }
    var num_p = $('#field_num_payeur_' + ident).val();
    field_ok($('#field_num_payeur_' + ident));
    if ($.trim(num_p) == '') {
        var nom_p = MODE_SAISIE_BATCH ? $('#field_mvmnt_mdl_payeur_' + ident+'_atn_end').val() : $('#field_nom_payeur_' + ident).val();
        //var cp_p = $('#field_cp_payeur_' + ident).val();
        if ($.trim(nom_p) == ''/* || $.trim(cp_p) == ''*/) {
            if(!notif_nom_p) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Les informations relatives &agrave; au payeur d\'un mouvement sont imcompletes.', 'bottom', 'right', null, null);
            }
            field_error($('#field_num_payeur_' + ident));
            rep = false;
            notif_nom_p = true;
        }
    }
    return rep;
}

function field_error(compos) {
    var parent = compos.closest('.form-line');
    if (parent.length > 0) {
        parent.addClass('focused').addClass('error').removeClass('warning').removeClass('success');
    }
}

function field_ok(compos) {
    var parent = compos.closest('.form-line');
    if (parent.length > 0) {
        parent.addClass('focused').removeClass('error').removeClass('warning').addClass('success');
    }
}

function controle_all_mvmnt() {
    var tab_title_code_promo = [];
    var tab_title_multi_promo = [];
    var rep = true;
    var with_mvmt = false;
    $(".grp_field_mvmnt").each(function() {
        with_mvmt = true;
        var ident = $(this).attr('my_ident');
        field_ok($('#field_titre_mvmnt_' + ident));
        var mvmnt = {
            titre: $('#field_titre_mvmnt_' + ident).val(),
            code_promo: $('#field_code_promo_' + ident).val()
        };
        var trouver = false;
        tab_title_code_promo.forEach(element => {
            if(element.titre == mvmnt.titre){
                trouver = true;
                if($.trim(element.code_promo) != '' && element.code_promo != mvmnt.code_promo){
                    //field_error($('#field_titre_mvmnt_' + ident));
                    tab_title_multi_promo.push(element.titre);
                }
            }
        });
        if(!trouver){
            tab_title_code_promo.push(mvmnt);
        }
        if (!controle_mvmnt($(this))) {
            rep = false;
        }
    });
    if(!with_mvmt){
        rep = false;
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Aucun mouvement!', 'bottom', 'right', null, null);
    }
    /*if(tab_title_multi_promo.length > 0){
        var msg = tab_title_multi_promo.length > 1 ? 'Ces titres de mouvement sont associ&eacute;s' : 'Ce titre de mouvement est associ&eacute;';
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: '+msg+' &agrav; des codes promotion diff&eacute;rents: '+tab_title_multi_promo.join(', '), 'bottom', 'right', null, null);
        return false;
    }*/
    return rep;
}

function controle_data() {
    notif_nom_ab = false;
    notif_nom_p = false;
    var ctrl_mvmnt = controle_all_mvmnt();
    var ctrl_oblig = true;
    $(".oblig").each(function() {
        field_ok($(this));
        if ($.trim($(this).val()) == '') {
            field_error($(this));
            if(ctrl_oblig) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Champ obligatoire non renseign&eacute;.', 'bottom', 'right', null, null);
            }
            ctrl_oblig = false;
        }
    });
    var typo = $('#field_typologie').val();
    var code_ecole_gci = $('#field_code_ecole').val();
    field_ok($('#field_code_ecole'));
    if($.inArray(typo, ['8','14', '15', '17', '18']) != -1 && $.trim(code_ecole_gci) == ''){
        field_error($('#field_code_ecole'));
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Code &eacute;cole/GCI obligatoire pour les flux r&eacute;seaux.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    var etat = $('#field_statut_saisie').val();
    var is_ok = $.inArray(1*etat, [1, 11]) > -1;
    var is_ci = $.inArray(1*etat, [7, 11]) > -1;
    var motif_ko = $('#field_motif_ko').val();
    var motif_ko_autre = $('#field_autre_motif_ko').val();
    field_ok($('#field_autre_motif_ko'));
    field_ok($('#field_motif_ko'));
    if (((!is_ok || is_ci) && motif_ko == 4 && $.trim(motif_ko_autre) == '') || ((!is_ok || is_ci) && motif_ko == null)) {
        field_error($('#field_autre_motif_ko'));
        field_error($('#field_motif_ko'));
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Motif du KO (ou CI) non renseign&eacute;.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    var nom_ci = $('#field_nom_circulaire').val();
    if (is_ci && (nom_ci == -1 || $('#field_fichier_circulaire').val() == '')) {//NOTE fichier CI
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Le nom et le fichier du circulaire doit &ecirc;tre rensegn&eacute;!', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    /*field_ok($('#field_nom_deleg'));
    if (etat == 7) {
        if($.trim($('#field_nom_deleg').val()) == ''){
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Nom d&eacute;l&eacute;gu&eacute; est obligatoire pour un pli en circulaire!', 'bottom', 'right', null, null);
            field_error($('#field_nom_deleg'));
            ctrl_oblig = false;
        }
    }*/
    if ($('#field_mode_paiement').val() == null) {
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Mode de paiement non renseign&eacute;.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    var ctrl_uniq_doc = true;
    var tab_doc = [];
    $("select.field_doc_correspond").each(function() {
        field_ok($(this));
        var id_doc = $(this).val();
        if ($.inArray(id_doc, tab_doc) == -1) {
            tab_doc.push(id_doc);
        } else {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Document de paiement en doublon: #' + id_doc, 'bottom', 'right', null, null);
            field_error($(this));
            ctrl_uniq_doc = false;
        }
    });
    if($("#row_montant_espece").is(":visible")){
        field_ok($('#field_montant_espece'));
        var reg = /^\d+([.,]\d+)?$/;
        if(!(reg.test($.trim($("#field_montant_espece").val())))){
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Montant invalide!', 'bottom', 'right', null, null);
            field_error($('#field_montant_espece'));
            ctrl_oblig = false;
        }
    }
    if (ctrl_mvmnt && ctrl_oblig && controle_cheque() && controle_cheque_kd() && ctrl_uniq_doc) {
        return true;
    }
    return false;
}

function quick_controle_data(){
    notif_nom_ab = false;
    notif_nom_p = false;
    var ctrl_mvmnt = controle_all_mvmnt();
    var ctrl_oblig = true;
    $(".oblig").each(function() {
        field_ok($(this));
        if ($.trim($(this).val()) == '') {
            field_error($(this));
            if(ctrl_oblig) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Champ obligatoire non renseign&eacute;.', 'bottom', 'right', null, null);
            }
            ctrl_oblig = false;
        }
    });
    if ($('#field_mode_paiement').val() == null) {
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Mode de paiement non renseign&eacute;.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    var ctrl_uniq_doc = true;
    var tab_doc = [];
    $("select.field_doc_correspond").each(function() {
        field_ok($(this));
        var id_doc = $(this).val();
        if(/^\d+$/.test(id_doc)) {
            if ($.inArray(id_doc, tab_doc) == -1) {
                tab_doc.push(id_doc);
            } else {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Document de paiement en doublon: #' + id_doc, 'bottom', 'right', null, null);
                field_error($(this));
                ctrl_uniq_doc = false;
            }
        }
    });
    if($("#row_montant_espece").is(":visible")){
        field_ok($('#field_montant_espece'));
        var reg = /^\d+([.,]\d+)?$/;
        if(!(reg.test($.trim($("#field_montant_espece").val())))){
            $("#row_montant_espece").val('0');
        }
    }
    if (/*ctrl_mvmnt && ctrl_oblig &&*/ controle_cheque() && /*controle_cheque_kd() && */ctrl_uniq_doc) {
        return true;
    }
    return false;
}

function ko_controle_data(){
    notif_nom_ab = false;
    notif_nom_p = false;
    var ctrl_uniq_doc = true;
    var tab_doc = [];
    var etat = $('#field_statut_saisie').val();
    var is_ok = $.inArray(1*etat, [1, 11]) > -1;
    var is_ci = $.inArray(1*etat, [7, 11]) > -1;
    var motif_ko = $('#field_motif_ko').val();
    var motif_ko_autre = $('#field_autre_motif_ko').val();
    field_ok($('#field_autre_motif_ko'));
    field_ok($('#field_motif_ko'));
    if (((!is_ok || is_ci) && motif_ko == 4 && $.trim(motif_ko_autre) == '') || ((!is_ok || is_ci) && motif_ko == null)) {
        field_error($('#field_autre_motif_ko'));
        field_error($('#field_motif_ko'));
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Motif du KO (ou CI) non renseign&eacute;.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    var nom_ci = $('#field_nom_circulaire').val();
    if (is_ci && (nom_ci == -1 || $('#field_fichier_circulaire').val() == '')) {//NOTE fichier CI
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Le nom du circulaire doit &ecirc;tre rensegn&eacute;!', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    /*field_ok($('#field_nom_deleg'));
    if (etat == 7) {
        if($.trim($('#field_nom_deleg').val()) == ''){
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Nom d&eacute;l&eacute;gu&eacute; est obligatoire pour un pli en circulaire!', 'bottom', 'right', null, null);
            field_error($('#field_nom_deleg'));
            ctrl_oblig = false;
        }
    }*/
    $("select.field_doc_correspond").each(function() {
        field_ok($(this));
        var id_doc = $(this).val();
        if(/^\d+$/.test(id_doc)) {
            if ($.inArray(id_doc, tab_doc) == -1) {
                tab_doc.push(id_doc);
            } else {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Document de paiement en doublon: #' + id_doc, 'bottom', 'right', null, null);
                field_error($(this));
                ctrl_uniq_doc = false;
            }
        }
    });
    if($("#row_montant_espece").is(":visible")){
        field_ok($('#field_montant_espece'));
        var reg = /^\d+([.,]\d+)?$/;
        if(!(reg.test($.trim($("#field_montant_espece").val())))){
            $("#row_montant_espece").val('0');
        }
    }
    if (/*ctrl_mvmnt && ctrl_oblig &&*/ ko_controle_cheque() && /*controle_cheque_kd() && */ctrl_uniq_doc) {
        return true;
    }
    return false;
}

function valeur_cheques() {
    var data = [];
    $(".grp_field_cheque").each(function() {
        var ident = $(this).attr('my_ident');
        var anomalie = $('#field_anomalie_' + ident).val();
        var cheque = {
            cmc7: $('#field_cmc7_' + ident).val(),
            rlmc: $('#field_rlmc_' + ident).val(),
            montant: u_montant($('#field_montant_' + ident).val()),
            anomalies: anomalie,//JSON.stringify(anomalie),
            id_etat_chq: $('#field_etat_chq_' + ident).val(),
            etat_modif: $('#field_etat_modif_chq_' + ident).val(),
            id_doc: $.trim($('#field_doc_' + ident).val())
            ,nom_client: $('#field_nom_client_' + ident).val()
            ,etranger: $('#field_chq_etr_' + ident).prop('checked') ? '1' : '0'
            ,ui_id: ident
        };
        data.push(cheque);
    });
    return data;
}

function valeur_cheques_kd() {
    var data = [];
    $(".grp_field_cheque_kd").each(function() {
        var ident = $(this).attr('my_ident');
        var cheque = {
            montant: u_montant($('#field_montant_kd_' + ident).val()),
            type: $.trim($('#field_type_chq_kd_' + ident).val()),
            code_barre: $.trim($('#field_code_barre_kd_' + ident).val()),
            id_doc: $.trim($('#field_doc_chq_kd_' + ident).val())
            ,ui_id: ident
        };
        data.push(cheque);
    });
    return data;
}

function valeur_mvmnts() {
    var data = {
        mvmnt: []
        ,payeurs: {}
        ,abonnes: {}
        ,promos: {}
    };
    $(".grp_field_mvmnt").each(function() {
        var ident = $(this).attr('my_ident');
        var mvmnt = {
            titre: $('#field_titre_mvmnt_' + ident).val(),
            code_promo: $('#field_code_promo_' + ident).val(),
            num_a: $('#field_num_abon_' + ident).val(),
            nom_a: MODE_SAISIE_BATCH ? $('#field_mvmnt_mdl_abonne_' + ident+'_atn_end').val() : $('#field_nom_abon_' + ident).val(),
            //cp_a: $('#field_cp_abon_' + ident).val(),
            num_p: $('#field_num_payeur_' + ident).val(),
            nom_p: MODE_SAISIE_BATCH ? $('#field_mvmnt_mdl_payeur_' + ident+'_atn_end').val() : $('#field_nom_payeur_' + ident).val() //,cp_p: $('#field_cp_payeur_' + ident).val()
            ,ui_id: ident
            ,qtt: $('#field_qunatity_nb_' + ident).val() == undefined ? '1' : $('#field_qunatity_nb_' + ident).val()
        };
        data.mvmnt.push(mvmnt);
        if(MODE_SAISIE_BATCH) {
            data.payeurs[ident] = compile_data_client('field_mvmnt_mdl_payeur_'+ident);
            data.abonnes[ident] = compile_data_client('field_mvmnt_mdl_abonne_'+ident);
            data.promos[ident] = {
                code_promo: $('#field_code_promo_' + ident).val()
                ,code_choix: $('#field_code_promo_choix_mvmnt_' + ident).val()
                ,code_produit: $('#field_code_promo_produit_mvmnt_' + ident).val()
            };
        }
    });
    return data;
}

function valeur_field_saisie_ctrl(){
    var data = [];
    $(".field_saisie_ctrl").each(function() {
        var valeur = $(this).val();
        if(Array.isArray(valeur)){
            valeur = valeur.join();
        }
        var saisie_ctrl = {
            id_field: $(this).attr('id')
            ,name_field: $(this).attr('nom_field')
            ,valeur: valeur
        };
        var my_id = $(this).attr('id');
        if (typeof my_id !== typeof undefined && my_id !== false) {
            data.push(saisie_ctrl);
        }
    });
    return data;
}

function recup_valeur() {
    var montant_esp = $("#row_montant_espece").is(":visible") ? $.trim($("#field_montant_espece").val()) : '0';
    $data_mvmnts = valeur_mvmnts();
    return {
        id_pli: ID_PLI,
        idefix: $('#field_idefix').val(),
        id_lot_saisie: ID_LOT_SAISIE,
        id_group_pli_possible: ID_GRP_PLI_POSS,
        datamatrix: $('#field_data_matrix').val(),
        code_promo: $('#field_promotion').val(),
        //type_coupon: $('#field_type_coupon').val(),
        id_soc: $('#field_soc').val(),
        typo: $('#field_typologie').val(),
        titre: $('#field_main_titre').val(),
        code_ecole_gci: $('#field_code_ecole').val(),
        mvmnts: JSON.stringify($data_mvmnts.mvmnt),
        mvmnt_payeurs: JSON.stringify($data_mvmnts.payeurs),
        mvmnt_abonnes: JSON.stringify($data_mvmnts.abonnes),
        mvmnt_promos: JSON.stringify($data_mvmnts.promos),
        paiements: JSON.stringify($('#field_mode_paiement').val()),
        montant_esp: u_montant(montant_esp),
        cheques: JSON.stringify(valeur_cheques()),
        cheques_cadeaux: JSON.stringify(valeur_cheques_kd()),
        statut: $('#field_statut_saisie').val(),
        motif_ko: $('#field_motif_ko').val(),
        autre_motif_ko: $('#field_autre_motif_ko').val(),
        nom_circ: $('#field_nom_circulaire').val() == -1 ? '' : $('#field_nom_circulaire').val(),
        nom_deleg: $.trim($('#field_nom_deleg').val()),
        fichier_circ: '',
        msg_ind: $('#field_msg_ind').prop('checked') ? '1' : '0',
        dmd_kdo: $('#field_dmd_kdo').prop('checked') ? '1' : '0',
        dmd_env_kdo: $('#field_dmd_env_kdo').prop('checked') ? '1' : '0',
        img_dezip: JSON.stringify($('#field_img_desar').val() == null ? [] : $('#field_img_desar').val())
        ,saisie_batch: MODE_SAISIE_BATCH ? 1 : 0
        ,field_saisie_ctrl: JSON.stringify(valeur_field_saisie_ctrl())
    }
}

function quick_recup_valeur() {
    var montant_esp = $("#row_montant_espece").is(":visible") ? $.trim($("#field_montant_espece").val()) : '0';
    $data_mvmnts = valeur_mvmnts();
    return {
        id_pli: ID_PLI,
        idefix: $('#field_idefix').val(),
        id_lot_saisie: ID_LOT_SAISIE,
        id_group_pli_possible: ID_GRP_PLI_POSS,
        datamatrix: $('#field_data_matrix').val(),
        code_promo: $('#field_promotion').val(),
        //type_coupon: $('#field_type_coupon').val(),
        id_soc: $('#field_soc').val(),
        typo: $('#field_typologie').val(),
        titre: $('#field_main_titre').val(),
        code_ecole_gci: $('#field_code_ecole').val(),
        mvmnts: JSON.stringify($data_mvmnts.mvmnt),
        mvmnt_payeurs: JSON.stringify([]),
        mvmnt_abonnes: JSON.stringify([]),
        mvmnt_promos: JSON.stringify([]),
        paiements: JSON.stringify($('#field_mode_paiement').val()),
        montant_esp: u_montant(montant_esp),
        cheques: JSON.stringify(valeur_cheques()),
        cheques_cadeaux: JSON.stringify(valeur_cheques_kd()),
        nom_circ: $('#field_nom_circulaire').val() == -1 ? '' : $('#field_nom_circulaire').val(),
        nom_deleg: $.trim($('#field_nom_deleg').val()),
        msg_ind: $('#field_msg_ind').prop('checked') ? '1' : '0',
        dmd_kdo: $('#field_dmd_kdo').prop('checked') ? '1' : '0',
        dmd_env_kdo: $('#field_dmd_env_kdo').prop('checked') ? '1' : '0',
        img_dezip: JSON.stringify($('#field_img_desar').val() == null ? [] : $('#field_img_desar').val())
        ,saisie_batch: MODE_SAISIE_BATCH ? 1 : 0
        ,field_saisie_ctrl: JSON.stringify(valeur_field_saisie_ctrl())
    }
}

function send_valeur(tmp_link_f_circ = '', ext_f_circ = '', nom_f_circ = '') {
    var data_to_serv = recup_valeur();
    data_to_serv.fichier_circ = tmp_link_f_circ;
    data_to_serv.ext_fichier_circ = ext_f_circ;
    data_to_serv.nom_fichier_circ = nom_f_circ;
    swal({
        title: 'Enregistrer la saisie?',
        text: 'Pli#' + ID_PLI,
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + '/saisie/saisie/save_saisie', data_to_serv, function(reponse) {
            if (reponse == 1) {
                swal({
                    title: 'Enregistrement saisie.',
                    text: 'Enregistrement réussi du pli#' + ID_PLI,
                    type: 'success',
                    //timer: 2000,
                    showConfirmButton: false
                    ,closeOnConfirm: false
                }, function(){in_load_pli();});
                in_load_pli();
                setTimeout(function() {
                    window.location.href = chemin_site + '/saisie/saisie?direct=1';
                }, 1000);
            } else if (reponse == 2) {
                swal({
                    title: 'Enregistrement saisie.',
                    text: 'Enregistrement réussi du pli#' + ID_PLI + ' en lot saisie BIS.',
                    type: 'warning'
                    //,showConfirmButton: false
                    ,closeOnConfirm: false
                }, function() {
                    in_load_pli();
                    window.location.href = chemin_site + '/saisie/saisie?direct=1';
                });
            } else {
                swal('Serveur', 'Erreur: ' + reponse, 'error');
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}

function quick_send_valeur() {
    var data_to_serv = quick_recup_valeur();
    swal({
        title: 'Enregistrer les donn\351es en cours',
        text: 'Les données du pli#' + ID_PLI+' seront enregistrées et vous pourrez toujours poursuivre votre saisie.',
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + '/saisie/saisie/quick_save', data_to_serv, function(reponse) {
            if (reponse == 1) {
                swal({
                    title: 'Donn\351es enregistr\351es.',
                    text: 'Vous pouvez continuer la saisie ou vous deconnecter.',
                    type: 'success',
                    timer: 2000,
                    showConfirmButton: true
                    ,closeOnConfirm: true
                }, function(){});
            // } else if (reponse == 2) {
            //     swal({
            //         title: 'Donn\351es enregistr\351es.',
            //         text: 'Vous pouvez continuer la saisie ou vous deconnecter.',
            //         type: 'warning'
            //         ,closeOnConfirm: true
            //     }, function() {
            //         window.location.href = chemin_site + '/saisie/saisie?direct=1';
            //     });
            } else {
                swal('Serveur', 'Erreur: ' + reponse, 'error');
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}
function ko_send_valeur(tmp_link_f_circ = '', ext_f_circ = '', nom_f_circ = '') {
    var data_to_serv = quick_recup_valeur();
    data_to_serv.statut = $('#field_statut_saisie').val();
    data_to_serv.motif_ko = $('#field_motif_ko').val();
    data_to_serv.autre_motif_ko = $('#field_autre_motif_ko').val();
    data_to_serv.fichier_circ = tmp_link_f_circ;
    data_to_serv.ext_fichier_circ = ext_f_circ;
    data_to_serv.nom_fichier_circ = nom_f_circ;
    //data_to_serv.mvmnts = JSON.stringify([]);
    swal({
        title: 'Mise en KO?',
        text: 'Le pli#' + ID_PLI+' sera mis en KO.',
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + '/saisie/saisie/ko_save', data_to_serv, function(reponse) {
            if (reponse == 1) {
                swal({
                    title: 'Serveur.',
                    text: 'Pli#' + ID_PLI + ' mis en KO.',
                    type: 'success',
                    //timer: 2000,
                    showConfirmButton: false
                    ,closeOnConfirm: false
                }, function(){in_load_pli();});
                in_load_pli();
                setTimeout(function() {
                    window.location.href = chemin_site + '/saisie/saisie?direct=1';
                }, 1000);
            } else {
                swal('Serveur', 'Erreur: ' + reponse, 'error');
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}

function quick_save() {
    if (quick_controle_data()) {
        quick_send_valeur();
    }
}

function enregistrer() {
    var etat = $('#field_statut_saisie').val();
    var is_ok = $.inArray(1*etat, [1, 11]) > -1;
    if(is_ok) {
        if (controle_data()) {
            if ($('#field_fichier_circulaire').val() == '') {//NOTE fichier CI
                send_valeur();
            } else {
                send_valeur_with_f_circ();
            }
        }
    } else {
        ko_enregistrer();
    }
}

function ko_enregistrer() {
    if(ko_controle_data()){
        if ($('#field_fichier_circulaire').val() == '') {//NOTE fichier CI
            ko_send_valeur();
        } else {
            ko_send_valeur_with_f_circ();
        }
    }
}

function annuler() {
    swal({
        title: "Annulation!",
        text: "Annuler la saisie?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, annuler!",
        cancelButtonText: "Non!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        in_load_pli();
        $.get(chemin_site + '/saisie/saisie/cancel_saisie/' + ID_PLI, {}, function() {
            window.location.href = chemin_site + '/saisie/saisie';
        });
    });
}

function send_valeur_with_f_circ() {
    var frm_f_circ = $('#form_fichier_circulaire');
    var frmdata_f_circ = (window.FormData) ? new FormData(frm_f_circ[0]) : null;
    var f_circ = (frmdata_f_circ !== null) ? frmdata_f_circ : frm_f_circ.serialize();
    $.ajax({
        url: chemin_site + '/saisie/saisie/save_file_circulaire',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: f_circ,
        success: function(from_serv) {
            try {
                var reponse = $.parseJSON(from_serv);
            } catch (err) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de l\'envoi du fichier de circulaire vers le serveur.', 'bottom', 'right', null, null);
                return '';
            }
            if (reponse.code == 1) {
                send_valeur(reponse.link, reponse.ext, reponse.name);
            } else {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de l\'envoi du fichier de circulaire vers le serveur.', 'bottom', 'right', null, null);
            }
        },
        error: function() {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de l\'envoi du fichier de circulaire vers le serveur.', 'bottom', 'right', null, null);
        }
    });
}

function ko_send_valeur_with_f_circ() {
    var frm_f_circ = $('#form_fichier_circulaire');
    var frmdata_f_circ = (window.FormData) ? new FormData(frm_f_circ[0]) : null;
    var f_circ = (frmdata_f_circ !== null) ? frmdata_f_circ : frm_f_circ.serialize();
    $.ajax({
        url: chemin_site + '/saisie/saisie/save_file_circulaire',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: f_circ,
        success: function(from_serv) {
            try {
                var reponse = $.parseJSON(from_serv);
            } catch (err) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de l\'envoi du fichier de circulaire vers le serveur.', 'bottom', 'right', null, null);
                return '';
            }
            if (reponse.code == 1) {
                ko_send_valeur(reponse.link, reponse.ext, reponse.name);
            } else {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de l\'envoi du fichier de circulaire vers le serveur.', 'bottom', 'right', null, null);
            }
        },
        error: function() {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de l\'envoi du fichier de circulaire vers le serveur.', 'bottom', 'right', null, null);
        }
    });
}

function show_info_lot() {
    $('#mdl_display_lot_saisie').modal('show');
}

function load_list_lot() {
    $("#lot_saisie_dtt").DataTable({
		bprocessing: true
		,bServerSide: true
		,bScrollCollapse: true
		,aLengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]]
		,pageLength: 5
		,bDestroy: true
		,bFilter: true
		,sScrollX: true
		,oLanguage: {
			sLengthMenu: "_MENU_ lignes par page"
			,sZeroRecords: "Aucun lot"
			,sEmptyTable: "Aucun lot"
			,sInfo: "de _START_ \340 _END_ sur _TOTAL_ lot"
			,sInfoFiltered: "(Total : _MAX_ lot)"
			,sLoadingRecords: "Chargement en cours..."
			,sProcessing: "Traitement en cours..."
			,oPaginate: {
				sFirst: "PREMIERE"
				,sPrevious: "PRECEDENTE"
				,sNext: "SUIVANTE"
				,sLast: "DERNIERE"
			}
			,sSearch: "Rechercher: "
		}
		,ajax: {
			url: chemin_site + "/saisie/saisie/list_lot_saisies"
			,sServerMethod: "POST"
			,data: function(d) {
				d.dt = "";
			}
		}
		,order: [[0, "desc"]]
		,fnDrawCallback: function(oSettings) {
			$('[data-toggle="tooltip"]').tooltip();
		}
		,aoColumnDefs: [
			{
				aTargets: [5, 7, 9]
				,fnCreatedCell: function(nTd, sData, oData, iRow, iCol) {
					$(nTd).addClass("align-center");
				}
			}
		]
		,columns: [
			{ data: "dt", targets: [0] }
			,{ data: "id", targets: [1] }
			,{ data: "soc", targets: [2] }
			,{ data: "typo", targets: [3] }
			,{ data: "paie", targets: [4] }
			,{ data: "nb", targets: [5] }
			,{ data: "pli", targets: [6] }
			,{ data: "nb_b", targets: [7] }
			,{ data: "pli_b", targets: [8] }
			,{ data: "tt", targets: [9] }
		]
	});
}

function show_info_grp_pli() {
    $('#mdl_display_group_pli').modal('show');
}

function load_list_grp_pli() {
    // $('#mdl_display_group_pli_body').html('');
    $.get(chemin_site + '/saisie/saisie/list_groupement_pli', {}, function(view) {
        $('#mdl_display_group_pli_body').html(view);
    });
}

function show_info_grp_pli_by_older() {
    $('#mdl_display_group_pli_by_older').modal('show');
}

function load_list_grp_pli_by_older() {
    // $('#mdl_display_group_pli_by_older_body').html('');
    $.get(chemin_site + '/saisie/saisie/list_groupement_pli_by_older', {}, function(view) {
        $('#mdl_display_group_pli_by_older_body').html(view);
    });
}

function copyToClipboard7cmc7(id_cmc7){
    var cmc7 = $(id_cmc7).val();
    var text = '0';
    if(cmc7.length >= 7){
        text = cmc7.substring(0, 7);
    }
    var dummy = document.createElement("input");
    document.body.appendChild(dummy);
    dummy.setAttribute('value', text);
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
}

/*function hors_perim() {
    var id_pli = ID_PLI;
    swal({
        title: 'Mettre en hors p\351rim\350tre ce pli?',
        text: 'Pli#' + id_pli,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/saisie/saisie/mise_en_hp/' + id_pli, {}, function(rep) {
            if (rep == 1) {
                swal({
                    title: "R\351ussi!",
                    text: "Pli#" + id_pli + " mis hors p\351rim\350tre.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
                setTimeout(function() {
                    window.location.href = chemin_site + '/saisie/saisie';
                }, 2000);
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Op&eacute,ration &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}

function valider_ko_scan() {
    var id_pli = ID_PLI;
    var motif = $('#sel_motif_ko_scan').val();
    var comment = $('#comment_ko_scan').val();
    var valide = true;
    if(motif == '14' && $.trim(comment) == ''){
        showNotification('alert-warning', '<i class="material-icons">error</i>Merci de pr&eacute;ciser le motif!.', 'top', 'right', null, null);
        valide = false;
    }
    if(valide){
        var data = {
            id_pli: id_pli
            ,motif: motif
            ,comment: comment
        };
        swal({
            title: 'Mettre en KO-scan ce pli?',
            text: 'Pli#' + id_pli,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Valider",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.get(chemin_site + '/saisie/saisie/mise_en_ko_scan', data, function(rep) {
                if (rep == 1) {
                    swal({
                        title: "R\351ussi!",
                        text: "Pli#" + id_pli + " en KO-scan",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: true
                    });
                    setTimeout(function() {
                        window.location.href = chemin_site + '/saisie/saisie';
                    }, 2000);
                } else {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Mise en KO-scan &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                    swal("Serveur", "Erreur! " + rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            });
        });
    }
}*/

function load_from_data_addr_client(id_g_field, class_g_field) {
    var data = {
        id_client: $.trim($('#'+id_g_field+'ctm_nbr').val())
    };
    if(data.id_client == ''){
        swal("Chargement de donn\351es client...", "Num\351ro client requis!", "error");
        return '';
    }
    swal({
        title: 'Charger les donn\351es du client?',
        text: 'Client#' + data.id_client,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/control/ajax/data_addr_client/'+data.id_client, {}, function(from_serv) {
            try {
                var reponse = $.parseJSON(from_serv);
            } catch (err) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de la recuperation des donn&eacute;es.', 'bottom', 'right', null, null);
                return '';
            }
            if (reponse.code == 0) {
                var client = reponse.client;
                swal({
                    title: "R\351ussi!",
                    text: "Donn\351es du client#" + data.id_client + " import\351es",
                    type: "success",
                    timer: 1000,
                    showConfirmButton: true
                });
                for(var col in client){
                    if($('#'+id_g_field+col)){
                        if($('#'+id_g_field+col).hasClass('sel_field_mvmnt_mdl')){
                            $('#'+id_g_field+col).selectpicker('val', client[col]);
                        }else{
                            $('#'+id_g_field+col).val(client[col]);
                        }
                        var his_twin = $('#'+id_g_field+col).attr('twin');
                        /*if(typeof his_twin !== typeof undefined && his_twin !== false){
                            $('#'+his_twin).val(client[col]);
                        }*/
                        if(($('#'+his_twin)).length > 0){
                            $('#'+his_twin).val(client[col]);
                        }
                    }
                }
                $.AdminBSB.input.activate();
            } else {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: '+reponse.msg, 'bottom', 'right', null, null);
                swal("Serveur", "Client#"+data.id_client+" introuvalble!", "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}

function maj_twin(me) {
    var val_num = $.trim(me.val());
    var id_twin = me.attr('my_twin');
    if(($('#'+id_twin)).length > 0){
        $('#'+id_twin).val(val_num);
    }
    var class_field_addr = me.attr('my_field_addr');
    $('.'+class_field_addr).each(function(){
        if(val_num == ''){
            $(this).removeAttr('disabled').removeAttr('readonly');
        }else{
            $(this).attr('disabled','').attr('readonly','');
        }
    });
}

function load_from_data_promo(me, class_g_field, my_id, id_chx, id_art) {
    if(!MODE_SAISIE_BATCH){
        return '';
    }
    var data = {
        code_promo: $.trim(me.val())
    };
    if(data.code_promo == ''){
        // swal("Chargement de donn\351es promotion...", "Code promotion requis!", "error");
        field_error(me);
        $('select.'+class_g_field).each(function(){
            $(this).selectpicker('destroy').html('').selectpicker();
        });
        // showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Code promotion vide.', 'bottom', 'right', null, null);
        return '';
    }
    $.get(chemin_site + '/control/ajax/data_promo_cba/'+data.code_promo, {}, function(from_serv) {
        try {
            var reponse = $.parseJSON(from_serv);
        } catch (err) {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de la recuperation des donn&eacute;es promotion.', 'bottom', 'right', null, null);
            return '';
        }
        if (reponse.code == 0) {
            var promos = reponse.promos;
            $('#'+id_chx).selectpicker('destroy').html('');
            var options = '';
            var ln_ligne_promo = promos.length;
            for (var index = 0; index < ln_ligne_promo; index++) {
                var promo = promos[index];
                options += '<option value="'+promo.code_choix_promo+'"'+(index == 0 ? ' selected' : '')+' data-subtext=" - '+promo.code_choix_promo+' - '+promo.code_art+'" >'+promo.lib_choix+'</option>';
            }
            field_ok(me);
            if(ln_ligne_promo == 0){
                field_error(me);
                options += '<option value="" selected data-subtext="" >Code promotion introuvable</option>';
            }
            $('#'+id_chx).html(options).selectpicker();
            load_from_data_promo_art(my_id, id_chx, id_art);
        } else {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: '+reponse.msg, 'bottom', 'right', null, null);
            // swal("Serveur", "Client#"+data.id_client+" introuvalble!", "error");
        }
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Recuperation donn&eacute;es promotion &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        // swal("Serveur", "Erreur serveur!", "error");
    });
}

function load_from_data_promo_art(id_sel_code_promo, id_sel_code_chx, id_me) {
    var data = {
        code_promo: $('#'+id_sel_code_promo).val()
        ,code_chx: $('#'+id_sel_code_chx).val()
    };
    $('#'+id_me).selectpicker('destroy').html('').selectpicker();
    field_error($('#'+id_sel_code_chx));
    $.post(chemin_site + '/control/ajax/data_promo_cba_art', data, function(from_serv) {
        try {
            var reponse = $.parseJSON(from_serv);
        } catch (err) {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de la recuperation des codes produit correspondant.', 'bottom', 'right', null, null);
            return '';
        }
        if (reponse.code == 0) {
            var promos = reponse.promos;
            $('#'+id_me).selectpicker('destroy').html('');
            var options = '';
            var ln_ligne_promo = promos.length;
            for (var index = 0; index < ln_ligne_promo; index++) {
                var promo = promos[index];
                options += '<option value="'+promo.code_art+'"'+(index == 0 ? ' selected' : '')+' data-subtext=" - '+promo.code_choix_promo+'" >'+promo.code_art+'</option>';
            }
            field_ok($('#'+id_sel_code_chx));
            if(ln_ligne_promo == 0){
                field_error($('#'+id_sel_code_chx));
                options += '<option value="" selected data-subtext="" >Aucun code produit trouvé</option>';
            }
            $('#'+id_me).html(options).selectpicker();
        } else {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: '+reponse.msg, 'bottom', 'right', null, null);
            // swal("Serveur", "Client#"+data.id_client+" introuvalble!", "error");
        }
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Recuperation donn&eacute;es codes produit &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        // swal("Serveur", "Erreur serveur!", "error");
    });
}

function compile_data_client(class_g) {
    var client = {};
    $('.'+class_g).each(function(){
        var code = $(this).attr('my_code');
        client[code] = $(this).val();
    });
    return client;
}

function interface_mode() {
    if(MODE_SAISIE_BATCH){
        $('.direct_only').addClass('hidden');
        $('.batch_only').removeClass('hidden');
    }else{
        $('.batch_only').addClass('hidden');
        $('.direct_only').removeClass('hidden');
    }
}

function redirecte_saisie_advantage() {
    var id_pli = ID_PLI;
    swal({
        title: 'Traiter ce pli en saisie Advantage?',
        text: 'Pli#' + id_pli,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/saisie/saisie/redirecte_saisie_advantage/' + id_pli, {}, function(rep) {
            if (rep == 1) {
                swal({
                    title: "R\351ussi!",
                    text: "Rechargement en mode saisie directe advantage du Pli#" + id_pli + ".",
                    type: "success",
                    //timer: 2000,
                    showConfirmButton: false
                    ,closeOnConfirm: false
                }, function(){in_load_pli();});
                in_load_pli();
                setTimeout(function() {
                    window.location.href = chemin_site + '/saisie/saisie?direct=1';
                }, 2000);
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Op&eacute,ration &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}

function vers_saisie_n2() {
    var id_pli = ID_PLI;
    swal({
        title: 'Envoyer ce pli en saisie niveau 2?',
        text: 'Pli#' + id_pli,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/saisie/saisie/vers_saisie_n2/' + id_pli, {}, function(rep) {
            if (rep == 1) {
                swal({
                    title: "R\351ussi!",
                    text: "Pli#" + id_pli + " mis en niveau 2.",
                    type: "success",
                    //timer: 2000,
                    showConfirmButton: false
                    ,closeOnConfirm: false
                }, function(){in_load_pli();});
                in_load_pli();
                setTimeout(function() {
                    window.location.href = chemin_site + '/saisie/saisie?direct=1';
                }, 2000);
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Op&eacute,ration &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}

function u_montant(montant='0.00') {
    montant = ('' + montant).replace(',', '.');
    montant = montant.replace(/[^0-9.]/g, '');
    montant = Number.parseFloat(montant);
    montant = isNaN(montant) ? 0 : montant;
    if(Number.isInteger(montant)){return ''+montant+'.00'; }
    var montant_str = montant.toString(10);
    return /^\d+\.\d{1}$/.test(montant_str) ? montant_str + '0' : montant_str;
}
