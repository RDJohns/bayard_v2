$(function () {

    init_nsprogress();

});

function init_nsprogress(){
    $( document ).ajaxStart(function() {
        NProgress.start();
    });
    $(document).ajaxStop(function() {
        NProgress.done();
    });
}

function search_chq(){
    var societe = $("#id_societe").val();
    var etat    = $("#id_etat").val();
    var cmc     = $("#cmc-txt").val();

    var data = {
        societe : societe,
        etat : etat,
        cmc : cmc
    };

    $('#table-chq').DataTable({
        dom: 'lBfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100],[5,10,25,50,100]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": false
        ,"oLanguage": {
            "sLengthMenu": "Afficher _MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "Affichage de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
            ,"sProcessing":""
        }
        ,"ajax":{
            //'dataType': 'json',
            'url': s_url+'cheque/traitement_cheque/get_cheque'
            ,"data": data
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //alert('test');
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },
        "initComplete": function(settings, json) {

        }
        ,'columns':[
            { "data": 'id_pli' }
            ,{ "data": 'nom_societe'}
            ,{ "data": 'lot_scan'}
            ,{ "data": 'cmc7'}
            ,{ "data": 'montant'}
            ,{ "data": 'etat'}
        ]
    });
}

function change_chq(id){
    var etat = $("#chq-"+id).val();
    $.post(s_url + '/cheque/etat_cheque/change_etat_chq', {id:id,etat:etat}, function(res) {
        if(res == 'success'){
            alert('cheque id:'+id+' à jour => '+etat);
        }
    });
}

function change_montant(id,id_saisie){
    var mont = $("#montant-"+id);
    var montant = mont.text();
    swal({
        title: "Modification montant",
        text: "Nouveau montant:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        cancelButtonText: "Annuler",
        inputPlaceholder: "Montant..."
    }, function (inputValue) {
        if(inputValue === false) return false;
        if ((isNaN(inputValue)) || (inputValue === ""))
        {
            swal.showInputError("Il faut renseigner un montant!");
            return false;
        }
        else{
            $.post(s_url + '/cheque/traitement_cheque/change_montant_chq', {id:id,id_saisie:id_saisie,montant:inputValue}, function(res) {
                if(res == 'success'){
                    mont.text(u00_montant(inputValue));
                    swal.close();
                }
            });
        }
    });
}

function u00_montant(montant='0.00') {
    montant = ('' + montant).replace(',', '.');
    montant = montant.replace(/[^0-9.]/g, '');
    montant = Number.parseFloat(montant);
    montant = isNaN(montant) ? 0 : montant;
    if(Number.isInteger(montant)){return ''+montant+'.00'; }
    var montant_str = montant.toString(10);
    return /^\d+\.\d{1}$/.test(montant_str) ? montant_str + '0' : montant_str;
}

