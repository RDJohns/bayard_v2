$(function () {

    init_nsprogress();

});

function init_nsprogress(){
    $( document ).ajaxStart(function() {
        NProgress.start();
    });
    $(document).ajaxStop(function() {
        NProgress.done();
    });
}

function search_chq(){
    var societe = $("#id_societe").val();
    var etat    = $("#id_etat").val();
    var cmc     = $("#cmc-txt").val();

    var d = new Date();

    var month = d.getMonth()+1;
    var day = d.getDate();

    var aujourdhui = ((''+day).length<2 ? '0' : '') + day + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + d.getFullYear() ;

    var data = {
        societe : societe,
        etat : etat,
        cmc : cmc
    };

    $('#table-chq').DataTable({
        dom: 'lBfrtip',
        buttons: [
            /*  {
                  extend : 'excelHtml5',
                  title: 'chèques_non_reb_'+aujourdhui
              }*/

            {
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": s_url+'/cheque/vips_cheque/get_extract_excel',
                        "type": "POST",
                        "data": data,
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'chèques_ko_tri_'+aujourdhui+'.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }
        ]
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100],[5,10,25,50,100]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,"oLanguage": {
            "sLengthMenu": "Afficher _MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "Affichage de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
            ,"sProcessing":""
        }
        ,"ajax":{
            //'dataType': 'json',
            'url': s_url+'cheque/vips_cheque/get_cheque'
            ,"data": data
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //alert('test');
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },
        "initComplete": function(settings, json) {

        }
        ,'columns':[
            { "data": 'date_courrier'}
            ,{ "data": 'id_pli' }
            ,{ "data": 'pli'}
            ,{ "data": 'nom_societe'}
            ,{ "data": 'lot_scan'}
            ,{ "data": 'etape'}
            ,{ "data": 'etat'}
            ,{ "data": 'statut'}
            ,{ "data": 'numero_payeur'}
            ,{ "data": 'nom_payeur'}
            ,{ "data": 'cmc7'}
            ,{ "data": 'montant'}
            ,{ "data": 'date_init'}
            ,{ "data": 'etat_chq'}
        ]
    });
}

function change_chq(id){
    var etat = $("#chq-"+id).val();
    $.post(s_url + '/cheque/etat_cheque/change_etat_chq', {id:id,etat:etat}, function(res) {
        if(res == 'success'){
            alert('cheque id:'+id+' à jour => '+etat);
        }
    });
}

function change_montant(id){
    var mont = $("#montant-"+id);
    var montant = mont.text();
    swal({
        title: "Modification montant",
        text: "Nouveau montant:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        cancelButtonText: "Annuler",
        inputPlaceholder: "Montant..."
    }, function (inputValue) {
        if(inputValue === false) return false;
        if ((isNaN(inputValue)) || (inputValue === ""))
        {
            swal.showInputError("Il faut renseigner un montant!");
            return false;
        }
        else{
            mont.text(inputValue);
            swal.close();
        }
    });
}
