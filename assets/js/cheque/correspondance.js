$(function () {

    init_nsprogress();

});

function init_nsprogress(){
    $( document ).ajaxStart(function() {
        NProgress.start();
    });
    $(document).ajaxStop(function() {
        NProgress.done();
    });
}

function search_chq(){
    var societe = $("#id_societe").val();
    var etat    = $("#id_etat").val();
    var cmc     = $("#cmc-txt").val();

    var d = new Date();

    var month = d.getMonth()+1;
    var day = d.getDate();

    var aujourdhui = ((''+day).length<2 ? '0' : '') + day + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + d.getFullYear() ;

    var data = {
        societe : societe,
        etat : etat,
        cmc : cmc
    };

    $('#table-chq').DataTable({
        dom: 'lBfrtip',
        buttons: [
            /*  {
                  extend : 'excelHtml5',
                  title: 'chèques_non_reb_'+aujourdhui
              }*/

            {
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": s_url+'/cheque/correspondance_cheque/get_extract_excel',
                        "type": "POST",
                        "data": data,
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'chèques_ko_reb_'+aujourdhui+'.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }
        ]
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100],[5,10,25,50,100]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,"oLanguage": {
            "sLengthMenu": "Afficher _MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "Affichage de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
            ,"sProcessing":""
        }
        ,"ajax":{
            //'dataType': 'json',
            'url': s_url+'cheque/correspondance_cheque/get_cheque'
            ,"data": data
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //alert('test');
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },
        "initComplete": function(settings, json) {

        }
        ,'columns':[
            { "data": 'date_courrier'}
            ,{ "data": 'id_pli' }
            ,{ "data": 'pli'}
            ,{ "data": 'nom_societe'}
            ,{ "data": 'lot_scan'}
            ,{ "data": 'etape'}
            ,{ "data": 'etat'}
            ,{ "data": 'statut'}
            ,{ "data": 'numero_payeur'}
            ,{ "data": 'nom_payeur'}
            ,{ "data": 'cmc7'}
            ,{ "data": 'montant'}
            ,{ "data": 'date_init'}
            ,{ "data": 'etat_chq'}
            /*,{ "data": 'lib_ko'}*/
        ]
    });

    ko_adv();
}

function ko_adv(){
    var societe = $("#id_societe").val();

    var data = {
        societe : societe
    };

    $('#table-adv').DataTable({
        dom: 'lBfrtip',
        buttons: [

            /*{
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": s_url+'/cheque/correspondance_cheque/get_extract_excel',
                        "type": "POST",
                        "data": data,
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'chèques_ko_reb_'+aujourdhui+'.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }*/
        ]
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100],[5,10,25,50,100]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,"oLanguage": {
            "sLengthMenu": "Afficher _MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "Affichage de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
            ,"sProcessing":""
        }
        ,"ajax":{
            //'dataType': 'json',
            'url': s_url+'cheque/correspondance_cheque/get_advantage'
            ,"data": data
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //alert('test');
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },
        "initComplete": function(settings, json) {

        }
        ,'columns':[
            { "data": 'date_creation'}
            ,{ "data": 'nom_societe' }
            ,{ "data": 'num_pmt'}
            ,{ "data": 'montant'}
            ,{ "data": 'num_payeur'}
            ,{ "data": 'nom_payeur'}
            ,{ "data": 'statut'}
        ]
    });
}

function change_chq(id){
    var etat = $("#chq-"+id).val();
    $.post(s_url + '/cheque/etat_cheque/change_etat_chq', {id:id,etat:etat}, function(res) {
        if(res == 'success'){
            alert('cheque id:'+id+' à jour => '+etat);
        }
    });
}

function change_montant(id){
    var mont = $("#montant-"+id);
    var montant = mont.text();
    swal({
        title: "Modification montant",
        text: "Nouveau montant:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        cancelButtonText: "Annuler",
        inputPlaceholder: "Montant..."
    }, function (inputValue) {
        if(inputValue === false) return false;
        if ((isNaN(inputValue)) || (inputValue === ""))
        {
            swal.showInputError("Il faut renseigner un montant!");
            return false;
        }
        else{
            mont.text(inputValue);
            swal.close();
        }
    });
}

function change_chq_corr(id_mst,id_soc){
    var etat = $("#chq-corr-"+id_mst+"-"+id_soc).val();
    swal({
        title: "Changement statut",
        text: "Voulez-vous vraiment modifier le statut du chèque?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4CAF50",
        confirmButtonText: "Confirmer",
        cancelButtonText: "Annuler",
        closeOnConfirm: false
    }, function (isConfirm) {
        if(isConfirm){
            $.post(s_url + '/cheque/correspondance_cheque/change_statut_corr', {id_mst:id_mst,id_soc:id_soc,etat:etat}, function(reponse) {
                if(reponse != 'success'){
                    alert(reponse);
                }
                else{
                    swal("Succès!", "Modification effectuée!", "success");
                    //window.location.href = s_url+"matchage/matchage";
                }
            });
        }
        else {
            $("#chq-"+id).val();
        }
    });
    /*$.post(s_url + '/cheque/etat_cheque/change_etat_chq', {id:id,etat:etat}, function(res) {
        if(res == 'success'){
            //alert('cheque id:'+id+' à jour => '+etat);
        }
    });*/
}

function show_payeur(id_pli,id){
    $('#payeurModal #content-payeur').html('');
    $('#pli_id').html('');
    $.ajax({
        'url': s_url+'cheque/correspondance_cheque/get_payeur_info'
        ,"data": {
            id_pli : id_pli,
            id : id
        }
        ,'type': 'POST'
        ,'dataType': 'json'
        ,'success' : function (data) {
            var payeur = '';
            var societe = 1;
            $("#pli_id").append(id_pli);
            $("#pli_id_form").val(id_pli);
            $("#chq_id_form").val(id);
            $.each(data, function(index, value) {
                societe = value.societe;
                cmc7    = value.cmc7;
                montant = value.montant;
                payeur += '<h6 class="card-inside-title">Num&eacute;ro payeur</h6>';
                payeur += '<div class="form-group">'+
                    '<div class="form-line" id="numero_payeur">'+
                    '<input type="text" class="form-control num_payeur" placeholder="num payeur" id="'+value.id+'" value="'+value.numero_payeur+'">'+
                    '</div>'+
                    '</div>';
                payeur += '<h6 class="card-inside-title">Nom payeur</h6>';
                payeur += '<div class="form-group">'+
                    '<div class="form-line" id="nom_payeur">'+
                    '<input type="text" class="form-control" placeholder="nom payeur" value="'+value.nom_payeur+'" disabled>'+
                    '</div>'+
                    '</div><br>';
            });
            $("#societe-form").val(societe).change();
            $("#montant-form").val(montant).change();
            $("#cmc7-form").val(cmc7).change();
            $('#payeurModal #content-payeur').append(payeur);
        }
    });
    $("#payeurModal").modal("show");
}

function save_modif_payeur(){
    swal({
        title: "Modification informations chèque",
        text: "Voulez-vous vraiment apporter des modifications sur le chèque?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4CAF50",
        confirmButtonText: "Confirmer",
        cancelButtonText: "Annuler",
        closeOnConfirm: false
    }, function (isConfirm) {
        if(isConfirm){
            var num = {};
            var id_pli  = $("#pli_id_form").val();
            var id      = $("#chq_id_form").val();
            var societe = $("#societe-form").val();
            var montant = $("#montant-form").val();

            if(isNaN(montant)){
                swal.close();
                showNotification('bg-red','Le montant n\'est pas de type num\351rique!','top','right',null,null);
                return false;
            }

            $( ".num_payeur" ).map(function() {
                num[$( this ).attr('id')] = $( this ).val();
            });

            $.post(s_url+'cheque/correspondance_cheque/update_payeur',{id_pli : id_pli, num_payeur : num,societe : societe,id_chq: id, montant : montant},function (data) {
                if(data == 'ok'){
                    $("#payeurModal").modal("hide");
                    swal.close();
                    showNotification('bg-green','Informations du pli #'+ id_pli +' modifiées avec succès','top','right',null,null);
                }
                else{
                    swal.close();
                    showNotification('bg-red','Une erreur s\'est produite au cours de l\'enregistrement','top','right',null,null);
                }
            });
            search_chq();
        }
    });

}
