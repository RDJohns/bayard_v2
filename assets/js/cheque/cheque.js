$(function () {

    init_nsprogress();

});

function init_nsprogress(){
    $( document ).ajaxStart(function() {
        NProgress.start();
    });
    $(document).ajaxStop(function() {
        NProgress.done();
    });
}

function search_chq(){
    var societe = $("#id_societe").val();
    var etat    = $("#id_etat").val();
    var cmc     = $("#cmc-txt").val();

    var data = {
        societe : societe,
        etat : etat,
        cmc : cmc
    };

    $('#table-chq').DataTable({
        dom: 'lBfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100],[5,10,25,50,100]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": false
        ,"oLanguage": {
            "sLengthMenu": "Afficher _MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "Affichage de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
            ,"sProcessing":""
        }
        ,"ajax":{
            //'dataType': 'json',
            'url': s_url+'cheque/etat_cheque/get_cheque'
            ,"data": data
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //alert('test');
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },
        "initComplete": function(settings, json) {

        }
        ,'columns':[
            { "data": 'id_pli' }
            ,{ "data": 'nom_societe'}
            ,{ "data": 'lot_scan'}
            ,{ "data": 'cmc7'}
            ,{ "data": 'montant'}
            ,{ "data": 'etat'}
        ]
    });
}

function change_chq(id,etat_i,id_pli,id_doc,id_saisie){
    var etat = $("#chq-"+id).val();
    swal({
        title: "Changement statut",
        text: "Voulez-vous vraiment modifier le statut du chèque?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4CAF50",
        confirmButtonText: "Confirmer",
        cancelButtonText: "Annuler",
        closeOnConfirm: false
    }, function (isConfirm) {
        if(isConfirm){
            $.post(s_url + '/cheque/etat_cheque/change_etat_chq', {id:id,etat:etat,id_pli:id_pli,id_doc:id_doc,id_saisie:id_saisie}, function(reponse) {
                if(reponse != 'success'){
                    alert(reponse);
                }
                else{
                    swal("Succès!", "Modification effectuée!", "success");
                    //window.location.href = s_url+"matchage/matchage";
                }
            });
        }
        else {
            $("#chq-"+id).val(etat_i);
        }
    });
    /*$.post(s_url + '/cheque/etat_cheque/change_etat_chq', {id:id,etat:etat}, function(res) {
        if(res == 'success'){
            //alert('cheque id:'+id+' à jour => '+etat);
        }
    });*/
}