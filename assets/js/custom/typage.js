var listeModePaiement = ["mode_paiement_0"];
var docPli            = ""; /*pour copier les images de tous les docs*/
var idPli             = 0;
var listeIDdoc        = [0];
var typeKo            = 0;
var idMotif            = 0;

$(function () {
    /* Pour le item raccourci du menu */
    $('.visible_menu').show();
    //data-id="infoTitre"
   
    
    
    $.AdminBSB.dropdownMenu.activate();
    $.AdminBSB.input.activate();
    $.AdminBSB.select.activate();
    init_nsprogress();
    
   
   /* Fermer modal jquery-magnify => touche ESC */
   var keys = {};
   onkeydown = onkeyup = function(e) {
       e = e || event;
       e.which = e.which || e.keyCode;
       keys[e.which] = e.type === 'keydown';
       if (keys[17] && keys[13]) { //ctrl+Enter: go
        
           
           if ($('[onclick="chargerPli();"]').length == 1) {
            chargerPli();
        } else if ($('[onclick="validerPli();"]').length == 1) {
            validerPli();
            e.preventDefault();
            e.stopPropagation();
        }
       }
       if (keys[17] && keys[66]) {
        
        $('#choixSociete').val(1).change();
        
           
       }
       if (keys[17] && keys[77]) {
        
        $('#choixSociete').val(2).change();
        
           
       }
       if (keys[17] && keys[8]) { //ctrl+back: annuler traitement
        
        if ($('[onclick="annulerPli();"]').length == 1) {
            
         annulerPli();
         e.preventDefault();
         e.stopPropagation();
        }
    }

       if (keys[18] && keys[82]) { //al+r 
        $("#raccourci-modal").modal("show");
        
         e.preventDefault();
         e.stopPropagation();
        
    }
    
    /*
       if (keys[18] && keys[72]) { //alt+h: hors périm
        
            if ($('[onclick="hpPli();"]').length == 1) {
                hpPli();
                e.preventDefault();
                e.stopPropagation();
            }
        }
        if (keys[18] && keys[75]) { //alt+k: ko
           
            if ($('[onclick="KOscanPli();"]').length == 1) {
                alert("123");
                KOscanPli();
                $("#motifRejet").focus();
                e.preventDefault();
                e.stopPropagation();
            }
        }

        */

       
    
       if (keys[17] && keys[16]) { //ctrl+Shft: afficher apercu document
           if ($('.img_b64').length > 0) {
               $($('.img_b64')[0]).trigger('click');
           }
       }
       /*if (keys[9]) { //ctrl+Shft: afficher apercu document
        $('.bootstrap-select').each( function(){
             console.log($(this)); // $(this) représente l'objet courant
        } );
        
       }*/
        
       if (keys[18] && keys[16] && keys[82]) { //alt+Shft+R: retourner document
           var is_magnify = false;
           if ($('.magnify-foot-toolbar .magnify-button-next').length > 0) {
               $('.magnify-foot-toolbar .magnify-button-next').trigger('click');
               is_magnify = true;
           }
           
       }
       
       if (keys[27]) { //Echap: fermer apercu document
           if ($('.magnify-head-toolbar .magnify-button-close').length > 0) {
               $('.magnify-head-toolbar .magnify-button-close').trigger('click');
           }
           if ($('.modal.in a.close').length > 0) {
               $('.modal.in a.close').trigger('click');
           }
       }
       if (keys[18] && keys[107]) { //alt++: zoom apercu
           if ($('.magnify-foot-toolbar .magnify-button-zoom-in').length > 0) {
               $('.magnify-foot-toolbar .magnify-button-zoom-in').trigger('click');
           }
       }
       if (keys[18] && keys[109]) { //alt+-: de-zoom apercu
           if ($('.magnify-foot-toolbar .magnify-button-zoom-in').length > 0) {
               $('.magnify-foot-toolbar .magnify-button-zoom-out').trigger('click');
           }
       }
   }

    /*form validation jquery pour toutes les forms*/
    $('form').each(function () {
        $(this).validate({
            highlight: function (input) {
                $(input).parents('.form-line').removeClass('focused success');
                $(input).parents('.form-line').addClass('focused error');
                $.AdminBSB.input.activate();
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('focused error');
                $.AdminBSB.input.activate();
                },
            rules: {
                    cpPayeurName: {maxlength: 3},
                    numPayeurName: {maxlength: 12,digits: true},
                    nomPayeurName: {maxlength : 30},
                    cPaysPayeurName: {maxlength : 30},
                    decriptionRejetName: {required: true},
                    nbAbonnementName: {required : true,digits: true,min:1}
            },
            onfocusout: function(element) {
                this.element(element);
            }
        });
    });

    var redirectInfo = getParameterByName('t'); 
    if(parseInt(redirectInfo)== 1 )
    {
        chargerPli();
    }
    
    
});
/*********FCT******************/

function getParameterByName(name) {
    var url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
function chargerPli()
{
    $(".preloader").show();
    
    $(".charger").attr("disabled","disabled");
    var choixSociete = $("#choixSociete").val();
    $.ajax({
        url : s_url+'typage/Typage/loadPli/'+choixSociete, /*maka pli ho an le current user*/
        type : 'POST',
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(ret, statut){
            /* console.log(ret); */


            var id_pli = parseInt(ret.id_pli);
            var retour = ret.retour;

            if(id_pli > 0 )
            {
                chargerSonPli();
            }
            else
            {
                if(retour.trim() == "encours_existe")
                {
                    chargerSonPli();
                }
                else if(retour.trim() == "ko")
                {
                    $(".preloader").hide();
                    $(".charger").removeAttr("disabled");
                }
                else if(retour.trim() == "pas_pli")
                {
                    $(".preloader").hide();
                    $(".image-set").html("<center><h4><span class=\"label label-danger\">Pas de pli à typer ! </span></h4></center>");
                    $(".charger").removeAttr("disabled");
                }
            }


            $(function () {
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.input.activate();
                $.AdminBSB.select.activate();
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
            });

        },

        error : function(resultat, statut, erreur){

            $("#boutonPli").hide();
            $(".preloader").hide();
        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function chargerSonPli() {
    $(".preloader").show();

    $(".arborescence").html("");
    $(".image-set").html("");

    $("#doc-recto").html("");
    $("#doc-verso").html("");



    $.ajax({
        url : s_url+'typage/Typage/recuperPliUtilisateur',
        type : 'POST',
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(ret, statut){
			
			/* console.log(ret); */
            if(parseInt(ret.error) == 1)
            {
                $(".image-set").html("<center><h1><span class=\"label label-danger\">Pas de pli à typer ! </span></h1></center>");

                $(".charger").removeAttr("disabled");
                
                
            }
            else
            {
                $(".arborescence").html("");
                $(".arborescence").html(ret.magn);
                $(".image-set").html("");
                $(".tab-document").hide();
                docPli  = ret.docu; /*on copie les images; utile lors "affiche/ cacher" click */
                $(".image-set").html(docPli);
                listeIDdoc =  ret.id_document;
                $('.metisFolder').metisMenu({toggle: true});


                $("#boutonPli").show();

				

                getPreDataChq(ret.idPli);
                
                 setPredataTypage(ret.idPli);

                /*$(".pli_rejet").append(code_html); */
                
				$('#infoSociete').val(ret.societe).change();
				
				getConsignePli(ret.idPli);

                /* console.log(listeIDdoc); */

                $(function () {
                    $.AdminBSB.dropdownMenu.activate();
                    $.AdminBSB.input.activate();
                    $.AdminBSB.select.activate();
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
                    $('[data-magnify=gallery]').magnify({
                        multiInstances: false
                        ,footToolbar: ['prev','zoomIn','zoomOut','fullscreen','rotateRight','rotateLeft','next']
                        ,initMaximized: true
                    });


                });
                $("").empty();    
                $(".charger").attr("onclick", "null");
                $(".charger").hide();
                $(".annuler-koscan").show();
                $("#boutonPli").show();

               
            }

        },

        error : function(resultat, statut, erreur){
            $("#boutonPli").hide();
            $(".preloader").hide();
        },

        complete : function(resultat, statut){
            $(".preloader").hide();


        }

    });
}

function getConsignePli(idPli)
{
    $("#titre-consige-typage").text("");
    $(".content-modal-consigne").html(''); 
    $.ajax(
        {
          url : s_url+"typage/Typage/getConsigne/"+idPli,
          type :"POST",
		  dataType:"json",
          success : function(r,e){
              
			if(parseInt(r.ok) == 1)
			{
                $(".text-consigne").html("");
                $(".img-typage-consigne").hide();
                $(".consigen-pli").hide();
                var consigneClient = r.consigne_client;
                 idMotif = r.id_motif;
                if(consigneClient.trim() != "")
                {
                    $(".text-consigne").html("Consigne pour le traitement du pli #"+idPli +" : "+r.consigne_client );
                    $(".consigen-pli").show();
                    $("#titre-consige-typage").text("Merci de lire la consigne pour le traitement du pli #"+idPli);
                    $(".content-modal-consigne").html('<blockquote class="m-b-25">'+consigneClient+'</blockquote>');
                    $("#modal-consigne").modal('show');
                    
                    
                }
                
                if(parseInt(r.nb_consigne) > 1 )
                {
                    $(".img-typage-consigne").show();
                   
                }
				
			}	
            
          }
        }
      )
}

function annulerPli()
{
    var strPliID    = $(".c-pli-id").text();
        idPli       = Number(strPliID);

    if(idPli > 0)
    {
        swal({
            title: "Confirmation",
            text: "Voulez-vous annuler le typage de ce pli ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ff0000",
            confirmButtonText: "OUI",
            cancelButtonText: "Fermer",
            showLoaderOnConfirm: true,
            closeOnConfirm: true
        }, function () {
            $.ajax({ /*Verification si Pli en standBY */
                url : s_url+'typage/Typage/checkIfStandBy/'+idPli,
                type : 'POST',
                dataType : 'json',
                beforeSend: function() {
                    GedGoToPageLogin();
                },
                success : function(resultat, statut){
                    if(resultat !== 0)
                    {
                        alert("Ce pli #"+idPli+" est en stand by!");
                        location.reload();
                    }
                    else
                    {
                        $.ajax({
                            url : s_url+'typage/Typage/annulerTypage/'+idPli,
                            type : 'POST',
                            dataType : 'json',
                            beforeSend: function() {
                                GedGoToPageLogin();
                            },
                            success : function(resultat, statut){
                                if(parseInt(resultat.update) == 1)
                                {
                                    /*location.reload();*/
                                    window.location.replace(s_url+'typage/Typage/');
                                }
                                else
                                {
                                    swal("Erreur !", "Erreur de serveur", "error");
                                }
                            },
                            error : function(resultat, statut, erreur){
                                swal("Erreur !", "Erreur de serveur", "error");
                            }
                        });

                    }
                },
                error : function(resultat, statut, erreur){
                    swal("Erreur !", "Erreur de serveur", "error");
                }
            });
        });
    }
    else
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de charger un pli '},{type: 'danger'});
    }
    
}
function rejeterPli(){
    
    var motifRejet  = $("#motifRejet").val();
    var description = "";
    var strTempDescription = "";
    if(motifRejet == 0 || motifRejet == null)
    {
        $("#txt-error").text("");
        $("#txt-error").text(" Motif KO obligatoire");
        $(".c-error").show().delay(2000).fadeOut();
        return 0;
    }
    
    var strTempMotifRejet = $( "#motifRejet option:selected" ).text();
    
    var strMotifRejet = $("#motifRejet :selected").text();
        strMotifRejet = strMotifRejet.toLowerCase();

    if(strMotifRejet.indexOf("autre") > -1)
    {
        description = $("#ko-scan-description").val();
        description = description.trim();

        if(description == "" || description == null)
        {
            $("#txt-error").text("");
            $("#txt-error").text(" Merci d'ajouter une description");
            $(".c-error").show().delay(2000).fadeOut();
            return 0;
        }
        else 
        {
             strTempDescription = " > : "+description ;
        }
    }
    
    var motifOperateur = strTempMotifRejet+" "+strTempDescription;
    
    var postData = {idPli:idPli,motifRejet:motifRejet,description:description,motifOperateur:motifOperateur};
    /*console.log(postData);
    return 0;return false;
    return 0;*/
    swal({
        title: "Confirmation",
        text: "Voulez-vous rejeter ce pli ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#ff0000",
        confirmButtonText: "OUI",
        cancelButtonText: "Fermer",
        showLoaderOnConfirm: true,
        closeOnConfirm: true
    }, function () {
        $.ajax({
            url : s_url+'typage/Typage/koScan/',
            type : 'POST',
            data:postData,
            dataType : 'json',
            beforeSend: function() {
                GedGoToPageLogin();
            },
            success : function(resultat, statut){


                if(parseInt(resultat.update) == 1)
                {
                   
					
					 $(newFunction()).modal("hide");
                        location.reload();

                }
                else
                {
                    swal("Erreur !", "Erreur de serveur", "error");
                }
            },
            error : function(resultat, statut, erreur){
                swal("Erreur !", "Erreur de serveur", "error");
            }
        });
    });

    function newFunction() {
        return "#ko-scan-rejet-pli";
    }
}
function KOscanPli()
{
    var strPliID    = $(".c-pli-id").text();
    idPli           = parseInt(strPliID);
    $("#ko-scan-rejet-pli-id").text("");
    $("#ko-scan-description").val("");
    /*$("#ko-scan-rejet-pli").modal("show");*/

    if(idPli > 0)
    {
        $.ajax({ /*Verification si Pli en standBY */
            url : s_url+'typage/Typage/checkIfStandBy/'+idPli,
            type : 'POST',
            dataType : 'json',
            beforeSend: function() {
                GedGoToPageLogin();
            },
            success : function(resultat, statut){
                if(resultat !== 0)
                {
                   alert("Ce pli #"+idPli+" est en stand by!");
                }
                else
                {
                    $("#ko-scan-rejet-pli-id").text(idPli);
                    $("#ko-scan-rejet-pli").modal("show");
                }
            },
            error : function(resultat, statut, erreur){
                swal("Erreur !", "Erreur de serveur", "error");
            }
        });

    }
    else
    {
        return 0;
    }
}

/*
function ajouterCheque()
{
    var nbCheque = $("#nbrCheque").val();
    $("#nbrCheque").val(parseInt($("#nbrCheque").val(),10)+1);
    var nbCheque = $("#nbrCheque").val();

        $.ajax({
            url : s_url+'typage/Typage/dynamiqueCheque/'+nbCheque,
            type : 'POST',
            dataType : 'html',
            beforeSend: function() {
                GedGoToPageLogin();
            },
            success : function(code_html, statut){



                $(".pli_rejet").append(code_html);
                $("#nbAbonment").click();



            },

            error : function(resultat, statut, erreur){

            },

            complete : function(resultat, statut){

            }

        });
}*/
function supprimerCheque(classDiv) {
    $("div."+classDiv).remove();
}


function getSociete() {
    var infoSociete = $("#infoSociete").val();
    $.ajax({
        url : s_url+'typage/Typage/listeTitre/'+infoSociete,
        type : 'POST',
        dataType : 'html',
        beforeSend: function() {
            GedGoToPageLogin();

         },
        success : function(code_html, statut){
            $(".ctitrePli").html('');
            $(".ctitrePli").append(code_html);
            $(function () {
                $.AdminBSB.browser.activate();
                $.AdminBSB.leftSideBar.activate();
                $.AdminBSB.rightSideBar.activate();
                $.AdminBSB.navbar.activate();
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.input.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.search.activate();
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
            });

        },

        error : function(resultat, statut, erreur){

            //console.log(resultat);
        },

        complete : function(resultat, statut){
            //console.log(resultat);
        }

    });

    $.ajax({
        url : s_url+'typage/Typage/listeTypologie/'+infoSociete,
        type : 'POST',
        dataType : 'html',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(code_html, statut){

            $(".c-typo-pli").html('');
            $(".c-typo-pli").append(code_html);
            $(function () {
                $.AdminBSB.browser.activate();
                $.AdminBSB.leftSideBar.activate();
                $.AdminBSB.rightSideBar.activate();
                $.AdminBSB.navbar.activate();
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.input.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.search.activate();
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
            });
            
        }, 

        error : function(resultat, statut, erreur){

            //console.log(resultat);
        },

        complete : function(resultat, statut){
            //console.log(resultat);
        }

    });
/*

    $.ajax({
        url : s_url+'typage/Typage/listeProvenance/'+infoSociete,
        type : 'POST',
        dataType : 'html',
        beforeSend: function() {
            GedGoToPageLogin();

         },
        success : function(code_html, statut){
            $(".c-provenance").html('');
            $(".c-provenance").append(code_html);
            $(function () {
                $.AdminBSB.browser.activate();
                $.AdminBSB.leftSideBar.activate();
                $.AdminBSB.rightSideBar.activate();
                $.AdminBSB.navbar.activate();
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.input.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.search.activate();
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
            });
            $('[data-toggle="tooltip"]').tooltip();
        },

        error : function(resultat, statut, erreur){

            
        },

        complete : function(resultat, statut){
            
        }

    });*/

} 
/**
 * todo Tester si vient de KOSCAN
 * @param {*} koScan 
 */
function verifDescription(koScan = 1) {
    var classSel = "cDecriptionRejet";
    if(koScan == 1)
    {
        var motifRejet = $("#motifRejet :selected").text();
    }
    else
    {
        var motifRejet = $("#motifAnomalie :selected").text();
        classSel = "cDecriptionRejet1";
    }
   
    /*motifAnomalie*/
        motifRejet = motifRejet.toLowerCase();
        if(motifRejet.indexOf("autre") > -1)
        {
            $("."+classSel).show();
            
        }
        else
        {
            $("."+classSel).hide();
        }

}
function verifModePaiement(classDiv,cmc7) {

    //alert(classDiv);
    var idCheque    = 0;
    var listeId     = [];
    $.each($("."+classDiv+" option:selected"), function(){

        var idSelect = $(this).val();
        listeId.push(idSelect);

        if ($('.mode_paiement_1').length > 0) { /*init champ cheque*/
            idSelect = 0;

        }

        if(idSelect == 1)
        {

            idCheque = 1;
            console.log("listeIDdoc");
            console.log(listeIDdoc);
            var postData  = {classDiv:classDiv,cmc7:cmc7,idDoc:listeIDdoc};
            $.ajax({
                url : s_url+'typage/Typage/dynamiqueCheque',
                type : 'POST',
                data: postData,
                dataType : 'html',
                beforeSend: function() {
                    GedGoToPageLogin();
                },
                success : function(code_html, statut){



                    $(".pli_rejet").append(code_html);
                    $(function () {
                        $.AdminBSB.browser.activate();
                        $.AdminBSB.dropdownMenu.activate();
                        $.AdminBSB.input.activate();
                        $.AdminBSB.select.activate();
                        setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
                    });

                },

                error : function(resultat, statut, erreur){

                    //console.log(resultat);
                },

                complete : function(resultat, statut){
                  //  console.log(resultat);
                }

            });


        }
    });

    if(listeId.indexOf("1") < 0) /*suppr toutes les chq si pas de select chq*/
    {
        $(".dynamique_cheque").remove();
    }


}

function ajouterCheque(classDiv,cmc7,doc) {

    if(cmc7 == 'null'  || cmc7 == undefined)
    {
        cmc7 = "";
    }

    var postData  = {classDiv:classDiv,cmc7:cmc7,infoDoc:doc,idDoc:listeIDdoc};
    $.ajax({
        url : s_url+'typage/Typage/dynamiqueCheque',
        type : 'POST',
        data:postData,
        dataType : 'html',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(code_html, statut){



            $(".pli_rejet").append(code_html);
            $(".addCheque."+classDiv).hide();
            $(function () {
                $.AdminBSB.browser.activate();
                $.AdminBSB.leftSideBar.activate();
                $.AdminBSB.rightSideBar.activate();
                $.AdminBSB.navbar.activate();
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.input.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.search.activate();
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
            });

        },

        error : function(resultat, statut, erreur){

            /* console.log(resultat); */
        },

        complete : function(resultat, statut){
            /* console.log(resultat); */
        }

    });
}
function supprimerCheque(classDiv) {

    var rangCheque = classDiv.split("_");
    if(rangCheque[2] > 1 )
    {

        var rangChequePrecedant = rangCheque[2] - 1 ;
        var classPrecedant = "mode_paiement_"+rangChequePrecedant;
        $(".addCheque."+classPrecedant).show();

    }
    $("."+classDiv).remove();
}


function checkCmc7(elCmc7,elmRlmc)
{
    var typedValue = $('#'+elCmc7).val().toString() + $('#'+elmRlmc).val().toString();
	 /* console.log(typedValue); */
	typedValue = typedValue.replace(/\s/g, "");
	if(!typedValue){
	return true;
	}
	
	var lngBuff=0;
	
	
	for (var i = 0 ;i < typedValue.length;i++)
	lngBuff = parseInt(lngBuff.toString() + typedValue[i]) % 97;


	if (lngBuff == 0){
	
	return true;
	}
	else {
	
	return false;
	}
}


function validerPli() {
    var listeCheque     = [];
    var autreInfo       = [];
    var listeAnomalieChq = [];

    var strPliID        = $(".c-pli-id").text();
        idPli           = parseInt(strPliID);

    if(strPliID == 0 || strPliID == null || strPliID.trim() == "" || strPliID == undefined)
    {
        return 0;
    }
    var infoSociete     = $("#infoSociete").val();
    var infoTitre       = $("#infoTitre").val();
    var typologie       = $("#typologie").val();
    var modPaie         = $("#mode-paiement").val();
    var infoDatamatrix  = $("#infoDatamatrix").val();
    var typologie       = $("#typologie").val();
    var typeCoupon      = $("#typeCoupon").val();
    var test            = 0;
    
    

    var codeEcoleGci    = $("#code-ecole-gci").val();

   
    
    if(codeEcoleGci == null || codeEcoleGci == undefined || codeEcoleGci == "undefined" || codeEcoleGci.trim() == "")
    {
        
		if(typologie == 8 || typologie == 14 || typologie == 15 || typologie == 17 || typologie == 18 )
		{
			notifUSer("Code école/GCI  est obligatoire");
			return 0;
		}
		
        codeEcoleGci = "";
    }
    
    
    if(parseInt(infoSociete) == 0 || infoSociete == null)
    {
        notifUSer("Choisir une socièté");
        $("#infoSociete").parents('.form-line').addClass('error');
        $( ".split_right" ).scrollTop(1);
        return 0;
    }
    if(parseInt(infoTitre) == 0 || infoTitre == null)
    {
        notifUSer("Choisir le titre du pli");
        $("#infoTitre").parents('.form-line').addClass('error');
        $( ".split_right" ).scrollTop(1);
        return 0;
    }

    if(parseInt(typologie) == 0 || typologie == null)
    {
        notifUSer("Choisir la typologie du pli");
        $("#typologie").parents('.form-line').addClass('error');
        $( ".split_right" ).scrollTop(1);
        return 0;
    }

    if(parseInt(modPaie) == 0 || modPaie == null)
    {
        notifUSer("Choisir le mode de paiement");
        $("#mode-paiement").parents('.form-line').addClass('error');
        return 0;
    }
    if(parseInt(nbAbonment) == 0 || nbAbonment == null)
    {
        notifUSer("Erreur sur le nombre d'abonnement");
        $("#nbAbonment").parents('.form-line').addClass('error');
        return 0;
    }
    
  var mouvement = [];
  
  $(".dynamique-bloc-payeur").each(function(index,item){
    
    var rangBloc = parseInt(index) + 1 ;
    var numeroPayeur = $("#numeropayeur-"+rangBloc).val();
    var numeroAbonne = $("#numeroabonne-"+rangBloc).val();
    var codePromo = $("#promo-"+rangBloc).val();
    var nomPayeur = $("#nompayeur-"+rangBloc).val();
    
    var bufferMouvement = {"id_pli":idPli,"titre":infoTitre,"numero_abonne":numeroAbonne,"numero_payeur":numeroPayeur,"nom_payeur":nomPayeur,"code_promo":codePromo};
        mouvement.push(bufferMouvement);

  });
  
    

   

    var arrayCmc7    = [];
     
       $('.dynamique_cheque').each(function(index,item){
          
           var className   = $(this).attr('class');
           var rang        = className.split("mode_paiement_");
           var rangCheque  = parseInt(rang[1]);

           var montant      = $("#chMontant_"+rangCheque).val();
           montant          = montant.trim();
           var cmc7         = $("#chCmc7_"+rangCheque).val();
           cmc7             = cmc7.trim();

           var rlmc         = $("#chRlmc_"+rangCheque).val();
           rlmc             = rlmc.trim();

           var anomalie     = $("#chAnomalie_"+rangCheque).val();

           

           var documentID   = $("#chqDoc_"+rangCheque).val();

           
          
           if(documentID <= 0 || documentID == null || documentID == "" || documentID == undefined)
           {
            infoInModal(rangCheque, " ID document ");
            test   = 1;
            return 0;
           }


          if(montant == "" || parseFloat(montant) == 0)
           {
               infoInModal(rangCheque, " Montant");
               test   = 1;
               return 0;
           }

           
           var countStr = charCount(montant); //xx.x

           if(countStr > 1 )
           {
                 montant = montant.slice('.', -1); 
           }

           if(cmc7 == "" || cmc7 == null)
           {
               infoInModal(rangCheque, " CMC7");
               test   = 1;
               return 0;
           }
           var numbers = /^[-+]?[0-9]+$/;
           if(!cmc7.match(numbers))
           {
                infoInModal(rangCheque, " CMC7 (Caractère invalide )");
                test   = 1;
                return 0;
           }

           if(cmc7.length != 31 )
           {
               infoInModal(rangCheque, " CMC7 (Nombre de caractère invalide )");
               test   = 1;
               return 0;
           }

           if(rlmc.length != 2 )
           {
               infoInModal(rangCheque, " RLMC (Nombre de caractère invalide )");
               test   = 1;
               return 0;
           }

           if(rlmc == "" || rlmc == null)
           {
               infoInModal(rangCheque, " RLMC");
               test   = 1;
               return 0;
           }


           if(anomalie <= 0 || anomalie == null || anomalie == "" || anomalie == undefined | anomalie == "null")
           {
                anomalie = [0];
           }

           var checkChq = checkCmc7("chCmc7_"+rangCheque,"chRlmc_"+rangCheque);
           if(!checkChq)
           {    
             infoInModal(rangCheque, " CMC7 ou RLMC");
             test   = 1;
             return 0;
           }
           checkChq = false;

           if(arrayCmc7.includes(cmc7))
           {
                infoInModal(rangCheque, " RLMC ("+cmc7+" ) existe déjà");
                test   = 1;
                return 0;
           }
           else
           {
                arrayCmc7.push(cmc7);
           }
           
           

          listeCheque.push({cmc7:cmc7,montant:montant,rlmc: rlmc,id_pli:idPli,id_doc:documentID});

          listeAnomalieChq.push({id_pli:idPli,id_document:documentID,anomalie_cheque_id:anomalie}) ;


       });
    
    autreInfo = {infoSociete:infoSociete,infoTitre:infoTitre,typologie:typologie,
                 modPaie:modPaie,infoDatamatrix:infoDatamatrix,
                 typeCoupon:typeCoupon,idPli:idPli,codeEcoleGci:codeEcoleGci
                 };
    


    console.log(autreInfo);

if(parseInt(test) == 0 )
{

    swal({
        title: "CONFIRMATION",
        text: "Vous voulez-vous vraiment valider ce pli ?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: true
    }, function () {
        var postData  = {autreInfo:autreInfo,listeCheque:listeCheque,listeAnomalieChq:listeAnomalieChq,mouvement:mouvement};
        $.ajax({ /*Verification si Pli en standBY */
            url : s_url+'typage/Typage/checkIfStandBy/'+idPli,
            type : 'POST',
            dataType : 'json',
            beforeSend: function() {
                GedGoToPageLogin();
            },
            success : function(resultat, statut){
                if(resultat !== 0)
                {
                    alert("Ce pli #"+idPli+" est en stand by!");
                    location.reload();
                }
                else
                {
                   $.ajax({
                        url : s_url+'typage/Typage/validerTypage/',
                        type : 'POST',
                        data: postData,
                        dataType : 'json',
                        beforeSend: function() {
                            GedGoToPageLogin();
                        },
                        success : function(resultat, statut){
                            if(resultat.statut == "OK")
                            {
                                goTO();
                            }
                            else
                            {
                                swal("Erreur !",resultat.comment, "error");
                            }
                        },
                        error : function(resultat, statut, erreur){
                            swal("Erreur !", "Erreur de serveur"+erreur, "error");
                        }
                    });

                }
            },
            error : function(resultat, statut, erreur){
                swal("Erreur !", "Erreur de serveur", "error");
            }
        });
    });

}
    return 0;


}

function goTO()
{   
    window.location.replace(s_url+'typage/Typage/?t=1');
}
function infoInModal(numCheque, champ) {
    var strErreurCheque = "";
        strErreurCheque += "Erreur sur le <span class=\"badge bg-blue\">Chèque #"+numCheque+"</span><br/>";
        strErreurCheque += "dans le champ : "+champ+"";
        $(".modal-cheque").html("");
        $(".modal-cheque").html(strErreurCheque);
      $("#id-modal-cheque").modal('show');
}
function modalAnomalieChq(idModal)
{
    $("#"+idModal).modal('show');

}
function afficheLesDocs() {

    if($('.alldoc:visible').length) /*alana iz reettr*/
    {
        $(".alldoc").fadeOut("slow");
        $(".image-set").html("");
        viderTab();
    }
    else
    {
        $("#doc-recto").html("");
        $("#doc-verso").html("");

        $(".image-set").html("");
        $(".image-set").html(docPli);
        $('[data-magnify=gallery]').magnify({
            multiInstances: false
            ,footToolbar: ['prev','zoomIn','zoomOut','fullscreen','rotateRight','rotateLeft','next']
            ,initMaximized: true
       });
        $(".magnify-maximize").css('width','70% !important');

        viderTab();

    }


}
function afficheDocID(id) {

     $(".image-set").html(""); /*vider le contenu du tous les docs pour eviter de voir des images n'appartient pas au ID doc*/
    $(".tab-document").show();

    /*vider le content*/
    $("#doc-recto").html("");
    $("#doc-verso").html("");

    $.ajax({
        url : s_url+'typage/Typage/documentParID/'+id,
        type : 'POST',
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(resultat, statut){



            /* console.log(resultat); */
            var imgRecto	   = "";
            var imgReVerso	   = "";
                imgRecto       +='<a  class="magni" data-magnify="gallery" data-caption="'+resultat.nomRecto+'" href="data:image/jpeg;base64,'+resultat.recto64+'">';
                imgRecto       +='<img style="width: 75% !important;" class="" src="data:image/jpeg;base64,'+resultat.recto64+'" alt="">';
                imgRecto       +='</a>';


              imgReVerso       +='<a  class="magni" data-magnify="gallery" data-caption="'+resultat.nomVerso+'" href="data:image/jpeg;base64,'+resultat.verso64+'">';
              imgReVerso       +='<img style="width: 75% !important;" class="" src="data:image/jpeg;base64,'+resultat.verso64+'" alt="">';
              imgReVerso       +='</a>';

            $("#doc-recto").html(imgRecto);
            $("#doc-verso").html(imgReVerso);

            $(function () {
               /* $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.input.activate();
                $.AdminBSB.select.activate();*/
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
            });

        },

        error : function(resultat, statut, erreur){

            /* console.log(resultat); */
        },

        complete : function(resultat, statut){
            $('[data-magnify=gallery]').magnify({
                multiInstances: false
                ,footToolbar: ['prev','zoomIn','zoomOut','fullscreen','rotateRight','rotateLeft','next']
                ,initMaximized: true
            });
        }

    });


}
function viderTab() {
    $("#doc-recto").html("");
    $("#doc-verso").html("");
    $(".tab-document").hide();
}




function hpPli() {

    var strPliID    = $(".c-pli-id").text();
    idPli           = Number(strPliID);
    if(idPli > 0)
    {
        swal({
            title: "CONFIRMATION",
            text: "Pli hors périmètre ?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Annuler!",
            showLoaderOnConfirm: true,
            closeOnConfirm: true
        }, function () {
           /*************/
                $.ajax({ /*Verification si Pli en standBY */
                    url : s_url+'typage/Typage/checkIfStandBy/'+idPli,
                    type : 'POST',
                    dataType : 'json',
                    beforeSend: function() {
                        GedGoToPageLogin();
                    },
                    success : function(resultat, statut){
                        if(resultat !== 0)
                        {
                            location.reload();
                        }
                        else
                        {
                           $.ajax({
                                url : s_url+'typage/Typage/hpTypage/'+idPli,
                                type : 'POST',
                                dataType : 'json',
                                beforeSend: function() {
                                    GedGoToPageLogin();
                                },
                                success : function(resultat, statut){
                                    if(parseInt(resultat.update) == 1)
                                    {
                                        location.reload();
                                    }
                                    else
                                    {
                                        swal("Erreur !", "Erreur de serveur", "error");
                                    }
                                },
                                error : function(resultat, statut, erreur){
                                    swal("Erreur !", "Erreur de serveur", "error");
                                }
                            });


                        }
                    },
                    error : function(resultat, statut, erreur){
                        swal("Erreur !", "Erreur de serveur", "error");
                    }
                });
            /***********/


        });
    }
    else
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de charger un pli '},{type: 'danger'});
    }
    
}

function KOinconnuTypage ()
{
    var strPliID    = $(".c-pli-id").text();
    idPli           = Number(strPliID);
    if(idPli > 0)
    {
       /* swal({
            title: "CONFIRMATION",
            text: "Voulez-vous vraiment mettre le pli en KO inconnu ?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Annuler!",
            showLoaderOnConfirm: true,
            closeOnConfirm: true
        }, function () {
          
                $.ajax({ 
                    url : s_url+'typage/Typage/checkIfStandBy/'+idPli,
                    type : 'POST',
                    dataType : 'json',
                    beforeSend: function() {
                        GedGoToPageLogin();
                    },
                    success : function(resultat, statut){
                        if(resultat !== 0)
                        {
                            location.reload();
                        }
                        else
                        {
                             pliKo(4);
                        }
                    },
                    error : function(resultat, statut, erreur){
                        swal("Erreur !", "Erreur de serveur", "error");
                    }
                });
            


        });*/
        
        typeKo = 4;
        $("#pli-ko-type").text(" KO inconnu ");
        $("#ko-pli-id").text(idPli);
        $("#ko-motif-rejet-pli").modal("show");
        $("#ko-motif-description").val("");
        
        
    }
    else
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de charger un pli '},{type: 'danger'});
    }
}

function KOenAttenteTypage()
{
    var strPliID    = $(".c-pli-id").text();
    idPli           = Number(strPliID);
    if(idPli > 0)
    {
       /* swal({
            title: "CONFIRMATION",
            text: "Voulez-vous vraiment mettre le pli en KO en attente dans typage ?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Annuler!",
            showLoaderOnConfirm: true,
            closeOnConfirm: true
        }, function () {
           
                $.ajax({ 
                    url : s_url+'typage/Typage/checkIfStandBy/'+idPli,
                    type : 'POST',
                    dataType : 'json',
                    beforeSend: function() {
                        GedGoToPageLogin();
                    },
                    success : function(resultat, statut){
                        if(resultat !== 0)
                        {
                            location.reload();
                        }
                        else
                        {
                            pliKo(6);
                        }
                    },
                    error : function(resultat, statut, erreur){
                        swal("Erreur !", "Erreur de serveur", "error");
                    }
                });
            


        });*/
        
        typeKo = 6;
        $("#pli-ko-type").text(" KO en attente ");
        $("#ko-pli-id").text(idPli);
        $("#ko-motif-rejet-pli").modal("show");
        $("#ko-motif-description").val("");
    }
    else
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de charger un pli '},{type: 'danger'});
    }
}

function mettreKOPli()
{
    var go = 1;

    /*var motif = $("#ko-motif-description").val();
    var go = 1;
    if(motif == null || motif == "null" || motif == undefined || motif == "undefined" || motif.trim() == "")
    {
        
        notifUSer("Merci de renseigner le motif");
        go = 0;
        
    }*/

    var motifRejet  = $("#motifAnomalie").val();
    var description = "";
    var strTempDescription = "";
    if(motifRejet == 0 || motifRejet == null)
    {
        $("#txt-error1").text("");
        $("#txt-error1").text(" Motif KO obligatoire");
        $(".c-error").show().delay(2000).fadeOut();
        return 0;
    }

    var strTempMotifRejet = $( "#motifAnomalie option:selected" ).text();

    var strMotifRejet     = $("#motifAnomalie :selected").text();
        strMotifRejet     = strMotifRejet.toLowerCase();

    if(strMotifRejet.indexOf("autre") > -1)
    {
        description = $("#ko-motif-description").val();
        description = description.trim();

        if(description == "" || description == null)
        {
            go = 0;
            $("#txt-error1").text("");
            $("#txt-error1").text(" Merci d'ajouter une description");
            $(".c-error").show().delay(2000).fadeOut();
            return 0;
        }
        else 
        {
            strTempDescription = " > : "+description ;
        }
    }

    var motifOperateur = strTempMotifRejet+" "+strTempDescription;

    if(go == 1)
    {
        if(typeKo > 0)
        {
             pliKo(typeKo,motifOperateur);
        }
       
        $("#ko-motif-rejet-pli").modal("hide");
    }
   
}
function getPreDataChq(idPli) {

   
        $.ajax({
            url : s_url+'typage/Typage/preData/'+idPli,
            type : 'POST',
            dataType : 'json',
            beforeSend: function() {
                GedGoToPageLogin();
            },
            success : function(resultat, statut){
    
                
                if(resultat.length > 0 && resultat[0].id_document !="")
                {
                    $('#infoSociete').val(resultat[0].societe).change();
                    i = 0;
                    $.each(resultat, function (key, data) {

                        var classDiv = "mode_paiement_"+i;
                        var docu = " (Document : "+data.id_document+" )";
                        ajouterCheque(classDiv,data.cmc7,docu);
                        i++;
                    });
    
    
    
                    $(".c-mode-paie").html("");
    
                    paiementCheque();
                }
            },
            error : function(resultat, statut, erreur){
                swal("Erreur !", "Erreur de serveur", "error");
            }
        });
    
   
}

function paiementCheque() {
    $.ajax({
        url : s_url+'typage/Typage/modePaiemntCheque',
        type : 'POST',
        dataType : 'html',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(resultat, statut){


            $(".c-mode-paie").html("");
            $(".c-mode-paie").html(resultat);
            $(function () {
                $.AdminBSB.browser.activate();
                $.AdminBSB.leftSideBar.activate();
                $.AdminBSB.rightSideBar.activate();
                $.AdminBSB.navbar.activate();
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.input.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.search.activate();
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
            });
        },
        error : function(resultat, statut, erreur){
            swal("Erreur !", "Erreur de serveur", "error");
        }
    });
}

function listePlis()
{
    alert("123");
}

function raccourci()
{
    $("#raccourci-modal").modal("show");
}

function isNumberKey(evt,elm)
       {
         var valElm = $("#"+elm).val();
         
         var strCount = charCount(valElm); 
         if(strCount > 1)
         {
            //parseFloat($(this).val()).toFixed(2)
            var str = valElm.slice('.', -1); 
            $("#"+elm).val(str);

            return false;
         }
         
         var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
          
       }

function setMontant(elm)
{
    var strVal  =$("#"+elm).val();
    var lastChar = strVal.slice(-1);
    if (lastChar == '.') {
        strVal = strVal.slice(0, -1);
    }
    montant = strVal.split(".");
    
   var str_montant = u00_montant(montant);
    $("#"+elm).val(str_montant);

}

function u00_montant(montant='0.00') {
    montant = ('' + montant).replace(',', '.');
    montant = montant.replace(/[^0-9.]/g, '');
    montant = Number.parseFloat(montant);
    montant = isNaN(montant) ? 0 : montant;
    if(Number.isInteger(montant)){return ''+montant+'.00'; }
    var montant_str = montant.toString(10);
    return /^\d+\.\d{1}$/.test(montant_str) ? montant_str + '0' : montant_str;
    }
function charCount(str) 
{
    var letterCount = 0;
    for (var position = 0; position < str.length; position++) 
    {
        if (str.charAt(position) == '.') 
            {
                letterCount += 1;
            }
        }
    return letterCount;
    
}

function init_nsprogress(){
    $(document).ajaxStart(function() {
        NProgress.start();
    });
   /* $(document).ajaxComplete(function() {
        NProgress.done();
    });
    $(document).ajaxSuccess(function() {
        NProgress.done();
    });*/
    $(document).ajaxStop(function() {
        NProgress.done();
    });
}

function getProvenanceType()
{
    var typologie       = $("#typologie").val();
    var infoSociete     = $("#infoSociete").val();
    $.ajax({
        url : s_url+'typage/Typage/checkProvenance/'+typologie+'/'+infoSociete,
        type : 'POST',
        dataType : 'html',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(resultat, statut){
            
            $(".c-provenance-type").html("");   
            $(".c-provenance-type").html(resultat);
            $(function () {
                $.AdminBSB.browser.activate();
                $.AdminBSB.leftSideBar.activate();
                $.AdminBSB.rightSideBar.activate();
                $.AdminBSB.navbar.activate();
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.input.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.search.activate();
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
            });
           
        },
        error : function(resultat, statut, erreur){
            swal("Erreur !", "Erreur de serveur", "error");
        }
    });
}

/*
function verifierAnomalieParRang(rang)
{
    var anomalie     = $("#chAnomalie_"+rang).val();
    if(anomalie != null && anomalie != "null")
    {
        console.log(anomalie);
        $("#texte-sans-anomalie_"+rang).text("Chèque avec anomalie");
        $("#texte-sans-anomalie_"+rang).css('color', 'red');
    }
    else
    {
        $("#texte-sans-anomalie_"+rang).text("Chèque sans anomalie");
        $("#texte-sans-anomalie_"+rang).css('color', 'black');
    }
   
}*/


/**
 * 
 * @param {*} documentRang 
 * vider le champ si efa misy any champ hafa
 * 
 */

function checkDocument(documentRang)
{
    var currentDoc = $("#chqDoc_"+documentRang).val();
    $('.c-chqDoc').each(function(i,e) { 
        var className   = $(this).attr('id');
        if(className != undefined && className != "undefined")
        {
            
            var rang        = className.split("chqDoc_");
            var rangCheque  = parseInt(rang[1]);
			var tempDoc =  $("#"+className).val();
            console.log(className);
            console.log(rangCheque);

            if(parseInt(currentDoc) == parseInt(tempDoc) && rangCheque != parseInt(documentRang) )
            {
                $("#chqDoc_"+documentRang).val(0);
                infoInModal(documentRang, "L'ID Document <br/><span style='color:red;'>L'ID document "+currentDoc+" est déjà utlisé dans le <span class=\"badge bg-purple\">Chèque #"+rangCheque+"</span>");
            }
        }

     });
     

}


function KOdefinitif()
{
    /*swal({
        title: "Confirmation",
        text: "Voulez-vous mettre en KO définitf ce pli ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#ff0000",
        confirmButtonText: "OUI",
        cancelButtonText: "Fermer",
        showLoaderOnConfirm: true,
        closeOnConfirm: true
    }, function () {
        pliKo(3);
    });*/
    var strPliID    = $(".c-pli-id").text();
    idPli           = Number(strPliID);
    
      typeKo = 3;
        $("#pli-ko-type").text(" KO définitf ");
        $("#ko-pli-id").text(idPli);
        $("#ko-motif-rejet-pli").modal("show");
        $("#ko-motif-description").val("");
}

/****
 * kd : 3
 * ko scan : 2
 * ko inc : 4
 * ko en attente : 6
 * 
 */
function pliKo(type,motif="")
{
    var strPliID    = $(".c-pli-id").text();
    
    idPli           = parseInt(strPliID);

    $.ajax({
        url : s_url+'typage/Typage/mettrePliKO/'+idPli+'/'+type,
        type : 'POST',
        dataType : 'text',
        data:{motif:motif},
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(resultat, statut){
            if(parseInt(resultat) > 0)
            {
                typeKo = 0;
                
                goTO();
                
            }
            else
            {
                swal("Erreur !", "Il y eu une erreur, merci de réessayer", "error");
            }

           
        },
        error : function(resultat, statut, erreur){
            swal("Erreur !", "Erreur de serveur", "error");
        }
    });
}


/***
 * type :
 * 1 : n° payeur
 * 2 : nom payeur
 * 3 : n° abonnee 
 * 4 : code_promo
 * 
 * */
function setPredataTypage(idPli)
{
  


    // $.ajax({
        // url : s_url+'typage/Typage/preDataNompPayeurBloc/'+idPli,
        // type : 'POST',
        // dataType : 'text',
        // async : false,
        // beforeSend: function() {
            // GedGoToPageLogin();
        // },
        // success : function(resultat, statut){
           
                // console.log("type : "+type);
                // switch(type)
                // {
                    // case 1:
                        // $("#numPayeur").val(resultat);
                        // break;
    
                   // case 2:
                        // $("#nomPayeur").val(resultat);
                        // break;
    
                    // case 3:
                        // $("#numAbonne").val(resultat);
                        // break;
    
                    // case 4:
                        // $("#codePromo").val(resultat);
                        // break;
                // }
            // },
            // error : function(resultat, statut, erreur){
                // swal("Erreur !", "Erreur de serveur", "error");
            // }
        // });
        
        
        
        
        
        
        
        
        
        
        
        
        $.ajax({
        url : s_url+'typage/Typage/preDataNompPayeurBloc/'+idPli,
        type : 'POST',
        dataType : 'text',
        async : false,
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(resultat, statut){
            $("#apres-info-payeur").append(resultat); 
            
        },
        error : function(resultat, statut, erreur){
            swal("Erreur !", "Erreur de serveur", "error");
        }
    });    
    

   
}


function ajouter_bloc_payeur(nbBloc)
{
    $.ajax({
        url : s_url+'typage/Typage/ajouterBlocPayeur/'+nbBloc,
        type : 'POST',
        dataType : 'text',
        async : false,
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(resultat, statut){
            $("#apres-info-payeur").append(resultat); 
            hideBtnAddDelPayeur(nbBloc);
            $(function () {
                $.AdminBSB.browser.activate();
                $.AdminBSB.leftSideBar.activate();
                $.AdminBSB.rightSideBar.activate();
                $.AdminBSB.navbar.activate();
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.input.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.search.activate();
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
            });
            
        },
        error : function(resultat, statut, erreur){
            swal("Erreur !", "Erreur de serveur", "error");
        }
    });
}

function supprimer_bloc_payeur(nbBloc)
{
    if(parseInt(nbBloc) > 1)
    {
        var parentBloc = parseInt(nbBloc) - 1;
        if(parentBloc > 1 )
        {
            $(".supprimer_bloc_payeur-"+parentBloc).show();
        }
        
        $(".ajouter_bloc_payeur-"+parentBloc).show();
        $(".bloc-payeur-"+nbBloc).remove();
    }
    else
    {
        notifUSer("Vous ne pouvez pas supprimer le dernier bloc");
    }
}
function hideBtnAddDelPayeur(nbBloc)
{
    $(".supprimer_bloc_payeur-"+nbBloc).hide();
    $(".ajouter_bloc_payeur-"+nbBloc).hide();
}

function visualiserConsigneTypage()
{
    var strPliID        = $(".c-pli-id").text();
        idPli           = parseInt(strPliID);
    
    if(strPliID == 0 || strPliID == null || strPliID.trim() == "" || strPliID == undefined)
    {
        swal("Information !", "ID pli non récupéré, merci de recharger la page", "info");
        return 0;
    }
    affiche_motif_consigne(idPli);
}

function affiche_motif_consigne(id_pli) {
    $.get(s_url + '/control/ajax/all_motif_consigne/'+id_pli, {}, function(reponse) 
        {
            $('#mdl_disp_motif_consigne_pli_id_pli').html(id_pli);
            $('#mdl_disp_motif_consigne_pli_body').html(reponse);
            $('#mdl_disp_motif_consigne_pli').modal('show');
        });
    }

function luConsigne()
{
    
    var strPliID        = $(".c-pli-id").text();
        idPli           = parseInt(strPliID);
        
        $.get(s_url + '/control/ajax/luConsigne/Typage/'+idPli+"/"+idMotif, {}, function(reponse) 
        {
            $("#modal-consigne").modal('hide');
        });
}