var idDpli = 0;
var tableDev;
$(document).keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        tableDev.ajax.reload();
    }
});
$(function () {
    $(".dev").click(function () {

        var idDpli      = $("#id-pli-dev").val();
        var classStr    = $(this).attr("class");
            lastClass   = classStr.substr( classStr.lastIndexOf(' ') + 1);

        console.log(lastClass);

        swal({
            title: "Confirmation",
            text: "Voulez-vous déverouiller ce pli ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ff0000",
            confirmButtonText: "OUI",
            cancelButtonText: "Fermer",
            showLoaderOnConfirm: true,
            closeOnConfirm: true
        }, function () {

            var infoPli = [];

            infoPli     = {idPli:idDpli,action:lastClass};

            $.ajax({
                url : s_url+'deverouiller/Pli/deverouillerPli',
                type : 'POST',
                dataType : 'json',
                data: infoPli,
                beforeSend: function() {
                    GedGoToPageLogin();
                },
                success : function(resultat, statut){
                    console.log(resultat);
                    if(parseInt(resultat.retour) == 1)
                    {
                       /* swal({
                            title: "",
                            text: "Déverouillage terminé",
                            type: "info",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "OK",
                            cancelButtonText: "Annuler!",
                            showLoaderOnConfirm: true,
                            closeOnConfirm: true
                        }, function () {
                            recherchePli();
                           $("#unlockModal").modal("hide");
                        });*/
						 recherchePli();
                           $("#unlockModal").modal("hide");

                    }
                    else
                    {
                        swal("Erreur !", "Erreur de serveur", "error");
                    }
                },
                error : function(resultat, statut, erreur){
                    swal("Erreur !", "Erreur de serveur", "error");
                }
            });
        });




    });
    tableDev = $('.deverouiller').DataTable({
        "language": {
            processing:     "Traitement en cours...",
            search:         "Rechercher&nbsp;:",
            lengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "scrollY": '600px',
        "processing": true,
        "scrollCollapse": true,
        "deferRender":    true,
        "scroller":       true,
        "serverSide": true,
        "ajax": {
            "url":s_url+'deverouiller/Pli/getListePli',
            "type": "POST",
            "data": function ( param ) {
                param.dtIdPli 	    = $("#dev-id-pli").val();
                param.dtTypage 	    = $("#dev-typage-par").val();
                param.dtSaisie	    = $("#dev-saisie-par").val();
                param.dtCtrl 		= $("#dev-ctrl-par").val();
                },
        },
        "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false //set not orderable
            }
        ],
        "fnInitComplete":function(){
            $('.dataTables_scrollBody').slimscroll({
                axis: 'both',
                distance: '3px',
                alwaysVisible:false
            });
            $(".dataTables_scrollHeadInner").css({"width":"100%"});
            $(".deverouiller ").css({"width":"100%"});
        },
        "drawCallback": function( settings ) {
            $(".dataTables_scrollHeadInner").css({"width":"100%"});
            $(".deverouiller ").css({"width":"100%"});
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.input.activate();
        }
    });

    $('.deverouiller thead').on( 'click', 'th', function () {
        setTimeout(function(){
            $(".dataTables_scrollHeadInner").css({"width":"100%"});
            $(".deverouiller ").css({"width":"100%"});
        }, 1000);
    } );
});
/****************/
function lockPli(id,idTtm) {
    $("#pli-unlock").text("");
    $("#pli-unlock").text(id);

    hideAllBtn();
    $("#id-pli-dev").val(id);

    switch (idTtm) {
        case 1:
            $(".dev-typage").show();
            break;
        case 4:
            $(".dev-saisie").show();
            $(".dev-typage-saisie").show();
            break;
        case 8:
            $(".dev-ctrl").show();
            $(".dev-saisie-ctrl").show();
            $(".dev-all").show();


            break;

    }
    $("#unlockModal").modal("show");
}
function hideAllBtn() {
    $(".dev").hide();
}

function recherchePli() {
    tableDev.ajax.reload();
}