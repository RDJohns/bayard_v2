var dateDebut;
var dateFin;
var societe;
var myPost;

var chargementSpinner = '<center><div class="preloader"><div class="spinner-layer pl-black"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>';
$('.provenance-date').datepicker({
    todayBtn: "linked",
    language: "fr",
    autoclose: true,
    todayHighlight: true,
    toggleActive: true
    
});

$(function () {
    $(".provenance-par-typologie").hide();
    $(".recap-provenance").hide();
    $(".provenance-par-date").hide();
   
});
function chargerProvenance()
{
    societe     = $("#filtre-societe").val();
    dateDebut   = $("#date-debut").val() == null ? 0 : $("#date-debut").val();
    dateFin     = $("#date-fin").val() == null ? 0 : $("#date-fin").val();
    $(".provenance-par-typologie").show();
    $(".recap-provenance").show();
    $(".provenance-par-date").show();

    $(".body-provenance-typologie").html(chargementSpinner);
    $(".body-provenance-date").html(chargementSpinner);
    $(".body-recap-provenance").html(chargementSpinner);

    if(dateDebut == 0 || dateFin == 0)
    {
        $("#date-debut").focus();
        NProgress.done();
        return false;
    }

    if(getDate(dateDebut) > getDate(dateFin))
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de rectifier votre filtre date '},{type: 'danger'});
        $("#date-debut").focus();
        return false;
    }
    else
    {
        myPost = {dateDebut:dateDebut,dateFin:dateFin,societe};
        
        recapProvenance();
        provenanceParDate();
        provenanceParTypologie();
    }
}

function recapProvenance()
{
    
    NProgress.start();
    $.ajax({
        url : s_url+'provenance/Pli/recapProvenance',
        type : 'POST',
        dataType : 'html',
        data    : myPost,
        success : function(data, statut){
            
            NProgress.done();
            $(".body-recap-provenance").html("");
            $(".body-recap-provenance").html(data);
            //$("provenance-recap").da
            /*$('#provenance-recap').DataTable();*/
            $('#provenance-recap').DataTable( {
                dom: 'Bfrtip',
                buttons: ['excel'],
                scrollY: '300px',
                fixedHeader: true,
            } );
        },

        error : function(resultat, statut, erreur){

            NProgress.done();
        },

        complete : function(resultat, statut){
            NProgress.done();
            

        }

    });
}

function provenanceParDate()
{
    NProgress.start();
    $.ajax({
        url : s_url+'provenance/Pli/provenanceParDate',
        type : 'POST',
        dataType : 'html',
        data    : myPost,
        success : function(data, statut){
            
            NProgress.done();
            $(".body-provenance-date").html("");
            $(".body-provenance-date").html(data);
            //$("provenance-recap").da
            /*$('#provenance-recap').DataTable();*/
            $('#provenance-par-date').DataTable( {
                 
                scrollY: '300px',
                scrollX: true,
                fixedHeader: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1
                },
                dom: 'Bfrtip',
                buttons: ['excel']
            } );
        },

        error : function(resultat, statut, erreur){

            NProgress.done();
        },

        complete : function(resultat, statut){
            NProgress.done();

        }

    });
}

function provenanceParTypologie()
{
    NProgress.start();
    $.ajax({
        url : s_url+'provenance/Pli/provenanceParTypologie',
        type : 'POST',
        dataType : 'html',
        data    : myPost,
        success : function(data, statut){
            
            NProgress.done();
            console.log(data);
            $(".body-provenance-typologie").html("");
            $(".body-provenance-typologie").html(data);
            $('#provenance-par-typologie').DataTable( {
                 
                scrollY: '300px',
                scrollX: true,
                fixedHeader: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1
                },
                dom: 'Bfrtip',
                buttons: ['excel']
            } );
        },

        error : function(resultat, statut, erreur){

            NProgress.done();
        },

        complete : function(resultat, statut){
            NProgress.done();

        }

    });
}