var dateDebut;
var dateFin;
var societe;
var myPost;
var tableListePlis;
var ci;
var count_export = 0;
$(".c-circulaire").hide();
$("#valider_ci").prop( "disabled", true );
$("#exporter_ci").prop( "disabled", true );
//var chargementSpinner = '<center><div class="preloader"><div class="spinner-layer pl-black"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>';
$('.provenance-date').datepicker({
    todayBtn: "linked",
    language: "fr",
    autoclose: true,
    todayHighlight: true,
    toggleActive: true
    
});

function chargerCirculaire()
{
    societe     = $("#filtre-societe").val();
    //alert(societe);
    dateDebut   = $("#date-debut").val() == null ? 0 : $("#date-debut").val();
    dateFin     = $("#date-fin").val() == null ? 0 : $("#date-fin").val();
   

    if(dateDebut == 0 || dateFin == 0)
    {
        $("#date-debut").focus();
        NProgress.done();
        return false;
    }

    if(getDate(dateDebut) > getDate(dateFin))
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de rectifier votre filtre date '},{type: 'danger'});
        $("#date-debut").focus();
        return false;
    }
    else
    {
        myPost = {dateDebut:dateDebut,dateFin:dateFin,societe};
        //$("#circulaire-table-liste").html(chargementSpinner);
        listePlis();
    }
    //listePlis();
}

function listePlis()
{
    $(".c-circulaire").show();
    $.post(s_url+'circulaire/Pli/getListeCirculaire', myPost, function(reponse) {
         if(reponse != ''){
            var res = JSON.parse(reponse);
            $('#liste_ci').html(res.html);
            $('#total').html(res.total);
            //alert(reponse);
        }
    });
 
  
}


function validation_ci(){
    swal({
        title: "Validation CI",
        text: "Voulez-vous valider les plis en circulaire",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4CAF50",
        confirmButtonText: "Oui",
        cancelButtonText: "Non",
        closeOnConfirm: false,
        //icon: "warning",
		buttons: ['Non!', 'Oui!'],
  		dangerMode: true
    },function (res) {
        $("#exporter_ci").prop( "disabled", !res );
        var table = new Array();
        $("#liste_ci tr").each(function(row,tr){
            var filter = $(tr).find("select").val();
            if(filter === "1"){
                var line = {
                    "date_courrier" : $(tr).find("td:eq(0)").text(),
                    "date_enregistrement" : $(tr).find("td:eq(1)").text(),
                    "commande" : $(tr).find("td:eq(2)").text(),
                    "lot_scan" : $(tr).find("td:eq(3)").text(),
                    "id_pli" : $(tr).find("td:eq(4)").text(),
                    "pli" : $(tr).find("td:eq(5)").text(),
                    "id_doc" : $(tr).find("td:eq(6)").text(),
                    "code" : $(tr).find("td:eq(7)").text(),
                    "circulaire" : $(tr).find("td:eq(8)").text(),
                    "num_image" : $(tr).find("td:eq(9)").text(),
                    "cmc7" : $(tr).find("td:eq(10)").text(),
                    "nom_deleg" : $(tr).find("td:eq(11)").text(),
                    "nom_payeur" : $(tr).find("td:eq(12)").text(),
                    "nom_orig_fichier_circulaire" : $(tr).find("td:eq(13)").text()
                };
                table.push(line);
            }
        });
        $.post(s_url+'circulaire/Pli/updateCirculaire', {valides : JSON.stringify(table)}, function(reponse) {
            if(reponse != ''){
            }
        });
        swal.close();
    });
}

function export_ci_valide(){
    var table = new Array();
    $("#liste_ci tr").each(function(row,tr){
        var filter = $(tr).find("select").val();
        if(filter === "1"){
            var line = {
                "date_courrier" : $(tr).find("td:eq(0)").text(),
                "date_enregistrement" : $(tr).find("td:eq(1)").text(),
                "commande" : $(tr).find("td:eq(2)").text(),
                "lot_scan" : $(tr).find("td:eq(3)").text(),
                "id_pli" : $(tr).find("td:eq(4)").text(),
                "pli" : $(tr).find("td:eq(5)").text(),
                "id_doc" : $(tr).find("td:eq(6)").text(),
                "code" : $(tr).find("td:eq(7)").text(),
                "circulaire" : $(tr).find("td:eq(8)").text(),
                "num_image" : $(tr).find("td:eq(9)").text(),
                "cmc7" : $(tr).find("td:eq(10)").text(),
                "nom_deleg" : $(tr).find("td:eq(11)").text(),
                "nom_payeur" : $(tr).find("td:eq(12)").text(),
                "nom_orig_fichier_circulaire" : $(tr).find("td:eq(13)").text()
            };
            table.push(line);
        }/*else{
            alert("Plis reporté");
        }*/
    });
    $.post(s_url+'circulaire/Pli/exportCirculaire', {valides : JSON.stringify(table)}, function(reponse) {
        if(reponse != ''){
            var csvData = new Blob([reponse], {type: 'text/xls;charset=utf-8;'});
            var csvURL = window.URL.createObjectURL(csvData);
            var tempLink = document.createElement('a');
            tempLink.href = csvURL;
            tempLink.setAttribute('download', 'Liste circulaire validé.xls');
            document.body.appendChild(tempLink);
            tempLink.click();
        }
    });
}

function change_etat_ci(obj)
{
    var etat_ci  = $(obj).val();
    if(etat_ci == '1') count_export++;
    else if(count_export > 0) count_export--; 
    $("#valider_ci").prop( "disabled", (count_export == 0) );
    if(count_export == 0) $("#exporter_ci").prop( "disabled", true );
}


