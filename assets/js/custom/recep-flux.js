var dateDebut;
var dateFin;
var societe;
var source;
var tableListePlis;
var myPost;

var chargementSpinner = '<center><div class="preloader"><div class="spinner-layer pl-black"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>';
$('.recpt-date').datepicker({
    todayBtn: "linked",
    language: "fr",
    autoclose: true, 
    todayHighlight: true,
    toggleActive: true

});

function init_nsprogress(){
    $( document ).ajaxStart(function() {
        $(".preloader-reception").show();
        NProgress.start();
    });
    /*$( document ).ajaxComplete(function() {
        NProgress.done();
    });
    $(document).ajaxSuccess(function() {
        NProgress.done();
    });*/
    $(document).ajaxStop(function() {
        NProgress.done();
        $(".preloader-reception").hide();
    });

   
}

$(function () {
    init_nsprogress();
    hideForm();
    $('[data-toggle="tooltip"]').tooltip({container: 'body'}); 
});
function hideForm()
{
    $(".ged-reception").hide();
    $(".ged-reception-flux").hide();
    $(".detail-typo-table").hide();
    $(".detail-typo-table-flux").hide();
    $(".detail-clo-table").hide();
    $(".detail-clo-table-flux").hide();
    $(".detail-encours-table").hide();
    $(".detail-encours-table-flux").hide();
    $(".detail-non-table").hide();
    $(".detail-non-table-flux").hide();
    $(".detail-encours-typo-table").hide();
    $(".detail-encours-typo-table-flux").hide();
    $(".voir-synthese").hide();
    $(".voir-synthese-flux").hide();
    $(".voir-liste-pli").hide();
    $(".voir-liste-pli-flux").hide();
    $(".voir-synthese-semaine").hide();
    $(".voir-synthese-semaine-flux").hide();
	$(".detail-anomalie-table").hide();
	$(".detail-anomalie-table-flux").hide();
	$(".detail-pli-passe-ko-table").hide();
}

function getMorris(type, element,json,couleur) {

    $("#"+element).empty();
    Morris.Donut({
        element: element,
        data: json,
        colors: couleur,
        resize : false,
        hoverCallback: function(index, options, content) {
            var data = options.data[index];
            $(".morris-hover").html('<div>Custom label: ' + data.label + '</div>');
        },
        formatter: function (y) {
            return y + '%'
        }
    });

}

function pliParTypologie() {

    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table" id="table-t">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Typologie</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Mvm</th>\n' +
        '\t\t\t<th style="width:15%">% Plis</th>\n' +
        '\t\t\t<th style="width:15%">% Mvm</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];
    $("#recp-typo").html(chargementSpinner+"chargement ...");
    $.ajax({
        url : s_url+'reception/Pli/pliParTypologie',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var pliTotal = [];
            var pliMvt   = [];

            var iT = 0;
            $.each(data, function(i, item) {

                if(item.typologie != null)
                {
                    var tmpTypo = ''+item.typologie+'';
                    if(item.nb_pli_par_typo != 0)
                    {
                        jsonPli.push({"label":tmpTypo, "value":item.pourcent_pli});
                    }
                    if(item.nb_mvt_par_typo != 0)
                    {
                        jsonMv.push({"label":tmpTypo, "value":item.pourcent_mvt});
                    }
                    pliTotal.push(item.pli_total);
                    pliMvt.push(item.nb_mvt_total);
                    tableRecTypologie += "<tr><td style=''>"+item.typologie+"</td><td>"+item.nb_pli_par_typo+"</td><td>"+item.nb_mvt_par_typo+"</td><td>"+item.pourcent_pli+" %</td><td>"+item.pourcent_mvt+" %</td></tr>";
                    iT++;
                }

            });
            var uniquePliTotal      = Array.from(new Set(pliTotal));
            var maxPlitotal         = Math.max.apply(Math, uniquePliTotal);

            var uniquePliTotalMv    = Array.from(new Set(pliMvt));
            var maxPlitotalMv       = Math.max.apply(Math, uniquePliTotalMv);





            if(iT > 0)
            {
                var  tableRecTypologieTotal = "<tfoot><tr style='font-weight: bold;'><td >Total</td><td>"+maxPlitotal+"</td><td>"+maxPlitotalMv+"</td><td></td><td></td></tr></tfoot>";
                tableRecTypologieTotal +="</table>";
                tableRecTypologie +=tableRecTypologieTotal;
                $("#recp-typo").html(tableRecTypologie);
                //console.log(jsonPli);
                var couleurPli = ['#00EAFF', '#AA00FF', '#FF7F00', '#0095FF', '#f58231', '#FF00AA', '#FFD400', '#6AFF00', '#bcf60c', '#EDB9B9', '#8F2323','#00EAFF', '#AA00FF', '#FF7F00', '#0095FF', '#f58231'];
                var couleurMv = [ '#EE5567', '#43CAA9', '#EC87BF', '#4FC0E8', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];


                $('#table-t').DataTable({ "lengthMenu": [[6, 10, 20,25, -1], [6, 10, 20,25, "Toutes"]]});

                $("#donut-typo-pli").empty();
                $("#donut-typo-mv").empty();

                getMorris('donut', "donut-typo-pli",jsonPli,couleurPli);
                getMorris('donut', "donut-typo-mv",jsonMv,couleurMv);
            }
            else
            {
                notifUSer("Nombre de plis par typologie : 0");
                $("#recp-typo").html("<b>Pas de données</b>");
            }

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function pliParTypologieFlux() {

    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table" id="table-t">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Typologie</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Abnmt</th>\n' +
        '\t\t\t<th style="width:15%">% Flux</th>\n' +
        '\t\t\t<th style="width:15%">% Abnmt</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];
    $("#recp-typo-flux").html(chargementSpinner+"chargement ...");
    $.ajax({
        url : s_url+'reception/Pli_flux/pliParTypologie',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var pliTotal = [];
            var pliMvt   = [];

            var iT = 0;
            $.each(data, function(i, item) {

                if(item.typologie != null)
                {
                    var tmpTypo = ''+item.typologie+'';
                    if(item.nb_pli_par_typo != 0)
                    {
                        jsonPli.push({"label":tmpTypo, "value":item.pourcent_flux});
                    }
                    if(item.nb_mvt_par_typo != 0)
                    {
                        jsonMv.push({"label":tmpTypo, "value":item.pourcent_abnmt});
                    }
                    pliTotal.push(item.flux_total);
                    pliMvt.push(item.nb_abnmt_total);
                    tableRecTypologie += "<tr><td style=''>"+item.typologie+"</td><td>"+item.nb_flux_par_typo+"</td><td>"+item.nb_abnmt_par_typo+"</td><td>"+item.pourcent_flux+" %</td><td>"+item.pourcent_abnmt+" %</td></tr>";
                    iT++;
                }

            });
            var uniquePliTotal      = Array.from(new Set(pliTotal));
            var maxPlitotal         = Math.max.apply(Math, uniquePliTotal);

            var uniquePliTotalMv    = Array.from(new Set(pliMvt));
            var maxPlitotalMv       = Math.max.apply(Math, uniquePliTotalMv);





            if(iT > 0)
            {
                var  tableRecTypologieTotal = "<tfoot><tr style='font-weight: bold;'><td >Total</td><td>"+maxPlitotal+"</td><td>"+maxPlitotalMv+"</td><td></td><td></td></tr></tfoot>";
                tableRecTypologieTotal +="</table>";
                tableRecTypologie +=tableRecTypologieTotal;
                $("#recp-typo-flux").html(tableRecTypologie);
                //console.log(jsonPli);
                var couleurPli = ['#00EAFF', '#AA00FF', '#FF7F00', '#0095FF', '#f58231', '#FF00AA', '#FFD400', '#6AFF00', '#bcf60c', '#EDB9B9', '#8F2323','#00EAFF', '#AA00FF', '#FF7F00', '#0095FF', '#f58231'];
                var couleurMv = [ '#EE5567', '#43CAA9', '#EC87BF', '#4FC0E8', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];


                $('#table-t').DataTable({ "lengthMenu": [[6, 10, 20,25, -1], [6, 10, 20,25, "Toutes"]]});

                $("#donut-typo-flux").empty();
                $("#donut-typo-abnmt").empty();

                getMorris('donut', "donut-typo-flux",jsonPli,couleurPli);
                getMorris('donut', "donut-typo-abnmt",jsonMv,couleurMv);
            }
            else
            {
                notifUSer("Nombre de plis par typologie : 0");
                $("#recp-typo").html("<b>Pas de données</b>");
            }

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function pliCloture() {

    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table" id="table-c">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Typologie</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Mvm</th>\n' +
        '\t\t\t<th style="width:15%">% Plis</th>\n' +
        '\t\t\t<th style="width:15%">% Mvm</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $.ajax({
        url : s_url+'reception/Pli/pliCloture',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){

            var  tableRecTypologieTotal = "";


            var iC  = 0;
            var pliTotal = [];
            var pliMvt   = [];
            $.each(data, function(i, item) {

                if(item.typologie != null)
                {
                    var tmpTypo = ''+item.typologie+'';
                    if(item.nb_pli_par_typo != 0)
                    {
                        jsonPli.push({"label":tmpTypo, "value":item.pourcent_pli});
                    }
                    if(item.nb_mvt_par_typo != 0)
                    {
                        jsonMv.push({"label":tmpTypo, "value":item.pourcent_mvt});
                    }
                    pliTotal.push(item.pli_total);
                    pliMvt.push(item.nb_mvt_total);
                    tableRecTypologie += "<tr><td style=''>"+item.typologie+"</td><td>"+item.nb_pli_par_typo+"</td><td>"+item.nb_mvt_par_typo+"</td><td>"+item.pourcent_pli+" %</td><td>"+item.pourcent_mvt+" %</td></tr>";
                    iC++;
                }
            });


            var uniquePliTotal      = Array.from(new Set(pliTotal));
            var maxPlitotal         = Math.max.apply(Math, uniquePliTotal);

            var uniquePliTotalMv    = Array.from(new Set(pliMvt));
            var maxPlitotalMv       = Math.max.apply(Math, uniquePliTotalMv);



            if(iC > 0)
            {
                var  tableRecTypologieTotal = "<tfoot><tr style='font-weight: bold;'><td >Total</td><td>"+maxPlitotal+"</td><td>"+maxPlitotalMv+"</td><td></td><td></td></tr></tfoot>";
                tableRecTypologieTotal +="</table>";
                tableRecTypologie +=tableRecTypologieTotal;
                $("#recp-clo").html(tableRecTypologie);
                // console.log(jsonPli);
                var couleurPli = ['#ec87bf', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C','#ff6262'];
                var couleurMv = [ '#EE5567', '#43CAA9', '#EC87BF', '#4FC0E8', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
                $("#donut-cloture-pli").empty();
                $("#donut-cloture-mv").empty();
                getMorris('donut', "donut-cloture-pli",jsonPli,couleurPli);
                getMorris('donut', "donut-cloture-mv",jsonMv,couleurMv);

            }
            else
            {
                $("#recp-clo").html('<b>Pas de données</b>');
                notifUSer("Nombre de plis clôturés : 0");
            }
            $('#table-c').DataTable({ "lengthMenu": [[6, 10, 20,25, -1], [6, 10, 20,25, "Toutes"]]});
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function pliClotureFlux() {

    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table" id="table-c-f">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Typologie</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Flux</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Abnmt</th>\n' +
        '\t\t\t<th style="width:15%">% Flux</th>\n' +
        '\t\t\t<th style="width:15%">% Abnmt</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $.ajax({
        url : s_url+'reception/Pli_flux/pliCloture',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){

            var  tableRecTypologieTotal = "";


            var iC  = 0;
            var pliTotal = [];
            var pliMvt   = [];
            $.each(data, function(i, item) {

                if(item.typologie != null)
                {
                    var tmpTypo = ''+item.typologie+'';
                    if(item.nb_flux_par_typo != 0)
                    {
                        jsonPli.push({"label":tmpTypo, "value":item.pourcent_flux});
                    }
                    if(item.nb_mvt_par_typo != 0)
                    {
                        jsonMv.push({"label":tmpTypo, "value":item.pourcent_abnmt});
                    }
                    pliTotal.push(item.flux_total);
                    pliMvt.push(item.nb_abnmt_total);
                    tableRecTypologie += "<tr><td style=''>"+item.typologie+"</td><td>"+item.nb_flux_par_typo+"</td><td>"+item.nb_abnmt_par_typo+"</td><td>"+item.pourcent_flux+" %</td><td>"+item.pourcent_abnmt+" %</td></tr>";
                    iC++;
                }
            });


            var uniquePliTotal      = Array.from(new Set(pliTotal));
            var maxPlitotal         = Math.max.apply(Math, uniquePliTotal);

            var uniquePliTotalMv    = Array.from(new Set(pliMvt));
            var maxPlitotalMv       = Math.max.apply(Math, uniquePliTotalMv);



            if(iC > 0)
            {
                var  tableRecTypologieTotal = "<tfoot><tr style='font-weight: bold;'><td >Total</td><td>"+maxPlitotal+"</td><td>"+maxPlitotalMv+"</td><td></td><td></td></tr></tfoot>";
                tableRecTypologieTotal +="</table>";
                tableRecTypologie +=tableRecTypologieTotal;
                $("#recp-clo-flux").html(tableRecTypologie);
                // console.log(jsonPli);
                var couleurPli = ['#ec87bf', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C','#ff6262'];
                var couleurMv = [ '#EE5567', '#43CAA9', '#EC87BF', '#4FC0E8', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
                $("#donut-cloture-flux").empty();
                $("#donut-cloture-abnmt").empty();
                getMorris('donut', "donut-cloture-flux",jsonPli,couleurPli);
                getMorris('donut', "donut-cloture-abnmt",jsonMv,couleurMv);

            }
            else
            {
                $("#recp-clo-flux").html('<b>Pas de données</b>');
                notifUSer("Nombre de plis clôturés : 0");
            }
            $('#table-c-f').DataTable({ "lengthMenu": [[6, 10, 20,25, -1], [6, 10, 20,25, "Toutes"]]});
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function pliEncours() {
    NProgress.start();
    var tableRecTypologie = "";
    $("#recp-en-cours").html(chargementSpinner);
    tableRecTypologie += '' +
        '<table class="table">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Etape</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Mvm</th>\n' +
        '\t\t\t<th style="width:15%">% Plis</th>\n' +
        '\t\t\t<th style="width:15%">% Mvm</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $(".preloader-head").show();
    $.ajax({
        url : s_url+'reception/Pli/pliEncours',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var iE = 0;
            var  tableRecTypologieTotal = "<tr style='font-weight: bold;'><td >Total</td><td>"+data[0].pli_total+"</td><td>"+data[0].mvt_total+"</td><td></td><td></td></tr>";
            $.each(data, function(i, item) {
                if(item.libelle != null )
                {
                    var libelle = ''+item.libelle+'';


                    jsonMv.push({"label":libelle, "value":item.pourcent_mv});
                    jsonPli.push({"label":libelle, "value":item.pourcent_pli});
                    tableRecTypologie += "<tr><td style=''>"+item.libelle+"</td><td>"+item.pli+"</td><td>"+item.mvt+"</td><td>"+item.pourcent_pli+" %</td><td>"+item.pourcent_mv+" %</td></tr>";
                    iE++;
                }
                $(".preloader-head").show();
            });
            tableRecTypologie +=tableRecTypologieTotal;
            tableRecTypologieTotal +="</table>";
            if(iE > 0)
            {
                // console.log(jsonPli);
                $("#recp-en-cours").html("");
                $("#recp-en-cours").html(tableRecTypologie);

                var couleurPli = ['#FE9E4F', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C'];
                var couleurMv = [ '#EE5567', '#43CAA9', '#EC87BF', '#4FC0E8', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];

                $("#donut-en-cours-pli").empty();
                $("#donut-en-cours-pli").empty();

                getMorris('donut', "donut-en-cours-pli",jsonPli,couleurPli);
                getMorris('donut', "donut-en-cours-mv",jsonMv,couleurMv);
                NProgress.done();
            }
            else
            {
                $("#recp-en-cours").html("<b>Pas de données</b>");
                notifUSer("Nombre de plis en cours : 0");

            }

            $(".preloader-head").hide();

            globaleReception();
            $("#stat_rech").prop("disabled",false);

        },

        error : function(resultat, statut, erreur){
            $(".preloader-head").hide();

        },

        complete : function(resultat, statut){
            // console.log(resultat);
            $(".preloader-head").show();
        }

    });
}

function pliEncoursFlux() {
    NProgress.start();
    var tableRecTypologie = "";
    $("#recp-en-cours").html(chargementSpinner);
    tableRecTypologie += '' +
        '<table class="table">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Etape</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Flux</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Abnmt</th>\n' +
        '\t\t\t<th style="width:15%">% Flux</th>\n' +
        '\t\t\t<th style="width:15%">% Abnmt</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $(".preloader-head").show();
    $.ajax({
        url : s_url+'reception/Pli_flux/pliEncours',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var iE = 0;
            var  tableRecTypologieTotal = "<tr style='font-weight: bold;'><td >Total</td><td>"+data[0].flux_total+"</td><td>"+data[0].abnmt_total+"</td><td></td><td></td></tr>";
            $.each(data, function(i, item) {
                if(item.libelle != null )
                {
                    var libelle = ''+item.libelle+'';


                    jsonMv.push({"label":libelle, "value":item.pourcent_abnmt});
                    jsonPli.push({"label":libelle, "value":item.pourcent_flux});
                    tableRecTypologie += "<tr><td style=''>"+item.libelle+"</td><td>"+item.flux+"</td><td>"+item.abnmt+"</td><td>"+item.pourcent_flux+" %</td><td>"+item.pourcent_abnmt+" %</td></tr>";
                    iE++;
                }
                $(".preloader-head").show();
            });
            tableRecTypologie +=tableRecTypologieTotal;
            tableRecTypologieTotal +="</table>";
            if(iE > 0)
            {
                // console.log(jsonPli);
                $("#recp-en-cours-flux").html("");
                $("#recp-en-cours-flux").html(tableRecTypologie);

                var couleurPli = ['#FE9E4F', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C'];
                var couleurMv = [ '#EE5567', '#43CAA9', '#EC87BF', '#4FC0E8', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];

                $("#donut-en-cours-flux").empty();
                $("#donut-en-cours-abnmt").empty();

                getMorris('donut', "donut-en-cours-flux",jsonPli,couleurPli);
                getMorris('donut', "donut-en-cours-abnmt",jsonMv,couleurMv);
                NProgress.done();
            }
            else
            {
                $("#recp-en-cours-flux").html("<b>Pas de données</b>");
                notifUSer("Nombre de plis en cours : 0");

            }

            $(".preloader-head").hide();

            globaleReceptionFlux();
            $("#stat_rech").prop("disabled",false);

        },

        error : function(resultat, statut, erreur){
            $(".preloader-head").hide();

        },

        complete : function(resultat, statut){
            // console.log(resultat);
            $(".preloader-head").show();
        }

    });
}


function nonTraiteEncours() {
    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:70%">#Pli</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">% </th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $.ajax({
        url : s_url+'reception/Pli/nonTraiteEncours',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var iR = 0;
            $.each(data, function(i, item) {
                if(item.total != null )
                {

                    tableRecTypologie +="<tr><td style=''>EN COURS</td><td>"+item.pli_en_cours+"</td><td>"+item.pourcent_en_cours+" %</td></tr>";
                    tableRecTypologie +="<tr><td style=''>NON TRAITE</td><td>"+item.pli_non_traite+"</td><td>"+item.pourcent_non_traite+" %</td></tr>";
                    jsonMv.push({"label":"EN COURS", "value":item.pourcent_en_cours});
                    jsonMv.push({"label":"NON TRAITE", "value":item.pourcent_non_traite});
                    iR++;
                }
            });

            tableRecTypologie +="</table>";

            console.log(jsonMv);

            if(iR > 0)
            {


                $("#recp-rest-traiter").html(tableRecTypologie);
                var couleurPli = ['#ffd8b1', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C'];
                var couleurMv = [ '#4FC0E8', '#43CAA9', '#EC87BF', '#EE5567', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
                /* getMorris('donut', "donut-en-cours-pli",jsonPli,couleurPli);*/

                $("#donut-restant-mv").empty();

                getMorris('donut', "donut-restant-mv",jsonMv,couleurMv);
            }
            else
            {
                $("#recp-rest-traiter").html("<b>Pas de données</b>");
            }



        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function nonTraiteEncoursFlux() {
    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:70%">#Flux</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Flux</th>\n' +
        '\t\t\t<th style="width:15%">% </th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $.ajax({
        url : s_url+'reception/Pli_flux/nonTraiteEncours',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var iR = 0;
            $.each(data, function(i, item) {
                if(item.total != null )
                {

                    tableRecTypologie +="<tr><td style=''>EN COURS</td><td>"+item.flux_en_cours+"</td><td>"+item.pourcent_en_cours+" %</td></tr>";
                    tableRecTypologie +="<tr><td style=''>NON TRAITE</td><td>"+item.flux_non_traite+"</td><td>"+item.pourcent_non_traite+" %</td></tr>";
                    jsonMv.push({"label":"EN COURS", "value":item.pourcent_en_cours});
                    jsonMv.push({"label":"NON TRAITE", "value":item.pourcent_non_traite});
                    iR++;
                }
            });

            tableRecTypologie +="</table>";

            console.log(jsonMv);

            if(iR > 0)
            {


                $("#recp-rest-traiter-flux").html(tableRecTypologie);
                var couleurPli = ['#ffd8b1', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C'];
                var couleurMv = [ '#4FC0E8', '#43CAA9', '#EC87BF', '#EE5567', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
                /* getMorris('donut', "donut-en-cours-pli",jsonPli,couleurPli);*/

                $("#donut-restant-abnmt").empty();

                getMorris('donut', "donut-restant-abnmt",jsonMv,couleurMv);
            }
            else
            {
                $("#recp-rest-traiter-flux").html("<b>Pas de données</b>");
            }



        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}


function detailTypologieParJour()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-typo" style="size:10px;">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th style="background-color: white;">date courrier</th>';
    strTable += '<th>Non typé</th>';
    strTable += '<th>3 4 sg la croix</th>';
    strTable += '<th>Annulation</th>';
    strTable += '<th>Changement adresse</th>';
    strTable += '<th>Changement de destinataire</th>';
    strTable += '<th>Changement titre</th>';
    strTable += '<th>Cheque seul</th>';
    strTable += '<th>Comite d entreprise</th>';
    strTable += '<th>Courrier</th>';
    strTable += '<th>Creation</th>';
    strTable += '<th>Creation vpc</th>';
    strTable += '<th>Croisement relance</th>';
    strTable += '<th>Envoi de numero</th>';
    strTable += '<th>Famille complexe</th>';
    strTable += '<th>Famille simple</th>';
    strTable += '<th>Gratuit</th>';
    strTable += '<th>Institutionnel complexe</th>';
    strTable += '<th>Institutionnel simple</th>';
    strTable += '<th>Paiement facture</th>';
    strTable += '<th>Reab avec courrier</th>';
    strTable += '<th>Reab complexe</th>';
    strTable += '<th>Reab simple</th>';
    strTable += '<th>Reexpedition</th>';
    strTable += '<th>Refus portage</th>';
    strTable += '<th>Retour pnd</th>';
    strTable += '<th>Stop relance</th>';
    strTable += '<th>Transfert src</th></tr></thead>';
    $(".detail-typo-table").show();
    $("#typologie-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/typologieJournalier',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            //
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
                strTable += '<td>'+item.info_0+'</td>';
                strTable += '<td>'+item.info_2+'</td>';
                strTable += '<td>'+item.info_3+'</td>';
                strTable += '<td>'+item.info_4+'</td>';
                strTable += '<td>'+item.info_5+'</td>';
                strTable += '<td>'+item.info_6+'</td>';
                strTable += '<td>'+item.info_7+'</td>';
                strTable += '<td>'+item.info_8+'</td>';
                strTable += '<td>'+item.info_9+'</td>';
                strTable += '<td>'+item.info_10+'</td>';
                strTable += '<td>'+item.info_11+'</td>';
                strTable += '<td>'+item.info_12+'</td>';
                strTable += '<td>'+item.info_13+'</td>';
                strTable += '<td>'+item.info_14+'</td>';
                strTable += '<td>'+item.info_15+'</td>';
                strTable += '<td>'+item.info_16+'</td>';
                strTable += '<td>'+item.info_17+'</td>';
                strTable += '<td>'+item.info_18+'</td>';
                strTable += '<td>'+item.info_19+'</td>';
                strTable += '<td>'+item.info_20+'</td>';
                strTable += '<td>'+item.info_21+'</td>';
                strTable += '<td>'+item.info_22+'</td>';
                strTable += '<td>'+item.info_23+'</td>';
                strTable += '<td>'+item.info_24+'</td>';
                strTable += '<td>'+item.info_25+'</td>';
                strTable += '<td>'+item.info_26+'</td>';
                strTable += '<td>'+item.info_27+'</td></tr>';


            });
            $("#typologie-par-jour").html("");
            $("#typologie-par-jour").html(strTable);
            $("#table-par-typo").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollY: '400px',
                scrollX: true,
                fixedHeader: true,
                scrollCollapse: true,
                /*fixedColumns:   {
                    leftColumns: 1

                },*/

            });
            $("#title-detail").text("Nombre de plis par typologie par date");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailTypologieParJourFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-typo" style="size:10px;">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th style="background-color: white;">date r&eacute;ception</th>';
    strTable += '<th>Non typé</th>';
    strTable += '<th>Multi-typologie</th>';
    strTable += '<th>3 4 sg la croix</th>';
    strTable += '<th>Televente</th>';
    strTable += '<th>Televente la croix dom</th>';
    strTable += '<th>Groupeurs annulation</th>';
    strTable += '<th>Groupeurs changement d\'adresse</th>';
    strTable += '<th>Groupeurs création</th>';
    strTable += '<th>Groupeurs envoi de numéro</th>';
    strTable += '<th>Groupeurs reprise de suspension</th>';
    strTable += '<th>Groupeurs suspension</th>';
    strTable += '<th>Paiement facture</th>';
    strTable += '<th>Changement adresse</th>';
    strTable += '<th>Changement de destinataire</th>';
    strTable += '<th>Changement titre</th>';
    strTable += '<th>Envoi de numéro</th>';
    strTable += '<th>Reexpédition</th>';
    strTable += '<th>Rejet de paiement</th>';
    strTable += '<th>Rejet payline</th>';
    strTable += '<th>Gratuit</th>';
    strTable += '<th>Institutionnel simple</th>';
    strTable += '<th>Refus portage</th></tr></thead>';
    $(".detail-typo-table-flux").show();
    $("#typologie-par-jour-flux").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli_flux/typologieJournalier',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            //
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.info_0+'</td>';
                strTable += '<td>'+item.info_1+'</td>';
                strTable += '<td>'+item.info_2+'</td>';
                strTable += '<td>'+item.info_3+'</td>';
                strTable += '<td>'+item.info_4+'</td>';
                strTable += '<td>'+item.info_5+'</td>';
                strTable += '<td>'+item.info_6+'</td>';
                strTable += '<td>'+item.info_7+'</td>';
                strTable += '<td>'+item.info_8+'</td>';
                strTable += '<td>'+item.info_9+'</td>';
                strTable += '<td>'+item.info_10+'</td>';
                strTable += '<td>'+item.info_11+'</td>';
                strTable += '<td>'+item.info_12+'</td>';
                strTable += '<td>'+item.info_13+'</td>';
                strTable += '<td>'+item.info_14+'</td>';
                strTable += '<td>'+item.info_15+'</td>';
                strTable += '<td>'+item.info_16+'</td>';
                strTable += '<td>'+item.info_17+'</td>';
                strTable += '<td>'+item.info_18+'</td>';
                strTable += '<td>'+item.info_19+'</td>';
                strTable += '<td>'+item.info_20+'</td>';
                strTable += '<td>'+item.info_21+'</td></tr>';


            });
            $("#typologie-par-jour-flux").html("");
            $("#typologie-par-jour-flux").html(strTable);
            $("#table-par-typo").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollY: '400px',
                scrollX: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1

                },

            });
            $("#title-detail").text("Nombre de flux par typologie par date");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailTypologieParSemaine()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-typo">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th style="background-color: white;">date courrier</th>';
    strTable += '<th>Non typé</th>';
    strTable += '<th>3 4 sg la croix</th>';
    strTable += '<th>Annulation</th>';
    strTable += '<th>Changement adresse</th>';
    strTable += '<th>Changement de destinataire</th>';
    strTable += '<th>Changement titre</th>';
    strTable += '<th>Cheque seul</th>';
    strTable += '<th>Comite d entreprise</th>';
    strTable += '<th>Courrier</th>';
    strTable += '<th>Creation</th>';
    strTable += '<th>Creation vpc</th>';
    strTable += '<th>Croisement relance</th>';
    strTable += '<th>Envoi de numero</th>';
    strTable += '<th>Famille complexe</th>';
    strTable += '<th>Famille simple</th>';
    strTable += '<th>Gratuit</th>';
    strTable += '<th>Institutionnel complexe</th>';
    strTable += '<th>Institutionnel simple</th>';
    strTable += '<th>Paiement facture</th>';
    strTable += '<th>Reab avec courrier</th>';
    strTable += '<th>Reab complexe</th>';
    strTable += '<th>Reab simple</th>';
    strTable += '<th>Reexpedition</th>';
    strTable += '<th>Refus portage</th>';
    strTable += '<th>Retour pnd</th>';
    strTable += '<th>Stop relance</th>';
    strTable += '<th>Transfert src</th></tr></thead>';

    $(".detail-typo-table").show();
    $("#typologie-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/typologieSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            //
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">S'+item.date_courrier+'</td>';
                strTable += '<td>'+item.info_0+'</td>';
                strTable += '<td>'+item.info_2+'</td>';
                strTable += '<td>'+item.info_3+'</td>';
                strTable += '<td>'+item.info_4+'</td>';
                strTable += '<td>'+item.info_5+'</td>';
                strTable += '<td>'+item.info_6+'</td>';
                strTable += '<td>'+item.info_7+'</td>';
                strTable += '<td>'+item.info_8+'</td>';
                strTable += '<td>'+item.info_9+'</td>';
                strTable += '<td>'+item.info_10+'</td>';
                strTable += '<td>'+item.info_11+'</td>';
                strTable += '<td>'+item.info_12+'</td>';
                strTable += '<td>'+item.info_13+'</td>';
                strTable += '<td>'+item.info_14+'</td>';
                strTable += '<td>'+item.info_15+'</td>';
                strTable += '<td>'+item.info_16+'</td>';
                strTable += '<td>'+item.info_17+'</td>';
                strTable += '<td>'+item.info_18+'</td>';
                strTable += '<td>'+item.info_19+'</td>';
                strTable += '<td>'+item.info_20+'</td>';
                strTable += '<td>'+item.info_21+'</td>';
                strTable += '<td>'+item.info_22+'</td>';
                strTable += '<td>'+item.info_23+'</td>';
                strTable += '<td>'+item.info_24+'</td>';
                strTable += '<td>'+item.info_25+'</td>';
                strTable += '<td>'+item.info_26+'</td>';
                strTable += '<td>'+item.info_27+'</td></tr>';


            });
            $("#typologie-par-jour").html("");
            $("#typologie-par-jour").html(strTable);
            $("#table-par-typo").DataTable({
                   dom: 'Bfrtip',
                    buttons: [
                        'excel'
                    ],
                scrollX:        true,
                scrollX: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail").text("Nombre de plis par typologie par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailTypologieParSemaineFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-typo">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th style="background-color: white;">date r&eacute;ception</th>';
    strTable += '<th>Non typé</th>';
    strTable += '<th>Multi-typologie</th>';
    strTable += '<th>3 4 sg la croix</th>';
    strTable += '<th>Televente</th>';
    strTable += '<th>Televente la croix dom</th>';
    strTable += '<th>Groupeurs annulation</th>';
    strTable += '<th>Groupeurs changement d\'adresse</th>';
    strTable += '<th>Groupeurs création</th>';
    strTable += '<th>Groupeurs envoi de numéro</th>';
    strTable += '<th>Groupeurs reprise de suspension</th>';
    strTable += '<th>Groupeurs suspension</th>';
    strTable += '<th>Paiement facture</th>';
    strTable += '<th>Changement adresse</th>';
    strTable += '<th>Changement de destinataire</th>';
    strTable += '<th>Changement titre</th>';
    strTable += '<th>Envoi de numéro</th>';
    strTable += '<th>Reexpédition</th>';
    strTable += '<th>Rejet de paiement</th>';
    strTable += '<th>Rejet payline</th>';
    strTable += '<th>Gratuit</th>';
    strTable += '<th>Institutionnel simple</th>';
    strTable += '<th>Refus portage</th></tr></thead>';

    $(".detail-typo-table-flux").show();
    $("#typologie-par-jour-flux").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli_flux/typologieSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            //
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.info_0+'</td>';
                strTable += '<td>'+item.info_1+'</td>';
                strTable += '<td>'+item.info_2+'</td>';
                strTable += '<td>'+item.info_3+'</td>';
                strTable += '<td>'+item.info_4+'</td>';
                strTable += '<td>'+item.info_5+'</td>';
                strTable += '<td>'+item.info_6+'</td>';
                strTable += '<td>'+item.info_7+'</td>';
                strTable += '<td>'+item.info_8+'</td>';
                strTable += '<td>'+item.info_9+'</td>';
                strTable += '<td>'+item.info_10+'</td>';
                strTable += '<td>'+item.info_11+'</td>';
                strTable += '<td>'+item.info_12+'</td>';
                strTable += '<td>'+item.info_13+'</td>';
                strTable += '<td>'+item.info_14+'</td>';
                strTable += '<td>'+item.info_15+'</td>';
                strTable += '<td>'+item.info_16+'</td>';
                strTable += '<td>'+item.info_17+'</td>';
                strTable += '<td>'+item.info_18+'</td>';
                strTable += '<td>'+item.info_19+'</td>';
                strTable += '<td>'+item.info_20+'</td>';
                strTable += '<td>'+item.info_21+'</td></tr>';


            });
            $("#typologie-par-jour-flux").html("");
            $("#typologie-par-jour-flux").html(strTable);
            $("#table-par-typo").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollX: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail").text("Nombre de flux par typologie par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function fermer(elm)
{
    $("."+elm).hide();
}




function detailCloParJour()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-clo-typo" style="size:10px;">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th style="background-color: white;">date courrier</th>';
    strTable += '<th>Non typé</th>';
    strTable += '<th>3 4 sg la croix</th>';
    strTable += '<th>Annulation</th>';
    strTable += '<th>Changement adresse</th>';
    strTable += '<th>Changement de destinataire</th>';
    strTable += '<th>Changement titre</th>';
    strTable += '<th>Cheque seul</th>';
    strTable += '<th>Comite d entreprise</th>';
    strTable += '<th>Courrier</th>';
    strTable += '<th>Creation</th>';
    strTable += '<th>Creation vpc</th>';
    strTable += '<th>Croisement relance</th>';
    strTable += '<th>Envoi de numero</th>';
    strTable += '<th>Famille complexe</th>';
    strTable += '<th>Famille simple</th>';
    strTable += '<th>Gratuit</th>';
    strTable += '<th>Institutionnel complexe</th>';
    strTable += '<th>Institutionnel simple</th>';
    strTable += '<th>Paiement facture</th>';
    strTable += '<th>Reab avec courrier</th>';
    strTable += '<th>Reab complexe</th>';
    strTable += '<th>Reab simple</th>';
    strTable += '<th>Reexpedition</th>';
    strTable += '<th>Refus portage</th>';
    strTable += '<th>Retour pnd</th>';
    strTable += '<th>Stop relance</th>';
    strTable += '<th>Transfert src</th></tr></thead>';

    $(".detail-clo-table").show();
    $("#clo-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/cloJournalier',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            //
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
                strTable += '<td>'+item.info_0+'</td>';
                strTable += '<td>'+item.info_2+'</td>';
                strTable += '<td>'+item.info_3+'</td>';
                strTable += '<td>'+item.info_4+'</td>';
                strTable += '<td>'+item.info_5+'</td>';
                strTable += '<td>'+item.info_6+'</td>';
                strTable += '<td>'+item.info_7+'</td>';
                strTable += '<td>'+item.info_8+'</td>';
                strTable += '<td>'+item.info_9+'</td>';
                strTable += '<td>'+item.info_10+'</td>';
                strTable += '<td>'+item.info_11+'</td>';
                strTable += '<td>'+item.info_12+'</td>';
                strTable += '<td>'+item.info_13+'</td>';
                strTable += '<td>'+item.info_14+'</td>';
                strTable += '<td>'+item.info_15+'</td>';
                strTable += '<td>'+item.info_16+'</td>';
                strTable += '<td>'+item.info_17+'</td>';
                strTable += '<td>'+item.info_18+'</td>';
                strTable += '<td>'+item.info_19+'</td>';
                strTable += '<td>'+item.info_20+'</td>';
                strTable += '<td>'+item.info_21+'</td>';
                strTable += '<td>'+item.info_22+'</td>';
                strTable += '<td>'+item.info_23+'</td>';
                strTable += '<td>'+item.info_24+'</td>';
                strTable += '<td>'+item.info_25+'</td>';
                strTable += '<td>'+item.info_26+'</td>';
                strTable += '<td>'+item.info_27+'</td></tr>';


            });
            $("#clo-par-jour").html("");
            $("#clo-par-jour").html(strTable);
            $("#table-clo-typo").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-clo").text("Nombre de plis clôturé par typologie par date");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailCloParJourFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-clo-typo" style="size:10px;">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th style="background-color: white;">date r&eacute;ception</th>';
    strTable += '<th>Non typé</th>';
    strTable += '<th>Multi-typologie</th>';
    strTable += '<th>3 4 sg la croix</th>';
    strTable += '<th>Televente</th>';
    strTable += '<th>Televente la croix dom</th>';
    strTable += '<th>Groupeurs annulation</th>';
    strTable += '<th>Groupeurs changement d\'adresse</th>';
    strTable += '<th>Groupeurs création</th>';
    strTable += '<th>Groupeurs envoi de numéro</th>';
    strTable += '<th>Groupeurs reprise de suspension</th>';
    strTable += '<th>Groupeurs suspension</th>';
    strTable += '<th>Paiement facture</th>';
    strTable += '<th>Changement adresse</th>';
    strTable += '<th>Changement de destinataire</th>';
    strTable += '<th>Changement titre</th>';
    strTable += '<th>Envoi de numéro</th>';
    strTable += '<th>Reexpédition</th>';
    strTable += '<th>Rejet de paiement</th>';
    strTable += '<th>Rejet payline</th>';
    strTable += '<th>Gratuit</th>';
    strTable += '<th>Institutionnel simple</th>';
    strTable += '<th>Refus portage</th></tr></thead>';

    $(".detail-clo-table-flux").show();
    $("#clo-par-jour-flux").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli_flux/cloJournalier',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            //
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.info_0+'</td>';
                strTable += '<td>'+item.info_1+'</td>';
                strTable += '<td>'+item.info_2+'</td>';
                strTable += '<td>'+item.info_3+'</td>';
                strTable += '<td>'+item.info_4+'</td>';
                strTable += '<td>'+item.info_5+'</td>';
                strTable += '<td>'+item.info_6+'</td>';
                strTable += '<td>'+item.info_7+'</td>';
                strTable += '<td>'+item.info_8+'</td>';
                strTable += '<td>'+item.info_9+'</td>';
                strTable += '<td>'+item.info_10+'</td>';
                strTable += '<td>'+item.info_11+'</td>';
                strTable += '<td>'+item.info_12+'</td>';
                strTable += '<td>'+item.info_13+'</td>';
                strTable += '<td>'+item.info_14+'</td>';
                strTable += '<td>'+item.info_15+'</td>';
                strTable += '<td>'+item.info_16+'</td>';
                strTable += '<td>'+item.info_17+'</td>';
                strTable += '<td>'+item.info_18+'</td>';
                strTable += '<td>'+item.info_19+'</td>';
                strTable += '<td>'+item.info_20+'</td>';
                strTable += '<td>'+item.info_21+'</td></tr>';


            });
            $("#clo-par-jour-flux").html("");
            $("#clo-par-jour-flux").html(strTable);
            $("#table-clo-typo").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-clo-flux").text("Nombre de flux clôturé par typologie par date");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailCloParSemaine()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-clo-sem" style="size:10px;">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th style="background-color: white;">Semaine</th>';
    strTable += '<th>Non typé</th>';
    strTable += '<th>3 4 sg la croix</th>';
    strTable += '<th>Annulation</th>';
    strTable += '<th>Changement adresse</th>';
    strTable += '<th>Changement de destinataire</th>';
    strTable += '<th>Changement titre</th>';
    strTable += '<th>Cheque seul</th>';
    strTable += '<th>Comite d entreprise</th>';
    strTable += '<th>Courrier</th>';
    strTable += '<th>Creation</th>';
    strTable += '<th>Creation vpc</th>';
    strTable += '<th>Croisement relance</th>';
    strTable += '<th>Envoi de numero</th>';
    strTable += '<th>Famille complexe</th>';
    strTable += '<th>Famille simple</th>';
    strTable += '<th>Gratuit</th>';
    strTable += '<th>Institutionnel complexe</th>';
    strTable += '<th>Institutionnel simple</th>';
    strTable += '<th>Paiement facture</th>';
    strTable += '<th>Reab avec courrier</th>';
    strTable += '<th>Reab complexe</th>';
    strTable += '<th>Reab simple</th>';
    strTable += '<th>Reexpedition</th>';
    strTable += '<th>Refus portage</th>';
    strTable += '<th>Retour pnd</th>';
    strTable += '<th>Stop relance</th>';
    strTable += '<th>Transfert src</th></tr></thead>';

    $(".detail-clo-table").show();
    $("#clo-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/cloTypologieSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            //
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">S'+item.date_courrier+'</td>';
                strTable += '<td>'+item.info_0+'</td>';
                strTable += '<td>'+item.info_2+'</td>';
                strTable += '<td>'+item.info_3+'</td>';
                strTable += '<td>'+item.info_4+'</td>';
                strTable += '<td>'+item.info_5+'</td>';
                strTable += '<td>'+item.info_6+'</td>';
                strTable += '<td>'+item.info_7+'</td>';
                strTable += '<td>'+item.info_8+'</td>';
                strTable += '<td>'+item.info_9+'</td>';
                strTable += '<td>'+item.info_10+'</td>';
                strTable += '<td>'+item.info_11+'</td>';
                strTable += '<td>'+item.info_12+'</td>';
                strTable += '<td>'+item.info_13+'</td>';
                strTable += '<td>'+item.info_14+'</td>';
                strTable += '<td>'+item.info_15+'</td>';
                strTable += '<td>'+item.info_16+'</td>';
                strTable += '<td>'+item.info_17+'</td>';
                strTable += '<td>'+item.info_18+'</td>';
                strTable += '<td>'+item.info_19+'</td>';
                strTable += '<td>'+item.info_20+'</td>';
                strTable += '<td>'+item.info_21+'</td>';
                strTable += '<td>'+item.info_22+'</td>';
                strTable += '<td>'+item.info_23+'</td>';
                strTable += '<td>'+item.info_24+'</td>';
                strTable += '<td>'+item.info_25+'</td>';
                strTable += '<td>'+item.info_26+'</td>';
                strTable += '<td>'+item.info_27+'</td></tr>';


            });
            $(".detail-clo-table").show();
            $("#clo-par-jour").html("");
            $("#clo-par-jour").html(strTable);
            $("#table-par-clo-sem").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollX: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-clo").text("Nombre de plis clôturé par typologie par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailCloParSemaineFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-clo-sem" style="size:10px;">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th style="background-color: white;">date r&eacute;ception</th>';
    strTable += '<th>Non typé</th>';
    strTable += '<th>Multi-typologie</th>';
    strTable += '<th>3 4 sg la croix</th>';
    strTable += '<th>Televente</th>';
    strTable += '<th>Televente la croix dom</th>';
    strTable += '<th>Groupeurs annulation</th>';
    strTable += '<th>Groupeurs changement d\'adresse</th>';
    strTable += '<th>Groupeurs création</th>';
    strTable += '<th>Groupeurs envoi de numéro</th>';
    strTable += '<th>Groupeurs reprise de suspension</th>';
    strTable += '<th>Groupeurs suspension</th>';
    strTable += '<th>Paiement facture</th>';
    strTable += '<th>Changement adresse</th>';
    strTable += '<th>Changement de destinataire</th>';
    strTable += '<th>Changement titre</th>';
    strTable += '<th>Envoi de numéro</th>';
    strTable += '<th>Reexpédition</th>';
    strTable += '<th>Rejet de paiement</th>';
    strTable += '<th>Rejet payline</th>';
    strTable += '<th>Gratuit</th>';
    strTable += '<th>Institutionnel simple</th>';
    strTable += '<th>Refus portage</th></tr></thead>';

    $(".detail-clo-table-flux").show();
    $("#clo-par-jour-flux").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli_flux/cloTypologieSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            //
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">S'+item.date_reception+'</td>';
                strTable += '<td>'+item.info_0+'</td>';
                strTable += '<td>'+item.info_1+'</td>';
                strTable += '<td>'+item.info_2+'</td>';
                strTable += '<td>'+item.info_3+'</td>';
                strTable += '<td>'+item.info_4+'</td>';
                strTable += '<td>'+item.info_5+'</td>';
                strTable += '<td>'+item.info_6+'</td>';
                strTable += '<td>'+item.info_7+'</td>';
                strTable += '<td>'+item.info_8+'</td>';
                strTable += '<td>'+item.info_9+'</td>';
                strTable += '<td>'+item.info_10+'</td>';
                strTable += '<td>'+item.info_11+'</td>';
                strTable += '<td>'+item.info_12+'</td>';
                strTable += '<td>'+item.info_13+'</td>';
                strTable += '<td>'+item.info_14+'</td>';
                strTable += '<td>'+item.info_15+'</td>';
                strTable += '<td>'+item.info_16+'</td>';
                strTable += '<td>'+item.info_17+'</td>';
                strTable += '<td>'+item.info_18+'</td>';
                strTable += '<td>'+item.info_19+'</td>';
                strTable += '<td>'+item.info_20+'</td>';
                strTable += '<td>'+item.info_21+'</td></tr>';


            });
            $(".detail-clo-table-flux").show();
            $("#clo-par-jour-flux").html("");
            $("#clo-par-jour-flux").html(strTable);
            $("#table-par-clo-sem").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollX: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-clo-flux").text("Nombre de flux clôturé par typologie par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailEncoursParJour()
{
   var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-encours-jour" style="size:10px;">';
   strTable += '<thead style ="background:white;">';
   strTable += '<tr>';
   strTable += '<th>Date courrier</th>';
   strTable += '<th>En cours de typage</th>';
   strTable += '<th>En attente de saisie</th>';
   strTable += '<th>En cours de saisie</th>';
   strTable += '<th>En attente de contrôle</th>';
   strTable += '<th>En cours de contrôle</th>';
   strTable += '<th>En attente de retraitement</th>';
   strTable += '<th>En cours de retraitement</th></tr></thead>';
	
   $(".detail-encours-table").show();
   $("#encours-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/encoursParJour',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           // console.log("xxx : "+data);
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.en_cours_typage+'</td>';
            strTable += '<td>'+item.en_attente_saisie+'</td>';
            strTable += '<td>'+item.en_cours_saisie+'</td>';
            strTable += '<td>'+item.en_attente_controle+'</td>';
            strTable += '<td>'+item.pli_en_cours_controle+'</td>';
            strTable += '<td>'+item.en_attente_retraitement+'</td>';
            strTable += '<td>'+item.en_cours_retraitement+'</td></tr>';
            

        });
        
        $(".detail-encours-table").show();
        $("#encours-par-jour").html("");
        $("#encours-par-jour").html(strTable);
        $("#table-par-encours-jour").DataTable({
            scrollX:        true,
            scrollCollapse: true,
            scrollX: true,
            fixedHeader: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
             $("#title-detail-encours").text("Nombre de plis en cours par jour");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailEncoursParJourFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-encours-jour" style="size:10px;">';
    strTable += '<thead style ="background:white;">';
    strTable += '<tr>';
    strTable += '<th>Date réception</th>';
    strTable += '<th>En cours de typage</th>';
    strTable += '<th>Flux typé</th>';
    strTable += '<th>En cours de saisie nv1</th>';
    strTable += '<th>Flux en escalade</th>';
    strTable += '<th>En cours de saisie nv2</th>';
    strTable += '<th>Flux en transfert consignes</th>';
    strTable += '<th>Flux en réception consignes</th>';
    strTable += '<th>En cours de retraitement</th></tr></thead>';

    $(".detail-encours-table-flux").show();
    $("#encours-par-jour-flux").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli_flux/encoursParJour',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            console.log(data);
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.en_cours_typage+'</td>';
                strTable += '<td>'+item.flux_type+'</td>';
                strTable += '<td>'+item.en_cours_saisie_nv1+'</td>';
                strTable += '<td>'+item.flux_escalade+'</td>';
                strTable += '<td>'+item.en_cours_saisie_nv2+'</td>';
                strTable += '<td>'+item.flux_trans_cons+'</td>';
                strTable += '<td>'+item.flux_recep_cons+'</td>';
                strTable += '<td>'+item.flux_en_cours_retraite+'</td></tr>';

            });

            $(".detail-encours-table-flux").show();
            $("#encours-par-jour-flux").html("");
            $("#encours-par-jour-flux").html(strTable);
            $("#table-par-encours-jour").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                scrollX: true,
                fixedHeader: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-encours-flux").text("Nombre de flux en cours par jour");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}
function detailEncoursParSemaine()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-encours-jour" style="size:10px;">';
    strTable += '<thead style ="background:white;">';
    strTable += '<tr>';
	strTable += '<th>Semaine</td>';
	strTable += '<th>En cours de typage</th>';
	strTable += '<th>En attente de saisie</th>';
	strTable += '<th>En cours de saisie</th>';
	strTable += '<th>En attente de contrôle</th>';
	strTable += '<th>En cours de contrôle</th>';
	strTable += '<th>En attente de retraitement</th>';
	strTable += '<th>En cours de retraitement</th></tr></thead>';
    $("#encours-par-jour").html(chargementSpinner);
    $(".detail-encours-table").show();

    $.ajax({
        url : s_url+'reception/Pli/encoursParSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            console.log(data);
            $.each(data, function(i, item) {
				strTable += '<tr>';
				strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
				strTable += '<td>'+item.en_cours_typage+'</td>';
				strTable += '<td>'+item.en_attente_saisie+'</td>';
				strTable += '<td>'+item.en_cours_saisie+'</td>';
				strTable += '<td>'+item.en_attente_controle+'</td>';
				strTable += '<td>'+item.pli_en_cours_controle+'</td>';
				strTable += '<td>'+item.en_attente_retraitement+'</td>';
				strTable += '<td>'+item.en_cours_retraitement+'</td></tr>';


            });

            $(".detail-encours-table").show();
            $("#encours-par-jour").html("");
            $("#encours-par-jour").html(strTable);
            $("#table-par-encours-jour").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                scrollX: true,
                fixedHeader: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-encours").text("Nombre de plis en cours par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailEncoursParSemaineFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-encours-jour" style="size:10px;">';
    strTable += '<thead style ="background:white;">';
    strTable += '<tr>';
    strTable += '<th>Semaine</th>';
    strTable += '<th>En cours de typage</th>';
    strTable += '<th>Flux typé</th>';
    strTable += '<th>En cours de saisie nv1</th>';
    strTable += '<th>Flux en escalade</th>';
    strTable += '<th>En cours de saisie nv2</th>';
    strTable += '<th>Flux en transfert consignes</th>';
    strTable += '<th>Flux en réception consignes</th>';
    strTable += '<th>En cours de retraitement</th></tr></thead>';
    $("#encours-par-jour-flux").html(chargementSpinner);
    $(".detail-encours-table-flux").show();

    $.ajax({
        url : s_url+'reception/Pli_flux/encoursParSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            console.log(data);
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.en_cours_typage+'</td>';
                strTable += '<td>'+item.flux_type+'</td>';
                strTable += '<td>'+item.en_cours_saisie_nv1+'</td>';
                strTable += '<td>'+item.flux_escalade+'</td>';
                strTable += '<td>'+item.en_cours_saisie_nv2+'</td>';
                strTable += '<td>'+item.flux_trans_cons+'</td>';
                strTable += '<td>'+item.flux_recep_cons+'</td>';
                strTable += '<td>'+item.flux_en_cours_retraite+'</td></tr>';


            });

            $(".detail-encours-table-flux").show();
            $("#encours-par-jour-flux").html("");
            $("#encours-par-jour-flux").html(strTable);
            $("#table-par-encours-jour").DataTable({
                scrollX:        true,
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollCollapse: true,
                scrollX: true,
                fixedHeader: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-encours-flux").text("Nombre de flux en cours par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function nonTraiteSemaine()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-non-jour" style="size:10px;">';
    strTable += '<thead style ="background:white;font-size: 11px;font-weight: bold;    ">';
    strTable += '<tr>';
    strTable += '<td>Semaine</td>';
    strTable += '<td>NON TRAITE</td>';
    strTable += '<td>EN COURS</td></thead>';


    $(".detail-non-table").show();
    $("#non-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/nonTraiteSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            console.log(data);
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">S'+item.date_courrier+'</td>';
                strTable += '<td>'+item.pli_non_traite+'</td>';
                strTable += '<td>'+item.pli_en_cours+'</td>';



            });

            $(".detail-non-table").show();
            $("#non-par-jour").html("");
            $("#non-par-jour").html(strTable);
            $("#table-par-non-jour").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                scrollX: true,
                fixedHeader: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-non").text("Nombre de plis par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function nonTraiteSemaineFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-non-jour" style="size:10px;">';
    strTable += '<thead style ="background:white;font-size: 11px;font-weight: bold;    ">';
    strTable += '<tr>';
    strTable += '<td>Semaine</td>';
    strTable += '<td>NON TRAITE</td>';
    strTable += '<td>EN COURS</td></thead>';


    $(".detail-non-table-flux").show();
    $("#non-par-jour-flux").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli_flux/nonTraiteSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            console.log(data);
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">S'+item.date_reception+'</td>';
                strTable += '<td>'+item.flux_en_cours+'</td>';
                strTable += '<td>'+item.flux_non_traite+'</td>';



            });

            $(".detail-non-table-flux").show();
            $("#non-par-jour-flux").html("");
            $("#non-par-jour-flux").html(strTable);
            $("#table-par-non-jour").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                scrollX: true,
                fixedHeader: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-non-flux").text("Nombre de flux par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function nonTraiteJour()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-non-jour" style="size:10px;">';
    strTable += '<thead style ="background:white;">';
    strTable += '<tr>';
    strTable += '<th>Date courrier</th>';
    strTable += '<th>NON TRAITE</th>';
    strTable += '<th>EN COURS</th></thead>';


    $(".detail-non-table").show();
    $("#non-par-jour").html(chargementSpinner);

    $.ajax({
        url : s_url+'reception/Pli/nonTraiteJour',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            console.log(data);
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
                strTable += '<td>'+item.pli_non_traite+'</td>';
                strTable += '<td>'+item.pli_en_cours+'</td>';



            });
            strTable += '</table>';
            $(".detail-non-table").show();
            $("#non-par-jour").html("");
            $("#non-par-jour").html(strTable);

            $("#table-par-non-jour").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                scrollX: true,
                fixedHeader: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-non").text("Nombre de plis par jour");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function nonTraiteJourFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-non-jour" style="size:10px;">';
    strTable += '<thead style ="background:white;">';
    strTable += '<tr>';
    strTable += '<th>Date réception</th>';
    strTable += '<th>NON TRAITE</th>';
    strTable += '<th>EN COURS</th></thead>';


    $(".detail-non-table-flux").show();
    $("#non-par-jour-flux").html(chargementSpinner);

    $.ajax({
        url : s_url+'reception/Pli_flux/nonTraiteJour',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            console.log(data);
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.flux_en_cours+'</td>';
                strTable += '<td>'+item.flux_non_traite+'</td>';



            });
            strTable += '</table>';
            $(".detail-non-table-flux").show();
            $("#non-par-jour-flux").html("");
            $("#non-par-jour-flux").html(strTable);

            $("#table-par-non-jour").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                scrollX: true,
                fixedHeader: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-non-flux").text("Nombre de flux par jour");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function fluxAnomalieJour(){

    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-ano-flux-jour" style="size:10px;">';
    strTable += '<thead style ="background:white;">';
    strTable += '<tr>';
    strTable += '<th rowspan="2">Date de réception</th>';
    strTable += '<th colspan="9">Actuel</th>';
    strTable += '<th colspan="9">Cumul</th>';
    strTable += '</tr>';
    strTable += '<tr>';
    strTable += '<th>ANOMALIE MAIL</th>';
    strTable += '<th>ANOMALIE PJ</th>';
    strTable += '<th>ANOMALIE FICHIER</th>';
    strTable += '<th>KO SRC</th>';
    strTable += '<th>KO DEFINITIF</th>';
    strTable += '<th>KO INCONNU</th>';
    strTable += '<th>KO EN ATTENTE</th>';
    strTable += '<th>REJETE</th>';
    strTable += '<th>HORS PERIMETRE</th>';
    strTable += '<th>ANOMALIE MAIL</th>';
    strTable += '<th>ANOMALIE PJ</th>';
    strTable += '<th>ANOMALIE FICHIER</th>';
    strTable += '<th>KO SRC</th>';
    strTable += '<th>KO DEFINITIF</th>';
    strTable += '<th>KO INCONNU</th>';
    strTable += '<th>KO EN ATTENTE</th>';
    strTable += '<th>REJETE</th>';
    strTable += '<th>HORS PERIMETRE</th>';
    strTable += '</tr></thead>';

    $(".detail-anomalie-table-flux").show();
    //$("#flux-anomalie-table").html(chargementSpinner);

    $.ajax({
        url : s_url+'reception/Pli_flux/fluxAnomalieJour',
        type : 'POST',
        data    : myPost,
        //dataType : 'json',
        success : function(data, statut){
            console.log(data);
            /*$.each(data, function(i, item) {
                strTable += '<t>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.flux_en_cours+'</td>';
                strTable += '<td>'+item.flux_non_traite+'</td>';

            });*/

            strTable += data;

            strTable += '</table>';
            $(".detail-anomalie-table-flux").show();
            $("#flux-anomalie-table").html("");
            $("#flux-anomalie-table").html(strTable);

            $("#table-ano-flux-jour").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                scrollX: true,
                fixedHeader: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-anomalie-table-flux").text("Répartition journalière des anomalies");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });

}

function fluxAnomalieSemaine(){

    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-ano-flux-jour" style="size:10px;">';
    strTable += '<thead style ="background:white;">';
    strTable += '<tr>';
    strTable += '<th rowspan="2">Semaine</th>';
    strTable += '<th colspan="9">Actuel</th>';
    strTable += '<th colspan="9">Cumul</th>';
    strTable += '</tr>';
    strTable += '<tr>';
    strTable += '<th>ANOMALIE MAIL</th>';
    strTable += '<th>ANOMALIE PJ</th>';
    strTable += '<th>ANOMALIE FICHIER</th>';
    strTable += '<th>KO SRC</th>';
    strTable += '<th>KO DEFINITIF</th>';
    strTable += '<th>KO INCONNU</th>';
    strTable += '<th>KO EN ATTENTE</th>';
    strTable += '<th>REJETE</th>';
    strTable += '<th>HORS PERIMETRE</th>';
    strTable += '<th>ANOMALIE MAIL</th>';
    strTable += '<th>ANOMALIE PJ</th>';
    strTable += '<th>ANOMALIE FICHIER</th>';
    strTable += '<th>KO SRC</th>';
    strTable += '<th>KO DEFINITIF</th>';
    strTable += '<th>KO INCONNU</th>';
    strTable += '<th>KO EN ATTENTE</th>';
    strTable += '<th>REJETE</th>';
    strTable += '<th>HORS PERIMETRE</th>';
    strTable += '</tr></thead>';

    $(".detail-anomalie-table-flux").show();
    //$("#flux-anomalie-table").html(chargementSpinner);

    $.ajax({
        url : s_url+'reception/Pli_flux/fluxAnomalieSemaine',
        type : 'POST',
        data    : myPost,
        //dataType : 'json',
        success : function(data, statut){
            console.log(data);
            /*$.each(data, function(i, item) {
                strTable += '<t>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.flux_en_cours+'</td>';
                strTable += '<td>'+item.flux_non_traite+'</td>';

            });*/

            strTable += data;

            strTable += '</table>';
            $(".detail-anomalie-table-flux").show();
            $("#flux-anomalie-table").html("");
            $("#flux-anomalie-table").html(strTable);

            $("#table-ano-flux-jour").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                scrollX:        true,
                scrollCollapse: true,
                scrollX: true,
                fixedHeader: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            $("#title-detail-anomalie-table-flux").text("Répartition hebdomadaire des anomalies");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });

}



function voirSynthese()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-synthese-jour">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th>Date de réception</th>';
    strTable += '<th>Date dernier traitement</th>';
    strTable += '<th>Nbr. plis reçus</th>';
    strTable += '<th>Total clôturé</th>';
    strTable += '<th>Total anomalie</th>';
    strTable += '<th>Total CI éditée</th>';
    strTable += '<th>Total en cours</th>';
    strTable += '<th>Total non-traité</th></thead>';


    $(".detail-encours-typo-table").show();

    $.ajax({
        url : s_url+'reception/Pli/syntheseJournaliere',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            console.log(data);
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
                strTable += '<td>'+item.dernier_traitement+'</td>';
                strTable += '<td>'+item.nb_pli_recus+'</td>';
                strTable += '<td>'+item.nb_cloture+'</td>';
                strTable += '<td>'+item.nb_anomalie+'</td>';
                strTable += '<td>'+item.nb_ci_editee+'</td>';
                strTable += '<td>'+item.nb_encours+'</td>';
                strTable += '<td>'+item.non_traite+'</td>';





            });
            strTable += '</table>';
            $(".voir-synthese").show();
            $("#synthese-journaliere").html(strTable);

            voirSyntheseSemaine();
            $(".detail-encours-typo-table").hide();
            $("#table-synthese-jour").DataTable({
                scrollX:        true,
                scrollCollapse: true
            });

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function voirSyntheseFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-synthese-jour">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th>Date de réception</th>';
    strTable += '<th>Date dernier traitement</th>';
    strTable += '<th>Nbr. flux reçus</th>';
    strTable += '<th>Total clôturé</th>';
    strTable += '<th>Total en cours</th>';
    strTable += '<th>Total non-traité</th></thead>';


    $(".detail-encours-typo-table").show();

    $.ajax({
        url : s_url+'reception/Pli_flux/syntheseJournaliere',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            console.log(data);
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.dernier_traitement+'</td>';
                strTable += '<td>'+item.nb_flux_recus+'</td>';
                strTable += '<td>'+item.nb_cloture+'</td>';
                strTable += '<td>'+item.nb_encours+'</td>';
                strTable += '<td>'+item.non_traite+'</td>';





            });
            strTable += '</table>';
            $(".voir-synthese-flux").show();
            $("#synthese-journaliere-flux").html(strTable);

            voirSyntheseSemaineFlux();
            $(".detail-encours-typo-table").hide();
            $("#table-synthese-jour").DataTable({
                scrollX:        true,
                scrollCollapse: true
            });

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}


function voirSyntheseSemaine()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-synthese-semaine">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th>Date de réception</th>';
    strTable += '<th>Date dernier traitement</th>';
    strTable += '<th>Nbr. plis reçus</th>';
    strTable += '<th>Total clôturé</th>';
    strTable += '<th>Total anomalie</th>';
    strTable += '<th >Total CI editée</th>';
    strTable += '<th>Total en cours</th>';
    strTable += '<th>Total non-traité</th></thead>';


    $(".voir-synthese-semaine").show();

    $.ajax({
        url : s_url+'reception/Pli/syntheseSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
                strTable += '<td>'+item.dernier_traitement+'</td>';
                strTable += '<td>'+item.nb_pli_recus+'</td>';
                strTable += '<td>'+item.nb_cloture+'</td>';
                strTable += '<td>'+item.nb_anomalie+'</td>';
                strTable += '<td>'+item.nb_ci_editee+'</td>';
                strTable += '<td>'+item.nb_encours+'</td>';
                strTable += '<td>'+item.non_traite+'</td>';



            });
            strTable += '</table>';
            $(".voir-synthese").show();
            $("#synthese-semaine").html(strTable);
            $("#table-synthese-semaine").DataTable({
                scrollX:        true,
                scrollCollapse: true
            });

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });





}

function voirSyntheseSemaineFlux()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-synthese-semaine">';
    strTable += '<thead>';
    strTable += '<tr>';
    strTable += '<th>Date de réception</th>';
    strTable += '<th>Date dernier traitement</th>';
    strTable += '<th>Nbr. plis reçus</th>';
    strTable += '<th>Total clôturé</th>';
    strTable += '<th>Total en cours</th>';
    strTable += '<th>Total non-traité</th></thead>';


    $(".voir-synthese-semaine-flux").show();

    $.ajax({
        url : s_url+'reception/Pli_flux/syntheseSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
            $.each(data, function(i, item) {
                strTable += '<tr>';
                strTable += '<td style="background-color: white;">'+item.date_reception+'</td>';
                strTable += '<td>'+item.dernier_traitement+'</td>';
                strTable += '<td>'+item.nb_flux_recus+'</td>';
                strTable += '<td>'+item.nb_cloture+'</td>';
                strTable += '<td>'+item.nb_encours+'</td>';
                strTable += '<td>'+item.non_traite+'</td>';



            });
            strTable += '</table>';
            $(".voir-synthese-flux").show();
            $("#synthese-semaine-flux").html(strTable);
            $("#table-synthese-semaine").DataTable({
                scrollX:        true,
                scrollCollapse: true
            });

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });





}


function listePlis()
{
    $(".voir-liste-pli").show();
    $(".detail-encours-typo-table").show();
    if (!$.fn.dataTable.isDataTable('#table-liste-plis')) {
        tableListePlis = $('#table-liste-plis').DataTable({
            "scrollY"  : '600px',
			"scrollX"   : true,
            "processing": true,
			"pageLength": 25,
            // "scrollCollapse": true,
            // "deferRender":    true,
            // "scroller":       true,
            "serverSide": true,
            "ajax": {
                "data"    : myPost,
                "url":s_url+'reception/Pli/getListePli',
                "type": "POST"
                /* "data": function ( param ) {
                     param.dtIdPli 	    = $("#dev-id-pli").val();
                     param.dtTypage 	    = $("#dev-typage-par").val();
                     param.dtSaisie	    = $("#dev-saisie-par").val();
                     param.dtCtrl 		= $("#dev-ctrl-par").val();
                 },*/
            },
            "dom": 'lBfrtip',
            buttons: [
                {
                    text: 'Excel',
                    action: function (e, dt, node, config) {
                        $.ajax({
                            "url":s_url+'reception/Pli/exportListePlis',
                            "type": "POST",
                            "data"    : myPost,
                            "success": function(res, status, xhr) {
                                var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                                var csvURL = window.URL.createObjectURL(csvData);
                                var tempLink = document.createElement('a');
                                tempLink.href = csvURL;
                                tempLink.setAttribute('download', 'Réception_plis.xls'); 
                                document.body.appendChild(tempLink);
                                tempLink.click();
                            }
                        });
                    }
                }
            ],
            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false //set not orderable
                }
            ],
            /*"lengthMenu": [[10, 20,50, -1], [10, 20,50, "all"]],*/
            /*"dom": 'Bfrtip',
             "buttons": [
                 'copy', 'csv', 'excel', 'pdf', 'print'
             ],*/
            "fnInitComplete":function(){
                // $("#checkth").val('0');
                // $('.dataTables_scrollBody').slimscroll({
                    // axis: 'both', 
                    // distance: '3px',
                    // alwaysVisible:true
                // });
                
                // var checktest = $("#checkth").val();
                
                // if(checktest == '0')
                 // {
                     // $("#checkth").val('1');
                    // $("[data-dt-idx='1']").click();
                 // }
             
                // $('.dataTables_scrollBody').slimscroll({ axis: 'both'}); 
                // $(".dataTables_scrollHeadInner").css({"width":"100%"});
                // $(".table-liste").css({"width":"100%"});
                $(".detail-encours-typo-table").hide();
            },
            "fnDrawCallback": function( settings ) {
                // $(".dataTables_scrollHeadInner").css({"width":"100%"});
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.input.activate();
                $('.dataTables_scrollBody').slimscroll({ axis: 'both'}); 
            }
        });
    }




}

function listePlisFlux()
{
    $(".voir-liste-pli-flux").show();
    $(".detail-encours-typo-table").show();
    if (!$.fn.dataTable.isDataTable('#table-liste-flux')) {
        tableListePlis = $('#table-liste-flux').DataTable({
            "scrollY": '600px',
            "processing": true,
            "scrollCollapse": true,
            "deferRender":    true,
            "scroller":       true,
            "serverSide": true,
            "ajax": {
                "data"    : myPost,
                "url":s_url+'reception/Pli_flux/getListePli',
                "type": "POST"
                /* "data": function ( param ) {
                     param.dtIdPli 	    = $("#dev-id-pli").val();
                     param.dtTypage 	    = $("#dev-typage-par").val();
                     param.dtSaisie	    = $("#dev-saisie-par").val();
                     param.dtCtrl 		= $("#dev-ctrl-par").val();
                 },*/
            },
            "dom": 'lBfrtip',
            buttons: [
                {
                    text: 'Excel',
                    action: function (e, dt, node, config) {
                        $.ajax({
                            "url":s_url+'reception/Pli/exportListePlis',
                            "type": "POST",
                            "data"    : myPost,
                            "success": function(res, status, xhr) {
                                var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                                var csvURL = window.URL.createObjectURL(csvData);
                                var tempLink = document.createElement('a');
                                tempLink.href = csvURL;
                                tempLink.setAttribute('download', 'Réception_plis.xls');
                                document.body.appendChild(tempLink);
                                tempLink.click();
                            }
                        });
                    }
                }
            ],
            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false //set not orderable
                }
            ],
            /*"lengthMenu": [[10, 20,50, -1], [10, 20,50, "all"]],*/
            /*"dom": 'Bfrtip',
             "buttons": [
                 'copy', 'csv', 'excel', 'pdf', 'print'
             ],*/
            "fnInitComplete":function(){
                $('.dataTables_scrollBody').slimscroll({
                    axis: 'both',
                    distance: '3px',
                    alwaysVisible:false
                });
                $(".dataTables_scrollHeadInner").css({"width":"100%"});
                $(".table-liste").css({"width":"100%"});
                $(".detail-encours-typo-table").hide();
            },
            "drawCallback": function( settings ) {
                $(".dataTables_scrollHeadInner").css({"width":"100%"});
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.input.activate();
            }
        });
    }




}

function resultatInitial()
{
    $(".detail-typo-table").hide();
    $(".detail-clo-table").hide();
    $(".detail-encours-table").hide();
    $(".detail-encours-table-flux").hide();
    $(".detail-non-table").hide();
    $(".voir-synthese").hide();
    $(".voir-liste-pli").hide();
    $(".voir-synthese-semaine").hide();
}

function globaleReception()
{


    $.ajax({
        url : s_url+'reception/Pli/globaleReception',
        data    : myPost,
        type : 'POST',
        dataType : 'json',
        success : function(data, statut){


            $.each(data, function(index, record) {
                $.each(record, function(key, value) {
                    console.log(key + ': ' + value);
                    $("#"+key).text(value);
                   // alert("#"+key+"== "+value);
                });
            });



        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function globaleReceptionFlux()
{


    $.ajax({
        url : s_url+'reception/Pli_flux/globaleReception',
        data    : myPost,
        type : 'POST',
        dataType : 'json',
        success : function(data, statut){


            $.each(data, function(index, record) {
                $.each(record, function(key, value) {
                    console.log(key + ': ' + value);
                    $("#"+key).text(value);
                });
            });



        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function charger_plis()
{
    /* dateDebut   = $("#date-debut").val();
     dateFin     = $("#date-fin").val();*/
    societe     = $("#filtre-societe").val();
    source      = $("#filtre-source").val();
    dateDebut   = $("#date-debut").val() == null ? 0 : $("#date-debut").val();
    dateFin     = $("#date-fin").val() == null ? 0 : $("#date-fin").val();
    hideForm();
    //NProgress.start();
    //pliParTypologie();
    if(dateDebut == 0 || dateFin == 0)
    {
        $("#date-debut").focus();
        NProgress.done();
        return false;
    }

    if(getDate(dateDebut) > getDate(dateFin))
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de rectifier votre filtre date '},{type: 'danger'});
        $("#date-debut").focus();
        return false;
    }
    else
    {
        if(source == '3'){
            myPost = {dateDebut:dateDebut,dateFin:dateFin,societe:societe,source:source};
            $('#table-liste-plis').DataTable().destroy();
            verifDonnees();
        }
        else{
            myPost = {dateDebut:dateDebut,dateFin:dateFin,societe:societe,source:source};
            verifDonneesFlux();
        }
    }

}

function verifDonneesFlux()
{

    $(".ged-reception-flux").hide();

    $("#area_chart").empty();
    $("#recp-typo").empty();
    $("#donut-restant-mv").empty();
    $("#donut-cloture-pli").empty();
    $("#donut-cloture-mv").empty();
    $("#donut-cloture-mv").empty();
    $("#donut-en-cours-pli").empty();
    $("#donut-en-cours-mv").empty();

    $("#stat_rech").prop("disabled",true);

    //NProgress.start();
    $.ajax({
        url : s_url+'reception/Pli_flux/verifDonnees',
        type : 'POST',
        dataType : 'json',
        data    : myPost,
        success : function(data, statut){
            if(data[0].nb > 0 )
            {
                //alert('données dispo');
                //resultatInitial();
                pliParTypologieFlux();
                pliClotureFlux();

                /* pliParTypologie();

                 pliCloture();
                 pliEncours();

                 nonTraiteEncours();*/
                globaleReceptionFlux();
                nonTraiteEncoursFlux();
                pliEncoursFlux();
                fluxAnomalie();
                $(".ged-reception-flux").show();
                $(".voir-liste-pli-flux").hide();
                $(".voir-synthese-flux").hide();
                $(".voir-synthese-semaine-flux").hide();

            }
            else
            {
                $(".ged-reception-flux").hide();
                $("#stat_rech").prop("disabled",false);
                $.notify({title: '<strong>information !</strong>',message: ' Pas de données '},{type: 'danger'});
            }



        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function verifDonnees()
{


    $(".ged-reception").hide();

    $("#area_chart").empty();
    $("#recp-typo").empty();
    $("#donut-restant-mv").empty();
    $("#donut-cloture-pli").empty();
    $("#donut-cloture-mv").empty();
    $("#donut-cloture-mv").empty();
    $("#donut-en-cours-pli").empty();
    $("#donut-en-cours-mv").empty();

    $("#stat_rech").prop("disabled",true);

    NProgress.start();
    $.ajax({ 
        url : s_url+'reception/Pli/verifDonnees',
        type : 'POST',
        dataType : 'json',
        data    : myPost,
        success : function(data, statut){
            if(data[0].nb > 0 )
            {

                resultatInitial();
                pliParTypologie();
                pliCloture();

                
                
                nonTraiteEncours();
                pliEncours();
                pliFermer();
                // globaleReception();
                
                $(".ged-reception").show();
                $(".voir-liste-pli").hide();
                $(".voir-synthese").hide();
                $(".voir-synthese-semaine").hide();

            }
            else
            {
                $(".ged-reception").hide();
                $("#stat_rech").prop("disabled",false);
                $.notify({title: '<strong>information !</strong>',message: ' Pas de données '},{type: 'danger'});
            }



        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailEncoursParTypologie()
{

    $.ajax({
        url : s_url+'reception/Pli/encoursParTypologie',
        type : 'POST',
        dataType : 'html',
        data    : myPost,
        success : function(ret, statut){
            $(".detail-encours-typo-table").show();
            $("#encours-par-typo").html(ret);

            $('#encours-par-typo-table').DataTable({

                scrollY: '400px',
                scrollX: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1
                }
            });
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }
    });

}

function detailEncoursParTypologieFlux()
{

    $.ajax({
        url : s_url+'reception/Pli_flux/encoursParTypologie',
        type : 'POST',
        dataType : 'html',
        data    : myPost,
        success : function(ret, statut){
            $(".detail-encours-typo-table-flux").show();
            $("#encours-par-typo-flux").html(ret);

            $('#encours-par-typo-table').DataTable({

                scrollY: '400px',
                scrollX: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1
                }
            });
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }
    });

}

function pliFermer() {
    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">#Pli</th>\n' +
        '\t\t\t<th style="width:15%;cursor:pointer;" data-placement="left" data-toggle="tooltip" title="Le nombre des plis restant à traiter selon le type de statut">Nbr. Plis actuel</th>\n' +
        '\t\t\t<th style="width:15%;cursor:pointer;" data-placement="left" data-toggle="tooltip" title="Pourcentage des plis restant à traiter selon le type de  statut">% </th>\n' +
        '\t\t\t<th style="width:15%;cursor:pointer;" data-placement="left" data-toggle="tooltip" title="Le nombre des plis ayant passé par le statut">Pli passé par le statut</th>\n' +
        '\t\t\t<th style="width:15%;cursor:pointer;" data-placement="left" data-toggle="tooltip" title="Pourcentage des plis ayant passé par le statut">% </th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $.ajax({
        url : s_url+'reception/Pli/pliAvecAnomalie',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
		var item = data;
            var iR = item.pli_anomalie;
			// console.log("ITEM"+data);
            
				
                
					
					tableRecTypologie +="<tr><td style=''>KO SCAN</td><td>"+item.pli_ko_scan+"</td><td>"+item.pr_ko_scan+" %</td><td>"+item.pli_ko_scan_+"</td><td>"+item.pr_ko_scan_+" %</td></tr>";
					tableRecTypologie +="<tr><td style=''>KO DEFINITIF</td><td>"+item.pli_ko_definitif+"</td><td>"+item.pr_ko_definitif+" %</td><td>"+item.pli_ko_definitif_+"</td><td>"+item.pr_ko_definitif_+" %</td></tr>";
                    tableRecTypologie +="<tr><td style=''>KO SRC</td><td>"+item.pli_ko_src+"</td><td>"+item.pr_ko_src+" %</td><td>"+item.pli_ko_src_+"</td><td>"+item.pr_ko_src_+" %</td></tr>";
                    tableRecTypologie +="<tr><td style=''>KO EN ATTENTE</td><td>"+item.pli_en_attente+"</td><td>"+item.pr_en_attente+" %</td><td>"+item.pli_en_attente_+"</td><td>"+item.pr_en_attente_+" %</td></tr>";
					tableRecTypologie +="<tr><td style=''>KO INCONNU</td><td>"+item.pli_ko_inconnu+"</td><td>"+item.pr_ko_inconnu+" %</td><td>"+item.pli_ko_inconnu_+"</td><td>"+item.pr_ko_inconnu_+" %</td></tr>";
                    tableRecTypologie +="<tr><td style=''>KO CIRCULAIRE</td><td>"+item.pli_ko_circulaire+"</td><td>"+item.pr_ko_circulaire+" %</td><td>"+item.pli_ko_circulaire_+"</td><td>"+item.pr_ko_circulaire_+" %</td></tr>";
                    tableRecTypologie +="<tr><td style=''>KO EN COURS BAYARD</td><td>"+item.pli_ko_en_cours+"</td><td>"+item.pr_ko_en_cours+" %</td><td>"+item.pli_ko_en_cours_+"</td><td>"+item.pr_ko_en_cours_+" %</td></tr>";
                    tableRecTypologie +="<tr><td style=''>CI EDITEE</td><td>"+item.pli_ko_ci_editee+"</td><td>"+item.pr_ko_ci_editee+" %</td><td>"+item.pli_ko_ci_editee_+"</td><td>"+item.pr_ko_ci_editee_+" %</td></tr>";
                    tableRecTypologie +="<tr><td style=''>OK + CI</td><td>"+item.pli_ok_ci+"</td><td>"+item.pr_ok_ci+" %</td><td>"+item.pli_ok_ci_+"</td><td>"+item.pr_ok_ci_+" %</td></tr>";
                    //tableRecTypologie +="<tr><td style=''>CI ENVOYEE</td><td>"+item.pli_ci_envoyee+"</td><td>"+item.pr_ci_envoyee+" %</td></tr>";

                var totalPliActuel = parseInt(item.pli_ko_scan)+parseInt(item.pli_ko_definitif)+parseInt(item.pli_ko_src)+parseInt(item.pli_en_attente)+parseInt(item.pli_ko_inconnu)+parseInt(item.pli_ko_circulaire)+parseInt(item.pli_ko_ci_editee);
                var totalPli = parseInt(item.pli_ko_scan_)+parseInt(item.pli_ko_definitif_)+parseInt(item.pli_ko_src_)+parseInt(item.pli_en_attente_)+parseInt(item.pli_ko_inconnu_)+parseInt(item.pli_ko_circulaire_)+parseInt(item.pli_ko_ci_editee_);
                    tableRecTypologie +="<tr><td style=''><b>TOTAL</b></td><td><b>"+totalPliActuel+"</b></td><td></td><td><b>"+totalPli+"</b></td><td></td></tr>";
					
                    jsonMv.push({"label":"KO DEFINITIF", "value":item.pr_ko_definitif});
                    jsonMv.push({"label":"KO SRC", "value":item.pr_ko_src});
                    jsonMv.push({"label":"KO EN ATTENTE", "value":item.pr_en_attente});
                    jsonMv.push({"label":"KO CIRCULAIRE", "value":item.pr_ko_circulaire});
                    jsonMv.push({"label":"KO SCAN", "value":item.pr_ko_scan});
                    jsonMv.push({"label":"KO INCONNU", "value":item.pr_ko_inconnu});
                    jsonMv.push({"label":"KO EN COURS BAYARD", "value":item.pr_ko_en_cours});
                    jsonMv.push({"label":"CI EDITEE", "value":item.pr_ko_ci_editee});
                    jsonMv.push({"label":"OK + CI", "value":item.pr_ok_ci});

                    jsonPli.push({"label":"KO DEFINITIF", "value":item.pr_ko_definitif_});
                    jsonPli.push({"label":"KO SRC", "value":item.pr_ko_src_});
                    jsonPli.push({"label":"KO EN ATTENTE", "value":item.pr_en_attente_});
                    jsonPli.push({"label":"KO CIRCULAIRE", "value":item.pr_ko_circulaire_});
                    jsonPli.push({"label":"KO SCAN", "value":item.pr_ko_scan_});
                    jsonPli.push({"label":"KO INCONNU", "value":item.pr_ko_inconnu_});
                    jsonPli.push({"label":"KO EN COURS BAYARD", "value":item.pr_ko_en_cours_});
                    jsonPli.push({"label":"CI EDITEE", "value":item.pr_ko_ci_editee_});
                    jsonPli.push({"label":"OK + CI", "value":item.pr_ok_ci_});
                   // jsonMv.push({"label":"CI ENVOYEE", "value":item.pr_ci_envoyee});
                    iR++;
                // }
            
            
            tableRecTypologie +="</table>";
         
            console.log(jsonMv);
            
            if(iR > 0)
            {
                
               
                $("#recp-pli-fermer").html(tableRecTypologie);
                var couleurPli = ['#ffd8b1', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C'];
                var couleurMv = [ '#4FC0E8', '#DA70AE', '#2F4F4F', '#3BAEDA', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
               /* getMorris('donut', "donut-en-cours-pli",jsonPli,couleurPli);*/
               $('[data-toggle="tooltip"]').tooltip({container: 'body'}); 
              $("#donut-pli-fermer").empty();

               getMorris('donut', "donut-pli-fermer",jsonMv,couleurMv);
               getMorris('donut', "donut-pli-ko-passe",jsonPli,couleurPli);
               
            }
            else
            {
                $("#recp-pli-fermer").html("<b>Pas de données</b>");
            }
            
          

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            $('[data-toggle="tooltip"]').tooltip({container: 'body'}); 

        }

    });
}

function fluxAnomalie() {
    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:70%">#Flux</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Flux actuel</th>\n' +
        '\t\t\t<th style="width:15%">% </th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Flux</th>\n' +
        '\t\t\t<th style="width:15%">% </th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $.ajax({
        url : s_url+'reception/Pli_flux/fluxAnomalie',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var item = data;
            var iR = item.flux_anomalie;
            // console.log("ITEM"+data);




            tableRecTypologie +="<tr><td style=''>ANOMALIE MAIL</td><td>"+item.flux_ko_mail+"</td><td>"+item.perc_ko_mail+" %</td><td>"+item.ttl_ko_mail+"</td><td>"+item.ptl_ko_mail+"%</td></tr>";
            tableRecTypologie +="<tr><td style=''>ANOMALIE PJ</td><td>"+item.flux_ko_pj+"</td><td>"+item.perc_ko_pj+" %</td><td>"+item.ttl_ko_pj+"</td><td>"+item.ptl_ko_pj+"%</td></tr>";
            tableRecTypologie +="<tr><td style=''>ANOMALIE FICHIER</td><td>"+item.flux_ko_fichier+"</td><td>"+item.perc_ko_fichier+" %</td><td>"+item.ttl_ko_fichier+"</td><td>"+item.ptl_ko_fichier+"%</td></tr>";
            tableRecTypologie +="<tr><td style=''>KO SRC</td><td>"+item.flux_ko_src+"</td><td>"+item.perc_ko_src+" %</td><td>"+item.ttl_ko_src+"</td><td>"+item.ptl_ko_src+"%</td></tr>";
            tableRecTypologie +="<tr><td style=''>KO DEFINITIF</td><td>"+item.flux_ko_definitif+"</td><td>"+item.perc_ko_definitif+" %</td><td>"+item.ttl_ko_definitif+"</td><td>"+item.ptl_ko_definitif+"%</td></tr>";
            tableRecTypologie +="<tr><td style=''>KO INCONNU</td><td>"+item.flux_ko_inconnu+"</td><td>"+item.perc_ko_inconnu+" %</td><td>"+item.ttl_ko_inconnu+"</td><td>"+item.ptl_ko_inconnu+"%</td></tr>";
            tableRecTypologie +="<tr><td style=''>KO EN ATTENTE</td><td>"+item.flux_ko_attente+"</td><td>"+item.perc_ko_attente+" %</td><td>"+item.ttl_ko_attente+"</td><td>"+item.ptl_ko_attente+"%</td></tr>";
            tableRecTypologie +="<tr><td style=''>KO EN COURS BAYARD</td><td>"+item.flux_ko_en_cours+"</td><td>"+item.perc_ko_en_cours+" %</td><td>"+item.ttl_ko_en_cours+"</td><td>"+item.ptl_ko_en_cours+"%</td></tr>";
            tableRecTypologie +="<tr><td style=''>REJETE</td><td>"+item.flux_ko_rejete+"</td><td>"+item.perc_ko_rejete+" %</td><td>"+item.ttl_ko_rejete+"</td><td>"+item.ptl_ko_rejete+"%</td></tr>";
            tableRecTypologie +="<tr><td style=''>HORS PERIMETRE</td><td>"+item.flux_ko_hp+"</td><td>"+item.perc_ko_hp+" %</td><td>"+item.ttl_ko_hp+"</td><td>"+item.ptl_ko_hp+"%</td></tr>";

            jsonPli.push({"label":"ANOMALIE MAIL", "value":item.perc_ko_mail});
            jsonPli.push({"label":"ANOMALIE PJ", "value":item.perc_ko_pj});
            jsonPli.push({"label":"ANOMALIE FICHIER", "value":item.perc_ko_fichier});
            jsonPli.push({"label":"KO SRC", "value":item.perc_ko_src});
            jsonPli.push({"label":"KO DEFINITIF", "value":item.perc_ko_definitif});
            jsonPli.push({"label":"KO INCONNU", "value":item.perc_ko_inconnu});
            jsonPli.push({"label":"KO EN ATTENTE", "value":item.perc_ko_attente});
            jsonPli.push({"label":"KO EN COURS BAYARD", "value":item.perc_ko_en_cours});
            jsonPli.push({"label":"REJETE", "value":item.perc_ko_rejete});
            jsonPli.push({"label":"HORS PERIMETRE", "value":item.perc_ko_hp});

            jsonMv.push({"label":"ANOMALIE MAIL", "value":item.ptl_ko_mail});
            jsonMv.push({"label":"ANOMALIE PJ", "value":item.ptl_ko_pj});
            jsonMv.push({"label":"ANOMALIE FICHIER", "value":item.ptl_ko_fichier});
            jsonMv.push({"label":"KO SRC", "value":item.ptl_ko_src});
            jsonMv.push({"label":"KO DEFINITIF", "value":item.ptl_ko_definitif});
            jsonMv.push({"label":"KO INCONNU", "value":item.ptl_ko_inconnu});
            jsonMv.push({"label":"KO EN ATTENTE", "value":item.ptl_ko_attente});
            jsonMv.push({"label":"KO EN COURS BAYARD", "value":item.ptl_ko_en_cours});
            jsonMv.push({"label":"REJETE", "value":item.ptl_ko_rejete});
            jsonMv.push({"label":"HORS PERIMETRE", "value":item.ptl_ko_hp});

            iR++;
            // }


            tableRecTypologie +="</table>";

            console.log(jsonMv);

            if(iR > 0)
            {


                $("#recp-flux-anomalie").html(tableRecTypologie);
                var couleurPli = ['#ffd8b1', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C'];
                var couleurMv = [ '#4FC0E8', '#DA70AE', '#8A6D60', '#3BAEDA', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
                /* getMorris('donut', "donut-en-cours-pli",jsonPli,couleurPli);*/

                $("#donut-flux-anomalie").empty();

                getMorris('donut', "donut-anomalie-flux",jsonPli,couleurMv);
                getMorris('donut', "donut-ttlano-flux",jsonMv,couleurPli);
            }
            else
            {
                $("#recp-flux-anomalie").html("<b>Pas de données</b>");
            }



        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function avecAnomalieSemaine()
{
	var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-anomalie-semaine" style="size:10px;">';
   strTable += '<thead style ="background:white;">';
   strTable += '<tr>';
   strTable += '<th>Semaine</th>';
   strTable += '<th>KO SCAN</th>';
   strTable += '<th>KO DEFINITIF</th>';
   strTable += '<th>KO SRC</th>';
   strTable += '<th>KO EN ATTENTE</th>';
   strTable += '<th>KO INCONNU</th>';
   strTable += '<th>KO CIRCULAIRE</th>';
   strTable += '<th>KO EN COURS BAYARD</th>';
   strTable += '<th>CI EDITEE</th>';
   strTable += '<th>OK + CI</th>';
   strTable += '</tr></thead>';
	
   $(".detail-anomalie-table").show();
   $("#anomalie-table").html(chargementSpinner);
   $("#title-detail-anomalie-table").text("Nombre de plis avec anomalie/CI EDITEE/OK + CI par semaine");
    $.ajax({
        url : s_url+'reception/Pli/avecAnomalieSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           // console.log("xxx : "+data);
           $.each(data, function(i, item) {
				strTable += '<tr>';
				strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
				strTable += '<td>'+item.pli_ko_scan+'</td>';
				strTable += '<td>'+item.pli_ko_definitif+'</td>';
				strTable += '<td>'+item.pli_ko_src+'</td>';
				strTable += '<td>'+item.pli_en_attente+'</td>';
				strTable += '<td>'+item.pli_ko_inconnu+'</td>';
				strTable += '<td>'+item.pli_ko_circulaire+'</td>';
				strTable += '<td>'+item.pli_ko_en_cours+'</td>';
				strTable += '<td>'+item.pli_ko_ci_editee+'</td>';
				strTable += '<td>'+item.pli_ok_ci+'</td></tr>';
				//strTable += '<td>'+item.pli_ci_envoyee+'</td></tr>';
            

        });
        
		
        $(".detail-encours-table").show();
        $("#anomalie-table").html("");
        $("#anomalie-table").html(strTable);
        $("#table-par-anomalie-semaine").DataTable({
            scrollX:        true,
            scrollCollapse: true,
            scrollX: true,
            fixedHeader: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
             
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
    pliPasseEnKO('s');
}


function avecAnomalieJour()
{
	var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-anomalie-jour" style="size:10px;">';
   strTable += '<thead style ="background:white;">';
   strTable += '<tr>';
   strTable += '<th>Date courrier</th>';
   strTable += '<th>KO SCAN</th>';
   strTable += '<th>KO DEFINITIF</th>';
   strTable += '<th>KO SRC</th>';
   strTable += '<th>KO EN ATTENTE</th>';
   strTable += '<th>KO INCONNU</th>';
   strTable += '<th>KO CIRCULAIRE</th>';
   strTable += '<th>KO EN COURS BAYARD</th>';
   strTable += '<th>CI EDITEE</th>';
   strTable += '<th>OK + CI</th>';
   strTable += '</tr></thead>';
	
   $(".detail-anomalie-table").show();
   $("#anomalie-table").html(chargementSpinner);
   $("#title-detail-anomalie-table").text("Nombre de plis avec anomalie par jour");
    $.ajax({
        url : s_url+'reception/Pli/avecAnomalieJour',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.pli_ko_scan+'</td>';
            strTable += '<td>'+item.pli_ko_definitif+'</td>';
            strTable += '<td>'+item.pli_ko_src+'</td>';
            strTable += '<td>'+item.pli_en_attente+'</td>';
            strTable += '<td>'+item.pli_ko_inconnu+'</td>';
            strTable += '<td>'+item.pli_ko_circulaire+'</td>';
            strTable += '<td>'+item.pli_ko_en_cours+'</td>';
            strTable += '<td>'+item.pli_ko_ci_editee+'</td>';
            strTable += '<td>'+item.pli_ok_ci+'</td>';
            strTable += '</tr>';
             

        });
        
		
        $(".detail-encours-table").show();
        $("#anomalie-table").html("");
        $("#anomalie-table").html(strTable);
        $("#table-par-anomalie-jour").DataTable({
            scrollX:        true,
            scrollCollapse: true,
            scrollX: true,
            fixedHeader: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
             
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });

    pliPasseEnKO('j');
}

function syntheseFermerSemaine()
{
    voirSynthese();
    document.body.scrollTop = 0; 
    document.documentElement.scrollTop = 0; 
}



/**
 * 
 * @param {*} granularite 's' ou 'j' 
 */
function pliPasseEnKO(granularite ='j')
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-pli-passe-ko-par-jour" style="size:10px;">';
   strTable += '<thead style ="background:white;">';
   strTable += '<tr>';
   strTable += '<th>Date courrier</th>';
   strTable += '<th>KO SCAN</th>';
   strTable += '<th>KO DEFINITIF</th>';
   strTable += '<th>KO SRC</th>';
   strTable += '<th>KO EN ATTENTE</th>';
   strTable += '<th>KO INCONNU</th>';
   strTable += '<th>KO CIRCULAIRE</th>';
   strTable += '<th>KO EN COURS BAYARD</th>';
   strTable += '<th>CI EDITEE</th>';
   strTable += '<th>OK + CI</th>';
   strTable += '</tr></thead>';

   $("#title-detail-pli-passe-ko-table").text("Nombre de plis passé en statut (KO SCAN, KO DEFINITIF, KO SRC, KO EN ATTENTE, KO INCONNU, KO CIRCULAIRE, CI EDITEE ET OK CIRCULAIRE) par jour");

   if(granularite != 'j')	
   {
        $("#title-detail-pli-passe-ko-table").text("Nombre de plis passé en statut (KO SCAN, KO DEFINITIF, KO SRC, KO EN ATTENTE, KO INCONNU, KO CIRCULAIRE, CI EDITEE ET OK CIRCULAIRE) par semaine");
   }
   $(".detail-pli-passe-ko-table").show();
   $("#pli-passe-ko-table").html(chargementSpinner);
   
    myPost["granularite"] = granularite;
    $.ajax({
        url : s_url+'reception/Pli/pliPasseEnKO',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.pli_ko_scan+'</td>';
            strTable += '<td>'+item.pli_ko_definitif+'</td>';
            strTable += '<td>'+item.pli_ko_src+'</td>';
            strTable += '<td>'+item.pli_en_attente+'</td>';
            strTable += '<td>'+item.pli_ko_inconnu+'</td>';
            strTable += '<td>'+item.pli_ko_circulaire+'</td>';
            strTable += '<td>'+item.pli_ko_en_cours+'</td>';
            strTable += '<td>'+item.pli_ko_ci_editee+'</td>';
            strTable += '<td>'+item.pli_ok_ci+'</td>';
            strTable += '</tr>';
             

        });
        
		
        $(".detail-pli-passe-ko-table").show();
        $("#pli-passe-ko-table").html("");
        $("#pli-passe-ko-table").html(strTable);
        $("#table-pli-passe-ko-par-jour").DataTable({
            scrollX:        true,
            scrollCollapse: true,
            scrollX: true,
            fixedHeader: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
             
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}