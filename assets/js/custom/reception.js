var dateDebut;
var dateFin;
var societe;
var tableListePlis;
var myPost;

var chargementSpinner = '<center><div class="preloader"><div class="spinner-layer pl-black"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>';
$('.recpt-date').datepicker({
    todayBtn: "linked",
    language: "fr",
    autoclose: true,
    todayHighlight: true,
    toggleActive: true
    
});
 
function init_nsprogress(){
    $( document ).ajaxStart(function() {
        $(".preloader-reception").show();
        NProgress.start();
    });
    /*$( document ).ajaxComplete(function() {
        NProgress.done();
    });
    $(document).ajaxSuccess(fu  nction() {
        NProgress.done();
    });*/
    $(document).ajaxStop(function() {
        NProgress.done();
        $(".preloader-reception").hide();
    });

    $('[data-toggle="tooltip"]').tooltip({container: 'body'}); 
}

$(function () {
    init_nsprogress();
    hideForm();
   
});
function hideForm()
{
    $(".ged-reception").hide();
    $(".ged-reception-flux").hide();
    $(".detail-typo-table").hide();
    $(".detail-clo-table").hide();
    $(".detail-encours-table").hide();
    $(".detail-non-table").hide();
    $(".detail-encours-typo-table").hide();
    $(".voir-synthese").hide();
    $(".voir-liste-pli").hide();
    $(".voir-synthese-semaine").hide();
}

function getMorris(type, element,json,couleur) {

    $("#"+element).empty();
    Morris.Donut({
            element: element,
            data: json,
            colors: couleur,
            resize : false,
            hoverCallback: function(index, options, content) {
                var data = options.data[index];
                $(".morris-hover").html('<div>Custom label: ' + data.label + '</div>');
            },
            formatter: function (y) {
                return y + '%'
            }
        });

}

function pliParTypologie() {
    
    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table" id="table-t">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Typologie</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Mvm</th>\n' +
        '\t\t\t<th style="width:15%">% Plis</th>\n' +
        '\t\t\t<th style="width:15%">% Mvm</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];
    $("#recp-typo").html(chargementSpinner+"chargement ...");
    $.ajax({
        url : s_url+'reception/Pli/pliParTypologie',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var pliTotal = [];
            var pliMvt   = [];
            
            var iT = 0;
            $.each(data, function(i, item) {

                if(item.typologie != null)
                {
                    var tmpTypo = ''+item.typologie+'';
                    if(item.nb_pli_par_typo != 0)
                    {
                        jsonPli.push({"label":tmpTypo, "value":item.pourcent_pli});
                    }
                    if(item.nb_mvt_par_typo != 0)
                    {
                        jsonMv.push({"label":tmpTypo, "value":item.pourcent_mvt});
                    }
                    pliTotal.push(item.pli_total);
                    pliMvt.push(item.nb_mvt_total);
                    tableRecTypologie += "<tr><td style=''>"+item.typologie+"</td><td>"+item.nb_pli_par_typo+"</td><td>"+item.nb_mvt_par_typo+"</td><td>"+item.pourcent_pli+" %</td><td>"+item.pourcent_mvt+" %</td></tr>";
                    iT++;
                }

            });
            var uniquePliTotal      = Array.from(new Set(pliTotal));
            var maxPlitotal         = Math.max.apply(Math, uniquePliTotal);

            var uniquePliTotalMv    = Array.from(new Set(pliMvt));
            var maxPlitotalMv       = Math.max.apply(Math, uniquePliTotalMv);
            




            if(iT > 0)
            {
                var  tableRecTypologieTotal = "<tfoot><tr style='font-weight: bold;'><td >Total</td><td>"+maxPlitotal+"</td><td>"+maxPlitotalMv+"</td><td></td><td></td></tr></tfoot>";
                tableRecTypologieTotal +="</table>";
                tableRecTypologie +=tableRecTypologieTotal;
                $("#recp-typo").html(tableRecTypologie);
                //console.log(jsonPli);
                var couleurPli = ['#00EAFF', '#AA00FF', '#FF7F00', '#0095FF', '#f58231', '#FF00AA', '#FFD400', '#6AFF00', '#bcf60c', '#EDB9B9', '#8F2323','#00EAFF', '#AA00FF', '#FF7F00', '#0095FF', '#f58231'];
                var couleurMv = [ '#EE5567', '#43CAA9', '#EC87BF', '#4FC0E8', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];


                $('#table-t').DataTable({ "lengthMenu": [[6, 10, 20,25, -1], [6, 10, 20,25, "Toutes"]]});

                $("#donut-typo-pli").empty();
                $("#donut-typo-mv").empty();

                getMorris('donut', "donut-typo-pli",jsonPli,couleurPli);
                getMorris('donut', "donut-typo-mv",jsonMv,couleurMv);
            }
            else
            {
               notifUSer("Nombre de plis par typologie : 0");
                $("#recp-typo").html("<b>Pas de données</b>");
            }

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function pliCloture() {

    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table" id="table-c">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Typologie</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Mvm</th>\n' +
        '\t\t\t<th style="width:15%">% Plis</th>\n' +
        '\t\t\t<th style="width:15%">% Mvm</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $.ajax({
        url : s_url+'reception/Pli/pliCloture',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){

            var  tableRecTypologieTotal = "";
            

            var iC  = 0;
            var pliTotal = [];
            var pliMvt   = [];
            $.each(data, function(i, item) {

                if(item.typologie != null)
                {
                    var tmpTypo = ''+item.typologie+'';
                    if(item.nb_pli_par_typo != 0)
                    {
                        jsonPli.push({"label":tmpTypo, "value":item.pourcent_pli});
                    }
                    if(item.nb_mvt_par_typo != 0)
                    {
                        jsonMv.push({"label":tmpTypo, "value":item.pourcent_mvt});
                    }
                    pliTotal.push(item.pli_total);
                    pliMvt.push(item.nb_mvt_total);
                    tableRecTypologie += "<tr><td style=''>"+item.typologie+"</td><td>"+item.nb_pli_par_typo+"</td><td>"+item.nb_mvt_par_typo+"</td><td>"+item.pourcent_pli+" %</td><td>"+item.pourcent_mvt+" %</td></tr>";
                    iC++;
                }
            });

            
            var uniquePliTotal      = Array.from(new Set(pliTotal));
            var maxPlitotal         = Math.max.apply(Math, uniquePliTotal);

            var uniquePliTotalMv    = Array.from(new Set(pliMvt));
            var maxPlitotalMv       = Math.max.apply(Math, uniquePliTotalMv);

            
           
            if(iC > 0)
            {
                var  tableRecTypologieTotal = "<tfoot><tr style='font-weight: bold;'><td >Total</td><td>"+maxPlitotal+"</td><td>"+maxPlitotalMv+"</td><td></td><td></td></tr></tfoot>";
                tableRecTypologie +=tableRecTypologieTotal;
                tableRecTypologieTotal +="</table>";
                $("#recp-clo").html(tableRecTypologie);
               // console.log(jsonPli);
                var couleurPli = ['#ec87bf', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C','#ff6262'];
                var couleurMv = [ '#EE5567', '#43CAA9', '#EC87BF', '#4FC0E8', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
                $("#donut-cloture-pli").empty();
                $("#donut-cloture-mv").empty();
                getMorris('donut', "donut-cloture-pli",jsonPli,couleurPli);
                getMorris('donut', "donut-cloture-mv",jsonMv,couleurMv);
                
            }
            else
            {
                $("#recp-clo").html('<b>Pas de données</b>');
                notifUSer("Nombre de plis clôturés : 0");
            }
            $('#table-c').DataTable({ "lengthMenu": [[6, 10, 20,25, -1], [6, 10, 20,25, "Toutes"]]});
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function pliEncours() {
    NProgress.start();
    var tableRecTypologie = "";
    $("#recp-en-cours").html(chargementSpinner);
    tableRecTypologie += '' +
        '<table class="table">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Etape</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Mvm</th>\n' +
        '\t\t\t<th style="width:15%">% Plis</th>\n' +
        '\t\t\t<th style="width:15%">% Mvm</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $(".preloader-head").show();
    $.ajax({
        url : s_url+'reception/Pli/pliEncours',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var iE = 0;
            var  tableRecTypologieTotal = "<tr style='font-weight: bold;'><td >Total</td><td>"+data[0].pli_total+"</td><td>"+data[0].mvt_total+"</td><td></td><td></td></tr>";
            $.each(data, function(i, item) {
                if(item.libelle != null )
                {
                    var libelle = ''+item.libelle+'';

                    
                    jsonMv.push({"label":libelle, "value":item.pourcent_mv});
                    jsonPli.push({"label":libelle, "value":item.pourcent_pli});
                    tableRecTypologie += "<tr><td style=''>"+item.libelle+"</td><td>"+item.pli+"</td><td>"+item.mvt+"</td><td>"+item.pourcent_pli+" %</td><td>"+item.pourcent_mv+" %</td></tr>";
                    iE++;
                }
                $(".preloader-head").show();
            });
            tableRecTypologie +=tableRecTypologieTotal;
            tableRecTypologieTotal +="</table>";
            if(iE > 0)
            {
               // console.log(jsonPli);
                $("#recp-en-cours").html("");
                $("#recp-en-cours").html(tableRecTypologie);
                
                var couleurPli = ['#FE9E4F', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C'];
                var couleurMv = [ '#EE5567', '#43CAA9', '#EC87BF', '#4FC0E8', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
                
                $("#donut-en-cours-pli").empty();
                $("#donut-en-cours-pli").empty();

                getMorris('donut', "donut-en-cours-pli",jsonPli,couleurPli);
                getMorris('donut', "donut-en-cours-mv",jsonMv,couleurMv);
                NProgress.done();
            }
            else
            {
                $("#recp-en-cours").html("<b>Pas de données</b>");
                notifUSer("Nombre de plis en cours : 0");

            }
            
            $(".preloader-head").hide();

           globaleReception();
           $("#stat_rech").prop("disabled",false);

        },

        error : function(resultat, statut, erreur){
            $(".preloader-head").hide();

        },

        complete : function(resultat, statut){
            // console.log(resultat);
            $(".preloader-head").show();
        }

    });
}


function nonTraiteEncours() {
    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:70%">#Pli</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">% </th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $.ajax({
        url : s_url+'reception/Pli/nonTraiteEncours',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var iR = 0;
            $.each(data, function(i, item) {
                if(item.total != null )
                {
                   
                    tableRecTypologie +="<tr><td style=''>EN COURS</td><td>"+item.pli_en_cours+"</td><td>"+item.pourcent_en_cours+" %</td></tr>";
                    tableRecTypologie +="<tr><td style=''>NON TRAITE</td><td>"+item.pli_non_traite+"</td><td>"+item.pourcent_non_traite+" %</td></tr>";
                    jsonMv.push({"label":"EN COURS", "value":item.pourcent_en_cours});
                    jsonMv.push({"label":"NON TRAITE", "value":item.pourcent_non_traite});
                    iR++;
                }
            });
            
            tableRecTypologie +="</table>";

            console.log(jsonMv);
            
            if(iR > 0)
            {
                
               
                $("#recp-rest-traiter").html(tableRecTypologie);
                var couleurPli = ['#ffd8b1', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C'];
                var couleurMv = [ '#4FC0E8', '#43CAA9', '#EC87BF', '#EE5567', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
               /* getMorris('donut', "donut-en-cours-pli",jsonPli,couleurPli);*/

              $("#donut-restant-mv").empty();

               getMorris('donut', "donut-restant-mv",jsonMv,couleurMv);
            }
            else
            {
                $("#recp-rest-traiter").html("<b>Pas de données</b>");
            }
            
          

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }
 
    });
}


function detailTypologieParJour()
{
   var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-typo" style="size:10px;">';
   strTable += '<thead>';
   strTable += '<tr>';
   strTable += '<th style="background-color: white;">date courrier</th>';
   strTable += '<th>Non typé</th>';
   strTable += '<th>3 4 sg la croix</th>';
   strTable += '<th>Annulation</th>';
   strTable += '<th>Changement adresse</th>';
   strTable += '<th>Changement de destinataire</th>';
   strTable += '<th>Changement titre</th>';
   strTable += '<th>Cheque seul</th>';
   strTable += '<th>Comite d entreprise</th>';
   strTable += '<th>Courrier</th>';
   strTable += '<th>Creation</th>';
   strTable += '<th>Creation vpc</th>';
   strTable += '<th>Croisement relance</th>';
   strTable += '<th>Envoi de numero</th>';
   strTable += '<th>Famille complexe</th>';
   strTable += '<th>Famille simple</th>';
   strTable += '<th>Gratuit</th>';
   strTable += '<th>Institutionnel complexe</th>';
   strTable += '<th>Institutionnel simple</th>';
   strTable += '<th>Paiement facture</th>';
   strTable += '<th>Reab avec courrier</th>';
   strTable += '<th>Reab complexe</th>';
   strTable += '<th>Reab simple</th>';
   strTable += '<th>Reexpedition</th>';
   strTable += '<th>Refus portage</th>';
   strTable += '<th>Retour pnd</th>';
   strTable += '<th>Stop relance</th>';
   strTable += '<th>Transfert src</th></tr></thead>';
    $(".detail-typo-table").show();
    $("#typologie-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/typologieJournalier',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           // 
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.info_0+'</td>';
            strTable += '<td>'+item.info_2+'</td>';
            strTable += '<td>'+item.info_3+'</td>';
            strTable += '<td>'+item.info_4+'</td>';
            strTable += '<td>'+item.info_5+'</td>';
            strTable += '<td>'+item.info_6+'</td>';
            strTable += '<td>'+item.info_7+'</td>';
            strTable += '<td>'+item.info_8+'</td>';
            strTable += '<td>'+item.info_9+'</td>';
            strTable += '<td>'+item.info_10+'</td>';
            strTable += '<td>'+item.info_11+'</td>';
            strTable += '<td>'+item.info_12+'</td>';
            strTable += '<td>'+item.info_13+'</td>';
            strTable += '<td>'+item.info_14+'</td>';
            strTable += '<td>'+item.info_15+'</td>';
            strTable += '<td>'+item.info_16+'</td>';
            strTable += '<td>'+item.info_17+'</td>';
            strTable += '<td>'+item.info_18+'</td>';
            strTable += '<td>'+item.info_19+'</td>';
            strTable += '<td>'+item.info_20+'</td>';
            strTable += '<td>'+item.info_21+'</td>';
            strTable += '<td>'+item.info_22+'</td>';
            strTable += '<td>'+item.info_23+'</td>';
            strTable += '<td>'+item.info_24+'</td>';
            strTable += '<td>'+item.info_25+'</td>';
            strTable += '<td>'+item.info_26+'</td>';
            strTable += '<td>'+item.info_27+'</td></tr>';
            

        });
        $("#typologie-par-jour").html("");
        $("#typologie-par-jour").html(strTable);
        $("#table-par-typo").DataTable({
            scrollY: '400px',
            scrollX: true,
            fixedHeader: true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 1
             
            },
            
        });
        $("#title-detail").text("Nombre de plis par typologie par date");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}
function detailTypologieParSemaine()
{
   var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-typo">';
   strTable += '<thead>';
   strTable += '<tr>';
   strTable += '<th style="background-color: white;">date courrier</th>';
   strTable += '<th>Non typé</th>';
   strTable += '<th>3 4 sg la croix</th>';
   strTable += '<th>Annulation</th>';
   strTable += '<th>Changement adresse</th>';
   strTable += '<th>Changement de destinataire</th>';
   strTable += '<th>Changement titre</th>';
   strTable += '<th>Cheque seul</th>';
   strTable += '<th>Comite d entreprise</th>';
   strTable += '<th>Courrier</th>';
   strTable += '<th>Creation</th>';
   strTable += '<th>Creation vpc</th>';
   strTable += '<th>Croisement relance</th>';
   strTable += '<th>Envoi de numero</th>';
   strTable += '<th>Famille complexe</th>';
   strTable += '<th>Famille simple</th>';
   strTable += '<th>Gratuit</th>';
   strTable += '<th>Institutionnel complexe</th>';
   strTable += '<th>Institutionnel simple</th>';
   strTable += '<th>Paiement facture</th>';
   strTable += '<th>Reab avec courrier</th>';
   strTable += '<th>Reab complexe</th>';
   strTable += '<th>Reab simple</th>';
   strTable += '<th>Reexpedition</th>';
   strTable += '<th>Refus portage</th>';
   strTable += '<th>Retour pnd</th>';
   strTable += '<th>Stop relance</th>';
   strTable += '<th>Transfert src</th></tr></thead>';

   $(".detail-typo-table").show();
   $("#typologie-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/typologieSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           // 
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">S'+item.date_courrier+'</td>';
            strTable += '<td>'+item.info_0+'</td>';
            strTable += '<td>'+item.info_2+'</td>';
            strTable += '<td>'+item.info_3+'</td>';
            strTable += '<td>'+item.info_4+'</td>';
            strTable += '<td>'+item.info_5+'</td>';
            strTable += '<td>'+item.info_6+'</td>';
            strTable += '<td>'+item.info_7+'</td>';
            strTable += '<td>'+item.info_8+'</td>';
            strTable += '<td>'+item.info_9+'</td>';
            strTable += '<td>'+item.info_10+'</td>';
            strTable += '<td>'+item.info_11+'</td>';
            strTable += '<td>'+item.info_12+'</td>';
            strTable += '<td>'+item.info_13+'</td>';
            strTable += '<td>'+item.info_14+'</td>';
            strTable += '<td>'+item.info_15+'</td>';
            strTable += '<td>'+item.info_16+'</td>';
            strTable += '<td>'+item.info_17+'</td>';
            strTable += '<td>'+item.info_18+'</td>';
            strTable += '<td>'+item.info_19+'</td>';
            strTable += '<td>'+item.info_20+'</td>';
            strTable += '<td>'+item.info_21+'</td>';
            strTable += '<td>'+item.info_22+'</td>';
            strTable += '<td>'+item.info_23+'</td>';
            strTable += '<td>'+item.info_24+'</td>';
            strTable += '<td>'+item.info_25+'</td>';
            strTable += '<td>'+item.info_26+'</td>';
            strTable += '<td>'+item.info_27+'</td></tr>';
            

        });
        $("#typologie-par-jour").html("");
        $("#typologie-par-jour").html(strTable);
        $("#table-par-typo").DataTable({
            scrollX:        true,
            scrollX: true,
            fixedHeader: true,
        scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 1
              
            }
        });
        $("#title-detail").text("Nombre de plis par typologie par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function fermer(elm)
{
    $("."+elm).hide();
}




function detailCloParJour()
{
   var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-clo-typo" style="size:10px;">';
   strTable += '<thead>';
   strTable += '<tr>';
   strTable += '<th style="background-color: white;">date courrier</th>';
   strTable += '<th>Non typé</th>';
   strTable += '<th>3 4 sg la croix</th>';
   strTable += '<th>Annulation</th>';
   strTable += '<th>Changement adresse</th>';
   strTable += '<th>Changement de destinataire</th>';
   strTable += '<th>Changement titre</th>';
   strTable += '<th>Cheque seul</th>';
   strTable += '<th>Comite d entreprise</th>';
   strTable += '<th>Courrier</th>';
   strTable += '<th>Creation</th>';
   strTable += '<th>Creation vpc</th>';
   strTable += '<th>Croisement relance</th>';
   strTable += '<th>Envoi de numero</th>';
   strTable += '<th>Famille complexe</th>';
   strTable += '<th>Famille simple</th>';
   strTable += '<th>Gratuit</th>';
   strTable += '<th>Institutionnel complexe</th>';
   strTable += '<th>Institutionnel simple</th>';
   strTable += '<th>Paiement facture</th>';
   strTable += '<th>Reab avec courrier</th>';
   strTable += '<th>Reab complexe</th>';
   strTable += '<th>Reab simple</th>';
   strTable += '<th>Reexpedition</th>';
   strTable += '<th>Refus portage</th>';
   strTable += '<th>Retour pnd</th>';
   strTable += '<th>Stop relance</th>';
   strTable += '<th>Transfert src</th></tr></thead>';
   
    $(".detail-clo-table").show();
    $("#clo-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/cloJournalier',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           // 
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.info_0+'</td>';
            strTable += '<td>'+item.info_2+'</td>';
            strTable += '<td>'+item.info_3+'</td>';
            strTable += '<td>'+item.info_4+'</td>';
            strTable += '<td>'+item.info_5+'</td>';
            strTable += '<td>'+item.info_6+'</td>';
            strTable += '<td>'+item.info_7+'</td>';
            strTable += '<td>'+item.info_8+'</td>';
            strTable += '<td>'+item.info_9+'</td>';
            strTable += '<td>'+item.info_10+'</td>';
            strTable += '<td>'+item.info_11+'</td>';
            strTable += '<td>'+item.info_12+'</td>';
            strTable += '<td>'+item.info_13+'</td>';
            strTable += '<td>'+item.info_14+'</td>';
            strTable += '<td>'+item.info_15+'</td>';
            strTable += '<td>'+item.info_16+'</td>';
            strTable += '<td>'+item.info_17+'</td>';
            strTable += '<td>'+item.info_18+'</td>';
            strTable += '<td>'+item.info_19+'</td>';
            strTable += '<td>'+item.info_20+'</td>';
            strTable += '<td>'+item.info_21+'</td>';
            strTable += '<td>'+item.info_22+'</td>';
            strTable += '<td>'+item.info_23+'</td>';
            strTable += '<td>'+item.info_24+'</td>';
            strTable += '<td>'+item.info_25+'</td>';
            strTable += '<td>'+item.info_26+'</td>';
            strTable += '<td>'+item.info_27+'</td></tr>';
            

        });
        $("#clo-par-jour").html("");
        $("#clo-par-jour").html(strTable);
        $("#table-clo-typo").DataTable({
            scrollX:        true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
        $("#title-detail-clo").text("Nombre de plis clôturé par typologie par date");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailCloParSemaine()
{
   var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-clo-sem" style="size:10px;">';
   strTable += '<thead>';
   strTable += '<tr>';
   strTable += '<th style="background-color: white;">date courrier</th>';
   strTable += '<th>Non typé</th>';
   strTable += '<th>3 4 sg la croix</th>';
   strTable += '<th>Annulation</th>';
   strTable += '<th>Changement adresse</th>';
   strTable += '<th>Changement de destinataire</th>';
   strTable += '<th>Changement titre</th>';
   strTable += '<th>Cheque seul</th>';
   strTable += '<th>Comite d entreprise</th>';
   strTable += '<th>Courrier</th>';
   strTable += '<th>Creation</th>';
   strTable += '<th>Creation vpc</th>';
   strTable += '<th>Croisement relance</th>';
   strTable += '<th>Envoi de numero</th>';
   strTable += '<th>Famille complexe</th>';
   strTable += '<th>Famille simple</th>';
   strTable += '<th>Gratuit</th>';
   strTable += '<th>Institutionnel complexe</th>';
   strTable += '<th>Institutionnel simple</th>';
   strTable += '<th>Paiement facture</th>';
   strTable += '<th>Reab avec courrier</th>';
   strTable += '<th>Reab complexe</th>';
   strTable += '<th>Reab simple</th>';
   strTable += '<th>Reexpedition</th>';
   strTable += '<th>Refus portage</th>';
   strTable += '<th>Retour pnd</th>';
   strTable += '<th>Stop relance</th>';
   strTable += '<th>Transfert src</th></tr></thead>';

   $(".detail-clo-table").show();
   $("#clo-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/cloTypologieSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           // 
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">S'+item.date_courrier+'</td>';
            strTable += '<td>'+item.info_0+'</td>';
            strTable += '<td>'+item.info_2+'</td>';
            strTable += '<td>'+item.info_3+'</td>';
            strTable += '<td>'+item.info_4+'</td>';
            strTable += '<td>'+item.info_5+'</td>';
            strTable += '<td>'+item.info_6+'</td>';
            strTable += '<td>'+item.info_7+'</td>';
            strTable += '<td>'+item.info_8+'</td>';
            strTable += '<td>'+item.info_9+'</td>';
            strTable += '<td>'+item.info_10+'</td>';
            strTable += '<td>'+item.info_11+'</td>';
            strTable += '<td>'+item.info_12+'</td>';
            strTable += '<td>'+item.info_13+'</td>';
            strTable += '<td>'+item.info_14+'</td>';
            strTable += '<td>'+item.info_15+'</td>';
            strTable += '<td>'+item.info_16+'</td>';
            strTable += '<td>'+item.info_17+'</td>';
            strTable += '<td>'+item.info_18+'</td>';
            strTable += '<td>'+item.info_19+'</td>';
            strTable += '<td>'+item.info_20+'</td>';
            strTable += '<td>'+item.info_21+'</td>';
            strTable += '<td>'+item.info_22+'</td>';
            strTable += '<td>'+item.info_23+'</td>';
            strTable += '<td>'+item.info_24+'</td>';
            strTable += '<td>'+item.info_25+'</td>';
            strTable += '<td>'+item.info_26+'</td>';
            strTable += '<td>'+item.info_27+'</td></tr>';
            

        });
        $(".detail-clo-table").show();
        $("#clo-par-jour").html("");
        $("#clo-par-jour").html(strTable);
        $("#table-par-clo-sem").DataTable({
            scrollX:        true,
            scrollX: true,
            fixedHeader: true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
        $("#title-detail-clo").text("Nombre de plis clôturé par typologie par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailEncoursParJour()
{
   var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-encours-jour" style="size:10px;">';
   strTable += '<thead style ="background:white;">';
   strTable += '<tr>';
   strTable += '<th>Date courrier..</th>';
   strTable += '<th>En cours de typage</th>';
   strTable += '<th>En attente de saisie</th>';
   strTable += '<th>En cours de saisie</th>';
   strTable += '<th>En attente de contrôle</th>';
   strTable += '<th>En attente de consigne</th>';
   strTable += '<th>En cours de contrôle</th>';
   strTable += '<th>En attente de retraitement</th>';
   strTable += '<th>En cours de retraitement</th></tr></thead>';
	alert("ok");
   $(".detail-encours-table").show();
   $("#encours-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/encoursParJour',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           console.log("xxx : "+data);
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.en_cours_typage+'</td>';
            strTable += '<td>'+item.en_attente_saisie+'</td>';
            strTable += '<td>'+item.en_cours_saisie+'</td>';
            strTable += '<td>'+item.en_attente_controle+'</td>';
            strTable += '<td>'+item.pli_en_cours_controle+'</td>';
            strTable += '<td>'+item.en_attente_retraitement+'</td>';
            strTable += '<td>'+item.en_cours_retraitement+'</td></tr>';
            

        });
        
        $(".detail-encours-table").show();
        $("#encours-par-jour").html("");
        $("#encours-par-jour").html(strTable);
        $("#table-par-encours-jour").DataTable({
            scrollX:        true,
            scrollCollapse: true,
            scrollX: true,
            fixedHeader: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
             $("#title-detail-encours").text("Nombre de plis en cours par jour");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}
function detailEncoursParSemaine()
{
   var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-encours-jour" style="size:10px;">';
   strTable += '<thead style ="background:white;">';
   strTable += '<tr>';
   strTable += '<th>Semaine</td>';
   strTable += '<th>En cours de typage</th>';
   strTable += '<th>En attente de saisie</th>';
   strTable += '<th>En cours de saisie</th>';
   strTable += '<th>En attente de contrôle</th>';
   strTable += '<th>En attente de consigne</th>';
   strTable += '<th>En cours de contrôle</th>';
   strTable += '<th>Chèque contrôle OK</th>';
   strTable += '<th>Chèque correction OK"</th>';
   strTable += '<th>Chèque validation OK</th></tr></thead>';
   $("#encours-par-jour").html(chargementSpinner);
   $(".detail-encours-table").show();

    $.ajax({
        url : s_url+'reception/Pli/encoursParSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           console.log(data);
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.en_cours_typage+'</td>';
            strTable += '<td>'+item.en_attente_saisie+'</td>';
            strTable += '<td>'+item.en_cours_saisie+'</td>';
            strTable += '<td>'+item.en_attente_controle+'</td>';
            strTable += '<td>'+item.pli_en_cours_controle+'</td>';
            strTable += '<td>'+item.en_attente_consigne+'</td>';
            strTable += '<td>'+item.chq_controle_ok+'</td>';
            strTable += '<td>'+item.chq_correction_ok+'</td>';
            strTable += '<td>'+item.chq_validation_ok+'</td></tr>';
            

        });
        
        $(".detail-encours-table").show();
        $("#encours-par-jour").html("");
        $("#encours-par-jour").html(strTable);
        $("#table-par-encours-jour").DataTable({
            scrollX:        true,
            scrollCollapse: true,
            scrollX: true,
            fixedHeader: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
             $("#title-detail-encours").text("Nombre de plis en cours par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}



function nonTraiteSemaine()
{
   var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-non-jour" style="size:10px;">';
   strTable += '<thead style ="background:white;font-size: 11px;font-weight: bold;    ">';
   strTable += '<tr>';
   strTable += '<td>Semaine</td>';
   strTable += '<td>NON TRAITE</td>';
   strTable += '<td>EN COURS</td></thead>';
   

   $(".detail-non-table").show();
   $("#non-par-jour").html(chargementSpinner);
    $.ajax({
        url : s_url+'reception/Pli/nonTraiteSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           console.log(data);
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">S'+item.date_courrier+'</td>';
            strTable += '<td>'+item.pli_en_cours+'</td>';
            strTable += '<td>'+item.pli_non_traite+'</td>';
           
            

        });
        
        $(".detail-non-table").show();
        $("#non-par-jour").html("");
        $("#non-par-jour").html(strTable);
        $("#table-par-non-jour").DataTable({
            scrollX:        true,
            scrollCollapse: true,
            scrollX: true,
            fixedHeader: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
             $("#title-detail-non").text("Nombre de plis par semaine");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}


function nonTraiteJour()
{
   var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-par-non-jour" style="size:10px;">';
   strTable += '<thead style ="background:white;">';
   strTable += '<tr>';
   strTable += '<th>Date courrier</th>';
   strTable += '<th>NON TRAITE</th>';
   strTable += '<th>EN COURS</th></thead>';
   

   $(".detail-non-table").show();
   $("#non-par-jour").html(chargementSpinner);

    $.ajax({
        url : s_url+'reception/Pli/nonTraiteJour',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           console.log(data);
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.pli_en_cours+'</td>';
            strTable += '<td>'+item.pli_non_traite+'</td>';
           
            

        });
        strTable += '</table>';
        $(".detail-non-table").show();
        $("#non-par-jour").html("");
        $("#non-par-jour").html(strTable);

        $("#table-par-non-jour").DataTable({
            scrollX:        true,
            scrollCollapse: true,
            scrollX: true,
            fixedHeader: true,
            fixedColumns:   {
                leftColumns: 1
             
            }
        });
             $("#title-detail-non").text("Nombre de plis par jour");
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}




function voirSynthese()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-synthese-jour">';
   strTable += '<thead>';
   strTable += '<tr>';
   strTable += '<th>Date de réception</th>';
   strTable += '<th>Date dernier traitement</th>';
   strTable += '<th>Nbr. plis reçus</th>';
   strTable += '<th>Total clôturé</th>';
   strTable += '<th>Total en cours</th>';
   strTable += '<th>Total non-traité</th></thead>';
   

   $(".detail-encours-typo-table").show();

    $.ajax({
        url : s_url+'reception/Pli/syntheseJournaliere',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           console.log(data);
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.dernier_traitement+'</td>';
            strTable += '<td>'+item.nb_pli_recus+'</td>';
            strTable += '<td>'+item.nb_cloture+'</td>';
            strTable += '<td>'+item.nb_encours+'</td>';
            strTable += '<td>'+item.non_traite+'</td>';
            
            
           
            

        });
        strTable += '</table>';
        $(".voir-synthese").show();
        $("#synthese-journaliere").html(strTable);
        
        voirSyntheseSemaine();
        $(".detail-encours-typo-table").hide();
        $("#table-synthese-jour").DataTable({
            scrollX:        true,
            scrollCollapse: true
        });
             
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}


function voirSyntheseSemaine()
{
    var strTable = '<table class="table table-bordered table-striped table-hover dataTable" width="100%" id="table-synthese-semaine">';
   strTable += '<thead>';
   strTable += '<tr>';
   strTable += '<th>Date de réception</th>';
   strTable += '<th>Date dernier traitement</th>';
   strTable += '<th>Nbr. plis reçus</th>';
   strTable += '<th>Total clôturé</th>';
   strTable += '<th>Total en cours</th>';
   strTable += '<th>Total non-traité</th></thead>';
   

   $(".voir-synthese-semaine").show();

    $.ajax({
        url : s_url+'reception/Pli/syntheseSemaine',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        success : function(data, statut){
           $.each(data, function(i, item) {
            strTable += '<tr>';
            strTable += '<td style="background-color: white;">'+item.date_courrier+'</td>';
            strTable += '<td>'+item.dernier_traitement+'</td>';
            strTable += '<td>'+item.nb_pli_recus+'</td>';
            strTable += '<td>'+item.nb_cloture+'</td>';
            strTable += '<td>'+item.nb_encours+'</td>';
            strTable += '<td>'+item.non_traite+'</td>';
           
            

        });
        strTable += '</table>';
        $(".voir-synthese").show();
        $("#synthese-semaine").html(strTable);
        $("#table-synthese-semaine").DataTable({
            scrollX:        true,
            scrollCollapse: true
        });
             
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });




   
}


function listePlis()
{
    $(".voir-liste-pli").show();
    $(".detail-encours-typo-table").show();
   if (!$.fn.dataTable.isDataTable('#table-liste-plis')) {
        tableListePlis = $('#table-liste-plis').DataTable({
            "scrollY": '600px',
            "processing": true,
            "scrollCollapse": true,
            "deferRender":    true,
            "scroller":       true,
            "serverSide": true,
            "ajax": {
                 "data"    : myPost,
                "url":s_url+'reception/Pli/getListePli',
                "type": "POST"
               /* "data": function ( param ) {
                    param.dtIdPli 	    = $("#dev-id-pli").val();
                    param.dtTypage 	    = $("#dev-typage-par").val();
                    param.dtSaisie	    = $("#dev-saisie-par").val();
                    param.dtCtrl 		= $("#dev-ctrl-par").val();
                },*/
            },
            "dom": 'lBfrtip',
            buttons: [
                 {
                    text: 'Excel',
                    action: function (e, dt, node, config) {
                        $.ajax({
                            "url":s_url+'reception/Pli/exportListePlis',
                            "type": "POST",
                            "data"    : myPost,
                            "success": function(res, status, xhr) {
                                var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                                var csvURL = window.URL.createObjectURL(csvData);
                                var tempLink = document.createElement('a');
                                tempLink.href = csvURL;
                                tempLink.setAttribute('download', 'Réception_plis.xls');
                                document.body.appendChild(tempLink);
                                tempLink.click();
                            }
                        });
                    }
                }
            ],
            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false //set not orderable
                }
            ],
            /*"lengthMenu": [[10, 20,50, -1], [10, 20,50, "all"]],*/
           /*"dom": 'Bfrtip',
            "buttons": [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],*/
            "fnInitComplete":function(){
                $('.dataTables_scrollBody').slimscroll({
                    axis: 'both',
                    distance: '3px',
                    alwaysVisible:false
                });
                $(".dataTables_scrollHeadInner").css({"width":"100%"});
                $(".table-liste").css({"width":"100%"});
                $(".detail-encours-typo-table").hide();
            },
            "drawCallback": function( settings ) {
                $(".dataTables_scrollHeadInner").css({"width":"100%"});
                $.AdminBSB.dropdownMenu.activate();
                $.AdminBSB.select.activate();
                $.AdminBSB.input.activate();
            }
        });
    }
    
    
    
    
}

function resultatInitial()
{
    $(".detail-typo-table").hide();
    $(".detail-clo-table").hide();
    $(".detail-encours-table").hide();
    $(".detail-non-table").hide();
    $(".voir-synthese").hide();
    $(".voir-liste-pli").hide();
    $(".voir-synthese-semaine").hide();
}

function globaleReception()
{
    

    $.ajax({
        url : s_url+'reception/Pli/globaleReception',
        data    : myPost,
        type : 'POST',
        dataType : 'json',
        success : function(data, statut){
            

            $.each(data, function(index, record) {
                $.each(record, function(key, value) {
                    console.log(key + ': ' + value);
                    $("#"+key).text(value);
                });
            });

        
             
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function charger_plis()
{
   /* dateDebut   = $("#date-debut").val();
    dateFin     = $("#date-fin").val();*/
    
    societe     = $("#filtre-societe").val();

    dateDebut   = $("#date-debut").val() == null ? 0 : $("#date-debut").val();
    dateFin     = $("#date-fin").val() == null ? 0 : $("#date-fin").val();
    hideForm();
    NProgress.start();
    //pliParTypologie();
    if(dateDebut == 0 || dateFin == 0)
    {
        $("#date-debut").focus();
        NProgress.done();
        return false;
    }

    if(getDate(dateDebut) > getDate(dateFin))
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de rectifier votre filtre date '},{type: 'danger'});
        $("#date-debut").focus();
        return false;
    }
    else
    {
        myPost = {dateDebut:dateDebut,dateFin:dateFin,societe};
        $('#table-liste-plis').DataTable().destroy();
        verifDonnees();
    }
   
}

function verifDonnees() 
{
   
    
    $(".ged-reception").hide();
    $(".ged-reception-flux").hide();
    $("#area_chart").empty();
    $("#recp-typo").empty();
    $("#donut-restant-mv").empty();
    $("#donut-cloture-pli").empty();
    $("#donut-cloture-mv").empty();
    $("#donut-cloture-mv").empty();
    $("#donut-en-cours-pli").empty();
    $("#donut-en-cours-mv").empty();
    
    $("#stat_rech").prop("disabled",true);

    NProgress.start();
    $.ajax({
        url : s_url+'reception/Pli/verifDonnees',
        type : 'POST',
        dataType : 'json',
        data    : myPost,
        success : function(data, statut){
            if(data[0].nb > 0 )
            {
                $(".value-span").text("0");
                resultatInitial();
                pliParTypologie();
                pliCloture();
               
               /* pliParTypologie();   
                
                pliCloture();
                pliEncours();

                nonTraiteEncours();*/
                globaleReception();
                nonTraiteEncours();
                pliEncours();
                pliFermer();
                $(".ged-reception").show();
                $(".voir-liste-pli").hide();
                $(".voir-synthese").hide();
                $(".voir-synthese-semaine").hide();

            }
            else
            {
                $(".ged-reception").hide();
                $("#stat_rech").prop("disabled",false);
                $.notify({title: '<strong>information !</strong>',message: ' Pas de données '},{type: 'danger'});
            }
            
        
             
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function detailEncoursParTypologie()
{

    $.ajax({
        url : s_url+'reception/Pli/encoursParTypologie',
        type : 'POST',
        dataType : 'html',
        data    : myPost,
        success : function(ret, statut){
            $(".detail-encours-typo-table").show();
            $("#encours-par-typo").html(ret);

            $('#encours-par-typo-table').DataTable({
                
                scrollY: '400px',
                scrollX: true,
                fixedHeader: true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1
                }
            });
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }
    });
    
}

function pliFermer() {
    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:70%">#Pli</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. Plis</th>\n' +
        '\t\t\t<th style="width:15%">% </th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $.ajax({
        url : s_url+'reception/Pli_flux/pliFermer',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var iR = 0;
            $.each(data, function(i, item) {
                if(item.total != null )
                {
                   
                    tableRecTypologie +="<tr><td style=''>PLI FERME/td><td>"+item.pli_ferme+"</td><td>"+item.pourcent_pli_ferme+" %</td></tr>";
                    jsonMv.push({"label":"EN COURS", "value":item.pourcent_pli_ferme});
                    iR++;
                }
            });
            
            tableRecTypologie +="</table>";

            console.log(jsonMv);
            
            if(iR > 0)
            {
                
               
                $("#recp-pli-fermer").html(tableRecTypologie);
                var couleurPli = ['#ffd8b1', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#E83E8C'];
                var couleurMv = [ '#4FC0E8', '#43CAA9', '#EC87BF', '#EE5567', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
               /* getMorris('donut', "donut-en-cours-pli",jsonPli,couleurPli);*/

              $("#donut-pli-fermer").empty();

               getMorris('donut', "donut-pli-fermer",jsonMv,couleurMv);
            }
            else
            {
                $("#recp-pli-fermer").html("<b>Pas de données</b>");
            }
            
          

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}

function exportPilotageFlux()
{
    
    dateDebut   = $("#date-debut").val() == null ? 0 : $("#date-debut").val();
    dateFin     = $("#date-fin").val() == null ? 0 : $("#date-fin").val();
    
    if(dateDebut == 0 || dateFin == 0)
    {
        $("#date-debut").focus();
        return false;
    }

    if(getDate(dateDebut) > getDate(dateFin))
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de rectifier votre filtre date '},{type: 'danger'});
        $("#date-debut").focus();
        return false;
    }
    else
    {
        window.location =  s_url+'reception/Pilotage_Flux/index2/?debut='+dateDebut+'&fin='+dateFin;
    }
    
}




 
