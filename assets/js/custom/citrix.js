var dateDebut;
var dateFin;
var societe;
var statut;
var paiement;
var typologie;
var myPost;
var tableListeCitrix;
$(".c-citrix").hide();
var chargementSpinner = '<center><div class="preloader"><div class="spinner-layer pl-black"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>';
$('.provenance-date').datepicker({
    todayBtn: "linked",
    language: "fr",
    autoclose: true,
    todayHighlight: true,
    toggleActive: true
    
});

function load_citrix()
{
    societe     = $("#filtre-societe").val();
    statut      = $("#filtre-statut").val();
    paiement    = $("#filtre-mpaiement").val();
    typologie   = $("#filtre-typologie").val();
    dateDebut   = $("#date-debut").val() == null ? 0 : $("#date-debut").val();
    dateFin     = $("#date-fin").val() == null ? 0 : $("#date-fin").val();
    //alert(paiement);
   

    if(dateDebut == 0 || dateFin == 0)
    {
        $("#date-debut").focus();
        NProgress.done();
        return false;
    }

    if(getDate(dateDebut) > getDate(dateFin))
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de rectifier votre filtre date '},{type: 'danger'});
        $("#date-debut").focus();
        return false;
    }
    else
    {
        myPost = {dateDebut:dateDebut,dateFin:dateFin,societe,statut,paiement,typologie};
        $("#citrix-table-liste").html(chargementSpinner);
        listeCitrix();
    }
	
}


function listeCitrix()
{
   $(".c-citrix").show();
   $('#table-liste-citrix').dataTable().fnDestroy();
    tableListeCitrix = $('#table-liste-citrix').dataTable({
        "scrollY": '600px',
        "processing": true,
        "scrollCollapse": true,
        "deferRender":    true,
        "scroller":       true,
        "serverSide": true,
        "paging" : true,
        "ajax": {
                "data"    : myPost,
                "url":s_url+'citrix/Visu_citrix/getListeSuiviCitrix', 
                "type": "POST"
            
        },
        "dom": 'lBfrtip',
            buttons: [
                {
                    text: 'Excel',
                    action: function (e, dt, node, config) {
                        $.ajax({
                            "url":s_url+'citrix/visu_citrix/exportListeCitrix',
                            "type": "POST",
                            "data"    : myPost,
                            "success": function(res, status, xhr) {
                                var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                                var csvURL = window.URL.createObjectURL(csvData);
                                var tempLink = document.createElement('a');
                                tempLink.href = csvURL;
                                tempLink.setAttribute('download', 'Liste de suivi virements citrix.xls');
                                document.body.appendChild(tempLink);
                                tempLink.click();
                            }
                        });
                    }
                }
            ],
        "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false //set not orderable
            }
        ],
        "fnInitComplete":function(){
			$("#citrix-table-liste").html("");
			
            $('.dataTables_scrollBody').slimscroll({
                axis: 'both',
                distance: '3px',
                alwaysVisible:false,
                height: auto
            });
            $(".dataTables_scrollHeadInner").css({"width":"100%"});
            //$(".table-liste").css({"width":"100%"});
             $(".dataTables_scrollHeadInner").css({"width":"100%"});
                $(".table-liste").css({"width":"100%"});
                $(".detail-encours-typo-table").hide();
        },
        "drawCallback": function( settings ) {
            $(".dataTables_scrollHeadInner").css({"width":"100%"});
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.input.activate();
        }
    });
 
    
}
