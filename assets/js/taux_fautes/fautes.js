var dateDebut;
var dateFin;
var societe;
var granularite;
var myPost;

var chargementSpinner = '<center><div class="preloader"><div class="spinner-layer pl-black"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>';

 
function init_nsprogress(){
    $( document ).ajaxStart(function() {
        $(".preloader-reception").show();
        NProgress.start();
    });
    /*$( document ).ajaxComplete(function() {
        NProgress.done();
    });
    $(document).ajaxSuccess(fu  nction() {
        NProgress.done();
    });*/
    $(document).ajaxStop(function() {
        NProgress.done();
        $(".preloader-reception").hide();
    });
}



function initDate()
{
      var start = moment();
      
      var end = moment();
      $('input[name="daterange"]').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
            'Aujourd\'hui': [moment(), moment()],
             'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             '7 jours précédents': [moment().subtract(6, 'days'), moment()],
             '30 derniers jours': [moment().subtract(29, 'days'), moment()],
             'Ce mois': [moment().startOf('month'), moment().endOf('month')],
             'Mois précedent': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          locale:{
            format: 'DD/MM/YYYY',
            daysOfWeek: ["Dim","Lun","Mar","Mer","Jeu","Ven","Sam"],
            monthNames: ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"],
            firstDay: 1},
      }, cb);
  
      cb(start, end);
}

function cb(start, end) {
    $('input[name="daterange"] span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}


function parTypologie()
{
    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table" id="table-typo">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Typologie</th>\n' +
        '\t\t\t<th style="width:15%">Nbr. champ</th>\n' +
        '\t\t\t<th style="width:15%"> Nbr. champs dans avec faute</th>\n' +
        '\t\t\t<th style="width:15%">Taux de fautes</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $("#recp-typo").html(chargementSpinner+"chargement ...");
    $.ajax({
        url : s_url+'visualisation/Fautes/fautesParTypologie',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var totalControled = [];
            var pliMvt   = [];
            var SommeChamp = 0;
            var SommeChampTotal = 0;
            var iT = 0;
            $.each(data, function(i, item) {

                if(item.champ != null)
                {
                    var champ = ''+item.champ+'';
                    if(item.change != 0)
                    {
                        jsonPli.push({"label":champ, "value":item.change});
                    }
                    SommeChamp += parseInt(item.change);
                    SommeChampTotal += parseInt(item.total_controled);
                   // totalControled.push(item.total_controled);
                    
                    tableRecTypologie += "<tr><td style=''>"+item.champ+"</td><td>"+item.total_controled+"</td><td>"+item.change+"</td><td>"+item.taux+" %</td></tr>";
                    iT++;
                }

            });
            var uniquePliTotal      = Array.from(new Set(totalControled));
            var maxPlitotal         = Math.max.apply(Math, uniquePliTotal);
            var tauxGlobal = SommeChamp*100.00/SommeChampTotal;
            if(iT > 0)
            {
                var  tableRecTypologieTotal = "<tfoot><tr style='font-weight: bold;'><td >Total</td><td>"+SommeChampTotal+"</td><td>"+SommeChamp+"</td><td>"+tauxGlobal.toFixed(2)+"%</td></tr></tfoot>";
                tableRecTypologieTotal +="</table>";
                tableRecTypologie +=tableRecTypologieTotal;
                $("#recp-typo").html(tableRecTypologie);
                //console.log(jsonPli);
                var couleurPli = [ '#0AE0FF', '#FF7F00', '#0095FF', '#f58231', '#FF00AA', '#FFD400', '#6AFF00', '#bcf60c', '#EDB9B9', '#8F2323','#00EAFF', '#AA00FF', '#FF7F00', '#0095FF', '#f58231'];
                


                $('#table-typo').DataTable({ "lengthMenu": [[6, 10, 20,25, -1], [6, 10, 20,25, "Toutes"]]});

                $("#donut-typo-pli").empty();
                
               getMorris('donut', "donut-typo-pli",jsonPli,couleurPli);
                
            }
            else
            {
               notifUSer("Nombre de plis par champ : 0");
                $("#recp-typo").html("<b>Pas de données</b>");
            }

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}


function getMorris(type, element,json,couleur) {

    $("#"+element).empty();
    Morris.Donut({
            element: element,
            data: json,
            colors: couleur,
            resize : false,
            hoverCallback: function(index, options, content) {
                var data = options.data[index];
                $(".morris-hover").html('<div>Custom label: ' + data.label + '</div>');
            },
            formatter: function (y) {
                return y + '%'
            }
        });

}



function parChamp()
{
    var tableRecTypologie = "";
    tableRecTypologie += '' +
        '<table class="table" id="table-champ">\n' +
        '\t<thead class="custom_font">\n' +
        '\t\t<tr>\n' +
        '\t\t\t<th style="width:40%">Champ</th>\n' +
        '\t\t\t<th style="width:15%"> Nbr. champ</th>\n' +
        '\t\t\t<th style="width:15%"> Nbr. champ avec faute</th>\n' +
        '\t\t\t<th style="width:15%">Taux de fautes</th>\n' +
        '\t\t  \n' +
        '\t\t</tr>\n' +
        '\t</thead>';
    var jsonPli = [];
    var  jsonMv   = [];

    $("#recp-champ").html(chargementSpinner+"chargement ...");
    $.ajax({
        url : s_url+'visualisation/Fautes/fautesParChamp',
        type : 'POST',
        data    : myPost,
        dataType : 'json',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(data, statut){
            var totalControled = [];
            var pliMvt   = [];
            var SommeChampErr = 0;
            var SommeChamp = 0;
            var iT = 0;
            $.each(data, function(i, item) {

                if(item.champ != null)
                {
                    var champ = ''+item.champ+'';
                    if(item.change != 0)
                    {
                        jsonPli.push({"label":champ, "value":item.change});
                    }
                    SommeChampErr += parseInt(item.change);
                    SommeChamp += parseInt(item.total_controled);
                    
                    totalControled.push(item.total_controled);
                    
                    tableRecTypologie += "<tr><td style=''>"+item.champ+"</td><td>"+item.total_controled+"</td><td>"+item.change+"</td><td>"+item.taux+" %</td></tr>";
                    iT++;
                }

            });
            var uniquePliTotal      = Array.from(new Set(totalControled));
            var maxPlitotal         = Math.max.apply(Math, uniquePliTotal);
            var tauxGlobal = SommeChampErr*100.00/SommeChamp;
            if(iT > 0)
            {
                var  tableRecTypologieTotal = "<tfoot><tr style='font-weight: bold;'><td >Total</td><td>"+SommeChamp+"</td><td>"+SommeChampErr+"</td><td>"+tauxGlobal.toFixed(2)+"%</td></tr></tfoot>";
                tableRecTypologieTotal +="</table>";
                tableRecTypologie +=tableRecTypologieTotal;
                $("#recp-champ").html(tableRecTypologie);
                //console.log(jsonPli);
                var couleurPli = [ '#4FC0E8', '#43CAA9', '#EC87BF', '#EE5567', '#FB6E53', '#FFCE55', '#5E9BEC', '#DB4455', '#36BC9B', '#DA70AE', '#E9573E', '#3BAEDA', '#F6BB43', '#4C88DE'];
                


                $('#table-champ').DataTable({ "lengthMenu": [[6, 10, 20,25, -1], [6, 10, 20,25, "Toutes"]]});

                $("#donut-champ-pli").empty();
                
               getMorris('donut', "donut-champ-pli",jsonPli,couleurPli); 
                
            }
            else
            {
               notifUSer("Nombre de plis par champ : 0");
                $("#recp-champ").html("<b>Pas de données</b>");
            }

        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}


function getMorris(type, element,json,couleur) {

    $("#"+element).empty();
    Morris.Donut({
            element: element,
            data: json,
            colors: couleur,
            resize : false,
            hoverCallback: function(index, options, content) {
                var data = options.data[index];
                $(".morris-hover").html('<div>Custom label: ' + data.label + '</div>');
            }/*,
            formatter: function (y) {
                return y + '%'
            }*/
        });

}

function getChartBar(type, element,json,couleur) {

    $("#"+element).empty();
    Morris.Donut({
            element: element,
            data: json,
            colors: couleur,
            resize : false,
            hoverCallback: function(index, options, content) {
                var data = options.data[index];
                $(".morris-hover").html('<div>Custom label: ' + data.label + '</div>');
            }/*,
            formatter: function (y) {
                return y + '%'
            }*/
        });
       /* Morris.Bar({
            element: 'donut-champ-pli',
            barSizeRatio:0.4,
            data: [
              { y: '2006', a: 100},
              { y: '2007', a: 75 },
              { y: '2008', a: 50 },
              { y: '2009', a: 75},
              { y: '2010', a: 50},
              { y: '2011', a: 75 },
              { y: '2012', a: 100}
            ],
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Series A'],
            hideHover: 'auto',
            behaveLikeLine: true,
            resize: true,
            pointFillColors: ['#ffffff'],
            pointStrokeColors: ['black'],
            lineColors: ['red', 'blue']
          });*/

}
function fautesParOperateurGoblale()
{
    var table ;
    /*tb-fautes-op-globale*/
    if ( $.fn.DataTable.isDataTable('#tb-fautes-op-globale')) {
        $('#tb-fautes-op-globale').DataTable().destroy();
      }
      
    table = $('#tb-fautes-op-globale').DataTable({
        "ajax":{
            "url" : s_url+"visualisation/Fautes/fautesParOperateurGoblale",
            "type": "POST",
            data:myPost
        },
        "scrollCollapse": true,
        "paging": false,
        "drawCallback": function( settings ) {
          $.AdminBSB.dropdownMenu.activate();
          $.AdminBSB.select.activate();
          $.AdminBSB.input.activate();
        }
      });
      
}
function fautesParOperateurDetaillee()
{
    var table ;
    /*tb-fautes-op-globale*/
    $("#table-ope-detaillee").html(chargementSpinner+"chargement ...");
    $.ajax(
        {
          url :s_url +"visualisation/Fautes/fautesParOperateurDetaillee",
          type :'POST',
          dataType :'html',
          data:myPost,
          success :function(retour,statut) {
              $("#table-ope-detaillee").html(retour);
              console.log(retour);
              $('#table-liste-op-detaille').DataTable(
                  {
                    "scrollX": true,
                    "lengthMenu": [[6, 10, 20,25, -1], [6, 10, 20,25, "Toutes"]],
                    fixedColumns:   {
                        leftColumns: 1 }
                  }
                  
              );
          },
          error:function(retour,statut,erreur){
              alert("Error on database");
          }
        });
      
}



function fautesParTypologieDetaillee()
{
    var table ;
    /*tb-fautes-op-globale*/
    $("#table-detail-typo-pli").html(chargementSpinner+"chargement ...");
    $.ajax(
        {
          url :s_url +"visualisation/Fautes/fautesParTypologieDetaillee",
          type :'POST',
          dataType :'html',
          data:myPost,
          success :function(retour,statut) {
              $("#table-detail-typo-pli").html(retour);
              console.log(retour);
              $('#table-liste-typ-detaille').DataTable(
                  {
                    "scrollX": true,
                    "lengthMenu": [[5, 10, 20,25, -1], [5, 10, 20,25, "Toutes"]],
                    fixedColumns:   {
                        leftColumns: 1 }
                  }
              );
          },
          error:function(retour,statut,erreur){
              alert("Error on database");
          }
        });
      
}

function parChampDetaillee()
{
    var table ;
    /*tb-fautes-op-globale*/
    $("#detail-champ-pli").html(chargementSpinner+"chargement ...");

    $.ajax(
        {
          url :s_url +"visualisation/Fautes/parChampDetaillee",
          type :'POST',
          dataType :'html',
          data:myPost,
          success :function(retour,statut) {
              $("#detail-champ-pli").html(retour);
              console.log(retour);
              $('#table-liste-chmp-detaille').DataTable(
                  {
                    "scrollX": true,
                    "lengthMenu": [[5, 10, 20,25, -1], [5, 10, 20,25, "Toutes"]],
                    fixedColumns:   {
                        leftColumns: 1 }
                  }
              );
          },
          error:function(retour,statut,erreur){
              alert("Error on database");
          }
        });
      
}


function getFautes()
{
    //notifUSer("Nombre de plis par champ : 0");   
    var daty = $("#date-saisie-fautes").val();
    var splitDate = daty.split("-");
    dateDebut = splitDate[0];
    dateFin   = splitDate[1];
    societe   = $("#filtre-societe-fautes").val();
    granularite = $("#granularite-fautes").val();
    myPost = {"debut":dateDebut,"fin":dateFin,"societe":societe,"granularite":granularite};

    /****Verification */
    $(".fautes-ged").hide();
    $.ajax({
        url : s_url+"visualisation/Fautes/verification", 
        type : 'POST',
        dataType : 'json',
        data    : myPost,
        success : function(data, statut){
            
            if(data[0].nb > 0 )
            {
               
                $(".fautes-ged").show();
                parChamp();
    
                parTypologie();
                fautesParOperateurGoblale();
                fautesParOperateurDetaillee();
                fautesParTypologieDetaillee();
                parChampDetaillee();
            }
            else
            {
                $.notify({title: '<strong>information !</strong>',message: ' Pas de données '},{type: 'danger'});
            }
            
        
             
        },

        error : function(resultat, statut, erreur){


        },

        complete : function(resultat, statut){
            // console.log(resultat);

        }

    });
}
/****************** */
$(function () {
    $(".fautes-ged").hide();
    init_nsprogress();
    initDate();

    

    /*
    Morris.Bar({
        element: 'donut-champ-pli',
        barSizeRatio:0.4,
        data: [
          { y: '2006', a: 100},
          { y: '2007', a: 75 },
          { y: '2008', a: 50 },
          { y: '2009', a: 75},
          { y: '2010', a: 50},
          { y: '2011', a: 75 },
          { y: '2012', a: 100}
        ],
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Series A'],
        labelColor: '#FFF',
        barColors: ['#000000', 'blue']
      });*/

   
});
 
