var keys = {};
onkeydown = onkeyup = function(e) {
    e = e || event;
    e.which = e.which || e.keyCode;
    keys[e.which] = e.type === 'keydown';
    if (keys[17] && keys[13]) { //ctrl+Enter: go
        if ($('[onclick="load_pli();"]').length == 1) {
            load_pli();
        } else if ($('[onclick="enregistrer();"]').length == 1) {
            enregistrer();
            e.preventDefault();
            e.stopPropagation();
        }
    }
    if (keys[17] && keys[8]) { //ctrl+back: annuler traitement
        if ($('[onclick="annuler();"]').length == 1) {
            annuler();
        }
    }
    if (keys[17] && keys[16]) { //ctrl+Shft: afficher apercu document
        if ($('.img_b64').length > 0) {
            $($('.img_b64')[0]).trigger('click');
        }
    }
    if (keys[18] && keys[106]) { //alt+*: retourner document
        var is_magnify = false;
        if ($('.magnify-foot-toolbar .magnify-button-next').length > 0) {
            $('.magnify-foot-toolbar .magnify-button-next').trigger('click');
            is_magnify = true;
        }
        if ($('.tab_doc_ap').length > 0 && !is_magnify) {
            var cible = null;
            $(".tab_doc_ap").each(function() {
                if (!($(this).hasClass('active'))) {
                    cible = ($(this).find('a[data-toggle="tab"]'))[0];
                }
            });
            if (cible !== null) {
                $(cible).trigger('click');
            }
        }
    }
    if (keys[18] && keys[34]) { //alt+page_down: next document
        var is_magnify = $('.magnify-modal').length > 0;
        if ($('#sel_view_doc').length > 0) {
            var opts = $('option.opt_doc');
            var val_selected = $('#sel_view_doc').val();
            if ($(opts[(opts.length - 1)]).attr('value') == val_selected) {
                $('#sel_view_doc').selectpicker('val', $(opts[0]).attr('value'));
            } else {
                var i_opt = 0;
                $('option.opt_doc').each(function() {
                    if ($(this).attr('value') == val_selected) {
                        $('#sel_view_doc').selectpicker('val', $(opts[i_opt + 1]).attr('value'));
                    }
                    i_opt++;
                });
            }
            show_doc();
            if (is_magnify) {
                if ($('.img_b64').length > 0) {
                    $($('.img_b64')[0]).trigger('click');
                }
            }
        }
    }
    if (keys[18] && keys[33]) { //alt+page_up: previous document
        var is_magnify = $('.magnify-modal').length > 0;
        if ($('#sel_view_doc').length > 0) {
            var opts = $('option.opt_doc');
            var val_selected = $('#sel_view_doc').val();
            if ($(opts[0]).attr('value') == val_selected) {
                $('#sel_view_doc').selectpicker('val', $(opts[(opts.length - 1)]).attr('value'));
            } else {
                var i_opt = 0;
                $('option.opt_doc').each(function() {
                    if ($(this).attr('value') == val_selected) {
                        $('#sel_view_doc').selectpicker('val', $(opts[i_opt - 1]).attr('value'));
                    }
                    i_opt++;
                });
            }
            show_doc();
            if (is_magnify) {
                if ($('.img_b64').length > 0) {
                    $($('.img_b64')[0]).trigger('click');
                }
            }
        }
    }
    if (keys[27]) { //Echap: fermer apercu document
        if ($('.magnify-head-toolbar .magnify-button-close').length > 0) {
            $('.magnify-head-toolbar .magnify-button-close').trigger('click');
        }
        if ($('.modal.in a.close').length > 0) {
            $('.modal.in a.close').trigger('click');
        }
    }
    if (keys[18] && keys[107]) { //alt++: zoom apercu
        if ($('.magnify-foot-toolbar .magnify-button-zoom-in').length > 0) {
            $('.magnify-foot-toolbar .magnify-button-zoom-in').trigger('click');
        }
    }
    if (keys[18] && keys[109]) { //alt+-: de-zoom apercu
        if ($('.magnify-foot-toolbar .magnify-button-zoom-in').length > 0) {
            $('.magnify-foot-toolbar .magnify-button-zoom-out').trigger('click');
        }
    }
}

function in_load_pli(){
    $('#contenu').html('<div id="div_loader" class="row m-t-25">'
                +'<div class="col-xs-12 align-center">'
                +'<div class="preloader pl-size-xl">'
                +'<div class="spinner-layer pl-light-blue">'
                +'<div class="circle-clipper left">'
                +'<div class="circle"></div>'
                +'</div>'
                +'<div class="circle-clipper right">'
                +'<div class="circle"></div>'
                +'</div>'
                +'</div>'
                +'</div>'
                +'</div>'
                +'</div>');
}

function load_pli() {
    var data = {
        trtmt_valid: ''
        ,trtmt_ctrl: '-1'
        ,user_ctrl: '-1'
    };
    if($('#sel_traitement_valid_init').length == 1){
        data.trtmt_valid = $('#sel_traitement_valid_init').val();
    }
    if($('#sel_traitement_ctrl_init').length == 1){
        data.trtmt_ctrl = $('#sel_traitement_ctrl_init').val();
    }
    if($('#sel_user_ctrl_init').length == 1){
        data.user_ctrl = $('#sel_user_ctrl_init').val();
    }
    in_load_pli();
    $.post(chemin_site + MY_URL + '/get_pli', data, function(reponse) {
        $('#contenu').html(reponse);
        $('#sel_view_doc').selectpicker();
        show_doc();
        init_compo_field();
    });
}

function set_titre() {
    var id_soc = $('#field_soc').val();
    $.get(chemin_site + '/control/ajax/option_titres/' + id_soc, {}, function(reponse) {
        $('#field_main_titre').selectpicker('destroy');
        $('#field_main_titre').html(reponse);
        $('#field_main_titre').selectpicker();
        $.get(chemin_site + '/control/ajax/option_titres/' + id_soc + '/1', {}, function(reponse) {
            $('select.sel_field_mvmnt').selectpicker('destroy');
            $('select.sel_field_mvmnt').html(reponse);
            $('select.sel_field_mvmnt').selectpicker();
            numerotation_mvmnt();
        });
    });
}

function get_typologie() {
    var id_soc = $('#field_soc').val();
    $.get(chemin_site + '/control/ajax/option_typologie/' + id_soc, {}, function(reponse) {
        var field_typo = '#field_typologie';
        if ($(field_typo)) {
            $(field_typo).selectpicker('destroy');
            $(field_typo).html(reponse);
            $(field_typo).selectpicker();
        }
    });
}

function show_doc() {
    if($('#sel_view_doc').length > 0){
        $.get(chemin_site + '/control/ajax/view_doc/' + $('#sel_view_doc').val(), {}, function(reponse) {
            $('#view_doc').html(reponse);
            //$('[data-magnify=gallery]').magnify({
            $('.img_b64').magnify({
                multiInstances: false,
                modalWidth: '74.5%',
                modalHeight: '100%',
                fixedModalPos: true,
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next'],
                fixedModalSize: true
            });
        });
    }
}

function init_compo_field() {
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
    $('.sel_field').selectpicker();
    autosize($('textarea.auto-growth'));
    $.AdminBSB.input.activate();
    if($('#sel_view_doc').length > 0){
        numerotation_mvmnt();
        numerotation_cheque();
        analyse_paiement();
        manage_etat();
    }
    interface_mode();
    $('[data-trigger="spinner"]').spinner();
    $('body').on('mouseenter', '.modal.fade.in .modal-dialog .modal-content', function(e){
        $('.modal.fade.in').css('z-index', '1091');
    });
    $('body').on('mouseleave', '.modal.fade.in .modal-dialog .modal-content', function(e){
        $('.modal.fade.in').css('z-index', '1050');
    });
}

function manage_etat() {
    var etat = $('#field_statut_saisie').val();
    //if ($.inArray(etat, ['2', '4', '5', '6']) > -1) {
    if ($.inArray(1*etat, [1]) == -1) {//etat != 1
        $('.if_ko').show();
        $('.if_ko_autre').show();
    } else {
        $('.if_ko').hide();
        $('.if_ko_autre').hide();
    }
    $('#parti_field').slimscroll({
        height: '85vh',
        color: 'rgba(0,0,0,0.5)',
        size: '8px',
        alwaysVisible: true,
    });
}

function analyse_paiement() {
    appli_chq();
    appli_chq_kd();
    appli_espece();
}

function appli_chq() {
    var avec_grp_field_chq = ($('.grp_field_cheque')).length > 0;
    var modes = $('#field_mode_paiement').val() === null ? [] : $('#field_mode_paiement').val();
    var mode_avec_cheque = modes.indexOf('1') > -1;
    if (mode_avec_cheque && !avec_grp_field_chq) {
        get_view_cheque();
    }
    if (!mode_avec_cheque && avec_grp_field_chq) {
        swal({
            title: "Confirmation!",
            text: "La suppression du mode de paiement par cheque entrainera la suppression des données des cheques de ce pli.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(ok) {
            if (ok) {
                $('.grp_field_cheque').remove();
            } else {
                modes.push('1');
                $('#field_mode_paiement').selectpicker('val', modes);
            }
        });
    }
}

function appli_chq_kd() {
    var avec_grp_field_chq_kd = ($('.grp_field_cheque_kd')).length > 0;
    var modes = $('#field_mode_paiement').val() === null ? [] : $('#field_mode_paiement').val();
    var mode_avec_cheque_kd = modes.indexOf('6') > -1;
    if (mode_avec_cheque_kd && !avec_grp_field_chq_kd) {
        get_view_cheque_kd();
    }
    if (!mode_avec_cheque_kd && avec_grp_field_chq_kd) {
        swal({
            title: "Confirmation!",
            text: "La suppression du mode de paiement par cheque cadeau entrainera la suppression des données des cheques cadeaux de ce pli.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(ok) {
            if (ok) {
                $('.grp_field_cheque_kd').remove();
            } else {
                modes.push('6');
                $('#field_mode_paiement').selectpicker('val', modes);
            }
        });
    }
}

function appli_espece() {
    var avec_esp = $("#row_montant_espece").is(":visible");
    var modes = $('#field_mode_paiement').val() === null ? [] : $('#field_mode_paiement').val();
    var mode_avec_esp = modes.indexOf('5') > -1;
    if(mode_avec_esp && !avec_esp){
        $("#row_montant_espece").show();
    }else if (!mode_avec_esp && avec_esp) {
        swal({
            title: "Confirmation!",
            text: "La suppression du mode de paiement par esp\350ce entrainera la suppression du montant esp\350ce de ce pli.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(ok) {
            if (ok) {
                $("#field_montant_espece").val('0');
                $("#row_montant_espece").hide();
            } else {
                modes.push('5');
                $('#field_mode_paiement').selectpicker('val', modes);
            }
        });
    }
}

function get_view_cheque() {
    $.get(chemin_site + '/control/ajax/get_cheque_view/' + ID_PLI, {}, function(reponse) {
        $('#section_paiement').append(reponse);
        $('select.sel_field_cheque').selectpicker();
        $.AdminBSB.input.activate();
        numerotation_cheque();
    });
}

function get_view_cheque_kd() {
    $.get(chemin_site + '/control/ajax/get_cheque_cadeau_view/' + ID_PLI, {}, function(reponse) {
        $('#section_paiement').append(reponse);
        $('select.sel_field_cheque_kd').selectpicker();
        $.AdminBSB.input.activate();
        numerotation_cheque_kd();
    });
}

function suppr_gr_field_cheque(id_grp) {
    swal({
        title: "Confirmation!",
        text: "La suppression de ce cheque entrainera la suppression de ses données.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Supprimer",
        cancelButtonText: "Annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        $(id_grp).remove();
        numerotation_cheque();
    });
}

function suppr_gr_field_cheque_kd(id_grp) {
    swal({
        title: "Confirmation!",
        text: "La suppression de ce cheque cadeau entrainera la suppression de ses données.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Supprimer",
        cancelButtonText: "Annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        $(id_grp).remove();
        numerotation_cheque_kd();
    });
}

function numerotation_cheque() {
    var num = 0;
    $(".grp_field_cheque").each(function() {
        num++;
        $($(this).find('.numerotation_cheque')[0]).html(num);
        $($(this).find('.clear_cheque')[0]).show();
        if ($(".grp_field_cheque").length == 1) {
            $($(this).find('.clear_cheque')[0]).hide();
        }
    });
}

function numerotation_cheque_kd() {
    var num = 0;
    $(".grp_field_cheque_kd").each(function() {
        num++;
        $($(this).find('.numerotation_cheque_kd')[0]).html(num);
        $($(this).find('.clear_cheque_kd')[0]).show();
        if ($(".grp_field_cheque_kd").length == 1) {
            $($(this).find('.clear_cheque_kd')[0]).hide();
        }
    });
}

function controle_cheque() {
    var tab_cmc7 = [];
    var etat = $('#field_statut_saisie').val();
    var is_ci = $.inArray(1*etat, [7, 11]) > -1;
    var reponse = true;
    $(".grp_field_cheque").each(function() {
        var ident = $(this).attr('my_ident');
        var cmc7 = $.trim($('#field_cmc7_' + ident).val());
        var etranger = $('#field_chq_etr_' + ident).prop('checked') ? '1' : '0';
        field_ok($('#field_cmc7_' + ident));
        field_ok($('#field_rlmc_' + ident));
        field_ok($('#field_nom_client_' + ident));
        var reg = /^\d{31}$/;
        if(etranger == '1'){
            var reg_cmc7_etr = /^\d+$/;
            if(!reg_cmc7_etr.test(cmc7)){
                if(reponse) {
                    showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: CMC7 non num&eacute;rique!', 'bottom', 'right', null, null);
                }
                field_error($('#field_cmc7_' + ident));
                reponse = false;
            }
        }else if (reg.test(cmc7)) {
            var rlmc = $('#field_rlmc_' + ident).val();
            var reg_rlmc = /^\d{1,2}$/;
            if(reg_rlmc.test(rlmc)){
                var valeur = cmc7.toString() + rlmc.toString();
                var increm = 0;
                for (var i_valeur = 0; i_valeur < valeur.length; i_valeur++) {
                    increm = parseInt(increm.toString() + valeur[i_valeur]) % 97;
                }
                if(increm != 0){
                    if(reponse) {
                        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: CMC7 ou RLMC invalide!', 'bottom', 'right', null, null);
                    }
                    field_error($('#field_cmc7_' + ident));
                    field_error($('#field_rlmc_' + ident));
                    reponse = false;
                }
            }else{
                if(reponse) {
                    showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: RLMC invalide!', 'bottom', 'right', null, null);
                }
                field_error($('#field_rlmc_' + ident));
                reponse = false;
            }
        }else{
            if(reponse) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: CMC7 invalide!', 'bottom', 'right', null, null);
            }
            field_error($('#field_cmc7_' + ident));
            reponse = false;
        }
        if ($.inArray(cmc7, tab_cmc7) == -1) {
            tab_cmc7.push(cmc7);
        } else {
            if(reponse) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: CMC7 en doublon.', 'bottom', 'right', null, null);
            }
            field_error($('#field_cmc7_' + ident));
            reponse = false;
        }
        var id_doc = $.trim($('#field_doc_' + ident).val());
        field_ok($('#field_doc_' + ident));
        var reg_nb = /^\d+$/;
        if (!(reg_nb.test(id_doc))) {
            if(reponse) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Les documents correspondants aux cheques sont obligatoires.', 'bottom', 'right', null, null);
            }
            field_error($('#field_doc_' + ident));
            reponse = false;
        }
        var montant = $('#field_montant_' + ident).val();
        field_ok($('#field_montant_' + ident));
        var reg = /^\d+([.,]\d+)?$/;
        if (!(reg.test($.trim(montant)))) {
            if(reponse) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Montant invalide!', 'bottom', 'right', null, null);
            }
            field_error($('#field_montant_' + ident));
            reponse = false;
        }
        if(is_ci){
            field_ok($('#field_nom_client_' + ident));
            if($.trim($('#field_nom_client_' + ident).val()) == ''){
                if(reponse) {
                    showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Nom client du ch&egrave;que est obligatoire pour un pli en circulaire!', 'bottom', 'right', null, null);
                }
                field_error($('#field_nom_client_' + ident));
                reponse = false;
            }
        }
    });
    return reponse;
}

function controle_cheque_kd() {
    var reponse = true;
    $(".grp_field_cheque_kd").each(function() {
        var ident = $(this).attr('my_ident');
        var id_doc = $.trim($('#field_doc_chq_kd_' + ident).val());
        field_ok($('#field_doc_chq_kd_' + ident));
        var reg_nb = /^\d+$/;
        if (!(reg_nb.test(id_doc))) {
            if(reponse) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Les documents correspondants aux cheques cadeaux sont obligatoires.', 'bottom', 'right', null, null);
            }
            field_error($('#field_doc_chq_kd_' + ident));
            reponse = false;
        }
        var montant = $('#field_montant_kd_' + ident).val();
        field_ok($('#field_montant_kd_' + ident));
        var reg = /^\d+([.,]\d+)?$/;
        if (!(reg.test($.trim(montant)))) {
            if(reponse) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Montant invalide!', 'bottom', 'right', null, null);
            }
            field_error($('#field_montant_kd_' + ident));
            reponse = false;
        }
    });
    return reponse;
}

function get_view_mvmnt() {
    var id_soc = $('#field_soc').val();
    $.get(chemin_site + '/control/ajax/get_mvmnt_view/' + id_soc, {}, function(reponse) {
        $('#main_card_mvmnt').append(reponse);
        $('select.sel_field_mvmnt').selectpicker();
        $('select.sel_field_mvmnt_mdl').selectpicker();
        $.AdminBSB.input.activate();
        interface_mode();
        numerotation_mvmnt();
    });
}

function suppr_gr_field_mvmnt(id_grp) {
    swal({
        title: "Confirmation!",
        text: "La suppression de ce mouvement entrainera la suppression de ses données.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Supprimer",
        cancelButtonText: "Annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        $(id_grp).remove();
        numerotation_mvmnt();
    });
}

function numerotation_mvmnt() {
    var nb_grp_field_mvmnt = ($('.grp_field_mvmnt')).length;
    if (nb_grp_field_mvmnt > 1) {
        var id_soc = $('#field_soc').val();
        $('#field_main_titre').selectpicker('val', id_soc + '000');
    } else {
        var ttr1 = $($("select.sel_field_mvmnt")[0]).val();
        $('#field_main_titre').selectpicker('val', ttr1);
    }
    var num = 0;
    $(".grp_field_mvmnt").each(function() {
        num++;
        $($(this).find('.numerotation_mvmnt')[0]).html(num);
        $($(this).find('.clear_mvmnt')[0]).show();
        if (nb_grp_field_mvmnt == 1) {
            $($(this).find('.clear_mvmnt')[0]).hide();
        }
    });
    $('[data-trigger="spinner"]').spinner();
}

function controle_mvmnt(groupe) {
    var ident = groupe.attr('my_ident');
    var rep = true;
    var num_a = $('#field_num_abon_' + ident).val();
    field_ok($('#field_num_abon_' + ident));
    if ($.trim(num_a) == '') {
        var nom_a = MODE_SAISIE_BATCH ? $('#field_mvmnt_mdl_abonne_' + ident+'_atn_end').val() : $('#field_nom_abon_' + ident).val();
        //var cp_a = $('#field_cp_abon_' + ident).val();
        if ($.trim(nom_a) == ''/* || $.trim(cp_a) == ''*/) {
            if(!notif_nom_ab) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Les informations relatives &agrave; l\'abonn&eacute; d\'un mouvement sont imcompletes.', 'bottom', 'right', null, null);
            }
            field_error($('#field_num_abon_' + ident));
            rep = false;
            notif_nom_ab = true;
        }
    }
    var num_p = $('#field_num_payeur_' + ident).val();
    field_ok($('#field_num_payeur_' + ident));
    if ($.trim(num_p) == '') {
        var nom_p = MODE_SAISIE_BATCH ? $('#field_mvmnt_mdl_payeur_' + ident+'_atn_end').val() : $('#field_nom_payeur_' + ident).val();
        //var cp_p = $('#field_cp_payeur_' + ident).val();
        if ($.trim(nom_p) == ''/* || $.trim(cp_p) == ''*/) {
            if(!notif_nom_p) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Les informations relatives &agrave; au payeur d\'un mouvement sont imcompletes.', 'bottom', 'right', null, null);
            }
            field_error($('#field_num_payeur_' + ident));
            rep = false;
            notif_nom_p = true;
        }
    }
    return rep;
}

function field_error(compos) {
    var parent = compos.closest('.form-line');
    if (parent.length > 0) {
        parent.addClass('focused').addClass('error').removeClass('warning').removeClass('success');
    }
}

function field_ok(compos) {
    var parent = compos.closest('.form-line');
    if (parent.length > 0) {
        parent.addClass('focused').removeClass('error').removeClass('warning').addClass('success');
    }
}

function valeur_cheques() {
    var data = [];
    $(".grp_field_cheque").each(function() {
        var ident = $(this).attr('my_ident');
        var anomalie = $('#field_anomalie_' + ident).val();
        var cheque = {
            cmc7: $('#field_cmc7_' + ident).val(),
            rlmc: $('#field_rlmc_' + ident).val(),
            montant: u_montant($('#field_montant_' + ident).val()),
            anomalies: anomalie,//JSON.stringify(anomalie),
            id_etat_chq: $('#field_etat_chq_' + ident).val(),
            etat_modif: $('#field_etat_modif_chq_' + ident).val(),
            id_doc: $.trim($('#field_doc_' + ident).val())
            ,nom_client: $('#field_nom_client_' + ident).val()
            ,etranger: $('#field_chq_etr_' + ident).prop('checked') ? '1' : '0'
            ,ui_id: ident
        };
        data.push(cheque);
    });
    return data;
}

function valeur_cheques_kd() {
    var data = [];
    $(".grp_field_cheque_kd").each(function() {
        var ident = $(this).attr('my_ident');
        var cheque = {
            montant: u_montant($('#field_montant_kd_' + ident).val()),
            type: $.trim($('#field_type_chq_kd_' + ident).val()),
            code_barre: $.trim($('#field_code_barre_kd_' + ident).val()),
            id_doc: $.trim($('#field_doc_chq_kd_' + ident).val())
            ,ui_id: ident
        };
        data.push(cheque);
    });
    return data;
}

function valeur_mvmnts() {
    var data = {
        mvmnt: []
        ,payeurs: {}
        ,abonnes: {}
        ,promos: {}
    };
    $(".grp_field_mvmnt").each(function() {
        var ident = $(this).attr('my_ident');
        var mvmnt = {
            titre: $('#field_titre_mvmnt_' + ident).val(),
            code_promo: $('#field_code_promo_' + ident).val(),
            num_a: $('#field_num_abon_' + ident).val(),
            nom_a: MODE_SAISIE_BATCH ? $('#field_mvmnt_mdl_abonne_' + ident+'_atn_end').val() : $('#field_nom_abon_' + ident).val(),
            //cp_a: $('#field_cp_abon_' + ident).val(),
            num_p: $('#field_num_payeur_' + ident).val(),
            nom_p: MODE_SAISIE_BATCH ? $('#field_mvmnt_mdl_payeur_' + ident+'_atn_end').val() : $('#field_nom_payeur_' + ident).val() //,cp_p: $('#field_cp_payeur_' + ident).val()
            ,ui_id: ident
            ,qtt: $('#field_qunatity_nb_' + ident).val() == undefined ? '1' : $('#field_qunatity_nb_' + ident).val()
        };
        data.mvmnt.push(mvmnt);
        if(MODE_SAISIE_BATCH) {
            data.payeurs[ident] = compile_data_client('field_mvmnt_mdl_payeur_'+ident);
            data.abonnes[ident] = compile_data_client('field_mvmnt_mdl_abonne_'+ident);
            data.promos[ident] = {
                code_promo: $('#field_code_promo_' + ident).val()
                ,code_choix: $('#field_code_promo_choix_mvmnt_' + ident).val()
                ,code_produit: $('#field_code_promo_produit_mvmnt_' + ident).val()
            };
        }
    });
    return data;
}

function valeur_field_saisie_ctrl(){
    var data = [];
    $(".field_saisie_ctrl").each(function() {
        var valeur = $(this).val();
        if(Array.isArray(valeur)){
            valeur = valeur.join();
        }
        var saisie_ctrl = {
            id_field: $(this).attr('id')
            ,name_field: $(this).attr('nom_field')
            ,valeur: valeur
        };
        var my_id = $(this).attr('id');
        if (typeof my_id !== typeof undefined && my_id !== false) {
            data.push(saisie_ctrl);
        }
    });
    return data;
}

function recup_valeur() {
    var montant_esp = $("#row_montant_espece").is(":visible") ? $.trim($("#field_montant_espece").val()) : '0';
    $data_mvmnts = valeur_mvmnts();
    return {
        id_pli: ID_PLI,
        idefix: $('#field_idefix').val(),
        datamatrix: $('#field_data_matrix').val(),
        code_promo: $('#field_promotion').val(),
        //type_coupon: $('#field_type_coupon').val(),
        id_soc: $('#field_soc').val(),
        typo: $('#field_typologie').val(),
        titre: $('#field_main_titre').val(),
        code_ecole_gci: $('#field_code_ecole').val(),
        mvmnts: JSON.stringify($data_mvmnts.mvmnt),
        mvmnt_payeurs: JSON.stringify($data_mvmnts.payeurs),
        mvmnt_abonnes: JSON.stringify($data_mvmnts.abonnes),
        mvmnt_promos: JSON.stringify($data_mvmnts.promos),
        paiements: JSON.stringify($('#field_mode_paiement').val()),
        montant_esp: u_montant(montant_esp),
        cheques: JSON.stringify(valeur_cheques()),
        cheques_cadeaux: JSON.stringify(valeur_cheques_kd()),
        statut: $('#field_statut_saisie').val(),
        motif_ko: $('#field_motif_ko').val(),
        autre_motif_ko: $('#field_autre_motif_ko').val(),
        nom_circ: $('#field_nom_circulaire').val() == -1 ? '' : $('#field_nom_circulaire').val(),
        nom_deleg: $.trim($('#field_nom_deleg').val()),
        fichier_circ: '',
        ext_fichier_circ: '',
        msg_ind: $('#field_msg_ind').prop('checked') ? '1' : '0',
        dmd_kdo: $('#field_dmd_kdo').prop('checked') ? '1' : '0',
        dmd_env_kdo: $('#field_dmd_env_kdo').prop('checked') ? '1' : '0',
        img_dezip: JSON.stringify($('#field_img_desar').val() == null ? [] : $('#field_img_desar').val()),
        modif_f_circ: modif_f_circ() ? 1 : 0,
        erreur: Array.isArray($('#field_erreur').val()) ? JSON.stringify($('#field_erreur').val()) : JSON.stringify([]),
        comm_erreur: $('#field_comm_erreur').val()
        ,saisie_batch: MODE_SAISIE_BATCH ? 1 : 0
        ,field_saisie_ctrl: JSON.stringify(valeur_field_saisie_ctrl())
    }
}

function restore_f_circ() {
    if ($('#current_f_circ').length > 0) {
        $('#current_f_circ').removeClass('font-line-through');
    }
    $('#field_fichier_circulaire').val('');
}

function select_f_circ() {
    if ($('#field_fichier_circulaire').val() != '') {
        if ($('#current_f_circ').length > 0) {
            $('#current_f_circ').addClass('font-line-through');
        }
    }
}

function delete_f_circ() {
    if ($('#current_f_circ').length > 0) {
        $('#current_f_circ').addClass('font-line-through');
    }
}

function clear_f_circ() {
    $('#field_fichier_circulaire').val('');
}

function have_file_circ() {
    if ($('#current_f_circ').length > 0) {
        if ($('#current_f_circ').hasClass('font-line-through')) {
            return !($('#field_fichier_circulaire').val() == '');
        } else {
            return true;
        }
    } else {
        return !($('#field_fichier_circulaire').val() == '');
    }
}

function modif_f_circ() {
    if ($('#current_f_circ').length > 0) {
        return $('#current_f_circ').hasClass('font-line-through');
    } else {
        return $('#field_fichier_circulaire').val() != '';
    }
}

function controle_all_mvmnt() {
    var tab_title_code_promo = [];
    var tab_title_multi_promo = [];
    var rep = true;
    var with_mvmt = false;
    $(".grp_field_mvmnt").each(function() {
        with_mvmt = true;
        var ident = $(this).attr('my_ident');
        field_ok($('#field_titre_mvmnt_' + ident));
        var mvmnt = {
            titre: $('#field_titre_mvmnt_' + ident).val(),
            code_promo: $('#field_code_promo_' + ident).val()
        };
        var trouver = false;
        tab_title_code_promo.forEach(element => {
            if(element.titre == mvmnt.titre){
                trouver = true;
                if($.trim(element.code_promo) != '' && element.code_promo != mvmnt.code_promo){
                    // field_error($('#field_titre_mvmnt_' + ident));
                    tab_title_multi_promo.push(element.titre);
                }
            }
        });
        if(!trouver){
            tab_title_code_promo.push(mvmnt);
        }
        if (!controle_mvmnt($(this))) {
            rep = false;
        }
    });
    if(!with_mvmt){
        rep = false;
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Aucun mouvement!', 'bottom', 'right', null, null);
    }
    /*if(tab_title_multi_promo.length > 0){
        var msg = tab_title_multi_promo.length > 1 ? 'Ces titres de mouvement sont associ&eacute;s' : 'Ce titre de mouvement est associ&eacute;';
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: '+msg+' &agrav; des codes promotion diff&eacute;rents: '+tab_title_multi_promo.join(', '), 'bottom', 'right', null, null);
        return false;
    }*/
    return rep;
}

function controle_data() {
    notif_nom_ab = false;
    notif_nom_p = false;
    var ctrl_mvmnt = controle_all_mvmnt();
    var ctrl_oblig = true;
    $(".oblig").each(function() {
        field_ok($(this));
        if ($.trim($(this).val()) == '') {
            field_error($(this));
            if(ctrl_oblig) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Champ obligatoire non renseign&eacute;.', 'bottom', 'right', null, null);
            }
            ctrl_oblig = false;
        }
    });
    var typo = $('#field_typologie').val();
    var code_ecole_gci = $('#field_code_ecole').val();
    field_ok($('#field_code_ecole'));
    if($.inArray(typo, ['8','14', '15', '17', '18']) != -1 && $.trim(code_ecole_gci) == ''){
        field_error($('#field_code_ecole'));
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Code &eacute;cole/GCI obligatoire pour les flux r&eacute;seaux.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    var etat = $('#field_statut_saisie').val();
    var is_ok = $.inArray(1*etat, [1, 11]) > -1;
    var is_ci = $.inArray(1*etat, [7, 11]) > -1;
    var motif_ko = $('#field_motif_ko').val();
    var motif_ko_autre = $('#field_autre_motif_ko').val();
    field_ok($('#field_autre_motif_ko'));
    field_ok($('#field_motif_ko'));
    if (((!is_ok || is_ci) && motif_ko == 4 && $.trim(motif_ko_autre) == '') || ((!is_ok || is_ci) && motif_ko == null)) {
        field_error($('#field_autre_motif_ko'));
        field_error($('#field_motif_ko'));
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Motif du KO (ou CI) non renseigné.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    var nom_ci = $('#field_nom_circulaire').val();
    if (is_ci && (nom_ci == -1 || !have_file_circ())) {//NOTE fichier CI
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Le nom et le fichier du circulaire doit &ecirc;tre renseign&eacute;!', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    /*field_ok($('#field_nom_deleg'));
    if (etat == 7) {
        if($.trim($('#field_nom_deleg').val()) == ''){
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Nom d&eacute;l&eacute;gu&eacute; est obligatoire pour un pli en circulaire!', 'bottom', 'right', null, null);
            field_error($('#field_nom_deleg'));
            ctrl_oblig = false;
        }
    }*/
    if ($('#field_mode_paiement').val() == null) {
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Mode de paiement non renseigné.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    if ($('#field_erreur').hasClass('sel_erreur_ctrl')) {
        if ($('#field_erreur').val() == '') {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: La Cat&eacute;gorie d\'erreur doit &ecirc;tre renseign&eacute;e!', 'bottom', 'right', null, null);
            ctrl_oblig = false;
        }
    }
    var ctrl_uniq_doc = true;
    var tab_doc = [];
    $("select.field_doc_correspond").each(function() {
        field_ok($(this));
        var id_doc = $(this).val();
        if ($.inArray(id_doc, tab_doc) == -1) {
            tab_doc.push(id_doc);
        } else {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Document de paiement en doublon: #' + id_doc, 'bottom', 'right', null, null);
            field_error($(this));
            ctrl_uniq_doc = false;
        }
    });
    if($("#row_montant_espece").is(":visible")){
        field_ok($('#field_montant_espece'));
        var reg = /^\d+([.,]\d+)?$/;
        if(!(reg.test($.trim($("#field_montant_espece").val())))){
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Montant invalide!', 'bottom', 'right', null, null);
            field_error($('#field_montant_espece'));
            ctrl_oblig = false;
        }
    }
    if (ctrl_mvmnt && ctrl_oblig && controle_cheque() && controle_cheque_kd() && ctrl_uniq_doc) {
        return true;
    }
    return false;
}

function send_valeur(tmp_link_f_circ = '', ext_f_circ = '', nom_f_circ = '') {
    var data_to_serv = all_valeur();
    data_to_serv.fichier_circ = tmp_link_f_circ;
    data_to_serv.ext_fichier_circ = ext_f_circ;
    data_to_serv.nom_fichier_circ = nom_f_circ;
    swal({
        title: 'Enregistrer?',
        text: 'Pli#' + ID_PLI,
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + MY_URL + '/save', data_to_serv, function(reponse) {
            if (reponse == 1) {
                swal({
                    title: 'Enregistrement.',
                    text: 'Enregistrement réussi du pli#' + ID_PLI,
                    type: 'success',
                    //timer: 2000,
                    showConfirmButton: false
                    ,closeOnConfirm: false
                }, function(){in_load_pli();});
                in_load_pli();
                setTimeout(function() {
                    window.location.href = chemin_site + MY_URL + '?direct=1';
                }, 1000);
            } else {
                swal('Serveur', 'Erreur: ' + reponse, 'error');
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}

function send_valeur_with_f_circ() {
    var frm_f_circ = $('#form_fichier_circulaire');
    var frmdata_f_circ = (window.FormData) ? new FormData(frm_f_circ[0]) : null;
    var f_circ = (frmdata_f_circ !== null) ? frmdata_f_circ : frm_f_circ.serialize();
    $.ajax({
        url: chemin_site + '/control/ajax/save_file_circulaire',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: f_circ,
        success: function(from_serv) {
            try {
                var reponse = $.parseJSON(from_serv);
            } catch (err) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de l\'envoi du fichier de circulaire vers le serveur.', 'bottom', 'right', null, null);
                return '';
            }
            if (reponse.code == 1) {
                send_valeur(reponse.link, reponse.ext, reponse.name);
            } else {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de l\'envoi du fichier de circulaire vers le serveur.', 'bottom', 'right', null, null);
            }
        },
        error: function() {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de l\'envoi du fichier de circulaire vers le serveur.', 'bottom', 'right', null, null);
        }
    });
}

function enregistrer() {
    if (controle_all_data()) {
        if ($('#field_fichier_circulaire').val() == '') {//NOTE fichier CI
            send_valeur();
        } else {
            send_valeur_with_f_circ();
        }
    }
}

function annuler() {
    swal({
        title: "Annulation!",
        text: 'Annuler l\'opération (pli#' + ID_PLI + ')?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, annuler!",
        cancelButtonText: "Non!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        in_load_pli();
        $.get(chemin_site + MY_URL + '/cancel/' + ID_PLI, {}, function() {
            window.location.href = chemin_site + MY_URL;
        });
    });
}

function show_info_ctrl() {
    $.get(chemin_site + '/control/ajax/activity', {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            //$('#mdl_display_activity_title').html(data.login);
            $('#mdl_display_activity_body').html(data.ihm);
            $('[data-toggle="tooltip"]').tooltip();
            $('#mdl_display_activity').modal('show');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function copyToClipboard7cmc7(id_cmc7){
    var cmc7 = $(id_cmc7).val();
    var text = '0';
    if(cmc7.length >= 7){
        text = cmc7.substring(0, 7);
    }
    var dummy = document.createElement("input");
    document.body.appendChild(dummy);
    dummy.setAttribute('value', text);
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
}

function load_from_data_addr_client(id_g_field, class_g_field) {
    var data = {
        id_client: $.trim($('#'+id_g_field+'ctm_nbr').val())
    };
    if(data.id_client == ''){
        swal("Chargement de donn\351es client...", "Num\351ro client requis!", "error");
        return '';
    }
    swal({
        title: 'Charger les donn\351es du client#'+data.id_client+' ?',
        text: 'Les donn\351es sur cette interface seront écrasées!',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/control/ajax/data_addr_client/'+data.id_client, {}, function(from_serv) {
            try {
                var reponse = $.parseJSON(from_serv);
            } catch (err) {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de la recuperation des donn&eacute;es.', 'bottom', 'right', null, null);
                return '';
            }
            if (reponse.code == 0) {
                swal({
                    title: "R\351ussi!",
                    text: "Donn\351es du client#" + data.id_client + " import\351es",
                    type: "success",
                    timer: 1000,
                    showConfirmButton: true
                });
                var client = reponse.client;
                for(var col in client){
                    if($('#'+id_g_field+col)){
                        if($('#'+id_g_field+col).hasClass('sel_field_mvmnt_mdl')){
                            $('#'+id_g_field+col).selectpicker('val', client[col]);
                        }else{
                            $('#'+id_g_field+col).val(client[col]);
                        }
                        var his_twin = $('#'+id_g_field+col).attr('twin');
                        /*if(typeof his_twin !== typeof undefined && his_twin !== false){
                            $('#'+his_twin).val(client[col]);
                        }*/
                        if(($('#'+his_twin)).length > 0){
                            $('#'+his_twin).val(client[col]);
                        }
                    }
                }
                // $('.'+class_g_field+'.field_adress_mdl').attr('disabled', '');
                $.AdminBSB.input.activate();
            } else {
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: '+reponse.msg, 'bottom', 'right', null, null);
                swal("Serveur", "Client#"+data.id_client+" introuvalble!", "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}

function maj_twin(me) {
    var val_num = $.trim(me.val());
    var id_twin = me.attr('my_twin');
    if(($('#'+id_twin)).length > 0){
        $('#'+id_twin).val(val_num);
    }
    var class_field_addr = me.attr('my_field_addr');
    $('.'+class_field_addr).each(function(){
        if(val_num == ''){
            $(this).removeAttr('disabled').removeAttr('readonly');
        }else{
            $(this).attr('disabled','').attr('readonly','');
        }
    });
}

function load_from_data_promo(me, class_g_field, my_id, id_chx, id_art) {
    if(!MODE_SAISIE_BATCH){
        return '';
    }
    var data = {
        code_promo: $.trim(me.val())
    };
    if(data.code_promo == ''){
        // swal("Chargement de donn\351es promotion...", "Code promotion requis!", "error");
        field_error(me);
        $('select.'+class_g_field).each(function(){
            $(this).selectpicker('destroy').html('').selectpicker();
        });
        // showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Code promotion vide.', 'bottom', 'right', null, null);
        return '';
    }
    $.get(chemin_site + '/control/ajax/data_promo_cba/'+data.code_promo, {}, function(from_serv) {
        try {
            var reponse = $.parseJSON(from_serv);
        } catch (err) {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de la recuperation des donn&eacute;es promotion.', 'bottom', 'right', null, null);
            return '';
        }
        if (reponse.code == 0) {
            var promos = reponse.promos;
            $('#'+id_chx).selectpicker('destroy').html('');
            var options = '';
            var ln_ligne_promo = promos.length;
            for (var index = 0; index < ln_ligne_promo; index++) {
                var promo = promos[index];
                options += '<option value="'+promo.code_choix_promo+'"'+(index == 0 ? ' selected' : '')+' data-subtext=" - '+promo.code_choix_promo+' - '+promo.code_art+'" >'+promo.lib_choix+'</option>';
            }
            field_ok(me);
            if(ln_ligne_promo == 0){
                field_error(me);
                options += '<option value="" selected data-subtext="" >Code promotion introuvable</option>';
            }
            $('#'+id_chx).html(options).selectpicker();
            load_from_data_promo_art(my_id, id_chx, id_art);
        } else {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: '+reponse.msg, 'bottom', 'right', null, null);
            // swal("Serveur", "Client#"+data.id_client+" introuvalble!", "error");
        }
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Recuperation donn&eacute;es promotion &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        // swal("Serveur", "Erreur serveur!", "error");
    });
}

function load_from_data_promo_art(id_sel_code_promo, id_sel_code_chx, id_me) {
    var data = {
        code_promo: $('#'+id_sel_code_promo).val()
        ,code_chx: $('#'+id_sel_code_chx).val()
    };
    $('#'+id_me).selectpicker('destroy').html('').selectpicker();
    field_error($('#'+id_sel_code_chx));
    $.post(chemin_site + '/control/ajax/data_promo_cba_art', data, function(from_serv) {
        try {
            var reponse = $.parseJSON(from_serv);
        } catch (err) {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Erreur lors de la recuperation des codes produit correspondant.', 'bottom', 'right', null, null);
            return '';
        }
        if (reponse.code == 0) {
            var promos = reponse.promos;
            $('#'+id_me).selectpicker('destroy').html('');
            var options = '';
            var ln_ligne_promo = promos.length;
            for (var index = 0; index < ln_ligne_promo; index++) {
                var promo = promos[index];
                options += '<option value="'+promo.code_art+'"'+(index == 0 ? ' selected' : '')+' data-subtext=" - '+promo.code_choix_promo+'" >'+promo.code_art+'</option>';
            }
            field_ok($('#'+id_sel_code_chx));
            if(ln_ligne_promo == 0){
                field_error($('#'+id_sel_code_chx));
                options += '<option value="" selected data-subtext="" >Aucun code produit trouvé</option>';
            }
            $('#'+id_me).html(options).selectpicker();
        } else {
            showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: '+reponse.msg, 'bottom', 'right', null, null);
            // swal("Serveur", "Client#"+data.id_client+" introuvalble!", "error");
        }
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Recuperation donn&eacute;es codes produit &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        // swal("Serveur", "Erreur serveur!", "error");
    });
}

function compile_data_client(class_g) {
    var client = {};
    $('.'+class_g).each(function(){
        var code = $(this).attr('my_code');
        client[code] = $(this).val();
    });
    return client;
}

function interface_mode() {
    if(MODE_SAISIE_BATCH){
        $('.direct_only').addClass('hidden');
        $('.batch_only').removeClass('hidden');
    }else{
        $('.batch_only').addClass('hidden');
        $('.direct_only').removeClass('hidden');
    }
}

function mode_advantage() {
    swal({
        title: 'Traiter ce pli(#'+ID_PLI+') en mode saisie Advantage?',
        text: 'Le changement est irreversible.',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/control/ajax/mode_advantage/' + ID_PLI, {}, function(rep) {
            if (rep == 0) {
                swal({
                    title: "R\351ussi!",
                    text: "Rechargement en mode saisie directe advantage.",
                    type: "success",
                    //timer: 1000,
                    showConfirmButton: false
                    ,closeOnConfirm: false
                });
                in_load_pli();
                setTimeout(function() {
                    window.location.href = chemin_site + MY_URL + '?direct=1';
                }, 2000);
            }else if (rep == 1) {
                swal({
                    title: 'BATCH d\351j\340 envoy\351',
                    text: 'Les données BATCH sont déjà dans Advantage (CRM), donc ne sont plus modifiables.',
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Valider",
                    // cancelButtonText: "Annuler",
                    showLoaderOnConfirm: true,
                    closeOnConfirm: false
                }, function() {
                    in_load_pli();
                    window.location.href = chemin_site + MY_URL + '?direct=1';
                });
            }else{
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: '+reponse.msg, 'bottom', 'right', null, null);
                swal("Serveur", "Erreur lors de l'op\351ration.", "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}

function u_montant(montant='0.00') {
    montant = ('' + montant).replace(',', '.');
    montant = montant.replace(/[^0-9.]/g, '');
    montant = Number.parseFloat(montant);
    montant = isNaN(montant) ? 0 : montant;
    if(Number.isInteger(montant)){return ''+montant+'.00'; }
    var montant_str = montant.toString(10);
    return /^\d+\.\d{1}$/.test(montant_str) ? montant_str + '0' : montant_str;
}

$( document ).ready(function() {
    /* Menu */
    $('.visible_menu').show();

});
function raccourci(){
    $('#mdl_display_raccourci').modal('show');
}
