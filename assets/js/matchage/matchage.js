$("#send-form").click(function () {
    var societe     = $("#id_societe").val();

    if(societe == ''){
        swal("Choix de la société", "Il faut choisir la société pour le matchage", "warning");
        return false;
    }

    var form_data   = new FormData();
    form_data.append('userfile', $("#userfile")[0].files[0]);
    form_data.append('id_societe', $("#id_societe").val());
    $.ajax({
        url: s_url+"matchage/matchage/upload_csv",
        type: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var msg = data.split('::');
            if(msg[0] == 'success'){
                do_match(msg[1],msg[2]);
            }
            else{
                $("#upload-error").html('');
                $("#upload-error").append(msg[1]).show();
                setTimeout(function () {
                    $("#upload-error").hide();
                }, 3000);
            }
        }
    });
});

$("#validate-match").click(function () {
    var societe     = $("#id_societe").val();

    if(societe == ''){
        swal("Choix de la société", "Il faut choisir la société pour le matchage", "warning");
        return false;
    }

    swal({
        title: "Validation matchage",
        text: "Voulez-vous vraiment valider ce matchage?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4CAF50",
        confirmButtonText: "Oui",
        cancelButtonText: "Non",
        closeOnConfirm: false
    }, function () {
        $.post(s_url + '/matchage/matchage/direct_validation', {id_soc : societe}, function(reponse) {
            if(reponse != ''){
                alert(reponse);
            }
            else{
                swal("Succès!", "Matchage validé!", "success");
                window.location.href = s_url+"matchage/matchage";
            }
        });
    });

});

$("#checkbox_nofile").change(function () {
    if($(this).is(':checked')){
        $("#send-form").hide();
        $("#validate-match").show();
    }
    else{
        $("#validate-match").hide();
        $("#send-form").show();
    }
});

function show_col(){
    var col = $(".hidden-column");
    var base_adv = $(".base-id-adv");
    var base_vv = $(".base-id-vv");
    if (col.is(':visible')){
        col.hide();
        base_adv.attr('colspan',5);
        base_vv.attr('colspan',9);
    }
    else{
        col.show();
        base_adv.attr('colspan',9);
        base_vv.attr('colspan',13);
    }
}

function do_match(filename,id_soc){
    $("#match-content").html('');
    var progress = $(".progress");
    var lib_progress = $("#lib-progress");
    progress.show();
    lib_progress.show();
    var elem = $(".progress-bar");
    var width = 0;
    var id_intervalle = setInterval(frame, 10000);
    function frame() {
        if (width >= 100) {
            clearInterval(id_intervalle);
            elem.css({'width':width+'%'});
            elem.text(width+'%');
        } else {
            width = width+1;
            elem.css({'width':width+'%'});
            elem.text(width+'%');
        }
    }
    $.post(s_url + 'matchage/matchage/display_match_bis', {name : filename, id_soc:id_soc}).done(function (reponse) {
        width = 100;
        setTimeout(function(){
            progress.hide();
            lib_progress.hide();
            $('#match-content').html(reponse)
        }, 1300);
    }).fail(function (xhr,status,error) {
        width = 100;
        lib_progress.text('Erreur lors du matchage!').css({'color':'red'});
        lib_progress.append('  <a href="'+s_url+'/matchage/matchage">retour</a>');
    });
}

function show_cheque(id_doc){
    var displayer = $('.displayer-content');
    var chq_disp  = $('.chq-displayer');
    var info_chq = $('.info-chq');
    displayer.hide();
    chq_disp.html('');
    $.post(s_url + 'matchage/matchage/get_chq', {id_pli : id_doc}, function(reponse) {
        chq_disp.append(reponse);
        displayer.slideDown(500);
        info_chq.css({'margin-top':'530px'});
    });
}

function close_displayer(){
    var displayer = $('.displayer-content');
    var info_chq = $('.info-chq');
    displayer.slideUp(500);
    info_chq.css({'margin-top':''});
}

function change_status(id){
    var state = $("#status-"+id).val();
    var montadv = $.trim($("#montadv-"+id).text());
    var montvv = $.trim($("#montvv-"+id).text());
    if(state != ''){
        if(state == '1'){
            if(montadv == montvv){
                $.post(s_url + 'matchage/matchage/update_statut_chq', {id_chq : id, state:state, montant:montvv}, function(reponse) {
                    if(reponse != ''){
                        alert(reponse);
                    }
                });
            }
            else{
                $("#status-"+id).val('');
                swal("Erreur", "Le montant n'a pas été mis à jour", "error");
            }
        }
        else{
            $.post(s_url + 'matchage/matchage/update_statut_chq', {id_chq : id, state:state,montant:''}, function(reponse) {
                if(reponse != ''){
                    alert(reponse);
                }
            });
        }

    }
}

function change_status_adv(id){
    var state = $("#statusadv-"+id).val();
    if(state != ''){
        if(state == '4'){
            swal({
                title: "Retirer chèque",
                text: "Voulez-vous vraiment retirer ce chèque? Le chèque ne sera plus visible après le matchage!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4CAF50",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm){
                    $.post(s_url + 'matchage/matchage/update_statut_chq', {id_chq : id, state:state,montant:''}, function(reponse) {
                        if(reponse != ''){
                            alert(reponse);
                        }
                    });
                    swal.close();
                }
                else {
                    $("#statusadv-"+id).val('');
                }
            });
        }
        else{
            $.post(s_url + 'matchage/matchage/update_statut_chq', {id_chq : id, state:state,montant:''}, function(reponse) {
                if(reponse != ''){
                    alert(reponse);
                }
            });
        }
    }
}

function change_montant(id){
    var montadv = $.trim($("#montadv-"+id).text());
    var montvv = $("#montvv-"+id);
    swal({
        title: "Modification montant",
        text: "Nouveau montant:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        cancelButtonText: "Annuler",
        inputPlaceholder: "Montant..."
    }, function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue === "") {
            swal.showInputError("Il faut renseigner un montant!"); return false
        }
        if(inputValue == montadv){
            //swal("Succès", "Le montant a été modifié avec succès", "success");
            montvv.text(inputValue);
            swal.close();
        }
        else{
            swal.showInputError("Les montants ne correspondent pas!"); return false
        }
    });
}

function change_comment(id){
    var commvv = $("#commvv-"+id).val();
    $.post(s_url + 'matchage/matchage/update_comment_chq', {id_chq : id, comm:commvv}, function(reponse) {
        if(reponse != ''){
            alert(reponse);
        }
    });
}

function change_comment_adv(id){
    var commadv = $("#commadv-"+id).val();
    $.post(s_url + 'matchage/matchage/update_comment_chq', {id_chq : id, comm:commadv}, function(reponse) {
        if(reponse != ''){
            alert(reponse);
        }
    });
}

function change_comment_ci(id){
    var commci = $("#commci-"+id).val();
    $.post(s_url + 'matchage/matchage/update_comment_chq', {id_chq : id, comm:commci}, function(reponse) {
        if(reponse != ''){
            alert(reponse);
        }
    });
}

function change_etat(id){
    var etat = $("#etat-"+id).val();
    if(etat != '0'){
        $.post(s_url + 'matchage/matchage/update_etat_chq', {id_chq : id, etat:etat}, function(reponse) {
            if(reponse != ''){
                alert(reponse);
            }
        });
    }
}

function check_states(){
    var err = 0;
    $("select.chq-state").each(function () {
        var val = $(this).val();
        if(val==''){
            swal("Statut des chèques", "Il y a encore des chèques qui n'ont pas de statut (Validé/A reporter)", "warning");
            err++;
        }
    });
    return err;
}

function check_etats(){
    var err = 0;
    $("select.chq-etat").each(function () {
        var val = $(this).val();
        if(val=='0'){
            swal("Etat des chèques", "Il ya encore des chèques en CI", "warning");
            err++;
        }
    });
    return err;
}

function check_match(id_soc){
    var c_states = check_states();
    var c_etats = check_etats();

    if(c_states == 0 && c_etats == 0){
        $.post(s_url + 'matchage/matchage/update_match', {e_match : '1', id_soc : id_soc}, function(reponse) {
            if(reponse != ''){
                alert(reponse);
            }
            else{
                $("#terminer").attr('disabled','disabled').hide();
                $("#exportation").show();
                $("#validation").removeAttr('disabled');
                $("select.chq-state").attr('disabled','disabled');
                $("select.chq-etat").attr('disabled','disabled');
                swal("Succès!", "Matchage terminé!", "success");
            }
        });
    }

}

function redo_match(id_soc){
    $.post(s_url + 'matchage/matchage/update_match', {e_match : '0', id_soc : id_soc}, function(reponse) {
        if(reponse != ''){
            alert(reponse);
        }
        else{
            window.location.href = s_url+"matchage/matchage";
        }
    });
}

function validate_match(id_soc){
    swal({
        title: "Validation matchage",
        text: "Voulez-vous vraiment valider ce matchage?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4CAF50",
        confirmButtonText: "Oui",
        cancelButtonText: "Non",
        closeOnConfirm: false
    }, function () {
        $.post(s_url + '/matchage/matchage/update_match', {e_match : '2', id_soc : id_soc}, function(reponse) {
            if(reponse != ''){
                alert(reponse);
            }
            else{
                swal("Succès!", "Matchage validé!", "success");
                window.location.href = s_url+"matchage/matchage";
            }
        });
    });

}

function export_match(){
    window.location.href = s_url+"matchage/matchage/export_chq_before_reb";
}

function report_manuel(){ // pour mettre tous les chèques à reporter
    $(".chq-state").val('0');

    setTimeout(function(){
        $(".chq-state").each(function(){
        var myid = $(this).attr('id');
        var splittedid = myid.split('-');
        if(splittedid[0] == 'statusadv'){ // pour les chèques absents dans ADV
            var realid = splittedid[1];
            change_status_adv(realid);
        }
    });},5000);
}