$("#send-form").click(function () {
    $.ajax({
        url: s_url+"matchage/matchage_ckd/upload_csv",
        type: "POST",
        data: new FormData($("#upload-form")[0]),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var msg = data.split('::');
            if(msg[0] == 'success'){
                do_match(msg[1]);
            }
            else{
                $("#upload-error").html('');
                $("#upload-error").append(msg[1]).show();
                setTimeout(function () {
                    $("#upload-error").hide();
                }, 3000);
            }
        }
    });
});

function show_col(){
    var col = $(".hidden-column");
    var base_adv = $(".base-id-adv");
    var base_vv = $(".base-id-vv");
    if (col.is(':visible')){
        col.hide();
        base_adv.attr('colspan',5);
        base_vv.attr('colspan',10);
    }
    else{
        col.show();
        base_adv.attr('colspan',7);
        base_vv.attr('colspan',11);
    }
}

function do_match(filename){
    $("#match-content").html('');
    var progress = $(".progress");
    var lib_progress = $("#lib-progress");
    progress.show();
    lib_progress.show();
    var elem = $(".progress-bar");
    var width = 0;
    var id_intervalle = setInterval(frame, 1500);
    function frame() {
        if (width >= 100) {
            clearInterval(id_intervalle);
            elem.css({'width':width+'%'});
            elem.text(width+'%');
        } else {
            width = width+1;
            elem.css({'width':width+'%'});
            elem.text(width+'%');
        }
    }
    $.post(s_url + 'matchage/matchage_ckd/display_match_bis', {name : filename}).done(function (reponse) {
        width = 100;
        setTimeout(function(){
            progress.hide();
            lib_progress.hide();
            $('#match-content').html(reponse)
        }, 1300);
    }).fail(function (xhr,status,error) {
        width = 100;
        lib_progress.text('Erreur lors du matchage!').css({'color':'red'});
        lib_progress.append('  <a href="'+s_url+'/matchage/matchage_ckd">retour</a>');
    });
}

function show_cheque(id_doc){
    var displayer = $('.displayer-content');
    var chq_disp  = $('.chq-displayer');
    var info_chq = $('.info-chq');
    displayer.hide();
    chq_disp.html('');
    $.post(s_url + 'matchage/matchage_ckd/get_chq', {id_pli : id_doc}, function(reponse) {
        chq_disp.append(reponse);
        displayer.slideDown(500);
        info_chq.css({'margin-top':'530px'});
    });
}

function close_displayer(){
    var displayer = $('.displayer-content');
    var info_chq = $('.info-chq');
    displayer.slideUp(500);
    info_chq.css({'margin-top':''});
}

function change_status(id_pli,id_doc){
    var state = $("#status-"+id_pli+id_doc).val();
    var montadv = $.trim($("#montadv-"+id_pli+id_doc).text());
    var montvv = $.trim($("#montvv-"+id_pli+id_doc).text());
    var typekdo = $("#typekdo-"+id_pli+id_doc).val();
    if(state != ''){
        if(state == '1'){
            if(montadv == montvv){
                $.post(s_url + 'matchage/matchage_ckd/update_statut_chq', {id_pli : id_pli, id_doc : id_doc, state:state, montant:montvv, typekdo : typekdo}, function(reponse) {
                    if(reponse != ''){
                        alert(reponse);
                    }
                });
            }
            else{
                $("#status-"+id_pli+id_doc).val('');
                swal("Erreur", "Le montant n'a pas été mis à jour", "error");
            }
        }
        else{
            $.post(s_url + 'matchage/matchage_ckd/update_statut_chq', {id_pli : id_pli, id_doc : id_doc, state:state,montant:'',typekdo:typekdo}, function(reponse) {
                if(reponse != ''){
                    alert(reponse);
                }
            });
        }

    }
}

function change_status_adv(id_pli,id_doc){
    var state = $("#statusadv-"+id_pli+id_doc).val();
    var typekdo = $("#typekdoadv-"+id_pli+id_doc).val();
    if(state != ''){
        if(state == '3'){
            swal({
                title: "Retirer chèque",
                text: "Voulez-vous vraiment retirer ce chèque? Le chèque ne sera plus visible après le matchage!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4CAF50",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm){
                    $.post(s_url + 'matchage/matchage_ckd/update_statut_chq', {id_pli : id_pli,id_doc : id_doc, state:state,montant:'', typekdo:''}, function(reponse) {
                        if(reponse != ''){
                            alert(reponse);
                        }
                    });
                    swal.close();
                }
                else {
                    $("#statusadv-"+id_pli+id_doc).val('');
                }
            });
        }
        else if(state == '1'){
            $.post(s_url + 'matchage/matchage_ckd/update_statut_chq', {id_pli : id_pli,id_doc:id_doc, state:state,montant:'',typekdo:typekdo}, function(reponse) {
                if(reponse != ''){
                    alert(reponse);
                }
            });
        }
        else{
            $.post(s_url + 'matchage/matchage_ckd/update_statut_chq', {id_pli : id_pli, id_doc:id_doc, state:state,montant:'',typekdo:''}, function(reponse) {
                if(reponse != ''){
                    alert(reponse);
                }
            });
        }
    }
}

function change_montant(id_pli,id_doc){
    var montadv = $.trim($("#montadv-"+id_pli+id_doc).text());
    var montvv = $("#montvv-"+id_pli+id_doc);
    swal({
        title: "Modification montant",
        text: "Nouveau montant:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        cancelButtonText: "Annuler",
        inputPlaceholder: "Montant..."
    }, function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue === "") {
            swal.showInputError("Il faut renseigner un montant!"); return false
        }
        if(inputValue == montadv){
            //swal("Succès", "Le montant a été modifié avec succès", "success");
            montvv.text(inputValue);
            swal.close();
        }
        else{
            swal.showInputError("Les montants ne correspondent pas!"); return false
        }
    });
}

function change_comment(id_pli,id_doc){
    var commvv = $("#commvv-"+id_pli+id_doc).val();
    $.post(s_url + 'matchage/matchage_ckd/update_comment_chq', {id_pli : id_pli, id_doc : id_doc, comm:commvv}, function(reponse) {
        if(reponse != ''){
            alert(reponse);
        }
    });
}

function change_comment_adv(id){
    var commadv = $("#commadv-"+id).val();
    $.post(s_url + 'matchage/matchage_ckd/update_comment_chq', {id_chq : id, comm:commadv}, function(reponse) {
        if(reponse != ''){
            alert(reponse);
        }
    });
}

function change_comment_ci(id){
    var commci = $("#commci-"+id).val();
    $.post(s_url + 'matchage/matchage_ckd/update_comment_chq', {id_chq : id, comm:commci}, function(reponse) {
        if(reponse != ''){
            alert(reponse);
        }
    });
}

function change_etat(id){
    var etat = $("#etat-"+id).val();
    if(etat != '0'){
        $.post(s_url + 'matchage/matchage_ckd/update_etat_chq', {id_chq : id, etat:etat}, function(reponse) {
            if(reponse != ''){
                alert(reponse);
            }
        });
    }
}

function check_states(){
    var err = 0;
    $("select.chq-state").each(function () {
        var val = $(this).val();
        if(val==''){
            swal("Statut des chèques", "Il y a encore des chèques qui n'ont pas de statut (Validé/A reporter)", "warning");
            err++;
        }
    });
    return err;
}

function check_etats(){
    var err = 0;
    $("select.chq-etat").each(function () {
        var val = $(this).val();
        if(val=='0'){
            swal("Etat des chèques", "Il ya encore des chèques en CI", "warning");
            err++;
        }
    });
    return err;
}

function check_match(){
    var c_states = check_states();
    var c_etats = check_etats();

    if(c_states == 0 && c_etats == 0){
        $.post(s_url + 'matchage/matchage_ckd/update_match', {e_match : '1'}, function(reponse) {
            if(reponse != ''){
                alert(reponse);
            }
            else{
                $("#terminer").attr('disabled','disabled').hide();
                $("#exportation").show();
                $("#validation").removeAttr('disabled');
                $("select.chq-state").attr('disabled','disabled');
                $("select.chq-etat").attr('disabled','disabled');
                swal("Succès!", "Matchage terminé!", "success");
            }
        });
    }

}

function redo_match(){
    $.post(s_url + 'matchage/matchage_ckd/update_match', {e_match : '0'}, function(reponse) {
        if(reponse != ''){
            alert(reponse);
        }
        else{
            window.location.href = s_url+"matchage/matchage_ckd";
        }
    });
}

function validate_match(){
    var c_states = check_states();
    var c_etats = check_etats();
    if(c_states == 0 && c_etats == 0){
        swal({
            title: "Validation matchage",
            text: "Voulez-vous vraiment valider ce matchage?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4CAF50",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false
        }, function () {
            $.post(s_url + '/matchage/matchage_ckd/update_match', {e_match : '2'}, function(reponse) {
                if(reponse != ''){
                    alert(reponse);
                }
                else{
                    swal("Succès!", "Matchage validé!", "success");
                    window.location.href = s_url+"matchage/matchage_ckd/report_ckd";
                }
            });
        });
    }

}

function export_match(){
    window.location.href = s_url+"matchage/matchage_ckd/export_chq_before_reb";
}