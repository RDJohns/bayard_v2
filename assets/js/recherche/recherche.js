var listeFiltre = [0];
var iDfiltre    = 0 ;
var table;
var idClick = 0;
var histo = 0;
var tousDocs = 0;
var paiement = 0;
var mouvement = 0;
var typeTraitement; /*mail ou sftp */
var recordTotal = 0;
var chargementSpinner = '<center><div class="preloader"><div class="spinner-layer pl-black"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>';
/***************************/


    $("body").on("change",'.c-ra-filtre',function(){ 
        iDfiltre = $(this).val();
        //event.preventDefault();
        if(iDfiltre.trim() != "" && iDfiltre != 0)
        {
            listeFiltre.push(iDfiltre); /*utile pour form dynamique*/
            changerFiltre();
        }
       
    });


    


$(function () {
    
    $(".btn-recherche").show();
    init_nsprogress();
    
    
});  
/********fct */
function getSourceTtmt(ttmt)
{
    listeFiltre = [0];
    iDfiltre    = 0 ;
    
    typeTraitement = ttmt;

    $.ajax({
        url : s_url +'recherche/avancee/setFiltre/'+ttmt,
        type: 'POST',
        data:{ ttmt:ttmt},
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success:function(res){
            $("#champ-de-recheche").html("");
            $("#colonne-recherche").html("");
            /* <div class="col-xs-6 col-sm-6" id="champ-de-recheche"></div>
                            <div class="col-xs-6 col-sm-6" >
                                <div class="body" id="colonne-recherche"></div>
                            </div><!-- fin **-->*/
            changerFiltre();
        }
    });

    /*1 - courrier; 2-flux*/
}
function changerFiltre() 
{
    var aDesactiver = [];
    aDesactiver = listeFiltre;
    var idFiltreChamp = iDfiltre;
    var selected = $('select option:selected');
    var rubriqueID = selected.closest('optgroup').attr('value');
    
    raColonne(aDesactiver);

    $.ajax({
    url : s_url +'recherche/avancee/getFiltreRa',
    type: 'POST',
    data:{ id:iDfiltre,desactiver:aDesactiver},
    beforeSend: function() {
        GedGoToPageLogin();
    },
    success:function(res){
    $("#liste-filtre").html("");
    $("#liste-filtre").html(res);

    if(idFiltreChamp > 0)
    {
        ajouterChamp(idFiltreChamp);
    }
    
    $(function () {
        $.AdminBSB.browser.activate();
        $.AdminBSB.leftSideBar.activate();
        $.AdminBSB.rightSideBar.activate();
        $.AdminBSB.navbar.activate();
        $.AdminBSB.dropdownMenu.activate();
        $.AdminBSB.input.activate();
        $.AdminBSB.select.activate();
        $.AdminBSB.search.activate();
        setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
    });
    }
});
}
function ajouterChamp(iDfiltre)
{
    $.ajax({
        url : s_url +'recherche/avancee/ajouterChamp',
        type: 'POST',
        data:{ id:iDfiltre},
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success:function(res){
            $("#champ-de-recheche").append(res);
        
        $(function () {
            $.AdminBSB.browser.activate();
            $.AdminBSB.leftSideBar.activate();
            $.AdminBSB.rightSideBar.activate();
            $.AdminBSB.navbar.activate();
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.input.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.search.activate();
            initDate();
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        });
        }
    });
}
function supprimerChamp(elm,iDfiltre)
{   
    listeFiltre = jQuery.grep(listeFiltre, function(value) {return value != iDfiltre;});
    
    iDfiltre = 0;
    changerFiltre();
    $("."+elm).remove();
}


function initDate()
{
      var start = moment();
      
      var end = moment();
      $('input[name="daterange"]').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
            'Aujourd\'hui': [moment(), moment()],
             'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             '7 jours précédents': [moment().subtract(6, 'days'), moment()],
             '30 derniers jours': [moment().subtract(29, 'days'), moment()],
             'Ce mois': [moment().startOf('month'), moment().endOf('month')],
             'Mois précedent': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          locale:{
            format: 'YYYY/MM/DD',
            daysOfWeek: ["Dim","Lun","Mar","Mer","Jeu","Ven","Sam"],
            monthNames: ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"],
            firstDay: 1},
      }, cb);
  
      cb(start, end);
}
function cb(start, end) {
    $('input[name="daterange"] span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}

function rechercher(type)
{
    /*0: recherche; 1 : excel */

    var postData = [];
    var listeColonneText =[];

    var go = 1;
    $(".recherche-avancee").each(function(){
        
        var tmp = [];
        var lastClass = $($(this)).attr('class').split(' ').pop();
        var filter  = "in";

        if (/date/i.test(lastClass))
        {
            filter = "date";
        }
        else
        {
            filter = $(".operateur_"+lastClass+" :selected").val();
        }

       
        var bufferValue = $("#"+lastClass.trim()).val();
        if(!bufferValue && $("#label_"+lastClass).text().trim() != "")
        {
            notifUSer("Merci de renseigner le champ : "+$("#label_"+lastClass).text());
            go = 0;
            return 0;
        }
        if(filter != undefined && bufferValue != undefined && lastClass.trim() !="open")
        {
            tmp =  {"colonne":lastClass,"filter":filter,"value":bufferValue};
            postData.push(tmp);
        }
        
    });
    
    /** ra-colonne-liste*/
    
    
    $("#ra-colonne-liste option:selected").each(function()
    {
      //  console.log($(this).text()+"*");
        listeColonneText.push($(this).text());
    });
    var listeColonne = $("#ra-colonne-liste").val();
    
   
   
    if(go == 1 )
    {
        if(type == 1 )
        {
            exportTableRa(listeColonneText,unique(postData),listeColonne);
        }
        else
        {
            createTHtable(listeColonneText,unique(postData),listeColonne);
        }
        
    }
   

   // console.log(unique(postData));
}

function exportTableRa(listeColonneText,postData,listeColonne)
{
   
    $.ajax({
        url : s_url +'recherche/export/setExport',
        type: 'POST',
        data:{ myData: unique(postData),listeColonne:listeColonne,listeColonneText:listeColonneText},
        success:function(res){
           window.location =  s_url +'recherche/export';
        }
    });

   
    return false;
}

function createTHtable(listeColonneText,postData,listeColonne)
{
    $.ajax({
        url : s_url +'recherche/avancee/createTHtable',
        type: 'POST',
        data:{ listeColonneText:listeColonneText},
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success:function(res){
            $("#listePli").html("");
            $("#listePli").html(res);
           createBodyTable(postData,listeColonne);
           console.log(postData);
           console.log(listeColonne);
        }
    });
}

function afficherDetailFlux(idClick_)
{
    idClick = idClick_;
   
    $("#detail-click-label-flux").text("");
    $("#detail-click-label-flux").text("Visualisation flux #"+idClick_);
    $('.nav-tabs > li:first-child > a')[0].click();
    $("#detail-click-flux").modal('show');
    
    infoGleFlux();
    
}
function createBodyTable(postData,listeColonne)
{
    $('#table-liste-data').dataTable().fnDestroy();

    $(".row-total").text();

    table = $('#table-liste-data').DataTable({
        "language": {
            processing:     "Traitement en cours...",
            search:         "Rechercher&nbsp;:",
            lengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur <span class='row-total'> _TOTAL_ </span> &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "paging": true,
        "ordering": false,
        // "responsive": true,
        "processing": true, 
        "serverSide": true,
        fixedHeader:true,
        "scrollX": true,
        "scrollY": "600px",
        "scrollCollapse": true,
        "searching": false,
        dom: 'flrtip',
        buttons: [
            'copy', 'csv', 'excel'
        ],
        "info": true,   // control table information display field
       /* "stateSave": true,*/  //restore table state on page reload,
        "lengthMenu": [[10, 20, 100, -1], [10, 40, 100,"tous "]],    
        "ajax": {
        "url": s_url +'recherche/avancee/afficherTable',
        "type": "POST",
        "data": function ( param ) {
            param.myData 	= unique(postData);
            param.listeColonne 	= listeColonne;
            param.recordTotal_ = $(".row-total").text();
        },
        "columnDefs": [
            { "orderable": false, "targets": 0 }
            ]
        },
        
		 fixedColumns:   {
            leftColumns: 2 },
    "drawCallback": function( settings ) {
        $.AdminBSB.dropdownMenu.activate();
        $.AdminBSB.select.activate();
        $.AdminBSB.input.activate();
    }
});
}
function unique(obj){
    var uniques=[];
    var stringify={};
    for(var i=0;i<obj.length;i++)
    {
       var keys=Object.keys(obj[i]);
       keys.sort(function(a,b) {return a-b});
       var str='';
        for(var j=0;j<keys.length;j++)
        {
           str+= JSON.stringify(keys[j]);
           str+= JSON.stringify(obj[i][keys[j]]);
        }
        if(!stringify.hasOwnProperty(str))
        {
            uniques.push(obj[i]);
            stringify[str]=true;
        }
    }
    return uniques;
}

function raColonne(aDesactiver)
{
    
    //colonne-recherche
    $.ajax({
        url : s_url +'recherche/avancee/raColonne',
        type: 'POST',
        data:{ id:aDesactiver},
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success:function(res){
            $("#colonne-recherche").html("");
            $("#colonne-recherche").html(res);
        $(function () {
            $.AdminBSB.browser.activate();
            $.AdminBSB.leftSideBar.activate();
            $.AdminBSB.rightSideBar.activate();
            $.AdminBSB.navbar.activate();
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.input.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.search.activate();
            initMultiSelect();
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        });
        }
    });
}

function initMultiSelect()
{
    $("#ra-colonne-liste").multiSelect({
        selectableHeader: "<b>Colonnes disponibles</b><br/><input type='text' class='field-input' placeholder='Rechercher un texte' autocomplete='off' style='margin-bottom: 14px;'>",
        selectionHeader: "<b>Colonnes sélectionnées</b><input type='text' class='field-input' placeholder='Rechercher un texte' autocomplete='off' style='margin-bottom: 14px;'>",
        afterInit: function(ms){
          var that = this,
              $selectableSearch = that.$selectableUl.prev(),
              $selectionSearch = that.$selectionUl.prev(),
              selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
              selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
             
          that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
          .on('keydown', function(e){
            if (e.which === 40)
            {
              that.$selectableUl.focus();
              return false;
            }
          });
      
          that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
          .on('keydown', function(e){
            if (e.which == 40)
            {
              that.$selectionUl.focus();
              return false;
            }
          });
        },
        afterSelect: function(){
          this.qs1.cache();
          this.qs2.cache();
        },
        afterDeselect: function(){
          this.qs1.cache();
          this.qs2.cache();
        }
          
    });
}


function afficherDetail(idClick_)
{
    idClick = idClick_;
   /* $("#detail-click-label").text("");
    $("#detail-click-label").text("Visualisation pli #".idClick_);*/
    $("#detail-click-label").text("");
    $("#detail-click-label").text("Visualisation pli #"+idClick_);
    histo = 0;
    tousDocs = 0;
    paiement = 0;
    mouvement = 0;
    
    $('.nav-tabs > li:first-child > a')[0].click();

    /*$('#detail-click').modal('show');
    infoGle();*/
    detail_pli(idClick_);
}

function afficherInformation(type)
{
    switch(type) {
        case 'mvt':
                infoMouvement();
          break;

          case 'doc':
                load_img_docs(idClick);
              break;
          case 'paie':
                afficherPaiement(idClick);
          break;

          case 'histo':
                afficher_histo(idClick);
            
            break;
      }
}
function infoGle()
{
    $("#info-gle").html("");
    $.ajax({
        url : s_url +'recherche/avancee/infoGle/'+idClick,
        type: 'POST',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success:function(res){
            
            $("#info-gle").html(res);
        $(function () {
            $.AdminBSB.browser.activate();
            $.AdminBSB.leftSideBar.activate();
            $.AdminBSB.rightSideBar.activate();
            $.AdminBSB.navbar.activate();
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.input.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.search.activate();
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        });
        }
    });
}

function infoMouvement()
{
    $("#info-mvt").html("");
    $.ajax({
        url : s_url +'recherche/avancee/infoMouvement/'+idClick,
        type: 'POST',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success:function(res){
            
            $("#info-mvt").html(res);
        $(function () {
            $.AdminBSB.browser.activate();
            $.AdminBSB.leftSideBar.activate();
            $.AdminBSB.rightSideBar.activate();
            $.AdminBSB.navbar.activate();
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.input.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.search.activate();
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        });
        }
    });
}

function afficherPaiement()
{
    $("#info-paie").html("");
    $.ajax({
        url : s_url +'recherche/avancee/getPaiement/'+idClick,
        type: 'POST',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success:function(res){
            
            $("#info-paie").html(res);
        $(function () {
            $.AdminBSB.browser.activate();
            $.AdminBSB.leftSideBar.activate();
            $.AdminBSB.rightSideBar.activate();
            $.AdminBSB.navbar.activate();
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.input.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.search.activate();
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        });
        }
    });
}

function init_nsprogress(){
    $( document ).ajaxStart(function() {
        $(".btn-recherche").hide();
        $(".preloader-reception").show();
        NProgress.start();
    });
    $(document).ajaxStop(function() {
        NProgress.done();
        $(".btn-recherche").show();
        $(".preloader-reception").hide();
    });
}

function afficher_histo(id_pli) {
    $.get(s_url + '/visual/visual/histo_pli/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            
            notifUSer(" Erreur serveur ");
            return '';
        }
        if (data.code == '1') {
            $('#info-doc').html("");
            $('#info-histo').html(data.ihm);
            histo = idClick;
        }else {
            notifUSer(" Erreur serveur ");
        }
    });
}

function load_img_docs(id_pli) {
    $.get(s_url + '/visual/visual/get_img_docs/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            notifUSer(" Erreur serveur ");
            return '';
        }
        if (data.code == '1') {
            $('#info-doc').html("");
            $('#info-doc').html(data.img);
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            tousDocs = idClick;
           
        } else {
            notifUSer(" Erreur serveur ");
        }
    });
}

function load_img_doc(id_doc) {
    $.get(s_url + '/visual/visual/get_img_doc/' + id_doc, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            notifUSer(" Erreur serveur ");
            return '';
        }
        if (data.code == '1') {
            $('#zone_img_temp').html(data.img);
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            $('#img_temp_recto').trigger('click');
        } else {
            notifUSer(" Erreur serveur ");
        }
    });
}

function infoAbo()
{
    
    $("#info-abo-flux").html("");
    $.ajax({
        url : s_url +'recherche/avancee/getAbonnement/'+idClick,
        type: 'POST',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success:function(res){
            
            $("#info-abo-flux").html(res);
        $(function () {
            $.AdminBSB.browser.activate();
            $.AdminBSB.leftSideBar.activate();
            $.AdminBSB.rightSideBar.activate();
            $.AdminBSB.navbar.activate();
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.input.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.search.activate();
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        });
        }
        });
   
}
function infoGleFlux()
{
    $("#info-gle-flux").html("");
    $.ajax({
        url : s_url +'recherche/avancee/infoGleFlux/'+idClick,
        type: 'POST',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success:function(res){
            
            $("#info-gle-flux").html(res);

        $(function () {
            $.AdminBSB.browser.activate();
            $.AdminBSB.leftSideBar.activate();
            $.AdminBSB.rightSideBar.activate();
            $.AdminBSB.navbar.activate();
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.input.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.search.activate();
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        });
        }
    });

    infoAbo();
    visu_histo();
}
//visu historique flux
function visu_histo()
{
    
    $('#info-histo-flux').html('');
    $.get(s_url + '/admin/support_push/histoFLux/'+idClick, {}, function(res) {
        $('#info-histo-flux').html(res);
    });
   
   
   
}




function detail_pli(id_pli) { 

    $('#ttmt-ko-img-content').html("");
    $('#ttmt-ko-img-content').html(chargementSpinner+"<br/><center>Chargement image ...</center>");
      $.get(s_url + '/visual/visual/detail_pli/' + id_pli, {}, function (reponse) {
          try {
              var data = $.parseJSON(reponse);
          } catch (error) {
              showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
              return '';
          }
          if (data.code == '1') {
              jsPanel.create({
                  headerTitle: data.titre,
                  headerToolbar: data.sous_titre,
                  footerToolbar: data.footer,
                  content: data.body,
                  contentSize: {
            width:  window.innerWidth * 0.9,
            height: window.innerHeight * 0.8
                  },
                  theme: 'light',
                  onbeforemaximize: function (panel) {
                      $('html,body').scrollTop(0);
                      return true;
          },
            callback: function() {
            this.content.style.padding = '10px';
          }
              });
              $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        
          } else {
              showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
          }
      }).fail(function () {
          showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération échouée!.', 'top', 'right', null, null);
          // swal("Serveur", "Erreur serveur!", "error");
      });
  }
  
  function load_docs_offset(id_pli, offset, id_dom) {
      $.get(s_url + '/visual/visual/view_docs_offset/' + id_pli + '/' + offset + '/' + id_dom, {}, function (reponse) {
          try {
              var data = $.parseJSON(reponse);
          } catch (error) {
              showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
              return '';
          }
          if (data.code == '1') {
              console.log(id_dom);
              $('#' + id_dom).html(data.ihm);
              $('[data-toggle="tooltip"]').tooltip();
              $('[data-toggle="popover"]').popover();
          } else {
              showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
          }
      });
  }
  
  function load_img_doc_face(id_doc, face = 0) {
      $.get(s_url + '/visual/visual/get_img_doc/' + id_doc, {}, function (reponse) {
          try {
              var data = $.parseJSON(reponse);
          } catch (error) {
              showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
              return '';
          }
          if (data.code == '1') {
              $('#zone_img_temp').html(data.img);
              $('[data-magnify=gallery]').magnify({
                  footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
              });
              if (face == 0) {
                  $('#img_temp_recto').trigger('click');
              } else {
                  $('#img_temp_verso').trigger('click');
              }
          } else {
              showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
          }
      });
  }

/******************/