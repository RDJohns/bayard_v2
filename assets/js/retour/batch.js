$(function() {
    //load_table();
    console.log(code_promo);
   
     init_compos();
    if(num_payeur.length>0) {
        $('#num_payeur').autocomplete({
            source : num_payeur
        });
    }
    if(nom_payeur.length>0) {
        console.log('rezrere');
        $('#nom_payeur').autocomplete({
            source : nom_payeur
        });
    }
    if(num_dest.length>0) {
        $('#num_dest').autocomplete({
            source : num_dest
        });
    }
    if(nom_dest.length>0) {
        $('#nom_dest').autocomplete({
            source : nom_dest
        });
    } 
    if(code_produit.length>0) {
        $('#code_titre').autocomplete({
            source : code_produit
        });
    } 
    if(code_promo.length>0) {
        $('#code_promo').autocomplete({
            source : code_promo
        });
    } 
    if(nom_fichier.length>0) {
        $('#nom_fichier').autocomplete({
            source : nom_fichier
        });
    } 
 });
     
 
 //initialisation datepicker
 function init_compos() {
     $('.dt_field').bootstrapMaterialDatePicker({
         format: 'DD/MM/YYYY',
         lang: 'fr',
         clearButton: true,
         clearText: 'Vider',
         cancelText: 'Annuler',
         weekStart: 1,
         switchOnClick: true,
         time: false
     }).on('change', function(e, date) {
         if ($(this).val() == '') {
             $(this).closest('.form-line').removeClass('focused');
         } else {
             $(this).closest('.form-line').addClass('focused');
         }
     });
 }
 
 $(document).on("keypress", function(e){
    if(e.which == 13){
        load_table();
    }
});
 
 
 function load_table() {
     table =  $('#tb_batch').DataTable({
         'bprocessing': true,
         'bServerSide': true,
         'bScrollCollapse': true,
         'aLengthMenu': [
             [25, 50, 100],
             [25, 50, 100]
         ],
         'pageLength': 25,
         'bDestroy': true,
         //'bStateSave': true,
         'bFilter': false,
         'sScrollX': true,
         'oLanguage': {
             'sLengthMenu': '_MENU_ lignes par page',
             'sZeroRecords': "Aucun pli",
             'sEmptyTable': "Aucun pli",
             'sInfo': "de _START_ \340 _END_ sur _TOTAL_ plis",
             'sInfoFiltered': '(Total : _MAX_ plis)',
             'sLoadingRecords': 'Chargement en cours...',
             'sProcessing': 'Traitement en cours...',
             'oPaginate': {
                 'sFirst': 'PREMIERE',
                 'sPrevious': 'PRECEDENTE',
                 'sNext': 'SUIVANTE',
                 'sLast': 'DERNIERE'
             },
             'sSearch': 'Rechercher: '
         },
         'ajax': {
             'url': chemin_site + '/anomalie/batch/getBatch',
             'sServerMethod': 'POST',
             'data': function(d) {
                 d.deb_env_batch = $("#deb_env_batch").val();
                 d.fin_env_batch = $("#fin_env_batch").val();
                 d.deb_courrier = $("#deb_courrier").val();
                 d.fin_courrier = $("#fin_courrier").val();
                 d.sel_soc = $("#sel_soc").val();
                 d.id_pli = $("#txt_recherche_id_pli").val();
                 d.num_payeur = $("#num_payeur").val();
                 d.nom_payeur = $("#nom_payeur").val();
                 d.num_dest = $("#num_dest").val();
                 d.nom_dest = $("#nom_dest").val();
                 d.code_titre = $("#code_titre").val();
                 d.code_promo = $("#code_promo").val();
                 d.nom_fichier = $("#nom_fichier").val();
             }
         },
         /*"order": [
             [0, "desc"]
         ],*/
         'fnDrawCallback': function(oSettings) {
             $('[data-toggle="tooltip"]').tooltip();
             set_action();
         },
         aoColumnDefs: [
             { 'bSortable': false, 'aTargets': [0] },
             {
                 'aTargets': [0,1,2,3,4,5,6,7,8,9,10,11,12],
                 "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                     $(nTd).addClass('align-center');
                 }
             }//,
             //{ "width": "30%", "aTargets": [13] }
         ],
         
         // aoColumnDefs: [{
         //     'aTargets': [3, 4, 5, 12, 13],
         //     "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
         //         $(nTd).addClass('align-center');
         //     }
         // }, { 'bSortable': false, 'aTargets': [12, 13] }],
         // 'columns': [
         //     { 'data': 'id_flux', 'targets': [0] },
         //     { 'data': 'source', 'targets': [1] },
             
         // ]
     });
     getStatBatch();
     window.location.href = '#search_result';
     //new $.fn.dataTable.FixedColumns( table );
 }

 //get total pli batch
 function getStatBatch()
 {
    $.ajax({
        "url":chemin_site + '/anomalie/batch/statBatch',
        "type": "POST",
        "data"    : {
            deb_env_batch :$("#deb_env_batch").val(),
            fin_env_batch : $("#fin_env_batch").val(),
            deb_courrier : $("#deb_courrier").val(),
            fin_courrier : $("#fin_courrier").val(),
            sel_soc : $("#sel_soc").val(),
            id_pli : $("#txt_recherche_id_pli").val(),
            num_payeur : $("#num_payeur").val(),
            nom_payeur : $("#nom_payeur").val(),
            num_dest : $("#num_dest").val(),
            nom_dest : $("#nom_dest").val(),
            code_titre : $("#code_titre").val(),
            code_promo : $("#code_promo").val(),
            nom_fichier : $("#nom_fichier").val()
        },
        "success": function(res) {
           data = JSON.parse(res);
           console.log(data);
            if(data.length>0){
                $('#pli_batch').text(data[0].pli_batch);
                $('#ok_batch').text(data[0].ok_batch);
                $('#ano_batch').text(data[0].ano_batch);
                $('#attente_retour').text(data[0].pas_retour_batch);
                $('#pli_recu').text(data[0].pli_recu);
                $('#pli_saisie').text(data[0].pli_saisie);
                $('#saisie_directe').text(data[0].saisie_directe);
                $('#saisie_batch').text(data[0].saisie_batch);
           } else {
                $('#pli_batch').text('0');
                $('#ok_batch').text('0');
                $('#ano_batch').text('0');
                $('#attente_retour').text('0');
                $('#pli_recu').text('0');
                $('#pli_saisie').text('0');
                $('#saisie_directe').text('0');
                $('#saisie_batch').text('0');
           }
           

        },
        "error": function(res) {
            swal("Serveur", "Erreur serveur!", "error");
        }
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        swal("Serveur", "Erreur serveur!", "error");
    });
 }
 
 function load_img_docs(id_pli) {
    $.get(chemin_site + '/visual/visual/get_img_docs/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#id_pli_docs').html(id_pli);
            $('#link_download_docs_zip').attr('href', $('#link_download_docs_zip').attr('href_init') + id_pli);
            $('#mdl_display_docs_body').html(data.img);
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            $('[data-toggle="tooltip"]').tooltip();
            $('#mdl_display_docs').modal('show');
            //$('#img_temp_recto_'+id_pli+'_0').trigger('click');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}