$(function() {
    $('#mdl_display_group_flux_by_older_body').html($('#html_loader').html());
    $('#mdl_display_group_flux_by_older').on('shown.bs.modal', function() {
        load_list_grp_fx_older();
    });
    $('#mdl_display_group_flux_by_older').on('hidden.bs.modal', function() {
        $('#mdl_display_group_flux_by_older_body').html($('#html_loader').html());
    });
    $('#mdl_display_lot_saisie').on('shown.bs.modal', function() {
        load_list_lot();
    });
    $('#menu_regroupement_flux_par_date').show();
    $('#menu_mdl_display_lot_saisie').show();
});
function afficher_modale_menu(id){
    $("#"+id).modal("show");
}

function new_lot_saisie() {
    in_load_flux();
    $.post(MY_URL+'/new_lot_saisie', {}, function(reponse) {
        $('#contenu').html(reponse);
        $('.sel_field').selectpicker();
    });
}

function load_flux() {
    $data = get_filters_value();
    in_load_flux();
    $.post(MY_URL + '/load_flux', $data, function(reponse) {
        $('#contenu').html(reponse);
        init_compo_field();
    });
}

function in_load_flux(){
    $('#contenu').html('<div id="div_loader" class="row m-t-25">'
                +'<div class="col-xs-12 align-center">'
                +'<div class="preloader pl-size-xl">'
                +'<div class="spinner-layer pl-light-blue">'
                +'<div class="circle-clipper left">'
                +'<div class="circle"></div>'
                +'</div>'
                +'<div class="circle-clipper right">'
                +'<div class="circle"></div>'
                +'</div>'
                +'</div>'
                +'</div>'
                +'</div>'
                +'</div>');
}

function next_flux() {
    $data = get_filters_value();
    in_load_flux();
    $.post(MY_URL + '/next_flux', $data, function(reponse) {
        $('#contenu').html(reponse);
        init_compo_field();
    });
}

function init_compo_field() {
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
    $('.sel_field').selectpicker();
    autosize($('textarea.auto-growth'));
    $.AdminBSB.input.activate();
    numerotation_abonnement();
    init_etat();
}

function init_etat() {
    gestion_multi_typo();
    $('#div_liste_abonnement').slimscroll({
        height: '84vh',
        color: 'rgba(0,0,0,0.5)',
        size: '8px',
        alwaysVisible: true,
    });
    change_state();
}

function get_view_abonnement() {
    $.get(chemin_site + '/push/saisie/ajax_push/new_abonnement/' + ID_SRC + '/' + $('#field_soc').val() , {}, function(reponse) {
        $('#div_liste_abonnement').append(reponse);
        $('select.sel_field_abonnement').selectpicker();
        $.AdminBSB.input.activate();
        numerotation_abonnement();
    });
}

function numerotation_abonnement() {
    var num = 0;
    $(".grp_field_abonnement").each(function() {
        num++;
        $($(this).find('.numerotation_abonnement')[0]).html('#'+num);
        $($(this).find('.clear_abonnement')[0]).show();
        if ($(".grp_field_abonnement").length == 1) {
            $($(this).find('.clear_abonnement')[0]).hide();
        }
        var ident = $(this).attr('my_ident');
        gestion_motif_ko_abonnement($('#field_abonnement_statut_'+ident).val(), 'dom_field_abonnement_motif_ko_'+ident);
    });
    gestion_multi_typo();
}

function gestion_motif_ko_abonnement(my_val, dom_motif_ko) {
    $('#'+dom_motif_ko).show();
    if(my_val == '1'){
        $('#'+dom_motif_ko).hide();
        $($('#'+dom_motif_ko).find('.field_abonnement')[0]).val('');
    }
}

function suppr_gr_field_abonnement(id_grp) {
    swal({
        title: "Confirmation!",
        text: "La suppression de cet abonnement entrainera la suppression de ses données.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Supprimer",
        cancelButtonText: "Annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        $(id_grp).remove();
        numerotation_abonnement();
    });
}

function gestion_multi_typo() {
    var first_typo = $($('select.field_abonnement_typo')[0]).val();
    $('#field_typo').selectpicker('val', first_typo);
    $("select.field_abonnement_typo").each(function() {
        if(first_typo != $(this).val()){
            $('#field_typo').selectpicker('val', '1');
        }
    });
}

function edit_pj(bt_edit_pj) {
    var li = bt_edit_pj.parent('li.list-group-item');
    var li = li[0];
    var span_txt = ($(li).find('.nom_fichier_pj'))[0];
    var pj = $(span_txt).html();
    swal({
        title: "Modification pi\350ce jointe!",
        text: 'Modifier "'+pj+'" en :',
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Nom du fichier"
    }, function (inputValue) {
        if (inputValue === false) return false;
        if ($.trim(inputValue) === "") {
            swal.showInputError("Nom invalide!"); return false;
        }
        if (compte_doublon_pj($.trim(inputValue)) > 0 && $.trim(inputValue) != pj) {
            swal.showInputError("fichier en doublon!"); return false;
        }
        $(span_txt).html(inputValue);
        //swal("Enregistrement!", "Nouveau nom: " + inputValue, "success");
        swal({
            title: "Enregistrement!",
            text: "Nouveau nom: " + inputValue,
            type: "success",
            timer: 1000,
            showConfirmButton: false
        });
    });
}

function del_pj(bt_del_pj) {
    var li = bt_del_pj.parent('li.list-group-item');
    var li = li[0];
    var span_txt = ($(li).find('.nom_fichier_pj'))[0];
    swal({
        title: "Confirmation!",
        text: 'Supprimer la pi\350ce jointe: "'+($(span_txt).html())+'"?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Supprimer",
        cancelButtonText: "Annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        $(li).remove();
    });
}

function add_pj() {
    swal({
        title: "Ajout pi\350ce jointe!",
        text: 'Nom du fichier :',
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Nom du fichier"
    }, function (inputValue) {
        if (inputValue === false) return false;
        if ($.trim(inputValue) === "") {
            swal.showInputError("Nom invalide!"); return false;
        }
        if (compte_doublon_pj($.trim(inputValue)) > 0) {
            swal.showInputError("fichier en doublon!"); return false;
        }
        $('#ul_list_pjs').append('<li class="list-group-item" is_ajoute="1" my_date=""><span class="nom_fichier_pj">'+$.trim(inputValue)+'</span> <span class="badge bg-pink m-t--5" onclick="del_pj($(this));"><i class="material-icons font-22 ico_modif_pj">clear</i></span><span class="badge bg-cyan m-t--5" onclick="edit_pj($(this));"><i class="material-icons font-22 ico_modif_pj">create</i></span></li>');
        swal({
            title: "Enregistrement!",
            text: "Nouvelle pi\351ce jointe: " + inputValue,
            type: "success",
            timer: 1000,
            showConfirmButton: false
        });
    });
}

function compte_doublon_pj(pj_name) {
    var nb = 0;
    $('#ul_list_pjs>li.list-group-item').each(function() {
        var pj = $($(this).find('span.nom_fichier_pj')[0]).html();
        if(pj_name == pj){
            nb++;
        }
    });
    return nb;
}

function change_soc() {
    var old_soc = ID_SOC;
    ID_SOC = $('#field_soc').val();
    var new_soc = ID_SOC;
    swal({
        title: "Confirmation!",
        text: 'La modification de la soci\351t\351 entrainera la r\351initialisation des champs "Titre" et "Typologie" de tous les abonnements de ce flux.',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuer",
        cancelButtonText: "Annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if(isConfirm){
            $.get(chemin_site + '/push/saisie/ajax_push/options_abonnement_titres/' + $('#field_soc').val() , {}, function(reponse) {
                $("select.field_abonnement_titre").each(function() {
                    $(this).selectpicker('destroy');
                    $(this).html(reponse);
                    $(this).selectpicker();
                });
                numerotation_abonnement();
                gestion_multi_typo();
            });
            $.get(chemin_site + '/push/saisie/ajax_push/options_abonnement_typos/' + ID_SRC + '/'  + $('#field_soc').val() , {}, function(reponse) {
                $("select.field_abonnement_typo").each(function() {
                    $(this).selectpicker('destroy');
                    $(this).html(reponse);
                    $(this).selectpicker();
                });
                numerotation_abonnement();
                gestion_multi_typo();
            });
        }else{
            $('#field_soc').selectpicker('val', old_soc);
            ID_SOC = old_soc;
        }
    });
}

function field_error(compos) {
    var parent = compos.closest('.form-line');
    if (parent.length > 0) {
        parent.addClass('focused').addClass('error').removeClass('warning').removeClass('success');
    }
}

function field_ok(compos) {
    var parent = compos.closest('.form-line');
    if (parent.length > 0) {
        parent.addClass('focused').removeClass('error').removeClass('warning').addClass('success');
    }
}

function control_abonnement() {
    var check = true;
    $(".grp_field_abonnement").each(function() {
        var ident = $(this).attr('my_ident');
        field_ok($('#field_abonnement_motif_ko_' + ident));
        if($('#field_abonnement_statut_' + ident).val() == '2' && $.trim($('#field_abonnement_motif_ko_' + ident).val()) == ''){
            if(check){
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Le motif du KO est obligatoire pour un abonnement KO!', 'bottom', 'right', null, null);
            }
            field_error($('#field_abonnement_motif_ko_' + ident));
            check = false;
        }
        var tarif = $('#field_abonnement_tarif_' + ident).val();
        field_ok($('#field_abonnement_tarif_' + ident));
        var reg = /^\d+([.,]\d+)?$/;
        if (!(reg.test($.trim(tarif))) && $.trim(tarif) != '') {
            if(check){
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Tarif invalide!', 'bottom', 'right', null, null);
            }
            field_error($('#field_abonnement_tarif_' + ident));
            check = false;
        }
    });
    return check;
}

function controle_data() {
    var ctrl_oblig = true;
    $(".oblig").each(function() {
        field_ok($(this));
        if ($.trim($(this).val()) == '') {
            field_error($(this));
            if(ctrl_oblig){
                showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Champ obligatoire non renseign&eacute;.', 'bottom', 'right', null, null);
            }
            ctrl_oblig = false;
        }
    });
    if(ctrl_oblig && control_abonnement()){
        return true;
    }
    return false;
}

function controle_escalade() {
    var ctrl_oblig = true;
    field_ok($('#field_motif_escalade'));
    if($('#field_statut').val() == 4 && $.trim($('#field_motif_escalade').val()) == ''){
        field_error($('#field_motif_escalade'));
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Le champ motif escalade est obligatoire pour un flux en &eacute;tat d\'escalade.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    return ctrl_oblig;
}

function controle_ko() {
    var ctrl_oblig = true;
    field_ok($('#field_motif_transfert'));
    if(/*$('#field_statut').val() == 8 &&*/ $.trim($('#field_motif_transfert').val()) == ''){
        field_error($('#field_motif_transfert'));
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Le champ motif KO est obligatoire pour un flux en KO.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    return ctrl_oblig;
}

function controle_ks() {
    var ctrl_oblig = true;
    field_ok($('#field_motif_ks'));
    if($('#field_statut').val() == 19 && $.trim($('#field_motif_ks').val()) == ''){
        field_error($('#field_motif_ks'));
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: Le champ motif KS-SRC est obligatoire pour un flux en KS.', 'bottom', 'right', null, null);
        ctrl_oblig = false;
    }
    return ctrl_oblig;
}

function valeur_abonnement() {
    var data = [];
    $(".grp_field_abonnement").each(function() {
        var ident = $(this).attr('my_ident');
        var abonnement = {
            id_flux: ID_FLUX
            ,titre: $('#field_abonnement_titre_' + ident).val()
            ,nom_abonne: $('#field_abonnement_nom_abon_' + ident).val()
            ,numero_abonne: $('#field_abonnement_num_abon_' + ident).val()
            ,nom_payeur: $('#field_abonnement_nom_payeur_' + ident).val()
            ,numero_payeur: $('#field_abonnement_num_payeur_' + ident).val()
            ,tarif: u_montant($('#field_abonnement_tarif_' + ident).val())
            ,typologie_abonnement: $('#field_abonnement_typo_' + ident).val()
            ,id_statut: $('#field_abonnement_statut_' + ident).val()
            ,motif_ko: $('#field_abonnement_statut_' + ident).val() == '1' ? '' : $('#field_abonnement_motif_ko_' + ident).val()
        };
        data.push(abonnement);
    });
    return data;
}

function valeur_pjs() {
    var data = [];
    $('#ul_list_pjs>li.list-group-item[is_ajoute="1"]').each(function() {
        var pjs = {
            id_flux: ID_FLUX
            ,nom_fichier: $($(this).find('span.nom_fichier_pj')[0]).html()
            ,extension_fichier: get_ext($($(this).find('span.nom_fichier_pj')[0]).html())
            ,date_injection: $(this).attr('my_date')
            ,ajoute: '1'
        };
        data.push(pjs);
    });
    return data;
}

function collecte_data() {
    return {
        id_flux: ID_FLUX
        ,typologie: $('#field_typo').val()
        ,nb_abonnement: ($(".grp_field_abonnement")).length
        ,entite: $('#field_entity').val()
        ,societe: $('#field_soc').val()
        ,id_source: ID_SRC
        ,motif_escalade: $('#field_motif_escalade').val()
        ,motif_transfert: $('#field_motif_transfert').val()
        ,motif_ks: $('#field_motif_ks').val()
        ,abonnements: JSON.stringify(valeur_abonnement())
        ,pjs: JSON.stringify(valeur_pjs())
        ,statut_pli: $('#field_statut').val()
    };
}

function get_ext(name_file) {
    var tab_f = $.trim(name_file).split('.');
    if(tab_f.length > 1){
        return tab_f[tab_f.length - 1];
    }
    return '';
}

function quick_save() {
    swal({
        title: "Confirmation!",
        text: 'Mettre \340 jour les donn\351es du flux?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuer",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }, function() {
        var data = collecte_data();
        $.post(chemin_site + '/push/saisie/ajax_push/quick_save', data, function(reponse) {
            if(reponse == '0'){
                swal({
                    title: "Sauvegard\351",
                    text: "Flux mis \340 jour.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            }else{
                swal({
                    title: "Operation annul\351e!",
                    text: "Il est possible que ce flux ne vous est plus assign\351.",
                    type: "error",
                    //timer: 2000,
                    showConfirmButton: true
                });
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
    });
}


function annuler() {
    swal({
        title: "Annulation!",
        text: "Liberer la saisie de ce flux?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Non!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function() {
        in_load_flux();
        $.get(MY_URL + '/unlock_cancel/' + ID_FLUX, {}, function() {
            window.location.href = MY_URL;
        });
    });
}

function terminer() {
    var data_to_serv = collecte_data();
    if (data_to_serv.statut_pli == 4) {
        escalade(data_to_serv);
    }else if(data_to_serv.statut_pli == 7){
        traiter(data_to_serv);
    }else if($.inArray(data_to_serv.statut_pli, ['21','22','23']) > -1){
        saisie_ko(data_to_serv);
    }else if (data_to_serv.statut_pli == 11) {
        anomalie_mail();
    }else if (data_to_serv.statut_pli == 12) {
        anomalie_pj();
    }else if (data_to_serv.statut_pli == 13) {
        hp();
    }else if (data_to_serv.statut_pli == 14) {
        anomalie_fichier();
    }else if (data_to_serv.statut_pli == 19) {
        ks_src(data_to_serv);
    }
    
}

function escalade(data_to_serv) {
    if (controle_escalade()) {
        swal({
            title: "Confirmation!",
            text: 'Mettre le flux#' + ID_FLUX + ' en escalade?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }, function() {
            $.post(chemin_site + '/push/saisie/ajax_push/escalade', data_to_serv, function(reponse) {
                if(reponse == '0'){
                    swal({
                        title: "Sauvegard\351",
                        text: "Flux mis en escalade.",
                        type: "success",
                        //timer: 1000,
                        showConfirmButton: false
                        ,closeOnConfirm: false
                    });
                    in_load_flux();
                    setTimeout(function() {
                        window.location.href = MY_URL + '?direct=1';
                    }, 2000);
                }else{
                    swal({
                        title: "Operation annul\351e!",
                        text: "Il est possible que ce flux ne vous est plus assign\351.",
                        type: "error",
                        //timer: 1000,
                        showConfirmButton: true
                    });
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            });
        });
    }
}

function anomalie_mail() {
    swal({
        title: "Confirmation!",
        text: 'Mettre le flux#' + ID_FLUX + ' en anomalie mail?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuer",
        cancelButtonText: "Annuler",
        //showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }, function() {
        swal({
            title: "Motif requis!",
            text: 'Motif de l\'anomalie :',
            type: "input",
            confirmButtonText: "Valider",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            animation: "slide-from-top",
            inputPlaceholder: "Motif..."
        }, function (inputValue) {
            if (inputValue === false) return false;
            if ($.trim(inputValue) == "") {
                swal.showInputError("Motif obligatoire!"); return false;
            }
            var data_to_serv = {
                id_flux: ID_FLUX
                ,motif: $.trim(inputValue)
            };
            $.post(chemin_site + '/push/saisie/ajax_push/anomalie_mail', data_to_serv, function(reponse) {
                if(reponse == '0'){
                    swal({
                        title: "Sauvegard\351",
                        text: "Flux mis en anomalie mail.",
                        type: "success",
                        //timer: 1000,
                        showConfirmButton: false
                        ,closeOnConfirm: false
                    });
                    in_load_flux();
                    setTimeout(function() {
                        window.location.href = MY_URL + '?direct=1';
                    }, 2000);
                }else{
                    swal({
                        title: "Operation annul\351e!",
                        text: "Il est possible que ce flux ne soit plus sous votre assignation.",
                        type: "error",
                        //timer: 1000,
                        showConfirmButton: true
                    });
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            });
        });
    });
}

function anomalie_pj() {
    swal({
        title: "Confirmation!",
        text: 'Mettre le flux#' + ID_FLUX + ' en anomalie PJ?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuer",
        cancelButtonText: "Annuler",
        //showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }, function() {
        swal({
            title: "Motif requis!",
            text: 'Motif de l\'anomalie :',
            type: "input",
            confirmButtonText: "Valider",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            animation: "slide-from-top",
            inputPlaceholder: "Motif..."
        }, function (inputValue) {
            if (inputValue === false) return false;
            if ($.trim(inputValue) == "") {
                swal.showInputError("Motif obligatoire!"); return false;
            }
            var data_to_serv = {
                id_flux: ID_FLUX
                ,motif: $.trim(inputValue)
            };
            $.post(chemin_site + '/push/saisie/ajax_push/anomalie_pj', data_to_serv, function(reponse) {
                if(reponse == '0'){
                    swal({
                        title: "Sauvegard\351",
                        text: "Flux mis en anomalie PJ.",
                        type: "success",
                        //timer: 1000,
                        showConfirmButton: false
                        ,closeOnConfirm: false
                    });
                    in_load_flux();
                    setTimeout(function() {
                        window.location.href = MY_URL + '?direct=1';
                    }, 2000);
                }else{
                    swal({
                        title: "Operation annul\351e!",
                        text: "Il est possible que ce flux ne soit plus sous votre assignation.",
                        type: "error",
                        //timer: 1000,
                        showConfirmButton: true
                    });
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            });
        });
    });
}

function anomalie_fichier() {
    swal({
        title: "Confirmation!",
        text: 'Mettre le flux#' + ID_FLUX + ' en anomalie fichier?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuer",
        cancelButtonText: "Annuler",
        //showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }, function() {
        swal({
            title: "Motif requis!",
            text: 'Motif de l\'anomalie :',
            type: "input",
            confirmButtonText: "Valider",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            animation: "slide-from-top",
            inputPlaceholder: "Motif..."
        }, function (inputValue) {
            if (inputValue === false) return false;
            if ($.trim(inputValue) == "") {
                swal.showInputError("Motif obligatoire!"); return false;
            }
            var data_to_serv = {
                id_flux: ID_FLUX
                ,motif: $.trim(inputValue)
            };
            $.post(chemin_site + '/push/saisie/ajax_push/anomalie_fichier', data_to_serv, function(reponse) {
                if(reponse == '0'){
                    swal({
                        title: "Sauvegard\351",
                        text: "Flux mis en anomalie fichier.",
                        type: "success",
                        //timer: 1000,
                        showConfirmButton: false
                        ,closeOnConfirm: false
                    });
                    in_load_flux();
                    setTimeout(function() {
                        window.location.href = MY_URL + '?direct=1';
                    }, 2000);
                }else{
                    swal({
                        title: "Operation annul\351e!",
                        text: "Il est possible que ce flux ne soit plus sous votre assignation.",
                        type: "error",
                        //timer: 1000,
                        showConfirmButton: true
                    });
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            });
        });
    });
}

function hp() {
    swal({
        title: "Confirmation!",
        text: 'Mettre le flux#' + ID_FLUX + ' en HP?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuer",
        cancelButtonText: "Annuler",
        //showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }, function() {
        swal({
            title: "Motif requis!",
            text: 'Motif de l\'anomalie :',
            type: "input",
            confirmButtonText: "Valider",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            animation: "slide-from-top",
            inputPlaceholder: "Motif..."
        }, function (inputValue) {
            if (inputValue === false) return false;
            if ($.trim(inputValue) == "") {
                swal.showInputError("Motif obligatoire!"); return false;
            }
            var data_to_serv = {
                id_flux: ID_FLUX
                ,motif: $.trim(inputValue)
            };
            $.post(chemin_site + '/push/saisie/ajax_push/hp', data_to_serv, function(reponse) {
                if(reponse == '0'){
                    swal({
                        title: "Sauvegard\351",
                        text: "Flux mis en HP.",
                        type: "success",
                        //timer: 1000,
                        showConfirmButton: false
                        ,closeOnConfirm: false
                    });
                    in_load_flux();
                    setTimeout(function() {
                        window.location.href = MY_URL + '?direct=1';
                    }, 2000);
                }else{
                    swal({
                        title: "Operation annul\351e!",
                        text: "Il est possible que ce flux ne soit plus sous votre assignation.",
                        type: "error",
                        //timer: 1000,
                        showConfirmButton: true
                    });
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            });
        });
    });
}

function saisie_ko(data_to_serv){
    if(controle_ko()){
        swal({
            title: "Confirmation!",
            text: 'Mettre le flux#' + ID_FLUX + ' en KO?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }, function() {
            $.post(chemin_site + '/push/saisie/ajax_push/saisie_ko', data_to_serv, function(reponse) {
                if(reponse == '0'){
                    swal({
                        title: "Sauvegard\351",
                        text: "Flux mis en KO.",
                        type: "success",
                        //timer: 1000,
                        showConfirmButton: false
                        ,closeOnConfirm: false
                    });
                    in_load_flux();
                    setTimeout(function() {
                        window.location.href = MY_URL + '?direct=1';
                    }, 2000);
                }else{
                    swal({
                        title: "Operation annul\351e!",
                        text: "Il est possible que ce flux ne soit plus sous votre assignation.",
                        type: "error",
                        //timer: 1000,
                        showConfirmButton: true
                    });
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            });
        });
    }
}

function ks_src(data_to_serv){
    if(controle_ks()){
        swal({
            title: "Confirmation!",
            text: 'Mettre le flux#' + ID_FLUX + ' en KS-SRC?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuer",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }, function() {
            $.post(chemin_site + '/push/saisie/ajax_push/ks', data_to_serv, function(reponse) {
                if(reponse == '0'){
                    swal({
                        title: "Sauvegard\351",
                        text: "Flux mis en KO-transfert SRC.",
                        type: "success",
                        //timer: 1000,
                        showConfirmButton: false
                        ,closeOnConfirm: false
                    });
                    in_load_flux();
                    setTimeout(function() {
                        window.location.href = MY_URL + '?direct=1';
                    }, 2000);
                }else{
                    swal({
                        title: "Operation annul\351e!",
                        text: "Il est possible que ce flux ne soit plus sous votre assignation.",
                        type: "error",
                        //timer: 1000,
                        showConfirmButton: true
                    });
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            });
        });
    }
}

function traiter(data_to_serv) {
    if(controle_data()){
        swal({
            title: 'Traitement termin\351?',
            text: 'Flux#' + ID_FLUX,
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Annuler!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.post(MY_URL + '/save_saisie_traite', data_to_serv, function(reponse) {
                if (reponse == 1) {
                    swal({
                        title: 'Enregistrement.',
                        text: 'Enregistrement réussi du flux#' + ID_FLUX,
                        type: 'success',
                        //timer: 2000,
                        showConfirmButton: false
                        ,closeOnConfirm: false
                    });
                    in_load_flux();
                    setTimeout(function() {
                        window.location.href = MY_URL + '?direct=1';
                    }, 2000);
                } else if (reponse == 2) {
                    swal({
                        title: 'Enregistrement saisie.',
                        text: 'Enregistrement réussi du flux#' + ID_FLUX + ' en lot saisie BIS.',
                        type: 'warning'
                        //,showConfirmButton: false
                        ,closeOnConfirm: false
                    }, function() {
                        in_load_flux();
                        window.location.href = MY_URL + '?direct=1';
                    });
                } else {
                    showNotification('alert-danger', '<i class="material-icons">error</i>'+reponse, 'top', 'right', null, null);
                    swal("Operation annul\351e!", "session perdue ou, il est possible que ce flux ne soit plus sous votre assignation.", "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            });
        });
    }
}

function load_list_grp_fx_older() {
    $('#mdl_display_group_flux_by_older_body').html($('#html_loader').html());
    $.get(MY_URL + '/list_grp_flux_older', {}, function(reponse) {
        $('#mdl_display_group_flux_by_older_body').html(reponse);
    });
}

function load_list_lot() {
    $("#lot_saisie_dtt").DataTable({
		bprocessing: true
		,bServerSide: true
		,bScrollCollapse: true
		,aLengthMenu: [[5, 10], [5, 10]]
		,pageLength: 5
		,bDestroy: true
		,bFilter: true
		//,sScrollX: true
		,oLanguage: {
			sLengthMenu: "_MENU_ lignes par page"
			,sZeroRecords: "Aucun lot"
			,sEmptyTable: "Aucun lot"
			,sInfo: "de _START_ \340 _END_ sur _TOTAL_ lot"
			,sInfoFiltered: "(Total : _MAX_ lot)"
			,sLoadingRecords: "Chargement en cours..."
			,sProcessing: "Traitement en cours..."
			,oPaginate: {
				sFirst: "PREMIERE"
				,sPrevious: "PRECEDENTE"
				,sNext: "SUIVANTE"
				,sLast: "DERNIERE"
			}
			,sSearch: "Rechercher: "
		}
		,ajax: {
			url: chemin_site + "/push/saisie/ajax_push/list_lot_saisies"
			,sServerMethod: "POST"
			,data: function(d) {
				d.dt = "";
			}
		}
		,order: [[0, "desc"]]
		,fnDrawCallback: function(oSettings) {
			$('[data-toggle="tooltip"]').tooltip();
		}
		,aoColumnDefs: [
			{
				aTargets: [5, 7, 9]
				,fnCreatedCell: function(nTd, sData, oData, iRow, iCol) {
					$(nTd).addClass("align-center");
				}
			}
		]
		,columns: [
			{ data: "dt", targets: [0] }
			,{ data: "id", targets: [1] }
			,{ data: "soc", targets: [2] }
			,{ data: "src", targets: [3] }
			,{ data: "typo", targets: [4] }
			,{ data: "nb", targets: [5] }
			,{ data: "flux", targets: [6] }
			,{ data: "nb_b", targets: [7] }
			,{ data: "flux_b", targets: [8] }
			,{ data: "tt", targets: [9] }
		]
	});
}

function change_state(){
    if(($('#field_statut')).length > 0){
        $('.line_motif_ko').hide();
        $('.line_motif_ks').hide();
        var status = $('#field_statut').val();
        if($.inArray(status, ['21', '22', '23']) > -1){
            $('.line_motif_ko').show();
        }
        if($.inArray(status, ['19']) > -1){
            $('.line_motif_ks').show();
        }
    }
}

function u_montant(montant='0.00') {
    montant = ('' + montant).replace(',', '.');
    montant = montant.replace(/[^0-9.]/g, '');
    montant = Number.parseFloat(montant);
    montant = isNaN(montant) ? 0 : montant;
    if(Number.isInteger(montant)){return ''+montant+'.00'; }
    var montant_str = montant.toString(10);
    return /^\d+\.\d{1}$/.test(montant_str) ? montant_str + '0' : montant_str;
}
