var listeModePaiement = ["mode_paiement_0"];
var docPli            = ""; /*pour copier les images de tous les docs*/
var idPli             = 0;
var spinner           = '<div class="preloader pl-size-xs"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>';
var societe;
var typeTtmt;
var postData;
var classElm = 0;
var anomalieType = "";

var etatFlux  = 0;
/*
$(".liste-boite-mail-dossier").on('click',function(){
    $('.clicked').removeClass('clicked');
    $(this).addClass('clicked');
    $("body").trigger('click');
 });
*/

$(".btn-typage").hide();
$(".liste-boite-mail-dossier").on({
	click: function(){
		$(".liste-boite-mail-dossier").css("background-color", "white");
		$('.clicked').removeClass('clicked');
		$(this).css("background-color", "#ccc");
		$(this).addClass('clicked');
		console.log(this);
		
	}  
});


$(function () {
	
	$.AdminBSB.browser.activate();
    $.AdminBSB.leftSideBar.activate();
    $.AdminBSB.rightSideBar.activate();
    $.AdminBSB.navbar.activate();
    $.AdminBSB.dropdownMenu.activate();
    $.AdminBSB.input.activate();
    $.AdminBSB.select.activate();
    $.AdminBSB.search.activate();
    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
    init_nsprogress();
});
/*********FCT******************/
function chargerPli()
{
   $(".class-loader").hide();
   $(".btn-typage-valide-push").show();
   $(".etat-traitement").hide();
   $(".content-traitement").hide();
   $(".content-mail").html("");
   $('.modal').modal('hide');
   var urlTraitement = "";
   $("#loader-flux").html(spinner);
   societe = $("#filtre-societe").val();
   typeTtmt = $("#filtre-traitement").val();
   var click = 1;
    var urlTraitement = 
   postData = {societe:societe,traitement:typeTtmt,click:click};
   //alert(typeTtmt);
   if(typeTtmt == 1)
   {
	   urlTraitement = s_url+'push/typage/Pli/listeEtatMail';
   }
   else{
	    urlTraitement = s_url+'push/typage/Pli/listeSftp';
   }
   
  
   $.ajax({
    url : urlTraitement,
    type : 'POST',
    data:postData,
    dataType : 'html',
    beforeSend: function() {
        GedGoToPageLogin();
    },
    success : function(ret, statut){
        $(".etat-push").html(ret);
		 $("#loader-flux").html("");
		 $(".etat-traitement").show();

    },

    error : function(resultat, statut, erreur){
        // $("#loader-flux").html("");
    },

    complete : function(resultat, statut){
		$("#loader-flux").html("");
		$(".etat-traitement").show();
    }

});

}

function getSftp(libelle,nbFlux,elm)
{
	$(".liste-boite-mail-dossier").css("background-color", "white");
    $("."+elm).css("background-color", "#ccc");
    $(".btn-typage").hide();
	$(".content-traitement").hide();
	
    if( nbFlux <= 0)
    {
        notifUSer("Pas de fichier à typer pour le dossier : "+libelle );
        $(".content-mail").html("");
        $(".content-body-mail").html("");
        return 0;
    }
	postData = {societe:societe,traitement:typeTtmt,libelle:libelle};
    $(".content-mail").html(spinner);
    $(".content-body-mail").html(spinner);
	$("#flux-content-rejet").html("");
	$("#flux-content-anomalie").html("");
	$.ajax({
        url : s_url+'push/typage/Pli/getFluxFichierAtraiter', 
        type : 'POST',
        dataType : 'json',
        data:postData,
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(ret, statut){
			
			if(ret.retour == 'ko')
			{
				chargerPli();
			}
			else
			{
				var strMail =  "";
			var flux = ret[0];
			
			/*left-panel */
			var base = s_url+'push/typage/Pli/download/'+flux.id_flux ;
			    strMail += "ID flux : "+flux.id_flux+'<br/>';
                strMail +='Fichier : <a  class="name_file" href="'+base+'" style="cursor:pointer;">'+flux.filename_origin+'</a>';
			/*strMail += "<span>Fichier : "+flux.filename_origin+"</span>";*/
			strMail += "<br/><span>Dossier : "+flux.sous_dossier+"</span>";
			strMail += "<br/><span>Date de réception : "+flux.date_reception+"</span>";
			$(".content-body-mail").html(strMail);
			$(".content-mail").html(strMail);
			$("#flux-content-rejet").html(strMail);
			$("#flux-content-anomalie").html(strMail);
            $("#id-flux").val(flux.id_flux);
            
            /*<span class="consigne-traitement-flux" id="id-consigne-traitement-flux">*/
            
			  var source = flux.id_source;
					if(source == 1)
					{
						$(".mail-anomalie").show();
					}
					else if(source == 2)
					{
						$(".sftp-anomalie").show();
					}
					$(".mail-sftp").show();
					
			/*contenu champ*/

             var postChamp;	
             
             getConsigneClient(flux.id_flux);

			 postChamp = {iDflux:flux.id_flux};
			  $.ajax({
                            url : s_url+'push/typage/Pli/getTypologie',
                            type : 'POST',
                            dataType : 'html',
                            data:postChamp,
                            beforeSend: function() {
                                GedGoToPageLogin();
                            },
                            success : function(ret, statut){
								
								$(".content-traitement").show();
                               
                                $(".ajax-champ").html("");
                                $(".ajax-champ").html(ret);
                                $(function () {
                                    $.AdminBSB.browser.activate();
                                    $.AdminBSB.leftSideBar.activate();
                                    $.AdminBSB.rightSideBar.activate();
                                    $.AdminBSB.navbar.activate();
                                    $.AdminBSB.dropdownMenu.activate();
                                    $.AdminBSB.input.activate();
                                    $.AdminBSB.select.activate();
                                    $.AdminBSB.search.activate();
                                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);});
                                    
                            },
                    
                            error : function(resultat, statut, erreur){},
                    
                            complete : function(resultat, statut){
                                $(function () {
                                    $.AdminBSB.browser.activate();
                                    $.AdminBSB.leftSideBar.activate();
                                    $.AdminBSB.rightSideBar.activate();
                                    $.AdminBSB.navbar.activate();
                                    $.AdminBSB.dropdownMenu.activate();
                                    $.AdminBSB.input.activate();
                                    $.AdminBSB.select.activate();
                                    $.AdminBSB.search.activate();
                                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);});
                            }
                    
                        });
						
						refreshEtatPush();
			}
			
			 
			 
			
						
		},

        error : function(resultat, statut, erreur){
            $(".content-body-mail").html("");
        },

        complete : function(resultat, statut){
            


        }

    });
	
}
function getMail(libelle,nbFlux,elm)
{
    $(".liste-boite-mail-dossier").css("background-color", "white");
    $("."+elm).css("background-color", "#ccc");
    $(".btn-typage").hide();
	 $(".content-traitement").hide();
    if( nbFlux <= 0)
    {
        notifUSer("Pas de mail à typer pour : "+libelle );
        $(".content-mail").html("");
        $(".content-body-mail").html("");
		chargerPli();
        return 0;
    }

    postData = {societe:societe,traitement:typeTtmt,libelle:libelle};
    $(".content-mail").html(spinner);
    $(".content-body-mail").html(spinner);
    $.ajax({
        url : s_url+'push/typage/Pli/getFluxMailAtraiter',
        type : 'POST',
        dataType : 'json',
        data:postData,
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(ret, statut){
				
				if(ret.retour == 'ko')
				{
					chargerPli();
				}
				else
				{
				$(".content-traitement").show();
                var flux = ret[0];
                var strMail =  "";
                var strObjet =  "";
                    strMail += "ID flux : "+flux.id_flux+'<br/>';
                    strMail += "<span>De : "+flux.from_flux+"    ("+flux.fromname_flux+")</span>";
                    strObjet = "<br/><span>Objet : "+flux.objet_flux+"</span>";
                   
                $(".content-mail").html(strMail+strObjet);
                strMail += "<br/><span>A : "+flux.to_flux+"</span>";
                strMail +=  strObjet;
                strMail += "<br/><span>Dossier : "+flux.dossier+"</span>";
                strMail += "<br/><span>Date envoi : "+flux.date_envoi_mail+"</span>";
                var base = s_url+'push/typage/Pli/download/'+flux.id_flux ;
                    strMail +='<br/>Fichier : <a  class="name_file" href="'+base+'" style="cursor:pointer;">'+flux.nom_fichier+'</a>';

                    $(".content-body-mail").html(strMail+'<br/><br/>');
					
					$("#flux-content-rejet").html(strMail);
					$("#flux-content-anomalie").html(strMail);
					
					$("#id-flux").val(flux.id_flux);
					
					var source = flux.id_source;
					if(source == 1)
					{
						$(".mail-anomalie").show();
					}
					else if(source == 2)
					{
						$(".sftp-anomalie").show();
					}
					$(".mail-sftp").show();
					
                       var postChamp;
                       getConsigneClient(flux.id_flux);
					   postChamp = {iDflux:flux.id_flux};
                        $.ajax({
                            url : s_url+'push/typage/Pli/getTypologie',
                            type : 'POST',
                            dataType : 'html',
                            data:postChamp,
                            beforeSend: function() {
                                GedGoToPageLogin();
                            },
                            success : function(ret, statut){
								
								$(".content-traitement").show();
                               
                                $(".ajax-champ").html("");
                                $(".ajax-champ").html(ret);
                                $(function () {
                                    $.AdminBSB.browser.activate();
                                    $.AdminBSB.leftSideBar.activate();
                                    $.AdminBSB.rightSideBar.activate();
                                    $.AdminBSB.navbar.activate();
                                    $.AdminBSB.dropdownMenu.activate();
                                    $.AdminBSB.input.activate();
                                    $.AdminBSB.select.activate();
                                    $.AdminBSB.search.activate();
                                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);});
                                    
                            },
                    
                            error : function(resultat, statut, erreur){},
                    
                            complete : function(resultat, statut){
                                $(function () {
                                    $.AdminBSB.browser.activate();
                                    $.AdminBSB.leftSideBar.activate();
                                    $.AdminBSB.rightSideBar.activate();
                                    $.AdminBSB.navbar.activate();
                                    $.AdminBSB.dropdownMenu.activate();
                                    $.AdminBSB.input.activate();
                                    $.AdminBSB.select.activate();
                                    $.AdminBSB.search.activate();
                                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);});
                            }
                    
                        });
						refreshEtatPush();
				}
			    
                   
        },

        error : function(resultat, statut, erreur){
            
        },

        complete : function(resultat, statut){
           


        }

    });
    $(function () {
        $.AdminBSB.browser.activate();
        $.AdminBSB.leftSideBar.activate();
        $.AdminBSB.rightSideBar.activate();
        $.AdminBSB.navbar.activate();
        $.AdminBSB.dropdownMenu.activate();
        $.AdminBSB.input.activate();
        $.AdminBSB.select.activate();
        $.AdminBSB.search.activate();
        setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);});
}

function nomPieceJointe(elm)
{
    var elmtDiv      = elm.split('nom_');
    var backElmt     = elm;
    var iTmp         = elmtDiv[1];
        iTmp         = iTmp*1;
   
    
        classElm +=1; 
    
    var actuelElmt = 'c-delete-'+classElm;

    var strDivNomPieceDynamic = '<div class="col-md-4 c-nom-piece-jointe '+actuelElmt+'" style="margin-bottom: 5px !important;">';
    strDivNomPieceDynamic +=' <b>Nom pièce jointe : </b>';
    strDivNomPieceDynamic +='<div class="input-group">';
    strDivNomPieceDynamic +='<div class="form-line">';
    strDivNomPieceDynamic +='<textarea rows="2" class="form-control no-resize text-nom-piece" placeholder="Saisir ici le nom de fichier ... "></textarea>';
           
    strDivNomPieceDynamic +='</div>';
    
    strDivNomPieceDynamic +='<button type="button" class="btn bg-blue-grey waves-effect" style="float:center;" onclick="supprimerNom(\''+actuelElmt+'\')" ><b>Supprimer</b></button>';
    strDivNomPieceDynamic +='</div>';
    strDivNomPieceDynamic +='</div>';
    $(".liste-div-nom-piece").append(strDivNomPieceDynamic);

}

function supprimerNom(elm)
{
    $("."+elm).remove();
}

function modalRejetFlux()
{
	var idFlux = $("#id-flux").val();
    $(".titre-anomalie").removeClass("bg-blue bg-black bg-brown bg-warning bg-orange");
    $(".titre-anomalie").addClass("bg-red");
	$(".text-rejet-piece").val("");

	/*$("#id-modal-motif-rejet").modal("show");
	$(".btn-annuler").show();*/
    $.ajax({ /*Verification si Pli en standBY */
        url : s_url+'push/typage/Pli/standBy/'+idFlux,
        type : 'POST',
        dataType : 'json',
        success : function(ret, statut){

            if(parseInt(ret) <= 0)
            {
                $("#id-modal-motif-rejet").modal("show");
                $(".btn-annuler").show();
            }
            else
            {
                alert("Ce flux #"+idFlux+" est en stand by!");
            }
        },

        error : function(resultat, statut, erreur){},

        complete : function(resultat, statut){}

    });

	$(".titre-anomalie").text("");
	$(".titre-anomalie").text("REJETER LE FLUX");
   ;

}

function rejeterFlux()
{
	var rejetText = $(".text-rejet-piece").val();
	
	var lengthRejet= rejetText.trim().length;
	
		if(lengthRejet <2)
		{
			alert("Merci de bien remplir le motif de rejet !");
			return 0;
		}
		
	if (confirm('Voulez-vous rejeter ce flux ?')) {
		
		var idFlux = $("#id-flux").val();
		var dataRejet = {motif:rejetText,idFlux:idFlux,anomalieType:'fichier'};
		$.ajax({
					url : s_url+'push/typage/Pli/rejet',
					type : 'POST',
					dataType : 'text',
					data:dataRejet,
					beforeSend: function() {
						GedGoToPageLogin();
					},
					success : function(ret, statut){
						
                      chargerPli();
					  //id-modal-motif-rejet
							
					},
			
					error : function(resultat, statut, erreur){},
			
					complete : function(resultat, statut){}
			
				});
	}
}

function envoiAnomalieFlux()
{
	var anomalie = $(".text-anomalie").val();
	
	var lengthAnomalie = anomalie.trim().length;
	
	if(lengthAnomalie <2)
	{
		alert("Merci de bien remplir le motif!");
		return 0;
	}
	
	if (confirm("Voulez-vous traiter ce flux en tant qu'anomalie ?")) {
		/*var idFlux = $("#id-flux").val();
		alert(anomalieType+idFlux);*/

        $(".spinner-modal").show();

		var idFlux = $("#id-flux").val();


		$(".btn-annuler").hide();
		var anomalieData = {idFlux:idFlux,anomalie:anomalie,typeTtmt:typeTtmt,etatFlux:etatFlux};
		
		
		// return 0;
		
		 $.ajax({
					url : s_url+'push/typage/Pli/envoiAnomalie',
					type : 'POST',
					dataType : 'text',
					data:anomalieData,
					beforeSend: function() {
						GedGoToPageLogin();
					},
					success : function(ret, statut){
						
                        $(".spinner-modal").show();
						
						$("#id-modal-motif-anomalie").modal("hide");
						$(".btn-annuler").show();
					  
						chargerPli();
							
					},
			
					error : function(resultat, statut, erreur){},
			
					complete : function(resultat, statut){}
			
				});
	} 
}

function modalAnomalieFlux(anomalie)
{
	var idFlux = $("#id-flux").val();
    $(".text-anomalie").val("");
	$(".spinner-modal").hide();
	$(".titre-anomalie").removeClass("bg-blue bg-black bg-brown bg-warning bg-orange");
	if(anomalie == 'pj')
	{
		$(".titre-anomalie").text("ANOMALIE PIECE JOINTE");
        $("div.titre-anomalie").addClass("bg-brown");
	}
	if(anomalie == 'mail')
	{
		$(".titre-anomalie").text("ANOMALIE MAIL");
        $("div.titre-anomalie").addClass("bg-orange");
	}
	if(anomalie == 'fichier')
	{
		$(".titre-anomalie").text("ANOMALIE FICHIER");
        $("div.titre-anomalie").addClass("bg-orange");
	}
	if(anomalie == 'hp')
	{
		$(".titre-anomalie").text("FLUX HORS PERIMETRE");
        $("div.titre-anomalie").addClass("bg-black");
	}
	anomalieType = anomalie;
	/**/


    $.ajax({ /*Verification si Pli en standBY */
        url : s_url+'push/typage/Pli/standBy/'+idFlux,
        type : 'POST',
        dataType : 'json',
        success : function(ret, statut){

            if(parseInt(ret) <= 0)
            {
                $(".btn-annuler").show();
                $("#id-modal-motif-anomalie").modal("show");
            }
            else
            {
                alert("Ce flux #"+idFlux+" est en stand by!");
            }
        },

        error : function(resultat, statut, erreur){},

        complete : function(resultat, statut){}

    });

}

function envoiMailHP()
{
	  if (confirm('Voulez-vous continuer et envoyer le mail?')) {
		var idFlux = $("#id-flux").val();
		typeTtmt = $("#filtre-traitement").val();
		var postHP = {idFlux:idFlux,traitement:typeTtmt};
		 $.ajax({
					url : s_url+'push/typage/Pli/horsPerimetre',
					type : 'POST',
					dataType : 'json',
					data:postHP,
					beforeSend: function() {
						GedGoToPageLogin();
					},
					success : function(ret, statut){
					   console.log(ret);
						if(ret.retour == 'ok')
						{
							chargerPli();
						}
						else
						{
							alert("erreur de la base de donnée");
						}
							
					},
			
					error : function(resultat, statut, erreur){},
			
					complete : function(resultat, statut){}
			
				});
	} 
}
/****************///
function listePlis()
{
    if (confirm('Voulez-vous continuer et envoyer un mail?')) {
		alert('Thanks for confirming');
	} 
}

function raccourci()
{
    $("#raccourci-modal").modal("show");
}

function isNumberKey(evt,elm)
       {
         var valElm = $("#"+elm).val();
         
         var strCount = charCount(valElm); 
         if(strCount > 1)
         {
            
            var str = valElm.slice('.', -1); 
            $("#"+elm).val(str);

            return false;
         }
         
         var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
          
       }




function init_nsprogress(){
    $(document).ajaxStart(function() {
        NProgress.start();
    });
    $(document).ajaxComplete(function() {
        NProgress.done();
    });
    $(document).ajaxSuccess(function() {
        NProgress.done();
    });
}

function validerPushTypage()
{
	 var nomPieceJointeMail     = [];
	 var autreInfo       		= [];
	 var infoSociete            = $("#typage-societe").val();
	 var typologie              = $("#typologie").val();
	 var entite			        = $("#entite").val();
	 var idFlux 				= $("#id-flux").val();
	 var flux					= [];
	 var abonnement 			= $("#abonnement").val();
	 
	 var pj						= 0 ;
	 var stop					= 0;
	 if(parseInt(infoSociete) == 0 || infoSociete == null)
	 {
        notifUSer("Choisir une socièté");
        $("#error-societe").show(500);

         setTimeout(function(){
             $("#error-societe").hide(500);
         }, 3000);
		stop = 1;
		return 0;
     }
	 
	if(parseInt(typologie) == 0 || typologie == null)
    {
        notifUSer("Choisir la typologie du flux");
        $("#error-typologie").show();
        setTimeout(function(){
            $("#error-typologie").hide(500);
        }, 3000);
		stop = 1;
        return 0;
    }
	
	if(parseInt(abonnement) == 0 || abonnement == null || abonnement.trim() == "")
	 {
        notifUSer("Nombre d'abonnement au moins 1");
		stop = 1;
		return 0;
     }
	 
	 $('textarea.text-nom-piece').each(function(){  
			var  textNomPiece = $(this).val();
			if(textNomPiece.trim() != "")
			{
				pj = 1;
				nomPieceJointeMail.push({nom_fichier_origine:textNomPiece,nom_fichier:textNomPiece,id_flux:idFlux,ajoute:1});
			}
	});
	
	$(".class-loader").show();
	$(".btn-typage-valide-push").hide();
	if(stop == 0 && idFlux > 0)
	{
		autreInfo = {typologie:typologie,entite:entite,societe:infoSociete,abonnement:parseInt(abonnement),idFlux:idFlux,pj:pj,nomPieceJointeMail:nomPieceJointeMail};
		
		//console.log(autreInfo);
        $.ajax({ /*Verification si Pli en standBY */
            url : s_url+'push/typage/Pli/standBy/'+idFlux,
            type : 'POST',
            dataType : 'json',
            success : function(ret, statut){

                if(parseInt(ret) <= 0)
                {
                    if (confirm('Voulez-vous enregistrer le typage de flux ?')) {
                        $.ajax({
                            url : s_url+'push/typage/Pli/saveTypage',
                            type : 'POST',
                            dataType : 'json',
                            data:autreInfo,
                            beforeSend: function() {
                                GedGoToPageLogin();
                            },
                            success : function(ret, statut){
                                console.log(ret);
                                if(ret.retour == 'ok')
                                {
                                    chargerPli();
                                }
                                else
                                {
                                    alert("erreur de la base de donnée");
                                }
                            },
                            error : function(resultat, statut, erreur){},
                            complete : function(resultat, statut){}

                        });
                    }
                    $(".class-loader").hide();
	                $(".btn-typage-valide-push").show();
                }
                else
                {
                    chargerPli();
                    alert("Ce flux #"+idFlux+" est en stand by!");

                }
            },

            error : function(resultat, statut, erreur){},

            complete : function(resultat, statut){}

        });
		

	}
}

function annulerTypage()
{
    var idFlux = $("#id-flux").val();

    $.ajax({ /*Verification si Pli en standBY */
        url : s_url+'push/typage/Pli/standBy/'+idFlux,
        type : 'POST',
        dataType : 'json',
        success : function(ret, statut){

            if(parseInt(ret) <= 0)
            {
                if (confirm('Voulez-vous annuler le traitement ?')) {
                    $(".class-loader").show();
                    $.ajax({
                        url : s_url+'push/typage/Pli/annulerTypage',
                        type : 'POST',
                        dataType : 'json',
                        beforeSend: function() {
                            GedGoToPageLogin();
                        },
                        success : function(ret, statut){
                            console.log(ret);
                            if(ret.retour == 'ok')
                            {
                                chargerPli();
                            }
                            else
                            {
                                alert("erreur de la base de donnée");
                            }
                        },
                        error : function(resultat, statut, erreur){},
                        complete : function(resultat, statut){}
                    });

                }
            }
            else
            {
                alert("Ce flux #"+idFlux+" est en stand by!");
                chargerPli();
            }
        },

        error : function(resultat, statut, erreur){},

        complete : function(resultat, statut){}

    });


}
function refreshEtatPush()
{
	 postData = {societe:societe,traitement:typeTtmt};
	  if(typeTtmt == 1)
   {
	   urlTraitement = s_url+'push/typage/Pli/listeEtatMail';
   }
   else{
	    urlTraitement = s_url+'push/typage/Pli/listeSftp';
   }
   
  
   $.ajax({
    url : urlTraitement,
    type : 'POST',
    data:postData,
    dataType : 'html',
    beforeSend: function() {
        GedGoToPageLogin();
    },
    success : function(ret, statut){
        $(".etat-push").html(ret);
		 $("#loader-flux").html("");
		 

    },

    error : function(resultat, statut, erreur){
        // $("#loader-flux").html("");
    },

    complete : function(resultat, statut){
		$("#loader-flux").html("");
		
    }
});
}




function getTypologie() {


    var societe = $("#typage-societe").val();
    var dataTypologie = {societe:societe,traitement:typeTtmt};
    $(".c-push-typologie").html("");
    $.ajax({
        url :s_url+'push/typage/Pli/listeTypologie',
        type : 'POST',
        data:dataTypologie,
        dataType : 'html',
        beforeSend: function() {
            GedGoToPageLogin();
        },
        success : function(ret, statut){
            $(".c-push-typologie").html("");
            $(".c-push-typologie").html(ret);

            $.AdminBSB.browser.activate();
            $.AdminBSB.leftSideBar.activate();
            $.AdminBSB.rightSideBar.activate();
            $.AdminBSB.navbar.activate();
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.input.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.search.activate();
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        },

        error : function(resultat, statut, erreur){

        },

        complete : function(resultat, statut){


        }
    });
}

function publiposter()
{
    var idFlux = $("#id-flux").val();
    
    if (confirm('Voulez-vous envoyer  ce flux pour publiposter ?'))
    {
        $.ajax({
            url :s_url+'push/typage/Pli/publiposter/'+idFlux,
            type : 'POST',
            dataType : 'json',
            beforeSend: function() {
                GedGoToPageLogin();
            },
            success : function(ret, statut){
                if(ret.retour == '1')
                    {
                        chargerPli();
                    }
                    else
                    {
                        alert("erreur de la base de donnée");
                    }
            },
            error : function(resultat, statut, erreur){
                alert("erreur de la base de donnée.");
            }
        });
    }
}


/****************************** */

function fluxKO(idEtat,str)
{
    var idFlux = $("#id-flux").val();
    $(".text-anomalie").val("");
    $(".spinner-modal").hide();
    $("div.titre-anomalie").addClass("bg-green");
	$(".titre-anomalie").html('Mettre en "'+str+'" le flux');
	etatFlux = parseInt(idEtat);
	/**/


    $.ajax({ /*Verification si Pli en standBY */
        url : s_url+'push/typage/Pli/standBy/'+idFlux,
        type : 'POST',
        dataType : 'json',
        success : function(ret, statut){

            if(parseInt(ret) <= 0)
            {
                $(".btn-annuler").show();
                $("#id-modal-motif-anomalie").modal("show");
            }
            else
            {
                alert("Ce flux #"+idFlux+" est en stand by!");
            }
        },

        error : function(resultat, statut, erreur){},

        complete : function(resultat, statut){}

    });
}


function getConsigneClient(idFlux)
{
    /*<span class="consigne-traitement-flux" id="id-consigne-traitement-flux">*/
    $(".consigne-traitement-flux").hide();
    
    $.ajax({ /*Verification si Pli en standBY */
        url : s_url+'push/typage/Pli/getConsigneClient/'+idFlux,
        type : 'POST',
        dataType : 'text',
        success : function(ret, statut){
            if(ret.trim() != "")
            {
                $(".consigne-traitement-flux").show();
                $("#id-consigne-traitement-flux").text(ret);
            }
        },

        error : function(resultat, statut, erreur){},

        complete : function(resultat, statut){}

    });
}