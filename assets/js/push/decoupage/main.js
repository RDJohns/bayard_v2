var listFileTemp = [];
var tableFichier;
var iNormal             = 0;
var listFileNormal 	    = [];
var idFlux              = 0;
var inFormNormal 		;
var nbFileNormal   	    ;


var iRejet              = 0;
var listFileRejet 	    = [];
var inFormRejet 		;
var nbFileRejet   	    ;

//chargerFichier();


$(function(){
    init_nsprogress();
    fillDataTableFichier();  
});
 
/**********************************/

Dropzone.autoDiscover = false; /* fixe error dropzone : uncaught Error: Dropzone already attached. */
Dropzone.prototype.defaultOptions.dictDefaultMessage = '<span><i class="fa fa-cloud-upload faa-float animated fa-4x" aria-hidden="true"></i><h5><b>Déposer ou cliquer pour ajouter</b></h5></span>';
$("#decoupage").dropzone({
    addRemoveLinks: true,
    maxFilesize: 5000,
    paramName: "file",
    thumbnailWidth: 20,
    thumbnailHeight: 20,
    dictRemoveFile: 'Supprimer',
    timeout:50000000,
    url: s_url+'push/decoupage/Depot/',
    init: function() {

        var myDropzone 	= this;
        var listFile 	= [];
        var isDelete	= 0;

        this.on("sending",function (file,xhr,formData) {
            formData.append("id", $("#decoupage-id-flux").text());
        });
        this.on("addedfile", function(file,response) {
            if(file.size > (1024 * 1024 * 5000))
            {
                this.removeFile(file);
            }
        });
        this.on("error", function(file, message) {
            this.removeFile(this.file.name);
        });
        this.on("removedfile", function(file) {
         if(isDelete == 0)
           {
               $.ajax({
                   url : s_url+'push/decoupage/Depot/delete',
                   type : 'POST',
                   data : 'myfile=' +file.name,
                   dataType : 'text'
               });

               isDelete = 0;


               if (file.name) { // enlever tous les fichier de meme nom and taille
                   var _i, _len, _ref = this.files.slice();
                   for (_i = 0, _len = this.files.length; _i < _len ; _i++)
                   {
                       if (_ref[_i].name === file.name && _ref[_i].size === file.size)
                       {
                           this.removeFile(_ref[_i]);
                       }
                   }
               }
          }


        });
        this.on("success", function(file, response) {
            /*file.name : nom de fichier d'origine*/
            /*response : nom de fichier dans le serveur retourner par php*/

            if(response.match(/error_upload*/))
            {
                var msg = "Erreur sur le fichier : "+file.name+"\n"+response;
                var type_msg = "error";
                alert(msg);
                this.removeFile(file);
                return false;
            }
            else
            {
                listFile[file.name] = response; /*stoker dans un array, utile pour la suprression*/
                listFileTemp = listFile;
            }


            /*console.log("file.name : "+file.name+"response : "+response);*/
        });
        this.on("queuecomplete", function (file) {
            //alert("All files have uploaded ");

            //-  $("#saveModif").show();
            //-   $("#etat_upload").text("");
        });
    }
});

/******REJET KO*** */

$("#decoupage-rejet-ko").dropzone({
    addRemoveLinks: true,
    maxFilesize: 5000,
    paramName: "file",
    thumbnailWidth: 20,
    thumbnailHeight: 20,
    dictRemoveFile: 'Supprimer',
    timeout:50000000,
    url: s_url+'push/decoupage/Rejet',
    init: function() {

        var myDropzone 	= this;
        var listFile 	= [];
        var isDelete	= 0;

        this.on("sending",function (file,xhr,formData) {
            formData.append("id", $("#decoupage-id-flux").text());
        });
        this.on("addedfile", function(file,response) {
            if(file.size > (1024 * 1024 * 5000))
            {
                this.removeFile(file);
            }
        });
        this.on("error", function(file, message) {
            this.removeFile(this.file.name);
        });
        this.on("removedfile", function(file) {

           console.log(file.name);


          if(isDelete == 0)
           {
               $.ajax({
                   url : s_url+'push/decoupage/Rejet/deleteRejet',
                   type : 'POST',
                   data : 'myfile=' +file.name,
                   dataType : 'text'
               });

               isDelete = 0;


               if (file.name) { // enlever tous les fichier de meme nom and taille
                   var _i, _len, _ref = this.files.slice();
                   for (_i = 0, _len = this.files.length; _i < _len ; _i++)
                   {
                       if (_ref[_i].name === file.name && _ref[_i].size === file.size)
                       {
                           this.removeFile(_ref[_i]);
                       }
                   }
               }
          }


        });
        this.on("success", function(file, response) {
            /*file.name : nom de fichier d'origine*/
            /*response : nom de fichier dans le serveur retourner par php*/

            if(response.match(/error_upload*/))
            {
                var msg = "Erreur sur le fichier : "+file.name+"\n"+response;
                var type_msg = "error";
                alert(msg);
                this.removeFile(file);
                return false;
            }
            else
            {
                listFile[file.name] = response; /*stoker dans un array, utile pour la suprression*/
                listFileTemp = listFile;
            }


            /*console.log("file.name : "+file.name+"response : "+response);*/
        });
        this.on("queuecomplete", function (file) {
            //alert("All files have uploaded ");

            //-  $("#saveModif").show();
            //-   $("#etat_upload").text("");
        });
    }
});

function chargerFichier() {


    $.ajax({
        url : s_url+'push/decoupage/Flux_Sftp/getFile',
        type : 'POST',
        dataType : 'json',
        success : function(ret, statut){
            /*
            * * etape = ok (nahazo)
            * etape = vide
            * etape = ko
            * */

        /*
        * /* Exemple : données
        * array(1) { [0]=> object(stdClass)#26 (6) {
        * ["id_flux"]=> string(4) "9739"
        * ["dossier_flux"]=> string(89) "/Vivetic/BAYARD/FICHIERS_STACI_retours_VPC/SUIVI_RETOURS_8508_30092019_30092019-st026.xls"
        * ["societe"]=> string(1) "1"
        * ["nom_societe"]=> string(6) "Bayard"
        * ["decoupe_par"]=> string(3) "454"
        * ["demande_decoupe_par"]=> NULL
        *  } }
        * 
        * var base = s_url+'push/typage/Pli/download/'+flux.id_flux ;
			    strMail += "ID flux : "+flux.id_flux+'<br/>';
                strMail +='Fichier : <a  class="name_file" href="'+base+'" style="cursor:pointer;">'+flux.filename_origin+'</a>';
        * */
      
       
       var base = s_url+'push/saisie/Ajax_push/download_sftp_file/'+ret.id_flux ;
       
      
            var strHtml;
            var etape = ret.etape;
            $(".c-fichier").html("");
            switch (etape)
            {
                case 'ok':
                    strHtml ="ID Flux      : <span id='decoupage-id-flux' >"+ret.id_flux+"<br/></span>";
                    strHtml +='Fichier : <a  class="name_file" href="'+base+'" style="cursor:pointer;color:white;">'+ret.dossier_flux+'</a><br/>';
                    strHtml +="Société     : "+ret.nom_societe+"<br/>";
                    strHtml +="Demande par : "+ret.demande_decoupe_par+"<br/>";
                    $(".c-fichier").html('<div class="alert alert-info">'+strHtml+'</div>');
                    $(".btn-annuler-tmt").show();
                    $(".btn-charger-fichier").hide();
                    $(".c-dropzone-decoupage").show();
                    idFlux = ret.id_flux;
                    break;
                case 'ko':
                        $(".c-fichier").html('<div class="alert alert-danger">Pas de fichier en attente de découpage<br/>Merci de réessayer plus tard</div>');
                        $(".btn-annuler-tmt").hide();
                        $(".c-dropzone-decoupage").hide();
                    break;
                default: /* errueur dans code*/
                    break;
            }

            //console.log(ret);
        },

        error : function(resultat, statut, erreur){
            alert("Erreur lors de la récupération du fichier. Merci de réessayer plus tard.");
        },

        complete : function(resultat, statut){}

    });
}

function annulerTraitement()
{
   
   var idFlux = $("#decoupage-id-flux").text();
   $.confirm({
        title: 'Demande de confirmation',
        content: 'Voulez-vous annuler le traitement ?',
        theme :'Supervan',
        buttons: {
            confirmer:  {
                text:'Oui, annuler',
                btnClass: 'btn bg-pink waves-effect',
                action:function()
                {
                    $.ajax({
                        url : s_url+'push/decoupage/Flux_Sftp/annuler/'+idFlux,
                        type : 'POST',
                        error : function(resultat, statut, erreur){alert("Il y une erreur lors de l'annulation de traitement. Mercide réessayer plus tard");},
                        complete : function(resultat, statut){
                           window.location.reload(true);
                        }
                    });
                }
            },
            fermer: {
                text: 'Non, fermer',
                btnClass:"btn bg-light-blue waves-effect"
            }
        }
    });
    
   
}

function annulerVersTypage() {
    $("#id-modal-annuler-vers-typage").modal("show");
}

function terminerDecoupage()
{
    iNormal             = 0;
    listFileNormal 	    = [];
    idFlux              = $("#decoupage-id-flux").text();
    inFormNormal 		= Dropzone.forElement("#decoupage").files;
    nbFileNormal   	    = inFormNormal.length;
    
    
    iRejet              = 0;
    listFileRejet 	    = [];
    inFormRejet 		= Dropzone.forElement("#decoupage-rejet-ko").files;
    nbFileRejet   	    = inFormRejet.length;

    for (iNormal = 0; iNormal < nbFileNormal; iNormal++)
    {
        listFileNormal[iNormal] = inFormNormal[iNormal].name; /*recupere le filename dans serveur*/
    }

    for (iRejet = 0; iRejet < nbFileRejet; iRejet++)
    {
        listFileRejet[iRejet] = inFormRejet[iRejet].name; /*recupere le filename dans serveur*/
    }

    if(iRejet == 0 && iNormal == 0 )
    {
        alert_("Merci de mettre au moins un fichier.");
        return 0;
    }
    
    
    $.confirm({
        title: 'Demande de confirmation',
        content: 'Voulez-vous continuer le découpage ? ',
        theme :'Supervan',
        buttons: {
            confirmer:  {
                text:'Oui, continuer',
                btnClass: 'btn bg-pink waves-effect',
                action:function()
                {
                    finaliserDecoupage();
                }
            },
            fermer: {
                text: 'Non, fermer',
                btnClass:"btn bg-light-blue waves-effect"
            }
        }
    });
}
function finaliserDecoupage() {
   
   var idFlux = $("#decoupage-id-flux").text();
   var postData = {idFlux:idFlux, fichier:listFileNormal};

   if(iNormal > 0)
   {
        $.ajax({
            url : s_url+'push/decoupage/Depot/deplacementFinal',
            type : 'POST',
            data: postData,
            dataType : 'text',
            success : function(ret, statut)
            {
                if(ret != "OK")
                {
                    alert_("Une erreur s'est produite merci de réessayer.");    
                }
                else
                {
                    if(iRejet == 0 )
                    {   
                       alert_("Découpage sans rejet terminé");
                       window.location.reload(true);
                    }
                }
            },
            error : function(resultat, statut, erreur)
            {
                alert_("Une erreur s'est produite merci de réessayer plus tard.");
            },
            complete : function(resultat, statut){}
        });
   }
   
   if(iRejet > 0 )    
    {
        terminerRejet();
    }
}

function fillDataTableFichier()
{
    $('#table-decoupage').dataTable().fnDestroy();
    
    tableFichier = $('#table-decoupage').DataTable({
        "language": {
            processing:     "Traitement en cours...",
            search:         "Rechercher&nbsp;:",
            lengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur <span class='row-total'> _TOTAL_ </span> &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "fnDrawCallback":function(){
          
            $('.dataTables_scrollBody').slimscroll({size: '10px', alwaysVisible: false,railVisible: true,});
            
        },
        initComplete: function() {
            $("#table-decoupage_filter input").unbind();
            $("#table-decoupage_filter input").bind("keyup", function(e) 
            {
                if(e.keyCode == 13) 
                {
                    tableFichier.search(this.value).draw();
                }
            });
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.select.activate();
            $.AdminBSB.input.activate();
        },
        "paging": true,
        "processing": true, 
        "serverSide": true,
        fixedHeader:true,
        // "scrollX": "100%",
        "scrollY": "300px",
        "scrollCollapse": true,
        /*"searching": false,*/
        //dom: 'flrtip',
        "info": true,
        "lengthMenu": [[10, 20, 100, -1], [10, 40, 100,"tous "]],    
        "ajax": {
        "url": s_url +'push/decoupage/Flux_Sftp/fichierPublipostes',
        "type": "POST"
    }
});
}


function terminerRejet() {

    var listFileRejet 	 = [];
    var inFormRejet 	 = Dropzone.forElement("#decoupage-rejet-ko").files;
    var nbFileRejet   	 = inFormRejet.length;
    var idFlux = $("#decoupage-id-flux").text();
    for (iRejet = 0; iRejet < nbFileRejet; iRejet++)
    {
        listFileRejet[iRejet] = inFormRejet[iRejet].name; 
    }
    var postDataRejet = {idFlux:idFlux, fichierRejet:listFileRejet};
    
    $.ajax({
            url : s_url+'push/decoupage/Rejet/deplacementRejet',
            type : 'POST',
            data: postDataRejet,
            dataType : 'text',
            success : function(ret, statut){
                if(ret != "OK")
                {
                    alert_("Une erreur s'est produite merci de réessayer.");  
                }
                else
                {
                    if(iNormal == 0)
                    {
                        alert_("Découpage  rejet terminé"); 
                    }
                    else
                    {
                        alert_("Découpage avec rejet terminé"); 
                    }
                    
                    window.location.reload(true);
                }
            },
            error : function(resultat, statut, erreur){
                alert_("Une erreur s'est produite merci de réessayer plus tard.");
            },
            complete : function(resultat, statut){}
        });
       
    
}


function init_nsprogress(){
    $( document ).ajaxStart(function() {
        
        $(".preloader-reception").show();
        NProgress.start();
    });
    $(document).ajaxStop(function() {
        NProgress.done();
        
        $(".preloader-reception").hide();
    });
}

function alert_(msg)
{
    $.alert({
        type:"red",
        theme:'material',
        icon: 'glyphicon glyphicon-info-sign',
        title: 'Information',
        content: msg,
        buttons:
        {
            fermer: {
                text: 'Fermer',
                btnClass:"btn bg-light-blue waves-effect"
            }
        }
    });
}