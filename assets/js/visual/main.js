$(function() {
    dttb_visu = null;
    fichier_up_ke = [];
    init_compos();
    //load_table();
});

function activateNotificationAndTasksScroll() {

    var relationClient = $("#relation-client").val();
    var slimHeight = '65vh';
    if(relationClient == 1)
    {
        var slimHeight = '15vh';
    }
    $('.navbar-right .dropdown-menu .body .menu').slimscroll({
        height: slimHeight,
        color: 'rgba(0,0,0,0.5)',
        size: '4px',
        alwaysVisible: false,
        borderRadius: '0',
        railBorderRadius: '0'
    });
}

function init_compos() {
    $('.dt_field').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY',
        lang: 'fr',
        clearButton: true,
        clearText: 'Vider',
        cancelText: 'Annuler',
        weekStart: 1,
        switchOnClick: true,
        time: false
    }).on('change', function(e, date) {
        if ($(this).val() == '') {
            $(this).closest('.form-line').removeClass('focused');
        } else {
            $(this).closest('.form-line').addClass('focused');
        }
    });
    $('.elem_filtre').keypress(function(e){
        if(e.keyCode == 13){
            load_table();
        }
    });
}

function load_table() {
    if(dttb_visu === null){
        dttb_visu = $('#visu_pli_dtt').DataTable({
            'bprocessing': true,
            'bServerSide': true,
            'bScrollCollapse': true,
            dom: 'lrBtip',
            'aLengthMenu': [
                [5, 10, 25, 50, 100],
                [5, 10, 25, 50, 100]
            ],
            //'bStateSave': true,
            'sScrollX': true,
            //'bPaginate': false,
            'pageLength': 5,
            //'bDestroy': true,
            'bFilter': false,
            'oLanguage': {
                'sLengthMenu': '_MENU_ lignes par page',
                'sZeroRecords': "Aucun pli",
                'sEmptyTable': "Aucun pli",
                'sInfo': "de _START_ \340 _END_ sur _TOTAL_ plis",
                'sInfoFiltered': '(Total : _MAX_ plis)',
                'sLoadingRecords': 'Chargement en cours...',
                'sProcessing': 'Traitement en cours...',
                'oPaginate': {
                    'sFirst': 'PREMIERE',
                    'sPrevious': 'PRECEDENTE',
                    'sNext': 'SUIVANTE',
                    'sLast': 'DERNIERE'
                },
                'sSearch': 'Rechercher: '
            },
            'ajax': {
                'url': chemin_site + '/visual/visual/plis',
                'sServerMethod': 'POST',
                'data': function(d) {
                    d.dt_mail_1 = $.trim($("#dt_mail_1").val());
                    d.dt_mail_2 = $.trim($("#dt_mail_2").val());
                    d.dt_act_1 = $.trim($("#dt_act_1").val());
                    d.dt_act_2 = $.trim($("#dt_act_2").val());
                    d.etape = JSON.stringify($("#sel_step").val() === null ? [] : $("#sel_step").val());
                    d.etat = JSON.stringify($("#sel_state").val() === null ? [] : $("#sel_state").val());
                    d.soc = JSON.stringify($("#sel_soc").val() === null ? [] : $("#sel_soc").val());
                    d.typo = JSON.stringify($("#sel_typo").val() === null ? [] : $("#sel_typo").val());
                    d.niveau_saisie = JSON.stringify($("#sel_niveau_saisie").val() === null ? [] : $("#sel_niveau_saisie").val());
                    d.md_paie = JSON.stringify($("#sel_md_paie").val() === null ? [] : $("#sel_md_paie").val());
                    d.find_id_pli = $.trim($("#txt_recherche_id_pli").val());
                    d.find_pli = $.trim($("#txt_recherche_pli").val());
                    d.find_paieur = $.trim($("#txt_recherche_payeur").val());
                    d.find_abonne = $.trim($("#txt_recherche_abonne").val());
                    d.find_cmc7 = $.trim($("#txt_recherche_cmc7").val());
                }
            },
            /*"order": [
                [0, "desc"]
            ],*/
            'fnDrawCallback': function(oSettings) {
                $('[data-toggle="tooltip"]').tooltip();
                //$('[data-toggle="popover"]').popover();
                set_action();
            },
            aoColumnDefs: [{
                'aTargets': [4,5,6,8,14,17],
                "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                    $(nTd).addClass('align-center');
                }
            }
            ,{ 'bSortable': false, 'aTargets': [10,14] },{'aTargets': [10,11,15,16,17,18,19], bVisible: false}],
            'buttons': [{extend: 'colvis', text:'+ colonnes', columns:[10,11,12,13,14,15,16,17,18,19]}],
            'columns': [
                { 'data': 'dtc', 'targets': [0] },
                { 'data': 'soc', 'targets': [1] },
                { 'data': 'idpli', 'targets': [2] },
                { 'data': 'pli', 'targets': [3] },
                { 'data': 'etape', 'targets': [4] },
                { 'data': 'step', 'targets': [5] },
                { 'data': 'stat', 'targets': [6] },
                { 'data': 'typo', 'targets': [7] },
                { 'data': 'mvmts', 'targets': [8] },
                { 'data': 'paies', 'targets': [9] },
                { 'data': 'cmc7s', 'targets': [10] },
                { 'data': 'id_com', 'targets': [11] },
                { 'data': 'consigne_op', 'targets': [12] },
                { 'data': 'consigne_client', 'targets': [13] },
                { 'data': 'comms', 'targets': [14] },
                { 'data': 'lot_ss', 'targets': [15] },
                { 'data': 'lot_sc', 'targets': [16] },
                { 'data': 'dtn', 'targets': [17] },
                { 'data': 'dth', 'targets': [18] },
                { 'data': 'niveau_saisie', 'targets': [19] },
            ]
        });
    }else {
        dttb_visu.ajax.reload();
    }
    window.location.href = '#zone_img_temp';
}

function set_action() {
    $('.ln_pli').each(function() {
        var id_pli = Number.parseInt($(this).attr('my_id_pli'));
        var lot_saisie = $.trim($(this).attr('my_lot_saisie'));
        var nb_mvmnt =  Number.parseInt($(this).attr('my_nb_mvmnt'));
        var bt_action = '';
        // bt_action += '<a href="javascript:comment_pli(\''+id_pli+'\')" class="list-group-item"><i class="material-icons font-13">comment</i> Commentaires</a>';
        bt_action += '<a href="javascript:detail_pli(\''+id_pli+'\')" class="list-group-item"><i class="material-icons font-13">mail_outline</i> D\351tails</a>';
        // bt_action += '<a href="javascript:load_img_docs(\''+id_pli+'\')" class="list-group-item"><i class="material-icons font-13">pageview</i> Documents</a>';
        // bt_action += '<a href="javascript:afficher_histo(\''+id_pli+'\')" class="list-group-item"><i class="material-icons font-13">restore</i> Historique</a>';
        // if(nb_mvmnt > 0) {
        //     bt_action += '<a href="javascript:load_mvmt_all(\''+id_pli+'\')" class="list-group-item"><i class="material-icons font-13">picture_in_picture_alt</i> Mouvements</a>';
        // }
        // if(lot_saisie != '') {
        //     bt_action += '<a href="javascript:afficher_lot_saisie(\''+lot_saisie+'\')" class="list-group-item"><i class="material-icons font-13">perm_media</i> Plis m\352me lot-saisie</a>';
        // }
        $(this).qtip({
            style: { classes: 'qtip-bootstrap qtip-rounded' },
            content: {
                button: true,
                text: '<div class="align-center"><span class="label label-info">Pli#' + id_pli + '</span></div><div class="list-group m-b-5">' + bt_action + '</div>'
            },
            position: {
                viewport: $(window),
                target: 'mouse',
                my: 'center',
                adjust: {
                    mouse: false,
                    resize: true,
                    scroll: true
                }
            },
            hide: {
                fixed: true,
                event: 'unfocus click'
            },
            show: {
                solo: true,
                event: 'click'
            }
        });
    });
}

function load_img_doc(id_doc) {
    $.get(chemin_site + '/visual/visual/get_img_doc/' + id_doc, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#zone_img_temp').html(data.img);
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            $('#img_temp_recto').trigger('click');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function load_img_docs(id_pli) {
    $.get(chemin_site + '/visual/visual/get_img_docs/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#id_pli_docs').html(id_pli);
            $('#typo_pli_docs').html(data.typo);
            $('#link_download_docs_zip').attr('href', $('#link_download_docs_zip').attr('href_init') + id_pli);
            $('#mdl_display_docs_body').html(data.img);
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            $('[data-toggle="tooltip"]').tooltip();
            $('#mdl_display_docs').modal('show');
            //$('#img_temp_recto_'+id_pli+'_0').trigger('click');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function load_mvmt(id_mvmt, id_pli) {
    $.get(chemin_site + '/visual/visual/get_view_mvmt/' + id_pli + '/' + id_mvmt, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#mdl_display_mvmt_title').html(id_pli);
            $('#mdl_display_mvmt_body').html(data.ihm);
            $('#mdl_display_mvmt').modal('show');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function load_mvmt_all(id_pli) {
    $.get(chemin_site + '/visual/visual/get_view_mvmt/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#mdl_display_mvmt_title').html(id_pli);
            $('#mdl_display_mvmt_body').html(data.ihm);
            $('#mdl_display_mvmt').modal('show');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function afficher_pli(id_pli) {
    $.get(chemin_site + '/visual/visual/display_pli/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#mdl_display_pli_title').html(id_pli);
            $('#mdl_display_pli_body').html(data.ihm);
            $('[data-toggle="tooltip"]').tooltip();
            $('#mdl_display_pli').modal('show');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function afficher_histo(id_pli) {
    $.get(chemin_site + '/visual/visual/histo_pli/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#mdl_display_histo_title').html(id_pli);
            $('#mdl_display_histo_body').html(data.ihm);
            $('[data-toggle="tooltip"]').tooltip();
            $('#mdl_display_histo').modal('show');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function afficher_lot_saisie(id_lot) {
    $.get(chemin_site + '/visual/visual/lot_saisie_view/' + id_lot, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#mdl_display_lot_saisie_title').html(id_lot);
            $('#mdl_display_lot_saisie_body').html(data.ihm);
            $('[data-toggle="tooltip"]').tooltip();
            $('#mdl_display_lot_saisie').modal('show');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function edit_consigne_ke(id_pli) {
    $.get(chemin_site + '/visual/visual/input_ke/' + id_pli, {}, function(reponse) {
        $('#mdl_input_ke > div.modal-dialog > div.modal-content').html(reponse);
        init_dropzone_ke();
        autosize($('textarea.auto-growth'));
        $('[data-toggle="tooltip"]').tooltip();
        $('#mdl_input_ke').modal('show');
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur serveur.', 'top', 'center', null, null);
    });
}

function init_dropzone_ke(){
    $("#ke_file_insert").dropzone({
        addRemoveLinks: true
        ,dictRemoveFile: 'Retirer'
        ,init:function(){
            fichier_up_ke = [];
            le_dropzone = this;
            this.on('success', function(file, resp){
                try {
                    var rep = $.parseJSON(resp);
                } catch (error) {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur upload.', 'bottom', 'right', null, null);
                    this.removeFile(file);
                    return false;
                }
                if(rep.code == 1){
                    var fichier = {
                        uid: file.upload.uuid
                        ,link: rep.link
                        ,orig_name: rep.name
                        ,ext: rep.ext
                        ,suppr: false
                    };
                    fichier_up_ke.push(fichier);
                }else{
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur upload: '+rep.er, 'bottom', 'right', null, null);
                    this.removeFile(file);
                    return false;
                }
            });
        }
        ,removedfile:function(file){
            $.each(fichier_up_ke, function(i, fichier){
                if(file.upload.uuid == fichier.uid){
                    fichier.suppr = true;
                }
            });
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        }
    });
}

function send_data_ke(id_pli) {
    var deleted_existed_files = [];
    $.each($('.name_existed_file_ke.deleted'), function(){
        deleted_existed_files.push($(this).attr('my_oid'));
    });
    var data = {
        id_pli: id_pli
        ,files: JSON.stringify(fichier_up_ke)
        ,comms: $.trim($('#comms_ke').val())
        ,deleted_oid: JSON.stringify(deleted_existed_files)
    }
    var verifie = true;
    if($.trim(data.comms) == ''){
        verifie = false;
        swal("Alerte", "Merci d'éditer la consigne.", "warning");
    }
    if(verifie){
        swal({
            title: 'Confirmation',
            text: 'Enregistrer?',
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Valider",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.post(chemin_site + '/visual/visual/set_consigne_ke', data, function(rep) {
                if (rep == 1) {
                    $('#mdl_input_ke').modal('hide');
                    swal({
                        title: "R\351ussi!",
                        text: "Consigne enregistr\351e.",
                        type: "success",
                        showConfirmButton: true
                    });
                } else {
                    swal("Erreur!", rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Erreur", "Erreur serveur!", "error");
            }).always(function() {
                load_table();
            });
        });
    }
}

function del_old_file_ke(id_existed_file_ke, me){
    if($('#'+id_existed_file_ke).hasClass('deleted')){
        $('#'+id_existed_file_ke).removeClass('deleted');
        $(me).html('delete');
        $(me).attr('title', 'Supprimer');
    }else{
        $('#'+id_existed_file_ke).addClass('deleted');
        $(me).html('delete_forever');
        $(me).attr('title', 'Restaurer');
    }
}

function affiche_commentaire(id_pli) {
    var data = {id_pli:id_pli};
    $.post(chemin_site + '/visual/visual/comments_pli', data, function(reponse) {
        $('#mdl_disp_comment_pli_id_pli').html(id_pli);
        $('#mdl_disp_comment_pli_body').html(reponse);
        $('#mdl_disp_comment_pli').modal('show');
    });
}

function comment_pli(id_pli) {
    var data = {id_pli:id_pli};
    $.post(chemin_site + '/visual/visual/comments_pli', data, function(reponse) {
        $('#mdl_comment_pli_id_pli').html(id_pli);
        $('#mdl_comment_pli_body').html(reponse);
        $('#mdl_comment_pli').modal('show');
    });
}

function add_comment_pli() {
    var data = {
        id_pli: Number.parseInt($.trim($('#mdl_comment_pli_id_pli').html()))
        ,comment:$.trim($('#new_comment_pli').val())
    };
    if(Number.isInteger(data.id_pli)) {
        $.post(chemin_site + '/visual/visual/comments_pli', data, function(reponse) {
            $('#new_comment_pli').val('');
            autosize.update($('#new_comment_pli'));
            $('#mdl_comment_pli_body').html(reponse);
        });
    }
}

$(document).on("keypress", function(e){
    if(e.which == 13){
        load_table();
    }
});

//code: display more pli informations on modale (need plugins: jdPanel)
function detail_pli(id_pli) {
    $.get(chemin_site + '/visual/visual/detail_pli/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            jsPanel.create({
                headerTitle: data.titre
                ,headerToolbar: data.sous_titre
                ,footerToolbar: data.footer
                ,content: data.body
                ,contentSize: {
                    width: '80%'
                    ,height: '80vh'
                }
                ,theme: 'light'
                ,onbeforemaximize: function(panel) {
                    $('html,body').scrollTop(0);
                    return true;
                }
            });
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        // swal("Serveur", "Erreur serveur!", "error");
    });
}

function load_docs_offset(id_pli, offset, id_dom){
    $.get(chemin_site + '/visual/visual/view_docs_offset/' + id_pli+'/'+offset+'/'+id_dom, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {console.log(id_dom);
            $('#'+id_dom).html(data.ihm);
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function load_img_doc_face(id_doc, face=0) {
    $.get(chemin_site + '/visual/visual/get_img_doc/' + id_doc, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#zone_img_temp').html(data.img);
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            if(face == 0) {
                $('#img_temp_recto').trigger('click');
            }else{
                $('#img_temp_verso').trigger('click');
            }
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}
//end code: display more pli informations on modale
