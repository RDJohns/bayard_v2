$(function() {
    init_compos();
});

//initialisation datepicker
function init_compos() {
    $('.nsaisi-date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        language: 'fr'
    });
}


function show_nosaisie(){
    var choix = $("#choix-societe").val();

    var nsaisi_date_debut = $("#nsaisi_date_debut").val();
    var nsaisi_date_fin = $("#nsaisi_date_fin").val();

    if(getDate(nsaisi_date_debut) > getDate(nsaisi_date_fin))
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de rectifier votre filtre date '},{type: 'danger'});
        $("#nsaisi_date_debut").focus();
        return false;
    }


    if(choix == ''){
        swal("Choix société", "Veuillez choisir une société", "warning");
        return false;
    }

    var data = {
        societe : choix,
        nsaisi_date_debut : nsaisi_date_debut,
        nsaisi_date_fin : nsaisi_date_fin
    };

    $('.card').css({'opacity':1});
    $( "#preloader_nsaisi" ).removeClass("nsaisi_preload");

    var  date  = new Date();
    var aujourdhui =  ( date.getDate() + '/' +  (date.getMonth() + 1)+ '/' +  date.getFullYear());

    $('#table-nosaisie').DataTable({
        dom: 'lBfrtip',
        buttons: [
           /* {
                extend : 'excelHtml5',
                title: 'chèques_non_saisis_'+aujourdhui
            }*/
            {
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": s_url+'/suivi/nosaisie/export_nsaisi_excel',
                        "type": "POST",
                        "data": data,
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'chèques_non_saisis_'+aujourdhui+'.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }
        ]
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100],[5,10,25,50,100]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": false
        ,"oLanguage": {
            "sLengthMenu": "Afficher _MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "Affichage de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
            ,"sProcessing":""
        }
        ,"ajax":{
            //'dataType': 'json',
            'url': s_url+'suivi/nosaisie/get_nosaisie'
            ,"data": data
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //alert('test');
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },
        "initComplete": function(settings, json) {
            $( "#preloader_nsaisi" ).addClass("nsaisi_preload");
          }
        ,'columns':[
            { "data": 'date_courrier'}
            ,{ "data": 'id_pli'}
            ,{ "data": 'pli'}
            ,{ "data": 'lot_scan'}
            ,{ "data": 'etape'}
            ,{ "data": 'etat'}
            ,{ "data": 'statut_pli'}
            ,{ "data": 'cmc7'}
            ,{ "data": 'montant'}
            ,{ "data": 'etat_chq'}
            ,{ "data": 'commande'}
        ]
    });
}