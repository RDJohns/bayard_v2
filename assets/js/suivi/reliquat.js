$(function() {
    init_compos();
});

//initialisation datepicker
function init_compos() {
    $('.nreb-date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        language: 'fr'
    });
}


function show_reliquat(){
   
    var choix = $("#choix-societe").val();
    var statut = $("#choix-statut").val();
    var nreb_date_debut = $("#nreb_date_debut").val();
    var nreb_date_fin = $("#nreb_date_fin").val();

    if(getDate(nreb_date_debut) > getDate(nreb_date_fin))
    {
        $.notify({title: '<strong>information !</strong>',message: ' Merci de rectifier votre filtre date '},{type: 'danger'});
        $("#nreb_date_debut").focus();
        return false;
    }


    if(choix == ''){
        swal("Choix société", "Veuillez choisir une société", "warning");
        return false;
    }

    /*if(statut == null){
        swal("Choix statut", "Veuillez choisir un statut", "warning");
        return false;
    }*/

    var data = {
        societe : choix,
        statut : statut,
        nreb_date_debut : nreb_date_debut,
        nreb_date_fin : nreb_date_fin
    };

    $('.card').css({'opacity':1});

    $( "#preloader_nreb" ).removeClass("nreb_preload");

        var  date  = new Date();
        var aujourdhui =  ( date.getDate() + '/' +  (date.getMonth() + 1)+ '/' +  date.getFullYear());

    $('#table-reliquat').DataTable({
        dom: 'lBfrtip',
        buttons: [
          /*  {
                extend : 'excelHtml5',
                title: 'chèques_non_reb_'+aujourdhui
            }*/

            {
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": s_url+'/suivi/reliquat/export_nreb_excel',
                        "type": "POST",
                        "data": data,
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'chèques_non_reb_'+aujourdhui+'.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }
        ]
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100],[5,10,25,50,100]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,"oLanguage": {
            "sLengthMenu": "Afficher _MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "Affichage de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
            ,"sProcessing":""
        }
        ,"ajax":{
            //'dataType': 'json',
            'url': s_url+'suivi/reliquat/get_reliquat'
            ,"data": data
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //alert('test');
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },
        "initComplete": function(settings, json) {
            $( "#preloader_nreb" ).addClass("nreb_preload");
          }
        ,'columns':[
            { "data": 'date_courrier' }
            ,{ "data": 'id_pli'}
            ,{ "data": 'pli'}
            ,{ "data": 'lot_scan'}
            ,{ "data": 'etape'}
            ,{ "data": 'etat'}
            ,{ "data": 'statut_pli'}
            ,{ "data": 'cmc7'}
            ,{ "data": 'montant'}
            ,{ "data": 'etat_chq'}
            ,{ "data": 'commande'}
        ]
    });
}

function get_last_date(){
    var date_deb = $("#nreb_date_debut").val();
    $.post(s_url+'suivi/reliquat/get_last_date',{date_debut : date_deb},function (data) {
        if(data != ''){
            $("#nreb_date_fin").datepicker("setDate", data);
        }
    });
}

