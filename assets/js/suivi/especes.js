$(function() {
    init_compos();
});
    

//initialisation datepicker
function init_compos() {
    $('.dt_field').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY',
        lang: 'fr',
        clearButton: true,
        clearText: 'Vider',
        cancelText: 'Annuler',
        weekStart: 1,
        switchOnClick: true,
        time: false
    }).on('change', function(e, date) {
        if ($(this).val() == '') {
            $(this).closest('.form-line').removeClass('focused');
        } else {
            $(this).closest('.form-line').addClass('focused');
        }
    });
}


function load_especes()
{
    if($('#dt_act_1').val() == '' || $('#dt_act_2').val() == '') {
        showNotification('alert-danger', '<i class="material-icons">error</i>Merci de renseigner les champs dates.', 'bottom', 'right', null, null);
        return false;
    }
    $('#suivi_especes').DataTable({
        'dom':'Bfrtip',
        'bprocessing': true,
        'bServerSide': true,
        'bScrollCollapse': true,
        'aLengthMenu': [
            [31,62],
            [31,62]
        ],
        'pageLength': 10,
        'bDestroy': true,
        //'bStateSave': true,
        'bFilter': false,
        'sScrollX': true,
        'oLanguage': {
            'sLengthMenu': '_MENU_ lignes par page',
            'sZeroRecords': "Aucun flux",
            'sEmptyTable': "Aucun flux",
            'sInfo': "de _START_ \340 _END_ sur _TOTAL_ flux",
            'sInfoFiltered': '(Total : _MAX_ plis)',
            'sLoadingRecords': 'Chargement en cours...',
            'sProcessing': 'Traitement en cours...',
            'oPaginate': {
                'sFirst': 'PREMIERE',
                'sPrevious': 'PRECEDENTE',
                'sNext': 'SUIVANTE',
                'sLast': 'DERNIERE'
            },
            'sSearch': 'Rechercher: '
        },
        'ajax': {
            'url': chemin_site + '/suivi/especes/getEspeces',
            'sServerMethod': 'POST',
            'data': function(d) {
                d.societe = $("#sel_societe").val();
                d.date_ttr1 = $("#dt_act_1").val();
                d.date_ttr2 = $("#dt_act_2").val();
                d.etape = $("#sel_etape").val();
                d.etat = $("#sel_etat").val();
            }
        },
        'fnDrawCallback': function(oSettings) {
            $('[data-toggle="tooltip"]').tooltip();
            set_action();
        },
        aoColumnDefs: [{ 'bSortable': false, 'aTargets': [0] },
    
        {
                'aTargets': [0,1,2,3],
                "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                    $(nTd).addClass('align-center');
                }
        }
        ],
        buttons: [
            {
                extend: 'excel',
                text: 'Exporter vers excel',
                title :'Suivi_especes_'+$("#dt_act_1").val()+'_'+$('#dt_act_2').val()
            }
        ]
        
        // aoColumnDefs: [{
        //     'aTargets': [3, 4, 5, 12, 13],
        //     "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
        //         $(nTd).addClass('align-center');
        //     }
        // }, { 'bSortable': false, 'aTargets': [12, 13] }],
        // 'columns': [
        //     { 'data': 'id_flux', 'targets': [0] },
        //     { 'data': 'source', 'targets': [1] },
            
        // ]
    });
    window.location.href = '#search_especes';
}