function load_table_ctrl() {
    $('#dmt_ctrl_dtt').DataTable({
        'bprocessing': true,
        'bServerSide': true,
        'bScrollCollapse': false,
        'aLengthMenu': [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        'pageLength': 10,
        'bDestroy': true,
        'bFilter': false,
        //'sScrollY': true,
        //'sScrollX': true,
        'oLanguage': {
            'sLengthMenu': '_MENU_ lignes par page',
            'sZeroRecords': "Aucun pli",
            'sEmptyTable': "Aucun pli",
            'sInfo': "de _START_ \340 _END_ sur _TOTAL_ plis",
            'sInfoFiltered': '(Total : _MAX_ plis)',
            'sLoadingRecords': 'Chargement en cours...',
            'sProcessing': 'Traitement en cours...',
            'oPaginate': {
                'sFirst': 'PREMIERE',
                'sPrevious': 'PRECEDENTE',
                'sNext': 'SUIVANTE',
                'sLast': 'DERNIERE'
            },
            'sSearch': 'Rechercher: '
        },
        'ajax': {
            'url': chemin_site + '/admin/dmt/data_ctrl',
            'sServerMethod': 'POST',
            'data': function(d) {
                var df = data_filtre();
                d.dt_act_1 = df.dt_act_1;
                d.dt_act_2 = df.dt_act_2;
                d.socs = df.socs;
                d.srcs = df.srcs;
                d.typos = df.typos;
                //d.paies = df.paies;
                d.status = df.status;
                d.ops = df.ops;
                d.id_ged = df.id_ged;
                d.id_cmd = df.id_cmd;
            }
        },
        /*"order": [
            [0, "desc"]
        ],*/
        'fnDrawCallback': function(oSettings) {
            $('[data-toggle="tooltip"]').tooltip();
        },
        aoColumnDefs: [{
            'aTargets': [4, 6, 7, 8],
            "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).addClass('align-center');
            }
        }/*, { 'bSortable': false, 'aTargets': [5] }*/],
        'columns': [
            { 'data': 'id_ged', 'targets': [0] },
            { 'data': 'src', 'targets': [1] },
            { 'data': 'soc', 'targets': [2] },
            { 'data': 'typo', 'targets': [3] },
            { 'data': 'nbm', 'targets': [4] },
            //{ 'data': 'paie', 'targets': [5] },
            { 'data': 'etat', 'targets': [5] },
            { 'data': 'dt1', 'targets': [6] },
            { 'data': 'dt2', 'targets': [7] },
            { 'data': 'stemp', 'targets': [8] },
            { 'data': 'par', 'targets': [9] },
            { 'data': 'cmd', 'targets': [10] }
        ]
    });
}
