$(function() {
    init_compos();
});

function init_compos() {
    $('.dt_field').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY',
        lang: 'fr',
        clearButton: true,
        clearText: 'Vider',
        cancelText: 'Annuler',
        weekStart: 1,
        switchOnClick: true,
        time: false
    }).on('change', function(e, date) {
        if ($(this).val() == '') {
            $(this).closest('.form-line').removeClass('focused');
        } else {
            $(this).closest('.form-line').addClass('focused');
        }
    });
}

function fission_val_srcs(id_sel) {
    var raw_val = sel_val_to_array(id_sel);
    var vals = {};
    raw_val.forEach(function(val) {
        var tb_val = val.split('_');
        if(vals['f_'+tb_val[1]] == undefined){
            vals['f_'+tb_val[1]] = [];
        }
        vals['f_'+tb_val[1]].push(tb_val[0]);
    });
    return vals;
}

function sel_val_to_array(id_sel) {
    return $('#'+id_sel).val() === null ? [] : $('#'+id_sel).val();
}

function data_filtre() {
    var df = {
        dt_act_1: $.trim($("#dt_act_1").val())
        ,dt_act_2: $.trim($("#dt_act_2").val())
        ,socs: JSON.stringify(sel_val_to_array('sel_soc'))
        ,srcs: JSON.stringify(sel_val_to_array('sel_src'))
        ,typos: JSON.stringify(fission_val_srcs('sel_typo'))
        //,paies: JSON.stringify(sel_val_to_array('sel_md_p'))
        ,status: JSON.stringify(fission_val_srcs('sel_state'))
        ,ops: JSON.stringify(sel_val_to_array('sel_op'))
        ,id_ged: $.trim($("#txt_recherche_id_pli").val())
        ,id_cmd: $.trim($("#txt_recherche_cmd").val())
    };
    // console.log(df);
    return df;
}

function load_data() {
    load_table_typage();
    load_table_saisie();
    load_table_ctrl();
}

function export_xl(source) {
    $.post(chemin_site + '/admin/dmt/'+source, data_filtre(), function (from_serv) {
        try {
            var reponse = $.parseJSON(from_serv);
        } catch (err) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
            return '';
        }
        window.location.href = chemin_site + '/admin/dmt/get_xl/'+reponse.name+'/'+reponse.token;
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        swal("Serveur", "Erreur serveur!", "error");
    });
}
