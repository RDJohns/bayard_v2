$(function() {
    dttb_support = null;
    $('#mdl_display_group_pli').on('shown.bs.modal', function() {
        load_list_grp_pli();
    });
    $('#mdl_display_group_pli').on('hidden.bs.modal', function() {
        $('#mdl_display_group_pli_body').html($('#html_loader').html());
    });
    $('#mdl_display_group_pli_body').html($('#html_loader').html());
    $('#mdl_display_group_pli_by_older').on('shown.bs.modal', function() {
        load_list_grp_pli_by_older();
    });
    $('#mdl_display_group_pli_by_older').on('hidden.bs.modal', function() {
        $('#mdl_display_group_pli_by_older_body').html($('#html_loader').html());
    });
    $('#mdl_display_group_pli_by_older_body').html($('#html_loader').html());
    //load_table();
    $('#mdl_ko_scan').on('shown.bs.modal', function() {
        $('#sel_motif_ko_scan').selectpicker('val', '1');
        $('#comment_ko_scan').val('');
    });
    $('.visible_menu').show();
    $('.elem_filtre').keypress(function(e){
        if(e.keyCode == 13){
            load_table();
        }
    });
});

function load_table() {
    if(dttb_support === null){
        dttb_support = $('#support_dtt').DataTable({
            'bprocessing': true,
            'bServerSide': true,
            'bScrollCollapse': true,
            dom: 'lrBtip',
            'aLengthMenu': [
                [5, 10, 25, 50, 100],
                [5, 10, 25, 50, 100]
            ],
            'pageLength': 5,
            //'bDestroy': true,
            'bFilter': false,
            'sScrollX': true,
            'oLanguage': {
                'sLengthMenu': '_MENU_ lignes par page',
                'sZeroRecords': "Aucun pli",
                'sEmptyTable': "Aucun pli",
                'sInfo': "de _START_ \340 _END_ sur _TOTAL_ plis",
                'sInfoFiltered': '(Total : _MAX_ plis)',
                'sLoadingRecords': 'Chargement en cours...',
                'sProcessing': 'Traitement en cours...',
                'oPaginate': {
                    'sFirst': 'PREMIERE',
                    'sPrevious': 'PRECEDENTE',
                    'sNext': 'SUIVANTE',
                    'sLast': 'DERNIERE'
                },
                'sSearch': 'Rechercher: '
            },
            'ajax': {
                'url': chemin_site + '/admin/support/plis',
                'sServerMethod': 'POST',
                'data': function(d) {
                    d.etat = JSON.stringify($("#sel_stat").val() === null ? [] : $("#sel_stat").val());
                    d.status = JSON.stringify($("#sel_status").val() === null ? [] : $("#sel_status").val());
                    d.ty_par = JSON.stringify($("#sel_type_par").val() === null ? [] : $("#sel_type_par").val());
                    d.ss_par = JSON.stringify($("#sel_saisie_par").val() === null ? [] : $("#sel_saisie_par").val());
                    d.ct_par = JSON.stringify($("#sel_control_par").val() === null ? [] : $("#sel_control_par").val());
                    d.rt_par = JSON.stringify($("#sel_retrait_par").val() === null ? [] : $("#sel_retrait_par").val());
                    d.soc = JSON.stringify($("#sel_soc").val() === null ? [] : $("#sel_soc").val());
                    d.typo = JSON.stringify($("#sel_typo").val() === null ? [] : $("#sel_typo").val());
                    d.niveau_saisie = JSON.stringify($("#sel_niveau_saisie").val() === null ? [] : $("#sel_niveau_saisie").val());
                    d.md_p = JSON.stringify($("#sel_md_p").val() === null ? [] : $("#sel_md_p").val());
                    d.find_idpli = $.trim($("#txt_recherche_id_pli").val());
                    d.find_cmc7 = $.trim($("#txt_recherche_cmc7").val());
                    d.find_lotn = $.trim($("#txt_recherche_lotnum").val());
                    d.find_lots = $.trim($("#txt_recherche_advent").val());
                }
            },
            /*"order": [
                [0, "desc"]
            ],*/
            'fnDrawCallback': function(oSettings) {
                $('[data-toggle="tooltip"]').tooltip();
                set_action();
            },
            aoColumnDefs: [{
                'aTargets': [3, 7, 14, 15],
                "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                    $(nTd).addClass('align-center');
                }
            }, { 'bSortable': false, 'aTargets': [14, 15] },{'aTargets': [2,8,10,11,12,13,15,16,18], bVisible: false}],
            'buttons': [{extend: 'colvis', text:'+ colonnes', columns:[2,8,10,11,12,13,15,16,18]}],
            'columns': [
                { 'data': 'id_pli', 'targets': [0] },
                { 'data': 'pli', 'targets': [1] },
                { 'data': 'lot_num', 'targets': [2] },
                { 'data': 'soc', 'targets': [3] },
                { 'data': 'etape', 'targets': [4] },
                { 'data': 'etat', 'targets': [5] },
                { 'data': 'status', 'targets': [6] },
                { 'data': 'rec', 'targets': [7] },
                { 'data': 'ty_par', 'targets': [8] },
                { 'data': 'type', 'targets': [9] },
                { 'data': 'lot_ss', 'targets': [10] },
                { 'data': 'ss_par', 'targets': [11] },
                { 'data': 'ct_par', 'targets': [12] },
                { 'data': 'rt_par', 'targets': [13] },
                { 'data': 'paie', 'targets': [14] },
                { 'data': 'cmc7s', 'targets': [15] },
                { 'data': 'ci', 'targets': [16] },
                { 'data': 'id_com', 'targets': [17] },
                { 'data': 'niveau_saisie', 'targets': [18] },
            ]
        });
    }else{
        dttb_support.ajax.reload();
    }
    window.location.href = '#zone_img_temp';
}

function set_action() {
    $('.ln_pli').each(function() {
        var id_pli = $(this).attr('my_id_pli');
        var flg_tt = $(this).attr('my_flg');
        var status = $(this).attr('my_stat');
        var rec = $(this).attr('my_rec');
        var is_in_decoup = $(this).attr('my_is_in_decoup') == 1;
        var is_with_f_ci = $(this).attr('with_f_ci') == 1;
        var is_with_pj = $(this).attr('my_nb_pj') > 0;
        var nb_motif_cons = 1 * $(this).attr('my_nb_motif_cons');
        var bt_action = '';
        bt_action += '<a href="javascript:detail_pli(\'' + id_pli + '\')" class="list-group-item"><i class="material-icons font-14">pageview</i> D&eacute;tail</a>';
        bt_action += '<a href="javascript:prio(\'' + id_pli + '\', \'' + rec + '\')" class="list-group-item"><i class="material-icons font-13">' + (rec == 1 ? 'low_priority' : 'priority_high') + '</i> ' + (rec == 1 ? 'D\351prioriser' : 'Prioriser') + '</a>';
        bt_action += '<a href="javascript:stand_by(\'' + id_pli + '\', \'' + rec + '\')" class="list-group-item"><i class="material-icons font-13">' + (rec != -1 ? 'pan_tool' : 'play_arrow') + '</i> ' + (rec != -1 ? 'Mettre en stand-by' : 'Continuer traitement') + '</a>';
        bt_action += '<a href="javascript:app_assignation_pli.affiche_modal(\'' + id_pli + '\')" class="list-group-item"><i class="material-icons font-13">contacts</i> Assignation ...</a>';
        // bt_action += '<a href="javascript:afficher_histo(\'' + id_pli + '\')" class="list-group-item"><i class="material-icons font-14">restore</i> Historique</a>';
        // if(is_with_f_ci){
        //     bt_action += '<a href="'+chemin_site+'/control/ajax/download_circulaire/'+id_pli+'" download="" class="list-group-item"><i class="material-icons font-13">file_download</i> T&eacute;l&eacute;charger circulaire</a>';
        // }
        // if(is_with_pj){
        //     bt_action += '<a href="'+chemin_site+'/admin/support/download_pjs/'+id_pli+'" download="" class="list-group-item"><i class="material-icons font-13">attachment</i> T&eacute;l&eacute;charger image(s) &agrave; d&eacute;sarchiver</a>';
        // }
        // if(nb_motif_cons > 0){
        //     bt_action += '<a href="javascript:affiche_motif_consigne(\'' + id_pli + '\')" class="list-group-item"><i class="material-icons font-13">assignment_late</i> '+nb_motif_cons+' motif-consignes</a>';
        // }else{
        //     bt_action += '<a href="javascript:void()" class="list-group-item" style="cursor:not-allowed;"><i class="material-icons font-13">assignment_late</i> 0 motif-consignes</a>';
        // }
        if ($.inArray(flg_tt, ['1', '4', '8'/*, '12'*/, '13']) > -1 &&  status == 1) {
            bt_action += '<a href="javascript:unlock(\'' + id_pli + '\', \'' + flg_tt + '\')" class="list-group-item"><i class="material-icons font-13">lock_open</i> D&eacute;verrouiller</a>';
        }
        if ( $.inArray(flg_tt, ['0', '2', '3'/*, '16'*/]) > -1 && status == 1 ) {
            bt_action += '<a href="javascript:diviser(\'' + id_pli + '\', \'' + flg_tt + '\')" class="list-group-item"><i class="material-icons font-13">content_cut</i> Diviser</a>';
        }
        if(status == 1) {
            if ( $.inArray(flg_tt, ['0', '1', '2', '3', '4'/*, '16', '21'*/]) > -1 ) {
                bt_action += '<a href="javascript:reinit(\'' + id_pli + '\', \'' + flg_tt + '\')" class="list-group-item"><i class="material-icons font-13">restore</i> Reinitialiser</a>';
            }else{
                bt_action += '<a href="javascript:reinit_force(\'' + id_pli + '\')" class="list-group-item"><i class="material-icons font-13">restore</i> Reinitialiser</a>';
            }
        }else{
            bt_action += '<a href="javascript:swal(\'Support\', \'L interface #Traitement des KO# est dediée à la manipulation des plis qui ne sont pas en état:OK! (pas ici)\', \'warning\');" class="list-group-item"><i class="material-icons font-13">restore</i> Reinitialiser</a>';
        }
        if ( $.inArray(flg_tt, ['0', '2', '3', '4'/*, '16', '21'*/]) > -1 &&  status == 1 ) {
            bt_action += '<a href="javascript:abandon(\'' + id_pli + '\', \'' + flg_tt + '\')" class="list-group-item"><i class="material-icons font-13">pan_tool</i> Abandonner <small>(Fermer)</small></a>';
        }
        if ( $.inArray(flg_tt, ['0', '2', '3'/*, '21'*/]) > -1 &&  status == 1) {
            bt_action += '<a href="javascript:ko_scan(\'' + id_pli + '\', \'' + flg_tt + '\')" class="list-group-item"><i class="material-icons font-13">print</i> mettre en KO-scan</a>';
        }
        if ( $.inArray(flg_tt, ['0', '2', '3', '16']) > -1 ) {
            // bt_action += '<a href="javascript:hors_perim(\'' + id_pli + '\', \'' + flg_tt + '\')" class="list-group-item"><i class="material-icons font-13">remove_circle</i> mettre hors p&eacute;rim&egrave;tre</a>';
        }
        if (flg_tt == 18) {
            //bt_action += '<a href="javascript:reprise(\'' + id_pli + '\', \'' + flg_tt + '\')" class="list-group-item"><i class="material-icons font-13">input</i> Reprendre le traitement</a>';
        }
        $(this).qtip({
            style: { classes: 'qtip-bootstrap qtip-rounded' },
            content: {
                button: true,
                text: '<div class="align-center"><span class="label label-info">Pli#' + id_pli + '</span></div><div class="list-group m-b-5">' + bt_action + '</div>'
            },
            position: {
                viewport: $(window),
                target: 'mouse',
                my: 'center',
                adjust: {
                    mouse: false,
                    resize: true,
                    scroll: true
                }
            },
            hide: {
                fixed: true,
                event: 'unfocus click'
            },
            show: {
                solo: true,
                event: 'click'
            }
        });
    });
}

function prio(id_pli, rec) {
    swal({
        title: (rec == 1 ? 'D\351prioriser' : 'Prioriser') + ' ce pli?',
        text: 'Pli#' + id_pli,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + '/admin/support/prio', { id_pli: id_pli, rec: rec }, function(rep) {
            if (rep == 1) {
                swal({
                    title: "R\351ussi!",
                    text: "Priorit\351 du pli#" + id_pli + " modifi\351e",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Enregistrement &eacute;chou&eacute;!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

function stand_by(id_pli, rec) {
    swal({
        title: (rec != -1 ? 'Mettre en stand-by' : 'Continuer traitement de') + ' ce pli?',
        text: 'Pli#' + id_pli,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + '/admin/support/set_stand', { id_pli: id_pli, rec: rec }, function(rep) {
            if (rep == 1) {
                swal({
                    title: "R\351ussi!",
                    text: "Op\351ration r\351ussie.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Enregistrement &eacute;chou&eacute;!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

function reinit(id_pli,flg_tt) {
    swal({
        title: 'R\351initialiser ce pli?',
        text: 'Pli#' + id_pli,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/admin/support/reinit/' + id_pli+'/'+flg_tt, {}, function(rep) {
            if (rep == 1) {
                swal({
                    title: "R\351ussi!",
                    text: "Pli#" + id_pli + " r\351initialis\351",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Réinitialisation &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

function reinit_force(id_pli) {
    swal({
        title: 'R\351initialisation!!',
        text: 'Le Pli#' + id_pli + ' est d\351j\340 \340 un stade tr\350s avanc\351 en traitement, vous \352tes sure de vouloir r\351initialiser?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/admin/support/reinit_with_force/' + id_pli, {}, function(rep) {
            if (rep == 0) {
                swal({
                    title: "R\351ussi!",
                    text: "Pli#" + id_pli + " r\351initialis\351",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            }else if(rep == 2){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Réinitialisation &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Erreur!", "Impossible de r\351initialiser ce pli car ses ch\350ques ont d\351j\340 \351t\351 remis en banque.", "error");
            }else if(rep == 3){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Réinitialisation &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Erreur!", "Impossible de r\351initialiser ce pli car il a \351t\351 divis\351.", "error");
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Réinitialisation &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Erreur!", "Impossible de r\351initialiser ce pli, l'op\351ration s'est \351chou\351e", "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Erreur!", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

function diviser(id_pli, flg_tr) {
    $.get(chemin_site + '/admin/support/ui_division/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#mdl_diviser_pli_title').html(id_pli);
            $('#mdl_diviser_pli_body').html(data.ui);
            $('#mdl_diviser_pli_bt').attr('onclick', 'valider_division(\'' + id_pli + '\',\'' + flg_tr + '\');');
            $('.sel_div_pli').selectpicker();
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            $('#mdl_diviser_pli').modal('show');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function generer_fils_pli() {
    var nb_div = $('#sel_div_pli').val();
    var options = '';
    var selected = 'selected';
    var index = 1;
    for (index = 1; index <= nb_div; index++) {
        options += '<option value="' + index + '" ' + selected + ' >Pli ' + index + '</option>';
        selected = '';
    }
    $('.sel_div_doc').selectpicker('destroy');
    $('.sel_div_doc').html(options);
    $('.sel_div_doc').selectpicker();

}

function load_img_doc(id_doc) {
    $.get(chemin_site + '/admin/support/get_img_doc/' + id_doc, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#zone_img_temp').html(data.img);
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            $('#img_temp_recto').trigger('click');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function valider_division(id_pli, flg_tr) {
    var nb_div = $('#sel_div_pli').val();
    var repartition_doc = [];
    var plis = [];
    $('select.sel_div_doc').each(function() {
        var pli = $(this).val();
        var doc = {
            id_doc: $(this).attr('my_id_doc'),
            pli: pli
        };
        repartition_doc.push(doc);
        if ($.inArray(pli, plis) == -1) {
            plis.push(pli);
        }
    });
    if (plis.length == nb_div) {
        swal({
            title: 'Confirmation',
            text: 'Diviser le pli#' + id_pli + ' en ' + nb_div + ' plis?',
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Diviser",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            var data = {
                id_pli: id_pli,
                docs: JSON.stringify(repartition_doc),
                flg_tr: flg_tr
            };
            $.get(chemin_site + '/admin/support/diviser', data, function(rep) {
                if (rep == 1) {
                    $('#mdl_diviser_pli').modal('hide');
                    swal({
                        title: "R\351ussi!",
                        text: "Pli divis\351.",
                        type: "success",
                        showConfirmButton: true
                    });
                } else {
                    swal("Erreur!", rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            }).always(function() {
                load_table();
            });
        });
    } else {
        swal("Pli vide d\351tect\351!", "Tous les plis doivent avoir au moins un document.", "warning");
    }
}

function abandon(id_pli, flg_tt) {
    swal({
        title: 'Abandonner ce pli?',
        text: 'Pli#' + id_pli,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/admin/support/abandon/' + id_pli+'/'+flg_tt, {}, function(rep) {
            if (rep == 1) {
                swal({
                    title: "R\351ussi!",
                    text: "Pli#" + id_pli + " Abandonn\351",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Modification &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

function ko_scan(id_pli, flg_tr) {
    $('#mdl_ko_scan_id_pli').html(id_pli);
    $('#mdl_ko_scan_bt').attr('onclick', 'valider_ko_scan(\'' + id_pli + '\',\'' + flg_tr + '\');');
    $('#mdl_ko_scan').modal('show');
}

function valider_ko_scan(id_pli, flg_tr) {
    var motif = $('#sel_motif_ko_scan').val();
    var comment = $('#comment_ko_scan').val();
    var valide = true;
    if(motif == '14' && $.trim(comment) == ''){
        showNotification('alert-warning', '<i class="material-icons">error</i>Merci de pr&eacute;ciser le motif!.', 'top', 'right', null, null);
        valide = false;
    }
    if(valide){
        var data = {
            id_pli: id_pli
            ,motif: motif
            ,comment: comment
            ,flg_tt_now: flg_tr
        };
        swal({
            title: 'Mettre en KO-scan ce pli?',
            text: 'Pli#' + id_pli,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Valider",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.get(chemin_site + '/admin/support/mise_en_ko_scan', data, function(rep) {
                if (rep == 1) {
                    $('#mdl_ko_scan').modal('hide');
                    swal({
                        title: "R\351ussi!",
                        text: "Pli#" + id_pli + " en KO-scan",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: true
                    });
                } else {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Mise en KO-scan &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                    swal("Serveur", "Erreur! " + rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            }).always(function() {
                load_table();
            });
        });
    }
}

function hors_perim(id_pli,flg_tt) {
    swal({
        title: 'Mettre en hors p\351rim\350tre ce pli?',
        text: 'Pli#' + id_pli,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/admin/support/hors_perim/' + id_pli+'/'+flg_tt, {}, function(rep) {
            if (rep == 1) {
                swal({
                    title: "R\351ussi!",
                    text: "Pli#" + id_pli + " mis hors p\351rim\350tre.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Modification &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

function afficher(id_pli) {
    $.get(chemin_site + '/admin/support/display/' + id_pli, {}, function(reponse) {
        $('#mdl_display_pli_title').html(id_pli);
        $('#mdl_display_pli_body').html(reponse);
        $('[data-toggle="tooltip"]').tooltip();
        $('#mdl_display_pli').modal('show');
    });
}

function unlock(id_pli, flg_tt) {
    var msg_unlock = '';
    switch (flg_tt * 1) {
        case 1:
            msg_unlock = 'le typage';
            break;
        case 4:
            msg_unlock = 'la saisie';
            break;
        case 8:
            msg_unlock = 'le contr\364le';
            break;
        case 12:
            msg_unlock = 'la valdation';
            break;
        case 13:
            msg_unlock = 'le retraitement';
            break;
        default:
            swal('Information', 'Le pli#' + id_pli + ' est non v\350rrouill\350!', 'info');
            break;
    }
    if (msg_unlock != '') {
        swal({
            title: 'D\350verrouiller ' + msg_unlock + ' de ce pli?',
            text: 'Pli#' + id_pli,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Valider",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.get(chemin_site + '/admin/support/unlock/' + id_pli + '/' + flg_tt, {}, function(rep) {
                if (rep == 1) {
                    swal({
                        title: "R\351ussi!",
                        text: msg_unlock + " du pli#" + id_pli + " a \350t\350 D\350verrouill\350.",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: true
                    });
                } else {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: D&eacute;verrouillage &eacute;chou&eacute;!.', 'top', 'right', null, null);
                    swal("Serveur", "Erreur! " + rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            }).always(function() {
                load_table();
            });
        });
    }
}

function reprise(id_pli,flg_tt) {
    swal({
        title: 'Reprendre le traitement de ce pli?',
        text: 'Pli#' + id_pli,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/admin/support/reprise_traitement/' + id_pli+'/'+flg_tt, {}, function(rep) {
            if (rep == 1) {
                swal({
                    title: "R\351ussi!",
                    text: "Pli#" + id_pli + " recuper\350.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Recuperation &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

function show_info_grp_pli() {
    $('#mdl_display_group_pli').modal('show');
}

function load_list_grp_pli() {
    $('#mdl_display_group_pli_body').html('');
    $.get(chemin_site + '/admin/support/list_groupement_pli', {}, function(view) {
        $('#mdl_display_group_pli_body').html(view);
    });
}

function show_info_grp_pli_by_older() {
    $('#mdl_display_group_pli_by_older').modal('show');
}

function load_list_grp_pli_by_older() {
    $('#mdl_display_group_pli_by_older_body').html('');
    $.get(chemin_site + '/admin/support/list_groupement_pli_by_older', {}, function(view) {
        $('#mdl_display_group_pli_by_older_body').html(view);
    });
}

function afficher_histo(id_pli) {
    $.get(chemin_site + '/admin/support/histo_pli/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#mdl_display_histo_title').html(id_pli);
            $('#mdl_display_histo_body').html(data.ihm);
            $('[data-toggle="tooltip"]').tooltip();
            $('#mdl_display_histo').modal('show');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function affiche_motif_consigne(id_pli) {
    $.get(chemin_site + '/control/ajax/all_motif_consigne/'+id_pli, {}, function(reponse) {
        $('#mdl_disp_motif_consigne_pli_id_pli').html(id_pli);
        $('#mdl_disp_motif_consigne_pli_body').html(reponse);
        $('#mdl_disp_motif_consigne_pli').modal('show');
    });
}
// function modifier(id_pli) {}

//code: display more pli informations on modale (need plugins: jdPanel)
function detail_pli(id_pli) {
    $.get(chemin_site + '/visual/visual/detail_pli/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            jsPanel.create({
                headerTitle: data.titre
                ,headerToolbar: data.sous_titre
                ,footerToolbar: data.footer
                ,content: data.body
                ,contentSize: {
                    width: '80%'
                    ,height: '80vh'
                }
                ,theme: 'light'
                ,onbeforemaximize: function(panel) {
                    $('html,body').scrollTop(0);
                    return true;
                }
            });
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        // swal("Serveur", "Erreur serveur!", "error");
    });
}

function load_docs_offset(id_pli, offset, id_dom){
    $.get(chemin_site + '/visual/visual/view_docs_offset/' + id_pli+'/'+offset+'/'+id_dom, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {console.log(id_dom);
            $('#'+id_dom).html(data.ihm);
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function load_img_doc_face(id_doc, face=0) {
    $.get(chemin_site + '/visual/visual/get_img_doc/' + id_doc, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#zone_img_temp').html(data.img);
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            if(face == 0) {
                $('#img_temp_recto').trigger('click');
            }else{
                $('#img_temp_verso').trigger('click');
            }
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}
//end code: display more pli informations on modale

app_assignation_pli = new Vue({
    el: '#content_assignation_pli_modal'
    ,data: {
        id_pli: ''
        //,pli: null
        ,op_typage: ''
        ,op_saisie: ''
        ,op_ctrl: ''
        ,users: []
        ,users_ctrl: []
    }
    ,methods: {
        affiche_modal: function(id_pli_i){
            var this_app = this;
            $.get(chemin_site + '/admin/support/data_assignation_pli/'+id_pli_i, null, function(from_server) {
                try {
                    var data = $.parseJSON(from_server);
                } catch (error) {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
                    return '';
                }
                if (data.code == '0') {
                    this_app.id_pli = id_pli_i;
                    //this_app.pli = data.pli;
                    this_app.op_typage = data.op_typage;
                    this_app.op_saisie = data.op_saisie;
                    this_app.op_ctrl = data.op_ctrl;
                    this_app.users = data.users;
                    this_app.users_ctrl = data.users_ctrl;
                    setTimeout(function(){ 
                        $('.sel_select_op').selectpicker('refresh'); 
                        $('.sel_select_op').selectpicker('render'); 
                    }, 100);
                    $('#mdl_assignation_pli').modal('show');
                } else {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
                }
            });
        }
        ,enregistrer: function () {
            var this_app = this;
            swal({
                title: "Confirmation",
                text: "Enregistrer?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Annuler!",
                showLoaderOnConfirm: true,
                closeOnConfirm: false
            }, function() {
                $.post(chemin_site + '/admin/support/assignation_pli', {
                    id_pli: this_app.id_pli
                    ,op_typage: Number.isInteger(this_app.op_typage*1) || this_app.op_typage != 0 ? this_app.op_typage : ''
                    ,op_saisie: Number.isInteger(this_app.op_saisie*1) || this_app.op_saisie != 0 ? this_app.op_saisie : ''
                    ,op_ctrl: Number.isInteger(this_app.op_ctrl*1) || this_app.op_ctrl != 0 ? this_app.op_ctrl : ''
                }, function(from_server) {
                    if (from_server == 0) {
                        swal({
                            title: "Enregistrement",
                            text: "R\351ussi!",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: true
                        });
                        $('#mdl_assignation_pli').modal('hide');
                    } else {
                        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + from_server, 'top', 'center', null, null);
                        swal("Serveur", "Erreur serveur!", "error");
                    }
                });
            });
        }
        ,vide_typage: function () {
            this.op_typage = '';
            setTimeout(function(){ $('.sel_select_op').selectpicker('render'); }, 100);
        }
        ,vide_saisie: function () {
            this.op_saisie = '';
            setTimeout(function(){ $('.sel_select_op').selectpicker('render'); }, 100);
        }
        ,vide_ctrl: function () {
            this.op_ctrl = '';
            setTimeout(function(){ $('.sel_select_op').selectpicker('render'); }, 100);
        }
    }
    ,computed: {
        with_typage: function(){
            return this.op_typage != '';
        }
        ,with_saisie: function(){
            return this.op_saisie != '';
        }
        ,with_ctrl: function(){
            return this.op_ctrl != '';
        }
    },
});
