$(function() {
    dttb_gr_user = null;
    load_table();
    $('.sel_gr_poss').selectpicker('destroy');
    $('.sel_gr_poss').multiSelect({ selectableOptgroup: true });
    $('#sel_member_gr_modal').selectpicker('destroy');
    $('#sel_member_gr_modal').multiSelect();
    $('#add_group_modal').on('show.bs.modal', function(e) {
        $('#sel_add_gr_poss').multiSelect('deselect_all');
        load_multi_select('sel_add_gr_poss_by');
        var frms_line_focused = $('#frm_add').find('.form-line.focused');
        frms_line_focused.each(function() {
            $(this).removeClass('focused');
        });
    });
});

function load_multi_select(id_by, $my_val = null) {
    var val_by = $('#' + id_by).val();
    var zone = $('#' + id_by).closest('.zone_sel_gr_poss');
    var sels = $(zone).find('.sel_gr_poss');
    var sel = sels[0];
    var id_sel = $(sel).attr('id');
    var val_sel = $my_val === null ? $(sel).val() : $my_val;
    $.get(chemin_site + '/admin/groups_user/get_grp_pli_poss/' + val_by, {},
        function(from_serv) {
            $(sel).multiSelect('deselect_all');
            $(sel).html(from_serv);
            $(sel).multiSelect('refresh');
            if (val_sel !== null) {
                $(sel).multiSelect('select', val_sel);
            }
        }
    );
}

function load_table() {
    if(dttb_gr_user === null){
        if ($('#group_admin_datatable')) {
            dttb_gr_user = $('#group_admin_datatable').DataTable({
                "bprocessing": true,
                "bServerSide": true,
                "bScrollCollapse": true,
                "aLengthMenu": [
                    [5, 10, 25, 50, 100],
                    [5, 10, 25, 50, 100]
                ],
                "pageLength": 5
                    //,responsive: true
                    ,
                //"bDestroy": true,
                "oLanguage": {
                    "sLengthMenu": "_MENU_ lignes par page",
                    "szeroRecords": "Aucune donn\351e",
                    "semptyTable": "Aucune donn\351e",
                    "sInfo": "de _START_ \340 _END_ sur _TOTAL_",
                    "sInfoFiltered": '(Total : _MAX_ enregistrements)',
                    "oPaginate": {
                        "sFirst": "PREMIERE",
                        "sPrevious": "PRECEDENTE",
                        "sNext": "SUIVANTE",
                        "sLast": "DERNIERE"
                    },
                    "sSearch": 'Rechercher: '
                },
                "ajax": {
                    'url': chemin_site + '/admin/groups_user/list_group',
                    'sServerMethod': 'POST'
                },
                "fnDrawCallback": function(oSettings) {
                    $('[data-toggle="tooltip"]').tooltip();
                },
                aoColumnDefs: [
                    { 'bSortable': false, 'aTargets': [1, 5] }, { "bSearchable": false, "aTargets": [2, 3, 4] }, {
                        "aTargets": [1, 2, 3, 4, 5],
                        "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                            $(nTd).addClass('align-center').addClass('v_aligne_m');
                        }
                    }, {
                        "aTargets": [0],
                        "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                            $(nTd).addClass('v_aligne_m');
                        }
                    }
                ],
                'columns': [
                    { "data": 'nom', "targets": [0] }, { "data": 'cr', "targets": [1] }, { "data": 'nb_cr', "targets": [2] }, { "data": 'nb_user', "targets": [3] }, { "data": 'actif', "targets": [4] }, { "data": 'action', "targets": [5] }
                ]
            });
        }
    }else{
        dttb_gr_user.ajax.reload();
    }
}

function modif_gr(b_id) {
    $.get(chemin_site + '/admin/groups_user/data_grp_user/' + atob(b_id), {},
        function(from_srv_json) {
            try {
                var from_serv = $.parseJSON(from_srv_json);
                if (from_serv.code == 0) {
                    $('#modif_gr_nom').val(from_serv.nom);
                    $('#modif_gr_actif').prop('checked', (from_serv.actif == 1));
                    load_multi_select('sel_modif_gr_poss_by', from_serv.grps);
                    $('#modif_gr_bt_ok').attr('onclick', 'send_modif_gr(\'' + b_id + '\');');
                    $('#modif_group_modal').modal('show');
                } else {
                    swal("Serveur", "Erreur! " + from_serv.msg, "error");
                }
            } catch (error) {
                swal("Serveur", "Erreur! " + from_serv, "error");
            }
        }
    );
}

function verif_data_modif(b_id) {
    var nom = $.trim($('#modif_gr_nom').val());
    var actif = $("#modif_gr_actif").prop('checked') ? 1 : 0;
    var grp_pli_poss = $('#sel_modif_gr_poss').val() !== null ? ($('#sel_modif_gr_poss').val()).join() : '';
    var rep_verif = { code: true };
    if (is_vide('modif_gr_nom')) {
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: le nom du groupe doit &ecirc;tre renseigner.', 'bottom', 'right', null, null);
        rep_verif.code = false;
    }
    if (rep_verif.code) {
        rep_verif.data = {
            b_id: b_id,
            b_nom: btoa(nom),
            b_actif: btoa(actif),
            b_grp_pli_poss: btoa(grp_pli_poss)
        };
    }
    return rep_verif;
}

function send_modif_gr(b_id) {
    var verif = verif_data_modif(b_id);
    if (verif.code) {
        swal({
            title: "Enregistrer?",
            text: "Les modifications sur ce groupe",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Annuler!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.post(chemin_site + '/admin/groups_user/modif_gr', verif.data, function(rep) {
                if (rep == 0) {
                    showNotification('alert-success', '<i class="material-icons">done</i> Modifications enregistr&eacute;es.', 'top', 'right', null, null);
                    swal({
                        title: "R\351ussi!",
                        text: "Modifications enregistr\351es.",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: true
                    });
                    $('#modif_group_modal').modal('hide');
                } else {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Modification &eacute;chou&eacute;e.', 'top', 'right', null, null);
                    swal("Serveur", "Erreur! " + rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            }).always(function() {
                load_table();
            });
        });
    }
}

function verif_data_new() {
    var nom = $.trim($('#add_gr_nom').val());
    var actif = $("#add_gr_actif").prop('checked') ? 1 : 0;
    var grp_pli_poss = $('#sel_add_gr_poss').val() !== null ? ($('#sel_add_gr_poss').val()).join() : '';
    var rep_verif = { code: true };
    if (is_vide('add_gr_nom')) {
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: le nom du groupe doit &ecirc;tre renseigner.', 'bottom', 'right', null, null);
        rep_verif.code = false;
    }
    if (rep_verif.code) {
        rep_verif.data = {
            b_nom: btoa(nom),
            b_actif: btoa(actif),
            b_grp_pli_poss: btoa(grp_pli_poss)
        };
    }
    return rep_verif;
}

function send_new_group() {
    var verif = verif_data_new();
    if (verif.code) {
        swal({
            title: "Enregistrer?",
            text: "Ce nouveau groupe",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Annuler!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.post(chemin_site + '/admin/groups_user/new_gr', verif.data, function(rep) {
                if (rep == 0) {
                    showNotification('alert-success', '<i class="material-icons">done</i> Groupe enregistr&eacute;.', 'top', 'right', null, null);
                    swal({
                        title: "R\351ussi!",
                        text: "Enregistrement r&eacute;ussi.",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: true
                    });
                    $('#add_group_modal').modal('hide');
                } else {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Enregistrement &eacute;chou&eacute;.', 'top', 'right', null, null);
                    swal("Serveur", "Erreur! " + rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            }).always(function() {
                load_table();
            });
        });
    }
}

function suppr_gr(b_id, b_nom) {
    swal({
        title: "Supprimer ce groupe?",
        text: atob(b_nom) + " (#" + atob(b_id) + ")!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, supprimer!",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.get(chemin_site + '/admin/groups_user/suppr_gr/' + atob(b_id), {}, function(rep) {
            if (rep == 0) {
                showNotification('alert-success', '<i class="material-icons">done</i> Groupe supprim&eacute;.', 'top', 'right', null, null);
                swal({
                    title: "R\351ussi!",
                    text: "Groupe supprim\351.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: suppression &eacute;chou&eacute;e.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

function is_vide(id_champ) {
    var champ = $('#' + id_champ);
    if (champ) {
        var valeur = $.trim(champ.val());
        if (valeur === '') {
            champ.parents('.form-line').addClass('focused').addClass('error').removeClass('success').removeClass('warning');
            return true;
        } else {
            champ.parents('.form-line').addClass('focused').removeClass('error').addClass('success').removeClass('warning');
            return false;
        }
    }
    return true;
}

function affectation(b_id, b_nom) {
    $('#ident_member_gr_modal').html(atob(b_nom) + ' #' + atob(b_id));
    $.get(chemin_site + '/admin/groups_user/data_grp_user_member/' + atob(b_id), {},
        function(from_srv_json) {
            try {
                var from_serv = $.parseJSON(from_srv_json);
                $('#sel_member_gr_modal').multiSelect('deselect_all');
                $('#sel_member_gr_modal').html(from_serv.operators);
                $('#sel_member_gr_modal').multiSelect('refresh');
                $('#sel_member_gr_modal').multiSelect('select', from_serv.members);
                $('#member_gr_bt_ok_modal').attr('onclick', 'send_member(\'' + b_id + '\');');
                $('#membre_group_modal').modal('show');
            } catch (error) {
                swal("Serveur", "Erreur serveur!", "error");
            }
        }
    );
}

function send_member(b_id) {
    var data_member = {
        b_id_gr: b_id,
        b_members: $('#sel_member_gr_modal').val() !== null ? ($('#sel_member_gr_modal').val()).join() : ''
    }
    swal({
        title: "Enregistrer?",
        text: "Les modifications sur les membres du groupe",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + '/admin/groups_user/save_member', data_member, function(rep) {
            if (rep == 0) {
                showNotification('alert-success', '<i class="material-icons">done</i> Modifications enregistr&eacute;es.', 'top', 'right', null, null);
                swal({
                    title: "R\351ussi!",
                    text: "Modifications enregistr\351es.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
                $('#membre_group_modal').modal('hide');
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Modification &eacute;chou&eacute;e.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}
