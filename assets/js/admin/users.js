$(function() {
    dttb_users = null;
    load_table();
    $('#add_user_modal').on('show.bs.modal', function(e) {
        init_modal_add_user();
    });
    $('#modif_user_modal').on('show.bs.modal', function(e) {
        manage_view_modal_modif_user();
    });
});

function manage_view_modal_add_user() {
    $('.traitement-ko').show();

    $('#add_user_type').val() == '2' ? $('.add_op_only').show() : $('.add_op_only').hide();
    $('#add_user_type').val() == '5' ? $('.add_op_push_only').show() : $('.add_op_push_only').hide();

    ($('#add_user_type').val().trim() == '4' || $('#add_user_type').val().trim() == '1' ) ? $('.traitement-ko').show() : $('.traitement-ko').hide();
   
    
}

function manage_view_modal_modif_user() {
    $('#modif_user_type').val() == '2' ? $('.modif_op_only').show() : $('.modif_op_only').hide();
    $('#modif_user_type').val() == '5' ? $('.modif_op_push_only').show() : $('.modif_op_push_only').hide();

    ($('#modif_user_type').val().trim() == '4' || $('#modif_user_type').val().trim() == '1' ) ? $('.traitement-ko').show() : $('.traitement-ko').hide();
    
}

function init_modal_add_user() {
    $('#add_user_login').val('');
    $('#add_user_pass').val('');
    $('#add_user_matr').val('');
    $('#add_user_type').selectpicker('val', '1');
    $('#add_user_op_niveau').selectpicker('val', '1');
    $('#add_user_group').selectpicker('val', '-1');
    $('#add_op_push_acces').selectpicker('val', '2');
    $('#add_user_actif').prop('checked', true);
    //$.AdminBSB.input.activate();
    $('#add_user_login').parents('.form-line').addClass('focused').removeClass('error').addClass('success').removeClass('warning');
    $('#add_user_pass').parents('.form-line').addClass('focused').removeClass('error').addClass('success').removeClass('warning');
    var frms_line_focused = $('#frm_add').find('.form-line.focused');
    frms_line_focused.each(function() {
        $(this).removeClass('focused');
    });
    manage_view_modal_add_user();
}

function load_table() {
    if(dttb_users === null){
        if ($('#user_admin_datatable')) {
            dttb_users = $('#user_admin_datatable').DataTable({
                "bprocessing": true,
                "bServerSide": true,
                "bScrollCollapse": true,
                "aLengthMenu": [
                    [5, 10, 25, 50, 100],
                    [5, 10, 25, 50, 100]
                ],
                "pageLength": 5
                    //,responsive: true
                    ,
                //"bDestroy": true,
                "oLanguage": {
                    "sLengthMenu": "_MENU_ lignes par page",
                    "szeroRecords": "Aucune donn\351e",
                    "semptyTable": "Aucune donn\351e",
                    "sInfo": "de _START_ \340 _END_ sur _TOTAL_",
                    "sInfoFiltered": '(Total : _MAX_ enregistrements)',
                    'sLoadingRecords': 'Chargement en cours...',
                    'sProcessing': 'Traitement en cours...',
                    'oPaginate': {
                        'sFirst': 'PREMIERE',
                        'sPrevious': 'PRECEDENTE',
                        'sNext': 'SUIVANTE',
                        'sLast': 'DERNIERE'
                    },
                    'sSearch': 'Rechercher: '
                },
                'ajax': {
                    'url': chemin_site + '/admin/users/list_user',
                    'sServerMethod': 'POST'
                },
                'fnDrawCallback': function(oSettings) {
                    $('[data-toggle="tooltip"]').tooltip();
                },
                aoColumnDefs: [
                    { 'bSortable': false, 'aTargets': [5] }, { 'bSearchable': false, 'aTargets': [5] }, {
                        'aTargets': [3, 4, 5],
                        'fnCreatedCell': function(nTd, sData, oData, iRow, iCol) {
                            $(nTd).addClass('align-center').addClass('v_aligne_m');
                        }
                    }, {
                        'aTargets': [0, 1, 2],
                        'fnCreatedCell': function(nTd, sData, oData, iRow, iCol) {
                            $(nTd).addClass('v_aligne_m');
                        }
                    }
                ],
                'columns': [
                    { "data": 'login', "targets": [0] }
                    , { "data": 'type', "targets": [1] }
                    , { "data": 'gr', "targets": [2] }
                    , { "data": 'etat', "targets": [3] }
                    , { "data": 'matricule', "targets": [4] }
                    , { "data": 'action', "targets": [5] }
                ]
            });
        }
    }else{
        dttb_users.ajax.reload();
    }
}

function modif_pers(b_id, b_login, b_id_tp, b_id_gr, b_actif, b_pass, b_matr, b_acces,avec_traitement_ko = 0, niveau_op = 1) {
    var id_user = atob(b_id);
    var login = atob(b_login);
    var id_tp = atob(b_id_tp);
    var id_gr = atob(b_id_gr);
    var actif = atob(b_actif);
    var pass = atob(b_pass);
    var matr = atob(b_matr);
    var acces = atob(b_acces);
    acces = acces == '' ? 1 : acces;
    niveau_op = niveau_op == '' ? 1 : niveau_op;
    $('#modif_user_login').val(login);
    $('#modif_user_pass').val(pass);
    $('#modif_user_matr').val(matr);
    $('#modif_user_type').selectpicker('val', id_tp);
    $('#modif_user_op_niveau').selectpicker('val', niveau_op);
    $('#modif_user_group').selectpicker('val', id_gr);
    $('#modif_op_push_acces').selectpicker('val', acces);
    var val_actif = actif == 1 ? true : false;
    var avecTraitementKO  =  avec_traitement_ko == 1 ? true : false;
    $('#modif_user_actif').prop('checked', val_actif);
    $('#modif-user-traitement-ko').prop('checked', avecTraitementKO);
    $('#modif_user_bt_ok').attr('onclick', 'send_modif_user(\'' + b_id + '\');');
    $('#modif_user_modal').modal('show');
    $('#frm_ln_modif_matr').addClass('focused');
    if(matr == ''){
        $('#frm_ln_modif_matr').removeClass('focused');
    }
    //$.AdminBSB.input.activate();
}

function verif_data_modif(b_id) {
    var login = $.trim($('#modif_user_login').val());
    var pswd = $.trim($('#modif_user_pass').val());
    var matr = $.trim($('#modif_user_matr').val());
    var id_tp = $('#modif_user_type').val();
    var niveau_op = $('#modif_user_op_niveau').val();
    var id_gr = $('#modif_user_group').val() == '-1' ? '' : $('#modif_user_group').val();
    var id_acces_op = $('#modif_op_push_acces').val();
    var actif = $("#modif_user_actif").prop('checked') ? 1 : 0;
    var traitementKo = $("#modif-user-traitement-ko").prop('checked') ? 1 : 0;
    var rep_verif = { code: true };
    $('#modif_user_login').parents('.form-line').addClass('focused').removeClass('error').addClass('success').removeClass('warning');
    $('#modif_user_pass').parents('.form-line').addClass('focused').removeClass('error').addClass('success').removeClass('warning');
    if (!verifie_login(login)) {
        $('#modif_user_login').parents('.form-line').addClass('focused').addClass('error').removeClass('success').removeClass('warning');
        rep_verif.code = false;
    }
    if (!verifie_pswd(pswd)) {
        $('#modif_user_pass').parents('.form-line').addClass('focused').addClass('error').removeClass('success').removeClass('warning');
        rep_verif.code = false;
    }
    if(matr != ''){
        if(!verifie_nb(matr)){
            $('#modif_user_matr').parents('.form-line').addClass('focused').addClass('error').removeClass('success').removeClass('warning');
            rep_verif.code = false;
        }
    }
    if (rep_verif.code) {
        rep_verif.data = {
            b_id: b_id,
            b_login: btoa(login),
            b_pswd: btoa(pswd),
            b_matr: btoa(matr),
            b_id_tp: btoa(id_tp),
            b_niveau_op: niveau_op,
            b_id_gr: id_gr > 0 && id_gr != '' ? btoa(id_gr) : btoa(''),
            b_id_acces_op: btoa(id_acces_op),
            b_actif: btoa(actif),
            b_ttmt_ko: btoa(traitementKo),
        };
    }
    return rep_verif;
}

function send_modif_user(b_id) {
    var verif = verif_data_modif(b_id);
    if (verif.code) {
        swal({
            title: "Enregistrer?",
            text: "Les modifications sur l'utilisateur",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Annuler!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.post(chemin_site + '/admin/users/modifier', verif.data, function(rep) {
                if (rep == 0) {
                    showNotification('alert-success', '<i class="material-icons">done</i> Modifications enregistr&eacute;es.', 'top', 'right', null, null);
                    swal({
                        title: "R\351ussi!",
                        text: "Modifications enregistr\351es.",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: true
                    });
                    $('#modif_user_modal').modal('hide');
                } else {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Modification &eacute;chou&eacute;e.', 'top', 'right', null, null);
                    swal("Serveur", "Erreur! " + rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            }).always(function() {
                load_table();
            });
        });
    }
}

function verif_data_new() {
    var login = $.trim($('#add_user_login').val());
    var pswd = $.trim($('#add_user_pass').val());
    var matr = $.trim($('#add_user_matr').val());
    var id_tp = $('#add_user_type').val();
    var niveau_op = $('#add_user_op_niveau').val();
    var id_gr = $('#add_user_group').val() == '-1' ? '' : $('#add_user_group').val();
    var id_acces_op = $('#add_op_push_acces').val();
    var actif = $("#add_user_actif").prop('checked') ? 1 : 0;
    var traitementKo = $("#user-traitement-ko").prop('checked') ? 1 : 0;
    var rep_verif = { code: true };
    $('#add_user_login').parents('.form-line').addClass('focused').removeClass('error').addClass('success').removeClass('warning');
    $('#add_user_pass').parents('.form-line').addClass('focused').removeClass('error').addClass('success').removeClass('warning');
    if (!verifie_login(login)) {
        $('#add_user_login').parents('.form-line').addClass('focused').addClass('error').removeClass('success').removeClass('warning');
        rep_verif.code = false;
    }
    if (!verifie_pswd(pswd)) {
        $('#add_user_pass').parents('.form-line').addClass('focused').addClass('error').removeClass('success').removeClass('warning');
        rep_verif.code = false;
    }
    if(matr != ''){
        if(!verifie_nb(matr)){
            $('#add_user_matr').parents('.form-line').addClass('focused').addClass('error').removeClass('success').removeClass('warning');
            rep_verif.code = false;
        }
    }
    if (rep_verif.code) {
        rep_verif.data = {
            b_login: btoa(login),
            b_pswd: btoa(pswd),
            b_matr: btoa(matr),
            b_id_tp: btoa(id_tp),
            b_niveau_op: niveau_op,
            b_id_gr: btoa(id_gr),
            b_id_acces_op: btoa(id_acces_op),
            b_actif: btoa(actif),
            b_ttmt_ko: btoa(traitementKo)
        };
    }
    return rep_verif;
}

function verifie_login(val) {
    rep = false;
    var reg = /^[a-zA-Z0-9_]{2,}$/;
    if (reg.test($.trim(val))) {
        rep = true;
    } else {
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: pour le LOGIN n\'utilisez que les caract&egrave;res a-zA-Z0-9_ et au minimum 2 caract&egrave;res.', 'bottom', 'right', null, null);
    }
    return rep;
}

function verifie_pswd(val) {
    rep = false;
    var reg = /^[a-zA-Z0-9+.#%*&_-]{6,}$/;
    if (reg.test($.trim(val))) {
        rep = true;
    } else {
        showNotification('alert-warning', '<i class="material-icons">error</i>Alerte: pour le mot de passe n\'utilisez que les caract&egrave;res a-zA-Z0-9+.#%*&_- et au minimum 6 caract&egrave;res.', 'bottom', 'right', null, null);
    }
    return rep;
}

function verifie_nb(val){
	rep = false;
	var reg = /^\d+$/;
	if(reg.test($.trim(val))){
		rep = true;
	}
	return rep;
}

function send_new_user() {
    var verif = verif_data_new();
    if (verif.code) {
        swal({
            title: "Enregistrer?",
            text: "L'utilisateur",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Annuler!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.post(chemin_site + '/admin/users/new_user', verif.data, function(rep) {
                if (rep == 0) {
                    showNotification('alert-success', '<i class="material-icons">done</i> Utilisateur enregistr&eacute;.', 'top', 'right', null, null);
                    swal({
                        title: "R\351ussi!",
                        text: "Enregistrement r\351ussi.",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: true
                    });
                    $('#add_user_modal').modal('hide');
                } else {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Enregistrement &eacute;chou&eacute;.', 'top', 'right', null, null);
                    swal("Serveur", "Erreur! " + rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Serveur", "Erreur serveur!", "error");
            }).always(function() {
                load_table();
            });
        });
    }
}

function suppr_pers(b_id, b_login) {
    swal({
        title: "Supprimer l' utilisateur?",
        text: atob(b_login) + " (#" + atob(b_id) + ")!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, supprimer!",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + '/admin/users/suppr', { id: b_id }, function(rep) {
            if (rep == 0) {
                showNotification('alert-success', '<i class="material-icons">done</i> Utilisateur supprim&eacute;.', 'top', 'right', null, null);
                swal({
                    title: "R\351ussi!",
                    text: "Utilisateur supprim\351.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
            } else {
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: suppression &eacute;chou&eacute;e.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

function is_vide(id_champ) {
    var champ = $('#' + id_champ);
    if (champ) {
        var valeur = $.trim(champ.val());
        if (valeur === '') {
            champ.parents('.form-line').addClass('focused').addClass('error').removeClass('success').removeClass('warning');
            return true;
        } else {
            champ.parents('.form-line').addClass('focused').removeClass('error').addClass('success').removeClass('warning');
            return false;
        }
    }
    return true;
}

app_wallboard_user = new Vue({
    el: '#content_wallboard_user_modal'
    ,data:{
        s_login: ''
        ,waiting: false
        ,initialised: false
        ,data_lignes:''
        ,time_s_load: null
        ,nbrs: {
            nb: 0
			,connecte: 0
			,connecte_op_cr: 0
			,connecte_op_cr_ttr: 0
			,connecte_op_fx: 0
			,connecte_op_fx_ttr: 0
        }
    }
    ,methods: {
        affiche_modal: function () {
            $('#wallboard_user_modal').modal('hide');
            this.s_login = '';
            this.initialised = false;
            this.reload_data();
        }
        ,reload_data: function(){
            if(this.waiting){
                this.wait_reload_data();
                return ;
            }
            this.waiting = true;
            // clearTimeout(this.time_s_load);
            this.time_s_load = null;
            this.nbrs.nb = 0;
            var this_app = this;
            $.post(chemin_site + '/admin/users/get_wallboard_user_activity', {s_login: this_app.s_login}, function(from_server) {
                this_app.waiting = false;
                try {
                    var data = $.parseJSON(from_server);
                } catch (error) {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
                    return '';
                }
                this_app.data_lignes = data.data;
                this_app.nbrs.nb = data.nb;
                this_app.nbrs.connecte = data.connecte;
                this_app.nbrs.connecte_op_cr = data.connecte_op_cr;
                this_app.nbrs.connecte_op_cr_ttr = data.connecte_op_cr_ttr;
                this_app.nbrs.connecte_op_fx = data.connecte_op_fx;
                this_app.nbrs.connecte_op_fx_ttr = data.connecte_op_fx_ttr;
                if(!this_app.initialised){
                    $('#wallboard_user_modal').modal('show');
                    this_app.initialised = true;
                }
            });
        }
        ,wait_reload_data: function(){
            var this_app = this;
            if(this.time_s_load != null){
                clearTimeout(this.time_s_load);
            }
            this.time_s_load = setTimeout(function(){ this_app.reload_data() }, 1000);
        }
    },
    watch: {
        s_login: function(){
            this.wait_reload_data();
        }
    },
});

//code: display more pli informations on modale (need plugins: jdPanel)
function detail_pli(id_pli) {
    $.get(chemin_site + '/visual/visual/detail_pli/' + id_pli, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#wallboard_user_modal').modal('hide');
            jsPanel.create({
                headerTitle: data.titre
                ,headerToolbar: data.sous_titre
                ,footerToolbar: data.footer
                ,content: data.body
                ,contentSize: {
                    width: '80%'
                    ,height: '80vh'
                }
                ,theme: 'light'
                ,onbeforemaximize: function(panel) {
                    $('html,body').scrollTop(0);
                    return true;
                }
            });
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        // swal("Serveur", "Erreur serveur!", "error");
    });
}

function load_docs_offset(id_pli, offset, id_dom){
    $.get(chemin_site + '/visual/visual/view_docs_offset/' + id_pli+'/'+offset+'/'+id_dom, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {console.log(id_dom);
            $('#'+id_dom).html(data.ihm);
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}

function load_img_doc_face(id_doc, face=0) {
    $.get(chemin_site + '/visual/visual/get_img_doc/' + id_doc, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '1') {
            $('#zone_img_temp').html(data.img);
            $('[data-magnify=gallery]').magnify({
                footToolbar: ['prev', 'zoomIn', 'zoomOut', 'fullscreen', 'rotateRight', 'rotateLeft', 'next']
            });
            if(face == 0) {
                $('#img_temp_recto').trigger('click');
            }else{
                $('#img_temp_verso').trigger('click');
            }
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.msg, 'top', 'center', null, null);
        }
    });
}
//end code: display more pli informations on modale

function histo_user(id_user) {
    $.get(chemin_site + '/admin/users/histo_user/' + id_user, {}, function(reponse) {
        try {
            var data = $.parseJSON(reponse);
        } catch (error) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
            return '';
        }
        if (data.code == '0') {
            jsPanel.create({
                headerTitle: data.titre
                ,headerToolbar: data.sous_titre
                //,footerToolbar: data.footer
                ,content: data.body
                ,footerToolbar: data.footer
                ,contentSize: {
                    width: '80%'
                    ,height: '80vh'
                }
                ,theme: 'light'
                ,onbeforemaximize: function(panel) {
                    $('html,body').scrollTop(0);
                    return true;
                }
            });
            eval(data.js);
            $('#'+data.id_tab).css('width', '100%');
        } else {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: ' + data.message, 'top', 'center', null, null);
        }
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Erreur serveur!.', 'top', 'right', null, null);
        // swal("Serveur", "Erreur serveur!", "error");
    });
}
