
$(function() {
    init_compos();
    //load_table();
    $('#mdl_display_abo').on('shown.bs.modal', function() {
       
        if($('#visu_abo').length >0 ) {
            $('#visu_abo').dataTable();
        }
    });
});

//initialisation datepicker
function init_compos() {
    $('.dt_field').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY',
        lang: 'fr',
        clearButton: true,
        clearText: 'Vider',
        cancelText: 'Annuler',
        weekStart: 1,
        switchOnClick: true,
        time: false
    }).on('change', function(e, date) {
        if ($(this).val() == '') {
            $(this).closest('.form-line').removeClass('focused');
        } else {
            $(this).closest('.form-line').addClass('focused');
        }
    });
}

$(document).on("keypress", function(e){
    if(e.which == 13){
        load_table();
    }
});

function load_table() {
    //vider array id_flux pour eviter les redondances
    arrayflux =[];
    table =  $('#support_push').DataTable({
        'bprocessing': true,
        'bServerSide': true,
        'bScrollCollapse': true,
        'aLengthMenu': [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        'pageLength': 5,
        'bDestroy': true,
        //'bStateSave': true,
        'bFilter': false,
        'sScrollX': true,
        'oLanguage': {
            'sLengthMenu': '_MENU_ lignes par page',
            'sZeroRecords': "Aucun flux",
            'sEmptyTable': "Aucun flux",
            'sInfo': "de _START_ \340 _END_ sur _TOTAL_ flux",
            'sInfoFiltered': '(Total : _MAX_ plis)',
            'sLoadingRecords': 'Chargement en cours...',
            'sProcessing': 'Traitement en cours...',
            'oPaginate': {
                'sFirst': 'PREMIERE',
                'sPrevious': 'PRECEDENTE',
                'sNext': 'SUIVANTE',
                'sLast': 'DERNIERE'
            },
            'sSearch': 'Rechercher: '
        },
        'ajax': {
            'url': chemin_site + '/admin/support_push/getPlis',
            'sServerMethod': 'POST',
            'data': function(d) {
                d.id_pli = $("#txt_recherche_id_pli").val();
                d.source = $("#sel_source").val();
                d.societe = $("#sel_societe").val();
                d.etat = $("#sel_etat").val();
                d.typologie = $("#sel_typologie").val();
                d.statut = $("#sel_statut").val();
                d.motif = $("#select_motif").val();
                d.op_saisi = $("#sel_op_saisie").val();
                d.op_typage = $("#sel_op_typage").val();
                d.from_abonne = $("#from_abonne").val();
                d.from_payeur = $("#from_payeur").val();
                d.date_ttr1 = $("#dt_act_1").val();
                d.date_ttr2 = $("#dt_act_2").val();
            }
        },
        /*"order": [
            [0, "desc"]
        ],*/
        'fnDrawCallback': function(oSettings) {
            $('[data-toggle="tooltip"]').tooltip();
            set_action();
        },
        aoColumnDefs: [
            { 'bSortable': false, 'aTargets': [0] },
            {
                'aTargets': [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
                "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                    $(nTd).addClass('align-center');
                }
            }//,
            //{ "width": "30%", "aTargets": [13] }
        ],
        
        // aoColumnDefs: [{
        //     'aTargets': [3, 4, 5, 12, 13],
        //     "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
        //         $(nTd).addClass('align-center');
        //     }
        // }, { 'bSortable': false, 'aTargets': [12, 13] }],
        // 'columns': [
        //     { 'data': 'id_flux', 'targets': [0] },
        //     { 'data': 'source', 'targets': [1] },
            
        // ]
    });
    window.location.href = '#search_result';
    new $.fn.dataTable.FixedColumns( table );
}

//fonction pour prioriser un traitement flux
function prioriser_traitement(id_flux)
{
    console.log(arrayflux);
    swal({
        title: "Prioriser?",
        text: "Le traitement du flux "+id_flux+"?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Prioriser",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + '/admin/support_push/prioriser/'+id_flux, function(rep) {
            if (rep == 0) {
                //showNotification('alert-success', '<i class="material-icons">done</i> Modifications enregistr&eacute;es.', 'top', 'right', null, null);
                swal({
                    title: "R\351ussi!",
                    text: "Modifications enregistr\351es.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
               
            } else {
                //showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Modification &eacute;chou&eacute;e.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

//functin pour retraiter un pli 
function retraitement(id_flux)
{
    swal({
        title: "Retraitement",
        text: "Voulez vous vraiment retraiter le flux #"+id_flux+"?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Retraiter",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {
        $.post(chemin_site + '/admin/support_push/retraitement/'+id_flux, function(rep) {
            if (rep == 0) {
                //showNotification('alert-success', '<i class="material-icons">done</i> Modifications enregistr&eacute;es.', 'top', 'right', null, null);
                swal({
                    title: "R\351ussi!",
                    text: "Modifications enregistr\351es.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: true
                });
                //vider array id_flux pour eviter les redondances
                arrayflux =[];
               
            } else {
                //showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Modification &eacute;chou&eacute;e.', 'top', 'right', null, null);
                swal("Serveur", "Erreur! " + rep, "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        }).always(function() {
            load_table();
        });
    });
}

var arrayflux =[];
function getChecked(id)
{
    if($('#ig_checkbox_'+id).prop('checked')){
        arrayflux.push(id);
    } else {
        arrayflux.splice($.inArray(id, arrayflux), 1);
        //arrayflux.pop(id);
    }
    console.log(arrayflux)
}     

//fontion reatraiter tous les flux selectionnés 
function RetraiterTous()
{
    console.log('tettst');
    console.log(arrayflux.length );
    if(arrayflux.length  === 0)
    {
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Merci de cocher au moins un flux!.', 'top', 'right', null, null);
        return false;
    }
    swal({
        title: "Retraitement",
        text: "Voulez vous vraiment retraiter tous les fluxs cochés?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Retraiter",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {

        $.ajax({
            "url":chemin_site + '/admin/support_push/retraiterTous',
            "type": "POST",
            "data"    : {info:arrayflux},
            "success": function(res, status, xhr) {
               if(res == 0 ){

                   swal({
                       title: "R\351ussi!",
                       text: "Modifications enregistr\351es.",
                       type: "success",
                       timer: 2000,
                       showConfirmButton: true
                   });
                //vider array id_flux pour eviter les redondances
                   arrayflux =[];
                   load_table();
               } else {
                    swal("Serveur", "Erreur serveur!", "error");
                    
               }
            },
            "error": function(res) {
                swal("Serveur", "Erreur serveur!", "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
     });
}

//unlock pli
function unlock($idpli, $flag){
    swal({
        title: "Déverrouillage",
        text: "Voulez vous vraiment déverrouiller ce flux?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Déverrouiller",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {

        $.ajax({
            "url":chemin_site + '/admin/support_push/unlock/'+$idpli+'/'+$flag,
            "type": "POST",
            "success": function(res, status, xhr) {
                console.log(res);
               if(res == 1 ){

                   swal({
                       title: "R\351ussi!",
                       text: "Modifications enregistr\351es.",
                       type: "success",
                       timer: 2000,
                       showConfirmButton: true
                   });
                //vider array id_flux pour eviter les redondances
                   arrayflux =[];
                   load_table();
               } else {
                    swal("Serveur", "Erreur serveur!", "error");
                    
               }
            },
            "error": function(res) {
                swal("Serveur", "Erreur serveur!", "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
     });
}

//initialiser flux
function initFlux($idpli, $flag){
    swal({
        title: "Initialisation",
        text: "Voulez vous vraiment initialiser ce flux?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Initialiser",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {

        $.ajax({
            "url":chemin_site + '/admin/support_push/init/'+$idpli+'/'+$flag,
            "type": "POST",
            "success": function(res, status, xhr) {
                console.log(res);
               if(res == 1 ){

                   swal({
                       title: "R\351ussi!",
                       text: "Modifications enregistr\351es.",
                       type: "success",
                       timer: 2000,
                       showConfirmButton: true
                   });
                //vider array id_flux pour eviter les redondances
                   arrayflux =[];
                   load_table();
               } else {
                    swal("Serveur", "Erreur serveur!", "error");
                    
               }
            },
            "error": function(res) {
                swal("Serveur", "Erreur serveur!", "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
     });
}

//visu abo
function visu_Abonnement(id_flux)
{
 
    $('.theme-indigo').css({
        'overflow':'scroll','padding-right': '17px'
    });
    $('#abo_body').html('');
    $.get(chemin_site + '/visualisation/visu_push/getAbonnement/'+id_flux, {}, function(view) {
        $('#abon_head').text('');
        $('#abo_body').html(view);
        $('#abon_head').text('Abonnement(s) flux #'+id_flux);
    });
    $('#mdl_display_abo').modal('show');
}

//visu anomalie
function visu_anomalie(id_flux){
    $('.theme-indigo').css({
        'overflow':'scroll','padding-right': '0px'
    });
    
    $('#ano_body').html('');
    $.get(chemin_site + '/visualisation/visu_push/getAnomalie/'+id_flux, {}, function(view) {
        
        $('#ano_body').html(view);
        $('.modal-title').text('Anomalie Flux#'+id_flux);
        
    });
    $('#mdl_display_ano').modal('show');
   

}

//visu historique flux
function visu_histo(id_flux)
{
    $('.theme-indigo').css({
        'overflow':'scroll','padding-right': '0px'
    });
    $('#histo_body').html('');
    $.get(chemin_site + '/admin/support_push/histoFLux/'+id_flux, {}, function(res) {
        
        $('#histo_head').text('');
        $('#histo_body').html(res);
        $('#histo_head').text('Historique flux #'+id_flux);
        
    });
    $('#mdl_display_histo').modal('show');
   
   
}
  
