$(function() {
    dttb_visu = null;
    fichier_up_ke = [];
    init_compos();
    //load_table();overflow
    $('#mdl_display_abo').on('shown.bs.modal', function() {
       
        if($('#visu_abo').length >0 ) {
            $('#visu_abo').dataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'excel'
                ]
            });
        }
        if($('#visu_jointes').length >0 ) {
            $('#visu_jointes').dataTable();
        }
    });
});
    

//initialisation datepicker
function init_compos() {
    $('.dt_field').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY',
        lang: 'fr',
        clearButton: true,
        clearText: 'Vider',
        cancelText: 'Annuler',
        weekStart: 1,
        switchOnClick: true,
        time: false
    }).on('change', function(e, date) {
        if ($(this).val() == '') {
            $(this).closest('.form-line').removeClass('focused');
        } else {
            $(this).closest('.form-line').addClass('focused');
        }
    });
}

function load_data(){
    // if($('#dt_act_1').val() == '' || $('#dt_act_2').val() == '') {
    //     showNotification('alert-danger', '<i class="material-icons">error</i>Merci de renseigner les champs dates.', 'bottom', 'right', null, null);
    //     return false;
    // }
    //$("#preloader_visu").removeClass("cl_visu");

    
  
    if(dttb_visu === null) {
        dttb_visu = $('#visu_push').DataTable({
            'bprocessing': true,
            'bServerSide': true,
            'bScrollCollapse': true,
            'fixedColumns': {
                'leftColumns': 1
            },
            'aLengthMenu': [
                [5, 10, 25, 50, 100],
                [5, 10, 25, 50, 100]
            ],
            'pageLength': 5,
            'bDestroy': true,
            //'bStateSave': true,
            'bFilter': false,
            'sScrollX': true,
            'oLanguage': {
                'sLengthMenu': '_MENU_ lignes par page',
                'sZeroRecords': "Aucun flux",
                'sEmptyTable': "Aucun flux",
                'sInfo': "de _START_ \340 _END_ sur _TOTAL_ flux",
                'sInfoFiltered': '(Total : _MAX_ plis)',
                'sLoadingRecords': 'Chargement en cours...',
                'sProcessing': 'Traitement en cours...',
                'oPaginate': {
                    'sFirst': 'PREMIERE',
                    'sPrevious': 'PRECEDENTE',
                    'sNext': 'SUIVANTE',
                    'sLast': 'DERNIERE'
                },
                'sSearch': 'Rechercher: '
            },
            'ajax': {
                'url': chemin_site + '/visualisation/visu_push/getData',
                'sServerMethod': 'POST',
                'data': function(d) {
                    d.from_flux = $("#from_flux").val();
                    d.from_name_flux = $("#from_name_flux").val();
                    d.objet_flux = $("#objet_flux").val();
                    d.from_abonne = $("#from_abonne").val();
                    d.from_payeur = $("#from_payeur").val();
                    d.source = $("#sel_source").val();
                    d.societe = $("#sel_societe").val();
                    d.typologie = $("#sel_typologie").val();
                    d.statut = $("#sel_statut").val();
                    d.fichier = $("#nom_fichier").val();
                    // d.op_saisi = $("#sel_op_saisie").val();
                    // d.op_typage = $("#sel_op_typage").val();
                    d.date_ttr1 = $("#dt_act_1").val();
                    d.date_ttr2 = $("#dt_act_2").val();
                    d.id_pli = $("#txt_recherche_id_pli").val();
                    d.dossier = $("#sel_dossier").val();
                    d.etat = $("#sel_etat").val();
                }
            },
            /*"order": [
                [0, "desc"]
            ],*/
            'fnDrawCallback': function(oSettings) {
                $('[data-toggle="tooltip"]').tooltip();
                set_action();
                $("#preloader_visu").hide();
            },
            aoColumnDefs: [
                { 'bSortable': false, 'aTargets': [2,16,17,18] },
                {
                    'aTargets': [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18],
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                        $(nTd).addClass('align-center');
                    }
                }
               // { "width": "15%", "aTargets": [13,14,15] }
            ],
            
            // aoColumnDefs: [{
            //     'aTargets': [3, 4, 5, 12, 13],
            //     "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
            //         $(nTd).addClass('align-center');
            //     }
            // }, { 'bSortable': false, 'aTargets': [12, 13] }],
            // 'columns': [
            //     { 'data': 'id_flux', 'targets': [0] },
            //     { 'data': 'source', 'targets': [1] },
                
            // ]
        });
    } else {
        dttb_visu.ajax.reload();
    }
    window.location.href = '#search_result';
    //new $.fn.dataTable.FixedColumns( table );
}

//visualisation abonnement
function visu_Abonnement(id_flux,societe,fichier,dern_traitement)
{
    
    $('.theme-indigo').css({
        'overflow':'scroll','padding-right': '17px'
    });

    $('#abo_body').html('');
    $.get(chemin_site + '/visualisation/visu_push/getAbonnement/'+id_flux, {societe:societe,fichier:fichier,dern_traitement:dern_traitement}, function(view) {
        
        $('#abo_head').text('');
        $('#abo_body').html(view);
        $('#abo_head').text('Abonnement(s) flux #'+id_flux);
        
    });
    $('#mdl_display_abo').modal('show');
   
   
}

function visual_jointes(id_flux)
{
    $('.theme-indigo').css({
        'overflow':'scroll','padding-right': '0px'
    });
    
    $('#abo_body').html('');
    $.get(chemin_site + '/visualisation/visu_push/getJointes/'+id_flux, {}, function(view) {
        
        $('#abo_head').text('');
        $('#abo_body').html(view);
        $('#abo_head').text('Pièce(s) jointe(s) flux#'+id_flux);
        
    });
    $('#mdl_display_abo').modal('show');
   
}

function visu_anomalie(id_flux){
    $('.theme-indigo').css({
        'overflow':'auto','padding-right': '0px'
    });
    
    $('#ano_body').html('');
    $.get(chemin_site + '/visualisation/visu_push/getAnomalie/'+id_flux, {}, function(view) {
        
        $('#ano_body').html(view);
        $('.modal-title').text('Anomalie Flux#'+id_flux);
        
    });
    $('#mdl_display_ano').modal('show');
   

}

function edit_consigne_ke(id_flux) {
    $.get(chemin_site + '/visualisation/visu_push/input_ke/' + id_flux, {}, function(reponse) {
        $('#mdl_input_ke > div.modal-dialog > div.modal-content').html(reponse);
        init_dropzone_ke();
        autosize($('textarea.auto-growth'));
        $('[data-toggle="tooltip"]').tooltip();
        $('#mdl_input_ke').modal('show');
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur serveur.', 'top', 'center', null, null);
    });
}

function init_dropzone_ke(){
    $("#ke_file_insert").dropzone({
        addRemoveLinks: true
        ,dictRemoveFile: 'Retirer'
        ,init:function(){
            fichier_up_ke = [];
            le_dropzone = this;
            this.on('success', function(file, resp){
                try {
                    var rep = $.parseJSON(resp);
                } catch (error) {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur upload.', 'bottom', 'right', null, null);
                    this.removeFile(file);
                    return false;
                }
                if(rep.code == 1){
                    var fichier = {
                        uid: file.upload.uuid
                        ,link: rep.link
                        ,orig_name: rep.name
                        ,ext: rep.ext
                        ,suppr: false
                    };
                    fichier_up_ke.push(fichier);
                }else{
                    showNotification('alert-danger', '<i class="material-icons">error</i>Erreur upload: '+rep.er, 'bottom', 'right', null, null);
                    this.removeFile(file);
                    return false;
                }
            });
        }
        ,removedfile:function(file){
            $.each(fichier_up_ke, function(i, fichier){
                if(file.upload.uuid == fichier.uid){
                    fichier.suppr = true;
                }
            });
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        }
    });
}

function send_data_ke(id_flux) {
    var deleted_existed_files = [];
    $.each($('.name_existed_file_ke.deleted'), function(){
        deleted_existed_files.push($(this).attr('my_oid'));
    });
    var data = {
        id_flux: id_flux
        ,files: JSON.stringify(fichier_up_ke)
        ,comms: $.trim($('#comms_ke').val())
        ,deleted_oid: JSON.stringify(deleted_existed_files)
    }
    var verifie = true;
    if($.trim(data.comms) == ''){
        verifie = false;
        swal("Alerte", "Merci d'éditer la consigne.", "warning");
    }
    if(verifie){
        swal({
            title: 'Confirmation',
            text: 'Enregistrer?',
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Valider",
            cancelButtonText: "Annuler",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function() {
            $.post(chemin_site + '/visualisation/visu_push/set_consigne_ke', data, function(rep) {
                if (rep == 1) {
                    $('#mdl_input_ke').modal('hide');
                    swal({
                        title: "R\351ussi!",
                        text: "Consigne enregistr\351e.",
                        type: "success",
                        showConfirmButton: true
                    });
                } else {
                    swal("Erreur!", rep, "error");
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
                swal("Erreur", "Erreur serveur!", "error");
            }).always(function() {
                load_table();
            });
        });
    }
}

function del_old_file_ke(id_existed_file_ke, me){
    if($('#'+id_existed_file_ke).hasClass('deleted')){
        $('#'+id_existed_file_ke).removeClass('deleted');
        $(me).html('delete');
        $(me).attr('title', 'Supprimer');
    }else{
        $('#'+id_existed_file_ke).addClass('deleted');
        $(me).html('delete_forever');
        $(me).attr('title', 'Restaurer');
    }
}

$(document).on("keypress", function(e){
    if(e.which == 13){
        load_data();
    }
});
