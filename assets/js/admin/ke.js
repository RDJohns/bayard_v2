
$(function() {
    init_compos();
    //load_table();
});

//initialisation datepicker
function init_compos() {
    $('.dt_field').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY',
        lang: 'fr',
        clearButton: true,
        clearText: 'Vider',
        cancelText: 'Annuler',
        weekStart: 1,
        switchOnClick: true,
        time: false
    }).on('change', function(e, date) {
        if ($(this).val() == '') {
            $(this).closest('.form-line').removeClass('focused');
        } else {
            $(this).closest('.form-line').addClass('focused');
        }
    });
}

function load_table() {
    arrayflux =[];
    $('#ke').DataTable({
        'bprocessing': true,
        'bServerSide': true,
        'bScrollCollapse': true,
        'aLengthMenu': [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        'pageLength': 5,
        'bDestroy': true,
        'bFilter': false,
        'sScrollX': true,
        'oLanguage': {
            'sLengthMenu': '_MENU_ lignes par page',
            'sZeroRecords': "Aucun pli",
            'sEmptyTable': "Aucun pli",
            'sInfo': "de _START_ \340 _END_ sur _TOTAL_ flux",
            'sInfoFiltered': '(Total : _MAX_ plis)',
            'sLoadingRecords': 'Chargement en cours...',
            'sProcessing': 'Traitement en cours...',
            'oPaginate': {
                'sFirst': 'PREMIERE',
                'sPrevious': 'PRECEDENTE',
                'sNext': 'SUIVANTE',
                'sLast': 'DERNIERE'
            },
            'sSearch': 'Rechercher: '
        },
        'ajax': {
            'url': chemin_site + '/admin/gestion_ke/getPlis',
            'sServerMethod': 'POST',
            'data': function(d) {
                d.id_pli = $("#txt_recherche_id_pli").val();
                d.societe = $("#sel_societe").val();
                d.typologie = $("#sel_typologie").val();
                d.date_c1 = $("#dt_c_1").val();
                d.date_c2 = $("#dt_c_2").val();
                d.date_t1 = $("#dt_t_1").val();
                d.date_t2 = $("#dt_t_2").val();
               
            }
        },
        'fnDrawCallback': function(oSettings) {
            $('[data-toggle="tooltip"]').tooltip();
        }
        
    });
    window.location.href = '#search_result';
}


var arrayflux =[];
function getChecked(id)
{
    if($('#ig_checkbox_'+id).prop('checked')){
        arrayflux.push(id);
    } else {
        arrayflux.splice($.inArray(id, arrayflux), 1);
        //arrayflux.pop(id);
    }
    console.log(arrayflux)
}     

//fontion reatraiter tous les flux selectionnés 
function PrioriserTous()
{
    
    if(arrayflux.length  === 0)
    {
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Merci de cocher au moins un pli!.', 'top', 'right', null, null);
        return false;
    }
    swal({
        title: "Priorisation",
        text: "Voulez vous vraiment prioriser tous les plis cochés?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Prioriser",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function() {

        $.ajax({
            "url":chemin_site + '/admin/gestion_ke/prioriserTous',
            "type": "POST",
            "data"    : {info:arrayflux},
            "success": function(res, status, xhr) {
               if(res == 0 ){

                   swal({
                       title: "R\351ussi!",
                       text: "Modifications enregistr\351es.",
                       type: "success",
                       timer: 2000,
                       showConfirmButton: true
                   });
                //vider array id_pli pour eviter les redondances
                   arrayflux =[];
                   load_table();
               } else {
                    swal("Serveur", "Erreur serveur!", "error");
                    
               }
            },
            "error": function(res) {
                swal("Serveur", "Erreur serveur!", "error");
            }
        }).fail(function(){
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
        });
     });
}


  
