$(function() {
    init_compos();
    //load_data_fautes();
});

function init_compos() {
    $('.dt_field').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY',
        lang: 'fr',
        clearButton: true,
        clearText: 'Vider',
        cancelText: 'Annuler',
        weekStart: 1,
        switchOnClick: true,
        time: false
    }).on('change', function(e, date) {
        if ($(this).val() == '') {
            $(this).closest('.form-line').removeClass('focused');
        } else {
            $(this).closest('.form-line').addClass('focused');
        }
    });
}

function load_data_fautes() {
    $('#zone_data_faute').html($('#html_loader').html());
    var data = {
        dt_ctrl_1: $.trim($("#dt_ctrl_1").val())
        ,dt_ctrl_2: $.trim($("#dt_ctrl_2").val())
    };
    $.post(chemin_site + '/admin/qualite/data_faute', data, function(reponse) {
        $('#zone_data_faute').html(reponse);
    }).fail(function(){
        $('#zone_data_faute').html('');
    });
}
function fautes_export_xl(taux) {
    var data = {
        dt_ctrl_1: $.trim($("#dt_ctrl_1").val())
        ,dt_ctrl_2: $.trim($("#dt_ctrl_2").val())
        ,taux: taux
    };
    $.post(chemin_site + '/admin/qualite/export_faute_xl', data, function (from_serv) {
        try {
            var reponse = $.parseJSON(from_serv);
        } catch (err) {
            showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
            swal("Serveur", "Erreur serveur!", "error");
            return '';
        }
        window.location.href = chemin_site + '/admin/qualite/get_xl/'+reponse.name+'/'+reponse.token;
    }).fail(function(){
        showNotification('alert-danger', '<i class="material-icons">error</i>Erreur: Opération &eacute;chou&eacute;e!.', 'top', 'right', null, null);
        swal("Serveur", "Erreur serveur!", "error");
    });
}
