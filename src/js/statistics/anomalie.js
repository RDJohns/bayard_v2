(function ($) {
 "use strict";
 
	
	/*$("#date_debut").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	
	 $("#date_fin").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	*/
	
	  $("#tab1").show();
	  $("#tab2").show();
	  $("#tab3").show();
	  $("#tab4").show();
	  $("#tab5").show();
	 
	

})(jQuery); 
function rech(){
    /*if(window.event.keyCode == 13){
        recherche_doc();
    }*/
}
//function funct_toogle(){
$("#tab_stat").click(function(){
		  $("#tab1").show();
		  $("#tab2").show();
		  $("#tab3").show();
		  $("#tab4").show();
		  $("#tab5").show();
		  $("#tab11").hide();
		});

$("#tab_plis").click(function(){
		  
		  $("#tab0").hide();
		  $("#tab1").hide();
		  $("#tab2").hide();
		  $("#tab3").hide();
		  $("#tab4").hide();
		  $("#tab5").show();
		  $("#tab11").hide();
});
$("#tab_tout").click(function(){
		  
		  $("#tab1").show();
		  $("#tab2").show();
		  $("#tab3").show();
		  $("#tab4").show();
		  $("#tab5").show();
		  $("#tab11").hide();
});
//}
function get_reception_traitement(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 $("#tab0").show();
}
function get_plis_avec_moyen_de_paiement_j(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 
		
	 $("#tab11").show();
	// alert(date_debut); 
}
function get_plis_avec_moyen_de_paiement_s(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
		
}
function quitter_tab11(){
	 $("#tab11").hide();
}
function quitter_tab0(){
	 $("#tab0").hide();
}

function charger_plis(){
	
    var numcommande = $("#numcommande").val();
    var numclient   = $("#numclient").val();
    var nom_client  = $("#nom_client").val();
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if(numcommande == '' && numclient == '' && nom_client == ''){
		
        if(date_debut == '' && date_fin == '' ){
			$("#error_message").show();
			setTimeout(function(){
				$("#error_message").fadeOut(1000);
			}, 2000);
			
			return false;

		}

    }
	
	
    $.post(url_site+'/statistics/statistics/get_stat_pli', {numcommande:numcommande, numclient:numclient, nom_client:nom_client, date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
            $("#result-rsrch").append(from_server).fadeIn(400);
			
			$('.counter').counterUp({
				delay: 10,
				time: 1000
			});	
				
				
				$('.js-basic-example').DataTable({
					responsive: true
				});

				//Exportable table
				$('.js-exportable').DataTable({
					dom: 'Bfrtip',
					responsive: true,
					buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print'
					]
				});
				
				
				$(function () {
					getMorris('donut', 'donut_chart');
					getMorrisDoc('donut', 'donut_document_chart');
					getMorrisPlisKO('donut', 'donut_plis_ko_chart');
					getMorrisPlisRestant('donut', 'donut_plis_restant_chart');
				});
				
				 $("#tab11").hide();
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun pli trouvé.'
            });
        }
    });

}

function repartition_par_doc(date_debut,date_fin){
	
	$("#donut_chart").html("tableau");
	 /*$.post(url_site+'/statistics/statistics/get_stat_pli', {numcommande:numcommande, numclient:numclient, nom_client:nom_client, date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){
            $("#result-rsrch").append(from_server).fadeIn(400);
		}*/
}
function voir_graph(date_debut,date_fin){
	
	$("#donut_chart").html("");
	getMorris('donut', 'donut_chart');
	 /*$.post(url_site+'/statistics/statistics/get_stat_pli', {numcommande:numcommande, numclient:numclient, nom_client:nom_client, date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){
            $("#result-rsrch").append(from_server).fadeIn(400);
		}*/
}

function getMorris(type, element) {
	
	var datas 		= $("#graph_donut").val();
	var elm 		= datas.split(',colors:');
	
	var data_colors = elm[1];
	var element     = elm[0].split('},{');
	var element_clr = elm[1].split("','");
	
	console.log(data_colors);
	
	var i 			= 0;
	var tb_data  	= new Array();
	var tb_color 	= new Array();
	var label   	= "";
	var valeur  	= "";
	var couleur 	= "";
	
	for(i ; i<element.length ; i++){
		
		var elm_donut 	= element[i].replace("[{","" );
		elm_donut 	  	= elm_donut.replace("}]", "");
		
		
		var data_donut  = elm_donut.split(",");
		var data_label  = data_donut[0];
		var data_value  = data_donut[1];
		var data_lbl    = data_label.split("'");
		var data_vl     = data_value.split(":");
		
		label           = data_lbl[1];
		valeur          = data_vl[1];		
		
		tb_data.push({'label':label,'value':valeur});		
		
	}
	
	
	for(var j = 0; j<element_clr.length ; j++){
		
		var elm_color  = element_clr[j].replace("['","" );
		elm_color      = elm_color.replace("']","" );
		tb_color.push(elm_color);
	}
	
	if(datas != ''){	
			Morris.Donut({
				element: 'donut_chart',
				data:  tb_data ,
				colors :  tb_color,
				formatter: function (y) { return y + '%'}
			});	
	}
	
}
function getMorrisDoc(type, element) {
	
	var datas 		= $("#graph_donut_doc").val();
	var elm 		= datas.split(',colors:');
	
	var data_colors = elm[1];
	var element     = elm[0].split('},{');
	var element_clr = elm[1].split("','");
	
	console.log(data_colors);
	
	var i 			= 0;
	var tb_data  	= new Array();
	var tb_color 	= new Array();
	var label   	= "";
	var valeur  	= "";
	var couleur 	= "";
	
	for(i ; i<element.length ; i++){
		
		var elm_donut 	= element[i].replace("[{","" );
		elm_donut 	  	= elm_donut.replace("}]", "");
		
		
		var data_donut  = elm_donut.split(",");
		var data_label  = data_donut[0];
		var data_value  = data_donut[1];
		var data_lbl    = data_label.split("'");
		var data_vl     = data_value.split(":");
		
		label           = data_lbl[1];
		valeur          = data_vl[1];		
		
		tb_data.push({'label':label,'value':valeur});		
		
	}
	
	
	for(var j = 0; j<element_clr.length ; j++){
		
		var elm_color  = element_clr[j].replace("['","" );
		elm_color      = elm_color.replace("']","" );
		tb_color.push(elm_color);
	}
	
	if(datas != ''){	
			Morris.Donut({
				element: 'donut_document_chart',
				data:  tb_data ,
				colors :  tb_color,
				formatter: function (y) { return y + '%'}
			});	
	}
	
}

function getMorrisPlisKO(type, element) {
	
	var datas 		= $("#graph_donut_plis_ko").val();
	var elm 		= datas.split(',colors:');
	
	var data_colors = elm[1];
	var element     = elm[0].split('},{');
	var element_clr = elm[1].split("','");
	
	console.log(data_colors);
	
	var i 			= 0;
	var tb_data  	= new Array();
	var tb_color 	= new Array();
	var label   	= "";
	var valeur  	= "";
	var couleur 	= "";
	
	for(i ; i<element.length ; i++){
		
		var elm_donut 	= element[i].replace("[{","" );
		elm_donut 	  	= elm_donut.replace("}]", "");
		
		
		var data_donut  = elm_donut.split(",");
		var data_label  = data_donut[0];
		var data_value  = data_donut[1];
		var data_lbl    = data_label.split("'");
		var data_vl     = data_value.split(":");
		
		label           = data_lbl[1];
		valeur          = data_vl[1];		
		
		tb_data.push({'label':label,'value':valeur});		
		
	}
	
	
	for(var j = 0; j<element_clr.length ; j++){
		
		var elm_color  = element_clr[j].replace("['","" );
		elm_color      = elm_color.replace("']","" );
		tb_color.push(elm_color);
	}
	
	if(datas != ''){	
			Morris.Donut({
				element: 'donut_plis_ko_chart',
				data:  tb_data ,
				colors :  tb_color,
				formatter: function (y) { return y + '%'}
			});	
	}
	
}


function getMorrisPlisRestant(type, element) {
	
	var datas 		= $("#graph_donut_plis_restant").val();
	var elm 		= datas.split(',colors:');
	
	var data_colors = elm[1];
	var element     = elm[0].split('},{');
	var element_clr = elm[1].split("','");
	
	console.log("--- restant ---"+datas);
	
	var i 			= 0;
	var tb_data  	= new Array();
	var tb_color 	= new Array();
	var label   	= "";
	var valeur  	= "";
	var couleur 	= "";
	
	for(i ; i<element.length ; i++){
		
		var elm_donut 	= element[i].replace("[{","" );
		elm_donut 	  	= elm_donut.replace("}]", "");
		
		
		var data_donut  = elm_donut.split(",");
		var data_label  = data_donut[0];
		var data_value  = data_donut[1];
		var data_lbl    = data_label.split("'");
		var data_vl     = data_value.split(":");
		
		label           = data_lbl[1];
		valeur          = data_vl[1];		
		
		tb_data.push({'label':label,'value':valeur});		
		
	}
	
	
	for(var j = 0; j<element_clr.length ; j++){
		
		var elm_color  = element_clr[j].replace("['","" );
		elm_color      = elm_color.replace("']","" );
		tb_color.push(elm_color);
	}
	
	if(datas != ''){	
			Morris.Donut({
				element: 'donut_plis_restant_chart',
				data:  tb_data ,
				colors :  tb_color,
				formatter: function (y) { return y + '%'}
			});	
	}
	
}
function chercher_plis_stat(date_debut,date_fin){
		 $.post(url_site+'/statistics/statistics/get_pli', {date_debut : date_debut, date_fin : date_fin, flag : 1}, function(from_server){
            $("#detail_stat").append(from_server).fadeIn(400);
		
    });
}
function afficher_pli(id_pli, id_doc) {

  /*  $("#info-pli").html('').fadeOut(300);

    $.post(url_site+'/visualisation/visualisation/affiche_pli', {id_pli:id_pli}, function(from_server){
        if(from_server != 0){
            $("#info-pli").append(from_server).fadeIn(400);
        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun pli trouvé.'
            });
        }
    });

    afficher_doc(id_doc);
*/
}

function afficher_doc(id_doc) {

   /* $("#result-rsrch").html('').fadeOut(300);

    $.post(url_site+'/visualisation/visualisation/affiche_doc', {id_doc:id_doc}, function(from_server){
        if(from_server != 0){
            $("#result-rsrch").append(from_server).fadeIn(400);
            $("#panel-info").slideDown("slow");
        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun pli trouvé.'
            });
        }
    });
*/
}

