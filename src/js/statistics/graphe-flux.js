var global = document.getElementById('chart-global').getContext('2d');
var gko = document.getElementById('chart-total-Ko').getContext('2d');
var pliClos = document.getElementById('chart-plis-cloture').getContext('2d');
var typo = document.getElementById('typo').value;
var elm  = typo.split(',');

if(labels.length>0) {
    //reception global
    var data = {
        labels: labels,
        datasets: [
            {
                label: "Cloturé",
                // backgroundColor: "#DA70AE",
                backgroundColor: "#8BC34A",
                data: cloture,
            },
            {
                label: "En cours",
                // backgroundColor: "#00CA94",
                backgroundColor: "#FF9800",
                data: encours,
            }
            ,
            {
                label: "Non traité",
                // backgroundColor: "#4FC0E8",
                backgroundColor: "#00BCD4",
                data: nontraite,
            }
            ,{
                label: "Anomalie",
                // backgroundColor: "#FB6E53",
                backgroundColor: "#E91E63",
                data: anomalie,
            }
        ]
    };
    // reception global KO
    var dataKO = {
        labels: labels,
        datasets: [
            {
                label: "Traité",
                backgroundColor: "#A3C4F7",
                data: traite,
            },
            {
                label: "Hors Périmetre",
                backgroundColor: "#5CD0DC",
                data: hp,
            }
            ,
            {
                label: "Rejeté",
                backgroundColor: "#00CA94",
                data: rejete,
            }

        ]
    };

    // plis cloturés
    var dataPlisClotures = {
        labels: labels,
        datasets: [
            {
                label: elm[0],
                backgroundColor: "#00EAFF",
                data: arrInfo_1,
            },
            {
                label: elm[1],
                backgroundColor: "#AA00FF",
                data: arrInfo_2,
            }
            ,
            {
                label: elm[2],
                backgroundColor: "#FF7F00",
                data: arrInfo_3,
            },
            {
                label: elm[3],
                backgroundColor: "#0095FF",
                data: arrInfo_4,
            }
            ,
            {
                label: elm[4],
                backgroundColor: "#E9573E",
                data: arrInfo_5,
            }
            ,
            {
                label: elm[5],
                backgroundColor: "#FF00AA",
                data: arrInfo_6,
            },{
                label: elm[6],
                backgroundColor: "#FFD400",
                data: arrInfo_7,
            },
            {
                label: elm[7],
                backgroundColor: "#6AFF00",
                data: arrInfo_8,
            }
            ,
            {
                label: elm[8],
                backgroundColor: "#bcf60c",
                data: arrInfo_9,
            },
            {
                label: elm[9],
                backgroundColor: "#EDB9B9",
                data: arrInfo_10,
            }
            ,

            {
                label: elm[10],
                backgroundColor: "#8F2323",
                data: arrInfo_11,
            }
            ,
            {
                label: elm[11],
                backgroundColor: "#00EAFF",
                data: arrInfo_12,
            },
            {
                label: elm[12],
                backgroundColor: "#AA00FF",
                data: arrInfo_13,
            }
            ,
            {
                label: elm[13],
                backgroundColor: "#3BAEDA",
                data: arrInfo_14,
            },
            {
                label: elm[14],
                backgroundColor: "#0095FF",
                data: arrInfo_15,
            }
            ,
            {
                label: elm[15],
                backgroundColor: "#f58231",
                data: arrInfo_16,
            }
            ,
            {
                label: elm[16],
                backgroundColor: "#EE5567",
                data: arrInfo_17,
            },
            {
                label: elm[17],
                backgroundColor: "#43CAA9",
                data: arrInfo_18,
            },
            {
                label: elm[18],
                backgroundColor: "#EC87BF",
                data: arrInfo_19,
            }
            ,
            {
                label: elm[19],
                backgroundColor: "#4FC0E8",
                data: arrInfo_20,
            },
            {
                label: elm[20],
                backgroundColor: "#FB6E53",
                data: arrInfo_21,
            }
        ]
    };

    console.log()
    //graphe reception global
    var chart = new Chart(global, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: data,
        // Configuration options go here
        options: {}
    });



    var chartKo = new Chart(gko, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: dataKO,
        // Configuration options go here
        options: {}
    });

    var chartPlisCoture = new Chart(pliClos, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: dataPlisClotures,
        // Configuration options go here
        options: {}
    });

}