(function ($) {
    "use strict";


    $("#date_debut").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
        ,format: 'dd/mm/yyyy'
    });

    $("#date_fin").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
        ,format: 'dd/mm/yyyy'
    });
    $("#mois_debut").datepicker({
        format: "mm/yyyy",
        viewMode: "months",
        minViewMode: "months"
    });
    $("#mois_fin").datepicker({
        format: "mm/yyyy",
        viewMode: "months",
        minViewMode: "months"
    });

    $("#tab1").show();
    $("#tab2").show();
    $("#tab3").show();
    $("#tab4").show();
    $("#tab5").show();

    $("#preloader_traitement").hide();
    $("#preloader_solde").hide();
    $("#preloader_traitement_dtl").hide();
    $("#message_alert").hide();
	$("#preloader").hide(); 
    load_societe();
    load_source();
    load_data_att();

    //setInterval(function(){
    //load_correction();
    //load_validation();
    //}, 30000);

})(jQuery);
function rech(){
    /*if(window.event.keyCode == 13){
        recherche_doc();
    }*/
}

function deconnexion(){
    swal({
        title: "CONFIRMATION",
        text: "Deconnexion",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: true
    }, function () {
        window.location.href = url_site +'/login';
    });
}

//function funct_toogle(){
$("#tab_stat").click(function(){
    $("#tab1").show();
    $("#tab2").show();
    $("#tab3").show();
    $("#tab4").show();
    $("#tab5").show();
    $("#tab11").hide();
    $("#tab12").hide();
    $("#tab13").hide();
});

$("#tab_plis").click(function(){

    $("#tab0").hide();
    $("#tab01").hide();
    $("#tab02").hide();
    $("#tab03").hide();
    $("#tab1").hide();
    $("#tab2").hide();
    $("#tab3").hide();
    $("#tab4").hide();
    $("#tab5").show();
    $("#tab11").hide();
    $("#tab12").hide();
    $("#tab13").hide();
});
$("#tab_tout").click(function(){

    $("#tab1").show();
    $("#tab2").show();
    $("#tab3").show();
    $("#tab4").show();
    $("#tab5").show();
    $("#tab11").hide();
    $("#tab12").hide();
    $("#tab13").hide();
    $("#tab0").hide();
    $("#tab01").hide();
    $("#tab02").hide();
    $("#tab03").hide();
});
//}
function get_reception_traitement(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $("#tab_synthese_reception").html('');
    if(date_debut == '' && date_fin == '' ){
        $("#error_message").show();
        setTimeout(function(){
            $("#error_message").fadeOut(1000);
        }, 2000);

        return false;

    }

    var url = url_site.replace("index.php","")+'src/img/loader.gif';
    $("#tab_synthese_reception").html('<img src="'+url+'" ><br /><br /></img>');
    $.post(url_site+'/statistics/statistics/get_reception_traitement', {date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#tab_synthese_reception").html("");
            $("#tab_synthese_reception").append(from_server).fadeIn(400);

            //Exportable table
            $('.js-export-table-synthese').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'csv', 'excel'
                ]
            });
            $("#tab0").show();
            $("#tab01").show();
            $("#tab02").show();
            $("#tab03").show();

        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });


}
function get_plis_avec_moyen_de_paiement_j(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();


    $("#tab11").show();
    // alert(date_debut);
}

function get_plis_avec_moyen_de_paiement_s(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
    $("#tab12").show();

}
function get_plis_avec_moyen_de_paiement_m(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
    $("#tab13").show();
}

function get_detail_mensuel(daty){

    $.post(url_site+'/statistics/statistics/recep_detail_mensuel', {date_debut : daty}, function(from_server){
        if(from_server != 0){

            $("#tab_detail_mensuel").html("");
            $("#tab_detail_mensuel").append(from_server).fadeIn(400);
            $("#tab_detail_mensuel").show();

        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}

function get_plis_cloture_j(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $.post(url_site+'/statistics/statistics/get_pli_cloture', {date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#tab21").html("");
            $("#tab21").append(from_server).fadeIn(400);


        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}
function get_plis_cloture_s(){

    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $.post(url_site+'/statistics/statistics/get_pli_cloture_hebdo', {date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#tab23").html("");
            $("#tab23").append(from_server).fadeIn(400);


        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
}
function get_plis_cloture_m(){

    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
    $.post(url_site+'/statistics/statistics/get_pli_cloture_mensuel', {date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#tab24").html("");
            $("#tab24").append(from_server).fadeIn(400);



        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}
function get_plis_cloture_produit(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $.post(url_site+'/statistics/statistics/get_bdc_volume', {date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#tab22").html("");
            $("#tab22").append(from_server).fadeIn(400);



        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}
function get_plis_ko_call(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
    $.post(url_site+'/statistics/statistics/get_pli_kocall_motif', {date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#detail_ko_call").html("");
            $("#detail_ko_call").append(from_server).fadeIn(400);


        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
}
function get_plis_ko_j(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $.post(url_site+'/statistics/statistics/get_pli_ko_journalier', {date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#tab31").html("");
            $("#tab31").append(from_server).fadeIn(400);


        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}
function get_plis_ko_s(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $.post(url_site+'/statistics/statistics/get_pli_ko_hebdo', {date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#tab32").html("");
            $("#tab32").append(from_server).fadeIn(400);


        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}
function get_plis_ko_m(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $.post(url_site+'/statistics/statistics/get_pli_ko_mensuel', {date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#tab33").html("");
            $("#tab33").append(from_server).fadeIn(400);


        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}
function quitter_tab11(){
    $("#tab11").hide();
}
function quitter_tab12(){
    $("#tab12").hide();
}
function quitter_tab13(){
    $("#tab13").hide();
}
function quitter_tab21(){
    $("#tab21").hide();
}
function quitter_tab22(){
    $("#tab22").hide();
}
function quitter_tab23(){
    $("#tab23").hide();
}
function quitter_tab24(){
    $("#tab24").hide();
}
function quitter_tab31(){
    $("#tab31").hide();
}
function quitter_tab32(){
    $("#tab32").hide();
}
function quitter_tab33(){
    $("#tab33").hide();
}
function detail_ko_call(){
    $("#detail_ko_call").hide();
}
function quitter_tab0(){
    $("#tab0").hide();
}
function quitter_tab01(){
    $("#tab01").hide();
}
function quitter_tab02(){
    $("#tab02").hide();
}
function quitter_tab03(){
    $("#tab03").hide();
}
function quitter_traitement_typologie(){
    $('#tab_typo').hide();
}
function quitter_traitement(){
    $("#tab_hebdo").hide();
}
function quitter_traitement_journalier(){
    $("#tab_journalier").hide();
}
function quitter_traitement_date(){ alert(9);
    $("#tab_traitement_date").hide();
}
function quitter_tab_mensuel_0(){
    $("#tab_menseuel_0").hide();
}
function quitter_pli_mpt_mensuel(){
    $("#tab_pli_mpt_mesuel").hide();
}
function quitter_pli_cloture_mensuel(){
    $("#tab_pli_cloture_mesuel").hide();
}
function quitter_pli_ko_mensuel(){
    $("#tab_pli_ko_mesuel").hide();
}
function quitter_pli_restant_mensuel(){
    $("#tab_pli_restant_mesuel").hide();
}
function quitter_detail_mensuel(){
    $("#tab_detail_mensuel").hide();
}
function charger_plis(){

    var numcommande = $("#numcommande").val();
    var numclient   = $("#numclient").val();
    var nom_client  = $("#nom_client").val();
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if(numcommande == '' && numclient == '' && nom_client == ''){

        if(date_debut == '' && date_fin == '' ){
            $("#error_message").show();
            setTimeout(function(){
                $("#error_message").fadeOut(1000);
            }, 2000);

            return false;

        }

    }

    $("#stat_rech").attr("disabled", "disabled");
    $.post(url_site+'/statistics/statistics/get_stat_pli', {numcommande:numcommande, numclient:numclient, nom_client:nom_client, date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#stat_rech").removeAttr("disabled");
            $("#result-rsrch").append(from_server).fadeIn(400);

            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });


            $('.js-basic-example').DataTable({
                responsive: true
            });

            //Exportable table
            $('.js-exportable').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'csv', 'excel'
                ]
            });


            $(function () {
                getMorris('donut', 'donut_chart');
                getMorrisDoc('donut', 'donut_document_chart');
                getMorrisPlisKO('donut', 'donut_plis_ko_chart');
                getMorrisPlisRestant('donut', 'donut_plis_restant_chart');
            });


            $("#tab11").hide();
            $("#tab12").hide();
            $("#tab13").hide();
            charger_detail_pli(date_debut,date_fin );

        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
            $("#stat_rech").removeAttr("disabled");
        }
    });

}

function charger_detail_pli(date_debut,date_fin){

    //var date_debut  = $("#date_debut").val();
    // var date_fin    = $("#date_fin").val();

    dataTable =  $('#detail_pli').DataTable( {

        dom: 'Bfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,buttons: [
            /* {
                    text: 'CSV',
                    action: function (e, dt, node, config) {
                        $.ajax({
                            "url": chemin_site+'/statistics/statistics/export_pli_csv',
                            "type": "POST",
                            "data": {date_deb:date_debut, date_fin:date_fin},
                            "success": function(res, status, xhr) {
                                var csvData = new Blob([res], {type: 'text/csv;charset=utf-8;'});
                                var csvURL = window.URL.createObjectURL(csvData);
                                var tempLink = document.createElement('a');
                                tempLink.href = csvURL;
                                tempLink.setAttribute('download', 'Ged_willemse_visualisation.csv');
                                tempLink.click();
                            }
                        });
                    }
                }
                , */{
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": url_site+'/statistics/statistics/export_pli_excel',
                        "type": "POST",
                        "data": {date_deb:date_debut, date_fin:date_fin},
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'Detail_pli.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }
        ]
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": url_site+'/statistics/statistics/ajax_detail_pli'
            ,"data": {date_debut : date_debut, date_fin : date_fin}
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //alert('test');
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },'columns':[
            { "data": 'date_courrier' }
            ,{ "data": 'date_numerisation' }
            ,{ "data": 'lot_scan' }
            ,{ "data": 'pli' }
            ,{ "data": 'dt_event' }
            ,{ "data": 'comment' }
            ,{ "data": 'statut' }
            ,{ "data": 'nb_doc' }
            ,{ "data": 'id_pli'}

        ]
    });

}
function repartition_par_doc(date_debut,date_fin){

    $("#donut_chart").html("tableau");

}
function voir_graph(date_debut,date_fin){

    $("#donut_chart").html("");
    getMorris('donut', 'donut_chart');

}

function getMorris(type, element) {

    var datas 		= $("#graph_donut").val();
    if(datas != '')
    {
        var elm 		= datas.split(',colors:');

        var data_colors = elm[1];
        var element     = elm[0].split('},{');
        var element_clr = elm[1].split("','");

        console.log(data_colors);

        var i 			= 0;
        var tb_data  	= new Array();
        var tb_color 	= new Array();
        var label   	= "";
        var valeur  	= "";
        var couleur 	= "";

        for(i ; i<element.length ; i++){

            var elm_donut 	= element[i].replace("[{","" );
            elm_donut 	  	= elm_donut.replace("}]", "");


            var data_donut  = elm_donut.split(",");
            var data_label  = data_donut[0];
            var data_value  = data_donut[1];
            var data_lbl    = data_label.split("'");
            var data_vl     = data_value.split(":");

            label           = data_lbl[1];
            valeur          = data_vl[1];

            tb_data.push({'label':label,'value':valeur});

        }


        for(var j = 0; j<element_clr.length ; j++){

            var elm_color  = element_clr[j].replace("['","" );
            elm_color      = elm_color.replace("']","" );
            tb_color.push(elm_color);
        }

        if(datas != ''){
            Morris.Donut({
                element: 'donut_chart',
                data:  tb_data ,
                colors :  tb_color,
                formatter: function (y) { return y + '%'}
            });
        }

    }
}
function getMorrisDoc(type, element) {

    var datas 		= $("#graph_donut_doc").val();
    if(datas != '')
    {
        var elm 		= datas.split(',colors:');

        var data_colors = elm[1];
        var element     = elm[0].split('},{');
        var element_clr = elm[1].split("','");

        console.log(data_colors);

        var i 			= 0;
        var tb_data  	= new Array();
        var tb_color 	= new Array();
        var label   	= "";
        var valeur  	= "";
        var couleur 	= "";

        for(i ; i<element.length ; i++){

            var elm_donut 	= element[i].replace("[{","" );
            elm_donut 	  	= elm_donut.replace("}]", "");


            var data_donut  = elm_donut.split(",");
            var data_label  = data_donut[0];
            var data_value  = data_donut[1];
            var data_lbl    = data_label.split("'");
            var data_vl     = data_value.split(":");

            label           = data_lbl[1];
            valeur          = data_vl[1];

            tb_data.push({'label':label,'value':valeur});

        }


        for(var j = 0; j<element_clr.length ; j++){

            var elm_color  = element_clr[j].replace("['","" );
            elm_color      = elm_color.replace("']","" );
            tb_color.push(elm_color);
        }

        if(datas != ''){
            Morris.Donut({
                element: 'donut_document_chart',
                data:  tb_data ,
                colors :  tb_color,
                formatter: function (y) { return y + '%'}
            });
        }
    }
}

function getMorrisPlisKO(type, element) {

    var datas 		= $("#graph_donut_plis_ko").val();
    if(datas != '')
    {
        var elm 		= datas.split(',colors:');

        var data_colors = elm[1];
        var element     = elm[0].split('},{');
        var element_clr = elm[1].split("','");

        console.log(data_colors);

        var i 			= 0;
        var tb_data  	= new Array();
        var tb_color 	= new Array();
        var label   	= "";
        var valeur  	= "";
        var couleur 	= "";

        for(i ; i<element.length ; i++){

            var elm_donut 	= element[i].replace("[{","" );
            elm_donut 	  	= elm_donut.replace("}]", "");


            var data_donut  = elm_donut.split(",");
            var data_label  = data_donut[0];
            var data_value  = data_donut[1];
            var data_lbl    = data_label.split("'");
            var data_vl     = data_value.split(":");

            label           = data_lbl[1];
            valeur          = data_vl[1];

            tb_data.push({'label':label,'value':valeur});

        }


        for(var j = 0; j<element_clr.length ; j++){

            var elm_color  = element_clr[j].replace("['","" );
            elm_color      = elm_color.replace("']","" );
            tb_color.push(elm_color);
        }

        if(datas != ''){
            Morris.Donut({
                element: 'donut_plis_ko_chart',
                data:  tb_data ,
                colors :  tb_color,
                formatter: function (y) { return y + '%'}
            });
        }
    }
}


function getMorrisPlisRestant(type, element) {

    var datas 		= $("#graph_donut_plis_restant").val();
    if(datas != '')
    {
        var elm 		= datas.split(',colors:');

        var data_colors = elm[1];
        var element     = elm[0].split('},{');
        var element_clr = elm[1].split("','");

        console.log("--- restant ---"+datas);

        var i 			= 0;
        var tb_data  	= new Array();
        var tb_color 	= new Array();
        var label   	= "";
        var valeur  	= "";
        var couleur 	= "";

        for(i ; i<element.length ; i++){

            var elm_donut 	= element[i].replace("[{","" );
            elm_donut 	  	= elm_donut.replace("}]", "");


            var data_donut  = elm_donut.split(",");
            var data_label  = data_donut[0];
            var data_value  = data_donut[1];
            var data_lbl    = data_label.split("'");
            var data_vl     = data_value.split(":");

            label           = data_lbl[1];
            valeur          = data_vl[1];

            tb_data.push({'label':label,'value':valeur});

        }


        for(var j = 0; j<element_clr.length ; j++){

            var elm_color  = element_clr[j].replace("['","" );
            elm_color      = elm_color.replace("']","" );
            tb_color.push(elm_color);
        }

        if(datas != ''){
            Morris.Donut({
                element: 'donut_plis_restant_chart',
                data:  tb_data ,
                colors :  tb_color,
                formatter: function (y) { return y + '%'}
            });
        }
    }
}
function chercher_plis_stat(date_debut,date_fin){
    $.post(url_site+'/statistics/statistics/get_pli', {date_debut : date_debut, date_fin : date_fin, flag : 1}, function(from_server){
        $("#detail_stat").append(from_server).fadeIn(400);

    });
}
function charger_traitement(){

    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
    var select_soc  = $("#select_soc").val();
    var select_source  = $("#select_source").val();
    //var mensuel    = $("#mensuel").val();
    var mensuel = $("#mensuel").is(':checked') ? 1 : 0;

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if(date_debut == '' && date_fin == '' && select_soc =='' && select_source == ''){
        $("#error_message").show();
        setTimeout(function(){
            $("#error_message").fadeOut(1000);
        }, 2000);

        return false;

    }
    if(select_soc =='' ){
        $("#error_message").show();
        setTimeout(function(){
            $("#error_message").fadeOut(1000);
        }, 2000);

        return false;

    }
    if(select_source =='' ){
        $("#error_message").show();
        setTimeout(function(){
            $("#error_message").fadeOut(1000);
        }, 2000);

        return false;

    }

    $("#stat_rech").attr("disabled", "disabled");
    $("#preloader_traitement").show();

    $.post(url_site+'/statistics/statistics_flux/get_traitement', {mensuel : mensuel ,select_soc, select_source, date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#stat_rech").removeAttr("disabled");
            $("#result-rsrch").append(from_server).fadeIn(400);
            $("#preloader_traitement").hide();
            $('.btn_synthese').removeClass('btn_synthese');
			//charger_traitement_saisie(select_soc,select_source,date_debut,date_fin);
            charger_synthese_typologie();


            //Exportable table
            $('.js-exportable').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'csv', 'excel'
                ]
            });         

			if(mensuel == 0)
            {
                $("#tab_mensuel").css("display","none");
                $("#tab_hebdo").css("display","");
                $("#tab_journalier").css("display","");
                charger_mouvement();
                charger_detail_traitement();

            }
            else
            {
                $("#tab_mensuel").css("display","");
                $("#tab_hebdo").css("display","none");
                $("#tab_journalier").css("display","none");
            }

        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
            $("#stat_rech").removeAttr("disabled");
            $("#preloader_traitement").hide();
        }
		
		
		
    });

}


function charger_synthese_typologie(){

    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
    var select_soc  = $("#select_soc").val();
	var select_source  = $("#select_source").val();

    $.post(url_site+'/statistics/statistics/get_traitement_typo', {select_soc : select_soc, date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            $("#traitement_typo").append(from_server).fadeIn(400);
            $('.js-table-typo').DataTable({
                "processing": true,
                "scrollCollapse": true,
                "deferRender":    true,
                "scroller":       true,
                "lengthChange": false,
                dom: 'lBfrtip',
                responsive: true,
                buttons: [
                    'excel'
                ]
            });

        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
		charger_traitement_saisie(select_soc,select_source,date_debut,date_fin);
    });

}

function charger_traitement_saisie(select_soc,select_source,date_debut,date_fin){
	$("#preloader").show();
    $.post(url_site+'/statistics/statistics_flux/get_traitement_plis_saisie', {select_soc, select_source,date_debut, date_fin, flag : 1}, function(from_server){
        $("#ajax_saisie").append(from_server).fadeIn(400);
		//Exportable table
            $('.js-exportable-saisie').DataTable({
				"scrollY": '300px',
				"scrollX": 'auto',
                "paginate":     false,
                "scrollCollapse": true,
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'csv', 'excel'
                ]
            });  
		$('.ajax-saisie').css('margin-top','20px');
		$("#preloader").hide();
		charger_traitement_mvt_saisie(select_soc,select_source,date_debut,date_fin);
		
    });
}

function charger_traitement_mvt_saisie(select_soc,select_source,date_debut,date_fin){
	$("#preloader_saisie").show();
    $.post(url_site+'/statistics/statistics_flux/get_traitement_mvt_saisie', {select_soc, select_source,date_debut, date_fin}, function(from_server){
        $("#ajax_mvt_saisie").append(from_server).fadeIn(400);
		//Exportable table
            $('.js-exportable-mvt').DataTable({
				"scrollY": '300px',
				"scrollX": 'auto',
                "paginate":     false,
                "scrollCollapse": true,
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'csv', 'excel'
                ]
            }); 
		$('.ajax-mvt-saisie').css('margin-top','50px');	
		$("#preloader_saisie").hide();		
		charger_traitement_controle(select_soc,select_source,date_debut,date_fin);
    });
}

function charger_traitement_controle(select_soc,select_source,date_debut,date_fin){
	$("#preloader_controle_pli").show();
    $.post(url_site+'/statistics/statistics_flux/get_traitement_plis_controle', {select_soc, select_source,date_debut, date_fin, flag : 1}, function(from_server){
        $("#ajax_controle").append(from_server).fadeIn(400);
		//Exportable table
            $('.js-exportable-controle').DataTable({
				"scrollY": '300px',
				"scrollX": 'auto',
				"paginate":     false,
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'csv', 'excel'
                ]
            });   
		$('.ajax-controle').css('margin-top','50px');
		$("#preloader_controle_pli").hide();		
		charger_traitement_mvt_controle(select_soc,select_source,date_debut,date_fin);
    });
}

function charger_traitement_mvt_controle(select_soc,select_source,date_debut,date_fin){
	$("#preloader_controle_mvt").show();
    $.post(url_site+'/statistics/statistics_flux/get_traitement_mvt_controle', {select_soc, select_source,date_debut, date_fin}, function(from_server){
        $("#ajax_mvt_controle").append(from_server).fadeIn(400);
		//Exportable table
            $('.js-exportable-mvt-controle').DataTable({
                "scrollY": '300px',
				"scrollX": 'auto',
				"paginate":     false,
				dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'csv', 'excel'
                ]
            });         
		$('.ajax-mvt-controle').css('margin-top','50px');	
		$("#preloader_controle_mvt").hide();
    });
	
		
}

function afficher_pli(id_pli, id_doc) {


}

function afficher_doc(id_doc) {

}


function charger_plis_mensuel(){

    var date_debut  = $("#mois_debut").val();
    var date_fin    = $("#mois_fin").val();
    var id_societe  = $("#select_soc").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);
    $("#stat_rech").attr("disabled", "disabled");

    if(id_societe == '' ){
        Lobibox.notify('error', {
            title: 'Résultat',
            sound: false,
            msg: ' Merci de renseigner la société !'
        });
        $("#stat_rech").removeAttr("disabled");
    }
    if(date_debut == '' && date_fin == '' && id_societe == '' ){
        $("#error_message").show();
        setTimeout(function(){
            $("#error_message").fadeOut(1000);
        }, 2000);

        return false;

    }


    $.post(url_site+'/statistics/statistics/get_stat_pli_mensuel', { date_debut : date_debut, date_fin : date_fin, id_societe : id_societe}, function(from_server){
        if(from_server != 0){

            $("#stat_rech").removeAttr("disabled");
            $("#result-rsrch").append(from_server).fadeIn(400);

            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });


            $('.js-basic-example').DataTable({
                responsive: true
            });

            $(".table-stat-mensuel-typologie").DataTable({
                scrollX:        true,
                dom: 'Bfrtip',
                responsive: true,
                aaSorting: [],
                buttons: [
                    'csv', 'excel'
                ],
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 1

                }
            });
            //Exportable table
            $('.js-exportable').DataTable({

                responsive: true,
                aaSorting: [],
                buttons: [
                    'csv', 'excel'
                ]
            });
            //aoColumnDefs: null,


            $(function () {
                getMorris('donut', 'donut_chart');
                getMorrisDoc('donut', 'donut_document_chart');
                getMorrisPlisKO('donut', 'donut_plis_ko_chart');
                getMorrisPlisRestant('donut', 'donut_plis_restant_chart');
            });

            $("#tab11").hide();
            $("#tab12").hide();
            $("#tab13").hide();


        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
            $("#stat_rech").removeAttr("disabled");
        }
    });

}

function load_correction(){

    $.post(url_site+'/statistics/statistics/get_pli_correction', {}, function(from_server){
        if(from_server != 0){

            $("#id_correction").html (from_server) ;

        }
    });

}
function load_data_att(){
    $("#preloader_solde").show();
    $.post(url_site+'/statistics/statistics/get_pli_statut_att', {}, function(from_server){
        if(from_server != 0){
            $("#id_pli_statut_att").html (from_server) ;
            $("#preloader_solde").hide();
        }
    });

}

function charger_mouvement(){
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
    var select_soc  = $("#select_soc").val();
    var select_source  = $("#select_source").val();

    $.post(url_site+'/statistics/statistics_flux/get_traitement_date', {select_soc, select_source, date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){

            //$("#stat_rech").removeAttr("disabled");
            $("#traitement_date_pli").append(from_server).fadeIn(400);

            //Exportable table
           /* $('.js-table-mvt').DataTable({
                "scrollY": '250px',
                "processing": true,
                "scrollCollapse": true,
                "deferRender":    true,
                "scroller":       true,
                "paginate":       false,
                dom: 'lBfrtip',
                responsive: true,
                buttons: [
                    'excel'
                ]
            });*/
			 //Exportable table
            $('.js-table-mvt').DataTable({
					"scrollY": '300px',
					"scrollX": 'auto',
					"processing": true,
					"scrollCollapse": true,
					"deferRender":    true,
					"scroller":       true,
					"lengthChange": false,
					"paginate":     false,
					dom: 'lBfrtip',
					responsive: true,
					buttons: [
						'excel'
					]
            });



        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
            //$("#stat_rech").removeAttr("disabled");
        }
    });
}
function charger_detail_traitement(){

    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
    var select_soc  = $("#select_soc").val();
    var select_source  = $("#select_source").val();
    $("#preloader_traitement_dtl").show();
    if(select_source == '3'){
    dataTable =  $('#traitement_pli').DataTable( {

        dom: 'Bfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,buttons: [
            /*  'csv', 'excel' */
            {
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": url_site+'/statistics/statistics_flux/export_traitement_excel',
                        "type": "POST",
                        "data": {date_deb:date_debut, date_fin:date_fin,select_soc : select_soc,source : select_source},
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'Traitement_pli.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }
        ]
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": url_site+'/statistics/statistics_flux/ajax_detail_traitement'
            ,"data": {date_debut : date_debut, date_fin : date_fin,select_soc : select_soc, source:select_source}
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },'columns':[
            { "data": 'nom_societe' }
            ,{ "data": 'dt_event' }
            ,{ "data": 'date_courrier' }
            ,{ "data": 'lot_scan' }
            ,{ "data": 'pli' }
            ,{ "data": 'id_pli'}
            ,{ "data": 'nb_mvt'}  
            ,{ "data": 'etape' }
			,{ "data": 'etat' }
			,{ "data": 'stat' }
            ,{ "data": 'libelle_motif' }
            ,{ "data": 'typologie' }
            ,{ "data": 'mode_paiement'}
            ,{ "data": 'cmc7'}
            ,{ "data": 'montant'}
            ,{ "data": 'rlmc'}
            ,{ "data": 'infos_datamatrix'}
            ,{ "data": 'motif_rejet'}
            ,{ "data": 'description_rejet'}
            ,{ "data": 'titre_titre'}
            // ,{ "data": 'titre_societe_nom'}


        ]
    });
    }
    else{
        dataTable =  $('#traitement_flux').DataTable( {

            dom: 'Bfrtip'
            ,"bProcessing": true
            ,"bServerSide": true
            ,"bScrollCollapse": true
            ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
            ,"pageLength": 10
            ,responsive: true
            ,"bDestroy": true
            ,"bSort": true
            ,buttons: [
                /*  'csv', 'excel' */
                {
                    text: 'Excel',
                    action: function (e, dt, node, config) {
                        $.ajax({
                            "url": url_site+'/statistics/statistics_flux/export_traitement_excel',
                            "type": "POST",
                            "data": {date_deb:date_debut, date_fin:date_fin,select_soc : select_soc,source : select_source},
                            "success": function(res, status, xhr) {
                                var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                                var csvURL = window.URL.createObjectURL(csvData);
                                var tempLink = document.createElement('a');
                                tempLink.href = csvURL;
                                tempLink.setAttribute('download', 'Traitement_flux.xls');
                                document.body.appendChild(tempLink);
                                tempLink.click();
                            }
                        });
                    }
                }
            ]
            ,"oLanguage": {
                "sLengthMenu": "_MENU_ par page"
                ,"sZeroRecords": "Aucune donn\351e"
                ,"sEmptyTable": "Aucune donn\351e"
                ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
                ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
            }
            ,"ajax":{
                //'dataType': 'json',
                "url": url_site+'/statistics/statistics_flux/ajax_detail_traitement'
                ,"data": {date_debut : date_debut, date_fin : date_fin,select_soc : select_soc, source:select_source}
                ,'type': 'POST'
            }
            ,"fnDrawCallback": function( oSettings ) {
                //$('[data-toggle="tooltip"]').tooltip();
                //set_actions();
            },'columns':[
                { "data": 'nom_societe' }
                ,{ "data": 'date_traitement'}
                ,{ "data": 'date_reception'}
                ,{ "data": 'sujet' }
                ,{ "data": 'source' }
                ,{ "data": 'id_flux' }
                ,{ "data": 'date_saisie_adv'}
                ,{ "data": 'id_lot_saisie_flux' }
                ,{ "data": 'typologie' }
                ,{ "data": 'statut_pli' }
                ,{ "data": 'lot_advantage' }
                ,{ "data": 'nb_abo' }
                // ,{ "data": 'titre_societe_nom'}


            ]
        });
    }
    $("#preloader_traitement_dtl").hide();
}


function load_societe(){

    $.post(url_site+'/statistics/statistics/get_societe', {}, function(from_server){
        if(from_server != 0){

            $("#select_soc").html (from_server) ;

        }
    });

}

function load_source(){

    $.post(url_site+'/statistics/statistics_flux/get_source', {}, function(from_server){
        if(from_server != 0){

            $("#select_source").html (from_server) ;

        }
    });

}

function suivi_solde(){

    var select_soc  = $("#select_soc").val();
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
    var source_visu   = $('input[name=source_visu]:checked').val();

    $("#message_alert").hide();
    if(select_soc == ""){
        $("#message_alert").html('Merci de renseigner la société !');
        $("#message_alert").show();
        return 0;
    }
    if(date_debut == "" || date_fin == ""){
        $("#message_alert").html('Merci de renseigner les dates !');
        $("#message_alert").show();
        return 0;
    }
    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    //#ed5a5a
    $("#stat_rech").attr("disabled", "disabled");
    $("#preloader_solde").show();
    $.post(url_site+'/statistics/statistics/get_solde', {date_debut : date_debut, date_fin : date_fin,select_soc: select_soc,source_visu: source_visu}, function(from_server){
        if(from_server != 0){

            $("#stat_rech").removeAttr("disabled");
            $("#result-rsrch").append(from_server).fadeIn(400);
            $("#preloader_solde").hide();
            $("#id_pli_statut_att").hide();

            $('.js-exportable').DataTable({
                "scrollY": '300px',
                "processing": true,
                "scrollCollapse": true,
                "deferRender":    true,
                "scroller":       true,
                "paginate":       false,
                dom: 'lBfrtip',
                responsive: true,
                buttons: [
                    'excel'
                ]
            });


            //charger_mouvement();



        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
            $("#stat_rech").removeAttr("disabled");
            $("#preloader_solde").hide();
        }
    });

}

//exporter vers excell tous les id_pli
function exporter(societe, titre, type,source = null)
{
    console.log(type);

    $.ajax({
        "url":url_site + '/statistics/statistics/exportExcell/'+societe+'/'+titre+'/'+type+'/'+source,
        "type": "POST",
        //"data"    : myPost,
        "success": function(res, status, xhr) {
            document.location.href = url_site+'/statistics/statistics/exportExcell/'+societe+'/'+titre+'/'+type+'/'+source;
        }
    });
}

//afficher le tableau de traitement par typologie
function get_synthese(){
    $('.trait_typo').removeClass('trait_typo');
}

