(function ($) {
 "use strict";
 
	
	$("#date_debut").datepicker({
		/*todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,*/format: 'dd/mm/yyyy'
	});
	
	 $("#date_fin").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	
	$("#date_debut_ttt").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	
	 $("#date_fin_ttt").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	  /*$("#tab1").show();
	  $("#tab2").show();
	  $("#tab3").show();
	  $("#tab4").show();
	  $("#tab5").show();
	  */
	 load_societe();
	// load_statut_extract();
	 //load_mode_paiement();
	 load_operateur();
	 load_typologie();
	
	$("#preloader_typage").hide();
	$("#preloader_saisie").hide();
	$("#preloader_controle").hide();
	$("#preloader_export_typage").hide();
	$("#preloader_export_saisie").hide();
	$("#preloader_export_controle").hide();
	$("#preloader_global_global").hide(); 
	$("#preloader_global_global_global").hide(); 
	$("#preloader_globalpli").hide(); 
	$("#preloader_globalop").hide(); 
	
	
	$('.navbar-right .dropdown-menu .body .menu').slimscroll({
		height: '65vh',
		color: 'rgba(0,0,0,0.5)',
		size: '4px',
		alwaysVisible: false,
		borderRadius: '0',
		railBorderRadius: '0'
	});
	
})(jQuery); 


		
function rech(){
    /*if(window.event.keyCode == 13){
        recherche_doc();
    }*/
}
function toogle_typage(i){
	
	if($("#date_typage_"+i).is(':hidden') == true) $("#date_typage_"+i).show();
     else	$("#date_typage_"+i).hide();
}
function toogle_saisie(i){
	
	if($("#date_saisie_"+i).is(':hidden') == true) $("#date_saisie_"+i).show();
     else	$("#date_saisie_"+i).hide();
}

var toggle_table = function(i,etape) {
$('div#date_'+etape+'_' + i).toggle();
$('div#detail' + i).toggleClass('in');
};

function toogle_controle(i){
	
	if($("#date_controle_"+i).is(':hidden') == true) $("#date_controle_"+i).show();
     else	$("#date_controle_"+i).hide();
}

function charger_statut(){ 	 
	
    $.post(url_site+'/dmt/dmt/visu_dmt', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
            $("#statut").append(from_server).fadeIn(400);			
			
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}


function charger_dmt(){ 
	
    var select_soc  	= $("#select_soc").val();
	var select_op  	    = $("#select_op").val();
    var select_typo    = $("#select_typo").val(); 
    var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if((date_debut_ttt == '' && date_fin_ttt == '' ) || (date_debut_ttt == '' && date_fin_ttt != '') || (date_debut_ttt != '' && date_fin_ttt == '')){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}	
	
	//$("#visu_dmt").attr("disabled", "disabled");
	
	 $.post(url_site+'/dmt/dmt/ajax_typage_global', {select_soc:select_soc,select_op:select_op,select_typo:select_typo, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt,action_ttt:'typage'}, function(from_server){ 
			
		 if(from_server != 0){
            // $("#pli_typage").append(from_server).fadeIn(400);			
            $("#dmt_global").html(from_server);			
            //$("#dmt_global").append(from_server).fadeIn(400);	
			$('#table_dmt_pli_typage').slimscroll({
				size: '7px',
				height:'290px',
				opacity: 0.2
			});
			$('#table_dmt_op_typage').slimscroll({
				size: '7px',
				height:'290px',
				opacity: 0.2
			});
			
			get_data();	
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}
function charger_dmt_saisie(){ 
	
    var select_soc  	= $("#select_soc").val();
	var select_op  	    = $("#select_op").val();
    var select_typo    = $("#select_typo").val(); 
    var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if((date_debut_ttt == '' && date_fin_ttt == '' ) || (date_debut_ttt == '' && date_fin_ttt != '') || (date_debut_ttt != '' && date_fin_ttt == '')){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}	
	
	//$("#visu_dmt").attr("disabled", "disabled");
	
	 $.post(url_site+'/dmt/dmt/ajax_typage_global', {select_soc:select_soc,select_op:select_op,select_typo:select_typo, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt,action_ttt:'saisie'}, function(from_server){ 
			
		 if(from_server != 0){
            // $("#pli_typage").append(from_server).fadeIn(400);			
            $("#dmt_global_saisie").html(from_server);			
            //$("#dmt_global").append(from_server).fadeIn(400);			
			$('#table_dmt_pli_saisie').slimscroll({
				size: '7px',
				height:'290px',
				opacity: 0.2
			});
			$('#table_dmt_op_saisie').slimscroll({
				size: '7px',
				height:'290px',
				opacity: 0.2
			});
			get_data();
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}
function charger_dmt_controle(){ 
	
    var select_soc  	= $("#select_soc").val();
	var select_op  	    = $("#select_op").val();
    var select_typo    = $("#select_typo").val(); 
    var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if((date_debut_ttt == '' && date_fin_ttt == '' ) || (date_debut_ttt == '' && date_fin_ttt != '') || (date_debut_ttt != '' && date_fin_ttt == '')){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}	
	
	//$("#visu_dmt").attr("disabled", "disabled");
	
	 $.post(url_site+'/dmt/dmt/ajax_typage_global', {select_soc:select_soc,select_op:select_op,select_typo:select_typo, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt,action_ttt:'controle'}, function(from_server){ 
			
		 if(from_server != 0){
            // $("#pli_typage").append(from_server).fadeIn(400);			
            $("#dmt_global_controle").html(from_server);			
            //$("#dmt_global").append(from_server).fadeIn(400);			
			$('#table_dmt_pli_controle').slimscroll({
				size: '7px',
				height:'290px',
				opacity: 0.2
			});
			$('#table_dmt_op_controle').slimscroll({
				size: '7px',
				height:'290px',
				opacity: 0.2
			});
			get_data();
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}
function charger_dmt_global_global(){ 
	
    var select_soc  	= $("#select_soc").val();
	var select_op  	    = $("#select_op").val();
    var select_typo    = $("#select_typo").val(); 
    var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if((date_debut_ttt == '' && date_fin_ttt == '' ) || (date_debut_ttt == '' && date_fin_ttt != '') || (date_debut_ttt != '' && date_fin_ttt == '')){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}	
	
	//$("#preloader_global_global").show();
	
	 $.post(url_site+'/dmt/dmt/ajax_global_global', {select_soc:select_soc,select_op:select_op,select_typo:select_typo, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt}, function(from_server){ 
			
		 if(from_server != 0){
			 
			$("#dmt_global_global").html(from_server);	

			$('#table_dmt_global_global').slimscroll({
				size: '7px',
				height:'290px',
				opacity: 0.2
			});		
            $('#table_dmt_pli_global').slimscroll({
				size: '7px',
				height:'290px',
				opacity: 0.2
			});
			$('#table_dmt_op_global').slimscroll({
				size: '7px',
				height:'290px',
				opacity: 0.2
			});
			get_data();
			$("#preloader_global_global").hide();
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
			$("#preloader_global_global").hide();
        }
    });

	charger_global_dmt();
}
function charger_typage_dmt(){
   
    var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo    = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	if(date_debut_ttt != '' && date_fin_ttt == ''  ){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}
	if(date_debut_ttt == '' || date_fin_ttt == ''  ){
		
		$("#error_message").html("Veuillez sélectionner les dates et la société !");
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		return false;

	}
	if( select_soc == ''  ){
		
		$("#error_message").html("Veuillez sélectionner la société !");
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		return false;

	}
	
	$("#visu_dmt").attr("disabled", "disabled"); 	
	$("#preloader_typage").show();	
	
	/* Chargement de DMT global traitement Typage*/
	charger_dmt();
	/* Chargement de DMT détail traitement Typage */
    dataTable    =  $('#dmt_pli').DataTable( {
     
         dom: 'Bfrtip'
        ,"bProcessing": true
		,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
		/*,"ordering": true*/
		,"order": []
		/*,"columnDefs": [{targets: [ 0 ],orderData: [ 0, 1 ]}, {targets: [ 1 ],orderData: [ 0, 1 ]}]*/
		,buttons: []
		/*[
				 {
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": chemin_site+'/dmt/dmt/export_dmt_typage',
                        "type": "POST",
						"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt},
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'Extraction.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }]	*/	
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"			
            ,"sProcessing":  $('#preloader_typage')[0].outerHTML.replace(/display\: none;/gm, '')		
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": chemin_site+'/dmt/dmt/ajax_typage_dmt'
            ,"data": {select_soc:select_soc, select_op : select_op, select_typo : select_typo, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt}
            ,'type': 'POST'
			,'async': 'false'
			,"complete" : function(resultat, statut){
				$("#preloader_typage").hide();	
				$("#preloader_saisie").show();		
				charger_saisie_dmt();
				
			}
			
        },
		initComplete: function() {
			$("#dmt_pli_filter input").unbind();
			$("#dmt_pli_filter input").bind("keyup", function(e) {
				if(e.keyCode == 13) {
					dataTable.search(this.value).draw();
				
				}
			});
		}
        ,"fnDrawCallback": function( oSettings ) {
            $("#preloader_typage").hide();	
			$("#preloader_saisie").show();
			
        } 
		,'columns':[
			 { "data": 'login_typage' }
			,{ "data": 'id_pli' } 
            ,{ "data": 'nom_societe' }
            ,{ "data": 'dt_event' }
			,{ "data": 'nb_mvt'}				
			,{ "data": 'typologie'}
			,{ "data": 'mode_paiement'}
			,{ "data": 'lot_scan' }
			,{ "data": 'libelle' }
			,{ "data": 'date_typage' }
			,{ "data": 'debut_fin_typage' }
			,{ "data": 'duree_typage' }
			
			
        ],
    });
	
	$('.dataTables_wrapper .dataTables_processing').css('margin-top','-50px');
	$('.dataTables_wrapper .dataTables_processing').css('font-size','16px');
	
					
	
}

function charger_saisie_dmt(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo    = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	if(date_debut_ttt != '' && date_fin_ttt == ''  ){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}
	//$("#preloader_saisie").show();
	
	/* Chargement de DMT global traitement Saisie*/
	charger_dmt_saisie();
	/* Chargement de DMT détail traitement Saisie */
	
     dataTableSaisie    =  $('#dmt_saisie_pli').DataTable( {
     
         dom: 'Bfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,buttons: []
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
			,"sProcessing":  $('#preloader_saisie')[0].outerHTML.replace(/display\: none;/gm, '')	
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": chemin_site+'/dmt/dmt/ajax_saisie_dmt'
            ,"data": {select_soc:select_soc, select_op : select_op, select_typo : select_typo, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt}
            ,'type': 'POST'
			,'async': 'false'
			,"complete" : function(resultat, statut){
				$("#preloader_saisie").hide();	
				$("#preloader_controle").show();
				charger_controle_dmt();
			}
        },
		initComplete: function() {
			$("#dmt_saisie_pli_filter input").unbind();
			$("#dmt_saisie_pli_filter input").bind("keyup", function(e) {
				if(e.keyCode == 13) {
					dataTableSaisie.search(this.value).draw();
					
				}
			});
		}
        ,"fnDrawCallback": function( oSettings ) {
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
			$("#preloader_saisie").hide();	
			$("#preloader_controle").show();
        },'columns':[
			 { "data": 'login_saisie' }
			,{ "data": 'id_pli' } 
            ,{ "data": 'nom_societe' }
            ,{ "data": 'dt_event' }
			,{ "data": 'nb_mvt'}				
			,{ "data": 'typologie'}
			,{ "data": 'mode_paiement'}
			,{ "data": 'lot_scan' }
			,{ "data": 'libelle' }
			,{ "data": 'date_saisie' }
			,{ "data": 'debut_fin_saisie' }
			,{ "data": 'duree_saisie' }
			
			
        ],
    });
	
	$('.dataTables_wrapper .dataTables_processing').css('margin-top','-50px');
	$('.dataTables_wrapper .dataTables_processing').css('font-size','16px');
	
				
}

function charger_controle_dmt(){
	
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo    = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	if(date_debut_ttt != '' && date_fin_ttt == ''  ){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}
	//$("#preloader_controle").show();
	
	/* Chargement de DMT global traitement Controle*/
	charger_dmt_controle();
	
	/* Chargement de DMT détail traitement Controle */
	
     dataTableCtrl    =  $('#dmt_controle_pli').DataTable( {
     
         dom: 'Bfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,buttons: []
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
			,"sProcessing":  $('#preloader_controle')[0].outerHTML.replace(/display\: none;/gm, '')	
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": chemin_site+'/dmt/dmt/ajax_controle_dmt'
            ,"data": {select_soc:select_soc, select_op : select_op, select_typo : select_typo, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt}
            ,'type': 'POST'
			,'async': 'false'
			,"complete" : function(resultat, statut){
				$("#preloader_controle").hide();
				$("#preloader_global_global").show();				
				charger_dmt_global_global();
			}
        },
		initComplete: function() {
			$("#dmt_controle_pli_filter input").unbind();
			$("#dmt_controle_pli_filter input").bind("keyup", function(e) {
				if(e.keyCode == 13) {
					dataTableCtrl.search(this.value).draw();
					
				}
			});
		}
        ,"fnDrawCallback": function( oSettings ) {
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
			$("#preloader_controle").hide();
			$("#preloader_global_global").show();
			$("#visu_dmt").removeAttr("disabled"); 
        },'columns':[
			 { "data": 'login_controle' }
			,{ "data": 'id_pli' } 
            ,{ "data": 'nom_societe' }
            ,{ "data": 'dt_event' }
			,{ "data": 'nb_mvt'}				
			,{ "data": 'typologie'}
			,{ "data": 'mode_paiement'}
			,{ "data": 'lot_scan' }
			,{ "data": 'libelle' }
			,{ "data": 'date_controle' }
			,{ "data": 'debut_fin_controle' }
			,{ "data": 'duree_controle' }
			
			
        ],
    });
	
	$('.dataTables_wrapper .dataTables_processing').css('margin-top','-50px');
	$('.dataTables_wrapper .dataTables_processing').css('font-size','16px');
	
	
}

function charger_global_dmt(){
	
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo    = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	if(date_debut_ttt != '' && date_fin_ttt == ''  ){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}
	//$("#preloader_global_global").show();
	
	/* Chargement de DMT global traitement*/
		
	/* Chargement de DMT détail traitement */
	
     dataTableGbl    =  $('#dmt_globalglobal').DataTable( {
     
         dom: 'Bfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,buttons: []
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
			,"sProcessing":  $('#preloader_global_global')[0].outerHTML.replace(/display\: none;/gm, '')	
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": chemin_site+'/dmt/dmt/ajax_globals_dmt'
            ,"data": {select_soc:select_soc, select_op : select_op, select_typo : select_typo, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt}
            ,'type': 'POST'
			,'async': 'false'
			,"complete" : function(resultat, statut){
				$("#preloader_global_global").hide();	
				
			}
        },
		initComplete: function() {
			$("#dmt_globalglobal_filter input").unbind();
			$("#dmt_globalglobal_filter input").bind("keyup", function(e) {
				if(e.keyCode == 13) {
					dataTableGbl.search(this.value).draw();
					
				}
			});
		}
        ,"fnDrawCallback": function( oSettings ) {
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
			$("#preloader_global_global").hide();
			$("#visu_dmt").removeAttr("disabled"); 
        },'columns':[
			 { "data": 'dt_event' }
			,{ "data": 'id_pli' } 
            ,{ "data": 'nom_societe' }
            ,{ "data": 'nb_mvt'}				
			,{ "data": 'typologie'}
			,{ "data": 'mode_paiement'}
			,{ "data": 'lot_scan' }
			,{ "data": 'libelle' }
			,{ "data": 'duree_typage' }
			,{ "data": 'duree_saisie' }
			,{ "data": 'duree_controle' }
			,{ "data": 'duree_totale' }
			
			
        ],
    });
	
	$('.dataTables_wrapper .dataTables_processing').css('margin-top','-50px');
	$('.dataTables_wrapper .dataTables_processing').css('font-size','16px');
	
	//charger_dmt_global_global();
	
}

function load_societe(){
	
	$.post(url_site+'/statistics/statistics/get_societe', {}, function(from_server){ 
        if(from_server != 0){
			
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_soc" name="select_soc">';
			    strHtml += '<option value="">-- Société --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_soc").html (strHtml) ;
			$.AdminBSB.select.activate();			
			
					
		}
	});
	
}
function load_statut_extract(){
	
	$.post(url_site+'/dmt/dmt/get_statut', {}, function(from_server){ 
        if(from_server != 0){
			
			//$("#select_statut").html (from_server) ;
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_statut" name="select_statut">';
			    strHtml += '<option value="">-- Statut --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_statut").html (strHtml) ;
			$.AdminBSB.select.activate();		
		}
	});
	
}
function load_mode_paiement(){
	
	$.post(url_site+'/dmt/dmt/get_mode_paiement', {}, function(from_server){ 
        if(from_server != 0){
			
			//$("#select_mp").html (from_server) ;
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_mp" name="select_mp">';
			    strHtml += '<option value="">-- Mode de paiement --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_mp").html (strHtml) ;
			$.AdminBSB.select.activate();	
					
		}
	});
	
}

function load_operateur(){
	
	$.post(url_site+'/dmt/dmt/get_operateur', {}, function(from_server){ 
        if(from_server != 0){
			
			//$("#select_op").html (from_server) ;
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_op" name="select_op">';
			    strHtml += '<option value="">-- Opérateur --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_op").html (strHtml) ;
			$.AdminBSB.select.activate();	
			//$("filter-option").html("-- Opérateur --");		
		}
	});
	
}

function load_typologie(){
	
	$.post(url_site+'/dmt/dmt/get_typologie', {}, function(from_server){ 
        if(from_server != 0){
			
			//$("#select_typo").html (from_server) ;
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_typo" name="select_typo">';
			    strHtml += '<option value="">-- Typologie --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_typo").html (strHtml) ;
			$.AdminBSB.select.activate();	
			//$("filter-option").html("-- Typologie --");				
		}
	});
	
}

function export_dmt_typage(){ 
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	
	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Traitement_typage_"+datyd+"_"+datyf+'.xls';
	$("#preloader_export_typage").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_typage',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'typage'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_export_typage").hide();	
		}
	});
	
}

function export_dmt_saisie(){ 
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	
	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Traitement_saisie_"+datyd+"_"+datyf+'.xls';
	$("#preloader_export_saisie").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_saisie',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'saisie'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_export_saisie").hide();	
		}
	});
	
}


function export_dmt_controle(){ 
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Traitement_controle_"+datyd+"_"+datyf+'.xls';
	$("#preloader_export_controle").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_controle',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'controle'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_export_controle").hide();	
		}
	});
	
}
function export_dmt_global_global(){ 
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_"+datyd+"_"+datyf+'.xls';
	$("#preloader_export_global_global").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_global_global',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'global'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_export_global_global").hide();	
		}
	});
	
}
function export_dmt_pli_gbl_typage(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_globale_typage_"+datyd+"_"+datyf+'.xls';
	$("#preloader_pli_typage").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_global',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'typage'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_pli_typage").hide();	
		}
	});
}
function export_dmt_op_gbl_typage(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_globale_typage_"+datyd+"_"+datyf+'.xls';
	$("#preloader_op_typage").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_global',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'typage'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_op_typage").hide();
			
		}
	});
}
function export_dmt_pli_gbl_saisie(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_globale_saisie_"+datyd+"_"+datyf+'.xls';
	$("#preloader_pli_saisie").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_global',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'saisie'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_pli_saisie").hide();	
		}
	});
}
function export_dmt_op_gbl_saisie(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_globale_saisie_"+datyd+"_"+datyf+'.xls';
	$("#preloader_op_saisie").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_global',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'saisie'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_op_saisie").hide();	
			
		}
	});
}
function export_dmt_pli_gbl_controle(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_globale_controle_"+datyd+"_"+datyf+'.xls';
	$("#preloader_pli_controle").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_global',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'controle'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_pli_controle").hide();	
		}
	});
}
function export_dmt_op_gbl_controle(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_globale_controle_"+datyd+"_"+datyf+'.xls';
	$("#preloader_op_controle").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_global',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'controle'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_op_controle").hide();	
		}
	});
}

function export_dmt_gbl_gbl_global(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_globale_"+datyd+"_"+datyf+'.xls';
	$("#preloader_global_global_global").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/ajax_global_global',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'1'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_global_global_global").hide();	
		}
	});
}
/*
function export_dmt_globalpli(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_globale_pli_"+datyd+"_"+datyf+'.xls';
	$("#preloader_globalpli").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_globalpli',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'controle'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_globalpli").hide();	
		}
	});
}

function export_dmt_globalop(){
	var select_soc  	= $("#select_soc").val();
    var select_op  	    = $("#select_op").val();
    var select_typo     = $("#select_typo").val();  
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
	
	var elm  = date_debut_ttt.split("/");
	var elm1 = date_fin_ttt.split("/");
	

	var datyd = date_debut_ttt.replace(/\//gm, '');
	var datyf = date_fin_ttt.replace(/\//gm, '');
	
	var fichier  = "Dmt_globale_agent_"+datyd+"_"+datyf+'.xls';
	$("#preloader_globalop").show();	
	 $.ajax({
		"url": chemin_site+'/dmt/dmt/export_dmt_globalop',
		"type": "POST",
		"data":{select_soc:select_soc,select_op:select_op,select_typo:select_typo,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,action_ttt:'controle'},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_globalop").hide();	
		}
	});
}

*/

function get_data(){
	
	hr_typage = $("#hr_typage").val();  
	hr_saisie = $("#hr_saisie").val();  
	hr_controle = $("#hr_controle").val();  
	var total = 0;
	if(hr_typage)
	{
	  $("#div_hr_typage").html(hr_typage);	
	  total += parseFloat(hr_typage);
	}
	if(hr_saisie)
	{
	  $("#div_hr_saisie").html(hr_saisie);	
	  total += parseFloat(hr_saisie);
	}
	if(hr_controle)
	{
	 $("#div_hr_controle").html(hr_controle);	
	 total += parseFloat(hr_controle);
	}
	var tot = total;
	var locale = 'fr';
	var options = {style: 'decimal',  minimumFractionDigits: 3, maximumFractionDigits: 3};
	var formatter = new Intl.NumberFormat(locale, options);
	
	$("#div_hr_totale").html(formatter.format(tot));
	copy_bloc();

}

function copy_bloc(){
	//temps de production
	//reproduire l'affichage de l'onglet typage dans les onglets saisie et controle
	$("#global_saisie").html($("#global_typage").html());
	$("#global_controle").html($("#global_typage").html());
	
}

function deconnexion(){
    swal({
        title: "CONFIRMATION",
        text: "Deconnexion",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: true
    }, function () {
        window.location.href = url_site +'/login';
    });
}