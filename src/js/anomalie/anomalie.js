(function ($) {
 "use strict";
 
	
	$("#date_debut").datepicker({
		/*todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,*/format: 'dd/mm/yyyy'
	});
	
	 $("#date_fin").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	
	$("#date_debut_ttt").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	
	 $("#date_fin_ttt").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	  /*$("#tab1").show();
	  $("#tab2").show();
	  $("#tab3").show();
	  $("#tab4").show();
	  $("#tab5").show();
	  */
	 load_societe();
	 load_statut_extract();
     load_mode_paiement();
     load_stat_saisie();

})(jQuery); 
function rech(){
    /*if(window.event.keyCode == 13){
        recherche_doc();
    }*/
}



function charger_statut(){ 	 
	
    $.post(url_site+'/anomalie/anomalie/visu_anomalie', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
            $("#statut").append(from_server).fadeIn(400);			
			
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}

function reinitialiser(){
	 load_societe();
	 load_statut_extract();
     load_mode_paiement();
     load_stat_saisie();

}

function charger_detail_plis(){ 
	
    var select_statut  	= $("#select_statut").val();
    var select_soc  	= $("#select_soc").val();
    var select_mp  		= $("#select_mp").val();
    var date_debut  	= $("#date_debut").val();
    var date_fin    	= $("#date_fin").val();
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if(date_debut == '' && date_fin == '' && date_debut_ttt == '' && date_fin_ttt == ''  ){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}	
	if((date_debut == '' && date_fin != '') || (date_debut_ttt == '' && date_fin_ttt != '')  ){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}
	if((date_debut != '' && date_fin == '') || (date_debut_ttt != '' && date_fin_ttt == '')  ){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}
	$("#visu_ano").attr("disabled", "disabled");
	 $.post(url_site+'/anomalie/anomalie/get_anomalie_pli', {select_statut:select_statut,select_soc:select_soc,select_mp:select_mp, date_debut : date_debut, date_fin : date_fin, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt}, function(from_server){ 
		charger_detail_anomalie();
		$("#visu_ano").removeAttr("disabled"); 
		
    });

}
function charger_detail_anomalie(){
   
    var select_statut  	= $("#select_statut").val();
    var select_soc  	= $("#select_soc").val();
    var select_mp  		= $("#select_mp").val();   
    var date_debut  	= $("#date_debut").val();
    var date_fin   		= $("#date_fin").val();
	var date_debut_ttt  = $("#date_debut_ttt").val();
    var date_fin_ttt    = $("#date_fin_ttt").val();  
    var select_statut_saisie  	= $("#select_stat_saisie").val();
	
     dataTable    =  $('#anomalie_pli').DataTable( {
     
         dom: 'Bfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,buttons: [
				/*{
                text: 'CSV',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": chemin_site+'/anomalie/anomalie/export_anomalie_csv',
                        "type": "POST",						"data":{date_debut:date_debut,date_fin:date_fin,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,statut:statut},
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'text/csv;charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'Plis_traites.csv');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }
            ,*/ {
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": chemin_site+'/anomalie/anomalie/export_anomalie_excel',
                        "type": "POST",
						"data":{select_statut:select_statut,select_soc:select_soc,select_mp:select_mp,date_debut:date_debut,date_fin:date_fin,date_fin_ttt:date_fin_ttt,date_debut_ttt:date_debut_ttt,stat_saisie :select_statut_saisie},
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'Extraction.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }]
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": chemin_site+'/anomalie/anomalie/ajax_detail_anomalie'
            ,"data": {select_statut:select_statut,select_soc:select_soc,select_mp:select_mp, date_debut : date_debut, date_fin : date_fin, date_debut_ttt : date_debut_ttt, date_fin_ttt : date_fin_ttt, stat_saisie :select_statut_saisie}
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },'columns':[
			 { "data": 'nom_societe' } 
            ,{ "data": 'date_courrier' }
			,{ "data": 'date_numerisation' } 
			,{ "data": 'dt_event' } 
			,{ "data": 'mode_paiement'}
			,{ "data": 'etape' }
			,{ "data": 'etat' }
			,{ "data": 'stat' }
			,{ "data": 'id_lot_saisie' }
			,{ "data": 'nb_mvt'}	
            ,{ "data": 'lot_scan' }
            ,{ "data": 'pli' }
            ,{ "data": 'id_pli'}
            ,{ "data": 'typologie'}
        ],
    });
	$("#visu_ano").removeAttr("disabled"); 
}

function load_societe(){
	
	$.post(url_site+'/statistics/statistics/get_societe', {}, function(from_server){ 
        if(from_server != 0){
			
			$("#select_soc").html (from_server) ;
					
		}
	});
	
}
function load_statut_extract(){
	
	$.post(url_site+'/anomalie/anomalie/get_statut', {}, function(from_server){ 
        if(from_server != 0){
			
			$("#select_statut").html (from_server) ;
					
		}
	});
	
}
function load_mode_paiement(){
	
	$.post(url_site+'/anomalie/anomalie/get_mode_paiement', {}, function(from_server){ 
        if(from_server != 0){
			
			$("#select_mp").html (from_server) ;
					
		}
	});
	
}

//load stat saisie
function load_stat_saisie(){
	
	$.post(url_site+'/anomalie/anomalie/get_stat_saisie', {}, function(from_server){ 
        if(from_server != 0){
			
			$("#select_stat_saisie").html (from_server) ;
					
		}
	});
	
}


function deconnexion(){
    swal({
        title: "CONFIRMATION",
        text: "Deconnexion",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: true
    }, function () {
        window.location.href = url_site +'/login';
    });
}