$(function(){
	init_nsprogress();
});

function init_nsprogress(){
    $( document ).ajaxStart(function() {
        NProgress.start();
    });

    $( document ).ajaxComplete(function() {
        NProgress.done();
    });

    $(document).ajaxSuccess(function() {
        NProgress.done();
    });
}
$('.users .typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
    },
    {
    name: 'states',
    source: substringMatcher(users)
});

$('.statpli').dropdown({
    searchNoData: '<li style="color:#ddd">Aucun résultats</li>',
    input: '<input type="text" maxLength="20" placeholder="Rechercher....">',
});

$('.typopli').dropdown({
    searchNoData: '<li style="color:#ddd">Aucun résultats</li>',
    input: '<input type="text" maxLength="20" placeholder="Rechercher....">',
});