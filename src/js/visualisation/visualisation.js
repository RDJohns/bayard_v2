$(function(){

    $("#to-accordion1").on("click", function () {
        //alert('test');
        if($("#collapse4").hasClass('in')){
            $("#to-accordion1 .dir-arrow i").removeClass('fa-angle-down');
            setTimeout(function () {
                $("#to-accordion1 .dir-arrow i").addClass('fa-angle-up');
            }, 200);
        }
        else{

            $("#to-accordion1 .dir-arrow i").removeClass('fa-angle-up');
            setTimeout(function () {
                $("#to-accordion1 .dir-arrow i").addClass('fa-angle-down');
            }, 200);
        }
    });

    $("#to-accordion2").on("click", function () {
        //alert('test');
        if($("#collapse5").hasClass('in')){
            $("#to-accordion2 span i").removeClass('fa-angle-down');
            setTimeout(function () {
                $("#to-accordion2 span i").addClass('fa-angle-up');
            }, 200);
        }
        else{

            $("#to-accordion2 span i").removeClass('fa-angle-up');
            setTimeout(function () {
                $("#to-accordion2 span i").addClass('fa-angle-down');
            }, 200);
        }
    });

});

var plitable = null ;

var init_table = 0;

/*var html_preloader = '<div id="absolute30" style="text-align: center">' +
    '                   <span></span><span></span><span></span><span></span><span></span>' +
    '                 </div>';*/

var html_preloader = '<!--div class="progress">' +
    '                   <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100">' +
    '                   </div>' +
    '                 </div-->';

function rech(){
    if(window.event.keyCode == 13){
        recherche_doc();
    }
}

function recherche_doc(){
    var numcommande     = $("#numcommande").val();
    var numclient       = $("#numclient").val();
    var nom_client      = $("#nom_client").val();
    var prenom_client   = $("#prenom_client").val();
    var numtel          = $("#numtel").val();
    var codepostal      = $("#codepostal").val();
    var statut          = $("#statut").val();
    var date_deb        = $("#date_deb").val();
    var date_fin        = $("#date_fin").val();

    if(date_deb != ''){
        deb = date_deb.split('/');
        date_deb = deb[2]+'-'+deb[1]+'-'+deb[0];
    }

    if(date_fin != ''){
        if(date_deb != ''){
            fin = date_fin.split('/');
            date_fin = fin[2]+'-'+fin[1]+'-'+fin[0];
        }
        else{
            alert('Veuillez renseigner correctement les dates');
            return false;
        }
    }

    //$("#result-rsrch").html('').show();
    //$("#result-rsrch").html(html_preloader);
    animate_progress();
    $("#doc-row").html('').fadeOut(300);

    if(numcommande == '' && numclient == '' && nom_client == '' && prenom_client == '' && numtel =='' && codepostal == '' && statut == '' && date_deb == '' && date_fin ==''){
        $("#result-rsrch").html('');
        $("#error_message").show();
        setTimeout(function(){
            $("#error_message").fadeOut(1000);
        }, 2000);

        return false;

    }

    plidata = {
        numcommande:numcommande,
        numclient:numclient,
        nom_client:nom_client,
        prenom_client:prenom_client,
        numtel:numtel,
        codepostal:codepostal,
        statut:statut,
        date_deb:date_deb,
        date_fin:date_fin
    };
	
	/*var table1 = $('#pli-table').DataTable();
 
	$('#bt_search').on( 'click', function () {
		table1.destroy();
	} );
	*/
	
    $('#pli-table').DataTable({
		dom: 'Bfrtip'
		,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100],[5,10,25,50,100]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": false
		,buttons: [
            {
                text: 'CSV',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": chemin_site+'/visualisation/visualisation/export_datatable_csv',
                        "type": "POST",
                        "data": plidata,
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'text/csv;charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'Ged_willemse_visualisation.csv');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }
			, {
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": chemin_site+'/visualisation/visualisation/export_datatable_excel',
                        "type": "POST",
                        "data": plidata,
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'Ged_willemse_visualisation.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
		    }
		]
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
        }
        ,"ajax":{
            //'dataType': 'json',
            'url': chemin_site+'/visualisation/visualisation/recherche_pli'
            ,"data": plidata
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //alert('test');
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        }
        ,'columns':[
            { "data": 'date_courrier' }
            ,{ "data": 'lot_scan'}
            ,{ "data": 'pli'}
            ,{ "data": 'document'}
            ,{ "data": 'num_commande' }
            ,{ "data": 'num_client' }
            ,{ "data": 'nom_client' }
            ,{ "data": 'prenom_client' }
            ,{ "data": 'tel_client' }
            ,{ "data": 'code_postal' }
            ,{ "data": 'statut' }
            ,{ "data": 'visualiser' }
        ]
    });
	
    $("#to-accordion1").trigger('click');

    if(!($("#collapse5").hasClass('in'))){
        $("#to-accordion2").trigger('click');
    }

}

function afficher_pli(id_pli, id_doc) {

    $("#info-pli").html('').fadeOut(300);

    $.post(url_site+'/visualisation/visualisation/affiche_pli', {id_pli:id_pli}, function(from_server){
        if(from_server != 0){
            $("#info-pli").append(from_server).fadeIn(400);
        }else{
            Lobibox.notify('error', {
                title: 'Résultat',
                sound: false,
                msg: ' Aucun pli trouvé.'
            });
        }
    });

    afficher_doc(id_doc);

}

function afficher_doc(id_doc) {

    $("#doc-row").fadeIn(300);
    $("#doc-row").html('');
    $("#doc-row").html(html_preloader);

    //ajuster_ecran();
	$("#doc_"+id_doc).attr("disabled", "disabled");
    $.post(url_site+'/visualisation/visualisation/affiche_doc', {
        id_doc:id_doc,
        //bool:bool,
    }, function(from_server){
        if(from_server != 0){
			$("#doc_"+id_doc).removeAttr("disabled");  
            $("#doc-row").html('');
            $("#doc-row").append(from_server);
            //ajuster_ecran();
            setTimeout(function () {
                $("#panel-info").slideDown("slow");
            }, 300);
            $('html, body').animate({
                scrollTop: $("#doc-row").offset().top
            }, 1500);
            $('[data-magnify=gallery]').magnify({
                /*multiInstances: false
                ,*/footToolbar: [/*'prev',*/'zoomIn','zoomOut','fullscreen'/*,'actualSize'*/,'rotateRight','rotateLeft'/*,'next'*/]
                //,fixedModalPos: true
            });
        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun pli trouvé.'
            });
			$("#doc_"+id_doc).removeAttr("disabled");  
        }
        console.log(from_server);
	
    });

}

function ajuster_ecran(){
    var hWindow = $(window).height();

    var taille_scroll = (hWindow * 37) / 100;

    //alert(taille_scroll);

    $("#recto").height(taille_scroll+'px');
    $("#verso").height(taille_scroll+'px');
    $("#panel-info").height(taille_scroll+'px');

    /*if(init_table == 0){
        plitable.destroy();

       plitable = $('.pli-table').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            paginate: false,
            scrollY : 200, //122
            scrollCollapse : true,
            "language": {
                "lengthMenu": "Afficher _MENU_ records par page",
                "zeroRecords": "Aucun résultat trouvé",
                "sInfo": "Affichage de _START_ à _END_ sur _TOTAL_ enregistrements",
                "infoEmpty": "Aucun enregistrement",
                "infoFiltered": "(filtré de _MAX_ enregistrements)",
                "sSearch" : "Chercher",
                "oPaginate" : {
                    "sPrevious" : "Précédent",
                    "sNext" : "Suivant"
                }
            },
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        init_table = 1;

    }*/

    /*$("div.dataTables_scrollBody").height('122px');
    /*$("div.dataTables_scrollHeadInner").css('padding-right','17px');*/

}

function animate_progress(){
    var width = 0;

    if(width <= 90){
        $(".progress-bar").width(width+'%');
        width++;
    }
}

//ajax pour recuperer la liste des productions
function recherche_prod(){
    var idplis = $("#idplis").val();
    var plis = $("#plis").val();
    var lotscan = $("#lotscan").val();
    var traitePar   = $("#traitePar").val();
    var statutPlis =  $("#statut").val();
    var typePlis =  $("#typologie").val();
    var typePar  = $("#typePar").val();
    var debut      = $("#date_deb_prod").val();
    var fin      = $("#date_fin_prod").val();
    var ttr_date      = $("#date_ttr").val();
    var ttr_date_fin      = $("#date_fin_ttr").val();

    //controler les datesdeb et fin
    if(debut != ''){
        deb = debut.split('/');
        debut = deb[2]+'-'+deb[1]+'-'+deb[0];
    }

    if(fin != ''){
        find = fin.split('/');
        fin = find[2]+'-'+find[1]+'-'+find[0];
       
    }
   
    if(ttr_date != ''){
        dt_ttr = ttr_date.split('/');
        ttr_date = dt_ttr[2]+'-'+dt_ttr[1]+'-'+dt_ttr[0];
    }

    if(ttr_date_fin != ''){
        dt_ttr_fun = ttr_date_fin.split('/');
        ttr_date_fin = dt_ttr_fun[2]+'-'+dt_ttr_fun[1]+'-'+dt_ttr_fun[0];
    }
    $("#result-rsrch").html('').show();
    $("#result-rsrch").html(html_preloader);
    animate_progress();
    $("#doc-row").html('').fadeOut(300);
    var testChamps = true;
    var testVide = true;
    var testVideTtr = true;
    //tester les champs vides
    testChamps = verifierChamps(idplis ,plis ,typePlis ,traitePar ,typePar ,lotscan ,statutPlis);
    //tester les dates courriers
    if ((debut !='' && fin == '') || (debut =='' && fin != '')) {
        testVide = false;
    } else if((debut =='' && fin == '')) {
        testVide = false;
    } else {
        testVide = true;
    }
    
    //tester les dates traitements
    if ((ttr_date !='' && ttr_date_fin == '') || (ttr_date =='' && ttr_date_fin != '')) {
        testVideTtr = false;
    } else if((ttr_date =='' && ttr_date_fin == '')) {
        testVideTtr = false;
    } else {
        testVideTtr = true;
    }

    if (testVide || testVideTtr || testChamps) {
    
        $("#bt_production").attr("disabled", "disabled");
        $.post(url_site+'/visualisation/visualisation/recherche_prod', {
            idplis:idplis,
            plis:plis,
            lotscan:lotscan,
            traitePar:traitePar,
            statutPlis:statutPlis,
            typePlis:typePlis,
            typePar:typePar,
            datedeb:debut,
            datefin:fin,
            dateTtr:ttr_date,
            dateTtrFin:ttr_date_fin
        }, function(from_server){
            if(from_server != 0){
            // $("#result-rsrch").html('');
            $("#bt_production").removeAttr("disabled");  
                $("#result-rsrch").append(from_server).fadeIn(400);

                if(!($("#collapse5").hasClass('in'))){
                    $("#to-accordion2").trigger('click');
                }

                $('.pli-table').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    scrollCollapse : true,
                    aoColumnDefs: [
                        { 'bSortable': false, 'aTargets': [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ] }
                    ],
                    buttons: [
                        'csv', 'excel'
                    ]
                });
                init_table = 0;
            } else {
                $("#result-rsrch").html('');
                Lobibox.notify('error', {
                    //delay: false,
                    title: 'Résultat',
                    sound: false,
                    msg: ' Aucun pli trouvé.'
                });

            }
        });

    } else {
        $("#result-rsrch").html('');
            $("#error_message").show();
            setTimeout(function(){
                $("#error_message").fadeOut(1000);
            }, 2000);
    }
}

//afficher details du pli
function afficher_histo(id_pli) {
    $("#doc-row").fadeIn(300);
    $("#doc-row").html('');
    $('.wiew_detail').attr("disabled", "disabled");
    $.post(url_site+'/visualisation/visualisation/detailler_pli',{
        id_pli:id_pli
    } ,function(data){
        if(data != 0) {
            $('.wiew_detail').removeAttr("disabled"); 
            $("#doc-row").html('');
            $("#doc-row").append(data);
            ajuster_ecran();
            setTimeout(function () {
                $("#panel-info").slideDown("slow");
            }, 300);
            //mettre le scroll dans le div #doc_row
            $('html, body').animate({
                scrollTop: $("#doc-row").offset().top
            }, 1500);
            $('.detail_plis').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                scrollCollapse : true,
                aoColumnDefs: [
                    { 'bSortable': false, 'aTargets': [ 0, 1, 2, 3, 4, 5, 6, 7 ] }
                ],
                buttons: [
                    'csv', 'excel'
                ]
            });
            init_table = 0;
        }
    });
}

//function qui affiche l'image  caché
$('.aficherImg').on('click', function() {
    idimg = $(this).attr('img');
    $('#'+idimg).trigger('click');
});



var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
      var matches, substringRegex;
  
      // an array that will be populated with substring matches
      matches = [];
  
      // regex used to determine if a string contains the substring `q`
      substrRegex = new RegExp(q, 'i');
  
      // iterate through the pool of strings and for any string that
      // contains the substring `q`, add it to the `matches` array
      $.each(strs, function(i, str) {
        if (substrRegex.test(str)) {
          matches.push(str);
        }
      });
  
      cb(matches);
    };
  };
  
  //fonction pour vérifier les champs vides
 function verifierChamps(idplis, plis ,typePlis ,traitePar ,typePar ,lotscan ,statutPlis) {
    if(idplis == '' && plis == '' && typePlis == '' && traitePar == '' && typePar == ''  && lotscan == '' && statutPlis == '') {
        setTimeout(function(){
            $("#error_message").fadeOut(1000);
        }, 2000);
               return false;
    } else {
        return true;
    }
 }

 //Initialisation des champs un à un
 $('.init').on('click',function(){
    //recupérer l'attribut flag pour distinguer les champs
    var idinput = $(this).attr('flag');
        //si le champ est de type date
        if (idinput == "date_deb_prod") {
            viderUnChamp(idinput);
            viderUnChamp("date_fin_prod");
        } else if (idinput == "date_ttr") {
            viderUnChamp(idinput);
            viderUnChamp("date_fin_ttr");
        } else {
            viderUnChamp(idinput);
        }
})

 //fonction pour vider un champ
 function viderUnChamp(champs) {
    if ($('#'+champs).val() != "") {
        $('#'+champs).val("");
    }
 }
 
 //array des champs à vider
 var input = ["idplis", "plis", "traitePar", "typePar", "lotscan", "date_deb_prod", "date_ttr"];
 //evenement sur le boutons initialiser les champs
 //seules les champs contenant des informations peuvent être vidé à part les select box qui ont déjà un pti croix pour vider.
 $('.viderChamps').on('click', function(){
    var ref = 0;
    var show = 0;
    //parcourir les inputs
    $(".input_theme").each(function(){
        idField = $(this).attr('id');
        //vérifier si le champ parcouru appartient à l'array input 
        if (jQuery.inArray( idField, input ) != -1 && $('#'+idField).val() == '') {
            ref = 0;
            $( "a[flag='"+idField+"']" ).addClass("hide");
        } else if(jQuery.inArray( idField, input ) != -1 && $('#'+idField).val() != '') {
            $( "a[flag='"+idField+"']" ).removeClass("hide");
            show = 1;
        }
    })

    if (show==0) {
        $( ".infoChamp" ).removeClass("hide");
        //$('.allfield, .divider').addClass("hide");
    } else if (show==1){
        $( ".infoChamp" ).addClass("hide");
       // $('.allfield, .divider').removeClass("hide");
    }
 })
