(function ($) {
 "use strict";
 
	
	$("#date_debut").datepicker({
		/*todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,*/format: 'dd/mm/yyyy'
	});
	
	 $("#date_fin").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	
	$("#date_debut_ttt").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	
	 $("#date_fin_ttt").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	  /*$("#tab1").show();
	  $("#tab2").show();
	  $("#tab3").show();
	  $("#tab4").show();
	  $("#tab5").show();
	  */
	 load_societe();
	// load_statut_extract();
	 //load_mode_paiement();
	 load_operateur();
	 load_typologie();
	
	$("#preloader_ind_global").hide();
	$("#preloader_ind_detail").hide();
	$("#preloader_global_export").hide();
	$("#preloader_detail_export").hide();
	$("#preloader_export_controle").hide();
	$("#preloader_global_global").hide(); 
	$("#preloader_global_global_global").hide(); 
	$("#preloader_globalpli").hide(); 
	$("#preloader_globalop").hide(); 
	$("#preloader_ind_op").hide(); 
	$("#tab_export").hide(); 
	$("#preloader_op_export").hide(); 
	
	$('.navbar-right .dropdown-menu .body .menu').slimscroll({
		height: '65vh',
		color: 'rgba(0,0,0,0.5)',
		size: '4px',
		alwaysVisible: false,
		borderRadius: '0',
		railBorderRadius: '0'
	});
	
})(jQuery); 


		
function rech(){
    /*if(window.event.keyCode == 13){
        recherche_doc();
    }*/
}
function toogle_typage(i){
	
	if($("#date_typage_"+i).is(':hidden') == true) $("#date_typage_"+i).show();
     else	$("#date_typage_"+i).hide();
}
function toogle_saisie(i){
	
	if($("#date_saisie_"+i).is(':hidden') == true) $("#date_saisie_"+i).show();
     else	$("#date_saisie_"+i).hide();
}

var toggle_table = function(i,etape) {
$('div#date_'+etape+'_' + i).toggle();
$('div#detail' + i).toggleClass('in');
};

function toogle_controle(i){
	
	if($("#date_controle_"+i).is(':hidden') == true) $("#date_controle_"+i).show();
     else	$("#date_controle_"+i).hide();
}

function charger_statut(){ 	 
	
    $.post(url_site+'/indicateur/indicateur/visu_indicateur', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
            $("#statut").append(from_server).fadeIn(400);			
			
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

}


function charger_indicateur_prod(){
   
    var select_soc  	= $("#select_soc").val();  
	var date_debut	    = $("#date_debut").val();
    var date_fin        = $("#date_fin").val();  
    var granularite      = $("#select_granu").val();  
	
	
	if(date_debut == '' || date_fin == '' ){
		
		$("#error_message").html("Veuillez sélectionner les dates !");
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		return false;

	}
	if(granularite == '' ){
		
		$("#error_message").html("Veuillez sélectionner la granularité !");
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		return false;

	}
	/*if(select_soc == ''  ){
		
		$("#error_message").html("Veuillez sélectionner la société !");
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		return false;

	}*/
		
	$("#visu_indicateur").attr("disabled", "disabled"); 	
	$("#preloader_ind_global").show();	
	//$("#down_global").css("color", "#343434"); 
	$("#down_detail").css("color", "#cccccc"); 
	dataTable    =  $('#indicateur_pli').DataTable( {
     
         dom: 'Bfrtip'
        ,"bProcessing": true
		,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
		/*,"ordering": true*/
		,"order": []
		/*,"columnDefs": [{targets: [ 0 ],orderData: [ 0, 1 ]}, {targets: [ 1 ],orderData: [ 0, 1 ]}]*/
		,buttons: []		
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "De _START_ \340 _END_ sur _TOTAL_"
			,"sInfoEmpty": "De 0 à 0 of 0 enregistrements"
            ,"sInfoFiltered": " enregistrements"			
            /*,"sInfoFiltered": "(filtrés de _MAX_ enregistrements)"	*/		
            ,"sProcessing":  $('#preloader_ind_global')[0].outerHTML.replace(/display\: none;/gm, '')		
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": chemin_site+'/indicateur/indicateur/ajax_indicateur_global'
            ,"data": {select_soc:select_soc, date_debut : date_debut, date_fin : date_fin, granularite :granularite}
            ,'type': 'POST'
			,'async': 'false'
			,"complete" : function(resultat, statut){
				$("#preloader_ind_global").hide();		 				
				charger_pli_par_op();
				
			}
			
        },
		initComplete: function() {
			$("#indicateur_pli_filter input").unbind();
			$("#indicateur_pli_filter input").bind("keyup", function(e) {
				if(e.keyCode == 13) {
					dataTable.search(this.value).draw(); 
					$("#visu_indicateur").removeAttr("disabled"); 
				}
			});
		}
        ,"fnDrawCallback": function( oSettings ) {
            $("#preloader_ind_global").hide();	
			$("#visu_indicateur").removeAttr("disabled"); 			
        } 
		,'columns':[
			 { "width": "20%","data": 'date_reception' }
			,{ "width": "10%","data": 'recu' } 
			,{ "width": "10%","data": 'typage' } 
            ,{ "width": "10%","data": 'saisie' }
            ,{ "width": "10%","data": 'controle' }
            ,{ "width": "10%","data": 'termine' }
			,{ "width": "10%","data": 'detail'}				
						
        ],
    });
	$('.dataTables_wrapper .dataTables_processing').css('margin-top','-50px');
	$('.dataTables_wrapper .dataTables_processing').css('font-size','16px');
	
					
	
}

function charger_detail(date){  
	var select_soc  = $("#select_soc").val();
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();  
    var granularite   = $("#select_granu").val();  
	
	$("#preloader_ind_detail").show();	
	if(date_debut != '' && date_fin == ''  ){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}
	
	
	
	$("#visu_indicateur").attr("disabled", "disabled"); 
	$("#down_detail").css("color", "#343434"); 
	//$("#down_global").css("color", "#cccccc"); 
	$("#date_detail_export").val(date);
	
    dataTableDetail    =  $('#indicateur_detail_pli').DataTable( {
     
         dom: 'Bfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,buttons: []
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "De _START_ \340 _END_ sur _TOTAL_"
			,"sInfoEmpty": "De 0 à 0 of 0 enregistrements"
            ,"sInfoFiltered": " enregistrements"
			/*,"sInfoFiltered": "(filtrés de _MAX_ enregistrements)"*/
			,"sProcessing":  $('#preloader_ind_detail')[0].outerHTML.replace(/display\: none;/gm, '')	
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": chemin_site+'/indicateur/indicateur/ajax_indicateur_detail'
            ,"data": {select_soc:select_soc, date_debut : date_debut, date_fin : date_fin,date_recep : date,granularite : granularite}
            ,'type': 'POST'
			,'async': 'false'
			,"complete" : function(resultat, statut){
				$("#preloader_ind_detail").hide();	
				$("#typage").removeClass("tab-pane fade in active").addClass("tab-pane fade");	
				$("#saisie").removeClass("tab-pane fade").addClass("tab-pane fade in active");	

				$("#div1").removeClass("active").addClass("");	
				$("#div2").removeClass("").addClass("active");
				
				}
        },
		initComplete: function() {
			$("#indicateur_detail_pli_filter input").unbind();
			$("#indicateur_detail_pli_filter input").bind("keyup", function(e) {
				if(e.keyCode == 13) {
					dataTableDetail.search(this.value).draw();
					$("#visu_indicateur").removeAttr("disabled"); 
					$("#typage").removeClass("tab-pane fade in active").addClass("tab-pane fade");	
					$("#saisie").removeClass("tab-pane fade").addClass("tab-pane fade in active");
					$("#div1").removeClass("active").addClass("");	
					$("#div2").removeClass("").addClass("active");
				}
			});
		}
        ,"fnDrawCallback": function( oSettings ) {
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
			$("#preloader_ind_detail").hide();	
			$("#visu_indicateur").removeAttr("disabled"); 
        },'columns':[
			{ "data": 'id_pli' } 
            ,{ "data": 'typologie'}				
			,{ "data": 'lot_scan'}			
			,{ "data": 'date_typage' }
			,{ "data": 'typage_par' }
			,{ "data": 'date_saisie' }
			,{ "data": 'saisie_par' }
			,{ "data": 'date_controle' }
			,{ "data": 'controle_par' }
			,{ "data": 'retraite_par' }
			,{ "data": 'date_termine' }
			,{ "data": 'date_courrier'}				
            
        ],
    });
	
	$('.dataTables_wrapper .dataTables_processing').css('margin-top','-50px');
	$('.dataTables_wrapper .dataTables_processing').css('font-size','16px');
	$("#daty_courrier").val(date);
				
}


function load_societe(){
	
	$.post(url_site+'/statistics/statistics/get_societe', {}, function(from_server){ 
        if(from_server != 0){
			
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_soc" name="select_soc">';
			    strHtml += '<option value="">-- Société --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_soc").html (strHtml) ;
			$.AdminBSB.select.activate();			
			
					
		}
	});
	
}
function load_statut_extract(){
	
	$.post(url_site+'/indicateur/indicateur/get_statut', {}, function(from_server){ 
        if(from_server != 0){
			
			//$("#select_statut").html (from_server) ;
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_statut" name="select_statut">';
			    strHtml += '<option value="">-- Statut --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_statut").html (strHtml) ;
			$.AdminBSB.select.activate();		
		}
	});
	
}
function load_mode_paiement(){
	
	$.post(url_site+'/indicateur/indicateur/get_mode_paiement', {}, function(from_server){ 
        if(from_server != 0){
			
			//$("#select_mp").html (from_server) ;
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_mp" name="select_mp">';
			    strHtml += '<option value="">-- Mode de paiement --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_mp").html (strHtml) ;
			$.AdminBSB.select.activate();	
					
		}
	});
	
}

function load_operateur(){
	
	$.post(url_site+'/indicateur/indicateur/get_operateur', {}, function(from_server){ 
        if(from_server != 0){
			
			//$("#select_op").html (from_server) ;
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_op" name="select_op">';
			    strHtml += '<option value="">-- Opérateur --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_op").html (strHtml) ;
			$.AdminBSB.select.activate();	
			//$("filter-option").html("-- Opérateur --");		
		}
	});
	
}

function load_typologie(){
	
	$.post(url_site+'/indicateur/indicateur/get_typologie', {}, function(from_server){ 
        if(from_server != 0){
			
			//$("#select_typo").html (from_server) ;
			var strHtml = '<select  data-live-search="true" class="btn-group form-control show-tick" id="select_typo" name="select_typo">';
			    strHtml += '<option value="">-- Typologie --</option>';
			    strHtml += from_server;
			    strHtml += '</select>';
			$(".c-select_typo").html (strHtml) ;
			$.AdminBSB.select.activate();	
			//$("filter-option").html("-- Typologie --");				
		}
	});
	
}

function charger_pli_par_op(){
	
	var select_soc  	= $("#select_soc").val();  
	var date_debut	    = $("#date_debut").val();
    var date_fin        = $("#date_fin").val();  
    var granularite      = $("#select_granu").val();  
	
	
	
	if(date_debut == '' || date_fin == '' ){
		
		$("#error_message").html("Veuillez sélectionner les dates !");
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		return false;

	}
	if(granularite == '' ){
		
		$("#error_message").html("Veuillez sélectionner la granularité !");
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		return false;

	}
			
		
	//$("#visu_indicateur").attr("disabled", "disabled"); 	
	$("#preloader_ind_op").show();	
	$("#tab_export").hide(); 
	dataTableRecapPli    =  $('#indicateur_pli_op').DataTable( {
     
         dom: 'Bfrtip'
        ,"bProcessing": true
		,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
		/*,"ordering": true*/
		,"order": []
		/*,"columnDefs": [{targets: [ 0 ],orderData: [ 0, 1 ]}, {targets: [ 1 ],orderData: [ 0, 1 ]}]*/
		,buttons: []		
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "De _START_ \340 _END_ sur _TOTAL_"
			,"sInfoEmpty": "De 0 à 0 of 0 enregistrements"
            ,"sInfoFiltered": " enregistrements"		
            /*,"sInfoFiltered": "(filtrés de _MAX_ enregistrements)"*/		
            ,"sProcessing":  $('#preloader_ind_op')[0].outerHTML.replace(/display\: none;/gm, '')		
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": chemin_site+'/indicateur/indicateur/ajax_indicateur_pli_op'
            ,"data": {select_soc:select_soc, date_debut : date_debut, date_fin : date_fin, granularite :granularite}
            ,'type': 'POST'
			,'async': 'false'
			,"complete" : function(resultat, statut){
				$("#preloader_ind_op").hide();						
				$("#tab_export").show(); 				
			}
			
        },
		initComplete: function() {
			$("#indicateur_pli_op_filter input").unbind();
			$("#indicateur_pli_op_filter input").bind("keyup", function(e) {
				if(e.keyCode == 13) {
					dataTableRecapPli.search(this.value).draw();
					//$("#visu_indicateur").removeAttr("disabled"); 
				}
			});
		}
        ,"fnDrawCallback": function( oSettings ) {
            //$("#preloader_ind_global").hide();	
			//$("#visu_indicateur").removeAttr("disabled"); 			
        } 
		,'columns':[
			 { "width": "20%","data": 'date_courrier' }
			,{ "width": "10%","data": 'recu' } 
			,{ "width": "10%","data": 'login' } 
			,{ "width": "10%","data": 'typage' } 
            ,{ "width": "10%","data": 'saisie' }
            ,{ "width": "10%","data": 'controle' }
            ,{ "width": "10%","data": 'termine' }
						
						
        ],
    });
	//,{ "width": "10%","data": 'detail'}	
	$('.dataTables_wrapper .dataTables_processing').css('margin-top','-50px');
	$('.dataTables_wrapper .dataTables_processing').css('font-size','16px');
	
					
	
}
function export_indicateur_prod(){ 
	var select_soc  	= $("#select_soc").val();  
	var date_debut	    = $("#date_debut").val();
    var date_fin        = $("#date_fin").val(); 
    var granularite     = $("#select_granu").val(); 
	
	var elm  = date_debut.split("/");
	var elm1 = date_fin.split("/");
	
	var datyd = date_debut.replace(/\//gm, '');
	var datyf = date_fin.replace(/\//gm, '');
	
	var fichier  = "Courriers_du_"+datyd+"_"+datyf+'.xls';
	$("#preloader_global_export").show();	
	 $.ajax({
		"url": chemin_site+'/indicateur/indicateur/export_indicateur_prod',
		"type": "POST",
		"data":{select_soc:select_soc,date_fin:date_fin,date_debut:date_debut,granularite:granularite},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_global_export").hide();	
		}
	});
	
}


function export_indicateur_detail(){
	var select_soc  	= $("#select_soc").val();  
	//var date_debut	= $("#date_debut").val();
	var date_debut	    = $("#daty_courrier").val();
	var date_fin	    = $("#date_fin").val();
	var granularite	    = $("#select_granu").val();
	var date 			= $("#date_detail_export").val();
	if(date_debut == '' ){
		return false;

	}
	var elm  = date_debut.split("/");
	

	var datyd = date_debut.replace(/\//gm, '');
	
	var fichier  = "Courriers_du_"+datyd+"_par_pli.xls";
	$("#preloader_op_typage").show();	
	 $.ajax({
		"url": chemin_site+'/indicateur/indicateur/export_indicateur_detail',
		"type": "POST",
		"data":{select_soc:select_soc,date_debut:date_debut,date_recep : date, date_fin : date_fin,granularite:granularite},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_op_typage").hide();
			
		}
	});
}
function export_indicateur_op(){ 
	var select_soc  	= $("#select_soc").val();  
	var date_debut	    = $("#date_debut").val();
    var date_fin        = $("#date_fin").val(); 
    var granularite     = $("#select_granu").val(); 
	
	var elm  = date_debut.split("/");
	var elm1 = date_fin.split("/");
	
	var datyd = date_debut.replace(/\//gm, '');
	var datyf = date_fin.replace(/\//gm, '');
	
	var fichier  = "Courriers_du_"+datyd+"_"+datyf+"_par_Agent.xls";
	$("#preloader_op_export").show();	
	 $.ajax({
		"url": chemin_site+'/indicateur/indicateur/export_indicateur_op',
		"type": "POST",
		"data":{select_soc:select_soc,date_fin:date_fin,date_debut:date_debut,granularite:granularite},
		"success": function(res, status, xhr) {
			var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
			var csvURL = window.URL.createObjectURL(csvData);
			var tempLink = document.createElement('a');
			tempLink.href = csvURL;
			tempLink.setAttribute('download', fichier);
			document.body.appendChild(tempLink);
			tempLink.click();
			$("#preloader_op_export").hide();	
		}
	});
	
}


function get_data(){
	
	hr_typage = $("#hr_typage").val();  
	hr_saisie = $("#hr_saisie").val();  
	hr_controle = $("#hr_controle").val();  
	var total = 0;
	if(hr_typage)
	{
	  $("#div_hr_typage").html(hr_typage);	
	  total += parseFloat(hr_typage);
	}
	if(hr_saisie)
	{
	  $("#div_hr_saisie").html(hr_saisie);	
	  total += parseFloat(hr_saisie);
	}
	if(hr_controle)
	{
	 $("#div_hr_controle").html(hr_controle);	
	 total += parseFloat(hr_controle);
	}
	var tot = total;
	var locale = 'fr';
	var options = {style: 'decimal',  minimumFractionDigits: 3, maximumFractionDigits: 3};
	var formatter = new Intl.NumberFormat(locale, options);
	
	$("#div_hr_totale").html(formatter.format(tot));
	copy_bloc();

}

function copy_bloc(){
	//temps de production
	//reproduire l'affichage de l'onglet typage dans les onglets saisie et controle
	$("#global_saisie").html($("#global_typage").html());
	$("#global_controle").html($("#global_typage").html());
	
}

function deconnexion(){
    swal({
        title: "CONFIRMATION",
        text: "Deconnexion",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Annuler!",
        showLoaderOnConfirm: true,
        closeOnConfirm: true
    }, function () {
        window.location.href = url_site +'/login';
    });
}