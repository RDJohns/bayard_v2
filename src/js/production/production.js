var glb_var;
(function ($) {
 "use strict";


	$("#date_debut").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	
	 $("#date_fin").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true
		,format: 'dd/mm/yyyy'
	});
	$("#mois_debut").datepicker({
		format: "mm/yyyy",
		viewMode: "months", 
		minViewMode: "months"
	});
	$("#mois_fin").datepicker({
		format: "mm/yyyy",
		viewMode: "months", 
		minViewMode: "months"
	});
	
	  $("#tab1").show();
	  $("#tab2").show();
	  $("#tab3").show();
	  $("#tab4").show();
	  $("#tab5").show();
	 
	

})(jQuery); 
function rech(){
    /*if(window.event.keyCode == 13){
        recherche_doc();
    }*/
}
//function funct_toogle(){
$("#tab_stat").click(function(){
		  $("#tab1").show();
		  $("#tab2").show();
		  $("#tab3").show();
		  $("#tab4").show();
		  $("#tab5").show();
		  $("#tab11").hide();
		  $("#tab12").hide();
		  $("#tab13").hide();
		});

$("#tab_plis").click(function(){
		  
		  $("#tab0").hide();
		  $("#tab01").hide();
		  $("#tab1").hide();
		  $("#tab2").hide();
		  $("#tab3").hide();
		  $("#tab4").hide();
		  $("#tab5").show();
		  $("#tab11").hide();
		  $("#tab12").hide();
		  $("#tab13").hide();
});
$("#tab_tout").click(function(){
		  
		  $("#tab1").show();
		  $("#tab2").show();
		  $("#tab3").show();
		  $("#tab4").show();
		  $("#tab5").show();
		  $("#tab11").hide();
		  $("#tab12").hide();
		  $("#tab13").hide();
});
//}
function get_reception_traitement(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 $("#tab0").show();
	 $("#tab01").show();
}
function get_plis_avec_moyen_de_paiement_j(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 
		
	 $("#tab11").show();
	// alert(date_debut); 
}

function get_plis_avec_moyen_de_paiement_s(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 $("#tab12").show();
		
}
function get_plis_avec_moyen_de_paiement_m(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 $("#tab13").show();
}
function get_plis_cloture_j(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 
		$.post(url_site+'/production/production/get_pli_cloture', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#tab21").html(""); 
            $("#tab21").append(from_server).fadeIn(400);			
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
		
}
function get_plis_cloture_s(){
	 
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 
		$.post(url_site+'/production/production/get_pli_cloture_hebdo', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#tab23").html(""); 
            $("#tab23").append(from_server).fadeIn(400);			
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
}
function get_plis_cloture_m(){
	
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	  $.post(url_site+'/production/production/get_pli_cloture_mensuel', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#tab24").html(""); 
            $("#tab24").append(from_server).fadeIn(400);
			
						
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
	
}
function get_plis_cloture_produit(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 
		 $.post(url_site+'/production/production/get_bdc_volume', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#tab22").html(""); 
            $("#tab22").append(from_server).fadeIn(400);
			
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
	
}
function get_plis_ko_call(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	  $.post(url_site+'/production/production/get_pli_kocall_motif', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#detail_ko_call").html(""); 
            $("#detail_ko_call").append(from_server).fadeIn(400);			
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
	  });
}
function get_plis_ko_j(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 
		$.post(url_site+'/production/production/get_pli_ko_journalier', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#tab31").html(""); 
            $("#tab31").append(from_server).fadeIn(400);			
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
		
}
function get_plis_ko_s(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 
		$.post(url_site+'/production/production/get_pli_ko_hebdo', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#tab32").html(""); 
            $("#tab32").append(from_server).fadeIn(400);			
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
		
}
function get_plis_ko_m(){
	 var date_debut  = $("#date_debut").val();
     var date_fin    = $("#date_fin").val();
	 
		$.post(url_site+'/production/production/get_pli_ko_mensuel', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#tab33").html(""); 
            $("#tab33").append(from_server).fadeIn(400);			
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
		
}
function quitter_tab11(){
	 $("#tab11").hide();
}
function quitter_tab12(){
	 $("#tab12").hide();
}
function quitter_tab13(){
	 $("#tab13").hide(); 
}
function quitter_tab21(){
	 $("#tab21").hide();
}
function quitter_tab22(){
	 $("#tab22").hide();
}
function quitter_tab23(){
	 $("#tab23").hide();
}
function quitter_tab24(){
	 $("#tab24").hide();
}
function quitter_tab31(){
	 $("#tab31").hide();
}
function quitter_tab32(){
	 $("#tab32").hide();
}
function quitter_tab33(){
	 $("#tab33").hide();
}
function detail_ko_call(){
	 $("#detail_ko_call").hide();
}
function quitter_tab0(){
	 $("#tab0").hide();
}
function quitter_tab01(){
	$("#tab01").hide();
}
function quitter_traitement(){
	$("#tab_hebdo").hide();
}
function quitter_traitement_journalier(){
	$("#tab_journalier").hide();
}
function quitter_tab_mensuel_0(){
	$("#tab_menseuel_0").hide();
}
function quitter_pli_mpt_mensuel(){
	$("#tab_pli_mpt_mesuel").hide();
}
function quitter_pli_cloture_mensuel(){
	$("#tab_pli_cloture_mesuel").hide();
}
function quitter_pli_ko_mensuel(){
	$("#tab_pli_ko_mesuel").hide();
}
function quitter_pli_restant_mensuel(){
	$("#tab_pli_restant_mesuel").hide();
}

function charger_production(){
	
    var numcommande = $("#numcommande").val();
    var numclient   = $("#numclient").val();
    var nom_client  = $("#nom_client").val();
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if(numcommande == '' && numclient == '' && nom_client == ''){
		
        if(date_debut == '' && date_fin == '' ){
			$("#error_message").show();
			setTimeout(function(){
				$("#error_message").fadeOut(1000);
			}, 2000);
			
			return false;

		}

    }
	
	$("#stat_rech").attr("disabled", "disabled");
    $.post(url_site+'/production/production/get_production', {numcommande:numcommande, numclient:numclient, nom_client:nom_client, date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			 $("#stat_rech").removeAttr("disabled");     
            $("#result-rsrch").append(from_server).fadeIn(400);
			
			$('.counter').counterUp({
				delay: 10,
				time: 1000
			});	
				
				
				$('.js-basic-example').DataTable({
					responsive: true
				});

				//Exportable table
				$('.js-exportable').DataTable({
					dom: 'Bfrtip',
					responsive: true,
					buttons: [
						 'csv', 'excel'
					]
				});
				
				
				$(function () {
					getMorris('donut', 'donut_chart');
					getMorrisDoc('donut', 'donut_document_chart');
					getMorrisPlisKO('donut', 'donut_plis_ko_chart');
					getMorrisPlisRestant('donut', 'donut_plis_restant_chart');
				});
				
				 $("#tab11").hide();
				 $("#tab12").hide();
				 $("#tab13").hide();
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
			$("#stat_rech").removeAttr("disabled");     
        }
    });

}

function repartition_par_doc(date_debut,date_fin){
	
	$("#donut_chart").html("tableau");
	 /*$.post(url_site+'/production/production/get_stat_pli', {numcommande:numcommande, numclient:numclient, nom_client:nom_client, date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){
            $("#result-rsrch").append(from_server).fadeIn(400);
		}*/
}
function voir_graph(date_debut,date_fin){
	
	$("#donut_chart").html("");
	getMorris('donut', 'donut_chart');
	 /*$.post(url_site+'/production/production/get_stat_pli', {numcommande:numcommande, numclient:numclient, nom_client:nom_client, date_debut : date_debut, date_fin : date_fin}, function(from_server){
        if(from_server != 0){
            $("#result-rsrch").append(from_server).fadeIn(400);
		}*/
}

function getMorris(type, element) {
	
	var datas 		= $("#graph_donut").val();
	var elm 		= datas.split(',colors:');
	
	var data_colors = elm[1];
	var element     = elm[0].split('},{');
	var element_clr = elm[1].split("','");
	
	console.log(data_colors);
	
	var i 			= 0;
	var tb_data  	= new Array();
	var tb_color 	= new Array();
	var label   	= "";
	var valeur  	= "";
	var couleur 	= "";
	
	for(i ; i<element.length ; i++){
		
		var elm_donut 	= element[i].replace("[{","" );
		elm_donut 	  	= elm_donut.replace("}]", "");
		
		
		var data_donut  = elm_donut.split(",");
		var data_label  = data_donut[0];
		var data_value  = data_donut[1];
		var data_lbl    = data_label.split("'");
		var data_vl     = data_value.split(":");
		
		label           = data_lbl[1];
		valeur          = data_vl[1];		
		
		tb_data.push({'label':label,'value':valeur});		
		
	}
	
	
	for(var j = 0; j<element_clr.length ; j++){
		
		var elm_color  = element_clr[j].replace("['","" );
		elm_color      = elm_color.replace("']","" );
		tb_color.push(elm_color);
	}
	
	if(datas != ''){	
			Morris.Donut({
				element: 'donut_chart',
				data:  tb_data ,
				colors :  tb_color,
				formatter: function (y) { return y + '%'}
			});	
	}
	
}
function getMorrisDoc(type, element) {
	
	var datas 		= $("#graph_donut_doc").val();
	var elm 		= datas.split(',colors:');
	
	var data_colors = elm[1];
	var element     = elm[0].split('},{');
	var element_clr = elm[1].split("','");
	
	console.log(data_colors);
	
	var i 			= 0;
	var tb_data  	= new Array();
	var tb_color 	= new Array();
	var label   	= "";
	var valeur  	= "";
	var couleur 	= "";
	
	for(i ; i<element.length ; i++){
		
		var elm_donut 	= element[i].replace("[{","" );
		elm_donut 	  	= elm_donut.replace("}]", "");
		
		
		var data_donut  = elm_donut.split(",");
		var data_label  = data_donut[0];
		var data_value  = data_donut[1];
		var data_lbl    = data_label.split("'");
		var data_vl     = data_value.split(":");
		
		label           = data_lbl[1];
		valeur          = data_vl[1];		
		
		tb_data.push({'label':label,'value':valeur});		
		
	}
	
	
	for(var j = 0; j<element_clr.length ; j++){
		
		var elm_color  = element_clr[j].replace("['","" );
		elm_color      = elm_color.replace("']","" );
		tb_color.push(elm_color);
	}
	
	if(datas != ''){	
			Morris.Donut({
				element: 'donut_document_chart',
				data:  tb_data ,
				colors :  tb_color,
				formatter: function (y) { return y + '%'}
			});	
	}
	
}

function getMorrisPlisKO(type, element) {
	
	var datas 		= $("#graph_donut_plis_ko").val();
	var elm 		= datas.split(',colors:');
	
	var data_colors = elm[1];
	var element     = elm[0].split('},{');
	var element_clr = elm[1].split("','");
	
	console.log(data_colors);
	
	var i 			= 0;
	var tb_data  	= new Array();
	var tb_color 	= new Array();
	var label   	= "";
	var valeur  	= "";
	var couleur 	= "";
	
	for(i ; i<element.length ; i++){
		
		var elm_donut 	= element[i].replace("[{","" );
		elm_donut 	  	= elm_donut.replace("}]", "");
		
		
		var data_donut  = elm_donut.split(",");
		var data_label  = data_donut[0];
		var data_value  = data_donut[1];
		var data_lbl    = data_label.split("'");
		var data_vl     = data_value.split(":");
		
		label           = data_lbl[1];
		valeur          = data_vl[1];		
		
		tb_data.push({'label':label,'value':valeur});		
		
	}
	
	
	for(var j = 0; j<element_clr.length ; j++){
		
		var elm_color  = element_clr[j].replace("['","" );
		elm_color      = elm_color.replace("']","" );
		tb_color.push(elm_color);
	}
	
	if(datas != ''){	
			Morris.Donut({
				element: 'donut_plis_ko_chart',
				data:  tb_data ,
				colors :  tb_color,
				formatter: function (y) { return y + '%'}
			});	
	}
	
}


function getMorrisPlisRestant(type, element) {
	
	var datas 		= $("#graph_donut_plis_restant").val();
	var elm 		= datas.split(',colors:');
	
	var data_colors = elm[1];
	var element     = elm[0].split('},{');
	var element_clr = elm[1].split("','");
	
	console.log("--- restant ---"+datas);
	
	var i 			= 0;
	var tb_data  	= new Array();
	var tb_color 	= new Array();
	var label   	= "";
	var valeur  	= "";
	var couleur 	= "";
	
	for(i ; i<element.length ; i++){
		
		var elm_donut 	= element[i].replace("[{","" );
		elm_donut 	  	= elm_donut.replace("}]", "");
		
		
		var data_donut  = elm_donut.split(",");
		var data_label  = data_donut[0];
		var data_value  = data_donut[1];
		var data_lbl    = data_label.split("'");
		var data_vl     = data_value.split(":");
		
		label           = data_lbl[1];
		valeur          = data_vl[1];		
		
		tb_data.push({'label':label,'value':valeur});		
		
	}
	
	
	for(var j = 0; j<element_clr.length ; j++){
		
		var elm_color  = element_clr[j].replace("['","" );
		elm_color      = elm_color.replace("']","" );
		tb_color.push(elm_color);
	}
	
	if(datas != ''){	
			Morris.Donut({
				element: 'donut_plis_restant_chart',
				data:  tb_data ,
				colors :  tb_color,
				formatter: function (y) { return y + '%'}
			});	
	}
	
}
function chercher_plis_stat(date_debut,date_fin){
		 $.post(url_site+'/production/production/get_pli', {date_debut : date_debut, date_fin : date_fin, flag : 1}, function(from_server){
            $("#detail_stat").append(from_server).fadeIn(400);
		
    });
}
function charger_traitement_chq(){
	
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
   
    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if(date_debut == '' && date_fin == '' ){
			$("#error_message").show();
			setTimeout(function(){
				$("#error_message").fadeOut(1000);
			}, 2000);
			
			return false;

	}
	    
	$("#stat_rech").attr("disabled", "disabled");
    $.post(url_site+'/production/production/get_chq_multiple', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#stat_rech").removeAttr("disabled");     
            $("#result-rsrch").append(from_server).fadeIn(400);
			
							
				/*$('.js-basic-example').DataTable({
					responsive: true
				});*/

				//Exportable table
				$('.js-exportable').DataTable({
					dom: 'Bfrtip',
					responsive: true,
					paginate: true,
					 scrollCollapse : true,
					"language": {
						"lengthMenu": "Afficher _MENU_ records par page",
						"zeroRecords": "Aucun résultat trouvé",
						"sInfo": "Affichage de _START_ à _END_ sur _TOTAL_ enregistrements",
						"infoEmpty": "Aucun enregistrement",
						"infoFiltered": "(filtré de _MAX_ enregistrements)",
						"sSearch" : "Chercher",
						"oPaginate" : {
							"sPrevious" : "Précédent",
							"sNext" : "Suivant"
						}
					},
					buttons: [
						'csv', 'excel'
					]
				});
				
				
					$("#tab_mensuel").css("display","");
					$("#tab_hebdo").css("display","none");
					$("#tab_journalier").css("display","none");
			
				
				/*buttons: [
						 'csv', 'excel'
					]
				$(function () {
					getMorris('donut', 'donut_chart');
					getMorrisDoc('donut', 'donut_document_chart');
					getMorrisPlisKO('donut', 'donut_plis_ko_chart');
					getMorrisPlisRestant('donut', 'donut_plis_restant_chart');
				});*/			
				 
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
		$("#stat_rech").removeAttr("disabled"); 
        }
    });

}
function charger_typage_chq(){
	
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
   
    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

    if(date_debut == '' && date_fin == '' ){
			$("#error_message").show();
			setTimeout(function(){
				$("#error_message").fadeOut(1000);
			}, 2000);
			
			return false;

	}
	    
	$("#stat_rech").attr("disabled", "disabled");
    $.post(url_site+'/production/production/get_typage_chq', {date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			$("#stat_rech").removeAttr("disabled");     
            $("#result-rsrch").append(from_server).fadeIn(400);
			
							
				/*$('.js-basic-example').DataTable({
					responsive: true
				});*/

				//Exportable table
				var table = $('.js-exportable').DataTable({
					dom: 'Bfrtip',
					responsive: true,
					/*scrollY: "400px", */
					/*scrollX: true, */
					scrollX: '100', 
					scrollCollapse: true,
					paging: true, 
					columnDefs: [	{ width: '5', targets: 0 } , { width: '5', targets: 1 },{ width: '5', targets: 2 } ],
					fixedColumns: { leftColumns: 3 } , 
					"bSort" : false,
					/*dom: 'Bfrtip',
					responsive: true,
					paginate: false,
					scrollY : 400,
					scrollX : true,
					scrollCollapse : true,
					fixedColumns: true ,
					"language": {
						"lengthMenu": "Afficher _MENU_ records par page",
						"zeroRecords": "Aucun résultat trouvé",
						"sInfo": "Affichage de _START_ à _END_ sur _TOTAL_ enregistrements",
						"infoEmpty": "Aucun enregistrement",
						"infoFiltered": "(filtré de _MAX_ enregistrements)",
						"oPaginate" : {
							"sPrevious" : "Précédent",
							"sNext" : "Suivant"
						}
					},
					"aLengthMenu":[[5],[5]],
					"pageLength": 5*/
					buttons: [
						'csv', 'excel'
					]
				});
									
					$("#tab_mensuel").css("display","");
					$("#tab_hebdo").css("display","none");
					$("#tab_journalier").css("display","none");
				
				
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
		$("#stat_rech").removeAttr("disabled"); 
        }
    });

}
function afficher_pli(id_pli, id_doc) {

  /*  $("#info-pli").html('').fadeOut(300);

    $.post(url_site+'/visualisation/visualisation/affiche_pli', {id_pli:id_pli}, function(from_server){
        if(from_server != 0){
            $("#info-pli").append(from_server).fadeIn(400);
        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });

    afficher_doc(id_doc);
*/
}

function afficher_doc(id_doc) {

   /* $("#result-rsrch").html('').fadeOut(300);

    $.post(url_site+'/visualisation/visualisation/affiche_doc', {id_doc:id_doc}, function(from_server){
        if(from_server != 0){
            $("#result-rsrch").append(from_server).fadeIn(400);
            $("#panel-info").slideDown("slow");
        }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
        }
    });
*/
}


function charger_plis_mensuel(){
	
    var date_debut  = $("#mois_debut").val();
    var date_fin    = $("#mois_fin").val();

    $("#result-rsrch").html('').fadeOut(300);
    $("#info-pli").html('').fadeOut(300);

   if(date_debut == '' && date_fin == '' ){
		$("#error_message").show();
		setTimeout(function(){
			$("#error_message").fadeOut(1000);
		}, 2000);
		
		return false;

	}

	$("#stat_rech").attr("disabled", "disabled");
    $.post(url_site+'/production/production/get_stat_pli_mensuel', { date_debut : date_debut, date_fin : date_fin}, function(from_server){ 
        if(from_server != 0){
			
			 $("#stat_rech").removeAttr("disabled");     
            $("#result-rsrch").append(from_server).fadeIn(400);
			
			$('.counter').counterUp({
				delay: 10,
				time: 1000
			});	
				
				
				$('.js-basic-example').DataTable({
					responsive: true
				});

				//Exportable table
				$('.js-exportable').DataTable({
					dom: 'Bfrtip',
					responsive: true,
					aaSorting: [],
					buttons: [
						 'csv', 'excel'
					]
				});
				//aoColumnDefs: null,


				$(function () {
					getMorris('donut', 'donut_chart');
					getMorrisDoc('donut', 'donut_document_chart');
					getMorrisPlisKO('donut', 'donut_plis_ko_chart');
					getMorrisPlisRestant('donut', 'donut_plis_restant_chart');
				});

				 $("#tab11").hide();
				 $("#tab12").hide();
				 $("#tab13").hide();
			
				
		 }else{
            Lobibox.notify('error', {
                //delay: false,
                title: 'Résultat',
                sound: false,
                msg: ' Aucun résultat trouvé.'
            });
			$("#stat_rech").removeAttr("disabled"); 
        }
    });

}



function charger_reb(){
   
    var date_debut  = $("#date_debut").val();
    var date_fin    = $("#date_fin").val();
	 dataTable    =  $('#reb').DataTable( {
       
         dom: 'lBfrtip'
        ,"bProcessing": true
        ,"bServerSide": true
        ,"bScrollCollapse": true
        ,"aLengthMenu":[[5,10,25,50,100, -1],[5,10,25,50,100, "Tous"]]
        ,"pageLength": 10
        ,responsive: true
        ,"bDestroy": true
        ,"bSort": true
        ,buttons: [
				{
                text: 'Excel',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": chemin_site+'/production/production/export_chq_tlmc_excel',
                        "type": "POST",
						"data":{date_debut:date_debut,date_fin:date_fin},
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'text/xls;charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            tempLink.setAttribute('download', 'Cheques_non_reb.xls');
                            document.body.appendChild(tempLink);
                            tempLink.click();
                        }
                    });
                }
            }]
        ,"oLanguage": {
            "sLengthMenu": "_MENU_ par page"
            ,"sZeroRecords": "Aucune donn\351e"
            ,"sEmptyTable": "Aucune donn\351e"
            ,"sInfo": "de _START_ \340 _END_ sur _TOTAL_"
            ,"sInfoFiltered": "(filtrés de _MAX_ total enregistrements)"
        }
        ,"ajax":{
            //'dataType': 'json',
            "url": url_site+'/production/production/ajax_reb'
            ,"data": {date_debut : date_debut, date_fin : date_fin}
            ,'type': 'POST'
        }
        ,"fnDrawCallback": function( oSettings ) {
            //$('[data-toggle="tooltip"]').tooltip();
            //set_actions();
        },'columns':[
             { "data": 'date_courrier' }
            ,{ "data": 'lot_scan' }
            ,{ "data": 'pli' }
            ,{ "data": 'code_type_pli' }
            ,{ "data": 'lib_type' }
            ,{ "data": 'statut' }
            ,{ "data": 'montant'}
            ,{ "data": 'cmc7'}
            ,{ "data": 'date_encaissement'}
            ,{ "data": 'id_pli'}
            ,{ "data": 'id_document'}
            
        ],
    });
	$("#bt_search_chq").removeAttr("disabled"); 
}



