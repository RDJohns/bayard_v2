select 
sum(nb_pli) pli_total,
 sum(nb_mouvement) nb_mouvement,
 sum(stock_typage) stock_typage,
 sum(en_cours_typage) en_cours_typage,
 sum(typage_ok) typage_ok,
 sum(ko_scan) ko_scan,
 sum(hors_peri) hors_peri,
 sum(stock_saisie) stock_saisie,
 sum(en_cours_saisie) en_cours_saisie,
 sum(saisie_ok) saisie_ok,
 sum(saisie_ko)saisie_ko,
 sum(stock_ctrl)stock_ctrl,
 sum(kd)kd,
 sum(ks)ks,
 sum(ctrl_ok)ctrl_ok,
 sum(ctrl_ci)ctrl_ci,
 sum(flag_reb) reb,
 'sep'::text sep,
 sum(mvt_typage_ok) mvt_typage_ok,
 sum(mvt_stock_saisie) mvt_stock_saisie,
 sum(mvt_en_cours_saisie) mvt_en_cours_saisie,
 sum(mvt_saisie_ok) mvt_saisie_ok,
 sum(mvt_saisie_ko)mvt_saisie_ko,
 sum(mvt_saisie_ci) mvt_saisie_ci,
 sum(mvt_stock_ctrl)mvt_stock_ctrl,
 sum(mvt_kd)mvt_kd,
 sum(mvt_ks)mvt_ks,
 sum(mvt_ctrl_ok)mvt_ctrl_ok,
 sum(mvt_ctrl_ci) mvt_ctrl_ci
 
 from (
 SELECT
    1 nb_pli,
    CASE WHEN view_nb_mouvement_pli.nb_mvt IS NULL THEN 0::bigint  ELSE view_nb_mouvement_pli.nb_mvt END AS nb_mouvement,
    case when view_histo_pli_oid.flag_traitement  = 0 or view_histo_pli_oid.flag_traitement is null then 1 else 0 end as stock_typage,
    case when view_histo_pli_oid.flag_traitement  = 1 then 1 else 0 end as en_cours_typage,

    case when view_histo_pli_oid.flag_traitement  = 2 then 1 else 0 end as typage_ok,
    case when view_histo_pli_oid.flag_traitement  = 2 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt  else 0 end as mvt_typage_ok,

    case when view_histo_pli_oid.flag_traitement  = 16 then 1 else 0 end as ko_scan,
    case when view_histo_pli_oid.flag_traitement  = 21 then 1 else 0 end as hors_peri,

    case when view_histo_pli_oid.flag_traitement  = 3 then 1 else 0 end as stock_saisie,
    case when view_histo_pli_oid.flag_traitement  = 3 and view_nb_mouvement_pli.nb_mvt is not null  then  view_nb_mouvement_pli.nb_mvt else 0 end as mvt_stock_saisie,
    
    case when view_histo_pli_oid.flag_traitement  = 4 then 1 else 0 end as en_cours_saisie,
    case when view_histo_pli_oid.flag_traitement  = 4 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_en_cours_saisie,

    case when view_histo_pli_oid.flag_traitement  = 5 and f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 1 then 1 else 0 end as saisie_ok,
    case when view_histo_pli_oid.flag_traitement  = 5 and f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 1 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_saisie_ok,

    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 2 and view_histo_pli_oid.flag_traitement = 17 then 1 else 0 end as saisie_ko,
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 2 and view_histo_pli_oid.flag_traitement = 17 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_saisie_ko,

    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 3 and view_histo_pli_oid.flag_traitement = 23 then 1 else 0 end as saisie_ci,
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 3 and view_histo_pli_oid.flag_traitement = 23 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_saisie_ci,

    case when  view_histo_pli_oid.flag_traitement in(6,7) then 1 else 0 end as stock_ctrl,
    case when  view_histo_pli_oid.flag_traitement in(6,7) and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_stock_ctrl,
    
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 6 and view_histo_pli_oid.flag_traitement = 20 then 1 else 0 end as kd,
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 6 and view_histo_pli_oid.flag_traitement = 20 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_kd,
    
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 4 and view_histo_pli_oid.flag_traitement = 18 then 1 else 0 end as ks,
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 4 and view_histo_pli_oid.flag_traitement = 18 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_ks,
    
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 1 and view_histo_pli_oid.flag_traitement = 9 then 1 else 0 end as ctrl_ok,
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 1 and view_histo_pli_oid.flag_traitement = 9 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_ctrl_ok,
    
    
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 3 and view_histo_pli_oid.flag_traitement = 25 then 1 else 0 end as ctrl_ci,
    case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 3 and view_histo_pli_oid.flag_traitement = 25 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_ctrl_ci,
    
    case when view_pli_en_reb.flag_reb is null then 0 else view_pli_en_reb.flag_reb  end as flag_reb
    FROM f_pli
     LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
     LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.id_pli = f_pli.id_pli
     LEFT JOIN view_nb_mouvement_pli ON view_nb_mouvement_pli.id_pli = f_pli.id_pli
     LEFT JOIN view_pli_en_reb on view_pli_en_reb.id_pli = f_pli.id_pli
     LEFT JOIN data_pli ON data_pli.id_pli = view_histo_pli_oid.id_pli
     LEFT JOIN societe ON societe.id = f_pli.societe
     LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = view_histo_pli_oid.flag_traitement
      where  date_courrier::date between '2019-01-16' and '2019-10-18'  ) as courrier