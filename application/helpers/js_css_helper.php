<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('test_method'))
{
    function test_method($var = '')
    {
        return $var;
    }   
}
if ( ! function_exists('assets_url'))
{
    function assets_url($array_file,$type)
    {
		$timenow = date("YmdHis");
        $res = '';
		if($type == 'js')
		{
			foreach ($array_file as $valuejs)
			{
				$res .= "
					<script src='".base_url("assets/".$valuejs.".js")."?v=".$timenow."' ></script>
				";
			}
		}
		if($type == 'css')
		{
			foreach ($array_file as $valuejs)
			{
				$res .= "
					<link rel='stylesheet' href='".base_url("assets/".$valuejs.".css")."?v=".$timenow."' />
				";
			}
		}
		
		echo $res;
    }   
}
if(!function_exists('execute_js'))
{
    function execute_notification($content)
    {

        $str = ' <script type="text/javascript">location.reload();</script>';

        echo $str;
    }
}

if ( ! function_exists('date_tobd'))
{
    function date_tobd($date = '')
    {
		// 26/04/2017 
		list($jours,$mois,$annee) = explode("/",$date);
		$date_final_bd = $annee.'-'.$mois.'-'.$jours;
		return $date_final_bd;
    }   
}
