<?php
if(!function_exists('menu_header'))
{
    function menu_header($array)
    {

        if(is_array($array))
        {
            $strMenu            = "";

            foreach ($array as $menu)
            {

                $lien           = $menu["lien"];
                $couleur        = $menu["couleur"];
                $titre          = $menu["titre"];
                $commentaire    = $menu["commentaire"];
                $icon           = $menu["icon"];

                $strMenu       .='
                <li>
                    <a href="'.site_url($lien).'">
                        <div class="icon-circle '.$couleur.'">
                            <i class="material-icons">'.$icon.'</i>
                        </div>
                        <div class="menu-info">
                            <h4>'.$titre.'</h4>
                            <p>
                                '.$commentaire.'
                            </p>
                        </div>
                    </a>
                </li>';
            }

            echo $strMenu;
        }
        else
        {
            menu_header_all($array);
        }

    }
}
if ( ! function_exists('datetime_fr'))
{
    function datetime_fr($datetime = '')
    {
        list($date,$time) = explode(" ",$datetime);
        list($annee,$mois,$jour) = explode("-",$date);
        $date_final_fr = $jour.'/'.$mois.'/'.$annee.' '.$time;
        return $date_final_fr;
    }
}

if(!function_exists('menu_header_all'))
{
    function menu_header_all($not)
    {

        $strMenu            = "";
        /**visualisation/fautes */
        $array              = array(
            array
            (
                "lien"=>"recherche/avancee",
                "couleur"=>" bg-light-green",
                "titre"=>"Recherche avancée",
                "commentaire"=>"Recherche avancée sur le courrier et le flux",
                "icon"=>"fa fa-search-plus",
                "niveau"=>"4"
            ),
            array
            (
                "lien"=>"visualisation/fautes",
                "couleur"=>" bg-pink",
                "titre"=>"Visualisation taux de fautes",
                "commentaire"=>"Visualisation taux de fautes",
                "icon"=>"fa fa-table",
                "niveau"=>"4"
            ),
            array
            (
                "lien"=>"saisie/saisie",
                "couleur"=>"bg-light-green",
                "titre"=>"Saisie",
                "commentaire"=>"Aller dans l'interface de saisie de pli",
                "icon"=>"fa fa-print",
                "niveau"=>"2"
            ),
            array
            (
                "lien"=>"typage/typage",
                "couleur"=>"bg-light-blue",
                "titre"=>"Typage pli",
                "commentaire"=>"Aller dans l'interface de typage de pli",
                "icon"=>"fa fa-laptop",
                "niveau"=>"2"
            ),
            array
            (
                "lien"=>"visual/visual",
                "couleur"=>" bg-cyan",
                "titre"=>"Visualisation des courriers",
                "commentaire"=>"Visu détaillée des courriers",
                "icon"=>"fa fa-eye",
                "niveau"=>"4"
            ),
			array
            (
                "lien"=>"visual/visual",
                "couleur"=>" bg-cyan",
                "titre"=>"Visualisation des courriers",
                "commentaire"=>"Visu détaillée des courriers",
                "icon"=>"fa fa-eye",
                "niveau"=>"8"
            )
			,
            /*array
            (
                "lien"=>"statistics/Stat",
                "couleur"=>" bg-brown",
                "titre"=>"Stat. GED",
                "commentaire"=>"Interface de stat GED Mada et GED CE",
                "icon"=>"fa fa-arrow-circle-right",
                "niveau"=>"4"
            ),*/
            array
            (
                "lien"=>"visualisation/visu_push",
                "couleur"=>" bg-deep-purple",
                "titre"=>"Visualisation des MAIL/SFTP",
                "commentaire"=>"Visu détaillée des MAIL/SFTP",
                "icon"=>"fa fa-sign-in",
                "niveau"=>"4"
            ),
			array
            (
                "lien"=>"visualisation/visu_push",
                "couleur"=>" bg-deep-purple",
                "titre"=>"Visualisation des MAIL/SFTP",
                "commentaire"=>"Visu détaillée des MAIL/SFTP",
                "icon"=>"fa fa-sign-in",
                "niveau"=>"8"
            )
			,
            array
            (
                "lien"=>"push/typage/Pli",
                "couleur"=>" bg-teal",
                "titre"=>"Typage flux",
                "commentaire"=>"Aller dans l'interface de typage de flux",
                "icon"=>"fa fa-eye",
                "niveau"=>"5"
            ),
            array
            (
                "lien"=>"push/saisie/saisie_push",
                "couleur"=>" bg-teal",
                "titre"=>"Saisie flux",
                "commentaire"=>"Aller dans l'interface de saisie de flux",
                "icon"=>"fa fa-eye",
                "niveau"=>"5"
            )
           ,
           array
            (
                "lien"=>"reception/pli_flux",
                "couleur"=>"bg-light-green",
                "titre"=>"Réception",
                "commentaire"=>"Suivi des réceptions des plis/flux",
                "icon"=>"fa fa-envelope",
                "niveau"=>"4"
            )          
            ,
           
            array
            (
                "lien"=>"statistics/statistics/stat_mensuel",
                "couleur"=>"bg-teal",
                "titre"=>"Suivi mensuel",
                "commentaire"=>"Réception mensuelle",
                "icon"=>"fa fa-signal",
                "niveau"=>"4"
            )
           ,
           array
            (
                "lien"=>"statistics/statistics/suivi_solde",
                "couleur"=>" bg-pink-cstm",
                "titre"=>"Suivi des soldes",
                "commentaire"=>"Soldes par date de courrier",
                "icon"=>"fa fa-battery-half",
                "niveau"=>"4"
            ),
            array
            (
                "lien"=>"statistics/statistics_flux/stat_traitement",
                "couleur"=>"bg-light-blue",
                "titre"=>"Traitement",
                "commentaire"=>"Suivi des traitements",
                "icon"=>"fa fa-tasks",
                "niveau"=>"4"
            )
           ,
            array
            (
                "lien"=>"anomalie/anomalie/visu_anomalie",
                "couleur"=>" bg-cyan-t",
                "titre"=>"Extraction",
                "commentaire"=>"Extraction des plis",
                "icon"=>"fa fa-file-text",
                "niveau"=>"4"
            ),
            array
            (
                "lien"=>"suivi/especes",
                "couleur"=>" bg-amber",
                "titre"=>"Suivi des espèces",
                "commentaire"=>"Espèces",
                "icon"=>"fa fa-money",
                "niveau"=>"4"
            ),
            array
            (
                "lien"=>"provenance/pli",
                "couleur"=>" bg-green",
                "titre"=>"Gestion des provenances",
                "commentaire"=>"Provenance des enveloppes",
                "icon"=>"fa fa-flag",
                "niveau"=>"4"
            ),
            array
            (
                "lien"=>"suivi/reliquat",
                "couleur"=>" bg-lime",
                "titre"=>"Suivi ch&egrave;ques non REB",
                "commentaire"=>"Ch&egrave;ques non remis en banque",
                "icon"=>"fa fa-list",
                "niveau"=>"4"
            ),
            array
            (
                "lien"=>"suivi/nosaisie",
                "couleur"=>" bg-blue-grey",
                "titre"=>"Suivi ch&egrave;ques non saisis",
                "commentaire"=>"Ch&egrave;ques non saisis à J+3",
                "icon"=>"fa fa-clipboard",
                "niveau"=>"4"
            )
			,
            array
            (
                "lien"=>"dmt/dmt/visu_dmt",
                "couleur"=>" bg-grenat",
                "titre"=>"Suivi des DMT",
                "commentaire"=>"Suivi des DMT",
                "icon"=>"fa fa-hourglass-1",
                "niveau"=>"4"
            ),
            array
            (
                "lien"=>"indicateur/indicateur/visu_indicateur",
                "couleur"=>" bg-light-blue",
                "titre"=>"Indicateurs de production",
                "commentaire"=>"Indicateurs de prod",
                "icon"=>"fa fa-industry",
                "niveau"=>"4"
            ),
            array
            (
                "lien"=>"cheque/etat_cheque",
                "couleur"=>" bg-light-green",
                "titre"=>"Etat des ch&egrave;ques",
                "commentaire"=>"Modification état des chèques",
                "icon"=>"fa fa-money",
                "niveau"=>"8"
            ),
            array
            (
                "lien"=>"matchage/newmatchage",
                "couleur"=>" bg-deep-purple",
                "titre"=>"Matchage",
                "commentaire"=>"Matchage des chèques",
                "icon"=>"fa fa-copy",
                "niveau"=>"1"
            ),
            array
            (
                "lien"=>"cheque/traitement_cheque",
                "couleur"=>" bg-light-green",
                "titre"=>"Traitement des ch&egrave;ques",
                "commentaire"=>"Modification état des chèques",
                "icon"=>"fa fa-money",
                "niveau"=>"1"
            )/*,
            array
            (
                "lien"=>"reception/Pilotage_Flux",
                "couleur"=>"bg-indigo",
                "titre"=>"Pilotage des flux",
                "commentaire"=>"Export excel pilotage des flux",
                "icon"=>"fa fa-file-excel-o",
                "niveau"=>"4"
            )*/


            
        );
        $i = 0;
        
        foreach ($array as $menu)
        {
          
            if(trim($not) != trim($menu["titre"]) && (int)$_SESSION['id_type_utilisateur'] == (int)$menu["niveau"])
            {
                $lien           = $menu["lien"];
                $couleur        = $menu["couleur"];
                $titre          = $menu["titre"];
                $commentaire    = $menu["commentaire"];
                $icon           = $menu["icon"];

                $strMenu       .='
                <li>
                    <a href="'.site_url($lien).'">
                        <div class="icon-circle '.$couleur.'">
                            <i class="'.$icon.'"></i>
                        </div>
                        <div class="menu-info">
                            <h4>'.$titre.'</h4>
                            <p>
                                '.$commentaire.'
                            </p>
                        </div>
                    </a>
                </li>';

                if($not == "Typage pli" && $i == 0)
                {
                    $i++;
                    $strMenu       .='
                    <li>
                        <a onclick="raccourci();">
                            <div class="icon-circle bg-blue">
                                <i class="material-icons">keyboard</i>
                            </div>
                            <div class="menu-info">
                                <h4>Raccourci (Alt+ R) </h4>
                                <p>Liste des raccourcis
                                </p>
                            </div>
                        </a>
                    </li>';
                }
            }

        }

        echo $strMenu;
    }
}