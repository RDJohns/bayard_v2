<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('date_fr_us')){
    function date_fr_us($fr){
        preg_match('#(\d{2})/(\d{2})/(\d{4})#',$fr, $matches);
        if(count($matches)>3){
            return $matches[3].'-'.$matches[2].'-'.$matches[1];
        }
        return '';
    }
}

if(!function_exists('date_us_fr')){
    function date_us_fr($fr){
        preg_match('#(\d{4})-(\d{2})-(\d{2})#',$fr, $matches);
        if(count($matches)>3){
            return $matches[3].'/'.$matches[2].'/'.$matches[1];
        }
        return '';
    }
}

if(!function_exists('date_1_2')){
    function date_1_2($dt1, $dt2){
        $dt_1 = date_fr_us($dt1);
        $dt_2 = date_fr_us($dt2);
        if(empty($dt_1) || empty($dt_2)){
            return array($dt_1, $dt_2);
        }
        return $dt_1 > $dt_2 ? array($dt_2, $dt_1) : array($dt_1, $dt_2);
    }
}

if(!function_exists('get_extension_file_name')){
    function get_extension_file_name($file_name){
        $tab_name = explode('.', $file_name);
        $ext = '';
        if(count($tab_name) > 1){
            $ext = $tab_name[count($tab_name)-1];
        }
        return $ext;
    }
}

if(!function_exists('compose_nom_ci')){
    function compose_nom_ci($id_lot_scan, $id_pli, $pli, $nom_file_orig){
        $id_lot_scan = str_replace(' ', '', $id_lot_scan);
        return $id_lot_scan.'_'.$id_pli.'_'.$pli.'.'.get_extension_file_name($nom_file_orig);
    }
}

if(!function_exists('cmc7s')){
    function cmc7s($cmc7s){
        $tab_rep = array();
        $tab_cmc7 = explode(',', $cmc7s);
        foreach ($tab_cmc7 as $cmc7) {
            array_push($tab_rep, '<span title="'.trim($cmc7).'">'.ellipsize(trim($cmc7), 15, .5).'</span>'); 
        }
        return implode(' / ', $tab_rep);
    }
}

if(!function_exists('comp_page_links')){
    function comp_page_links($a, $b){
        return $a['titre'] > $b['titre'] ? 1 : -1;
    }
}

if(!function_exists('u_montant')){
    function u_montant($montant='0.00'){
        $montant = str_replace(',', '.', ''.$montant);
        $montant = preg_replace('/[^0-9.]/', '',$montant);
        $montant = floatval($montant);
        if(intval($montant) == $montant){return $montant.'.00';}
        return preg_match('/^\d+\.\d{1}$/', ''.$montant) ? $montant.'0' : ''.$montant;
    }
}
