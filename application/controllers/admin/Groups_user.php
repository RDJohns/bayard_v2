<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Groups_user extends CI_Controller{

	public function index(){
		$this->load->library('portier');
		$this->portier->must_admin();
		$this->go();
	}

	public function go(){
		$this->load->library('admin/page');
		$this->page->add_css('../../src/template/css/font-awesome.min');
		$this->page->set_titre('Administration des groupes utilisateur');
		
		//$this->page->add_links('Groupe utilisateur', site_url('admin/groups_user'), 'fa fa-users', 'cyan');
		$this->page->add_links('Administration des utilisateurs', site_url('admin/users'), 'fa fa-user', 'cyan-t');
		$this->page->add_links('Matchage', site_url('matchage/matchage'), 'fa fa-copy', 'deep-purple');
		$this->page->add_links('Matchage ch&egrave;ques cadeaux', site_url('matchage/matchage_ckd'), 'fa fa-gift', 'pink');
		$this->page->add_links('Support', site_url('admin/support'), 'fa fa-cogs', 'light-green');
		$this->page->add_links('Support-push', site_url('admin/support_push'), 'fa fa-upload', 'teal');
		$this->page->add_links('Priorisation des KE', site_url('admin/gestion_ke'),'fa fa-tasks', 'amber');
		$this->page->add_links('DMT', site_url('admin/dmt'), 'fa fa-tachometer', 'purple');
		$this->page->add_links('Qualit&eacute;', site_url('admin/qualite'), 'fa fa-eyedropper', 'lime');
		$this->page->add_links('Regroupements des plis', 'javascript:show_info_grp_pli();', 'fa fa-inbox', 'lime');
		$this->page->add_links('Regroupements des plis <br> par date-courrier', 'javascript:show_info_grp_pli_by_older();', 'fa fa-inbox', 'blue-grey');
		$this->page->add_js('admin/groups_user');
		$this->page->add_css('admin/groups_user');
		$this->page->afficher( $this->load->view('admin/groups_user', array(),TRUE) );
	}

	public function list_group(){
		$correspondance_field = array('tb_gr_u.nom_groupe_utilisateur', '', 'nb_critere', 'nb_u', 'tb_gr_u.actif');
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('admin/model_groups_user');
			$resultat["draw"] = $this->input->post_get('draw');
			$offset = $this->input->post_get('start');
			$limit = $this->input->post_get('length');
			$search = $this->input->post_get('search');
			$arg_find = strtolower($search['value']);
			$order = $this->input->post_get('order');
			$arg_ordo = $correspondance_field[$order[0]['column']];
			$sens = $order[0]['dir'];
			$resultat["iTotalRecords"] = $this->model_groups_user->nb_all_group();
			$resultat["iTotalDisplayRecords"] = $this->model_groups_user->get_groups_user($limit, $offset, $arg_find, $arg_ordo, $sens, NULL);
			$grs = $this->model_groups_user->get_groups_user($limit, $offset, $arg_find, $arg_ordo, $sens, NULL, FALSE);
			$nb_grp_pli_poss = $this->model_groups_user->nb_grp_pli_poss();
			foreach ($grs as $gr) {
				$row = array(
					'nom' => $gr->nom.' <small>[#'.$gr->id.']</small>'
					,'cr' => character_limiter($gr->critere, 1000)/*ellipsize($gr->critere, 150, .5)*/
					,'nb_cr' => ($gr->nb_critere == '' ? 0 : $gr->nb_critere) . ' / ' . $nb_grp_pli_poss
					,'nb_user' => $gr->nb_u == '' ? 0 : $gr->nb_u
					,'actif' => '<span class="label bg-'.($gr->actif == 1 ? 'green">OUI' : 'deep-orange">NON').'</span>'
					,'action' => ''
				);
				$row['action'] = '<div class="icon-button-demo">'
				. '<button type="button" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" onclick="modif_gr(\''.base64_encode($gr->id).'\');" data-toggle="tooltip" data-placement="top" title="Modifier">'
				. '<i class="material-icons">create</i>'
				. '</button>';																		
				$row['action'] .= '<button type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float" onclick="suppr_gr(\''.base64_encode($gr->id).'\', \''.base64_encode($gr->nom).'\');" data-toggle="tooltip" data-placement="top" title="Supprimer">'
				. '<i class="material-icons">delete</i>'
				. '</button></div>';																		
				$row['action'] .= '<button type="button" class="btn btn-info btn-circle waves-effect waves-circle waves-float" onclick="affectation(\''.base64_encode($gr->id).'\', \''.base64_encode($gr->nom).'\');" data-toggle="tooltip" data-placement="top" title="Modifier membre">'
				. '<i class="material-icons">people</i>'
				. '</button></div>';																		
				$row['DT_RowAttr'] = array(
					'my_id_gr' => $gr->id
				);
				$row['DT_RowClass'] = 'gr_tab_line';
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
		echo json_encode($resultat);
	}

	public function get_grp_pli_poss($order_by=NULL){
		$this->load->model('admin/model_groups_user');
		$data = $this->model_groups_user->gr_pli_pos($order_by);
		$options = '';
		$c_grp_opt = '';
		foreach ($data as $option) {
			if ($option->focused != $c_grp_opt) {
				if($c_grp_opt != ''){
					$options .= '</optgroup>';
				}
				$options .= '<optgroup label="'.$option->grp_opt.'">';
				$c_grp_opt = $option->focused;
			}
			$options .= '<option value="'.$option->id_groupe_pli_possible.'">'.$option->opt1.'<small> ['.$option->opt2.']</small>'.'</option>';
		}
		if($c_grp_opt != ''){
			$options .= '</optgroup>';
		}
		echo $options;
	}

	public function new_gr(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if(isset($_REQUEST['b_nom']) && isset($_REQUEST['b_actif']) && isset($_REQUEST['b_grp_pli_poss'])){
			$this->load->model('admin/model_groups_user');
			$nom = base64_decode($this->input->post_get('b_nom'));
			if($this->model_groups_user->exist_nom_grp_user($nom)){
				echo 'Ce nom de groupe existe déjà.';
				log_message('error', 'admin>groups_user>new_gr:le nom du groupe "'.$nom.'" exist déjà');
				return '';
			}
			$actif = base64_decode($this->input->post_get('b_actif'));
			$str_id_grp_pli_poss = base64_decode($this->input->post_get('b_grp_pli_poss'));
			$tab_id_grp_pli_poss = empty($str_id_grp_pli_poss) ? array() : explode(',', $str_id_grp_pli_poss);
			$data_new_grp_user = array(
				'nom_groupe_utilisateur' => $nom
				,'actif' => $actif
			);
			$id_grp_user = $this->model_groups_user->new_grp_user($data_new_grp_user);
			if(is_null($id_grp_user)){
				echo 'Enregistrement échoué.';
				log_message('error', 'admin>groups_user>new_gr:enregistrement échoué');
				return '';
			}
			$data_assignation = array();
			foreach ($tab_id_grp_pli_poss as $id_grp_pli_poss) {
				array_push($data_assignation, array(
					'id_groupe_utilisateur' => $id_grp_user
					,'id_view_groupe_pli_possible' => $id_grp_pli_poss
				));
			}
			$this->model_groups_user->assignation_new($id_grp_user, $data_assignation);
			echo 0;
			return '';
		}
		echo 'Param&egrave;tre manquant';
		log_message('error', 'admin>groups_user>new_gr:parametre manquant');
	}

	public function modif_gr(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if(isset($_REQUEST['b_id']) && isset($_REQUEST['b_nom']) && isset($_REQUEST['b_actif']) && isset($_REQUEST['b_grp_pli_poss'])){
			$this->load->model('admin/model_groups_user');
			$nom = base64_decode($this->input->post_get('b_nom'));
			$id = base64_decode($this->input->post_get('b_id'));
			if($this->model_groups_user->exist_nom_grp_user($nom, $id)){
				echo 'Ce nom de groupe existe déjà.';
				log_message('error', 'admin>groups_user>modif_gr:le nom du groupe "'.$nom.'" exist déjà');
				return '';
			}
			$actif = base64_decode($this->input->post_get('b_actif'));
			$str_id_grp_pli_poss = base64_decode($this->input->post_get('b_grp_pli_poss'));
			$tab_id_grp_pli_poss = empty($str_id_grp_pli_poss) ? array() : explode(',', $str_id_grp_pli_poss);
			$data_grp_user = array(
				'nom_groupe_utilisateur' => $nom
				,'actif' => $actif
			);
			if(!$this->model_groups_user->modif_grp_user($id, $data_grp_user)){
				echo 'Enregistrement &eacute;chou&eacute;.';
				log_message('error', 'admin>groups_user>modif_gr:enregistrement échoué');
				return '';
			}
			$data_assignation = array();
			foreach ($tab_id_grp_pli_poss as $id_grp_pli_poss) {
				array_push($data_assignation, array(
					'id_groupe_utilisateur' => $id
					,'id_view_groupe_pli_possible' => $id_grp_pli_poss
				));
			}
			$this->model_groups_user->assignation_new($id, $data_assignation);
			echo 0;
			return '';
		}
		echo 'Param&egrave;tre manquant';
		log_message('error', 'admin>groups_user>modif_gr:parametre manquant');
	}

	public function data_grp_user($id_grp_u=NULL){
		$this->load->model('admin/model_groups_user');
		$data = $this->model_groups_user->grp_user_assignation_pos($id_grp_u);
		$reponse = new stdClass();
		if(count($data) > 0){
			if($data[0]->supprime == 1){
				$reponse->code = 1;
				$reponse->msg = 'Groupe utilisateur déjà supprimé!';
				echo json_encode($reponse);
				return '';
			}else{
				$reponse->code = 0;
				$reponse->nom = $data[0]->nom_groupe_utilisateur;
				$reponse->actif = $data[0]->actif;
				$tab_id_grp_pli_pos = array();
				foreach ($data as $ind => $line) {
					array_push($tab_id_grp_pli_pos, $line->id_view_groupe_pli_possible);
				}
				$reponse->grps = $tab_id_grp_pli_pos;
				echo json_encode($reponse);
				return '';
			}
		}
		$reponse->code = 1;
		$reponse->msg = 'Groupe utilisateur introuvable!';
		echo json_encode($reponse);
	}

	public function suppr_gr($id_grp){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		$this->load->model('admin/model_groups_user');
		echo $this->model_groups_user->suppr_gr($id_grp) ? '0' : 'Suppression du échoué';
	}

	public function data_grp_user_member($id_grp){
		$this->load->model('admin/model_groups_user');
		$reponse = new stdClass();
		$members = $this->model_groups_user->member_grp($id_grp);
		$tab_members = array();
		foreach ($members as $member) {
			array_push($tab_members, $member->id_utilisateur);
		}
		$reponse->members = $tab_members;
		$operators = $this->model_groups_user->operateur();
		$opt_operators = '';
		foreach ($operators as $operator) {
			$opt_operators .= '<option value="'.$operator->id_utilisateur.'">'.$operator->login.'</option>';
		}
		$reponse->operators = $opt_operators;
		echo json_encode($reponse);
	}

	public function save_member(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if(isset($_REQUEST['b_id_gr']) && isset($_REQUEST['b_members'])){
			$this->load->model('admin/model_groups_user');
			$id = base64_decode($this->input->post_get('b_id_gr'));
			$str_id_members = $this->input->post_get('b_members');
			$tab_id_members = empty($str_id_members) ? NULL : explode(',', $str_id_members);
			echo $this->model_groups_user->set_members($id, $tab_id_members) ? 0 : 'Enregistrement échoué!';
			return '';
		}
		echo 'Param&egrave;ttre manquant';
		log_message('error', 'admin>groups_user>modif_gr:parametre manquant');
	}

}
