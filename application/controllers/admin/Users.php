<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller{

	public function index(){
		$this->load->library('portier');
		$this->portier->must_admin();
		$this->go();
	}

	private function go(){
		$this->load->library('admin/page');
		$this->page->add_css('../../src/template/css/font-awesome.min');
		$this->page->set_titre('Administration des utilisateurs');
		$this->page->add_links('Groupe utilisateur', site_url('admin/groups_user'), 'fa fa-users', 'cyan');

		if(intval($this->session->userdata('avec_traitement_ko')) == 1)
		{
			$this->page->add_links('Traitement des KO / Circulaire', site_url('traitement/KO'), 'fa fa-arrow-circle-right', 'orange');
		}
		
		//$this->page->add_links('D&eacute;verrouillage pli', site_url('deverouiller/pli'));
		$this->page->add_links('Matchage', site_url('matchage/matchage'), 'fa fa-copy', 'deep-purple');
		// $this->page->add_links('Nouveau matchage', site_url('matchage/NewMatchage'), 'fa fa-copy', 'deep-purple');
        $this->page->add_links('Matchage ch&egrave;ques cadeaux', site_url('matchage/matchage_ckd'), 'fa fa-gift', 'pink');
		$this->page->add_links('Support', site_url('admin/support'), 'fa fa-cogs', 'light-green');
		$this->page->add_links('Support-push', site_url('admin/support_push'), 'fa fa-upload', 'teal');
		// $this->page->add_links('Priorisation des KE', site_url('admin/gestion_ke'),'fa fa-tasks', 'amber');
		$this->page->add_links('DMT', site_url('admin/dmt'), 'fa fa-tachometer', 'purple');
		$this->page->add_links('Qualit&eacute;', site_url('admin/qualite'), 'fa fa-eyedropper', 'lime');
		$this->page->add_links('Regroupements des plis', 'javascript:show_info_grp_pli();', 'fa fa-inbox', 'lime');
		$this->page->add_links('Regroupements des plis <br> par date-courrier', 'javascript:show_info_grp_pli_by_older();', 'fa fa-inbox', 'blue-grey');

		$this->page->add_js('admin/users');
		$this->page->add_css('admin/users');
		$this->load->model('admin/model_users');

		
		$data_view_user = array(
			'liste_types' => $this->model_users->list_type_user()
			,'liste_groupes' => $this->model_users->list_grp_user(FALSE)
			,'liste_groupes_all' => $this->model_users->list_grp_user()
			,'liste_type_acces_push' => $this->model_users->type_acces_push()
		);
		$this->page->afficher( $this->load->view('admin/users', $data_view_user,TRUE) );
	}

	public function list_user(){
		$correspondance_field = array('login', 'nom_type_utilisateur', 'nom_groupe_utilisateur', 'tbu.actif', 'tbu.matr_gpao');
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('admin/model_users');
			$resultat["draw"] = $this->input->post_get('draw');
            $offset = $this->input->post_get('start');
            $limit = $this->input->post_get('length');
            $search = $this->input->post_get('search');
            $arg_find = strtolower($search['value']);
            $order = $this->input->post_get('order');
            $arg_ordo = $correspondance_field[$order[0]['column']];
            $sens = $order[0]['dir'];
			$resultat["iTotalRecords"] = $this->model_users->nb_all_user();
			$resultat["iTotalDisplayRecords"] = $this->model_users->get_users($limit, $offset, $arg_find, $arg_ordo, $sens, NULL);
			$users = $this->model_users->get_users($limit, $offset, $arg_find, $arg_ordo, $sens, NULL, FALSE);
			foreach ($users as $user) {
				$this->load->library('encrypt');
				$pass = $this->encrypt->decode($user->pass, Kryptxt);
				$row = array(
					'login' => $user->login.' <small>[#'.$user->id.']</small>'
					,'type' => $user->tp
					,'gr' => empty($user->gr) ? '' : $user->gr.' <small>[#'.$user->id_gr.']</small>'
					,'etat' => '<span class="label bg-'.($user->user_actif == 1 ? 'green">OUI' : 'deep-orange">NON').'</span>'
					,'matricule' => $user->matricule
					,'action' => '<div class="icon-button-demo">'
				);
				if ($user->id != $this->session->id_utilisateur) {
					$row['action'] = ''
						. '<button type="button" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" onclick="modif_pers(\''.base64_encode($user->id).'\', \''.base64_encode($user->login).'\', \''.base64_encode($user->id_tp).'\', \''.base64_encode($user->id_gr).'\', \''.base64_encode($user->user_actif).'\',\''.base64_encode($pass).'\',\''.base64_encode($user->matricule).'\',\''.base64_encode($user->id_type_acces).'\',\''.$user->avec_traitement_ko.'\',\''.$user->niveau_op.'\');" data-toggle="tooltip" data-placement="top" title="Modifier">'
						. '<i class="material-icons">create</i>'
						. '</button>';																		
					$row['action'] .= '<button type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float" onclick="suppr_pers(\''.base64_encode($user->id).'\', \''.base64_encode($user->login).'\');" data-toggle="tooltip" data-placement="top" title="Supprimer">'
						. '<i class="material-icons">delete</i>'
						. '</button>';																		
				}
				$row['action'] .= '<button type="button" class="btn btn-info btn-circle waves-effect waves-circle waves-float" onclick="histo_user('.$user->id.');" data-toggle="tooltip" data-placement="top" title="Historique">'
						. '<i class="material-icons">history</i>'
						. '</button></div>';
				$row['DT_RowAttr'] = array(
					'my_id_user' => $user->id
				);
                $row['DT_RowClass'] = 'user_tab_line';
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
        echo json_encode($resultat);
	}

	public function suppr(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if(isset($_REQUEST['id'])){
			$id_user = base64_decode($this->input->post_get('id'));
			$this->load->model('admin/model_users');
			echo $this->model_users->suppr($id_user) ? 0 : 'Suppression &eacute;chou&eacute;e.';
			return '';
		}
		echo 'Param&egrave;ttre manquant';
	}

	public function new_user(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if(isset($_REQUEST['b_login']) && isset($_REQUEST['b_pswd']) && isset($_REQUEST['b_id_tp']) && isset($_REQUEST['b_actif'])){
			$login = base64_decode($this->input->post_get('b_login'));
			$this->load->model('admin/model_users');
			if($this->model_users->exist_login($login)){
				echo 'Ce login existe déjà.';
				return '';
			}
			$matricule = base64_decode($this->input->post_get('b_matr'));
			$id_grp_user = base64_decode($this->input->post_get('b_id_gr'));
			$pass = base64_decode($this->input->post_get('b_pswd'));
			$this->load->library('encrypt');
			$data = array(
				'login' => $login
				,'pass' => $this->encrypt->encode($pass, Kryptxt)
				,'matr_gpao' => is_numeric($matricule) ? $matricule : NULL
				,'id_type_utilisateur' => base64_decode($this->input->post_get('b_id_tp'))
				,'id_groupe_utilisateur' => is_numeric($id_grp_user) ? $id_grp_user : NULL
				,'actif' => base64_decode($this->input->post_get('b_actif'))
				,'avec_traitement_ko' => base64_decode($this->input->post_get('b_ttmt_ko'))
				,'niveau_op' => $this->input->post_get('b_niveau_op')
			);
			if($data['id_type_utilisateur'] != 2){
				$data['id_groupe_utilisateur'] = NULL;
				$data['niveau_op'] = NULL;
			}
			$id_acces = NULL;
			if($data['id_type_utilisateur'] == 5){
				$id_acces = base64_decode($this->input->post_get('b_id_acces_op'));
			}
			echo $this->model_users->new_user($data, $id_acces) ? 0 : 'Enregistrement échoué.';
			return '';
		}
		echo 'Param&egrave;ttre manquant';
	}

	public function modifier(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if(isset($_REQUEST['b_id']) && isset($_REQUEST['b_login']) && isset($_REQUEST['b_pswd']) && isset($_REQUEST['b_id_tp']) && isset($_REQUEST['b_actif'])){
			$login = base64_decode($this->input->post_get('b_login'));
			$id_user = base64_decode($this->input->post_get('b_id'));
			$this->load->model('admin/model_users');
			if($this->model_users->exist_login($login, $id_user)){
				echo 'Ce login existe d&eacute;j&agrave.';
				return '';
			}
			$matricule = base64_decode($this->input->post_get('b_matr'));
			$id_grp_user = base64_decode($this->input->post_get('b_id_gr'));
			$pass = base64_decode($this->input->post_get('b_pswd'));
			$this->load->library('encrypt');
			$data = array(
				'login' => $login
				,'pass' => $this->encrypt->encode($pass, Kryptxt)
				,'matr_gpao' => is_numeric($matricule) ? $matricule : NULL
				,'id_type_utilisateur' => base64_decode($this->input->post_get('b_id_tp'))
				,'id_groupe_utilisateur' => is_numeric($id_grp_user) ? $id_grp_user : NULL
				,'actif' => base64_decode($this->input->post_get('b_actif'))
				,'avec_traitement_ko' => base64_decode($this->input->post_get('b_ttmt_ko'))
				,'niveau_op' => $this->input->post_get('b_niveau_op')
			);
			if($data['id_type_utilisateur'] != 2){
				$data['id_groupe_utilisateur'] = NULL;
				$data['niveau_op'] = NULL;
			}
			$id_acces = NULL;
			if($data['id_type_utilisateur'] == 5){
				$id_acces = base64_decode($this->input->post_get('b_id_acces_op'));
			}
			echo $this->model_users->modifier($id_user, $data, $id_acces) ? 0 : 'Enregistrement &eacute;chou&eacute;.';
			return '';
		}
		echo 'Param&egrave;ttre manquant';
	}

	public function get_wallboard_user_activity()
	{
		$this->load->model('admin/model_users');
		$data = $this->model_users->wallboard_activity($this->input->post_get('s_login'));
		$connecte = 0;
		$connecte_op_cr = 0;
		$connecte_op_cr_ttr = 0;
		$connecte_op_fx = 0;
		$connecte_op_fx_ttr = 0;
		foreach ($data as $key => $value) {
			if($value->connecte == 1){
				$connecte++;
				if( in_array($value->id_type_u, array(2,3,5))) {
					switch ($value->id_flux) {
						case 1:
							$connecte_op_cr++;
							if($value->occup == 1){
								$connecte_op_cr_ttr++;
							}
							break;
						case 2:
							$connecte_op_fx++;
							if($value->occup == 1){
								$connecte_op_fx_ttr++;
							}
							break;
						case 3:
							$connecte_op_fx++;
							if($value->occup == 1){
								$connecte_op_fx_ttr++;
							}
							break;
						
						default:
							break;
					}
				}
			}
		}
		echo json_encode(array(
			'data' => $this->load->view('admin/users/tab_wlb_users', array('lignes' => $data), TRUE)
			,'nb' => count($data)
			,'connecte' => $connecte
			,'connecte_op_cr' => $connecte_op_cr
			,'connecte_op_cr_ttr' => $connecte_op_cr_ttr
			,'connecte_op_fx' => $connecte_op_fx
			,'connecte_op_fx_ttr' => $connecte_op_fx_ttr
		));
	}

	public function histo_user($id_user)
	{
		$this->load->model('admin/model_users');
		$user = $this->model_users->get_user($id_user);
		if(is_null($user)){
			echo json_encode(array(
				'code' => 1
				,'message' => 'Utilisateur introuvable'
			));
			return '';
		}
		$id_tb = 'tab_histo_'.date('U').mt_rand();
		echo json_encode(array(
			'code' => 0
			,'titre' => 'Historique'
			,'sous_titre' => 'Compte: '.$user->login.(is_numeric($user->matr_gpao) ? " [$user->matr_gpao]" : '')
			,'js' => $this->load->view('admin/users/js_tb_histo', array('id_tb' => $id_tb, 'id_user' => $id_user), TRUE)
			,'body' => $this->load->view('admin/users/tb_histo', array('id_tb' => $id_tb, 'id_user' => $id_user), TRUE)
			,'footer' => '<button type="button" class="btn btn-info waves-effect" onclick="'.$id_tb.'.ajax.reload()"><i class="material-icons">autorenew</i></button>'
			,'id_tab' => $id_tb
		));
	}

	public function dttbl_histo_user()
	{
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('admin/model_users');
			$resultat["draw"] = $this->input->post_get('draw');
			$id_user = $this->input->post_get('id_user');
			$offset = $this->input->post_get('start');
			$limit = $this->input->post_get('length');
			$resultat["iTotalRecords"] = $this->model_users->histo_user($id_user, $limit, $offset);
			$resultat["iTotalDisplayRecords"] = $resultat["iTotalRecords"];
			$lignes = $this->model_users->histo_user($id_user, $limit, $offset, FALSE);
			foreach ($lignes as $key => $ligne) {
				$action = $ligne->label_action;
				$action = $ligne->coms == '' ? $action : "<span title=\"$ligne->coms\">$action</span>";
				$pli = is_numeric($ligne->id_pli) && $ligne->id_pli > 0 ? $ligne->id_pli : '';
				if (is_numeric($pli)) {
					$pli = '#'.$pli;
					if($ligne->id_flux == 1) {
						$pli = "<a href=\"javascript:detail_pli($ligne->id_pli)\">$pli</a>";
					}
				}
				$row = array(
					'date_action' => (new DateTime($ligne->date_action))->format('d/m/Y H:i:s')
					,'action' => $action
					,'flux' => $ligne->flux
					,'pli' => $pli
					,'ip' => $ligne->ip
				);
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
		echo json_encode($resultat);
	}

}
