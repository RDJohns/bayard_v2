<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Qualite extends CI_Controller{

    public function index(){
		$this->load->library('portier');
		$this->portier->must_admin();
		$this->go();
    }
    
    public function go(){
		$this->load->library('admin/page');
		$this->page->set_titre('Qualité');
		$this->page->add_css('../../src/template/css/font-awesome.min');
		$this->page->add_links('Administration des utilisateurs', site_url('admin/users'), 'fa fa-user', 'cyan-t');
		$this->page->add_links('Groupe utilisateur', site_url('admin/groups_user'), 'fa fa-users', 'cyan');
		//$this->page->add_links('D&eacute;verrouillage pli', site_url('deverouiller/pli'));
        $this->page->add_links('Matchage', site_url('matchage/matchage'), 'fa fa-copy', 'deep-purple');
        $this->page->add_links('Matchage ch&egrave;ques cadeaux', site_url('matchage/matchage_ckd'), 'fa fa-gift', 'pink');
		$this->page->add_links('Support', site_url('admin/support'), 'fa fa-cogs', 'light-green');
		$this->page->add_links('Support-push', site_url('admin/support_push'), 'fa fa-upload', 'teal');
		// $this->page->add_links('Priorisation des KE', site_url('admin/gestion_ke'),'fa fa-tasks', 'amber');
		$this->page->add_links('DMT', site_url('admin/dmt'), 'fa fa-tachometer', 'purple');
		$this->page->add_links('Regroupements des plis', 'javascript:show_info_grp_pli();', 'fa fa-inbox', 'lime');
		$this->page->add_links('Regroupements des plis <br> par date-courrier', 'javascript:show_info_grp_pli_by_older();', 'fa fa-inbox', 'blue-grey');
		$this->page->add_js('admin/cality');
		$this->page->add_css('admin/cality');
		$this->load->model('admin/model_cality');
		$data_view = array(
			'typos' => array()
			,'paiements' => array()
			,'socities' => array()
		);
		$this->page->afficher($this->load->view('admin/cality/cality', $data_view, TRUE));
	}

	public function data_faute(){
		echo $this->create_view_faute('Tol&eacute;rance à 1%', 1).$this->create_view_faute('Tol&eacute;rance à 3%', 3);
	}

	private function create_view_faute($titre, $taux=NULL){
		$this->load->helper('function1');
		$dt_ctrl = date_1_2($this->input->post_get('dt_ctrl_1'), $this->input->post_get('dt_ctrl_2'));
		$this->load->model('admin/model_cality');
		$total_echantillon = $this->model_cality->nb_echantillon($dt_ctrl, $taux);
		$total_echantillon_avec_faute = $this->model_cality->echantillon_avec_faute($dt_ctrl, $taux);
		$total_echantillon_sans_faute = $total_echantillon - $total_echantillon_avec_faute;
		$fautes = $this->model_cality->fautes($dt_ctrl, $taux);
		foreach ($fautes as $key => $faute) {
			$faute->prc = $faute->nb * 100 / $total_echantillon_avec_faute;
		}
		$data_view = array(
			'total' => $total_echantillon
			,'sans' => $total_echantillon_sans_faute
			,'avec' => $total_echantillon_avec_faute
			,'fautes' => $fautes
			,'titre' => $titre
			,'taux' => $taux
		);
		return $this->load->view('admin/cality/fautes', $data_view, TRUE);
	}

	public function export_faute_xl(){
		$this->load->helper('function1');
		$dt_ctrl = date_1_2($this->input->post_get('dt_ctrl_1'), $this->input->post_get('dt_ctrl_2'));
		$taux = $this->input->post_get('taux');
		$this->load->model('admin/model_cality');
		$data = $this->model_cality->erreur_echantillon($dt_ctrl, $taux);
		$this->load->library('admin/export_xl');
		$this->export_xl->add_page('Plis avec erreur à seuil '.$taux.'%');
		$titre = array('Id.Pli', 'lot ADV', 'Saisi par', 'S.Matr.GPAO', 'Sortie Saisie', 'Contrôlé par', 'C.Matr.GPAO', 'Du', 'Sortie contrôle', 'Erreur trouvé', 'Commentaire');
		$width = array(10, 20, 15, 15, 15, 15, 15, 25, 15, 35, 80);
		$this->export_xl->write_colomn_title($titre, $width);
		$this->export_xl->add_void_line();
		foreach ($data as $key => $ln) {
			$this->export_xl->write_nb($ln->id_pli);
			$this->export_xl->write_txt($ln->lot_adv);
			$this->export_xl->write_txt($ln->op_par);
			$this->export_xl->write_txt($ln->op_gpao);
			$this->export_xl->write_txt($ln->out_saisie);
			$this->export_xl->write_txt($ln->ctrl_par);
			$this->export_xl->write_txt($ln->ctrl_gpao);
			$this->export_xl->write_txt((new DateTime($ln->dt_ctrl))->format('d/m/Y H:i:s'));
			$this->export_xl->write_txt($ln->out_ctrl);
			$this->export_xl->write_txt($ln->erreur);
			$this->export_xl->write_txt($ln->com_erreur);
			$this->export_xl->add_void_line();
		}
		$dt1 =  str_replace('-', '', trim($dt_ctrl[0]));
		$dt2 =  str_replace('-', '', trim($dt_ctrl[1]));
		$name_f = 'Echantillons_en_erreur';
		$name_f = $dt1 != '' ? $name_f.'_from_'.$dt1 : $name_f;
		$name_f = $dt2 != '' ? $name_f.'_to_'.$dt2 : $name_f;
		$token = date('U').mt_rand();
		echo json_encode(array(
			'name' => $this->export_xl->create_file($name_f, $token)
			,'token' => $token
		));
	}
	
	public function get_xl($name_f=NULL, $token=NULL){
		if(!is_null($name_f) && trim($name_f) != '' && trim($name_f) != '*' && trim($name_f) != '.' && !is_null($token)){
			$this->load->helper('download');
			$this->load->helper('file');
			$file_temp = './'.urlFileTemp.$token.'_token_'.$name_f;
			try {
				$fx = read_file($file_temp);
				unlink($file_temp);
				force_download($name_f, $fx);
			} catch (Exception $exc) {
				show_error('Fichier introuvable!', 500, 'ERREUR');
			}finally{
				unlink($file_temp);
			}
		}else{
			show_error('Fichier introuvable!', 500, 'ERREUR');
		}
	}
    
}
