<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller{

	public function index(){
		$this->load->library('portier');
		$this->portier->must_admin();
		$this->go();
	}

	public function go(){
		$this->load->library('admin/page');
		$this->page->set_titre('Support');
		$this->page->add_css('../../src/template/css/font-awesome.min');
		$this->page->add_links('Administration des utilisateurs', site_url('admin/users'), 'fa fa-user', 'cyan-t');
		$this->page->add_links('Groupe utilisateur', site_url('admin/groups_user'), 'fa fa-users', 'cyan');
		//$this->page->add_links('D&eacute;verrouillage pli', site_url('deverouiller/pli'));
		$this->page->add_links('Matchage', site_url('matchage/matchage'), 'fa fa-copy', 'deep-purple');
		// $this->page->add_links('Nouveau matchage', site_url('matchage/NewMatchage'), 'fa fa-copy', 'deep-purple');
		$this->page->add_links('Matchage ch&egrave;ques cadeaux', site_url('matchage/matchage_ckd'), 'fa fa-gift', 'pink');
		$this->page->add_links('Support-push', site_url('admin/support_push'), 'fa fa-upload', 'teal');
		// $this->page->add_links('Priorisation des KE', site_url('admin/gestion_ke'),'fa fa-tasks', 'amber');
		$this->page->add_links('DMT', site_url('admin/dmt'), 'fa fa-tachometer', 'purple');
		$this->page->add_links('Qualit&eacute;', site_url('admin/qualite'), 'fa fa-eyedropper', 'lime');
		$this->page->add_links('Regroupements des plis', 'javascript:show_info_grp_pli();', 'fa fa-inbox', 'lime');
		$this->page->add_links('Regroupements des plis <br> par date-courrier', 'javascript:show_info_grp_pli_by_older();', 'fa fa-inbox', 'blue-grey');		
		
		$this->page->add_js('admin/support');
		$this->page->add_css('admin/support');
		$this->load->model('admin/model_support');
		$data_view = array(
			'stats' => $this->model_support->flag_traitement()
			,'status' => $this->model_support->status()
			,'operators' => $this->model_support->user(2)
			,'controllers' => $this->model_support->user(3)
			,'socities' => $this->model_support->societe()
			,'types' => $this->model_support->typologie()
			,'md_paiements' => $this->model_support->mode_paiement()
			,'motifs_rejet' => $this->model_support->motif_rejet()
		);
		$this->page->afficher($this->load->view('admin/support/support', $data_view, TRUE));
	}

	public function plis(){
		$correspondance_field = array('id_pli','pli','lot_scan','tbsoc.nom_societe','flgtt_etape','flgtt_etat','(CASE WHEN flag_rewrite_status_client <> 0 AND tbstat.id_statut_saisie NOT IN (8, 24) THEN \'\' ELSE tbstatus.libelle END)','recommande','tbu_ty.login','tbtypo.typologie','id_lot_saisie','tbu_ss.login','tbu_ct.login','tbu_rt.login','paiements', 'cmc7s','circulaire','id_com','niveau_saisie');
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('admin/model_support');
			$resultat["draw"] = $this->input->post_get('draw');
			$offset = $this->input->post_get('start');
			$limit = $this->input->post_get('length');
			$search = $this->input->post_get('search');
			$arg_find = strtolower($search['value']);
			$order = $this->input->post_get('order');
			$arg_ordo = $correspondance_field[$order[0]['column']];
			$sens = $order[0]['dir'];
			$param_plus = array(
				'etat' => json_decode($this->input->post_get('etat'))
				,'status' => json_decode($this->input->post_get('status'))
				,'ty_par' => json_decode($this->input->post_get('ty_par'))
				,'ss_par' => json_decode($this->input->post_get('ss_par'))
				,'ct_par' => json_decode($this->input->post_get('ct_par'))
				,'rt_par' => json_decode($this->input->post_get('rt_par'))
				,'soc' => json_decode($this->input->post_get('soc'))
				,'typo' => json_decode($this->input->post_get('typo'))
				,'niveau_saisie' => json_decode($this->input->post_get('niveau_saisie'))
				,'md_paie' => json_decode($this->input->post_get('md_p'))
				,'find_idpli' => strtolower($this->input->post_get('find_idpli'))
				,'find_cmc7' => strtolower($this->input->post_get('find_cmc7'))
				,'find_lotn' => strtolower($this->input->post_get('find_lotn'))
				,'find_lots' => strtolower($this->input->post_get('find_lots'))
			);//var_dump($param_plus);
			$resultat["iTotalRecords"] = $this->model_support->pli_total();
			$resultat["iTotalDisplayRecords"] = $this->model_support->get_plis($limit, $offset, $arg_find, $arg_ordo, $sens, $param_plus);
			$plis = $this->model_support->get_plis($limit, $offset, $arg_find, $arg_ordo, $sens, $param_plus, FALSE);
			$this->load->helper('function1');
			foreach ($plis as $pli) {
				$lab_rec = '<span class="label label-default">?</span>';
				switch ($pli->rec) {
					case '-1':
						$lab_rec = '<span class="label label-warning">STAND-BY</span>';
						break;
					case '0':
						$lab_rec = '<span class="label label-default">NON</span>';
						break;
					case '1':
						$lab_rec = '<span class="label label-danger">OUI</span>';
						break;
					default:
						$lab_rec = '<span class="label label-default">?</span>';
						break;
				}
				$lot_ss = trim($pli->lot_ss);
				$row = array(
					'id_pli' => $pli->id_pli
					,'pli' => $pli->pli
					,'lot_num' => $pli->lot_num
					,'soc' => $pli->nom_societe
					,'etape' => $pli->flgtt_etape
					,'etat' => $pli->flgtt_etat_disp
					,'status' => $pli->etat
					,'rec' => $lab_rec
					,'ty_par' => $pli->ty_par
					,'type' => $pli->typo
					,'lot_ss' => $lot_ss . ($pli->lot_ss_bis == 1 && strlen($lot_ss) > 0 ? '<sup class="font-italic col-pink font-10">BIS</sup>' : '')
					,'ss_par' => $pli->ss_par
					,'ct_par' => $pli->ct_par
					,'rt_par' => $pli->rt_par
					,'paie' => implode(', ', array_unique(explode('|', $pli->paiements)))
					,'cmc7s' => $pli->cmc7s//cmc7s($pli->cmc7s)
					,'ci' => $pli->circulaire
					,'id_com' => $pli->id_com
					,'niveau_saisie' => 'Niveau '.$pli->niveau_saisie
					,'DT_RowAttr' => array(
						'my_id_pli' => $pli->id_pli
						,'my_flg' => $pli->id_flg
						,'my_stat' => $pli->status
						,'my_rec' => $pli->rec
						,'my_is_in_decoup' => $pli->in_decoup
						,'my_nb_pj' => $pli->nb_pj
						,'my_nb_motif_cons' => $pli->nb_motif_cons
						,'with_f_ci' => (empty($pli->fichier_circulaire) || empty($pli->nom_orig_fichier_circulaire)) ? '0' : '1'
						// ,'data-toggle' => 'tooltip'
						// ,'data-placement' => 'top'
						// ,'title' => 'Cliquez pour afficher les actions'
					)
					,'DT_RowClass' => 'ln_pli'
				);
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
		echo json_encode($resultat);
	}

	public function prio(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if(isset($_REQUEST['id_pli']) && isset($_REQUEST['rec'])){
			$id_pli = $this->input->post_get('id_pli');
			$rec = $this->input->post_get('rec');
			if(is_numeric($id_pli) && is_numeric($rec)){
				$rec = $rec == 1 ? 0 : 1;
				$this->load->model('admin/model_support');
				$this->model_support->prio($id_pli, $rec);
				echo '1';
				return '';
			}
		}
		log_message('error', 'Support> prio -> arguments manquants');
		echo 'Pli introuvable!';
	}

	public function set_stand(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if(isset($_REQUEST['id_pli']) && isset($_REQUEST['rec'])){
			$id_pli = $this->input->post_get('id_pli');
			$rec = $this->input->post_get('rec');
			if(is_numeric($id_pli) && is_numeric($rec)){
				$rec = $rec == -1 ? 1 : -1;
				$this->load->model('admin/model_support');
				$this->model_support->stand_by($id_pli, $rec);
				echo '1';
				return '';
			}
		}
		log_message('error', 'Support> prio -> arguments manquants');
		echo 'Pli introuvable!';
	}
	
	public function histo_pli($id_pli){
		if(is_numeric($id_pli)){
			$this->load->model('admin/model_support');
			$pli = $this->model_support->data_pli($id_pli);
			$data_events = $this->model_support->histo($id_pli);
			$pli_events = array();
			foreach ($data_events as $data_event) {
				$event = new stdClass();
				$event->flag = $data_event->flag;
				$event->color = $data_event->flag == 0 ? 'warning' : 'info';
				$event->header = $data_event->flag == 0 ? $data_event->lbl : $data_event->lbl;
				$event->body = $data_event->flag == 0 ? 'Action de '.$data_event->login : 'changement d\'&eacute;tape';
				$event->moment = $data_event->moment == '' ? '' : (new DateTime($data_event->moment))->format('d/m/Y G:i:s');
				$event->coms = $data_event->com == '' ? '' : '('.$data_event->com.')';
				$event->motif_ko = $data_event->motif_ko;
				$event->dt_motif_ko = $data_event->dt_motif_ko == '' ? '' : (new DateTime($data_event->dt_motif_ko))->format('d/m/Y G:i:s');
				$event->consigne_ko = $data_event->consigne_ko;
				$event->dt_consigne = $data_event->dt_consigne == '' ? '' : (new DateTime($data_event->dt_consigne))->format('d/m/Y G:i:s');
				array_push($pli_events, $event);
			}
			$data_view = array(
				'pli_events' => $pli_events
				,'dt_courrier' => $pli->date_creation == '' ? '' : (new DateTime($pli->date_creation))->format('d/m/Y G:i:s')
			);
			if(!is_null($pli)){
				echo json_encode(array(
					'code' => 1
					,'ihm' => $this->load->view('admin/support/histo', $data_view, TRUE)
				));
				return '';
			}
		}
		log_message('error', 'admin> support> histo_pli: failed loading histo pli view pli#'.$id_pli);
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Pli introuvable!'
		));
	}

	public function reinit($id_pli=NULL, $flg_tt){
		if (is_numeric($id_pli)) {
			$this->load->library('portier');
			$this->portier->auto_inspecter_v(TRUE);
			$this->portier->must_admin();
			$this->load->model('admin/model_support');
			echo $this->model_support->reinit($id_pli, $flg_tt) ? 1 : 'Le pli a changé d\'état!';
			return '';
		}
		echo 'Pli introuvable!';
	}

	public function abandon($id_pli=NULL, $flg_tt){
		if (is_numeric($id_pli)) {
			$this->load->library('portier');
			$this->portier->auto_inspecter_v(TRUE);
			$this->portier->must_admin();
			$this->load->model('admin/model_support');
			$rep = $this->model_support->abandon($id_pli, $flg_tt);
			echo $rep[0] ? '1' : $rep[1];
			return '';
		}
		echo 'Pli introuvable!';
	}

	/*public function ko_scan($id_pli=NULL){
		if (is_numeric($id_pli)) {
			$this->load->library('portier');
			$this->portier->auto_inspecter_v(TRUE);
			$this->portier->must_admin();
			$this->load->model('admin/model_support');
			echo $this->model_support->ko_scan($id_pli) ? 1 : 0;
			return '';
		}
		echo 'Pli introuvable!';
	}*/
	
	public function mise_en_ko_scan(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if(isset($_REQUEST['id_pli']) && isset($_REQUEST['motif']) && isset($_REQUEST['flg_tt_now'])){
			$id_pli = $this->input->post_get('id_pli');
			$flg_tt_now = $this->input->post_get('flg_tt_now');
			if(is_numeric($id_pli) && is_numeric($this->input->post_get('motif')) && is_numeric($flg_tt_now)) {
				$data = array(
					'motif_rejet_id' => (int)$this->input->post_get('motif')
					,'utilisateurs_id' => (int)$this->session->id_utilisateur
					,'pli_id' => (int)$id_pli
					,'description' => $this->input->post_get('comment')
				);
				$this->load->model('saisie/model_pli_saisie');
				$this->load->model('admin/model_support');
				echo $this->model_support->ko_scan($id_pli, $flg_tt_now, $data) ? 1 : 'Le pli a changé d\'état!';
				return '';
			}
			log_message('error', 'admin> admin> mise_en_ko_scan> argument non-conforme pli#'.$id_pli.', motif: '.$this->input->post_get('motif').', flag_tt_now: '.$flg_tt_now);
			echo 'parametres non-conforme!';
			return '';
		}
		log_message('error', 'admin> admin> mise_en_ko_scan> id_pli non numerique: ');
		echo 'Pli introuvable!';
	}

	public function hors_perim($id_pli=NULL, $flg_tt){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if (is_numeric($id_pli)) {
			$this->load->model('admin/model_support');
			echo $this->model_support->hors_perim($id_pli, $flg_tt) ? 1 : 'Le pli a changé d\'état!';
			return '';
		}
		echo 'Pli introuvable!';
	}

	public function ui_division($id_pli){
		if(is_numeric($id_pli)){
			$this->load->model('admin/model_support');
			$pli = $this->model_support->data_pli($id_pli);
			if(!is_null($pli)){
				$docs = $this->model_support->data_docs($id_pli);
				$data_view = array(
					'pli' => $pli
					,'nb_doc' => count($docs)
					,'docs' => $docs
				);
				echo json_encode(array(
					'code' => 1
					,'ui' => $this->load->view('admin/support/division', $data_view, TRUE)
				));
				return '';
			}
		}
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Pli introuvable!'
		));
	}

	public function get_img_doc($id_doc){
		if(is_numeric($id_doc)){
			ini_set('memory_limit', '-1');
			$this->load->model('admin/model_support');
			$doc = $this->model_support->doc($id_doc);
			if(!is_null($doc)){
				$data_view = array(
					'doc' => $doc
					,'empty_b64' => base64_encode(file_get_contents(base_url('assets/images/empty_papper.jpg')))
				);
				echo json_encode(array(
					'code' => 1
					,'img' => $this->load->view('admin/support/document', $data_view, TRUE)
				));
				return '';
			}
		}
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Fichier introuvable!'
		));
	}

	public function diviser(){
		if(isset($_REQUEST['id_pli']) && isset($_REQUEST['docs']) && isset($_REQUEST['flg_tr'])){
			$this->load->library('portier');
			$this->portier->auto_inspecter_v(TRUE);
			$this->portier->must_admin();
			$id_pli = $_REQUEST['id_pli'];
			$flg_tr = $_REQUEST['flg_tr'];
			$docs = json_decode($this->input->post_get('docs'));
			$pli_fils = array();
			foreach ($docs as $doc) {
				if(!isset($pli_fils[$doc->pli])){
					$pli_fils[$doc->pli] =  array();
				}
				array_push($pli_fils[$doc->pli], $doc->id_doc);
			}
			$this->load->model('admin/model_support');
			$division = $this->model_support->division($id_pli, $pli_fils, $flg_tr);
			if($division[0]){
				echo 1;
				return '';
			}else{
				echo $division[1];
				log_message('error', 'support> diviser> division echoue du pli#'.$id_pli.' : '.$division[1]);
				return '';
			}
		}
		echo 'Paramètre manquant';
		log_message('error', 'support> diviser> parametre manquant');
	}

	public function display($id_pli){
		if(is_numeric($id_pli)){
			$this->load->model('admin/model_support');
			$pli = $this->model_support->pli_view($id_pli);
			if(!is_null($pli)){
				$data_view = array(
					'pli' => $pli
					,'docs' => $this->model_support->data_docs($id_pli)
					,'paies' => $this->model_support->paiement_view($id_pli)
					,'chqs' => $this->model_support->cheques($id_pli)
					,'chqs_kd' => $this->model_support->cheques_kd($id_pli)
					,'mvmts' => $this->model_support->mouvements($id_pli)
					,'motif_consigne' => $this->model_support->get_motif_consigne($id_pli)
				);
				echo $this->load->view('admin/support/pli', $data_view, TRUE);
				return '';
			}
		}
		echo 'Pli introuvable!';
	}

	public function unlock($id_pli=NULL, $flg_tt=NULL){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_admin();
		if (is_numeric($id_pli) && is_numeric($flg_tt)) {
			$this->load->model('admin/model_support');
			$pli = $this->model_support->pli($id_pli);
			if (is_null($pli)) {
				echo 'Pli introuvable!';
				return '';
			}
			if($pli->flag_traitement != $flg_tt){
				echo 'D&eacute;verrouillage annul&eacute; car le pli a chang&eacute; d\'&eacute;tat!';
				return '';
			}
			switch ($flg_tt) {
				case '1':
					if($this->model_support->unlock($id_pli, $flg_tt, 0)){
						$this->histo->action(11, 'support', $id_pli);
					}
					break;
				case '4':
					if($this->model_support->unlock($id_pli, $flg_tt, 3)){
						$this->histo->action(12, 'support', $id_pli);
					}
					break;
				case '8':
					if($this->model_support->unlock($id_pli, $flg_tt, 7)){
						$this->histo->action(26, 'support', $id_pli);
					}
					break;
				case '13':
					if($this->model_support->unlock($id_pli, $flg_tt, 11)){
						$this->histo->action(28, 'support', $id_pli);
					}
					break;
				default:
					echo 'D&eacute;verrouillage annul&eacute; car le pli est non v&eacute;rrouill&eacute;!';
					return '';
					break;
			}
			$this->histo->pli($id_pli, 'deverrouillage support');
			echo 1;
			return '';
		}
		echo 'Paramètres non-conformes!';
	}

	public function reprise_traitement($id_pli, $flg_tt){
		if (is_numeric($id_pli)) {
			$this->load->library('portier');
			$this->portier->auto_inspecter_v(TRUE);
			$this->portier->must_admin();
			$this->load->model('admin/model_support');
			echo $this->model_support->recuperation_ks($id_pli, $flg_tt) ? 1 : 'Le pli a changé d\'état!';
			return '';
		}
		echo 'Pli introuvable!';
	}
	
	public function download_circulaire($id_pli){//deprecated: use control/ajax/download_circulaire
		if (is_numeric($id_pli)) {
			$this->load->helper('function1');
			$this->load->helper('download');
			$this->load->helper('file');
			$this->load->model('admin/model_support');
			$data_pli = $this->model_support->ged_data_pli($id_pli);
			if(!is_null($data_pli)){
				//$nom_ci = compose_nom_ci($data_pli->lot_scan, $id_pli, $data_pli->pli, $data_pli->nom_orig_fichier_circulaire);
				force_download($data_pli->nom_orig_fichier_circulaire, read_file('./'.upFileCirc.$data_pli->fichier_circulaire));
				return '';
			}
			force_download('no_fichier.txt', 'Pas de fichier CI pour le pli#'.$id_pli);
			return '';
		}
		log_message('error', 'admin>support>download_circulaire: Pli introuvable! pli#'.$id_pli);
		force_download('error.txt', 'Pli introuvable: pli#'.$id_pli);
	}

	public function download_pjs($id_pli){
		if (is_numeric($id_pli)) {
			$this->load->library('zip');
			$this->load->model('admin/model_support');
			$pjs = $this->model_support->ged_pjs($id_pli);
			foreach ($pjs as $pj) {
				$this->zip->add_data($pj->n_ima_recto, base64_decode($pj->n_ima_base64_recto));
				$this->zip->add_data($pj->n_ima_verso, base64_decode($pj->n_ima_base64_verso));
			}
			if(count($pjs) > 0){
				$this->zip->download('pjs_pli'.$id_pli.'.zip');
			}else{
				$this->load->helper('download');
				force_download('no_pj.txt', 'Aucun PJ pour le pli#'.$id_pli);
			}
		}else{
			log_message('error', 'admin>support>download_pjs: Pli introuvable! pli#'.$id_pli);
			force_download('error.txt', 'Pli introuvable: pli#'.$id_pli);
		}
	}
	
	public function list_groupement_pli(){
		// $this->load->model('admin/model_support');
		$this->load->model('saisie/model_visu');
		$data_view = array(
			// 'grps' => $this->model_support->group_pli1()
			'grps' => $this->model_visu->group_pli1()
			,'totaux_wait' => 0
			,'totaux_current' => 0
			,'totaux' => 0
		);
		echo $this->load->view('admin/support/groupement', $data_view, TRUE);
	}

	public function list_groupement_pli_by_older(){
		// $this->load->model('admin/model_support');
		$this->load->model('saisie/model_visu');
		$data_view = array(
			// 'grps' => $this->model_support->group_pli1_by_older()
			'grps' => $this->model_visu->group_pli1_by_older()
			,'totaux_wait' => 0
		);
		echo $this->load->view('admin/support/groupement_by_older', $data_view, TRUE);
	}

	private function erase_img(){
		$source = base64_encode(file_get_contents(base_url('depot/84506recto.jpg')));
		$this->load->model('admin/model_support');
		//echo $this->model_support->erase_img('84506', $source) ? 'ok': 'ko';
	}

	private function correction_data_pli($auto=FALSE){
		if($auto){
			$this->load->model('admin/model_support');
			//echo $this->model_support->correction_data_pli();
		}
	}

	public function download_all_ci(){
		$this->load->model('admin/model_support');
		$this->load->library('zip');
		$this->load->helper('file');
		$plis = $this->model_support->download_all_ci();
		foreach ($plis as $pli) {
			$f = './'.upFileCirc.$pli->fichier_circulaire;
			if(file_exists($f)){
				$this->zip->add_data('pli'.$pli->id_pli.'_'.$pli->nom_orig_fichier_circulaire, read_file($f));
			}
		}
		$this->zip->download('ci_plis.zip');
	}
	
	public function reinit_with_force($id_pli=NULL){
		if(is_numeric($id_pli)){
			$this->load->model('admin/model_support');
			if($this->model_support->with_chq_saisie_reb($id_pli)){
				echo 2;
				return '';
			}
			if($this->model_support->is_divised($id_pli)){
				echo 3;
				return '';
			}
			$pli = array($id_pli);
			if($this->model_support->reinit_extra($pli)){
				echo 0;
				return '';
			}
		}
		echo 1;
	}

	private function reinit_extra(){
		$this->load->model('admin/model_support');
		$plis = array();
		//echo $this->model_support->reinit_extra($plis) ? 'ok' : 'ko!';
	}

	public function admin_root_action()
	{
		// $this->load->model('model_manip_db');
		// $plis = array();
		//echo '<\br>'.($this->model_manip_db->reinit_courrier_all() ? 'courrier reinitialise' : 'reinitialisation courrier failed');
		//echo '<\br>'.($this->model_manip_db->reinit_flux_all() ? 'flux reinitialise' : 'reinitialisation flux failed');
	}
	
	public function data_assignation_pli($id_pli){
		if(is_numeric($id_pli)){
			ini_set('memory_limit', '-1');
			$this->load->model('admin/model_support');
			$pli = $this->model_support->pli($id_pli);
			$typage = $this->model_support->get_assignation_pli($id_pli, 0);
			$saisie = $this->model_support->get_assignation_pli($id_pli, 3);
			$ctrl = $this->model_support->get_assignation_pli($id_pli, 7);
			if(!is_null($pli)){
				echo json_encode(array(
					'code' => 0
					//,'pli' => $pli
					,'users' => $this->model_support->operateur(2)
					,'users_ctrl' => $this->model_support->operateur(3)
					,'op_typage' => is_null($typage) ? '' : $typage->operateur
					,'op_saisie' => is_null($saisie) ? '' : $saisie->operateur
					,'op_ctrl' => is_null($ctrl) ? '' : $ctrl->operateur
				));
				return '';
			}
		}
		echo json_encode(array(
			'code' => 1
			,'msg' => 'Pli introuvable!'
		));
	}

	public function assignation_pli()
	{
		if(is_numeric($this->input->post_get('id_pli'))){
			$this->load->library('portier');
			$this->portier->auto_inspecter_v(TRUE);
			$this->portier->must_admin();
			$data = array(
				'id_pli' => $this->input->post_get('id_pli')
				,'typage' => is_numeric($this->input->post_get('op_typage')) ? $this->input->post_get('op_typage') : NULL
				,'saisie' => is_numeric($this->input->post_get('op_saisie')) ? $this->input->post_get('op_saisie') : NULL
				,'control' => is_numeric($this->input->post_get('op_ctrl')) ? $this->input->post_get('op_ctrl') : NULL
			);
			$this->load->model('admin/model_support');
			$this->model_support->save_assignation_pli($data);
			echo 0;
		}else{
			echo 'Paramètres non-conformes!';
		}
	}

}
