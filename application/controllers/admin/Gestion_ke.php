<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gestion_ke extends CI_Controller{

    public function index()
    {
      $this->load->library('portier');
      
      $this->portier->must_admin();
      $this->showPlisKe();
    }
    
    //template KE
    public function showPlisKe()
    {
        
        $this->load->library('admin/page');
        $this->page->set_titre('Priorisation des KE');
		    $this->page->add_css('../../src/template/css/font-awesome.min');
	    	$this->page->add_links('Groupe utilisateur', site_url('admin/groups_user'), 'fa fa-users', 'cyan');
        $this->page->add_links('Matchage', site_url('matchage/matchage'), 'fa fa-copy', 'deep-purple');
		    $this->page->add_links('Support', site_url('admin/support'), 'fa fa-cogs', 'light-green');
        $this->page->add_links('Support MAIL/SFTP', site_url('admin/support_push'),'fa fa-sign-in', 'amber');
        $this->page->add_links('Support-push', site_url('admin/support_push'), 'fa fa-upload', 'teal');
        $this->page->add_links('DMT', site_url('admin/dmt'), 'fa fa-tachometer', 'purple');
		    $this->page->add_links('Qualit&eacute;', site_url('admin/qualite'), 'fa fa-eyedropper', 'lime');
        
        $this->page->add_js('admin/ke');
        $this->page->add_css('admin/ke');
        $this->load->model('admin/model_ke', 'ke_model');
        //get societe 
        $data_soc = $this->ke_model->getSociete();
        //get typologie 
        $data_typo = $this->ke_model->getTypologie();
        $data = array(
          'societe' => $data_soc,
          'typologies' => $data_typo
        );
        $this->page->afficher($this->load->view('ke/gestion_ke', $data, true));
    }

    //get plis selon les critere de recherche
    public function getPlis()
    {
        //load model 
        $this->load->model('admin/model_ke', 'ke_model');
        
        $draw = intval($this->input->post_get("draw"));
        $length            = $this->input->post_get('length');
        $start             = $this->input->post_get('start');
        
        //critere de recherche
        $id_flux = $this->input->post_get('id_pli');
        $societe= $this->input->post_get('societe');
        $typologie= $this->input->post_get('typologie');
        $clause_where = "";
        //s'il y a des critères de recherche définis 
        $clause_where .= $this->wherelike($id_flux, 'f_pli.id_pli');
        $clause_where .= $this->wherelike($societe, 'societe.id');
        $clause_where .= $this->wherelike($typologie, 'typologie.id');  
        //date courrier
        $date_courrier1 = $this->input->post_get('date_c1');
        $date_courrier2 = $this->input->post_get('date_c2');
        $clause_where .= $this->whereDateC($date_courrier1, $date_courrier2);
        //date traitement
        $date_traitement1 = $this->input->post_get('date_t1');
        $date_traitement2 = $this->input->post_get('date_t2');
        $clause_where .= $this->whereDateT($date_traitement1, $date_traitement2);
        $data =$this->ke_model->getPli($clause_where, $length, $start);
        $row = array();
        if (count($data)>0) {
          foreach($data as $r) {  
              $row[] = array(
                '<input type="checkbox" onclick= "getChecked('.$r->id_pli.')" class="filled-in chkbx" value= "'.$r->id_pli.'" id="ig_checkbox_'.$r->id_pli.'">
                <label for="ig_checkbox_'.$r->id_pli.'"></label>',
                $r->id_pli, 
                $r->nom_societe, 
                $r->typologie,
                $r->date_courrier,
                $r->dt_event,
                $r->lot_scan,
                $r->libelle_motif,
                $r->autre_motif_ko

              );
          }
        }
        $total =  $this->ke_model->getTotal($clause_where, $length, $start);
        $record = sizeof($total)>0?$total[0]->count:0;
        $output = array(
          "draw" => $draw,
          "recordsTotal" =>$record,
          "recordsFiltered" => $record,
          "data" => $row
        );
        echo json_encode($output);
    }

    /*
      former les clauses where
    */
    function wherelike($value, $colonne)
    {
        if(is_array($value)) {
          //convertir array string to array integer
          $value = implode(",", $value);
        }
        if($value != '') {
          $clause_where = "and ".$colonne." in (".$value.") ";
          return $clause_where;
        }
        //var_dump($clause_where);die;
    }

    //where like date
    function whereDateC($dc1, $dc2)
    {
        if($dc1 != '' and $dc2 != '') {
            if($dc1 == $dc2) {
              $where = "and date_courrier::date = '".$dc1."'";
            } else {
              $where = "and date_courrier::date between '".$dc1."' and '".$dc2."'";
            }
            return $where;
        }
        
    }

    function whereDateT($dt1, $dt2)
    {
        if($dt1 != '' and $dt2 != '') {
            if($dt1 == $dt2) {
              $where = "and dt_event::date = '".$dt1."'";
            } else {
              $where = "and dt_event::date between '".$dt1."' and '".$dt2."'";
            }
            return $where;
        }
        
    }

    //changer ke_prio to 1
    function prioriserTous()
    {
      $ids = $this->input->post_get('info');
      $this->load->model('admin/model_ke', 'ke_model');
      if($ids) {
          $this->ke_model->prioriserPli($ids);
      }
    
    }

}
