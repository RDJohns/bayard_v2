<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Support_push extends CI_Controller{

    public function index()
    {
        $this->load->library('portier');
        $this->portier->must_admin();
        $this->showSupport();
    }
    
    //template support push
    public function showSupport()
    {
        $this->load->library('admin/page');
        $this->page->set_titre('Support push');
        $this->page->add_css('../../src/template/css/font-awesome.min');
        $this->page->add_links('Groupe utilisateur', site_url('admin/groups_user'), 'fa fa-users', 'cyan');
        //$this->page->add_links('D&eacute;verrouillage pli', site_url('deverouiller/pli'));
        $this->page->add_links('Matchage', site_url('matchage/matchage'), 'fa fa-copy', 'deep-purple');
        // $this->page->add_links('Nouveau matchage', site_url('matchage/NewMatchage'), 'fa fa-copy', 'deep-purple');
        $this->page->add_links('Matchage ch&egrave;ques cadeaux', site_url('matchage/matchage_ckd'), 'fa fa-gift', 'pink');
        $this->page->add_links('Support', site_url('admin/support'), 'fa fa-cogs', 'light-green');
        // $this->page->add_links('Priorisation des KE', site_url('admin/gestion_ke'),'fa fa-tasks', 'amber');
        $this->page->add_links('DMT', site_url('admin/dmt'), 'fa fa-tachometer', 'purple');
        $this->page->add_links('Qualit&eacute;', site_url('admin/qualite'), 'fa fa-eyedropper', 'lime');
        $this->page->add_links('Regroupements des plis', 'javascript:show_info_grp_pli();', 'fa fa-inbox', 'lime');
		$this->page->add_links('Regroupements des plis <br> par date-courrier', 'javascript:show_info_grp_pli_by_older();', 'fa fa-inbox', 'blue-grey');
		
        $this->page->add_js('admin/support');
        $this->page->add_js('admin/support_push');
        $this->page->add_js('admin/fixedColumns');
        $this->page->add_css('admin/support_push');
        $this->load->model('admin/model_support_push', 'push_model');
        //get source
        $data_source = $this->push_model->getSource();
        //get societe 
        $data_soc = $this->push_model->getSociete();
        //get typologie 
        $data_typo = $this->push_model->getTypologie();
        //statut pli
        $data_stat_pli = $this->push_model->getStatut();
        //recuperer les op typage(type_acces in bdd = 1)
        $op_typage =  $this->push_model->getUserFlux('1,2');
        //recuperer les op saisie(type_acces in bdd = 2,3)
        $op_saisie =  $this->push_model->getUserFlux('2,3');
        //recuperer les motifs blocages
        $motif_blocage =  $this->push_model->getMotifBlocage();
        //recupere etat flux
        $etat_flux = $this->push_model->getEtat();
        $data = array(
          'source' => $data_source,
          'societe' => $data_soc,
          'typologies' => $data_typo,
          'statut' => $data_stat_pli,
          'user_typage' => $op_typage,
          'user_saisie' => $op_saisie,
          'motif' => $motif_blocage,
          'etat' => $etat_flux
        );
        $this->page->afficher($this->load->view('push/support/index', $data, true));
    }

    //get plis selon les critere de recherche
    public function getPlis()
    {
        //load model 
        $this->load->model('admin/model_support_push', 'push_model');
        //datatable data
        $this->load->helper('url');
        $this->load->model("push/typage/Model_Push", "MPush");
        $draw = intval($this->input->post_get("draw"));
        $length            = $this->input->post_get('length');
        $start             = $this->input->post_get('start');
        //critere de recherche
        $id_flux = $this->input->post_get('id_pli');
        $source = $this->input->post_get('source');
        $societe= $this->input->post_get('societe');
        $typologie= $this->input->post_get('typologie');
        $statut = $this->input->post_get('statut');
        $motif = $this->input->post_get('motif');
        $op_saisi = $this->input->post_get('op_saisi');
        $op_typage = $this->input->post_get('op_typage');
        $from_abonne = $this->input->post_get('from_abonne');
        $from_payeur = $this->input->post_get('from_payeur');
        $etat = $this->input->post_get('etat');
        $clause_where = "";
        //s'il y a des critères de recherche définis 
        $clause_where .= $this->wherelike($id_flux, 'flux.id_flux');
        $clause_where .= $this->wherelike($source, 'source.id_source');
        $clause_where .= $this->wherelike($societe, 'societe.id');
        $clause_where .= $this->wherelike($typologie, 'id_typologie');
        $clause_where .= $this->wherelike($statut, 'statut_pli.id_statut');
        $clause_where .= $this->wherelike($motif, 'id_motif');
        $clause_where .= $this->wherelike($op_saisi, 'saisie_par');
        $clause_where .= $this->wherelike($op_typage, 'type_par');
        $clause_where .= $this->wherelike($from_abonne, 'nom_abonne');
        $clause_where .= $this->wherelike($from_payeur, 'nom_payeur');
        $clause_where .= $this->wherelike($etat, 'etat_pli_id');
        //date reception
        $date_recep1 = $this->input->post_get('date_ttr1');
        $date_recep2 = $this->input->post_get('date_ttr2');
        $clause_where .= $this->whereDate($date_recep1, $date_recep2);
        $data =$this->push_model->getFlux($clause_where, $length, $start);
        $row = array();
        $row = $this->extractRow($data, $row);
        $total =  $this->push_model->getTotal($clause_where, $length, $start);
        $record = sizeof($total)>0?$total[0]->count:0;
        $output = array(
          "draw" => $draw,
          "recordsTotal" =>$record,
          "recordsFiltered" => $record,
          "data" => $row
        );
        echo json_encode($output);
        //var_dump($clause_where);die;
    }

    /*
      former les clauses where
    */
    function wherelike($value, $colonne)
    {
        //print_r($value);die;
        if(is_array($value)) {
            //convertir array string to array integer
            $value = implode(",", $value);
        }
        if($value != '') {
            $clause_where = "and ".$colonne." in (".$value.") ";
            if($colonne == 'nom_abonne') {
                $flux_id_abo =  $this->push_model->getIdFluxAbonnne($value);

                if (count($flux_id_abo)>0){
                    $fluAbo = array();
                    foreach($flux_id_abo as $flux){
                            array_push($fluAbo,$flux->id_flux);   
                    }
                    $flux_id_ab = implode(",", $fluAbo);
                    $clause_where = "and flux.id_flux in ($flux_id_ab)";
                }else{
                    $clause_where = "and flux.id_flux = 0";
                }
              }
            if($colonne == 'nom_payeur') {
                $flux_id_pay =  $this->push_model->getIdFluxPayeur($value);

                if (count($flux_id_pay)>0){
                    $fluPay = array();
                    foreach($flux_id_pay as $flux){
                            array_push($fluPay,$flux->id_flux);   
                    }
                    $flux_id_pa = implode(",", $fluPay);
                    $clause_where = "and flux.id_flux in ($flux_id_pa)";
                }else{
                    $clause_where = "and flux.id_flux = 0";
                }
            }
            return $clause_where;
        }   
       
    }

    //where like date
    function whereDate($dt1, $dt2)
    {
        if($dt1 != '' and $dt2 != '') {
            if($dt1 == $dt2) {
                $where = "and date_reception::date = '".$dt1."'";
            } else {
                $where = "and date_reception::date between '".$dt1."' and '".$dt2."'";
            }
            return $where;
        }
    }

    //prioriser un traitement flux
    function prioriser($id_flux)
    {
        $this->load->model('admin/model_support_push', 'push_model');
        $this->push_model->prioriserFlux($id_flux);
    }

    //passer un flux de flag 8 a 9 pour retrraitement
    function retraitement($id_flux)
    {
        $this->load->model('admin/model_support_push', 'push_model');
        $this->push_model->retraiterFlux($id_flux);
    }

    //passer plusieurs flux de flag 8 a 9 pour retrraitement
    function retraiterTous()
    {
        $ids = $this->input->post_get('info');
        $this->load->model('admin/model_support_push', 'push_model');
        if($ids) {
            $this->push_model->retraiterFlux($ids);
        }
    }

    //fetch file 
    public function getFile($fluxID)
    {
        $this->load->model("push/typage/Model_Push", "MPush");
        return $this->MPush->getFile($fluxID);
    }

    public function saveLog($msg)
	{
        $info = $this->session->userdata('info_personnel');
        log_message_push('error', $info.' > Typage flux >'.$msg);
	}

    //download fichier mail ou sftp
    public function download($fluxID)
    {
        $flux = $this->getFile($fluxID);
		    $obj = $flux[0];
        $file = "";
        $this->saveLog(" tentative de download # ".$fluxID);
        if($flux) {
            $file   = "";
            $source = $obj->id_source;
            $this->saveLog(" tentative de download # ".$fluxID." source : ".(int)$source);
            if((int)$source == 1 ) {
                $file = FCPATH.'SFTP_MAIL/mail/'.$obj->emplacement.'/'.$obj->nom_fichier;
                $fileName = $obj->nom_fichier;

            } elseif((int)$source == 2) {
                $emplacement = str_replace('/mnt/ged_bayard/','',$obj->emplacement);
                $file = FCPATH.'SFTP_MAIL/'.$emplacement.'/'.$obj->nom_fichier;
                $fileName = $obj->filename_origin;
            }
        }
        if(file_exists($file)) {
            $this->saveLog(" download fichier existe # ".$fluxID." source : ".$fileName);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            //header('Content-Disposition: attachment; filename='.basename($fileName));
            header('Content-Disposition: attachment; filename="' . basename($fileName).'"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        } else {
            $this->saveLog(" download fichier n'existe pas # ".$fluxID);
        }
       
        
    }

    //processus de deverrouillage flux
    public function unlock($id_pli, $flg_tt)
    {
		$this->load->library('portier');
		$this->portier->must_admin();
		$this->load->model('admin/model_support_push', 'push_model');
		if (is_numeric($id_pli) && is_numeric($flg_tt)) {
			$pli = $this->push_model->pli($id_pli);
			if (is_null($pli)) {
			echo 'Pli introuvable!';
			return '';
			}
			if($pli->statut_pli != $flg_tt){
			echo 'D&eacute;verrouillage annul&eacute; car le pli a chang&eacute; d\'&eacute;tat!';
			return '';
			}
			switch ($flg_tt) {
			//en cours typage
			case '1':
				$this->push_model->unlockTypage($id_pli);              
				break;
			//en cours niveau 1
			case '3':
				$this->push_model->unlockNiv1($id_pli);              
				break;
			//en cours niveau 2
			case '5':
				$this->push_model->unlockNiveau2($id_pli);              
				break;
			//en cours niveau 3
			case '10':
				$this->push_model->unlockRetraitement($id_pli);              
				break;
			default:
				echo 'D&eacute;verrouillage annul&eacute; car le pli est non v&eacute;rrouill&eacute;!';
				return '';
				break;
			}
			//$this->histo->pli($id_pli, 'deverrouillage support');
			echo 1;
			return '';
      }
      echo 'Paramètres non-conformes!';
    }

    //processus d'initialisation flux
    public function init($id_pli, $flg_tt)
    {
        $this->load->library('portier');
        $this->portier->must_admin();
        $this->load->model('admin/model_support_push', 'push_model');
        if(is_numeric($id_pli)) {
            $pli = $this->push_model->pli($id_pli);
            if (is_null($pli)) {
                echo 'Pli introuvable!';
                return '';
            }
            if($pli->statut_pli != $flg_tt) {
                echo 'Initialisation annul&eacute; car le pli a chang&eacute; d\'&eacute;tat!';
                return '';
            }
            $this->push_model->initialiserFlux($id_pli);       
            echo 1;
            return '';
        }
        echo 'Paramètres non-conformes!';
    }

    //reinitialisation manuelle
    public function reinit_flux_manuel(){
        $id_flux_tab = array(13905,13906);
        $this->load->model('admin/model_support_push');
        foreach ($id_flux_tab as $key => $id_flux) {//decommenter la ligne suivante
            //echo '<p>'.$id_flux.($this->model_support_push->initialiserFlux($id_flux, FALSE) ? ' => OK' : ' => KO').'</p>';
        }
        echo '<p>Termine</p>';
    }

    //extract row for datatable
    public function extractRow($data, $row)
    {
      //url download fixhier eml ou excel sftp
    	// $url = $this->config->base_url().'index.php/admin/support_push/download/';
    	$url = $this->config->base_url().'index.php/visualisation/visu_push/download/';
     // $row[] = array();
      	if (count($data)>0) {
		   	foreach($data as $r) {
				//deverrouiller un flux
				if( $r->id_statut == 1 ||  $r->id_statut == 3 ||  $r->id_statut == 5 ||  $r->id_statut == 10 ) { 
					$html =  'title="Deverrouiller"  onclick="unlock('.$r->id_flux.', '.$r->id_statut.')" ';
				} else {
					$html =  'title="Pas de deverrouillage"  disabled ';
				};
				//initialiser les flux
				if( $r->id_statut == 0 ||  $r->id_statut == 7 ) {
					$init =  'title="Initialisation impossible"  disabled ';
				} else {
					$init =  'title="Initialiser"  onclick="initFlux('.$r->id_flux.', '.$r->id_statut.')"';
                };
                
                //gestion des historiques flux
                if(intval($r->id_statut) != 0) {
                    $histo = '<a style="cursor:pointer;" onclick="visu_histo('.$r->id_flux.')">'.$r->date_reception.'</a>';
                } else {
                    $histo = $r->date_reception;
                }

                //gestion des abonnement
                $balise = "disabled ";
                $title_abo = "Pas d'abonnement disponible";
                $dspl = "display:none;";
                
                if( intval($r->nbr_abo)>0) {
                    $balise = 'onclick= "visu_Abonnement('.$r->id_flux.')" ';
                    $title_abo = "Voir abonnement(s)";
                    $dspl = "display:inline-block;";
                  };
                  
                
                //gestion des anomalie
                $title_ano = "Pas d'anomalie";
                $ano = "disabled ";
                $array_etat = array(11,12,14,19,21,22,23,100);
                if(in_array(intval($r->etat_pli_id),$array_etat)) {
                    $ano = 'onclick= "visu_anomalie('.$r->id_flux.')" ';
                    $title_ano = "Voir anomalie(s)";
                };

				$row[] = array(
					$r->id_statut == 8?'
					<input type="checkbox" onclick= "getChecked('.$r->id_flux.')" class="filled-in chkbx" value= "'.$r->id_flux.'" id="ig_checkbox_'.$r->id_flux.'">
					<label for="ig_checkbox_'.$r->id_flux.'"></label>
					':'<input type="checkbox" disabled class="filled-in" value= "'.$r->id_flux.'" id="ig_checkbox_'.$r->id_flux.'">
					<label for="ig_checkbox_'.$r->id_flux.'"></label>',
					'<a class="name_file" href="'.$url.$r->id_flux.'" style="cursor:pointer;">#'.$r->id_flux.'</a>', 
					$r->source, 
					$r->societe, 
					$r->typologie,
					$r->statut,
                    $r->lib_etat,
					//$r->motif,
					$r->motif_transfert,
					$r->motif_escalade,
					$r->op_saisie,
                    $r->op_typage,
                    '<span class="label bg-blue abon" '.$balise.' style="'.$dspl.'" data-toggle="tooltip" data-placement="left" title="'.$title_abo.'">'.$r->nbr_abo.'</span>',
                    '<span class="label bg-'.($r->flag_important == 1 ? 'red">OUI' : 'light-green">NON').'</span>',
					$histo,
					//afficher l'icone de retatraitement si statu en attente consigne(8)
					// $r->id_statut == 8?'<button type="button" onclick="retraitement('.$r->id_flux.')" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left" title="Retraiter">
					// 	<i class="material-icons">settings</i>
					// </button>'
					// 	:
					// '<button type="button" disabled class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left" title="Pas de retraitement">
					// 	<i class="material-icons">settings</i>
                    // </button>',
                    '<button type="button"'.($r->flag_important == 1 ? 'disabled title="Déjà priorisé"':'title="Prioriser"  onclick="prioriser_traitement('.$r->id_flux.')" ').' class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left">
					<i class="material-icons">publish</i>
					</button>', 
                    '<button type="button"'.$html.'class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left" title="Deverrouiller">
					<i class="material-icons">lock_open</i>
					</button>',
                   '<button type="button" '.$init.' class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left">
                    <i class="material-icons">trending_up</i>
                    </button>',
                    '<button type="button" '.$ano.' class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left" title="'.$title_ano.'">
                    <i class="material-icons">announcement</i>
                    </button>'
				);
			}
      };
      return $row;
    }

    //récupérer l'historique d'un flux
    public function histoFLux($idflux)
    {
        $this->load->model('admin/model_support_push', 'push_model');
        $pli['pli_events'] = $this->push_model->getHistoriqueFlux($idflux);
        //var_dump( $pli['pli_events']);die;
        echo $this->load->view('push/support/histo', $pli, TRUE);       
    }

}
