<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dmt extends CI_Controller{

	private $correspondance_field;
	private $colomn_raw_xl;
	private $colomn_raw_xl_width;
	private $colomn_by_user_xl;
	private $colomn_by_user_xl_width;

	public function __construct(){
		parent::__construct();
		$this->correspondance_field = array('id_ged', 'source', 'nom_societe', 'typo', 'nb_mouvement'/*, 'paiements'*/, 'etat', 'dt_deb', 'dt_fin', 'duree', 'matr_gpao');
		$this->colomn_raw_xl = array('Id', 'Source', 'Societe', 'Typologie', 'Nbr mvmnt'/*, 'Paiements'*/, 'Etat', 'Début', 'Fin', 'Durée', 'Par', 'Mtr GPAO', 'Commande');
		$this->colomn_raw_xl_width = array(10, 10, 10, 15, 10/*, 20*/, 10, 30, 30, 20, 15, 10, 10);
		$this->colomn_by_user_xl = array('Login', 'Mtr GPAO', 'Durée');
		$this->colomn_by_user_xl_width = array(15, 10, 20);
		ini_set('memory_limit', '6144M');
	}

    public function index(){
		$this->load->library('portier');
		$this->portier->must_admin();
		$this->go();
    }
    
    public function go(){
		$this->load->library('admin/page');
		$this->page->set_titre('DMT');
		$this->page->add_css('../../src/template/css/font-awesome.min');
		$this->page->add_links('Administration des utilisateurs', site_url('admin/users'), 'fa fa-user', 'cyan-t');
		$this->page->add_links('Groupe utilisateur', site_url('admin/groups_user'), 'fa fa-users', 'cyan');
		//$this->page->add_links('D&eacute;verrouillage pli', site_url('deverouiller/pli'));
        $this->page->add_links('Matchage', site_url('matchage/matchage'), 'fa fa-copy', 'deep-purple');
        $this->page->add_links('Matchage ch&egrave;ques cadeaux', site_url('matchage/matchage_ckd'), 'fa fa-gift', 'pink');
		$this->page->add_links('Support', site_url('admin/support'), 'fa fa-cogs', 'light-green');
		$this->page->add_links('Support-push', site_url('admin/support_push'), 'fa fa-upload', 'teal');
		$this->page->add_links('Priorisation des KE', site_url('admin/gestion_ke'),'fa fa-tasks', 'amber');
		$this->page->add_links('Qualit&eacute;', site_url('admin/qualite'), 'fa fa-eyedropper', 'lime');
		$this->page->add_links('Regroupements des plis', 'javascript:show_info_grp_pli();', 'fa fa-inbox', 'lime');
		$this->page->add_links('Regroupements des plis <br> par date-courrier', 'javascript:show_info_grp_pli_by_older();', 'fa fa-inbox', 'blue-grey');
		$this->page->add_js('admin/dmt/dmt');
		$this->page->add_js('admin/dmt/typage');
		$this->page->add_js('admin/dmt/saisie');
		$this->page->add_js('admin/dmt/ctrl');
		$this->page->add_css('admin/dmt');
		$this->load->model('admin/dmt/model_dmt');
		$data_view = array(
			'status' => $this->model_dmt->status()
			,'types' => $this->model_dmt->typo()
			// ,'md_paiements' => $this->model_dmt->mode_paiement()
			,'socities' => $this->model_dmt->societe()
			,'sources' => array(
				array('id' => 0, 'titre' => 'Courrier')
				,array('id' => 1, 'titre' => 'Mail')
				,array('id' => 2, 'titre' => 'SFTP')
			)
			,'ops' => $this->model_dmt->ops()
		);
		$this->page->afficher($this->load->view('admin/dmt', $data_view, TRUE));
	}

	private function data_filtre($correspondance_field){
		$this->load->helper('function1');
		$dt_act = date_1_2($this->input->post_get('dt_act_1'), $this->input->post_get('dt_act_2'));
		$search = $this->input->post_get('search');
		$order = $this->input->post_get('order');
		return array(
			'offset' => $this->input->post_get('start')
			,'limit' => $this->input->post_get('length')
			,'arg_find' => strtolower($search['value'])
			,'arg_ordo' => $correspondance_field[$order[0]['column']]
			,'sens' => $order[0]['dir']
			,'excel' => FALSE
			,'type' => 0
			,'filtre' => array(
				'dt_act_1' => $dt_act[0]
				,'dt_act_2' => $dt_act[1]
				,'socs' => json_decode($this->input->post_get('socs'))
				,'srcs' => json_decode($this->input->post_get('srcs'))
				,'typos' => json_decode($this->input->post_get('typos'))
				// ,'paies' => json_decode($this->input->post_get('paies'))
				,'status' => json_decode($this->input->post_get('status'))
				,'ops' => json_decode($this->input->post_get('ops'))
				,'id_ged' => $this->input->post_get('id_ged')
				,'id_cmd' => strtolower($this->input->post_get('id_cmd'))
			)
		);
	}

	public function data_typage(){
		$correspondance_field = $this->correspondance_field;
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('admin/dmt/model_dmt_typage');
			$resultat["draw"] = $this->input->post_get('draw');
			$resultat["iTotalDisplayRecords"] = $this->model_dmt_typage->get_lns($this->data_filtre($correspondance_field));
			$lns = $this->model_dmt_typage->get_lns($this->data_filtre($correspondance_field), FALSE);
			$resultat["iTotalRecords"] = $this->model_dmt_typage->lns_total();
			foreach ($lns as $ln) {
				$duree = str_replace('day', 'Jr', $ln->duree);
				$duree = explode('.', $duree);
				$row = array(
					'id_ged' => $ln->id_ged
					,'src' => $ln->source
					,'soc' => $ln->nom_societe
					,'typo' => $ln->typo
					,'nbm' => $ln->nb_mouvement
					// ,'paie' => $ln->paiements
					,'etat' => $ln->etat
					,'dt1' => (new DateTime($ln->dt_deb))->format('d/m/Y H:i:s')
					,'dt2' => (new DateTime($ln->dt_fin))->format('d/m/Y H:i:s')
					,'stemp' => $duree[0]
					,'par' => $ln->login.(is_numeric($ln->matr_gpao) ? ' <small>('.$ln->matr_gpao.')</small>' : '')
					,'cmd' => $ln->cmd
					,'DT_RowAttr' => array(
						'my_part' => 'typage'
					)
					,'DT_RowClass' => 'ln_data'
				);
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
		echo json_encode($resultat);
	}

	public function data_saisie(){
		$correspondance_field = $this->correspondance_field;
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('admin/dmt/model_dmt_saisie');
			$resultat["draw"] = $this->input->post_get('draw');
			$resultat["iTotalDisplayRecords"] = $this->model_dmt_saisie->get_lns($this->data_filtre($correspondance_field));
			$lns = $this->model_dmt_saisie->get_lns($this->data_filtre($correspondance_field), FALSE);
			$resultat["iTotalRecords"] = $this->model_dmt_saisie->lns_total();
			foreach ($lns as $ln) {
				$duree = str_replace('day', 'Jr', $ln->duree);
				$duree = explode('.', $duree);
				$row = array(
					'id_ged' => $ln->id_ged
					,'src' => $ln->source
					,'soc' => $ln->nom_societe
					,'typo' => $ln->typo
					,'nbm' => $ln->nb_mouvement
					// ,'paie' => $ln->paiements
					,'etat' => $ln->etat
					,'dt1' => (new DateTime($ln->dt_deb))->format('d/m/Y H:i:s')
					,'dt2' => (new DateTime($ln->dt_fin))->format('d/m/Y H:i:s')
					,'stemp' => $duree[0]
					,'par' => $ln->login.(is_numeric($ln->matr_gpao) ? ' <small>('.$ln->matr_gpao.')</small>' : '')
					,'cmd' => $ln->cmd
					,'DT_RowAttr' => array(
						'my_part' => 'typage'
					)
					,'DT_RowClass' => 'ln_data'
				);
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
		echo json_encode($resultat);
	}

	public function data_ctrl(){
		$correspondance_field = $this->correspondance_field;
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('admin/dmt/model_dmt_ctrl');
			$resultat["draw"] = $this->input->post_get('draw');
			$resultat["iTotalDisplayRecords"] = $this->model_dmt_ctrl->get_lns($this->data_filtre($correspondance_field));
			$lns = $this->model_dmt_ctrl->get_lns($this->data_filtre($correspondance_field), FALSE);
			$resultat["iTotalRecords"] = $this->model_dmt_ctrl->lns_total();
			foreach ($lns as $ln) {
				$duree = str_replace('day', 'Jr', $ln->duree);
				$duree = explode('.', $duree);
				$row = array(
					'id_ged' => $ln->id_ged
					,'src' => $ln->source
					,'soc' => $ln->nom_societe
					,'typo' => $ln->typo
					,'nbm' => $ln->nb_mouvement
					// ,'paie' => $ln->paiements
					,'etat' => $ln->etat
					,'dt1' => (new DateTime($ln->dt_deb))->format('d/m/Y H:i:s')
					,'dt2' => (new DateTime($ln->dt_fin))->format('d/m/Y H:i:s')
					,'stemp' => $duree[0]
					,'par' => $ln->login.(is_numeric($ln->matr_gpao) ? ' <small>('.$ln->matr_gpao.')</small>' : '')
					,'cmd' => $ln->cmd
					,'DT_RowAttr' => array(
						'my_part' => 'typage'
					)
					,'DT_RowClass' => 'ln_data'
				);
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
		echo json_encode($resultat);
	}

	private function filtre_xl(){
		$this->load->helper('function1');
		$dt_act = date_1_2($this->input->post_get('dt_act_1'), $this->input->post_get('dt_act_2'));
		ini_set('memory_limit', '-1');
		return  array(
			'excel' => TRUE
			,'type' => 1
			,'filtre' => array(
				'dt_act_1' => $dt_act[0]
				,'dt_act_2' => $dt_act[1]
				,'socs' => json_decode($this->input->post_get('socs'))
				,'srcs' => json_decode($this->input->post_get('srcs'))
				,'typos' => json_decode($this->input->post_get('typos'))
				// ,'paies' => json_decode($this->input->post_get('paies'))
				,'status' => json_decode($this->input->post_get('status'))
				,'ops' => json_decode($this->input->post_get('ops'))
				,'id_ged' => $this->input->post_get('id_ged')
				,'id_cmd' => strtolower($this->input->post_get('id_cmd'))
			)
		);
	}

	private function write_raw_data($raw_data){
		foreach ($raw_data as $ln_data) {
			$duree = str_replace('day', 'Jr', $ln_data->duree);
			$duree = explode('.', $duree);
			$this->export_xl->write_nb($ln_data->id_ged);
			$this->export_xl->write_txt($ln_data->source);
			$this->export_xl->write_txt($ln_data->nom_societe);
			$this->export_xl->write_txt($ln_data->typo);
			$this->export_xl->write_nb($ln_data->nb_mouvement);
			// $this->export_xl->write_txt($ln_data->paiements);
			$this->export_xl->write_txt($ln_data->etat);
			$this->export_xl->write_date((new DateTime($ln_data->dt_deb))->format('d/m/Y H:i:s'));
			$this->export_xl->write_date((new DateTime($ln_data->dt_fin))->format('d/m/Y H:i:s'));
			$this->export_xl->write_txt($duree[0]);
			$this->export_xl->write_txt($ln_data->login);
			$this->export_xl->write_nb($ln_data->matr_gpao);
			$this->export_xl->write_txt($ln_data->cmd);
			$this->export_xl->add_void_line();
		}
	}

	private function write_by_user_data($by_user_data){
		foreach ($by_user_data as $ln_data) {
			$duree = str_replace('day', 'Jr', $ln_data->duree);
			$duree = explode('.', $duree);
			$this->export_xl->write_txt($ln_data->login);
			$this->export_xl->write_nb($ln_data->matr_gpao);
			$this->export_xl->write_txt($duree[0]);
			$this->export_xl->add_void_line();
		}
	}

	public function export_typage_xl(){
		$filtres = $this->filtre_xl();
		$this->load->model('admin/dmt/model_dmt_typage');
		$data_typage = $this->model_dmt_typage->get_lns($filtres, FALSE);
		$this->load->library('admin/export_xl');
		$this->export_xl->add_page('Brutes');
		$this->export_xl->write_colomn_title($this->colomn_raw_xl, $this->colomn_raw_xl_width);
		$this->export_xl->add_void_line();
		$this->write_raw_data($data_typage);
		$filtres['type'] = 2;
		$data_by_user = $this->model_dmt_typage->get_lns($filtres, FALSE);
		$this->export_xl->add_page('Par utilisateurs');
		$this->export_xl->write_colomn_title($this->colomn_by_user_xl, $this->colomn_by_user_xl_width);
		$this->export_xl->add_void_line();
		$this->write_by_user_data($data_by_user);
		$this->finition_xl('typage', $filtres['filtre']['dt_act_1'], $filtres['filtre']['dt_act_2']);
	}

	public function export_saisie_xl(){
		$filtres = $this->filtre_xl();
		$this->load->model('admin/dmt/model_dmt_saisie');
		$data_saisie = $this->model_dmt_saisie->get_lns($filtres, FALSE);
		$this->load->library('admin/export_xl');
		$this->export_xl->add_page('Brutes');
		$this->export_xl->write_colomn_title($this->colomn_raw_xl, $this->colomn_raw_xl_width);
		$this->export_xl->add_void_line();
		$this->write_raw_data($data_saisie);
		$filtres['type'] = 2;
		$data_by_user = $this->model_dmt_saisie->get_lns($filtres, FALSE);
		$this->export_xl->add_page('Par utilisateurs');
		$this->export_xl->write_colomn_title($this->colomn_by_user_xl, $this->colomn_by_user_xl_width);
		$this->export_xl->add_void_line();
		$this->write_by_user_data($data_by_user);
		$this->finition_xl('saisie', $filtres['filtre']['dt_act_1'], $filtres['filtre']['dt_act_2']);
	}

	public function export_ctrl_xl(){
		$filtres = $this->filtre_xl();
		$this->load->model('admin/dmt/model_dmt_ctrl');
		$data_ctrl = $this->model_dmt_ctrl->get_lns($filtres, FALSE);
		$this->load->library('admin/export_xl');
		$this->export_xl->add_page('Brutes');
		$this->export_xl->write_colomn_title($this->colomn_raw_xl, $this->colomn_raw_xl_width);
		$this->export_xl->add_void_line();
		$this->write_raw_data($data_ctrl);
		$filtres['type'] = 2;
		$data_by_user = $this->model_dmt_ctrl->get_lns($filtres, FALSE);
		$this->export_xl->add_page('Par utilisateurs');
		$this->export_xl->write_colomn_title($this->colomn_by_user_xl, $this->colomn_by_user_xl_width);
		$this->export_xl->add_void_line();
		$this->write_by_user_data($data_by_user);
		$this->finition_xl('controle', $filtres['filtre']['dt_act_1'], $filtres['filtre']['dt_act_2']);
	}

	private function finition_xl($name='', $dt1, $dt2){
		$this->load->helper('function1');
		$dt1 =  str_replace('-', '', trim($dt1));
		$dt2 =  str_replace('-', '', trim($dt2));
		$name_f = 'DMT_'.$name;
		$name_f = $dt1 != '' ? $name_f.'_from_'.$dt1 : $name_f;
		$name_f = $dt2 != '' ? $name_f.'_to_'.$dt2 : $name_f;
		$token = date('U').mt_rand();
		echo json_encode(array(
			'name' => $this->export_xl->create_file($name_f, $token)
			,'token' => $token
		));
	}

	public function get_xl($name_f=NULL, $token=NULL){
		if(!is_null($name_f) && trim($name_f) != '' && trim($name_f) != '*' && trim($name_f) != '.' && !is_null($token)){
			$this->load->helper('download');
			$this->load->helper('file');
			$file_temp = './'.urlFileTemp.$token.'_token_'.$name_f;
			try {
				$fx = read_file($file_temp);
				unlink($file_temp);
				force_download($name_f, $fx);
			} catch (Exception $exc) {
				show_error('Fichier introuvable!', 500, 'ERREUR');
			}finally{
				unlink($file_temp);
			}
		}else{
			show_error('Fichier introuvable!', 500, 'ERREUR');
		}
	}
    
}
