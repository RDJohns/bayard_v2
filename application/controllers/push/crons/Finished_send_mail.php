<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Finished_send_mail extends CI_Controller{


    private $all_flux;
    private $logos;
    private $config_mail;

    public function __construct(){
        parent::__construct();
        $this->load->model('push/crons/model_finished');
        $this->all_flux = array();
        $url_logos = './assets/images/';
        $this->load->helper('file');
        $this->logos = array(
            '1' => base64_encode(read_file($url_logos . 'bayard-logo.png'))
            ,'2' => base64_encode(read_file($url_logos . 'milan-logo.png'))
        );
        $this->config_mail = array(
            'mailtype' => 'html'
            ,'protocol' => 'smtp'
            ,'mailpath' => '/usr/sbin/sendmail'
            ,'smtp_host' => 'mx.ful.bayard-presse.com'
            ,'smtp_port' => '465'
            ,'wrapchars' => '76'
            ,'validate' => TRUE
            ,'priority' => 3
            ,'smtp_timeout' => '15'
            ,'charset' => 'UTF-8'
            ,'smtp_crypto' => 'ssl'
            ,'useragent' => 'PHPMailer'
        );
    }

    public function index(){
        log_message_push('error', 'info=> running cron "push/cron_finished_send_mail".');
        $this->engin();
        log_message_push('error', 'info=> cron "push/cron_finished_send_mail" terminated.');
    }

    private function engin(){
        $this->load->library('email');
        $this->get_data();
        foreach ($this->all_flux as $key => $flux) {
            $abonnements = $this->model_finished->abonnements($flux->id_flux);
            if($flux->id_source == 1){
                $pjs = $this->model_finished->pjs($flux->id_flux);
                $this->mail_case($flux, $pjs, $abonnements);
            }elseif ($flux->id_source == 2) {
                $this->sftp_case($flux, $abonnements);
            }else{
                log_message_push('error', 'crons/finished_send_mail/engin=> source flux inconnue.');
            }echo '<br>----------------------------------------------------------------------------------<br>';
        }
    }

    private function get_data(){
        $this->all_flux = $this->model_finished->get_data();
    }

    private function mail_case($flux, $pjs, $abonnements){
        $pjs = is_array($pjs) ? $pjs : array();
        $abonnements = is_array($abonnements) ? $abonnements : array();
        $this->email->clear();
        $this->config_mail['smtp_user'] = $flux->mail_login;
        $this->config_mail['smtp_pass'] = $flux->mail_mdp;
        $this->email->initialize($this->config_mail);
        $this->email->from($flux->mail_from, $flux->mail_from_name);
        $this->email->to($flux->mail_to);
        $this->email->subject('Accusé de traitement');
        $data_view = array(
            'logo' => $this->logos[$flux->id_soc]
            ,'soc_name' => $flux->nom_societe
            ,'mail_object' => $flux->objet_flux
            ,'pjs' => $pjs
            ,'multi_pj' => count($pjs) > 1 ? 's' : ''
            ,'with_pj' => count($pjs) > 0
            ,'abonnements' => $abonnements
        );
        echo '<br>'.$flux->id_soc.'smtp_user: '.$flux->mail_login;
        echo '<br>smtp_pass: '.$flux->mail_mdp;
        echo '<br>from: '.$flux->mail_from.' - '.$flux->mail_from_name;
        echo '<br>to: '.$flux->mail_to;
        echo '<br>sujet: Accusé de traitement<br>';
        echo $this->load->view('push/crons/finished_mail', $data_view, TRUE);
        //$this->mail->message($this->load->view('push/crons/finished', $data_view, TRUE));
        //$this->email->send();
    }

    private function sftp_case($flux, $abonnements){
        $abonnements = is_array($abonnements) ? $abonnements : array();
        $this->email->clear();
        $this->config_mail['smtp_user'] = $flux->sftp_login;
        $this->config_mail['smtp_pass'] = $flux->sftp_mdp;
        $this->email->initialize($this->config_mail);
        $this->email->from($flux->sftp_from, $flux->sftp_from_name);
        $this->email->to($flux->sftp_to);
        $this->email->subject('Accusé de traitement');
        $data_view = array(
            'logo' => $this->logos[$flux->id_soc]
            ,'soc_name' => $flux->nom_societe
            ,'nom_fichier_sftp' => $flux->filename_origin
            ,'abonnements' => $abonnements
        );
        echo '<br>smtp_user: '.$flux->sftp_login;
        echo '<br>smtp_pass: '.$flux->sftp_mdp;
        echo '<br>from: '.$flux->sftp_from.' - '.$flux->sftp_from_name;
        echo '<br>to: '.$flux->sftp_to;
        echo '<br>sujet: Accusé de traitement<br>';
        echo $this->load->view('push/crons/finished_sftp', $data_view, TRUE);
        //$this->mail->message($this->load->view('push/crons/finished', $data_view, TRUE));
        //$this->email->send();
    }

}
