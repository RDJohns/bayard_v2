<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_src extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("push/notification_src/Model_NotificationSrc", "MSrc");
        ini_set("memory_limit", "-1");
        set_time_limit(0);
    }

    public function index(){
        $this->send_mail();
    }

    public function send_mail(){

        $module_nom  = "CRON NOTIFICATION TRANSFERT SRC - GED BAYARD" ;
        $nom_fichier = "10.90\GED\bayard\index.php\push\\notification_src\\notificatio_src" ;
        $date_jour   = date("Y-m-d");
        $couple 	 = date("YmdHis");
        $statut      = "debut" ;

        $this->MSrc->add_trace_cron($module_nom, $nom_fichier, $date_jour, $couple, $statut);

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.vivetic.com',
            'smtp_port' => 587,
            'smtp_user' => 'noreply@vivetic.com', // change it to yours
            'smtp_pass' => 'N0reply2015', // change it to yours
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE,
            'wrapchars' => 76,
            'useragent' => 'PHPMailer',
            //'smtp_crypto' => 'ssl',
            'validate' => TRUE,
            'mailpath' => '/usr/sbin/sendmail'
        );

        $adresse_destinataire = array('agences@ful.bayard-presse.com','agences@ful.milan.fr'); // destinataires des mails dont les SRC doivent retourner au client ==> from_flux

        $logo = 'assets/images/vivetic.png';
        $this->load->library('email', $config);
        $this->load->library('zip');

        $res_societe = $this->MSrc->getSociete();

        foreach($res_societe as $societe){
            $res_dest = $this->MSrc->getDestinataire($societe->id);

            foreach ($res_dest as $dest){
                $res_src = $this->MSrc->getSrc($dest->id, 1);
                $data_view_source['source'] = 'Mail';
                if(!empty($res_src)) {
                    foreach ($res_src as $src) {

                        /*$this->zip->read_file('./SFTP_MAIL/mail/'.$src->fichier);
                        list($filename,$ext) = explode('.',$src->nom_fichier);
                        //$this->zip->archive('./temp_src/'.$filename.'.zip');
                        $zip_file = $this->zip->get_zip();*/

                        $subject        = "";
                        $view_mail      = "";
                        $destinataire   = "";

                        if(in_array(strtolower($src->to_flux),$adresse_destinataire)){
                            $data_agence_view["societe"] = $societe->nom_societe;
                            $subject = 'RETOUR DEMANDE SANS TRAITEMENT '.$src->objet_flux;
                            $view_mail = $this->load->view('push/notification_src/notification_agence_view',$data_agence_view,true);
                            $destinataire = $src->from_flux;
                            //$destinataire = "angeline.rakotomalala@vivetic.mg";
                        }
                        else{
                            $subject = 'TRANSFERT VIVETIC VERS SRC ID '.$src->id_flux;
                            $view_mail_source = " ";
                            $data_view_source['flux'] = $src;
                            $view_mail_source .= $this->load->view('push/notification_src/notification_source_view', $data_view_source, true);
                            $data_view["notification_source_view"] = $view_mail_source;
                            $data_view["nom_societe"] = $societe->nom_societe;

                            $view_mail = $this->load->view('push/notification_src/notification_view', $data_view, true);
                            $destinataire = $dest->destinataire_mail;
                            //$destinataire = "mahery.rakotoson@vivetic.mg";
                        }

                        $this->email->set_newline("\r\n");
                        $this->email->from('noreply@vivetic.com', 'Traitement Flux ' . $societe->nom_societe);
                        $this->email->to($destinataire);// change it to yours
                        //$this->email->cc('mahery.rakotoson@vivetic.mg');
                        $this->email->cc('riantsoa.rakotomalala@vivetic.mg,yves.andriamihaja@vivetic.mg,jeancharles.rakotoarimanana@vivetic.mg,mahery.rakotoson@vivetic.mg,expertmetier_bayard@vivetic.mg,coachqualite_bayard@vivetic.mg,yves.raharime@vivetic.mg,bayard.suivimail@vivetic.mg');
                        $this->email->subject($subject);
                        //$this->email->phpmailer->AddStringAttachment($zip_file,$filename.'.zip');
                        $this->email->phpmailer->AddStringAttachment(file_get_contents('./SFTP_MAIL/mail/'.$src->fichier),$src->nom_fichier,'base64','application/octet-stream');

                        if(!in_array(strtolower($src->to_flux),$adresse_destinataire)){
                            $this->email->phpmailer->AddEmbeddedImage($logo, 'logo_vivetic'); // les mails retournés aux courriers bayard et milan affichent le logo vivetic
                        }

                        $this->email->message($view_mail);
                        //$this->zip->clear_data();

                        if ($this->email->send()) {
                            $this->MSrc->add_trace_src($src->id_flux);
                            $this->email->clear(TRUE);
                            echo 'destinataire : '.$dest->id.', id_flux : '.$src->id_flux.' sent.<br>';
                        } else {
                            show_error($this->email->print_debugger());
                        }
                    }
                }
                else{
                    echo 'societe : '.$societe->id.' dest :'.$dest->id.' : aucun envoi';
                }

            }
        }

        $statut = 'termine';
        $this->MSrc->add_trace_cron($module_nom, $nom_fichier, $date_jour, $couple, $statut);
    }
}