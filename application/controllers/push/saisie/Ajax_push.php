<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_push extends CI_Controller{

    public function download_mail($id_flux){
        $this->load->helper('download');
        $this->load->helper('file');
        $this->load->model('push/saisie/model_flux');
        $flux = $this->model_flux->flux_line($id_flux);
        if(!is_null($flux) && $flux->id_source == 1){
            $emplacement = './SFTP_MAIL/mail/'.$flux->emplacement.'/'.$flux->nom_fichier;
            force_download($flux->nom_fichier, read_file($emplacement));
            return '';
        }
        force_download('no_fichier.txt', 'fichier introuvable');
        return '';
    }

    public function download_sftp_file($id_flux){
        $this->load->helper('download');
        $this->load->helper('file');
        $this->load->model('push/saisie/model_flux');
        $flux = $this->model_flux->flux_line($id_flux);

        $obj = $flux;

        if((int)$obj->id_flux_parent > 0 && (int)$obj->decoupe_par > 0 )
        {
            $this->load->library('decoupage/Lib_Sftp',null,'LibSftp');
            $this->LibSftp->downloadFluxPubliposte((int)$obj->id_flux);
        }
        else
        {
            if(!is_null($flux) && $flux->id_source == 2){
                $emplacement = './SFTP_MAIL/sftp/';
                $tab_url = explode('/'.'sftp/', $flux->emplacement);
                $emplacement .= $tab_url[1].'/'.$flux->nom_fichier;
                force_download($flux->filename_origin, read_file($emplacement));
                return '';
            }
            force_download('no_fichier.txt', 'fichier introuvable');
            return '';
        }
    }

    public function new_abonnement($source=NULL, $societe=NULL){
        $this->load->library('push_saisie/abonnement');
        echo $this->abonnement->get_view(NULL, $source, $societe);
    }

    public function options_abonnement_titres($societe=NULL){
        $this->load->library('push_saisie/abonnement');
        echo $this->abonnement->get_options_titres($societe);
    }

    public function options_abonnement_typos($source=NULL, $societe=NULL){
        $this->load->library('push_saisie/abonnement');
        echo $this->abonnement->get_options_typos($source, $societe);
    }

    public function quick_save(){
        $this->load->library('portier');
        $this->portier->must_op_push();
        $this->load->library('push_saisie/flux');
        //var_dump($this->flux->collecte_data());
        echo $this->flux->quick_save() ? '0' : '1';
    }

    public function escalade(){
        $this->load->library('portier');
        $this->portier->must_op_push();
        $this->load->library('push_saisie/flux');
        echo $this->flux->escalade() ? '0' : '1';
    }

    public function saisie_ko(){
        $this->load->library('portier');
        $this->portier->must_op_push();
        $this->load->library('push_saisie/flux');
        echo $this->flux->saisie_ko() ? '0' : '1';
    }

    public function ks(){
        $this->load->library('portier');
        $this->portier->must_op_push();
        $this->load->library('push_saisie/flux');
        echo $this->flux->ks() ? '0' : '1';
    }

    public function hp(){
        $this->load->library('portier');
        $this->portier->must_op_push();
        $this->load->library('push_saisie/flux');
        echo $this->flux->hp() ? '0' : '1';
    }

    public function anomalie_pj(){
        $this->load->library('portier');
        $this->portier->must_op_push();
        $this->load->library('push_saisie/flux');
        echo $this->flux->anomalie_pj() ? '0' : '1';
    }

    public function anomalie_mail(){
        $this->load->library('portier');
        $this->portier->must_op_push();
        $this->load->library('push_saisie/flux');
        echo $this->flux->anomalie_mail() ? '0' : '1';
    }

    public function anomalie_fichier(){
        $this->load->library('portier');
        $this->portier->must_op_push();
        $this->load->library('push_saisie/flux');
        echo $this->flux->anomalie_fichier() ? '0' : '1';
    }
    
	public function list_lot_saisies(){
		$correspondance_field = array('dt', 'identifiant', 'soc', 'src', 'typo', 'nb_flux', 'flux', 'nb_flux_bis', 'flux_bis', 'total');
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('push/saisie/model_flux');
			$resultat["draw"] = $this->input->post_get('draw');
            $offset = $this->input->post_get('start');
            $limit = $this->input->post_get('length');
            $search = $this->input->post_get('search');
            $arg_find = strtolower($search['value']);
            $order = $this->input->post_get('order');
            $arg_ordo = $correspondance_field[$order[0]['column']];
            $sens = $order[0]['dir'];
			$param_plus = array();
			//$param_plus['dt'] = $this->input->post_get('types');
			$resultat["iTotalRecords"] = $this->model_flux->my_lot_total();
			$resultat["iTotalDisplayRecords"] = $this->model_flux->lot_saisie($limit, $offset, $arg_find, $arg_ordo, $sens, $param_plus);
			$lots = $this->model_flux->lot_saisie($limit, $offset, $arg_find, $arg_ordo, $sens, $param_plus, FALSE);
			foreach ($lots as $n => $lot) {
				$dt = $lot->dt == '' ? '' : (new DateTime($lot->dt))->format('d/m/Y H:i');
				$row = array(
					'dt' => $dt
					,'id' => $lot->identifiant
					,'soc' => $lot->soc
					,'src' => $lot->src
					,'typo' => $lot->typo
					,'nb' => $lot->nb_flux
					,'flux' => $lot->flux
					,'nb_b' => $lot->nb_flux_bis
					,'flux_b' => $lot->flux_bis
					,'tt' => $lot->total
				);
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
        echo json_encode($resultat);
	}

}
