<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Retraitement_push extends CI_Controller{

    private $acces_page;

    public function __construct(){
        parent::__construct();
        $this->acces_page = 3;//retraitement
        $this->link_init = site_url('push/saisie/retraitement_push');
        $this->libele_boutons = array('Charger un pli pour saisie niveau 2', 'Un autre pli pour saisie niveau 2');
    }
    
    public function index(){
		$this->load->library('portier');
		$this->portier->must_op_push($this->acces_page);
		$this->go();
    }
    
    private function go(){
        $this->load->library('push_saisie/page');
        $this->page->set_titre('Retraitement');
        $this->page->add_links('saisie niveau 2 ', site_url('push/saisie/saisie_push2'), 'border_color', 'orange');
		$this->page->add_css('push/saisie/page');
		$this->page->add_js('push/saisie/page');
		$this->page->add_js('push/saisie/retraitement');
        $this->page->set_my_url('/push/saisie/saisie_push');
        $this->page->set_my_url($this->link_init);
        $this->load->library('push_saisie/lot_saisie');
        $this->lot_saisie->initialise(FALSE);
        $lbl_bt = $this->lot_saisie->is_new ? $this->libele_boutons[0] : $this->libele_boutons[1];
        $this->page->afficher( $this->lot_saisie->get_init_view($lbl_bt, $this->link_init) );
    }

    public function new_lot_saisie(){
        $this->load->library('portier');
		$this->portier->must_op_push($this->acces_page, TRUE);
        $this->load->library('push_saisie/lot_saisie');
        $this->lot_saisie->initialise(TRUE);
        $lbl_bt = $this->libele_boutons[0];
        echo $this->lot_saisie->get_init_view($lbl_bt, $this->link_init);
    }

    public function load_flux(){
        $this->load->library('portier');
        $this->portier->must_op_push($this->acces_page, TRUE);
        $this->load->model('push/saisie/model_retraitement');
        $this->load->library('push_saisie/filtres');
        $flux = $this->model_retraitement->piocher_flux($this->session->id_utilisateur, $this->filtres->get_value());
        if (is_null($flux)) {
			$this->load->library('push_saisie/lot_saisie');
            $this->lot_saisie->initialise(TRUE);
            $lbl_bt = $this->libele_boutons[0];
            echo $this->lot_saisie->get_init_view($lbl_bt, $this->link_init, 'Aucun flux disponible avec cette sp&eacute;cification!');
		} else {
			$this->saisie_flux($flux);
		}
    }

    public function next_flux(){
        $this->load->library('portier');
        $this->portier->must_op_push($this->acces_page, TRUE);
        $this->load->model('push/saisie/model_retraitement');
        $this->load->library('push_saisie/filtres');
        $flux = $this->model_retraitement->piocher_flux($this->session->id_utilisateur, $this->filtres->get_value());
        if (is_null($flux)) {
			$this->load->library('push_saisie/lot_saisie');
            $this->lot_saisie->initialise(TRUE);
            $lbl_bt = $this->libele_boutons[0];
            echo $this->lot_saisie->get_init_view($lbl_bt, $this->link_init, 'Il n\'y a plus de flux disponible avec cette sp&eacute;cification!');
		} else {
			$this->saisie_flux($flux);
		}
    }

    private function saisie_flux($flux){
        $this->load->model('push/saisie/model_retraitement');
        $this->load->library('push_saisie/flux');
        $info_ke = $this->model_retraitement->info_ke($flux->id_flux);
        echo $this->flux->get_view($flux, $this->model_retraitement->output_status(), FALSE, TRUE, TRUE, 7, $info_ke);
    }

    public function save_saisie_traite(){
        $this->load->library('portier');
        $this->portier->must_op_push($this->acces_page, TRUE);
        $this->load->model('push/saisie/model_flux');
        $this->load->library('push_saisie/lot_saisie');
        $data_lot_saisie = $this->lot_saisie->data_save();
        if(!is_null($data_lot_saisie)){
            $this->load->library('push_saisie/flux');
            $data_save = $this->flux->data_traite();
            if(!is_null($data_save)){
                $bis = FALSE;
                if(!$this->lot_saisie->compatible($data_lot_saisie, $data_save->societe, $data_save->id_source, $data_save->typologie)){
                    $data_save->data_flux['lot_saisie_bis'] = 1;
                    $bis = TRUE;
                }//echo var_dump($data_save);echo var_dump($data_lot_saisie);die();
                $rep_save = $this->model_flux->save_traitement($data_save, $data_lot_saisie);
                if($rep_save[0]){
                    if($bis){
                        echo '2';
                    }else{
                        echo '1';
                    }
                }else{
                    echo $rep_save[1];
                    log_message('error', 'push> saisie> retraitement_push> save_saisie_traite> traitement echoue! '.$rep_save[1].', flux#'.$data_save->id_flux.', op#'.$this->session->id_utilisateur);
                    return '';
                }
            }else {
                log_message('error', 'push> saisie> retraitement_push> save_saisie_traite> no data flux!');
                echo ('Données perdues!');
            }
        }else{
            log_message('error', 'push> saisie> retraitement_push> save_saisie_traite> Lot saisie introuvable!');
            echo ('Lot saisie introuvable!');
        }
    }

    public function unlock_cancel($id_flux){
        $this->load->library('portier');
        $this->portier->must_op_push($this->acces_page, TRUE);
        $this->load->model('push/saisie/model_retraitement');
        if(is_numeric($id_flux)){
            $this->load->library('portier');
            if($this->model_retraitement->unlock_cancel($id_flux)){
                $this->histo->action(55, '', $id_flux);
                $this->histo->flux($id_flux);
                echo '0';
                return '';
            }
            echo 'Ce flux ne vous est plus assigné!';
            return '';
        }else{
            log_message('error', 'push> saisie> retraitement_push> unlock_cancel> id_flux non numerique: '.$id_flux);
            echo 'Flux introuvable!';
        }
    }
    
    public function list_grp_flux_older(){
        $this->load->model('push/saisie/model_retraitement');
        $data = array(
            'grps' => $this->model_retraitement->group_fx_by_older()
        );
        echo $this->load->view('push/saisie/visu_grp_flux', $data, TRUE);
    }
    
	public function download_all_files_ke($id_flux){
		if(is_numeric($id_flux)){
			$this->load->library('zip');
			$this->load->model('push/saisie/model_retraitement');
			$files = $this->model_retraitement->files_ke($id_flux);
			foreach ($files as $key_f => $file) {
				$this->zip->add_data($file->nom_orig, pg_unescape_bytea($file->fichier));
			}
			if(count($files) > 0){
				$this->zip->download('fichiers_consigne_pli'.$id_flux.'.zip');
			}else{
				$this->load->helper('download');
				force_download('no_files.txt', 'Erreur: Aucun fichier trouvé pour le flux#'.$id_flux);
			}
		}else{
			log_message('error', 'push> saisie> retraitement_push> download_all_files_ke: flux introuvable! flux#'.$id_flux);
			force_download('error.txt', 'Erreur: Flux introuvable: flux#'.$id_flux);
		}
	}

}
