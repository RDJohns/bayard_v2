<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/push/decoupage/Depot.php');

/***
 * Class rejet: allow to upload files in local  rejet-ko
 */

class Rejet extends Depot
{

    public $resFile;
    public $res_file_ftp;
    public $myname;
    public $incre           = 0;
    public $checkname       ="";
    public $hexaName    = "";
    public $clientName         = "";
    public $fileExt         = "";
    public $destination;
    public $tempDestination = "/html/remontee_attachment/TEST_SI/TMP_DECOUPAGE/"; 
    public $idFlux;
    private $CI;

    function __construct()
    {
        parent::__construct();
    }
   

  
    
    public function index()
    {
        $this->idFlux = (int)$this->input->post('id');
        $this->destination = $this->tempDestination;
        if (!$this->upload->do_upload('file'))
        {
            echo "error_upload";
        }
        else
        {
            $this->resFile      = array();
            $this->resFile      = $this->upload->data();
                        
            $mode               = 'auto';
            $source             = "";
            $destinationFile    = "";
            $source             = $this->resFile['full_path'];

            $this->hexaName     = $this->resFile['file_name'];
            $this->clientName   = $this->resFile['client_name']; 
            $this->fileExt      = $this->resFile['file_ext'];


            $arrayFichier = array(
                "id_flux"=>$this->idFlux,
                "user_id"=>(int)$this->session->userdata('id_utilisateur'),
                "nom_fichier_client"=>$this->clientName,
                "nom_fichier_dans_serveur"=>$this->hexaName
            );
            $dataLog = array(
                "id_flux"=>$this->idFlux,
                "nom_fichier_client"=>$this->clientName,
                "nom_fichier_dans_serveur"=>$this->hexaName,
                "ip"=>$this->input->ip_address(),
                "user_id"=>(int)$this->session->userdata('id_utilisateur'),
                "type"=>"rejet",
                "action"=>"insertion"
            );
           
            $this->insertNomFichier($arrayFichier,$dataLog);
        }
    }

    public function deleteRejet()
    {
        $this->load->helper('directory');
        
        $map          = directory_map($this->conf['upload_path'], 1);
        $listeFichier = $this->getFichierServeur($_POST['myfile']);

        if($listeFichier)
        {
            foreach ($listeFichier as $item)
            {
                $path           =  $this->tempDestination.$item->nom_fichier_dans_serveur;
                $tmpDestination =  $this->tempDestination;
                
                if(count($map) > 0)
                {
                    foreach($map as $key=>$value)
                    {
                        $tempExist         = str_replace($tmpDestination,"",$value);
                        if(trim($tempExist) == $item->nom_fichier_dans_serveur)
                        {
                            $this->deleteLocal($item->nom_fichier_dans_serveur);
                            $this->deleteInDB($item->nom_fichier_dans_serveur);
                        }
                    }
                }
            }
        }
    }


    public function deplacementRejet()
    {
        $this->load->helper('directory');
        $map         = directory_map($this->conf['upload_path'], 1);

        $idFlux     = $this->input->post('idFlux');
        $fichier    = $this->input->post('fichierRejet');
        $arrayKO    = array();
        
        $flux   = $this->getLocation($idFlux);
        $location   = $flux["dossier_local"];
        $fluxOrig   = $flux["dossier"];
        $ret        = "" ;
        
        if(is_array($fichier) && count($fichier) > 0)
        {
            $j = 1;

            for($i = 0; $i< count($fichier); $i++)
            {
                $fichierServeur = $this->MCDecoupage->getFichierServeur($fichier[$i]);
                $fichierClient  =  $fichier[$i];
        
                foreach($fichierServeur as $item)
                {
                    $checkname =  $item->nom_fichier_dans_serveur;
                    
                    if(count($map) > 0)
                    {
                    
                        foreach($map as $key=>$value)
                        {
                            $tempExist         = str_replace($this->destination,"",$value);
                            if(trim($tempExist) == $checkname)
                            {
                               
                                $filenameRejet = "REJET_KO__".$idFlux."_".$j;

                                $filenameRejet              = pathinfo($item->nom_fichier_client)['filename'].'_REJET_KO__'.$idFlux."_".$j;
                                $extRejet                   = pathinfo($item->nom_fichier_client, PATHINFO_EXTENSION);
                                $fichierFinalRejet          = $filenameRejet.'.'.$extRejet;

                                
                                if (file_exists("DECOUPAGE/rejet_ko/".$item->nom_fichier_dans_serveur)) {
                                    unlink("DECOUPAGE/rejet_ko/".$item->nom_fichier_dans_serveur);
                                }
                                $rename =  rename($this->conf['upload_path'].'/'.$item->nom_fichier_dans_serveur, "DECOUPAGE/rejet_ko/".$item->nom_fichier_dans_serveur);
                                
                                 
                                $dataLog = array(
                                    "id_flux"=>$idFlux,
                                    "nom_fichier_client"=>$fichierClient,
                                    "nom_fichier_dans_serveur"=>$item->nom_fichier_dans_serveur,
                                    "ip"=>$this->input->ip_address(),
                                    "user_id"=>(int)$this->session->userdata('id_utilisateur'),
                                    "action" => "deplacement OK",
                                    "type"=> "rejet");
                                

                                if($rename)
                                {
                                    $j++;
                                    $fluxPubliposte   = array(
                                        "id_flux_parent"=>$idFlux,
                                        "flux_origine"  =>$fluxOrig,
                                        "flux_publiposte" =>$item->nom_fichier_dans_serveur,
                                        "user_id" =>$this->session->userdata('id_utilisateur'),
                                        "type_publiposte"=>"REJET KO",
                                        "fichier"=>$fichierFinalRejet
                                    ) ;
                                    $dataFlux      = array(
                                        "id_flux"=>$idFlux,
                                        "statut_pli"=>20,
                                        "nom_fichier"=> $item->nom_fichier_dans_serveur,
                                        "filename_origin"=>$fichierFinalRejet,
                                        "emplacement"=>"DECOUPAGE/rejet_ko/".$item->nom_fichier_dans_serveur
                                    );

                                    $updateFlux = $this->MCDecoupage->updateFlux((int)$idFlux,$fluxPubliposte,$dataFlux);
                                    
                                    if($updateFlux > 0)
                                    {
                                        $this->MCDecoupage->insertionLogFile($dataLog);
                                    }
                                    
                                    $ret = "OK";
                                    
                                }
                                else
                                { 
                                    $ret = "Erreur de serveur merci réessayer.";
                                }
                            }
                        }
                    }
                }
            }
        }
        echo $ret;
    }

    /**test git */
}


