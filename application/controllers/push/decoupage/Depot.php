<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/***
 * Class Depot: allow to upload files in 10.70
 */

class Depot extends CI_Controller
{

    public $res_file;
    public $res_file_ftp;
    public $myname;
    public $incre           = 0;
    public $checkname       ="";
    public $origine_file    = "";
    public $rawname         = "";
    public $fileext         = "";
    public $destination;
    public $tempDestination = "/Vivetic/TMP_DECOUPAGE/";   
    public $idFlux;
    private $CI;

    function __construct()
    {
        parent::__construct();
        $this->CI       = &get_instance();
        $this->load->model('push/decoupage/Model_FluxSftp','MCDecoupage');
        $this->load->helper('directory');

        $this->conf['upload_path']      = 'depot/temp_decoupage/';
        $this->conf['allowed_types']    = '*';
        $this->conf['max_filename']     = '255';
        $this->conf['remove_spaces']    = false;
        $this->conf['remove_percent']   = false;

        $this->conf['overwrite']        = false;
        $this->conf['detect_mime']      = false;
        $this->conf['encrypt_name']     = true;
        $this->conf['max_size']         = '1073741';
        $this->load->library('upload', $this->conf);
        $this->load->library('ftp');
        /* $config['hostname']             = 'ftp.milan.fr';
        $config['username']             = 'accueil_vivetic@ftp.milan.fr';
        $config['password']             = '1cD2v1RX';
        $config['port']                 = 22;

        $config['debug']                = FALSE; */
        ini_set('max_execution_time', '0'); 
        // $this->ftp->connect($config);
    }

    public function index1()
    {
        echo "1234";
    }
    public function index()
    {
        ini_set('max_execution_time', '0'); 
        $this->idFlux = (int)$this->input->post('id');
        $this->destination = $this->tempDestination;

        if (!$this->upload->do_upload('file'))
        {
            echo "error_upload > ".$this->conf['upload_path'] ;
            echo $this->upload->display_errors();
        }
        else
        {
            $this->res_file     = array();
            $this->res_file     = $this->upload->data();
            
            $source             = "";
            $destinationFile    = "";
            $source             = $this->res_file['full_path'];

            $this->origine_file = $this->res_file['file_name'];
            $this->rawname      = $this->res_file['client_name'];
            $this->fileext      = $this->res_file['file_ext'];

            $arrayFichier = array(
                "id_flux"=>$this->idFlux,
                "user_id"=>(int)$this->session->userdata('id_utilisateur'),
                "nom_fichier_client"=>$this->rawname,
                "nom_fichier_dans_serveur"=>$this->origine_file
            );

            $dataLog = array(
                "id_flux"=>$this->idFlux,
                "nom_fichier_client"=>$this->rawname,
                "nom_fichier_dans_serveur"=>$this->origine_file,
                "ip"=>$this->input->ip_address(),
                "user_id"=>(int)$this->session->userdata('id_utilisateur'),
                "action"=>"insertion"
            );

            $this->insertNomFichier($arrayFichier,$dataLog);
        }
    }
    public function index2()
    {
        //$test = $this->MCDecoupage->getFluxADecoupe();

       // var_dump($test);
       ini_set('max_execution_time', '0'); 
        $this->idFlux = (int)$this->input->post('id');
        $this->destination = $this->tempDestination;
        if (!$this->upload->do_upload('file'))
        {
            echo "error_upload";
            $this->res_file     = array();
            $this->res_file     = $this->upload->data();
                        
            $mode               = 'auto';
            $source             = "";
            $destinationFile    = "";
            $source             = $this->res_file['full_path'];

            $this->origine_file = $this->res_file['file_name'];
            $this->rawname      = $this->res_file['client_name'];
            $this->fileext      = $this->res_file['file_ext'];
        }
        else
        {
            $this->res_file     = array();
            $this->res_file     = $this->upload->data();
                        
            $mode               = 'auto';
            $source             = "";
            $destinationFile    = "";
            $source             = $this->res_file['full_path'];

            $this->origine_file = $this->res_file['file_name'];
            $this->rawname      = $this->res_file['client_name'];
            $this->fileext      = $this->res_file['file_ext'];

            $real_name          = $this->get_file_is_exist($this->res_file['file_name']);

            $destinationFile    = $this->destination.$real_name;

            if($this->ftp->upload($source,$destinationFile, $mode, 0775))
            {
                $this->delete_ftp($this->res_file['file_name']);

                $arrayFichier = array(
                    "id_flux"=>$this->idFlux,
                    "user_id"=>(int)$this->session->userdata('id_utilisateur'),
                    "nom_fichier_client"=>$this->rawname,
                    "nom_fichier_dans_serveur"=>$this->origine_file
                );

                $dataLog = array(
                    "id_flux"=>$this->idFlux,
                    "nom_fichier_client"=>$this->rawname,
                    "nom_fichier_dans_serveur"=>$this->origine_file,
                    "ip"=>$this->input->ip_address(),
                    "user_id"=>(int)$this->session->userdata('id_utilisateur'),
                    "action"=>"insertion"
                );
               
                $this->insertNomFichier($arrayFichier,$dataLog);
            }
            else
            {
                echo "error_upload";
            }
        }
    }

   public function delete()
   {
        $listeFichier = $this->getFichierServeur($_POST['myfile']);
        if($listeFichier)
        {
            foreach ($listeFichier as $item)
            {
                $this->deleteLocal($item->nom_fichier_dans_serveur);
                $this->deleteInDB($item->nom_fichier_dans_serveur);
            }
        }
   }
    public function delete1()
    {
        $listeFichier = $this->getFichierServeur($_POST['myfile']);
        if($listeFichier)
        {
            foreach ($listeFichier as $item)
            {
                $path =  $this->tempDestination.$item->nom_fichier_dans_serveur;
                $tmpDestination =  $this->tempDestination;
                $array                      = $this->ftp->list_files($tmpDestination);

                if(count($array) > 0)
                {
                    foreach($array as $key=>$value){
                        $temp_exist         = str_replace($tmpDestination,"",$value);
                        if(trim($temp_exist ) == $item->nom_fichier_dans_serveur)
                        {
                            $this->deleteInDB($item->nom_fichier_dans_serveur);
                            $this->ftp->delete_file($path);
                        }
                    }
                }
            }
        }
    }

    public function deleteLocal($file)
    {

        $path =  $this->conf['upload_path'].$file;
        unlink($path);
    }

    public function delete_ftp($file)
    {

        $path =  $this->conf['upload_path'].$file;
        unlink($path);
    }

    
    public function get_file_is_exist($checkname)
    {
        $array                      = $this->ftp->list_files($this->destination);

        if(count($array) > 0)
        {
            foreach($array as $key=>$value){
                $temp_exist         = str_replace($this->destination,"",$value);
                $this->myname       = $checkname;
                if(trim($temp_exist ) == $checkname)
                {
                    echo "error_upload : le fichier existe déjà dans le serveur\nMerci de changer le nom : \n"; 
                }

            }
            return $this->myname;
        }
        else
        {
            return $checkname;
        }

    }

    public function insertNomFichier($array,$dataLog)
    {
        return $this->MCDecoupage->insertNomFichier($array,$dataLog);
    }

    public function getFichierServeur($fileClient)
    {
        return $this->MCDecoupage->getFichierServeur($fileClient);
    }

    public function deleteInDB($file)
    {
        return $this->MCDecoupage->deleteInDB($file);
    }

    public function deplacementFinal()
    {
        $idFlux     = $this->input->post('idFlux');
        $fichier    = $this->input->post('fichier');
        $arrayKO    = array();
        $ret        = "";
        
        $flux       = $this->getLocation($idFlux);
        $location   = $flux["dossier_local"];
        $fluxOrig   = $flux["dossier"];
        $map        =  directory_map("./".$this->conf['upload_path'], 1);
        
        if(is_array($fichier) && count($fichier) > 0)
        {
            $k =  1;
            
            for($i = 0; $i< count($fichier); $i++)
            {
                $fichierServeur = $this->MCDecoupage->getFichierServeur($fichier[$i]);
                $fichierClient  =  $fichier[$i];
                foreach($fichierServeur as $item)
                {
                    /*$item->nom_fichier_dans_serveur*/
                    if(in_array($item->nom_fichier_dans_serveur,$map)) 
                    {
                        
                        $dataLog = array(
                            "id_flux"=>$idFlux,
                            "nom_fichier_client"=>$fichierClient,
                            "nom_fichier_dans_serveur"=>$item->nom_fichier_dans_serveur,
                            "ip"=>$this->input->ip_address(),
                            "user_id"=>(int)$this->session->userdata('id_utilisateur'));
                        
                        $file              = pathinfo($item->nom_fichier_client)['filename'].'__'.$idFlux."_".$k;
                        $ext               = pathinfo($item->nom_fichier_client, PATHINFO_EXTENSION);
                        $fichierFinal      = $file.'.'.$ext;
                        
                        

                        if (file_exists($location.$item->nom_fichier_dans_serveur)) {
                            unlink($location.$item->nom_fichier_dans_serveur);
                        }
                        // //echo str_replace(FCPATH,"","/var/www/html/9420/t/bayard/DECOUPAGE/MILAN/TELEVENTE_DPI/aa02c397258a2419abeadd52a2b6c7c4.dll");
                        $moveFile = rename("./".$this->conf['upload_path'].$item->nom_fichier_dans_serveur,$location.$item->nom_fichier_dans_serveur);
                        if($moveFile)
                        {
                            $k++;

                            $fluxPubliposte   = array(
                                "id_flux_parent"=>$idFlux,
                                "flux_origine"  =>$fluxOrig,
                                "flux_publiposte" =>$item->nom_fichier_dans_serveur,
                                "user_id" =>$this->session->userdata('id_utilisateur'),
                                "fichier"=>$fichierFinal
                            ) ;
                            $dataFlux      = array(
                                    "id_flux"=>$idFlux,
                                    "statut_pli"=>0,
                                    "nom_fichier"=> $item->nom_fichier_dans_serveur,
                                    "filename_origin"=>$fichierFinal,
                                    "emplacement"=>str_replace(FCPATH."/","",$location.$item->nom_fichier_dans_serveur)

                            );
                            $updateFlux = $this->MCDecoupage->updateFlux((int)$idFlux,$fluxPubliposte,$dataFlux);
                            
                            if((int)$updateFlux > 0 )
                            {
                                //deplacementFinal
                                $this->CI->histo->action(83, $location.$item->nom_fichier_dans_serveur, $idFlux);
                                $dataLog["action"] = "deplacement OK";
                                $ret               =  "OK";
                            }
                        }
                        else
                        {
                            $dataLog["action"] = "deplacement KO";
                            
                        }

                        $this->MCDecoupage->insertionLogFile($dataLog);
                    }
                }
            }
        }
       /* else
        {
            echo "Pas de fichier";
        }*/

        echo $ret;
    }

    public function deplacementFinal1()
    {
        $idFlux     = $this->input->post('idFlux');
        $fichier    = $this->input->post('fichier');
        $arrayKO    = array();
        
        $location   = $this->getLocation($idFlux);
        
        

        $ret        = "" ;
        
        if(is_array($fichier) && count($fichier) > 0)
        {
            for($i = 0; $i< count($fichier); $i++)
            {
                $fichierServeur = $this->MCDecoupage->getFichierServeur($fichier[$i]);
                $fichierClient  =  $fichier[$i];
                foreach($fichierServeur as $item)
                {
                    // echo "<br/>".$item->nom_fichier_dans_serveur;
                    $array     = $this->ftp->list_files($this->tempDestination);
                    $checkname = $this->tempDestination.$item->nom_fichier_dans_serveur;
                    if(count($array) > 0)
                    {
                        foreach($array as $key=>$value)
                        {
                            $temp_exist         = str_replace($this->destination,"",$value);
                            if(trim($temp_exist ) == $checkname)
                            {
                                $dataLog = array(
                                    "id_flux"=>$idFlux,
                                    "nom_fichier_client"=>$fichierClient,
                                    "nom_fichier_dans_serveur"=>$item->nom_fichier_dans_serveur,
                                    "ip"=>$this->input->ip_address(),
                                    "user_id"=>(int)$this->session->userdata('id_utilisateur'));
                                
                               
                                $retour = $this->ftp->move($this->tempDestination.$item->nom_fichier_dans_serveur, $this->tempDestination.'..'.$location.$fichierClient);
                                
                                if(!$retour)
                                {
                                    $arrayKO[]          = $fichierClient;
                                    $dataLog["action"] = "deplacement KO";
                                }
                                else
                                {
                                    $updateFlux = $this->MCDecoupage->updateFlux((int)$idFlux);
                                    if((int)$updateFlux > 0 )
                                    {
                                        //$this->load->library('decoupage/Lib_Sftp',null,'LibSftp');

                                        //$delete = $this->LibSftp->moveFlux($idFlux);
                                        $this->CI->histo->action(83, $this->tempDestination.'..'.$location.$fichierClient, $idFlux);
                                        $dataLog["action"] = "deplacement OK";
                                        $ret               =  "OK";

                                    }
                                    
                                }
                                $this->MCDecoupage->insertionLogFile($dataLog);
                            }
                        }
                    }
                }
            }
        }
        echo $ret;
    }

    public function getLocation($idFlux)
    {
        $location       = $this->MCDecoupage->getLocation($idFlux);
        if($location)
        {
            $dossierLocal =  dirname(FCPATH.str_replace("Vivetic","DECOUPAGE",$location[0]->dossier_flux))."/";
            return array(
                     "daty"=>date("Y-m-d", strtotime($location[0]->date_reception)),
                     "dossier"=>$location[0]->dossier_flux,
                     "dossier_local"=>"DECOUPAGE/publiposter/"
                    );
        }
        return array();
    }
}


/**test git */
/**test git */