<?php
class Flux_Sftp extends CI_Controller {

    var $col            = array("id_flux","fichier_origine","fichier","date_action","type_publiposte");

    public function __construct()
    {
        parent::__construct();
        
        if((int)$this->session->userdata('id_type_utilisateur') != 6)
        {
            redirect(base_url(),'refresh');
        }
        else
        {
            $this->load->library('decoupage/Lib_Sftp',null,'LibSftp');
            $this->load->model('push/decoupage/Model_FluxSftp','MCDecoupage');
            $this->load->model('push/decoupage/Model_FichierUser','MFichierUser');
        }
    }
    
    public function index2()
    {
        $idFlux = 15630;
        echo $this->LibSftp->downloadFluxPubliposte($idFlux);
    }

    public function downloadFluxPubliposte($idFlux)
    {
        $this->LibSftp->downloadFluxPubliposte($idFlux);
    }

    public function index()
    {
        $this->viewPrincipale();

        //echo str_replace(FCPATH,"","/var/www/html/9420/t/bayard/DECOUPAGE/MILAN/TELEVENTE_DPI/aa02c397258a2419abeadd52a2b6c7c4.dll");
    }
    
    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Découpage fichier";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "content_cut";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'plugins/dropzone/dropzone',
            'plugins/dropzone/basic',
            'plugins/font-awesome/css/font-awesome-animation',
            'plugins/jquery-confirm/jquery-confirm.min',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            '../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap'
        );

        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            'plugins/dropzone/dropzone',
            'plugins/jquery-confirm/jquery-confirm.min',
            'js/pages/tables/jquery-datatable',
            'js/push/decoupage/main'
        );

        $this->LibSftp->moveFlux();
        $this->load->view("main/header",$head);
        $this->load->view("push/decoupage/main");
        $this->load->view("main/footer",$foot);

        /*
         * sftp.vivetic.com
         * bayard_mandat
         * vsX3p#4W!Vum97Tapez un message
         *
         * ftp.milan.fr
         * accueil_vivetic
         * 1cD2v1RX
         * */
    }

    public function getFile()
    {
        $arrayRet = array();
        /*
         * etape = ok (nahazo)
         * etape = vide
         * etape = ko
         * etape = encours
         * */
        $aDecouper = $this->MCDecoupage->getFluxADecoupe();
        /* Exemple : données
         * array(1) { [0]=> object(stdClass)#26 (6) {
         * ["id_flux"]=> string(4) "9739"
         * ["dossier_flux"]=> string(89) "/Vivetic/BAYARD/FICHIERS_STACI_retours_VPC/SUIVI_RETOURS_8508_30092019_30092019-st026.xls"
         * ["societe"]=> string(1) "1"
         * ["nom_societe"]=> string(6) "Bayard"
         * ["decoupe_par"]=> string(3) "454"
         * ["demande_decoupe_par"]=> NULL
         *  } }
         * */


        $fichier = $aDecouper[0];
        if(count($fichier) > 0)
        {
            $arrayRet = $fichier;
            $arrayRet["etape"] = "ok";
            echo json_encode($arrayRet);
        }
        else
        {
            echo json_encode(array("etape"=>"ko","fichier"=>"","id"=>0));
        }
    }

    public function annuler($idFlux=0)
    {
        $annuler =  $this->MCDecoupage->annulerDecoupage($idFlux);
        echo $annuler;
    }

    public function fichierPublipostes()
    {
       
        $data       = array();
        $output     = array();

        
        $publiposte     = $this->MFichierUser->fichierPublipostes();
        $countFiltered  = $this->MFichierUser->countFiltered();
        $countAll       = $this->MFichierUser->countAll();
        
        foreach($publiposte as $itemListe)
        {
            $row        = array();
            $iCol       = 0;
            $url        = base_url();
            
            foreach ($this->col as $itemColonne) 
            {
                
                if($itemColonne == "fichier_origine" || $itemColonne == "fichier" ) 
                {
                    $dowload =  base_url()."index.php/push/decoupage/Flux_Sftp/downloadFichierPubliposte/".$itemListe->id_flux."/".$itemListe->id_decoupe."/".$iCol;
                    $row[]   = "<a style='cursor:pointer;' href='".$dowload."' title='Cliquer pour télécharger'>".$itemListe->$itemColonne."</a>";
                    $iCol++;
                }
                else
                {
                    $row[]  = $itemListe->$itemColonne;
                }
            }
            $data[]         = $row;
        }

        $output = array(
                "draw"            => $_POST['draw'],
                "recordsTotal"    => $countAll,
                "recordsFiltered" => $countFiltered,
                "data"            => $data
            );
    
            echo json_encode($output);
    }

    public function downloadFichierPubliposte($idFlux,$idDecoupage,$type)
    {
        $idDecoupage = $this->LibSftp->downloadFichierPubliposte($idFlux,$idDecoupage,$type);
    }

    
/**test git */
}