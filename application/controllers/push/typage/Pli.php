<?php

class Pli extends CI_Controller
{
    public $retourPli       = array();
    private $CI;
    public $saisieEncours   = 0;
    public $infoUser        = "";
    

    public function __construct()
    {
        parent::__construct();
        $this->CI       = &get_instance();
        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message_push('error', $this->session->userdata('infouser').' session perdue');
		}
		///id_type_utilisateur

		if((int)$this->session->userdata('id_type_utilisateur') != 5)
        {

            $this->saveLog(" Page non autorisee");
            redirect('login');
            exit("");
		}
		$this->setSessionUser();

        $this->load->model("push/typage/Model_Push", "MPush");
		
    }

    public function index()
    {
		$this->viewPrincipale();
	}

    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();
        $champ                      = array();
        $visualistation             = array();


        

        $head["title"]               =  $head["menu"] = "Typage flux";
        $head["icon"]                = "label";
        $head["theme"]               = 'theme-teal';
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/nprogress/nprogress',
            'plugins/magnify/jquery.magnify',
            'plugins/metisMenu/font-awesome',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/sweetalert/sweetalert',
            'css/style',
            'css/themes/all-themes',
            'css/push/typage/typage',
            '../src/template/css/font-awesome.min'
        );


        $foot['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/magnify/jquery.magnify',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'plugins/nprogress/nprogress',
            'js/pages/ui/notifications',
            'js/admin',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/push/typage/typage'
        );


        
        $this->load->view("main/header",$head);

        $this->load->view("push/typage/Visualisation");
		$this->load->view("push/typage/Modal");
        
        
        

        $this->load->view("main/footer",$foot);
        

    }

    public function listeEtatMail()
    {
        $societe         = $this->input->post("societe");
        $libelle      	 = $this->input->post("libelle");

        $boiteMail       = $this->MPush->listeEtatMail( $societe,$libelle);
		$strEtat         = '';
		
		if((int)$this->input->post("click") == 1)
		{
			$this->session->set_userdata('libelle',"");
			$this->saveLog('liste Mail');
		}

        foreach($boiteMail as $itemEtat)
        {

            
			$sessionLibelle = $this->session->userdata('libelle');
			$bg = "";
			if(trim($itemEtat->libelle) == trim($sessionLibelle))
			{
				$bg = ' style="background-color: rgb(204, 204, 204);"';
			}

            $bgSpan = " bg-black ";
            if((int)$itemEtat->nb_flux <= 0)
            {
                $bgSpan = " bg-blue-grey ";
            }

			$class      = implode(unpack("H*", $itemEtat->libelle));
            $strEtat   .= '<a '.$bg.' href="#" onclick="getMail(\''.trim($itemEtat->libelle).'\','.$itemEtat->nb_flux.',\''.$class.'\');" class="list-group-item liste-boite-mail-dossier '.$class.'">';
            $strEtat   .= '<span class="badge '.$bgSpan.'">'.$itemEtat->nb_flux.'</span>&nbsp;&nbsp;'.$itemEtat->libelle;
            $strEtat   .= '</a>';
            $class      = "";
        }


        echo $strEtat;
        
    }
	public function listeSftp()
	{
		$societe         = $this->input->post("societe");
        $libelle         = $this->input->post("libelle");

        $boiteMail       = $this->MPush->listeSftp($societe,$libelle);
		$strEtat         = '';
		
		$this->saveLog('liste dossier');

		if((int)$this->input->post("click") == 1)
		{
			$this->session->set_userdata('libelle',"");
		}
        foreach($boiteMail as $itemEtat)
        {

            
			$sessionLibelle = $this->session->userdata('libelle');
			$bg = "";
			if(trim($itemEtat->nom_dossier) == trim($sessionLibelle))
			{
				$bg = ' style="background-color: rgb(204, 204, 204);"';
			}
			
			$class      = implode(unpack("H*", $itemEtat->nom_dossier));
            $strEtat   .= '<a '.$bg.' href="#" onclick="getSftp(\''.trim($itemEtat->nom_dossier).'\','.$itemEtat->nb_flux.',\''.$class.'\');" class="list-group-item liste-boite-mail-dossier '.$class.'">';

            $bgSpan = " bg-black ";
			if((int)$itemEtat->nb_flux <= 0)
            {
                $bgSpan = " bg-blue-grey ";
            }

			
			$strEtat   .= '<span class="badge '.$bgSpan.'">'.$itemEtat->nb_flux.'</span>&nbsp;&nbsp;'.$itemEtat->nom_dossier;
						
            
            $strEtat   .= '</a>';
            $class      = "";
        }


        echo $strEtat;
	}

    public function getFlux()
    {
        $monFlux    = $this->MPush->getMyFlux();

        $retour     = array();

        if($monFlux > 0 )
        {
            $flux                            = $this->MPush->afficherFlux($monFlux);

            $retour["id_flux"]               = $flux[0]->id_flux;
            $retour["objet_flux"]            = $flux[0]->objet_flux;
            $retour["from_flux"]             = $flux[0]->from_flux;
            $retour["fromname_flux"]         = $flux[0]->fromname_flux;
            $retour["to_flux"]               = $flux[0]->to_flux;
            $retour["nom_fichier"]           = $flux[0]->nom_fichier;
            $retour["filename_eml_origine"]  = $flux[0]->filename_eml_origine;
            $retour["date_envoi_mail"]       = $flux[0]->date_envoi_mail;

        }
        else
        {
            $retour =  array("id_flux"=>0,"retour"=>"");
			$this->saveLog(" Pas de flux");
        }
    }
    
    public function getConsigneClient($idFlux)
    {
        $flux  = $this->MPush->getConsigneClient($idFlux);
        if($flux)
        {
            echo $flux[0]->motif_operateur;
            return "";
        }
        echo ""; return "";
    }
	
    
	
    


    public function getFluxMailAtraiter()
    {
        $societe         = $this->input->post("societe");
        $traitement      = $this->input->post("traitement");
        $libelle         = $this->input->post("libelle");

        $flux            = $this->MPush->getFluxMailAtraiter($societe,$traitement,$libelle);
		
		$this->session->set_userdata('libelle',$libelle);
		
        if($flux == null || $flux == NULL)
        {
            $this->saveLog(" PAS DE FLUX : getFluxMailAtraiter()");
			echo json_encode(array("retour"=>"ko"));
        }
        else
        {
            echo json_encode($flux);
        }

        return "";
        
    }
	
	
	public function getFluxFichierAtraiter()
    {
        $societe         = $this->input->post("societe");
        $traitement      = $this->input->post("traitement");
        $libelle         = $this->input->post("libelle");
		$this->session->set_userdata('libelle',$libelle);
        $flux            = $this->MPush->getFluxMailAtraiter($societe,$traitement,$libelle); 
		
		
        if($flux == null || $flux == NULL)
        {
            $this->saveLog(" PAS DE FLUX : getFluxFichierAtraiter()");
		   echo json_encode(array("retour"=>"ko"));
        }
        else
        {
            echo json_encode($flux);
        }
        return "";
        
    }
	
	

   
    public function getFile($fluxID)
    {
        return $this->MPush->getFile($fluxID);
       
    }

    public function getTypologie()
    {
        
        $iDflux             = $this->input->post("iDflux");
		
		$infoFlux           =		$this->MPush->getSociete($iDflux );
		if($infoFlux)
		{
			$info               = $infoFlux[0];
			$societe            = $info->societe;
			$traitement         = $info->id_source;
			
			$typologie          = $this->MPush->getTypologie($societe,$traitement);
			$this->listeChamp($societe,$traitement, $typologie,$iDflux );
			
		}
       


    }
    public function listeTypologie()
    {
        $societe      = (int)$this->input->post("societe");
        $traitement   = $this->input->post("traitement");

        $typologie    = $this->MPush->getTypologie($societe,$traitement);


        $strTypologie          = "";
        $strTypologie         .= '<select class="form-group form-control" id="typologie" style="max-height: 65px;" data-live-search="true" data-show-subtext="true">';
        $strTypologie         .= '<option value="0" selected disabled="disabled">--Choisir une typologie--</option>';


        foreach ($typologie as $itemTypologie)
        {
            $strTypologie      .= '<option value="'.(int)$itemTypologie->id_typologie.'" >'.$itemTypologie->typologie.'</option>';
        }
        echo $strTypologie;

    }

    public function listeChamp($societe,$traitement, $typologie,$iDflux )
    {
        $champ                  = array();
        $champ['societe']       = (int)$societe;
        $champ['traitement']    = (int)$traitement;
        $champ['typologie']     = $typologie;
		
		$pj                     = $this-> getPJ($iDflux);
		
		$champ['piece']			= 0;
		if($pj)
		{
			 $champ['piece']     = $pj;
		}
        $this->load->view("push/typage/Champ", $champ,false);    
    }
	
	public function getFluxID($idFlux )
	{
		return $this->MPush->afficherFlux((int)$idFlux);
	}
	
	public function envoiAnomalie()
	{
		/*var anomalieData = {idFlux:idFlux,anomalie:anomalie,typeTtmt:typeTtmt,etatFlux:etatFlux};*/
        $idFlux         	     = $this->input->post("idFlux");
		$typeTtmt         	     = intval($this->input->post("typeTtmt"));
		$anomalie 				 = $this->input->post("anomalie");
		$etatFlux				 = $this->input->post("etatFlux");
		$anomalieFlux	         = array();
		$tbFlux			   	     = array();
		$motifConsigne			 = array();
		
		$anomalieFlux['statut_pli_id'] = 2;
		$anomalieFlux['utilisateur']   = intval($this->session->userdata('id_utilisateur'));
		$anomalieFlux['flux_id']       = intval($idFlux);
        $anomalieFlux['anomalie']      =  $anomalie;
        $anomalieFlux['etat_flux']     = intval($etatFlux);

		$tbFlux['type_par']			   = (int)$this->session->userdata('id_utilisateur');
		$tbFlux['statut_pli']		   = 2;
		$tbFlux['etat_pli_id']		   = intval($etatFlux);
		

		$motifConsigne["id_flux"]	                =  intval($idFlux);
		$motifConsigne["motif_operateur_flux"]	    = $anomalie; 
		$motifConsigne["id_etat_pli"]	            = intval($etatFlux);
		$tbFlux["date_dernier_statut_ko"]	        = date('Y-m-d H:i:s');

		

		echo  $this->MPush->saveAnomalie($anomalieFlux,$tbFlux,$motifConsigne,intval($idFlux)); 

	}
	
	public function getSociete($iDflux )
	{
		return $this->MPush->getSociete($iDflux);
	}
	public function getPJ($iDflux)
	{
		return $this->MPush->getPJ($iDflux);
	}
	
	public function saveTypage()
	{
		
		$tableFlux = array();
		$nomPJ     = array();
		
		$ret       = array();
			
		$pj = (int)$this->input->post('pj');
		
		if($pj > 0 )
		{
			$nomPJ = $this->input->post('nomPieceJointeMail');
			$tbPj  = $this->MPush->savePJ($nomPJ);
			
		}
		if($this->input->post('entite'))
		{
			$tableFlux['entite'] = $this->input->post('entite');
		}
		
        $tableFlux['statut_pli']    = 2;
        $tableFlux['etat_pli_id']    = 1;
		$tableFlux['typologie']     = (int)$this->input->post('typologie');
		$tableFlux['nb_abonnement'] = (int)$this->input->post('abonnement');
		$tableFlux['societe']       = (int)$this->input->post('societe');
		$tableFlux['type_par']      = (int)$this->session->userdata('id_utilisateur');
		
		$tbPj = $this->MPush->saveUpdatePushTypage($tableFlux,(int)$this->input->post('idFlux'));
		
		if($tbPj)
		{
			
			/*mandefa mail de update 1 recept* */
			$this->saveLog("Typage terminé id# : ".(int)$this->input->post('idFlux'));
			/*$envoi = $this->envoiMailreception((int)$this->input->post('idFlux')); */
			$ret['retour'] = 'ok';
		}
		else
		{
			$this->saveLog("Typage terminé KO id# : ".(int)$this->input->post('idFlux'));
			$ret['retour'] = 'ko';
		}
		echo json_encode($ret);
		return "";
		
	}

	public function envoiMailreception($idFlux)
	{
		$recepFlux 	= $this->MPush->getFluxIDType($idFlux);
		$envoiMail  = 0;
		$dataView   = array();
		$to         = array();
		$config     = array();
		
		if($recepFlux)
		{
			$obj = $recepFlux[0];
			
			$idSource                    = (int)$obj->id_source;

			$objet                       = "Accusé de réception";

			$dataView['societe']         = (int)$obj->societe;
		
			if($idSource == 1) /*mail*/
			{
				$config['smtp_user']        = trim($obj->mail);
				$config['smtp_pass']        = trim($obj->mdp);
				
				$dataView['objet_flux']     = $obj->objet_flux;
				$dataView['fichier']        = (isset($obj->pj)) ? $obj->pj : "" ;
				$dataView['source']         = 1;
				$envoiMail				    = 1;
                $to['from']                 = $obj->from_flux;
                $to['fromName']             = $obj->fromname_flux;

			}
			else /*sftp*/
			{
				if((int)$obj->envoi_accuse_reception == 1 )
				{
					if((int)$obj->societe == 1 && (int)$obj->societe_bayard == 1)
					{
						if(trim($obj->destinataire_mail_sftp) != "")
						{
							$tmpDest                    = explode('|',$obj->destinataire_mail_sftp);
							$config['smtp_user']        = trim($obj->mail_sftp);
							$config['smtp_pass']        = trim($obj->mdp_sftp);
							$dataView['fichier']        = (isset($obj->fichier_sftp)) ? $obj->fichier_sftp : "" ;
							$envoiMail                  = 1;
							$dataView['source']         = 1;
							
						}
						else
						{
							$envoiMail = 0;
                            $this->saveLog(" envoi_accuse_reception destinataire_mail_sftp) == '' : #".(int)$idFlux);
						}
					}
					elseif((int)$obj->societe == 2 && (int)$obj->societe_milan == 1)
					{
						if(trim($obj->destinataire_mail_sftp) != "")
						{
							$tmpDest                    = explode('|',$obj->destinataire_mail_sftp);
							$config['smtp_user']        = trim($obj->mail_sftp);
							$config['smtp_pass']        = trim($obj->mdp_sftp);
							$dataView['fichier']        = (isset($obj->fichier_sftp)) ? $obj->fichier_sftp : "" ;
							$envoiMail                  = 1;
							$dataView['source']         = 1;
							
						}
						else
						{
							$envoiMail = 0;
                            $this->saveLog(" envoi_accuse_reception destinataire_mail_sftp) == '' : #".(int)$idFlux);
						}
					}
					if(is_array($tmpDest) && count($tmpDest)> 0)
                    {
                        $to['from'] = $tmpDest;
                    }
				}
			}
			
			
			if($envoiMail > 0)
			{
				$config['useragent']        = 'PHPMailer';  
				$config['protocol']         = 'smtp';
				$config['mailpath'] 		= '/usr/sbin/sendmail';
				$config['smtp_host']        = 'mx.ful.bayard-presse.com';




				$config['smtp_port']        = 465 ;
				$config['smtp_timeout']     = 15;                 
				$config['wordwrap']         = true;
				$config['wrapchars']        = 76;
				$config['mailtype']         = 'html';             
				$config['charset']          = 'UTF-8';            
				$config['validate']         = true;
				$config['priority']         = 3;                  
				$config['crlf']             = "\r\n";             
				$config['newline']          = "\r\n";             
				$config['bcc_batch_mode']   = false;
				$config['smtp_crypto']      = 'ssl';
				
				$this->load->library('email',$config);
				
				
				$filename = '';
        
				if((int)$obj->societe == 1 )
				{
					$filename   = 'bayard-logo.png';
				}
				elseif((int)$obj->societe == 2)
				{
					$filename   = 'milan-logo.png';
				}
	
				$base       = BASEPATH;

				$rep        = 'assets/images/'.$filename;
				$directory  = str_replace("system/", $rep, $base);
				
				
				$bodyMail['bodyMail'] = $dataView;
					$fromEmail = $config['smtp_user'] ;
					
				$this->email->SMTPAuth   = true;
				$this->email->SMTPSecure = "ssl";
				$this->email->SMTPSecure = "ssl";
				$this->email->SMTPSecure = 'tls';

				$this->email->set_mailtype('html');
				
				
				$this->email->phpmailer->AddEmbeddedImage($directory, 'logo_mail');
				$viewMail = $this->load->view('email/email_reception.php', $bodyMail, true);
				
				$this->email->from($fromEmail, null);
				
				$toName = "test";

				if($idSource == 1)
                {
                    $this->email->toWithName('harii.andriamanamandrato@vivetic.mg',$to['from']);
                }
                else
                {
                    //$this->email->toWithName('mailmahefarivo@gmail.com',$toName);
                    if(is_array($tmpDest) && count($tmpDest) > 0 )
                    {
                        foreach ($tmpDest as $itemMail)
                        {
                            $this->email->toWithName('harii.andriamanamandrato@vivetic.mg',$itemMail);
                            $this->saveLog(" liste mail ".$itemMail." #".(int)$idFlux);
                        }
                    }
                    else
                    {
                        $envoiMail = 0;
                    }

                }


				
				
				
				
				
			$this->email->subject($objet);

            $this->email->message($viewMail);

            if($envoiMail == 1 )
            {
               /* $this->email->bcc('benjamin.mail007@gmail.com');*/

                $this->email->bcc(array('benjamin.randriamoramanana@vivetic.mg'));

                if(!$this->email->send())
                {
                    $err = 'Message could not be sent.';
                    $err .= 'Mailer Error: ' . $this->mail->ErrorInfo;

                    $this->saveLog(" mail reception non envoyé # : ".(int)$idFlux.' erreur : '.$err);
                  $this->CI->histo->action(70, "Typage flux", $idFlux);
                    echo 'ko';
                }
                else
                {
                    $this->saveLog(" mail reception envoyé # : ".(int)$idFlux);
                    $this->MPush->updateFlagMail($idFlux);
                    //update
                   $this->CI->histo->action(71, "Typage flux", $idFlux);
                    echo "";
                }
            }

			}
			
			$envoiMail = 0;
			
		}
		
	}
	
	public function rejet()
	{
		$tbFlux						   = array();
		
		
		$anomalieFlux				   = array();
		$tbFlux						   = array();
		
		/*var dataRejet = {motif:rejetText,idFlux:idFlux,anomalieType:'fichier'};*/
		
		$statutPliId			       = 15;
		$anomalieType                  = $this->input->post("anomalieType");
        $idFlux         	           = $this->input->post("idFlux");
        $motif         	               = $this->input->post("motif");
		$whereUpdate                   = 0;
		$anomalieFlux['statut_pli_id'] = $statutPliId;
		
		$anomalieFlux['utilisateur']   = (int)$this->session->userdata('id_utilisateur');
		$anomalieFlux['flux_id']       = (int)$this->input->post("idFlux");
		$anomalieFlux['anomalie']      = $motif;
		$tbFlux['type_par']			   = (int)$this->session->userdata('id_utilisateur');
		$tbFlux['statut_pli']		   = $statutPliId;
		$whereUpdate				   = (int)$this->input->post("idFlux");

        $this->saveLog(" debut rejet flux #".(int)$idFlux);

        $this->MPush->deleteAnomalie($whereUpdate);

		$retAnomalie = $this->MPush->saveAnomalie($anomalieFlux,$tbFlux,$whereUpdate);
        $this->saveLog(" tentative rejet flux #".(int)$idFlux);
		
			if($retAnomalie)
			{
				echo 'ok';
                $this->saveLog("rejet flux ok #".(int)$idFlux);
			}
			else
			{
				 echo 'ko';
                 $this->saveLog($this->input->post("idFlux")."flux ko");
			}
		
	}
	
	public function annulerTypage()
	{
		$ret	       = array();
        $this->saveLog("tentative d'annulation");
		$retAnnulation = $this->MPush->annulerTypage();
		
		
		if((int)$retAnnulation > 0)
			{
				$ret['retour'] = 'ko';
                $this->saveLog("annulation ko");
			}
			else
			{
				$ret['retour'] = 'ok';
                $this->saveLog("annulation ok");
			}
		echo json_encode($ret);	
	}
	
	public function setSessionUser()
	{
		$ip =  $_SERVER['REMOTE_ADDR'];
		$userTypage	= (int)$this->session->userdata('id_utilisateur');
		$this->session->set_userdata('info_personnel', $ip." ".$userTypage);
	}
	
	public function saveLog($msg)
	{
		$info = $this->session->userdata('info_personnel');
		log_message_push('error', $info.' > Typage flux >'.$msg);
	}
	
	 public function download($fluxID)
    {
        
        $flux = $this->getFile($fluxID);
		$obj = $flux[0];
        $file = "";

        $this->saveLog(" tentative de download # ".$fluxID);
		if($flux)
		{
			$file   = "";
			$source = $obj->id_source;

            $this->saveLog(" tentative de download # ".$fluxID." source : ".(int)$source);

            if((int)$obj->id_flux_parent > 0 && (int)$obj->decoupe_par > 0 )
            {
                $this->load->library('decoupage/Lib_Sftp',null,'LibSftp');
                $this->LibSftp->downloadFluxPubliposte((int)$obj->id_flux);

            }
            else
            {
                if((int)$source == 1 )// mail
                {
                    $file = FCPATH.'SFTP_MAIL/mail/'.$obj->emplacement.'/'.$obj->nom_fichier;
                    $fileName = $obj->nom_fichier;

                }
                elseif((int)$source == 2)
                {
                    $emplacement = str_replace('/mnt/ged_bayard/','',$obj->emplacement);
                    echo $file = FCPATH.'SFTP_MAIL/'.$emplacement.'/'.$obj->nom_fichier;
                    $fileName = $obj->filename_origin;
                }

                if(file_exists($file))
                {
                    $this->saveLog(" download fichier existe # ".$fluxID." source : ".$fileName);
                    /*header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename='.basename($fileName));
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));
                    ob_clean();
                    flush();
                    readfile($file);*/

                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . basename($fileName).'"');
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));
                    ob_clean();
                    flush();

                    readfile($file);
                    exit;
                }
                else
                {
                    $this->saveLog(" download fichier n'existe pas # ".$fluxID);
                    exit("<b>Erreur de la récupération du fichier</b><br/> <a href='".$_SERVER["HTTP_REFERER"]."'>Cliquer ici pour revenir à la page précedante</a>");
                }
            }


		}



    }

    public function standBy($idFlux)
    {
        /*
         * $tbPj = $this->MPush->saveUpdatePushTypage($tableFlux,(int)$this->input->post('idFlux'));
         * */
        $idFlux = $this->MPush->standBy($idFlux);
        if($idFlux)
        {
            echo 1;
        } else echo 0;
	}

	public function publiposter($idFlux)
	{
		$data = array();
		
		if((int)$idFlux > 0)
		{
			$this->load->model("push/decoupage/Model_FluxSftp", "MDecoupage");
			$update = $this->MDecoupage->publiposter((int)$idFlux);
			$data   = array("retour"=>(int)$update);
		}
		else
		{
			$data   = array("retour"=>0);
		}
		echo json_encode($data);
	}
}
