<?php

class CronMail extends CI_Controller
{
    public $retourPli       = array();
    private $CI;

    public function __construct()
    {
        parent::__construct();
        $this->CI       = &get_instance();
        $this->load->model("push/typage/Model_CronMail", "MCron");
        $this->load->model("push/typage/Model_Push", "MPush");
    }
	
	public function testMail()
	{
		/*$this->envoiMailreception(2309);*/
		
		$arr = array(5425);
		for($i = 0; $i < count($arr); $i++)
		{
			$this->envoiMailreception($arr[$i]);
		}
	}
	
    public function index()
    {
        $this->run();
    }
    public function getFluxType()
    {
        $fluxTypeMailNonEnvoye = $this->MCron->getFluxType();

        if($fluxTypeMailNonEnvoye)
        {
            foreach ($fluxTypeMailNonEnvoye as $itemNonEnvoye)
            {
                $this->envoiMailreception((int)$itemNonEnvoye->id_flux);
            }
        }
    }

    public function getFluxAnomalieType()
    {
        $fluxAnomalieType = $this->MCron->getFluxAnomalieType();

        if($fluxAnomalieType)
        {
            foreach ($fluxAnomalieType as $itemAnomalieType)
            {
                $itemAnomalieType->id_flux;
                $typeTtmt  = (int)$itemAnomalieType->id_source;
                $idFlux    = (int)$itemAnomalieType->id_flux;

                if($typeTtmt == 1 )
                {
                    $flux	            = $this->MCron->getMailInfo($idFlux);
                }
                else
                {
                    $flux	            = $this->MCron->getMailInfoSftp($idFlux);
                }

               $this->envoiMailAnomalie($flux,$typeTtmt,$idFlux);
            }
        }
    }


    public function envoiMailreception($idFlux)
    {
        $recepFlux 	= $this->MPush->getFluxIDType($idFlux);
        $envoiMail  = 0;
        $dataView   = array();
        $to         = array();
        $config     = array();
		
		echo $idFlux;
        if($recepFlux)
        {
            $obj = $recepFlux[0];
            $idSource                    = (int)$obj->id_source;
            $objet                       = "Accusé de réception";
            $dataView['societe']         = (int)$obj->societe;

            $this->saveTraceEnvoiMail($idFlux,2,'[RECEPTION]DEBUT',"");

            if($idSource == 1) /*mail*/
            {
                $config['smtp_user']        = trim($obj->mail);
                $config['smtp_pass']        = trim($obj->mdp);

                $dataView['objet_flux']     = $obj->objet_flux;
                $dataView['fichier']        = (isset($obj->pj)) ? $obj->pj : "" ;
                $dataView['source']         = 1;
                $envoiMail				    = 1;
                $to['from']                 = $obj->from_flux;
                $to['fromName']             = $obj->fromname_flux;

            }
            else /*sftp*/
            {
                if((int)$obj->envoi_accuse_reception == 1 )
                {
                    if((int)$obj->societe == 1 && (int)$obj->societe_bayard == 1)
                    {
                        if(trim($obj->destinataire_mail_sftp) != "")
                        {
                            $tmpDest                    = explode('|',$obj->destinataire_mail_sftp);
                            $config['smtp_user']        = trim($obj->mail_sftp);
                            $config['smtp_pass']        = trim($obj->mdp_sftp);
                            $dataView['fichier']        = (isset($obj->fichier_sftp)) ? $obj->fichier_sftp : "" ;
                            $envoiMail                  = 1;
                            $dataView['source']         = 2;

                        }
                        else
                        {
                            $envoiMail = 0;
                            $this->saveLog(" envoi_accuse_reception destinataire_mail_sftp) == '' : #".(int)$idFlux);
                        }
                    }
                    elseif((int)$obj->societe == 2 && (int)$obj->societe_milan == 1)
                    {
                        if(trim($obj->destinataire_mail_sftp) != "")
                        {
                            $tmpDest                    = explode('|',$obj->destinataire_mail_sftp);
                            $config['smtp_user']        = trim($obj->mail_sftp);
                            $config['smtp_pass']        = trim($obj->mdp_sftp);
                            $dataView['fichier']        = (isset($obj->fichier_sftp)) ? $obj->fichier_sftp : "" ;
                            $envoiMail                  = 1;
                            $dataView['source']         = 2;

                        }
                        else
                        {
                            $envoiMail = 0;
                            $this->saveLog(" envoi_accuse_reception destinataire_mail_sftp) == '' : #".(int)$idFlux);
                        }
                    }
                    if(is_array($tmpDest) && count($tmpDest)> 0)
                    {
                        $to['from'] = $tmpDest;
                    }
                }
            }


            if($envoiMail > 0)
            {
                $config['useragent']        = 'PHPMailer';
                $config['protocol']         = 'smtp';
                $config['mailpath'] 		= '/usr/sbin/sendmail';
                $config['smtp_host']        = 'mx.ful.bayard-presse.com';

                $config['smtp_port']        = 465 ;
                $config['smtp_timeout']     = 15;
                $config['wordwrap']         = true;
                $config['wrapchars']        = 76;
                $config['mailtype']         = 'html';
                $config['charset']          = 'UTF-8';
                $config['validate']         = true;
                $config['priority']         = 3;
                $config['crlf']             = "\r\n";
                $config['newline']          = "\r\n";
                $config['bcc_batch_mode']   = false;
                $config['smtp_crypto']      = 'ssl';

                $this->load->library('email',$config);

                
				
                $bodyMail['bodyMail'] = $dataView;
                $fromEmail = $config['smtp_user'] ;

                $this->email->SMTPAuth   = true;
                $this->email->SMTPSecure = "ssl";
                $this->email->SMTPSecure = "ssl";
                $this->email->SMTPSecure = 'tls';
                $this->email->set_mailtype('html');
				
				$filename = '';
				
				$base       = BASEPATH;
                if((int)$obj->societe == 1 )
                {
                    $filename   = 'bayard-logo.png';
					
                }
                elseif((int)$obj->societe == 2)
                {
                    $filename   = 'milan-logo.png';
					
                }

                
				$rep        = 'assets/images/'.$filename;
				$directory  = str_replace("system/", $rep, $base);
				$this->email->phpmailer->AddEmbeddedImage($directory, 'logo_mail');
				
				
                $viewMail = $this->load->view('email/email_reception.php', $bodyMail, true);
                $this->email->from($fromEmail, null);

                if($idSource == 1)
                {
                    $this->email->toWithName($to['from'],$to['from']);
                    //$this->email->toWithName($to['from'],"xxxxxxxxxxxxxxxx");
                    // $this->email->toWithName('mailmahefarivo@gmail.com',"Mailmahefarivo");
                }
                else
                {
                    if(is_array($tmpDest) && count($tmpDest) > 0 )
                    {
                       foreach ($tmpDest as $itemMail)
                        {
                            $this->email->toWithName($itemMail,$itemMail);
                            // $this->saveLog(" liste mail ".$itemMail." #".(int)$idFlux);
                        }
						
						// $this->email->toWithName($itemMail,$itemMail);
						// $this->email->toWithName('mailmahefarivo@gmail.com',"Mailmahefarivo-10");
                    }
                    else
                    {
                        $envoiMail = 0;
                    }

                }

                $this->email->subject($objet);
                $this->email->message($viewMail);
                $this->saveTraceEnvoiMail($idFlux,2,'[RECEPTION]EN ATTENTE',"envoi = ".$envoiMail);
               if($envoiMail == 1 )
                {
                    $this->email->bcc(array('benjamin.randriamoramanana@vivetic.mg'));

                    if(!$this->email->send())
                    {
                        $err = 'Message could not be sent.';
                        $err .= 'Mailer Error: ' . $this->mail->ErrorInfo;

                        $this->saveLog(" mail reception non envoyé # : ".(int)$idFlux.' erreur : '.$err);
                        $this->CI->histo->action(70, "Typage flux cron", $idFlux);

                        $this->saveTraceEnvoiMail($idFlux,2,'[RECEPTION]ERREUR',"erreur : ".$err);
                        echo 'ko';
                    }
                    else
                    {
                        
						
						$this->saveLog(" mail reception envoyé # : ".(int)$idFlux);
                        $this->MPush->updateFlagMail($idFlux);
                        //update
                        $this->CI->histo->action(71, "Typage flux : cron", $idFlux);
                        $this->saveTraceEnvoiMail($idFlux,2,'[RECEPTION]FINI',"");
                        echo "";
                    }
					$this->email->phpmailer->clearAttachments();
                    $this->email->phpmailer->clearAllRecipients();
                }

            }

            $envoiMail = 0;

        }

    }


    public function envoiMailAnomalie($flux,$typeTtmt,$idFlux)
    {
        $config						= array();
        $bodyMail					= array();
        $destMailSftp               = array();

        $this->load->helper('url');

        $config['useragent']        = 'PHPMailer';
        $config['protocol']         = 'smtp';
        $config['mailpath'] 		= '/usr/sbin/sendmail';
        $config['smtp_host']        = 'mx.ful.bayard-presse.com';
        $config['smtp_port']        = 465 ;
        $config['smtp_timeout']     = 15;
        $config['wordwrap']         = true;
        $config['wrapchars']        = 76;
        $config['mailtype']         = 'html';
        $config['charset']          = 'UTF-8';
        $config['validate']         = true;
        $config['priority']         = 3;
        $config['crlf']             = "\r\n";
        $config['newline']          = "\r\n";
        $config['bcc_batch_mode']   = false;
        $config['smtp_crypto']      = 'ssl';
        $config['bcc_batch_size']   = 200;

        $fromEmail					= "";
        $toName					    = "";
        $to 					    = "";
        $objet 						= "";
        $strObjet 					= "";
        $envoi						= 1;
        $filename			        = "";
        $anomalieType               = "";

        if($flux)
        {
            $objFlux = $flux[0];
            $statutPliId = $objFlux->statut_pli;

            $this->saveTraceEnvoiMail($idFlux,$objFlux->statut_pli,'[ANOMALIE]DEBUT',"");

            $config['smtp_user']        = trim($objFlux->mail);
            $config['smtp_pass']        = trim($objFlux->mdp);
            $fromEmail					= trim($objFlux->mail);
            $anomalie                   = isset($objFlux->anomalie) ? trim($objFlux->anomalie) : " ";
            $envoi						= 1;
            $bodyMail['societe']		= (int)$objFlux->societe;
            $bodyMail['typeTtmt']		= (int)$typeTtmt;

            $filename   				= '';

            

            $strObjet = isset($objFlux->objet_flux)?$objFlux->objet_flux : " ";

            switch($statutPliId)
            {
                case 11: /*mail anomalie*/
                    $anomalieType = "mail";
                    break;

                case 12: /*Anomalie PJ*/
                    $anomalieType = "pj";
                    break;

                case 13: /*Hors périmètre*/
                    $anomalieType = "hp";
                    if((int)$typeTtmt == 2)
                    {
                        $strObjet	 = "HORS PERIMETRE";
                    }
                    break;

                case 14: /*Anomalie fichier*/
                    $anomalieType = "fichier";
                    if((int)$typeTtmt == 2)
                    {
                        $strObjet	 = "ANOMALIE FICHIER";
                    }
                    break;
            }


            $post['anomalie']       = $anomalie;
            $post['idFlux']         = $idFlux;
            $post['anomalieType']   = $anomalieType;

            $this->saveLog(" <br/>envoi Mail flux (anomalie) 02 #".$idFlux);
            if($typeTtmt == 1 )
            {
                 $toName					= trim($objFlux->fromname_flux)." (".trim($objFlux->from_flux).")";
                 $to                        = trim($objFlux->from_flux);
                $strObjet					= $objFlux->objet_flux;
            }
            else
            {
                $envoi	= 0;
                $post['fichier']    	= $objFlux->filename_origin;

                if((int)$objFlux->envoi_mail_hors_p == 1 || (int)$objFlux->envoi_anomalie_fichier == 1)
                {
                    $destMailSftp =  explode('|',$objFlux->destinataire_mail);
                    $envoi	      = 1;
                }
            }
            if(((int)$objFlux->id_source == 2 && (int)$objFlux->envoi_anomalie_fichier == 0 && $statutPliId == 14) || ((int)$objFlux->id_source == 2 && (int)$objFlux->envoi_mail_hors_p == 0 && $statutPliId == 13))
            {
                $envoi	= 0;
                $this->MPush->updateFlagMail($idFlux);
            }

        }
		

        $bodyMail['bodyMail']		= $post;

        $this->load->library('email',$config);
        $this->email->SMTPAuth   = true;
        $this->email->SMTPSecure = "ssl";
        $this->email->SMTPSecure = "ssl";
        $this->email->SMTPSecure = 'tls';

        $this->email->set_mailtype('html');
		
		
		
		$base   = BASEPATH;
		
		if((int)$objFlux->societe == 1 )
        {
            $filename   = 'bayard-logo.png';
			$rep        = 'assets/images/'.$filename;
			$directory  = str_replace("system/", $rep, $base);
			
			$this->email->phpmailer->AddEmbeddedImage($directory, 'logo_mail');
        }
        elseif((int)$objFlux->societe == 2)
        {
            $filename   = 'milan-logo.png';
			 $rep        = 'assets/images/'.$filename;
			$directory  = str_replace("system/", $rep, $base);
			
			$this->email->phpmailer->AddEmbeddedImage($directory, 'logo_mail_milan');
        }
			
       
       
		

        
        $viewMail = $this->load->view('email/email_view.php', $bodyMail, true);

        $this->email->from($fromEmail, null);

        if($typeTtmt == 2 )
        {
            $envoi = 0;
            if(is_array($destMailSftp) && count($destMailSftp) > 0)
            {
                $envoi = 1;
                $this->email->to($destMailSftp);
            }

        }
        else
        {
            // $this->email->toWithName($to,$toName);
            $this->email->toWithName($to,$to);
        }

        $this->email->bcc(array('benjamin.randriamoramanana@vivetic.mg'));

        $this->email->subject($strObjet);
        $this->email->message($viewMail);

        $this->saveTraceEnvoiMail($idFlux,$objFlux->statut_pli,'[ANOMALIE]EN ATTENTE',"envoi = ".$envoi);

        if($envoi == 1)
        {
          if(!$this->email->send()){
           $error =  'Message could not be sent.';
           $error .= 'Mailer Error: ' . $this->mail->ErrorInfo;
           $this->CI->histo->action(69, "Typage flux", (int)$post['idFlux']);

           $this->saveTraceEnvoiMail($idFlux,$objFlux->statut_pli,'[ANOMALIE]ERREUR',"Erreur : ".$error);

          }else{
              $this->MPush->updateFlagMail($idFlux);
              $this->CI->histo->action(68, "Typage flux", (int)$post['idFlux']);

              $this->saveTraceEnvoiMail($idFlux,$objFlux->statut_pli,'[ANOMALIE]FINI',"");
           }
		   
			$this->email->phpmailer->clearAttachments();
			$this->email->phpmailer->clearAllRecipients();
        }
        $envoi = 0;
    }


    public function saveLog($msg)
    {
        $info = "Cron envoi mail: ";
        log_message_push('error', $info.' > Typage flux >'.$msg);
        echo '<br/>'.$info.' > Typage flux >'.$msg;
    }

    public function run()
    {
       $this->getFluxType();
       $this->getFluxAnomalieType();
	   
        /*echo "123456789000000000001";
        $this->envoiMailTest();*/
        /*$this->envoiMailTest();*/
       /* $this->envoiMailTest();*/
	   //echo "1234";
    }




    public function envoiMailTest()
    {
        $config						= array();

        $config['useragent']        = 'PHPMailer';
        $config['protocol']         = 'smtp';
        $config['mailpath'] 		= '/usr/sbin/sendmail';
        $config['smtp_host']        = 'mx.ful.bayard-presse.com';
        $config['smtp_port']        = 465 ;
        $config['smtp_timeout']     = 15;
        $config['wordwrap']         = true;
        $config['wrapchars']        = 76;
        $config['mailtype']         = 'html';
        $config['charset']          = 'UTF-8';
        $config['validate']         = true;
        $config['priority']         = 3;
        $config['crlf']             = "\r\n";
        $config['newline']          = "\r\n";
        $config['bcc_batch_mode']   = false;
        $config['smtp_crypto']      = 'ssl';
        $config['bcc_batch_size']   = 200;

        $config['smtp_user']        = 'groupeurs@ful.bayard-presse.com';
        $config['smtp_pass']        = 'Rh30!nw3';



        $this->load->library('email',$config);

        $this->email->SMTPAuth   = true;
        $this->email->SMTPSecure = "ssl";
        $this->email->SMTPSecure = "ssl";
        $this->email->SMTPSecure = 'tls';
        $this->email->set_mailtype('html');

        $this->email->from("groupeurs@ful.bayard-presse.com", "TEST");

        $this->email->toWithName('mailmahefarivo@gmail.com','$toName');

        $this->email->bcc(array('benjamin.randriamoramanana@vivetic.mg'));

        $this->email->subject("OBJET MAIL");

        $this->email->message("MESSAGE MAIL");

		/*$this->email->send();*/

        $this->saveTraceEnvoiMail(0,0,'etat',"");
	}

    /***
     * @param $fluxID
     * @param $statutFlux
     * @param $etat ('DEBUT','FINI')
     * @param $comms
     */
	public function saveTraceEnvoiMail($fluxID,$statutFlux,$etat,$comms="")
    {
        $data               = array();
        $data['flux_id']    = (int)$fluxID;
        $data['statut_pli'] = (int)$statutFlux;
        $data['etat']       =  $etat;
        $data['comms']      =  $comms;
        $this->MCron->saveTraceMail($data);
    }
}
