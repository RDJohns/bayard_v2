<?php

class Pli extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        set_time_limit(7200);

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }

        $this->load->model("provenance/Model_Provenance", "MProvenance");
        
    }

    public function index()
    {
        
        $this->session->set_userdata('infouser', " IP:". $this->input->ip_address()." LOGIN: ".(int)$this->session->userdata('id_utilisateur'));
        if((int)$_SESSION['id_type_utilisateur'] == 4 )
        {
            $this->viewPrincipale();

        }
        else{
            redirect(base_url());
        }
        
    }

    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Gestion des provenances";
        
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "flag";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'plugins/morrisjs/morris',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            'css/custom/reception',
            'css/custom/provenance',
			'../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/raphael/raphael.min',
            'plugins/morrisjs/morris'
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/custom/provenance'
        );


        $this->load->view("main/header",$head);
        $this->load->view("provenance/provenance",$head);
        $this->load->view("main/footer",$foot);
    }

    public function recapProvenance()
    {
        $strTable   = "";
        
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $recap      = $this->MProvenance->recapProvenance($date1,$date2,(int)$societe);


        $bordure      = '';
        
        $strTable   .= "<table id='provenance-recap' width='100%' class='table table-bordered table-striped table-hover js-basic-example dataTable'><thead><tr>";
        
        $strTable   .= "<th>Provenance</th>";
        $strTable   .= "<th>Total des provenances</th>";
        $strTable   .= "<th>Réseau</th>";
        $strTable   .= "<th>Courrier</th></tr>";
        $strTable   .=" </thead><tbody>";
        
        $reseau = $courrier = $total = 0;

        foreach($recap as $item)
        {
            $strTable     .= "<tr>";
                $strTable .= "<td  >".$item->provenance."</td>";
                $strTable .= "<td  >".$item->provenance_total."</td>";
                $strTable .= "<td  >".$item->provenance_reseau."</td>";
                $strTable .= "<td  >".$item->provenance_courrier."</td>";
            $strTable     .= "</tr>";

            $reseau      += $item->provenance_reseau;
            $courrier    += $item->provenance_courrier;
            $total       += $item->provenance_total;
        }

        $strTable     .= "</tbody><tfoot style='background: #cccccc6e;font-weight: bold'><tr>";
        $strTable     .= "<td  >TOTAL </td>";
        $strTable     .= "<td  >".$total."</td>";
        $strTable     .= "<td  >".$reseau."</td>";
        $strTable     .= "<td  >".$courrier."</td>";
        $strTable     .= "</tr></tfoot>";

        $reseau = $courrier = $total = 0;
        $strTable     .= "</table>";
        echo $strTable;
    }
    
    public function provenanceParDateMilan($date1,$date2)
    {
        $strTable   = "";
       
        $societe    = 2;

        $recap      = $this->MProvenance->provenanceParDateMilan($date1,$date2);
        
        $strTable   .= "<table id='provenance-par-date' class='table table-bordered table-striped table-hover js-basic-example dataTable' ><thead><tr>";
        
        $strTable   .= "<th style='background:white;'>Date courrier</th>";
        $strTable   .= "<th>PAS DE PROVENANCE</th>";
        $strTable   .= "<th>94800 VILLEJUIF</th>";
        $strTable   .= "<th>AUTORISATION 31101</th>";
        $strTable   .= "<th>AUTORISATION 80033</th>";
        $strTable   .= "<th>AUTORISATION 80060</th>";
        $strTable   .= "<th>AUTORISATION 95803</th>";
        $strTable   .= "<th>AUTRES</th>";
        $strTable   .= "<th>CS 13037</th>";
        $strTable   .= "<th>CS 30075</th>";
        $strTable   .= "<th>LR 19827</th>";
        $strTable   .= "<th>LR 22496</th>";
        $strTable   .= "<th>LR 35890</th>";
        $strTable   .= "<th>LR 45891</th>";
        $strTable   .= "<th>LR 90133</th>";
        $strTable   .= "<th>LR 92467</th>";
        $strTable   .= "<th>MILAN REPONSE 22496</th>";
        $strTable   .= "<th>TSA 10029</th>";
        $strTable   .= "<th>TSA 20030</th>";
        $strTable   .= "<th>TSA 30031</th>";
        $strTable   .= "<th>TSA 50006</th>";
        $strTable   .=" </tr></thead><tbody>";

        

        foreach($recap as $item)
        {
            $strTable   .=" <tr>";
                $strTable   .=" <td style='background:white;font-size: 12px;'>".$item->date_courrier."</td>"; 
                $strTable   .=" <td>".$item->provenance_id_0."</td>";
                $strTable   .=" <td>".$item->provenance_id_37."</td>";
                $strTable   .=" <td>".$item->provenance_id_38."</td>";
                $strTable   .=" <td>".$item->provenance_id_39."</td>";
                $strTable   .=" <td>".$item->provenance_id_40."</td>";
                $strTable   .=" <td>".$item->provenance_id_41."</td>";
                $strTable   .=" <td>".$item->provenance_id_42."</td>";
                $strTable   .=" <td>".$item->provenance_id_43."</td>";
                $strTable   .=" <td>".$item->provenance_id_44."</td>";
                $strTable   .=" <td>".$item->provenance_id_45."</td>";
                $strTable   .=" <td>".$item->provenance_id_46."</td>";
                $strTable   .=" <td>".$item->provenance_id_47."</td>";
                $strTable   .=" <td>".$item->provenance_id_48."</td>";
                $strTable   .=" <td>".$item->provenance_id_49."</td>";
                $strTable   .=" <td>".$item->provenance_id_50."</td>";
                $strTable   .=" <td>".$item->provenance_id_51."</td>";
                $strTable   .=" <td>".$item->provenance_id_52."</td>";
                $strTable   .=" <td>".$item->provenance_id_53."</td>";
                $strTable   .=" <td>".$item->provenance_id_54."</td>";
                $strTable   .=" <td>".$item->provenance_id_55."</td>";
                $strTable   .=" </tr>";
        }
        $strTable   .=" </tbody>";
        $strTable     .= "</table>";

        echo $strTable;

    }

    public function provenanceParDateBayard($date1,$date2)
    {
        $strTable    = "";
        
        $recap       = $this->MProvenance->provenanceParDateBayard($date1,$date2);
        
        $strTable   .= "<table id='provenance-par-date' class='table table-bordered table-striped table-hover js-basic-example dataTable' ><thead><tr>";
        $strTable   .= "<th style='background:white;'>Date courrier</th>";
        $strTable   .= "<th>PAS DE PROVENANCE</th>";
        $strTable   .= "<th> 59789 LILLE CEDEX 9 </th>";
        $strTable   .= "<th> AUTORISATION 20018 </th>";
        $strTable   .= "<th> AUTORISATION 52454 </th>";
        $strTable   .= "<th> AUTORISATION 59120 </th>";
        $strTable   .= "<th> AUTORISATION 85802 </th>";
        $strTable   .= "<th> AUTORISATION 95803 </th>";
        $strTable   .= "<th> AUTRES </th>";
        $strTable   .= "<th> BP48  </th>";
        $strTable   .= "<th> BP20029 </th>";
        $strTable   .= "<th> CS 30075 </th>";
        $strTable   .= "<th> LA CROIX </th>";
        $strTable   .= "<th> LR 19827 </th>";
        $strTable   .= "<th> LR 20108 </th>";
        $strTable   .= "<th> LR 22496 </th>";
        $strTable   .= "<th> LR 35890 </th>";
        $strTable   .= "<th> LR 40119 </th>";
        $strTable   .= "<th> LR 45891 </th>";
        $strTable   .= "<th> LR 90133  </th>";
        $strTable   .= "<th> SANS CP </th>";
        $strTable   .= "<th> TOULOUSE 31101 </th>";
        $strTable   .= "<th> TSA10004 </th>";
        $strTable   .= "<th> TSA 20018 </th>";
        $strTable   .= "<th> TSA 10029 </th>";
        $strTable   .= "<th> TSA 10047 </th>";
        $strTable   .= "<th> TSA 10056 </th>";
        $strTable   .= "<th> TSA 10065 </th>";
        $strTable   .= "<th> TSA 20030 </th>";
        $strTable   .= "<th> TSA 30031 </th>";
        $strTable   .= "<th> TSA 43054 </th>";
        $strTable   .= "<th> TSA 50006 </th>";
        $strTable   .= "<th> TSA 60007 </th>";
        $strTable   .= "<th> TSA 70026 </th>";
        $strTable   .= "<th> TSA 70071 </th>";
        $strTable   .= "<th> TSA 80009 </th>";
        $strTable   .= "<th> T 60130 </th>";
        $strTable   .= "<th> T 59075 </th>";        
        $strTable   .=" </tr></thead><tbody>";
        
        foreach($recap as $item)
        {
            $strTable   .=" <tr>";
                $strTable   .=" <td style='background:white;font-size: 12px;'>".$item->date_courrier."</td>"; 
                $strTable   .=" <td>".$item->provenance_id_0."</td>";
                $strTable   .=" <td>".$item->provenance_id_1."</td>";
                $strTable   .=" <td>".$item->provenance_id_2."</td>";
                $strTable   .=" <td>".$item->provenance_id_3."</td>";
                $strTable   .=" <td>".$item->provenance_id_4."</td>";
                $strTable   .=" <td>".$item->provenance_id_5."</td>";
                $strTable   .=" <td>".$item->provenance_id_6."</td>";
                $strTable   .=" <td>".$item->provenance_id_7."</td>";
                $strTable   .=" <td>".$item->provenance_id_8."</td>";
                $strTable   .=" <td>".$item->provenance_id_9."</td>";
                $strTable   .=" <td>".$item->provenance_id_10."</td>";
                $strTable   .=" <td>".$item->provenance_id_11."</td>";
                $strTable   .=" <td>".$item->provenance_id_12."</td>";
                $strTable   .=" <td>".$item->provenance_id_13."</td>";
                $strTable   .=" <td>".$item->provenance_id_14."</td>";
                $strTable   .=" <td>".$item->provenance_id_15."</td>";
                $strTable   .=" <td>".$item->provenance_id_16."</td>";
                $strTable   .=" <td>".$item->provenance_id_17."</td>";
                $strTable   .=" <td>".$item->provenance_id_18."</td>";
                $strTable   .=" <td>".$item->provenance_id_19."</td>";
                $strTable   .=" <td>".$item->provenance_id_20."</td>";
                $strTable   .=" <td>".$item->provenance_id_21."</td>";
                $strTable   .=" <td>".$item->provenance_id_22."</td>";
                $strTable   .=" <td>".$item->provenance_id_23."</td>";
                $strTable   .=" <td>".$item->provenance_id_24."</td>";
                $strTable   .=" <td>".$item->provenance_id_25."</td>";
                $strTable   .=" <td>".$item->provenance_id_26."</td>";
                $strTable   .=" <td>".$item->provenance_id_27."</td>";
                $strTable   .=" <td>".$item->provenance_id_28."</td>";
                $strTable   .=" <td>".$item->provenance_id_29."</td>";
                $strTable   .=" <td>".$item->provenance_id_30."</td>";
                $strTable   .=" <td>".$item->provenance_id_31."</td>";
                $strTable   .=" <td>".$item->provenance_id_32."</td>";
                $strTable   .=" <td>".$item->provenance_id_33."</td>";
                $strTable   .=" <td>".$item->provenance_id_34."</td>";
                $strTable   .=" <td>".$item->provenance_id_35."</td>";
                $strTable   .=" <td>".$item->provenance_id_36."</td>";
            $strTable   .=" </tr>";
        }
        $strTable   .=" </tbody>";
        $strTable     .= "</table>";

        echo $strTable;
        
    }

    public function provenanceParDate()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $retour     = "";

        if($societe == 2)
        {
            $retour = $this->provenanceParDateMilan($date1,$date2);
        }
        else 
        {
            $retour = $this->provenanceParDateBayard($date1,$date2);
        }
        
        echo $retour;
    }

    public function provenanceParTypologie0()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $retour     = "";

        if($societe == 2)
        {
            $retour = $this->provenanceParTypologieMilan($date1,$date2);
        }
        else 
        {
            $retour = $this->provenanceParTypologieBayard($date1,$date2);
        }
        
        echo $retour;
    }

    public function provenanceParTypologieMilan($date1,$date2)
    {
        $strTable    = "";
        
        $recap       = $this->MProvenance->provenanceParTypologieMilan($date1,$date2);
        
        $strTable  .= "<table id='provenance-par-typologie' class='table table-bordered table-striped table-hover js-basic-example dataTable' ><thead><tr>";
        $strTable .= "<th style='background:white;'> Provenance </th>";
        $strTable .= "<th> 3 4 SG La croix </th>";
        $strTable .= "<th> Annulation </th>";
        $strTable .= "<th> Changement d'adresse </th>";
        $strTable .= "<th> Changement de destinataire </th>";
        $strTable .= "<th> Changement titre </th>";
        $strTable .= "<th> Cheque seul </th>";
        $strTable .= "<th> Courrier </th>";
        $strTable .= "<th> Création </th>";
        $strTable .= "<th> Création VPC </th>";
        $strTable .= "<th> Croisement relance </th>";
        $strTable .= "<th> Envoi de numéro </th>";
        $strTable .= "<th> Gratuit </th>";
        $strTable .= "<th> Paiement facture </th>";
        $strTable .= "<th> REAB avec courrier </th>";
        $strTable .= "<th> REAB complexe </th>";
        $strTable .= "<th> REAB simple </th>";
        $strTable .= "<th> Réexpedition </th>";
        $strTable .= "<th> Réfus portage </th>";
        $strTable .= "<th> Retour PND </th>";
        $strTable .= "<th> STOP relance </th>";
        $strTable .= "<th> Transfert SRC </th>";
        $strTable  .=" </tr></thead><tbody>";
        foreach($recap as $item)
        {
            $strTable   .=" <tr>";
                $strTable   .=" <td style='background:white;'>".$item->provenance."</td>";
                $strTable   .=" <td>".$item->typologie_2."</td>";
                $strTable   .=" <td>".$item->typologie_3."</td>";
                $strTable   .=" <td>".$item->typologie_4."</td>";
                $strTable   .=" <td>".$item->typologie_5."</td>";
                $strTable   .=" <td>".$item->typologie_6."</td>";
                $strTable   .=" <td>".$item->typologie_7."</td>";
                $strTable   .=" <td>".$item->typologie_9."</td>";
                $strTable   .=" <td>".$item->typologie_10."</td>";
                $strTable   .=" <td>".$item->typologie_11."</td>";
                $strTable   .=" <td>".$item->typologie_12."</td>";
                $strTable   .=" <td>".$item->typologie_13."</td>";
                $strTable   .=" <td>".$item->typologie_16."</td>";
                $strTable   .=" <td>".$item->typologie_19."</td>";
                $strTable   .=" <td>".$item->typologie_20."</td>";
                $strTable   .=" <td>".$item->typologie_21."</td>";
                $strTable   .=" <td>".$item->typologie_22."</td>";
                $strTable   .=" <td>".$item->typologie_23."</td>";
                $strTable   .=" <td>".$item->typologie_24."</td>";
                $strTable   .=" <td>".$item->typologie_25."</td>";
                $strTable   .=" <td>".$item->typologie_26."</td>";
                $strTable   .=" <td>".$item->typologie_27."</td>";
                
            $strTable       .=" </tr>";
        }

         $strTable          .=" </tbody>";
         echo $strTable     .= "</table>";

    }
    public function provenanceParTypologieBayard($date1,$date2)
    {
        $strTable  = "";
        
        $recap     = $this->MProvenance->provenanceParTypologieBayard($date1,$date2);
        
        $strTable .= "<table id='provenance-par-typologie' class='table table-bordered table-striped table-hover js-basic-example dataTable' width='100%'><thead><tr>";
        $strTable .= "<th>provenance</th>";
        $strTable .= "<th>Comité d'entreprise </th>";
        $strTable .= "<th>Famille complexe </th>";
        $strTable .= "<th>Famille simple </th>";
        $strTable .= "<th>Institutionnel complexe </th>";
        $strTable .= "<th>Institutionnel simple </th>";
        $strTable  .=" </tr></thead><tbody>";

        foreach($recap as $item)
        {
            $strTable   .=" <tr>";
                $strTable   .=" <td>".$item->provenance."</td>";
                $strTable   .=" <td>".$item->typologie_8."</td>";
                $strTable   .=" <td>".$item->typologie_14."</td>";
                $strTable   .=" <td>".$item->typologie_15."</td>";
                $strTable   .=" <td>".$item->typologie_17."</td>";
                $strTable   .=" <td>".$item->typologie_18."</td>";
            $strTable   .=" </tr>";
        }

        $strTable   .=" </tbody>";
        echo $strTable     .= "</table>";


    }

    public function provenanceParTypologie() 
    {
        $strTable  = "";
        $strTable .= "<table id='provenance-par-typologie' class='table table-bordered table-striped table-hover js-basic-example dataTable' width='100%'><thead><tr>";
        $strTable .= "<th> 	Provenance	</th>";
        $strTable .= "<th> 	3 4 SG La croix	</th>";
        $strTable .= "<th> 	Annulation	</th>";
        $strTable .= "<th> 	Changement d'adresse	</th>";
        $strTable .= "<th> 	Changement de destinataire	</th>";
        $strTable .= "<th> 	Changement titre	</th>";
        $strTable .= "<th> 	Cheque seul	</th>";
        $strTable .= "<th> 	Comité d'entreprise	</th>";
        $strTable .= "<th> 	Courrier	</th>";
        $strTable .= "<th> 	Création	</th>";
        $strTable .= "<th> 	Création VPC	</th>";
        $strTable .= "<th> 	Croisement relance	</th>";
        $strTable .= "<th> 	Envoi de numéro	</th>";
        $strTable .= "<th> 	Famille complexe	</th>";
        $strTable .= "<th> 	Famille simple	</th>";
        $strTable .= "<th> 	Gratuit	</th>";
        $strTable .= "<th> 	Institutionnel complexe	</th>";
        $strTable .= "<th> 	Institutionnel simple	</th>";
        $strTable .= "<th> 	Paiement facture	</th>";
        $strTable .= "<th> 	REAB avec courrier	</th>";
        $strTable .= "<th> 	REAB complexe	</th>";
        $strTable .= "<th> 	REAB simple	</th>";
        $strTable .= "<th> 	Réexpedition	</th>";
        $strTable .= "<th> 	Réfus portage	</th>";
        $strTable .= "<th> 	Retour PND	</th>";
        $strTable .= "<th> 	STOP relance	</th>";
        $strTable .= "<th> 	Transfert SRC	</th>";
        $strTable  .=" </tr></thead><tbody>";

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $recap      = $this->MProvenance->provenanceParTypologie($date1,$date2,(int)$societe);

         foreach($recap as $item)
        {
            $strTable   .=" <tr>";
            $strTable   .=" <td style='background:white;'>".$item->provenance."</td>";
            $strTable .=" <td>".$item->info_2."</td>";
            $strTable .=" <td>".$item->info_3."</td>";
            $strTable .=" <td>".$item->info_4."</td>";
            $strTable .=" <td>".$item->info_5."</td>";
            $strTable .=" <td>".$item->info_6."</td>";
            $strTable .=" <td>".$item->info_7."</td>";
            $strTable .=" <td>".$item->info_8."</td>";
            $strTable .=" <td>".$item->info_9."</td>";
            $strTable .=" <td>".$item->info_10."</td>";
            $strTable .=" <td>".$item->info_11."</td>";
            $strTable .=" <td>".$item->info_12."</td>";
            $strTable .=" <td>".$item->info_13."</td>";
            $strTable .=" <td>".$item->info_14."</td>";
            $strTable .=" <td>".$item->info_15."</td>";
            $strTable .=" <td>".$item->info_16."</td>";
            $strTable .=" <td>".$item->info_17."</td>";
            $strTable .=" <td>".$item->info_18."</td>";
            $strTable .=" <td>".$item->info_19."</td>";
            $strTable .=" <td>".$item->info_20."</td>";
            $strTable .=" <td>".$item->info_21."</td>";
            $strTable .=" <td>".$item->info_22."</td>";
            $strTable .=" <td>".$item->info_23."</td>";
            $strTable .=" <td>".$item->info_24."</td>";
            $strTable .=" <td>".$item->info_25."</td>";
            $strTable .=" <td>".$item->info_26."</td>";
            $strTable .=" <td>".$item->info_27."</td>";
            $strTable   .=" </tr>";
        } 

        $strTable   .=" </tbody>";
        echo $strTable     .= "</table>";

    }

}