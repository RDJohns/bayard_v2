<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Anomalie extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('portier');
        //$this->portier->must_observ();
		$this->load->model('anomalie/Model_anomalie', 'mvisu');
    }

    public function index(){
       
        $this->visu_anomalie();
       // $this->stat_traitement();

    }

  
    public function visu_anomalie(){
		ini_set('memory_limit', '-1');
		$type_statut   = $this->mvisu->get_statut_traitement();
		$statut_saisie = $this->mvisu->get_statut_saisie();
		
        $array_view_statut = array();

       if($type_statut) $array_view_statut['type_statut'] = $type_statut;

        $data["menu_acif"]="Extraction";
		$this->load->view('anomalie/header.php');
        $this->load->view('anomalie/header_anomalie.php',$data);
		if($type_statut){
            $this->load->view('anomalie/visu_anomalie.php', $array_view_statut);
        }
        else{
            echo 0;
        }
        $this->load->view('anomalie/footer.php');
		$this->load->view('anomalie/footer_anomalie.php');
    }
	
	
	 public function get_anomalie_pli()
    {
		ini_set('memory_limit', '-1');
        $statut         = $this->input->post('statut');
        $date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');

      
		 $this->load->view('anomalie/anomalie.php');
    }
	public function ajax_detail_anomalie(){
		
		$arr = array();

		ini_set('memory_limit', '-1');
        $select_statut 		= $this->input->post('select_statut');
        $select_mp 	= $this->input->post('select_mp');
        $select_soc 	= $this->input->post('select_soc');
        $date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');
        $sel_stat_saisie   = $this->input->post('stat_saisie');
		$extract = 0;
			
		$where_date_courrier   = $where_date_courrier_ttt = $where_other = $where_f_courrier = $where_f_ttt = $where_mode_paie ="";
		$where_statut 		   = "";
		
		
		if($date_debut != '' && $date_fin != ''){
				$where_date_courrier 	.=  " and (f_lot_numerisation.date_courrier between '".$date_debut."' and '".$date_fin."' )";
				$where_f_courrier .= "'".$date_debut."' , '".$date_fin."'";
				
        }
		
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " and (f.dt_event::date between '".$date_debut_ttt."' and '".$date_fin_ttt."') ";
				$where_f_ttt .= "'".$date_debut_ttt."' , '".$date_fin_ttt."'";
		}
		//var_dump($select_soc);die;
		if($select_soc != ''){		  
			$where_other .= "  and f_pli.societe = '".$select_soc."' ";
		}
		
		if($select_statut != ""){ 
			$where_other .= ' and f_pli.flag_traitement = '.$select_statut.' ' ;
		}
		
		if($sel_stat_saisie != ""){ 
			$where_other .= ' and statut_saisie.id_statut_saisie  = '.$sel_stat_saisie.' AND   flag_rewrite_status_client = 0' ;
		}
		if($select_mp != ""){ 
           //$where_other .= ' and mode_paiement.id_mode_paiement = '.$select_mp.' ' ;
		   $where_mode_paie .= " and (id_mp ilike '".$select_mp.",%' or id_mp ilike '%,".$select_mp.",%' or id_mp ilike ',".$select_mp."%' or id_mp ='".$select_mp."')";
		}
		
		if($date_debut != '' && $date_fin != '' && $date_debut_ttt != '' && $date_fin_ttt != ''){
			$extract = 1; //par date de courrier et date de traitement
		}else{
			if($date_debut != '' && $date_fin != '' && $date_debut_ttt == '' && $date_fin_ttt == ''){
				$extract = 2; //par date de courrier
			}
			if($date_debut == '' && $date_fin == '' && $date_debut_ttt != '' && $date_fin_ttt != ''){
				$extract = 3; //par date de traitement
			}
		}		

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}//else echo "vide";
		
		//echo $col." ".$dir; 
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}

		$arr = $this->mvisu->get_datatables_anomalie($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$length,$start,$post_order,$search,$col,$dir,$extract);
		//var_dump($arr);die;
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_anomalie($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$extract);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_anomalie($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$post_order,$search,$col,$dir,$extract);
		$no = $this->input->post('start');
		
		foreach($arr as $p){
				$row = array(
					'nom_societe' => $p->nom_societe,					
                    'date_courrier' => $p->date_courrier,
					'date_numerisation' => $p->date_numerisation,
					'dt_event' => $p->dt_event,
					'mode_paiement' => $p->mode_paiement,
					'etape' => $p->flgtt_etape,
					'etat' => $p->flgtt_etat_disp,
					'stat' => $p->etat,
					'id_lot_saisie' => $p->id_lot_saisie,
					'nb_mvt' => $p->nb_mvt,
					'lot_scan' => $p->lot_scan,
					'pli' => $p->pli,
					'id_pli' => '<span class="fa fa-envelope"></span> '.$p->id_pli,			
					'typologie' => $p->typologie			
                );

                array_push($data, $row);
            }
            $dtable["data"] = $data;

            echo json_encode($dtable);
		
	}
	
	  public function export_anomalie_csv(){

        $statut         = $this->input->post('statut');
        $date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
        $date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');

        $array_result = $this->mvisu->get_anomalie_pli( $statut, $date_debut, $date_fin, $date_debut_ttt, $date_fin_ttt);

        $array_export = array();
        $array_export['list_export'] = $array_result;

        $this->load->view('anomalie/export_anomalie_csv.php', $array_export);

    }

    public function export_anomalie_excel(){

        $select_statut 	= $this->input->post('select_statut');
        $select_mp 	    = $this->input->post('select_mp');
        $select_soc 	= $this->input->post('select_soc');
        $date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
		$date_fin_ttt   = $this->input->post('date_fin_ttt');
		$sel_stat_saisie   = $this->input->post('stat_saisie');
		$extract = 0;
				
		$where_date_courrier = $where_date_courrier_ttt = $where_other = $where_f_courrier = $where_f_ttt = $where_mode_paie ="";
		$where_statut = "";
		if($date_debut != '' && $date_fin != ''){
				$where_date_courrier 	.=  " and (f_lot_numerisation.date_courrier between '".$date_debut."' and '".$date_fin."' )";
				$where_f_courrier .= "'".$date_debut."' , '".$date_fin."'";
							
        }
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " and (f.dt_event::date between '".$date_debut_ttt."' and '".$date_fin_ttt."') ";
				$where_f_ttt .= "'".$date_debut_ttt."' , '".$date_fin_ttt."'";
		}
		if($select_soc != ''){		  
			$where_other .= "  and f_pli.societe = '".$select_soc."' ";
		}
		
		if($select_statut != ""){ 
          
		   list($id, $identifiant) = explode("_", $select_statut );
		 
		$where_other .= ' and f_pli.flag_traitement = '.$id.' ' ;
		   
		}
		
		if($sel_stat_saisie != ""){ 
			$where_other .= ' and statut_saisie.id_statut_saisie  = '.$sel_stat_saisie.' AND   flag_rewrite_status_client = 0' ;
		}

		if($select_mp != ""){ 
           //$where_other .= ' and mode_paiement.id_mode_paiement = '.$select_mp.' ' ;
		   $where_mode_paie = " and (id_mp ilike '".$select_mp.",%' or id_mp ilike '%,".$select_mp.",%' or id_mp ilike ',".$select_mp."%' or id_mp ='".$select_mp."')";
		}
		
		if($date_debut != '' && $date_fin != '' && $date_debut_ttt != '' && $date_fin_ttt != ''){
			$extract = 1; //recherche par date de courrier et date de traitement
		}else{
			if($date_debut != '' && $date_fin != '' && $date_debut_ttt == '' && $date_fin_ttt == ''){
				$extract = 2; //recherche par date de courrier
			}
			if($date_debut == '' && $date_fin == '' && $date_debut_ttt != '' && $date_fin_ttt != ''){
				$extract = 3; //recherche par date de traitement
			}
		}	
		//var_dump($sel_stat_saisie);die;
		
        $array_result = $this->mvisu->get_anomalie_pli($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$extract);

        $array_export = array();
        $array_export['list_export'] = $array_result;

        $this->load->view('anomalie/export_anomalie_excel.php', $array_export);

    }
	public function get_societe()
    {
			$list = $this->mvisu->get_societe();
			$array_view_plis['list_societe'] = $list;
			if($list) $this->load->view('statistics/societe_select.php', $array_view_plis);		
       
    }
	public function get_statut(){
			//$list1 = $this->mvisu->get_statut_saisie ();
			$list2 = $this->mvisu->get_statut_traitement ();			
			$array_view_plis['list_statut'] = $list2;
			if($list2) $this->load->view('anomalie/statut_select.php', $array_view_plis);	
		
	}
	public function get_mode_paiement()
    {
			$list = $this->mvisu->get_mode_paiement();
			$array_view_plis['list_mp'] = $list;
			if($list) $this->load->view('anomalie/modepaiement_select.php', $array_view_plis);		
       
	}
	//get statut saisie 
	public function get_stat_saisie()
	{
		$list = $this->mvisu->statut_saisie();
		$array_view_plis['list_stat_saisie'] = $list;
		//var_dump($array_view_plis);die;
		if($list) $this->load->view('anomalie/stat_saisie_select.php', $array_view_plis);
	}
	
}
?>
