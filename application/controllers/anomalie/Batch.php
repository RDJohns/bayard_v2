<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Batch extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->load->model('admin/model_support_push', 'push_model');
        
        $this->load->model('anomalie/model_batch', 'bth');
    }

    public function index()
    {
        $this->portier->must_observ();
        $this->retourBatch();
    }

    //formulaire de visualisatino retour batch
    public function retourBatch()
    {
        //echo('batch');die;
        $this->load->library('admin/page');
		$this->page->add_css('../../src/template/css/font-awesome.min');
        $this->page->add_js('admin/support');
        $this->page->set_titre('<i class="fa fa-bug"></i> Retour batch');
		$this->page->add_links('Visualisation des courriers', site_url('visual/visual'), 'fa fa-eye', 'cyan');
        $this->page->add_links('Visualisation des MAIL/SFTP', site_url('visualisation/visu_push'), 'fa fa-sign-in', 'deep-purple');
		$this->page->add_links('Réception', site_url('reception/pli_flux'), 'fa fa-envelope', 'light-green');
		$this->page->add_links('Suivi mensuel', site_url('statistics/statistics/stat_mensuel'), 'fa fa-signal', 'teal');
		$this->page->add_links('Suivi des soldes', site_url('statistics/statistics/suivi_solde'), 'fa fa-battery-half', 'pink-cstm');
		$this->page->add_links('Traitement', site_url('statistics/statistics_flux/stat_traitement'), 'fa fa-tasks', 'light-blue');
		$this->page->add_links('Extraction', site_url('anomalie/anomalie/visu_anomalie'), 'fa fa-file-text', 'cyan-t');
		$this->page->add_links('Suivi des espèces', site_url('suivi/especes'), 'fa fa-money', 'amber');
		$this->page->add_links('Suivi des provenances', site_url('provenance/pli'), 'fa fa-flag', 'green');
		$this->page->add_links('Suivi des ch&egrave;ques non REB', site_url('suivi/reliquat'), 'fa fa-list', 'lime');
        $this->page->add_links('Suivi des ch&egrave;ques non saisis', site_url('suivi/nosaisie'), 'fa fa-clipboard', 'blue-grey');
		$this->page->add_links('Suivi des DMT', site_url('dmt/dmt/visu_dmt'), 'fa fa-hourglass-1', 'grenat');
        $this->page->add_js('retour/batch');
        $this->page->add_css('admin/visu_push');
        $this->page->add_css('visual/main');
        $this->page->add_css('visual/batch');
       
        //get societe 
        $data_soc = $this->push_model->getSociete();
        $nom_payeur = $this->bth->getInfos('datn_end ',' nom_payeur');
        $num_payeur = $this->bth->getInfos('dnr_nbr ',' num_payeur');
        $num_dest = $this->bth->getInfos('ctm_nbr ',' num_dest');
        $nom_dest = $this->bth->getInfos('atn_end ',' nom_dest');
        $code_produit = $this->bth->getInfos('itm_num ',' code_produit');
        $code_promo = $this->bth->getInfos('pmo_cde ',' code_promo');
        $nom_fichier = $this->bth->getInfosFichier('nom ',' nom_fichier');
        //array $data
        $data = array(
            'societes' => $data_soc,
            'num_payeur' => $num_payeur,
            'nom_payeur' => $nom_payeur,
            'num_dest' => $num_dest,
            'nom_dest' => $nom_dest,
            'code_produit' => $code_produit,
            'code_promo' => $code_promo,
            'nom_fichier' => $nom_fichier
        );
        $this->page->afficher($this->load->view('anomalie/retourBatch',$data, true));
    }

    function getBatch()
    {
        $draw = intval($this->input->post_get("draw"));
        $length            = $this->input->post_get('length');
        $start             = $this->input->post_get('start');
        //critere de recherche
        //date_env_batch
        $deb_env_batch = $this->input->post_get('deb_env_batch');
        $fin_env_batch = $this->input->post_get('fin_env_batch');
        //date_courrier
        $deb_courrier = $this->input->post_get('deb_courrier');
        $fin_courrier = $this->input->post_get('fin_courrier');
        //autres criteres de recherche
        $sel_soc = $this->input->post_get('sel_soc');
        $id_pli = $this->input->post_get('id_pli');
        $num_payeur= $this->input->post_get('num_payeur');
        $nom_payeur= $this->input->post_get('nom_payeur');
        $num_dest = $this->input->post_get('num_dest');
        $nom_dest = $this->input->post_get('nom_dest');
        $code_titre = $this->input->post_get('code_titre');
        $code_promo = $this->input->post_get('code_promo');
        $nom_fichier = $this->input->post_get('nom_fichier');
        //initialisation var close where
        $clause_where = "";
        //s'il y a des critères de recherche définis 
        $clause_where .= $this->wherelike($id_pli, 'cba.id_pli', false);
        $clause_where .= $this->wherelike($sel_soc, 'cba.societe', false);
        $clause_where .= $this->wherelike($num_payeur, 'dnr_nbr', true);
        $clause_where .= $this->wherelike($nom_payeur, 'datn_end', true);
        $clause_where .= $this->wherelike($num_dest, 'ctm_nbr', true);
        $clause_where .= $this->wherelike($nom_dest, 'atn_end', true);
        $clause_where .= $this->wherelike($code_titre, 'itm_num', true);
        $clause_where .= $this->wherelike($code_promo, 'pmo_cde', true);
        $clause_where .= $this->wherelike($nom_fichier, 'nom', true);
        //s'il y a des criteres de date
        $clause_where .= $this->whereDate($deb_env_batch, $fin_env_batch, 'btch_dte');
        $clause_where .= $this->whereDate($deb_courrier, $fin_courrier, 'date_courrier');
        // //envoi des criteres de rechrch vers model cba
        //var_dump($clause_where);die;
        $data = $this->bth->getCba($length, $start, $clause_where);
        //formation array output
        $row = array();
        if (count($data)>0) {
            foreach($data as $r) {
                $row[] = array(
                    $r->id_pli,
                    '<span class="elem_activ" title="Afficher les documents du pli" onclick="load_img_docs(\''. $r->id_pli.'\');">#'.$r->pli.'</span>',
                    $r->date_courrier,
                    $r->societe, 
                    $r->num_payeur,
                    $r->nom_payeur,
                    //$r->motif,
                    $r->num_dest,
                    $r->nom_dest,
                    $r->code_produit,
                    $r->code_promo,
                    $r->date_env,
                    $r->code_er,
                    $r->nom_fichier,
                    
                );
            }
        }
        $total =  $this->bth->count($clause_where, $length, $start);
        $record = sizeof($total)>0?$total[0]->nb:0;
//var_dump($data);die;
        $output = array(
            "draw" => $draw,
            "recordsTotal" =>$record,
            "recordsFiltered" => $record,
            "data" => $row
        );
        echo json_encode($output);
    }

    //mettre en place les clause xhere
    function wherelike($value, $colonne, $unite)
    {
        //print_r($value);die;
        if(is_array($value)) {
            //convertir array string to array integer
            $value = implode(",", $value);
        }
        if($value != '') {
            $cl = " =".$value;
            if($unite)
            {
                $cl =" ilike '%".$value."%'";
            }
            $clause_where = "and ".$colonne." $cl  ";
            return $clause_where;
        }   
       
    }

    //where like date
    function whereDate($dt1, $dt2, $nom_col_date)
    {
        if($dt1 != '' and $dt2 != '') {
            if($dt1 == $dt2) {
                $where = "and $nom_col_date::date = '".$dt1."'";
            } else {
                $where = "and $nom_col_date::date between '".$dt1."' and '".$dt2."'";
            }
            return $where;
        }
    }
    //stat batch
    function statBatch()
    {
        //critere de recherche
        //date_env_batch
        $deb_env_batch = $this->input->post_get('deb_env_batch');
        $fin_env_batch = $this->input->post_get('fin_env_batch');
        //date_courrier
        $deb_courrier = $this->input->post_get('deb_courrier');
        $fin_courrier = $this->input->post_get('fin_courrier');
        //autres criteres de recherche
        $sel_soc = $this->input->post_get('sel_soc');
        $id_pli = $this->input->post_get('id_pli');
        $num_payeur= $this->input->post_get('num_payeur');
        $nom_payeur= $this->input->post_get('nom_payeur');
        $num_dest = $this->input->post_get('num_dest');
        $nom_dest = $this->input->post_get('nom_dest');
        $code_titre = $this->input->post_get('code_titre');
        $code_promo = $this->input->post_get('code_promo');
        $nom_fichier = $this->input->post_get('nom_fichier');
        //initialisation var close where
        $clause_where = "";
        //s'il y a des critères de recherche définis 
        $clause_where .= $this->wherelike($id_pli, 'commande_cba.id_pli', false);
        $clause_where .= $this->wherelike($sel_soc, 'data_pli.societe', false);
        $clause_where .= $this->wherelike($num_payeur, 'dnr_nbr', true);
        $clause_where .= $this->wherelike($nom_payeur, 'datn_end', true);
        $clause_where .= $this->wherelike($num_dest, 'ctm_nbr', true);
        $clause_where .= $this->wherelike($nom_dest, 'atn_end', true);
        $clause_where .= $this->wherelike($code_titre, 'itm_num', true);
        $clause_where .= $this->wherelike($code_promo, 'pmo_cde', true);
        $clause_where .= $this->wherelike($nom_fichier, 'nom', true);
         //s'il y a des criteres de date
        $clause_where .= $this->whereDate($deb_env_batch, $fin_env_batch, 'btch_dte');
        $clause_where .= $this->whereDate($deb_courrier, $fin_courrier, 'date_courrier');
        $data = $this->bth->getDetailBatch($clause_where);
        echo json_encode($data);

    }
}