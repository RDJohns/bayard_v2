<?php defined('BASEPATH') OR exit('No direct script access allowed');

class production extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->portier->must_observ();
		$this->load->model('production/Model_production', 'mvisu');
    }

    public function index(){

        $this->production_chq();
        $this->productivite();
      //  $this->stat_mensuel();

    }

    public function view_chq(){

        $this->load->view('production/header_chq.php');
        $this->load->view('production/header_production.php');
        $this->load->view('production/view_chq.php');
        $this->load->view('production/footer_production.php');
		$this->load->view('production/footer.php');
        

    }
	 public function view_typage(){

        $this->load->view('production/header.php');
        $this->load->view('production/header_production.php');
        $this->load->view('production/view_typage.php');
        $this->load->view('production/footer_production.php');
		$this->load->view('production/footer.php');
        

    }
	 public function view_prod(){

        /** Your Code here */
		$this->load->view('production/header.php');
       //$this->load->view('production/left_menu.php');
        $this->load->view('production/header_production.php');
        $this->load->view('production/view_prod.php');
		$this->load->view('production/footer.php');
		$this->load->view('production/footer_production.php');
        
       

    }
    public function production_chq(){

        $this->load->view('production/header_chq.php');
        $this->load->view('production/header_production.php');
        $this->load->view('production/view_chq.php');
        $this->load->view('production/footer_production.php');
		$this->load->view('production/footer.php');
        

    }
	 public function productivite(){

        /** Your Code here */
		$this->load->view('production/header.php');
        $this->load->view('production/header_production.php');
        $this->load->view('production/view_production.php');
		$this->load->view('production/footer.php');
		$this->load->view('production/footer_production.php');
        
       

    }
	/*public function stat_mensuel(){

        $this->load->view('production/header.php');
        $this->load->view('production/header_production.php');
        $this->load->view('production/stat_mensuel.php');
        $this->load->view('production/footer_production.php');
		$this->load->view('production/footer.php');
        

    }*/
	

	 public function get_chq_multiple()
    {

        $date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');
        $flag 		=  $this->input->post('flag');
        $array_view_plis = array();
        $tab_dates  = $tab_daty = $tab_min_daty_courrier = array();
        $tab_week   = $tab_month = array();
		$liste_result_traitement_mens = array();
        
		
		$tab_min_daty_courrier	     = $this->mvisu->get_min_date_courrier($date_debut, $date_fin);
		if(!isset($tab_min_daty_courrier[0]["date_courrier"])) $tab_min_daty_courrier[0]["date_courrier"] = $date_debut;
		$tab_dates 	   			     = $this->mvisu->get_dates($tab_min_daty_courrier[0]["date_courrier"], $date_fin);
		//$liste_result 	   		 = $this->mvisu->get_stat_traitement($date_debut, $date_fin);
        $liste_result_plis 			 = $this->mvisu->get_multi_cheque($date_debut, $date_fin);
        
		//$array_view_plis['liste_pli'] 	 = $liste_result;
		$array_view_plis['list']      		 = $liste_result_plis;
		
		
		foreach($tab_dates as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			//echo $daty." ".$tab_week[0]["week"] ;echo "</br>";
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			
			$tab_month 	= $this->mvisu->get_month_of_date($daty);
			if(!empty($tab_dates)) $tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		
		$array_view_plis['liste_date']    = $tab_daty;
		if(!empty($tab_dates)) $array_view_plis['liste_mois']    = $tab_mois;
		/****************************** Fin Plis traite ************************************************************/
		
		
		//$array_view_plis['total_plis']    	  		 = $pli_total;
		
        if($liste_result_plis){
            $this->load->view('production/production_chq.php', $array_view_plis);
        }
        else{
            echo 0;
        }
    }
	 public function visu_chq_tlmc(){

        $this->load->view('production/header_chq.php');
        $this->load->view('production/header_production.php');
        $this->load->view('production/visu_chq_tlmc.php');
        $this->load->view('production/footer_production.php');
		$this->load->view('production/footer.php');
        

    }
	 public function get_typage_chq()
    {

        $date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');
        $flag 		=  $this->input->post('flag');
        $array_view_plis = array();
        $tab_dates  = $tab_daty  = $tab_min_daty_courrier = $tab_data_print = array();
        $tab_week   = $tab_month = $data_dates = array();
		$liste_result_plis 			  = $liste_plis = $tab_titre = array();
		$liste_typologie = array();
		$tab_composition_pli = array();
		//$liste_typologie = array('BDC avec 1 CHQ','BDC avec plusieurs CHQ','BDC avec num CB','BDC avec un mandat facture','BDC avec PMT (CHQ ou CB) + courriers ou écritures sur BDC','Chèque seul (avec au dos N° cde internet ou aucune indication)','BDC sans PMT (ni CHQ ni CB) sans lignes de cdes >> à scanner  via QR Code (jeux)','Courriers (sans BDC)','BDC avec espèce','Une enveloppe avec parrainage de catalogue','Jeu payé');
		$liste_result_traitement_mens = array();
        
		
		$tab_data_print	      		  = $this->mvisu->get_data_print();
		$tab_min_daty_courrier	      = $this->mvisu->get_min_date_courrier($date_debut, $date_fin);
		//if(!isset($tab_min_daty_courrier[0]["date_courrier"])) $tab_min_daty_courrier[0]["date_courrier"] = $date_debut;
		$tab_dates 	   			      = $this->mvisu->get_dates($date_debut, $date_fin);
		
		foreach($tab_data_print as $keyprint => $tab_print){						
			$id = $tab_print['composition_pli'];
			
			array_push($liste_typologie, $tab_print['id'].'|'.$tab_print['libelle_categorie']);
			array_push($tab_titre, $tab_print['id'].'|'.$tab_print['libelle_wf']."|".$tab_print['type_saisie']);
			array_push($tab_composition_pli, $tab_print['composition_pli']);
			
			$tab1 = array();
			$tab1 = $this->mvisu->get_typage($tab_dates,$id);
			switch ($id) {
				case '1,17,18,31':
				$liste_plis_1_17_18 = $tab1;
				break;
				case '6,19,20,32':
				$liste_plis_6_19_20 = $tab1;
				break;
				case '2,26':
				$liste_plis_2 = $tab1;
				break;
				case '5':
				$liste_plis_5 = $tab1;
				break;
				case '25':
				$liste_plis_25 = $tab1;
				break;
				case '14,21,22': 
				$liste_plis_14 = $tab1;
				break;
				case '997':
				$liste_plis_997 = $tab1; // aucun type de pli défini
				break;
				case '998':
				$liste_plis_998 = $tab1; // aucun type de pli défini
				break;
				case '15,16':
				$liste_plis_15_16 = $tab1;
				break;
				case '12,27,29':
				$liste_plis_12_27_29 = $tab1;
				break;
				case '999':
				$liste_plis_999 = $tab1; // aucun type de pli défini
				break;
				case '3':
				$liste_plis_3 = $tab1;
				break;
				case '23,24,28,30':
				$liste_plis_28_30 = $tab1;
				break;
				case '8,9':
				$liste_plis_8_9 = $tab1;
				break;
				
			}
		}
		
		/*$liste_plis_1_17_18        = $this->mvisu->get_typage($tab_dates,'1,17,18,19');
		$liste_plis_6       		  = $this->mvisu->get_typage($tab_dates,'6');
		$liste_plis_2        		  = $this->mvisu->get_typage($tab_dates,'2');
		$liste_plis_5        		  = $this->mvisu->get_typage($tab_dates,'5');
		$liste_plis_25        		  = $this->mvisu->get_typage($tab_dates,'25');
		$liste_plis_14        		  = $this->mvisu->get_typage($tab_dates,'14');
		$liste_plis_15_16        	  = $this->mvisu->get_typage($tab_dates,'15,16');
		$liste_plis_12_27_29          = $this->mvisu->get_typage($tab_dates,'12,27,29');
		$liste_plis_3                 = $this->mvisu->get_typage($tab_dates,'3');
		$liste_plis_28_30             = $this->mvisu->get_typage($tab_dates,'28,30');
		$liste_plis_8_9               = $this->mvisu->get_typage($tab_dates,'8,9');
        
		*/
		$array_view_plis['list_1_17_18']        = $liste_plis_1_17_18;
		$array_view_plis['list_6_19_20']        = $liste_plis_6_19_20;
		$array_view_plis['list_2']              = $liste_plis_2;
		$array_view_plis['list_5']              = $liste_plis_5;
		$array_view_plis['list_25']             = $liste_plis_25;
		$array_view_plis['list_14']             = $liste_plis_14;
		$array_view_plis['list_997']            = $liste_plis_997;
		$array_view_plis['list_998']            = $liste_plis_998;
		$array_view_plis['list_15_16']          = $liste_plis_15_16;
		$array_view_plis['list_12_27_29']       = $liste_plis_12_27_29;
		$array_view_plis['list_999']            = $liste_plis_999;
		$array_view_plis['list_3']              = $liste_plis_3;
		$array_view_plis['list_28_30']          = $liste_plis_28_30;
		$array_view_plis['list_8_9']            = $liste_plis_8_9;
		$array_view_plis['liste_typologie']     = $liste_typologie;
		$array_view_plis['tab_data_print']      = $tab_titre;
		$array_view_plis['tab_composition_pli'] = $tab_composition_pli;
		
		
		foreach($tab_dates as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			array_push($data_dates, str_replace("-","",$daty));
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			
			$tab_month 	= $this->mvisu->get_month_of_date($daty);
			if(!empty($tab_dates)) $tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		
		$array_view_plis['liste_date']     = $tab_daty;
		$array_view_plis['tab_dates']      = $tab_dates;
		$array_view_plis['data_dates']     = $data_dates;
		$array_view_plis['liste_typologie']     = $liste_typologie;
		if(!empty($tab_dates)) $array_view_plis['liste_mois']    = $tab_mois;
		/****************************** Fin Plis traite ************************************************************/
		
		
		if($liste_typologie){
            $this->load->view('production/typage_chq.php', $array_view_plis);
        }
        else{
            echo 0;
        }
    }
	
	
	 public function get_production()
    {

        $num_cmd    = $this->input->post('numcommande');
        $num_client = $this->input->post('numclient');
        $nom_client = $this->input->post('nom_client');
        $date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');

        $arr_view_doc = array();
		$tab_dates    = $tab_daty = $tab_dates_traitt = array();
        $tab_week     = array();
        $tab_month    = $tab_mois = $tab_dates_ttt = array();
		$tab_dates_traitement = array();
        
		
		$tab_dates 	   			     = $this->mvisu->get_dates($date_debut, $date_fin);	
		$tab_dates_traitement        = $this->mvisu->get_dates_ttt($date_debut, $date_fin);
        $liste_result 	   			 = $this->mvisu->get_stat_plis( $date_debut, $date_fin);
        $liste_result_plis 			 = $this->mvisu->get_list_plis($date_debut, $date_fin);
        $liste_result_doc  			 = $this->mvisu->get_list_document($date_debut, $date_fin);
        $liste_result_plis_traite    = $this->mvisu->get_list_plis_traite($date_debut, $date_fin);
        $liste_result_plis_recu_par_date          = $this->mvisu->get_plis_recu_par_date($date_debut, $date_fin);
        $liste_result_plis_traitement_date        = $this->mvisu->get_traitement_par_date($date_debut, $date_fin);
        $liste_result_plis_reception_date         = $this->mvisu->get_reception_par_date($date_debut, $date_fin);
		
		$array_view_doc['liste_pli'] = $liste_result;
		$array_view_doc['list']      = $liste_result_plis;
		$array_view_doc['liste_doc'] = $liste_result_doc;
		$array_view_doc['liste_plis_recu_par_date'] = $liste_result_plis_recu_par_date;
		$array_view_doc['liste_result_plis_traitement_date'] = $liste_result_plis_traitement_date;
		$array_view_doc['liste_result_plis_reception_date']  = $liste_result_plis_reception_date;
		
		$dttt_min = $tab_dates_traitement[0]["date_min_ttt"] ;
		$dttt_max = $tab_dates_traitement[0]["date_max_ttt"] ;
		$tab_dates_traitt			 = $this->mvisu->get_dates($dttt_min, $dttt_max);	
		$array_type_pli     = array();
		$tab_list_code      = array();
		$tab_list_comment   = array();
		$tab_count_pli_type = array();
		$tab_count_pli      = array();
		$j = 0;
		$nbtotalpli = 0;
		$type_pli = '';
		
		$tab_reception_date = array();
		foreach($liste_result_plis_reception_date as $k_plis_reception_date){
			//print_r($k_plis_reception_date);
			$tab_reception_date[$k_plis_reception_date["date_courrier"]] = $k_plis_reception_date["nb_plis"];
		}
		
		$array_view_doc['liste_result_plis_reception']  = $tab_reception_date;
		
		foreach($array_view_doc['list'] as $klist ){  
		
			if (!in_array($klist["code_type_pli"], $tab_list_code)) {
				$tab_list_code[$j]  	  =  $klist["code_type_pli"];
				$tab_list_comment[$j] 	  =  $klist["comment"];
				$j++;
			}
			
			
			
			if(isset($tab_count_pli[$klist["code_type_pli"]])) $tab_count_pli[$klist["code_type_pli"]]  += 1;
			else $tab_count_pli[$klist["code_type_pli"]] = 1;
			
			
			if(empty($tab_count_pli_type[$klist["code_type_pli"]]["val"]))
				$tab_count_pli_type[$klist["code_type_pli"]]["val"] = 0;
			
			if (!in_array($klist["code_type_pli"], $tab_list_code)) {
				$tab_count_pli_type[$klist["code_type_pli"]]["val"] = 0;
			}else $tab_count_pli_type[$klist["code_type_pli"]]["val"] += 1;
		
			$nbtotalpli++;
		}
		
				
		$tab_pourcent_typtpli = array();
		$data_donut  = '';
		$color_donut = '';
		$tab_donut   = array();
		$tab_donut_doc   = array();
		$data_donut_doc  = '';	
		$color_donut_doc = '';				
		$tab_color_donut = array('rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)','rgb(174, 201, 91)', 'rgb(213, 140, 223)', 'rgb(140, 194, 230)', 'rgb(221, 101, 102)', 'rgb(242, 222, 222)','rgb(154, 169, 178)', 'rgb(255, 137, 42)', 'rgb(223, 240, 216)', 'rgb(252, 248, 227)', 'rgb(104, 188, 49)','rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)');
		foreach($tab_count_pli_type as $k_pli_type => $v_pli_type){
			$kcode     = $k_pli_type;
			$nbplitype = $v_pli_type["val"];
			$tab_pourcent_typtpli[$kcode] = number_format($nbplitype *100 / $nbtotalpli,2);
			//echo $kcode." ".($nbplitype *100)." / ".$nbtotalpli."</br>";
		}
		
		$j2 = 0;
		foreach($tab_count_pli as $codepli =>$nbpli){
			
			if ($data_donut == ''){
				$data_donut = "[{label:'".$codepli."',value:".$tab_pourcent_typtpli[$codepli]."}";
				$color_donut = "colors:['".$tab_color_donut[$j2]."'";
				
			}else {
				$data_donut .= ",{label:'".$codepli."',value:".$tab_pourcent_typtpli[$codepli]."}";
				$color_donut .= ",'".$tab_color_donut[$j2]."'";
			}
			$j2++;
		}
		
		if ($data_donut != ''){
			$data_donut .= "],";
			$data_donut .= $color_donut."]";
		}
		
		$tab_count_pli["total"] = $nbtotalpli;
		
	
		$tab_list_code["count"] = $j;
		$array_view_doc['code_type_pli'] = $tab_list_code;
		$array_view_doc['lib_type_pli']  = $tab_list_comment;
		$array_view_doc['nb_type_pli']   = $tab_count_pli;
		$array_view_doc['data_donut']    = $data_donut;
		
		
		/***************************** Document ****************************************************************/
		
		$tab_list_doc = $tab_list_docpli = $tab_count_docpli = array();
		$k = $nb_docpli = 0;
		$tab_list_document = array();
		
		
		$plis_avec_moyen_paiement = 0;		
		$tab_list_doc = array("CHQ","CB","Autre MPT","Non traités");
		foreach($array_view_doc['liste_doc'] as $kdoc ){  
			$tab_count_docpli["CHQ"]  	    = $kdoc["avec_paiement_chq"]  ;
			$tab_count_docpli["CB"]   	    = $kdoc["avec_paiement_cb"]  ;
			$tab_count_docpli["Autre MPT"]  = $kdoc["avec_paiement_autre"]  ;
			$tab_count_docpli["Non typé"]   = $kdoc["non_traite"]  ;
			$plis_non_traite 				= $kdoc["non_traite"]  ;
			$plis_avec_moyen_paiement	    = 	$kdoc["avec_paiement_chq"] + $kdoc["avec_paiement_cb"] + $kdoc["avec_paiement_autre"] ;	
			$autres_plis = ($nbtotalpli-$plis_avec_moyen_paiement-$plis_non_traite > 0) ? $nbtotalpli-$plis_avec_moyen_paiement-$plis_non_traite : 0;
			if ($autres_plis > 0) $tab_count_docpli["Autres plis"] = $autres_plis  ;
		}
		$j3 = 0;
		foreach($tab_count_docpli as $keydoc => $nbdoc){
			
			
			$tab_list_document[$keydoc]["code"]     = $keydoc;
			$tab_list_document[$keydoc]["nb"]       = $nbdoc;
			$tab_list_document[$keydoc]["pourcent"] = ($nbtotalpli > 0) ? number_format($nbdoc*100/$nbtotalpli,2) : 0;
			
			if ($data_donut_doc == ''){
				$data_donut_doc = "[{label:'".$keydoc."',value:".$tab_list_document[$keydoc]["pourcent"]."}";
				$color_donut_doc = "colors:['".$tab_color_donut[$j3]."'";
				
			}else {
				$data_donut_doc .= ",{label:'".$keydoc."',value:".$tab_list_document[$keydoc]["pourcent"]."}";
				$color_donut_doc .= ",'".$tab_color_donut[$j3]."'";
			}
			$j3++;
		}
		
		$nplis_sans_moyen_paiement = 0;
		if($nbtotalpli-$plis_avec_moyen_paiement-$plis_non_traite > 0){
			$nplis_sans_moyen_paiement = $nbtotalpli-$plis_avec_moyen_paiement;
			$pcent_sans_moyen_paiement =  number_format(($nplis_sans_moyen_paiement*100/$nbtotalpli),2);
			$data_donut_doc .= ",{label:'Autres plis',value:".$pcent_sans_moyen_paiement."}";
			$color_donut_doc .= ",'".$tab_color_donut[$j3]."'";
		}
		if ($data_donut_doc != ''){
			$data_donut_doc .= "],";
			$data_donut_doc .= $color_donut_doc."]";
		}
		
		$array_view_doc['data_document']    	  = $tab_list_document;
		$array_view_doc['data_donut_document']    = $data_donut_doc;
		//echo $data_donut_doc;
		/*echo "<pre>";
		echo $nb_docpli;
		print_r($tab_list_document);
		echo "</pre>";
		*/
		
		/****************************** Fin Document ************************************************************/
		/****************************** ****Plis traite *********************************************************/
		
		$tab_pli_traite = array();
		$tab_pli_ko     = array();
		$tab_list 		= array();		
		$data_donut_plis_traite = $color_donut_plis_traite = '';
		
		foreach($liste_result_plis_traite as $tab_list_plis_traite ){  
			$tab_pli_traite["BDC_CHQ"]  	= $tab_list_plis_traite["bdc_avec_chq"]  ;
			$tab_pli_traite["BDC_CB"]   	= $tab_list_plis_traite["bdc_avec_cb"]  ;
			$tab_pli_traite["BDC_ESP"]  	= $tab_list_plis_traite["bdc_avec_espece"]  ;
			$tab_pli_traite["MANDAT"]   	= $tab_list_plis_traite["bdc_avec_mandat_facture"]  ;
			$tab_pli_traite["BDP"]          = $tab_list_plis_traite["bdp"]  ;			
			$tab_pli_traite["BDP_CHQ"]          = $tab_list_plis_traite["bdp_chq"]  ;			
			$tab_pli_traite["Autre"]        = $tab_list_plis_traite["autre_plis"]  ;			
			$tab_pli_traite["CLOTURE"]  	= $tab_list_plis_traite["ok"]  ;
			
			$tab_pli_ko["ko_inconnu"]   	= $tab_list_plis_traite["ko_inconnu"]  ;
			$tab_pli_ko["ko_scan"]      	= $tab_list_plis_traite["ko_scan"]  ;
			$tab_pli_ko["ko_call"]      	= $tab_list_plis_traite["ko_call"]  ;
			$tab_pli_ko["ko_def"]       	= $tab_list_plis_traite["ko_def"]  ;			
			$tab_pli_ko["ko_circulaire"]    = $tab_list_plis_traite["ko_circulaire"]  ;			
			$tab_pli_ko["ko_en_attente"]    = $tab_list_plis_traite["ko_en_attente"]  ;			
			$tab_pli_ko["ko_reclammation"]    = $tab_list_plis_traite["ko_reclammation"]  ;			
			$tab_pli_ko["ko_litige"]    = $tab_list_plis_traite["ko_litige"]  ;			
			
			
			$tab_pli_ko["KO"]  	    	    = $tab_list_plis_traite["ko"]  ;
			//$tab_pli_traite["TRAITE"]   	= $tab_list_plis_traite["traite"]  ;
		}
		
		$j3 = 0;
		$tab_pli_traite_lib = array("BDC_CHQ"=>"Payé par CHQ","BDC_CB"=>"Payé par CB","BDC_ESP"=>"Payé par ESPECES","MANDAT"=>"Payé par MANDAT FACTURE","BDP"=>"Bon de participation","BDP_CHQ"=>"Bon de participation avec CHQ","Autre"=>"Autre type de plis","CLOTURE"=>"PLIS CLOTURES");
		$pourcent_cloture_bdc = 0;	
		$cloture_bdc = 0;	
		foreach($tab_pli_traite as $keypt => $nbpt){
			
			$tab_list[$keypt]["code"]     = $keypt;
			$tab_list[$keypt]["nb"]       = $nbpt;			
			$tab_list[$keypt]["pourcent"] = ($tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"] > 0) ? number_format($nbpt*100/($tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"]),2) : 0;
			$pourcent_cloture = 0;
			if ($keypt != 'CLOTURE')
			{   // 23.80% => 15 plis
				if ($data_donut_plis_traite == ''){
					$data_donut_plis_traite = "[{label:'".$tab_pli_traite_lib[$keypt]."',value:".$tab_list[$keypt]["pourcent"]."}";
					$color_donut_plis_traite = "colors:['".$tab_color_donut[$j3]."'";
					
				}else {
					$data_donut_plis_traite .= ",{label:'".$tab_pli_traite_lib[$keypt]."',value:".$tab_list[$keypt]["pourcent"]."}";
					$color_donut_plis_traite .= ",'".$tab_color_donut[$j3]."'";
				}
				$j3++;
				$pourcent_cloture_bdc += $tab_list[$keypt]["pourcent"];
			}else 
			{   $pourcent_cloture = $tab_list[$keypt]["pourcent"];
			    $cloture_bdc = $nbpt;
			}
			
		}
		/*
		if($pourcent_cloture - $pourcent_cloture_bdc > 0) {			
			$pourcent_autre_type = $pourcent_cloture - $pourcent_cloture_bdc;
			if($pourcent_autre_type > 0){
				$tab_list["autre"]["code"]     = "Autre";
				$tab_list["autre"]["nb"]       = $tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"]-$cloture_bdc;			
				$tab_list["autre"]["pourcent"] =  $pourcent_autre_type;
				$data_donut_plis_traite .= ",{label:'Autres type de plis',value:".$pourcent_autre_type."}";
				$color_donut_plis_traite .= ",'".$tab_color_donut[$j3]."'";
			}
		}*/
		
		
		if ($data_donut_plis_traite != ''){
			$data_donut_plis_traite .= "],";
			$data_donut_plis_traite .= $color_donut_plis_traite."]";
		}
		
		//--- les ko 
		$j4 			= 0;
		$tab_list_ko    = array();		
		$tab_pli_ko_lib = array("ko_scan"=>"KO SCAN","ko_inconnu"=>"KO INCONNU","ko_call"=>"KO CALL","ko_def"=>"KO DEFINITIF","ko_circulaire"=>"KO Circulaire","ko_en_attente"=>"KO En attente","ko_reclammation"=>"KO Réclammation","ko_litige"=>"KO Litige","ko"=>"PLIS KO");
		$data_donut_plis_ko = $color_donut_plis_ko = '';
		foreach($tab_pli_ko as $keypko => $nbpko){
						
			$tab_list_ko[$keypko]["code"]     = $keypko;
			$tab_list_ko[$keypko]["nb"]       = $nbpko;			
			$tab_list_ko[$keypko]["pourcent"] = ($tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"]> 0) ? number_format($nbpko*100/($tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"]),2) : 0;
			if ($keypko != 'KO')
			{
				if ($data_donut_plis_ko == ''){
					$data_donut_plis_ko = "[{label:'".$tab_pli_ko_lib[$keypko]."',value:".$tab_list_ko[$keypko]["pourcent"]."}";
					$color_donut_plis_ko = "colors:['".$tab_color_donut[$j4]."'";
					
				}else {
					$data_donut_plis_ko .= ",{label:'".$tab_pli_ko_lib[$keypko]."',value:".$tab_list_ko[$keypko]["pourcent"]."}";
					$color_donut_plis_ko .= ",'".$tab_color_donut[$j4]."'";
				}
				$j4++;
			}
		}
		if ($data_donut_plis_ko != ''){
			$data_donut_plis_ko .= "],";
			$data_donut_plis_ko .= $color_donut_plis_ko."]";
		}
		
		$total_plis_traites = $tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"];
		$array_view_doc['data_plis_cloture']    	 = $tab_list;
		$array_view_doc['data_plis_ko']    	 		 = $tab_list_ko;
		$array_view_doc['data_donut_plis_traite']    = $data_donut_plis_traite;		
		$array_view_doc['data_donut_plis_ko']    	 = $data_donut_plis_ko;
		$array_view_doc['total_plis_traites']    	 = $total_plis_traites;
		/****************************** Fin Plis traite ************************************************************/
		/****************************** Plis restant à traiter ************************************************************/
		
	
		$tab_list_restant    = array();	
			
		$tab_pli_restant_lib = array("en_cours"=>"EN COURS","nouveau"=>"NON TRAITE","RESTANT"=>"PLIS RESTANT");
		$data_donut_plis_restant = $color_donut_plis_restant = '';
		foreach($liste_result as $ktab_pli){
			$pli_total   =  $ktab_pli['nouveau']+$ktab_pli['encours']+$ktab_pli['traite']+$ktab_pli['anomalie'];
			$pli_restant =  $ktab_pli['nouveau']+$ktab_pli['encours'];
		}
			
			
		$pourcent_nouveau  =($pli_total > 0) ? number_format($ktab_pli['nouveau']*100/($pli_total),2) : 0;
		$pourcent_encours  =($pli_total > 0) ? number_format($ktab_pli['encours']*100/($pli_total),2) : 0;
			
		$keyprest = "nouveau";
		$tab_list_restant[$keyprest]["code"]     = "nouveau";
		$tab_list_restant[$keyprest]["nb"]       = $ktab_pli['nouveau'];			
		$tab_list_restant[$keyprest]["pourcent"] = $pourcent_nouveau;
		
		$keyprest = "en_cours";
		$tab_list_restant[$keyprest]["code"]     = "en_cours";
		$tab_list_restant[$keyprest]["nb"]       = $ktab_pli['encours'];			
		$tab_list_restant[$keyprest]["pourcent"] = $pourcent_encours;
			
		if ($data_donut_plis_restant == ''){
			$data_donut_plis_restant = "[{label:'Non traité',value:".$pourcent_nouveau."},{label:'En cours',value:".$pourcent_encours."}],";
			$color_donut_plis_restant = "colors:['".$tab_color_donut[9]."','".$tab_color_donut[10]."']";
			
		}			
		
		$data_donut_plis_restant .= $color_donut_plis_restant;
		
		
		$total_plis_restant = $tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"];
		$array_view_doc['data_plis_restant']    	  = $tab_list_restant;
		$array_view_doc['data_donut_plis_restant']    = $data_donut_plis_restant;
		$array_view_doc['total_plis_restant']    	  = $pli_restant;
		$array_view_doc['total_plis']    	  		  = $pli_total;
				
		$tab_dates_merges = array_merge ($tab_dates,$tab_dates_traitt);
				
		//foreach($tab_dates as $kdaty=>$vdaty ){  
		foreach($tab_dates_merges as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			
			$tab_month 	= $this->mvisu->get_month_of_date($daty);
			$tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		$array_view_doc['liste_date']    = $tab_daty;
		$array_view_doc['liste_mois']    = $tab_mois;
		
		/****************************** Fin Restant ************************************************************/
        if($pli_total > 0){
            $this->load->view('production/production.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
    }

	public function ajax_reb(){
		
		
		$arr = array();

		ini_set('memory_limit', '-1');
        $date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
		
		
		$where_date =  "";
		$where_statut = "";
		if($date_debut != '' && $date_fin != ''){
				$where_date 	.=  " and (date_courrier between '".$date_debut."' and '".$date_fin."' )";
				
        }
			

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}
				
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}

		$arr = $this->mvisu->get_datatables_reb($where_date,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_reb($where_date);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_reb($where_date,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		foreach($arr as $p){
				if($p->statut == 'Anomalie')
						$p->statut = $p->type_ko;
				$row = array(
                   
                    'date_courrier' => $p->date_courrier,
					'lot_scan' => $p->lot_scan,
					'pli' => $p->pli,
					'code_type_pli' => $p->code_type_pli,
					'lib_type' => $p->lib_type,
					'statut' => $p->statut,
					'montant' => $p->montant,
					'cmc7' => $p->cmc7,
					'date_encaissement' => $p->date_encaissement,
					'id_pli' => $p->id_pli,	
					'id_document' => $p->id_doc,				
                );

                array_push($data, $row);
            }

            $dtable["data"] = $data;

            echo json_encode($dtable);
		
	}
	
	
    public function export_chq_tlmc_excel(){

        $date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
       
        $array_result = $this->mvisu->get_reb(  $date_debut, $date_fin);

        $array_export = array();
        $array_export['list_export'] = $array_result;

        $this->load->view('production/export_chq_tlmc_excel.php', $array_export);

    }

			
	
	
}
?>
