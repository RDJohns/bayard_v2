<?php

class Pli extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }
        $this->load->model("deverouiller/Model_Deverouiller", "MDeverouiller");
    }

    public function index()
    {
        
        $this->session->set_userdata('infouser', " IP:". $this->input->ip_address()." LOGIN: ".(int)$this->session->userdata('id_utilisateur'));
        $this->viewPrincipale();
    }

    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Déverouillage pli";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "lock";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller'
        );


        $foot['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/bootstrap-select/js/bootstrap-select',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/custom/deverouiller'
        );


        $this->load->view("main/header",$head);
        $this->load->view("deverouiller/Unlock");
        $this->load->view("deverouiller/Modal_unclock");
        $this->load->view("main/footer",$foot);
    }

    public function getListePli()
    {
        $output             = array();

        $listePlie          = $this->MDeverouiller->getListePli();
        $recordsTotal       = $this->MDeverouiller-> recordsTotal();
        $recordsFiltered    = $this->MDeverouiller-> recordsFiltered();
        $output             = $this->fillDataTable($listePlie,$recordsTotal,$recordsFiltered);

        echo json_encode($output);
    }
    public function  fillDataTable($obj,$recordsTotal,$recordsFiltered)
    {
        $data  = array();

        foreach ($obj as $liste)
        {
            $row = array();
            $row[] = $liste->id_pli;
            $row[] = ucfirst($liste->traitement);
            $row[] = $liste->typage_par;
            $row[] = $liste->saisie_par;
            $row[] = $liste->controle_par;

            $str    = '<center><button type="button" class="btn btn-circle waves-effect waves-circle waves-float" onclick="lockPli('.(int)$liste->id_pli.','.(int)$liste->flag_traitement.')">
                      <i class="material-icons">lock</i></button></center>';
            $row[]  = $str;
            $data[] = $row;
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data"            => $data,
        );

        return $output;
    }
    public function deverouillerPli()
    {

        $idPli    = (int)$this->input->post("idPli");
        $action   = trim($this->input->post("action"));
        $arrayDev = array();
        $ret      = 0;

        switch ($action)
        {
            case 'dev-typage':
                $arrayDev = array("typage_par"=>null,"flag_traitement"=>0);

                break;

            case 'dev-saisie-ctrl':
                $arrayDev = array("saisie_par"=>null,"controle_par"=>null,"flag_traitement"=>2);
                break;

            case 'dev-saisie':
                $arrayDev = array("saisie_par"=>null,"flag_traitement"=>2);
                break;

            case 'dev-typage-saisie':
                $arrayDev = array("saisie_par"=>null,"typage_par"=>null,"flag_traitement"=>0);
                break;

            case 'dev-all':
                $arrayDev = array("saisie_par"=>null,"typage_par"=>null,"controle_par"=>null,"flag_traitement"=>0);
                break;

            case 'dev-ctrl':
                $arrayDev = array("controle_par"=>null,"flag_traitement"=>5);
                break;
        }

        $ret = $this->MDeverouiller->deverouillerPli($idPli,$arrayDev);

            echo json_encode(array("retour"=>(int)$ret));
            return "";
    }

}