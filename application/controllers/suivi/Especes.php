<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Especes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->load->model('admin/model_support_push', 'push_model');
        $this->load->model('suivi/model_especes', 'esp_model');
       
       
      
    }

    public function index()
    {
        $this->portier->must_observ();
        $this->especes();
        
    }

    //suivi especes
    function especes()
    {
        $this->load->library('admin/page');
		$this->page->add_css('../../src/template/css/font-awesome.min');
        $this->page->add_js('admin/support');
        $this->page->set_titre('<i class="fa fa-money"></i> Suivi des espèces');
    $this->page->add_links('Visualisation des courriers', site_url('visual/visual'), 'fa fa-eye', 'cyan');
    $this->page->add_links('Visualisation batch', site_url('anomalie/batch'), 'fa fa-bug', 'red');
        $this->page->add_links('Visualisation des MAIL/SFTP', site_url('visualisation/visu_push'), 'fa fa-sign-in', 'deep-purple');
		$this->page->add_links('Réception', site_url('reception/pli_flux'), 'fa fa-envelope', 'light-green');
		$this->page->add_links('Suivi mensuel', site_url('statistics/statistics/stat_mensuel'), 'fa fa-signal', 'teal');
		$this->page->add_links('Suivi des soldes', site_url('statistics/statistics/suivi_solde'), 'fa fa-battery-half', 'pink-cstm');
		$this->page->add_links('Traitement', site_url('statistics/statistics_flux/stat_traitement'), 'fa fa-tasks', 'light-blue');
		$this->page->add_links('Extraction', site_url('anomalie/anomalie/visu_anomalie'), 'fa fa-file-text', 'cyan-t');
		$this->page->add_links('Suivi des espèces', site_url('suivi/especes'), 'fa fa-money', 'amber');
		$this->page->add_links('Suivi des provenances', site_url('provenance/pli'), 'fa fa-flag', 'green');
		$this->page->add_links('Suivi des ch&egrave;ques non REB', site_url('suivi/reliquat'), 'fa fa-list', 'lime');
        $this->page->add_links('Suivi des ch&egrave;ques non saisis', site_url('suivi/nosaisie'), 'fa fa-clipboard', 'blue-grey');
		$this->page->add_links('Suivi des DMT', site_url('dmt/dmt/visu_dmt'), 'fa fa-hourglass-1', 'grenat');
        $this->page->add_js('suivi/especes');
        $this->page->add_css('suivi/especes');
       
        //get societe 
        $data_soc = $this->push_model->getSociete();
        $data_etape = $this->esp_model->flag_traitement();
        $data_etat = $this->esp_model->status();
        $data = array(
            'societes' => $data_soc,
            'etapes' => $data_etape,
            'etats' => $data_etat,
        );
       
        $this->page->afficher($this->load->view('suivi/especes',$data, true));
    
    }

    //especes
    function getEspeces()
    {
        //datatable
        $draw = intval($this->input->post_get("draw"));
        $length            = $this->input->post_get('length');
        $start             = $this->input->post_get('start');
        //data search
        $date_recep1 = $this->input->post_get('date_ttr1');
        $date_recep2 = $this->input->post_get('date_ttr2');
        $societe = $this->input->post_get('societe');
        $etape = $this->input->post_get('etape');
        $etat = $this->input->post_get('etat');
        $ids_pli = $this->esp_model->getIdsPli();
        //var_dump($ids_pli);die;
        $clause_where = "";
        $where_pli = "";
        $where_f_pli = "";
        $where_pli .= $this->where($ids_pli, 'id_pli');
        $where_f_pli .= $this->where_sans_and($ids_pli, 'f_pli.id_pli');
        $clause_where .= $this->whereDate($date_recep1, $date_recep2);
        $clause_where .= $this->where($societe, 'soc_id');
        $clause_where .= $this->where($etape, 'id_flag_traitement');
        $clause_where .= $this->where($etat, 'id_statut_saisie');
        //var_dump($clause_where);die;
        $data = $this->esp_model->getPaieEspeces($where_pli,$where_f_pli,$clause_where, $length, $start,$date_recep1,$date_recep2);
        //var_dump($data);die;
        $row = array();
        if (count($data)>0) {
            foreach($data as $r) {
                $row[] = array(
                  $r->date_courrier, 
                  $r->dt_event, 
                  $r->id_pli, 
                  $r->lot_scan, 
                  $r->commande, 
                  $r->societe,                 
                  $r->montant.' €'
                );
            }
        }
        $total = $this->esp_model->countResult($where_pli, $where_f_pli, $clause_where,$date_recep1,$date_recep2);
        $sum = $this->esp_model->sumEspeces($where_pli, $where_f_pli, $clause_where,$date_recep1,$date_recep2);
        //var_dump($total);die;
        $record = sizeof($total)>0?$total[0]->count:0;
        $totalMoney = sizeof($sum)>0?$sum[0]->sum:0;
        //var_dump($row,sizeof($row),$record);die;
        $newIndice = sizeof($row);
        $row[$newIndice] = array('', '', '','','','Total', $totalMoney." €");
        $output = array(
            "draw" => $draw,
            "recordsTotal" =>$record,
            "recordsFiltered" => $record,
            "data" => $row
          );
        echo json_encode($output);
       
    }

    //where like date
    function whereDate($dt1, $dt2)
    {
        if($dt1 != '' and $dt2 != '') {
            if($dt1 == $dt2) {
              $where = "and dt_event::date = '".$dt1."'";
            } else {
              $where = "and dt_event::date between '".$dt1."' and '".$dt2."'";
            }
            return $where;
        }
        
    }

    /*
      former les clauses where
    */
    function where($value, $colonne)
    {
        //print_r($value);die;
        if(is_array($value)) {
          //convertir array string to array integer
          $value = implode(",", $value);
        }
        if($value != '') {
          $clause_where = "and ".$colonne." in (".$value.") ";
          return $clause_where;
        }
        //var_dump($clause_where);die;
    }

    /*
      former les clauses where
    */
    function where_sans_and($value, $colonne)
    {
        //print_r($value);die;
        if(is_array($value)) {
          //convertir array string to array integer
          $value = implode(",", $value);
        }
        if($value != '') {
          $clause_where = " ".$colonne." in (".$value.") ";
          return $clause_where;
        }
        //var_dump($clause_where);die;
    }
    //exporter 
    public function exporter($societe=null,$date_recep1,$date_recep2)
    {
      // $date_recep1 = $this->input->post_get('date_ttr1');
      // $date_recep2 = $this->input->post_get('date_ttr2');
      // $societe = $this->input->post_get('societe');
      $ids_pli = $this->esp_model->getIdsPli();
        $clause_where = "";
        $where_pli = "";
        $where_f_pli = "";
        $where_pli .= $this->where($ids_pli, 'id_pli');
        $where_f_pli .= $this->where_sans_and($ids_pli, 'f_pli.id_pli');
        $clause_where .= $this->whereDate($date_recep1, $date_recep2);
        $clause_where .= $this->where($societe, 'soc_id');
        $data = $this->esp_model->getPaieEspeces($where_pli,$where_f_pli,$clause_where, $length = null, $start= null,$date_recep1,$date_recep2);
        $array_export = array();
        $array_export['list_export'] = $data['data'];
        $this->load->view('suivi/export.php', $array_export);
      var_dump($societe);die;
    }
}