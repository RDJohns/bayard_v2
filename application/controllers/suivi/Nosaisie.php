<?php
class Nosaisie extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->load->model("suivi/Model_reliquat", "Mreliquat");
    }

    public function index()
    {
        $this->portier->must_observ();
        $this->show_nosaisie();
    }

    public function show_nosaisie(){
        //$reliquat = $this->Mreliquat->get_list_reliquat();
        $head["title"]  = "Suivi des ch&egrave;ques non saisis";
        $head["menu"]   = "<i class='fa fa-clipboard'></i> Suivi des ch&egrave;ques non saisis";
        $head["icon"]   = "<i class='fa fa-clipboard'></i>";

        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/b_datepicker/css/datepicker',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/magnify/jquery.magnify',
            'plugins/metisMenu/font-awesome',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/sweetalert/sweetalert',
            'css/style',
            'css/themes/all-themes',
            'css/custom/typage',
            '../src/template/css/font-awesome.min',
            'css/suivi/nsaisi'
        );


        $foot['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/b_datepicker/js/bootstrap-datepicker',
            'plugins/b_datepicker/js/bootstrap-datepicker.fr',
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/magnify/jquery.magnify',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'js/pages/ui/notifications',
            'js/admin',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/suivi/nosaisie'
        );

        $head["theme"] = 'theme-blue';

        $this->load->view("main/header",$head);
        $this->load->view("suivi/nosaisie_view");
        $this->load->view("main/footer",$foot);
    }

    public function get_nosaisie(){
        $date_recep = date('Y-m-d', strtotime('-3 days'));
        $num_date = date('N',strtotime($date_recep));
        if($num_date == 7){ //vérifie si la date à J-3 est dimanche et change la date à J-4
            $date_recep = date('Y-m-d', strtotime('-4 days'));
        }

        $societe = $this->input->post('societe');

        $nsaisi_date_debut = $this->input->post('nsaisi_date_debut');
        $date_deb = str_replace('/', '-', $nsaisi_date_debut);
        $nsaisi_date_deb = date("Y-m-d", strtotime($date_deb));
        
        $nsaisi_date_fin = $this->input->post('nsaisi_date_fin');
        $date_fn = str_replace('/', '-', $nsaisi_date_fin);
        $nsaisi_date_fn = date("Y-m-d", strtotime($date_fn));


        $length = $this->input->post('length');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');

        $result = $this->Mreliquat->get_list_nosaisie($date_recep, $societe, $length, $start, $nsaisi_date_deb, $nsaisi_date_fn);
        $nb_result = $this->Mreliquat->get_nb_nosaisie($date_recep, $societe, $nsaisi_date_deb, $nsaisi_date_fn);

        $dtable = array();
        $data = array();

        if($result){
            $dtable["draw"] = $draw;
            $dtable["recordsTotal"] = count($result);
            $dtable["recordsFiltered"] = $nb_result[0]->nb_reliquat;
            foreach($result as $value){
                $row = array(
                    'date_courrier' => date('d/m/Y', strtotime($value->date_courrier)),
                    'id_pli'        => $value->id_pli,
                    'pli'           => $value->pli,
                    'lot_scan'      => $value->lot_scan,
                    'etape'         => $value->flgtt_etape,
                    'etat'          => $value->flgtt_etat,
                    'statut_pli'    => $value->libelle,
                    'cmc7'          => $value->cmc7,
                    'montant'       => $value->montant,
                    'etat_chq'      => $value->lbl_etat_chq,
                    'commande'      => $value->commande
                );

                array_push($data, $row);
            }

            $dtable["data"] = $data;

            echo json_encode($dtable);
        }
        else{
            $dtable["draw"] = $draw;
            $dtable["recordsTotal"] = 0;
            $dtable["recordsFiltered"] = 0;
            $dtable["data"] = '';

            echo json_encode($dtable);
        }
    }

    public function export_nsaisi_excel(){

        $array_export = array();
        $societe = $this->input->post('societe');

        $nsaisi_date_debut = $this->input->post('nsaisi_date_debut');
        $date_deb = str_replace('/', '-', $nsaisi_date_debut);
        $nsaisi_date_deb = date("Y-m-d", strtotime($date_deb));
        
        $nsaisi_date_fin = $this->input->post('nsaisi_date_fin');
        $date_fn = str_replace('/', '-', $nsaisi_date_fin);
        $nsaisi_date_fn = date("Y-m-d", strtotime($date_fn));

        $result = $this->Mreliquat->get_list_nosaisie_a_exporter($societe, $nsaisi_date_deb, $nsaisi_date_fn);

        $array_export['result']      = $result;

        $this->load->view('suivi/export_nsaisi_view.php', $array_export);

    }

}