<?php

class Pli extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        set_time_limit(7200);

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }
        $this->load->model('circulaire/Model_Circulaire','MCirculaire');

       
    }

    public function index()
    {
        $this->session->set_userdata('infouser', " IP:". $this->input->ip_address()." LOGIN: ".(int)$this->session->userdata('id_utilisateur'));
        if((int)$_SESSION['id_type_utilisateur'] == 3 )
        {
            $this->viewPrincipale();

        }
        else{
            redirect(base_url());
        }
        
    }
      public function getListeCirculaire()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $listCi = $this->MCirculaire->getListePlisCirculaire($date1,$date2,(int)$societe);
        $strTable = "";
        $nb_ligne = 0;        
        foreach($listCi as $item )
        {   
            $strTable .= "<tr>";
            $strTable .= "<td>".$item->date_courrier."</td>";
            $strTable .= "<td>".$item->date_enregistrement."</td>";
            $strTable .= "<td>".$item->commande."</td>";
            $strTable .= "<td>".$item->lot_scan."</td>";
            $strTable .= "<td>".$item->id_pli."</td>";
            $strTable .= "<td>".$item->pli."</td>";
            $strTable .= "<td>".$item->id_doc."</td>";
            $strTable .= "<td>".$item->code."</td>";
            $strTable .= "<td>".$item->circulaire."</td>";
            $img_url = base_url().'index.php/circulaire/pli/download_image_desarchive/'.$item->id_pli;
            $image_desarchive = '<a  class="name_file" href="'.$img_url.'" style="cursor:pointer;">'.$item->num_image.'</a>';
            $strTable .= "<td>".$image_desarchive."</td>";
            $strTable .= "<td>".$item->cmc7."</td>";
            $strTable .= "<td>".$item->nom_deleg."</td>";
            $strTable .= "<td>".$item->nom_client."</td>";
            $base = FCPATH.upFileCirc.$item->fichier_circulaire;
            $base = base_url().'index.php/circulaire/pli/download/'.$item->fichier_circulaire.'/'.$item->nom_orig_fichier_circulaire;
            $fichier = '<a  class="name_file" href="'.$base.'" style="cursor:pointer;">'.$item->nom_orig_fichier_circulaire.'</a>';
            $strTable .= "<td>".$fichier."</td>";
            $strTable .= "<td>";
            $strTable .= '<select class="form-control ci-etat" id="traitement" onchange="change_etat_ci(this)">';  
            $strTable .= '<option value="0"> Choix </option>';
            $strTable .= '<option value="1">VALIDER</option>';
            $strTable .= '<option value="2">REPORTER</option>';
            $strTable .= '<option value="3">RETIRER</option>';
            $strTable .= '</select>';
            $strTable .= "</td>";
            $strTable .= "</tr>";
            $nb_ligne ++;
        }
        //echo $strTable;
        $res = new stdClass;
        $res->total = $nb_ligne;
        $res->html = $strTable;
        echo json_encode($res);
        log_message('error', 'nombre de ligne inserée'.$nb_ligne);
    }

    public function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Circulaire";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "equalizer";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'plugins/morrisjs/morris',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            'css/custom/reception',
            'css/custom/circulaire',
            '../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/raphael/raphael.min',
            'plugins/morrisjs/morris'
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/custom/reception',
            'js/custom/circulaire'
        );
        $this->load->view("main/header",$head);
        $this->load->view("circulaire/Circulaire",$head);
       
        $this->load->view("main/footer",$foot);
    }

    //Download fichier circulaire
    function download($filename,$fileBD)
    {
        $file = FCPATH.upFileCirc.$filename;
				
        if(file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($fileBD));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
            
    
    }

    //Download image desarchive
    
    public function download_image_desarchive($id_pli){
		if (is_numeric($id_pli)) {
			$this->load->library('zip');
			$this->load->model('circulaire/Model_Circulaire');
			$pjs = $this->MCirculaire->ged_pjs($id_pli);
			foreach ($pjs as $pj) {
				$this->zip->add_data($pj->n_ima_recto, base64_decode($pj->n_ima_base64_recto));
				$this->zip->add_data($pj->n_ima_verso, base64_decode($pj->n_ima_base64_verso));
			}
			if(count($pjs) > 0){
				$this->zip->download('pjs_pli'.$id_pli.'.zip');
			}else{
				$this->load->helper('download');
				force_download('no_pj.txt', 'Aucun PJ pour le pli#'.$id_pli);
			}
		}else{
			log_message('error', 'admin>support>download_pjs: Pli introuvable! pli#'.$id_pli);
			force_download('error.txt', 'Pli introuvable: pli#'.$id_pli);
		}
    }
    
    public function exportCirculaire()      
    {
        $strTable = "";
        $valides      = stripcslashes($this->input->post("valides"));
        $listePli = json_decode($valides);
        //var_dump($listePli);
        $bordure = ' style="border:1px solid #000;" ';
        $bordure_text = ' style="border:1px solid;text-align: left;';

        $strTable   .= "<table><thead><tr>";
        $strTable   .= "<th".$bordure.">Date de Réception du courrier</th>";
        $strTable   .= "<th".$bordure.">Date de traitement CI</th>";
        $strTable   .= "<th".$bordure.">Commande</th>";
        $strTable   .= "<th".$bordure.">Ref. lot scan</th>";
        $strTable   .= "<th".$bordure.">ID Pli</th>";
        $strTable   .= "<th".$bordure.">Pli</th>";
        $strTable   .= "<th".$bordure.">ID Doc.</th>";   
        $strTable   .= "<th".$bordure.">Titre</th>";
        $strTable   .= "<th".$bordure.">Type Circulaire</th>";
        $strTable   .= "<th".$bordure.">N° images</th>";
        $strTable   .= "<th".$bordure.">CMC7</th>";
        $strTable   .= "<th".$bordure.">Délégué</th>";
        $strTable   .= "<th".$bordure.">Nom du client</th>";
        $strTable   .= "<th".$bordure.">Fichier</th>";
        $strTable   .=" </thead><tbody>";
        
        foreach($listePli as $item )
        {
             $strTable .= "<tr>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->date_courrier."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->date_enregistrement."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->commande."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >'".$item->lot_scan."'</td>";
                $strTable .= "<td style='border:1px solid ; text-align: left;' >".$item->id_pli."</td>";
                $strTable .= "<td style='border:1px solid ; text-align: left;' >'".$item->pli."'</td>";
                $strTable .= "<td style='border:1px solid ; text-align: left;' >".$item->id_doc."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>'".$item->code."'</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->circulaire."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->num_image."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>'".$item->cmc7."'</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->nom_deleg."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->nom_client."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->nom_orig_fichier_circulaire."</td>";

               

            $strTable .= "</tr>";
        }
        $strTable .= "</tbody></table>";
        echo $strTable;
        
    }

    public function updateCirculaire()      
    {
        $valides      = stripcslashes($this->input->post("valides"));
        $listePli = json_decode($valides);
        foreach($listePli as $item )
        {
            //var_dump($item->id_pli); 
            $this->MCirculaire->updateFlagCiAfterValidation($item->id_pli);
        }
        echo "ok";        
    }

    public function updateCI()
    {
        
    }
    
}

?>