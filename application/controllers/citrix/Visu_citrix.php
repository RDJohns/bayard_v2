<?php

class Visu_citrix extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }
        $this->load->model('citrix/Model_citrix','Mcitrix');

       
    }

    public function index()
    {
        $this->session->set_userdata('infouser', " IP:". $this->input->ip_address()." LOGIN: ".(int)$this->session->userdata('id_utilisateur'));
        if((int)$_SESSION['id_type_utilisateur'] == 3 )
        {
            $this->viewPrincipale();
            

        }
        else{
            redirect(base_url());
        }
        
    }

    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Suivi de saisie de virements citrix";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "equalizer";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'plugins/morrisjs/morris',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            'css/custom/citrix',
            '../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/raphael/raphael.min',
            'plugins/morrisjs/morris'
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/custom/citrix'
            
        );


        $this->load->view("main/header_visu",$head);
        $this->load->view("citrix/visu_citrix",$head);
       
        $this->load->view("main/footer",$foot);
    }

    public function getListeSuiviCitrix()
    {
        $obCountAll = $countFiltered = 0 ;
        $obList     = $this->Mcitrix->get_citrix();
        $obCountAll = $this->Mcitrix->count_all();
        $countFiltered = $this->Mcitrix->count_filtered();
        $output       = $this->fill_datatable($obList,$obCountAll,$countFiltered);

        echo json_encode($output);  
    }
    public function fill_datatable($obList,$obCountAll,$countFiltered)
    {

            $data   = array();
            $no     = $_POST['start'];
            $iLigne = 1;
            $id     = 0;
            foreach ($obList as $citrix)
            {
                $no++;
                $row           = array();

                $row[]         = $citrix->date_enregistrement;
                $row[]         = $citrix->societe;
                $row[]         = $citrix->numero_abonne;
                $row[]         = $citrix->nom_abonne;
                $row[]         = $citrix->montant;
                $row[]         = $citrix->commande;
                $row[]         = $citrix->statut;
                $row[]         = $citrix->code_postal_payeur;           
                $row[]         = $citrix->numero_payeur;
                $row[]         = $citrix->nom_payeur;
                $row[]         = $citrix->mode_paiement;
                $row[]         = $citrix->typologie;
                $data[]               = $row;
            }

            $output = array(
                "draw"            => $_POST['draw'],
                "recordsTotal"    => $obCountAll,
                "recordsFiltered" => $countFiltered,
                "data"            => $data,
            );

            return $output;
    }   

    public function exportListeCitrix()      
    {
        $strTable = "";
        
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $statut     = $this->input->post("statut");
        $paiement   = $this->input->post("paiement");
        $typologie  = $this->input->post("typologie");
        
        $listeCitrix = $this->Mcitrix->exportCitrix($date1,$date2,$societe,$statut,$paiement,$typologie);


        $bordure = ' style="border:1px solid #000;" ';
        $bordure_text = ' style="border:1px solid;text-align: left;';

        $strTable   .= "<table><thead><tr>";
        $strTable   .= "<th".$bordure.">Date enregistrement</th>";
        $strTable   .= "<th".$bordure.">Soci&eacute;t&eacute;</th>";
        $strTable   .= "<th".$bordure.">Num&eacute;ro abonn&eacute;</th>";
        $strTable   .= "<th".$bordure.">Nom abonn&eacute;</th>";
        $strTable   .= "<th".$bordure.">Montant</th>";
        $strTable   .= "<th".$bordure.">Commande</th>";
        $strTable   .= "<th".$bordure.">Statut</th>";
        $strTable   .= "<th".$bordure.">Code postal payeur</th>";   
        $strTable   .= "<th".$bordure.">Num&eacute;ro payeur</th>";
        $strTable   .= "<th".$bordure.">Nom payeur</th>";
        $strTable   .= "<th".$bordure.">Mode de paiement</th>";
        $strTable   .= "<th".$bordure.">Typologie</th>";
        $strTable   .=" </thead><tbody>";
        
        foreach($listeCitrix as $item )
        {
             $strTable .= "<tr>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->date_enregistrement."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->societe."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >'".$item->numero_abonne."'</td>";
                $strTable .= "<td style='border:1px solid ; text-align: left;' >".$item->nom_abonne."</td>";
                $strTable .= "<td style='border:1px solid ; text-align: left;' >'".$item->montant."'</td>";
                $strTable .= "<td style='border:1px solid ; text-align: left;' >".$item->commande."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>'".$item->statut."'</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->code_postal_payeur."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->numero_payeur."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>'".$item->nom_payeur."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->mode_paiement."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->typologie."</td>";
                
            $strTable .= "</tr>";
        }
         $strTable .= "</tbody></table>";
        echo $strTable;
        
    }
    
}