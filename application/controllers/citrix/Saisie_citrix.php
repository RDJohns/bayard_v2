<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Saisie_citrix extends CI_Controller{


    public function __construct()
    {
        parent::__construct();

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }
        $this->load->model('citrix/Model_citrix','Mcitrix');

       
    }

    public function index(){
      $this->session->set_userdata('infouser', " IP:". $this->input->ip_address()." LOGIN: ".(int)$this->session->userdata('id_utilisateur'));
        if((int)$_SESSION['id_type_utilisateur'] == 3 )
        {
            $this->view();
            

        }
        else{
            redirect(base_url());
        }
    }

    private function view(){
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Nouveau saisie virements citrix";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "equalizer";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'plugins/morrisjs/morris',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            'css/custom/citrix',
            '../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/raphael/raphael.min',
            'plugins/morrisjs/morris'
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/custom/citrix'
            
        );

        $this->load->view("main/header_citrix",$head);
        $this->load->view("citrix/list_citrix",$head);
       
        $this->load->view("main/footer",$foot);
    }


    public function listeVirementsCitrix()      
    {
        $listeCitrix = $this->Mcitrix->get_all_virements_citrix();
        $data = array();
        $no = $_POST['start'];
            foreach ($listeCitrix as $list) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" class="data-check" value="'.$list->id.'">';
            $row[] = $list->date_enregistrement;
            $row[] = $list->societe;
            $row[] = $list->numero_abonne;
            $row[] = $list->nom_abonne;
            $row[] = $list->montant;
            $row[] = $list->commande;
            $row[] = $list->statut;
            $row[] = $list->code_postal_payeur;
            $row[] = $list->numero_payeur;
            $row[] = $list->nom_payeur;
            $row[] = $list->mode_paiement;
            $row[] = $list->typologie;
            $row[] = '<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_virements_citrix('."'".$list->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
            $row[] = '<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_virements_citrix('."'".$list->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';          
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Mcitrix->count_all_list(),
                        "recordsFiltered" => $this->Mcitrix->count_filtered_list(),
                        "data" => $data,
                );
        echo json_encode($output);
               
    }


    public function ajax_edit($id)
    {
        $data = $this->Mcitrix->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        //$this->_validate();
        $data = array(
                'date_enregistrement' => $this->input->post('date_enregistrement'),
                'societe' => $this->input->post('societe'),
                'numero_abonne' => $this->input->post('numero_abonne'),
                'nom_abonne' => $this->input->post('nom_abonne'),
                'montant' => $this->input->post('montant'),
                'commande' => $this->input->post('commande'),
                'statut' => $this->input->post('statut'),
                'code_postal_payeur' =>$this->input->post('code_postal_payeur'),
                'numero_payeur' =>$this->input->post('numero_payeur'),
                'nom_payeur' =>$this->input->post('nom_payeur'),
                'mode_paiement' => $this->input->post('mode_paiement'),
                'typologie' => $this->input->post('typologie'), 
                
            );

        $insert = $this->Mcitrix->save($data);

        echo json_encode(array("status" => TRUE));
    }


    public function ajax_update()
    {
        //$this->_validate();
        $data = array(
                'date_enregistrement' => $this->input->post('date_enregistrement'),
                'societe' => $this->input->post('societe'),
                'numero_abonne' => $this->input->post('numero_abonne'),
                'nom_abonne' => $this->input->post('nom_abonne'),
                'montant' => $this->input->post('montant'),
                'commande' => $this->input->post('commande'),
                'statut' => $this->input->post('statut'),
                'code_postal_payeur' =>$this->input->post('code_postal_payeur'),
                'numero_payeur' =>$this->input->post('numero_payeur'),
                'nom_payeur' =>$this->input->post('nom_payeur'),
                'mode_paiement' => $this->input->post('mode_paiement'),
                'typologie' => $this->input->post('typologie'), 
            );
        $this->Mcitrix->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

   /* private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('societe') == '')
        {
            $data['inputerror'][] = 'societe';
            $data['error_string'][] = 'societe obligatoire';
            $data['status'] = FALSE;
        }

        if($this->input->post('numero_abonne') == '')
        {
            $data['inputerror'][] = 'numero_abonne';
            $data['error_string'][] = 'Numero abonné obligatoire';
            $data['status'] = FALSE;
        }

        if($this->input->post('nom_abonne') == '')
        {
            $data['inputerror'][] = 'nom_abonne';
            $data['error_string'][] = 'Nom abonné obligatoire';
            $data['status'] = FALSE;
        }

        if($this->input->post('montant') == '')
        {
            $data['inputerror'][] = 'montant';
            $data['error_string'][] = 'Montant obligatoire';
            $data['status'] = FALSE;
        }

        if($this->input->post('commande') == '')
        {
            $data['inputerror'][] = 'commande';
            $data['error_string'][] = 'commande obligatoire';
            $data['status'] = FALSE;
        }

        if($this->input->post('statut') == '')
        {
            $data['inputerror'][] = 'statut';
            $data['error_string'][] = 'statut obligatoire';
            $data['status'] = FALSE;
        }

        if($this->input->post('mode_paiement') == '')
        {
            $data['inputerror'][] = 'mode_paiement';
            $data['error_string'][] = 'Mode de paiement obligatoire';
            $data['status'] = FALSE;
        }

        if($this->input->post('typologie') == '')
        {
            $data['inputerror'][] = 'typologie';
            $data['error_string'][] = 'Typologie obligatoire';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }*/
}
