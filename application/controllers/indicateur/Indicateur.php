<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Indicateur extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('portier');
        //$this->portier->must_observ();
		$this->load->model('indicateur/Model_indicateur', 'mvisu');
    }

    public function index(){
       
        $this->visu_indicateur();
       // $this->stat_traitement();

    }

  
    public function visu_indicateur(){ 
		ini_set('memory_limit', '-1');
		$type_statut   = $this->mvisu->get_statut_traitement();
		$statut_saisie = $this->mvisu->get_statut_saisie();
		
        $array_view_statut = array();

       if($type_statut) $array_view_statut['type_statut'] = $type_statut;

        $data["menu_acif"]= "Indicateurs de production";
		$this->load->view('indicateur/header.php');
        $this->load->view('indicateur/header_indicateur.php',$data);
		if($type_statut){
            $this->load->view('indicateur/visu_indicateur.php', $array_view_statut);
        }
        else{
            echo 0;
        }
        $this->load->view('indicateur/footer.php');
		$this->load->view('indicateur/footer_indicateur.php');
    }
	
	
	 public function get_indicateur_pli()
    {
		ini_set('memory_limit', '-1');
        $statut         = $this->input->post('statut');
        $date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');

      
		 $this->load->view('indicateur/indicateur.php');
    }
	public function ajax_typage_global(){
		
			$arr_gbl = $arr_pli = $arr_user = array();			
			$select_soc 	= $this->input->post('select_soc');
			$select_op      = $this->input->post('select_op');
			$select_typo    = $this->input->post('select_typo');
			$date_debut_ttt = $this->input->post('date_debut_ttt');
			$date_fin_ttt   = $this->input->post('date_fin_ttt');
			$action_ttt   = $this->input->post('action_ttt');
			
			switch ($action_ttt) {
				case 'typage':
					$table_traitement = "view_indicateur_typage";
					break;
				case 'saisie':
					$table_traitement = "view_indicateur_saisie";
					break;
				case 'controle':
					$table_traitement = "view_indicateur_controle";
					break;
			}

			$where_date_courrier_ttt = $where_date_ttt = $where_other = $where_data = $where_date =  $where_traitement = "";
			$join = $join_global = "";	$where_statut = "";
			
			if($date_debut_ttt != '' && $date_fin_ttt != ''){
					$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
					$where_date_ttt .= " AND (f_indicateur.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
			}
			if($select_soc != ''){		  
					$where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
					$where_date_ttt .= "  AND societe.id = '".$select_soc."' ";
			}
			if($select_typo != ''){
					$where_other .=  " AND f_indicateur.id_typologie in ( ".$select_typo." )";
			}
			
			if($select_op != ''){
					$where_traitement .=  " AND ".$table_traitement.".id_utilisateur in ( ".$select_op." )";
					$where_other .= " AND trace.id_user in (".$select_op.")";
					$join = " inner join trace on trace.id_pli = $table_traitement.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
					$join_global = " inner join trace on trace.id_pli = $table_traitement.id_pli ";
					$where_data = " WHERE ".$table_traitement.".id_utilisateur in (".$select_op.")";
					
			}			
			if($date_debut_ttt != '' && $date_fin_ttt != '' && $select_soc != ''){
				$where_date .=  "'".$date_debut_ttt."' , '".$date_fin_ttt."', ".$select_soc."";			
			}
		
		/*$arr_gbl = $this->mvisu->get_indicateur_global($where_date_courrier_ttt,$where_other,$where_traitement,$where_data,$join,$table_traitement);*/
		$arr_plis  = array();
		$ids_pli   = '';
		
		$arr_plis = $this->mvisu->get_pli_cloture($where_date);

		if(count($arr_plis) == 0) $ids_pli = '0';
		else
		foreach($arr_plis as $kpli => $v_pli){
			$ids_pli .= ($ids_pli == '') ? $v_pli["id_pli"] : ','.$v_pli["id_pli"];
		}
		
		$arr_gbl = $this->mvisu->get_indicateur_globals($ids_pli, $where_date_ttt,$where_other,$where_data,$where_date, $join_global,$table_traitement);
		
		foreach($arr_gbl as $arr_k=>$tab_pli){
						
			$id_pli  = $tab_pli["id_pli"];
			$id_user = $tab_pli["id_user"];
			//$nb = $tab_pli["nb"];
			//$indicateur = $tab_pli["indicateur"];
			
						
			if (empty($arr_pli[$id_pli]["duree"]))
			{
				$arr_pli[$id_pli]["id_pli"] = $tab_pli["id_pli"];
				$arr_pli[$id_pli]["duree"] = $tab_pli["duree"];
				
			}
			else
			$arr_pli[$id_pli]["duree"] += $tab_pli["duree"];
		
			/****/
			
			if (empty($arr_user[$id_user]["duree"])){
				$arr_user[$id_user]["id_user"] = $tab_pli["id_user"];
				$arr_user[$id_user]["login"]  = $tab_pli["login"];
				$arr_user[$id_user]["duree"]  = $tab_pli["duree"];
				$arr_user[$id_user]["nb"]     = 1;
			}
			else{
				$arr_user[$id_user]["duree"] += $tab_pli["duree"];
				$arr_user[$id_user]["nb"]    += 1;
			}
		
			/*$arr_pli[$id_pli]   = (empty($arr_pli[$id_pli]) ? $arr_pli[$id_pli]+$tab_pli["duree"] : $tab_pli["duree"] );
			$arr_user[$id_user] = (empty($arr_user[$id_user]) ? $arr_user[$id_user]+$tab_pli["duree"] : $tab_pli["duree"] );*/
		}
		
		
		$array_view_plis['action_ttt']  = $action_ttt;
		$array_view_plis['arr_pli']  = $arr_pli;
		$array_view_plis['arr_user'] = $arr_user;
		
       if($array_view_plis) $this->load->view('indicateur/ajax_pli_typage.php', $array_view_plis);
		
	}
	
	public function ajax_global_global(){
		
			$arr_gbl = $arr_pli = $arr_user = $arr_data = array();			
			$select_soc 	= $this->input->post('select_soc');
			$select_op      = $this->input->post('select_op');
			$select_typo    = $this->input->post('select_typo');
			$date_debut_ttt = $this->input->post('date_debut_ttt');
			$date_fin_ttt   = $this->input->post('date_fin_ttt');
			$export   		= $this->input->post('action_ttt');
				

			$where_other = $where_data = $where_typage = $where_saisie = $where_controle = $where_date_indicateur = $where_data_pli ="";
			$join = "";	$where_statut = "";
			
			
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_data_pli  .=  " AND (f.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
				
		}
		
		if($select_soc != ''){		  
				$where_data_pli .=  "  AND f_pli.societe = '".$select_soc."' ";
		}		
		
		if($select_typo != ''){
				$where_data_pli .=  " AND f_pli.typologie in ( ".$select_typo." )";
        }
		
		if($select_op != ''){
				$where_typage   .=  " AND view_indicateur_typage.id_utilisateur in ( ".$select_op." )";
				$where_saisie   .=  " AND view_indicateur_saisie.id_utilisateur in ( ".$select_op." )";
				$where_controle .=  " AND view_indicateur_controle.id_utilisateur in ( ".$select_op." )";
				$where_data     .=  " AND trace.id_user in (".$select_op.")";
				
				
				$join       = " INNER join trace on trace.id_pli  = f_pli.id_pli ";
				
        }				
		
		if($date_debut_ttt != '' && $date_fin_ttt != '' && $select_soc != ''){
				$where_date_indicateur =  "'".$date_debut_ttt."' , '".$date_fin_ttt."', ".$select_soc."";			
		}	
		
		
		$arr_plis  = array();
		$ids_pli  = '';
		
		$arr_plis = $this->mvisu->get_pli_cloture($where_date_indicateur);

		if(count($arr_plis) == 0) $ids_pli = '0';
		else
		foreach($arr_plis as $kpli => $v_pli){
			$ids_pli .= ($ids_pli == '') ? $v_pli["id_pli"] : ','.$v_pli["id_pli"];
		}
		
		$arr_gbl = $this->mvisu->get_indicateur_global_global($ids_pli,$where_typage,$where_saisie,$where_controle,$where_data_pli,$where_date_indicateur);
		
		foreach($arr_gbl as $arr_k=>$tab_pli){
						
			$id_pli  = $tab_pli["id_pli"];
			$id_user = $tab_pli["id_user"];
			
						
			if (empty($arr_pli[$id_pli]["duree"]))
			{
				$arr_pli[$id_pli]["id_pli"] = $tab_pli["id_pli"];
				$arr_pli[$id_pli]["duree"] = $tab_pli["duree"];
				
			}
			else
			$arr_pli[$id_pli]["duree"] += $tab_pli["duree"];
		
			/****/
			
			if (empty($arr_user[$id_user]["duree"])){
				$arr_user[$id_user]["id_user"] = $tab_pli["id_user"];
				$arr_user[$id_user]["login"] = $tab_pli["login"];
				$arr_user[$id_user]["duree"] = $tab_pli["duree"];
				
			}
			else
			$arr_user[$id_user]["duree"] += $tab_pli["duree"];
		
			if (empty($arr_data[$id_pli][$id_user]["duree"])){
				$arr_data[$id_pli][$id_user]["id_pli"]  = $tab_pli["id_pli"];
				$arr_data[$id_pli][$id_user]["id_user"] = $tab_pli["id_user"];
				$arr_data[$id_pli][$id_user]["login"]   = $tab_pli["login"];
				$arr_data[$id_pli][$id_user]["duree"]   = $tab_pli["duree"];
				
			}
			else
			$arr_data[$id_pli][$id_user]["duree"] += $tab_pli["duree"];
		}
		
			
		$array_view_plis['arr_pli']  = $arr_pli;
		$array_view_plis['arr_user'] = $arr_user;
		$array_view_plis['arr_data'] = $arr_data;
	
	    if ($export == '1') $this->load->view('indicateur/export_indicateur_global_global.php', $array_view_plis);
		else
        if($array_view_plis) $this->load->view('indicateur/ajax_pli_global.php', $array_view_plis);
		
	}
	
	public function ajax_indicateur_global(){
		
		$arr = array();

		ini_set('memory_limit', '-1');
        $select_soc 	= $this->input->post('select_soc');
		$date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
        $granularite    = $this->input->post('granularite');


				
		$clauseWhere  = $where_date  =  "";
		$join = "";
		$where_statut = "";
		
		if($date_debut != '' && $date_fin != ''){
				$clauseWhere .= " AND (lot_numerisation.date_courrier::date between '".$date_debut."' AND '".$date_fin."') ";
					
		}
		if($select_soc != ''){		  
				$clauseWhere .= "  AND pli.societe = '".$select_soc."' ";
		}
		
		if($date_debut != '' && $date_fin != '' ){
				$where_date .=  "'".$date_debut."' , '".$date_fin."'";			
		}	

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}
		
		$arr = $this->mvisu->get_datatables_indicateur($clauseWhere,$granularite,$where_date,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_indicateur($clauseWhere,$granularite,$where_date);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_indicateur($clauseWhere,$granularite,$where_date,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		$i1 = 0;
		
		
		foreach($arr as $p){
				//$detail = '<i class="fa fa-plus" data-target="#detail'.$i1.'" aria-expanded="false" aria-controls="detail'.$i1.'" onclick="toggle_table('.$i1.',\'typage\')"></i>
					//<div id="date_typage_'.$i1.'" ><i class="fa fa-file"></i></div>';
				$v_daty = $p->date_deb.'|'.$p->date_fin;
				
				$detail = '<center><button class="btn btn-primary" title="" data-toggle="tooltip" data-placement="bottom" onclick="charger_detail(\''.$v_daty.'\')" data-original-title="Voir détail" style="padding:6px 6px;"><i class="fa fa-eye" data-target="#detail'.$i1.'" aria-expanded="false" aria-controls="detail'.$i1.'" style="font-size:14px;top:-1px;"></i></button></center>';
				//$p->pli_typage_fini = preg_replace('/\./m', ',', $p->pli_typage_fini);
				$row = array(
					'date_reception' => $p->date_courrier,
					'recu' => $p->pli_recu,			
					'typage' => $p->pli_typage_fini,			
					'saisie' => $p->pli_saisie_finie,					
                    'controle' => $p->pli_controle_fini,
					'termine' => $p->pli_termine,								
					'detail' => $detail								
                );

                array_push($data, $row);
				$i1++;
            }
            $dtable["data"] = $data;

            echo json_encode($dtable);
		
	}
	
	public function ajax_indicateur_detail(){
		
		$arr = array();
		$tab_date_fin = array();

		ini_set('memory_limit', '-1');
        /*$select_soc 	= $this->input->post('select_soc');
        $date_debut     = $this->input->post('date_debut');*/
        $date_ffin       = $this->input->post('date_fin');
		$select_soc 	= $this->input->post('select_soc');
        $date_recep     = $this->input->post('date_recep');
		$granularite    = $this->input->post('granularite');

				
		$clauseWhere = $where_date = "";
		$join = "";
		$where_statut = "";
		
		list($date_deb,$date_fin) = explode("|",$date_recep);
		if($granularite == 'j')
		{
			if($date_recep != ''){
					$clauseWhere .= " AND (lot_numerisation.date_courrier::date = '".$date_deb."') ";
			}
			if($select_soc != ''){		  
					$clauseWhere .= "  AND pli.societe = '".$select_soc."' ";
			}
			if($date_recep != '' ){
							$where_date .=  "'".$date_deb."' , '".$date_fin."'";			
					}			
		}
		else{
			
			if($granularite == 'm') {
				
				$tab_date_fin = $this->mvisu->get_date_fin($date_ffin, $date_fin );
				$date_fin = $tab_date_fin[0]['date_fin_fin'];
			}
			
			if($date_recep != ''){
					
					$clauseWhere .= " AND (lot_numerisation.date_courrier::date between '".$date_deb."' and '".$date_fin."' ) ";
			}
			if($select_soc != ''){		  
					$clauseWhere .= "  AND pli.societe = '".$select_soc."' ";
			}
			if($date_recep != '' ){
							$where_date .=  "'".$date_deb."' , '".$date_fin."'";			
					}			
		}
		

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}
		
		
		$arr = $this->mvisu->get_datatables_detail_indicateur($clauseWhere,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_detail_indicateur($clauseWhere);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_detail_indicateur($clauseWhere,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		
		$i1 = 0;													
		foreach($arr as $p){
				$p->typage_par = (empty($p->typage_par) || $p->typage_par =='') ? 'Auto' :$p->typage_par ;			
				$row = array(
					'id_pli'        => '<i class="fa fa-envelope"></i> '.$p->id_pli,			
					'typologie'     => $p->typologie,					
                    'lot_scan'      => $p->lot_scan,
					'date_typage'   => $p->date_typage,
					'typage_par'    => $p->typage_par,
					'date_saisie'   => $p->date_saisie,							
					'saisie_par'    => $p->saisie_par,							
					'date_controle' => $p->date_controle,							
					'controle_par'  => $p->controle_par,							
					'retraite_par'  => $p->retraite_par,							
					'date_termine'  => $p->date_termine,
					'date_courrier' => $p->date_courrier				
										
                );

                array_push($data, $row);
				$i1++;
            }
            $dtable["data"] = $data;

            echo json_encode($dtable);
	}
	
	public function ajax_indicateur_pli_op(){
		
		$arr = array();

		ini_set('memory_limit', '-1');
        $select_soc 	= $this->input->post('select_soc');
		$date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
        $granularite    = $this->input->post('granularite');


				
		$clauseWhere  = $where_date  =  "";
		$join = "";
		$where_statut = "";
		
		if($date_debut != '' && $date_fin != ''){
				$clauseWhere .= " AND (lot_numerisation.date_courrier::date between '".$date_debut."' AND '".$date_fin."') ";
					
		}
		if($select_soc != ''){		  
				$clauseWhere .= "  AND pli.societe = '".$select_soc."' ";
		}
		
		if($date_debut != '' && $date_fin != '' ){
				$where_date .=  "'".$date_debut."' , '".$date_fin."'";			
		}	

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}
		
		
		$arr = $this->mvisu->get_datatables_indicateur_op($clauseWhere,$granularite,$where_date,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_indicateur_op($clauseWhere,$granularite,$where_date);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_indicateur_op($clauseWhere,$granularite,$where_date,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		$i1 = 0;
		
		
		foreach($arr as $p){
				//$detail = '<i class="fa fa-plus" data-target="#detail'.$i1.'" aria-expanded="false" aria-controls="detail'.$i1.'" onclick="toggle_table('.$i1.',\'typage\')"></i>
					//<div id="date_typage_'.$i1.'" ><i class="fa fa-file"></i></div>';
				$v_daty = $p->date_deb.'|'.$p->date_fin;
				
				$detail = '<center><button class="btn btn-primary" title="" data-toggle="tooltip" data-placement="bottom" onclick="charger_detail_op(\''.$v_daty.'\')" data-original-title="Voir détail" style="padding:6px 6px;"><i class="fa fa-eye" data-target="#detail'.$i1.'" aria-expanded="false" aria-controls="detail'.$i1.'" style="font-size:14px;top:-1px;"></i></button></center>';
				//$p->pli_typage_fini = preg_replace('/\./m', ',', $p->pli_typage_fini);
				$row = array(
					'date_courrier' => $p->date_courrier,
					'recu' => $p->pli_recu,			
					'login' => $p->login,			
					'typage' => $p->pli_typage_fini,			
					'saisie' => $p->pli_saisie_finie,					
                    'controle' => $p->pli_controle_fini,
					'termine' => $p->pli_termine							
                );
				//,	'detail' => $detail	
                array_push($data, $row);
				$i1++;
            }
            $dtable["data"] = $data;

            echo json_encode($dtable);
		
	}
	
	public function export_indicateur_prod(){

        $select_soc 	= $this->input->post('select_soc');
		$date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
		$granularite    = $this->input->post('granularite');
				
		$clauseWhere = $where_date = "";
		
		if($date_debut != '' && $date_fin != ''){
				$clauseWhere .= " AND (lot_numerisation.date_courrier::date between '".$date_debut."' AND '".$date_fin."') ";
					
		}
		if($select_soc != ''){		  
				$clauseWhere .= "  AND pli.societe = '".$select_soc."' ";
		}
		if($date_debut != '' && $date_fin != '' ){
				$where_date .=  "'".$date_debut."' , '".$date_fin."'";			
		}
		
        $array_result = $this->mvisu->get_indicateur_prod($clauseWhere,$granularite,$where_date);
		$array_export = array();
        $array_export['list_export'] = $array_result;
        $this->load->view('indicateur/export_indicateur_excel.php', $array_export);

    }
	
	public function export_indicateur_detail(){

        $select_soc 	= $this->input->post('select_soc');
		$date_debut     = $this->input->post('date_debut');
		$date_ffin     = $this->input->post('date_fin');
        $granularite   = $this->input->post('granularite');
        $date_exp    = $this->input->post('date_recep');
				
		$clauseWhere = "";
		$where_date = "";
		
		//list($date_deb,$date_fin) = explode("|",$date_debut);
		
		list($date_deb,$date_fin) = explode("|",$date_exp);
		if($granularite == 'j')
		{
			if($date_deb != ''){
				$clauseWhere .= " AND (lot_numerisation.date_courrier::date = '".$date_deb."') ";
					
			}
			if($select_soc != ''){		  
					$clauseWhere .= "  AND pli.societe = '".$select_soc."' ";
			}
			if($date_deb != '' ){
							$where_date .=  "'".$date_deb."' , '".$date_deb."'";			
					}
		}
		else{
			if($granularite == 'm') {
				
				$tab_date_fin = $this->mvisu->get_date_fin($date_ffin, $date_fin );
				$date_fin = $tab_date_fin[0]['date_fin_fin'];
			}
			
			if($date_deb != ''){
					
					$clauseWhere .= " AND (lot_numerisation.date_courrier::date between '".$date_deb."' and '".$date_fin."' ) ";
			}
			if($select_soc != ''){		  
					$clauseWhere .= "  AND pli.societe = '".$select_soc."' ";
			}
			if($date_deb != '' ){
							$where_date .=  "'".$date_deb."' , '".$date_fin."'";			
					}			
		}
        $array_result = $this->mvisu->get_indicateur_detail($clauseWhere);
		
        $array_export = array();
		$array_export['list_export'] = $array_result;

        $this->load->view('indicateur/export_indicateur_detail.php', $array_export);

    }
	
	public function export_indicateur_op(){

        $select_soc 	= $this->input->post('select_soc');
		$date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
		$granularite    = $this->input->post('granularite');
				
		$clauseWhere = $where_date = "";
		
		if($date_debut != '' && $date_fin != ''){
				$clauseWhere .= " AND (lot_numerisation.date_courrier::date between '".$date_debut."' AND '".$date_fin."') ";
					
		}
		if($select_soc != ''){		  
				$clauseWhere .= "  AND pli.societe = '".$select_soc."' ";
		}
		if($date_debut != '' && $date_fin != '' ){
				$where_date .=  "'".$date_debut."' , '".$date_fin."'";			
		}
		
        $array_result = $this->mvisu->get_indicateur_prod_op($clauseWhere,$granularite,$where_date);
		$array_export = array();
        $array_export['list_export'] = $array_result;
        $this->load->view('indicateur/export_indicateur_op.php', $array_export);

    }
	public function get_societe()
    {
			$list = $this->mvisu->get_societe();
			$array_view_plis['list_societe'] = $list;
			if($list) $this->load->view('statistics/societe_select.php', $array_view_plis);		
       
    }
	public function get_operateur()
    {
			$list = $this->mvisu->get_operateur();
			$array_view_plis['list_operateur'] = $list;
			if($list) $this->load->view('statistics/operateur_select.php', $array_view_plis);		
       
    }
	public function get_typologie()
    {
			$list = $this->mvisu->get_typologie();
			$array_view_plis['list_typologie'] = $list;
			if($list) $this->load->view('statistics/typologie_select.php', $array_view_plis);		
       
    }
	public function get_statut(){
			$list1 = $this->mvisu->get_statut_saisie ();
			$list2 = $this->mvisu->get_statut_traitement ();
			$list = array();
			
			$i = 0;
			foreach($list1 as $k => $tab1){
				
				$list[$i]["id"]    = $tab1["id_statut_saisie"]."_ss";
				$list[$i]["value"] = $tab1["libelle"];
				$i++;
			}
			foreach($list2 as $k => $tab2){
				
				$list[$i]["id"]    = $tab2["id_flag_traitement"]."_ft";
				$list[$i]["value"] = $tab2["traitement"];
				$i++;
			}
			
			/*foreach($list2 as $k => $tab2){
				$k = $tab2["id_flag_traitement"];
				$v = $tab2["traitement"];
				$list["id"]    = $k;
				$list["value"] = $v;
			}*/
			
			$array_view_plis['list_statut'] = $list;
			if($list) $this->load->view('indicateur/statut_select.php', $array_view_plis);	
		
	}
	public function get_mode_paiement()
    {
			$list = $this->mvisu->get_mode_paiement();
			$array_view_plis['list_mp'] = $list;
			if($list) $this->load->view('indicateur/modepaiement_select.php', $array_view_plis);		
       
    }
	
}
?>
