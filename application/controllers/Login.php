<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index(){
        $this->load->library('portier');
		$this->portier->deconnexion();
        $this->load->view('login/page');
        // $this->load->library('run_script');
    }

    public function connexion() {//ajax connexion
        $reponse = array('code' => 1);
        if( isset($_REQUEST['login']) and isset($_REQUEST['pswd']) ){
            $login = base64_decode($this->input->post_get('login'));
            $pswd = base64_decode($this->input->post_get('pswd'));
            $this->load->model('model_login');
            $data = $this->model_login->connexion($login, $pswd);
            if(!is_null($data)){
                $reponse['code'] = 0;
                $this->session->id_utilisateur = $data->id_utilisateur;
                $this->session->id_type_utilisateur = $data->id_type_utilisateur;
                $this->session->avec_traitement_ko = intval($data->avec_traitement_ko);
                $this->session->login = $data->login;
                $this->session->niveau_op = is_numeric($data->niveau_op) ? $data->niveau_op : 1;
                $this->histo->action(1);
                //regulation logout auto si y en a pas
                switch ($this->session->id_type_utilisateur) {
                    case '1'://page administrateur
						log_message('info', 'Login de '.$login.' en admin');
                        $reponse['data'] =  site_url('admin/users');
                        echo json_encode($reponse);
						break;
					case '2'://page operateur
						log_message('info', 'Login de '.$login.' en operateur');
                        $reponse['data'] =  site_url('typage/typage');
                        echo json_encode($reponse);
						break;
					case '3'://page controleur
						log_message('info', 'Login de '.$login.' en contrôleur qualité');
                        $reponse['data'] =  site_url('control/control');
                        echo json_encode($reponse);
                        break;
                    case '100':/* pour relation client Benja*/
                    case '4'://page obervateur
                    case '8'://cdn /*Benja*/
						log_message('info', 'Login de '.$login.' en observateur');
                        $reponse['data'] =  site_url('visual/visual');

                        /*Benja pour la recherche avancee*/
                        $this->session->set_userdata('TypeTtmt', 1);
                        /*Fin Benja*/
                        echo json_encode($reponse);
						break;
                    case '5'://operateur push
                        $data_op_push = $this->model_login->my_acces_push($data->id_utilisateur);
                        if(is_null($data_op_push)){
                            log_message('error', 'Login: operateur push acces inconnu = '.$this->session->id_type_utilisateur);
                            $reponse['data'] =  site_url('chantier');
                            echo json_encode($reponse);
                        }else {
                            $this->session->acces_op_push = $data_op_push->id_type_acces;
                            switch ($data_op_push->id_type_acces) {
                                case '2'://typage push
                                    log_message('info', 'Login de '.$login.' en '.$data_op_push->comm);
                                    $reponse['data'] =  site_url('push/typage/pli');
                                    echo json_encode($reponse);
                                    break;
                                /*case '2'://saisie1 push
                                    log_message('info', 'Login de '.$login.' en '.$data_op_push->comm);
                                    $reponse['data'] =  site_url('push/saisie/saisie_push');
                                    echo json_encode($reponse);
                                    break;*/
                                case '3'://saisie2 & retraitement push
                                    log_message('info', 'Login de '.$login.' en '.$data_op_push->comm);
                                    $reponse['data'] =  site_url('push/saisie/saisie_push2');
                                    echo json_encode($reponse);
                                    break;
                                default:
                                    log_message('error', 'Login: operateur push acces inconnu = '.$this->session->id_type_utilisateur);
                                    $reponse['data'] =  site_url('chantier');
                                    echo json_encode($reponse);
                                    break;
                            }
                        }
						break;

                    case '6':
                        $reponse['data'] =  site_url('push/decoupage/Flux_Sftp');
                        echo json_encode($reponse);
                        break;
                    default://page login
						log_message('error', 'Login: type utilisateur inconnu = '.$this->session->id_type_utilisateur);
                        $reponse['data'] =  site_url('chantier');
                        echo json_encode($reponse);
						break;
                }
            }else{
                log_message('info', 'Login: access refuse pour '.$login.'/'.$pswd);
                $reponse['data'] = 'Accès refusé';
                echo json_encode($reponse);
                return '';
            }
        }else{
            log_message('error', 'Login: Parametre incomplet');
            $reponse['data'] = 'Parametres incomplets';
            echo json_encode($reponse);
        }
    }

}
