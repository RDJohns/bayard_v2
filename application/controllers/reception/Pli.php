<?php

class Pli extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        set_time_limit(7200); 

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }

        $this->load->model("reception/Model_Reception", "Mreception");
        $this->load->model("reception/Model_Liste_plis", "Mliste");
    }

    public function index()
    {
        $this->session->set_userdata('infouser', " IP:". $this->input->ip_address()." LOGIN: ".(int)$this->session->userdata('id_utilisateur'));
        if((int)$_SESSION['id_type_utilisateur'] == 4 )
        {
            $this->viewPrincipale();

        }
        else{
            redirect(base_url());
        }
        
    }

    public  function viewPrincipale()
    {
        $head                        = array();
        $foot                        = array();

        $head["title"]               = $head["menu"] = "Réception";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "equalizer";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'plugins/morrisjs/morris',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            'css/custom/reception',
			'../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/raphael/raphael.min',
            'plugins/morrisjs/morris'
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'plugins/popper/popper',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/custom/reception'
        );


        $this->load->view("main/header",$head);
        $this->load->view("reception/Filtre");
        $this->load->view("reception/Suivi-typologie");
        $this->load->view("reception/Suivi-cloture");
        $this->load->view("reception/Suivi-en-cours");
        $this->load->view("reception/Restant-a-traiter");
        $this->load->view("main/footer",$foot);
    }
    public function pliParTypologie()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe"); 
    
        $receptionParTypo = $this->Mreception->pliParTypologie($date1,$date2,(int)$societe);
        echo json_encode($receptionParTypo);
        return "";

    }
	public function pliAvecAnomalie() 
	{
		$date1             = $this->input->post("dateDebut");
        $date2             = $this->input->post("dateFin");
        $societe           = $this->input->post("societe");
		
        $pliAnomalie       = 0;
        $pliKoScan         = 0;
        $pliKoDefinitif    = 0;
        $pliKoInconnu      = 0;
        $pliKoSRC          = 0;
        $pliEnAttente      = 0;
        $pliKoCirculaire   = 0;
        $pliKoCiEditee     = 0;
        $pliCiEnvoyee      = 0;
        $pliKoEnCours      = 0;
        $pliOkCi           = 0;

        $pliAnomaliePr     = 0;
        $pliKoScanPr       = 0;
        $pliKoDefinitifPr  = 0;
        $pliKoInconnuPr    = 0;
        $pliKoSRCPr        = 0;
        $pliEnAttentePr    = 0;
        $pliKoCirculairePr = 0;
        $pliKoCiEditeePr   = 0;
        $pliCiEnvoyeePr    = 0;
        $pliKoEnCoursPr    = 0;
        $pliOkCiPr         = 0;

        $pliAnomalie_       = 0;
        $pliKoScan_         = 0;
        $pliKoDefinitif_    = 0;
        $pliKoInconnu_      = 0;
        $pliKoSRC_          = 0;
        $pliEnAttente_      = 0;
        $pliKoCirculaire_   = 0;
        $pliKoCiEditee_     = 0;
        $pliCiEnvoyee_      = 0;
        $pliKoEnCours_      = 0;
        $pliOkCi_           = 0;

        $pliAnomaliePr_     = 0;
        $pliKoScanPr_       = 0;
        $pliKoDefinitifPr_  = 0;
        $pliKoInconnuPr_    = 0;
        $pliKoSRCPr_        = 0;
        $pliEnAttentePr_    = 0;
        $pliKoCirculairePr_ = 0;
        $pliKoCiEditeePr_   = 0;
        $pliCiEnvoyeePr_    = 0;
        $pliKoEnCoursPr_    = 0;
        $pliOkCiPr_         = 0;

	$anomalie   = $this->Mreception->pliAvecAnomalie($date1,$date2,intval($societe));
		
		
		if($anomalie)
		{
			
			
			/*
			SUM(pli_anomalie) pli_anomalie,
			SUM(pli_ko_scan) pli_ko_scan,
			SUM(pli_ko_definitif) pli_ko_definitif,
			SUM(pli_ko_inconnu) pli_ko_inconnu,
			SUM(pli_ko_src) pli_ko_src,
			SUM(pli_en_attente) pli_en_attente,
			SUM(pli_ko_circulaire) pli_ko_circulaire,
			SUM(pli_ko_ci_editee) pli_ko_ci_editee,
			SUM(pli_ci_envoyee) pli_ci_envoyee
		*/
		
			if(intval($anomalie[0]->pli_anomalie) > 0)
			{
				$pliAnomalie = intval($anomalie[0]->pli_anomalie);
				
				if(intval($anomalie[0]->pli_ko_scan) > 0)
				{
					$pliKoScan      = intval($anomalie[0]->pli_ko_scan);
					$pliKoScanPr    = round((($anomalie[0]->pli_ko_scan)*100.00)/($pliAnomalie),2);
				}
				
				if(intval($anomalie[0]->pli_ko_definitif) > 0)
				{
					$pliKoDefinitif     = intval($anomalie[0]->pli_ko_definitif);
					$pliKoDefinitifPr   = round((($anomalie[0]->pli_ko_definitif)*100.00)/($pliAnomalie),2);
				}
				
				if(intval($anomalie[0]->pli_ko_inconnu) > 0)
				{
					$pliKoInconnu   = intval($anomalie[0]->pli_ko_inconnu);
					$pliKoInconnuPr = round((($anomalie[0]->pli_ko_inconnu)*100.00)/($pliAnomalie),2);
				}
				
				if(intval($anomalie[0]->pli_ko_src) > 0)
				{
					$pliKoSRC   = intval($anomalie[0]->pli_ko_src);
					$pliKoSRCPr = round((($anomalie[0]->pli_ko_src)*100.00)/($pliAnomalie),2);
				}
				
				if(intval($anomalie[0]->pli_en_attente) > 0)
				{
					$pliEnAttente   = intval($anomalie[0]->pli_en_attente);
					$pliEnAttentePr = round((($anomalie[0]->pli_en_attente)*100.00)/($pliAnomalie),2);
				}
				
				if(intval($anomalie[0]->pli_ko_circulaire) > 0)
				{
					$pliKoCirculaire    = intval($anomalie[0]->pli_ko_circulaire);
					$pliKoCirculairePr  = round((($anomalie[0]->pli_ko_circulaire)*100.00)/($pliAnomalie),2);
				}
				
				if(intval($anomalie[0]->pli_ko_ci_editee) > 0)
				{
					$pliKoCiEditee      = intval($anomalie[0]->pli_ko_ci_editee);
					$pliKoCiEditeePr    = round((($anomalie[0]->pli_ko_ci_editee)*100.00)/($pliAnomalie),2);
                }

                if(intval($anomalie[0]->pli_ko_en_cours) > 0)
				{
					$pliKoEnCours      = intval($anomalie[0]->pli_ko_en_cours);
					$pliKoEnCoursPr    = round((($anomalie[0]->pli_ko_en_cours)*100.00)/($pliAnomalie),2);
                }

                if(intval($anomalie[0]->pli_ok_ci) > 0)
				{
					$pliOkCi      = intval($anomalie[0]->pli_ok_ci);
					$pliOkCiPr    = round((($anomalie[0]->pli_ok_ci)*100.00)/($pliAnomalie),2);
                }
            }

            /*pli passé dans KO */
            if(intval($anomalie[1]->pli_anomalie) > 0)
            {
                $pliAnomalie_           = intval($anomalie[1]->pli_anomalie);
                
                if(intval($anomalie[1]->pli_ko_scan) > 0)
                {
                    $pliKoScan_         = intval($anomalie[1]->pli_ko_scan);
                    $pliKoScanPr_       = round((($anomalie[1]->pli_ko_scan)*100.00)/($pliAnomalie_),2);
                }
                
                if(intval($anomalie[1]->pli_ko_definitif) > 0)
                {
                    $pliKoDefinitif_    = intval($anomalie[1]->pli_ko_definitif);
                    $pliKoDefinitifPr_  = round((($anomalie[1]->pli_ko_definitif)*100.00)/($pliAnomalie_),2);
                }
                
                if(intval($anomalie[1]->pli_ko_inconnu) > 0)
                {
                    $pliKoInconnu_      = intval($anomalie[1]->pli_ko_inconnu);
                    $pliKoInconnuPr_    = round((($anomalie[1]->pli_ko_inconnu)*100.00)/($pliAnomalie_),2);
                }
                
                if(intval($anomalie[1]->pli_ko_src) > 0)
                {
                    $pliKoSRC_          = intval($anomalie[1]->pli_ko_src);
                    $pliKoSRCPr_        = round((($anomalie[1]->pli_ko_src)*100.00)/($pliAnomalie_),2);
                }
                
                if(intval($anomalie[1]->pli_en_attente) > 0)
                {
                    $pliEnAttente_      = intval($anomalie[1]->pli_en_attente);
                    $pliEnAttentePr_    = round((($anomalie[1]->pli_en_attente)*100.00)/($pliAnomalie_),2);
                }
                
                if(intval($anomalie[1]->pli_ko_circulaire) > 0)
                {
                    $pliKoCirculaire_   = intval($anomalie[1]->pli_ko_circulaire);
                    $pliKoCirculairePr_ = round((($anomalie[1]->pli_ko_circulaire)*100.00)/($pliAnomalie_),2);
                }
                
                if(intval($anomalie[1]->pli_ko_ci_editee) > 0)
                {
                    $pliKoCiEditee_     = intval($anomalie[1]->pli_ko_ci_editee);
                    $pliKoCiEditeePr_   = round((($anomalie[1]->pli_ko_ci_editee)*100.00)/($pliAnomalie_),2);
                }

                if(intval($anomalie[1]->pli_ko_en_cours) > 0)
                {
                    $pliKoEnCours_     = intval($anomalie[1]->pli_ko_en_cours);
                    $pliKoEncoursPr_   = round((($anomalie[1]->pli_ko_ci_editee)*100.00)/($pliAnomalie_),2);
                }

                if(intval($anomalie[1]->pli_ok_ci) > 0)
                {
                    $pliOkCi_     = intval($anomalie[1]->pli_ok_ci);
                    $pliOkCiPr_   = round((($anomalie[1]->pli_ok_ci)*100.00)/($pliAnomalie_),2);
                }
            }

		}
		
		/*
			SUM(pli_anomalie) pli_anomalie,
			SUM(pli_ko_scan) pli_ko_scan,
			SUM(pli_ko_definitif) pli_ko_definitif,
			SUM(pli_ko_inconnu) pli_ko_inconnu,
			SUM(pli_ko_src) pli_ko_src,
			SUM(pli_en_attente) pli_en_attente,
			SUM(pli_ko_circulaire) pli_ko_circulaire,
			SUM(pli_ko_ci_editee) pli_ko_ci_editee,
			SUM(pli_ci_envoyee) pli_ci_envoyee*/
		
		$retour = array();
		$retour  =  array(
					"pli_anomalie"=>intval($pliAnomalie),
					"pli_ko_scan"=>$pliKoScan,
					"pli_ko_definitif"=>$pliKoDefinitif,
					"pli_ko_inconnu"=>$pliKoInconnu,
					"pli_ko_src"=>$pliKoSRC,
					"pli_en_attente"=>$pliEnAttente,
					"pli_ko_circulaire"=>$pliKoCirculaire,
					"pli_ko_ci_editee"=>$pliKoCiEditee,
					"pli_ci_envoyee"=>$pliCiEnvoyee,
					"pli_ko_en_cours"=>$pliKoEnCours,
					"pli_ok_ci"=>$pliOkCi,
					"pr_anomalie"=>$pliAnomaliePr,
					"pr_ko_scan"=>$pliKoScanPr,
					"pr_ko_definitif"=>$pliKoDefinitifPr,
					"pr_ko_inconnu"=>$pliKoInconnuPr,
					"pr_ko_src"=>$pliKoSRCPr,
					"pr_en_attente"=>$pliEnAttentePr,
					"pr_ko_circulaire"=>$pliKoCirculairePr,
					"pr_ko_ci_editee"=>$pliKoCiEditeePr,
                    "pr_ci_envoyee"=>$pliCiEnvoyeePr,
                    "pr_ko_en_cours"=>$pliKoEnCoursPr,
                    "pr_ok_ci"=>$pliOkCiPr,
                    "pli_anomalie_"=>intval($pliAnomalie_),
                    "pli_ko_scan_"=>$pliKoScan_,
                    "pli_ko_definitif_"=>$pliKoDefinitif_,
                    "pli_ko_inconnu_"=>$pliKoInconnu_,
                    "pli_ko_src_"=>$pliKoSRC_,
                    "pli_en_attente_"=>$pliEnAttente_,
                    "pli_ko_circulaire_"=>$pliKoCirculaire_,
                    "pli_ko_ci_editee_"=>$pliKoCiEditee_,
                    "pli_ci_envoyee_"=>$pliCiEnvoyee_,
                    "pli_ko_en_cours_"=>$pliKoEnCours_,
                    "pli_ok_ci_"=>$pliOkCi_,
                    "pr_anomalie_"=>$pliAnomaliePr_,
                    "pr_ko_scan_"=>$pliKoScanPr_,
                    "pr_ko_definitif_"=>$pliKoDefinitifPr_,
                    "pr_ko_inconnu_"=>$pliKoInconnuPr_,
                    "pr_ko_src_"=>$pliKoSRCPr_,
                    "pr_en_attente_"=>$pliEnAttentePr_,
                    "pr_ko_circulaire_"=>$pliKoCirculairePr_,
                    "pr_ko_ci_editee_"=>$pliKoCiEditeePr_,
                    "pr_ci_envoyee_"=>$pliCiEnvoyeePr_,
                    "pr_ko_en_cours_"=>$pliKoEnCoursPr_,
                    "pr_ok_ci_"=>$pliOkCiPr_
					);
		echo json_encode($retour); return "";
	}
    public function pliEncours()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        
        $pliEncours = array();
        
       $pliTotalEncours             = 0;
	   $mvtTotalEncours             = 0;
        $receptionParTypo           = $this->Mreception->pliEncours2($date1,$date2,(int)$societe);
		if($receptionParTypo)
		{
			if(intval($receptionParTypo[0]->pli_en_cours) > 0)
			{
				$pliTotalEncours             = intval($receptionParTypo[0]->pli_en_cours);
				$pourcentTypage              = round((($receptionParTypo[0]->en_cours_typage)*100.00)/($pliTotalEncours),2);
				$pourcentEnAttenteSaisie     = round((($receptionParTypo[0]->en_att_saisie)*100.00)/($pliTotalEncours),2);
				$pourcentEncoursSaisie       = round((($receptionParTypo[0]->en_cours_saisie)*100.00)/($pliTotalEncours),2);
				$pourcentTEnAttenteCtrl      = round((($receptionParTypo[0]->en_att_ctrl)*100.00)/($pliTotalEncours),2);
				$pourcentEncoursCtrl         = round((($receptionParTypo[0]->en_cours_ctrl)*100.00)/($pliTotalEncours),2);
				$pourcentAttenteRetraitement = round((($receptionParTypo[0]->en_attente_retraitement)*100.00)/($pliTotalEncours),2);
				$pourcentEnCoursRettmt       = round((($receptionParTypo[0]->en_cours_retraitement)*100.00)/($pliTotalEncours),2);
			}
			
			if(intval($receptionParTypo[0]->mvt_pli_en_cours) > 0 )
			{
				$mvtTotalEncours                = intval($receptionParTypo[0]->mvt_pli_en_cours);
				$mvtPourcentTypage              = round((($receptionParTypo[0]->nb_mvt_en_cours_typage)*100.00)/($mvtTotalEncours),2);
				$mvtPourcentEnAttenteSaisie     = round((($receptionParTypo[0]->mvt_en_att_saisie)*100.00)/($mvtTotalEncours),2);
				$mvtPourcentEncoursSaisie       = round((($receptionParTypo[0]->mvt_en_cours_saisie)*100.00)/($mvtTotalEncours),2);
				$mvtPourcentTEnAttenteCtrl      = round((($receptionParTypo[0]->mvt_en_att_ctrl)*100.00)/($mvtTotalEncours),2);
				$mvtPourcentEncoursCtrl         = round((($receptionParTypo[0]->mvt_en_cours_ctrl)*100.00)/($mvtTotalEncours),2);
				$mvtPourcentAttenteRetraitement = round((($receptionParTypo[0]->mvt_en_attente_retraitement)*100.00)/($mvtTotalEncours),2);
				$mvtPourcentEnCoursRettmt       = round((($receptionParTypo[0]->mvt_en_cours_retraitement)*100.00)/($mvtTotalEncours),2);
			}
			
		}
        
        if($pliTotalEncours <= 0 )
        {
            $pourcentTypage              = 0 ;
            $pourcentEnAttenteSaisie     = 0 ;
            $pourcentEncoursSaisie       = 0 ;
            $pourcentTEnAttenteCtrl      = 0 ;
            $pourcentEncoursCtrl         = 0 ;
            $pourcentAttenteRetraitement = 0 ;
            $pourcentEnCoursRettmt       = 0 ; 
          
        }
		
        
         if($mvtTotalEncours <= 0 )
        {
            $mvtPourcentTypage              = 0 ;
            $mvtPourcentEnAttenteSaisie     = 0 ;
            $mvtPourcentEncoursSaisie       = 0 ;
            $mvtPourcentTEnAttenteCtrl      = 0 ;
            $mvtPourcentEncoursCtrl         = 0 ;
            $mvtPourcentAttenteRetraitement = 0 ;
            $mvtPourcentEnCoursRettmt       = 0 ;
            
        }
        
        /*
             coalesce(SUM(validation_ok),0)::text validation_ok,
                coalesce(SUM(mvt_validation_ok),0)::text mvt_validation_ok,
            */
        $pliEncours = array(
                            array(
                                
                                "pli_total"=>$receptionParTypo[0]->pli_en_cours,
                                "mvt_total"=>$receptionParTypo[0]->mvt_pli_en_cours,
                               
                            ),
                            array(
                                "libelle"=>"En cours de typage",
                                "pli"=>$receptionParTypo[0]->en_cours_typage,
                                "mvt"=>$receptionParTypo[0]->nb_mvt_en_cours_typage,
                                "pourcent_pli"=>$pourcentTypage,
                                "pourcent_mv"=>$mvtPourcentTypage
                            ),
                             array(
                                "libelle"=>"En attente de saisie",
                                "pli"=>$receptionParTypo[0]->en_att_saisie,
                                "mvt"=>$receptionParTypo[0]->mvt_en_att_saisie,
                                "pourcent_pli"=>$pourcentEnAttenteSaisie,
                                "pourcent_mv"=>$mvtPourcentEnAttenteSaisie
                            ),
                            array(
                                "libelle"=>"En cours de saisie",
                                "pli"=>$receptionParTypo[0]->en_cours_saisie,
                                "mvt"=>$receptionParTypo[0]->mvt_en_cours_saisie,
                                "pourcent_pli"=>$pourcentEncoursSaisie,
                                "pourcent_mv"=>$mvtPourcentEncoursSaisie
                            ),
                            array(
                                "libelle"=>"En attente de contrôle",
                                "pli"=>$receptionParTypo[0]->en_att_ctrl,
                                "mvt"=>$receptionParTypo[0]->mvt_en_att_ctrl,
                                "pourcent_pli"=>$pourcentTEnAttenteCtrl,
                                "pourcent_mv"=>$mvtPourcentTEnAttenteCtrl
                            ),
                            array(
                                "libelle"=>"En cours de contrôle",
                                "pli"=>$receptionParTypo[0]->en_cours_ctrl,
                                "mvt"=>$receptionParTypo[0]->mvt_en_cours_ctrl,
                                "pourcent_pli"=>$pourcentEncoursCtrl,
                                "pourcent_mv"=>$mvtPourcentEncoursCtrl
                            ),
                            array(
                                "libelle"=>"En attente de retraitement",
                                "pli"=>$receptionParTypo[0]->en_attente_retraitement,
                                "mvt"=>$receptionParTypo[0]->mvt_en_attente_retraitement,
                                "pourcent_pli"=>$pourcentAttenteRetraitement,
                                "pourcent_mv"=>$mvtPourcentAttenteRetraitement
                            ),
                            array(
                                "libelle"=>"En cours de retraitement",
                                "pli"=>$receptionParTypo[0]->en_cours_retraitement,
                                "mvt"=>$receptionParTypo[0]->mvt_en_cours_retraitement,
                                "pourcent_pli"=>$pourcentEnCoursRettmt,
                                "pourcent_mv"=>$mvtPourcentEnCoursRettmt
                            )
                        );
        
        // var_dump($pliEncours);
        echo json_encode($pliEncours);
        return "";

    }
    public function pliCloture()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $receptionParTypo = $this->Mreception->pliCloture($date1,$date2,(int)$societe);
        echo json_encode($receptionParTypo);
        return "";

    }
    public function  typologieJournalier()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

        
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->typologieJournalier($date1,$date2,(int)$societe);
        
        $arrayTable = array();
       $i = 0;

        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"] = daty_db_to_fr($item->date_courrier);
            $arrayTable[$i]["info_0"] = $item->info_0;
            $arrayTable[$i]["info_2"] = $item->info_2;
            $arrayTable[$i]["info_3"] = $item->info_3;
            $arrayTable[$i]["info_4"] = $item->info_4;
            $arrayTable[$i]["info_5"] = $item->info_5;
            $arrayTable[$i]["info_6"] = $item->info_6;
            $arrayTable[$i]["info_7"] = $item->info_7;
            $arrayTable[$i]["info_8"] = $item->info_8;
            $arrayTable[$i]["info_9"] = $item->info_9;
            $arrayTable[$i]["info_10"] = $item->info_10;
            $arrayTable[$i]["info_11"] = $item->info_11;
            $arrayTable[$i]["info_12"] = $item->info_12;
            $arrayTable[$i]["info_13"] = $item->info_13;
            $arrayTable[$i]["info_14"] = $item->info_14;
            $arrayTable[$i]["info_15"] = $item->info_15;
            $arrayTable[$i]["info_16"] = $item->info_16;
            $arrayTable[$i]["info_17"] = $item->info_17;
            $arrayTable[$i]["info_18"] = $item->info_18;
            $arrayTable[$i]["info_19"] = $item->info_19;
            $arrayTable[$i]["info_20"] = $item->info_20;
            $arrayTable[$i]["info_21"] = $item->info_21;
            $arrayTable[$i]["info_22"] = $item->info_22;
            $arrayTable[$i]["info_23"] = $item->info_23;
            $arrayTable[$i]["info_24"] = $item->info_24;
            $arrayTable[$i]["info_25"] = $item->info_25;
            $arrayTable[$i]["info_26"] = $item->info_26;
            $arrayTable[$i]["info_27"] = $item->info_27;

            $i++;
        
        }
       
       echo json_encode($arrayTable);
        
    
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function nonTraiteEncours()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $reception  = $this->Mreception->nonTraiteEncours($date1,$date2,(int)$societe);
        echo json_encode($reception);
        return "";
    }

    public function pliFermer()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        
        $pliFermer = $this->Mreception->pliFermer($date1,$date2,$societe);
        echo  json_encode($pliFermer);
        return "";

    }

    public function  typologieSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->typologieSemaine($date1,$date2,(int)$societe);
        
        $arrayTable = array();
       $i = 0;

        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"] = $item->semaine;
            $arrayTable[$i]["info_0"] = $item->info_0;
            $arrayTable[$i]["info_2"] = $item->info_2;
            $arrayTable[$i]["info_3"] = $item->info_3;
            $arrayTable[$i]["info_4"] = $item->info_4;
            $arrayTable[$i]["info_5"] = $item->info_5;
            $arrayTable[$i]["info_6"] = $item->info_6;
            $arrayTable[$i]["info_7"] = $item->info_7;
            $arrayTable[$i]["info_8"] = $item->info_8;
            $arrayTable[$i]["info_9"] = $item->info_9;
            $arrayTable[$i]["info_10"] = $item->info_10;
            $arrayTable[$i]["info_11"] = $item->info_11;
            $arrayTable[$i]["info_12"] = $item->info_12;
            $arrayTable[$i]["info_13"] = $item->info_13;
            $arrayTable[$i]["info_14"] = $item->info_14;
            $arrayTable[$i]["info_15"] = $item->info_15;
            $arrayTable[$i]["info_16"] = $item->info_16;
            $arrayTable[$i]["info_17"] = $item->info_17;
            $arrayTable[$i]["info_18"] = $item->info_18;
            $arrayTable[$i]["info_19"] = $item->info_19;
            $arrayTable[$i]["info_20"] = $item->info_20;
            $arrayTable[$i]["info_21"] = $item->info_21;
            $arrayTable[$i]["info_22"] = $item->info_22;
            $arrayTable[$i]["info_23"] = $item->info_23;
            $arrayTable[$i]["info_24"] = $item->info_24;
            $arrayTable[$i]["info_25"] = $item->info_25;
            $arrayTable[$i]["info_26"] = $item->info_26;
            $arrayTable[$i]["info_27"] = $item->info_27;

            $i++;
        
        }
       
       echo json_encode($arrayTable);
        
    
    }

    public function  cloJournalier()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

       $date1       = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->cloJournalier($date1,$date2,(int)$societe);
        
        $arrayTable = array();
       $i = 0;

        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"] = daty_db_to_fr($item->date_courrier);
            $arrayTable[$i]["info_0"] = $item->info_0;
            $arrayTable[$i]["info_2"] = $item->info_2;
            $arrayTable[$i]["info_3"] = $item->info_3;
            $arrayTable[$i]["info_4"] = $item->info_4;
            $arrayTable[$i]["info_5"] = $item->info_5;
            $arrayTable[$i]["info_6"] = $item->info_6;
            $arrayTable[$i]["info_7"] = $item->info_7;
            $arrayTable[$i]["info_8"] = $item->info_8;
            $arrayTable[$i]["info_9"] = $item->info_9;
            $arrayTable[$i]["info_10"] = $item->info_10;
            $arrayTable[$i]["info_11"] = $item->info_11;
            $arrayTable[$i]["info_12"] = $item->info_12;
            $arrayTable[$i]["info_13"] = $item->info_13;
            $arrayTable[$i]["info_14"] = $item->info_14;
            $arrayTable[$i]["info_15"] = $item->info_15;
            $arrayTable[$i]["info_16"] = $item->info_16;
            $arrayTable[$i]["info_17"] = $item->info_17;
            $arrayTable[$i]["info_18"] = $item->info_18;
            $arrayTable[$i]["info_19"] = $item->info_19;
            $arrayTable[$i]["info_20"] = $item->info_20;
            $arrayTable[$i]["info_21"] = $item->info_21;
            $arrayTable[$i]["info_22"] = $item->info_22;
            $arrayTable[$i]["info_23"] = $item->info_23;
            $arrayTable[$i]["info_24"] = $item->info_24;
            $arrayTable[$i]["info_25"] = $item->info_25;
            $arrayTable[$i]["info_26"] = $item->info_26;
            $arrayTable[$i]["info_27"] = $item->info_27;

            $i++;
        
        }
       
       echo json_encode($arrayTable);
        
    
    }

    public function  cloTypologieSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

       $date1       = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->cloTypologieSemaine($date1,$date2,(int)$societe);
        
        $arrayTable = array();
       $i = 0;

        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"] = $item->semaine;
            $arrayTable[$i]["info_0"] = $item->info_0;
            $arrayTable[$i]["info_2"] = $item->info_2;
            $arrayTable[$i]["info_3"] = $item->info_3;
            $arrayTable[$i]["info_4"] = $item->info_4;
            $arrayTable[$i]["info_5"] = $item->info_5;
            $arrayTable[$i]["info_6"] = $item->info_6;
            $arrayTable[$i]["info_7"] = $item->info_7;
            $arrayTable[$i]["info_8"] = $item->info_8;
            $arrayTable[$i]["info_9"] = $item->info_9;
            $arrayTable[$i]["info_10"] = $item->info_10;
            $arrayTable[$i]["info_11"] = $item->info_11;
            $arrayTable[$i]["info_12"] = $item->info_12;
            $arrayTable[$i]["info_13"] = $item->info_13;
            $arrayTable[$i]["info_14"] = $item->info_14;
            $arrayTable[$i]["info_15"] = $item->info_15;
            $arrayTable[$i]["info_16"] = $item->info_16;
            $arrayTable[$i]["info_17"] = $item->info_17;
            $arrayTable[$i]["info_18"] = $item->info_18;
            $arrayTable[$i]["info_19"] = $item->info_19;
            $arrayTable[$i]["info_20"] = $item->info_20;
            $arrayTable[$i]["info_21"] = $item->info_21;
            $arrayTable[$i]["info_22"] = $item->info_22;
            $arrayTable[$i]["info_23"] = $item->info_23;
            $arrayTable[$i]["info_24"] = $item->info_24;
            $arrayTable[$i]["info_25"] = $item->info_25;
            $arrayTable[$i]["info_26"] = $item->info_26;
            $arrayTable[$i]["info_27"] = $item->info_27;

            $i++;
        
        }
       
       echo json_encode($arrayTable);
        
    
    }


    public function  encoursParSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->parSemaineEncours($date1,$date2,(int)$societe);
        
        $arrayTable = array();
        $i          = 0;

        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"]            = "S ".$item->semaine;
            $arrayTable[$i]["en_cours_typage"]          = $item->en_cours_typage;
            $arrayTable[$i]["en_attente_saisie"]        = $item->en_att_saisie;
            $arrayTable[$i]["en_cours_saisie"]          = $item->en_cours_saisie;
            $arrayTable[$i]["en_attente_controle"]      = $item->en_att_ctrl;
            $arrayTable[$i]["pli_en_cours_controle"]    = $item->en_cours_ctrl;
            $arrayTable[$i]["en_attente_retraitement"]  = $item->en_attente_retraitement;
            $arrayTable[$i]["en_cours_retraitement"]    = $item->en_cours_retraitement;

            $i++;
        }
       
       echo json_encode($arrayTable);
        
    
    }

    public function  encoursParJour()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

       $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->detailEncours($date1,$date2,(int)$societe);
        
        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"]            = $item->courrier_date;
            $arrayTable[$i]["en_cours_typage"]          = $item->en_cours_typage;
            $arrayTable[$i]["en_attente_saisie"]        = $item->en_att_saisie;
            $arrayTable[$i]["en_cours_saisie"]          = $item->en_cours_saisie;
            $arrayTable[$i]["en_attente_controle"]      = $item->en_att_ctrl;
            $arrayTable[$i]["pli_en_cours_controle"]    = $item->en_cours_ctrl;
            $arrayTable[$i]["en_attente_retraitement"]      = $item->en_attente_retraitement;
            $arrayTable[$i]["en_cours_retraitement"]          = $item->en_cours_retraitement; 
            
            


            $i++;
        }
       
       echo json_encode($arrayTable);
        
    
    }
	
	public function  avecAnomalieJour()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

       $date1       = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->avecAnomalieJour($date1,$date2,(int)$societe);
        
        $arrayTable = array();
        $i          = 0;
        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"]            = $item->courrier_date;
            $arrayTable[$i]["pli_ko_scan"]              = $item->pli_ko_scan;
            $arrayTable[$i]["pli_ko_definitif"]         = $item->pli_ko_definitif;
            $arrayTable[$i]["pli_ko_inconnu"]           = $item->pli_ko_inconnu;
            $arrayTable[$i]["pli_ko_src"]               = $item->pli_ko_src;
            $arrayTable[$i]["pli_en_attente"]           = $item->pli_en_attente;
            $arrayTable[$i]["pli_ko_circulaire"]        = $item->pli_ko_circulaire;
            $arrayTable[$i]["pli_ko_ci_editee"]         = $item->pli_ko_ci_editee; 
            $arrayTable[$i]["pli_ci_envoyee"]           = $item->pli_ci_envoyee; 
            $arrayTable[$i]["pli_ok_ci"]                = $item->pli_ok_ci;
            $arrayTable[$i]["pli_ko_en_cours"]          = $item->pli_ko_en_cours;
            $i++;
        }
       
       echo json_encode($arrayTable);
    }
    /**
     * $granularite 's' ou 'j'
     */
    public function  pliPasseEnKO()
    {
        $entete         = array();
        $ligne1         = array();
        $touteTypo      = array();
        $iTemp          = 0; 
        $i              = 0;
        $semaine        = "";
        $date1          = $this->input->post("dateDebut");
        $date2          = $this->input->post("dateFin");
        $societe        = $this->input->post("societe");
        $granularite    = $this->input->post("granularite");

        $res            = $this->Mreception->pliPasseEnKO($date1,$date2,intval($societe),$granularite);

        if($granularite == 's')
        {
            $semaine = "S ";
        }

        $arrayTable     = array();
        $i              = 0;
        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"]            = $semaine.$item->courrier_date;
            $arrayTable[$i]["pli_ko_scan"]              = $item->pli_ko_scan;
            $arrayTable[$i]["pli_ko_definitif"]         = $item->pli_ko_definitif;
            $arrayTable[$i]["pli_ko_inconnu"]           = $item->pli_ko_inconnu;
            $arrayTable[$i]["pli_ko_src"]               = $item->pli_ko_src;
            $arrayTable[$i]["pli_en_attente"]           = $item->pli_en_attente;
            $arrayTable[$i]["pli_ko_circulaire"]        = $item->pli_ko_circulaire;
            $arrayTable[$i]["pli_ko_ci_editee"]         = $item->pli_ko_ci_editee; 
            $arrayTable[$i]["pli_ci_envoyee"]           = $item->pli_ci_envoyee; 
            $arrayTable[$i]["pli_ok_ci"]                = $item->pli_ok_ci;
            $arrayTable[$i]["pli_ko_en_cours"]          = $item->pli_ko_en_cours;


            $i++;
        }
       
       echo json_encode($arrayTable);
    }

	
	public function  avecAnomalieSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->avecAnomalieSemaine($date1,$date2,(int)$societe);
        
        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"]            = $item->courrier_date;
            $arrayTable[$i]["pli_ko_scan"]          = $item->pli_ko_scan;
            $arrayTable[$i]["pli_ko_definitif"]        = $item->pli_ko_definitif;
            $arrayTable[$i]["pli_ko_inconnu"]          = $item->pli_ko_inconnu;
            $arrayTable[$i]["pli_ko_src"]      = $item->pli_ko_src;
            $arrayTable[$i]["pli_en_attente"]    = $item->pli_en_attente;
            $arrayTable[$i]["pli_ko_circulaire"]      = $item->pli_ko_circulaire;
            $arrayTable[$i]["pli_ko_ci_editee"]          = $item->pli_ko_ci_editee; 
            $arrayTable[$i]["pli_ci_envoyee"]          = $item->pli_ci_envoyee; 
            $arrayTable[$i]["pli_ko_en_cours"]          = $item->pli_ko_en_cours;
            $arrayTable[$i]["pli_ok_ci"]          = $item->pli_ok_ci;

            


            $i++;
        }
       
       echo json_encode($arrayTable);
    }
	
	
	
    public function  nonTraiteSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

       $date1       = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->nonTraiteSemaine($date1,$date2,(int)$societe);
        
        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"]         = $item->semaine;
            $arrayTable[$i]["pli_en_cours"]          = $item->pli_en_cours;
            $arrayTable[$i]["pli_non_traite"]        = $item->pli_non_traite;

            $i++;
        }
       
       echo json_encode($arrayTable);
        
    
    }
    public function  nonTraiteJour()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->nonTraiteJour($date1,$date2,(int)$societe);
        
        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"]         = $item->date_courrier;
            $arrayTable[$i]["pli_en_cours"]          = $item->pli_en_cours;
            $arrayTable[$i]["pli_non_traite"]        = $item->pli_non_traite;

            $i++;
        }
       
       echo json_encode($arrayTable);
        
    
    }
    public function  syntheseSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->syntheseSemaine($date1,$date2,(int)$societe);
        
        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"]        = "S".$item->semaine;
            $arrayTable[$i]["dernier_traitement"]   = daty_db_to_fr($item->dernier_traitement);
            $arrayTable[$i]["nb_pli_recus"]         = $item->nb_pli_recus;
            $arrayTable[$i]["non_traite"]           = $item->non_traite;
            $arrayTable[$i]["nb_cloture"]           = $item->nb_cloture;
            $arrayTable[$i]["nb_encours"]           = $item->nb_encours;
            //$arrayTable[$i]["nb_ferme"]             = $item->nb_ferme;
            $arrayTable[$i]["nb_ci_editee"]         = $item->nb_ci_editee;
            $arrayTable[$i]["nb_anomalie"]          = $item->nb_anomalie;

            $i++;
        }
       
       echo json_encode($arrayTable);
        
    
    }
    public function  syntheseJournaliere()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0; 

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $res        = $this->Mreception->syntheseJournaliere($date1,$date2,(int)$societe);
        
        $arrayTable = array();
        $i          = 0;
        
        foreach($res as $item)
        {
            
            $arrayTable[$i]["date_courrier"]        = daty_db_to_fr($item->date_courrier);
            $arrayTable[$i]["dernier_traitement"]   = daty_db_to_fr($item->dernier_traitement);
            $arrayTable[$i]["nb_pli_recus"]         = $item->nb_pli_recus;
            $arrayTable[$i]["non_traite"]           = $item->non_traite;
            $arrayTable[$i]["nb_cloture"]           = $item->nb_cloture;
           // $arrayTable[$i]["nb_ferme"]           = $item->nb_ferme;
            $arrayTable[$i]["nb_encours"]           = $item->nb_encours;
			$arrayTable[$i]["nb_ci_editee"]         = $item->nb_ci_editee;
            $arrayTable[$i]["nb_anomalie"]          = $item->nb_anomalie;

            $i++;
        }
       
       echo json_encode($arrayTable);
    
    }
    
    public function getListePli()
    {
        $output             = array(); 
        $date1              = $this->input->post("dateDebut");
        $date2              = $this->input->post("dateFin");
        $societe            = $this->input->post("societe");
        $listePliReception  = $this->Mliste->getListePli($date1,$date2,(int)$societe);
        $recordsTotal       = $this->Mliste->recordsTotal($date1,$date2,(int)$societe);
        $recordsFiltered    = $this->Mliste->recordsFiltered($date1,$date2,(int)$societe);
        $output             = $this->fillListePli($listePliReception,$recordsTotal,$recordsFiltered);
      
		
        echo json_encode($output);
    }


    public function  fillListePli($obj,$recordsTotal,$recordsFiltered)
    {
        $data  = array(); 
		
        foreach ($obj as $liste)
        {
            $row      = array();
			$cmc7s    = str_replace("|NR","",$liste->cmc7s);
			$rlmcs 	  = str_replace("|NR","",$liste->rlmcs);
			$montants = str_replace("NR","",$liste->montants);
			$cmc7s 	  = str_replace("NR","",$cmc7s);
			$rlmcs    = str_replace("NR","",$rlmcs);
                $row[] =  $liste->date_courrier;
                $row[] =  $liste->date_numerisation;
                $row[] =  $liste->dt_event;
                $row[] =  $liste->lot_scan;
                $row[] =  $liste->pli;
                $row[] =  $liste->typologie;
                $row[] =  $liste->etape;
                $row[] =  $liste->etat;
                $row[] =  $liste->statut;
                $row[] =  $liste->nb_mouvement;
                $row[] =  $liste->id_lot_saisie;
                $row[] =  $liste->modes_paiements;
                $row[] =  $cmc7s;
                $row[] =  $montants;           
                $row[] =  $liste->nom_abonne;
                $row[] =  $liste->numero_abonne;
                $row[] =  "<span class='fa fa-envelope'></span> ".$liste->id_pli;
           
		
            $data[] = $row;
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data"            => $data,
        );

        return $output;
    }

    public function globaleReception()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        
        $output     = $this->Mreception->globaleReception($date1,$date2,(int)$societe);
        echo json_encode($output);
        return "";
    }

    public function verifDonnees()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $output     = $this->Mreception->verifDonnees($date1,$date2,(int)$societe);
        echo json_encode($output); return "";
    }

    public function exportListePlis()
    {
        
        $strTable = "";
        
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        
        $listePliReception  = $this->Mliste->exportListePli($date1,$date2,(int)$societe);


        $bordure = ' style="border:1px solid #cecece;" ';
        $bordure_text = ' style="border:1px solid;text-align: left;';

        $strTable   .= "<table><thead><tr>";
        $strTable   .= "<th".$bordure.">Date courrier</th>";
        $strTable   .= "<th".$bordure.">Date numérisation</th>";
        $strTable   .= "<th".$bordure.">Dernier traitement</th>";
        $strTable   .= "<th".$bordure.">Ref. lot scan</th>";
        $strTable   .= "<th".$bordure.">Pli</th>";
        $strTable   .= "<th".$bordure.">Typologie</th>";
        $strTable   .= "<th".$bordure.">Etape</th>";
        $strTable   .= "<th".$bordure.">Etat</th>";
        $strTable   .= "<th".$bordure.">Statut</th>";
        $strTable   .= "<th".$bordure.">Nbr. mouvement</th>";
        $strTable   .= "<th".$bordure.">ID lot de saisie</th>";
        $strTable   .= "<th".$bordure.">Moyen de paiement</th>";
        $strTable   .= "<th".$bordure.">CMC7</th>";
        $strTable   .= "<th".$bordure.">Montant</th>";
        $strTable   .= "<th".$bordure.">Nom abonné</th>";
        $strTable   .= "<th".$bordure.">N° abonné</th>";
        $strTable   .= "<th".$bordure.">ID pli</th></tr>";
        $strTable   .=" </thead><tbody>";
        $i = 1;
        foreach($listePliReception as $item)
        {
                $cmc7s    = str_replace("|NR","",$item->cmc7s);
                $cmc7s 	  = str_replace("NR","",$cmc7s);

                $strTable .= "<tr>";
                $strTable .= "<td style='border:1px solid ; text-align: left;' >".$item->date_courrier."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->date_numerisation."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->dt_event."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->lot_scan."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;' >'".$item->pli."'</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->typologie."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->etape."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->etat."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->statut."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->nb_mouvement."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->id_lot_saisie."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->modes_paiements."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>'".$cmc7s."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->montants."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->nom_abonne."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->numero_abonne."</td>";
                $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->id_pli."</td>";

            $strTable .= "</tr>";
            $i++;
        }

        $strTable .= "</tbody></table>";
        echo $strTable;

    }

    public  function encoursParTypologie()
    {
        $date1                  = $this->input->post("dateDebut");
        $date2                  = $this->input->post("dateFin");
        $societe                = $this->input->post("societe");

        $encoursParTypologie    = $this->Mreception->encoursParTypologie($date1,$date2,(int)$societe);
        $strTable               = ' <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="encours-par-typo-table">
                                <thead>
                                    <tr>
                                        <th>Typologie</th>
                                        <th>Nbr. Mvt</th>
                                        <th>En cours typage</th>
                                        <th>En cours saisie</th>
                                        <th>En attente saisie</th>
                                        <th>En attente contrôle</th>
                                        <th>En cours de contrôle</th>
                                        <th>En attente de consigne</th>
                                        <th>Chèque contrôle OK</th>
                                        <th>Chèque correction OK</th>
                                        <th>Chèque validation OK</th>
                                        <th>Chèque KD en cours</th>
                                        <th>Chèque KS en cours</th>
                                    </tr>
                                </thead>
                                <tbody>';
       
        foreach($encoursParTypologie as $liste)
        {
            
            $strTable .="<tr>";
            $strTable .="<td>".$liste->typologie."</td>";
            $strTable .="<td>".$liste->nb_mouvement."</td>";
            $strTable .="<td>".$liste->pli_en_cours_typage."</td>";
            $strTable .="<td>".$liste->pli_en_cours_saisie."</td>";
            $strTable .="<td>".$liste->en_attente_saisie."</td>";
            $strTable .="<td>".$liste->en_attente_controle."</td>";
            $strTable .="<td>".$liste->pli_en_cours_controle."</td>";
            $strTable .="<td>".$liste->pli_en_attente_consigne."</td>";
            $strTable .="<td>".$liste->chq_controle_ok."</td>";
            $strTable .="<td>".$liste->chq_correction_ok."</td>";
            $strTable .="<td>".$liste->chq_validation_ok."</td>";
            $strTable .="<td>".$liste->chq_kd_en_cours."</td>";
            $strTable .="<td>".$liste->chq_ks_en_cours."</td>";
            $strTable .="</tr>";
            
        }
        $strTable .="</tbody></table>";
        echo $strTable;

    }

}