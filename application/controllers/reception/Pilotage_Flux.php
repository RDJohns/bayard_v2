<?php

class Pilotage_Flux extends CI_Controller
{

    private $activeSheet;
    public function __construct()
    {
        parent::__construct();
        set_time_limit(7200);

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            redirect('login','refresh');
            exit();
        }
        $this->load->library('ra/Excel',null,'excel');
        $this->load->library('ra/Style_export',null,"StyleExport");
        $this->load->model("reception/Model_Pilotage_Flux", "MPilotage");
    }

     public function index()   
     {
        $this->viewPrincipale();
     }

    public function index2() /* https://stackoverflow.com/questions/49498097/phpexcel-template-load-and-saving-with-header */
    {
        $this->session->set_userdata('dateDebut', $_GET['debut']);
        $this->session->set_userdata('datefin', $_GET['fin']);

        $dateDebut            = $this->session->userdata('dateDebut');
        $datefin              = $this->session->userdata('datefin');
        $daty                 = "du ".$dateDebut." au ".$datefin;
        $fileType             = 'Excel5'; 
        
        $fileName             = 'depot/Export_unique.xls'; 
        $objReader            = PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel          = $objReader->load($fileName);
        $this->activeSheet    = $objPHPExcel->setActiveSheetIndex(0);
        
        $this->setValueCourrier();
        $this->setValueFlux(1);
        $this->setValueFlux(2);
        
        $this->activeSheet->setCellValue('B2',$daty);
        $this->activeSheet->freezePane('C8');

        $xlsName              = 'Pilotage_des_flux';          
        $objWriter            = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
        header("Content-Disposition: attachment;filename=$xlsName.xlsx"); 
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    public function setValueCourrier()
    {
        $PilotageCourrier = $this->MPilotage->setValueCourrier();
        if($PilotageCourrier)
        {
            $data = $PilotageCourrier[0];
            
            $this->activeSheet->setCellValue('C8',$data->pli_total);
            $this->activeSheet->setCellValue('D8', $data->stock_typage);
            $this->activeSheet->setCellValue('E8', $data->en_cours_typage);
            $this->activeSheet->setCellValue('F8', $data->typage_ok);
            $this->activeSheet->setCellValue('G8', $data->ko_scan+$data->hors_peri); /*Anomalie à traiter*/
            $this->activeSheet->setCellValue('I8', $data->stock_saisie);
            $this->activeSheet->setCellValue('J8', $data->en_cours_saisie);
            $this->activeSheet->setCellValue('K8', $data->saisie_ok);
            $this->activeSheet->setCellValue('L8', $data->saisie_ko);
            $this->activeSheet->setCellValue('M8', $data->saisie_ci);
            $this->activeSheet->setCellValue('N8', $data->stock_ctrl);
            $this->activeSheet->setCellValue('O8', $data->kd);
            $this->activeSheet->setCellValue('P8', $data->ks);
            $this->activeSheet->setCellValue('Q8', $data->ke);
            $this->activeSheet->setCellValue('R8', $data->ctrl_ok);
            $this->activeSheet->setCellValue('S8', $data->ctrl_ci);
            $this->activeSheet->setCellValue('T8', $data->reb);

            $this->setMouvementCourrier($data);
        }
       
    }

    public function setMouvementCourrier($data)
    {
        $this->activeSheet->setCellValue('C9',$data->nb_mouvement);
        $this->activeSheet->setCellValue('F9', $data->mvt_typage_ok);
        $this->activeSheet->setCellValue('I9', $data->mvt_stock_saisie);
        $this->activeSheet->setCellValue('J9', $data->mvt_en_cours_saisie);
        $this->activeSheet->setCellValue('K9', $data->mvt_saisie_ok);
        $this->activeSheet->setCellValue('L9', $data->mvt_saisie_ko);
        $this->activeSheet->setCellValue('M9', $data->mvt_saisie_ci);
        $this->activeSheet->setCellValue('N9', $data->mvt_stock_ctrl);
        $this->activeSheet->setCellValue('O9', $data->mvt_kd);
        $this->activeSheet->setCellValue('P9', $data->mvt_ks);
        $this->activeSheet->setCellValue('Q9', $data->mvt_ke);
        $this->activeSheet->setCellValue('R9', $data->mvt_ctrl_ok);
        $this->activeSheet->setCellValue('S9', $data->mvt_ctrl_ci);
            
    }
    public function setValueFlux($source)
    {
        $PilotageMail  = $this->MPilotage->setValueFlux($source);

        if($source == 1)
        {
            $rang = 10;
        }
        else
        {
            $rang = 12;
        }         

        if($PilotageMail)
        {
            $data     = $PilotageMail[0];

            $this->activeSheet->setCellValue('C'.$rang,$data->flux_recus);
            $this->activeSheet->setCellValue('D'.$rang, $data->stock_typage);
            $this->activeSheet->setCellValue('E'.$rang, $data->flux_en_cours_typage);
            $this->activeSheet->setCellValue('F'.$rang, $data->typage_ok);
            $this->activeSheet->setCellValue('H'.$rang, $data->rejete); /*status rejeté*/
            $this->activeSheet->setCellValue('I'.$rang, $data->stock_saisie);
            $this->activeSheet->setCellValue('J'.$rang, $data->en_cours_saisie);
            $this->activeSheet->setCellValue('K'.$rang, $data->saisie_ok);
            $this->activeSheet->setCellValue('Q'.$rang, $data->escalade);
            $this->setValueAbonnement($data,$rang);
        }
    }

    public function setValueAbonnement($data,$rang)
    {
            $aboRang = $rang+1;
            $this->activeSheet->setCellValue('C'.$aboRang,$data->nb_abonnement);
            $this->activeSheet->setCellValue('F'.$aboRang, $data->abonnement_typage_ok);
            $this->activeSheet->setCellValue('I'.$aboRang, $data->abonnement_stock_saisie);
            $this->activeSheet->setCellValue('J'.$aboRang, $data->abonnement_en_cours_saisie);
            $this->activeSheet->setCellValue('K'.$aboRang, $data->abonnement_saisie_ok);
            $this->activeSheet->setCellValue('Q'.$aboRang, $data->abonnement_escalade);
    }

    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Pilotage des flux";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "cloud_download";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            'css/custom/reception',
			'../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap'
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/custom/reception'
        );


        $this->load->view("main/header",$head);
         $this->load->view("reception/Filtre-pilotage-flux");
        $this->load->view("main/footer",$foot);
    }
}