﻿select 
count(id_flux) flux_recus, 
sum(stock_typage)stock_typage,
sum(flux_en_cours_typage)flux_en_cours_typage,

sum(typage_ok)typage_ok,
sum(mvt_typage_ok)mvt_typage_ok,

sum(flux_hors_peri)flux_hors_peri,
sum(flux_mail_anomalie)flux_mail_anomalie,
sum(flux_anomalie_pj)flux_anomalie_pj,
sum(flux_anomalie_fichier)flux_anomalie_fichier,
sum(stock_saisie)stock_saisie,

sum(en_cours_saisie)en_cours_saisie,
sum(mvt_en_cours_saisie)mvt_en_cours_saisie,

sum(escalade)escalade,
sum(mvt_escalade)mvt_escalade

 from (
select 
flux.id_flux,
CASE WHEN vto.statut_pli = 0 OR vto.statut_pli IS NULL THEN 1 ELSE 0  END AS stock_typage,
CASE WHEN vto.statut_pli = 1 THEN 1 ELSE 0 END AS flux_en_cours_typage,

CASE WHEN vto.statut_pli = 2 THEN 1 ELSE 0 END AS typage_ok,
CASE WHEN vto.statut_pli = 2 and flux.nb_abonnement is not null THEN flux.nb_abonnement ELSE 0 END AS mvt_typage_ok,

CASE WHEN vto.statut_pli = 13 THEN 1 ELSE 0 END AS flux_hors_peri, 
CASE WHEN vto.statut_pli = 11 THEN 1 ELSE 0 END AS flux_mail_anomalie,
CASE WHEN vto.statut_pli = 12 THEN 1 ELSE 0 END AS flux_anomalie_pj,
CASE WHEN vto.statut_pli = 14 THEN 1 ELSE 0 END AS flux_anomalie_fichier,
CASE WHEN vto.statut_pli = 2 THEN 1 ELSE 0 END AS stock_saisie,

CASE WHEN vto.statut_pli  in(3,5) THEN 1 ELSE 0 END AS en_cours_saisie,
CASE WHEN vto.statut_pli  in(3,5) and flux.nb_abonnement is not null THEN flux.nb_abonnement ELSE 0 END AS mvt_en_cours_saisie,

CASE WHEN vto.statut_pli = 4  THEN 1 ELSE 0 END AS escalade,
CASE WHEN vto.statut_pli = 4  and flux.nb_abonnement is not null THEN flux.nb_abonnement ELSE 0 END AS mvt_escalade

FROM flux
     LEFT JOIN view_traitement_oid vto ON vto.id_flux = flux.id_flux
     LEFT JOIN view_nb_abonnement vna ON vna.id_flux = flux.id_flux
     LEFT JOIN typologie ON typologie.id_typologie = flux.typologie
     LEFT JOIN societe ON societe.id = flux.societe
     LEFT JOIN source ON source.id_source = flux.id_source 
     where source.id_source = 1 and flux.date_reception between '2019-01-01' and '2019-10-21') as flux