<?php

class Pli_flux extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        set_time_limit(7200);

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }

        $this->load->model("reception/Model_Reception_Flux", "Mreception");
        $this->load->model("reception/Model_Liste_plis_flux", "Mliste");
    }

    public function index()
    {
        $this->session->set_userdata('infouser', " IP :". $this->input->ip_address()." LOGIN: ".(int)$this->session->userdata('id_utilisateur'));
        if((int)$_SESSION['id_type_utilisateur'] == 4 )
        {
            $this->viewPrincipale();

        }
        else{
            redirect(base_url());
        }
        
        
    }

    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Réception";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "equalizer";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'plugins/morrisjs/morris',
            'plugins/jquery-slimscroll/prettify',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            'css/custom/reception',
            '../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/raphael/raphael.min',
            'plugins/morrisjs/morris'
        );

        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            // 'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll_recep',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/popper/popper',
            'js/custom/recep-flux'
        );


        $this->load->view("main/header",$head);
        $this->load->view("reception/Filtre_recep");
        $this->load->view("reception/Suivi-typologie");
        $this->load->view("reception/Suivi-cloture");
        $this->load->view("reception/Suivi-en-cours");
        $this->load->view("reception/Restant-a-traiter");
        $this->load->view("main/footer",$foot);
    }
    public function pliParTypologie()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $receptionParTypo = $this->Mreception->pliParTypologie($date1,$date2,(int)$societe,(int)$source);
        echo json_encode($receptionParTypo);
        return "";

    }
    public function pliEncours()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $receptionParTypo = $this->Mreception->pliEncours($date1,$date2,(int)$societe,(int)$source);
        echo json_encode($receptionParTypo);
        return "";

    }
    public function pliCloture()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source    = $this->input->post("source");

        $receptionParTypo = $this->Mreception->pliCloture($date1,$date2,(int)$societe,(int)$source);
        echo json_encode($receptionParTypo);
        return "";

    }
    public function  typologieJournalier()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;


        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->typologieJournalier($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i = 0;

        foreach($res as $item)
        {
            $arrayTable[$i]["info_0"] = $item->info_0;
            $arrayTable[$i]["info_2"] = $item->info_2;
            $arrayTable[$i]["info_3"] = $item->info_3;
            $arrayTable[$i]["info_4"] = $item->info_4;
            $arrayTable[$i]["info_5"] = $item->info_5;
            $arrayTable[$i]["info_6"] = $item->info_6;
            $arrayTable[$i]["info_7"] = $item->info_7;
            $arrayTable[$i]["info_8"] = $item->info_8;
            $arrayTable[$i]["info_9"] = $item->info_9;
            $arrayTable[$i]["info_10"] = $item->info_10;
            $arrayTable[$i]["info_11"] = $item->info_11;
            $arrayTable[$i]["info_12"] = $item->info_12;
            $arrayTable[$i]["info_13"] = $item->info_13;
            $arrayTable[$i]["info_14"] = $item->info_14;
            $arrayTable[$i]["info_15"] = $item->info_15;
            $arrayTable[$i]["info_16"] = $item->info_16;
            $arrayTable[$i]["info_17"] = $item->info_17;
            $arrayTable[$i]["info_18"] = $item->info_18;
            $arrayTable[$i]["info_19"] = $item->info_19;
            $arrayTable[$i]["info_20"] = $item->info_20;
            $arrayTable[$i]["info_21"] = $item->info_21;
            if($source == '3'){
                $arrayTable[$i]["date_courrier"] = $item->date_courrier;
                $arrayTable[$i]["info_22"] = $item->info_22;
                $arrayTable[$i]["info_23"] = $item->info_23;
                $arrayTable[$i]["info_24"] = $item->info_24;
                $arrayTable[$i]["info_25"] = $item->info_25;
                $arrayTable[$i]["info_26"] = $item->info_26;
                $arrayTable[$i]["info_27"] = $item->info_27;
            }
            else{
                $arrayTable[$i]["date_reception"] = $item->date_reception;
                $arrayTable[$i]["info_1"] = $item->info_1;
            }

            $i++;

        }

        echo json_encode($arrayTable);


    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function nonTraiteEncours()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $reception = $this->Mreception->nonTraiteEncours($date1,$date2,(int)$societe,(int)$source);
        echo json_encode($reception);
        return "";
    }

    public function fluxAnomalie(){
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $flux_anomalie = $flux_ko_mail = $flux_ko_pj = $flux_ko_fichier = $flux_ko_src = $flux_ko_definitif = 0;
        $flux_ko_inconnu = $flux_ko_pj = $flux_ko_attente = $flux_ko_rejete = $flux_ko_hp = $flux_ko_en_cours = 0;

        $ttl_anomalie = $ttl_ko_mail = $ttl_ko_pj = $ttl_ko_fichier = $ttl_ko_src = $ttl_ko_definitif = 0;
        $ttl_ko_inconnu = $ttl_ko_pj = $ttl_ko_attente = $ttl_ko_rejete = $ttl_ko_hp = $ttl_ko_en_cours= 0;

        $perc_ko_mail = $perc_ko_fichier = $perc_ko_src = $perc_ko_definitif = 0;
        $perc_ko_inconnu = $perc_ko_pj = $perc_ko_attente = $perc_ko_rejete = $perc_ko_hp = $perc_ko_en_cours = 0;

        $ptl_ko_mail = $ptl_ko_fichier = $ptl_ko_src = $ptl_ko_definitif = 0;
        $ptl_ko_inconnu = $ptl_ko_pj = $ptl_ko_attente = $ptl_ko_rejete = $ptl_ko_hp = $ptl_ko_en_cours = 0;

        $recep_anomalie = $this->Mreception->fluxAnomalie($date1,$date2,(int)$societe,(int)$source);

        if($recep_anomalie){
            $anomalie = $recep_anomalie[0];

            if(intval($anomalie->flux_anomalie) > 0){
                $flux_anomalie = intval($anomalie->flux_anomalie);

                $flux_ko_mail = intval($anomalie->flux_ko_mail);
                $perc_ko_mail = round(($flux_ko_mail*100)/$flux_anomalie,2);

                $flux_ko_pj = intval($anomalie->flux_ko_pj);
                $perc_ko_pj = round(($flux_ko_pj*100)/$flux_anomalie,2);

                $flux_ko_fichier = intval($anomalie->flux_ko_fichier);
                $perc_ko_fichier = round(($flux_ko_fichier*100)/$flux_anomalie,2);

                $flux_ko_src = intval($anomalie->flux_ko_src);
                $perc_ko_src = round(($flux_ko_src*100)/$flux_anomalie,2);

                $flux_ko_definitif = intval($anomalie->flux_ko_definitif);
                $perc_ko_definitif = round(($flux_ko_definitif*100)/$flux_anomalie,2);

                $flux_ko_inconnu = intval($anomalie->flux_ko_inconnu);
                $perc_ko_inconnu = round(($flux_ko_inconnu*100)/$flux_anomalie,2);

                $flux_ko_attente = intval($anomalie->flux_ko_attente);
                $perc_ko_attente = round(($flux_ko_attente*100)/$flux_anomalie,2);

                $flux_ko_rejete = intval($anomalie->flux_ko_rejete);
                $perc_ko_rejete = round(($flux_ko_rejete*100)/$flux_anomalie,2);

                $flux_ko_hp = intval($anomalie->flux_ko_hp);
                $perc_ko_hp = round(($flux_ko_hp*100)/$flux_anomalie,2);

                $flux_ko_en_cours = intval($anomalie->flux_ko_en_cours);
                $perc_ko_en_cours = round(($flux_ko_en_cours*100)/$flux_anomalie,2);

            }

            if(intval($anomalie->total_anomalie) > 0){
                $ttl_anomalie = intval($anomalie->total_anomalie);

                $ttl_ko_mail = intval($anomalie->total_ko_mail);
                $ptl_ko_mail = round(($ttl_ko_mail*100)/$ttl_anomalie,2);

                $ttl_ko_pj = intval($anomalie->total_ko_pj);
                $ptl_ko_pj = round(($ttl_ko_pj*100)/$ttl_anomalie,2);

                $ttl_ko_fichier = intval($anomalie->total_ko_fichier);
                $ptl_ko_fichier = round(($ttl_ko_fichier*100)/$ttl_anomalie,2);

                $ttl_ko_src = intval($anomalie->total_ko_src);
                $ptl_ko_src = round(($ttl_ko_src*100)/$ttl_anomalie,2);

                $ttl_ko_definitif = intval($anomalie->total_ko_definitif);
                $ptl_ko_definitif = round(($ttl_ko_definitif*100)/$ttl_anomalie,2);

                $ttl_ko_inconnu = intval($anomalie->total_ko_inconnu);
                $ptl_ko_inconnu = round(($ttl_ko_inconnu*100)/$ttl_anomalie,2);

                $ttl_ko_attente = intval($anomalie->total_ko_attente);
                $ptl_ko_attente = round(($ttl_ko_attente*100)/$ttl_anomalie,2);

                $ttl_ko_rejete = intval($anomalie->total_ko_rejete);
                $ptl_ko_rejete = round(($ttl_ko_rejete*100)/$ttl_anomalie,2);

                $ttl_ko_hp = intval($anomalie->total_ko_hp);
                $ptl_ko_hp = round(($ttl_ko_hp*100)/$ttl_anomalie,2);

                $ttl_ko_en_cours = intval($anomalie->total_ko_en_cours);
                $ptl_ko_en_cours = round(($ttl_ko_en_cours*100)/$ttl_anomalie,2);

            }
        }

        $tab_anomalie = array(
            'flux_anomalie'     => $flux_anomalie,
            'flux_ko_mail'      => $flux_ko_mail,
            'flux_ko_pj'        => $flux_ko_pj,
            'flux_ko_fichier'   => $flux_ko_fichier,
            'flux_ko_src'       => $flux_ko_src,
            'flux_ko_definitif' => $flux_ko_definitif,
            'flux_ko_inconnu'   => $flux_ko_inconnu,
            'flux_ko_attente'   => $flux_ko_attente,
            'flux_ko_rejete'    => $flux_ko_rejete,
            'flux_ko_hp'        => $flux_ko_hp,
            'flux_ko_en_cours'  => $flux_ko_en_cours,
            'perc_ko_mail'      => $perc_ko_mail,
            'perc_ko_pj'        => $perc_ko_pj,
            'perc_ko_fichier'   => $perc_ko_fichier,
            'perc_ko_src'       => $perc_ko_src,
            'perc_ko_definitif' => $perc_ko_definitif,
            'perc_ko_inconnu'   => $perc_ko_inconnu,
            'perc_ko_attente'   => $perc_ko_attente,
            'perc_ko_rejete'    => $perc_ko_rejete,
            'perc_ko_hp'        => $perc_ko_hp,
            'perc_ko_en_cours' => $perc_ko_en_cours,
            'ttl_anomalie'     => $ttl_anomalie,
            'ttl_ko_mail'      => $ttl_ko_mail,
            'ttl_ko_pj'        => $ttl_ko_pj,
            'ttl_ko_fichier'   => $ttl_ko_fichier,
            'ttl_ko_src'       => $ttl_ko_src,
            'ttl_ko_definitif' => $ttl_ko_definitif,
            'ttl_ko_inconnu'   => $ttl_ko_inconnu,
            'ttl_ko_attente'   => $ttl_ko_attente,
            'ttl_ko_rejete'    => $ttl_ko_rejete,
            'ttl_ko_hp'        => $ttl_ko_hp,
            'ttl_ko_en_cours'  => $ttl_ko_en_cours,
            'ptl_ko_mail'      => $ptl_ko_mail,
            'ptl_ko_pj'        => $ptl_ko_pj,
            'ptl_ko_fichier'   => $ptl_ko_fichier,
            'ptl_ko_src'       => $ptl_ko_src,
            'ptl_ko_definitif' => $ptl_ko_definitif,
            'ptl_ko_inconnu'   => $ptl_ko_inconnu,
            'ptl_ko_attente'   => $ptl_ko_attente,
            'ptl_ko_rejete'    => $ptl_ko_rejete,
            'ptl_ko_hp'        => $ptl_ko_hp,
            'ptl_ko_en_cours'  => $ptl_ko_en_cours
        );

        echo json_encode($tab_anomalie);
        return "";
    }

    public function fluxAnomalieJour(){
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $flux_anomalie = $flux_ko_mail = $flux_ko_pj = $flux_ko_fichier = $flux_ko_src = $flux_ko_definitif = 0;
        $flux_ko_inconnu = $flux_ko_pj = $flux_ko_attente = $flux_ko_rejete = $flux_ko_hp = 0;

        $ttl_anomalie = $ttl_ko_mail = $ttl_ko_pj = $ttl_ko_fichier = $ttl_ko_src = $ttl_ko_definitif = 0;
        $ttl_ko_inconnu = $ttl_ko_pj = $ttl_ko_attente = $ttl_ko_rejete = $ttl_ko_hp = 0;

        $recep_anomalie = $this->Mreception->fluxAnomalieJour($date1,$date2,(int)$societe,(int)$source);

        $tab_ano = '';
        if($recep_anomalie){
            foreach ($recep_anomalie as $anomalie){
                $tab_ano .= '<tr>';
                $tab_ano .= '<td>'.$anomalie->date_reception.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_mail.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_pj.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_fichier.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_src.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_definitif.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_inconnu.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_attente.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_rejete.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_hp.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_mail.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_pj.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_fichier.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_src.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_definitif.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_inconnu.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_attente.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_rejete.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_hp.'</td>';
                $tab_ano .= '</tr>';
            }
        }

        echo $tab_ano;
    }

    public function fluxAnomalieSemaine(){
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $flux_anomalie = $flux_ko_mail = $flux_ko_pj = $flux_ko_fichier = $flux_ko_src = $flux_ko_definitif = 0;
        $flux_ko_inconnu = $flux_ko_pj = $flux_ko_attente = $flux_ko_rejete = $flux_ko_hp = 0;

        $ttl_anomalie = $ttl_ko_mail = $ttl_ko_pj = $ttl_ko_fichier = $ttl_ko_src = $ttl_ko_definitif = 0;
        $ttl_ko_inconnu = $ttl_ko_pj = $ttl_ko_attente = $ttl_ko_rejete = $ttl_ko_hp = 0;

        $recep_anomalie = $this->Mreception->fluxAnomalieSemaine($date1,$date2,(int)$societe,(int)$source);

        $tab_ano = '';
        if($recep_anomalie){
            foreach ($recep_anomalie as $anomalie){
                $tab_ano .= '<tr>';
                $tab_ano .= '<td>S'.$anomalie->semaine.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_mail.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_pj.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_fichier.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_src.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_definitif.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_inconnu.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_attente.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_rejete.'</td>';
                $tab_ano .= '<td>'.$anomalie->flux_ko_hp.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_mail.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_pj.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_fichier.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_src.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_definitif.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_inconnu.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_attente.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_rejete.'</td>';
                $tab_ano .= '<td>'.$anomalie->total_ko_hp.'</td>';
                $tab_ano .= '</tr>';
            }
        }

        echo $tab_ano;
    }

    public function  typologieSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->typologieSemaine($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i = 0;

        foreach($res as $item)
        {

            $arrayTable[$i]["info_0"] = $item->info_0;
            $arrayTable[$i]["info_2"] = $item->info_2;
            $arrayTable[$i]["info_3"] = $item->info_3;
            $arrayTable[$i]["info_4"] = $item->info_4;
            $arrayTable[$i]["info_5"] = $item->info_5;
            $arrayTable[$i]["info_6"] = $item->info_6;
            $arrayTable[$i]["info_7"] = $item->info_7;
            $arrayTable[$i]["info_8"] = $item->info_8;
            $arrayTable[$i]["info_9"] = $item->info_9;
            $arrayTable[$i]["info_10"] = $item->info_10;
            $arrayTable[$i]["info_11"] = $item->info_11;
            $arrayTable[$i]["info_12"] = $item->info_12;
            $arrayTable[$i]["info_13"] = $item->info_13;
            $arrayTable[$i]["info_14"] = $item->info_14;
            $arrayTable[$i]["info_15"] = $item->info_15;
            $arrayTable[$i]["info_16"] = $item->info_16;
            $arrayTable[$i]["info_17"] = $item->info_17;
            $arrayTable[$i]["info_18"] = $item->info_18;
            $arrayTable[$i]["info_19"] = $item->info_19;
            $arrayTable[$i]["info_20"] = $item->info_20;
            $arrayTable[$i]["info_21"] = $item->info_21;
            if($source == '3'){
                $arrayTable[$i]["date_courrier"] = $item->semaine;
                $arrayTable[$i]["info_22"] = $item->info_22;
                $arrayTable[$i]["info_23"] = $item->info_23;
                $arrayTable[$i]["info_24"] = $item->info_24;
                $arrayTable[$i]["info_25"] = $item->info_25;
                $arrayTable[$i]["info_26"] = $item->info_26;
                $arrayTable[$i]["info_27"] = $item->info_27;
            }
            else{
                $arrayTable[$i]["date_reception"] = $item->semaine;
                $arrayTable[$i]["info_1"] = $item->info_1;
            }

            $i++;

        }

        echo json_encode($arrayTable);


    }

    public function  cloJournalier()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->cloJournalier($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i = 0;

        foreach($res as $item)
        {

            $arrayTable[$i]["info_0"] = $item->info_0;
            $arrayTable[$i]["info_2"] = $item->info_2;
            $arrayTable[$i]["info_3"] = $item->info_3;
            $arrayTable[$i]["info_4"] = $item->info_4;
            $arrayTable[$i]["info_5"] = $item->info_5;
            $arrayTable[$i]["info_6"] = $item->info_6;
            $arrayTable[$i]["info_7"] = $item->info_7;
            $arrayTable[$i]["info_8"] = $item->info_8;
            $arrayTable[$i]["info_9"] = $item->info_9;
            $arrayTable[$i]["info_10"] = $item->info_10;
            $arrayTable[$i]["info_11"] = $item->info_11;
            $arrayTable[$i]["info_12"] = $item->info_12;
            $arrayTable[$i]["info_13"] = $item->info_13;
            $arrayTable[$i]["info_14"] = $item->info_14;
            $arrayTable[$i]["info_15"] = $item->info_15;
            $arrayTable[$i]["info_16"] = $item->info_16;
            $arrayTable[$i]["info_17"] = $item->info_17;
            $arrayTable[$i]["info_18"] = $item->info_18;
            $arrayTable[$i]["info_19"] = $item->info_19;
            $arrayTable[$i]["info_20"] = $item->info_20;
            $arrayTable[$i]["info_21"] = $item->info_21;
            if($source == '3'){
                $arrayTable[$i]["date_courrier"] = $item->date_courrier;
                $arrayTable[$i]["info_22"] = $item->info_22;
                $arrayTable[$i]["info_23"] = $item->info_23;
                $arrayTable[$i]["info_24"] = $item->info_24;
                $arrayTable[$i]["info_25"] = $item->info_25;
                $arrayTable[$i]["info_26"] = $item->info_26;
                $arrayTable[$i]["info_27"] = $item->info_27;
            }
            else{
                $arrayTable[$i]["date_reception"] = $item->date_reception;
                $arrayTable[$i]["info_1"] = $item->info_1;
            }

            $i++;

        }

        echo json_encode($arrayTable);


    }

    public function  cloTypologieSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->cloTypologieSemaine($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i = 0;

        foreach($res as $item)
        {

            $arrayTable[$i]["info_0"] = $item->info_0;
            $arrayTable[$i]["info_2"] = $item->info_2;
            $arrayTable[$i]["info_3"] = $item->info_3;
            $arrayTable[$i]["info_4"] = $item->info_4;
            $arrayTable[$i]["info_5"] = $item->info_5;
            $arrayTable[$i]["info_6"] = $item->info_6;
            $arrayTable[$i]["info_7"] = $item->info_7;
            $arrayTable[$i]["info_8"] = $item->info_8;
            $arrayTable[$i]["info_9"] = $item->info_9;
            $arrayTable[$i]["info_10"] = $item->info_10;
            $arrayTable[$i]["info_11"] = $item->info_11;
            $arrayTable[$i]["info_12"] = $item->info_12;
            $arrayTable[$i]["info_13"] = $item->info_13;
            $arrayTable[$i]["info_14"] = $item->info_14;
            $arrayTable[$i]["info_15"] = $item->info_15;
            $arrayTable[$i]["info_16"] = $item->info_16;
            $arrayTable[$i]["info_17"] = $item->info_17;
            $arrayTable[$i]["info_18"] = $item->info_18;
            $arrayTable[$i]["info_19"] = $item->info_19;
            $arrayTable[$i]["info_20"] = $item->info_20;
            $arrayTable[$i]["info_21"] = $item->info_21;
            if($source == '3'){
                $arrayTable[$i]["date_courrier"] = $item->semaine;
                $arrayTable[$i]["info_22"] = $item->info_22;
                $arrayTable[$i]["info_23"] = $item->info_23;
                $arrayTable[$i]["info_24"] = $item->info_24;
                $arrayTable[$i]["info_25"] = $item->info_25;
                $arrayTable[$i]["info_26"] = $item->info_26;
                $arrayTable[$i]["info_27"] = $item->info_27;
            }
            else{
                $arrayTable[$i]["date_reception"] = $item->semaine;
                $arrayTable[$i]["info_1"] = $item->info_1;
            }

            $i++;

        }

        echo json_encode($arrayTable);


    }


    public function  encoursParSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->parSemaineEncours($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i          = 0;

        foreach($res as $item)
        {
            if($source == '3'){
                $arrayTable[$i]["date_courrier"]            = "S".$item->semaine;
                $arrayTable[$i]["en_cours_typage"]          = $item->en_cours_typage;
                $arrayTable[$i]["en_attente_saisie"]        = $item->en_attente_saisie;
                $arrayTable[$i]["en_cours_saisie"]          = $item->en_cours_saisie;
                $arrayTable[$i]["en_attente_controle"]      = $item->en_attente_controle;
                $arrayTable[$i]["pli_en_cours_controle"]    = $item->pli_en_cours_controle;
                $arrayTable[$i]["en_attente_consigne"]      = $item->en_attente_consigne;
                $arrayTable[$i]["chq_controle_ok"]          = $item->chq_controle_ok;
                $arrayTable[$i]["chq_correction_ok"]        = $item->chq_correction_ok;
                $arrayTable[$i]["chq_validation_ok"]        = $item->chq_validation_ok;
            }
            else{
                $arrayTable[$i]["date_reception"]           = "S".$item->semaine;
                $arrayTable[$i]["en_cours_typage"]          = $item->en_cours_typage;
                $arrayTable[$i]["flux_type"]                = $item->flux_type;
                $arrayTable[$i]["en_cours_saisie_nv1"]      = $item->en_cours_saisie_nv1;
                $arrayTable[$i]["flux_escalade"]            = $item->flux_escalade;
                $arrayTable[$i]["en_cours_saisie_nv2"]      = $item->en_cours_saisie_nv2;
                $arrayTable[$i]["flux_trans_cons"]          = $item->flux_trans_cons;
                $arrayTable[$i]["flux_recep_cons"]          = $item->flux_recep_cons;
                $arrayTable[$i]["flux_en_cours_retraite"]   = $item->flux_en_cours_retraite;
            }

            $i++;
        }

        echo json_encode($arrayTable);


    }

    public function  encoursParJour()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->detailEncours($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            if($source == '3'){
                $arrayTable[$i]["date_courrier"]            = $item->date_courrier;
                $arrayTable[$i]["en_cours_typage"]          = $item->en_cours_typage;
                $arrayTable[$i]["en_attente_saisie"]        = $item->en_attente_saisie;
                $arrayTable[$i]["en_cours_saisie"]          = $item->en_cours_saisie;
                $arrayTable[$i]["en_attente_controle"]      = $item->en_attente_controle;
                $arrayTable[$i]["pli_en_cours_controle"]    = $item->pli_en_cours_controle;
                $arrayTable[$i]["en_attente_consigne"]      = $item->en_attente_consigne;
                $arrayTable[$i]["chq_controle_ok"]          = $item->chq_controle_ok;
                $arrayTable[$i]["chq_correction_ok"]        = $item->chq_correction_ok;
                $arrayTable[$i]["chq_validation_ok"]        = $item->chq_validation_ok;
            }
            else{
                $arrayTable[$i]["date_reception"]           = $item->date_reception;
                $arrayTable[$i]["en_cours_typage"]          = $item->en_cours_typage;
                $arrayTable[$i]["flux_type"]                = $item->flux_type;
                $arrayTable[$i]["en_cours_saisie_nv1"]      = $item->en_cours_saisie_nv1;
                $arrayTable[$i]["flux_escalade"]            = $item->flux_escalade;
                $arrayTable[$i]["en_cours_saisie_nv2"]      = $item->en_cours_saisie_nv2;
                $arrayTable[$i]["flux_trans_cons"]          = $item->flux_trans_cons;
                $arrayTable[$i]["flux_recep_cons"]          = $item->flux_recep_cons;
                $arrayTable[$i]["flux_en_cours_retraite"]   = $item->flux_en_cours_retraite;
            }

            $i++;
        }

        echo json_encode($arrayTable);


    }
    public function  nonTraiteSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->nonTraiteSemaine($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            if($source == '3'){
                $arrayTable[$i]["date_courrier"]         = $item->semaine;
                $arrayTable[$i]["pli_en_cours"]          = $item->pli_en_cours;
                $arrayTable[$i]["pli_non_traite"]        = $item->pli_non_traite;
            }
            else{
                $arrayTable[$i]["date_reception"]        = $item->semaine;
                $arrayTable[$i]["flux_en_cours"]         = $item->flux_en_cours;
                $arrayTable[$i]["flux_non_traite"]       = $item->flux_non_traite;
            }

            $i++;
        }

        echo json_encode($arrayTable);


    }
    public function  nonTraiteJour()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->nonTraiteJour($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            if($source == '3'){
                $arrayTable[$i]["date_courrier"]         = $item->date_courrier;
                $arrayTable[$i]["pli_en_cours"]          = $item->pli_en_cours;
                $arrayTable[$i]["pli_non_traite"]        = $item->pli_non_traite;
            }
            else{
                $arrayTable[$i]["date_reception"]        = $item->date_reception;
                $arrayTable[$i]["flux_en_cours"]         = $item->flux_en_cours;
                $arrayTable[$i]["flux_non_traite"]       = $item->flux_non_traite;
            }

            $i++;
        }

        echo json_encode($arrayTable);


    }
    public function  syntheseSemaine()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->syntheseSemaine($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            if($source == '3'){
                $arrayTable[$i]["date_courrier"]       = "S".$item->date_courrier;
                $arrayTable[$i]["nb_pli_recus"]        = $item->nb_pli_recus;
            }
            else{
                $arrayTable[$i]["date_reception"]      = "S".$item->semaine;
                $arrayTable[$i]["nb_flux_recus"]       = $item->nb_flux_recus;
            }
            $arrayTable[$i]["dernier_traitement"]   = $item->dernier_traitement;
            $arrayTable[$i]["non_traite"]           = $item->non_traite;
            $arrayTable[$i]["nb_cloture"]           = $item->nb_cloture;
            $arrayTable[$i]["nb_encours"]           = $item->nb_encours;

            $i++;
        }

        echo json_encode($arrayTable);


    }
    public function  syntheseJournaliere()
    {
        $entete     = array();
        $ligne1     = array();
        $touteTypo  = array();
        $iTemp      = 0;

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $res        = $this->Mreception->syntheseJournaliere($date1,$date2,(int)$societe,(int)$source);

        $arrayTable = array();
        $i          = 0;
        //var_dump($res);
        foreach($res as $item)
        {
            if($source == '3'){
                $arrayTable[$i]["date_courrier"]       = $item->date_courrier;
                $arrayTable[$i]["nb_pli_recus"]        = $item->nb_pli_recus;
            }
            else{
                $arrayTable[$i]["date_reception"]      = $item->date_reception;
                $arrayTable[$i]["nb_flux_recus"]       = $item->nb_flux_recus;
            }
            $arrayTable[$i]["dernier_traitement"]   = $item->dernier_traitement;
            $arrayTable[$i]["non_traite"]           = $item->non_traite;
            $arrayTable[$i]["nb_cloture"]           = $item->nb_cloture;
            $arrayTable[$i]["nb_encours"]           = $item->nb_encours;

            $i++;
        }

        echo json_encode($arrayTable);

    }

    public function getListePli()
    {
        $output             = array();
        $date1              = $this->input->post("dateDebut");
        $date2              = $this->input->post("dateFin");
        $societe            = $this->input->post("societe");
        $source             = $this->input->post("source");
        $listePliReception  = $this->Mliste->getListePli($date1,$date2,(int)$societe,(int)$source);
        $recordsTotal       = $this->Mliste->recordsTotal($date1,$date2,(int)$societe,(int)$source);
        $recordsFiltered    = $this->Mliste->recordsFiltered($date1,$date2,(int)$societe,(int)$source);
        $output             = $this->fillListePli($listePliReception,$recordsTotal,$recordsFiltered,$source);


        echo json_encode($output);
    }


    public function  fillListePli($obj,$recordsTotal,$recordsFiltered,$source)
    {
        $data  = array();

        if($source == 3){
            foreach ($obj as $liste)
            {
                $row = array();
                $row[] = $liste->date_courrier;
                $row[] =  $liste->date_numerisation;
                $row[] =  $liste->dt_event;
                $row[] =  $liste->lot_scan;
                $row[] =  $liste->pli;
                $row[] =  $liste->typologie;
                $row[] =  $liste->statut;
                $row[] =  $liste->nb_mouvement;
                $row[] =  $liste->id_lot_saisie;
                $row[] =  $liste->modes_paiements;
                $row[] =  $liste->id_pli;

                $data[] = $row;
            }
            $output = array(
                "draw"            => $_POST['draw'],
                "recordsTotal"    => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "data"            => $data,
            );

            return $output;
        }
        else{
            foreach ($obj as $liste)
            {
                $row = array();
                $row[] = $liste->date_reception;
                $row[] =  $liste->date_traitement;
                $row[] =  $liste->typologie;
                $row[] =  $liste->statut;
                $row[] =  $liste->etat;
                $row[] =  $liste->nb_abonnement;
                $row[] =  $liste->id_flux;

                $data[] = $row;
            }
            $output = array(
                "draw"            => $_POST['draw'],
                "recordsTotal"    => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "data"            => $data,
            );

            return $output;
        }
    }

    public function globaleReception()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");

        $output     = $this->Mreception->globaleReception($date1,$date2,(int)$societe,(int)$source);
        echo json_encode($output);
        return "";
    }

    public function verifDonnees()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        $source     = $this->input->post("source");
        $output     = $this->Mreception->verifDonnees($date1,$date2,(int)$societe,(int)$source);
        echo json_encode($output); return "";
    }

    public function exportListePlis()
    {

        $strTable = "";

        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");

        $listePliReception  = $this->Mliste->exportListePli($date1,$date2,(int)$societe);


        $bordure = ' style="border:1px solid #cecece;" ';
        $bordure_text = ' style="border:1px solid;text-align: left;';

        $strTable   .= "<table><thead><tr>";
        $strTable   .= "<th".$bordure.">Date courrier</th>";
        $strTable   .= "<th".$bordure.">Date numérisation</th>";
        $strTable   .= "<th".$bordure.">Dernier traitement</th>";
        $strTable   .= "<th".$bordure.">Ref. lot scan</th>";
        $strTable   .= "<th".$bordure.">Pli</th>";
        $strTable   .= "<th".$bordure.">Typologie</th>";
        $strTable   .= "<th".$bordure.">Statut</th>";
        $strTable   .= "<th".$bordure.">Nbr. mouvement</th>";
        $strTable   .= "<th".$bordure.">ID lot de saisie</th>";
        $strTable   .= "<th".$bordure.">Moyen de paiement</th>";
        $strTable   .= "<th".$bordure.">ID pli</th></tr>";
        $strTable   .=" </thead><tbody>";
        $i = 1;
        foreach($listePliReception as $item)
        {
            $strTable .= "<tr>";
            $strTable .= "<td style='border:1px solid ; text-align: left;' >".$item->date_courrier."</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->date_numerisation."</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->dt_event."</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;' >".$item->lot_scan."</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;' >'".$item->pli."'</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->typologie."</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->statut."</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->nb_mouvement."</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->id_lot_saisie."</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->modes_paiements."</td>";
            $strTable .= "<td style='border:1px solid;text-align: left;'>".$item->id_pli."</td>";

            $strTable .= "</tr>";
            $i++;
        }

        $strTable .= "</tbody></table>";
        echo $strTable;

    }

    public  function encoursParTypologie()
    {
        $date1                  = $this->input->post("dateDebut");
        $date2                  = $this->input->post("dateFin");
        $societe                = $this->input->post("societe");
        $source                 = $this->input->post("source");

        $encoursParTypologie    = $this->Mreception->encoursParTypologie($date1,$date2,(int)$societe,(int)$source);
        if($source == '3'){
            $strTable               = ' <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="encours-par-typo-table">
                                    <thead>
                                        <tr>
                                            <th>Typologie</th>
                                            <th>Nbr. Mvt</th>
                                            <th>En cours typage</th>
                                            <th>En cours saisie</th>
                                            <th>En attente saisie</th>
                                            <th>En attente contrôle</th>
                                            <th>En cours de contrôle</th>
                                            <th>En attente de consigne</th>
                                            <th>Chèque contrôle OK</th>
                                            <th>Chèque correction OK</th>
                                            <th>Chèque validation OK</th>
                                            <th>Chèque KD en cours</th>
                                            <th>Chèque KS en cours</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            foreach($encoursParTypologie as $liste)
            {

                $strTable .="<tr>";
                $strTable .="<td>".$liste->typologie."</td>";
                $strTable .="<td>".$liste->nb_mouvement."</td>";
                $strTable .="<td>".$liste->pli_en_cours_typage."</td>";
                $strTable .="<td>".$liste->pli_en_cours_saisie."</td>";
                $strTable .="<td>".$liste->en_attente_saisie."</td>";
                $strTable .="<td>".$liste->en_attente_controle."</td>";
                $strTable .="<td>".$liste->pli_en_cours_controle."</td>";
                $strTable .="<td>".$liste->pli_en_attente_consigne."</td>";
                $strTable .="<td>".$liste->chq_controle_ok."</td>";
                $strTable .="<td>".$liste->chq_correction_ok."</td>";
                $strTable .="<td>".$liste->chq_validation_ok."</td>";
                $strTable .="<td>".$liste->chq_kd_en_cours."</td>";
                $strTable .="<td>".$liste->chq_ks_en_cours."</td>";
                $strTable .="</tr>";

            }
            $strTable .="</tbody></table>";
        }
        else{
            $strTable               = ' <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="encours-par-typo-table">
                                    <thead>
                                        <tr>
                                            <th>Typologie</th>
                                            <th>Nbr. Abnmt</th>
                                            <th>En cours de typage</th>
                                            <th>Flux typé</th>
                                            <th>En cours de saisie nv1</th>
                                            <th>Flux en escalade</th>
                                            <th>En cours de saisie nv2</th>
                                            <th>Flux en transfert consignes</th>
                                            <th>Flux en réception consignes</th>
                                            <th>En cours de retraitement</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            foreach($encoursParTypologie as $liste)
            {

                $strTable .="<tr>";
                $strTable .="<td>".$liste->typologie."</td>";
                $strTable .="<td>".$liste->nb_abonnement."</td>";
                $strTable .="<td>".$liste->flux_en_cours_typage."</td>";
                $strTable .="<td>".$liste->flux_type."</td>";
                $strTable .="<td>".$liste->en_cours_saisie_nv1."</td>";
                $strTable .="<td>".$liste->flux_escalade."</td>";
                $strTable .="<td>".$liste->en_cours_saisie_nv2."</td>";
                $strTable .="<td>".$liste->flux_trans_cons."</td>";
                $strTable .="<td>".$liste->flux_recep_cons."</td>";
                $strTable .="<td>".$liste->flux_en_cours_retraite."</td>";
                $strTable .="</tr>";

            }
            $strTable .="</tbody></table>";
        }
        echo $strTable;

    }

    public function pliFermer()
    {
        $date1      = $this->input->post("dateDebut");
        $date2      = $this->input->post("dateFin");
        $societe    = $this->input->post("societe");
        
        $pliFermer = $this->Mreception->pliFermer($date1,$date2,$societe);
        echo  json_encode($pliFermer);
        return "";

    }

}