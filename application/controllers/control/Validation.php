<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Validation extends CI_Controller{

	public function index(){
		$this->load->library('portier');
		$this->portier->must_calit();
		$this->go();
	}
	
	private function go(){
		$this->load->library('control/page');
		$this->page->set_titre('Contr&ocirc;le validation de pli');
		$this->page->add_links('Contr&ocirc;le', site_url('control/control'));
		$this->page->add_links('Retraitement', site_url('control/retraitement'));
		$this->page->add_js('control/main');
		$this->page->add_js('control/validation');
		$this->page->add_css('control/main');
		$this->page->add_css('control/validation');
		$this->page->set_my_url('/control/validation');
		$this->load->model('control/model_pli_validation');
		$data_init = array(
			'lbl_bouton' => 'Recharger un pli pour validation'
			,'traitements' => $this->model_pli_validation->choix_init()
			,'trtmt_valid' => $this->session->has_userdata('trtmt_valid') ? $this->session->trtmt_valid : '-1'
		);
		$this->page->afficher($this->load->view('control/validation/initial', $data_init, TRUE));
	}

	public function get_pli(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v();
		$this->portier->must_calit();
		$trtmt_valid = isset($_REQUEST['trtmt_valid']) ? $this->input->post_get('trtmt_valid') : '-1';
		$this->session->trtmt_valid = $trtmt_valid;
		$this->load->model('control/model_pli_validation');
		$pli = $this->model_pli_validation->piocher_pli($this->session->id_utilisateur, $trtmt_valid);
		if (is_null($pli)) {
			$data_init = array(
				'message' => $trtmt_valid == '-1' ? 'Aucun pli disponible pour validation!' : 'Aucun pli &agrave; valider pour ce traitement!'
				,'lbl_bouton' => 'Charger un pli pour validation'
				,'traitements' => $this->model_pli_validation->choix_init()
				,'trtmt_valid' => $trtmt_valid
			);
			echo $this->load->view('control/no_pli', $data_init, TRUE);
		}else{
			$this->histo->pli($pli->id_pli);
			$this->saisie_pli($pli);
		}
	}
	
	private function saisie_pli($pli){
		$this->load->library('control/data');
		$data_view_pli = $this->data->get_data($pli);
		$data_view_pli['statut_ctrl'] = $this->model_pli->statut_control();
		echo $this->load->view('control/validation/pli', $data_view_pli, TRUE);
	}
	
	public function cancel($id_pli=NULL){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_calit();
		if(is_numeric($id_pli)){
			$this->load->model('control/model_pli_validation');
			if($this->model_pli_validation->unlocked($id_pli)){
				return '';
			}
		}else {
			log_message('error', 'control> validation> cancel_validation> id_pli non numerique: '.$id_pli);
		}
	}
	
	public function save(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_calit();
		if(isset($_REQUEST['id_pli'])){
			$this->load->library('control/data_save');
			if($this->data_save->initialize()){
				switch ($this->data_save->statut) {
					case '1':
						$flag_traitement = 15;
						$this->data_save->data_pli['motif_ko'] = NULL;
						$this->data_save->data_pli['autre_motif_ko'] = NULL;
						$action_ctrl = 34;
						break;
					case '3':
						$flag_traitement = 25;
						$this->data_save->data_pli['motif_ko'] = NULL;
						$this->data_save->data_pli['autre_motif_ko'] = NULL;
						$action_ctrl = 35;
						break;
					case '4':
						$flag_traitement = 18;
						$action_ctrl = 36;
						break;
					case '5':
						$flag_traitement = 19;
						$action_ctrl = 37;
						break;
					case '6':
						$flag_traitement = 20;
						$action_ctrl = 38;
						break;
					default:
						echo 'Argument[statut] non conforme!';
						log_message('error', 'control> validation> save> unexpected status='.$this->data_save->statut.', op#'.$this->session->id_utilisateur.', pli#'.$this->data_save->id_pli);
						return '';
						break;
				}
				$this->data_save->pli['flag_traitement'] = $flag_traitement;
				$this->data_save->is_control = TRUE;
				$this->data_save->old_flg = 12;
				$this->load->model('control/model_pli');
				$rep_save = $this->model_pli->save($this->data_save);
				if($rep_save[0]){
					echo '1';
					$this->histo->action($action_ctrl, '', $this->data_save->id_pli);
					$this->histo->pli($this->data_save->id_pli);
					$this->load->library('saisie/data_cba');
					$this->data_cba->set_data($this->data_save);
				}else{
					echo $rep_save[1];
					log_message('error', 'control> validation> save> Enregistrement échoué:'.$rep_save[1].', pli#'.$this->data_save->id_pli.', op#'.$this->session->id_utilisateur);
				}
			}else{
				echo $this->data_save->output;
			}
		}else{
			echo 'Paramètre manquant';
			log_message('error', 'control> validation> save> parametre manquant op#'.$this->session->id_utilisateur);
		}
	}

}
