<?php

class Echantillonnage extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("control/Model_echantillonnage", "Mechantillon");
    }

    public function index()
    {
        $this->echantillonner();
    }

    public function echantillonner()
    {
        if(!empty($this->Mechantillon->get_pli_to_cut_crga()) || !empty($this->Mechantillon->get_pli_to_cut_autres())){
            //if($this->Mechantillon->verif_heure()){
                if(!empty($this->Mechantillon->get_pli_to_cut_crga())){
                    $upd = $this->Mechantillon->update_decoupage_in_pli(1);
                    if($upd)
                        log_message('error', 'Erreur lors du découpage : '.$upd);
                }
                if(!empty($this->Mechantillon->get_pli_to_cut_autres())){
                    $upd = $this->Mechantillon->update_decoupage_in_pli(3);
                    if($upd)
                        log_message('error', 'Erreur lors du découpage : '.$upd);
                }
                echo "Echantillonnage terminé".PHP_EOL;
            //}
        }
    }

}