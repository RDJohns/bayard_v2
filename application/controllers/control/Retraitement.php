<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Retraitement extends CI_Controller{

	public function index(){
		$this->load->library('portier');
		$this->portier->must_calit();
		$this->go();
	}
	
	private function go(){
		$this->load->library('control/page');
		$this->page->set_titre('Retraitement de pli');
		$this->page->add_links('Contr&ocirc;le', site_url('control/control'));
		// $this->page->add_links('Validation', site_url('control/validation'));
		$this->page->add_js('control/main');
		$this->page->add_js('control/retraitement');
		$this->page->add_css('control/main');
		$this->page->add_css('control/retraitement');
		$this->page->set_my_url('/control/retraitement');
		$data_init = array(
			'lbl_bouton' => 'Charger un pli pour retraitement'
			,'direct_load_pli' => $this->input->post_get('direct') == 1 ? TRUE : FALSE
		);
		$this->page->afficher($this->load->view('control/initial', $data_init, TRUE));
	}

	public function get_pli(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v();
		$this->portier->must_calit();
		$this->load->model('control/model_pli_retraitement');
		$pli = $this->model_pli_retraitement->piocher_pli($this->session->id_utilisateur);
		if (is_null($pli)) {
			$data_init = array(
				'message' => 'Aucun pli disponnible pour retraitement!'
				,'lbl_bouton' => 'Recharger un &eacute;chantillon pour contr&ocirc;le'
				,'direct_load_pli' => FALSE
			);
			echo $this->load->view('control/no_pli', $data_init, TRUE);
		}else{
			$this->histo->pli($pli->id_pli);
			$this->saisie_pli($pli);
		}
	}
	
	private function saisie_pli($pli){
		$this->load->helper('text');
		$this->load->library('control/data');
		$data_view_pli = $this->data->get_data($pli);
		$data_view_pli['statut_retrait'] = $this->model_pli->statut_retraitement($data_view_pli['data_pli']);
		$data_view_pli['status_in'] = /*$data_view_pli['status_in'] == 5 ? 6 : */$data_view_pli['status_in'];
		$this->load->model('control/model_pli_retraitement');
		$info_ke = $this->model_pli_retraitement->info_ke($pli->id_pli);
		$data_view_pli['data_ke'] = $info_ke->data_ke;
		$data_view_pli['nb_files'] = $info_ke->nb_fichier_ke;
		echo $this->load->view('control/retraitement/pli', $data_view_pli, TRUE);
	}

	public function download_all_files_ke($id_pli){
		if(is_numeric($id_pli)){
			$this->load->library('zip');
			$this->load->model('control/model_pli_retraitement');
			$files = $this->model_pli_retraitement->files_ke($id_pli);
			foreach ($files as $key_f => $file) {
				$this->zip->add_data($file->nom_orig, pg_unescape_bytea($file->fichier));
			}
			if(count($files) > 0){
				$this->zip->download('fichiers_consigne_pli'.$id_pli.'.zip');
			}else{
				$this->load->helper('download');
				force_download('no_files.txt', 'Erreur: Aucun fichier trouvé pour le pli#'.$id_pli);
			}
		}else{
			log_message('error', 'controller> control> retraitement> download_all_files_ke: Pli introuvable! pli#'.$id_pli);
			force_download('error.txt', 'Erreur: Pli introuvable: pli#'.$id_pli);
		}
	}
	
	public function cancel($id_pli=NULL){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_calit();
		if(is_numeric($id_pli)){
			$this->load->model('control/model_pli_retraitement');
			if($this->model_pli_retraitement->unlocked($id_pli)){
				return '';
			}
		}else {
			log_message('error', 'control> retraitement> cancel_retraitement> id_pli non numerique: '.$id_pli);
		}
	}
	
	public function save(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_calit();
		if(isset($_REQUEST['id_pli'])){
			$this->load->library('control/data_save');
			if($this->data_save->initialize()){
				if(!in_array($this->data_save->etat_pli_by_chq, array(1, 11)) && in_array($this->data_save->statut, array(1, 11))){
					$this->data_save->data_pli['motif_ko'] = $this->data_save->statut == 1 ? 4 : $this->data_save->data_pli['motif_ko'];
					$this->data_save->statut = $this->data_save->etat_pli_by_chq;
					$this->data_save->data_pli['statut_saisie'] = $this->data_save->statut;
					$this->data_save->pli['statut_saisie'] = $this->data_save->statut;
					$this->data_save->data_pli['autre_motif_ko'] = ($this->data_save->data_pli['autre_motif_ko'] == '' ? '' : $this->data_save->data_pli['autre_motif_ko'].'; ').$this->data_save->motif_ko_by_chq;
				}
				if($this->data_save->saisie_batch && !$this->data_save->ok_data_batch){
					echo $this->data_save->error_data_batch;
					return '';
				}
				$flag_traitement = 9;
				switch ($this->data_save->statut) {
					case '1'://ok
						$this->data_save->data_pli['motif_ko'] = NULL;
						$this->data_save->data_pli['autre_motif_ko'] = NULL;
						$this->data_save->pli['date_termine'] = date('Y-m-d H:i:s');
						$action_ctrl = 29;
						break;
					case '11'://ok-ci
						// $this->data_save->data_pli['motif_ko'] = NULL;
						// $this->data_save->data_pli['autre_motif_ko'] = NULL;
						$this->data_save->pli['date_termine'] = date('Y-m-d H:i:s');
						$action_ctrl = 125;
						break;
					case '2'://ko scan
						$action_ctrl = 100;
						break;
					case '3'://ko def
						$action_ctrl = 33;
						break;
					case '4'://ko inconnu
						$action_ctrl = 101;
						break;
					case '5'://ko src
						$action_ctrl = 31;
						break;
					case '6'://ko en attente
						$action_ctrl = 32;
						break;
					case '7'://ko circ
						$action_ctrl = 30;
						break;
					default:
						echo 'Argument[statut] non conforme!';
						log_message('error', 'control> retraitement> save> unexpected status='.$this->data_save->statut.', op#'.$this->session->id_utilisateur.', pli#'.$this->data_save->id_pli);
						return '';
						break;
				}
				$this->data_save->pli['flag_traitement'] = $flag_traitement;
				$this->data_save->data_pli['flag_traitement'] = $flag_traitement;
				$this->data_save->motif_consigne = NULL;
				$link_motif_consigne = '';
				if(!in_array($this->data_save->statut, array(1))) {
					$link_motif_consigne = $this->data_save->id_pli.date('YmdHis');
					$choix_motif = $this->data_save->data_pli['motif_ko'] == 4 ? '' : $this->model_pli->get_label_motif_ko($this->data_save->data_pli['motif_ko']);
					$this->data_save->motif_consigne = array(
						'id_pli' => $this->data_save->id_pli
						,'motif_operateur' => $this->data_save->data_pli['autre_motif_ko'] == '' ? $choix_motif : $choix_motif.$this->data_save->data_pli['autre_motif_ko']
						,'id_flg_trtmnt' => $flag_traitement
						,'id_stt_ss' => $this->data_save->statut
						,'link_histo' => $link_motif_consigne
					);
					$this->data_save->data_pli['date_dernier_statut_ko'] = date('Y-m-d H:i:s');
				}
				$this->data_save->is_control = FALSE;
				$this->data_save->old_flg = 13;
				if($this->data_save->saisie_batch) {
					$this->load->library('saisie/data_cba');
					$this->data_save->saisie_batch = FALSE;
					$this->data_cba->set_data($this->data_save);
					if(!$this->data_cba->can_to_batch){
						echo $this->data_cba->error_msg;
						return '';
					}
				}
				$this->load->library('saisie/data_saisi_ctrl');
				$this->data_saisi_ctrl->ctrl_values($this->data_save);
				// $this->data_save->data_saisie_ctrl = array();//annule field control pour retraitement
				// echo var_dump($this->data_save->motif_consigne);die();exit();
				$this->load->model('control/model_pli');
				$rep_save = $this->model_pli->save($this->data_save);
				if($rep_save[0]){
					echo '1';
					$this->histo->action($action_ctrl, '', $this->data_save->id_pli);
					$this->histo->pli($this->data_save->id_pli, ($link_motif_consigne == '' ? '' : 'KO/CI'), $link_motif_consigne);
					// if($this->data_save->saisie_batch) {
					// 	$this->data_cba->to_cba();
					// }
				}else{
					echo $rep_save[1];
					log_message('error', 'control> retraitement> save> Enregistrement échoué:'.$rep_save[1].', pli#'.$this->data_save->id_pli.', op#'.$this->session->id_utilisateur);
				}
			}else{
				echo $this->data_save->output;
			}
		}else{
			echo 'Paramètre manquant';
			log_message('error', 'control> retraitement> save> parametre manquant op#'.$this->session->id_utilisateur);
		}
	}

}
