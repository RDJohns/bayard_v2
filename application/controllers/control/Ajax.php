<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller{

	public function view_doc($id_doc=NULL){
        $this->load->library('control/data');
        echo $this->data->view_doc($id_doc);
    }
    
    public function option_titres($id_soc=NULL, $titre_i=NULL){
        $this->load->library('control/data');
        echo $this->data->option_titres($id_soc, $titre_i);
    }
    
	public function option_typologie($id_soc=1){
        $this->load->library('control/data');
        echo $this->data->option_typologie($id_soc);
    }

    public function get_cheque_view($id_pli){
        $this->load->library('control/data');
        echo $this->data->get_cheque_view(array('id_pli'=>$id_pli));
    }

    public function get_cheque_cadeau_view($id_pli){
        $this->load->library('control/data');
        echo $this->data->get_cheque_kd_view(array('id_pli'=>$id_pli));
    }
    
	public function get_mvmnt_view($id_soc=1){
        $this->load->library('control/data');
        echo $this->data->get_mvmnt_view(array('id_soc'=>$id_soc));
    }
    
	public function save_file_circulaire(){
		$config_upload = array(
			'upload_path' => './'.upTmpFileCirc
			,'overwrite' => FALSE
			,'file_ext_tolower' => TRUE
			,'encrypt_name' => TRUE
			,'allowed_types' => '*'
		);
		$this->load->library('upload', $config_upload);
		if($this->upload->do_upload('fichier_circulaire')){
			echo json_encode(array(
				'code' => 1
				,'link' => $this->upload->data('file_name')
				,'ext' => $this->upload->data('file_ext')
				,'name' => $this->upload->data('orig_name')
			));
			$this->clear_old_file('./'.upTmpFileCirc);
		}else{
			echo json_encode(array(
				'code' => 0
				,'er' => $this->upload->display_errors()
			));
		}
	}

	private function clear_old_file($rep=NULL, $days=2){
		if(is_null($rep)){
			return '';
		}
		$dt_limit = new DateTime(date('Y-m-d'));
		$dt_limit->modify('-'.$days.' day');
		$this->load->helper('directory');
		$files = directory_map($rep, 1);
		foreach ($files as $file) {
			$dt_last_modif = filemtime($rep.$file);
			if($dt_limit->format('U') > $dt_last_modif && $file != 'read_me.txt'){
				if(file_exists($rep.$file)){
					unlink($rep.$file);
				}
			}
		}
	}

	/*public function download_circulaire($f_circ, $nom){
		$this->load->helper('download');
		$this->load->helper('file');
		force_download($nom, read_file('./'.upFileCirc.$f_circ));
	}*/
	
	public function download_circulaire($id_pli){
		if (is_numeric($id_pli)) {
			$this->load->helper('function1');
			$this->load->helper('download');
			$this->load->helper('file');
			$this->load->model('control/model_pli');
			$data_pli = $this->model_pli->data_pli($id_pli);
			if(!is_null($data_pli)){
				//$nom_ci = compose_nom_ci($data_pli->lot_scan, $id_pli, $data_pli->pli, $data_pli->nom_orig_fichier_circulaire);
				$nom_ci = $data_pli->nom_orig_fichier_circulaire;
				if(empty($nom_ci) || is_null($nom_ci) || $nom_ci == ''){
					force_download('no_fichier.txt', 'Aucun fichier circulaire pour le pli#'.$id_pli);
					return '';
				}
				$url_file_circulaire = './'.upFileCirc.$data_pli->fichier_circulaire;
				if(file_exists($url_file_circulaire)){
					force_download($nom_ci, read_file($url_file_circulaire));
				}elseif(strpos(site_url(), 'ged-server.madcom.local/GED/bayard_v2') === FALSE){//TODO: changer en DNS
					try {
						$url_90 = 'http://ged-server.madcom.local/GED/bayard_v2/index.php/control/ajax/download_circulaire/'.$id_pli;//TODO: changer en DNS
						$ch_90 = curl_init($url_90);
						curl_setopt($ch_90, CURLOPT_RETURNTRANSFER, true);
						$data_file_ci = curl_exec($ch_90);
						curl_close($ch_90);
						force_download($nom_ci, $data_file_ci);
					} catch (Exception $ex) {
						force_download('no_fichier.txt', 'Fichier circulaire Introuvable!');
					}
				}else {
					force_download('no_fichier.txt', 'Fichier circulaire Introuvable!');
				}
				return '';
			}
			force_download('no_fichier.txt', 'Aucun fichier circulaire pour le pli#'.$id_pli);
			return '';
		}
		log_message('error', 'control>ajax>download_circulaire: Pli introuvable! pli#'.$id_pli);
		force_download('error.txt', 'Pli introuvable: pli#'.$id_pli);
	}
	
	public function activity(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v();
		$this->portier->must_calit();
		$this->load->model('control/model_pli');
		$data_view = array(
			'population' => $this->model_pli->population()
			,'activity' => $this->model_pli->activity($this->session->id_utilisateur)
		);
		echo json_encode(array(
			'code' => 1
			,'login' => $this->session->login
			,'ihm' => $this->load->view('control/activity', $data_view, TRUE)
		));
	}

	public function data_addr_client($id_client = null)
	{
		$rep = array(
			'code' => 1
		);
		if(is_numeric($id_client)){
			$this->load->model('saisie/model_cba');
			$client = $this->model_cba->get_data_client($id_client);
			if(is_null($client)){
				$rep['msg'] = 'Client introuvable!';
			}else{
				$rep['code'] = 0;
				$rep['client'] = $client;
			}
		}else{
			$rep['msg'] = 'Num&eacute;ro client invalide!';
		}
		echo json_encode($rep);
	}

	public function data_promo_cba($code_promo = null)
	{
		$rep = array(
			'code' => 0
		);
		$this->load->model('saisie/model_cba');
		$rep['promos'] = $this->model_cba->get_promo($code_promo);
		echo json_encode($rep);
	}

	public function data_promo_cba_art()
	{
		$code_promo = $this->input->post_get('code_promo');
		$code_chx = $this->input->post_get('code_chx');
		$rep = array(
			'code' => 0
		);
		$this->load->model('saisie/model_cba');
		$rep['promos'] = $this->model_cba->get_promo_art($code_promo, $code_chx);
		echo json_encode($rep);
	}

	public function mode_advantage($id_pli)
	{
		$this->load->model('control/model_pli');
		$pli = $this->model_pli->pli($id_pli);
		if($pli->controle_par == $this->session->id_utilisateur || $pli->retraite_par == $this->session->id_utilisateur){
			if(in_array($pli->flag_traitement, array(8, 13))) {
				$this->load->model('saisie/model_cba');
				echo $this->model_cba->mode_directe($id_pli);
			} else {
				echo 'Ce pli ne vous est plus assigné!';
			}
		}else{
			echo 'Ce pli ne vous est plus assigné!';
		}
	}

	public function all_motif_consigne($id_pli)
	{
		$this->load->model('control/model_pli');
		$motif_consignes = $this->model_pli->get_all_motif_consigne($id_pli);
		echo $this->load->view('control/motif_consignes', array('motif_consignes'=>$motif_consignes), TRUE);
	}
	/*public function rectif_image($id_doc)//test rectifie orientation image
	{
		$this->load->library('control/data');
		$images = $this->data->test_ori_image($id_doc);
		if(is_array($images)){
			$this->load->view('images', array('images'=>$images));
		}else{
			echo $images;
		}
	}*/



	public function luConsigne($etape,$idPli,$idMotif)
	{
		$this->load->model("traitement_ko/Model_Traitement_KO", "MtraitementKO");
		$data = array("id_pli"=>$idPli,"etape"=>$etape,"user_id"=>$this->session->id_utilisateur,"id_motif"=>$idMotif);
		$this->MtraitementKO->marquerLu($data);
	}
    
}
