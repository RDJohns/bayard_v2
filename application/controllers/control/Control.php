<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Control extends CI_Controller{

	public function index(){
		$this->load->library('portier');
		$this->portier->must_calit();
		$this->go();
	}
	
	private function go(){
		$this->load->library('control/page');
		$this->page->set_titre('Contr&ocirc;le des plis OK');
		// $this->page->add_links('Validation', site_url('control/validation'));
		$this->page->add_links('Retraitement', site_url('control/retraitement'));
		$this->page->add_links('Suivi Circulaire', site_url('circulaire/pli'),'fa fa-paperclip', 'black');
		$this->page->add_links('virements citrix sur la GED', site_url('citrix/Saisie_citrix'));
		$this->page->add_js('control/main');
		$this->page->add_js('control/control');
		$this->page->add_css('control/main');
		$this->page->add_css('control/control');
		$this->page->set_my_url('/control/control');
		$this->load->model('control/model_pli_control');
		$data_init = array(
			'lbl_bouton' => 'Charger un &eacute;chantillon pour contr&ocirc;le'
			,'trtmt_ctrl' => $this->session->has_userdata('trtmt_ctrl') ? $this->session->trtmt_ctrl : '-1'
			,'user_ctrl' => $this->session->has_userdata('user_ctrl') ? $this->session->user_ctrl : '-1'
			,'filtres' =>  $this->model_pli_control->get_data_filtre()
			,'direct_load_pli' => $this->input->post_get('direct') == 1 ? TRUE : FALSE
		);
		$this->page->afficher($this->load->view('control/control/initial', $data_init, TRUE));
		$this->load->model('control/model_pli');
		$this->model_pli->verif_control();
	}

	public function get_pli(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v();
		$this->portier->must_calit();
		$trtmt_ctrl = isset($_REQUEST['trtmt_ctrl']) ? $this->input->post_get('trtmt_ctrl') : '-1';
		$user_ctrl = isset($_REQUEST['user_ctrl']) ? $this->input->post_get('user_ctrl') : '-1';
		$this->session->trtmt_ctrl = $trtmt_ctrl;
		$this->session->user_ctrl = $user_ctrl;
		$filtre = array(
			'typo' => $trtmt_ctrl
			,'user' => $user_ctrl
		);
		$this->load->model('control/model_pli_control');
		$pli = $this->model_pli_control->piocher_pli($this->session->id_utilisateur, $filtre);
		if (is_null($pli)) {
			$data_init = array(
				'message' => $trtmt_ctrl == '-1' && $user_ctrl == '-1' ? 'Aucun pli disponnible pour contr&ocirc;le!' : 'Aucun pli &agrave; contr&ocirc;ler pour ces crit&egrave;res!'
				,'lbl_bouton' => 'Recharger un &eacute;chantillon pour contr&ocirc;le'
				,'trtmt_ctrl' => $trtmt_ctrl
				,'user_ctrl' => $user_ctrl
				,'filtres' =>  $this->model_pli_control->get_data_filtre()
				,'direct_load_pli' => FALSE
			);
			echo $this->load->view('control/control/no_pli', $data_init, TRUE);
		}else{
			$this->histo->pli($pli->id_pli);
			$this->saisie_pli($pli);
		}
	}

	private function saisie_pli($pli){
		$this->load->helper('text');
		$this->load->library('control/data');
		$data_view_pli = $this->data->get_data($pli);
		$data_view_pli['statut_ctrl'] = $this->model_pli->statut_control($data_view_pli['data_pli']);
		echo $this->load->view('control/control/pli', $data_view_pli, TRUE);
	}

	public function cancel($id_pli=NULL){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_calit();
		if(is_numeric($id_pli)){
			$this->load->model('control/model_pli_control');
			if($this->model_pli_control->unlocked($id_pli)){
				return '';
			}
		}else {
			log_message('error', 'control> control> cancel_control> id_pli non numerique: '.$id_pli);
		}
	}

	public function save(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_calit();
		if(isset($_REQUEST['id_pli'])){
			$this->load->library('control/data_save');
			if($this->data_save->initialize()){
				if(!in_array($this->data_save->etat_pli_by_chq, array(1, 11)) && in_array($this->data_save->statut, array(1, 11))){
					$this->data_save->data_pli['motif_ko'] = $this->data_save->statut == 1 ? 4 : $this->data_save->data_pli['motif_ko'];
					$this->data_save->statut = $this->data_save->etat_pli_by_chq;
					$this->data_save->data_pli['statut_saisie'] = $this->data_save->statut;
					$this->data_save->pli['statut_saisie'] = $this->data_save->statut;
					$this->data_save->data_pli['autre_motif_ko'] = ($this->data_save->data_pli['autre_motif_ko'] == '' ? '' : $this->data_save->data_pli['autre_motif_ko'].'; ').$this->data_save->motif_ko_by_chq;
				}
				if($this->data_save->saisie_batch && !$this->data_save->ok_data_batch){
					echo $this->data_save->error_data_batch;
					return '';
				}
				$flag_traitement = 9;
				switch ($this->data_save->statut) {
					case '1'://ok
						$this->data_save->data_pli['motif_ko'] = NULL;
						$this->data_save->data_pli['autre_motif_ko'] = NULL;
						$this->data_save->pli['date_termine'] = date('Y-m-d H:i:s');
						$action_ctrl = 21;
						break;
					case '11'://ok-ci
						// $this->data_save->data_pli['motif_ko'] = NULL;
						// $this->data_save->data_pli['autre_motif_ko'] = NULL;
						$this->data_save->pli['date_termine'] = date('Y-m-d H:i:s');
						$action_ctrl = 124;
						break;
					case '2'://ko scan
						$action_ctrl = 102;
						break;
					case '3'://ko def
						$action_ctrl = 22;
						break;
					case '4'://ko inconnu
						$action_ctrl = 103;
						break;
					case '5'://ko src
						$action_ctrl = 25;
						break;
					case '6'://ko en attente
						$action_ctrl = 24;
						break;
					case '7'://ko circ
						$action_ctrl = 23;
						break;
					default:
						echo 'Argument[statut] non conforme!';
						log_message('error', 'control> control> save> unexpected status='.$this->data_save->statut.', op#'.$this->session->id_utilisateur.', pli#'.$this->data_save->id_pli);
						return '';
						break;
				}
				$this->data_save->pli['flag_traitement'] = $flag_traitement;
				$this->data_save->data_pli['flag_traitement'] = $flag_traitement;
				$this->data_save->motif_consigne = NULL;
				$link_motif_consigne = '';
				if(!in_array($this->data_save->statut, array(1))) {
					$link_motif_consigne = $this->data_save->id_pli.date('YmdHis');
					$choix_motif = $this->data_save->data_pli['motif_ko'] == 4 ? '' : $this->model_pli->get_label_motif_ko($this->data_save->data_pli['motif_ko']);
					$this->data_save->motif_consigne = array(
						'id_pli' => $this->data_save->id_pli
						,'motif_operateur' => $this->data_save->data_pli['autre_motif_ko'] == '' ? $choix_motif : $choix_motif.$this->data_save->data_pli['autre_motif_ko']
						,'id_flg_trtmnt' => $flag_traitement
						,'id_stt_ss' => $this->data_save->statut
						,'link_histo' => $link_motif_consigne
					);
					$this->data_save->data_pli['date_dernier_statut_ko'] = date('Y-m-d H:i:s');
				}
				$this->data_save->is_control = TRUE;
				$this->data_save->old_flg = 8;
				if($this->data_save->saisie_batch) {
					$this->load->library('saisie/data_cba');
					$this->data_save->saisie_batch = FALSE;
					$this->data_cba->set_data($this->data_save);
					if(!$this->data_cba->can_to_batch){
						echo $this->data_cba->error_msg;
						return '';
					}
				}
				$this->load->library('saisie/data_saisi_ctrl');
				$this->data_saisi_ctrl->ctrl_values($this->data_save);
				// echo var_dump($this->data_save->data_pli);die();exit();
				$this->load->model('control/model_pli');
				$rep_save = $this->model_pli->save($this->data_save);
				if($rep_save[0]){
					echo '1';
					$this->histo->action($action_ctrl, '', $this->data_save->id_pli);
					$this->histo->pli($this->data_save->id_pli, ($link_motif_consigne == '' ? '' : 'KO/CI'), $link_motif_consigne);
					$this->model_pli->verif_control();
					// if($this->data_save->saisie_batch) {
					// 	$this->data_cba->to_cba();
					// }
				}else{
					echo $rep_save[1];
					log_message('error', 'control> control> save> Enregistrement échoué:'.$rep_save[1].', pli#'.$this->data_save->id_pli.', op#'.$this->session->id_utilisateur);
				}
			}else{
				echo $this->data_save->output;
			}
		}else{
			echo 'Paramètre manquant';
			log_message('error', 'control> control> save> parametre manquant op#'.$this->session->id_utilisateur);
		}
	}

}
