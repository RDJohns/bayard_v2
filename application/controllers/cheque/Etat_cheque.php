<?php
class Etat_cheque extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->load->helper(array('form', 'url'));
        $this->config->set_item('language', 'french');
        $this->load->model("cheque/Model_cheque", "Cheque");
        ini_set("memory_limit", "-1");
        set_time_limit(0);
    }

    public function index()
    {
        $this->portier->must_cdn();
        $this->etat_cheque();
    }

    public function etat_cheque(){
        $head["title"]               = "Etat des ch&egrave;ques";
        $head["icon"]                = "label";

        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/magnify/jquery.magnify',
            '../src/template/css/font-awesome.min',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/sweetalert/sweetalert',
            'plugins/nprogress/nprogress',
            'css/style',
            'css/themes/all-themes',
            'css/custom/typage',
            'css/custom/matchage',
            '../src/template/css/font-awesome.min'
        );


        $foot['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/magnify/jquery.magnify',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'js/pages/ui/notifications',
            'js/admin',
            'plugins/sweetalert/sweetalert.min',
            'plugins/nprogress/nprogress',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/cheque/cheque'
        );

        $head["theme"] = 'theme-blue';

        $head["menu"] = 'Etat des ch&egrave;ques';

        $arr_chq['error']   = '';
        $arr_chq['societe'] = $this->Cheque->get_societe();
        $arr_chq['etat']    = $this->Cheque->get_etat();

        $this->load->view("main/header",$head);
        $this->load->view("cheque/etat_chq_view", $arr_chq);
        $this->load->view("main/footer",$foot);
    }

    public function get_cheque(){
        $societe    = $this->input->post('societe');
        $etat       = $this->input->post('etat');
        $cmc        = $this->input->post('cmc');

        $results    = $this->Cheque->fill_datatable_cheque($societe,$etat,$cmc);

        echo json_encode($results);

    }

    public function change_etat_chq(){
        $id     = $this->input->post('id');
        $etat   = $this->input->post('etat');
        $id_pli = $this->input->post('id_pli');
        $id_doc = $this->input->post('id_doc');
        $id_saisie = $this->input->post('id_saisie');

        $upd = $this->Cheque->update_etat_chq($id,$etat,$id_pli,$id_doc,$id_saisie);

        if($upd){
            echo 'success';
        }
        else{
            echo 'error';
        }
    }
}