<?php
class Correspondance_cheque extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->load->helper(array('form', 'url', 'function1'));
        $this->config->set_item('language', 'french');
        $this->load->model("cheque/Model_cheque", "Cheque");
        ini_set("memory_limit", "-1");
        set_time_limit(0);
    }

    public function index()
    {
        $this->portier->must_admin_cdn();
        $this->etat_cheque();
    }

    public function etat_cheque(){
        $head["title"]               = "Liste des ch&egrave;ques KO REB";
        $head["icon"]                = "label";

        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/magnify/jquery.magnify',
            'plugins/metisMenu/font-awesome',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/sweetalert/sweetalert',
            'plugins/nprogress/nprogress',
            'css/style',
            'css/themes/all-themes',
            'css/custom/typage',
            'css/custom/matchage',
            '../src/template/css/font-awesome.min'
        );


        $foot['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/magnify/jquery.magnify',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'js/pages/ui/notifications',
            'js/admin',
            'plugins/sweetalert/sweetalert.min',
            'plugins/nprogress/nprogress',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/pages/ui/tooltips-popovers',
            'js/cheque/correspondance'
        );

        $head["theme"] = 'theme-blue';

        $head["menu"] = "Liste des ch&egrave;ques KO REB";

        $arr_chq['error']   = '';
        $arr_chq['societe'] = $this->Cheque->get_societe();
        $arr_chq['etat']    = $this->Cheque->get_etat();

        $this->load->view("main/header",$head);
        $this->load->view("cheque/ko_reb_view", $arr_chq);
        $this->load->view("main/footer",$foot);
    }

    public function get_cheque(){
        $societe    = $this->input->post('societe');
        $etat       = $this->input->post('etat');
        $cmc        = $this->input->post('cmc');

        $results    = $this->Cheque->fill_correspondance_reb($societe,$etat,$cmc);

        echo json_encode($results);

    }

    public function change_etat_chq(){
        $id     = $this->input->post('id');
        $etat   = $this->input->post('etat');

        $upd = $this->Cheque->update_etat_chq($id,$etat);

        if($upd){
            echo 'success';
        }
        else{
            echo 'error';
        }
    }

    public function get_extract_excel()
    {
        $societe    = $this->input->post('societe');
        $etat       = $this->input->post('etat');
        $cmc        = $this->input->post('cmc');

        $results    = $this->Cheque->fill_extract_ko_reb($societe,$etat,$cmc);

        $array_export['result']      = $results;

        $this->load->view('cheque/export_ko_reb_view.php', $array_export);
    }

    public function get_advantage(){
        $societe    = $this->input->post('societe');

        $results = $this->Cheque->fill_ko_adv($societe);

        echo json_encode($results);
    }

    public function change_statut_corr(){
        $id     = $this->input->post('id_mst');
        $id_soc = $this->input->post('id_soc');
        $etat   = $this->input->post('etat');

        $upd = $this->Cheque->update_statut_corr($id,$id_soc,$etat);

        if($upd){
            echo 'success';
        }
        else{
            echo 'error';
        }
    }

    public function get_payeur_info(){
        $id_pli = $this->input->post('id_pli');
        $id     = $this->input->post('id');

        $res = $this->Cheque->get_payeur_info($id_pli,$id);

        echo json_encode($res);
    }

    public function update_payeur(){
        $id_pli     = $this->input->post('id_pli');
        $list_payeur = $this->input->post('num_payeur');
        $societe    = $this->input->post('societe');
        $id_chq     = $this->input->post('id_chq');
        $montant    = $this->input->post('montant');

        $upd = $this->Cheque->update_payeur($id_pli,$list_payeur,$societe,$id_chq,$montant);

        if($upd){
            echo 'ok';
        }
        else{
            echo $upd;
        }

    }
}