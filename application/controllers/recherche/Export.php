<?php

class Export extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model("recherche/Model_Recherche", "MRecherche");
        $this->load->model("recherche/Model_RechercheFlux", "MRechercheFlux");
        $this->load->library('ra/Excel',null,'excel');
        $this->load->library('ra/Style_export',null,"StyleExport");
    }
    public function index()
    {
       
        
        //activate worksheet number 1
        
        $listeColExcel     = $this->excel->listeColExcel();
        /*["date_reception", "id_flux", "id_lot_saisie", "nom_societe", "source", "statut", "typologie"]*/
        
        

        $listeColonneText = array();
        $listeColonneText = $this->session->userdata('listeColonneText');
        $colNumber        = 1;
        $listNumber       = 1;
        $coulThead        = '#58c9f3';
        foreach($listeColonneText as $h){
            $this->excel->getActiveSheet()->setCellValue($listeColExcel[$colNumber].$listNumber, $h);
            $this->excel->getActiveSheet()->getStyle($listeColExcel[$colNumber].$listNumber)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle($listeColExcel[$colNumber].$listNumber)
                        ->applyFromArray($this->StyleExport->background($coulThead))
                        ->applyFromArray($this->StyleExport->font_color('FFFFFF'))
                        ->applyFromArray($this->StyleExport->border_style('000000'));
            $colNumber++;    
        }
        $this->excel->getActiveSheet()->freezePane('B2');


        $filtre = $this->session->userdata('filtre'); 
        $listeColonne =  implode(",", $this->session->userdata('listeColonne')).""; 
        
       

        if($this->session->userdata('TypeTtmt') == 1)
        {
            $ob = $this->MRecherche->exportExcel($filtre,$listeColonne);  
        }
        else
        {
            $ob = $this->MRechercheFlux->exportExcel($filtre,$listeColonne);  
        }

        if($ob )
        {
            
            $colNumber = 1;
            foreach($ob as $itemListe)
            {
               
                $listeNumber = 1;
                $colNumber   +=1;
                foreach($this->session->userdata('listeColonne') as $itemColonne)
                {
                    $row[] = $itemListe->$itemColonne;
                    $this->excel->getActiveSheet()->setCellValue($listeColExcel[$listeNumber].$colNumber,"'".$itemListe->$itemColonne."'");
                    $this->excel->getActiveSheet()->getStyle($listeColExcel[$listeNumber].$colNumber)->applyFromArray($this->StyleExport->border_style('000000'));
                    $listeNumber   += 1;
                }
               
            }

            foreach(range('A','XZ') as $columnID)
            {
                $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
            }

        }


        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Recherche_avancée');
        //set cell A1 content with some text
        
       
       
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
         
        $filename='recherche_avancée.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                    
       
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function indexd()
    {
        $filtre = $this->session->userdata('filtre'); 
        $listeColonne =  implode(",", $this->session->userdata('listeColonne')).""; 
        $flux = $this->MRechercheFlux->exportExcel($filtre,$listeColonne);
       

        if($flux )
        {
            $listeNumber = 2;
            foreach($flux as $item)
            {
                $colNumber   = 1;
                $listeNumber += 1;
                $this->excel->getActiveSheet()->setCellValue($listeColExcel[$col_number].$liste_number, $ref);
            }
        }

    }
    public function setExport()
    {
        $this->session->set_userdata("filtre",$_POST["myData"]);
        $this->session->set_userdata("listeColonne",$_POST["listeColonne"]);
        $this->session->set_userdata("listeColonneText",$_POST["listeColonneText"]);
    }

    
}