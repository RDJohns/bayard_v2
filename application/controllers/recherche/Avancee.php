<?php

class Avancee extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        set_time_limit(7200);

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }

        $this->load->model("recherche/Model_Filtre", "Mfiltre");
        $this->load->library('ra/Lib_Form',null,'LibForm');
        $this->load->model("recherche/Model_Recherche", "MRecherche");
        $this->load->model("recherche/Model_RechercheFlux", "MRechercheFlux");
    }

    public function index()
    {
        $this->viewPrincipale();
       // echo $this->LibForm->getFiltreRa(); 
    }

    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Recherche avancée";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "search";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            /*'plugins/dataTable/dataTables.bootstrap',*/
            'plugins/dataTable/fixedHeader.bootstrap',
            'plugins/dataTable/responsive.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'plugins/morrisjs/morris',
            'css/style',
            'css/themes/all-themes',
            'plugins/daterangepicker/daterangepicker',
            'plugins/multi-select/css/multi-select',
            'plugins/timeline/timeline',
            'plugins/magnify/jquery.magnify',
            'plugins/jspanel/jspanel',
             'css/recherche/recherche',
			'../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap'
            
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/dataTable/jquery.dataTables',
            'plugins/dataTable/dataTables.fixedHeader',
            'plugins/dataTable/dataTables.responsive',
            'plugins/dataTable/responsive.bootstrap',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            /*'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',*/
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/daterangepicker/moment.min',
            'plugins/magnify/jquery.magnify',
            'plugins/daterangepicker/daterangepicker',
            'plugins/multi-select/js/jquery.multi-select',
            'plugins/multi-select/js/jquery.quicksearch',
            'plugins/jspanel/jspanel',
            'plugins/jspanel/extensions/contextmenu/jspanel.contextmenu',
            'plugins/jspanel/extensions/hint/jspanel.hint',
            'plugins/jspanel/extensions/modal/jspanel.modal',
            'plugins/jspanel/extensions/tooltip/jspanel.tooltip',
            'plugins/jspanel/extensions/dock/jspanel.dock',
             'js/recherche/recherche',
        );

        $arr['filtre']  = $this->Mfiltre->getFiltre();
        
        $this->load->view("main/header",$head);
        $this->load->view("recherche/Filtre",$arr);
        $this->load->view("recherche/Modal");
        $this->load->view("main/footer",$foot);
    }

    public function getFiltreRa()
    {
        echo $this->LibForm->getFiltreRa(); 
    }

    public function ajouterChamp()
    {
        $this->LibForm->ajouterChamp(); 
    }
    public function raColonne()
    {
        $this->LibForm->raColonne(); 
    }

    public function afficherTable()
    {
        //$this->output->enable_profiler(TRUE);
        $filtre = $_POST["myData"];
        $str = "";
        $recordsTotal = 0;
        $recordsFiltered = 0;

        $listeColonne =  implode(",", $_POST["listeColonne"]).""; 

        $this->session->set_userdata("columnOrder",$_POST["listeColonne"]);

       
       if($this->session->userdata('TypeTtmt') == 1)
        {
            $getListePli        = $this->MRecherche->getListe($filtre,$listeColonne); 
            $recordsTotal       = $this->MRecherche->recordsTotal($filtre,$listeColonne);
            $recordsFiltered    = $recordsTotal;

            

        }
        else
        {
            $getListePli        = $this->MRechercheFlux->getListe($filtre,$listeColonne);
            $recordsTotal       = $this->MRechercheFlux->recordsTotal($filtre,$listeColonne);
            $recordsFiltered    = $this->MRechercheFlux->recordsFiltered($filtre,$listeColonne);     
        }

        
        
        

        $data   = array();
        $no     = $_POST['start'];

     foreach($getListePli as $itemListe)
        {
            $row           = array();

           // $row[] = $itemListe->id_click;
           //<i class="material-icons">visibility</i>
           
            if($this->session->userdata('TypeTtmt') == 1)
            {
               /*  $row[] = '<center><br/><button type="button" class="btn bg-teal btn-circle waves-effect waves-circle waves-float edit" onclick="afficherDetail('.$itemListe->id_click.');"><i class="material-icons">format_list_bulleted</i></button></center>'; */
               $row[] = '<center><i style="cursor:pointer !important;" onclick="afficherDetail('.$itemListe->id_click.');" class="material-icons view-ra-detail">visibility</i></center>'; 
            }
            else
            {
                /* $row[] = '<center><br/><button type="button" class="btn bg-teal btn-circle waves-effect waves-circle waves-float edit" onclick="afficherDetailFlux('.$itemListe->id_click.');"><i class="material-icons">format_list_bulleted</i></button></center>'; */
                $row[] = '<center><i style="cursor:pointer !important;" onclick="afficherDetailFlux('.$itemListe->id_click.');" class="material-icons view-ra-detail">visibility</i></center>'; 
            }
            foreach($_POST["listeColonne"] as $itemColonne)
            {
              

                if(strpos($itemColonne, "date_") !== false && trim($itemListe->$itemColonne) != "")
                {
                    $row[] = date("d/m/Y", strtotime($itemListe->$itemColonne)); 
                }
                else
                {
                    $row[] = $itemListe->$itemColonne;
                }
            }
            
           
           
            $data[] = $row;

          
        }
        
        $output = array(
        "draw"            => $_POST['draw'],
        "recordsTotal"    =>  $recordsTotal,
        "recordsFiltered" => $recordsFiltered,
        "data"            => $data);

        echo json_encode($output);

    }

    public function createTHtable()
    {
        $table = '';
        
        if(is_array($_POST["listeColonneText"]))
        {
            $table .= '<table class="table table-bordered table-striped table-hover dataTable table-liste" id="table-liste-data">';
            $table .= '<thead><tr>';
            $table .= '<th width="10px">Action</th>';
            $table .= "<th>".implode("</th><th>",$_POST["listeColonneText"])."</th>";
            $table .= '</tr></thead>';
            $table .= '<tbody></tbody>';
            $table .= '</table>';
        }
        
       
        echo $table;
    }
    public function infoGle($idClic)
    {
        //37260
        $infoGle = $this->Mfiltre->infoGle($idClic);
        if($infoGle)
        {
            $infoGleObj = $infoGle[0];
            $nbPaiement = $nbDoc = 0;
            $nbPaiement = count(explode(";",$infoGleObj->mode_paiement));
            $nbDoc = count(explode(";",$infoGleObj->numero_image_verso));
            $titre = $infoGleObj->titre." (".$infoGleObj->code.")";
            $strInfoGle = "";
            $strInfoGle .= "<div class='body'><table class='table table-striped table-hover table-info'>";
            $strInfoGle .='<tr><td class="text-info-gle">Societe</td><td>'.$infoGleObj->nom_societe.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Typologie</td><td>'.$infoGleObj->typologie.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Titre</td><td>'.$titre.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Lot advatange</td><td>'.$infoGleObj->id_lot_saisie.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Commande</td><td>'.$infoGleObj->commande.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Lot scan</td><td>'.$infoGleObj->lot_scan.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Nombre moyen de paiement</td><td>'.(int)$nbPaiement.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Moyen de paiement</td><td>'.$infoGleObj->mode_paiement.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Nom image</td><td>'.$infoGleObj->numero_image_verso.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Nombre document</td><td>'.(int)$nbDoc.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">ID document</td><td>'.$infoGleObj->id_doc.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Nombre mouvement</td><td>'.(int)$infoGleObj->nb_mouvement.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Date création</td><td>'.$infoGleObj->date_creation.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Date courrier</td><td>'.$infoGleObj->date_reception.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Dernier traitement</td><td>'.$infoGleObj->date_traitement.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Délégué</td><td>'.$infoGleObj->nom_deleg.'</td></tr>';
            
            echo $strInfoGle .= "</table></div>";
        }
      
       // var_dump($infoGle);
    }

    public function infoMouvement($idClic)
    {
        $infoMvt = $this->Mfiltre->infoMouvement($idClic); 
        $strInfoMvt = "";
        $strInfoMvt .= "<table class='table table-striped table-hover table-info'>";
        if($infoMvt)
        {
            $strInfoMvt .="<thead><tr><th>Titre</th><th>Nom abonné</th><th>N° abonné</th><th>Nom payeur</th><th>N° payeur</th><th>Code promo</th></tr></thead>";
            $strInfoMvt .="<tbody>";
            foreach($infoMvt as $item)
            {
                $strInfoMvt .="<tr>";
                $strInfoMvt .="<td>".$item->titre."</td>";
                $strInfoMvt .="<td>".$item->nom_abonne."</td>";
                $strInfoMvt .="<td>".$item->numero_abonne."</td>";
                $strInfoMvt .="<td>".$item->nom_payeur."</td>";
                $strInfoMvt .="<td>".$item->numero_payeur."</td>";
                $strInfoMvt .="<td>".$item->code_promo."</td>";
                $strInfoMvt .="</tr>";
            }
            $strInfoMvt .="</tbody>";
        }
        echo $strInfoMvt .= "</table>";
    }

    public function getPaiement($idClic)
    {
        $infoListePaie = $this->Mfiltre->getPaiement($idClic);

        $strPaie = "";
        if($infoListePaie)
        {
            $strPaie .= "Liste mode de paiement : ".$infoListePaie[0]->liste_paie."<br/>";
        }
        
        $infoChq    = $this->getPaiementChq($idClic);
        $infoChqKdo = $this->getChequekdo($idClic);
        echo $strPaie.$infoChq.$infoChqKdo;
        //var_dump( $infoListePaie);
    }

    public function getChequekdo($idClic)
    {
        $infoListePaie = $this->Mfiltre->getChequekdo($idClic);

        $str = "";

        if($infoListePaie)
        {
            $str .= "<br/>Paiement chèque cadeau : <br/>";
            $str .= "<table class='table table-striped table-hover table-info'>";
            $str .="<thead><tr><th>Type chèque cadeau</th><th>Montant</th><th>ID document</th></tr></thead>";
            $str .="<tbody>";
            /*load_img_doc*/
            foreach($infoListePaie as $item)
            {
                $str .="<tr>";
                $str .="<td>".$item->type_cheque_cadeau."</td>";
                $str .="<td>".$item->montant."</td>";
                $str .="<td><a style='cursor:pointer;' title='Cliquez pour afficher' onclick='load_img_doc(".$item->id_doc.");'>".$item->id_doc."</a></td>";
                $str .="</tr>";
            }
    
            $str .="</tbody>";
            $str .= "</table>";
        }
       

       return $str;
    }
    public function getPaiementChq($idClic)
    {
        $infoListePaie = $this->Mfiltre->getPaiementChq($idClic);
        $str = "";
        if($infoListePaie)
        {
            $str .= "<br/>Paiement chèque : <br/>";
            $str .= "<table class='table table-striped table-hover table-info'>";
            $str .="<thead><tr><th>Cmc7</th><th>Rlmc</th><th>Montant</th><th>Anomalie</th></tr></thead>";
            $str .="<tbody>";
            foreach($infoListePaie as $item)
            {
                $str .="<tr>";
                $str .="<td>".$item->cmc7."</td>";
                $str .="<td>".$item->rlmc."</td>";
                $str .="<td>".$item->montant."</td>";
                $str .="<td>".$item->anomalie."</td>";
                $str .="</tr>";
            }

            $str .="</tbody>";
            $str .= "</table>";
        }
        return $str;
    }

    public function setFiltre($ttmt)
    {
        /*1 - courrier; 2-flux*/
        $this->session->set_userdata("TypeTtmt",(int)$ttmt);
        echo $this->session->userdata('TypeTtmt');
    }
     
    public function infoGleFlux($idFlux)
    {
        
        $info = $this->Mfiltre->infoGleFlux($idFlux);
        if($info)
        {
            $infoGleObj = $info[0];

            $strInfoGle = "";
            $strInfoGle .= "<div class='body'><table class='table table-striped table-hover table-info'>";
            $strInfoGle .='<tr><td class="text-info-gle">Source</td><td>'.$infoGleObj->source.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">ID flux</td><td>'.$infoGleObj->id_flux.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Societe</td><td>'.$infoGleObj->nom_societe.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Dossier</td><td>'.$infoGleObj->sous_dossier.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Typologie</td><td>'.$infoGleObj->typologie.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Lot advantage</td><td>'.$infoGleObj->id_lot_saisie.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">date reception</td><td>'.$infoGleObj->date_reception.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Statut</td><td>'.$infoGleObj->statut.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Nb. abonnement</td><td>'.$infoGleObj->nb_abonnement.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Entité</td><td>'.$infoGleObj->entite.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Motif escalade</td><td>'.$infoGleObj->motif_escalade.'</td></tr>';
            $strInfoGle .='<tr><td class="text-info-gle">Motif transfert</td><td>'.$infoGleObj->motif_transfert.'</td></tr>';
            
            
            /*
                var base = s_url+'push/typage/Pli/download/'+flux.id_flux ;
			    strMail += "ID flux : "+flux.id_flux+'<br/>';
                strMail +='Fichier : <a  class="name_file" href="push/typage/Pli/download/" style="cursor:pointer;"></a>';
            */

            if($infoGleObj->id_source == 1) /*mail*/
            {
                $strInfoGle .='<tr><td class="text-info-gle">Date d\'envoi mail</td><td>'.$infoGleObj->date_envoi_mail.'</td></tr>';
                $strInfoGle .='<tr><td class="text-info-gle">Objet</td><td>'.$infoGleObj->objet_flux.'</td></tr>';
                $strInfoGle .='<tr><td class="text-info-gle">Expéditeur mail</td><td>'.$infoGleObj->from_flux.'</td></tr>';
                $strInfoGle .='<tr><td class="text-info-gle">Destinataire mail</td><td>'.$infoGleObj->to_flux.'</td></tr>';
                $strInfoGle .='<tr><td class="text-info-gle">Nb. pièce jointe</td><td>'.$infoGleObj->nb_piece_jointe.'</td></tr>';
                $strInfoGle .='<tr><td class="text-info-gle"> <a  class="name_file" href="avancee/download/'.$infoGleObj->id_flux.'" style="cursor:pointer;"><button type="button" class="btn bg-pink waves-effect">&nbsp;&nbsp;Télécharger et voir le mail</button></a></td></tr>';
            }
            else /*sftp*/
            {
                
                $strInfoGle .='<tr><td class="text-info-gle">Emplacement</td><td>'.$infoGleObj->emplacement.'</td></tr>';
                $strInfoGle .='<tr><td class="text-info-gle">Nom fichier</td><td>'.$infoGleObj->nom_fichier.'</td></tr>';
                $strInfoGle .='<tr><td class="text-info-gle"> <a  class="name_file" href="avancee/download/'.$infoGleObj->id_flux.'" style="cursor:pointer;"><button type="button" class="btn bg-pink waves-effect">&nbsp;&nbsp;Télécharger le fichier</button></a></td></tr>';
            }
           
            
            echo $strInfoGle .= "</table></div>";
        }
    }

    public function getAbonnement($fluxID)
    {
        
        $abo = $this->Mfiltre->getAbonnement($fluxID);
        $strInfo = "Pas de données";
        if($abo)
        {
            $strInfo = "";
            $strInfo .= "<table class='table table-striped table-hover table-info'>";
            
                $strInfo .="<thead><tr><th>Titre</th><th>Nom abonné</th><th>N° abonné</th><th>Nom payeur</th><th>N° payeur</th><th>Typologie</th><th>Statut</th></tr></thead>";
                $strInfo .="<tbody>";
                foreach($abo as $item)
                {
                    $strInfo .="<tr>";
                    $strInfo .="<td>".$item->titre."</td>";
                    $strInfo .="<td>".$item->nom_abonne."</td>";
                    $strInfo .="<td>".$item->numero_abonne."</td>";
                    $strInfo .="<td>".$item->numero_payeur."</td>";
                    $strInfo .="<td>".$item->nom_payeur."</td>";
                    $strInfo .="<td>".$item->typologie."</td>";
                    $strInfo .="<td>".$item->statut."</td>";
                    $strInfo .="</tr>";
                }
                $strInfo .="</tbody>";
            
             $strInfo .= "</table>";
        }
        
        echo $strInfo;
        
    }
    public function download($fluxID)
    {
        $flux = $this->getFile($fluxID);
		$obj = $flux[0];
        $file = "";
        
		if($flux)
		{
			$file   = "";
            $source = $obj->id_source;
            if((int)$source == 1 )// mail
			{
				$file = FCPATH.'SFTP_MAIL/mail/'.$obj->emplacement.'/'.$obj->nom_fichier;
				$fileName = $obj->nom_fichier;

			}
			elseif((int)$source == 2)
			{
				$emplacement = str_replace('/mnt/ged_bayard/','',$obj->emplacement);
				echo $file = FCPATH.'SFTP_MAIL/'.$emplacement.'/'.$obj->nom_fichier;
				$fileName = $obj->filename_origin;
			}
		}
	  
				
      if(file_exists($file))
      {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($fileName).'"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
           exit;
        }
        else
        {
            exit("<b>Erreur de la récupération du fichier</b><br/>Le fichier n'est pas dans le serveur.<br/> <a href='".$_SERVER["HTTP_REFERER"]."'><br/>Cliquer ici pour revenir à la page précedante</a>");
        }
       
        
    }

    public function getFile($fluxID)
    {
        return $this->Mfiltre->getFile($fluxID);
    }
}