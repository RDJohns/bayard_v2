<?php

class Typage extends CI_Controller
{
    public $retourPli       = array();

    public $saisieEncours   = 0;
    public $infoUser        = "";
    public $noImage         = "iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAC4xJREFUeJztnVusFVcdh79zuBULEkpPI9JG24SaSq0BCyVphYeamBCTVgmBGuVQsDTaND5U4yWNPogxGo2JxlgIxsYHY2LiBWuNQRqKyoPWaorGAq0IbaQ3LlKp0Ar1YZ2j5+w9M2evmVm3Wb8v+T20nDN7zX/9v7P3zKyZDUIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghRFiGgJWhByFEjAwBXwVeB7YGHosQUTFRjvFIEiEolkOSCEG1HJJEZM0gckgSkS0rGUwOSSKyZSuSRIhKJIlIniHH298KbLf4+buBHZavMQwsA24GlgLXAlcCC4DZuN/H3HkNeBk4AxwFDgJ/Bh4FDmD++IkKXLyTDAHvAR4ETlhuX/GXF4CdwGr0h6qStiS5BLgHOGy5PSV8DgN3ATP7ZlUAzSUZwbyFh55opVmOAXcgCmkqyShwwXIbSpzZA1yD6KOpJJuQJF3JaWAtoo+mktzJZEnOA7uB+4E1wGJgLjow9MFM4DLgOuA24HPAXsycDDq/23wPOgWaSrIZeAzYAszzM2RhwXzMafs/Mtj87gSmBRlpxOhiYh6sAZ5gMElED5IkD6YD9wHn0MctayRJPiwFnqJ6fj8QbHQRI0nyYQT4PdVnt64ONrqIkST5MJdqSXaHG1pcjPT8tyTJhxGqP26tDze0OJgFHMGcsp2IJMmHpZQfuB8FZoQbWng+iinERSRJztxH+bxuCTiuoAwxeVWuJMmX6ZRfJzlEpisibqW/GJIkX95H+ZzeEnBcwfguxcV4rOBnJUkelC1Lsb3LNHmGgZew+8wpSbrP+DFpb54LOagQ3EhxIc5TvfBQknSb+ZSvAl4ScFze+TjFRRjk4pAk6Tb7KJ7He0IMZjjEi2LOfRfx6AC/uwOzjHpQtiNJUuKRkv+f1TvIfor/Sqyx2IbeSbrJ+ymevz0hB+WbYxQXYbHldiRJ93g7xXP3dMhB+eYsxUWYW2NbkqRbXE7xvL0YclC+uUhxEepeMZUk3WEW5Wc4s6GscZsgSbqDi/5IClcFkCTdQIKUpA0kSfpIkJK0hSRJGwlSkjaRJOkiQUrSNpIkTSRISVwgSdJDgpTEFZIkLSRISVwiSdJBgpTENZIkDSRISXwgSeJHgpTEF5IkbkL3R3BiKIAkiZcY+iMosRRAksRJLP0RjJgKIEniI6b+CEJsBZAkcRFbf3gnxgJIkniIsT+8EmsBuizJELAI80yyVcBNwFXE+dzbWPvDGzEXoEuSzAFGgV3AKYrHfxp4CPNM5DlhhtlHzP3hhdgLkLokszHfGX8Su/04NfZ7s/0PeRKx94dzUihAqpKsYOovyZwqh4Hlvgc+gRT6wympFCA1STYAr9JMjvGcH9teCFLpD2ekVIBUJNlA+eOU6uYiYSRJqT+ckFoBYpdkBe29cxS9k/j+uJVaf7ROigWIVZLZND/mmCqH8HvgnmJ/tEqqBYhRkvstx1Q3n/WwL+Ok2h+tkXIBYpJkDvancuvmJHCpw32ZSMr90QqpFyAWSUYtx9E0o472o5fU+6MxXShADJLsshxD0+xysA9FdKE/GtGVAoSUZAizTMSnIKfws3arK/1Rmy4VIJQkiyxft60samn8VXSpP2rRtQKEkKTsm4Jdx8c1ka71hzVdLIBvSd5t+XptZVXDcQ9CF/vDiq4WwKckegfpMF0ugC9JdAzSYbpeAB+SDFF+E5SrnERnsbyQQwF8SPJTy9domp/UGGMdcuiPSnIpgGtJNlpuv2k2Wo6vLrn0Ryk5FcClJJcCJyy3XzcngDdY7309cuqPQnIrgEtJPmO57br5VI39rktu/dFHjgVwJcklmPs1XMrx5Njr+CLH/phErgVwJcm7MHf+uZDjHLCs9h7XI9f++B85F8CVJOtwc0/6ugb7Wpec+wNQAVxK0tY7yTnCyEHFmLKhCwWYCewAvljz911+3Dpoue3ePIn/j1UT6UJ/NCL1AiwE9vP/cX+95nZcSTIL+DTwkuX2T2DOVvk8IC8i9f5oTMoFWAn8g/6xP0C9ZRiur5N8CPgx5feun8RcId+Iv+scU5Fyf7RCqgX4CNWf8b8HTKuxXV9rtxZiPoKtwqzKXYSe7h4lqRVgBvBtBmveH479vC0x3OMeC6n1R+ukVIA3Ab/Brnl/hjkOsEWSGFLqDyekUoCbgGexa9rx7KbeZ3pJkk5/OCOFAmzGXAuoI8d4fg3MrfHauUuSQn84JeYCzAC+RTMxJuZ3wPwa48hZkpj7wwuxFuAKYB/tyTGePwEjNcaTqySx9oc3YizAjcAz2DWkTf4KvLnGuHKT5Fri7A+vxFaAUeDfFeNqK08Bb6kxvhwkGQLuBV4hvv7wTiwFmA58o2I8LnIMWFxjrF2W5CrgV0y9T9kQQwFGgL0VY3GZ48CSGmPuoiSjDP6M4WwIXYBlwNGKcfjIi8DSGmPviiQjmDViNvuSDSEL8GH8HG8MklOYxY+2pC7J7cDz2NcrG0IUYDpmWXpoKXrzMrC6xv6kKMk84EHq1yobfBfgcuCRitcNnVeA99bYr5QkuRVzgqJJnbLBZwGWAn+veM1Ych64rcb+xS7JbMyZwjbul88GXwX4INXn1WPLa8CGGvsZqyQrMLfvtlWfbHBdgGnA1ypeJ+ZcAO6ssc8xSTID+ALwH8sxSZAxXBZgAWapeehGb5KLwMdq7HsMkiwBHrcchwTpwVUBbgD+VrH91PKJGjUIJckw8Ema3yIgQXBTgPXA2Yptp5rP16iFb0muwc0qaAlC8wJMA75Ssc0u5Ms16uJLkq2Yazk+6pANbRVgPvDLiu11Kd/E/gkkLiVZCDzsuQbZ0EYB3gE8XbGtLuY7mM/6NriQZD3+vpdEgmBfgHXAvyq20+V8H7Nsxoa2JLkM+EHAfc+GugUYBr5U8fu55EeYZwPb0FSSBdR/wosEsaROAeYDv6j43dzyMGYZhw1NJdlM+1+xIEEKsC3A9ZjbVUM3ZWx5BJhTWel+UpYkG2wKsBZ/pxFTzH7MMnIbUpUkGwYpwDDmuzdCvqWnkj9gjhFsSFGSbJiqAPOAn1f8nNKfA5jnCNuQmiTZUFWA62j+DUm55iBw5aCTMEZKkmRDWQFuB85U/LsydY5g1kbZkIok2VBWAB1vtJNngbdNMQe9T55PQZJsCN1AOeR5zHKcIjZhvnqt98as2CXJhtDNk0tOYJ45PM4VTH4WVdHdizFLkg2hGyen/BO4GXN890LBv6ckSTaEbprcUvXFo6+TjiTZELphlP6kIEk2hG4GpTixS5INoRtBKU/MkmTDq4RvBKU8FzCngicSWpILZESI2zUV+4bc1DNvISU5Q0YcIXwDKFMnJkmOkBF7CT/5ymCJRZJ9ZMR2wk+8MnhikGQnGXEv4SddsUtoSeo8qzhZbiD8hCv2CSnJO8mIIYrXBSnxJ4Qkx7F/qmTy7CT8ZCv1JRntmU+XkjxAhqwm/EQraUhyCxkyBBwm/EQrcUtygIy5i/CTrMQtySYyZibNvxpYCR9XkhzE/kHdneMOwk+wEqcktg/D6yx7CD/BSpySCMxznE4TfoKVdiTZOHl6JUkbrCX85CqSJGq2EX5yFUkSNbrC3p1IEgdMQ5J0KZLEEfq41Z1IEkesRWe3uhJJ4oirgd2En2BFkkTNeuAo4SdZaZbD9F8hlyQtMQPYAhwi/EQrdvkLZn3VjL5ZNUiSFhnC3COwA3iO8JOvFOc45manQe/niE6SrtzGeD3mBqwlmG9WeivwxrHMDDesLLgInMU8DPAZzLv748BvgScwjWzDVsxTbwblbswfSiGyIbp3EiFiQ5IIMQW2kiwPM0whwjGoJNvozjG1EFZMJYnkENlTJonkEGKMXkkkhxA9jEsiOYQoYTmSQwghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCFEYP4LGsFS1woTTe4AAAAASUVORK5CYII=";

    public function __construct()
    {
        parent::__construct();

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }

        $this->load->model("paiement/Model_Paiement", "Mpaiement");
        $this->load->model("typologie/Model_Typologie", "Mtypologie");
        $this->load->model("rejet/Model_Rejet", "Mrejet");
    }

    public function index()
    {
        $this->session->set_userdata('infouser', " IP:". $this->input->ip_address()." LOGIN: ".(int)$this->session->userdata('id_utilisateur'));
        $this->viewPrincipale();
    }

    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();
        $champ                      = array();
        $idPliSaisieEncours         = 0;


        $champ["mPaiement"]         = $this->Mpaiement->modePaiementListe();
        $champ["motifRejet"]        = $this->Mrejet->motifRejet();
        $champ["societe"]           = $this->Mtypologie->societe();

        $head["title"]               =  $head["menu"] = "Typage pli";
        $head["icon"]                = "label";
        $head["theme"]               = 'theme-teal';
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/nprogress/nprogress',
            'plugins/magnify/jquery.magnify',
            'plugins/metisMenu/font-awesome',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/sweetalert/sweetalert',
            'css/style',
            'css/themes/all-themes',
            'css/custom/typage','../src/template/css/font-awesome.min'
        );


        $foot['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/magnify/jquery.magnify',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'plugins/nprogress/nprogress',
            'js/pages/ui/notifications',
            'js/admin',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/custom/typage'
        );




        /*$this->saisieEncours = $this->saisieEncours();

        if(count($this->saisieEncours) > 0 )
        {
            $idPliSaisieEncours = (int)$this->saisieEncours[0]->id_pli;
        }*/


       $this->load->view("main/header",$head);
       /* if($idPliSaisieEncours > 0)
        {
            $this->load->view("typage/Encours",array("idPliSaisie"=>$idPliSaisieEncours));
        }*/

        $this->load->view("typage/Visualisation");
        $this->load->view("typage/Champ",$champ);
        $this->load->view("typage/ErreurCheque");
        $this->load->view("typage/koScan",$champ);

        $this->load->view("main/footer",$foot);
        

    }

    public function dynamiqueCheque()
    {
        $data                   = array();
        $valueCheque            = $this->input->post("classDiv");
        $cmc7                   = (trim($this->input->post("cmc7")) != "") ? $this->input->post("cmc7") : "";
        $data["anomalieCheque"] = $this->Mtypologie->listeAnomalieCheque();
        $data["valueCheque"]    = $valueCheque;
        $data["cmc7"]           = $cmc7;
        
        $data["doc"]            = (trim($this->input->post("infoDoc")) != "") ? $this->input->post("infoDoc") : "";
        
        $data["currentDoc"]     = intval(preg_replace("/[^0-9]/", "", $data["doc"]));
        $data["idDoc"]          = $this->input->post("idDoc");
        $this->load->view("typage/Cheque",$data,false);

    }

    
    public function listeTitre($idSociete)
    {
        if((int)$idSociete == 0 )
        {
            log_message('error', $this->session->userdata('infouser').'Pas de soiete recuperer');
        }

        $titre = $this->Mtypologie->listeTitre((int)$idSociete);

        $strTitre          = "";
        $strTitre         .= '<select class="form-group form-control" id="infoTitre" data-live-search="true">';
        $strTitre         .= '<option value="0" selected disabled="disabled">--Choisir un titre--</option>';

        if($titre)
        {
            foreach ($titre as $itemTitre)
            {
                $strTitre  .= '<option value="'.(int)$itemTitre->id.'">'.$itemTitre->code.' - '.$itemTitre->titre.'</option>';
            }
        }

        $strTitre .= '</select>';
        $strTitre .='<label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Titre : </label>';
        echo $strTitre;
    }


    /*Test  ra manana pli en cours, si non maka*/
    public function loadPli($societe)
    {
        $getPli         = $this->Mtypologie->getPli($societe);
        echo json_encode($getPli);
    }


    /*alaiana le pli sa alefa any view*/
    public function recuperPliUtilisateur()
    {
        $doc        = null;
        $mag        = null;
        $statut     = 1;
        $pliInfo    = null;

        $idPli      = $this->Mtypologie->getPliID();
        $pliID = 0;
        $listeDocument = array();
        if(count($idPli) > 0)
        {
            $pliID      = (int)$idPli[0]->id_pli;
            $societe     = (int)$idPli[0]->societe;
            if((int)$pliID > 0)
            {
                $pliInfo = $this->Mtypologie->recuperPliUtilisateur((int)$pliID); 
                
                foreach($pliInfo as $valueIDdoc)
                {
                    $listeDocument[] = $valueIDdoc->id_document;
                }

                $statut  = 0;
                $doc     = $this->load->view("typage/Magnify",array("resultat"=>$pliInfo),true);
                $mag     = $this->load->view("typage/Document",array("info"=>$pliInfo),true);
            }
            else
            {
                log_message('error', $this->session->userdata('infouser').' erreur ajax loadPli user');
            }
        }


        echo json_encode(array("docu"=>$doc,"magn"=>$mag,"error"=>$statut,"idPli"=>(int)$pliID,"societe"=>(int)$societe,"id_document"=> $listeDocument));
    }

    /*reefa avy micklic doc any @ Metis menu*/ 
    public function documentParID($id)
    {
        $arrayDoc   = array();
        $noImage    = $this->noImage;
        $doc        = $this->Mtypologie->getDocID((int)$id);

       $arrayDoc   = array("recto64"=>(trim($doc[0]->n_ima_base64_recto)== "" ?$noImage:$doc[0]->n_ima_base64_recto),
                            "nomRecto"=>(trim($doc[0]->n_ima_recto) == null ? "Image pas de nom ": $doc[0]->n_ima_recto),
                            "verso64"=>(trim($doc[0]->n_ima_base64_verso) == "" ? $noImage : $doc[0]->n_ima_base64_verso ),
                            "nomVerso"=>(trim($doc[0]->n_ima_verso) == "" ? 'Image pas de nom' :$doc[0]->n_ima_verso ));

       echo json_encode($arrayDoc);
    }

    public function saisieEncours()
    {
        return $this->Mtypologie->saisieEncours();
    }

    public function annulerTypage($idPli)
    {
        $json = $this->Mtypologie->annulerTypage($idPli);
        echo json_encode($json);
    }


    public function koScan()
    {

        $arrayPost      = array();
        $dataPli        = array();
        $idPli          = $this->input->post("idPli");
        $motifRejet     = $this->input->post("motifRejet");
        $description    = $this->input->post("description");
        $motifOperateur    = $this->input->post("motifOperateur");
        $arrayPost      = array("utilisateurs_id"=>(int)$this->session->userdata('id_utilisateur'),"motif_rejet_id"=>$motifRejet,"pli_id"=>(int)$idPli,"description"=>$description);
        $motifConsigne  = array("id_pli"=>$idPli,
                                "motif_operateur"=>$motifOperateur,
                                "id_flg_trtmnt"=>2,
                                "id_stt_ss"=>2
                            );
        $dataPli        = array("motif_rejet"=>$motifRejet,"motif_ko"=>$motifRejet,"statut_saisie"=>2,"autre_motif_ko"=>$description);
        $json           = $this->Mtypologie->koScan($arrayPost,$motifConsigne,$dataPli);
        echo json_encode($json);

    }
    public function hpTypage($idPli) 
    {
        $json = $this->Mtypologie->hpTypage($idPli);
        echo json_encode($json);
    }
    public function validerTypage()
    {
        log_message('error', $this->session->userdata('infouser').' DEBUT  : VALIDATION TYPAGE ');

        $arrayInfo      = array();
        $listeCheque    = array();
        $modePaie       = array();
        $typage         = array();
        $statutTypage   = array();

        $mouvement      = array();
        $dataPli        = array();
        $paiementCheque = array();
        $paiement       = array();
        $pli            = array();
        $listeAnomalieChq = array();
        $paiementChqAnomalie = array();

        $infoSociete    = "";
        $typologie      = 0;
        $nbAbonment     = 0;
        $infoDatamatrix = "";
        $codePromo      = "";
        $typeCoupon     = "";
        $idPli          = 0;
        $retPaiemnt     = 0;
        $retPaieCheque  = 0;
        $statutSaisie   = 1;

        $arrayInfo      = $this->input->post("autreInfo");
        $listeCheque    = $this->input->post("listeCheque");
        $modePaieCheque = 1;

        
        $avecCodePromo     = 0;
        $avecNumeroAbonne  = 0;
        $avecNumeroPayeur  = 0;

        $listeCodePromo  = array();
        $listeNumAbonne  = array();
        $listeNumPayeur  = array();
        $etatIgnorer     = array();

        $mouvement       = $this->input->post("mouvement");
        $nbMv              = count($this->input->post("mouvement"));
        
        
        $modePaie                       = $arrayInfo["modPaie"];
        $typologie                      = (int)$arrayInfo["typologie"];
        $idPli                          = (int)$arrayInfo["idPli"];

        $dataPli["societe"]             = $arrayInfo["infoSociete"];
        $dataPli["infos_datamatrix"]    = $arrayInfo["infoDatamatrix"];
        $dataPli["titre"]               = $arrayInfo["infoTitre"];
        /*$dataPli["code_promotion"]      = $arrayInfo["codePromo"];*/
        
        $dataPli["flag_traitement"]     = 2;
        $dataPli["type_coupon"]         = $arrayInfo["typeCoupon"];
        $dataPli["statut_saisie"]       = 1;

        $nbPaiement                     = count($modePaie);
        $dataPli["mvt_nbr"]             = $nbMv;

        
        
         
        $tempPaiementChqAnomalie = $this->input->post("listeAnomalieChq");

        for($iTempChAnomalie = 0 ; $iTempChAnomalie < count($tempPaiementChqAnomalie);$iTempChAnomalie++)
        {
            $tempListeAnomaie = $tempPaiementChqAnomalie[$iTempChAnomalie]["anomalie_cheque_id"];
            $documentID       = intval( $tempPaiementChqAnomalie[$iTempChAnomalie]["id_document"]);
            $idPli_           = intval( $tempPaiementChqAnomalie[$iTempChAnomalie]["id_pli"]);
            for($iTempListeAnomalie = 0; $iTempListeAnomalie < count($tempListeAnomaie);$iTempListeAnomalie++)
            {
                $paiementChqAnomalie[]      = array("id_doc"=>$documentID,"id_pli"=>$idPli_,"id_anomalie_chq"=>intval($tempListeAnomaie[$iTempListeAnomalie]));
                $listeAnomalieChq[]         = intval($tempListeAnomaie[$iTempListeAnomalie]);
            } 
             
            $listeAnomalieChq = array_unique($listeAnomalieChq);
            if(count($listeAnomalieChq) > 0)
            {
                $etatPli                      = $this->Mtypologie->getEtatPliByAnomalieChq($listeAnomalieChq);
                if($etatPli)
                {
                    $statutSaisie             = intval($etatPli[0]->etat_pli);  
                }
            }
        }
        
       log_message('error', $this->session->userdata('infouser').' Typage  : Nombre anomalie =  '.intval($iTempChAnomalie)." #".$idPli);
       if($statutSaisie == 7 || $statutSaisie == 5) 
       {
            $statutSaisie = 1;
       }
        
        $dataPli["statut_saisie"]           = $statutSaisie;
        $dataPli["typologie"]               = $typologie;
        $dataPli["code_ecole_gci"]          = $arrayInfo["codeEcoleGci"];
        
        if(count($paiementChqAnomalie) > 0)
        {
            $anomalieCheque = $this->Mtypologie->anomalieCheque($paiementChqAnomalie,$arrayInfo["idPli"]);
        }
        
        if( $idPli <= 0)
        {
            $statutTypage = array("statut"=>'KO',"methode"=>'validerTypage',"comment"=>'Erreur lors du paiement pli# non trouvé');
            log_message('error', $this->session->userdata('infouser').'methode=>validerTypage,comment=>Erreur lors du paiement pli# non trouvé');
            echo json_encode($statutTypage);
            return "";
        }

        for($j = 0 ;$j < $nbPaiement ;$j++ )
        {
            $paiement[$j]["id_mode_paiement"] = $modePaie[$j];
            $paiement[$j]["id_pli"]           = $idPli;
        }


        $retPaiemnt = $this->Mtypologie->paiement($paiement);
        
        if($retPaiemnt > 0)
        {
            $retMvtmnt = $this->Mtypologie->mouvement($mouvement);

            if($retMvtmnt <= 0 )
            {
                log_message('error', $this->session->userdata('infouser').'Erreur de mouvement. pli#'.$idPli);
                $statutTypage = array("statut"=>'KO',"methode"=>'Mtypologie->mouvement',"comment"=>'Erreur lors de la création du mouvement pli#'.$idPli);
                echo json_encode($statutTypage);
                return "";
            }
            if(in_array($modePaieCheque,$modePaie))
            {
                log_message('error', $this->session->userdata('infouser').' Typage  : modePaieCheque  #'.$idPli);
                $dataPli["avec_chq"]              = 1;
                
                $retPaieCheque  = $this->Mtypologie->cheque($listeCheque,(int)$arrayInfo["idPli"]);

                $this->Mtypologie->insererEtatCheque($idPli); /*set etat cheque */

                if( $retPaieCheque <= 0 )
                {
                    log_message('error', $this->session->userdata('infouser').'Erreur de paiement cheque. pli#'.$idPli);
                    $statutTypage = array("statut"=>'KO',"methode"=>'Mtypologie->cheque',"comment"=>'Erreur lors du paiement cheque pli#'.$idPli);
                    echo json_encode($statutTypage);
                    return "";
                }
            }

            $pli = array(
                "flag_traitement"=>2,
                "typologie"=> $typologie,
                "societe"=>intval($dataPli["societe"]),
                "statut_saisie"=>$statutSaisie,
                "date_typage"=>date("Y-m-d H:i:s")
            );
            if(intval($statutSaisie) > 1 )
            {
                $dataPli["date_dernier_statut_ko"]             = date("Y-m-d H:i:s");
            }
            log_message('error', $this->session->userdata('infouser').' Debut : MAJ data_pli '.(int)$idPli);
            $ret = $this->Mtypologie->dataPli((int)$arrayInfo["idPli"], $dataPli,$pli,$listeAnomalieChq);
            
            switch ((int)$ret)
            {
                case 0:
                    $statutTypage = array("statut"=>'KO',"methode"=>'Mtypologie->dataPli',"comment"=>'Erreur du serveur. Réessayer #'.$idPli);
                    log_message('error', $this->session->userdata('infouser').'Erreur du serveur. Réessayer #'.$idPli);
                    echo json_encode($statutTypage);
                    return "";
                    break;
                
                case  2:
                    $statutTypage = array("statut"=>'KO',"methode"=>'Mtypologie->dataPli',"comment"=>'Le pli#'.$idPli.' est déjà typé par d\'autre utilisateur');
                    log_message('error', $this->session->userdata('infouser').'Le pli#'.$idPli.' est déjà typé par d\'autre utilisateur');
                    echo json_encode($statutTypage);
                    return "";
                    break;
            }
        }
        else
        {

            log_message('error', $this->session->userdata('infouser').'Erreur de paiement. pli#'.$idPli);
            $statutTypage = array("statut"=>'KO',"methode"=>'Mtypologie->paiement',"comment"=>'Erreur lors du paiement pli#'.$idPli);
            echo json_encode($statutTypage);
            return "";
        }

        $this->Mtypologie->getPli(10);
        log_message('error', $this->session->userdata('infouser').' FIN OK : MAJ data_pli '.(int)$idPli);
        echo json_encode(array("statut"=>"OK"));
        return "";
    }

    public function listeTypologie($idSociete)
    {
        if((int)$idSociete == 0 )
        {
            log_message('error', $this->session->userdata('infouser').'Pas de soiete recuperer');
        }

        $listeTypologie        = $this->Mtypologie->litesTypologies((int)$idSociete);

        $strTypologie          = "";
        /*$strTypologie         .= '<select class="form-group form-control" id="typologie" style="max-height: 65px;"  onchange="getProvenanceType();" data-live-search="true" data-show-subtext="true">';*/
        $strTypologie         .= '<select class="form-group form-control" id="typologie" style="max-height: 65px;"  data-live-search="true" data-show-subtext="true">';
        $strTypologie         .= '<option value="0" selected disabled="disabled">--Choisir une typologie--</option>';

        foreach ($listeTypologie as $itemTypologie)
        {
            $strTypologie      .= '<option value="'.(int)$itemTypologie->id_typologie.'" data-subtext="<em>('.$itemTypologie->type.')</em>">'.$itemTypologie->typologie.'</option>';
        }


        $strTypologie           .= '</select>';
        $strTypologie           .='<label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Typologie : </label>';
        echo $strTypologie;

    }
    public function preData($idPli)
    {
        $json = $this->Mtypologie->preData($idPli);

        
        echo json_encode($json);
    }


    public function modePaiemntCheque()
    {
        $paiement = $this->Mpaiement->modePaiementListe();



        $strModePaiement = "<div class='form-line'>";

        $strModePaiement    .= '<select class="form-group form-control mode_paiement_0" id="mode-paiement" onchange="verifModePaiement(\'mode_paiement_0\',\'\');" multiple data-live-search="true">';
        foreach ($paiement as $itemMpaiemnt)
        {
            $selected  = ' ';
            if((int)$itemMpaiemnt->id_mode_paiement == 1)
            {
                $selected = " selected ";
            }
            $strModePaiement    .= '<option value="'.$itemMpaiemnt->id_mode_paiement.'"  '.$selected.'>'.$itemMpaiemnt->mode_paiement.'</option>';
        }
        $strModePaiement        .= '</select>';
       echo $strModePaiement        .="<label class='form-label' style='top: -10px;left: 0;font-size: 12px;z-index: 999;'>Mode de paiement : </label></div>";
       return "";
    }

    public function listeProvenance($idSociete)
    {
        if((int)$idSociete == 0 )
        {
            log_message('error', $this->session->userdata('infouser').'Pas de soiete recuperer');
        }

        $titre = $this->Mtypologie->listeProvenance((int)$idSociete);

        $strTitre          = "";
        $strTitre         .= '<select class="form-group form-control" id="provenance-id" data-live-search="true">';
        $strTitre         .= '<option value="0" selected disabled="disabled">--Choisir--</option>';

        if($titre)
        {
            foreach ($titre as $itemTitre)
            {
                $strTitre  .= '<option value="'.(int)$itemTitre->id.'">'.$itemTitre->provenance.'</option>';
            }
        }

        $strTitre .= '</select>';
        $strTitre .='<label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Provenance : </label>';
        echo $strTitre;
    }
    public function checkProvenance($typologie,$societe)
    {
        $checkProvenance = $this->Mtypologie->checkProvenance((int)$typologie,(int)$societe);
        $str             = '<select id="provenance-type-id" class="form-group form-control" data-live-search="true">';
        $strO            = "";

        if($checkProvenance)
        {
            $retour = array("type"=>$checkProvenance[0]->type,"provenanceID"=>$checkProvenance[0]->provenance_type_id);
            
            if((int)$checkProvenance[0]->provenance_type_id == 1)
            {
                $strO  = '<option value="0" disabled="disabled"> --Choisir --</option>';
                $strO .= '<option value="1" selected >Réseau</option>';
                $strO .= '<option value="2" disabled="disabled">Courrier</option>';
            }
            else if($checkProvenance[0]->provenance_type_id == 2)
            {
                $strO  = '<option value="0" disabled> --Choisir --</option>';
                $strO .= '<option value="2" selected >Courrier</option>';
                $strO .= '<option value="1" disabled="disabled" >Réseau</option>';
            }
        }
        else
        {
            $str .= '<option value="0" selected disabled="disabled"> --Choisir --</option>';
        }

        $str    .=$strO;
        $str    .= '</select>';
        echo $str  .="<label class='form-label' style='top: -10px;left: 0;font-size: 12px;z-index: 999;'>Type de provenance : </label></div>";
    }

    public function checkIfStandBy($idPli)
    {
        $checkID = $this->Mtypologie->standBy($idPli);
        if($checkID)
        {
            echo 1;
        }
        else echo 0;
    }
    

    public function mettrePliKO($idPli,$idKO)
    {
        $this->load->model("typologie/Model_koTypage", "Mko");

        $pli                            = array();
        $dataPli                        = array();
        $pli["typage_par"]              = intval($this->session->userdata('id_utilisateur'));
        $pli["flag_traitement"]         = 2 ;
        $dataPli["flag_traitement"]     = 2 ;
        $motifConsigne                  = array();
        /*motif */
        $motif = $this->input->post("motif");

        if(trim($motif) != "")
        {
            $motifConsigne["motif_operateur"] = $motif;
            $motifConsigne["id_pli"]        = $idPli;
            $motifConsigne["id_flg_trtmnt"] = 2;
            $motifConsigne["id_stt_ss"]     = intval($idKO);
        }
        if(intval($idKO) <= 1)
        {
            echo "0";
            return "";
        }
        
        $pli["statut_saisie"]           = intval($idKO);
        $dataPli["statut_saisie"]       = intval($idKO);

        $pli["date_typage"]             = date("Y-m-d H:i:s");
        $dataPli["date_dernier_statut_ko"]             = date("Y-m-d H:i:s");
        
        echo intval($this->Mko->mettrePliKO($pli,$dataPli,$idPli,$motifConsigne));
        return "";
    }


    /***
     * type :
     * 1 : n° payeur
     * 2 : nom payeur
     * 3 : n° abonnee 
     * 4 : code_promo
     * */

    public function setPredataTypage($idPli,$type)
    {
        $champ = "";
        $retour = "";
        /*abonne_destinataire */
        switch($type)
        {
            case 1:
                $champ = " predata.abonne_payeur ";
            break;

            case 2:
                $champ = "";
            break;
    
            case 3:
                $champ = " predata.abonne_destinataire ";
            break;

           
            case 4:
                $champ = " predata.code_promo ";
            break;
        }

        if(trim($champ) != "")
        {
            $predata = $this->Mtypologie->preDataNomPayeur($idPli,$champ);
            if($predata)
            {
                $j = 0;
                foreach($predata as $valueData)
                {
                    if($j == 0)
                    {
                        $retour .= $valueData->champ;
                    }
                    else
                    {
                        $retour .= " - ".$valueData->champ;
                    }
                    

                    $j++;
                }
                echo $retour;
                return "";
            }
            echo "";
            return "";
        }
        if(trim($champ) == "" && $type == 2)
        {
            $this->load->model("typologie/Model_PreData", "PreData");
            $nomPayeur = "";
            $numero             = $this->PreData->numeroCodepromo($idPli);
            if($numero)
            {
                foreach($numero as $k)
                {
                    $nom = $this->PreData->getNomPayeur(trim($k->abonne_payeur));
                    $nomPayeur = ($nom) ? $nom[0]->nom : "";
                }
            }
            echo $nomPayeur;
            return "";
           
        }
        echo "";
        return "";

    }
    
	
	public function getConsigne($idPli) 
	{
        $retour = array();
        $retour["ok"] = "0";
        if(intval($idPli) > 0)
		{
			$this->load->model("traitement_ko/Model_Traitement_KO", "MtraitementKO");
			
			$motifConsigne = $this->MtraitementKO->getMotifConsigne(array($idPli));
			if($motifConsigne)
			{
                $obj = $motifConsigne[0];
                $retour["ok"] = "1";
                $retour["id_pli"] = $obj->id_pli;
                $retour["consigne_client"] = $obj->consigne_client;
                $retour["nb_consigne"] = $obj->nb_consigne;
                $retour["id_motif"] = $obj->id_motif;
                
			}
			
		}
		
	    echo json_encode($retour);
		return "";
    }
    
    public function preDataNompPayeurBloc($idPli)
    {
        $this->load->model("typologie/Model_PreData", "PreData");
        
        $numero             = $this->PreData->numeroCodepromo($idPli);
        $nombreBloc         = 1;
        $aBlocContent       = array();
        $strContentBloc     = "";
        $nombrePayeur       = 0;
        if($numero)
        {
            $nombrePayeur   = count($numero);
            $strBounton    = "";
            foreach($numero as $k)
            {
                $nomPayeur = "";
                if(trim($k->abonne_payeur) != "")
                {
                    $nom = $this->PreData->getNomPayeur(trim($k->abonne_payeur));
                    $nomPayeur = ($nom) ? $nom[0]->nom : "";
                }
                $doc =  (trim($k->id_document) != "") ? "  (Document : ".$k->id_document.")" : "";
                $numeropayeur = (trim($k->abonne_payeur) != "") ? $k->abonne_payeur : "000000000000";
                $numeroabonne = (trim($k->abonne_destinataire) != "") ? $k->abonne_destinataire : "000000000000";
                $promo = (trim($k->code_promo) != "") ? $k->code_promo : "";
                if( $nombreBloc == $nombrePayeur && $nombrePayeur > 1 )
                {
                    $strBounton = "
                            <button type='button'  tabindex='-1' onclick='ajouter_bloc_payeur(".intval($nombreBloc).");' class='ajouter_bloc_payeur-".intval($nombreBloc)." btn btn-primary btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                                <i class='material-icons'><b>plus_one</b></i>
                            </button>
                            <button type='button' tabindex='-1' onclick='supprimer_bloc_payeur(".intval($nombreBloc).");' class='supprimer_bloc_payeur-".intval($nombreBloc)." btn bg-blue-grey btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                            <i class='material-icons'><b>remove</b></i>
                            </button>" ;
                }
                else if($nombreBloc == $nombrePayeur && $nombrePayeur <= 1)
                {
                    $strBounton = "
                    <button type='button' tabindex='-1' onclick='ajouter_bloc_payeur(".intval($nombreBloc).");' class='ajouter_bloc_payeur-".intval($nombreBloc)." btn btn-primary btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                        <i class='material-icons'><b>plus_one</b></i>
                    </button>
                    <button type='button' tabindex='-1' style='display:none;' onclick='supprimer_bloc_payeur(".intval($nombreBloc).");' class='supprimer_bloc_payeur-".intval($nombreBloc)." btn bg-blue-grey btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                    <i class='material-icons'><b>remove</b></i>
                    </button>" ;
                }
                else if($nombreBloc < $nombrePayeur)
                {
                    $strBounton = "
                    <button type='button'  tabindex='-1' style='display:none;' onclick='ajouter_bloc_payeur(".intval($nombreBloc).");' class='ajouter_bloc_payeur-".intval($nombreBloc)." btn btn-primary btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                        <i class='material-icons'><b>plus_one</b></i>
                    </button>
                    <button type='button' tabindex='-1' style='display:none;' onclick='supprimer_bloc_payeur(".intval($nombreBloc).");' class='supprimer_bloc_payeur-".intval($nombreBloc)."  btn bg-blue-grey btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                    <i class='material-icons'><b>remove</b></i>
                    </button>" ;
                }

                $strContentBloc .= " 
                <div class='row clearfix dynamique-bloc-payeur bloc-payeur-".intval($nombreBloc)."'>
                    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                        <div class='card'>
                            <span class='badge bg-teal'> # ".intval($nombreBloc).$doc."</span>
                            <div class='body'>
                                <div class='form-group form-float'>
                                    <div class='form-line'>
                                        <input type='text' value='".$numeropayeur."' class='form-control' minlength='-1' maxlength='12' name='numeropayeur' id='numeropayeur-".intval($nombreBloc)."' onkeyup='if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')' />
                                        <label class='form-label'>Numéro payeur :</label>
                                    </div>
                                </div>
                                <div class='form-group form-float'>
                                    <div class='form-line'>
                                        <input type='text' class='form-control' value='".$nomPayeur."'  maxlength='30' id='nompayeur-".intval($nombreBloc)."' name='nompayeur' />
                                        <label class='form-label'>Nom payeur : </label>
                                    </div>
                                </div>
                                <div class='form-group form-float'>
                                    <div class='form-line'>
                                        <input type='text' class='form-control' value='".$numeroabonne."' id='numeroabonne-".intval($nombreBloc)."' maxlength='12' onkeyup='if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')' />
                                        <label class='form-label'>Numéro abonné : </label>
                                    </div>
                                </div>
                                <div class='form-group form-float'>
                                    <div class='row'>
                                        <div class='col-sm-8'>
                                            <div class='form-line'>
                                                <input type='text' class='form-control' value='".$promo."' id='promo-".intval($nombreBloc)."' />
                                                <label class='form-label'>Code promotion : </label>
                                            </div>
                                        </div>
                                        <div class='col-sm-4'>
                                           ".$strBounton."
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
                $nombreBloc++;
            }

           
        }
        else
        {
            $strContentBloc = "
            <div class='row clearfix dynamique-bloc-payeur bloc-payeur-".intval($nombreBloc)."'>
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class='card'>
                    <span class='badge bg-teal'> # ".intval($nombreBloc)."</span>
                    <div class='body'>
                        <div class='form-group form-float'>
                            <div class='form-line'>
                                <input type='text' value='000000000000' class='form-control' minlength='-1' maxlength='12' name='numeropayeur' id='numeropayeur-".intval($nombreBloc)."' onkeyup='if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')' />
                                <label class='form-label'>Numéro payeur :</label>
                            </div>
                        </div>
                        <div class='form-group form-float'>
                            <div class='form-line'>
                                <input type='text' class='form-control' value=''  maxlength='30' id='nompayeur-".intval($nombreBloc)."' name='nompayeur' />
                                <label class='form-label'>Nom payeur : </label>
                            </div>
                        </div>
                        <div class='form-group form-float'>
                            <div class='form-line'>
                                <input type='text' class='form-control' value='000000000000' id='numeroabonne-".intval($nombreBloc)."' maxlength='12' onkeyup='if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')' />
                                <label class='form-label'>Numéro abonné : </label>
                            </div>
                        </div>
                        <div class='form-group form-float'>
                            <div class='row'>
                                <div class='col-sm-8'>
                                    <div class='form-line'>
                                        <input type='text' class='form-control' value='' id='promo-".intval($nombreBloc)."' />
                                        <label class='form-label'>Code promotion : </label>
                                    </div>
                                </div>
                                <div class='col-sm-4'>
                                    <button type='button' tabindex='-1' onclick='ajouter_bloc_payeur(".intval($nombreBloc).");' class='ajouter_bloc_payeur-".intval($nombreBloc)." btn btn-primary btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                                        <i class='material-icons'><b>plus_one</b></i>
                                    </button>
                                    <button type='button' style='display:none;' tabindex='-1' onclick='supprimer_bloc_payeur(".intval($nombreBloc).");' class='supprimer_bloc_payeur-".intval($nombreBloc)." btn bg-blue-grey btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                                        <i class='material-icons'><b>remove</b></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            ";
        }
        


         echo $strContentBloc;
    }

    function ajouterBlocPayeur($blocCourant)
    {
        $nombreBloc = $blocCourant + 1;
        $strContentBloc = "
        <div class='row clearfix dynamique-bloc-payeur bloc-payeur-".intval($nombreBloc)."'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='card'>
                <span class='badge bg-teal'> # ".intval($nombreBloc)."</span>
                <div class='body'>
                    <div class='form-group form-float'>
                        <div class='form-line'>
                            <input type='text' value='000000000000' class='form-control' minlength='-1' maxlength='12' name='numeropayeur' id='numeropayeur-".intval($nombreBloc)."' onkeyup='if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')' />
                            <label class='form-label'>Numéro payeur :</label>
                        </div>
                    </div>
                    <div class='form-group form-float'>
                        <div class='form-line'>
                            <input type='text' class='form-control' value=''  maxlength='30' id='nompayeur-".intval($nombreBloc)."' name='nompayeur' />
                            <label class='form-label'>Nom payeur : </label>
                        </div>
                    </div>
                    <div class='form-group form-float'>
                        <div class='form-line'>
                            <input type='text' class='form-control' value='000000000000' id='numeroabonne-".intval($nombreBloc)."' maxlength='12' onkeyup='if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')' />
                            <label class='form-label'>Numéro abonné : </label>
                        </div>
                    </div>
                    <div class='form-group form-float'>
                        <div class='row'>
                            <div class='col-sm-8'>
                                <div class='form-line'>
                                    <input type='text' class='form-control' value='' id='promo-".intval($nombreBloc)."' />
                                    <label class='form-label'>Code promotion : </label>
                                </div>
                            </div>
                            <div class='col-sm-4'>
                                <button type='button' tabindex='-1' onclick='ajouter_bloc_payeur(".intval($nombreBloc).");' class='ajouter_bloc_payeur-".intval($nombreBloc)." btn btn-primary btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                                    <i class='material-icons'><b>plus_one</b></i>
                                </button>
                                <button type='button' style='display:inline;' tabindex='-1' onclick='supprimer_bloc_payeur(".intval($nombreBloc).");' class='supprimer_bloc_payeur-".intval($nombreBloc)." btn bg-blue-grey btn-circle waves-effect waves-circle waves-float' data-toggle='tooltip' data-placement='top'>
                                    <i class='material-icons'><b>remove</b></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";

    echo $strContentBloc;
        
    }

   
	
}
