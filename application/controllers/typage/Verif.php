<?php

class Verif extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if(intval($this->session->userdata('id_utilisateur')) == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
            echo  json_encode(array("session"=>"ko"));
            return "";
        }
        echo  json_encode(array("session"=>"ok"));
        return "";
    }

   /* public function ecrireLog()
    {

        $log = trim($this->input->post("log"));
        log_message('error', $this->session->userdata('infouser').' '.$log);
    }*/

}