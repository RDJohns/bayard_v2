<?php

class Typage_auto extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        /*if ((int)$this->session->userdata('id_utilisateur') == 0) {
            log_message('error', $this->session->userdata('infouser') . ' session perdue');
        }*/

        //$this->load->model("paiement/Model_Paiement", "Mpaiement");
        //$this->load->model("typologie/Model_Typologie", "Mtypologie");
        $this->load->model("typage/Model_typage_auto", "Mtypage");
    }

    public function index()
    {
        //$this->session->set_userdata('infouser', " IP:" . $this->input->ip_address() . " LOGIN: " . (int)$this->session->userdata('id_utilisateur')); 
        $this->get_plis();
    }

    public function get_plis(){
        log_message_typage('error','info ==> Lancement typage auto.');
        $plis = $this->Mtypage->get_pli_to_flag();

        log_message_typage('error','Nombre plis en typage auto : '.count($plis));
        /*echo 'liste de plis a flaguer';
        foreach($plis as $pli){
            echo $pli->id_pli.' : '.$pli->type_document.'<br>';
        }*/

        $plis_manuel = $this->Mtypage->get_pli_to_manuel();

        log_message_typage('error', 'Nombre plis en typage manuel : '.count($plis_manuel));
        /*echo 'liste de plis a flaguer manuel';
        foreach($plis_manuel as $pli){
            echo $pli->id_pli.' : '.$pli->type_document.'<br>';
        }*/

        $upd = $this->Mtypage->update_flag_batch();
        if($upd == ''){
            $this->Mtypage->get_pli_for_typage();
        }

        log_message_typage('error', 'info ==> Typage auto terminé.');
    }

}