<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Cron_reb_temp extends CI_Controller
{

	public function __construct()
	{
        parent::__construct();
        $this->load->model('cron/Model_cron_reb_temp', 'rebtemp');
    }

    public function index()
    {
    	//echo'ok';
    		/*if($this->mcron->have_today()){
			log_message('error', 'REB today already exist; process aborted.');
		}else{*/
			log_message_reb('error', 'info=> lancement de cron_reb.');
			$this->insert_reb_temp();
		//}

    }

	 public function insert_reb_temp()
	 {
		$synchro = $this->rebtemp->get_flag_synchro();
		//var_dump($synchro);
		if ($synchro) {
			foreach ($synchro as $sync) {
				if ($sync->flag == 1) {
					$list_pli = $this->rebtemp->get_pli_from_ged();
        			//var_dump($list_pli);
			        if($list_pli){
						$nb_reb = 0;
			            foreach ($list_pli as $item){
			                $id_pli = $item->id_pli;
			                $code = '1301';
			                $lot_scan = str_replace('_', '-', $item->lot_scan);
			                $idenveloppe = $lot_scan.'_'.$item->pli;
			                $num_image_chq = modif_nom_fichier_image( $item->n_ima_recto );
			                $cmc = $item->cmc7;
							$cmc_nostr = preg_replace('/[^0-9]/', '', $cmc);
							$date_tri = $item->date_tri;
							$date_create = new DateTime();
							$date_creation = $date_create->format("Y-m-d H:i:s");
							$montant = trim($item->montant);
			            	$montant = str_replace(',', '.', $montant);
			            	$montant = is_numeric($montant) ? $montant * 100 : 0;
							$num_serie = substr($cmc_nostr, 0, 7);
			            	$num_zib = substr($cmc_nostr, 7, 12);
							$num_compte = substr($cmc_nostr, 19);
							$recto = modif_nom_fichier_image($item->n_ima_recto);
							$verso = modif_nom_fichier_image($item->n_ima_verso);
			                $list_ima_chq = implode(";", array($recto, $verso));
			                $commande = $item->commande;
			                $n_lot = $item->commande.'0001';
			                $typo = $item->typologie;
			                $fic_tri = $item->fichier_tri;
			                //$saisie = $item->libelle;

			                if ($item->societe == 1) {
			                	$societe = '00';
			                }else{
			                	$societe = '01';
			                }

			                if($item->etat_reb ==1) {
			                	$traitement = 'OK';
			                	$code = '1300';
			                }elseif($item->etat_reb == 2){
			                	$traitement = 'KO';
			                	$code = '1104';
			                }else{
				                $saisie = $item->statut_saisie;
				                if($saisie == 1){
				                	$traitement = 'OK';
				                	$code = '1300';
				                }elseif ($saisie == 3){
				                	$traitement = 'CI';
				                	$code = '1301';
				                }else{
				                	$traitement = 'KO';
				                	$code = '1104';
				                }
			                }
							$data_to_save = array(
								'idenveloppe' => $idenveloppe
								,'titre' => '00'
								,'numero_image_cheque' => $num_image_chq
								,'date_tri' => $date_tri
								,'date_creation' => $date_creation
								,'montant' => $montant
								,'numero_serie' => $num_serie
								,'zib' => $num_zib
								,'numero_compte' => $num_compte
								,'list_ima_chq' => $list_ima_chq
								,'commande' => $commande
								,'n_lot' => $n_lot
								,'fichier_tri' =>$fic_tri
								,'id_pli' => $id_pli
								,'id_document' => $item->id_doc
								,'typologie' => $typo
								,'traitement' =>$traitement
								,'titre' =>$societe
								,'code' =>$code
							);
							if(strlen($cmc_nostr) == 31){
								
								if($this->rebtemp->save_reb($data_to_save, $item->id_doc)){
									$nb_reb++;
								}
							}else{
								log_message_reb('error', 'cron> cron_pli_reb> insert_reb -> CMC7 INVALIDE: pli#'.$id_pli.'> doc#'.$item->id_doc.'> cmc7#'.$item->cmc7);
							}
						}
						log_message_reb('error', 'info=> cron> cron_pli_reb> insert_reb -> nb_reb('.date('Y-m-d').'): '.$nb_reb);
						if($nb_reb > 0){
							$data_volume = array(
								'volume' => $this->rebtemp->get_nb_volume()
								,'date' => date('Y-m-d')
							);
							$this->rebtemp->save_volume($data_volume);
							$this->rebtemp->update_synchro();
						}
						/*}elseif ($sync->flag == 3) {
							$list_reb = $this->rebtemp->get_reb_from_temp();
							//var_dump($list_reb);
								if($list_reb){
									$nb_reb = 0;
									foreach ($list_reb as $reb){
										$data_to_reb = array(
											'idenveloppe' => $reb->idenveloppe
											,'numero_image_cheque' =>$reb->numero_image_cheque
											,'date_tri' => $reb->date_tri
											,'date_creation' => $reb->date_creation
											,'montant' => $reb->montant
											,'numero_serie' => $reb->numero_serie
											,'zib' => $reb->zib
											,'numero_compte' => $reb->numero_compte
											,'list_ima_chq' => $reb->list_ima_chq
											,'commande' => $reb->commande
											,'n_lot' => $reb->n_lot
											,'fichier_tri' =>$reb->fichier_tri
											,'id_pli' => $reb->id_pli
											,'id_document' => $reb->id_document
											,'typologie' => $reb->typologie
											,'traitement' =>$reb->traitement
											,'titre' =>$reb->titre
											,'code' =>$reb->code

										);
										if($reb->id_pli != NULL){
											if($this->rebtemp->save_remise_en_banque($data_to_reb, $reb->id_pli)){
												$nb_reb++;
											}else{
												log_message('error', 'reb->id_pli != NULL');
											}
											//var_dump($data_to_reb);
										}else{
											log_message('error', 'cron> cron_pli_reb> insert_base_reb -> Pli inexiste: pli#'.$id_pli);
										}
									}
									log_message('error', 'info=> cron> cron_pli_reb> insert_base_reb -> nb_reb('.date('Y-m-d').'): '.$nb_reb);
									if($nb_reb > 0){
										$data_volume = array(
											'volume' => $this->rebtemp->get_nb_volume()
											,'date' => date('Y-m-d')
										);
										$this->rebtemp->save_volume($data_volume);
									}
								}*/
					}else{
						echo "execution script ssh";
					}
				}
			}
		}
	}
}

function modif_nom_fichier_image($nom_fichier){
	preg_match('#^(.*\()(.*)(\).*)$#',$nom_fichier, $matches);
	if(count($matches)>3){
		$parti = $matches[2];
		if(is_numeric(trim($parti))){
			$parti = trim($parti) * 1;
		}
		return $matches[1].$parti.$matches[3];
		
	}else{
		return $nom_fichier;
	}
}
