<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_reb extends CI_Controller
{
	public function __construct(){

		parent::__construct();
        $this->load->model('cron/Model_base_reb', 'reb');
	}

	public function index(){
		$this->insert_base_reb();
	}

	public function insert_base_reb(){
		$list_reb = $this->reb->get_reb_from_temp();
        var_dump($list_reb);
        if($list_reb){
			$nb_reb = 0;
            foreach ($list_reb as $reb){
				$data_to_reb = array(
					'idenveloppe' => $reb->idenveloppe
					,'numero_image_cheque' =>$reb->numero_image_cheque
					,'date_tri' => $reb->date_tri
					,'date_creation' => $reb->date_creation
					,'montant' => $reb->montant
					,'numero_serie' => $reb->numero_serie
					,'zib' => $reb->zib
					,'numero_compte' => $reb->numero_compte
					,'list_ima_chq' => $reb->list_ima_chq
					,'commande' => $reb->commande
					,'n_lot' => $reb->n_lot
					,'fichier_tri' =>$reb->fichier_tri
					,'id_pli' => $reb->id_pli
					,'id_document' => $reb->id_document
					,'typologie' => $reb->typologie
					,'traitement' =>$reb->traitement
					,'titre' =>$reb->titre
				);
				if($reb->id_pli != NULL){
					if($this->reb->save_reb($data_to_reb, $reb->id_document)){
						$nb_reb++;
					}
					//var_dump($data_to_reb);
				}else{
					log_message('error', 'cron> cron_pli_reb> insert_base_reb -> Pli inexiste: pli#'.$id_pli);
				}
            }
            log_message('error', 'info=> cron> cron_pli_reb> insert_base_reb -> nb_reb('.date('Y-m-d').'): '.$nb_reb);
			if($nb_reb > 0){
				$data_volume = array(
					'volume' => $this->reb->get_nb_volume()
					,'date' => date('Y-m-d')
				);
				$this->reb->save_volume($data_volume);
			}
        }

	}
}