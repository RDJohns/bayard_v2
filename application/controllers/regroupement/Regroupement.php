<?php

class Regroupement extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("regroupement/Model_regroupement", "Mregroupement");
    }

    public function index()
    {
        $this->regrouper();
    }

    public function regrouper()
    {
        log_message('debug', 'run regroupement');
        $pli_exists = $this->Mregroupement->get_pli_to_group();
        if(!empty($pli_exists)){
            $id_regroupement = $this->Mregroupement->init_dt_regroupement();
            $this->Mregroupement->set_group_pli($id_regroupement);
            //echo "Regroupement terminé".PHP_EOL;
            log_message('debug', 'Regroupement terminé');
        }
    }

}