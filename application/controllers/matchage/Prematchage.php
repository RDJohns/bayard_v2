<?php

class Prematchage extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("matchage/Model_new_matchage", "Match");
    }

    public function index()
    {
        $this->prematcher();
    }

    public function prematcher(){
        $societe = $this->Match->get_societe();
        foreach($societe as $soc){
            $upd = $this->Match->set_prematch_societe($soc->id);
            if($upd){
                echo 'Préparation des données de matchage '.$soc->nom_societe.' terminée'.PHP_EOL;
                log_message_reb('error', 'Lancement prématchage du '.date('Y-m-d'));
            }
            else{
                echo 'Préparation des données de matchage '.$soc->nom_societe.' échouée'.PHP_EOL;
                log_message_reb('error', 'Lancement prématchage échoué du '.date('Y-m-d'));
            }
        }
        
    }

}
