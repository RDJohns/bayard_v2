<?php

class NewMatchage extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->load->helper(array('form', 'url', 'function1', 'path'));
        $this->config->set_item('language', 'french');
        $this->load->model("matchage/Model_new_matchage", "NMatch");
        ini_set("memory_limit", "-1");
        set_time_limit(0);
    }

    public function index()
    {
        $this->portier->must_admin();
        $this->matcher();
    }

    public function matcher()
    {

        $head["title"]               = "Matchage ch&egrave;que";
        $head["icon"]                = "label";

        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/magnify/jquery.magnify',
            '../src/template/css/font-awesome.min',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/sweetalert/sweetalert',
            'css/style',
            'css/themes/all-themes',
            'css/custom/typage',
            'css/custom/matchage'
        );


        $foot['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/magnify/jquery.magnify',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'js/pages/ui/notifications',
            'js/admin',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/matchage/nmatchage'
            //'js/matchage/matchage'
        );

        $head["theme"] = 'theme-blue';

        $head["menu"] = "Matchage";

        $arr_import['error'] = '';
        $arr_import['societe'] = $this->NMatch->get_societe();
        //$this->NMatch->get_info_chq_corr(000001,123456,1,1);
        $this->load->view("main/header",$head);
        $this->load->view("nmatchage/import_match", $arr_import);
        $this->load->view("main/footer",$foot);

        $this->load->library('run_reb');
    }

    public function upload_csv(){
        $config['upload_path']          = './match_depot/';
        $config['allowed_types']        = 'csv';
        //$config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);



        if ( ! $this->upload->do_upload('userfile'))
        {
            echo 'error::'.$this->upload->display_errors();
        }
        else{
            $upload_data = $this->upload->data();
            $id_societe = $this->input->post('id_societe');
            echo 'success::'.$upload_data['file_name'].'::'.$id_societe;
        }
    }

    public function display_match_bis(){

        $filename   = $this->input->post('name');
        $id_societe = (int) $this->input->post('id_soc');

        log_message_reb('error','info=> Debut matchage : '.$id_societe);
        $this->NMatch->set_prematch_societe($id_societe);  // revérifier si il y a encore des chèques à matcher
        $insert = $this->NMatch->fill_table_mst($filename,$id_societe); // insérer les données adv vers la table temporaire

        if(!$insert){
            log_message('error', 'Erreur lors de l\'insertion dans la table matchage_temp');
            $this->load->view("nmatchage/error_view");
            exit;
        }

        /*** fin préparation des données ***/

        $data_temp = $this->NMatch->get_match_temp($id_societe);
        foreach ($data_temp as $tempc){
            $cheque = $this->NMatch->get_info_chq_corr(str_pad($tempc->num_pmt,7,'0',STR_PAD_LEFT),$tempc->num_payeur,$id_societe,$tempc->id_mst); // rechercher le chèque dans GED avec numéro paiement et numéro payeur
        }
        $data_adv = $this->NMatch->get_match_temp_adv($id_societe);// Récupération des données depuis matchage_temp
        $tab_adv_diff_vv = array(); // tableau écart en montant
        $tab_vv_no_adv = array(); // tableau chèques ebsent dans ADV
        $tab_adv_no_vv = array(); // tableau chèques absent dans GED
        $tab_adv_ci_vv = array(); // tableau chèques CI dans GED mais non CI dans ADV
        foreach ($data_adv as $temp){
            $cheque = $this->NMatch->get_info_chq(str_pad($temp->num_pmt,7,'0',STR_PAD_LEFT),$temp->num_payeur,$id_societe); // rechercher le chèque dans GED avec numéro paiement et numéro payeur
            if($cheque){
                //array_push($tab_id_trouve, $cheque[0]->id);
                //$this->histo->action(46, $cheque[0]->cmc7, $cheque[0]->id_pli);
                $montant_adv = str_replace(',','.',$temp->montant_adv);
                $montant_vv = str_replace(',','.',$cheque[0]->montant);
                if($montant_adv != $montant_vv){
                    $diff = null;
                    if($montant_adv > $montant_vv){
                        $diff = '<i class="material-icons" style="color: red;">chevron_right</i>';
                    }else{
                        $diff = '<i class="material-icons" style="color: red;">chevron_left</i>';
                    }
                    $array_diff = array(
                        'num_cde'           => $temp->num_cde,
                        'num_payeur_adv'    => $temp->num_payeur,
                        'nom_payeur_adv'    => $temp->nom_payeur,
                        'num_recu_adv'      => $temp->num_recu,
                        'mode_pmt_adv'      => $temp->mode_pmt,
                        'num_pmt_adv'       => $temp->num_pmt,
                        'montant_pmt_adv'   => $montant_adv,
                        'ctg_dte'           => $temp->ctg_dte,
                        'ctg_nme'           => $temp->ctg_nme,
                        'diff'              => $diff,
                        'id_pli'            => $cheque[0]->id_pli,
                        'num_payeur_vv'     => $cheque[0]->numero_payeur,
                        'nom_payeur_vv'     => $cheque[0]->nom_payeur,
                        'num_abonne_vv'     => $cheque[0]->numero_abonne,
                        'cmc7_vv'           => $cheque[0]->cmc7,
                        'montant_vv'        => $montant_vv,
                        'statut_vv'         => $cheque[0]->flag_traitement,
                        'dt_enreg_vv'       => date('d/m/Y H:i:s', strtotime($cheque[0]->dt_enregistrement)),
                        'saisie_par_vv'     => $cheque[0]->saisie_par,
                        'id_chq'            => $cheque[0]->id,
                        'id_doc'            => $cheque[0]->id_doc,
                        'id_lot_saisie'     => $cheque[0]->id_lot_saisie,
                    );
                    array_push($tab_adv_diff_vv, $array_diff);
                }
                if($cheque[0]->statut_saisie == 3){
                    array_push($tab_adv_ci_vv, $cheque[0]);
                }
            }
            else{
                array_push($tab_adv_no_vv, $temp);
            }
        }

        $tab_vv_no_adv = $this->NMatch->get_not_in_adv($id_societe);

        $etat_ecart = $this->NMatch->get_etat_ecart();
        $etat_abs_adv = $this->NMatch->get_etat_abs_adv();
        $upd_syncro = $this->NMatch->update_flag_syncro($id_societe);
        //$body["cheques"]    = $cheques;
        $body["st_ecart"]   = $etat_ecart;
        $body["st_abs_adv"] = $etat_abs_adv;
        $body["tab_diff"]   = $tab_adv_diff_vv;
        $body["tab_vv"]     = $tab_vv_no_adv;
        $body["tab_adv"]    = $tab_adv_no_vv;
        $body["tab_ci"]     = $tab_adv_ci_vv;
        $body["id_societe"] = $id_societe;

        log_message_reb('error','info=> Fin matchage : '.$id_societe);

        //unlink('./match_depot/'.$filename);

        $this->load->view("nmatchage/match_field", $body);
    }

    public function direct_validation(){
        $id_societe = (int) $this->input->post('id_soc');

        $upd = $this->NMatch->direct_validation($id_societe);

        if($upd){
            echo '';
        }
        else{
            echo 'erreur : '.$upd;
        }
    }

    public function get_chq(){
        $id_pli = (int) $this->input->post('id_pli');

        $docs = $this->NMatch->get_doc($id_pli);
        $arr_view_doc = array();

        $arr_view_doc['documents'] = $docs;

        $this->load->view("nmatchage/chq_details", $arr_view_doc);

    }

    public function update_statut_chq(){
        $id_chq = $this->input->post('id_chq');
        $state = $this->input->post('state');
        $montant = $this->input->post('montant');

        $data = null;

        if($state == '1' && $montant != ''){

            $data_chq = $this->NMatch->get_data_chq($id_chq);
            if($data_chq){
                $this->histo->action(49,$data_chq[0]->cmc7,$data_chq[0]->id_pli);
            }

            $data = array(
                'etat_matchage' => $state,
                'montant' => $montant
            );
        }
        else{

            $data_chq = $this->NMatch->get_data_chq($id_chq);
            if($data_chq){
                if($state == '0') $this->histo->action(47,$data_chq[0]->cmc7,$data_chq[0]->id_pli);
                if($state == '1') $this->histo->action(49,$data_chq[0]->cmc7,$data_chq[0]->id_pli);
                if($state == '4') $this->histo->action(48,$data_chq[0]->cmc7,$data_chq[0]->id_pli);
            }

            $data = array(
                'etat_matchage' => $state
            );

        }

        $upd = $this->NMatch->update_chq($id_chq, $data);

        if($upd){
            echo '';
        }
        else{
            echo 'erreur : '.$upd;
        }

    }

    public function update_comment_chq(){
        $id_chq = $this->input->post('id_chq');
        $comment = $this->input->post('comm');

        $data = array(
            'commentaire' => $comment
        );

        $upd = $this->NMatch->update_comment($id_chq, $data);

        if($upd){
            echo '';
        }
        else{
            echo 'erreur : '.$upd;
        }

    }

    public function update_etat_chq(){
        $id_chq = $this->input->post('id_chq');
        $etat = (int) $this->input->post('etat');

        $data = array(
            'etat_reb' => $etat
        );

        $upd = $this->NMatch->update_etat($id_chq, $data);

        if($upd){
            echo '';
        }
        else{
            echo 'erreur : '.$upd;
        }

    }

    public function test_date(){
        /*$test = $this->Match->test_date();
        echo '<h2>'.count($test).'</h2><br>';
        foreach ($test as $v){
            echo $v->id_pli."=>".date('Y-m-d H:i:s',strtotime($v->dt_enregistrement))."<br>";
        }*/

        $test = $this->NMatch->update_for_validation();
    }

    public function update_match(){
        $etat = (int) $this->input->post('e_match');
        $id_societe = (int) $this->input->post('id_soc');

        $upd = $this->NMatch->update_etat_match($etat,$id_societe);

        if($upd){
            echo '';
        }
        else{
            echo $upd;
        }
    }

    public function update_montant(){
        $id         = $this->input->post('id_chq');
        $montant    = u_montant($this->input->post('montant'));

        $data = array('montant' => $montant);

        $upd = $this->NMatch->update_montant($id,$data);

        if($upd){
            echo '';
        }
        else{
            echo $upd;
        }
    }

    public function update_flag_syncro(){
        $id_soc = (int) $this->input->post('id_soc');

        $upd_flag = $this->NMatch->update_flag_syncro($id_soc);

        if($upd_flag){
            echo '';
        }
        else{
            echo $upd_flag;
        }
    }

    public function export_chq_before_reb(){
        $this->load->library('excel');

        $cheques = $this->NMatch->get_chq_before_reb();

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('GED_Bayard_cheques_'.date('Ymd'));

        $sheet = $this->excel->getActiveSheet();

        //en-tête des colonnes
        $sheet->setCellValue('A1', 'Id Pli');
        $sheet->setCellValue('B1', 'N° Payeur');
        $sheet->setCellValue('C1', 'Nom Payeur');
        $sheet->setCellValue('D1', 'N° Abonné');
        $sheet->setCellValue('E1', 'N° CMC7');
        $sheet->setCellValue('F1', 'Montant Payé');
        $sheet->setCellValue('G1', 'Statut Pli');
        $sheet->setCellValue('H1', 'Date saisie');
        $sheet->setCellValue('I1', 'Id lot saisie');
        $sheet->setCellValue('J1', 'Matricule saisie');
        $sheet->setCellValue('K1', 'Societe');

        $sheet->getStyle('A1:K1')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '478dff')
                ),
                'font' => array(
                    'bold' => true
                )
            )
        );

        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);

        $lg = 2;

        foreach ($cheques as $chq){
            $sheet->setCellValue('A'.$lg, $chq->id_pli);
            $sheet->setCellValue('B'.$lg, $chq->numero_payeur);
            $sheet->setCellValue('C'.$lg, $chq->nom_payeur);
            $sheet->setCellValue('D'.$lg, $chq->numero_abonne);
            $sheet->setCellValue('E'.$lg, $chq->cmc7);
            $sheet->setCellValue('F'.$lg, $chq->montant);
            $sheet->setCellValue('G'.$lg, $chq->flag_traitement);
            $sheet->setCellValue('H'.$lg, $chq->dt_enregistrement);
            $sheet->setCellValue('I'.$lg, $chq->id_lot_saisie);
            $sheet->setCellValue('J'.$lg, $chq->saisie_par);
            $sheet->setCellValue('K'.$lg, $chq->match_societe);

            $lg++;
        }

        $filename='GED_Bayard_cheques_'.date("Ymd").'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        exit;
    }

    /*public function update_cloturation_manuel(){
        $this->Match->update_manuel_cloturation();
    }*/

}