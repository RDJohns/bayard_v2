<?php

class Matchage_ckd extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->load->helper(array('form', 'url'));
        $this->config->set_item('language', 'french');
        $this->load->model("matchage/Model_match_ckd", "Match");
        ini_set("memory_limit", "-1");
        set_time_limit(0);
    }

    public function index()
    {
        $this->portier->must_admin();
        $this->matcher();
    }

    public function matcher()
    {

        $head["title"]               = "Matchage ch&egrave;ques cadeaux";
        $head["icon"]                = "label";

        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/magnify/jquery.magnify',
            'plugins/metisMenu/font-awesome',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/sweetalert/sweetalert',
            'css/style',
            'css/themes/all-themes',
            'css/custom/typage',
            '../src/template/css/font-awesome.min',
        );


        $foot['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/magnify/jquery.magnify',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/metisMenu/metisMenu',
            'plugins/metisMenu/mm-folder',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'js/pages/ui/notifications',
            'js/admin',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/matchage/ckdjs'
        );

        $head["theme"] = 'theme-blue';

        $head["menu"] = array(

        );

        $this->load->view("main/header",$head);
        $this->load->view("matchage/import_match_ckd", array('error' => ''));
        $this->load->view("main/footer",$foot);

    }

    public function upload_csv(){
        $config['upload_path']          = './ckd_depot/';
        $config['allowed_types']        = 'xlsx';
        //$config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);



        if ( ! $this->upload->do_upload('userfile'))
        {
            echo 'error::'.$this->upload->display_errors();
        }
        else{
            $upload_data = $this->upload->data();
            echo 'success::'.$upload_data['file_name'];
        }
    }

    public function display_match_chq(){

        //sleep(5);
        $this->Match->set_prematch_chq(); // revérifier si il y a encore des chèques à matcher

        $filename = $this->input->post('name');

        $this->load->library('excel');

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);

        $objPHPExcel = $objReader->load("./ckd_depot/".$filename);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        $rows = array();

        //$cheques = $this->Match->get_liste_chq();

        $tab_num_pmt_adv = array();
        $tab_num_pmt_vv = array();

        $tab_adv_diff_vv = array();
        //$tab_vv_no_adv = array();
        $tab_adv_no_vv = array();
        $tab_adv_ci_vv = array();

        for ($row = 2; $row <= $highestRow; ++$row) {
            $societe = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
            $montant_adv = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
            $num_payeur_adv = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
            $code_pmt_adv = $objWorksheet->getCellByColumnAndRow(17, $row)->getValue();
            $date_lot_adv = $objWorksheet->getCellByColumnAndRow(18, $row)->getValue();
            $nom_lot_adv = $objWorksheet->getCellByColumnAndRow(19, $row)->getValue();
            $code_user_adv = $objWorksheet->getCellByColumnAndRow(20, $row)->getValue();

            $soc = null;
            switch ($societe){
                case 'Bayard' : $soc = 1;
                    break;
                case 'Milan' : $soc = 2;
                    break;
            }
            $cheque = $this->Match->get_info_chq($soc,str_pad($num_payeur_adv,12,'0',STR_PAD_LEFT),$code_pmt_adv,$nom_lot_adv);
            if($cheque){
                //$this->histo->action(46, $cheque[0]->cmc7, $cheque[0]->id_pli);
                $montant_adv = str_replace(',','.',$montant_adv);
                $montant_vv = str_replace(',','.',$cheque[0]->montant);
                if($montant_adv != $montant_vv){
                    $diff = null;
                    if($montant_adv > $montant_vv){
                        $diff = '<i class="material-icons" style="color: red;">chevron_right</i>';
                    }
                    else{
                        $diff = '<i class="material-icons" style="color: red;">chevron_left</i>';
                    }
                    $array_diff = array(
                        'societe'           => $societe,
                        'montant_adv'       => $montant_adv,
                        'num_payeur_adv'    => $num_payeur_adv,
                        'code_pmt_adv'      => $code_pmt_adv,
                        'date_lot_adv'      => $date_lot_adv,
                        'nom_lot_adv'       => $nom_lot_adv,
                        'code_user_adv'     => $code_user_adv,
                        'diff'              => $diff,
                        'id_pli'            => $cheque[0]->id_pli,
                        'id_doc'            => $cheque[0]->id_doc,
                        'id_lot_saisie'     => $cheque[0]->id_lot_saisie,
                        'num_payeur_vv'     => $cheque[0]->numero_payeur,
                        'montant_vv'        => $cheque[0]->montant,
                        'id_kdo'            => $cheque[0]->id_kdo,
                        'type_chq_kdo'      => $cheque[0]->type_chq_kdo,
                        'statut_pli'        => $cheque[0]->statut_saisie,
                        'date_saisie'       => date('d/m/Y H:i:s', strtotime($cheque[0]->dt_enregistrement)),
                        'matricule_saisie'  => $cheque[0]->saisie_par,
                    );
                    array_push($tab_adv_diff_vv, $array_diff);
                }
                if($cheque[0]->statut_saisie == 3){
                    array_push($tab_adv_ci_vv, $cheque[0]);
                }
            }
            else{
                array_push($tab_adv_no_vv, array(
                    'societe'           => $societe,
                    'montant_adv'       => $montant_adv,
                    'num_payeur_adv'    => $num_payeur_adv,
                    'code_pmt_adv'      => $code_pmt_adv,
                    'date_lot_adv'      => $date_lot_adv,
                    'nom_lot_adv'       => $nom_lot_adv,
                    'code_user_adv'     => $code_user_adv
                ));
            }
        }

        $tab_vv_no_adv = $this->Match->get_not_in_adv();

        //$body["cheques"]    = $cheques;
        $body["tab_diff"]   = $tab_adv_diff_vv;
        $body["tab_vv"]     = $tab_vv_no_adv;
        $body["tab_adv"]    = $tab_adv_no_vv;
        $body["tab_ci"]     = $tab_adv_ci_vv;
        $body["tab_ckd"]    = $this->Match->get_type_kdo();

        //unlink('./ckd_depot/'.$filename);

        $this->load->view("matchage/match_ckd_field", $body);
    }

    public function display_match_bis(){

        //sleep(5);
        $this->Match->set_prematch_chq(); // revérifier si il y a encore des chèques à matcher

        $filename = $this->input->post('name');

        /***** préparation des données *****/
        $prepare = $this->Match->empty_match_temp(); // vider la table temporaire

        if(!$prepare){
            exit;
        }

        $insert = $this->Match->fill_match_temp($filename);

        if(!$insert){
            exit;
        }

        /*** fin préparation des données ***/

        $data_temp = $this->Match->get_match_temp();

        $tab_adv_diff_vv = array();
        $tab_vv_no_adv = array();
        $tab_adv_no_vv = array();
        $tab_adv_ci_vv = array();
        //$tab_id_trouve = array();

        foreach ($data_temp as $temp){
            $soc = null;
            switch ($temp->societe){
                case 'Bayard' : $soc = 1;
                    break;
                case 'Milan' : $soc = 2;
                    break;
            }
            $cheque = $this->Match->get_info_chq($soc,$temp->num_client,$temp->code_pmt,$temp->nom_lot);
            if($cheque){
                //array_push($tab_id_trouve, $cheque[0]->id);
                //$this->histo->action(46, $cheque[0]->cmc7, $cheque[0]->id_pli);
                $montant_adv = str_replace(',','.',$temp->montant);
                $montant_vv = str_replace(',','.',$cheque[0]->montant);
                if($montant_adv != $montant_vv){
                    $diff = null;
                    if($montant_adv > $montant_vv){
                        $diff = '<i class="material-icons" style="color: red;">chevron_right</i>';
                    }
                    else{
                        $diff = '<i class="material-icons" style="color: red;">chevron_left</i>';
                    }
                    $array_diff = array(
                        'societe'           => $temp->societe,
                        'montant_adv'       => $temp->montant,
                        'num_payeur_adv'    => $temp->num_client,
                        'code_pmt_adv'      => $temp->code_pmt,
                        'date_lot_adv'      => $temp->date_lot,
                        'nom_lot_adv'       => $temp->nom_lot,
                        'code_user_adv'     => $temp->code_utilisateur,
                        'diff'              => $diff,
                        'id_pli'            => $cheque[0]->id_pli,
                        'id_doc'            => $cheque[0]->id_doc,
                        'id_lot_saisie'     => $cheque[0]->id_lot_saisie,
                        'num_payeur_vv'     => $cheque[0]->numero_payeur,
                        'montant_vv'        => $cheque[0]->montant,
                        'id_kdo'            => $cheque[0]->id_kdo,
                        'type_chq_kdo'      => $cheque[0]->type_chq_kdo,
                        'statut_pli'        => $cheque[0]->statut_saisie,
                        'date_saisie'       => date('d/m/Y H:i:s', strtotime($cheque[0]->dt_enregistrement)),
                        'matricule_saisie'  => $cheque[0]->saisie_par
                    );
                    array_push($tab_adv_diff_vv, $array_diff);
                }
                if($cheque[0]->statut_saisie == 3){
                    array_push($tab_adv_ci_vv, $cheque[0]);
                }
            }
            else{
                array_push($tab_adv_no_vv, $temp);
            }
        }

        /*$upd_trouve = $this->Match->update_trouve($tab_id_trouve);

        if(!$upd_trouve){
            exit;
        }*/

        $tab_vv_no_adv = $this->Match->get_not_in_adv();

        //$body["cheques"]    = $cheques;
        $body["tab_diff"]   = $tab_adv_diff_vv;
        $body["tab_vv"]     = $tab_vv_no_adv;
        $body["tab_adv"]    = $tab_adv_no_vv;
        $body["tab_ci"]     = $tab_adv_ci_vv;
        $body["tab_ckd"]    = $this->Match->get_type_kdo();

        //unlink('./match_depot/'.$filename);

        $this->load->view("matchage/match_ckd_field", $body);
    }

    public function get_chq(){
        $id_pli = (int) $this->input->post('id_pli');

        $docs = $this->Match->get_doc($id_pli);
        $arr_view_doc = array();

        $arr_view_doc['documents'] = $docs;

        $this->load->view("matchage/ckd_details", $arr_view_doc);

    }

    public function update_statut_chq(){
        $id_pli = $this->input->post('id_pli');
        $id_doc = $this->input->post('id_doc');
        $state = $this->input->post('state');
        $montant = $this->input->post('montant');
        $typekdo = $this->input->post('typekdo');

        $upd = $this->Match->update_chq($id_pli, $id_doc, $state, $montant, $typekdo);

        if($upd){
            echo '';
        }
        else{
            echo 'erreur : '.$upd;
        }

    }

    public function update_comment_chq(){
        $id_pli = (int) $this->input->post('id_pli');
        $id_doc = (int) $this->input->post('id_doc');
        $comment = $this->input->post('comm');

        $data = array(
            'commentaire' => $comment
        );

        $upd = $this->Match->update_comment($id_pli,$id_doc, $data);

        if($upd){
            echo '';
        }
        else{
            echo 'erreur : '.$upd;
        }

    }

    public function update_etat_chq(){
        $id_chq = $this->input->post('id_chq');
        $etat = (int) $this->input->post('etat');

        $data = array(
            'etat_reb' => $etat
        );

        $upd = $this->Match->update_etat($id_chq, $data);

        if($upd){
            echo '';
        }
        else{
            echo 'erreur : '.$upd;
        }

    }

    public function test_date(){
        /*$test = $this->Match->test_date();
        echo '<h2>'.count($test).'</h2><br>';
        foreach ($test as $v){
            echo $v->id_pli."=>".date('Y-m-d H:i:s',strtotime($v->dt_enregistrement))."<br>";
        }*/

        $test = $this->Match->update_for_validation();
    }

    public function update_match(){
        $etat = (int) $this->input->post('e_match');

        $upd = $this->Match->update_etat_match($etat);

        if($upd){
            echo '';
        }
        else{
            echo $upd;
        }
    }

    public function export_chq_before_reb(){
        $this->load->library('excel');

        $cheques = $this->Match->get_chq_before_reb();

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('GED_Bayard_cheques_'.date('Ymd'));

        $sheet = $this->excel->getActiveSheet();

        //en-tête des colonnes
        $sheet->setCellValue('A1', 'Id Pli');
        $sheet->setCellValue('B1', 'N° Payeur');
        $sheet->setCellValue('C1', 'Nom Payeur');
        $sheet->setCellValue('D1', 'N° Abonné');
        $sheet->setCellValue('E1', 'N° CMC7');
        $sheet->setCellValue('F1', 'Montant Payé');
        $sheet->setCellValue('G1', 'Statut Pli');
        $sheet->setCellValue('H1', 'Date saisie');
        $sheet->setCellValue('I1', 'Id lot saisie');
        $sheet->setCellValue('J1', 'Matricule saisie');

        $sheet->getStyle('A1:J1')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '478dff')
                ),
                'font' => array(
                    'bold' => true
                )
            )
        );

        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $lg = 2;

        foreach ($cheques as $chq){
            $sheet->setCellValue('A'.$lg, $chq->id_pli);
            $sheet->setCellValue('B'.$lg, $chq->numero_payeur);
            $sheet->setCellValue('C'.$lg, $chq->nom_payeur);
            $sheet->setCellValue('D'.$lg, $chq->numero_abonne);
            $sheet->setCellValue('E'.$lg, $chq->cmc7);
            $sheet->setCellValue('F'.$lg, $chq->montant);
            $sheet->setCellValue('G'.$lg, $chq->flag_traitement);
            $sheet->setCellValue('H'.$lg, $chq->dt_enregistrement);
            $sheet->setCellValue('I'.$lg, $chq->id_lot_saisie);
            $sheet->setCellValue('J'.$lg, $chq->saisie_par);

            $lg++;
        }

        $filename='GED_Bayard_cheques_'.date("Ymd").'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        exit;
    }

    public function report_ckd(){

        $last_week_number = date('W - Y', strtotime('last Week'));
        $date_report = date('Y-m-d');

        $this->load->library('excel');

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load("./ckd_report/reporting_ckd_template.xlsx");
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->setTitle('RECAP_S'.$last_week_number);

        $lg = 3;
        $data_report = $this->Match->get_recap_reporting_ckd($date_report,'');

        foreach ($data_report as $row){

            $lg++;

            $sheet->setCellValue('B'.$lg, $row->commande);
            $sheet->setCellValue('C'.$lg, $row->nb_lire);
            $sheet->setCellValue('D'.$lg, $row->mt_lire);
            $sheet->setCellValue('E'.$lg, $row->nb_cadhoc);
            $sheet->setCellValue('F'.$lg, $row->mt_cadhoc);
            $sheet->setCellValue('G'.$lg, $row->nb_cado);
            $sheet->setCellValue('H'.$lg, $row->mt_cado);
            $sheet->setCellValue('I'.$lg, $row->nb_tirgrp);
            $sheet->setCellValue('J'.$lg, $row->mt_tirgrp);
            $sheet->setCellValue('K'.$lg, $row->nb_accor);
            $sheet->setCellValue('L'.$lg, $row->mt_accor);
            $sheet->getRowDimension($lg)->setRowHeight(30);
        }

        $lg_total = $lg+1;

        $sheet->setCellValue('B'.$lg_total, 'TOTAL');
        $sheet->setCellValue('C'.$lg_total, '=SUM(C4:C'.$lg.')');
        $sheet->setCellValue('D'.$lg_total, '=SUM(D4:D'.$lg.')');
        $sheet->setCellValue('E'.$lg_total, '=SUM(E4:E'.$lg.')');
        $sheet->setCellValue('F'.$lg_total, '=SUM(F4:F'.$lg.')');
        $sheet->setCellValue('G'.$lg_total, '=SUM(G4:G'.$lg.')');
        $sheet->setCellValue('H'.$lg_total, '=SUM(H4:H'.$lg.')');
        $sheet->setCellValue('I'.$lg_total, '=SUM(I4:I'.$lg.')');
        $sheet->setCellValue('J'.$lg_total, '=SUM(J4:J'.$lg.')');
        $sheet->setCellValue('K'.$lg_total, '=SUM(K4:K'.$lg.')');
        $sheet->setCellValue('L'.$lg_total, '=SUM(L4:L'.$lg.')');
        $sheet->getRowDimension($lg_total)->setRowHeight(30);

        $sheet->getStyle('C'.$lg_total.':L'.$lg_total)->getFont()->setBold(true);

        $sheet->getStyle('B4:B'.$lg)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e3e3')
                )
            )
        );

        $sheet->getStyle('C'.$lg_total.':L'.$lg_total)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e3e3')
                )
            )
        );

        $sheet->getStyle('B2:L'.$lg_total)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => '9E9E9E')
                    )
                )
            )
        );

        $objPHPExcel->setActiveSheetIndex(1);
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->setTitle('Liste des CKD S'.$last_week_number);

        $lg = 1;
        $data_report = $this->Match->get_detail_reporting_ckd($date_report,'');

        foreach($data_report as $row){

            $lg++;

            $sheet->setCellValue('A'.$lg, $row->commande);
            $sheet->setCellValue('B'.$lg, $row->id_pli);
            $sheet->setCellValue('C'.$lg, $row->lot_scan);
            $sheet->setCellValue('D'.$lg, $row->id_lot_saisie);
            $sheet->setCellValue('E'.$lg, $row->nom_image);
            $sheet->setCellValue('F'.$lg, date('d/m/Y', strtotime($row->date_courrier)));
            $sheet->setCellValue('G'.$lg, date('d/m/Y H:i:s', strtotime($row->date_matchage)));
            $sheet->setCellValue('H'.$lg, $row->montant);

        }

        foreach (range('A','H') as $col){
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        $sheet->getStyle('A1:H'.$lg)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => '9E9E9E')
                    )
                )
            )
        );

        //$objPHPExcel->setActiveSheetIndex(0);

        $name_file = 'Reporting_ckd_hebdoS'.$last_week_number.'.xlsx';

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('./ckd_report/'.$name_file);

        $this->send_report_to_mail($name_file, $last_week_number, null);
    }

    public function report_monthly_ckd(){

        $firstdaymonth = date('Y-m-d', strtotime('first day of last month'));
        $lastdaymonth = date('Y-m-d', strtotime('last day of last month'));

        $date_array = array('01' => 'Janvier', '02' => 'Février', '03' => 'Mars', '04' => 'Avril', '05' => 'Mai', '06' => 'Juin', '07' => 'Juillet', '08' => 'Août', '09' => 'Septembre', '10' => 'Octobre', '11' => 'Novembre', '12' => 'Décembre');

        $last_month = $date_array[date('m', strtotime('last month'))].' - '.date('Y', strtotime('last month'));

        $this->load->library('excel');

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load("./ckd_report/reporting_ckd_template.xlsx");
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->setTitle('RECAP_'.$last_month);

        $lg = 3;
        $data_report = $this->Match->get_recap_reporting_ckd($firstdaymonth,$lastdaymonth);

        foreach ($data_report as $row){

            $lg++;

            $sheet->setCellValue('B'.$lg, $row->commande);
            $sheet->setCellValue('C'.$lg, $row->nb_lire);
            $sheet->setCellValue('D'.$lg, $row->mt_lire);
            $sheet->setCellValue('E'.$lg, $row->nb_cadhoc);
            $sheet->setCellValue('F'.$lg, $row->mt_cadhoc);
            $sheet->setCellValue('G'.$lg, $row->nb_cado);
            $sheet->setCellValue('H'.$lg, $row->mt_cado);
            $sheet->setCellValue('I'.$lg, $row->nb_tirgrp);
            $sheet->setCellValue('J'.$lg, $row->mt_tirgrp);
            $sheet->setCellValue('K'.$lg, $row->nb_accor);
            $sheet->setCellValue('L'.$lg, $row->mt_accor);
            $sheet->getRowDimension($lg)->setRowHeight(30);
        }

        $lg_total = $lg+1;

        $sheet->setCellValue('B'.$lg_total, 'TOTAL');
        $sheet->setCellValue('C'.$lg_total, '=SUM(C4:C'.$lg.')');
        $sheet->setCellValue('D'.$lg_total, '=SUM(D4:D'.$lg.')');
        $sheet->setCellValue('E'.$lg_total, '=SUM(E4:E'.$lg.')');
        $sheet->setCellValue('F'.$lg_total, '=SUM(F4:F'.$lg.')');
        $sheet->setCellValue('G'.$lg_total, '=SUM(G4:G'.$lg.')');
        $sheet->setCellValue('H'.$lg_total, '=SUM(H4:H'.$lg.')');
        $sheet->setCellValue('I'.$lg_total, '=SUM(I4:I'.$lg.')');
        $sheet->setCellValue('J'.$lg_total, '=SUM(J4:J'.$lg.')');
        $sheet->setCellValue('K'.$lg_total, '=SUM(K4:K'.$lg.')');
        $sheet->setCellValue('L'.$lg_total, '=SUM(L4:L'.$lg.')');
        $sheet->getRowDimension($lg_total)->setRowHeight(30);

        $sheet->getStyle('C'.$lg_total.':L'.$lg_total)->getFont()->setBold(true);

        $sheet->getStyle('B4:B'.$lg)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e3e3')
                )
            )
        );

        $sheet->getStyle('C'.$lg_total.':L'.$lg_total)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e3e3')
                )
            )
        );

        $sheet->getStyle('B2:L'.$lg_total)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => '9E9E9E')
                    )
                )
            )
        );

        $objPHPExcel->setActiveSheetIndex(1);
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->setTitle('Liste des CKD '.$last_month);

        $lg = 1;
        $data_report = $this->Match->get_detail_reporting_ckd($firstdaymonth,$lastdaymonth);

        foreach($data_report as $row){

            $lg++;

            $sheet->setCellValue('A'.$lg, $row->commande);
            $sheet->setCellValue('B'.$lg, $row->id_pli);
            $sheet->setCellValue('C'.$lg, $row->lot_scan);
            $sheet->setCellValue('D'.$lg, $row->id_lot_saisie);
            $sheet->setCellValue('E'.$lg, $row->nom_image);
            $sheet->setCellValue('F'.$lg, date('d/m/Y', strtotime($row->date_courrier)));
            $sheet->setCellValue('G'.$lg, date('d/m/Y H:i:s', strtotime($row->date_matchage)));
            $sheet->setCellValue('H'.$lg, $row->montant);

        }

        foreach (range('A','H') as $col){
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        $sheet->getStyle('A1:H'.$lg)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => '9E9E9E')
                    )
                )
            )
        );

        //$objPHPExcel->setActiveSheetIndex(0);

        $name_file = 'Reporting_ckd_mensuel_'.$last_month.'.xlsx';

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('./ckd_report/'.$name_file);

        $this->send_report_to_mail($name_file,null,$last_month);
    }

    public function send_report_to_mail($file, $week, $month){
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.vivetic.com',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@vivetic.com', // change it to yours
            'smtp_pass' => 'N0reply2015', // change it to yours
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE,
            'wrapchars' => 76,
            'useragent' => 'PHPMailer',
            'smtp_crypto' => 'ssl',
            'validate' => TRUE,
            'mailpath' => '/usr/sbin/sendmail'
        );

        $logo = 'assets/images/vivetic.png';

        $this->load->library('email', $config);
        $data_view["week"] = $week;
        $data_view["month"] = $month;
        $view_mail = $this->load->view('matchage/email_view', $data_view, true);
        $subject = '';
        if($week){
            $subject = 'Reporting hebdomadaire chèques cadeaux S'.$week;
        }else{
            $subject = 'Reporting mensuel des chèques cadeaux : '.$month;
        }
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@vivetic.com'); // change it to yours
        $this->email->to('superviseur_cdn@vivetic.com,bayard_cdn@vivetic.com');// change it to yours
        $this->email->cc('si@vivetic.mg');
        $this->email->subject($subject);
        $this->email->attach('./ckd_report/'.$file);
        $this->email->phpmailer->AddEmbeddedImage($logo, 'logo_vivetic');

        $this->email->message($view_mail);
        if($this->email->send())
        {
            if($week){
                redirect('matchage/matchage_ckd');
            }
            else{
                echo 'Email sent.';
            }
        }
        else
        {
            show_error($this->email->print_debugger());
        }
    }

    /*public function update_cloturation_manuel(){
        $this->Match->update_manuel_cloturation();
    }*/

}