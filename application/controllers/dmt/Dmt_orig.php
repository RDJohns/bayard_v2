<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dmt extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('portier');
        //$this->portier->must_observ();
		$this->load->model('dmt/Model_dmt', 'mvisu');
    }

    public function index(){
       
        $this->visu_dmt();
       // $this->stat_traitement();

    }

  
    public function visu_dmt(){ 
		ini_set('memory_limit', '-1');
		$type_statut   = $this->mvisu->get_statut_traitement();
		$statut_saisie = $this->mvisu->get_statut_saisie();
		
        $array_view_statut = array();

       if($type_statut) $array_view_statut['type_statut'] = $type_statut;

        
		$this->load->view('dmt/header.php');
        $this->load->view('dmt/header_dmt.php');
		if($type_statut){
            $this->load->view('dmt/visu_dmt.php', $array_view_statut);
        }
        else{
            echo 0;
        }
        $this->load->view('dmt/footer.php');
		$this->load->view('dmt/footer_dmt.php');
    }
	
	
	 public function get_dmt_pli()
    {
		ini_set('memory_limit', '-1');
        $statut         = $this->input->post('statut');
        $date_debut     = $this->input->post('date_debut');
        $date_fin       = $this->input->post('date_fin');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');

      
		 $this->load->view('dmt/dmt.php');
    }
	public function ajax_typage_global(){
		
			$arr_gbl = $arr_pli = $arr_user = array();			
			$select_soc 	= $this->input->post('select_soc');
			$select_op      = $this->input->post('select_op');
			$select_typo    = $this->input->post('select_typo');
			$date_debut_ttt = $this->input->post('date_debut_ttt');
			$date_fin_ttt   = $this->input->post('date_fin_ttt');
			$action_ttt   = $this->input->post('action_ttt');
			
			switch ($action_ttt) {
				case 'typage':
					$table_traitement = "view_dmt_typage";
					break;
				case 'saisie':
					$table_traitement = "view_dmt_saisie";
					break;
				case 'controle':
					$table_traitement = "view_dmt_controle";
					break;
			}

			$where_date_courrier_ttt = $where_date_ttt = $where_other = $where_data = $where_traitement = "";
			$join = $join_global = "";	$where_statut = "";
			
			if($date_debut_ttt != '' && $date_fin_ttt != ''){
					$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
					$where_date_ttt .= " AND (view_pli_stat.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
			}
			if($select_soc != ''){		  
					$where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
					$where_date_ttt .= "  AND societe.id = '".$select_soc."' ";
			}
			if($select_typo != ''){
					$where_other .=  " AND typologie.id  in ( ".$select_typo." )";
			}
			
			if($select_op != ''){
					$where_traitement .=  " AND ".$table_traitement.".id_utilisateur in ( ".$select_op." )";
					$where_data .= " AND ".$table_traitement.".id_utilisateur in (".$select_op.")";
					$where_other .= " AND trace.id_user in (".$select_op.")";
					$join = " inner join trace on trace.id_pli = $table_traitement.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
					$join_global = " inner join trace on trace.id_pli = $table_traitement.id_pli ";
					
			}			


		/*$arr_gbl = $this->mvisu->get_dmt_global($where_date_courrier_ttt,$where_other,$where_traitement,$where_data,$join,$table_traitement);*/
		$arr_gbl = $this->mvisu->get_dmt_globals($where_date_ttt,$where_other,$join_global,$table_traitement);
		
		foreach($arr_gbl as $arr_k=>$tab_pli){
						
			$id_pli  = $tab_pli["id_pli"];
			$id_user = $tab_pli["id_user"];
			
						
			if (empty($arr_pli[$id_pli]["duree"]))
			{
				$arr_pli[$id_pli]["id_pli"] = $tab_pli["id_pli"];
				$arr_pli[$id_pli]["duree"] = $tab_pli["duree"];
				
			}
			else
			$arr_pli[$id_pli]["duree"] += $tab_pli["duree"];
		
			/****/
			
			if (empty($arr_user[$id_user]["duree"])){
				$arr_user[$id_user]["id_user"] = $tab_pli["id_user"];
				$arr_user[$id_user]["login"] = $tab_pli["login"];
				$arr_user[$id_user]["duree"] = $tab_pli["duree"];
				
			}
			else
			$arr_user[$id_user]["duree"] += $tab_pli["duree"];
		
			/*$arr_pli[$id_pli]   = (empty($arr_pli[$id_pli]) ? $arr_pli[$id_pli]+$tab_pli["duree"] : $tab_pli["duree"] );
			$arr_user[$id_user] = (empty($arr_user[$id_user]) ? $arr_user[$id_user]+$tab_pli["duree"] : $tab_pli["duree"] );*/
		}
		
		
		$array_view_plis['action_ttt']  = $action_ttt;
		$array_view_plis['arr_pli']  = $arr_pli;
		$array_view_plis['arr_user'] = $arr_user;
		
       if($array_view_plis) $this->load->view('dmt/ajax_pli_typage.php', $array_view_plis);
		
	}
	
	public function ajax_global_global(){
		
			$arr_gbl = $arr_pli = $arr_user = $arr_data = array();			
			$select_soc 	= $this->input->post('select_soc');
			$select_op      = $this->input->post('select_op');
			$select_typo    = $this->input->post('select_typo');
			$date_debut_ttt = $this->input->post('date_debut_ttt');
			$date_fin_ttt   = $this->input->post('date_fin_ttt');
			$export   		= $this->input->post('action_ttt');
				

			$where_date_courrier_ttt = $where_other = $where_data = $where_ttt_typage = $where_ttt_saisie = $where_ttt_controle = "";
			$join = "";	$where_statut = "";
			
			if($date_debut_ttt != '' && $date_fin_ttt != ''){
					//$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
					$where_date_courrier_ttt .= " AND (view_pli_stat.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
					
			}
			if($select_soc != ''){		  
					//$where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
					$where_date_courrier_ttt .= "  AND view_pli_stat.societe = '".$select_soc."' ";
			}
			if($select_typo != ''){
					$where_other .=  " AND view_pli_stat.id_typologie  in ( ".$select_typo." )";
					//$where_other .=  " AND typologie.id  in ( ".$select_typo." )";
			}
			
			if($select_op != ''){
					$where_ttt_typage .=  " AND view_dmt_typage.id_utilisateur in ( ".$select_op." )";
					$where_ttt_saisie .=  " AND view_dmt_saisie.id_utilisateur in ( ".$select_op." )";
					$where_ttt_controle .=  " AND view_dmt_controle.id_utilisateur in ( ".$select_op." )";
					//$where_data .= " AND trace.id_user in (".$select_op.")";
					//$join = " inner join trace on trace.id_pli = view_pli_stat.id_pli ";
					
			}			


		$arr_gbl = $this->mvisu->get_dmt_global_global($where_date_courrier_ttt,$where_other,$where_data,$join,$where_ttt_typage,$where_ttt_saisie,$where_ttt_controle);
		
		foreach($arr_gbl as $arr_k=>$tab_pli){
						
			$id_pli  = $tab_pli["id_pli"];
			$id_user = $tab_pli["id_user"];
			
						
			if (empty($arr_pli[$id_pli]["duree"]))
			{
				$arr_pli[$id_pli]["id_pli"] = $tab_pli["id_pli"];
				$arr_pli[$id_pli]["duree"] = $tab_pli["duree"];
				
			}
			else
			$arr_pli[$id_pli]["duree"] += $tab_pli["duree"];
		
			/****/
			
			if (empty($arr_user[$id_user]["duree"])){
				$arr_user[$id_user]["id_user"] = $tab_pli["id_user"];
				$arr_user[$id_user]["login"] = $tab_pli["login"];
				$arr_user[$id_user]["duree"] = $tab_pli["duree"];
				
			}
			else
			$arr_user[$id_user]["duree"] += $tab_pli["duree"];
		
			if (empty($arr_data[$id_pli][$id_user]["duree"])){
				$arr_data[$id_pli][$id_user]["id_pli"]  = $tab_pli["id_pli"];
				$arr_data[$id_pli][$id_user]["id_user"] = $tab_pli["id_user"];
				$arr_data[$id_pli][$id_user]["login"]   = $tab_pli["login"];
				$arr_data[$id_pli][$id_user]["duree"]   = $tab_pli["duree"];
				
			}
			else
			$arr_data[$id_pli][$id_user]["duree"] += $tab_pli["duree"];
		}
		
			
		$array_view_plis['arr_pli']  = $arr_pli;
		$array_view_plis['arr_user'] = $arr_user;
		$array_view_plis['arr_data'] = $arr_data;
	
	    if ($export == '1') $this->load->view('dmt/export_dmt_global_global.php', $array_view_plis);
		else
        if($array_view_plis) $this->load->view('dmt/ajax_pli_global.php', $array_view_plis);
		
	}
	
	public function ajax_typage_dmt(){
		
		$arr = array();

		ini_set('memory_limit', '-1');
        $select_soc 	= $this->input->post('select_soc');
        $select_op      = $this->input->post('select_op');
        $select_typo    = $this->input->post('select_typo');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');

				
		$where_date_courrier_ttt = $where_other = $where_typage = $where_saisie = $where_controle = $where_data = "";
		$join = "";
		$where_statut = "";
		
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
		}
		if($select_soc != ''){		  
				$where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
		}
		
		/*if($select_statut != ""){ 
          
			list($id, $identifiant) = explode("_", $select_statut );
				if($identifiant == 'ft'){
					$where_other .= ' AND view_pli_stat.flag_traitement = '.$id.' ' ;
				}
				if($identifiant == 'ss'){
					$where_other .= ' AND view_pli_stat.statut_saisie = '.$id.' ' ;
				}
		}
		
		if($select_mp != ""){ 
				$where_other .= ' AND mode_paiement.id_mode_paiement = '.$select_mp.' ' ;
		}
		*/
		if($select_typo != ''){
				$where_other .=  " AND typologie.id in ( ".$select_typo." )";
        }
		
		if($select_op != ''){
				$where_typage .=  " AND view_dmt_typage.id_utilisateur in ( ".$select_op." )";
				$where_saisie .=  " AND view_dmt_saisie.id_utilisateur in ( ".$select_op." )";
				$where_controle .=  " AND view_dmt_controle.id_utilisateur in ( ".$select_op." )";
				$where_data .= " AND trace.id_user in (".$select_op.")";
				$join = " left join trace on trace.id_pli = data_pli.id_pli ";
				//$join = " left join trace on trace.id_pli = data_pli.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
        }
		
	
			

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}

		$arr = $this->mvisu->get_datatables_dmt($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_dmt($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_dmt($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		$i1 = 0;
		
		foreach($arr as $p){
				if($p->date_deb_min.' - '.$p->date_fin_max == $p->debut_fin_typage){
					$dt_date_gbl = '<font color="#000">'.$p->debut_fin_typage.'</font>';
					$date_typ = $dt_date_gbl;
				}else{
					$dt_date_dtl = '<font color="#000">'.$p->date_deb_min.' - '.$p->date_fin_max.'</font>';
					$dt_date_gbl = '<font color="blue">'.$p->debut_fin_typage.'</font>';
					$date_typ = '<i class="fa fa-plus" data-target="#detail'.$i1.'" aria-expanded="false" aria-controls="detail'.$i1.'" onclick="toggle_table('.$i1.',\'typage\')"></i>
					<div id="date_typage_'.$i1.'" >'.$dt_date_dtl.'</div>
					<div class="collapse" id="detail'.$i1.'">
						<div class="">
							'.$dt_date_gbl.'
						</div>
					</div>';
				}
				$p->duree_typage = preg_replace('/\./m', ',', $p->duree_typage);
				$row = array(
					'login_typage' => $p->login_typage,
					'id_pli' => $p->id_pli,			
					'nom_societe' => $p->nom_societe,					
                    'dt_event' => $p->dt_event,
					'nb_mvt' => $p->nb_mvt,
					'typologie' => $p->typologie,
					'mode_paiement' => $p->mode_paiement,
					'lot_scan' => $p->lot_scan,
					'libelle' => $p->libelle,
					'date_typage' => $p->date_typage,
					'debut_fin_typage' => $date_typ,
					'duree_typage' => $p->duree_typage								
                );

                array_push($data, $row);
				$i1++;
            }
            $dtable["data"] = $data;

            echo json_encode($dtable);
		
	}
	
	public function ajax_saisie_dmt(){
		
		$arr = array();

		ini_set('memory_limit', '-1');
        $select_soc 	= $this->input->post('select_soc');
        $select_op      = $this->input->post('select_op');
        $select_typo    = $this->input->post('select_typo');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');

				
		$where_date_courrier_ttt = $where_other = $where_saisie = $where_data = "";
		$join = "";
		$where_statut = "";
		
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
		}
		if($select_soc != ''){		  
				$where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
		}
		
		/*if($select_statut != ""){ 
          
			list($id, $identifiant) = explode("_", $select_statut );
				if($identifiant == 'ft'){
					$where_other .= ' AND view_pli_stat.flag_traitement = '.$id.' ' ;
				}
				if($identifiant == 'ss'){
					$where_other .= ' AND view_pli_stat.statut_saisie = '.$id.' ' ;
				}
		}
		
		if($select_mp != ""){ 
				$where_other .= ' AND mode_paiement.id_mode_paiement = '.$select_mp.' ' ;
		}*/
		
		if($select_typo != ''){
				$where_other .=  "AND typologie.id  in ( ".$select_typo." )";
        }
		
		if($select_op != ''){
				$where_saisie .=  " AND view_dmt_saisie.id_utilisateur in ( ".$select_op." )";
				$join = " left join trace on trace.id_pli = data_pli.id_pli";
				//$where_data .= " AND trace.id_user in (".$select_op.")";				
				//$join = " left join trace on trace.id_pli = data_pli.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
        }
		
	
			

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}

		$arr = $this->mvisu->get_datatables_dmt_saisie($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_dmt_saisie($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_dmt_saisie($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		
		$i1 = 0;
		foreach($arr as $p){
				if($p->date_deb_min.' - '.$p->date_fin_max == $p->debut_fin_saisie){
					$dt_date_gbl = '<font color="#000">'.$p->debut_fin_saisie.'</font>';
					$date_typ = $dt_date_gbl;
				}else{
					$dt_date_dtl = '<font color="#000">'.$p->date_deb_min.' - '.$p->date_fin_max.'</font>';
					$dt_date_gbl = '<font color="blue">'.$p->debut_fin_saisie.'</font>';
					$date_typ = '<i class="fa fa-plus" data-target="#detail'.$i1.'" aria-expanded="false" aria-controls="detail'.$i1.'" onclick="toggle_table('.$i1.',\'saisie\')"></i>
					<div id="date_saisie_'.$i1.'" >'.$dt_date_dtl.'</div>
					<div class="collapse" id="detail'.$i1.'">
						<div class="">
							'.$dt_date_gbl.'
						</div>
					</div>';
				}
				$p->duree_saisie = preg_replace('/\./m', ',', $p->duree_saisie);
				$row = array(
					'login_saisie' => $p->login_saisie,
					'id_pli' => $p->id_pli,			
					'nom_societe' => $p->nom_societe,					
                    'dt_event' => $p->dt_event,
					'nb_mvt' => $p->nb_mvt,
					'typologie' => $p->typologie,
					'mode_paiement' => $p->mode_paiement,
					'lot_scan' => $p->lot_scan,
					'libelle' => $p->libelle,
					'date_saisie' => $p->date_saisie,
					'debut_fin_saisie' => $date_typ,
					'duree_saisie' => $p->duree_saisie								
                );

                array_push($data, $row);
				$i1++;
            }
            $dtable["data"] = $data;

            echo json_encode($dtable);
	}
	
	public function ajax_controle_dmt(){
		
		$arr = array();

		ini_set('memory_limit', '-1');
        $select_soc 	= $this->input->post('select_soc');
        $select_op      = $this->input->post('select_op');
        $select_typo    = $this->input->post('select_typo');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');

				
		$where_date_courrier_ttt = $where_other = $where_controle = $where_data = "";
		$join = "";
		$where_statut = "";
		
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
		}
		
		if($select_soc != ''){		  
				$where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
		}		
		
		if($select_typo != ''){
				$where_other .=  " AND typologie.id  in ( ".$select_typo." )";
        }
		
		if($select_op != ''){
				$where_controle .=  " AND view_dmt_controle.id_utilisateur in ( ".$select_op." )";
				$where_data .= " AND trace.id_user in (".$select_op.")";
				$join = " left join trace on trace.id_pli = data_pli.id_pli ";
				//$join = " left join trace on trace.id_pli = data_pli.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
        }				

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}

		$arr = $this->mvisu->get_datatables_dmt_ctrl($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_dmt_ctrl($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_dmt_ctrl($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		
		$i1 = 0;
		foreach($arr as $p){
				if($p->date_deb_min.' - '.$p->date_fin_max == $p->debut_fin_controle){
					$dt_date_gbl = '<font color="#000">'.$p->debut_fin_controle.'</font>';
					$date_typ = $dt_date_gbl;
				}else{
					$dt_date_dtl = '<font color="#000">'.$p->date_deb_min.' - '.$p->date_fin_max.'</font>';
					$dt_date_gbl = '<font color="blue">'.$p->debut_fin_controle.'</font>';
					$date_typ = '<i class="fa fa-plus" data-target="#detail'.$i1.'" aria-expanded="false" aria-controls="detail'.$i1.'" onclick="toggle_table('.$i1.',\'controle\')"></i>
					<div id="date_controle_'.$i1.'" >'.$dt_date_dtl.'</div>
					<div class="collapse" id="detail'.$i1.'">
						<div class="">
							'.$dt_date_gbl.'
						</div>
					</div>';
				}
				$p->duree_controle = preg_replace('/\./m', ',', $p->duree_controle);
				$row = array(
					'login_controle' => $p->login_controle,
					'id_pli' => $p->id_pli,			
					'nom_societe' => $p->nom_societe,					
                    'dt_event' => $p->dt_event,
					'nb_mvt' => $p->nb_mvt,
					'typologie' => $p->typologie,
					'mode_paiement' => $p->mode_paiement,
					'lot_scan' => $p->lot_scan,
					'libelle' => $p->libelle,
					'date_controle' => $p->date_controle,
					'debut_fin_controle' => $date_typ,
					'duree_controle' => $p->duree_controle													
                );

                array_push($data, $row);
				$i1++;
            }
            $dtable["data"] = $data;

            echo json_encode($dtable);
		
	}
	
	public function ajax_globals_dmt(){
		
		$arr = array();

		ini_set('memory_limit', '-1');
        $select_soc 	= $this->input->post('select_soc');
        $select_op      = $this->input->post('select_op');
        $select_typo    = $this->input->post('select_typo');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');

				
		$where_date_courrier_ttt = $where_other = $where_typage = $where_saisie = $where_controle = $where_data = $where_ss_data = $where_data_pli = $wheredata_pli = $where_date ="";
		$join = $join_trace = "";
		$where_statut = "";
		
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
				$where_data_pli .= " AND (view_statut_pli_cloture.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
				$where_date = "'".$date_debut_ttt."' , '".$date_fin_ttt."'";
				
				$wheredata_pli  .= " AND (f.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
				
		}
		
		if($select_soc != ''){		  
				$where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
				$where_data_pli .= "  AND f_pli.societe = '".$select_soc."' ";
				$wheredata_pli  .= "  AND f_pli.societe = '".$select_soc."' ";
		}		
		
		if($select_typo != ''){
				$where_other .=  " AND typologie.id  in ( ".$select_typo." )";
				$where_data_pli .=  " AND f_pli.typologie in ( ".$select_typo." )";
				$wheredata_pli  .=  " AND f_pli.typologie in ( ".$select_typo." )";
        }
		
		if($select_op != ''){
				$where_typage   .=  " AND view_dmt_typage.id_utilisateur in ( ".$select_op." )";
				$where_saisie   .=  " AND view_dmt_saisie.id_utilisateur in ( ".$select_op." )";
				$where_controle .=  " AND view_dmt_controle.id_utilisateur in ( ".$select_op." )";
				$where_data     .= " AND trace.id_user in (".$select_op.")";
				$where_data_pli .= " AND trace.id_user in (".$select_op.")";
				
				
				//$join = " left join trace on trace.id_pli = view_pli_stat.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
				$join = " left join trace on trace.id_pli       = view_pli_stat.id_pli ";
				$join_trace = " left join trace on trace.id_pli = f_pli.id_pli ";
        }				

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}

		$arr = $this->mvisu->get_datatables_dmt_global($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$where_data_pli,$wheredata_pli,$where_date,$join,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_dmt_global($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$where_data_pli,$wheredata_pli,$where_date,$join);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_dmt_global($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$where_data_pli,$wheredata_pli,$where_date,$join,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		
		
		foreach($arr as $p){
				$duree_totale = $p->duree_typage+ $p->duree_saisie+ $p->duree_controle;
				$row = array(
					'dt_event' => $p->dt_event,
					'id_pli' => $p->id_pli,			
					'nom_societe' => $p->nom_societe,					
                    'nb_mvt' => $p->nb_mvt,
					'typologie' => $p->typologie,
					'mode_paiement' => $p->mode_paiement,
					'lot_scan' => $p->lot_scan,
					'libelle' => $p->libelle,
					'duree_typage' => $p->duree_typage,
					'duree_saisie' => $p->duree_saisie,
					'duree_controle' => $p->duree_controle,														
					'duree_totale' => $duree_totale														
                );

                array_push($data, $row);
            }
            $dtable["data"] = $data;

            echo json_encode($dtable);
		
	}
	
	
	public function export_dmt_typage(){

        $select_soc 	= $this->input->post('select_soc');
        $select_op      = $this->input->post('select_op');
        $select_typo    = $this->input->post('select_typo');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');
        $action_ttt     = $this->input->post('action_ttt');
		
				
		$where_date_courrier_ttt = $where_other = $where_typage = $where_data = "";
		$join = "";
		$where_statut = "";
		
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
		}
		if($select_soc != ''){		  
		   $where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
		}		
		
		if($select_typo != ''){
				$where_other .=  " AND typologie.id in ( ".$select_typo." )";
        }
		
		if($select_op != ''){
				$where_typage .=  " AND view_dmt_typage.id_utilisateur in ( ".$select_op." )";
				$where_saisie .=  " AND view_dmt_saisie.id_utilisateur in ( ".$select_op." )";
				$where_controle .=  " AND view_dmt_controle.id_utilisateur in ( ".$select_op." )";
				$where_data .= " AND trace.id_user in (".$select_op.")";
				$join = " left join trace on trace.id_pli = data_pli.id_pli";
				//$join = " left join trace on trace.id_pli = data_pli.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
        }
		
        $array_result = $this->mvisu->get_dmt_pli_typage($where_date_courrier_ttt,$where_other,$where_typage,$where_data,$join);
		$array_export = array();
        $array_export['list_export'] = $array_result;
		$array_export['action_ttt'] = $action_ttt;
        $this->load->view('dmt/export_dmt_excel.php', $array_export);

    }
	
	public function export_dmt_saisie(){

        $select_soc 	= $this->input->post('select_soc');
        $select_op      = $this->input->post('select_op');
        $select_typo    = $this->input->post('select_typo');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');
		$action_ttt     = $this->input->post('action_ttt');

				
		$where_date_courrier_ttt = $where_other = $where_saisie = $where_data = "";
		$join = "";
		$where_statut = "";
		
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
		}
		if($select_soc != ''){		  
		   $where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
		}		
		
		if($select_typo != ''){
				$where_other .=  " AND typologie.id  in ( ".$select_typo." )";
        }
		
		if($select_op != ''){
				$where_saisie .=  " AND view_dmt_saisie.id_utilisateur in ( ".$select_op." )";
				$where_data .= " AND trace.id_user in (".$select_op.")";
				$join = " left join trace on trace.id_pli = data_pli.id_pli ";
				//$join = " left join trace on trace.id_pli = data_pli.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
        }
		
        $array_result = $this->mvisu->get_dmt_pli_saisie($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join);
		
        $array_export = array();
		$array_export['action_ttt'] = $action_ttt;
        $array_export['list_export'] = $array_result;

        $this->load->view('dmt/export_dmt_excel.php', $array_export);

    }
	
	public function export_dmt_controle(){

        $select_soc 	= $this->input->post('select_soc');
        $select_op      = $this->input->post('select_op');
        $select_typo    = $this->input->post('select_typo');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');
		$action_ttt     = $this->input->post('action_ttt');
				
		$where_date_courrier_ttt = $where_other = $where_controle = $where_data = "";
		$join = "";
		$where_statut = "";
		
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
		}
		if($select_soc != ''){		  
		   $where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
		}		
		
		if($select_typo != ''){
				$where_other .=  " AND typologie.id  in ( ".$select_typo." )";
        }
		
		if($select_op != ''){
				$where_controle .=  " AND view_dmt_controle.id_utilisateur in ( ".$select_op." )";
				$where_data .= " AND trace.id_user in (".$select_op.")";
				$join = " left join trace on trace.id_pli = data_pli.id_pli ";
				//$join = " left join trace on trace.id_pli = data_pli.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
        }
		
        $array_result = $this->mvisu->get_dmt_pli_controle($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join);
		
        $array_export = array();
        $array_export['action_ttt'] = $action_ttt;
        $array_export['list_export'] = $array_result;

        $this->load->view('dmt/export_dmt_excel.php', $array_export);

    }
	
	public function export_dmt_global(){
		
			$arr_gbl = $arr_pli = $arr_user = array();			
			$select_soc 	= $this->input->post('select_soc');
			$select_op      = $this->input->post('select_op');
			$select_typo    = $this->input->post('select_typo');
			$date_debut_ttt = $this->input->post('date_debut_ttt');
			$date_fin_ttt   = $this->input->post('date_fin_ttt');
			$action_ttt   = $this->input->post('action_ttt');
			
			switch ($action_ttt) {
				case 'typage':
					$table_traitement = "view_dmt_typage";
					break;
				case 'saisie':
					$table_traitement = "view_dmt_saisie";
					break;
				case 'controle':
					$table_traitement = "view_dmt_controle";
					break;
			}

			$where_date_courrier_ttt = $where_other = $where_data = $where_traitement = "";
			$join = "";	$where_statut = "";
			
			if($date_debut_ttt != '' && $date_fin_ttt != ''){
					$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
			}
			if($select_soc != ''){		  
					$where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
			}
			if($select_typo != ''){
					$where_other .=  " AND typologie.id  in ( ".$select_typo." )";
			}
			
			if($select_op != ''){
					$where_traitement .=  " AND ".$table_traitement.".id_utilisateur in ( ".$select_op." )";
					$where_data .= " AND trace.id_user in (".$select_op.")";
					//$join = " left join trace on trace.id_pli = data_pli.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
					$join = " left join trace on trace.id_pli = data_pli.id_pli ";
					
			}			


		$arr_gbl = $this->mvisu->get_dmt_global($where_date_courrier_ttt,$where_other,$where_traitement,$where_data,$join,$table_traitement);
		
		foreach($arr_gbl as $arr_k=>$tab_pli){
						
			$id_pli  = $tab_pli["id_pli"];
			$id_user = $tab_pli["id_user"];
			
						
			if (empty($arr_pli[$id_pli]["duree"]))
			{
				$arr_pli[$id_pli]["id_pli"] = $tab_pli["id_pli"];
				$arr_pli[$id_pli]["duree"] = $tab_pli["duree"];
				
			}
			else
			$arr_pli[$id_pli]["duree"] += $tab_pli["duree"];
		
			/****/
			
			if (empty($arr_user[$id_user]["duree"])){
				$arr_user[$id_user]["id_user"] = $tab_pli["id_user"];
				$arr_user[$id_user]["login"] = $tab_pli["login"];
				$arr_user[$id_user]["duree"] = $tab_pli["duree"];
				
			}
			else
			$arr_user[$id_user]["duree"] += $tab_pli["duree"];
		
			/*$arr_pli[$id_pli]   = (empty($arr_pli[$id_pli]) ? $arr_pli[$id_pli]+$tab_pli["duree"] : $tab_pli["duree"] );
			$arr_user[$id_user] = (empty($arr_user[$id_user]) ? $arr_user[$id_user]+$tab_pli["duree"] : $tab_pli["duree"] );*/
		}
		
		
		$array_view_plis['action_ttt']  = $action_ttt;
		$array_view_plis['arr_pli']  = $arr_pli;
		$array_view_plis['arr_user'] = $arr_user;
		
       if($array_view_plis) $this->load->view('dmt/export_dmt_global.php', $array_view_plis);
		
	}
	
	public function export_dmt_global_global(){

        $select_soc 	= $this->input->post('select_soc');
        $select_op      = $this->input->post('select_op');
        $select_typo    = $this->input->post('select_typo');
		$date_debut_ttt = $this->input->post('date_debut_ttt');
        $date_fin_ttt   = $this->input->post('date_fin_ttt');
		$action_ttt     = $this->input->post('action_ttt');
				
		$where_date_courrier_ttt = $where_other = $where_typage = $where_saisie = $where_controle = $where_data = "";
		$join = "";
		$where_statut = "";
		
		if($date_debut_ttt != '' && $date_fin_ttt != ''){
				$where_date_courrier_ttt .= " AND (view_histo_pli_oid.dt_event::date between '".$date_debut_ttt."' AND '".$date_fin_ttt."') ";
		}
		if($select_soc != ''){		  
		   $where_date_courrier_ttt .= "  AND societe.id = '".$select_soc."' ";
		}		
		
		if($select_typo != ''){
				$where_other .=  " AND typologie.id  in ( ".$select_typo." )";
        }
		
		if($select_op != ''){
				$where_typage .=  " AND view_dmt_typage.id_utilisateur in ( ".$select_op." )";
				$where_saisie .=  " AND view_dmt_saisie.id_utilisateur in ( ".$select_op." )";
				$where_controle .=  " AND view_dmt_controle.id_utilisateur in ( ".$select_op." )";
				$where_data .= " AND trace.id_user in (".$select_op.")";
				
				//$join = " left join trace on trace.id_pli = view_pli_stat.id_pli AND date_action BETWEEN '".$date_debut_ttt."' AND '".$date_fin_ttt."'";
				$join = " left join trace on trace.id_pli = view_pli_stat.id_pli ";
        }		
		
        $array_result = $this->mvisu->get_dmt($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join);
		
        $array_export = array();
        $array_export['action_ttt'] = $action_ttt;
        $array_export['list_export'] = $array_result;

        $this->load->view('dmt/export_dmt.php', $array_export);

    }
	public function get_societe()
    {
			$list = $this->mvisu->get_societe();
			$array_view_plis['list_societe'] = $list;
			if($list) $this->load->view('statistics/societe_select.php', $array_view_plis);		
       
    }
	public function get_operateur()
    {
			$list = $this->mvisu->get_operateur();
			$array_view_plis['list_operateur'] = $list;
			if($list) $this->load->view('statistics/operateur_select.php', $array_view_plis);		
       
    }
	public function get_typologie()
    {
			$list = $this->mvisu->get_typologie();
			$array_view_plis['list_typologie'] = $list;
			if($list) $this->load->view('statistics/typologie_select.php', $array_view_plis);		
       
    }
	public function get_statut(){
			$list1 = $this->mvisu->get_statut_saisie ();
			$list2 = $this->mvisu->get_statut_traitement ();
			$list = array();
			
			$i = 0;
			foreach($list1 as $k => $tab1){
				
				$list[$i]["id"]    = $tab1["id_statut_saisie"]."_ss";
				$list[$i]["value"] = $tab1["libelle"];
				$i++;
			}
			foreach($list2 as $k => $tab2){
				
				$list[$i]["id"]    = $tab2["id_flag_traitement"]."_ft";
				$list[$i]["value"] = $tab2["traitement"];
				$i++;
			}
			
			/*foreach($list2 as $k => $tab2){
				$k = $tab2["id_flag_traitement"];
				$v = $tab2["traitement"];
				$list["id"]    = $k;
				$list["value"] = $v;
			}*/
			
			$array_view_plis['list_statut'] = $list;
			if($list) $this->load->view('dmt/statut_select.php', $array_view_plis);	
		
	}
	public function get_mode_paiement()
    {
			$list = $this->mvisu->get_mode_paiement();
			$array_view_plis['list_mp'] = $list;
			if($list) $this->load->view('dmt/modepaiement_select.php', $array_view_plis);		
       
    }
	
}
?>
