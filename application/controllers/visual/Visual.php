<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Visual extends CI_Controller{

	

	public function index(){
		/*$this->load->library('portier');
		$this->portier->must_observ();*/
		//$this->go();
		
		if((int)$this->session->userdata('id_type_utilisateur') != 8) /* pour relation cdn Benja*/
		{
			/*$this->load->library('portier');
			$this->portier->must_observ();	*/
			$this->go();
		}
		else
		{
			$this->goCdnPage();
		}
	}
	
	public function goCdnPage()
	{
		$this->load->library('visual/page');
		$this->page->set_titre('<i class="fa fa fa-eye"></i> Visualisation des courriers');
		$this->page->add_links('Visualisation des MAIL/SFTP', site_url('visualisation/visu_push'), 'fa fa-sign-in', 'deep-purple');
		$this->page->add_links('Tratitement des KO', site_url('traitement/KO'), 'fa fa-arrow-circle-right', 'orange');
		$this->page->add_js('visual/main');
		$this->page->add_css('visual/main');
		$this->page->set_my_url('/visual/visual');

		$this->load->model('visual/model_pli');

		$data_init = array(
			'steps' => $this->model_pli->flag_traitement()
			,'status' => $this->model_pli->statut_saisie()
			,'socities' => $this->model_pli->societe()
			,'types' => $this->model_pli->typologie()
			,'md_paiements' => $this->model_pli->mode_paiement()
			,'relation_client' => 1
		);

		$this->page->afficher($this->load->view('visual/visual_pli', $data_init, TRUE));
	}

	public function go(){
		$this->load->library('visual/page');
		$this->page->set_titre('<i class="fa fa fa-eye"></i> Visualisation des courriers');
		// $this->page->set_titre('<i class="fa fa-arrow-circle-right"></i> Stat. GED');
		
		$this->page->add_links('Extraction', site_url('anomalie/anomalie/visu_anomalie'), 'fa fa-file-text', 'cyan-t');
		$this->page->add_links('Indicateurs de production', site_url('indicateur/indicateur/visu_indicateur'), 'fa fa-industry', 'light-blue');
		$this->page->add_links('Réception', site_url('reception/pli_flux'), 'fa fa-envelope', 'light-green');
		$this->page->add_links('Recherche avancée Pli et Flux', site_url('recherche/avancee'), 'fa fa-search-plus', 'pink');
		$this->page->add_links('Suivi mensuel', site_url('statistics/statistics/stat_mensuel'), 'fa fa-signal', 'teal');
		$this->page->add_links('Suivi des soldes', site_url('statistics/statistics/suivi_solde'), 'fa fa-battery-half', 'pink-cstm');
		$this->page->add_links('Stat. GED', site_url('statistics/Stat'), 'fa fa-arrow-circle-right', 'brown');
		$this->page->add_links('Suivi des espèces', site_url('suivi/especes'), 'fa fa-money', 'amber');
		$this->page->add_links('Suivi des ch&egrave;ques non REB', site_url('suivi/reliquat'), 'fa fa-list', 'lime');
		$this->page->add_links('Suivi des ch&egrave;ques non saisis', site_url('suivi/nosaisie'), 'fa fa-clipboard', 'blue-grey');
		$this->page->add_links('Suivi des DMT', site_url('dmt/dmt/visu_dmt'), 'fa fa-hourglass-1', 'grenat');
		$this->page->add_links('Suivi des provenances', site_url('provenance/pli'), 'fa fa-flag', 'green');
		$this->page->add_links('Traitement', site_url('statistics/statistics_flux/stat_traitement'), 'fa fa-tasks', 'light-blue');
		if(intval($this->session->userdata('avec_traitement_ko')) == 1)
		{
			$this->page->add_links('Traitement des KO / Circulaire', site_url('traitement/KO'), 'fa fa-arrow-circle-right', 'orange');
		}
		$this->page->add_links('Visualisation batch', site_url('anomalie/batch'), 'fa fa-bug', 'red');
		$this->page->add_links('Visualisation des MAIL/SFTP', site_url('visualisation/visu_push'), 'fa fa-sign-in', 'deep-purple');
		$this->page->add_links('Visualisation taux de fautes', site_url('visualisation/fautes'), 'fa fa-table', 'pink');
		
		
		/*$this->page->add_links('Pilotage des flux', site_url('reception/Pilotage_Flux'), 'fa fa-file-excel-o', 'indigo'); */
		$this->page->add_js('visual/main');
		$this->page->add_css('visual/main');
		$this->page->set_my_url('/visual/visual');
		$this->load->model('visual/model_pli');
		$data_init = array(
			'steps' => $this->model_pli->flag_traitement()
			,'status' => $this->model_pli->statut_saisie()
			,'socities' => $this->model_pli->societe()
			,'types' => $this->model_pli->typologie()
			,'md_paiements' => $this->model_pli->mode_paiement()
			,'relation_client' => 0
		);
		$this->page->afficher($this->load->view('visual/visual_pli', $data_init, TRUE));
	}
	
	public function plis(){
		$this->load->helper('text');
		$correspondance_field = array('date_courrier','tbsoc.nom_societe','sub_pli.idpli','pli','flgtt_etape','flgtt_etat','(CASE WHEN flag_rewrite_status_client <> 0 THEN \'\' ELSE tbstat.libelle END)','tbtypo.typologie', '(CASE WHEN nb_mvmts IS NULL THEN 0 ELSE nb_mvmts END)','nb_paie','cmc7s','id_com','motif_operateur','consigne_client','comms','id_lot_saisie','lot_scan','date_numerisation','dttrt'/*,'typage'*/,'niveau_saisie');
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('visual/model_pli');
			$resultat["draw"] = $this->input->post_get('draw');
			$offset = $this->input->post_get('start');
			$limit = $this->input->post_get('length');
			$search = $this->input->post_get('search');
			$arg_find = strtolower($search['value']);
			$order = $this->input->post_get('order');
			$arg_ordo = $correspondance_field[$order[0]['column']];
			$sens = $order[0]['dir'];
			$this->load->helper('function1');
			$c_dt = date_1_2($this->input->post_get('dt_mail_1'), $this->input->post_get('dt_mail_2'));
			$trt_dt = date_1_2($this->input->post_get('dt_act_1'), $this->input->post_get('dt_act_2'));
			$param_plus = array(
				'c_dt1' => $c_dt[0]
				,'c_dt2' => $c_dt[1]
				,'trt_dt1' => $trt_dt[0]
				,'trt_dt2' => $trt_dt[1]
				,'etape' => json_decode($this->input->post_get('etape'))
				,'etat' => json_decode($this->input->post_get('etat'))
				,'soc' => json_decode($this->input->post_get('soc'))
				,'typo' => json_decode($this->input->post_get('typo'))
				,'niveau_saisie' => json_decode($this->input->post_get('niveau_saisie'))
				,'md_paie' => json_decode($this->input->post_get('md_paie'))
				,'find_id_pli' => strtolower($this->input->post_get('find_id_pli'))
				,'find_pli' => strtolower($this->input->post_get('find_pli'))
				,'find_pr' => strtolower($this->input->post_get('find_paieur'))
				,'find_ab' => strtolower($this->input->post_get('find_abonne'))
				,'find_cmc7' => strtolower($this->input->post_get('find_cmc7'))
			);
			$resultat["iTotalRecords"] = $this->model_pli->pli_total();
			$resultat["iTotalDisplayRecords"] = $this->model_pli->get_plis($limit, $offset, $arg_find, $arg_ordo, $sens, $param_plus);
			$plis = $this->model_pli->get_plis($limit, $offset, $arg_find, $arg_ordo, $sens, $param_plus, FALSE);
			$this->load->helper('function1');
			foreach ($plis as $pli) {
				$step = $pli->etape . ($pli->rec == -1 ? '<br><span class="font-10 font-italic col-pink">[Stand-by]</span>' : '');
				if ($pli->id_flag_trtmnt == 19/* && $pli->ke_prio == 0*/) {
					$step = '<span class="elem_activ" title="Editer consigne" onclick="edit_consigne_ke(\''.$pli->idpli.'\');">#'.$step.'</span>';
				}
				$row = array(
					'dtc' => $pli->dtcourrier == '' ? '' : (new DateTime($pli->dtcourrier))->format('d/m/Y')
					,'dtn' => $pli->dtnum == '' ? '' : (new DateTime($pli->dtnum))->format('d/m/Y H:i:s')
					,'lot_sc' => $pli->lotScan
					,'soc' => $pli->soc
					,'idpli' => $pli->idpli
					,'pli' => $pli->pli
					,'etape' => $pli->flgtt_etape
					,'step' => $pli->flgtt_etat_disp
					,'stat' => $pli->etat
					,'typo' => $pli->typo == '' ? '' : '<span data-toggle="tooltip" data-placement="bottom"  title="Sous-lot : '.$pli->typo1.'">'.$pli->typo.'</span>'
					//,'docs' => str_replace('{icon_doc}', '<i class="material-icons icon_mini">find_in_page</i>', $pli->docs)
					//,'mvmts' => str_replace('{icon_mvmt}', '<i class="material-icons icon_mini">picture_in_picture_alt</i>', $pli->mvmts)
					,'mvmts' => $pli->mvmts
					,'lot_ss' => $pli->lotss == '' ? '' : $pli->lotss.($pli->lotss1 == 1 ? '<sup class="font-italic col-pink font-10">BIS</sup>' : '')
					,'dth' => ($pli->lotss == '' ? '' : (new DateTime($pli->dttrt))->format('d/m/Y'))
					,'paies' => implode(', ', array_unique(explode('|', $pli->paiements)))
					,'cmc7s' => $pli->cmc7s//cmc7s($pli->cmc7s)
					,'id_com' => $pli->id_com
					,'consigne_op' => '<span data-toggle="tooltip" data-placement="left" title="'.$pli->motif_operateur.'">'.character_limiter($pli->motif_operateur, 15).'</span>'
					,'consigne_client' => '<span data-toggle="tooltip" data-placement="left" title="'.$pli->consigne_client.'">'.character_limiter($pli->consigne_client, 15).'</span>'
					,'comms' => is_numeric($pli->nb_comms) && $pli->nb_comms > 0 ? '<span class="badge bg-cyan font-20" style="cursor:pointer;" onclick_="affiche_commentaire('.$pli->idpli.');">'.$pli->nb_comms.' <!--i class="material-icons font-15">info</i--></span>' : '' 
					,'niveau_saisie' => 'Niveau '.$pli->niveau_saisie
					,'typage' => $pli->typage
					,'DT_RowAttr' => array(
						'my_id_pli' => $pli->idpli
						,'my_lot_saisie' => $pli->lotss
						,'my_nb_mvmnt' => $pli->mvmts
						,'title' => 'Cliquez pour afficher les actions'
					)
					,'DT_RowClass' => 'ln_pli'
				);
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
		echo json_encode($resultat);
	}
	
	public function get_img_doc($id_doc){
		if(is_numeric($id_doc)){
			ini_set('memory_limit', '-1');
			$this->load->model('visual/model_pli');
			$doc = $this->model_pli->doc($id_doc);
			if(!is_null($doc)){
				$data_view = array(
					'doc' => $doc
					,'empty_b64' => base64_encode(file_get_contents(base_url('assets/images/empty_papper.jpg')))
				);
				echo json_encode(array(
					'code' => 1
					,'img' => $this->load->view('visual/document', $data_view, TRUE)
				));
				return '';
			}
		}
		log_message('error', 'visual> visual> get_img_doc: failed loading document view doc#'.$id_doc);
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Fichier introuvable!'
		));
	}
	
	public function get_img_docs($id_pli){
		if(is_numeric($id_pli)){
			ini_set('memory_limit', '-1');
			$this->load->model('visual/model_pli');
			$docs = $this->model_pli->docs($id_pli);
			if(count($docs) > 0){
				//$pli = $this->model_pli->data_pli($id_pli);
				$typo = $this->model_pli->typo_pli($id_pli);
				// $have_mtf_c = !is_null($this->model_pli->get_motif_consigne($id_pli));
				$motif_consignes = $this->model_pli->get_all_motif_consigne($id_pli);
				$data_view = array(
					'id_pli' => $id_pli
					//,'pli' => $pli
					//,'with_mtf_c' => $have_mtf_c
					,'docs' => $docs
					,'motif_consignes' => $motif_consignes
					,'empty_b64' => base64_encode(file_get_contents(base_url('assets/images/empty_papper.jpg')))
				);
				echo json_encode(array(
					'code' => 1
					,'img' => $this->load->view('visual/documents', $data_view, TRUE)
					,'typo' => $typo
					// ,'mtf_c' => $have_mtf_c ? '' : ''
				));
				return '';
			}
		}
		log_message('error', 'visual> visual> get_img_docs: failed loading documents view pli#'.$id_pli);
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Fichier introuvable!'
		));
	}

	public function get_view_mvmt($id_pli=NULL, $id_mvmt=NULL){
		if(is_numeric($id_pli)){
			$this->load->model('visual/model_pli');
			$mvmts = $this->model_pli->mouvements($id_pli);
			$data_view = array(
				'mvmts' => $mvmts
				,'activ_id' => is_numeric($id_mvmt) ? $id_mvmt : (isset($mvmts[0]) ? $mvmts[0]->id_ : NULL)
			);
			echo json_encode(array(
				'code' => 1
				,'ihm' => $this->load->view('visual/mouvement', $data_view, TRUE)
			));
			return '';
		}
		log_message('error', 'visual> visual> get_view_mvmt: failed loading mvmt view pli#'.$id_pli);
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Pli introuvable!'
		));
	}

	public function display_pli($id_pli){
		if(is_numeric($id_pli)){
			$this->load->model('visual/model_pli');
			$this->load->helper('function1');
			$pli = $this->model_pli->pli_view($id_pli);
			$data_pli = $this->model_pli->data_pli($id_pli);
			if(!is_null($pli)){
				$data_view = array(
					'pli' => $pli
					,'docs' => $this->model_pli->data_docs($id_pli)
					,'paies' => $this->model_pli->paiement_view($id_pli)
					,'chqs' => $this->model_pli->cheques($id_pli)
					,'chqs_kd' => $this->model_pli->cheques_kd($id_pli)
					,'mvmts' => $this->model_pli->mouvements($id_pli)
					,'motif_consigne' => $this->model_pli->get_motif_consigne($id_pli)
					,'f_ci' => is_null($data_pli) ? '' : $data_pli->nom_orig_fichier_circulaire
					,'with_f_ci' => (is_null($data_pli) ? FALSE : ((!empty($data_pli->fichier_circulaire) && !empty($data_pli->nom_orig_fichier_circulaire)) ? TRUE : FALSE))
					,'nom_ci' => compose_nom_ci($pli->lot_scan, $id_pli, $pli->pli, $data_pli->nom_orig_fichier_circulaire)
				);
				echo json_encode(array(
					'code' => 1
					,'ihm' => $this->load->view('visual/pli', $data_view, TRUE)
				));
				return '';
			}
		}
		log_message('error', 'visual> visual> display_pli: failed loading pli view pli#'.$id_pli);
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Pli introuvable!'
		));
	}

	public function histo_pli($id_pli){
		if(is_numeric($id_pli)){
			$this->load->model('visual/model_pli');
			$pli = $this->model_pli->pli($id_pli);
			$data_events = $this->model_pli->histo($id_pli);
			$pli_events = array();
			foreach ($data_events as $data_event) {
				$event = new stdClass();
				$event->flag = $data_event->flag;
				$event->color = $data_event->flag == 0 ? 'warning' : 'info';
				$event->header = $data_event->flag == 0 ? $data_event->lbl : $data_event->lbl;
				$event->body = $data_event->flag == 0 ? 'Action de '.$data_event->login : 'changement d\'&eacute;tape';
				$event->moment = $data_event->moment == '' ? '' : (new DateTime($data_event->moment))->format('d/m/Y G:i:s');
				$event->coms = $data_event->com == '' ? '' : '('.$data_event->com.')';
				$event->motif_ko = $data_event->motif_ko;
				$event->dt_motif_ko = $data_event->dt_motif_ko == '' ? '' : (new DateTime($data_event->dt_motif_ko))->format('d/m/Y G:i:s');
				$event->consigne_ko = $data_event->consigne_ko;
				$event->dt_consigne = $data_event->dt_consigne == '' ? '' : (new DateTime($data_event->dt_consigne))->format('d/m/Y G:i:s');
				array_push($pli_events, $event);
			}
			$data_view = array(
				'pli_events' => $pli_events
				,'dt_courrier' => $pli->date_creation == '' ? '' : (new DateTime($pli->date_creation))->format('d/m/Y G:i:s')
			);
			if(!is_null($pli)){
				echo json_encode(array(
					'code' => 1
					,'ihm' => $this->load->view('visual/histo', $data_view, TRUE)
				));
				return '';
			}
		}
		log_message('error', 'visual> visual> histo_pli: failed loading histo pli view pli#'.$id_pli);
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Pli introuvable!'
		));
	}

	public function lot_saisie_view($id_lot_saisie){
		if(!empty($id_lot_saisie)){
			$this->load->model('visual/model_pli');
			$lot_saisie = $this->model_pli->detail_lot_saisie($id_lot_saisie);
			if(!is_null($lot_saisie)){
				$data_view = array(
					'lot' => $lot_saisie
				);
				echo json_encode(array(
					'code' => 1
					,'ihm' => $this->load->view('visual/lot_saisie', $data_view, TRUE)
				));
				return '';
			}
		}
		log_message('error', 'visual> visual> lot_saisie_view: failed loading lot_saisie view #'.$id_lot_saisie);
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Lot saisie introuvable!'
		));
	}

	public function set_consigne_ke(){
		$id_pli = $this->input->post('id_pli');
		$consigne = $this->input->post('comms');
		$new_files = json_decode($this->input->post('files'));
		$deleted_files = json_decode($this->input->post('deleted_oid'));
		if(is_numeric($id_pli) && $consigne != ''){
			$data = new stdClass();
			$data->id_pli = $id_pli;
			$data->data_pli_ke = array(
				'id_pli' => $id_pli
				,'commentaire' => $consigne
			);
			$data->deleted_oids = $deleted_files;
			$this->load->helper('file');
			$data->fichier_ke = array();
			$links = array();
			foreach ($new_files as $key_new_file => $new_file) {
				$link = './'.urlFileTemp.$new_file->link;
				$data_new_file = array(
					'id_pli' => $id_pli
					,'nom_orig' => $new_file->orig_name
					,'fichier' => pg_escape_bytea(file_get_contents($link))
					,'taille' => filesize($link)
				);
				array_push($links, $link);
				array_push($data->fichier_ke, $data_new_file);
			}
			$this->load->model('visual/model_pli');
			try {
				$this->model_pli->save_data_consigne_ke($data);
			} catch (Exception $th) {
				log_message('error', 'controller> visual> visual> set_consigne_ke: '.$th->getMessage());
				echo 'Erreur enregistrement.';
				return '';
			}
			echo 1;
			foreach ($links as $link) {
				if(!delete_files($link)){
					log_message('error', 'controller> visual> visual> set_consigne_ke: suppression impossible du fichier '.$link);
				}
			}
		}else{
			echo 'Données manquantes';
		}
	}

	public function input_ke($id_pli){
		if(is_numeric($id_pli)){
			$this->load->model('visual/model_pli');
			$data_view = array(
				'id_pli' => $id_pli
				,'info_ke' => $this->model_pli->info_ke($id_pli)
			);
			$this->load->helper('number');
			$reponse = $this->load->view('visual/input_ke', $data_view, TRUE);
		}else{
			$reponse = '<div class="modal-body" ><div class="alert alert-warning"><strong>Erreur!</strong> Pli introuvable.</div></div>';
			$reponse .= '<div class="modal-footer"><button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button></div>';
		}
		echo $reponse;
	}

	public function input_file_ke(){
		$config_upload = array(
			'upload_path' => './'.urlFileTemp
			,'overwrite' => FALSE
			,'file_ext_tolower' => TRUE
			,'encrypt_name' => TRUE
			,'allowed_types' => '*'
			,'max_size' => '512'
		);
		$this->load->library('upload', $config_upload);
		if($this->upload->do_upload('file')){
			echo json_encode(array(
				'code' => 1
				,'link' => $this->upload->data('file_name')
				,'ext' => $this->upload->data('file_ext')
				,'name' => $this->upload->data('orig_name')
			));
			$this->clear_old_file('./'.urlFileTemp);
		}else{
			echo json_encode(array(
				'code' => 0
				,'er' => $this->upload->display_errors()
			));
		}
	}
	
	private function clear_old_file($rep=NULL, $days=2){
		if(is_null($rep)){
			return '';
		}
		$dt_limit = new DateTime(date('Y-m-d'));
		$dt_limit->modify('-'.$days.' day');
		$this->load->helper('directory');
		$files = directory_map($rep, 1);
		foreach ($files as $file) {
			$dt_last_modif = filemtime($rep.$file);
			if($dt_limit->format('U') > $dt_last_modif && $file != 'index.html'){
				if(file_exists($rep.$file)){
					unlink($rep.$file);
				}
			}
		}
	}

	public function download_file_ke($oid_file_ke){
		$this->load->helper('download');
		$this->load->helper('file');
		if (is_numeric($oid_file_ke)) {
			$this->load->model('visual/model_pli');
			$fichier = $this->model_pli->fichier_ke($oid_file_ke);
			if(!is_null($fichier)){
				force_download($fichier->nom_orig, pg_unescape_bytea($fichier->fichier));
				return '';
			}
		}
		log_message('error', 'controller> visual> visual> download_file_ke: fichier introuvable! oid#'.$oid_file_ke);
		force_download('error.txt', 'Fichier introuvable');
	}
	
	public function download_circulaire($id_pli){//deprecated: use control/ajax/download_circulaire
		if (is_numeric($id_pli)) {
			$this->load->helper('function1');
			$this->load->helper('download');
			$this->load->helper('file');
			$this->load->model('visual/model_pli');
			$data_pli = $this->model_pli->data_pli($id_pli);
			if(!is_null($data_pli)){
				//$nom_ci = compose_nom_ci($data_pli->lot_scan, $id_pli, $data_pli->pli, $data_pli->nom_orig_fichier_circulaire);
				force_download($data_pli->nom_orig_fichier_circulaire, read_file('./'.upFileCirc.$data_pli->fichier_circulaire));
				return '';
			}
			force_download('no_fichier.txt', 'Pas de fichier CI pour le pli#'.$id_pli);
			return '';
		}
		log_message('error', 'controller> visual> visual> download_circulaire: Pli introuvable! pli#'.$id_pli);
		force_download('error.txt', 'Pli introuvable: pli#'.$id_pli);
	}
	
	public function download_pjs($id_pli){
		if (is_numeric($id_pli)) {
			$this->load->library('zip');
			$this->load->model('visual/Model_pli');
			$pjs = $this->Model_pli->ged_pjs($id_pli);
			foreach ($pjs as $pj) {
				$this->zip->add_data($pj->n_ima_recto, base64_decode($pj->n_ima_base64_recto));
				$this->zip->add_data($pj->n_ima_verso, base64_decode($pj->n_ima_base64_verso));
			}
			if(count($pjs) > 0){
				$this->zip->download('pjs_pli'.$id_pli.'.zip');
			}else{
				$this->load->helper('download');
				force_download('no_pj.txt', 'Aucun PJ pour le pli#'.$id_pli);
			}
		}else{
			log_message('error', 'controller> visual> visual> download_pjs: Pli introuvable! pli#'.$id_pli);
			force_download('error.txt', 'Pli introuvable: pli#'.$id_pli);
		}
	}
	
	public function download_all_docs($id_pli){
		if (is_numeric($id_pli)) {
			ini_set('memory_limit', '-1');
			$this->load->library('zip');
			$this->load->model('visual/Model_pli');
			$pjs = $this->Model_pli->docs($id_pli);
			foreach ($pjs as $pj) {
				$this->zip->add_data($pj->n_ima_recto, base64_decode($pj->n_ima_base64_recto));
				$this->zip->add_data($pj->n_ima_verso, base64_decode($pj->n_ima_base64_verso));
			}
			$this->zip->download('documents_pli'.$id_pli.'.zip');
		}else{
			log_message('error', 'controller> visual> visual> download_all_docs: Pli introuvable! pli#'.$id_pli);
			force_download('error.txt', 'Pli introuvable: pli#'.$id_pli);
		}
	}

	public function comments_pli()
	{
		if(isset($_REQUEST['id_pli']) && is_numeric($_REQUEST['id_pli'])){
			$id_pli = $_REQUEST['id_pli'];
			$new_comment = NULL;
			$this->load->library('portier');
			if(isset($_REQUEST['comment']) && $this->portier->valide){
				$new_comment = array(
					'id_pli_comment' => $id_pli
					,'commentaire' => $_REQUEST['comment'] == '' ? 'No comment' : $_REQUEST['comment']
					,'commented_by' => $this->session->id_utilisateur
				);
			}
			$this->load->model('visual/Model_pli');
			$comments = $this->Model_pli->comments($id_pli, $new_comment);
			echo $this->load->view('visual/comments', array('comments'=>$comments), TRUE);
			return '';
		}
		echo 'Erreur pli#'.$_REQUEST['id_pli'];
	}
	
	public function detail_pli($id_pli)
	{
		if(is_numeric($id_pli)){
			$this->load->model('visual/model_pli');
			$this->load->helper('function1');
			$pli = $this->model_pli->pli_view($id_pli);
			if(!is_null($pli)){
				$data_lot_saisie = $this->model_pli->detail_lot_saisie($pli->id_lot_saisie);
				$pli_lot = array();
				$pli_lot_bis = array();
				if(!is_null($data_lot_saisie)) {
					if($data_lot_saisie->nbpli > 0) {
						$pli_lot = str_replace('#', '', $data_lot_saisie->plis);
						$pli_lot = str_replace(' ', '', $pli_lot);
						$pli_lot = explode('|', $pli_lot);
					}
					if($data_lot_saisie->nbpli_bis > 0) {
						$pli_lot_bis = str_replace('#', '', $data_lot_saisie->plis_bis);
						$pli_lot_bis = str_replace(' ', '', $pli_lot_bis);
						$pli_lot_bis = explode('|', $pli_lot_bis);
					}
				}
				$data_view = array(
					'histo' => $this->view_histo_pli($id_pli)
					,'docs' => $this->view_docs_offset($id_pli)
					,'nb_docs' => $this->model_pli->nb_docs_pli($id_pli)
					,'empty_b64' => base64_encode(file_get_contents(base_url('assets/images/empty_papper.jpg')))
					,'paies' => $this->model_pli->paiement_view($id_pli)
					,'pli' => $pli
					,'pli2' => $this->model_pli->data_pli($id_pli)
					,'chqs' => $this->model_pli->chqs_b64($id_pli)
					,'chqs_kd' => $this->model_pli->chqs_kd_b64($id_pli)
					,'mvmts' => $this->model_pli->mouvements($id_pli)
					,'id_pli' => $id_pli
					,'lot_saisie' => $data_lot_saisie
					,'pli_lot' => $pli_lot
					,'pli_lot_bis' => $pli_lot_bis
					,'motifs_cons' => $this->model_pli->get_all_motif_consigne($id_pli)
					,'ui_id' => '_'.date('YmdHis').mt_rand()
					,'comments' => $this->model_pli->comments($id_pli)
				);
				echo json_encode(array(
					'code' => 1
					,'titre' => 'Pli#'.$id_pli.' - '.$pli->nom_societe.' - '.$pli->typo
					,'sous_titre' => $this->load->view('visual/detail_pli_tab', $data_view, TRUE)
					,'footer' => '<a href="'.(site_url('visual/visual/download_all_docs/')).$id_pli.'" download="" type="button" class="btn btn-xs btn-primary waves-effect m-t--10" title="T&eacute;l&eacute;charger">'
						.'<i class="material-icons">file_download</i><span>Pli.zip</span></a>'
					,'body' => $this->load->view('visual/detail_pli', $data_view, TRUE)
				));
				return '';
			}
		}
		log_message('error', 'visual> visual> detail_pli: pli#'.$id_pli.' introuvable');
		echo json_encode(array(
			'code' => 0
			,'msg' => 'Pli introuvable!'
		));
	}
	
	public function view_histo_pli($id_pli)
	{
		$pli = $this->model_pli->pli($id_pli);
		$data_events = $this->model_pli->histo($id_pli);
		$pli_events = array();
		foreach ($data_events as $data_event) {
				$event = new stdClass();
				$event->flag = $data_event->flag;
				$event->color = $data_event->flag == 0 ? 'warning' : 'info';
				$event->header = $data_event->flag == 0 ? $data_event->lbl : $data_event->lbl;
				$event->body = $data_event->flag == 0 ? 'Action de '.$data_event->login : 'changement d\'&eacute;tape';
				$event->moment = $data_event->moment == '' ? '' : (new DateTime($data_event->moment))->format('d/m/Y G:i:s');
				$event->coms = $data_event->com == '' ? '' : '('.$data_event->com.')';
				$event->motif_ko = $data_event->motif_ko;
				$event->dt_motif_ko = $data_event->dt_motif_ko == '' ? '' : (new DateTime($data_event->dt_motif_ko))->format('d/m/Y G:i:s');
				$event->consigne_ko = $data_event->consigne_ko;
				$event->dt_consigne = $data_event->dt_consigne == '' ? '' : (new DateTime($data_event->dt_consigne))->format('d/m/Y G:i:s');
				array_push($pli_events, $event);
			}
			if(!is_null($pli)){
				$data_view = array(
					'pli_events' => $pli_events
					,'dt_courrier' => $pli->date_creation == '' ? '' : (new DateTime($pli->date_creation))->format('d/m/Y G:i:s')
				);
				return $this->load->view('visual/histo', $data_view, TRUE);
			}else{
				return '<div class="alert alert-warning">'
					.'<strong>Aucune</strong> donn&eacute;es.'
					.'</div>';
			}
	}
	
	public function view_docs_offset($id_pli, $offset=0, $id_dom=NULL)
	{
		$this->load->model('visual/model_pli');
		$this->load->helper('function1');
		$limit = 25;
		$total = $this->model_pli->nb_docs_pli($id_pli);
		$nb_btn = $total > 0 ? ceil($total / $limit) : 0;
		$btn_current = floor($offset / $limit);
		$data_view_docs = array(
			'docs' => $this->model_pli->visu_docs_offset($id_pli, $offset, $limit)
			,'limit' => $limit
			,'nb_btn' => $nb_btn
			,'btn_current' => $btn_current
			,'offset' => $offset
			,'total' => $total
			,'id_pli' => $id_pli
			,'all' => is_null($id_dom)
			,'id_dom' => $id_dom
		);
		if(is_null($id_dom)){
			return $this->load->view('visual/docs_offset', $data_view_docs, TRUE);
		}else{
			echo json_encode(array(
				'code' => 1
				,'ihm' => $this->load->view('visual/docs_offset', $data_view_docs, TRUE)
			));
		}
	}

}
