<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller{

    public function index()
    {
        $this->nb_num_vs_ged();
    }

    public function nb_num_vs_ged()
    {
        $this->load->model('visual/model_notification');
        $data_view = array(
            'dbs_courrier' => $this->model_notification->num_vs_ged_courrier()
            ,'dbs_mail' => $this->model_notification->num_vs_ged_mail()
        );
        $this->load->view('visual/notification/nb_num_vs_ged', $data_view);
    }

    public function detail_num_vs_ged()
    {
        $this->load->model('visual/model_notification');
        $data_view = array(
            'dbs_courrier' => $this->model_notification->detail_num_vs_ged_courrier()
            ,'dbs_mail' => $this->model_notification->detail_num_vs_ged_mail()
        );
        echo json_encode(array(
            'code' => 1
            ,'titre' => 'NUM&Eacute;RIS&Eacute;S Vs PLIS'
            ,'sous_titre' => $this->load->view('visual/notification/detail_num_vs_ged_tab', $data_view, TRUE)
            ,'footer' => ''
            ,'body' => $this->load->view('visual/notification/detail_num_vs_ged', $data_view, TRUE)
        ));
    }

}