<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KO extends CI_Controller
{
    private $CI;
    public function __construct()
    {
        parent::__construct();

        /**
         * type user = 8 => cdn
         * type user = 1 => admin
         * type user = 4 => obs
         */

        if(intval($this->session->userdata('avec_traitement_ko')) == 1 || intval($this->session->userdata('id_type_utilisateur')) == 8)
        {
            $this->CI =& get_instance();
            $this->load->model("traitement_ko/Model_Traitement_KO", "MtraitementKO");
        }
        else
        {
            redirect(base_url());
            exit("Pas d'accès pour vous"); 
        }
        
		
    }

    public function index()
    {
        $this->viewPrincipale();
    }

    
    
    public  function viewPrincipale()
    {
        $head                        = array();
        $foot                        = array();

        $head["title"]               = $head["menu"] = "Traitement des KO / Circulaire";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "bug_report";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/dataTable/dataTables.bootstrap',
            'plugins/dataTable/fixedHeader.bootstrap',
            'plugins/dataTable/responsive.bootstrap',
           'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/jquery-datatable/extensions/dataTables.checkboxes',
            'plugins/nprogress/nprogress',
            'plugins/morrisjs/morris',
            'css/style',
            'css/themes/all-themes',
            'plugins/daterangepicker/daterangepicker',
            'plugins/multi-select/css/multi-select',
            'plugins/timeline/timeline',
            'plugins/magnify/jquery.magnify',
             'css/recherche/recherche',
            '../src/template/css/font-awesome.min',
            'plugins/contextMenu/jquery.contextMenu',
            'plugins/jspanel/jspanel',
            'css/traitement_ko/ko'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap'
            
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/dataTable/jquery.dataTables',
            'plugins/dataTable/dataTables.fixedHeader',
            'plugins/dataTable/dataTables.responsive',
            'plugins/dataTable/responsive.bootstrap',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            /*'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',*/
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/jquery-datatable/extensions/dataTables.checkboxes',
            
            
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
           'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/daterangepicker/moment.min',
            'plugins/magnify/jquery.magnify',
            'plugins/daterangepicker/daterangepicker',
           'plugins/multi-select/js/jquery.multi-select',
            'plugins/multi-select/js/jquery.quicksearch',
            'plugins/sweetalert/sweetalert',
            'plugins/popper/popper',
            'plugins/contextMenu/jquery.contextMenu',
            'plugins/jspanel/jspanel',
            'plugins/jspanel/extensions/contextmenu/jspanel.contextmenu',
            'plugins/jspanel/extensions/hint/jspanel.hint',
            'plugins/jspanel/extensions/modal/jspanel.modal',
            'plugins/jspanel/extensions/tooltip/jspanel.tooltip',
            'plugins/jspanel/extensions/dock/jspanel.dock',
            'plugins/jquery-datatable/datetime-moment',
             'js/traitement_ko/ko',
        );

       $data["etatSaisie"] = $this->MtraitementKO->getEtatPli();
        
        $data["userType"] = $this->getUserTypeGed();
        $this->load->view("main/header",$head);
        $this->load->view("anomalie/traitement_ko/gestion", $data);
        $this->load->view("main/footer",$foot);

       /* echo "<pre>";
        print_r($this->session->all_userdata());
        echo "</pre>";*/
    }
    
    public function listePliKo()
    {
        $data               = array();
        $aListeStatutSaisie = array(-1);
        $this->load->helper('text');
        $typeUser           = $this->getUserTypeGed(); /**CDT, Client,CDN*/
        $userGedType        = trim(mb_strtolower($typeUser));
        $listeEtatPli       = $this->input->post("etatPli");
        $filtreEtape        = $this->input->post("filtreEtape");
        $societe            = $this->input->post("societe");
        $source             = intval($this->input->post("source")); /* 1 : flux ; 2 : courrier */
        $typologie             = $this->input->post("typologie"); /* 1 : flux ; 2 : courrier */

        $daty               = $this->input->post("daty");
        $aListeStatutSaisie = array(0);
        switch($userGedType)
        {
            case 'cdt':
                $aListeStatutSaisie = array(3, 5, 6, 2, 4, 7,10,9);
            break;

            case 'client':
                $aListeStatutSaisie = array( 7,3,4,5,6,10,9,12,24,11,12,24,);
                break;
            case 'cdn':
                $aListeStatutSaisie = array(2,7,10,9,11);
            break;
        }

        if(($source == 1 || $source ==  3) && ($userGedType == "client" || $userGedType == "cdt" ))
        {
            $aListeStatutSaisie = array(11,12,14,15,19,21,22,23,24);
        }
        
        
        $list           = $this->MtraitementKO->listePliKo($aListeStatutSaisie,$listeEtatPli,$filtreEtape,$societe,$source,$daty,$typologie); 
        $i              = 0;
        foreach($list as $ko)
        {
            $row        = array();
            $etape      = trim(mb_strtolower($ko->etape));

            if(intval($ko->source_traitement) == 2)
            {
                $checkbox = '<span style="display:none;" class="input-group-addon"><input type="checkbox" class="filled-in chk-col-indigo c-id-pli" id="'.$ko->id_ged.'" attrib-ko-avant="'.$ko->statut_avant_ko_encours.'"  attrib-etape="'.$etape.'" attrib-statut-saisie="'.$ko->statut_saisie.'"><label for="'.$ko->id_ged.'"></label></span>';
            }
            else
            {
                $checkbox = '<span style="display:none;" class="input-group-addon"><input type="checkbox" class="filled-in chk-col-teal c-id-pli" id="'.$ko->id_ged.'" attrib-ko-avant="'.$ko->statut_avant_ko_encours.'"  attrib-etape="'.$etape.'" attrib-statut-saisie="'.$ko->statut_saisie.'"><label for="'.$ko->id_ged.'"></label></span>';
            }
            
            /*$row[]      = $checkbox; si avec multi select*/
            
            $row[]      = $checkbox.$ko->id_ged; 
           
            $row[]      = "<center>".$ko->nom_societe."</center>";
            $row[]      = $ko->courrier_date;
            $row[]      = $ko->date_dernier_statut_ko;
            $row[]      = $ko->source;
            $row[]      = $ko->typologie;
            $row[]      = $ko->etape;
            $row[]      = $ko->libelle;
            
            $isMailSftp = 0;

            $tempSource  = trim(mb_strtolower($ko->source));
            if($tempSource == "mail" || $tempSource == "sftp")
            {
                $isMailSftp = 1;
            }

            /*if (strlen($ko->motif_operateur) > 150)
            {
                $str    = substr($$ko->motif_operateur, 0, strpos($$ko->motif_operateur, ' ', 150)) ;
                $row[]  =  $str.'<span  style="cursor:pointer;color:#1f91f3;" data-placement="left" data-toggle="tooltip" title="'.$ko->motif_operateur.'">ici</span>';
            }
            else
            {
                $row[]  = $ko->motif_operateur;
            }*/
            
            $row[]  = $ko->motif_operateur;
            
            if(intval($this->session->userdata('id_type_utilisateur')) == 8)
            {
                
                $row[]     = $ko->lot_scan;
                $row[]     = $ko->commande;
            }
            $row[]     = $ko->date_envoi_ci;
            
            
         
            if(intval($ko->commentaire) > 0)
            {
                $row[]  = '<span class="align-center badge bg-cyan font-15" style="cursor:pointer;" data-html="true" onclick="comment_pli('.$ko->id_ged.','.$isMailSftp.');" data-placement="left" data-toggle="tooltip" title="Cliquer pour ajouter/ voir <br/> le commentaire">'.$ko->commentaire.'</span>';
            }
            else
            {
                $row[]  = '<span class="align-center badge bg-cyan font-15" style="cursor:pointer;" data-html="true" onclick="comment_pli('.$ko->id_ged.','.$isMailSftp.');" data-placement="left" data-toggle="tooltip" title="Cliquer pour ajouter <br/> le commentaire">+</span>';
            }
            
            $data[]    = $row;
            $i++;
        }

        
        $output         = array("data" => $data);
        
        echo json_encode($output);
    }

    public function setEtatPli()
    {
       
        
        $batchPli                           = array();
        $batchDataPli                       = array();
        $listePli                           = array();
        $sortie                             = array();
        $dataPli                            = array();
        $pli                                = array();
        $pliNotIn                           = array(0);
        $idMotif                            = 0;

        $sortie                             = array ("a_traiter"=> 1 ,
                                                "ci_editee"=> 9,
                                                "ci_envoyee"=>10,
                                                "cloture_src"=>0,
                                                "cloture"=> 8 ,
                                                "ko_ci"=> 7,
                                                "kd"=> 3,
                                                "ko_en_cours_by"=> 12,
                                                "ko_en_attente"=> 6);
        
        
		$tracePli                           = array(
                                            "a_traiter"=> 120 ,
                                            "ci_editee"=> 111,
                                            "ci_envoyee"=>114,
                                            "cloture_src"=>116,
                                            "cloture"=> 109 ,
                                            "ko_ci"=> 112,
                                            "kd"=> 110,
                                            "ko_en_cours_by"=> 126,
                                            "ko_en_attente"=> 113
                                        );
										
        $etape                              = trim(mb_strtolower($this->input->post("etape")));
      
        $listePli                           = $this->input->post("listePli");
        
        $source                             = intval($this->input->post("source")); /**2 : courrier; 1 : flux  */
        $currentEtatSaisie                  = intval($this->input->post("currentEtatSaisie")); 


        /**
        * verifier si le pli est déjà updat-er par d'autre pers
        */
        
        if(intval($this->input->post("liste_diff")) > 0 )
        {
            $pliNotIn                      = explode(",",$this->input->post("liste_diff"));
        }
        
        
        if($source == 2 )
        {   
            $dataPli["par_ttmt_ko "]            = 1;
            $dataPli["flag_batch"]              = 0;
            $dataPli["statut_avant_ko_encours"] = null;

            if(trim(mb_strtolower($this->input->post("nouvelEtat"))) == "a_traiter")
                {
                    $dataPli["date_envoi_ci"]           = null;
                    $dataPli["motif_ci"]                = null;
                    /*$pli["recommande"]                  = 1; proble CE enregistrement partiel*/
                    $pli["id_lot_saisie"]               = null;
                    
                    
                    
                    switch($etape)
                    {
                        case "typage":
                            $dataPli["flag_traitement"] = 0;
                            $dataPli["typologie"]       = 0;
                            $dataPli["titre"]           = null;
                            $dataPli["code_promotion"]  = null;
                            $dataPli["statut_saisie"]   = 1;
                            

                            $pli["flag_traitement"]     = 0;
                            $pli["typologie"]           = 0;
                            $pli["id_groupe_pli"]       = null;
                            $pli["typage_par"]          = null;
                            $pli["statut_saisie"]       = 1;
                            $pli["date_typage"]         = null;
                            break;

                        case 'saisie':
                            $dataPli["flag_traitement"] = 3;
                            $dataPli["statut_saisie"]   = 1;

                            $pli["flag_traitement"]     = 3;
                            $pli["statut_saisie"]       = 1;
                            $pli["saisie_par"]          = null;
                            $pli["date_saisie"]         = null;
                            break;
                    }
                }
                elseif(trim(mb_strtolower($this->input->post("nouvelEtat"))) == "cloture") /**abandonner le traitement : statut_saisie = 8 */
                {
                    $pli["statut_saisie"]               = 8;
                    $dataPli["statut_saisie"]           = 8;
                    $pli["flag_traitement"]             = 14;
                    $dataPli["flag_traitement"]         = 14;
                }
                elseif(trim(mb_strtolower($this->input->post("nouvelEtat"))) == "cloture_src") 
                {
                    $pli["flag_traitement"]             = 14;
                    $dataPli["flag_traitement"]         = 14;
                    $pli["statut_saisie"]               = 1;
                    $dataPli["statut_saisie"]           = 1;
                }
                else /*kd,ko_ci,ko_en_attente,ci_editee,ci_envoyee,ko_en_cours_by*/
                {
                    $nouvelEtat                         = trim(mb_strtolower($this->input->post("nouvelEtat")));
                    $flagNvlEtat                        = $sortie[$nouvelEtat];

                    $pli["statut_saisie"]               = intval($flagNvlEtat);
                    $dataPli["statut_saisie"]           = intval($flagNvlEtat);
                    //currentEtatSaisie
                    if($nouvelEtat == "ci_envoyee")
                    {
                        $date                           = DateTime::createFromFormat('d/m/Y',$this->input->post("dateEnvoieCi"));
                        $dataPli["date_envoi_ci"]       = $date->format('Y-m-d');
                    }
                    elseif($nouvelEtat == "ko_en_cours_by") /* TODO KO EN COURS BAYARD*/
                    {
                        
                        $dataPli["statut_avant_ko_encours"]       = $currentEtatSaisie;
                    }
                }

                /*setEtapli($aPli,$aDataPli, $listePli)*/ 
                $iSenAttente = intval($this->input->post("en_attente"));
                
                
                if($iSenAttente >= 1)
                {
                    $motifConsigne["consigne_client"]  = $this->input->post("consigne");
                    $motifConsigne["dt_consigne"]      = date("Y-m-d H:i:s");
                    $idMotif                           = intval($this->input->post("id_motif"));
                    $retour = $this->MtraitementKO->setEtapli($pli,$dataPli, $listePli,$pliNotIn,$idMotif,$motifConsigne); 
                }
                else
                {
                    $retour = $this->MtraitementKO->setEtapli($pli,$dataPli, $listePli,$pliNotIn,$idMotif); 
                }

                $nouvelEtat  = trim(mb_strtolower($this->input->post("nouvelEtat")));
                $idTrace     = $tracePli[$nouvelEtat];



                if(intval($idTrace) > 0 && intval($retour["update"]) > 0)
                {
                    $this->CI->histo->action_plis(intval($idTrace), "", $listePli);
                    $this->CI->histo->plis($listePli);
                } 
        }
        else if($source == 1 || $source == 3)
        {
            
            $aflux          = array();
            $etape          = trim(mb_strtolower($this->input->post("etape")));
            $iConsigne      = 0 ;
            $trace          = 0;
            $aflux["statut_avant_ko_encours"] = null;

            if(trim(mb_strtolower($this->input->post("nouvelEtat"))) == "a_traiter")
            {
                $aflux["etat_pli_id"]    = 1;
                $aflux["flag_important"] = 1;
                $iConsigne               = 1 ;
                $trace = 120;
                switch($etape)
                    {
                        case "typage":
                            $aflux["statut_pli"]                 = 0;
                            $aflux["typologie"]                  = 0;
                            $aflux["type_par"]                   = 0;
                            $aflux["mail_reception_typage"]      = 0;
                            $aflux["nb_abonnement"]              = 0;
                            break;

                        case 'saisie':
                            $aflux["statut_pli"]                = 9;
                            break;
                    }
            }
            else if(trim(mb_strtolower($this->input->post("nouvelEtat"))) == "cloture")
            {
                $aflux["etat_pli_id"]    = 100;
                $trace = 109;
            }
            else if(trim(mb_strtolower($this->input->post("nouvelEtat"))) == "cloture_src")
            {
                $aflux["etat_pli_id"]      = 1;
                $aflux["statut_pli"]       = 7;
                $aflux["cloture_flux"]     = 1;
                
                $trace = 109;
               
            }
            else if(trim(mb_strtolower($this->input->post("nouvelEtat"))) == "ko_en_cours_by")
            {
                $aflux["etat_pli_id"]    = 24;
                $trace                   = 126;
                $aflux["statut_avant_ko_encours"]       = $currentEtatSaisie;
            }
            
            
            if($iConsigne >= 1 )
            {
                
                $idMotif                           = intval($this->input->post("id_motif"));
                $retour = $this->MtraitementKO->setEtatFlux($listePli,$aflux,$pliNotIn,$idMotif,array("consigne_client_flux"=>$this->input->post("consigne"))); 
            }
            else
            {
                $retour = $this->MtraitementKO->setEtatFlux($listePli,$aflux,$pliNotIn,$idMotif); 
            }
           
            
            if(intval($retour["update"]) > 0)
            {
                $this->CI->histo->action_plis(intval($trace), "", $listePli);
                $this->CI->histo->flux_tab($listePli);
            } 

        }

        print_r($dataPli);
        print_r($pli);
    }

    
    public function getUserTypeGed()
    {
        /**
         * type user = 8 => cdn
         * type user = 1 => admin
         * type user = 4 => obs
         */

        if(intval($this->session->userdata('avec_traitement_ko')) == 1 && intval($this->session->userdata('id_type_utilisateur')) == 1)
        {
            return "cdt";
        }
        elseif(intval($this->session->userdata('avec_traitement_ko')) == 1 && intval($this->session->userdata('id_type_utilisateur')) == 4)
        {
            return "client";
        }

        if(intval($this->session->userdata('id_type_utilisateur')) == 8)
        {
            return "cdn";
        }

        return "";
        
    }
    
    public function getEtatPli($all = "all")
    {
        $etatSaisie = $this->MtraitementKO->getEtatPli();

        $strEtatPli  = '<select class="form-control show-tick elem_filtre"  onchange="setFiltreStatut();" id="etat-pli" tabindex="-98"  data-live-search="true"  multiple>';
        $strEtatPli .= '<option value="0"  disabled>--Choisir le statut du pli--</option>';
        $strEtatPli .= '<option value="all" >--Tout sélectionner--</option>';
        $strEtatPli .= '<option value="none" >--Tout déselectionner--</option>';
        
        $selected = "";
        if($all == "all")
        {
            $selected = " selected ";
        }
        if($all == "none")
        {
            $selected = "";
        }
        foreach($etatSaisie as $valueEtatSaisie)
        {
            $strEtatPli .= "<option value='".intval($valueEtatSaisie->id_statut_saisie)."' ".$selected.">".$valueEtatSaisie->libelle."</option>";
        }
        $strEtatPli .="</select>";
        echo $strEtatPli;
    }
	
	public function getConsigne($idPli)
	{
		if(intval($idPli) > 0)
		{
			$motifConsigne = $this->MtraitementKO->getMotifConsigne(array($idPli));
			if($motifConsigne)
			{
				if(trim($motifConsigne[0]->consigne_client) != "")
				{
					echo $motifConsigne[0]->consigne_client;
					return "";
				}
				echo "";
				return "";
			}
			echo "";
			return "";
		}
		
		echo "";
		return "";
	}

    public function getMotifConsigne() 
    {
        $listePli           = $this->input->post("listePli");
        $retour             = array() ;
        
        $retour["id_motif"] = 0;
        $retour["motif"]    = 0;
        $motifConsigne      = null;
        if(intval($this->input->post("source")) == 1 || intval($this->input->post("source")) == 3 )
        {
            $motifConsigne = $this->MtraitementKO->getMotifConsigneFlux($listePli);
            
        }
        else
        {
            $motifConsigne = $this->MtraitementKO->getMotifConsigne($listePli);
        }
		
        if($motifConsigne)
        {
            $retour["id_motif"] = $motifConsigne[0]->id_motif;
            $retour["motif"]    = $motifConsigne[0]->motif_operateur;
            
        }
        echo json_encode($retour);
        return "";
    }


    public function etatSaisieGedBySource($source=2,$all="")
    {
        $etatSaisie = $this->MtraitementKO->etatSaisieGedBySource($source);
        $isBoutonReset = $this->input->post("isBoutonReset");

        $strEtatPli  = '<select class="form-control show-tick elem_filtre"  onchange="setFiltreStatut();" id="etat-pli" tabindex="-98"  data-live-search="true"  multiple>';
        $strEtatPli .= '<option value="0"  disabled>--Choisir le statut du pli--</option>';
        $strEtatPli .= '<option value="all" >--Tout sélectionner--</option>';
        $strEtatPli .= '<option value="none" >--Tout déselectionner--</option>';
        $aKoAttente =  array(6,23);
        $selected = "";
        if($all == "all")
        {
            $selected = " selected ";
        }
        if($all == "none")
        {
            $selected = "";
        }
        foreach($etatSaisie as $valueEtatSaisie)
        {
           
            $selected2   = "";
            if(intval($this->session->userdata('id_type_utilisateur')) == 4 && in_array(intval($valueEtatSaisie->id_statut_saisie),$aKoAttente) && trim($isBoutonReset) == "non")
            {
                $selected2  = " selected ";
            }
            $strEtatPli .= "<option value='".intval($valueEtatSaisie->id_statut_saisie)."' ".$selected." ".$selected2.">".$valueEtatSaisie->libelle."</option>";
        }

        $strEtatPli .="</select>";
        echo $strEtatPli;
    }
    //TODO verification traitement KO avant modal et avant validation MAJ
    public function verificationAvantValidation()
    {
        /*var objPliAverifier = {
            "pliAverifier" :pliAverifier, 
            "intSourceTraitement" :intSourceTraitement,
            "etapeSelected" : etapeSelected, 
            "statut" :statutSaisieSelected
        };  */
        $listePi      = array() ;

        $aDiff        = array() ;
        $source       = 0 ;
        $etape        = 0 ; 
        $statut       = 0 ;
        $avecDiff     = 0;
        $listePi      = $this->input->post("pliAverifier");
        $source       = $this->input->post("intSourceTraitement");
        $etape        = $this->input->post("etapeSelected");
        $statut       = $this->input->post("intStatutSaisieSelected");
        $intEtape     = 0 ; 
        $strDiff      = "";

        $listePliDiff = "";
        $strDiff      = "";
        $listeDiff    = "";
        $iTotal       = 0;
        if(trim(strtolower($etape)) == "saisie")
        {
            $intEtape   = 2 ;
        }
        else if(trim(strtolower($etape)) == "typage")
        {
            $intEtape = 1 ; 
        }

        
        $verif = $this->MtraitementKO->verificationAvantValidation($listePi,$source,$intEtape ,$statut);
        sort($verif);
        sort($listePi);
        
        
        foreach($listePi as $a)
        {
            if(!in_array($a,$verif))
            {
                $aDiff[]  =  $a;
                $avecDiff++;
            }
            $iTotal++;
        }

        
        if(intval($avecDiff) > 0 )
        {
            $listePliDiff = implode(", #", $aDiff);
            $listeDiff = implode(",", $aDiff);
            $strDiff       = "#".$listePliDiff;
        }

        /** */
        echo json_encode(array("retour"=>intval($avecDiff), "id_diff"=>$strDiff,"liste_diff"=>$listeDiff,"i_total"=>intval($iTotal)));
        
        return "";
    }


    public function comments_flux()
	{
		if(isset($_REQUEST['id_pli']) && is_numeric($_REQUEST['id_pli'])){
			$id_pli = $_REQUEST['id_pli'];
			$new_comment = NULL;
			$this->load->library('portier');
			if(isset($_REQUEST['comment']) && $this->portier->valide){
				$new_comment = array(
					'id_pli_comment' => $id_pli
					,'commentaire' => $_REQUEST['comment'] == '' ? 'No comment' : $_REQUEST['comment']
					,'commented_by' => $this->session->id_utilisateur
				);
			}
			
			$comments = $this->MtraitementKO->comments($id_pli, $new_comment);
			echo $this->load->view('visual/comments', array('comments'=>$comments), TRUE);
			return '';
		}
		echo 'Erreur pli#'.$_REQUEST['id_pli'];
    }
    
    public function paveTraitementKO()
    {
        $this->load->helper('text');
        $typeUser           = $this->getUserTypeGed(); 
        $userGedType        = trim(mb_strtolower($typeUser));
        $listeEtatPli       = $this->input->post("etatPli");
        $filtreEtape        = $this->input->post("filtreEtape");
        $societe            = $this->input->post("societe");
        $source             = intval($this->input->post("source")); 
        $typologie         = $this->input->post("typologie"); 

        $daty               = $this->input->post("daty");
        $pave               = null;
        $retour             = array();
        $obj                = null;
        /*$pave = $this->MtraitementKO->paveCourrier($daty,$listeEtatPli,$societe);*/
        
        switch($userGedType)
            {
                case 'cdt':
                    $aListeStatutSaisie = array(3, 5, 6, 2, 4, 7,10,9);
                break;
    
                case 'client':
                    $aListeStatutSaisie = array( 7,3,4,5,6,10,9,12,24,11,12,24,);
                    break;
                case 'cdn':
                    $aListeStatutSaisie = array(2,7,10,9,11);
                break;
            }

        if(($source == 1 || $source ==  3) && ($userGedType == "client" || $userGedType == "cdt" ))
        {
            $aListeStatutSaisie = array(11,12,14,15,19,21,22,23);
        }

        if(intval($this->input->post("source")) == 1 || intval($this->input->post("source")) == 3 ) /*sftp et mail */
        {
            $retour["source"]                   = "ms" ;
            $pave = $this->MtraitementKO->paveMailSftp($daty,$listeEtatPli,$societe,$source,$filtreEtape,$aListeStatutSaisie,$typologie);
            
            if($pave) 
            {
                $obj                                     = $pave[0];
                $retour["ko_mail_anomalie"]              = $obj->ko_mail_anomalie ;
                $retour["ko_anomalie_pj"]                = $obj->ko_anomalie_pj ;
                $retour["hors_perim"]                    = $obj->hors_perim ;
                $retour["ko_anomalie_fichier"]           = $obj->ko_anomalie_fichier ;
                $retour["rejeter"]                       = $obj->rejeter ;
                $retour["ko_transfert_src"]              = $obj->ko_transfert_src ;
                $retour["ko_definitif"]                  = $obj->ko_definitif ;
                $retour["ko_inconnu"]                    = $obj->ko_inconnu ;
                $retour["ko_en_attente"]                 = $obj->ko_en_attente ;
                $retour["ko_en_cours_bayard"]            = $obj->ko_en_cours_bayard ;
            }
        }
        else
        {
           $pave = $this->MtraitementKO->paveCourrier($daty,$listeEtatPli,$societe,$filtreEtape,$aListeStatutSaisie,$typologie);
            if($pave) 
            {
                $obj                            = $pave[0];
                $retour["pli_ko_scan"]          = $obj->pli_ko_scan ;
                $retour["pli_ko_definitif"]     = $obj->pli_ko_definitif ;
                $retour["pli_ko_iconnu"]        = $obj->pli_ko_iconnu ;
                $retour["pli_ko_src"]           = $obj->pli_ko_src ;
                $retour["pli_ko_ko_en_attente"] = $obj->pli_ko_ko_en_attente ;
                $retour["pli_ko_circulaire"]    = $obj->pli_ko_circulaire ;
                $retour["pli_ci_editee"]        = $obj->pli_ci_editee ;
                $retour["pli_ko_ci_envoyee"]    = $obj->pli_ko_ci_envoyee ;
                $retour["pli_ok_ci"]            = $obj->pli_ok_ci ;
                $retour["pli_ko_en_cours_by"]   = $obj->pli_ko_en_cours_by ;
                $retour["source"]               = "courrier" ;
                
            }
        }
        echo json_encode($retour);
    }

    public function listeTypologieBySource($source = 2)
    {
        $typologie = $this->MtraitementKO->listeTypologieBySource($source);
        

        $strTypologie  = '<select class="form-control show-tick elem_filtre"   id="filtre-typologie" tabindex="-98"  data-live-search="true"  multiple>';
        $strTypologie .= '<option value="0"  disabled>--Choisir la typologie--</option>';
        
        foreach($typologie as $valuetypologie)
        {
            $strTypologie .= "<option value='".intval($valuetypologie->id_typologie)."' >".$valuetypologie->typologie."</option>";
        }

        $strTypologie .="</select>";
        echo $strTypologie;
    }
}
?>

