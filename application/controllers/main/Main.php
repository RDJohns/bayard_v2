<?php
/**
 * Created by PhpStorm.
 * User: 9420
 * Date: 05/03/2019
 * Time: 14:49
 */

class Main extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->viewPrincipale();
    }
    public  function viewPrincipale()
    {
        $head['css'] = array(
            'plugins/bootstrap/css/bootstrap.min',
            'css/style',
            'css/themes/all-themes',
        );
        $foot['js'] = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
        );

        $this->load->view("main/header",$head);
        $this->load->view("main/footer",$foot);
    }



}