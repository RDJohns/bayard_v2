<?php

class Stat extends CI_Controller
{

    private $listeColExcel;
    private $whereLocation       = " and 1 = 2 ";
    private $whereLocationCE     = " and 1 = 2 ";
    private $whereMail           = " and 4 = 5 ";
    private $whereCourrier       = " and 4 = 5 ";
    private $whereSftp           = " and 4 = 5 ";

    private $activeSheet_0;
    private $activeSheet_1;
    private $activeSheet_2;
    private $activeSheet_3;

    private $societe_            = "";

    public function __construct()
    {
        parent::__construct();
        $this->load->library('ra/Excel',null,'excel');
        $this->load->library('ra/Style_export',null,"StyleExport");

        $this->listeColExcel = $this->excel->listeColExcel();
    }

    public function index()
    {
        
        if((int)$_SESSION['id_type_utilisateur'] == 4 )
        {
            $this->viewPrincipale();

        }
        else
        {
            redirect(base_url());
        }
        
    }

    public  function viewPrincipale()
    {
        

        $head                       = array();
        $foot                       = array();
       
        $head["title"]               = $head["menu"] = "Stat. GED";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            /*'plugins/morrisjs/morris',*/
            'plugins/jquery-confirm/jquery-confirm.min',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            'css/custom/reception',
            'css/stat/stat',
            'css/stat/table',
			'../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            /*'plugins/raphael/raphael.min',
            'plugins/morrisjs/morris'*/
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            //'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/jquery-datatable/extensions/dataTables.rowsGroup',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'js/admin',
            /*'js/pages/tables/jquery-datatable',*/
            'plugins/jquery-confirm/jquery-confirm.min',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/stat/stat'
        );

        
        $this->load->view("main/header",$head);

        
        $this->load->view("statistics/stat/filtre");
        $this->load->view("statistics/stat/table"); 
        /* $this->load->view("statistics/stat/filtre"); */

        $data                       = array();
        
        $this->load->view("main/footer",$foot);
    }

    public function rechecher()
    {
        $data                = array();
        
        $this->getWhere();

        $this->load->model('statistics/Model_Stat_GED', 'MData');
       
        $data["courrier_mada"]      = $this->MData->dataCourrier($this->societe_,$this->whereCourrier,$this->whereLocation,'ged');
        $data["courrier_ce"]        = $this->MData->dataCourrier($this->societe_,$this->whereCourrier,$this->whereLocationCE,'ged_ce');
        $data["flux_mail"]          = $this->MData->dataFlux($this->societe_,$this->whereMail,$this->whereLocation,1);
        $data["flux_sftp"]          = $this->MData->dataFlux($this->societe_,$this->whereSftp,$this->whereLocation,2);
        
        
        $response                   = $this->load->view('statistics/stat/table', $data ,TRUE);

        $this->destroySession();

        $this->output->set_output($response);
    }

    public function setSession()
    {
        $this->session->set_userdata('dateDebut', $this->input->post("dateDebut"));
        $this->session->set_userdata('datefin', $this->input->post("dateFin"));
        $this->session->set_userdata('societe', implode(",",$this->input->post("societe")));
        /*$this->session->set_userdata('locationGed', $this->input->post("locationGed"));
        $this->session->set_userdata('typeFlux', $this->input->post("typeFlux"));*/
    }
    
    public function destroySession()
    {
        $this->session->unset_userdata('dateDebut');
        $this->session->unset_userdata('dateFin');
        $this->session->unset_userdata('societe');
        $this->session->unset_userdata('locationGed');
        $this->session->unset_userdata('typeFlux');
    }

    public function exportID()
    {
        $this->load->model('statistics/Model_Stat_GED', 'MData');

        $fileName               = 'depot/id.xls'; 
        $fileType               = 'Excel5'; 
        $objReader              = PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel            = $objReader->load($fileName);


        $this->activeSheet_0    = $objPHPExcel->setActiveSheetIndex(0);
        $this->activeSheet_1    = $objPHPExcel->setActiveSheetIndex(1);
        $this->activeSheet_2    = $objPHPExcel->setActiveSheetIndex(2);
        $this->activeSheet_3    = $objPHPExcel->setActiveSheetIndex(3);
        

        $this->getWhere();
        
        $courrier               = $this->MData->courrierID($this->societe_,$this->whereCourrier,$this->whereLocation,'ged');
        $courrierCE             = $this->MData->courrierID($this->societe_,$this->whereCourrier,$this->whereLocationCE,'ged_ce');
        $mail                   = $this->MData->fluxID($this->societe_,$this->whereMail,$this->whereLocation,1);
        $sftp                   = $this->MData->fluxID($this->societe_,$this->whereSftp,$this->whereLocation,2);
        
        $this->setExcel($this->activeSheet_0,$courrier);
        $this->setExcel($this->activeSheet_1,$courrierCE);
        $this->setExcel($this->activeSheet_2,$mail);
        $this->setExcel($this->activeSheet_3,$sftp);
         
        $xlsName              = 'Stat_GED';          
        $objWriter            = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $cacheMethod          = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings        = array( 'memoryCacheSize' => '1208MB');

       PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
        header("Content-Disposition: attachment;filename=$xlsName.xlsx"); 
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
    
    public function setExcel($sheet,$objet)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        if($objet)
        {
            $i = 3;
            $k = 1;
            foreach ($objet as $item) 
            {
                $sheet->setCellValue($this->listeColExcel[$k].$i,intval($item->id_pli));
                $sheet->setCellValue($this->listeColExcel[$k + 1].$i,$item->comment);
                
                $i = $i + 1 ;
                if($i % 7000 == 0)
                {
                    $i = 3 ;
                    $k = $k +3 ;
                }
            }

            //var_dump($objet);
        }
    }
    public function getWhere()
    {
        
        $locationGed         = $this->session->userdata('locationGed');
        $societe             = $this->session->userdata('societe');
        $typeFlux            = $this->session->userdata('typeFlux');
        if(in_array(0,$locationGed))
        {
            $this->whereLocation =  $this->whereLocationCE     = " and 1 = 1 ";
        }
        
        if(in_array(0,$societe))
        {
            $societe =  array(1,2);
        }

        if(in_array(1,$locationGed))
        {
            $this->whereLocation = " and 1 = 1 ";
        }

        if(in_array(2,$locationGed))
        {
            $this->whereLocationCE  = " and 1 = 1 ";
        }


        if(in_array(0,$typeFlux))
        {
            $this->whereMail =  $this->whereCourrier   =  $this->whereSftp  = " and 1 = 1 ";
        }

        if(in_array(1,$typeFlux))
        {
            $this->whereCourrier =  " and 1 = 1 ";
        }

        if(in_array(2,$typeFlux))
        {
            $this->whereMail = " and 1 = 1 ";
        }

        if(in_array(3,$typeFlux))
        {
           $this->whereSftp  = " and 1 = 1 ";
        }

        $this->societe_                   = implode(",",$societe);
    }
   
    public function GetStatCourrier($lieu ="mada")
    {
        $aColonne = array();
        $aData = array();
        $aColonne = array("pli_total","mvt_total","stock_typage","typage_en_cours","typage_ok_typage","typage_ok_typage_mvt","stock_attente_saisie","stock_attente_saisie_mvt","en_cours_saisie","en_cours_saisie_mvt","saisie_ok","saisie_ok_mvt","stock_ctrl","stock_ctrl_mvt","en_cours_ctrl","en_cours_ctrl_mvt","clo_apres_ctrl","sans_apres_ctrl_mvt","clo_sans_ctrl","clo_sans_ctrl_mvt","ko_scan","ko_scan_mvt","ko_kd","ko_kd_mvt","ko_kinc","ko_kinc_mvt","ko_src","ko_src_mvt","ko_en_attt","ko_en_attt_mvt","ko_cir","ko_cir_mvt","clo_sans_tt","clo_sans_tt_mvt","edit","cir_edit_mvt","cir_envoyee","cir_envoyee_mvt","ok_cir","ok_cir_mvt","ko_en_cours_by","ko_en_cours_by_mvt","divise","divise_mvt");
        $this->load->model('statistics/Model_Stat_GED', 'MData');
        if($lieu == "mada")
        {
            $dataCourrier = $this->MData->GetStatCourrier("ged");
        }
        else
        {
            $dataCourrier = $this->MData->GetStatCourrier("ged_ce");
        }
        
        
        if($dataCourrier)
        {
            $obj = $dataCourrier[0];
            $idHtml = "";
            if($lieu == "ce")
            {
                $idHtml ="_ce";
            }
            foreach($aColonne as $itemCol)
            {
                $aData[$itemCol.$idHtml] = intval($obj->$itemCol);
            }
        }
        else
        {
            foreach($aColonne as $itemCol)
            {
                $aData[$itemCol] = 0;
            }
        }
        echo json_encode($aData);
    }

    public function GetSftpMail($source = "mail") /*1 mail */
    {
        $aColonne = array();
        $aData = array();
        $aColonne = array("flux_total","flux_stock_typage","flux_en_cours_typage","flux_type","flux_en_cours_nv1","flux_escalade","flux_en_cours_nv2","flux_traite","flux_trans_cons","flux_recep_cons","flux_en_cours_retraite","flux_mail_anom","flux_anom_pj","flux_hors_perim","flux_anom_fic","flux_rejete","flux_cloture","flux_cloture_sans_ttmt","flux_stock_typage_nb_abo","flux_en_cours_typage_nb_abo","flux_type_nb_abo","flux_en_cours_nv1_nb_abo","flux_escalade_nb_abo","flux_en_cours_nv2_nb_abo","flux_traite_nb_abo","flux_trans_cons_nb_abo","flux_recep_cons_nb_abo","flux_en_cours_retraite_nb_abo","flux_mail_anom_nb_abo","flux_anom_pj_nb_abo","flux_hors_perim_nb_abo","flux_anom_fic_nb_abo","flux_rejete_nb_abo","flux_cloture_nb_abo","flux_cloture_sans_ttmt_nb_abo","nb_abonnement_total","flux_anom_src","flux_ko_definitif","flux_ko_inconnu","flux_ko_attente","flux_anom_src_nb_abo","flux_ko_definitif_nb_abo","flux_ko_inconnu_nb_abo","flux_ko_attente_nb_abo");
        $this->load->model('statistics/Model_Stat_GED', 'MData');
//        $source = 1;
        $idHtml = "";
        if($source != "mail")
        {
            $idHtml = "_sftp";
            $source = 2;
        }
        else
        {
            $source = 1;
        }
        
        $dataCourrier = $this->MData->GetSftpMail($source);        
        
        if($dataCourrier)
        {
            $obj = $dataCourrier[0];
            
           
            foreach($aColonne as $itemCol)
            {
                $aData[$itemCol.$idHtml] = intval($obj->$itemCol);
            }
        }
        else
        {
            foreach($aColonne as $itemCol)
            {
                $aData[$itemCol] = 0;
            }
        }
        echo json_encode($aData);
    }
}