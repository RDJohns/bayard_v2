<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('portier');
		//$this->portier->must_observ();
		$this->load->library("Excel");
		$this->load->model('statistics/Model_statistics', 'mvisu');
    }

    public function index(){

        $this->stat_plis();
        $this->stat_traitement();
        $this->stat_mensuel();

    }

    //public function visualisation_stat(){
    public function stat_plis(){

        $this->load->view('statistics/header.php');
        $this->load->view('statistics/header_statistic.php');
        $this->load->view('statistics/stat_plis.php');
        $this->load->view('statistics/footer_statistic.php');
		$this->load->view('statistics/footer.php');
        

    }
	 public function stat_traitement(){

        $titre = "<i class='fa fa-tasks' style='color:#fff'></i> Traitement";
		$array_view_plis['titre'] = $titre; 
		$this->load->view('statistics/header.php');
       //$this->load->view('statistics/left_menu.php');
        $this->load->view('statistics/header_statistic.php',$array_view_plis);
        $this->load->view('statistics/stat_traitement.php');
		$this->load->view('statistics/footer.php');
		$this->load->view('statistics/footer_statistic.php');
        
       

    }
	public function stat_mensuel(){
		
		$titre = "<i class='fa fa-bar-chart' style='color:#fff'></i> Suivi mensuel";
		$array_view_plis['titre'] = $titre;
		$array_view_plis['display_none']= "Suivi mensuel";       
        $this->load->view('statistics/header.php');
        $this->load->view('statistics/header_statistic.php',$array_view_plis);
        $this->load->view('statistics/stat_mensuel.php');
        $this->load->view('statistics/footer_statistic.php');
		$this->load->view('statistics/footer.php');
        

    }
	
	public function suivi_solde(){

		
		$titre = "<i class='fa fa-battery-half' style='color:#fff'></i> Suivi des soldes";
		$array_view_plis['titre'] = $titre;   
		$array_view_plis['display_none']= "Suivi des soldes";     
        $this->load->view('statistics/header.php');
        $this->load->view('statistics/header_statistic.php',$array_view_plis);
		$this->load->view('statistics/suivi_solde.php');
        $this->load->view('statistics/footer_statistic.php');
		$this->load->view('statistics/footer.php');
       

	}
	
	//Visualisation des delais de saisie par 3 typologie

	public function visu_typo(){

		
		$titre = "<i class='fa fa-eye' style='color:#fff'></i> Visualisation par typologie";
		$array_view_plis['titre'] = $titre;   
		$array_view_plis['display_none']= "Visualisation par 3 typologie";     
        $this->load->view('statistics/header.php');
        $this->load->view('statistics/header_statistic.php',$array_view_plis);
		$this->load->view('statistics/view_visu_typo.php');
        $this->load->view('statistics/footer_statistic.php');
		$this->load->view('statistics/footer.php');
       

	}
	
	 public function get_pli()
    {
		ini_set('memory_limit', '-1');
        $date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');
		$flag 		=  $this->input->post('flag');
        $arr_view_doc = array();

        $liste_result = $this->mvisu->get_list_plis($date_debut, $date_fin);
		$array_view_plis['liste_pli_detail'] = $liste_result;

        if($liste_result){
            $this->load->view('statistics/list_stat_plis.php', $array_view_plis);
        }
        else{
            echo 0;
        }
    }
	
	public function get_pli_statut_att(){

		$list_courrier_att_bayard = $this->mvisu->get_pli_courrier_att_saisie(1);
		$list_sftp_att_bayard 	  = $this->mvisu->get_pli_sftp_att_saisie(1,2);
		$list_mail_att_bayard     = $this->mvisu->get_pli_sftp_att_saisie(1,1);
		$list_courrier_att_milan = $this->mvisu->get_pli_courrier_att_saisie(2);
		$list_sftp_att_milan = $this->mvisu->get_pli_sftp_att_saisie(2,2);
		$list_mail_att_milan = $this->mvisu->get_pli_sftp_att_saisie(2,1);
		
		$array_view_plis['list_courrier_att_bayard'] = $list_courrier_att_bayard;
		$array_view_plis['list_sftp_att_bayard'] 	 = $list_sftp_att_bayard;
		$array_view_plis['list_mail_att_bayard'] 	 = $list_mail_att_bayard;
		$array_view_plis['list_courrier_att_milan']  = $list_courrier_att_milan;
		$array_view_plis['list_sftp_att_milan']  	 = $list_sftp_att_milan;
		$array_view_plis['list_mail_att_milan']  	 = $list_mail_att_milan;
		$this->load->view('statistics/solde_pli.php', $array_view_plis);
	}
	public function get_pli_validation()
    {
		$nb = 0;
		if(isset($this->session->login) && !empty($this->session->login)) {
			$liste_result = $this->mvisu->get_pli_restant_a_valider();		
			$nb = $liste_result[0]["nb"];
			$array_view_plis['nb'] = $nb;
			
			 if($liste_result)  $this->load->view('statistics/pli_correction.php', $array_view_plis);
			 else echo "?";
		} else echo "?";
    }
	public function get_pli_correction()
    {
		$nb = 0;
		if(isset($this->session->login) && !empty($this->session->login)) {
			$liste_result = $this->mvisu->get_pli_restant_a_corriger();		
			$nb = $liste_result[0]["nb"];
			$array_view_plis['nb'] = $nb;
			
			if($liste_result) $this->load->view('statistics/pli_correction.php', $array_view_plis);
			else echo "?";
		} else echo "?";
       
    }
	public function get_societe()
    {
			$list = $this->mvisu->get_societe();
			$array_view_plis['list_societe'] = $list;
			if($list) $this->load->view('statistics/societe_select.php', $array_view_plis);		
       
    }

    public function get_source()
    {
        $list = $this->mvisu->get_source();
        $array_view_plis['list_source'] = $list;
        if($list) $this->load->view('statistics/source_select.php', $array_view_plis);

    }
	 public function get_categorie()
    {
        $list = $this->mvisu->get_categorie();
        $array_view_plis['list_categorie'] = $list;
        if($list) $this->load->view('statistics/categorie_select.php', $array_view_plis);

    }
	
	
	 public function get_traitement()
    {
		ini_set('memory_limit', '-1');
        $date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');
        $mensuel    = $this->input->post('mensuel');
        $id_societe = $this->input->post('select_soc');
		$flag 		=  $this->input->post('flag');
        $array_view_plis = array();
        $tab_dates  = $tab_daty = $tab_min_daty_courrier = array();
        $tab_week   = $tab_month = array();
		$liste_result_traitement_mens = $liste_date_recep_mens = array();
        
		
		$tab_min_daty_courrier	     = $this->mvisu->get_min_date_courrier($date_debut, $date_fin);
		if(!isset($tab_min_daty_courrier[0]["date_courrier"]))$tab_min_daty_courrier[0]["date_courrier"] = $date_debut;
		$tab_dates 	   			     = $this->mvisu->get_dates($tab_min_daty_courrier[0]["date_courrier"], $date_fin);
		$liste_result 	   			 = $this->mvisu->get_stat_traitement($date_debut, $date_fin, $id_societe);
        $liste_result_plis 			 = $this->mvisu->get_list_traitement($date_debut, $date_fin, $id_societe);
        $liste_result_traitement_ko  = $this->mvisu->get_traitement_ko($date_debut, $date_fin, $id_societe);
        $liste_solde_j               = $this->mvisu->get_stat_solde($date_debut, $date_fin, $id_societe,1);
        $liste_solde_mvt_j           = $this->mvisu->get_stat_solde_mvt($date_debut, $date_fin, $id_societe,2);
       
		
		$array_view_plis['liste_pli'] 		 = $liste_result;
		$array_view_plis['list']      		 = $liste_result_plis;
		//$array_view_plis['liste_mensuelle']  = $liste_result_traitement_mens;
		$array_view_plis['mensuel']          = $mensuel;
		
		$array_type_pli     = array();
		$tab_list_code      = array();
		$tab_list_comment   = array();
		$tab_count_pli_type = array();
		$tab_count_pli      = array();
		$j = 0;
		$nbtotalpli = 0;
		$type_pli = '';
		
		foreach($tab_dates as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			//echo $daty." ".$tab_week[0]["week"] ;echo "</br>";
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			
			$tab_month 	= $this->mvisu->get_month_of_date($daty);
			$tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		
        
				
		foreach($array_view_plis['list'] as $klist ){  
		
			if (!in_array($klist["code_type_pli"], $tab_list_code)) {
				$tab_list_code[$j]  	  =  $klist["code_type_pli"];
				$tab_list_comment[$j] 	  =  $klist["comment"];
				$j++;
			}
			
			
			
			if(isset($tab_count_pli[$klist["code_type_pli"]])) $tab_count_pli[$klist["code_type_pli"]]  += 1;
			else $tab_count_pli[$klist["code_type_pli"]] = 1;
			
			
			if(empty($tab_count_pli_type[$klist["code_type_pli"]]["val"]))
				$tab_count_pli_type[$klist["code_type_pli"]]["val"] = 0;
			
			if (!in_array($klist["code_type_pli"], $tab_list_code)) {
				$tab_count_pli_type[$klist["code_type_pli"]]["val"] = 0;
			}else $tab_count_pli_type[$klist["code_type_pli"]]["val"] += 1;
		
			//$nbtotalpli++;
		}
		$nbtotalpli = $array_view_plis['liste_pli'][0]['pli_total'];
				
		$tab_pourcent_typtpli = array();
		foreach($tab_count_pli_type as $k_pli_type => $v_pli_type){
			$kcode     = $k_pli_type;
			$nbplitype = $v_pli_type["val"];
			$tab_pourcent_typtpli[$kcode] = number_format($nbplitype *100 / $nbtotalpli,2);
			
		}
		
	
		
		//$tab_count_pli["total"] = $nbtotalpli;		
		$tab_count_pli["total"] = $nbtotalpli;		
	
		$tab_list_code["count"] = $j;
		$array_view_plis['code_type_pli'] = $tab_list_code;
		$array_view_plis['lib_type_pli']  = $tab_list_comment;
		$array_view_plis['nb_type_pli']   = $tab_count_pli;
		$array_view_plis['liste_date']    = $tab_daty;
		
		
		/****************************** ****Plis traite *********************************************************/
		
		$tab_pli_ko     = array();
		$tab_mvt     	= array();
		$tab_list 		= array();		
		$data_donut_plis_traite = $color_donut_plis_traite = '';
		
		foreach($liste_result_traitement_ko as $tab_list_traitement ){  
			$tab_pli_ko["ok_advantage"]     = $tab_list_traitement["ok_advantage"]  ;	
			$tab_pli_ko["ko_scan"]          = $tab_list_traitement["ko_scan"]  ;
			$tab_pli_ko["hors_perimetre"]   = $tab_list_traitement["hors_perimetre"]  ;
			$tab_pli_ko["ko_ks"]  			= $tab_list_traitement["ko_ks"]  ;
			$tab_pli_ko["ko_ke"]        	= $tab_list_traitement["ko_ke"]  ;
			$tab_pli_ko["ko_def"]           = $tab_list_traitement["ko_def"]  ;			
			$tab_pli_ko["ko_circulaire"]    = $tab_list_traitement["ko_circulaire"]  ;			
			$tab_pli_ko["cloture_ok"]    	= $tab_list_traitement["cloture_ok"]  ;			
			//$tab_pli_ko["cloture"]    		= $tab_list_traitement["ok"];	
			
			$tab_mvt["ok_advantage"]     	= $tab_list_traitement["mvt_ok_advantage"]  ;	
			$tab_mvt["ko_scan"]          	= $tab_list_traitement["mvt_ko_scan"]  ;
			$tab_mvt["hors_perimetre"]   	= $tab_list_traitement["mvt_hors_perimetre"]  ;
			$tab_mvt["ko_ks"]  				= $tab_list_traitement["mvt_ko_ks"]  ;
			$tab_mvt["ko_ke"]        		= $tab_list_traitement["mvt_ko_ke"]  ;
			$tab_mvt["ko_def"]           	= $tab_list_traitement["mvt_ko_def"]  ;			
			$tab_mvt["ko_circulaire"]    	= $tab_list_traitement["mvt_ko_circulaire"]  ;			
			$tab_mvt["cloture_ok"]    		= $tab_list_traitement["mvt_cloture_ok"]  ;			
			$tab_mvt["cloture"]    			= $tab_list_traitement["mvt_ok"];
			
			list($pli_total, $mvt_total)	= ($tab_list_traitement["pli_total_en_traitement"] != null ) ? explode(",",$tab_list_traitement["pli_total_en_traitement"]) : array(0,0) ;	
			
			$array_view_plis['total']     = $pli_total;
			$array_view_plis['mvt_total'] = $mvt_total;
			
			$tab_pli_ko["KO"]  	    	    = $tab_list_traitement["ko"]  ;
		
		}
		//--- les ko 
		
		$tab_list_pli   = $tab_list_mvt = array();		
		$tab_pli_ko_lib = array("ok_advantage"=>"OK Advantage","ko_scan"=>"KO SCAN","hors_perimetre"=>"Hors périmètre","ko_circulaire"=>"Circulaire","ko_ks"=>"Transfert SRC","ko_ke"=>"En attente de consigne","ko_def"=>"KO Définitif","ko"=>"PLIS KO");
		
		$data_donut_plis_ko = $color_donut_plis_ko = '';
		foreach($tab_pli_ko as $keypko => $nbpko){
						
			$tab_list_pli[$keypko]["code"]     = $keypko;
			$tab_list_pli[$keypko]["nb"]       = $nbpko;			
			$tab_list_pli[$keypko]["pourcent"] = ($tab_list_traitement["ok"]+$tab_list_traitement["ko"]> 0) ? number_format($nbpko*100/($tab_list_traitement["ok"]+$tab_list_traitement["ko"]),2) : 0;
			
		}
		foreach($tab_mvt as $keymvt => $nbmvt){
						
			$tab_list_mvt[$keymvt]["code"]     = $keymvt;
			$tab_list_mvt[$keymvt]["nb"]       = $nbmvt;			
			$tab_list_mvt[$keymvt]["pourcent"] = ($tab_list_traitement["mvt_ok"]> 0) ? number_format($nbmvt*100/($tab_list_traitement["mvt_ok"]),2) : 0;
			
		}
		if ($data_donut_plis_ko != ''){
			$data_donut_plis_ko .= "],";
			$data_donut_plis_ko .= $color_donut_plis_ko."]";
		}
		//echo "<pre>";print_r($tab_list_pli);echo "</pre>";
		$total_plis_traites = $tab_list_traitement["ok"]+$tab_list_traitement["ko"];
		$array_view_plis['data_plis_cloture']    	 = $tab_list;
		$array_view_plis['tab_pli']    	 	 		 = $tab_list_pli;
		$array_view_plis['tab_pli_mvt']    	 	     = $tab_list_mvt;
		$array_view_plis['total_plis_traites']    	 = $total_plis_traites;
		
		$array_view_plis['liste_date']    = $tab_daty;
		$array_view_plis['liste_mois']    = $tab_mois;
		
		/****************************** Début solde des plis ***************************************************/
		$list_solde_pli = array();
		foreach($liste_solde_j as $ksolde => $SoldeJ){
			
			$date_j 		 				 = $SoldeJ["date_j"];
			$list_solde_pli[$date_j]["ok"] 			 = $SoldeJ["ok"];
			$list_solde_pli[$date_j]["ko_scan"] 		 = $SoldeJ["ko_scan"];
			$list_solde_pli[$date_j]["hors_perimetre"]= $SoldeJ["hors_perimetre"];
			$list_solde_pli[$date_j]["ks"] 			 = $SoldeJ["ks"];
			$list_solde_pli[$date_j]["ke"] 			 = $SoldeJ["ke"];
			$list_solde_pli[$date_j]["kd"] 			 = $SoldeJ["kd"];
			$list_solde_pli[$date_j]["ci"] 			 = $SoldeJ["ci"];
			//$rubrique 						 = $SoldeJ["rubrique"];
			$societe 						 = $SoldeJ["societe"];
			
			
		}

		$array_view_plis['list_solde_pli']    = $list_solde_pli;	
		
		/****************************** Début solde des mouvements ************************************************/
		$list_solde_mvt = array();
		$list_societe   = array();
		foreach($liste_solde_mvt_j as $ksolde => $SoldeJ){
			
			$date_j 		 				 = $SoldeJ["date_j"];
			$list_solde_mvt[$date_j]["ok"] 			 = $SoldeJ["ok"];
			$list_solde_mvt[$date_j]["ko_scan"] 		 = $SoldeJ["ko_scan"];
			$list_solde_mvt[$date_j]["hors_perimetre"]= $SoldeJ["hors_perimetre"];
			$list_solde_mvt[$date_j]["ks"] 			 = $SoldeJ["ks"];
			$list_solde_mvt[$date_j]["ke"] 			 = $SoldeJ["ke"];
			$list_solde_mvt[$date_j]["kd"] 			 = $SoldeJ["kd"];
			$list_solde_mvt[$date_j]["ci"] 			 = $SoldeJ["ci"];
			//$rubrique 						 = $SoldeJ["rubrique"];
			$societe 						 = $SoldeJ["societe"];			
			
		}
		
		/****************************** Début saisie  ************************************************/
		
		$liste_solde_jr     = $this->mvisu->get_saisie_par_date_traitement($date_debut, $date_fin, $id_societe);
		$liste_solde_mvt_jr = $this->mvisu->get_saisie_mvt_par_date_traitement($date_debut, $date_fin, $id_societe);
	
		
		$array_view_plis['liste_solde_jr']      = $liste_solde_jr;	
		$array_view_plis['liste_solde_mvt_jr']  = $liste_solde_mvt_jr;	
		$array_view_plis['date_fin'] 			= $date_fin;


		$array_view_plis['list_solde_mvt']    = $list_solde_mvt;	
       // if($array_view_plis['total'] > 0){
            $this->load->view('statistics/form_traitement_plis.php', $array_view_plis);
       // }
        
    }
	
	
	public function get_traitement_date(){
		
		$date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');
        $id_societe = $this->input->post('select_soc');
		$list  = $this->mvisu->get_traitement_bydate($date_debut, $date_fin, $id_societe);
		$array['list']    = $list;
		if(count($array) > 0){
            $this->load->view('statistics/detail_traitement.php', $array);
        }
	}

	public function get_traitement_typo(){
		
		$date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');
        $id_societe = $this->input->post('select_soc');
		$list  = $this->mvisu->get_traitement_typologie($date_debut, $date_fin, $id_societe);
		$array['list']    = $list;
		if(count($array) > 0){
            $this->load->view('statistics/detail_typo.php', $array);
        }
	}

	public function get_stat_pli()
    {
		ini_set('memory_limit', '-1');
        $num_cmd    = $this->input->post('numcommande');
        $num_client = $this->input->post('numclient');
        $nom_client = $this->input->post('nom_client');
        $date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');

        $arr_view_doc = array();
		$tab_dates    = $tab_daty = $tab_dates_traitt = array();
        $tab_week     = array();
        $tab_month    = $tab_mois = $tab_dates_ttt = array();
		$tab_dates_traitement = array();
        
		
		$tab_dates 	   			     = $this->mvisu->get_dates($date_debut, $date_fin);	
		$tab_dates_traitement        = $this->mvisu->get_dates_ttt($date_debut, $date_fin);
        $liste_result 	   			 = $this->mvisu->get_stat_plis( $date_debut, $date_fin);
        $liste_result_plis 			 = $this->mvisu->get_list_plis($date_debut, $date_fin);
        $liste_result_doc  			 = $this->mvisu->get_list_document($date_debut, $date_fin);
        $liste_result_plis_traite    = $this->mvisu->get_list_plis_traite($date_debut, $date_fin);
        $liste_result_plis_recu_par_date          = $this->mvisu->get_plis_recu_par_date($date_debut, $date_fin);
        $liste_result_plis_new       = $this->mvisu->get_list_newplis($date_debut, $date_fin);

		$array_view_doc['liste_pli'] = $liste_result;
		$array_view_doc['list']      = $liste_result_plis;
		$array_view_doc['liste_doc'] = $liste_result_doc;
		$array_view_doc['liste_plis_recu_par_date'] = $liste_result_plis_recu_par_date;
		$array_view_doc['liste_result_plis_new']    = $liste_result_plis_new;

		
		$dttt_min = $tab_dates_traitement[0]["date_min_ttt"] ;
		$dttt_max = $tab_dates_traitement[0]["date_max_ttt"] ;
		if(!isset($dttt_min)) $dttt_min = $date_debut;
		if(!isset($dttt_max)) $dttt_max = $date_fin;
		$tab_dates_traitt			 = $this->mvisu->get_dates($dttt_min, $dttt_max);	
		$array_type_pli     = array();
		$tab_list_code      = array();
		$tab_list_comment   = array();
		$tab_count_pli_type = array();
		$tab_count_pli      = array();
		$j = 0;
		$nbtotalpli = 0;
		$type_pli = '';
		

		foreach($array_view_doc['list'] as $klist ){  
		
			if (!in_array($klist["code_type_pli"], $tab_list_code)) {
				$tab_list_code[$j]  	  =  $klist["code_type_pli"];
				$tab_list_comment[$j] 	  =  $klist["comment"];
				$j++;
			}
			
			if(isset($tab_count_pli[$klist["code_type_pli"]])) $tab_count_pli[$klist["code_type_pli"]]  += 1;
			else $tab_count_pli[$klist["code_type_pli"]] = 1;
			
			
			if(empty($tab_count_pli_type[$klist["code_type_pli"]]["val"]))
				$tab_count_pli_type[$klist["code_type_pli"]]["val"] = 0;
			
			if (!in_array($klist["code_type_pli"], $tab_list_code)) {
				$tab_count_pli_type[$klist["code_type_pli"]]["val"] = 0;
			}else $tab_count_pli_type[$klist["code_type_pli"]]["val"] += 1;
		
			$nbtotalpli++;
		}
		
				
		$tab_pourcent_typtpli = array();
		$data_donut  = '';
		$color_donut = '';
		$tab_donut   = array();
		$tab_donut_doc   = array();
		$data_donut_doc  = '';	
		$color_donut_doc = '';				
		$tab_color_donut = array('rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)','rgb(174, 201, 91)', 'rgb(213, 140, 223)', 'rgb(140, 194, 230)', 'rgb(221, 101, 102)', 'rgb(242, 222, 222)','rgb(154, 169, 178)', 'rgb(255, 137, 42)', 'rgb(223, 240, 216)', 'rgb(252, 248, 227)', 'rgb(104, 188, 49)','rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)','rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)','rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)','rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)','rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)','rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)','rgb(227, 127, 100)', 'rgb(254, 224, 116)', 'rgb(175, 78, 150)', 'rgb(32, 145, 207)', 'rgb(104, 188, 49)');
		foreach($tab_count_pli_type as $k_pli_type => $v_pli_type){
			$kcode     = $k_pli_type;
			$nbplitype = $v_pli_type["val"];
			$tab_pourcent_typtpli[$kcode] = number_format($nbplitype *100 / $nbtotalpli,2);
			//echo $kcode." ".($nbplitype *100)." / ".$nbtotalpli."</br>";
		}
		
		$j2 = 0;
		foreach($tab_count_pli as $codepli =>$nbpli){
			
			if ($data_donut == ''){
				$data_donut = "[{label:'".$codepli."',value:".$tab_pourcent_typtpli[$codepli]."}";
				$color_donut = "colors:['".$tab_color_donut[$j2]."'";
				
			}else {
				$data_donut .= ",{label:'".$codepli."',value:".$tab_pourcent_typtpli[$codepli]."}";
				$color_donut .= ",'".$tab_color_donut[$j2]."'";
			}
			$j2++;
		}
		
		if ($data_donut != ''){
			$data_donut .= "],";
			$data_donut .= $color_donut."]";
		}
		
		$tab_count_pli["total"] = $nbtotalpli;
		
	
		$tab_list_code["count"] = $j;
		$array_view_doc['code_type_pli'] = $tab_list_code;
		$array_view_doc['lib_type_pli']  = $tab_list_comment;
		$array_view_doc['nb_type_pli']   = $tab_count_pli;
		$array_view_doc['data_donut']    = $data_donut;
		
		
		/***************************** Document ****************************************************************/
		
		$tab_list_doc = $tab_list_docpli = $tab_count_docpli = array();
		$k = $nb_docpli = 0;
		$tab_list_document = array();
		
		
		$plis_avec_moyen_paiement = 0;		
		$tab_list_doc = array("CHQ","CB","Autre MPT","Non traités");
		foreach($array_view_doc['liste_doc'] as $kdoc ){  
			$tab_count_docpli["CHQ"]  	    = $kdoc["avec_paiement_chq"]  ;
			$tab_count_docpli["CB"]   	    = $kdoc["avec_paiement_cb"]  ;
			$tab_count_docpli["Autre MPT"]  = $kdoc["avec_paiement_autre"]  ;
			$tab_count_docpli["Non typé"]   = $kdoc["non_traite"] + count($liste_result_plis_new);
			$plis_non_traite 				= $kdoc["non_traite"] + count($liste_result_plis_new);
			$plis_avec_moyen_paiement	    = 	$kdoc["avec_paiement_chq"] + $kdoc["avec_paiement_cb"] + $kdoc["avec_paiement_autre"] ;	
			//echo $nbtotalpli; exit;
			$autres_plis = ($nbtotalpli-$plis_avec_moyen_paiement-$plis_non_traite > 0) ? $nbtotalpli-$plis_avec_moyen_paiement-$plis_non_traite : 0;
			if ($autres_plis > 0) $tab_count_docpli["Autres plis"] = $autres_plis  ;
		}
		$j3 = 0;
		foreach($tab_count_docpli as $keydoc => $nbdoc){
			
			
			$tab_list_document[$keydoc]["code"]     = $keydoc;
			$tab_list_document[$keydoc]["nb"]       = $nbdoc;
			$tab_list_document[$keydoc]["pourcent"] = ($nbtotalpli > 0) ? number_format($nbdoc*100/$nbtotalpli,2) : 0;
			
			if ($data_donut_doc == ''){
				$data_donut_doc = "[{label:'".$keydoc."',value:".$tab_list_document[$keydoc]["pourcent"]."}";
				$color_donut_doc = "colors:['".$tab_color_donut[$j3]."'";
				
			}else {
				$data_donut_doc .= ",{label:'".$keydoc."',value:".$tab_list_document[$keydoc]["pourcent"]."}";
				$color_donut_doc .= ",'".$tab_color_donut[$j3]."'";
			}
			$j3++;
		}
		
		$nplis_sans_moyen_paiement = 0;

		if ($data_donut_doc != ''){
			$data_donut_doc .= "],";
			$data_donut_doc .= $color_donut_doc."]";
		}
	
		$array_view_doc['data_document']    	  = $tab_list_document;
		$array_view_doc['data_donut_document']    = $data_donut_doc;
	
		/*echo "<pre>";
		echo $nb_docpli;
		print_r($tab_list_document);
		echo "</pre>";
		*/
		
		/****************************** Fin Document ************************************************************/
		/****************************** ****Plis traite *********************************************************/
		
		$tab_pli_traite = array();
		$tab_pli_ko     = array();
		$tab_list 		= array();		
		$data_donut_plis_traite = $color_donut_plis_traite = '';
		
		foreach($liste_result_plis_traite as $tab_list_plis_traite ){  
			$tab_pli_traite["BDC_CHQ"]  	= $tab_list_plis_traite["bdc_avec_chq"]  ;
			$tab_pli_traite["BDC_CB"]   	= $tab_list_plis_traite["bdc_avec_cb"]  ;
			$tab_pli_traite["BDC_ESP"]  	= $tab_list_plis_traite["bdc_avec_espece"]  ;
			$tab_pli_traite["MANDAT"]   	= $tab_list_plis_traite["bdc_avec_mandat_facture"]  ;
			$tab_pli_traite["BDP"]          = $tab_list_plis_traite["bdp"]  ;			
			$tab_pli_traite["BDP_CHQ"]      = $tab_list_plis_traite["bdp_chq"]  ;			
			$tab_pli_traite["Autre"]        = $tab_list_plis_traite["autre_plis"]  ;			
			$tab_pli_traite["CLOTURE"]  	= $tab_list_plis_traite["ok"]  ;
			
			$tab_pli_ko["ko_inconnu"]   	= $tab_list_plis_traite["ko_inconnu"]  ;
			$tab_pli_ko["ko_scan"]      	= $tab_list_plis_traite["ko_scan"]  ;
			$tab_pli_ko["ko_reclammation"]  = $tab_list_plis_traite["ko_reclammation"]  ;
			$tab_pli_ko["ko_litige"]      	= $tab_list_plis_traite["ko_litige"]  ;
			$tab_pli_ko["ko_call"]      	= $tab_list_plis_traite["ko_call"]  ;
			$tab_pli_ko["ko_def"]       	= $tab_list_plis_traite["ko_def"]  ;			
			$tab_pli_ko["ko_circulaire"]    = $tab_list_plis_traite["ko_circulaire"]  ;			
			$tab_pli_ko["ko_en_attente"]    = $tab_list_plis_traite["ko_en_attente"]  ;			
			
			
			$tab_pli_ko["KO"]  	    	    = $tab_list_plis_traite["ko"]  ;
			//$tab_pli_traite["TRAITE"]   	= $tab_list_plis_traite["traite"]  ;
		}
		
		$j3 = 0;
		$tab_pli_traite_lib = array("BDC_CHQ"=>"Payé par CHQ","BDC_CB"=>"Payé par CB","BDC_ESP"=>"Payé par ESPECES","MANDAT"=>"Payé par MANDAT FACTURE","BDP"=>"Bon de participation","BDP_CHQ"=>"Bon de participation avec CHQ","Autre"=>"Autre type de plis","CLOTURE"=>"PLIS CLOTURES");
		$pourcent_cloture_bdc = 0;	
		$cloture_bdc = 0;	
		foreach($tab_pli_traite as $keypt => $nbpt){
			
			$tab_list[$keypt]["code"]     = $keypt;
			$tab_list[$keypt]["nb"]       = $nbpt;			
			$tab_list[$keypt]["pourcent"] = ($tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"] > 0) ? number_format($nbpt*100/($tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"]),2) : 0;
			$pourcent_cloture = 0;
			if ($keypt != 'CLOTURE')
			{   // 23.80% => 15 plis
				if ($data_donut_plis_traite == ''){
					$data_donut_plis_traite = "[{label:'".$tab_pli_traite_lib[$keypt]."',value:".$tab_list[$keypt]["pourcent"]."}";
					$color_donut_plis_traite = "colors:['".$tab_color_donut[$j3]."'";
					
				}else {
					$data_donut_plis_traite .= ",{label:'".$tab_pli_traite_lib[$keypt]."',value:".$tab_list[$keypt]["pourcent"]."}";
					$color_donut_plis_traite .= ",'".$tab_color_donut[$j3]."'";
				}
				$j3++;
				$pourcent_cloture_bdc += $tab_list[$keypt]["pourcent"];
			}else 
			{   $pourcent_cloture = $tab_list[$keypt]["pourcent"];
			    $cloture_bdc = $nbpt;
			}
			
		}
				
		
		if ($data_donut_plis_traite != ''){
			$data_donut_plis_traite .= "],";
			$data_donut_plis_traite .= $color_donut_plis_traite."]";
		}
		
		//--- les ko 
		$j4 			= 0;
		$tab_list_ko    = array();		
		$tab_pli_ko_lib = array("ko_scan"=>"KO SCAN","ko_inconnu"=>"KO INCONNU","ko_call"=>"KO CALL","ko_def"=>"KO DEFINITIF","ko_circulaire"=>"KO Circulaire","ko_en_attente"=>"KO En attente","ko_reclammation"=>"KO Réclammation","ko_litige"=>"KO Litige","ko"=>"PLIS KO");
		$data_donut_plis_ko = $color_donut_plis_ko = '';
		foreach($tab_pli_ko as $keypko => $nbpko){
						
			$tab_list_ko[$keypko]["code"]     = $keypko;
			$tab_list_ko[$keypko]["nb"]       = $nbpko;			
			$tab_list_ko[$keypko]["pourcent"] = ($tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"]> 0) ? number_format($nbpko*100/($tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"]),2) : 0;
			if ($keypko != 'KO')
			{
				if ($data_donut_plis_ko == ''){
					$data_donut_plis_ko = "[{label:'".$tab_pli_ko_lib[$keypko]."',value:".$tab_list_ko[$keypko]["pourcent"]."}";
					$color_donut_plis_ko = "colors:['".$tab_color_donut[$j4]."'";
					
				}else {
					$data_donut_plis_ko .= ",{label:'".$tab_pli_ko_lib[$keypko]."',value:".$tab_list_ko[$keypko]["pourcent"]."}";
					$color_donut_plis_ko .= ",'".$tab_color_donut[$j4]."'";
				}
				$j4++;
			}
		}
		if ($data_donut_plis_ko != ''){
			$data_donut_plis_ko .= "],";
			$data_donut_plis_ko .= $color_donut_plis_ko."]";
		}
		
		$total_plis_traites = $tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"];
		$array_view_doc['data_plis_cloture']    	 = $tab_list;
		$array_view_doc['data_plis_ko']    	 		 = $tab_list_ko;
		$array_view_doc['data_donut_plis_traite']    = $data_donut_plis_traite;		
		$array_view_doc['data_donut_plis_ko']    	 = $data_donut_plis_ko;
		$array_view_doc['total_plis_traites']    	 = $total_plis_traites;
		/****************************** Fin Plis traite ************************************************************/
		/****************************** Plis restant à traiter ************************************************************/
		
	
		$tab_list_restant    = array();	
			
		$tab_pli_restant_lib = array("en_cours"=>"EN COURS","nouveau"=>"NON TRAITE","RESTANT"=>"PLIS RESTANT");
		$data_donut_plis_restant = $color_donut_plis_restant = '';
		foreach($liste_result as $ktab_pli){
			$pli_total   =  $ktab_pli['nouveau']+$ktab_pli['encours']+$ktab_pli['traite']+$ktab_pli['anomalie'];
			$pli_restant =  $ktab_pli['nouveau']+$ktab_pli['encours'];
		}
			
			
		$pourcent_nouveau  =($pli_total > 0) ? number_format($ktab_pli['nouveau']*100/($pli_total),2) : 0;
		$pourcent_encours  =($pli_total > 0) ? number_format($ktab_pli['encours']*100/($pli_total),2) : 0;
			
		$keyprest = "nouveau";
		$tab_list_restant[$keyprest]["code"]     = "nouveau";
		$tab_list_restant[$keyprest]["nb"]       = $ktab_pli['nouveau'];			
		$tab_list_restant[$keyprest]["pourcent"] = $pourcent_nouveau;
		
		$keyprest = "en_cours";
		$tab_list_restant[$keyprest]["code"]     = "en_cours";
		$tab_list_restant[$keyprest]["nb"]       = $ktab_pli['encours'];			
		$tab_list_restant[$keyprest]["pourcent"] = $pourcent_encours;
			
		if ($data_donut_plis_restant == ''){
			$data_donut_plis_restant = "[{label:'Non traité',value:".$pourcent_nouveau."},{label:'En cours',value:".$pourcent_encours."}],";
			$color_donut_plis_restant = "colors:['".$tab_color_donut[9]."','".$tab_color_donut[10]."']";
			
		}			
		
		$data_donut_plis_restant .= $color_donut_plis_restant;
		
		
		$total_plis_restant = $tab_list_plis_traite["ok"]+$tab_list_plis_traite["ko"];
		$array_view_doc['data_plis_restant']    	  = $tab_list_restant;
		$array_view_doc['data_donut_plis_restant']    = $data_donut_plis_restant;
		$array_view_doc['total_plis_restant']    	  = $pli_restant;
		$array_view_doc['total_plis']    	  		  = $pli_total;
				
		$tab_dates_merges = array_merge ($tab_dates,$tab_dates_traitt);
				
		//foreach($tab_dates as $kdaty=>$vdaty ){  
		foreach($tab_dates_merges as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			
			$tab_month 	= $this->mvisu->get_month_of_date($daty);
			$tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		$array_view_doc['liste_date']    = $tab_daty;
		$array_view_doc['liste_mois']    = $tab_mois;
		
		/****************************** Fin Restant ************************************************************/
        if($pli_total > 0){
            $this->load->view('statistics/form_stat_plis.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
    }

	public function get_detail_pli()
    {
		ini_set('memory_limit', '-1');
		$array_pli = array();
        $date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');

		$liste_result_plis 	= $this->mvisu->get_list_plis($date_debut, $date_fin);

		$array_pli['list']  = $liste_result_plis;
		if($liste_result_plis){
            $this->load->view('statistics/detail_pli.php', $array_pli);
            
        }
        else{
            echo 0;
        }
    }
	
	public function get_pli_cloture()
    {

        $date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');

        $arr_view_doc = array();

        $liste_result_plis_cloture_par_date    = $this->mvisu->get_plis_cloture_par_date($date_debut, $date_fin);
       
		$array_view_doc['liste_plis_cloture_par_date'] = $liste_result_plis_cloture_par_date;		
				
        if($liste_result_plis_cloture_par_date){
            $this->load->view('statistics/stat_plis_cloture.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
    }

	public function get_pli_cloture_hebdo()
    {

        $date_debut   = $this->input->post('date_debut');
        $date_fin     = $this->input->post('date_fin');
		$tab_week     = array();
        $tab_month    = $tab_mois = array();
		
        $arr_view_doc = $tab_dates   = $tab_week = $tab_daty = array();
		$tab_dates_traitement = array();        
		
		$tab_dates 	   			     = $this->mvisu->get_dates($date_debut, $date_fin);	
		$tab_dates_traitement        = $this->mvisu->get_dates_ttt($date_debut, $date_fin);
        $liste_result_plis_cloture_par_date    = $this->mvisu->get_plis_cloture_par_date($date_debut, $date_fin);
        $dttt_min 				     = $tab_dates_traitement[0]["date_min_ttt"] ;
		$dttt_max 					 = $tab_dates_traitement[0]["date_max_ttt"] ;
		if(!isset($dttt_min)) $dttt_min = $date_debut;
		if(!isset($dttt_max)) $dttt_max = $date_fin;
		$tab_dates_traitt			 = $this->mvisu->get_dates($dttt_min, $dttt_max);
		
		$tab_dates_merges = array_merge ($tab_dates,$tab_dates_traitt);
		foreach($tab_dates_merges as $kdaty=>$vdaty ){
		//foreach($tab_dates as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			$tab_month 				 = $this->mvisu->get_month_of_date($daty);
			$tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		
		$array_view_doc['liste_plis_cloture_par_date'] = $liste_result_plis_cloture_par_date;
		$array_view_doc['liste_date']    = $tab_daty;
		$array_view_doc['liste_mois']    = $tab_mois;	
		
        if($liste_result_plis_cloture_par_date){
            $this->load->view('statistics/stat_plis_cloture_hebdo.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
    }

	public function get_pli_cloture_mensuel()
    {

        $date_debut   = $this->input->post('date_debut');
        $date_fin     = $this->input->post('date_fin');
		$tab_week     = array();
        $tab_month    = $tab_mois = array();

        $arr_view_doc = $tab_dates  = $tab_week = $tab_daty = array();
		$tab_dates 	   			     			= $this->mvisu->get_dates($date_debut, $date_fin);
        $liste_result_plis_cloture_par_date     = $this->mvisu->get_plis_cloture_par_date($date_debut, $date_fin);
       
				
		foreach($tab_dates as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			$tab_month 				 = $this->mvisu->get_month_of_date($daty);
			$tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		
		$array_view_doc['liste_plis_cloture_par_date'] = $liste_result_plis_cloture_par_date;
		$array_view_doc['liste_date']    = $tab_daty;
		$array_view_doc['liste_mois']    = $tab_mois;
		
        if($liste_result_plis_cloture_par_date){
            $this->load->view('statistics/stat_plis_cloture_mensuel.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
    }

	public function get_pli_ko_journalier(){
		$date_debut   = $this->input->post('date_debut');
        $date_fin     = $this->input->post('date_fin');
		$tab_week     = array();
        $tab_month    = $tab_mois = array();
		
        $arr_view_doc = $tab_dates   = $tab_week = $tab_daty = array();
		$tab_dates 	   			     = $this->mvisu->get_dates($date_debut, $date_fin);
		$liste_result_plis_ko        = $this->mvisu->get_pli_ko($date_debut, $date_fin);
		
		foreach($tab_dates as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			$tab_month 				 = $this->mvisu->get_month_of_date($daty);
			$tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		
		$array_view_doc['liste_result_plis_ko'] = $liste_result_plis_ko;
		$array_view_doc['liste_date']    = $tab_daty;
		$array_view_doc['liste_mois']    = $tab_mois;	
		
        if($liste_result_plis_ko){
            $this->load->view('statistics/stat_plis_ko_journalier.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
	}
	
	public function get_pli_ko_hebdo(){
		$date_debut   = $this->input->post('date_debut');
        $date_fin     = $this->input->post('date_fin');
		$tab_week     = array();
        $tab_month    = $tab_mois = array();
		
        $arr_view_doc = $tab_dates   = $tab_week = $tab_daty = $tab_dates_traitement = array();
		
		$tab_dates 	   			 = $this->mvisu->get_dates($date_debut, $date_fin);	
		$tab_dates_traitement    = $this->mvisu->get_dates_ttt($date_debut, $date_fin);
		$liste_result_plis_ko    = $this->mvisu->get_pli_ko($date_debut, $date_fin);
		
		$dttt_min = $tab_dates_traitement[0]["date_min_ttt"] ;
		$dttt_max = $tab_dates_traitement[0]["date_max_ttt"] ;
		if(!isset($dttt_min)) $dttt_min = $date_debut;
		if(!isset($dttt_max)) $dttt_max = $date_fin;
		$tab_dates_traitt		= $this->mvisu->get_dates($dttt_min, $dttt_max);

		$tab_dates_merges = array_merge ($tab_dates,$tab_dates_traitt);
		foreach($tab_dates_merges as $kdaty=>$vdaty ){ 
		//foreach($tab_dates as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			$tab_month 				 = $this->mvisu->get_month_of_date($daty);
			$tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		
		$array_view_doc['liste_result_plis_ko'] = $liste_result_plis_ko;
		$array_view_doc['liste_date']    = $tab_daty;
		$array_view_doc['liste_mois']    = $tab_mois;	
		
        if($liste_result_plis_ko){
            $this->load->view('statistics/stat_plis_ko_hebdo.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
	}
	
	public function get_pli_ko_mensuel(){
		$date_debut   = $this->input->post('date_debut');
        $date_fin     = $this->input->post('date_fin');
		$tab_week     = array();
        $tab_month    = $tab_mois = array();
		
        $arr_view_doc = $tab_dates   = $tab_week = $tab_daty = array();
		$tab_dates 	   			     = $this->mvisu->get_dates($date_debut, $date_fin);
		$liste_result_plis_ko    = $this->mvisu->get_pli_ko($date_debut, $date_fin);
		
		foreach($tab_dates as $kdaty=>$vdaty ){  
			$daty 		= $vdaty['daty'];
			$tab_week 	= $this->mvisu->get_week_of_date($daty);
			$tab_daty["daty"][$daty] = $daty;
			$tab_daty["num"][$daty]  = "S".$tab_week[0]["week"];
			$tab_month 				 = $this->mvisu->get_month_of_date($daty);
			$tab_mois["mois"][$daty] = $tab_month[0]["mois"];
		}
		
		$array_view_doc['liste_result_plis_ko'] = $liste_result_plis_ko;
		$array_view_doc['liste_date']    = $tab_daty;
		$array_view_doc['liste_mois']    = $tab_mois;	
		
        if($liste_result_plis_ko){
            $this->load->view('statistics/stat_plis_ko_mensuel.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
	}
	public function get_pli_kocall_motif()
    {

        $date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');

        $arr_view_doc = array();

        $liste_result_plis_ko_call    = $this->mvisu->get_pli_kocall_motif($date_debut, $date_fin);
       
		$array_view_doc['liste_plis_ko_call_motif'] = $liste_result_plis_ko_call;
				
        if($liste_result_plis_ko_call){
            $this->load->view('statistics/stat_plis_kocall.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
    }
	public function get_bdc_volume(){
		$date_debut = $this->input->post('date_debut');
        $date_fin   = $this->input->post('date_fin');

        $array_view_doc = array();

        $liste_bdc_volume    = $this->mvisu->get_bdc_volume($date_debut, $date_fin);
       
		$array_view_doc['liste_bdc_volume'] = $liste_bdc_volume;
		
				
        if($liste_bdc_volume){
            $this->load->view('statistics/stat_bdc_volume.php', $array_view_doc);
            
        }
        else{
            echo 0;
        }
	}
	
	
	public function get_stat_pli_mensuel()
    {

       $date_debut = $this->input->post('date_debut');
       $date_fin   = $this->input->post('date_fin');
       $id_societe = $this->input->post('id_societe');
       $id_source  = $this->input->post('id_source');
	   $pli_total  = 0;
	   if (strlen($date_debut)<10) $date_debut = '01/'.$date_debut;
	   if (strlen($date_fin)<10)   $date_fin = '01/'.$date_fin;

        $arr_view_doc = $liste_result_mensuel = $list_ko_mensuel = array();
		$tab_dates    = $tab_daty = $tab_week = array();
        $tab_month    = $tab_mois = $tab_typo = array();
		$lib_typo = "";
		       
		
		$tab_dates 	   			 	 = $this->mvisu->get_dates_months($date_debut, $date_fin);
		if($id_source == '3') {
            $liste_result_mensuel    = $this->mvisu->get_stat_mensuel($date_debut, $date_fin, $id_societe);
            $list_ko_mensuel 		 = $this->mvisu->get_ko_mensuel($date_debut, $date_fin, $id_societe);
        }
        else{
            $liste_result_mensuel    = $this->mvisu->get_stat_mensuel_flux($date_debut, $date_fin, $id_societe,$id_source);
		}
        $liste_result_plis_typo	     = $this->mvisu->get_stat_mensuel_typologlie( $date_debut, $date_fin,$id_societe,$id_source);
        $liste_typo	 			     = $this->mvisu->get_typologlie($id_source);
		
        /*foreach($liste_result_ko_motif as $k_pli_ko => $v_pli_ko){
			if(!in_array($v_pli_ko["libelle_motif"],$tab_ko_motif) && $v_pli_ko["libelle_motif"] != "" && !empty($v_pli_ko["libelle_motif"])){ 
				array_push($tab_ko_motif, $v_pli_ko["libelle_motif"]);
			}
		}
		foreach($liste_result_ko_motif as $k_pli_ko => $v_pli_ko){
			if(!in_array($v_pli_ko["date_mth"],$tab_dates) && $v_pli_ko["date_mth"] != "" && !empty($v_pli_ko["date_mth"])){ 
				array_push($tab_dates, $v_pli_ko["date_mth"]);
			}
		}
		*/
		
		foreach($liste_result_mensuel as $pli){
			//$pli_total +=  $pli['non_traite']+$pli['encours']+$pli['traite']+$pli['anomalie']+$pli['ferme']+$pli['divise']+$pli['ci_traite'];		
			if($id_source == '3') $pli_total +=  $pli['total'];
			else $pli_total +=  $pli['non_traite']+$pli['encours']+$pli['traite']+$pli['anomalie'];			
		}
		
		foreach($liste_typo as $tab){
			 ///$tab_typo["id"] 		= $tab["id"];
			 $tab_typo[$tab["id"]]  = $tab["typologie"];
			 $lib_typo = ($lib_typo == "") ? $tab["typologie"] : $lib_typo.",".$tab["typologie"];
		}
		
		$array_view_doc['liste_pli']      = $liste_result_mensuel;
		$array_view_doc['list_pli_ko']    = $list_ko_mensuel;
		$array_view_doc['liste_pli_typo'] = $liste_result_plis_typo;
		$array_view_doc['tab_typo'] 	  = $tab_typo;
		$array_view_doc['lib_typo'] 	  = $lib_typo;
		$array_view_doc['months']         = $tab_dates;
		
		//echo "<pre>";print_r($list_ko_mensuel); echo "</pre>";exit;
		
		/****************************** Fin Restant ************************************************************/
        if($pli_total > 0){
            if($id_source == '3'){
                $this->load->view('statistics/stat_plis_mensuel.php', $array_view_doc);
            }
            else{
                $this->load->view('statistics/stat_flux_mensuel.php', $array_view_doc);
            }
            
        }
        else{
            echo 0;
        }
    }

	public function recep_detail_mensuel(){
		$date_debut = $this->input->post('date_debut');
        $id_societe = $this->input->post('id_societe');
		$id_source  = $this->input->post('id_source');
       // $date_fin   = $this->input->post('date_fin');

        $array_view_lot = $liste_lot = array();
		$tab_month 	  = $this->mvisu->get_last_month($date_debut);
		if($id_source == '3'){
		    $liste_lot    = $this->mvisu->get_lot_mensuel($date_debut, $tab_month[0]["date_fin"],$id_societe);
        }
        else{
            $liste_lot    = $this->mvisu->get_lot_mensuel_flux($date_debut, $tab_month[0]["date_fin"],$id_societe,$id_source);
        }
     
		$array_view_lot['liste_lot'] = $liste_lot;
		
				
        if($liste_lot){
            if($id_source == '3'){
                $this->load->view('statistics/recep_detail_mensuel.php', $array_view_lot);
            }
            else{
                $this->load->view('statistics/recep_detail_mensuel_flux.php', $array_view_lot);
            }
            
        }
        else{
            echo 0;
        }
	}
	
	public function ajax_detail_pli(){
		
		
		$where_date_courrier = "";
		$arr = array();

		$date_debut = $this->input->post('date_debut');
		$date_fin   = $this->input->post('date_fin');

		
		$where_date_courrier = $where_date_courrier_lot = "";
		if($date_debut != '' && $date_fin != ''){
				$where_date_courrier 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
				$where_date_courrier_lot .= " view_lot.date_courrier  between '".$date_debut."' and '".$date_fin."' ";
		}
			

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}//else echo "vide";
		
		//echo $col." ".$dir; 
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}

		$arr = $this->mvisu->get_datatables($where_date_courrier,$where_date_courrier_lot,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all($where_date_courrier,$where_date_courrier_lot);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered($where_date_courrier,$where_date_courrier_lot,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		foreach($arr as $p){
				list($ann,$mois,$jr) = explode("-",$p->date_courrier);
				
				$date_traitement = ($p->dt_event == '0001-01-01') ? '' : $p->dt_event; 
				$code_type_pli   = ($p->code_type_pli == 'NT' && $p->dt_event == '0001-01-01') ? '' : $p->code_type_pli; 
				$commentaire     = ( $p->dt_event == '0001-01-01') ? '' : $p->comment; 
				$row = array(
                   // 'date_courrier' => $jr."/".$mois."/".$ann,
                    'pli' => $p->pli,
					'id_pli' => $p->id_pli,
					'typologie' => $p->typologie,
					'type_ko' => $p->type_ko,
					'flag_traitement' => $p->flag_traitement,
					'traitement' => $p->traitement,
					'dt_event' => $date_traitement,
					'lot_scan' => $p->lot_scan,
					'date_courrier' => $p->date_courrier,
					'date_numerisation' => $p->date_numerisation,
					'statut' => $p->statut,
					'code_type_pli' => $code_type_pli,
					'comment' => $commentaire,
					'nb_doc' => $p->nb_doc
                );

                array_push($data, $row);
            }

            $dtable["data"] = $data;

            echo json_encode($dtable);
		
	}
	
	public function ajax_detail_traitement(){
		
		
		$clause_where = "";
		$arr = array();

		$date_debut = $this->input->post('date_debut');
		$date_fin   = $this->input->post('date_fin');
	    $id_societe = $this->input->post('select_soc');

		
		$clause_where = $where_date_traitement_lot = "";
		$f_clause_where = "";
		if($date_debut != '' && $date_fin != ''){
				$clause_where 	.= " histo_pli.dt_event::date between '".$date_debut."' and '".$date_fin."' ";
		}
		if($id_societe != ''){
				$clause_where 	.= " and data_pli.societe = '".$id_societe."' ";
		}
		$f_clause_where = "'''".$date_debut."''' , '''".$date_fin."''',".$id_societe."";

		$length = $this->input->post('length');
		$start  = $this->input->post('start');
		$post_order = $this->input->post('order');
		$search = $this->input->post("search[value]");

		$col = 1;
		$dir = "";
		if(!empty($post_order)) {
			foreach($post_order as $o) {
				$col = $o['column'];
				$dir= $o['dir'];
			}
		}//else echo "vide";
		
		//echo $col." ".$dir; 
		
		if($dir != "asc" && $dir != "desc") {
			$dir = "asc";
		}

		$arr = $this->mvisu->get_datatables_traitement($clause_where,$f_clause_where,$length,$start,$post_order,$search,$col,$dir);
		
		$dtable = array();
		$data = array();
		$dtable["draw"] = $this->input->post_get('draw');
		$dtable["recordsTotal"]    = $this->mvisu->count_all_traitement($clause_where,$f_clause_where);
		$dtable["recordsFiltered"] = $this->mvisu->count_filtered_traitement($clause_where,$f_clause_where,$post_order,$search,$col,$dir);
		$no = $this->input->post('start');
		foreach($arr as $p){
				//." ".$p->txt_anomalie
				$row = array(
                   // 'date_courrier' => $jr."/".$mois."/".$ann,
                    'nom_societe' => $p->nom_societe,
                    'dt_event' => $p->dt_event,
					'date_courrier' => $p->date_courrier,
					'lot_scan' => $p->lot_scan,
					'pli' => $p->pli,
					'id_pli' => "<span class='fa fa-envelope'></span>".$p->id_pli,
					'nb_mvt' => $p->nb_mvt,
					'statut' => $p->statut,
					'libelle' => $p->libelle,
					'txt_anomalie' => $p->txt_anomalie,
					'libelle_motif' => $p->libelle_motif,
					'typologie' => $p->typologie,
					'mode_paiement' => $p->mode_paiement,
					'cmc7' => $p->cmc7,
					'montant' => $p->montant,
					'rlmc' => $p->rlmc,
					'infos_datamatrix' => $p->infos_datamatrix,
					'motif_rejet' => $p->motif_rejet,
					'description_rejet' => $p->description_rejet,
					'titre_titre' => $p->titre_titre,
					//'titre_societe_nom' => $p->titre_societe_nom,
					
                );

                array_push($data, $row);
            }

            $dtable["data"] = $data;

            echo json_encode($dtable);
		
	}
	
	 public function export_pli_csv(){

        $date_deb       = $this->input->post('date_deb');
        $date_fin       = $this->input->post('date_fin');

        $array_result = $this->mvisu->get_list_plis($date_deb, $date_fin);

        $array_export = array();
        $array_export['list_export'] = $array_result;

        $this->load->view('statistics/export_csv.php', $array_export);

    }

    public function export_pli_excel(){

        $date_deb       = $this->input->post('date_deb');
        $date_fin       = $this->input->post('date_fin');

        $array_result = $this->mvisu->get_list_plis($date_deb, $date_fin);
        $array_export = array();
        $array_export['list_export'] = $array_result;

        $this->load->view('statistics/export_excel.php', $array_export);

    }
	  public function export_traitement_excel(){

        $date_debut = $this->input->post('date_deb');
		$date_fin   = $this->input->post('date_fin');
	    $id_societe = $this->input->post('select_soc');

		$clause_where = $f_clause_where = "";
		if($date_debut != '' && $date_fin != ''){
				$clause_where 	.= " histo_pli.dt_event::date between '".$date_debut."' and '".$date_fin."' ";
		}
		if($id_societe != ''){
				$clause_where 	.= " and data_pli.societe = '".$id_societe."' ";
		}			
		$f_clause_where = "'''".$date_debut."''' , '''".$date_fin."''',".$id_societe."";
		
        $array_result = $this->mvisu->get_traitement_excel($clause_where, $f_clause_where);
        $array_export = array();
        $array_export['list'] = $array_result;

        $this->load->view('statistics/export_excel_traitement.php', $array_export);

    }
	
	 public function get_solde(){
		 ini_set('memory_limit', '-1');
        $date_j = date("Y-m-d");
        $id_societe = $this->input->post('select_soc');
		$date_debut = $this->input->post('date_debut');
		$date_fin   = $this->input->post('date_fin');
		$id_source   = $this->input->post('source_visu'); 
		$select_cat_typo   = $this->input->post('select_cat_typo');
		
        $array_view_plis = array();
        $tab_dates     = $tab_daty = $tab_min_daty_courrier = array();
        $tab_week      = $tab_month = $liste_solde_j = $list_societe = $liste_anomalie = $liste_anomalie_mvt = $list_ano = $list_ano_mvt = $liste_solde_mvt_j = $list_pli_sla = $list_mvt_sla = $list_ano_sla = $liste_anomalie_sla= array();
		$list_flux_sla = $list_flux_abo_sla = array();
		
		$id_src = ($id_source =='mail') ? 1 : 2;
		$module =($id_source == 'sftp' or $id_source =='mail') ? 'Abonnements' : 'Mouvements';
		
		if($select_cat_typo == '-1') {
			
				if($date_debut != '' && $date_fin != '')
				{ 
					if($id_source == 'sftp' || $id_source =='mail')
					{
						
						$list_pli_sla 	   = $this->mvisu->get_solde_sftp_sla($date_debut, $date_fin, $date_j, $id_societe,$id_src);
						$list_mvt_sla 	   = $this->mvisu->get_suivi_mvt_sftp_sla($date_debut, $date_fin, $date_j, $id_societe,$id_src);
						
					}
					else
					{  
						
						$list_pli_sla 	   = $this->mvisu->get_solde_courrier_sla($date_debut, $date_fin, $date_j, $id_societe);
						$list_mvt_sla 	   = $this->mvisu->get_suivi_mvt_courrier_sla($date_debut, $date_fin, $date_j, $id_societe);
					}
				}			
			
				
				foreach($list_pli_sla as $key => $tab){
					$id_soc 		= $tab["societe"];
					$nom_societe 	= $tab["nom_societe"];
					$date_courrier 	= $tab["date_courrier"];
					if(!in_array($nom_societe , $list_societe)){
						array_push( $list_societe,$nom_societe );
					}
					
					if($id_source == 'sftp' || $id_source =='mail')
						$list_ano_sla = $this->mvisu->get_reception_flux_anomalie_sla($date_courrier, $id_soc,$id_src);
					else
						$list_ano_sla = $this->mvisu->get_reception_anomalie_sla($date_courrier, $id_soc);
					
					if($id_source == 'sftp' || $id_source =='mail'){
						foreach($list_ano_sla as $k_ano_sla => $l_ano_sla){
						if($list_ano_sla[$k_ano_sla]["societe"] == $id_soc){
							$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_scan']  = $l_ano_sla["ko_scan"];
							$liste_anomalie_sla[$date_courrier][$nom_societe]['hp']  = $l_ano_sla["hors_peri"];
							$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_ko_scan']  = $l_ano_sla["mvt_ko_scan"];
							$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_hors_peri']  = $l_ano_sla["mvt_hors_peri"];
						}
					}
					}else{
						foreach($list_ano_sla as $k_ano_sla => $l_ano_sla){
							if($list_ano_sla[$k_ano_sla]["societe"] == $id_soc){
								$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_scan']         = $l_ano_sla["ko_scan"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_def']  		 = $l_ano_sla["ko_def"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_inconnu']      = $l_ano_sla["ko_inconnu"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_src']          = $l_ano_sla["ko_src"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_ke']    		 = $l_ano_sla["ko_ke"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_ci']    		 = $l_ano_sla["ko_ci"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_ci_editee']    = $l_ano_sla["ko_ci_editee"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_ci_envoyee']    = $l_ano_sla["ko_ci_envoyee"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['ko_abondonne']    = $l_ano_sla["ko_abondonne"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['divise']    		 = $l_ano_sla["divise"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_ko_scan']     = $l_ano_sla["mvt_ko_scan"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_ko_def']      = $l_ano_sla["mvt_ko_def"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_ko_inconnu']  = $l_ano_sla["mvt_ko_inconnu"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_ko_src']      = $l_ano_sla["mvt_ko_src"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_ko_ke']       = $l_ano_sla["mvt_ko_ke"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_ko_ci']       = $l_ano_sla["mvt_ko_ci"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_ci_editee']   = $l_ano_sla["mvt_ci_editee"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_ci_envoyee']  = $l_ano_sla["mvt_ci_envoyee"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_abondonne']   = $l_ano_sla["mvt_abondonne"];
								$liste_anomalie_sla[$date_courrier][$nom_societe]['mvt_divise']      = $l_ano_sla["mvt_divise"];
							}
						}
					}
					
					
							
				}
				$array_view_plis['list_societe']     = $list_societe;	
				$array_view_plis['list_pli_sla']     = $list_pli_sla;	
				$array_view_plis['list_mvt_sla']	 = $list_mvt_sla;			
				$array_view_plis['liste_anomalie_sla']	 = $liste_anomalie_sla;			
				$array_view_plis['module']	 		= $module;			
				$array_view_plis['source']	 		= $id_source;			
				if(count($list_mvt_sla) > 0 || count($list_mvt_sla) > 0){
					$this->load->view('statistics/view_solde_pli.php', $array_view_plis);
				}
			
			}
			else $this->get_saisie_typologie();
	 }
	 public function get_saisie()
    {
		ini_set('memory_limit', '-1');
        $date_j = date("Y-m-d");
        $id_societe = $this->input->post('select_soc');
		$date_debut = $this->input->post('date_debut');
		$date_fin   = $this->input->post('date_fin');
		$id_source   = $this->input->post('source_visu'); 
		$select_cat_typo   = $this->input->post('select_cat_typo');
		
        $array_view_plis = array();
        $tab_dates     = $tab_daty = $tab_min_daty_courrier = array();
        $tab_week      = $tab_month = $liste_solde_j = $list_societe = $liste_anomalie = $liste_anomalie_mvt = $list_ano = $list_ano_mvt = $liste_solde_mvt_j = $list_pli_sla = $list_mvt_sla = $list_ano_sla = $liste_anomalie_sla= array();
		$list_flux_sla = $list_flux_abo_sla = array();
		
		$id_src = ($id_source =='mail') ? 1 : 2;
		$module =($id_source == 'sftp' or $id_source =='mail') ? 'Abonnements' : 'Mouvements';
		
		
		if($date_debut != '' && $date_fin != '')
		{ 
			if($id_source == 'sftp' || $id_source =='mail')
			{
				$liste_solde_j 	   = $this->mvisu->get_suivi_solde_sftp($date_debut, $date_fin, $date_j, $id_societe,$id_src);
				$liste_solde_mvt_j = $this->mvisu->get_suivi_solde_mvt_sftp($date_debut, $date_fin, $date_j, $id_societe,$id_src);
				
				
			}
			else
			{  
				$liste_solde_j 	   = $this->mvisu->get_suivi_solde_courrier($date_debut, $date_fin, $date_j, $id_societe); 
				$liste_solde_mvt_j = $this->mvisu->get_suivi_solde_mvt_courrier($date_debut, $date_fin, $date_j, $id_societe);
				
				
			}
		}
		
	   	foreach($liste_solde_j as $key => $tab){
			$id_soc 		= $tab["societe"];
			$nom_societe 	= $tab["nom_societe"];
			$date_courrier 	= $tab["date_courrier"];
			if(!in_array($nom_societe , $list_societe)){
				array_push( $list_societe,$nom_societe );
			}
			
			if($id_source == 'sftp' || $id_source =='mail') 
				$list_ano = $this->mvisu->get_reception_anomalie_sftp($date_courrier, $id_soc,$id_src);
			else 
				$list_ano = $this->mvisu->get_reception_anomalie($date_courrier, $id_soc);
			if($id_source == 'sftp' || $id_source =='mail') {
				foreach($list_ano as $k_ano => $l_ano){
					if($list_ano[$k_ano]["societe"] == $id_soc){
						$liste_anomalie[$date_courrier][$nom_societe]  = $l_ano["anomalie"];
					}
				}	
			}else{
				foreach($list_ano as $k_ano => $l_ano){
					if($list_ano[$k_ano]["societe"] == $id_soc){
						$liste_anomalie[$date_courrier][$nom_societe]['anomalie']  		= $l_ano["anomalie"];
						$liste_anomalie[$date_courrier][$nom_societe]['ci']  	   		= $l_ano["ci"];
						$liste_anomalie[$date_courrier][$nom_societe]['ko_scan']  	   = $l_ano["ko_scan"];
						$liste_anomalie[$date_courrier][$nom_societe]['ko_def']  	   = $l_ano["ko_def"];
						$liste_anomalie[$date_courrier][$nom_societe]['ko_inconnu']    = $l_ano["ko_inconnu"];
						$liste_anomalie[$date_courrier][$nom_societe]['ko_ks']  	   = $l_ano["ko_ks"];
						$liste_anomalie[$date_courrier][$nom_societe]['ko_ke']  	   = $l_ano["ko_ke"];
						$liste_anomalie[$date_courrier][$nom_societe]['ko_circulaire'] = $l_ano["ko_circulaire"];
						$liste_anomalie[$date_courrier][$nom_societe]['ko_abondonne']  = $l_ano["ko_abondonne"];
						$liste_anomalie[$date_courrier][$nom_societe]['divise']  	   = $l_ano["divise"];
						$liste_anomalie[$date_courrier][$nom_societe]['ci_editee']     = $l_ano["ci_editee"];
						$liste_anomalie[$date_courrier][$nom_societe]['ci_envoyee']    = $l_ano["ci_envoyee"];
		
					}
				}
			}
				
		}
		
		foreach($liste_solde_mvt_j as $key => $tab){
			$id_soc 		= $tab["societe"];
			$nom_societe 	= $tab["nom_societe"];
			$date_courrier 	= $tab["date_courrier"];
			
			if($id_source == 'sftp' || $id_source =='mail')
				$list_ano_mvt = $this->mvisu->get_reception_anomalie_sftp_abo($date_courrier, $id_soc,$id_src);
			else
				$list_ano_mvt = $this->mvisu->get_reception_anomalie_mvt($date_courrier, $id_soc);
			if($id_source == 'sftp' || $id_source =='mail'){
				foreach($list_ano_mvt as $k_ano_mvt => $l_ano_mvt){
					if($list_ano_mvt[$k_ano_mvt]["societe"] == $id_soc){
					$liste_anomalie_mvt[$date_courrier][$nom_societe]  = $l_ano_mvt["anomalie"];
					}				
				}
			}else{
				foreach($list_ano_mvt as $k_ano_mvt => $l_ano_mvt){
					if($list_ano_mvt[$k_ano_mvt]["societe"] == $id_soc){
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['anomalie']  		= $l_ano_mvt["anomalie"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ko_scan']  		= $l_ano_mvt["ko_scan"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ko_def']  		= $l_ano_mvt["ko_def"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ko_inconnu']  	= $l_ano_mvt["ko_inconnu"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ko_ks']  		= $l_ano_mvt["ko_ks"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ko_ke']  		= $l_ano_mvt["ko_ke"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ko_circulaire']  = $l_ano_mvt["ko_circulaire"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ko_abondonne']  	= $l_ano_mvt["ko_abondonne"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['divise']  		= $l_ano_mvt["divise"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ci_editee']  	= $l_ano_mvt["ci_editee"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ci_envoyee']  	= $l_ano_mvt["ci_envoyee"];
					$liste_anomalie_mvt[$date_courrier][$nom_societe]['ci']  			= $l_ano_mvt["ci"];
					}				
				}
			}
					
		}
		
		
		$array_view_plis['liste_solde_j']    = $liste_solde_j;	
		$array_view_plis['liste_solde_mvt_j']= $liste_solde_mvt_j;	
		$array_view_plis['list_societe']     = $list_societe;	
		$array_view_plis['liste_anomalie']   = $liste_anomalie;	
		$array_view_plis['liste_anomalie_mvt'] = $liste_anomalie_mvt;
		
		$array_view_plis['module']	 		= $module;			
		$array_view_plis['source']	 		= $id_source;			
		if(count($liste_solde_j) > 0 || count($liste_solde_mvt_j) > 0){
			$this->load->view('statistics/view_saisie_pli.php', $array_view_plis);
        } 
        
	}
	
	 public function get_saisie_typologie(){
     
		 ini_set('memory_limit', '-1');
        $date_j = date("Y-m-d");
        $id_societe = $this->input->post('select_soc');
		$date_debut = $this->input->post('date_debut');
		$date_fin   = $this->input->post('date_fin');
		$select_cat_typo   = $this->input->post('select_cat_typo');
		$id_source   = $this->input->post('source_visu'); 
		
		
        $array_view_plis = array();
        $tab_dates     = $tab_daty = $tab_min_daty_courrier = array();
        $tab_week      = $tab_month = $liste_solde_j = $list_societe = $liste_anomalie = $liste_anomalie_mvt = $list_ano = $list_ano_mvt = $liste_solde_mvt_j = $list_pli_sla = $list_mvt_sla = $list_ano_sla = $liste_anomalie_sla= array();
		$list_flux_sla = $list_flux_abo_sla = array();
		
		$id_src = ($id_source =='mail') ? 1 : 2;
		$module =($id_source == 'sftp' or $id_source =='mail') ? 'Abonnements' : 'Mouvements';
		
		if($date_debut != '' && $date_fin != '')
				{ 
					if($id_source != 'sftp' && $id_source != 'mail')
					{
						$list_pli_sla 	   = $this->mvisu->get_saisie_typologie_courrier($date_debut, $date_fin, $date_j, $id_societe, $select_cat_typo);
						$list_mvt_sla 	   = $this->mvisu->get_saisie_typologie_mvt_courrier($date_debut, $date_fin, $date_j, $id_societe, $select_cat_typo);				
						
					}
					else
					{  
						$list_pli_sla 	   = $this->mvisu->get_saisie_typologie_sftp($date_debut, $date_fin, $date_j, $id_societe,$select_cat_typo, $id_src);
						$list_mvt_sla 	   = $this->mvisu->get_saisie_typologie_mvt_sftp($date_debut, $date_fin, $date_j, $id_societe,$select_cat_typo,$id_src);
						
					}
				}
				
				
				
				foreach($list_pli_sla as $key => $tab){
					$id_soc 		= $tab["societe"];
					$nom_societe 	= $tab["nom_societe"];
					$date_courrier 	= $tab["date_courrier"];
					$id_categorie 	= $tab["id_categorie"];
					if(!in_array($nom_societe , $list_societe)){
						array_push( $list_societe,$nom_societe );
					}
					
					if($id_source != 'sftp' && $id_source !='mail')
						$list_ano_sla = $this->mvisu->get_saisie_anomalie_courrier($date_courrier, $id_soc, $id_categorie);				
					else
						$list_ano_sla = $this->mvisu->get_saisie_anomalie_flux($date_courrier, $id_soc, $id_src, $id_categorie);
					
					if($id_source == 'sftp' || $id_source =='mail'){
						foreach($list_ano_sla as $k_ano_sla => $l_ano_sla){
						
						if($list_ano_sla[$k_ano_sla]["societe"] == $id_soc && $list_ano_sla[$k_ano_sla]["date_courrier"] == $date_courrier
						&& $list_ano_sla[$k_ano_sla]["id_categorie"] == $id_categorie){
						 
							$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['ko_scan']      = $l_ano_sla["ko_scan"];
							$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['hp']  	     = $l_ano_sla["hors_peri"];
							$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['mvt_ko_scan']  = $l_ano_sla["mvt_ko_scan"];
							$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['mvt_hors_peri']   = $l_ano_sla["mvt_hors_peri"];
							}
						}
					}else {
						foreach($list_ano_sla as $k_ano_sla => $l_ano_sla){
							
							if($list_ano_sla[$k_ano_sla]["societe"] == $id_soc && $list_ano_sla[$k_ano_sla]["date_courrier"] == $date_courrier
							&& $list_ano_sla[$k_ano_sla]["id_categorie"] == $id_categorie){
							 
								$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['ko_scan']      = $l_ano_sla["ko_scan"];
								$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['ko_def']  	 = $l_ano_sla["ko_def"];
								$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['mvt_ko_scan']  = $l_ano_sla["mvt_ko_scan"];
								$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['mvt_ko_def']   = $l_ano_sla["mvt_ko_def"];
								$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['anomalie']      = $l_ano_sla["anomalie"];
								$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['ci']   		  = $l_ano_sla["ci"];
								$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['mvt_anomalie']  = $l_ano_sla["mvt_anomalie"];
								$liste_anomalie_sla[$date_courrier][$nom_societe][$id_categorie]['mvt_ci']   = $l_ano_sla["mvt_ci"];
							}
						}
					}
							
				}
				/*echo "<pre>";
				print_r($liste_anomalie_sla);
				echo "</pre>";*/
				$array_view_plis['list_societe']     = $list_societe;	
				$array_view_plis['list_pli_sla']     = $list_pli_sla;	
				$array_view_plis['list_mvt_sla']	 = $list_mvt_sla;			
				$array_view_plis['liste_anomalie_sla']	 = $liste_anomalie_sla;			
				$array_view_plis['module']	 		= $module;			
				$array_view_plis['source']	 		= $id_source;	
				if(count($list_pli_sla) > 0 || count($list_pli_sla) > 0){
					$this->load->view('statistics/view_saisie_solde.php', $array_view_plis);
				}
       
		 
	 }
	
	
	
	//export xcell 
	public function exportExcell($id_societe, $titre, $type, $source=null)
	{
		//$id_societe = $this->input->post_get('societe');
		//var_dump($id_societe);die;
		if($type == "c") {
			$data = $this->mvisu->exporterExcel($id_societe);
			$data2 = $this->mvisu->exporterExcel2($id_societe);
		} else {
			//a typer
			//var_dump('zay');die;
			$data = $this->mvisu->sftp($id_societe,$source, ' and flux.statut_pli = 0 or flux.statut_pli  is null');
			$data2 = $this->mvisu->sftp($id_societe, $source,' and flux.statut_pli in (1,2,3,4,5,6) and id_lot_saisie_flux is null');
		}
		
		//var_dump($type);die;
		$phpObj = $this->excel;
		$phpObj->createSheet();
		$this->getBayrdCourrier($phpObj, $data, $titre);
		$this->getMilanCourrier($phpObj, $data2, $titre);
		$filename='Suivi_soldes.xls';
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		$objWriter = PHPExcel_IOFactory::createWriter($phpObj, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save("php://output");
		echo('ok');
	}

	//reporting bayrd courrier
	public function getBayrdCourrier($phpObj, $data, $titre)
	{
		$phpObj->setActiveSheetIndex(0);
		$phpObj->getActiveSheet()->setTitle($titre);
		$phpObj->getActiveSheet()->freezePane('A2');
		//array des entetes des cellules
		$entete = array("Id Pli", "Typologie", "Statut", "Date courrier");
		$colonne = array("A", "B", "C", "D");
		$arrayWidth = array("15", "35", "35","35");
		//$phpObj->getActiveSheet()->getRowDimension('1')->setRowHeight(50);
		//width colonne reporting typage
		$this->setWidthColumn($phpObj, $arrayWidth, $colonne);
		//former l'entete		
		$this->mettreEntete($colonne, $entete,2);
		$this->ecrireData($data);
	}

	//reporting Milan courrier
	public function getMilanCourrier($phpObj, $data, $titre)
	{
		$phpObj->setActiveSheetIndex(0);
		$phpObj->getActiveSheet()->setTitle($titre);
		$phpObj->getActiveSheet()->freezePane('A2');
		//array des entetes des cellules
		$entete = array("Id Pli", "Typologie", "Statut", "Date courrier");
		$colonne = array("F", "G", "H", "I");
		$arrayWidth = array("15", "35", "35", "35");
		//$phpObj->getActiveSheet()->getRowDimension('1')->setRowHeight(50);
		//width colonne reporting typage
		$this->setWidthColumn($phpObj, $arrayWidth, $colonne);
		//former l'entete		
		$this->mettreEntete($colonne, $entete,2);
		$this->ecrireData2($data);
	}

	//fonction à utiliser pour l'écriture sur les entêtes de l'excell
	public function mettreEntete($col, $titreCol, $deb_ecri = null)
	{
		$nbr = count($col);
		//index de la cellule à ecrire. ici 1 par défaut
		if ($deb_ecri == null) {
			$indexCellule = 1;
		} else {
			$indexCellule = $deb_ecri;
		}
		//$this->excel->getActiveSheet()->freezePane('A1');
		for ($cpt=0; $cpt<$nbr; $cpt++) {
			
			//$this->setBorderCell($col[$cpt]."$indexCellule:".$col[$cpt]."3");
			$this->setCellValue($col[$cpt]."$indexCellule", $titreCol[$cpt]);
			//$this->setBorderVertical($col[$cpt]."$indexCellule");			
		}
	
	}
	

	//mettre un border pour une cellule
	public function setBorderCell($col)
	{
		$this->excel->getActiveSheet()->getStyle($col)->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style'  => PHPExcel_Style_Border::BORDER_THIN,
						'color'  => array('rgb' => '000000'),
					),
				)
			)
		);
		$this->excel->getActiveSheet()->getSheetView()->setZoomScale(80);
	}

	//mettre un valeur dans une celulle
	public function setCellValue($col, $value)
	{
		$this->excel->getActiveSheet()->setCellValue($col, $value);
	}
	
	//bordure vertical
	function setBorderVertical($col)
	{
		$styleArray = array(
			'borders' => array(
				'right' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('argb' => 'c00000'),
				)
			),
		);

		$this->excel->getActiveSheet()->getStyle("A4:A4")->applyFromArray($styleArray);
	}

	//width pour les colonnes
	public function setWidthColumn($phpExcell, $arrayWidth, $arrayColonne)
	{
		$nbr = count($arrayWidth);
		$phpExcell->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
		//$phpExcell->getActiveSheet()->getColumnDimensionByColumn("A")->setWidth(10);
		for ($cpt=0; $cpt<$nbr; $cpt++) {
			//width column
			$phpExcell->getActiveSheet()->getColumnDimension($arrayColonne[$cpt])->setWidth($arrayWidth[$cpt]);
		   
	    }
	}

	//ecriture sur l'entête de l'excell reporting typage
	public function ecrireData($data)
	{
		//print_r($data);die;
		$num_row = 3;
		$this->fusionnerCelulle("A1","D1");
		$this->setCenter("A1");
		$this->setCellValue("A1", "IDENTIFIANT PLI A TYPER");
		//$this->setBorderCell("A1");
		if (count($data)>0) {
			foreach ($data as $value) {
				//$daty = new DateTime($value->daty);
				$this->setCellValue("A"."$num_row", $value->id_pli);
				$this->setCellValue("B"."$num_row", $value->typologie);
				$this->setCellValue("C"."$num_row", $value->flag_traitement);
				$this->setCellValue("D"."$num_row", PHPExcel_Shared_Date::PHPToExcel(DateTime::createFromFormat('d-m-Y', $value->dc)));

				$this->excel->getActiveSheet()->getStyle("D"."$num_row")->getNumberFormat()->setFormatCode('dd/mm/yyyy');
				
				$this->excel->getActiveSheet()->getRowDimension($num_row)->setRowHeight(22);
				$num_row++;	
			}
			$num_row -= 1;
			$this->setBorderCell("A2:D$num_row");
			$this->setCenter("A2:D$num_row");
		}
	}

	//ecriture sur l'entête de l'excell reporting typage
	public function ecrireData2($data)
	{
		//print_r($data);die;
		$num_row = 3;
		$this->fusionnerCelulle("F1","I1");
		$this->setCenter("F1");
		$this->setCellValue("F1", "IDENTIFIANT PLI EN ATTENTE DE SAISIE");
		//$this->setBorderCell("A1");
		if (count($data)>0) {
			foreach ($data as $value) {
				//$daty = new DateTime($value->daty);
				$this->setCellValue("F"."$num_row", $value->id_pli);
				$this->setCellValue("G"."$num_row", $value->typologie);
				$this->setCellValue("H"."$num_row", $value->flag_traitement);
				$this->setCellValue("I"."$num_row", PHPExcel_Shared_Date::PHPToExcel(DateTime::createFromFormat('d-m-Y', $value->dc)));

				$this->excel->getActiveSheet()->getStyle("I"."$num_row")->getNumberFormat()->setFormatCode('dd/mm/yyyy');
				
				$this->excel->getActiveSheet()->getRowDimension($num_row)->setRowHeight(22);
				$num_row++;	
			}
			$num_row -= 1;
			$this->setBorderCell("F2:I$num_row");
			$this->setCenter("F2:I$num_row");
		}
	}

	//horizontal center
	public function setCenter($col)
	{
		$styleArray = array(
			    'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'wrap'   	 => true,
				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		        'rotation'   => 0
				
			),
		);
		$this->excel->getActiveSheet()->getStyle($col)->applyFromArray($styleArray);;
	}

	//fusionner deux cellulles en cas de besoin
	public function fusionnerCelulle($col1, $col2)
	{
		$this->excel->getActiveSheet()->mergeCells("$col1:$col2");
	}
}
?>
