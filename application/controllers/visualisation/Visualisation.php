<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Visualisation extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->portier->must_observ();
        $this->load->model('visualisation/Model_visualisation', 'mvisu');
    }

    public function index(){

        $this->visualisation_pli();

    }

    public function visualisation_pli(){

        $type_statut = $this->mvisu->get_statut_traitement();

        $array_view_statut = array();

        if($type_statut){
            $array_view_statut['type_statut'] = $type_statut;
        }

        $this->load->view('visualisation/header.php');
        //$this->load->view('visualisation/left_menu.php');
        $this->load->view('visualisation/visualisation.php', $array_view_statut);
        $this->load->view('visualisation/footer-visualise.php');
        $this->load->view('visualisation/footer.php');

    }

    public function recherche_pli()
    {
        $num_cmd        = $this->input->post('numcommande');
        $num_client     = $this->input->post('numclient');
        $nom_client     = $this->input->post('nom_client');
        $prenom_client  = $this->input->post('prenom_client');
        $num_tel        = $this->input->post('numtel');
        $code_postal    = $this->input->post('codepostal');
        $statut         = $this->input->post('statut');
        $date_deb       = $this->input->post('date_deb');
        $date_fin       = $this->input->post('date_fin');

        $length         = $this->input->post_get('length');
        $start          = $this->input->post_get('start');
        $search         = $this->input->post_get('search');

        $search = trim($search['value']);

        if(!isset($length) and !isset($start)){
            $length = 10;
            $start = 0;
        }

        $array_result = $this->mvisu->get_doc($num_cmd, $num_client, $nom_client, $prenom_client, $num_tel, $code_postal, $statut, $date_deb, $date_fin, $length, $start, $search);
        $liste_result = $array_result['list_result'];
        $nb_filtered = $array_result['nb_total'][0]->nb_doc;
        //$array_view_doc['liste_pli_doc'] = $liste_result;

        if($array_result){
            $dtable = array();
            $data = array();
            $dtable["draw"] = $this->input->post_get('draw');
            $dtable["recordsTotal"] = count($array_result['list_result']);
            $dtable["recordsFiltered"] = $nb_filtered;
            foreach($liste_result as $value){
				list($ann,$mois,$jr) = explode("-",$value->date_courrier);
                $row = array(
                    'date_courrier' => $jr."/".$mois."/".$ann,
                    'pli'           => $value->pli,
                    'lot_scan'      => $value->lot_scan,
                    'document'      => $value->type_document,
                    'num_commande'  => ($value->num_commande)?$value->num_commande:'',
                    'num_client'    => ($value->num_client)?$value->num_client:'',
                    'nom_client'    => ($value->nom_client)?$value->nom_client:'',
                    'prenom_client' => ($value->prenom_client)?$value->prenom_client:'',
                    'tel_client'    => ($value->num_tel_client_pli)?$value->num_tel_client_pli:'',
                    'code_postal'   => ($value->code_postal)? $value->code_postal:'',
                    'statut'        => $value->traitement,
                    'visualiser'    => '<button class="btn btn-info" id="doc_'.$value->id_document.'" onclick="afficher_doc('.$value->id_document.')"><i class="fa fa-eye"></i></button>'
                );

                array_push($data, $row);
            }

            $dtable["data"] = $data;

            echo json_encode($dtable);

            //$this->load->view('visualisation/liste_pli.php', $array_view_doc);
        }
        else{
            echo 0;
        }
    }

    public function export_datatable_csv(){

        $num_cmd        = $this->input->post('numcommande');
        $num_client     = $this->input->post('numclient');
        $nom_client     = $this->input->post('nom_client');
        $prenom_client  = $this->input->post('prenom_client');
        $num_tel        = $this->input->post('numtel');
        $code_postal    = $this->input->post('codepostal');
        $statut         = $this->input->post('statut');
        $date_deb       = $this->input->post('date_deb');
        $date_fin       = $this->input->post('date_fin');

        $array_result = $this->mvisu->get_doc($num_cmd, $num_client, $nom_client, $prenom_client, $num_tel, $code_postal, $statut, $date_deb, $date_fin, null, null, null);

        $array_export = array();
        $array_export['list_export'] = $array_result['list_result'];

        $this->load->view('visualisation/export_csv.php', $array_export);

    }

    public function export_datatable_excel(){

        $num_cmd        = $this->input->post('numcommande');
        $num_client     = $this->input->post('numclient');
        $nom_client     = $this->input->post('nom_client');
        $prenom_client  = $this->input->post('prenom_client');
        $num_tel        = $this->input->post('numtel');
        $code_postal    = $this->input->post('codepostal');
        $statut         = $this->input->post('statut');
        $date_deb       = $this->input->post('date_deb');
        $date_fin       = $this->input->post('date_fin');

        $array_result = $this->mvisu->get_doc($num_cmd, $num_client, $nom_client, $prenom_client, $num_tel, $code_postal, $statut, $date_deb, $date_fin, null, null, null);

        $array_export = array();
        $array_export['list_export'] = $array_result['list_result'];

        $this->load->view('visualisation/export_excel.php', $array_export);

    }

    public function affiche_pli(){

        $id_pli = $this->input->post('id_pli');
        //$id_doc = $this->input->post('id_doc');

        $array_view_pli = array();

        $pli = $this->mvisu->get_one_pli($id_pli);

        $array_view_pli['info_pli'] = $pli;

        if($pli){
            $this->load->view('visualisation/info_pli', $array_view_pli);
        }

    }

    public function affiche_doc(){

        $id_doc = $this->input->post('id_doc');
        //$bool = $this->input->post('bool');

        $array_view_doc = array();

        //$doc = $this->mvisu->get_one_doc($id_doc);

        $info = $this->mvisu->get_info_doc($id_doc);

        $array_view_doc['doc'] = $info['doc'];
        $array_view_doc['info'] = $info['champs'];
        //$array_view_doc['bool'] = $bool;

        if($info){
            $this->load->view('visualisation/info_doc', $array_view_doc);
        }

    }

    public function visualise_anomalis(){
        $this->load->view('admin/header.php');
        //$this->load->view('visualisation/left_menu.php');
        $this->load->view('visualisation/header_visualise.php');
        $this->load->view('visualisation/view_anomalie.php');
        $this->load->view('visualisation/footer-visualise.php');
        $this->load->view('admin/footer.php');
    }

    public function production()
    {
        $this->load->view('visualisation/header.php');
        //typologi document
        $doc = $this->mvisu->getFlagDoc();
        //typologie plis
        $typePlis = $this->mvisu->getTypePlis();
        //stat traitement
        $statut = $this->mvisu->get_statut_traitement();
        //find users
        $users = $this->mvisu->getUsers();
        $array['doc'] = $doc;
        $array['typePlis'] = $typePlis;
        $array['statut'] = $statut;
        $array['users'] = $users;

        //$this->load->view('visualisation/left_menu.php');
        $this->load->view('visualisation/production.php', $array);
        $this->load->view('visualisation/footer-visualise.php');
        $this->load->view('visualisation/footer.php');

    }

    //focntion appelle via ajax pour afficher les resultat
    public function recherche_prod()
    {
        //recuperation de données postées
        $idpli       = $this->input->post('idplis');
        $plis        = $this->input->post('plis');
        $lotScan     = $this->input->post('lotscan');
        $traiterPar      = $this->input->post('traitePar');
        $statutPlis  = $this->input->post('statutPlis');
        $typePlis    = $this->input->post('typePlis');
        $typerPar  = $this->input->post('typePar');
        $datedeb     = $this->input->post('datedeb');
        $datefin     = $this->input->post('datefin');
        $dateTtr     = $this->input->post('dateTtr');
        $dateTtrFin  = $this->input->post('dateTtrFin');

        //methode appeler pour la liste des productions
        $doc['prod'] = $this->mvisu->get_production($idpli, $plis, $lotScan, $traiterPar, $statutPlis, $typePlis, $typerPar, $datedeb, $datefin, $dateTtr, $dateTtrFin);
        $this->load->view('visualisation/list_production.php', $doc);
    }

    //detailler les plis
    public function detailler_pli()
    {
        $idpli       = $this->input->post('id_pli');
        $data = array();
        $data['pli'] = $this->mvisu->get_production($idpli);
        $data['id'] = $idpli;
        //test        
        if($data){
            $this->load->view('visualisation/detail_pli', $data);
        }

    }

}
