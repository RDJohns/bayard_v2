<?php

class Fautes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        set_time_limit(7200); 

        if((int)$this->session->userdata('id_utilisateur') == 0)
        {
            log_message('error', $this->session->userdata('infouser').' session perdue');
        }

        $this->load->model("taux_fautes/Model_Taux_Fautes", "MFautes");
        
    }

    public function index()
    {
        $this->session->set_userdata('infouser', " IP:". $this->input->ip_address()." LOGIN: ".(int)$this->session->userdata('id_utilisateur'));
        if((int)$_SESSION['id_type_utilisateur'] == 4 )
        {
            $this->viewPrincipale();

        }
        else{
            redirect(base_url());
        }
        
    }

    public  function viewPrincipale()
    {
        $head                       = array();
        $foot                       = array();

        $head["title"]               = $head["menu"] = "Visualisation taux de fautes";
        $head["theme"]               = 'theme-teal';
        $head["icon"]                = "equalizer";
        $head['css']                 = array(
            'font/font',
            'font/icon',
            'plugins/bootstrap/css/bootstrap.min',
            'plugins/node-waves/waves',
            'plugins/animate-css/animate',
            'plugins/jquery-spinner/css/bootstrap-spinner',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/sweetalert/sweetalert',
            'plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            'plugins/bootstrap-select/css/bootstrap-select',
            'plugins/bootstrap-datepicker/css/bootstrap-datepicker',
            'plugins/nprogress/nprogress',
            'plugins/morrisjs/morris',
            'plugins/daterangepicker/daterangepicker',
            'css/style',
            'css/themes/all-themes',
            'css/custom/deverouiller',
            'css/custom/reception',
			'../src/template/css/font-awesome.min'
        );


        $head['js']                 = array(
            'plugins/jquery/jquery',
            'plugins/bootstrap/js/bootstrap',
            'plugins/raphael/raphael.min',
            'plugins/morrisjs/morris'
        );
        
        $foot['js']                 = array(
            'plugins/node-waves/waves',
            'js/pages/ui/tooltips-popovers',
            'js/pages/ui/notifications',
            'plugins/jquery-validation/jquery.validate',
            'plugins/jquery-validation/localization/messages_fr',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-notify/bootstrap-notify',
            'js/pages/ui/notifications',
            'plugins/jquery-datatable/jquery.dataTables',
            'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            'plugins/jquery-datatable/extensions/export/dataTables.buttons.min',
            'plugins/jquery-datatable/extensions/export/buttons.flash.min',
            'plugins/jquery-datatable/extensions/export/jszip.min',
            'plugins/jquery-datatable/extensions/export/pdfmake.min',
            'plugins/jquery-datatable/extensions/export/vfs_fonts',
            'plugins/jquery-datatable/extensions/export/buttons.html5.min',
            'plugins/jquery-datatable/extensions/export/buttons.print.min',
            'plugins/jquery-datatable/extensions/dataTables.fixedColumns',
            'plugins/sweetalert/sweetalert.min',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'plugins/nprogress/nprogress',
            'plugins/bootstrap-select/js/bootstrap-select',
            'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
            'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min',
            'plugins/daterangepicker/moment.min',
            'plugins/daterangepicker/daterangepicker',
            'js/admin',
            'js/pages/tables/jquery-datatable',
            'plugins/jquery-slimscroll/jquery.slimscroll',
            'js/pages/ui/dialogs',
            'js/pages/ui/modals',
            'js/taux_fautes/fautes',
        );


        $this->load->view("main/header",$head);
        $this->load->view("taux_fautes/filtre");
        $this->load->view("taux_fautes/donut_typologie_champ"); 
        
     
        $this->load->view("taux_fautes/par_typologie");
        $this->load->view("taux_fautes/par_champ");
        $this->load->view("taux_fautes/par_operateur");
        
        
       
        $this->load->view("main/footer",$foot);
    }

    public function fautesParChamp()
    {
        $date1      = $this->input->post("debut");
        $date2      = $this->input->post("fin");
        $societe    = $this->input->post("societe"); 
    
        $receptionParTypo = $this->MFautes->fautesParChamp($date1,$date2,$societe);
        echo json_encode($receptionParTypo);
        return "";
    }
    public function fautesParTypologie()
    {
        $date1      = $this->input->post("debut");
        $date2      = $this->input->post("fin");
        $societe    = $this->input->post("societe"); 
    
        $receptionParTypo = $this->MFautes->fautesParTypologie($date1,$date2,$societe);
        echo json_encode($receptionParTypo);
        return "";
    }

    public function fautesParOperateurGoblale()
    {
        $column =  array();
        $data   = array();
        $column = array("login","controled","change","taux");

        $date1      = 0;//$this->input->post("dateDebut");
        $date2      = 0;//$this->input->post("dateFin");
        $societe    = 0;//$this->input->post("societe"); 

        $date1      = $this->input->post("debut");
        $date2      = $this->input->post("fin");
        $societe    = $this->input->post("societe");  

        $liste = $this->MFautes->fautesParOperateurGoblale($date1,$date2,$societe);
        if($liste)
        {
            foreach($liste as $l)
            {
                $row = array();
                foreach($column as $itCol)
                    {
                        $row[] = $l->$itCol;
                    }
                $data[] = $row;
            }
        }

        $output     = array("data" => $data);
        echo json_encode($output);
        return "";
    }

    public function fautesParOperateurDetaillee()
    {
        $date1      = $this->input->post("debut");
        $date2      = $this->input->post("fin");
        $societe    = $this->input->post("societe"); 
        $granularite    = $this->input->post("granularite"); 

        $user       = array();
        $thead      = array();
        $date       = array();
        $login      = array();

        $strGranularite =" dt_saisie::date ";
        
        if(trim($granularite) == 'j')
        {
            $strGranularite =" dt_saisie::date ";
        }

        if(trim($granularite) == 's')
        {
            $strGranularite =" date_part('week'::text, dt_saisie) ";
           
        }

        $tabfinale = "";
        $liste = $this->MFautes->fautesParOperateurDetaillee($date1,$date2,$societe,$strGranularite);
        
        if($liste)
        {
            foreach($liste as $l)
            {
               $user[] = $l->saisie_par;
               $thead[]= $l->dt_saisie;
               $data[$l->dt_saisie][$l->saisie_par]["login"] = $l->login ;
               $data[$l->dt_saisie][$l->saisie_par]["controled"] =$l->controled ;
               $data[$l->dt_saisie][$l->saisie_par]["change"] = $l->change ;
               $data[$l->dt_saisie][$l->saisie_par]["taux"] = $l->taux ;
               $login[$l->saisie_par] = $l->login;
            }
           //     var_dump($login);
            $strTable       = "<table id='table-liste-op-detaille' class='table table-bordered table-striped table-hover dataTable' width='100%' >";
            $strTableTh     = "<thead style='background: white !important;'><tr><th>Opérateur</th>";
            $strTableBody   = "";
            $iThead     = 0;
            $thead = array_unique($thead);
            array_multisort($thead);
           // $login = array_unique($login);
            $user = array_unique($user);
            
            //var_dump($login);
            foreach($user as $listUser)
            {
                /*$bodyTLogin     = "<tr>";
                $bodyChampErr   = "<tr>";
                $bodyChampTotal = "<tr>";*/
                $bodyTaux       = "<tr>";
                
                $bodyTaux      .= "<td style='background: white !important;'>".$login[$listUser]."</td>";
                
                foreach($thead as $listeDate)
                {
                    if($iThead == 0 )
                    {
                        //2020-07-13
                        /*list($annee,$mois,$jour) = explode("-",$listeDate);
                        $strTableTh .= "<th>".$jour."/".$mois."</th>";*/
                        if(trim($granularite) == 's')
                        {
                            $strTableTh .= "<th>S".$listeDate."</th>";
                        }
                        else
                        {
                            list($annee,$mois,$jour) = explode("-",$listeDate);
                            $strTableTh .= "<th>".$jour."/".$mois."</th>";
                        }
                    }
                    
                    $taux = isset($data[$listeDate][$listUser]["taux"]) ? $data[$listeDate][$listUser]["taux"] : 0;   
                    $bodyTaux      .= "<td>".$taux."</td>";
                    
                }
                $bodyTaux       .= "</tr>";

                $strTableBody   .= $bodyTaux;
                $iThead++;
            }
            $strTableTh .= "</tr></thead>";

            $tabfinale =  $strTable.$strTableTh.$strTableBody."</table>";
        }

       
        
        echo $tabfinale;

    }
    public function fautesParTypologieDetaillee()
    {
        $aTypologie = array();
        $data = array();
        $thead = array();
        
        $date1      = $this->input->post("debut");
        $date2      = $this->input->post("fin");
        $societe    = $this->input->post("societe"); 
        $granularite    = $this->input->post("granularite"); 

        $strGranularite =" dt_saisie::date ";
        
        if(trim($granularite) == 'j')
        {
            $strGranularite =" dt_saisie::date ";
        }

        if(trim($granularite) == 's')
        {
            $strGranularite =" date_part('week'::text, dt_saisie) ";
           
        }

        $typologie = $this->MFautes->getTypologie();
        foreach($typologie as $t)
        {
            $aTypologie[$t->id ]= $t->typologie;
        }

        $liste = $this->MFautes->parTypologieDetaillee($date1,$date2,$societe,$strGranularite);
        $iThead     = 0;
        if($liste)
        {
            $strTable       = "<table id='table-liste-typ-detaille' class='table table-bordered table-striped table-hover dataTable' width='100%' >";
            $strTableTh     = "<thead style='background: white !important;'><tr><th>Typologie</th>";
            $strTableBody   = "";
            $iThead     = 0;

            foreach($liste as $l)
            {
               
               $thead[]= $l->dt_saisie;
               $data[$l->dt_saisie][$l->typologie]["id_typologie"] = $l->typologie ;
               $data[$l->dt_saisie][$l->typologie]["taux"] = $l->taux ;
            }
            $keyTypo = array_keys($aTypologie);
            $thead = array_unique($thead);
            array_multisort($thead);
            foreach($keyTypo as $typo )
            {
                /*$bodyTLogin     = "<tr>";
                $bodyChampErr   = "<tr>";
                $bodyChampTotal = "<tr>";*/
                $bodyTaux       = "<tr>";
                
                $bodyTaux      .= "<td style='background: white !important;'>".$aTypologie[$typo]."</td>";
                
                foreach($thead as $listeDate)
                {
                    if($iThead == 0 )
                    {
                        
                        /* list($annee,$mois,$jour) = explode("-",$listeDate);
                        $strTableTh .= "<th>".$jour."/".$mois."</th>";*/
                        if(trim($granularite) == 's')
                        {
                            $strTableTh .= "<th>S".$listeDate."</th>";
                        }
                        else
                        {
                            list($annee,$mois,$jour) = explode("-",$listeDate);
                            $strTableTh .= "<th>".$jour."/".$mois."</th>";
                        }
                    }
                    
                    $taux = isset($data[$listeDate][$typo]["taux"]) ? $data[$listeDate][$typo]["taux"] : 0;   
                    $bodyTaux      .= "<td>".$taux."</td>";
                    
                }
                $bodyTaux       .= "</tr>";

                $strTableBody   .= $bodyTaux;
                $iThead++;
               
                //echo "<br/>".$aTypologie[$typo];
               
            }
            
            
            
        }
         $strTableTh .= "</tr></thead>";

        $tabfinale =  $strTable.$strTableTh.$strTableBody."</table>";
        echo $tabfinale; return "";
    }


    public function parChampDetaillee()
    {
        $date1      = $this->input->post("debut");
        $date2      = $this->input->post("fin");
        $societe    = $this->input->post("societe"); 
        $granularite    = $this->input->post("granularite"); 

       /* $date1      = "2020-01-01";
        $date2      = "2020-12-31";
        $societe    = 1; 
        $granularite = 'j';*/

        $user       = array();
        $thead      = array();
        $date       = array();
        $champ      = array();
        

        /*
            dt_saisie,
	        coalesce(login,'Inconnu') login,controled,	
            saisie_par,round(change*100.00/controled,2)||'%' taux,
            change
         */
        $strGranularite =" dt_saisie::date ";
        
        if(trim($granularite) == 'j')
        {
            $strGranularite =" dt_saisie::date ";
        }

        if(trim($granularite) == 's')
        {
            $strGranularite =" date_part('week'::text, dt_saisie) ";
           
        }
        $tabfinale = "";
        $liste = $this->MFautes->parChampDetaillee($date1,$date2,$societe,$strGranularite);
        
        if($liste)
        {
            foreach($liste as $l)
            {
               $champNom[] = $l->name_field;
               $thead[]= $l->dt_saisie;
               $data[$l->dt_saisie][$l->name_field]["champ"] = $l->champ ;
               $data[$l->dt_saisie][$l->name_field]["taux"] = $l->taux ;
               $champ[$l->name_field] = $l->champ;
            }
           
            $strTable       = "<table id='table-liste-chmp-detaille' class='table table-bordered table-striped table-hover dataTable' width='100%' >";
            $strTableTh     = "<thead style='background: white !important;'><tr><th>Champ</th>";
            $strTableBody   = "";
            $iThead     = 0;
            $thead = array_unique($thead);
            array_multisort($thead);
           
            $champNom = array_unique($champNom);
            
            //var_dump($login);
            foreach($champNom as $listChamp)
            {
                /*$bodyTLogin     = "<tr>";
                $bodyChampErr   = "<tr>";
                $bodyChampTotal = "<tr>";*/
                $bodyTaux       = "<tr>";
                
                $bodyTaux      .= "<td style='background: white !important;'>".$champ[$listChamp]."</td>";
                
                foreach($thead as $listeDate)
                {
                    if($iThead == 0 )
                    {
                        //2020-07-13
                        if(trim($granularite) == 's')
                        {
                            $strTableTh .= "<th>S".$listeDate."</th>";
                        }
                        else
                        {
                            list($annee,$mois,$jour) = explode("-",$listeDate);
                            $strTableTh .= "<th>".$jour."/".$mois."</th>";
                        }
                        
                    }
                    
                    $taux = isset($data[$listeDate][$listChamp]["taux"]) ? $data[$listeDate][$listChamp]["taux"] : 0;   
                    $bodyTaux      .= "<td>".$taux."</td>";
                    
                }
                $bodyTaux       .= "</tr>";

                $strTableBody   .= $bodyTaux;
                $iThead++;
            }
            $strTableTh .= "</tr></thead>";

            $tabfinale =  $strTable.$strTableTh.$strTableBody."</table>";
        }

       
        
        echo $tabfinale;

    }
    public function verification()
    {
        $date1      = $this->input->post("debut");
        $date2      = $this->input->post("fin");
        $societe    = $this->input->post("societe"); 
        $liste = $this->MFautes->verification($date1,$date2,$societe);
        echo json_encode($liste); return "";
    }
    
}