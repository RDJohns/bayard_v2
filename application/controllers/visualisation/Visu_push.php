<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Visu_push extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('portier');
        $this->load->model('admin/model_support_push', 'push_model');
        $this->load->model('visualisation/model_visu_push', 'visu_model');
      }

    public function index()
    {
        
        if((int)$this->session->userdata('id_type_utilisateur') != 7) /* pour relation client Benja*/
        {
            /* $this->portier->must_observ(); */
            $this->visualisation();
        }
        else
        {
            $this->goRelationClient();
        }
    }


    public function goRelationClient()
    {
        
      $this->load->library('admin/page');
        $this->page->add_css('../../src/template/css/font-awesome.min');
        $this->page->set_titre('<i class="fa fa-sign-in"></i> Visualisation des MAIL/SFTP');
        $this->page->add_links('Visualisation des courriers', site_url('visual/visual'), 'fa fa-eye', 'cyan');

        $this->page->add_js('admin/support');
        $this->page->add_js('admin/fixedColumns');
        $this->page->add_js('admin/visu_push');
        $this->page->add_css('admin/visu_push');
        //get source
        $data_source = $this->push_model->getSource();
        //get societe 
        $data_soc = $this->push_model->getSociete();
        //get typologie 
        $data_typo = $this->push_model->getTypologie();
        //statut pli
        $data_stat_pli = $this->push_model->getStatut();
        //filtre date obligatoire  
        $data_dossier = $this->push_model->getDossier();
        
        $data = array();
        
        $data = array(
            'source' => $data_source,
            'societe' => $data_soc,
            'typologies' => $data_typo,
            'statut' => $data_stat_pli,
            'dossier'=>$data_dossier,
            'relation_client' => 1
          );
        $this->page->afficher($this->load->view('push/visu/index',$data, true));

    }

    //template support push
    public function visualisation()
    {
      //$this->load->library('visual/page');
        $this->load->library('admin/page');
        $this->page->add_css('../../src/template/css/font-awesome.min');
        $this->page->set_titre('<i class="fa fa-sign-in"></i> Visualisation des MAIL/SFTP');
        $this->page->add_links('Recherche avancée Pli et Flux', site_url('recherche/avancee'), 'fa fa-search-plus', 'pink');
        $this->page->add_links('Visualisation des courriers', site_url('visual/visual'), 'fa fa-eye', 'cyan');
        $this->page->add_links('Visualisation batch', site_url('anomalie/batch'), 'fa fa-bug', 'red');
        $this->page->add_links('Stat. GED', site_url('statistics/Stat'), 'fa fa-arrow-circle-right', 'brown');
        $this->page->add_links('Visualisation des MAIL/SFTP', site_url('visualisation/visu_push'), 'fa fa-sign-in', 'deep-purple');
        $this->page->add_links('Réception', site_url('reception/pli_flux'), 'fa fa-envelope', 'light-green');
        $this->page->add_links('Suivi mensuel', site_url('statistics/statistics/stat_mensuel'), 'fa fa-signal', 'teal');
        $this->page->add_links('Suivi des soldes', site_url('statistics/statistics/suivi_solde'), 'fa fa-battery-half', 'pink-cstm');
        $this->page->add_links('Traitement', site_url('statistics/statistics_flux/stat_traitement'), 'fa fa-tasks', 'light-blue');
        $this->page->add_links('Extraction', site_url('anomalie/anomalie/visu_anomalie'), 'fa fa-file-text', 'cyan-t');
        $this->page->add_links('Suivi des espèces', site_url('suivi/especes'), 'fa fa-money', 'amber');
        $this->page->add_links('Suivi des provenances', site_url('provenance/pli'), 'fa fa-flag', 'green');
        $this->page->add_links('Suivi des ch&egrave;ques non REB', site_url('suivi/reliquat'), 'fa fa-list', 'lime');
        $this->page->add_links('Suivi des ch&egrave;ques non saisis', site_url('suivi/nosaisie'), 'fa fa-clipboard', 'blue-grey');
        $this->page->add_links('Suivi des DMT', site_url('dmt/dmt/visu_dmt'), 'fa fa-hourglass-1', 'grenat');
        $this->page->add_js('admin/support');
        $this->page->add_js('admin/fixedColumns');
        $this->page->add_js('admin/visu_push');
        $this->page->add_css('admin/visu_push');
        //get source
        $data_source = $this->push_model->getSource();
        //get societe 
        $data_soc = $this->push_model->getSociete();
        //get typologie 
        $data_typo = $this->push_model->getTypologie();
        //statut pli
        $data_stat_pli = $this->push_model->getStatut();
        //var_dump($data_stat_pli);die;
        //filtre date obligatoire  
        $data_dossier = $this->push_model->getDossier();
        //Etat pli
        $data_etat_pli = $this->visu_model->getEtat();
        
        //filtre date obligatoire            
        $data = array();
        $data = array(
            'source' => $data_source,
            'societe' => $data_soc,
            'typologies' => $data_typo,
            'statut' => $data_stat_pli,
            'dossier'=>$data_dossier,
            'relation_client' => 0,
            'etat'=>$data_etat_pli
            // 'statut' => $data_stat_pli,
            // 'user_typage' => $op_typage,
            // 'user_saisie' => $op_saisie,
            // 'motif' => $motif_blocage
          );
        $this->page->afficher($this->load->view('push/visu/index',$data, true));
    }

    //criteres de recherche
    function getData()
    {
       
        $clause_where = '';
        $date_recep1 = $this->input->post_get('date_ttr1');
        $date_recep2 = $this->input->post_get('date_ttr2');
        $from = $this->input->post_get('from_flux');
        $from_name_flux = $this->input->post_get('from_name_flux');
        $objet_flux = $this->input->post_get('objet_flux');
        $from_abonne = $this->input->post_get('from_abonne');
        $from_payeur = $this->input->post_get('from_payeur');
        $societe = $this->input->post_get('societe');
        $source = $this->input->post_get('source');
        $typologie= $this->input->post_get('typologie');
        $statut = $this->input->post_get('statut');
        $fichier = $this->input->post_get('fichier');
        $id_flux = $this->input->post_get('id_pli');
        $dossier = $this->input->post_get('dossier');
        $etat = $this->input->post_get('etat');
        $order = $this->input->post_get('order');
        
        //cluase where
        $clause_where .= $this->whereDate($date_recep1, $date_recep2);
        //param value and column
        $clause_where .= $this->whereIlike($from, 'from_flux');
        $clause_where .= $this->whereIlike($from_name_flux, 'fromname_flux');
        $clause_where .= $this->whereIlike($objet_flux, 'objet_flux');
        $clause_where .= $this->where($societe, 'societe.id');
        $clause_where .= $this->where($id_flux, 'flux.id_flux');
        $clause_where .= $this->where($source, 'source.id_source');
        $clause_where .= $this->where($typologie, 'id_typologie');
        $clause_where .= $this->where($statut, 'statut_pli.id_statut');
        $clause_where .= $this->whereIlike($from_abonne, 'nom_abonne');
        $clause_where .= $this->whereIlike($from_payeur, 'nom_payeur');
        $clause_where .= $this->whereIlike($fichier, 'nom_fichier');

        $clause_where .= $this->whereInString($dossier,"sous_dossier");
        $clause_where .= $this->where($etat, 'flux.etat_pli_id');
        //var_dump($clause_where);die;
        $draw = intval($this->input->post_get("draw"));
        $length            = $this->input->post_get('length');
        $start             = $this->input->post_get('start');
        //order by
        $orderBy = $this->getOrder($order[0]);
        //date reception
        $data = $this->visu_model->getFlux($clause_where, $length, $start, $orderBy);  
        //var_dump($data);die; 
        $row = array();
        if (count($data)>0) {
            foreach($data as $r) {
                $balise = "disabled ";
                $html = "disabled ";
                $title = "Pas d'abonnement disponible";
                $title_ano = "Pas d'anomalie";
                if( intval($r->nbr_abo)>0) {
                  $fic = $r->id_source == 1?$r->nom_fichier:$r->filename_origin;
                  $balise = ' onclick= "visu_Abonnement('.$r->id_flux.','."'$r->societe'".','."'$fic'".','."'$r->dern_traitement'".')" ';
                  $title = "Voir abonnement(s)";
                };
                if( intval($r->id_etat_pli == 11 || $r->id_etat_pli == 12  || $r->id_etat_pli == 14 || $r->id_etat_pli == 100)) {
                  $html = 'onclick= "visu_anomalie('.$r->id_flux.')" ';
                  $title_ano = "Voir anomalie(s)";  
                };
                $statut = $r->statut;
                if($r->id_statut == 8){
                  $statut = '<a href="javascript:void(0);" title="Editer consigne" onclick="edit_consigne_ke(\''.$r->id_flux.'\');">'.$statut.'</a>';
                }
                  /*http://localhost:8888/GED/bayard/index.php/push/typage/Pli/download/10785
                   strMail +='Fichier : <a  class="name_file" href="'+base+'" style="cursor:pointer;">'+flux.filename_origin+'</a>';
                   download
                  */
                $fichier = $r->id_source == 1?$r->nom_fichier:$r->filename_origin;
                $downloadFile = "<a title= 'Cliquer pour télécharger le fchier : ".$fichier."' href='".base_url()."index.php/visualisation/visu_push/download/".$r->id_flux."'>".$fichier."</a>";

                $r->statut_lib == 0 ? $etat = $r->etat : $etat = " " ;
                $row[] = array(
                  $r->id_flux,
                  $r->sous_dossier,
                  /*$r->id_source == 1?$r->nom_fichier:$r->filename_origin,*/ 
                  $downloadFile,
                  $r->objet_flux, 
                  $r->from_flux, 
                  $r->fromname_flux,
                  $r->societe,
                  $r->source,
                  $r->etape_lib,//etape (typage, saisie,reatraitement)
                  $r->etat_lib,//etat(en cous, a traiter, termine)
                  $etat,
                  $r->typologie,
                  $r->dern_traitement,
                  $r->entite,
                  $r->nbr_abo,
                  $r->motif,
                  $r->id_source == 1?'<button type="button" onclick="visual_jointes('.$r->id_flux.')" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left" title="Pièces jointes">
                  <i class="material-icons">attach_file</i>
                  </button>'
                  :
                  '<button type="button" disabled class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left" title="Pas de pièce(s) jointe(s)">
                  <i class="material-icons">attach_file</i>',
                  '<button type="button" '.$balise.' class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left" title="'.$title.'">
                  <i class="material-icons">more</i> </button>',
                  '<button type="button" '.$html.' class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="left" title="'.$title_ano.'">
                  <i class="material-icons">announcement</i>
                  </button>'
                );
            }
          }
          $total =  $this->push_model->getTotal($clause_where, $length, $start);
          $record = sizeof($total)>0?$total[0]->count:0;
          $output = array(
            "draw" => $draw,
            "recordsTotal" =>$record,
            "recordsFiltered" => $record,
            "data" => $row
          );
          echo json_encode($output);
        
        //var_dump($data);die;

    }

    //where like date
    function whereDate($dt1, $dt2)
    {
        if($dt1 != '' and $dt2 != '') {
            if($dt1 == $dt2) {
              $where = "and date_reception::date = '".$dt1."'";
            } else {
              $where = "and date_reception::date between '".$dt1."' and '".$dt2."'";
            }
            return $where;
        }
        
    }

   /*
      former les clauses where
    */
    function whereIlike($value, $colonne)
    {
      
        if($value != '') {
            $clause_where = " and ".$colonne." ilike '%".$value."%' ";
            //var_dump($value);die;
            if($colonne == 'nom_fichier') {
              $clause_where = " and (nom_fichier ilike '%".$value."%' or filename_origin ilike '%".$value."%')";
              //return $clause_where;
            }

            if($colonne == 'nom_abonne') {
              $flux_id_abo =  $this->push_model->getIdFluxAbonnne($value);

              if (count($flux_id_abo)>0){
                  $fluAbo = array();
                  foreach($flux_id_abo as $flux){
                          array_push($fluAbo,$flux->id_flux);   
                  }
                  $flux_id_ab = implode(",", $fluAbo);
                  $clause_where = "and flux.id_flux in ($flux_id_ab)";
              }else{
                  $clause_where = "and flux.id_flux = 0";
              }
            }
          if($colonne == 'nom_payeur') {
              $flux_id_pay =  $this->push_model->getIdFluxPayeur($value);

              if (count($flux_id_pay)>0){
                  $fluPay = array();
                  foreach($flux_id_pay as $flux){
                          array_push($fluPay,$flux->id_flux);   
                  }
                  $flux_id_pa = implode(",", $fluPay);
                  $clause_where = "and flux.id_flux in ($flux_id_pa)";
              }else{
                  $clause_where = "and flux.id_flux = 0";
              }
          }
            return $clause_where;
        }
        
        //var_dump($clause_where);die;
    }

    public function whereInString($value,$colonne)
    {
      //print_r($value);die;
      $where = '';
      if(is_array($value) && count($value) > 0 ) {
        //convertir array string to array integer
        $where = implode("','", $value);
      }
      if($where != '') {
        $clause_where = "and ".$colonne." in ('".$where."')";
        return $clause_where;
      }
    }


    /*
      former les clauses where
    */
    function where($value, $colonne)
    {
      //print_r($value);die;
        if(is_array($value)) {
          //convertir array string to array integer
          $value = implode(",", $value);
        }
        if($value != '') {
          
          $clause_where = "and ".$colonne." in (".$value.") ";
          if($colonne=='flux.etat_pli_id') {
            $clause_where .=' and statut_lib::integer = 0';
          }
          
          return $clause_where;
        }
    }
    

    //visualisation abonnement
    function getAbonnement($idflux)
    {
      $societe = ""; 
      $fichier = ""; 
      $date_traitement = ""; 
      if($_REQUEST) {
          $societe = $_REQUEST['societe']; 
          $fichier = $_REQUEST['fichier']; 
          $date_traitement = $_REQUEST['dern_traitement']; 
      }
      $data_view = array(
        'abos' => $this->visu_model->getAbo($idflux),
        'type' => 1,
        'idflux' => $idflux,
        'societe' => $societe,
        'fichier' => $fichier,
        'date_traitement' => $date_traitement
      );
      //var_dump($data_view);
      echo $this->load->view('push/visu/list_abo', $data_view, TRUE);
     
    }
    //visualisation piece jointes
    function getJointes($idflux)
    {
      $data_view = array(
        'jointes' => $this->visu_model->getPieceJointe($idflux),
        'type' => 0
        
      );
      echo $this->load->view('push/visu/list_abo', $data_view, TRUE);
     
    }

    //visualiation des anomalies
    function getAnomalie($idflux){
      $data_view = array(
        'anomalie' => $this->visu_model->getAnomalie($idflux)
      );

      //$data_view = $this->visu_model->getAnomalie($idflux)
      echo $this->load->view('push/visu/anomalie', $data_view, TRUE);
    }


    public function download($fluxID)
    {
        
          $flux = $this->getFile($fluxID);
          $obj = $flux[0];
          $file = "";

      if($flux)
      {
        $file   = "";
        $source = $obj->id_source;

          if((int)$obj->id_flux_parent > 0 && (int)$obj->decoupe_par > 0 )
          {
              $this->load->library('decoupage/Lib_Sftp',null,'LibSftp');
              $this->LibSftp->downloadFluxPubliposte((int)$obj->id_flux);
          }
          else
          {
              if((int)$source == 1 )// mail
              {
                  $file = FCPATH.'SFTP_MAIL/mail/'.$obj->emplacement.'/'.$obj->nom_fichier;
                  $fileName = $obj->nom_fichier;

              }
              elseif((int)$source == 2)
              {
                  $emplacement = str_replace('/mnt/ged_bayard/','',$obj->emplacement);
                  echo $file = FCPATH.'SFTP_MAIL/'.$emplacement.'/'.$obj->nom_fichier;
                  $fileName = $obj->filename_origin;
              }

              if(file_exists($file) && strpos(site_url(), 'ged-server.madcom.local/GED/bayard_v2') !== FALSE)
              {

                  /*header('Content-Description: File Transfer');
                  header('Content-Type: application/octet-stream');
                  header('Content-Disposition: attachment; filename='.basename($fileName));
                  header('Content-Transfer-Encoding: binary');
                  header('Expires: 0');
                  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                  header('Pragma: public');
                  header('Content-Length: ' . filesize($file));
                  ob_clean();
                  flush();
                  readfile($file);*/

                  header('Content-Description: File Transfer');
                  header('Content-Type: application/octet-stream');
                  header('Content-Disposition: attachment; filename="' . basename($fileName).'"');
                  header('Content-Transfer-Encoding: binary');
                  header('Expires: 0');
                  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                  header('Pragma: public');
                  header('Content-Length: ' . filesize($file));
                  ob_clean();
                  flush();

                  readfile($file);
                  exit;
              }
              else
              {
                if(strpos(site_url(), 'ged-server.madcom.local/GED/bayard_v2') === FALSE){
                  $this->load->helper('download');
                  try {
                    $url_90 = 'http://ged-server.madcom.local/GED/bayard_v2/index.php/visualisation/visu_push/download/'.$fluxID;//TODO: changer en DNS
                    $ch_90 = curl_init($url_90);
                    curl_setopt($ch_90, CURLOPT_RETURNTRANSFER, true);
                    $data_file_ci = curl_exec($ch_90);
                    curl_close($ch_90);
                    force_download(basename($fileName), $data_file_ci);
                  } catch (Exception $ex) {
                    exit("<b>Erreur de la récupération du fichier</b><br/> <a href='".$_SERVER["HTTP_REFERER"]."'>Cliquer ici pour revenir à la page précedante</a>");
                  }
                }else{
                  exit("<b>Erreur de la récupération du fichier</b><br/> <a href='".$_SERVER["HTTP_REFERER"]."'>Cliquer ici pour revenir à la page précedante</a>");
                }
              }

          }


      }
    }
    public function getFile($fluxID)
    {
      $this->load->model("push/typage/Model_Push", "MPush");  
      return $this->MPush->getFile($fluxID);
       
    }
    
    public function input_ke($id_flux){
      if(is_numeric($id_flux)){
        $data_view = array(
          'id_flux' => $id_flux
          ,'info_ke' => $this->visu_model->info_ke($id_flux)
        );
        $this->load->helper('number');
        $reponse = $this->load->view('push/visu/input_ke', $data_view, TRUE);
      }else{
        $reponse = '<div class="modal-body" ><div class="alert alert-warning"><strong>Erreur!</strong> Pli introuvable.</div></div>';
        $reponse .= '<div class="modal-footer"><button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button></div>';
      }
      echo $reponse;
    }
    
	public function input_file_ke(){
		$config_upload = array(
			'upload_path' => './'.urlFileTemp
			,'overwrite' => FALSE
			,'file_ext_tolower' => TRUE
			,'encrypt_name' => TRUE
			,'allowed_types' => '*'
			,'max_size' => '512'
		);
		$this->load->library('upload', $config_upload);
		if($this->upload->do_upload('file')){
			echo json_encode(array(
				'code' => 1
				,'link' => $this->upload->data('file_name')
				,'ext' => $this->upload->data('file_ext')
				,'name' => $this->upload->data('orig_name')
			));
			$this->clear_old_file('./'.urlFileTemp);
		}else{
			echo json_encode(array(
				'code' => 0
				,'er' => $this->upload->display_errors()
			));
		}
  }
  
	private function clear_old_file($rep=NULL, $days=2){
		if(is_null($rep)){
			return '';
		}
		$dt_limit = new DateTime(date('Y-m-d'));
		$dt_limit->modify('-'.$days.' day');
		$this->load->helper('directory');
		$files = directory_map($rep, 1);
		foreach ($files as $file) {
			$dt_last_modif = filemtime($rep.$file);
			if($dt_limit->format('U') > $dt_last_modif && $file != 'index.html'){
				if(file_exists($rep.$file)){
					unlink($rep.$file);
				}
			}
		}
  }
  
	public function set_consigne_ke(){
		$id_flux = $this->input->post('id_flux');
		$consigne = $this->input->post('comms');
		$new_files = json_decode($this->input->post('files'));
		$deleted_files = json_decode($this->input->post('deleted_oid'));
		if(is_numeric($id_flux) && $consigne != ''){
			$data = new stdClass();
			$data->id_flux = $id_flux;
			$data->data_flux_ke = array(
				'id_flux' => $id_flux
				,'commentaire' => $consigne
			);
			$data->deleted_oids = $deleted_files;
			$this->load->helper('file');
			$data->fichier_ke = array();
			$links = array();
			foreach ($new_files as $key_new_file => $new_file) {
				$link = './'.urlFileTemp.$new_file->link;
				$data_new_file = array(
					'id_flux' => $id_flux
					,'nom_orig' => $new_file->orig_name
					,'fichier' => pg_escape_bytea(file_get_contents($link))
					,'taille' => filesize($link)
				);
				array_push($links, $link);
				array_push($data->fichier_ke, $data_new_file);
			}
			try {
				$this->visu_model->save_data_consigne_ke($data);
			} catch (Exception $th) {
				log_message('error', 'controller> visualisation> visu_push> set_consigne_ke: '.$th->getMessage());
				echo 'Erreur enregistrement.';
				return '';
			}
			echo 1;
			foreach ($links as $link) {
				if(!delete_files($link)){
					log_message('error', 'controller> visualisation> visu_push> set_consigne_ke: suppression impossible du fichier '.$link);
				}
			}
		}else{
			echo 'Données manquantes';
		}
  }
  
	public function download_file_ke($oid_file_ke){
		$this->load->helper('download');
		$this->load->helper('file');
		if (is_numeric($oid_file_ke)) {
			$fichier = $this->visu_model->fichier_ke($oid_file_ke);
			if(!is_null($fichier)){
				force_download($fichier->nom_orig, pg_unescape_bytea($fichier->fichier));
				return '';
			}
		}
		log_message('error', 'controller> visualisation> visu_push> download_file_ke: fichier introuvable! oid#'.$oid_file_ke);
		force_download('error.txt', 'Fichier introuvable');
  }
  
  //get order comumn
 function getOrder($order)
 {
   $col = array(
     'flux.id_flux',
     'flux.sous_dossier',
     'nom_fichier',
     'objet_flux',
     'from_flux',
     'fromname_flux',
     'societe.societe',
     'source.source', 
     'statut_pli.etape_lib',//etape
     'statut_pli.etat_lib',//etat
     'etat_pli.libelle_etat_pli',//statut
     'typologie.typologie',
     'dern_traitement::date',
     'entite',
     'nb_abonnement',
     'motif'
    );
    $colorder = $col[$order['column']];
    $dir = $order['dir'];
    $whereCOlumn = " order by   $colorder $dir";
    return $whereCOlumn;
 }
}
