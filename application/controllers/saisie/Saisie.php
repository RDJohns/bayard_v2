<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Saisie extends CI_Controller{

	private $clients_name;

	public function __construct(){
		parent::__construct();
		$this->load->helper('function1');
		$this->clients_name = NULL;
		if(!is_numeric($this->session->niveau_op)) {
			$this->load->model('admin/model_users');
			$this->model_users->reset_data_user();
		}
	}

    public function index(){
		$this->load->library('portier');
		$this->portier->must_op();
		$this->go();
		$this->load->library('run_script');
	}

	private function go(){
		$this->load->library('saisie/page');
		$this->page->set_titre('Saisie de pli');
		$this->page->add_links('Typage de pli', site_url('typage/typage'));
		$this->page->add_js('saisie/saisie');
		$this->page->add_css('saisie/saisie');
		$this->load->model('saisie/model_pli_saisie');
		$last_lot_saisie = $this->model_pli_saisie->last_lot_saisie_today($this->session->id_utilisateur);
		$this->session->is_rejet_batch = isset($this->session->is_rejet_batch) ? $this->session->is_rejet_batch : FALSE;
		if(is_null($last_lot_saisie)){
			$this->page->afficher($this->lot_saisie(FALSE, FALSE));
		}else{
			$this->session->current_id_lot_saisie = $last_lot_saisie->identifiant;
			$this->session->current_id_grp_pli_poss = $last_lot_saisie->id_group_pli_possible;
			$data_initial = array(
				'current_id_lot_saisie' => $last_lot_saisie->identifiant
				,'nb_pli_in_current_lot_saisie' => $this->model_pli_saisie->nb_pli_in_lot_saisie($last_lot_saisie->identifiant)
				,'rejet_batch' => $this->session->is_rejet_batch
				,'direct_load_pli' => $this->input->post_get('direct') == 1 ? TRUE : FALSE
				,'data_lot_saisie' => $last_lot_saisie
				,'force_new_lot' => FALSE
			);
			if($data_initial['nb_pli_in_current_lot_saisie'] >= 20){
				$data_initial['direct_load_pli'] = FALSE;
				$data_initial['force_new_lot'] = TRUE;
			}
			$this->page->afficher( $this->load->view('saisie/load_pli', $data_initial, TRUE) );
		}
		// $this->load->library('run_script');
	}
	
	public function lot_saisie($no_pli=FALSE, $have_lot_saisie=TRUE, $msg_erreur='', $ajax=FALSE){
		if(isset($_REQUEST['no_pli']) && isset($_REQUEST['have_lot_saisie'])){
			$ajax = TRUE;
			$no_pli = $this->input->post_get('no_pli') == 1;
			$have_lot_saisie = $this->input->post_get('have_lot_saisie') == 1;
		}
		$this->session->is_rejet_batch = isset($this->session->is_rejet_batch) ? $this->session->is_rejet_batch : FALSE;
		$this->load->model('saisie/model_pli_saisie');
		$prop_id_lot_saisie = $this->model_pli_saisie->id_lot_saisie_gen($this->session->id_utilisateur, $this->session->login);
		if(is_null($prop_id_lot_saisie)){
			$erreur = 'Erreur: Impossible de g&eacute;n&eacute;rer un id_lot_saisie valide!';
			if($ajax){
				echo $erreur;
				return '';
			}
			return $erreur;
		}
		$data_last_lot = $this->model_pli_saisie->last_lot_saisie($this->session->id_utilisateur);
		$current_id_soc = is_null($data_last_lot) ? '' : $data_last_lot->id_soc;
		$current_id_typologie = is_null($data_last_lot) ? '' : $data_last_lot->id_typologie;
		$current_id_gr_paiement = is_null($data_last_lot) ? '' : $data_last_lot->id_groupe_paiement;
		$data_initial = array(
			'list_grp_pli_pos_soc' => $this->model_pli_saisie->gr_pli_pos_for_user($this->session->id_utilisateur, 'soc')
			,'list_grp_pli_pos_typologie' => $this->model_pli_saisie->gr_pli_pos_for_user($this->session->id_utilisateur, 'typologie')
			,'list_grp_pli_pos_gr_payement' => $this->model_pli_saisie->gr_pli_pos_for_user($this->session->id_utilisateur, 'groupe_paiement')
			,'current_societe' => isset($this->session->current_spec_categorie_pli_societe) ? $this->session->current_spec_categorie_pli_societe : $current_id_soc
			,'current_typologie' => isset($this->session->current_spec_categorie_pli_typologie) ? $this->session->current_spec_categorie_pli_typologie : $current_id_typologie
			,'current_gr_payement' => isset($this->session->current_spec_categorie_pli_gr_paiement) ? $this->session->current_spec_categorie_pli_gr_paiement : $current_id_gr_paiement
			,'id_lot_saisie' => $prop_id_lot_saisie
			,'no_pli' => $no_pli
			,'have_lot_saisie' => $have_lot_saisie
			,'msg_erreur' => $msg_erreur
			,'rejet_batch' => $this->session->is_rejet_batch
			,'direct_load_pli' => $this->input->post_get('direct') == 1 ? TRUE : FALSE
		);
		$this->session->new_id_lot_saisie = $data_initial['id_lot_saisie'];
		if($ajax) {
			echo $this->load->view('saisie/lot', $data_initial, TRUE);
		} else {
			return $this->load->view('saisie/lot', $data_initial, TRUE);
		}
	}
    
    public function set_lot_saisie_pli(){
        $this->load->library('portier');
		$this->portier->auto_inspecter_v();
		$this->portier->must_op();
		$this->session->is_rejet_batch = isset($_REQUEST['rejet_batch']) ? ($this->input->post_get('rejet_batch') == 1 ? TRUE : FALSE) : FALSE;
		$this->load->model('saisie/model_pli_saisie');
		$last_lot_saisie = $this->model_pli_saisie->last_lot_saisie_today($this->session->id_utilisateur);
		if(isset($_REQUEST['id_societe']) && isset($_REQUEST['id_typologie']) && isset($_REQUEST['id_gr_paiement'])){
			$this->session->current_spec_categorie_pli_societe = $spec_categorie_pli_societe = $this->input->post_get('id_societe');
			$this->session->current_spec_categorie_pli_typologie = $spec_categorie_pli_typologie = $this->input->post_get('id_typologie');
			$this->session->current_spec_categorie_pli_gr_paiement = $spec_categorie_pli_gr_paiement = $this->input->post_get('id_gr_paiement');
			$this->session->current_id_grp_pli_poss = $code_groupe_poss = $spec_categorie_pli_typologie.'-'.$spec_categorie_pli_gr_paiement.'-'.$spec_categorie_pli_societe;
			$pli = $this->model_pli_saisie->piocher_pli_for_saisie($this->session->id_utilisateur, $code_groupe_poss, $this->session->is_rejet_batch);
			if(is_null($pli)){
				echo $this->lot_saisie(TRUE, !is_null($last_lot_saisie));
			}else{
				$this->session->current_id_lot_saisie = $this->session->new_id_lot_saisie;
				$this->saisie_pli($pli);
			}
		}else {
			echo $this->lot_saisie(FALSE, !is_null($last_lot_saisie), 'Erreu: Param&egrave;tres manquants!');
		}
	}
	
	public function get_pli(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v();
		$this->portier->must_op();
		$this->session->is_rejet_batch = isset($_REQUEST['rejet_batch']) ? ($this->input->post_get('rejet_batch') == 1 ? TRUE : FALSE) : FALSE;
		$this->load->model('saisie/model_pli_saisie');
		$pli = $this->model_pli_saisie->piocher_pli_for_saisie($this->session->id_utilisateur, $this->session->current_id_grp_pli_poss, $this->session->is_rejet_batch);
		if (is_null($pli)) {
			echo $this->lot_saisie(TRUE);
		} else {
			//saisie pli dans lot existant $this->session->current_id_lot_saisie
			$this->saisie_pli($pli);
		}
	}

	private function saisie_pli($pli){
		$this->load->model('saisie/model_pli_saisie');
		$data_pli = $this->model_pli_saisie->data_pli($pli->id_pli);
		$id_docs = $this->model_pli_saisie->id_documents($pli->id_pli);
		if(is_null($data_pli)){
			echo('Erreur: Les donn&eacute;es du pli sont introuvables!');
			return '';
		}
		$this->load->library('saisie/pre_data');
		$this->pre_data->set_predata($pli->id_pli);
		$view_mvmnt = '';
		$data_mvmnt = $this->model_pli_saisie->mvmnt_of_pli($pli->id_pli);
		$data_titre = $this->model_pli_saisie->titre($data_pli->societe, TRUE);
		$clients_num = array();
		$data_view_mvmnt = array();
		foreach ($data_mvmnt as $k_mvmnt => $mvmnt) {
			if (isset($this->pre_data->mvmnts[$k_mvmnt])) {
				$pre_mvmvnt = $this->pre_data->mvmnts[$k_mvmnt];
				if(!is_numeric(trim($mvmnt->titre))){
					$mvmnt->titre = $pre_mvmvnt->is_with_id_titre ? $pre_mvmvnt->id_titre : $mvmnt->titre;
				}
				if(strlen(trim($mvmnt->code_promo)) < 3){
					$mvmnt->code_promo = $pre_mvmvnt->is_with_code_promo ? $pre_mvmvnt->src->code_promo : '';
				}
				if(!is_numeric(trim($mvmnt->numero_abonne))){
					$mvmnt->numero_abonne = $pre_mvmvnt->is_with_num_ab ? $pre_mvmvnt->src->abonne_destinataire : '';
					$mvmnt->nom_abonne = isset($mvmnt->nom_abonne) ? $mvmnt->nom_abonne : '';
					$mvmnt->nom_abonne = $pre_mvmvnt->is_with_num_ab ? '' : $mvmnt->nom_abonne;
				}
				if(!is_numeric(trim($mvmnt->numero_payeur))){
					$mvmnt->numero_payeur = $pre_mvmvnt->is_with_num_payeur ? $pre_mvmvnt->src->abonne_payeur : '';
					$mvmnt->nom_payeur = !$pre_mvmvnt->is_with_num_payeur && $pre_mvmvnt->is_with_nom_payeur ? $pre_mvmvnt->src->nom : '';
				}
			}
			array_push($data_view_mvmnt, array(
				'id_soc' => $data_pli->societe
				,'ajax' => FALSE
				,'data_mvmnt' => $mvmnt
				,'data_titre' => $data_titre
				,'data_pli' => $data_pli
			));
			if(is_numeric($mvmnt->numero_abonne) && $mvmnt->numero_abonne > 0){
				array_push($clients_num, $mvmnt->numero_abonne);
			}
			if(is_numeric($mvmnt->numero_payeur) && $mvmnt->numero_payeur > 0){
				array_push($clients_num, $mvmnt->numero_payeur);
			}
		}
		if(count($data_mvmnt) < count($this->pre_data->mvmnts)){
			$nb_mvmnt = count($data_mvmnt);
			for ($i=$nb_mvmnt; $i < count($this->pre_data->mvmnts); $i++) {
				$mvmnt = array('id_soc'=>$data_pli->societe, 'ajax'=>FALSE,'data_titre' => $data_titre,'data_pli' => $data_pli);
				if(isset($this->pre_data->mvmnts[$i])){
					$mvmnt['pre_data_mvmnt'] = array();
					$pre_mvmvnt = $this->pre_data->mvmnts[$i];
					if($pre_mvmvnt->is_with_id_titre){$mvmnt['pre_data_mvmnt']['ttr'] = $pre_mvmvnt->id_titre;}
					if($pre_mvmvnt->is_with_code_promo){$mvmnt['pre_data_mvmnt']['code_promo'] = $pre_mvmvnt->src->code_promo;}
					if($pre_mvmvnt->is_with_num_payeur){
						$mvmnt['pre_data_mvmnt']['num_payeur'] = $pre_mvmvnt->src->abonne_payeur;
						array_push($clients_num, $pre_mvmvnt->src->abonne_payeur);
					}
					if($pre_mvmvnt->is_with_nom_payeur && !$pre_mvmvnt->is_with_num_payeur){$mvmnt['pre_data_mvmnt']['nom_payeur'] = $pre_mvmvnt->src->nom;}
					if($pre_mvmvnt->is_with_num_ab){
						$mvmnt['pre_data_mvmnt']['num_abonne'] = $pre_mvmvnt->src->abonne_destinataire;
						array_push($clients_num, $pre_mvmvnt->src->abonne_destinataire);
					}
				}
				array_push($data_view_mvmnt, $mvmnt);
			}
		}
		$this->clients_name = array();
		if(count($clients_num) > 0){
			$this->load->model('saisie/model_cba');
			$this->clients_name = $this->model_cba->get_clients_name($clients_num);
		}
		foreach ($data_view_mvmnt as $key => $value) {
			$view_mvmnt .= $this->get_mvmnt_view($value);
		}
		if(count($data_view_mvmnt) == 0){
			$view_mvmnt = $this->get_mvmnt_view(array('id_soc'=>$data_pli->societe, 'ajax'=>FALSE,'data_titre' => $data_titre,'data_pli' => $data_pli));
		}
		$view_cheque = '';
		$data_cheque = $this->model_pli_saisie->cheques($pli->id_pli);
		$anom_chqs = $this->model_pli_saisie->anom_cheque();
		foreach ($data_cheque as $k_chq => $cheque) {
			if(isset($this->pre_data->chqs[$k_chq])){
				if(strlen(trim($cheque->nom_client)) < 1) {
					$cheque->nom_client = $this->pre_data->chqs[$k_chq]->is_with_nom_client ? $this->pre_data->chqs[$k_chq]->src->n_client : $cheque->nom_client;
				}
			}
			$view_cheque .= $this->get_cheque_view(array(
				'id_pli' => $pli->id_pli
				, 'ajax' => FALSE
				, 'data_chq' => $cheque
				, 'id_docs' => $id_docs
				, 'anom_chqs' => $anom_chqs
			));
		}
		$view_cheque_kd = '';
		$data_cheque_kd = $this->model_pli_saisie->cheques_kd($pli->id_pli);
		$type_chqs_kd = $this->model_pli_saisie->type_chq_kd();
		foreach ($data_cheque_kd as $cheque_kd) {
			$view_cheque_kd .= $this->get_cheque_kd_view(array(
				'id_pli'=>$pli->id_pli, 'chq_kd'=>$cheque_kd
				, 'ajax'=>FALSE
				, 'id_docs' => $id_docs
				, 'type_chqs_kd' => $type_chqs_kd
			));
		}
		$motif_consignes = $this->model_pli_saisie->get_all_motif_consigne($pli->id_pli);
		$nb_motif_consign = count($motif_consignes);
		$data_view_pli = array(
			'pli' => $pli
			,'id_grp_pli_poss' => $this->session->current_id_grp_pli_poss
			,'data_pli' => $data_pli
			,'id_lot_saisie' => $this->session->current_id_lot_saisie
			,'docs' => $id_docs
			,'socs' => $this->model_pli_saisie->societe()
			,'titres' => $this->model_pli_saisie->titre($data_pli->societe)
			,'typos' => $this->model_pli_saisie->typologie($data_pli->societe)
			,'mode_paiements' => $this->model_pli_saisie->mode_paiement()
			,'view_mvmnts' => $view_mvmnt
			,'statut_saisies' => $this->model_pli_saisie->statut_saisie($data_pli)
			,'ko_motifs' => $this->model_pli_saisie->motif_ko()
			,'circulaires' => $this->model_pli_saisie->liste_circulaires()
			,'paiements' => $this->model_pli_saisie->my_paiement($pli->id_pli)
			,'montant_esp' => $this->model_pli_saisie->paie_esp($pli->id_pli)
			,'view_cheques' => $view_cheque
			,'view_cheques_kd' => $view_cheque_kd
			,'motifs_rejet' => $this->model_pli_saisie->motif_rejet()
			,'motif_consigne' => $nb_motif_consign > 0 ? $motif_consignes[($nb_motif_consign-1)] : NULL
			,'motif_consignes' => $motif_consignes
			,'nb_motif_consign' => $nb_motif_consign
			,'ids_anom_chq_bloquant' => $this->model_pli_saisie->anom_chq_bloquant()
			,'niveau_op' => $this->session->niveau_op
		);
		echo $this->load->view('saisie/pli', $data_view_pli, TRUE);
	}

	public function view_doc($id_doc=NULL){
		if(is_numeric($id_doc)){
			$this->load->model('saisie/model_pli_saisie');
			$data_doc = $this->model_pli_saisie->document($id_doc);
			if (count($data_doc) > 0) {
				$empty_b64 = base64_encode(file_get_contents(base_url('assets/images/empty_papper.jpg')));
				$data_view_doc = array(
					'doc' => $data_doc[0]
					,'doc_b64_recto' => trim($data_doc[0]->n_ima_base64_recto) == '' ? $empty_b64 : trim($data_doc[0]->n_ima_base64_recto)
					,'doc_b64_verso' => trim($data_doc[0]->n_ima_base64_verso) == '' ? $empty_b64 : trim($data_doc[0]->n_ima_base64_verso)
				);
				echo $this->load->view('saisie/document', $data_view_doc, TRUE);
				return '';
			}
		}
		echo 'Document #'.$id_doc.' introuvable!';
	}

	public function option_titres($id_soc=NULL, $titre_i=NULL){
		$options = '';
		$this->load->model('saisie/model_pli_saisie');
		$titre_i = $titre_i == '1' ? TRUE : FALSE;
		$data_titre = $this->model_pli_saisie->titre($id_soc, $titre_i);
		foreach ($data_titre as $n => $titre) {
			$selected = $n == 0 ? 'selected' : '';
			$options .= '<option value="'.$titre->id.'" title="'.$titre->code.'" data-subtext=" - '.$titre->code.'" '.$selected.' >'.$titre->titre.'</option>';
		}
		echo $options;
	}

	public function option_typologie($id_soc=1){
		$options = '';
		$this->load->model('saisie/model_pli_saisie');
		$data_typo = $this->model_pli_saisie->typologie($id_soc);
		foreach ($data_typo as $n => $typo) {
			$selected = $n == 0 ? 'selected' : '';
			$options .= '<option value="'.$typo->id.'" data-subtext=" - '.$typo->sous_lot.'" '.$selected.' >'.$typo->typologie.'</option>';
		}
		echo $options;
	}

	public function get_cheque_view($args=array()){
		if(is_numeric($args)){
			$args = array('id_pli'=>$args, 'ajax'=>TRUE);
		}
		$args = is_array($args) ? $args : array();
		if(!isset($args['id_pli']) || !is_numeric($args['id_pli'])){
			log_message('error', 'Saisie> Saisie> get_cheque_view => id_pli non numeric');
			echo '';
			return '';
		}
		$id_pli = $args['id_pli'];
		$this->load->model('saisie/model_pli_saisie');
		$data_cheque = array(
			'anomalies' => isset($args['anom_chqs']) ? $args['anom_chqs'] : $this->model_pli_saisie->anom_cheque()
			,'docs' => isset($args['id_docs']) ? $args['id_docs'] : $this->model_pli_saisie->id_documents($id_pli)
			,'paiement_anomali' => array()
			,'id_etat_chq' => 2
			,'etat_modif' => 0
			,'cmc7' => isset($args['data_chq']) ? $args['data_chq']->cmc7 : ''
			,'rlmc' => isset($args['data_chq']) ? $args['data_chq']->rlmc : ''
			,'montant' => isset($args['data_chq']) ? $args['data_chq']->montant : ''
			,'nom_client' => isset($args['data_chq']) ? $args['data_chq']->nom_client : ''
			,'etranger' => isset($args['data_chq']) ? $args['data_chq']->etranger : 0
			,'identifiant' => isset($args['data_chq']) && strlen($args['data_chq']->ui_id) > 8 ? $args['data_chq']->ui_id : NULL
			,'id_doc' => isset($args['data_chq']) ? $args['data_chq']->id_doc : ''
			,'id_doc_lock' => FALSE
			,'tab_id_anom_corrige' => array()
		);
		if(is_numeric($data_cheque['id_doc'])){
			$id_doc = $data_cheque['id_doc'];
			$data_cheque['paiement_anomali'] = $this->model_pli_saisie->paiement_anomali($id_doc);
			$data_cheque['id_etat_chq'] = $args['data_chq']->id_etat_chq;
			$data_cheque['etat_modif'] = $args['data_chq']->etat_modif;
			$data_cheque['id_doc_lock'] = TRUE;
			$data_cheque['anom_corrige'] = $this->model_pli_saisie->paiement_chq_ano_corrige($id_doc);
			foreach ($data_cheque['anom_corrige'] as $ano_c) {
				array_push($data_cheque['tab_id_anom_corrige'], $ano_c->id_anomalie_chq);
			}
		}
		if(!isset($args['ajax']) || (is_bool($args['ajax']) && $args['ajax'] === TRUE)){
			echo $this->load->view('saisie/cheque', $data_cheque, TRUE);
		}else {
			return $this->load->view('saisie/cheque', $data_cheque, TRUE);
		}
	}

	public function get_cheque_kd_view($args=array()){
		if(is_numeric($args)){
			$args = array('id_pli'=>$args, 'ajax'=>TRUE);
		}
		$args = is_array($args) ? $args : array();
		if(!isset($args['id_pli']) || !is_numeric($args['id_pli'])){
			log_message('error', 'saisie> Saisie> get_cheque_kd_view => id_pli non numeric');
			echo '';
			return '';
		}
		$id_pli = $args['id_pli'];
		$this->load->model('saisie/model_pli_saisie');
		$data_cheque = array(
			'choix_type_chq_kds' => isset($args['type_chqs_kd']) ? $args['type_chqs_kd'] : $this->model_pli_saisie->type_chq_kd()
			,'docs' =>  isset($args['id_docs']) ? $args['id_docs'] : $this->model_pli_saisie->id_documents($id_pli)
			,'montant' => isset($args['chq_kd']) ? $args['chq_kd']->montant : ''
			,'type_chq_kd' => isset($args['chq_kd']) ? $args['chq_kd']->type_chq_kdo : ''
			,'code_barre' => isset($args['chq_kd']) ? $args['chq_kd']->code_barre : ''
			,'id_doc' => isset($args['chq_kd']) ? $args['chq_kd']->id_doc : ''
			,'identifiant' => isset($args['chq_kd']) && strlen($args['chq_kd']->ui_id) > 8 ? $args['chq_kd']->ui_id : NULL
		);
		if(!isset($args['ajax']) || (is_bool($args['ajax']) && $args['ajax'] === TRUE)){
			echo $this->load->view('saisie/cheque_cadeau', $data_cheque, TRUE);
		}else {
			return $this->load->view('saisie/cheque_cadeau', $data_cheque, TRUE);
		}
    }
	
	public function get_mvmnt_view($args=array()){
		if(!is_array($args)){
			$id_soc = is_numeric($args) /*&& in_array($args, array(1, 2))*/ ? $args : 1;
			$args=array('id_soc' => $id_soc);
		}
		$this->load->model('saisie/model_pli_saisie');
		$data_mvmnt = array(
			'titres' => isset($args['data_titre']) ? $args['data_titre'] : $this->model_pli_saisie->titre((isset($args['id_soc']) ? $args['id_soc'] : 1), TRUE)
			,'ttr' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->titre : 1
			,'code_promo' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->code_promo : ''
			,'num_abonne' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->numero_abonne : ''
			,'nom_abonne' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->nom_abonne : ''
			//,'cp_abonne' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->code_postal_abonne : ''
			,'num_payeur' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->numero_payeur : ''
			,'nom_payeur' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->nom_payeur : ''
			//,'cp_payeur' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->code_postal_payeur : ''
			,'identifiant' => isset($args['data_mvmnt']) && strlen($args['data_mvmnt']->ui_id) > 8 ? $args['data_mvmnt']->ui_id : NULL
		);
		if(isset($args['pre_data_mvmnt']) && is_array($args['pre_data_mvmnt'])){
			foreach ($args['pre_data_mvmnt'] as $key_pdm => $pdm) {
				$data_mvmnt[$key_pdm] = $pdm;
			}
		}
		$data_mvmnt['nom_abonne'] = $this->nom_client($data_mvmnt['num_abonne'], $data_mvmnt['nom_abonne']);
		$data_mvmnt['nom_payeur'] = $this->nom_client($data_mvmnt['num_payeur'], $data_mvmnt['nom_payeur']);
		$this->load->model('saisie/model_cba');
		$data_mvmnt['is_batch'] = isset($args['data_pli']) && $args['data_pli']->flag_batch == 0 ? FALSE : TRUE;
		$data_mvmnt['code_promos'] = array();
		if(isset($args['data_pli']) && $args['data_pli']->flag_batch == 1) {
			$data_mvmnt['code_promos'] = $this->model_cba->get_promo($data_mvmnt['code_promo']);
		}
		$ajax = isset($args['ajax']) ? $args['ajax'] : TRUE;
		if($ajax){
			echo $this->load->view('saisie/mouvement', $data_mvmnt, TRUE);
		}else {
			return $this->load->view('saisie/mouvement', $data_mvmnt, TRUE);
		}
	}

	private function nom_client($num, $nom='')
	{
		$num = trim($num);
		$nom = trim($nom.'');
		if(is_numeric($num) && $num > 0 && $nom == '' && !is_null($this->clients_name)){
			foreach ($this->clients_name as $key => $name) {
				if(($name->ctm_nbr*1) == ($num*1)){
					return $name->atn_end;
				}
			}
		}
		return $nom;
	}
	
	private function copy_ci()
	{
		$id_pli = $this->input->post_get('id_pli');
		$temp_file_circulaire = $this->input->post_get('fichier_circ');
		$fichier_circulaire = '';
		if($temp_file_circulaire != ''){
			$ext = $this->input->post_get('ext_fichier_circ');
			$temp_file_circulaire = './'.upTmpFileCirc.$temp_file_circulaire;
			$new_name = $id_pli.$ext;
			if(rename($temp_file_circulaire, './'.upFileCirc.$new_name)){
				$fichier_circulaire = $new_name;
				if(file_exists($temp_file_circulaire)){
					unlink($temp_file_circulaire);
				}
			}else {
				echo 'Impossible d\'enregistrer le fichier de circulaire!';
				log_message('error', 'Enregistrement saisie> erreur copy fichier circulaire <'.$temp_file_circulaire.'> to <'.upFileCirc.$new_name.'>, op#'.$this->session->id_utilisateur.', pli#'.$id_pli);
				return array('code'=>FALSE, 'fichier_circulaire'=>'');
			}
		}
		return array('code'=>TRUE, 'fichier_circulaire'=>$fichier_circulaire);
	}

	public function save_saisie(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_op();
		$this->load->model('saisie/model_pli_saisie');
		if(isset($_REQUEST['id_pli'])){
			$statut = $this->input->post_get('statut');
			$data_saisie = $this->get_data_saisie();
			if(!in_array($data_saisie->etat_pli_by_chq, array(1, 11))){
				$motif_ko = $this->input->post_get('autre_motif_ko');
				$this->ko_save($data_saisie, ($motif_ko == '' ? '' : $motif_ko.'; ').$data_saisie->motif_ko_by_chq);
				return '';
			}
			$saisie_batch = is_numeric($this->input->post_get('saisie_batch')) && $this->input->post_get('saisie_batch') == 1 ? TRUE : FALSE;
			if($saisie_batch && !$data_saisie->ok_data_batch){
				echo $data_saisie->error_data_batch;
				return '';
			}
			$id_pli = $this->input->post_get('id_pli');
			$id_lot_saisie = $this->input->post_get('id_lot_saisie');
			$paiements = json_decode($this->input->post_get('paiements'));
			$action_fichier_ci = $this->copy_ci();
			if($action_fichier_ci['code']){
				$fichier_circulaire = $action_fichier_ci['fichier_circulaire'];
			}else{
				return '';
			}
			$data_pli = array(
				'societe' => $this->input->post_get('id_soc')
				,'infos_datamatrix' => $this->input->post_get('datamatrix')
				,'titre' => $this->input->post_get('titre')
				,'code_promotion' => $this->input->post_get('code_promo')
				,'statut_saisie' => $statut
				,'motif_ko' => $this->input->post_get('motif_ko')
				,'autre_motif_ko' => $this->input->post_get('autre_motif_ko')
				,'nom_circulaire' => $this->input->post_get('nom_circ') == '' ? NULL : $this->input->post_get('nom_circ')
				,'fichier_circulaire' => $fichier_circulaire
				,'nom_orig_fichier_circulaire' => $this->input->post_get('nom_fichier_circ')
				,'message_indechiffrable' => $this->input->post_get('msg_ind')
				,'dmd_kdo_fidelite_prim_suppl' => $this->input->post_get('dmd_kdo')
				,'dmd_envoi_kdo_adrss_diff' => $this->input->post_get('dmd_env_kdo')
				//,'type_coupon' => $this->input->post_get('type_coupon')
				,'nom_deleg' => $this->input->post_get('nom_deleg')
				,'code_ecole_gci' => $this->input->post_get('code_ecole_gci')
				,'flag_saisie' => 1
				,'pli_nouveau' => 0
				,'par_ttmt_ko' => 0
				,'date_dernier_statut_ko' => date('Y-m-d H:i:s')
			);
			$flag_traitement = $saisie_batch ? 6 : 5;
			/*if (in_array($this->input->post_get('typo'), array(9,27))) {
				$flag_traitement = 14;
			}*/
			$action_saisie = $saisie_batch ? ($statut == 1 ? 91 : 123) : ($statut == 1 ? 6 : 122);
			$pli = array(
				'flag_traitement' => $flag_traitement
				,'idefix' => $this->input->post_get('idefix')
				,'typologie' => $this->input->post_get('typo')
				,'id_lot_saisie' => $id_lot_saisie
				,'lot_saisie_bis' => 0
				,'sorti_saisie' => $statut
				,'statut_saisie' => $statut
				,'societe' => $data_pli['societe']
			);
			$data_pli['typologie'] = $this->input->post_get('typo');
			$data_pli['flag_traitement'] = $flag_traitement;
			$id_group_pli_possible = $this->input->post_get('id_group_pli_possible');
			$his_grp_pli_poss = $pli['typologie'].'-'.$this->model_pli_saisie->id_grp_paie_for_paiement($paiements).'-'.$data_pli['societe'];
			$bis = FALSE;
			if($id_group_pli_possible != $his_grp_pli_poss){
				$pli['lot_saisie_bis'] = 1;
				$bis = TRUE;
			}
			$data_pli['mvt_nbr'] = count($data_saisie->mvmnts);
			$data_pli['avec_chq'] = count($data_saisie->cheques) > 0 ? 1 : 0;
			$data_saisie->id_lot_saisie = $id_lot_saisie;
			$data_saisie->id_group_pli_possible = $id_group_pli_possible;
			$data_saisie->pli = $pli;
			$data_saisie->data_pli = $data_pli;
			$data_saisie->action_saisie = $action_saisie;
			$data_saisie->saisie_batch = FALSE;
			$data_saisie->statut = $statut;
			$choix_motif = $data_pli['motif_ko'] == 4 ? '' : $this->model_pli_saisie->get_label_motif_ko($data_pli['motif_ko']);
			$data_saisie->link_motif_consigne = $id_pli.date('YmdHis');
			$data_saisie->motif_consigne = array(
				'id_pli' => $id_pli
				,'motif_operateur' => $data_pli['autre_motif_ko'] == '' ? $choix_motif : $choix_motif.$data_pli['autre_motif_ko']
				,'id_flg_trtmnt' => $flag_traitement
				,'id_stt_ss' => $statut
				,'link_histo' => $data_saisie->link_motif_consigne
			);
			if(in_array($statut, array(1))) {
				$data_saisie->motif_consigne = NULL;
				$data_pli['date_dernier_statut_ko'] = NULL;
				$data_pli['motif_ko'] = NULL;
				$data_pli['autre_motif_ko'] = NULL;
				$data_saisie->link_motif_consigne = '';
			}
			$data_saisie->data_pli = $data_pli;
			if($saisie_batch && in_array($statut, array(1))){
				$this->load->library('saisie/data_cba');
				$this->data_cba->set_data($data_saisie);
				if(!$this->data_cba->can_to_batch){
					echo $this->data_cba->error_msg;
					return '';
				}
				$data_saisie->data_pli['dt_batch'] = date('Y-m-d');
			}
			$this->load->library('saisie/data_saisi_ctrl');
			$this->data_saisi_ctrl->saisie_values($data_saisie);
			// echo var_dump($data_saisie);die();exit();
			if(preg_match("#^\d+$#", $statut) && in_array($statut, array(1, 11)) && (preg_match("#\d+#", $this->input->post_get('dmd_env_kdo')))) {
				$rep_save = $this->model_pli_saisie->save_sasie($data_saisie);
			}else{
				echo 'Données non-conformes';
				log_message('error', 'controllers>saisie>Saisie>save_saisie data non-conforme op#'.$this->session->id_utilisateur.', pli#'.$id_pli.', statut_saisie:'.$statut);
				return '';
			}
			if($rep_save[0]){
				if($bis){
					echo '2';
				}else{
					echo '1';
				}
				// if($saisie_batch){
				// 	$this->data_cba->to_cba();
				// }
			}else{
				echo $rep_save[1];
				log_message('error', 'Enregistrement saisie échoué:'.$rep_save[1].', pli#'.$id_pli.', op#'.$this->session->id_utilisateur);
			}
		}else{
			echo 'Paramètre manquant';
			log_message('error', 'Enregistrement saisie> parametre manquant op#'.$this->session->id_utilisateur);
		}
	}

	public function cancel_saisie($id_pli=NULL){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_op();
		if(is_numeric($id_pli)){
			$this->load->model('saisie/model_pli_saisie');
			if($this->model_pli_saisie->unlocked_saisie($id_pli)){
				return '';
			}
		}else {
			log_message('error', 'saisie> saisie> cancel_saisie> id_pli non numerique: '.$id_pli);
		}
	}

	public function save_file_circulaire(){
		$config_upload = array(
			'upload_path' => './'.upTmpFileCirc
			,'overwrite' => FALSE
			,'file_ext_tolower' => TRUE
			,'encrypt_name' => TRUE
			,'allowed_types' => '*'
		);
		$this->load->library('upload', $config_upload);
		if($this->upload->do_upload('fichier_circulaire')){
			echo json_encode(array(
				'code' => 1
				,'link' => $this->upload->data('file_name')
				,'ext' => $this->upload->data('file_ext')
				,'name' => $this->upload->data('orig_name')
			));
			$this->clear_old_file('./'.upTmpFileCirc);
		}else{
			echo json_encode(array(
				'code' => 0
				,'er' => $this->upload->display_errors()
			));
		}
	}

	private function clear_old_file($rep=NULL, $days=2){
		if(is_null($rep)){
			return '';
		}
		$dt_limit = new DateTime(date('Y-m-d'));
		$dt_limit->modify('-'.$days.' day');
		$this->load->helper('directory');
		$files = directory_map($rep, 1);
		foreach ($files as $file) {
			$dt_last_modif = filemtime($rep.$file);
			if($dt_limit->format('U') > $dt_last_modif && $file != 'read_me.txt'){
				if(file_exists($rep.$file)){
					unlink($rep.$file);
				}
			}
		}
	}

	public function list_lot_saisies(){
		$correspondance_field = array('dt', 'identifiant', 'soc', 'typologie', 'groupe_paiement', 'nbplis', 'plis', 'nbplis_bis', 'plis_bis', 'total');
		$resultat = array(
			'draw' => 0
			,'iTotalRecords' => 0
			,'iTotalDisplayRecords' => 0
		);
		$data = array();
		if(isset($_REQUEST['draw'])){
			$this->load->model('saisie/model_visu');
			$resultat["draw"] = $this->input->post_get('draw');
            $offset = $this->input->post_get('start');
            $limit = $this->input->post_get('length');
            $search = $this->input->post_get('search');
            $arg_find = strtolower($search['value']);
            $order = $this->input->post_get('order');
            $arg_ordo = $correspondance_field[$order[0]['column']];
            $sens = $order[0]['dir'];
			$param_plus = array();
			//$param_plus['dt'] = $this->input->post_get('types');
			$resultat["iTotalRecords"] = $this->model_visu->my_lot_total();
			$resultat["iTotalDisplayRecords"] = $this->model_visu->lot_saisie($limit, $offset, $arg_find, $arg_ordo, $sens, $param_plus);
			$lots = $this->model_visu->lot_saisie($limit, $offset, $arg_find, $arg_ordo, $sens, $param_plus, FALSE);
			foreach ($lots as $n => $lot) {
				$dt = $lot->dt_creat == '' ? '' : (new DateTime($lot->dt_creat))->format('d/m/Y H:i');
				$row = array(
					'dt' => $dt
					,'id' => $lot->id_lot
					,'soc' => $lot->soc
					,'typo' => $lot->typo
					,'paie' => $lot->paie
					,'nb' => $lot->nbplis
					,'pli' => $lot->plis
					,'nb_b' => $lot->nbplis_bis
					,'pli_b' => $lot->plis_bis
					,'tt' => $lot->total
				);
				array_push($data, $row);
			}
		}
		$resultat["aaData"] = $data;
        echo json_encode($resultat);
	}

	public function list_groupement_pli(){
		$this->load->library('run_script');
		$this->load->model('saisie/model_visu');
		$this->load->model('saisie/model_pli_saisie');
		$my_data_poss = $this->model_pli_saisie->gr_pli_pos_for_user($this->session->id_utilisateur);
		$my_poss = array();
		foreach ($my_data_poss as $poss) {
			array_push($my_poss, $poss->id_groupe_pli_possible);
		}
		$data_view = array(
			'grps' => $this->model_visu->group_pli1()
			,'my_poss' => $my_poss
			,'totaux_wait' => 0
			,'totaux_current' => 0
			,'totaux' => 0
			,'only_nv1' => $this->session->id_type_utilisateur == 2 && $this->session->niveau_op == 1
		);
		echo $this->load->view('saisie/groupement', $data_view, TRUE);
	}

	public function list_groupement_pli_by_older(){
		$this->load->library('run_script');
		$this->load->model('saisie/model_visu');
		$this->load->model('saisie/model_pli_saisie');
		$my_data_poss = $this->model_pli_saisie->gr_pli_pos_for_user($this->session->id_utilisateur);
		$my_poss = array();
		foreach ($my_data_poss as $poss) {
			array_push($my_poss, $poss->id_groupe_pli_possible);
		}
		$data_view = array(
			'grps' => $this->model_visu->group_pli1_by_older()
			,'my_poss' => $my_poss
			,'totaux_wait' => 0
			,'only_nv1' => $this->session->id_type_utilisateur == 2 && $this->session->niveau_op == 1
		);
		echo $this->load->view('saisie/groupement_by_older', $data_view, TRUE);
	}
/*
	public function mise_en_ko_scan(){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_op();
		if(isset($_REQUEST['id_pli']) && isset($_REQUEST['motif'])){
			$id_pli = $this->input->post_get('id_pli');
			if(is_numeric($id_pli) && is_numeric($this->input->post_get('motif'))) {
				$data = array(
					'motif_rejet_id' => (int)$this->input->post_get('motif')
					,'utilisateurs_id' => (int)$this->session->id_utilisateur
					,'pli_id' => (int)$id_pli
					,'description' => $this->input->post_get('comment')
				);
				$this->load->model('saisie/model_pli_saisie');
				echo $this->model_pli_saisie->ko_scan($id_pli, $data) ? 1 : 'Le pli ne vous est plus assigné!';
				return '';
			}
			log_message('error', 'saisie> saisie> mise_en_ko_scan> argument non-conforme pli#'.$id_pli.', motif: '.$this->input->post_get('motif'));
			echo 'parametres non-conforme!';
			return '';
		}
		log_message('error', 'saisie> saisie> mise_en_ko_scan> id_pli non numerique: ');
		echo 'Pli introuvable!';
	}

	public function mise_en_hp($id_pli){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_op();
		if (is_numeric($id_pli)) {
			$this->load->model('saisie/model_pli_saisie');
			echo $this->model_pli_saisie->hors_perim($id_pli) ? 1 : 'Le pli ne vous est plus assigné!';
			return '';
		}
		log_message('error', 'saisie> saisie> mise_en_hp> id_pli non numerique: '.$id_pli);
		echo 'Pli introuvable!';
	}*/

	public function redirecte_saisie_advantage($id_pli){
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_op();
		if (is_numeric($id_pli)) {
			$this->load->model('saisie/model_pli_saisie');
			echo $this->model_pli_saisie->redirect_to_saisi_direct($id_pli) ? 1 : 'Le pli ne vous est plus assigné!';
			return '';
		}
		log_message('error', 'saisie> saisie> redirection vers saisie advantage> id_pli non numerique: '.$id_pli);
		echo 'Pli introuvable!';
	}

	public function vers_saisie_n2($id_pli) {
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_op();
		if (is_numeric($id_pli)) {
			$this->load->model('saisie/model_pli_saisie');
			echo $this->model_pli_saisie->vers_saisie_n2($id_pli) ? 1 : 'Le pli ne vous est plus assigné!';
			return '';
		}
		log_message('error', 'saisie> saisie> vers_saisie_n2> id_pli non numerique: '.$id_pli);
		echo 'Pli introuvable!';
	}

	public function quick_save(){//save current saisie data
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_op();
		$this->load->model('saisie/model_pli_saisie');
		if(isset($_REQUEST['id_pli'])){
			$id_pli = $this->input->post_get('id_pli');
			$paiements = json_decode($this->input->post_get('paiements'));
			$data_pli = array(
				'societe' => $this->input->post_get('id_soc')
				,'infos_datamatrix' => $this->input->post_get('datamatrix')
				,'titre' => $this->input->post_get('titre')
				,'code_promotion' => $this->input->post_get('code_promo')
				,'nom_circulaire' => $this->input->post_get('nom_circ') == '' ? NULL : $this->input->post_get('nom_circ')
				,'message_indechiffrable' => $this->input->post_get('msg_ind')
				,'dmd_kdo_fidelite_prim_suppl' => $this->input->post_get('dmd_kdo')
				,'dmd_envoi_kdo_adrss_diff' => $this->input->post_get('dmd_env_kdo')
				//,'type_coupon' => $this->input->post_get('type_coupon')
				,'nom_deleg' => $this->input->post_get('nom_deleg')
				,'typologie' => $this->input->post_get('typo')
				,'code_ecole_gci' => $this->input->post_get('code_ecole_gci')
				// ,'flag_traitement' => 3
				,'pli_nouveau' => 0
			);
			$pli = array(
				'idefix' => $this->input->post_get('idefix')
				,'typologie' => $this->input->post_get('typo')
				,'societe' => $data_pli['societe']
				// ,'flag_traitement' => 3
				// ,'saisie_par' => NULL
				// ,'recommande' => -1
			);
			$id_group_pli_possible = $this->input->post_get('id_group_pli_possible');
			$his_grp_pli_poss = $pli['typologie'].'-'.$this->model_pli_saisie->id_grp_paie_for_paiement($paiements).'-'.$data_pli['societe'];
			if($id_group_pli_possible != $his_grp_pli_poss){// si incompatible avec regroupement alors sortir du groupe saisie
				// $pli['id_groupe_pli'] = NULL;
				// $pli['flag_traitement'] = 2;
			}
			$data_saisie = $this->get_data_saisie();
			// $data_saisie->motif_consigne = NULL;
			$data_pli['mvt_nbr'] = count($data_saisie->mvmnts);
			$data_pli['avec_chq'] = count($data_saisie->cheques) > 0 ? 1 : 0;
			$data_saisie->pli = $pli;
			$data_saisie->data_pli = $data_pli;
			// echo var_dump($data_saisie);die();exit();
			if(preg_match("#\d+#", $this->input->post_get('dmd_env_kdo'))) {
				$rep_save = $this->model_pli_saisie->quick_save($data_saisie);
			}else{
				echo 'Données non-conformes';
				log_message('error', 'controllers>saisie>Saisie>quick_save data non-conforme op#'.$this->session->id_utilisateur.', pli#'.$id_pli);
				return '';
			}
			if($rep_save[0]){
				if($id_group_pli_possible != $his_grp_pli_poss){
					echo 1;//echo 2;
				}else{
					echo 1;
				}
			}else{
				echo $rep_save[1];
				log_message('error', 'Quick save saisie échoué:'.$rep_save[1].', pli#'.$id_pli.', op#'.$this->session->id_utilisateur);
			}
		}else{
			echo 'Paramètre manquant';
			log_message('error', 'Quick save saisie> parametre manquant op#'.$this->session->id_utilisateur);
		}
	}

	public function ko_save($data_saisie_ok=NULL, $motif_ci='') {
		$this->load->library('portier');
		$this->portier->auto_inspecter_v(TRUE);
		$this->portier->must_op();
		$this->load->model('saisie/model_pli_saisie');
		if(isset($_REQUEST['id_pli'])){
			$statut = $this->input->post_get('statut');
			$data_saisie = is_null($data_saisie_ok) ? $this->get_data_saisie() : $data_saisie_ok;
			$motif_ko = $this->input->post_get('autre_motif_ko');
			if(in_array($statut, array(1, 11))){
				$statut = $data_saisie->etat_pli_by_chq;
				$motif_ko = $motif_ci;
			}
			$saisie_batch = is_numeric($this->input->post_get('saisie_batch')) && $this->input->post_get('saisie_batch') == 1 ? TRUE : FALSE;
			$id_pli = $this->input->post_get('id_pli');
			$id_lot_saisie = $this->input->post_get('id_lot_saisie');
			$paiements = json_decode($this->input->post_get('paiements'));
			$action_fichier_ci = $this->copy_ci();
			if($action_fichier_ci['code']){
				$fichier_circulaire = $action_fichier_ci['fichier_circulaire'];
			}else{
				return '';
			}
			$data_pli = array(
				'societe' => $this->input->post_get('id_soc')
				,'infos_datamatrix' => $this->input->post_get('datamatrix')
				,'titre' => $this->input->post_get('titre')
				,'code_promotion' => $this->input->post_get('code_promo')
				,'statut_saisie' => $statut
				,'motif_ko' => ($this->input->post_get('statut') == 1 ? 4 : $this->input->post_get('motif_ko'))
				,'autre_motif_ko' => trim($motif_ko)
				,'nom_circulaire' => $this->input->post_get('nom_circ') == '' ? NULL : $this->input->post_get('nom_circ')
				,'fichier_circulaire' => $fichier_circulaire
				,'nom_orig_fichier_circulaire' => $this->input->post_get('nom_fichier_circ')
				,'message_indechiffrable' => $this->input->post_get('msg_ind')
				,'dmd_kdo_fidelite_prim_suppl' => $this->input->post_get('dmd_kdo')
				,'dmd_envoi_kdo_adrss_diff' => $this->input->post_get('dmd_env_kdo')
				//,'type_coupon' => $this->input->post_get('type_coupon')
				,'nom_deleg' => $this->input->post_get('nom_deleg')
				,'code_ecole_gci' => $this->input->post_get('code_ecole_gci')
				,'pli_nouveau' => 0
				,'par_ttmt_ko' => 0
				,'date_dernier_statut_ko' => date('Y-m-d H:i:s')
			);
			$flag_traitement = $saisie_batch ? 6 : 5;
			switch ($statut) {
				case '2'://ko scan
					$action_saisie = $saisie_batch ? 94 : 74;
					$data_saisie->chq_saisie = array();
					break;
				case '3'://ko def
					$action_saisie = $saisie_batch ? 96 : 8;
					$data_saisie->chq_saisie = array();
					break;
				case '4'://ko inconnu
					$action_saisie = $saisie_batch ? 95 : 73;
					$data_saisie->chq_saisie = array();
					break;
				case '5'://ko src
					$action_saisie = $saisie_batch ? 97 : 92;
					$data_saisie->chq_saisie = array();
					break;
				case '6'://ko en attente
					$action_saisie = $saisie_batch ? 98 : 93;
					// $data_saisie->chq_saisie = array();
					break;
				case '7'://ko circ
					$action_saisie = $saisie_batch ? 99 : 14;
					// $data_pli['dt_enregistrement'] = date('Y-m-d H:i:s');
					break;
				default:
					echo 'Argument[statut] non conforme!';
					log_message('error', 'Enregistrement saisie> unexpected status='.$statut.', op#'.$this->session->id_utilisateur.', pli#'.$id_pli);
					return '';
					break;
			}
			$pli = array(
				'flag_traitement' => $flag_traitement
				,'idefix' => $this->input->post_get('idefix')
				,'typologie' => $this->input->post_get('typo')
				,'id_lot_saisie' => $id_lot_saisie
				,'lot_saisie_bis' => 0
				,'sorti_saisie' => $flag_traitement
				,'statut_saisie' => $statut
				,'societe' => $data_pli['societe']
			);
			$data_pli['typologie'] = $this->input->post_get('typo');
			$data_pli['flag_traitement'] = $flag_traitement;
			$id_group_pli_possible = $this->input->post_get('id_group_pli_possible');
			$his_grp_pli_poss = $pli['typologie'].'-'.$this->model_pli_saisie->id_grp_paie_for_paiement($paiements).'-'.$data_pli['societe'];
			$bis = FALSE;
			if($id_group_pli_possible != $his_grp_pli_poss){
				$pli['lot_saisie_bis'] = 1;
				$bis = TRUE;
			}
			foreach ($data_saisie->mvmvnts_saisie as $key_m_s => $value_m_s) {
				$data_saisie->mvmvnts_saisie[$key_m_s]['stat_pli'] = $statut;
			}
			$data_pli['mvt_nbr'] = 1;
			$data_pli['avec_chq'] = count($data_saisie->cheques) > 0 ? 1 : 0;
			$data_saisie->id_lot_saisie = $id_lot_saisie;
			$data_saisie->id_group_pli_possible = $id_group_pli_possible;
			$data_saisie->pli = $pli;
			$data_saisie->data_pli = $data_pli;
			$data_saisie->statut = $statut;
			$data_saisie->action_saisie = $action_saisie;
			$choix_motif = $data_pli['motif_ko'] == 4 ? '' : $this->model_pli_saisie->get_label_motif_ko($data_pli['motif_ko']);
			$data_saisie->link_motif_consigne = $id_pli.date('YmdHis');
			$data_saisie->motif_consigne = array(
				'id_pli' => $id_pli
				,'motif_operateur' => $data_pli['autre_motif_ko'] == '' ? $choix_motif : $choix_motif.$data_pli['autre_motif_ko']
				,'id_flg_trtmnt' => $flag_traitement
				,'id_stt_ss' => $statut
				,'link_histo' => $data_saisie->link_motif_consigne
			);
			// echo var_dump($data_saisie);die();exit();
			if(preg_match("#\d+#", $this->input->post_get('dmd_env_kdo'))) {
				$rep_save = $this->model_pli_saisie->ko_save_sasie($data_saisie);
			}else{
				echo 'Données non-conformes';
				log_message('error', 'controllers>saisie>Saisie>ko_save data non-conforme op#'.$this->session->id_utilisateur.', pli#'.$id_pli);
				return '';
			}
			if($rep_save[0]){
				echo '1';
			}else{
				echo $rep_save[1];
				log_message('error', 'Enregistrement ko_saisie échoué:'.$rep_save[1].', pli#'.$id_pli.', op#'.$this->session->id_utilisateur);
			}
		}else{
			echo 'Paramètre manquant';
			log_message('error', 'Enregistrement ko_saisie> parametre manquant op#'.$this->session->id_utilisateur);
		}
	}

	private function get_data_saisie(){// synthetise les data saisie
		$this->load->model('saisie/model_cba');
		$id_pli = $this->input->post_get('id_pli');
		$statut = $this->input->post_get('statut');
		$paiements = json_decode($this->input->post_get('paiements'));
		$chq_only = TRUE;
		$data_paiements = array();
		foreach ($paiements as $paiement) {
			array_push($data_paiements, array(
				'id_pli' => $id_pli
				,'id_mode_paiement' => $paiement
			));
			if($paiement != 1){
                $chq_only = FALSE;
            }
		}
		$saisie_batch = is_numeric($this->input->post_get('saisie_batch')) && $this->input->post_get('saisie_batch') == 1 ? TRUE : FALSE;
		$mvmnt_payeurs = json_decode($this->input->post_get('mvmnt_payeurs'));
		$mvmnt_abonnes = json_decode($this->input->post_get('mvmnt_abonnes'));
		$mvmnt_promos = json_decode($this->input->post_get('mvmnt_promos'));
		$ok_data_batch = TRUE;
		$error_data_batch = '';
		$mvmnts = json_decode($this->input->post_get('mvmnts'));
		$data_mvmvnts = array();
		$qtt_mvmnt = array();
		$mvmvnts_saisie = array();
		foreach ($mvmnts as $key_mvmnt => $mvmnt) {
			$c_mvmnt = array(
				'id_pli' => $id_pli
				,'titre' => $mvmnt->titre
				,'code_promo' => $mvmnt->code_promo
				,'numero_abonne' => $mvmnt->num_a
				,'nom_abonne' => $mvmnt->nom_a
				//,'code_postal_abonne' => $mvmnt->cp_a
				,'numero_payeur' => $mvmnt->num_p
				,'nom_payeur' => $mvmnt->nom_p
				,'ui_id' => $mvmnt->ui_id
				//,'code_postal_payeur' => $mvmnt->cp_p
			);
			$mvmnt_saisie = array(
				'id_pli' => $id_pli
				,'id_titre' => $mvmnt->titre
				,'num_ab' => $c_mvmnt['numero_abonne']
				,'nom_ab' => $c_mvmnt['nom_abonne']
				,'num_p' => $c_mvmnt['numero_payeur']
				,'nom_p' => $c_mvmnt['nom_payeur']
				,'ui_id' => $c_mvmnt['ui_id']
				,'code_promo' => $c_mvmnt['code_promo']
				,'code_chx' => ''
				,'code_prod' => ''
				,'quantite' => $mvmnt->qtt
				,'id_op' => $this->session->id_utilisateur
				,'flag_batch' => '0'
				,'stat_pli' => $this->input->post_get('statut')
			);
			if($saisie_batch && in_array($statut, array('1'))){
				$ui_id = $mvmnt->ui_id;
				$qtt_mvmnt[$ui_id] = $mvmnt->qtt;
				$payeur = $mvmnt_payeurs->$ui_id;
				$c_mvmnt['numero_payeur'] = $payeur->ctm_nbr;
				$c_mvmnt['nom_payeur'] = $payeur->atn_end;
				$abonne = $mvmnt_abonnes->$ui_id;
				$c_mvmnt['numero_abonne'] = $abonne->ctm_nbr;
				$c_mvmnt['nom_abonne'] = $abonne->atn_end;
				$promo = $mvmnt_promos->$ui_id;
				$verif_data_batch = $this->model_cba->verif_data_batch($payeur, $abonne, $promo, $chq_only);
				if(!$verif_data_batch['is_ok']){
					$error_data_batch .= $ok_data_batch ? '' : '; ';
					$ok_data_batch = FALSE;
					$error_data_batch .= 'mouvement#'.($key_mvmnt+1).': '.$verif_data_batch['msg'];
				}
				$mvmnt_saisie['flag_batch'] = '1';
				$mvmnt_saisie['num_ab'] = $c_mvmnt['numero_abonne'];
				$mvmnt_saisie['nom_ab'] = $c_mvmnt['nom_abonne'];
				$mvmnt_saisie['num_p'] = $c_mvmnt['numero_payeur'];
				$mvmnt_saisie['nom_p'] = $c_mvmnt['nom_payeur'];
				$mvmnt_saisie['code_chx'] = $promo->code_choix;
				$mvmnt_saisie['code_prod'] = $promo->code_produit;
			}
			array_push($data_mvmvnts, $c_mvmnt);
			array_push($mvmvnts_saisie, $mvmnt_saisie);
		}
		$cheques = json_decode($this->input->post_get('cheques'));
		$data_cheque = array();
		$cheques_anomalies = array();
		$histo_chq = array();
		$ref_etat_chq = $this->model_pli_saisie->cheque_etat_tab();
		$ref_anom_chq_etat = $this->model_pli_saisie->anom_cheque_tab();
		$etat_pli_by_chq_temp = new stdClass();
		$etat_pli_by_chq_temp->etat_pli = 1;
		$etat_pli_by_chq_temp->prio_anom = 0;
		$motif_ko_by_chq = '';
		$chq_saisie = array();
		foreach ($cheques as $cheque) {
			$id_etat_chq = $cheque->id_etat_chq;
			$paie_anomalie = $cheque->anomalies;
			$id_etat_chq_temp = $ref_etat_chq[2];
            if(is_array($paie_anomalie)){
                foreach ($paie_anomalie as $key_p_anom => $p_anom) {
                    array_push($cheques_anomalies, array(
                        'id_doc' => $cheque->id_doc
                        ,'id_pli' => $id_pli
                        ,'id_anomalie_chq' => $p_anom
                        ,'ui_id_pca' => $cheque->ui_id
					));
					if($id_etat_chq_temp->priorite > $ref_anom_chq_etat[$p_anom]->prio_stat){
						$id_etat_chq_temp = $ref_etat_chq[$ref_anom_chq_etat[$p_anom]->id_etat_chq];
					}
					if($etat_pli_by_chq_temp->prio_anom < $ref_anom_chq_etat[$p_anom]->prio_anom /*&& $id_etat_chq != 4*/){
						$etat_pli_by_chq_temp = $ref_anom_chq_etat[$p_anom];
					}
					if(!in_array($ref_anom_chq_etat[$p_anom]->etat_pli, array(1, 11))) {
						$motif_ko_by_chq .= $motif_ko_by_chq == '' ? 'anomalie chèque: ' : ', ';
						$motif_ko_by_chq .= $ref_anom_chq_etat[$p_anom]->label_anom;
					}
				}
			}
			if($id_etat_chq != 4/* && $cheque->etat_modif == 0*/){
                $id_etat_chq = $id_etat_chq_temp->id_etat_chq;
            }
			array_push($data_cheque, array(
				'id_pli' => $id_pli
				,'montant' => u_montant($cheque->montant)
				,'cmc7' => $cheque->cmc7
				,'rlmc' => $cheque->rlmc
				,'id_etat_chq' => $id_etat_chq
				,'etat_modif' => $cheque->etat_modif
				,'id_doc' => $cheque->id_doc
				,'nom_client' => $cheque->nom_client
				,'etranger' => $cheque->etranger == 1 ? 1 : 0
				,'ui_id' => $cheque->ui_id
			));
			array_push($histo_chq, array(
				'id_doc' => $cheque->id_doc
				,'cmc7' => $cheque->cmc7
				,'anomalies' => is_array($paie_anomalie) ? join(',', $paie_anomalie) : ''
				,'etat' => $id_etat_chq
				,'etape' => 'saisie'
				,'commentaire' => 'saisie/saisie-partielle'
				,'id_pli' => $id_pli
				,'ui_id_hc' => $cheque->ui_id
			));
			if($cheque->etranger == 0) {
				array_push($chq_saisie, array(
					'montant' => u_montant($cheque->montant)
					,'cmc7' => $cheque->cmc7
					,'rlmc' => $cheque->rlmc
					,'id_etat_chq' => $id_etat_chq
					,'match_societe' => $this->input->post_get('id_soc')
					,'id_pli' => $id_pli
				));
			}
		}
		$cheques_cadeaux = json_decode($this->input->post_get('cheques_cadeaux'));
		$cheques_cadeaux = is_array($cheques_cadeaux) ? $cheques_cadeaux : array();
		$data_cheques_cadeaux = array();
		foreach ($cheques_cadeaux as $cheque_kd) {
			array_push($data_cheques_cadeaux, array(
				'id_pli' => $id_pli
				,'montant' => u_montant($cheque_kd->montant)
				,'type_chq_kdo' => $cheque_kd->type
				,'code_barre' => $cheque_kd->code_barre
				,'id_doc' => is_numeric($cheque_kd->id_doc) ? $cheque_kd->id_doc : NULL
				,'ui_id' => $cheque_kd->ui_id
			));
		}
		$paie_esp = array(
			'id_pli' => $id_pli
			,'montant' => u_montant(is_null($this->input->post_get('montant_esp')) ? '0' : $this->input->post_get('montant_esp'))
		);
		$data_saisie = new stdClass();
		$data_saisie->id_pli = $id_pli;
		$data_saisie->mvmnts = $data_mvmvnts;
		$data_saisie->mvmnt_payeurs = $mvmnt_payeurs;
		$data_saisie->mvmnt_abonnes = $mvmnt_abonnes;
		$data_saisie->mvmnt_promos = $mvmnt_promos;
		$data_saisie->mvmnt_qtt = $qtt_mvmnt;
		$data_saisie->ok_data_batch = $ok_data_batch;
		$data_saisie->error_data_batch = $error_data_batch;
		$data_saisie->paiements = $data_paiements;
		$data_saisie->cheques = $data_cheque;
		$data_saisie->chq_saisie = $chq_saisie;
		$data_saisie->paiement_chq_anomalies = $cheques_anomalies;
		$data_saisie->histo_cheque = $histo_chq;
		$data_saisie->cheques_cadeaux = $data_cheques_cadeaux;
		$data_saisie->paie_esp = $paie_esp;
		$data_saisie->img_dezip = json_decode($this->input->post_get('img_dezip'));
		$data_saisie->op = $this->session->id_utilisateur;
		$data_saisie->etat_pli_by_chq = $etat_pli_by_chq_temp->etat_pli;
		$data_saisie->motif_ko_by_chq = $motif_ko_by_chq;
		$data_saisie->mvmvnts_saisie = $mvmvnts_saisie;
		return $data_saisie;
	}
    
}
