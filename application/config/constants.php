<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

//database table name
defined('TB_user') OR define('TB_user', 'utilisateurs');
defined('TB_tpUser') OR define('TB_tpUser', 'type_utilisateur');
defined('TB_pli') OR define('TB_pli', 'pli');
defined('TB_grUser') OR define('TB_grUser', 'groupe_utilisateur');
defined('TB_assignation') OR define('TB_assignation', 'assignation');
defined('TB_groupe_pli') OR define('TB_groupe_pli', 'groupe_pli');
defined('TB_paiement') OR define('TB_paiement', 'paiement');
defined('TB_mode_paiement') OR define('TB_mode_paiement', 'mode_paiement');
defined('TB_data_pli') OR define('TB_data_pli', 'data_pli');
defined('TB_regroupement') OR define('TB_regroupement', 'regroupement');
defined('TB_decoupage') OR define('TB_decoupage', 'decoupage');
defined('Vw_pli') OR define('Vw_pli', 'view_pli');
defined('Vw_grPliPos') OR define('Vw_grPliPos', 'view_groupe_pli_possible');
defined('Vw_dataPli') OR define('Vw_dataPli', 'view_data_pli');
defined('TB_lotSaisie') OR define('TB_lotSaisie', 'lot_saisie');
defined('TB_document') OR define('TB_document', 'document');
defined('TB_soc') OR define('TB_soc', 'societe');
defined('TB_typo') OR define('TB_typo', 'typologie');
defined('TB_titre') OR define('TB_titre', 'titres');
defined('TB_anomChq') OR define('TB_anomChq', 'anomalie_cheque');
defined('TB_etatChq') OR define('TB_etatChq', 'etat_cheque');
defined('TB_paieChqAnom') OR define('TB_paieChqAnom', 'paiement_chq_anomalie');
defined('TB_histoChq') OR define('TB_histoChq', 'histo_cheque');
defined('TB_mvmnt') OR define('TB_mvmnt', 'mouvement');
defined('TB_statS') OR define('TB_statS', 'statut_saisie');
defined('TB_motifKo') OR define('TB_motifKo', 'motif_ko');
defined('TB_lstCirc') OR define('TB_lstCirc', 'liste_circulaires');
defined('TB_cheque') OR define('TB_cheque', 'paiement_cheque');
defined('TB_chequeAnoCorrige') OR define('TB_chequeAnoCorrige', 'paiement_chq_ano_corrige');
defined('TB_chequeTest') OR define('TB_chequeTest', 'pmtcheque_test');
defined('TB_categEr') OR define('TB_categEr', 'categorie_erreur');
defined('TB_ctrlPliEr') OR define('TB_ctrlPliEr', 'controle_pli_erreur');
defined('TB_grPaie') OR define('TB_grPaie', 'groupe_paiement');
defined('TB_histo') OR define('TB_histo', 'histo_pli');
defined('TB_trace') OR define('TB_trace', 'trace');
defined('TB_action') OR define('TB_action', 'tb_action');
defined('TB_flgTrt') OR define('TB_flgTrt', 'flag_traitement');
defined('Vw_flgTrt') OR define('Vw_flgTrt', 'view_flag_traitement');
defined('Vw_user') OR define('Vw_user', 'view_utilisateur');
defined('Vw_doc') OR define('Vw_doc', 'view_document');
defined('Vw_lastHisto') OR define('Vw_lastHisto', 'view_histo_pli_oid');
defined('Vw_lotNum') OR define('Vw_lotNum', 'view_lot');
defined('TB_lotNumerisation') OR define('TB_lotNumerisation', 'lot_numerisation');
defined('Vw_pcheque') OR define('Vw_pcheque', 'view_p_cheque');
defined('Vw_typologie') OR define('Vw_typologie', 'view_typlogie');
defined('Vw_statutSaisie') OR define('Vw_statutSaisie', 'view_statut_saisie');
defined('Vw_Societe') OR define('Vw_Societe', 'view_societe');
defined('TB_reb_temp') OR define('TB_reb_temp', 'remise_en_banque_msc_temp_new');
//defined('TB_reb_temp_new') OR define('TB_reb_temp_new', 'remise_en_banque_msc_new');
defined('TB_base_reb') OR define('TB_base_reb', 'remise_en_banque_msc_new_old');
defined('TB_synchro') OR define('TB_synchro', 'syncro_ged_bayard');
defined('FTB_document') OR define('FTB_document', 'f_document');
defined('FTB_documentImg') OR define('FTB_documentImg', 'f_document_image');
defined('FTB_lotNumerisation') OR define('FTB_lotNumerisation', 'f_lot_numerisation');
defined('FTB_flgTrt') OR define('FTB_flgTrt', 'f_flag_traitement');
defined('FTB_pli') OR define('FTB_pli', 'f_pli');
defined('FTB_user') OR define('FTB_user', 'f_utilisateurs');
defined('FTB_data_pli') OR define('FTB_data_pli', 'f_data_pli');
defined('TB_etatMatchage') OR define('TB_etatMatchage', 'etat_matchage');
defined('FTB_cheque') OR define('FTB_cheque', 'f_paiement_cheque');
defined('FTB_paie') OR define('FTB_paie', 'f_paiement');
defined('FTB_mdPaie') OR define('FTB_mdPaie', 'f_mode_paiement');
defined('FTB_typo') OR define('FTB_typo', 'f_typologie');
defined('FTB_lstCirc') OR define('FTB_lstCirc', 'f_liste_circulaires');
defined('FTB_soc') OR define('FTB_soc', 'f_societe');
defined('FTB_statuS') OR define('FTB_statuS', 'f_statut_saisie');
defined('FTB_motifCons') OR define('FTB_motifCons', 'f_motif_consigne');
defined('TB_mtfRj') OR define('TB_mtfRj', 'motif_rejet');
defined('TB_mtfKoScn') OR define('TB_mtfKoScn', 'motif_ko_scan');
defined('TB_chqKd') OR define('TB_chqKd', 'cheque_cadeau');
defined('FTB_chqKd') OR define('FTB_chqKd', 'f_cheque_cadeau');
defined('FTB_paieChqKd') OR define('FTB_paieChqKd', 'f_paiement_cheque_cadeau');
defined('TB_paieChqKd') OR define('TB_paieChqKd', 'paiement_cheque_cadeau');
defined('TB_paieChqKdTest') OR define('TB_paieChqKdTest', 'paiement_ckd_test');
defined('TB_etatCkd') OR define('TB_etatCkd', 'etat_cheque_cadeau');
defined('TB_paieEsp') OR define('TB_paieEsp', 'paiement_espece');
defined('TB_preData') OR define('TB_preData', 'predata');
defined('FTB_preData') OR define('FTB_preData', 'f_predata');
defined('TB_matchTempBy') OR define('TB_matchTempBy', 'matchage_temp_by');
defined('TB_matchTempMi') OR define('TB_matchTempMi', 'matchage_temp_mi');
defined('TB_matchCkdTemp') OR define('TB_matchCkdTemp', 'matchage_ckd_temp');
defined('TB_koMatchage') OR define('TB_koMatchage', 'type_ko_matchage');
defined('FTB_opAcces') OR define('FTB_opAcces', 'f_operateur_acces');
defined('VW_dureeTypage') OR define('VW_dureeTypage', 'view_duree_typage');
defined('FTB_decoupage') OR define('FTB_decoupage', 'f_decoupage');
defined('VW_dureeCtrl') OR define('VW_dureeCtrl', 'view_duree_ctrl');
defined('VW_dureeSaisie') OR define('VW_dureeSaisie', 'view_duree_saisie');
defined('VW_erreurCtrl') OR define('VW_erreurCtrl', 'view_erreur_ctrl');
defined('FVW_pliCourrier') OR define('FVW_pliCourrier', 'f_pli_courrier');
defined('TB_dataKe') OR define('TB_dataKe', 'data_pli_ke');
defined('TB_fichierKe') OR define('TB_fichierKe', 'fichier_ke');
defined('TB_mstRebBy') OR define('TB_mstRebBy', 'mst_reb_advantage_bayard'); //Table new matchage bayard
defined('TB_mstRebMi') OR define('TB_mstRebMi', 'mst_reb_advantage_milan'); //Table new matchage milan
defined('TB_cba') OR define('TB_cba', 'commande_cba');
defined('TB_addrCba') OR define('TB_addrCba', 'adresses_cba');
defined('TB_promoCba') OR define('TB_promoCba', 'promo_cba');
defined('TB_saisiCtrlField') OR define('TB_saisiCtrlField', 'saisie_ctrl_field');
defined('TB_motifConsigne') OR define('TB_motifConsigne', 'motif_consigne');
defined('TB_commentsPli') OR define('TB_commentsPli', 'comments_pli');
defined('TB_rejetCbaFl') OR define('TB_rejetCbaFl', 'rejet_cba_files');
defined('TB_rejetCbaLn') OR define('TB_rejetCbaLn', 'rejet_cba_lignes');
defined('TB_chequeSaisie') OR define('TB_chequeSaisie', 'cheque_saisie');
defined('TB_mvmntSaisie') OR define('TB_mvmntSaisie', 'mvmnt_saisie');
defined('TB_suiviNum') OR define('TB_suiviNum', 'suivi_numerisation');
defined('FTB_suiviNum') OR define('FTB_suiviNum', 'f_suivi_numerisation');
defined('TB_suiviMail') OR define('TB_suiviMail', 'suivi_mail');
defined('VW_mstReb') OR define('VW_mstReb', 'mst_reb');
defined('FTB_etatChq') OR define('FTB_etatChq', 'f_etat_cheque');
defined('FTB_paieChqAnom') OR define('FTB_paieChqAnom', 'f_paiement_chq_anomalie');
defined('FTB_chqAnom') OR define('FTB_chqAnom', 'f_anomalie_cheque');
defined('TB_assignationPli') OR define('TB_assignationPli', 'assignation_pli');
defined('TB_histoAssignationPli') OR define('TB_histoAssignationPli', 'histo_assignation_pli');
defined('FTB_fxTypologie') OR define('FTB_fxTypologie', 'flux_typologie');
//clé d'encriptage
defined('Kryptxt') OR define('Kryptxt', 'coNOPETche2019');

//URL upload
defined('upTmpFileCirc') OR define('upTmpFileCirc', 'depot/circulaires/tmp/');//param local (test)
defined('upFileCirc') OR define('upFileCirc', 'depot/circulaires/');//param local (test)
// defined('upTmpFileCirc') OR define('upTmpFileCirc', 'SFTP_MAIL/depot/circulaires/tmp/');
// defined('upFileCirc') OR define('upFileCirc', 'SFTP_MAIL/depot/circulaires/');

//URL temp file
defined('urlFileTemp') OR define('urlFileTemp', 'depot/temp_files/');

//tables push
defined('TB_statutPli') OR define('TB_statutPli', 'statut_pli');
defined('TB_flux') OR define('TB_flux', 'flux');
defined('TB_abonmnt') OR define('TB_abonmnt', 'abonnement');
defined('TB_opAcces') OR define('TB_opAcces', 'operateur_acces');
defined('TB_tpOpAcces') OR define('TB_tpOpAcces', 'type_operateur_acces');
defined('TB_sources') OR define('TB_sources', 'source');
defined('TB_typologieFlux') OR define('TB_typologieFlux', 'typologie');
defined('TB_lotSaisieFlux') OR define('TB_lotSaisieFlux', 'lot_saisie_flux');
defined('VW_lotSaisieFlux') OR define('VW_lotSaisieFlux', 'view_lot_saisie_flux');
defined('TB_titreFx') OR define('TB_titreFx', 'titre');
defined('TB_statutAb') OR define('TB_statutAb', 'statut_abonnement');
defined('TB_pjs') OR define('TB_pjs', 'pieces_jointes');
defined('TB_anomalieFlux') OR define('TB_anomalieFlux', 'anomalie_flux');
defined('TB_histoFx') OR define('TB_histoFx', 'traitement');
defined('TB_lstMail') OR define('TB_lstMail', 'liste_mail');
defined('TB_lstDossier') OR define('TB_lstDossier', 'liste_dossier');
defined('TB_socFx') OR define('TB_socFx', 'societe');
defined('TB_traceMailSrc') OR define('TB_traceMailSrc', 'trace_mail_src');
defined('VW_typoCrFx') OR define('VW_typoCrFx', 'view_typo_courrier_flux');
defined('VW_statusCrFx') OR define('VW_statusCrFx', 'view_status_v2_courrier_flux');
defined('VW_dureeTypageFx') OR define('VW_dureeTypageFx', 'view_duree_typage_flux');
defined('VW_dureeSaisieFx') OR define('VW_dureeSaisieFx', 'view_duree_saisie_flux');
defined('TB_dataKeFx') OR define('TB_dataKeFx', 'data_flux_ke');
defined('TB_fichierKeFx') OR define('TB_fichierKeFx', 'fichier_ke');
defined('TB_motifConsigneFx') OR define('TB_motifConsigneFx', 'motif_consigne_flux');

//table reb
defined('TB_volumeReb') OR define('TB_volumeReb', 'volume_ged_bayard_reb');
defined('TB_volumeRebCe') OR define('TB_volumeRebCe', 'volume_ged_bayard_reb_ce');
defined('TB_reb') OR define('TB_reb', 'remise_en_banque_msc_new');
defined('TB_rebCe') OR define('TB_rebCe', 'remise_en_banque_msc_ce');
defined('TB_rebTp') OR define('TB_rebTp', 'remise_en_banque_msc_new');
defined('TB_rebCeTp') OR define('TB_rebCeTp', 'remise_en_banque_msc_ce');
defined('TB_syncro') OR define('TB_syncro', 'syncro_ged_bayard');
defined('TB_syncroCe') OR define('TB_syncroCe', 'syncro_ged_bayard_ce');
