<!DOCTYPE html>
<html>
    <head>
        <!-- Google Fonts -->
        <title>Ged | <?php echo $title; ?> </title>
        <!--<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"> !-->
        <link rel="icon" href="<?php echo base_url();?>assets/images/favicon2.png" type="image/x-icon">



        <?php

       

        if(isset($css) && count($css) > 0)
        {
            assets_url($css,$type='css');
        }

        if(isset($js) && count($js) > 0)
        {
            assets_url($js,$type='js');
    
        }

        ?>
        <script>
            var s_url = "<?php echo site_url()."/"; ?>";
        </script>
        <style>
            ul.menu {list-style-type: none;}
            #id-scroll {
            position:fixed;
            right:10px;
            bottom:10px;
            cursor:pointer;
            width:50px;
            height:50px;
            background-color:#3f51b5;
            text-indent:-9999px;
            display:none;
            -webkit-border-radius:25px;
            -moz-border-radius:25px;
            border-radius:25px;
            opacity: 0.5;
        }
        #id-scroll span {
            position:absolute;
            top:50%;
            left:50%;
            margin-left:-8px;
            margin-top:-12px;
            height:0;
            width:0;
            border:8px solid transparent;
            border-bottom-color:#ffffff
        }
        #id-scroll:hover {
            background-color:#009688;
            opacity:1;
            filter:"alpha(opacity=100)";
            -ms-filter:"alpha(opacity=100)";
        }
         
      .dropdown-menu > .active > a
        , .dropdown-menu > .active > a:hover
        , .dropdown-menu > .active > a:focus {
        /* text-decoration: none !important; */
        background-color: #337ab7 !important;
        color: #fff !important;
        }
        .bootstrap-select .dropdown-menu li.selected a {
        background-color: #337ab7a6 !important;
        color: #fff !important;
        }
        .bootstrap-select .dropdown-menu .active a
        , .bootstrap-select .dropdown-menu li.selected a span small
        , .bootstrap-select .dropdown-menu li a:hover span small
        , .bootstrap-select .dropdown-menu li a:focus span small {
        background-color: transparent;
        color: #fff !important;
        }
        .bootstrap-select .dropdown-menu li a:hover
        , .bootstrap-select .dropdown-menu a:focus {
        background-color: #337ab7 !important;
        color: #fff !important;
        }
        .bootstrap-select .dropdown-menu li.active.selected a{
        background-color: #337ab7 !important;
        color: #fff !important;
        }
            </style>
    </head>
    <body class="<?php echo $theme; ?>" id="body">
        <nav class="navbar">
            <a class="navbar-brand" href="">
                <img src="<?php echo base_url().'/assets/images/logo.png'?>" alt="logo" style="display: block;width: 36px !important;">
            </a>
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="#" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="#" class="bars" style="display: none;"></a>
                   <ol class="breadcrumb">
                        <li><a style="color: #fff;text-decoration: none;font-size: 15px;"><i class="material-icons">home</i> GED - BAYARD</a></li>
                        <li class="" style="color: #fff;text-decoration: none;font-size: 15px;">
                            <i class="material-icons"><?php echo $icon; ?></i>&nbsp;
                            <?php echo $title; ?>
                        </li>
                    </ol>
                    
                </div>

                <?php   
                    $CI =&get_instance();
                    $actif =isset($menu_acif)?$menu_acif:null; 
                    $CI->menu->menu();
                ?>    
                <!--ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">view_headline</i>

                        </a>
                        <ul class="dropdown-menu">
                            <li class="header" style="text-align: left !important;margin-left: 12px;">
                                Utilisateur : <i><?php echo (trim($this->session->userdata('login')== ""? "non défini ":$this->session->userdata('login'))); ?></i>
                            </li>
                            <li class="body">
                                <ul class="menu" id="menu-menu-id" style="height:500px !important">

                                    <?php menu_header($menu); ?>
                                    <li>
                                        <a href="#" onclick="deconnexion();">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">input</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Deconnexion</h4>
                                                <p>
                                                    Aller dans la page de login
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </li>
                </ul-->

            </div>
        </nav>
        

