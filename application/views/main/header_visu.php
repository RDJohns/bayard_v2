<!DOCTYPE html>
<html>
    <head>
        <!-- Google Fonts -->
        <title>Ged | <?php echo $title; ?> </title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">



        <?php
        if(isset($css) && count($css) > 0)
        {
            assets_url($css,$type='css');
        }

        if(isset($js) && count($js) > 0)
        {
            assets_url($js,$type='js');
    
        }

        ?>
        <script>
            var s_url = "<?php echo site_url()."/"; ?>";
        </script>
        <style>
            ul.menu {list-style-type: none;}
        </style>
    </head>
    <body class="<?php echo $theme; ?>" id="body">
        <nav class="navbar">
            <a class="navbar-brand" href="">
                <img src="<?php echo base_url().'/assets/images/logo.png'?>" alt="logo" style="display: block;width: 36px !important;">
            </a>
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="#" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="#" class="bars" style="display: none;"></a>
                   <ol class="breadcrumb">
                        <li><a style="color: #fff;text-decoration: none;font-size: 15px;"><i class="material-icons">home</i> GED - BAYARD</a></li>
                        <li class="" style="color: #fff;text-decoration: none;font-size: 15px;">
                            <i class="material-icons"><?php echo $icon; ?></i>&nbsp;
                            <?php echo $title; ?>
                        </li>
                    </ol>
                    
                </div>
                <?php   
                    $CI =&get_instance();
                    // $actif =isset($menu_acif)?$menu_acif:null; 
                    $CI->menu->menu();
                ?>
                <!--ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">view_headline</i>

                        </a>
                        <ul class="dropdown-menu">
                            <li class="header" style="text-align: left !important;margin-left: 12px;">
                                Utilisateur : <i><?php echo (trim($this->session->userdata('login')== ""? "non défini ":$this->session->userdata('login'))); ?></i>
                            </li>
                            <li class="body">
                                <ul class="menu">

                                    <?php menu_header($menu); ?>
                                     <li>
                                        <a href="<?php echo site_url('citrix/saisie_citrix');?>">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">input</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Nouveau virements sur citrix</h4>
                                                <p>
                                                    Saisie de nouveau virement sur citrix
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="deconnexion();">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">input</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Deconnexion</h4>
                                                <p>
                                                    Aller dans la page de login
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </li>
                </ul-->

            </div>
        </nav>
        

