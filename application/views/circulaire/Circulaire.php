<br><br><br><br>
  <div class="container-fluid">
        <div class="row">
            <div class="col-md-1 filtre">Date du </div>
            <div class="col-md-3">
                <div class="input-daterange input-group" id="bs_datepicker_range_container">
                    <div class="form-line">
                        <input type="text" class="form-control provenance-date" id="date-debut" value="<?php echo date( 'd/m/Y', strtotime( 'monday this week' ) );?>">
                    </div>
                    <span class="input-group-addon">au&nbsp;&nbsp;</span>
                    <div class="form-line">
                        <input type="text" class="form-control provenance-date" id="date-fin" value="<?php echo date( 'd/m/Y', strtotime( 'friday this week' ) );?>">
                    </div>
                    
                </div>
            </div>
            <div class="col-md-2">
                <select class="form-control show-tick" tabindex="-98" id="filtre-societe">
                    <option value="0" disabled >-- Choisir une socièté --</option>
                    <option value="1">Bayard</option>
                    <option value="2">Milan</option>
                </select>
            </div>
            <div class="col-md-1">
                    <button class="btn btn-primary waves-effect waves-light" id="stat_rech" onclick="chargerCirculaire();">&nbsp;Rechercher</button>
            </div>
        </div>
 </div>



 <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 c-circulaire" style="margin-top: 5px;">
        <div class="card">
            <div class="header">
                    <h5><span><h5><span>Liste ciculaire</span></h5></span></h5>
                    <div id="circulaire-table-liste">Total: <span id="total"></span></div>
            </div>
            <div class="body body-circulaire-liste">
                <div id="circulaire-liste">
                    <table class="table table-hover table-bordered" id="table-liste-plis-circulaire">
                        <thead>
                            <tr>
                                <tr>
                                    <th>Réception du courrier</th>
                                    <th>Date de traitement CI</th>
                                    <th>Commande</th>
                                    <th>ID Lot scan</th>
                                    <th>ID Pli</th>
                                    <th>Pli</th>
                                    <th>ID Doc</th>       
                                    <th>Titre</th>
                                    <th>Type Circulaire</th>
                                    <th>Image Desarchivé</th>
                                    <th>CMC7</th>
                                    <th>Délégué</th>
                                    <th>Nom du client</th>
                                    <th>Fichier</th>
                                    <th>Traitement</th>
                                </tr>                        
                            </tr>
                        </thead>
                        <tbody id="liste_ci">              
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <button type="button" class="btn bg-teal btn-lg waves-effect" id="valider_ci"  onclick="validation_ci()">
                <i class="material-icons">check_circle</i>
                <span>Enregistrer validation</span>
            </button>
        </div>
        <div class="col-md-4 text-center">
            <button type="button" class="btn bg-teal btn-lg waves-effect export_ci" id="exporter_ci"  disabled onclick="export_ci_valide()" >
                <i class="material-icons">check_circle</i>
                <span>Exporter CI </span>
            </button>
        </div>

</div>


