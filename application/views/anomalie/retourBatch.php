<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card card_filtre">
            <div class="body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Date envoi batch: </b></div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="deb_env_batch" />
                                        <label class="form-label">début</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="fin_env_batch" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Courrier du : </b></div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="deb_courrier" />
                                        <label class="form-label">début</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="fin_courrier" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Société:</b></p>
                        <select class="form-control show-tick" id="sel_soc"  data-live-search="true" title="Toutes" data-size="2" onchange="">
                            <?php foreach ($societes as $socity) : ?>
                            <option value="<?php echo $socity->id ?>"><?php echo $socity->nom_societe ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="txt_recherche_id_pli" onkeyup="//load_table();" />
                                <label class="form-label">Rechercher pli#ID</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="num_payeur" onkeyup="//load_table();" />
                                <label class="form-label">N° payeur</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="nom_payeur" onkeyup="//load_table();" />
                                <label class="form-label">Nom payeur</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="num_dest" onkeyup="//load_table();" />
                                <label class="form-label">N° client destinataire</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="nom_dest" onkeyup="//load_table();" />
                                <label class="form-label">Nom destinataire</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="code_titre" onkeyup="//load_table();" />
                                <label class="form-label">Code produit / Code titre</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="code_promo" onkeyup="//load_table();" />
                                <label class="form-label">Code promotion</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="nom_fichier" onkeyup="//load_table();" />
                                <label class="form-label">Nom fichier</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col_hx">
                        <button type="button" class="btn btn-block btn-info btn-lg waves-effect m-t-20" onclick="load_table();">
                            <i class="material-icons">search</i>
                            <span>Rechercher / Rafraichir</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Graph begin -->

<div class="card card_filtre st_gr">
    <!-- stat pli saisie -->
    <div class="row">
        <div class="col-lg-12">
            <div class="body">
                <div class="col-lg-3">
                    <div class="info-box bg-red1 hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">compare_arrows</i>
                        </div>
                        <div class="content">
                            <div class="text">Plis reçus</div>
                            <span class="info-text"><span id="pli_recu"></span></span><br/>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">layers_clear</i>
                        </div>
                        <div class="content">
                            <div class="text">Saisie</div>
                            <span class="info-text"><span id="pli_saisie" class="value-span"></span></span><br/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="info-box bg-green_k hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">check_circle_outline</i>
                        </div>
                        <div class="content">
                            <div class="text">Saisie directe</div>
                            <span class="info-text"><span id="saisie_directe" class="value-span"></span></span><br/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Saisie batch</div>
                            <span class="info-text"><span id="saisie_batch" class="value-span"></span></span><br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end stat -->
    <!-- Begin stat batch -->
    <div class="row">
          <div class="col-lg-12">
            <div class="body">
                <div class="col-lg-3">
                    <div class="info-box bg-cyan-t hover-expand-effect">
                            <div class="icon">

                                <i class="material-icons">mail</i>
                            </div>
                            <div class="content">
                                <div class="text">Total pli batch</div>
                                <span class="info-text"><span id="pli_batch"></span></span><br/>
                            </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="fa fa-check-square"></i>
                        </div>
                        <div class="content">
                            <div class="text">Plis Ok batch</div>
                            <span class="info-text"><span id="ok_batch"></span></span><br/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="info-box bg-red7 hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">bug_report</i>
                        </div>
                        <div class="content">
                            <div class="text">Plis anomalie batch</div>
                            <span class="info-text"><span id="ano_batch"></span></span><br/>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="info-box bg-red5  hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">hourglass_full</i>
                        </div>
                        <div class="content">
                            <div class="text">En attente retour batch</div>
                            <span class="info-text"><span id="attente_retour"></span></span><br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Graph end -->

<div class="row"  id="search_result">
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <table id="tb_batch" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="align-center">#ID</th>
                            <th class="align-center">Pli</th>
                            <th class="align-center">Date courrier</th>
                            <th class="align-center">Société</th>
                            <th class="align-center">Numero payeur</th>
                            <th class="align-center">Nom payeur</th>
                            <th class="align-center">Numero destinataire</th>
                            <th class="align-center">Nom destinataire</th>
                            <th class="align-center">Code produit</th>
                            <th class="align-center">Code promo</th>
                            <th class="align-center">Date envoi batch</th>
                            <th class="align-center">Code d'erreur</th>
                            <th class="align-center">Nom fichier</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- modal visu pli -->
<!--  modals -->
<div class="modal fade" id="mdl_display_docs" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Les documents du pli#<span id="id_pli_docs"></span> 
                        <a href="" id="link_download_docs_zip" href_init="<?php echo (site_url('visual/visual/download_all_docs/')) ?>" download="" type="button" class="btn btn-primary waves-effect m-t--10" title="T&eacute;l&eacute;charger">
                            <i class="material-icons">file_download</i>
                            <span>Zip</span>
                        </a>
                    </h4>
                </div>
                <div class="modal-body" id="mdl_display_docs_body" style="max-height: 77vh; overflow-y: auto;"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                </div>
            </div>
        </div>
</div>
<script>
    var num_payeur = <?php echo json_encode($num_payeur) ?>;
    var nom_payeur = <?php echo json_encode($nom_payeur) ?>;
    var num_dest = <?php echo json_encode($num_dest) ?>;
    var nom_dest = <?php echo json_encode($nom_dest) ?>;
    var code_produit = <?php echo json_encode($code_produit) ?>;
    var code_promo = <?php echo json_encode($code_promo) ?>;
    var nom_fichier = <?php echo json_encode($nom_fichier) ?>;
</script>