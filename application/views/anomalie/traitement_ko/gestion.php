<br><br><br><br>
<?php 
    
    $strEtatPli  = '<select class="form-control show-tick elem_filtre"  id="etat-pli" tabindex="-98" onchange="setFiltreStatut();" data-live-search="true"  multiple>';
    $strEtatPli .= '<option value="0"  disabled>--Choisir le statut du pli--</option>';
    $strEtatPli .= '<option value="all" >--Tout sélectionner--</option>';
    $strEtatPli .= '<option value="none" >--Tout déselectionner--</option>';
   
    
    foreach($etatSaisie as $valueEtatSaisie)
    {
        $selected   = "";
        if(intval($this->session->userdata('id_type_utilisateur')) == 4 && intval($valueEtatSaisie->id_statut_saisie) == 6)
        {
            $selected  = " selected ";
        }
        
        $strEtatPli .= "<option value='".intval($valueEtatSaisie->id_statut_saisie)."' ".$selected.">".$valueEtatSaisie->libelle."</option>";

    }
    $strEtatPli .="</select>";
    
    $hideOtherCdn = " col-lg-2 ";
    $strCenterDivCdn = "";
    $strThTable =" ";
    if(intval($this->session->userdata('id_type_utilisateur')) == 8)
    {
        
        $hideOtherCdn = "style='display:none !important;'";
        $strCenterDivCdn = '<div class="col-lg-1"></div>';
        $strThTable      = '<th width="35px">Lot scan</th><th width="15px">Commande</th>';
    }

?>
<!--
<div id="global-filtre">
    <div class="container-fluid">
        
    </div>
</div>
-->

<!--filtre-->
<div id="ko-body">
    <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
    <div class="panel-group" id="accordion_10" role="tablist" aria-multiselectable="true">
        <div class="panel panel-col-indigo">
            <div class="panel-heading" role="tab" id="headingOne_10">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseOne_10" aria-expanded="true" aria-controls="collapseOne_10" class="">
                        Filtre pour la recherche 
                    </a>
                </h4>
            </div>
            <div id="collapseOne_10" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_10" aria-expanded="true" style="">
                <div class="panel-body">
                    <div class="row" style="margin-left: -5px;margin-right: -5px;">
                        <div class="row clearfix">
                            <div class="col-md-3 ">
                                <p>
                                    <b>Date réception : </b>
                                </p>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="date-courrier" class="form-control recherche-avancee elem_filtre" name="daterange" value="2018/07/01 - 2018/07/01" style="background-color: white !important;"  />
                                    </div>
                                
                                </div>
                                

                            </div>
                            <div class="col-md-3">
                                <p>
                                    <b>Société : </b>
                                </p>
                                <select class="form-control show-tick elem_filtre" data-live-search="true" id="filtre-societe" multiple>
                                    <option value="1" selected>Bayard</option>
                                    <option value="2" selected>Milan</option>
                                    
                                </select>

                            </div>
                            <div class="col-md-3">
                                <p>
                                    <b>Source : </b>
                                </p>
                                <select class="form-control show-tick elem_filtre" data-live-search="true" id="filtre-source" onchange="setSource(1);">
                                    <option value="1">Mail </option>
                                    <option value="3">SFTP </option>
                                    <option value="2" selected>Courrier</option>
                                </select>

                            </div>
                            <div class="col-md-3">
                                <p>
                                    <b>Etape : </b>
                                </p>
                                <select class="form-control show-tick elem_filtre" data-live-search="true" id="filtre-etape" multiple>
                                
                                    <option value="1" >Typage </option>
                                    <option value="2">Saisie </option>
                                    <option value="3">Contrôle (Courrier)</option>
                                </select>
                            </div>
                        </div>


                        <div class="row clearfix" >
                            <div class="col-md-3 ">
                                <p>
                                    <b>Statut : </b>
                                </p>
                                <div class="form-group">
                                    <div class="form-line c-statut-pli">
                                       <?php echo $strEtatPli;?>
                                    </div>
                                
                                </div>
                                

                            </div>
                            <div class="col-md-3 ">
                                <p>
                                    <b>Typologie : </b>
                                </p>
                                <div class="form-group">
                                    <div class="form-line c-typologie-pli">
                                       
                                    </div>
                                
                                </div>
                                

                            </div>
                            
                            <div class="col-md-3">
                                <p>
                                    <b>&nbsp;</b>
                                </p>
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary waves-effect" onclick="afficherPliKO();">Rechercher</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button type="button" class="btn bg-blue-grey waves-effect" onclick="reinitialiser();">Réinitisaliser</button>
                                </div>
                            </div>
                        </div>


                            <!-- fin liste input-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- fin filtre -->


<!-- TODO Traitement KO bloc anomalie -->
<!-- info-box courrier -->
<div class="bloc-courrier pave-ko" style="display:none !important;">
    <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
        <div class="row ged-traitement-ko-bloc" style="display: block;">
        <?php echo $strCenterDivCdn; ?>
        <div class="col-lg-2">
            <div class="info-box bg-cyan-t hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">print</i>
                </div>
                <div class="content">
                    <div class="text">KO SCAN</div>
                    <span class="info-text">Nbr. : <span id="pli-ko-scan" class="value-span">0</span></span>
                    
                </div>
            </div>
        </div>
        <div class="col-lg-2" <?php echo  $hideOtherCdn; ?> >
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">remove_circle</i>
                </div>
                <div class="content">
                    <div class="text">KO DEFINITIF</div>
                    <span class="info-text">Nbr. : <span id="pli-ko-definitif" class="value-span">0</span></span><br>
                </div>
            </div>
        </div>
        <div class="col-lg-2" <?php echo  $hideOtherCdn; ?>>
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">new_releases</i>
                </div>
                <div class="content">
                    <div class="text">KO INCONNU</div>
                    <span class="info-text">Nbr. : <span id="pli-ko-inconnu" class="value-span">0</span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-2" <?php echo  $hideOtherCdn; ?>>
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">feedback</i>
                </div>
                <div class="content">
                    <div class="text">KO SRC</div>
                    <span class="info-text">Nbr. : <span id="pli-ko-src" class="value-span">0</span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-2" <?php echo  $hideOtherCdn; ?>>
            <div class="info-box bg-red7 hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">pause</i>
                </div>
                <div class="content">
                    <div class="text">KO EN ATTENTE</div>
                    <span class="info-text">Nbr. : <span id="pli-ko-en-attente" class="value-span">0</span></span>            
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-red1 hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">attach_file</i>
                </div>
                <div class="content">
                    <div class="text">KO CIRCULAIRE</div>
                    <span class="info-text">Nbr. :  <span id="pli-ko-circulaire">-</span></span>            
                </div>
            </div>
        </div>
        
        <div class="col-lg-2" <?php echo  $hideOtherCdn; ?>>
            
        </div>

        <div class="col-lg-2" <?php echo  $hideOtherCdn; ?> >
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">hourglass_full</i>
                </div>
                <div class="content">
                    <div class="text str-ko-en-cours-by" style="font-size: 11px !important;">KO EN COURS BAYARD</div>
                    <span class="info-text">Nbr. :  <span id="pli-ko-en-cours-by">-</span></span>            
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-teal hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">verified_user</i>
                </div>
                <div class="content">
                    <div class="text">OK+CIRCULAIRE</div>
                    <span class="info-text">Nbr. :  <span id="pli-ok-circulaire">-</span></span>            
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-deep-purple hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">layers_clear</i>
                </div>
                <div class="content">
                    <div class="text">CI EDITEE</div>
                    <span class="info-text">Nbr. :  <span id="pli-ci-editee">-</span></span>            
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-blue-grey hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">send</i>
                </div>
                <div class="content">
                    <div class="text">CI ENVOYEE</div>
                    <span class="info-text">Nbr. :  <span id="pli-ci-envoyee">-</span></span>            
                </div>
            </div>
        </div>
        <?php echo $strCenterDivCdn; ?>
    </div>
    </div> 
</div>
<!-- KO MAIL SFTP !-->
<div class="bloc-ms pave-ko" style="display:none !important;" >
    <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12"  <?php echo  $hideOtherCdn; ?>>
        <div class="row ged-traitement-ko-bloc" style="display: block;">
        <div class="col-lg-2">
            <div class="info-box bg-cyan-t hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">info</i>
                </div>
                <div class="content">
                    <div class="text">KO : Mail anomalie</div>
                    <span class="info-text">Nbr. : <span id="ko-mail-anomalie" class="value-span">0</span></span>
                    
				</div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">attachment</i>
                </div>
                <div class="content">
                    <div class="text">KO : Anomalie PJ</div>
                    <span class="info-text">Nbr. : <span id="ko-anomalie-pj" class="value-span">0</span></span><br>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">location_off</i>
                </div>
                <div class="content">
                    <div class="text">Hors périmètre</div>
                    <span class="info-text">Nbr. : <span id="ko-hors-perimetre" class="value-span">0</span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">insert_drive_file</i>
                </div>
                <div class="content">
                    <div class="text">KO : Anomalie fichier</div>
                    <span class="info-text">Nbr. : <span id="ko-anomalie-fichier" class="value-span">0</span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-red7 hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">do_not_disturb_on</i>
                </div>
                <div class="content">
                    <div class="text">Rejeter</div>
                    <span class="info-text">Nbr. : <span id="ko-rejeter" class="value-span">0</span></span>            
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-red1 hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">send</i>
                </div>
                <div class="content">
                    <div class="text">KO transfert SRC</div>
                    <span class="info-text">Nbr. :  <span id="ko-transfert-src-ms">0</span></span>            
                </div>
            </div>
        </div>
        
        <div class="col-lg-2">
            
        </div>

        <div class="col-lg-2">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">error</i>
                </div>
                <div class="content">
                    <div class="text">KO Définitif</div>
                    <span class="info-text">Nbr. :  <span id="ko-definitif-ms">0</span></span>            
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-teal hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">new_releases</i>
                </div>
                <div class="content">
                    <div class="text">KO Inconnu</div>
                    <span class="info-text">Nbr. :  <span id="ko-inconnu-ms">0</span></span>            
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-deep-purple hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">pause</i>
                </div>
                <div class="content">
                    <div class="text">KO en attente</div>
                    <span class="info-text">Nbr. :  <span id="ko-en-attente-ms">0</span></span>            
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="info-box bg-blue-grey hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">hourglass_full</i>
                </div>
                <div class="content">
                    <div class="text">KO en cours Bayard</div>
                    <span class="info-text">Nbr. :  <span id="ko-en-cours-bayard-ms">0</span></span>            
                </div>
            </div>
        </div>
        
    </div>
    </div> 
</div>




<!-- fin info-box courrier -->

<!-- TODO Liste KO -->
<div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12 ">
    <div class="row ged-reception" style="display: block;">
    <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">	
		<div class="card">
			<div class="body">
				<div class="body contenu-liste-anomalie">
                    <table class="table table-bordered table-striped table-hover contenu-liste-anomalie-table" id="traitement-ko">
                        <thead>
                            <tr>
                                <!--<th width="10px" style="padding-left: 28px;"><span class="input-group-addon"><input type="checkbox" class="filled-in chk-col-grey" id="all" disabled><label for="all"></label></span></th>-->
                                <th  id="thead-table"> ID #</th>
                                <th  width="20px" >Société</th>
                                <th width="20px">Date réception</th>
                                <th width="20px">Date dernier statut KO</th>
                                <th width="20px">Source </th>
                                <th width="35px">Typologie</th>
                                <th width="35px">Etape</th>
                                <th width="20px">Statut</th>
                                <th >Motif KO</th>
                                
                                <?php echo $strThTable; ?>
                                <th width="20px">Date envoi CI</th>
                                <th width="10px">Commentaire</th>
                            </tr>
                        </thead>
                        
                        <tbody></tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
<!-- Fin Lite KO -->

</div>    

<div class="preloader-reception">
   <span>Chargement de données...</span>
</div>




 <!-- Model image pli --> 
 <div class="modal fade" id="modal-ttmt-ko-img-content" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" >
                        <br/>
                        Les documents du pli#<span id="id_pli_docs"></span> - <span id="typo_pli_docs"></span> 
                        <a href="" id="link_download_docs_zip" download="" type="button" class="btn btn-primary waves-effect m-t--10" title="T&eacute;l&eacute;charger">
                            <i class="material-icons">file_download</i>
                            <span>Zip</span>
                        </a>
                </h4>
            </div>
            <div class="modal-body" id="ttmt-ko-img-content">
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal confirmation ; et texte commentaire-->

<div class="modal fade" id="confirmation-ttmt-ko" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header modal-col-teal">
                <h4 class="modal-title" id="confirmation-ttmt-ko-label"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                     <!--   <div class="form-group form-float">
                            <div class="form-line">
                                <textarea name="description" cols="15" rows="3" class="form-control no-resize" id="motif-operateur" readonly></textarea>
                                <label class="form-label">Description du motif :</label>
                            </div>
                        </div>
                    -->
                        <b>Description du motif :</b>
                        <div class="input-group">
                            <div class="form-line">
                                <textarea name="description" cols="15" rows="3" class="form-control no-resize" id="motif-operateur" readonly></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                <div class="col-sm-12">
                  <!--  <div class="form-group form-float">
                        <div class="form-line">
                            <textarea name="description" cols="15" rows="3" class="form-control no-resize" id="consigne-pli"></textarea>
                            <label class="form-label">Consigne sur le pli :</label>
                        </div>
                    </div> -->

                    <b>Consigne :</b>
                        <div class="input-group">
                            <div class="form-line">
                                <textarea name="description" cols="15" rows="3" class="form-control no-resize" id="consigne-pli"></textarea>
                            </div>
                        </div>
                </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" id="enregistrer" class="btn btn-primary waves-effect" onclick="setStatutPli()">Valider</button>
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="type-utilisateur" value="<?php echo $userType; ?>"/>


<div class="preloader-reception">
<div class="preloader pl-size-xs">
                                    <div class="spinner-layer pl-red-grey">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
        <span>Chargement de données...</span>
    </div>




    <div class="hide_img" id="zone_img_temp"></div>








<!-- TODO Modal ajout commentaire dans traitement KO" -->

<div class="modal fade" id="mdl_comment_pli" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg_" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <div class="row">
               <div class="col-lg-8">
                  <h4 class="modal-title">Pli#<span id="mdl_comment_pli_id_pli"></span></h4>
               </div>
               <div class="col-lg-4 align-right">
                  <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
               </div>
            </div>
         </div>
         <div class="modal-body" id="mdl_comment_pli_body" style="max-height: 60vh; overflow-y: auto;"></div>
         <div class="modal-footer">
            <div class="row">
               <div class="col-lg-12">
                  <div class="input-group">
                     <div class="form-line">
                        <textarea rows="1" class="form-control no-resize auto-growth" placeholder="Saisir ici votre commentaire..." id="new_comment_pli"></textarea>
                     </div>
                     <span class="input-group-addon">
                     <button type="button" class="btn btn-default bg-light-green_ btn-circle-lg waves-effect waves-circle waves-float" onclick="add_comment_pli();">
                     <i class="material-icons">chat</i>
                     </button>
                     </span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- fin ajout commentaire-->