
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab5">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
         <div class="analytics-content" id="detail_stat">
        
		 <div class="body">
         <div class="table-responsive">
		<?php  
		
		$list_pli = '<table class="table table-bordered table-striped table-hover dataTable js-export-table" id="detail_pli">                      
		 <thead  class="custom_font">
		
            <tr>
                <th>Courrier du</th>
                <th>Numérisation du</th>
                <th>Lot Scan</th>
                <th>Pli</th>
                <th>Traitement du</th>
                <th>Type de pli</th>
                <th>Statut</th>
                <th>Nbr. Document</th>
                <th>Volume produit</th>
                <th>Clé pli</th>
            </tr>
        </thead> 
         <tbody class="custom_font">';
			 
				$j = 0;					
				for($j = 0; $j < count($list); $j++){
					$list[$j]["comment"] = ($list[$j]["comment"] == 'Non typé') ? '':$list[$j]["comment"];
					if($list[$j]["statut"] == 'Anomalie')
						$list[$j]["statut"] = $list[$j]["type_ko"];
				  $list_pli .= '<tr>
					 <td>'.$list[$j]["date_courrier"].'</td>
					 <td>'.$list[$j]["date_numerisation"].'</td>
					 <td>'.$list[$j]["lot_scan"].'</td>
					 <td>'.$list[$j]["pli"].'</td>
					 <td>'.$list[$j]["dt_event"].'</td>
					 <td>'.$list[$j]["comment"].'</td>
					 <td>'.$list[$j]["statut"].'</td>
					 <td>'.$list[$j]["nb_doc"].'</td>
					 <td>'.$list[$j]["vol_produit"].'</td>
					 <td>'.$list[$j]["id_pli"].'</td>
				</tr>';
					

				}
			
				
			$list_pli .= '</tbody>
                               	
		</table>';
			echo $list_pli ;
		 ?>
         </div>
         </div>
		</div>
		</div>
    </div>
	</div>
	
