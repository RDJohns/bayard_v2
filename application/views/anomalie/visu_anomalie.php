<script>
    var url_site = '<?php echo site_url(); ?>';
</script>
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="logo-pro">
                    <a href="index.html"><img class="main-logo" src="" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="header-top-area">
         </div> 
    </div>
    <br />
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="">
				<div class="sparkline13-hd" style="padding:5px 0px 0px 20px" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="main-sparkline13-hd">
					<div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin:0px 0px 20px 0px">
							<!--h1 style="color:#69B145"><i class="fa fa-envelope-square" style="color:#C62057"></i>
							Suivi des réceptions</h1-->  
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<!--span  style="float: right;margin-top:10px;">
								<ol class="breadcrumb">
									
									<li id="tab_plis">
										<a href="javascript:void(0);">
											<i class="fa fa-envelope"></i> Voir la liste des plis
										</a>
									</li>									
									<li id="tab_stat">
										<a onclick="get_reception_traitement()" style="cursor:pointer;">
											<i class="fa fa-bar-chart"></i> Voir la synthèse
										</a>
									</li>
									<li id="tab_tout">
									    <a href="javascript:void(0);">
											<i class="fa fa-home"></i> Voir le résultat initial
										 </a>
									</li>
								</ol>
							</span-->
						</div>
						</div>
					</div>
					
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<span class="pull-right" style="font-size:13px;margin-top:8px;"> Courrier du </span></div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group" nowrap="nowrap">
								<div class="input-daterange input-group" id="bs_datepicker_range_container">
									<div class="form-line">
										<input type="text" id="date_debut" name="date_debut" value="<?php /*echo date("d/m/Y"); */echo date('d/m/Y', strtotime('-7 days'));?>" class="form-control" placeholder="Date début...">
									</div>
										<span class="input-group-addon">au</span>
									<div class="form-line">
										<input type="text" id="date_fin" name="date_fin" value="<?php echo date("d/m/Y");?>" class="form-control" placeholder="Date fin...">
									</div>
                                 </div>								
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
							<div class="form-group">
								<div class="row clearfix">
								<select class="btn-group bootstrap-select form-control show-tick" id="select_statut" name="select_statut">
									<option value="">-- Etape --</option>
										
								</select>
								</div>	
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="form-group">
								 <div class="row clearfix">
									<select class="btn-group bootstrap-select form-control show-tick" id="select_stat_saisie" name="select_stat_saisie">
										<option value="">-- Statut --</option>
									</select>
								</div>	
							</div>
                        </div>
					
						<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
						</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<span class="pull-right" style="font-size:13px;margin-top:8px;"> Traitement du </span></div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group" nowrap="nowrap">
								<div class="input-daterange input-group" id="bs_datepicker_range_container">
									<div class="form-line">
										<input type="text" id="date_debut_ttt" name="date_debut_ttt" class="form-control" placeholder="Début traitement ...">
									</div>
										<span class="input-group-addon">au</span>
									<div class="form-line">
										<input type="text" id="date_fin_ttt" name="date_fin_ttt" class="form-control" placeholder="Fin traitement...">
									</div>
								 </div>								
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
							 <div class="form-group">
								 <div class="row clearfix">
									<select class="btn-group bootstrap-select form-control show-tick" id="select_mp" name="select_mp">
										<option value="">-- Mode de paiement --</option>
											
									</select>
								</div>	
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
							<div class="form-group">
								<div class="row clearfix">
								<select class="btn-group bootstrap-select form-control show-tick" id="select_soc" name="select_soc">
									<option value="">-- Société --</option>
										
								</select>
								</div>	
							</div>
						</div>
						
						<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
							 <div class="form-group">
								
									<button class="btn btn-primary waves-effect waves-light" id="stat_rech" onclick="charger_detail_plis()">
									<i class="fa fa-search"></i>&nbsp;Rechercher</button>									
									
							</div>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
							 <div class="form-group">
								
									<button class="btn btn-primary waves-effect waves-light" onclick="reinitialiser()">
									<i class="fa fa-eraser"></i>&nbsp;Réinitialiser</button>
									
							</div>
						</div>
					
					</div>
					<div id="result-rsrch" style="display: block;">					
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					    <div class="">
                            <div class="sparkline13-graph">
									<div class="datatable-dashv1-list custom-datatable-overright">
										<div id="toolbar">
											<!--select class="form-control dt-tb">
												<option value="">Export Basic</option>
												<option value="all">Export All</option>
												<option value="selected">Export Selected</option>
											</select-->
										</div>
									
									</div>
								</div>
							</div>
						</div>
					</div>
             
				
            </div>
        </div>
    </div>
	</br>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
		<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
		<em id="error_message">*Veuillez saisir les criteres de recherche</em>
		</div>
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab5" style="margin-top:50px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
         <div class="analytics-content" id="detail_stat">
        
		 <div class="body">
         <div class="table-responsive">		
              <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="anomalie_pli">
			  <thead  class="custom_font">
		
            <tr>
                <th>Société</th>
                <th>Courrier du</th>
				<th>Numérisation du</th>
                <th>Traitement du</th>
                <th>Mode de paiement</th>				
                <th>Etape</th>
                <th>Etat</th>
                <th>Statut</th>
                <th>Id lot</br>Advantage</th>
                <th>Mouve-</br>ment</th>
                <th>Lot Scan</th>
                <th>Pli</th>
                <th>Id pli</th>
                <th>Typologie</th>
            </tr>
        </thead> 
         <tbody class="custom_font">
		 </tbody>
                               	
		</table></div>
         </div>
		</div>
		</div>
    </div>
	</div>
    <div class="library-book-area mg-t-30">
        <div class="container-fluid">
        </div>
    </div>
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
    </div>
    <div class="courses-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
        <div class="footer-copyright-area" style="position: fixed !important; bottom:0 !important; width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright © 2018. All rights reserved DEV-SI VIVETIC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
