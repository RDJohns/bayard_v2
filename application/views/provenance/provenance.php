<br><br><br><br>
<div id="global-filtre">


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1 filtre">Courrier du </div>
            <div class="col-md-3">
                <div class="input-daterange input-group" id="bs_datepicker_range_container">
                    <div class="form-line">
                        <input type="text" class="form-control provenance-date" id="date-debut" value="<?php echo date( 'd/m/Y', strtotime( 'monday this week' ) );?>">
                    </div>
                    <span class="input-group-addon">au&nbsp;&nbsp;</span>
                    <div class="form-line">
                        <input type="text" class="form-control provenance-date" id="date-fin" value="<?php echo date( 'd/m/Y', strtotime( 'friday this week' ) );?>">
                    </div>
                    
                </div>
            </div>
            <div class="col-md-2">
                <select class="form-control show-tick" tabindex="-98" id="filtre-societe">
                    <option value="0" disabled >-- Choisir une socièté --</option>
                    <option value="1">Bayard</option>
                    <option value="2">Milan</option>
                </select>
            </div>
            <div class="col-md-1">
                    <button class="btn btn-primary waves-effect waves-light" id="stat_rech" onclick="chargerProvenance();">&nbsp;Rechercher</button>
            </div>
    </div>
 </div>
    <div class="preloader-provenance">
        <span>Chargment de données...</span>
    </div>


<div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 recap-provenance"  style="margin-top:5px;">
        <div class="card">
            <div class="header">
                    <h5><span><h5><span>Récapitulatif des provenances </span></h5></span></h5>
            </div>
            <div class="body body-recap-provenance">
                    
                    
            </div>
        </div>
 </div>

 <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 provenance-par-date"  style="margin-top:5px;">
        <div class="card">
            <div class="header">
                    <h5><span><h5><span>Provenance par date</span></h5></span></h5>
            </div>
            <div class="body body-provenance-date">
                    
                    
            </div>
        </div>
 </div>

 <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 provenance-par-typologie"  style="margin-top:5px;">
        <div class="card">
            <div class="header">
                    <h5><span><h5><span>Provenance par typologie</span></h5></span></h5>
            </div>
            <div class="body body-provenance-typologie">
                    
                    
            </div>
        </div>
 </div>


