<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table style="max-width: 100%;">
        <tr>
            <th>original</th>
            <th>corrigée</th>
        </tr>
        <tr>
            <td><img src="data:image/jpeg;base64,<?php echo base64_encode($images['recto_orig']); ?>" class="img_b64" /></td>
            <td><img src="data:image/jpeg;base64,<?php echo base64_encode($images['recto_fixed']); ?>" class="img_b64" /></td>
        </tr>
        <tr>
            <td><img src="data:image/jpeg;base64,<?php echo base64_encode($images['verso_orig']); ?>" class="img_b64" /></td>
            <td><img src="data:image/jpeg;base64,<?php echo base64_encode($images['verso_fixed']); ?>" class="img_b64" /></td>
        </tr>
    </table>
</body>
<style>
    .img_b64{
        max-width: 100%;
    }
</style>
</html>