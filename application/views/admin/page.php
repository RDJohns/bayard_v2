<?php defined('BASEPATH') OR exit('No direct script access allowed');

    if(!isset($additional_css)){$additional_css = '';}
    if(!isset($additional_js)){$additional_js = '';}
    if(!isset($page_titre)){$page_titre = '';}
    if(!isset($contenu)){$contenu = '';}

    $url_assets = base_url('assets/');

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>GED-BAYARD</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo $url_assets ?>images/favicon.ico?1" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="<?php echo $url_assets ?>font/font.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $url_assets ?>font/icon.css" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo $url_assets ?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo $url_assets ?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo $url_assets ?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Bootstrap Spinner Css -->
    <link href="<?php echo $url_assets ?>plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="<?php echo $url_assets ?>plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="<?php echo $url_assets ?>plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?php echo $url_assets ?>plugins/bootstrap-select/css/bootstrap-select.css?1" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo $url_assets ?>plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo $url_assets ?>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.min.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="<?php echo $url_assets ?>plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- nprogress Css -->
    <link rel="stylesheet" href="<?php echo $url_assets ?>plugins/nprogress/nprogress.css">
    
    <!-- qtip2 -->
	<link rel="stylesheet" href="<?php echo $url_assets ?>plugins/qtip2/css.min.css">

    <!-- Dropzone Css -->
    <link href="<?php echo $url_assets ?>plugins/dropzone/dropzone.css" rel="stylesheet">
    
    <!-- magnify Css -->
    <link href="<?php echo $url_assets ?>plugins/magnify/jquery.magnify.css" rel="stylesheet" />

	<!-- Morris Css >
    <link href="<?php echo base_url(); ?>template/plugins/morrisjs/morris.css" rel="stylesheet" /-->
    
    <!-- timeline Css -->
    <link href="<?php echo $url_assets ?>plugins/timeline/timeline.css" rel="stylesheet" />
    
    <!-- jspanel Css -->
    <link href="<?php echo $url_assets ?>plugins/jspanel/jspanel.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo $url_assets ?>css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo $url_assets ?>css/themes/all-themes.css" rel="stylesheet" />

	<link href="<?php echo $url_assets ?>css/admin/page.css" rel="stylesheet" />
	<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" rel="stylesheet" />

    <!-- my CSS -->
    <link href="<?php echo $url_assets ?>css/my_style.css?1" rel="stylesheet">
    <?php echo $additional_css; ?>

    <style>
        .dz-remove{
            color: white;
            background: #fb8888;
            font-weight: bold;
            border-radius: 24px;
            opacity: 0.4;
        }
        .dz-remove:hover{
            text-decoration: none;
            color: white;
            border-radius: 5px;
            background: #fc7070;
            opacity: 1;
        }
        a.dz-remove:link{
            text-decoration: none !important;
            color: white;
        }
    </style>

    <script>
        var chemin_site = '<?php echo site_url(); ?>';
        var date_fr_now = '<?php echo date('d/m/Y'); ?>';
    </script>

</head>

<body class="theme-indigo">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Chargement...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <a class="navbar-brand" href="">
                <img src="<?php echo base_url().'/assets/images/logo.png'?>" alt="logo" style="display: block;width: 36px !important;">
            </a>
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#"><?php echo ucfirst($page_titre); ?></a>
            </div>
            <?php   $CI =&get_instance();
                   // $actif =isset($menu_acif)?$menu_acif:null; 
                    $CI->menu->menu();
            ?>
            <!--div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">view_headline</i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header"><?php echo $login_user ?></li>
                            <li class="body">
                                <ul class="menu">
                                    <?php foreach ($links as $link): ?>
                                        <li>
                                            <a href="<?php echo $link['lien'] ?>">
                                                <div class="icon-circle bg-<?php echo $link['color'] ?>">
                                                    <i class="<?php echo $link['icon'] ?>"></i>
                                                </div>
                                                <div class="menu-info">
                                                    <h4><?php echo $link['titre'] ?></h4>
                                                </div>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                    <li>
                                        <a href="javascript:deconnexion();">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">input</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Deconnexion</h4>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div-->
        </div>
    </nav>
    <!-- #Top Bar -->

    <section class="content">
        <div class="container-fluid" id="ctnr_main">
            <!-- div class="block-header">
                <h2><?php echo ucfirst($page_titre); ?></h2>
            </div -->

            <!-- Body -->
            <?php echo $contenu; ?>
            <!-- End Body -->

        </div>
    </section>
    <!-- fin modale modale modification de mot de passe -->

    <!-- Jquery Core Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap-select/js/bootstrap-select.js?1"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap-notify/bootstrap-notify.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/node-waves/waves.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo $url_assets ?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-steps/jquery.steps.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/sweetalert/sweetalert.min.js"></script>

    <!-- qtip2 -->
    <script src="<?php echo $url_assets ?>plugins/qtip2/js.min.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/dropzone/dropzone.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.colVis.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Select Plugin Js -->
    <!-- <script src="<?php echo $url_assets ?>plugins/bootstrap-select/js/bootstrap-select.js"></script> -->

    <!-- Multi Select Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- nprogress js -->
    <script src="<?php echo $url_assets; ?>plugins/nprogress/nprogress.js"></script>

    <!-- nprogress js -->
    <script src="<?php echo $url_assets; ?>plugins/magnify/jquery.magnify.js"></script>
    
    <!-- pjpanel -->
    <script src="<?php echo $url_assets; ?>plugins/jspanel/jspanel.js"></script>
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/contextmenu/jspanel.contextmenu.js"></script>
    <!-- <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/datepicker/jspanel.datepicker.js"></script> -->
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/hint/jspanel.hint.js"></script>
    <!-- <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/layout/jspanel.layout.js"></script> -->
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/modal/jspanel.modal.js"></script>
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/tooltip/jspanel.tooltip.js"></script>
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/dock/jspanel.dock.js"></script>

	<!-- Morris Plugin Js >
    <script src="<?php echo base_url(); ?>template/plugins/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>template/plugins/morrisjs/morris.js"></script-->

    <!-- highchart Plugin Js -->
    <script src="<?php echo $url_assets; ?>plugins/highcharts/highcharts.js"></script>
    <script src="<?php echo $url_assets; ?>plugins/highcharts/exporting.js"></script>
    <script src="<?php echo $url_assets; ?>plugins/highcharts/export_data.js"></script>

    <!-- Demo Js -->
    <script src="<?php echo $url_assets ?>js/demo.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo $url_assets ?>js/admin.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/tables/jquery-datatable.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/ui/tooltips-popovers.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/ui/notifications.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/forms/basic-form-elements.js"></script>
    <!-- autocompletion -->
    <script src="<?php echo $url_assets ?>js/pages/autocompletion/jquery-ui.js"></script>
   
    <!-- viewjs -->
    <!-- <script src="<?php echo $url_assets ?>plugins/vuejs/vuejs_dev.js"></script> -->
    <script src="<?php echo $url_assets ?>plugins/vuejs/vuejs_prod.js"></script>

    <!-- my js -->
    <?php echo $additional_js; ?>
    <script>
        $(function(){
            main_init();
        });
        function main_init() {
            init_nsprogress();
            $('[data-toggle="tooltip"]').tooltip();
            $('.spinner input').keypress(function(e){
                if(((e.charCode<48)||(e.charCode>57))&&(e.charCode!=0)){
                    e.preventDefault();
                    e.stopPropagation();
		        }
            });
            $('.nb').keypress(function(e){
                if(((e.charCode<48)||(e.charCode>57))&&(e.charCode!=0)){
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
            $('.no_saisi').keypress(function(e){
                e.preventDefault();
                e.stopPropagation();
            });
            $('.no_space').keypress(function(e){
                if(e.charCode == 32 || e.charCode == 9){
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        }

        function init_nsprogress(){
            $( document ).ajaxStart(function() {
                NProgress.start();
            });
            
            $(document).ajaxStop(function() {
                NProgress.done();
            });
        }
        
        function activateNotificationAndTasksScroll() {

            var slimHeight = "65vh";
            if($("#relation-client").length)
            {
                var relationClient = $("#relation-client").val(); /*user relation client*/
                if(relationClient > 0 )
                {
                    slimHeight = "15vh";
                }
            }
            $('.navbar-right .dropdown-menu .body .menu').slimscroll({
                height: slimHeight,
                color: 'rgba(0,0,0,0.5)',
                size: '4px',
                alwaysVisible: false,
                borderRadius: '0',
                railBorderRadius: '0'
            });
        }

        function deconnexion(){
            swal({
				title: "CONFIRMATION",
				text: "Deconnexion",
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Oui",
				cancelButtonText: "Annuler!",
				showLoaderOnConfirm: true,
				closeOnConfirm: false
			}, function () {
				window.location.href = chemin_site+'/login';
            });
        }
    </script>

    <!-- Jquery Spinner Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-spinner/js/jquery.spinner.js"></script>

</body>

</html>
