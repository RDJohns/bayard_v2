<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php foreach ($lignes as $key => $ligne): ?>
    <tr>
        <?php
            $login = $ligne->login;
            $login .= is_numeric($ligne->matr_gpao) ? " [$ligne->matr_gpao]" : '';
        ?>
        <th class="tab_w_u_col_login"><?php echo $login ?></th>
        <td class="tab_w_u_col_tp"><?php echo $ligne->type_u ?></td>
        <?php
            $connecte = $ligne->connecte == 1 ? "<span class=\"badge bg-cyan\">Oui</span> <small>[$ligne->ip_log]</small>" : "<span class=\"badge bg-orange\">Non</span>";
        ?>
        <td class="tab_w_u_col_con"><?php echo $connecte ?></td>
        <?php
            $occup = $ligne->occup == 1 ? $ligne->type_trtment : '';
        ?>
        <td class="tab_w_u_col_ocup"><?php echo $occup ?></td>
        <td class="tab_w_u_col_fx"><?php echo $ligne->flux ?></td>
        <td class="tab_w_u_col_dt_recpt"><?php echo ($ligne->dt_reception == '' ? '' : (new DateTime($ligne->dt_reception))->format('d/m/Y')) ?></td>
        <?php
            $pli = '';
            if(is_numeric($ligne->id_pli_act)){
                $pli = "#$ligne->id_pli_act - $ligne->nom_societe - $ligne->typologie";
            }
            if($ligne->paiements != ''){
                $pli .= ' - '.$ligne->paiements;
            }
            $pli .= is_numeric($ligne->id_pli_act) ? " [$ligne->mouvement]" : '';
            if($ligne->id_flux == 1){
                $pli = '<a href="javascript:detail_pli('.$ligne->id_pli_act.')">'.$pli.'</a>';
            }
        ?>
        <td class="tab_w_u_col_soc"><?php echo $pli ?></td>
        <?php
            $duree = str_replace('days', 'jours', $ligne->duree);
            $duree = str_replace('day', 'jour', $duree);
            $duree = explode('.', $duree);
            $duree = isset($duree[0]) && $ligne->label_action != '' ? " <small>[$duree[0]]</small>" : ' <small>[Jamais]</small>';
            $act = $ligne->label_action.$duree;
        ?>
        <td class="tab_w_u_col_act"><?php echo $act ?></td>
    </tr>
<?php endforeach; ?>
