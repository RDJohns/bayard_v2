<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php echo $id_tb ?> = $('#<?php echo $id_tb ?>').DataTable({
    'pageLength': 10
    ,'bFilter': false
    ,dom: 'tip'
    ,"ordering": false
    ,'bprocessing': true
    ,'bServerSide': true
    ,'oLanguage': {
        'sInfo': "de _START_ \340 _END_ sur _TOTAL_ lignes"
        ,'sProcessing': 'Traitement en cours...',
        'oPaginate': {
            'sFirst': 'PREMIERE',
            'sPrevious': 'PRECEDENTE',
            'sNext': 'SUIVANTE',
            'sLast': 'DERNIERE'
        }
    }
    ,'ajax': {
        'url': chemin_site + '/admin/users/dttbl_histo_user',
        'type': 'POST',
        'data': function(d) {
            d.id_user = <?php echo $id_user ?>
        }
    }
    ,'columns': [
        { 'data': 'date_action', 'targets': [0] },
        { 'data': 'action', 'targets': [1] },
        { 'data': 'flux', 'targets': [2] },
        { 'data': 'pli', 'targets': [3] },
        { 'data': 'ip', 'targets': [4] }
    ]
});