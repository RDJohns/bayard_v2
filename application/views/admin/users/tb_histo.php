<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="table-responsives">
    <table class="table table-hover table-striped" id="<?php echo $id_tb ?>" >
        <thead>
            <tr>
                <th></th>
                <th>Action</th>
                <th>Flux</th>
                <th>Pli</th>
                <th>IP</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
