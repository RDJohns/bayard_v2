<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card card_filtre">
            <div class="body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Traitement du : </b></div>
                            <div class="col-xs-4 align-center col_hx">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_act_1" />
                                        <label class="form-label">debut</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center col_hx">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_act_2" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Soci&eacute;t&eacute; :</b></div>
                            <div class="col-xs-9 col_hx_">
                                <select class="form-control show-tick" id="sel_soc" multiple data-live-search="true" title="Toutes" data-size="7" onchange="">
                                    <?php foreach ($socities as $socity) : ?>
                                        <option value="<?php echo $socity->id ?>"><?php echo $socity->nom_societe ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Source :</b></div>
                            <div class="col-xs-9 col_hx_">
                                <select class="form-control show-tick" id="sel_src" multiple data-live-search="true" title="Toutes" data-size="5" onchange="">
                                    <?php foreach ($sources as $source) : ?>
                                        <option value="<?php echo $source['id'] ?>"><?php echo $source['titre'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Typologie :</b></div>
                            <div class="col-xs-9 col_hx_">
                                <select class="form-control show-tick" id="sel_typo" multiple data-live-search="true" title="Toutes" data-size="7" onchange="">
                                    <?php foreach ($types as $type) : ?>
                                        <option value="<?php echo $type->id ?>" data-subtext="<?php echo ' - ' . $type->source ?>"><?php echo $type->titre ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Mode de paiement :</b></div>
                            <div class="col-xs-9 col_hx_">
                                <select class="form-control show-tick" id="sel_md_p" multiple data-live-search="true" title="Toutes" data-size="7" onchange="">
                                    <?php //foreach ($md_paiements as $md_paiement) : ?>
                                    <option value="<?php //echo $md_paiement->id_mode_paiement ?>"><?php //echo $md_paiement->mode_paiement ?></option>
                                    <?php //endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Statut :</b></div>
                            <div class="col-xs-9">
                                <select class="form-control show-tick" id="sel_state" multiple data-live-search="true" title="Tous" data-size="7" onchange="">
                                    <?php foreach ($status as $statu) : ?>
                                        <option value="<?php echo $statu->id ?>" data-subtext="<?php echo ' - ' . $statu->source ?>"><?php echo $statu->titre ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Op&eacute;rateur :</b></div>
                            <div class="col-xs-9">
                                <select class="form-control show-tick" id="sel_op" multiple data-live-search="true" title="Tous" data-size="7" onchange="">
                                    <?php foreach ($ops as $op) : ?>
                                        <option value="<?php echo $op->id_utilisateur ?>" data-subtext="<?php echo ' - '.$op->matr_gpao ?>"><?php echo $op->login ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="txt_recherche_id_pli" onkeyup="" />
                                <label class="form-label">Rechercher: #ID Pli</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30_ frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="txt_recherche_cmd" onkeyup="//load_table();" />
                                <label class="form-label">Rechercher: Commande</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="button" class="btn btn-block btn-info btn-lg waves-effect m-t-20" onclick="load_data();">
                                    <i class="material-icons">search</i>
                                    <span>Rechercher / Rafraichir</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#typage" data-toggle="tab">
                                    <i class="material-icons">style</i> TYPAGE
                                    <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" onclick="export_xl('export_typage_xl');" title="Export-Excel">
                                        <i class="material-icons">file_download</i>
                                    </button>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#saisie" data-toggle="tab">
                                    <i class="material-icons">border_color</i> SAISIE
                                    <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" onclick="export_xl('export_saisie_xl');" title="Export-Excel">
                                        <i class="material-icons">file_download</i>
                                    </button>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#ctrl" data-toggle="tab">
                                    <i class="material-icons">done_all</i> CONTR&Ocirc;LE
                                    <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" onclick="export_xl('export_ctrl_xl');" title="Export-Excel">
                                        <i class="material-icons">file_download</i>
                                    </button>
                                </a>
                            </li>
                        </ul>
                
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="typage">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="dmt_typage_dtt" class="table table-bordered table-striped table-hover dttbl" >
                                            <thead>
                                                <tr>
                                                    <th class="align-center">#ID.GED</th>
                                                    <th class="align-center">Source</th>
                                                    <th class="align-center">Soci&eacute;t&eacute;</th>
                                                    <th class="align-center">Type</th>
                                                    <th class="align-center">Nb.Mouvement</th>
                                                    <!-- <th class="align-center">Paiement</th> -->
                                                    <th class="align-center">Statut</th>
                                                    <th class="align-center">D&eacute;but</th>
                                                    <th class="align-center">Fin</th>
                                                    <th class="align-center">Dur&eacute;e</th>
                                                    <th class="align-center">Par</th>
                                                    <th class="align-center">Commande</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="saisie">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="dmt_saisie_dtt" class="table table-bordered table-striped table-hover dttbl" >
                                            <thead>
                                                <tr>
                                                    <th class="align-center">#ID.GED</th>
                                                    <th class="align-center">Source</th>
                                                    <th class="align-center">Soci&eacute;t&eacute;</th>
                                                    <th class="align-center">Type</th>
                                                    <th class="align-center">Nb.Mouvement</th>
                                                    <!-- <th class="align-center">Paiement</th> -->
                                                    <th class="align-center">Statut</th>
                                                    <th class="align-center">D&eacute;but</th>
                                                    <th class="align-center">Fin</th>
                                                    <th class="align-center">Dur&eacute;e</th>
                                                    <th class="align-center">Par</th>
                                                    <th class="align-center">Commande</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="ctrl">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="dmt_ctrl_dtt" class="table table-bordered table-striped table-hover dttbl" >
                                            <thead>
                                                <tr>
                                                    <th class="align-center">#ID.GED</th>
                                                    <th class="align-center">Source</th>
                                                    <th class="align-center">Soci&eacute;t&eacute;</th>
                                                    <th class="align-center">Type</th>
                                                    <th class="align-center">Nb.Mouvement</th>
                                                    <!-- <th class="align-center">Paiement</th> -->
                                                    <th class="align-center">Statut</th>
                                                    <th class="align-center">D&eacute;but</th>
                                                    <th class="align-center">Fin</th>
                                                    <th class="align-center">Dur&eacute;e</th>
                                                    <th class="align-center">Par</th>
                                                    <th class="align-center">Commande</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
