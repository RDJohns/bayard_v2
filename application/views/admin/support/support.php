<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card card_filtre">
            <div class="body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>&Eacute;tape de traitement :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_stat" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php //foreach ($stats as $stat): ?>
                            <!-- <option value="<?php /*echo $stat->id_flag_traitement*/ ?>" data-subtext="<?php /*echo ' - '.$stat->comment*/ ?>" ><?php /*echo $stat->traitement*/ ?></option> -->
                            <?php //endforeach; ?>
                            <?php $grp = ''; $ln = 0; ?>
                            <?php foreach ($stats as $step) : ?>
                                <?php if($grp != $step->flgtt_etape){
                                    if($ln > 0){echo '</optgroup>';}
                                    $grp = $step->flgtt_etape;
                                    $ln++; ?>
                                    <optgroup label="<?php echo $step->flgtt_etape ?>">
                                <?php } ?>
                                <option value="<?php echo $step->id_flag_traitement ?>" data-subtext="<?php echo ' - ' . $step->flgtt_etape ?>"><?php echo $step->flgtt_libelle_etape ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Statut :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_status" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($status as $statu): ?>
                            <option value="<?php echo $statu->id_statut_saisie ?>" data-subtext="<?php echo ' - '.$statu->code ?>" ><?php echo $statu->libelle ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Typ&eacute; par :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_type_par" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($operators as $operator): ?>
                            <option value="<?php echo $operator->id_utilisateur ?>" ><?php echo $operator->login ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <p><b>Saisi par :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_saisie_par" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($operators as $operator): ?>
                            <option value="<?php echo $operator->id_utilisateur ?>" ><?php echo $operator->login ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <p><b>Niveau saisie :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_niveau_saisie" multiple data-live-search="true" title="Tous" data-size="3" onchange="//load_table();" >
                            <option value="1" >Niveau 1</option>
                            <option value="2" >Niveau 2</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Contr&ocirc;l&eacute; par :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_control_par" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($controllers as $controller): ?>
                            <option value="<?php echo $controller->id_utilisateur ?>" ><?php echo $controller->login ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Retrait&eacute; par :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_retrait_par" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($controllers as $controller): ?>
                            <option value="<?php echo $controller->id_utilisateur ?>" ><?php echo $controller->login ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Typologie :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_typo" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($types as $type): ?>
                            <option value="<?php echo $type->id ?>" data-subtext="<?php echo ' - ' . $type->sous_lot ?>"><?php echo $type->typologie ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <p><b>Mode de paiement :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_md_p" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($md_paiements as $md_paiement): ?>
                            <option value="<?php echo $md_paiement->id_mode_paiement ?>"><?php echo $md_paiement->mode_paiement ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <p><b>Soci&eacute;t&eacute; :</b></p>
                        <select class="form-control show-tick elem_filtre" id="sel_soc" multiple data-live-search="true" title="Toutes" data-size="2" onchange="">
                            <?php foreach ($socities as $socity) : ?>
                            <option value="<?php echo $socity->id ?>"><?php echo $socity->nom_societe ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control elem_filtre" id="txt_recherche_id_pli" onkeyup="//load_table();" />
                                <label class="form-label">Rechercher pli#ID</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control elem_filtre" id="txt_recherche_cmc7" onkeyup="//load_table();" />
                                <label class="form-label">Rechercher CMC7</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control elem_filtre" id="txt_recherche_lotnum" onkeyup="//load_table();" />
                                <label class="form-label">Rechercher Lot.Num/Commande</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float m-t-30 frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control elem_filtre" id="txt_recherche_advent" onkeyup="//load_table();" />
                                <label class="form-label">Rechercher Lot.saisie(Advantage)</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <button type="button" class="btn btn-block btn-info btn-lg waves-effect m-t-20" onclick="load_table();">
                            <i class="material-icons">search</i>
                            <span>Rechercher / Rafraichir</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <table id="support_dtt" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="align-center">#ID</th>
                            <th class="align-center">Pli</th>
                            <th class="align-center">Lot.Num&eacute;</th>
                            <th class="align-center">Soci&eacute;t&eacute;</th>
                            <th class="align-center">&Eacute;tape</th>
                            <th class="align-center">&Eacute;tat</th>
                            <th class="align-center">Statut</th>
                            <th class="align-center">Prioris&eacute;</th>
                            <th class="align-center">Typ&eacute; par</th>
                            <th class="align-center">Type</th>
                            <th class="align-center">Lot.Saisie</th>
                            <th class="align-center">Saisie par</th>
                            <th class="align-center">Control&eacute; par</th>
                            <th class="align-center">Retrait&eacute; par</th>
                            <th class="align-center">Paiement</th>
                            <th class="align-center">CMC7</th>
                            <th class="align-center">Circulaire</th>
                            <th class="align-center">Commande</th>
                            <th class="align-center">Niveau saisie</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="hide_img" id="zone_img_temp"></div>
<!-- modale division -->
<div class="modal fade" id="mdl_diviser_pli" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Diviser le pli#<span id="mdl_diviser_pli_title"></span></h4>
            </div>
            <div class="modal-body" id="mdl_diviser_pli_body"></div>
            <div class="modal-footer">
                <button type="button" id="mdl_diviser_pli_bt" class="btn btn-warning waves-effect" onclick="">
                    <i class="material-icons">check</i>
                    <span>Valider</span>
                </button>
                <button type="button" class="btn btn-success waves-effect" data-dismiss="modal">
                    <i class="material-icons">backspace</i>
                    <span>Annuler</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_display_pli" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pli#<span id="mdl_display_pli_title"></span></h4>
            </div>
            <div class="modal-body" id="mdl_display_pli_body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_display_group_pli" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Regroupements disponibles</h4>
            </div>
            <div class="modal-body" id="mdl_display_group_pli_body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_display_group_pli_by_older" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Regroupements disponibles par date-courrier</h4>
            </div>
            <div class="modal-body" id="mdl_display_group_pli_by_older_body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_display_histo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pli#<span id="mdl_display_histo_title"></span></h4>
            </div>
            <div class="modal-body" id="mdl_display_histo_body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_ko_scan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Mise en KO-Scan du Pli#<span id="mdl_ko_scan_id_pli"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <p><b>Motif :</b></p>
                        <select class="form-control show-tick" id="sel_motif_ko_scan" data-live-search="true" data-size="7" onchange="//load_table();" >
                            <?php foreach ($motifs_rejet as $motif): ?>
                            <option value="<?php echo $motif->id ?>" ><?php echo $motif->motif ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-12 m-t-25">
                        <label for="comment_ko_scan">Commentaire :</label>
                        <div class="form-group">
                            <div class="form-line">
                                <textarea rows="1" class="form-control no-resize auto-growth" id="comment_ko_scan" placeholder="Appuyer sur la touche <Entr&eacute;e> pour aller &agrave; la ligne..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="mdl_ko_scan_bt" class="btn btn-link waves-effect" onclick="">VALIDER</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">ANNULER</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_disp_motif_consigne_pli" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg_" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-lg-8">
                        <h4 class="modal-title">Motif-consignes du Pli#<span id="mdl_disp_motif_consigne_pli_id_pli"></span></h4>
                    </div>
                    <div class="col-lg-4 align-right">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                    </div>
                </div>
            </div>
            <div class="modal-body" id="mdl_disp_motif_consigne_pli_body" style="max-height: 60vh; overflow-y: auto;"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_assignation_pli" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="content_assignation_pli_modal">
            <div class="modal-header">
                <h4 class="modal-title">Assignation du Pli#{{id_pli}}</span></h4>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <p><b>Typage :</b></p>
                        <select class="form-control show-tick sel_select_op" data-live-search="true" data-size="10" v-model="op_typage">
                            <option value="" data-subtext="" >Aucun</option>
                            <option v-for="option in users" v-bind:key="option.id_utilisateur" :value="option.id_utilisateur" :data-subtext="' '+option.matr_gpao" >{{ option.login }}</option>
                        </select>
                        <p class="align-center" v-if="with_typage"><i class="material-icons col-red" style="cursor: pointer;" @click="vide_typage" title="effacer">clear</i></p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <p><b>Saisie :</b></p>
                        <select class="form-control show-tick sel_select_op" data-live-search="true" data-size="10" v-model="op_saisie">
                            <option value="" data-subtext="" >Aucun</option>
                            <option v-for="option in users" v-bind:key="option.id_utilisateur" :value="option.id_utilisateur" :data-subtext="' '+option.matr_gpao" >{{ option.login }}</option>
                        </select>
                        <p class="align-center" v-if="with_saisie"><i class="material-icons col-red" style="cursor: pointer;" @click="vide_saisie" title="effacer">clear</i></p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <p><b>Contr&ocirc;le :</b></p>
                        <select class="form-control show-tick sel_select_op" data-live-search="true" data-size="10" v-model="op_ctrl">
                            <option value="" data-subtext="" >Aucun</option>
                            <option v-for="option in users_ctrl" v-bind:key="option.id_utilisateur" :value="option.id_utilisateur" :data-subtext="' '+option.matr_gpao" >{{ option.login }}</option>
                        </select>
                        <p class="align-center" v-if="with_ctrl"><i class="material-icons col-red" style="cursor: pointer;" @click="vide_ctrl" title="effacer">clear</i></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="" class="btn btn-link waves-effect" v-on:click="enregistrer">VALIDER</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" >ANNULER</button>
            </div>
        </div>
    </div>
</div>

<div id="html_loader" class="hidden">
    <div class="row m-t-25">
        <div class="col-xs-12 align-center">
            <div class="preloader pl-size-xl">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #support_dtt_length{
        display: inline !important;
        margin-left: 50px !important;
    }
</style>
