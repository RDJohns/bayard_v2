<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if (count($grps) > 0): ?>
<div class="row row_list_regroupement" id="row_list_regroupement">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th class="align-center">Date courrier</th>
                <th class="align-center">Soci&eacute;t&eacute;</th>
                <th class="align-center">Type de pli</th>
                <th class="align-center">Type de paiement</th>
                <th class="align-center">En attente de saisie</th></th>
                <th class="align-center">Rejet BATCH</th></th>
                <th class="align-center">Niveau 2</th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($grps as $grp): ?>
            <tr>
                <td><?php echo ($grp->dt_cr == '' ? '' : (new DateTime($grp->dt_cr))->format('d/m/Y')) ?></td>
                <td><?php echo $grp->soc ?></td>
                <td><?php echo $grp->typo ?></td>
                <td><?php echo $grp->paie ?></td>
                <td class="align-center"><?php echo $grp->nb_w ?></td>
                <td class="align-center"><?php echo $grp->nb_w_rj_cba ?></td>
                <td class="align-center"><?php echo $grp->nb_n2 ?></td>
                <?php $totaux_wait += $grp->nb_w; ?>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td class="align-center font-bold" colspan="4">TOTAL</td>
                <td class="align-center font-bold"><?php echo $totaux_wait ?></td>
            </tr>
        </tbody>
    </table>
</div>
<?php else: ?>
<div class="row">
    <div class="col-xs-12 align-center">
        <div class="alert alert-info">
            Aucun regroupement de pli!
        </div>
    </div>
</div>
<?php endif; ?>
