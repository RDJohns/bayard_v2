<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-8 align-right">
        <p><b>Diviser le pli<small class="col-pink font-italic">#<?php echo $pli->id_pli ?></small>  en :</b></p>
    </div>
    <div class="col-lg-4 align-left">
        <select class="form-control show-tick sel_div_pli" id="sel_div_pli" data-live-search="false" data-size="10" onchange="generer_fils_pli();" >
            <?php $selected = 'selected'; ?>
            <?php for($nb_div = 2; $nb_div <= $nb_doc; $nb_div++){ ?>
                <option value="<?php echo $nb_div ?>" <?php echo $selected ?> data-subtext=" plis" ><?php echo $nb_div ?></option>
                <?php $selected = ''; ?>
            <?php } ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 table-responsive" style="max-height:75vh;overflow-y:auto;">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="align-center">Documents</th>
                    <th class="align-center"></th>
                    <th class="align-center">Plis</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($docs as $doc): ?>
                <tr>
                    <td class="align-left">Document #<?php echo $doc->id_document ?></td>
                    <td class="align-left">
                        <button type="button" class="btn bg-purple btn-circle waves-effect waves-circle waves-float" onclick="load_img_doc('<?php echo $doc->id_document ?>');" data-toggle="tooltip" data-placement="top" title="Aperçu">
                            <i class="material-icons">search</i>
                        </button>
                    </td>
                    <td class="align-center">
                        <div class="row">
                            <div class="col-lg-12">
                            <select class="form-control show-tick sel_div_pli sel_div_doc" id="sel_div_doc_<?php echo $doc->id_document ?>" data-live-search="false" data-size="10" my_id_doc="<?php echo $doc->id_document ?>" data-container="body" >
                                <option value="1">Pli 1</option>
                                <option value="2">Pli 2</option>
                            </select>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
