<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<img id="img_temp_recto" src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_recto) == '' ? $empty_b64 : $doc->n_ima_base64_recto ); ?>" data-magnify="gallery" data-caption="<?php echo 'Document#'.$doc->id_document.' / RECTO['.$doc->n_ima_recto.']'; ?>" data-group="<?php echo $doc->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_recto) == '' ? $empty_b64 : $doc->n_ima_base64_recto ); ?>" />
<img id="img_temp_verso" src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_verso) == '' ? $empty_b64 : $doc->n_ima_base64_verso ); ?>" data-magnify="gallery" data-caption="<?php echo 'Document#'.$doc->id_document.' / VERSO['.$doc->n_ima_verso.']'; ?>" data-group="<?php echo $doc->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_verso) == '' ? $empty_b64 : $doc->n_ima_base64_verso ); ?>" />
