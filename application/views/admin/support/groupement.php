<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if (count($grps) > 0): ?>
<div class="row row_list_regroupement" id="row_list_regroupement">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th class="align-center">Soci&eacute;t&eacute;</th>
                <th class="align-center">Type de pli</th>
                <th class="align-center">Type de paiement</th>
                <th class="align-center">En attente de saisie<br><small>(Rejet BATCH - Nv2)</small></th>
                <th class="align-center">En cours de saisie</th>
                <th class="align-center">TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($grps as $grp): ?>
            <tr>
                <td><?php echo $grp->soc ?></td>
                <td><?php echo $grp->typo ?></td>
                <td><?php echo $grp->paie ?></td>
                <td class="align-center"><?php echo $grp->nb_w . ' <small>('.$grp->nb_w_rj_cba . ' - ' . $grp->nb_n2 . ')</small>' ?></td>
                <td class="align-center"><?php echo $grp->nb_s ?></td>
                <td class="align-center"><?php echo $grp->total ?></td>
                <?php $totaux_wait += $grp->nb_w; ?>
                <?php $totaux_current += $grp->nb_s; ?>
                <?php $totaux += $grp->total; ?>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td class="align-center font-bold" colspan="3">TOTAUX</td>
                <td class="align-center font-bold"><?php echo $totaux_wait ?></td>
                <td class="align-center font-bold"><?php echo $totaux_current ?></td>
                <td class="align-center font-bold"><?php echo $totaux ?></td>
            </tr>
        </tbody>
    </table>
</div>
<?php else: ?>
<div class="row">
    <div class="col-xs-12 align-center">
        <div class="alert alert-info">
            Aucun regroupement de pli!
        </div>
    </div>
</div>
<?php endif; ?>
