<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card card_filtre card_m_b_10">
            <div class="body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Contr&ocirc;le du : </b></div>
                            <div class="col-xs-4 align-center col_hx">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_ctrl_1" />
                                        <label class="form-label">debut</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center col_hx">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_ctrl_2" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <button type="button" class="btn btn-block btn-info btn-lg waves-effect m-t-20" onclick="load_data_fautes();">
                            <i class="material-icons">search</i>
                            <span>Rechercher / Rafraichir</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12" id="zone_data_faute">
        
    </div>
</div>

<div id="html_loader" class="hidden">
        <div class="row m-t-25">
            <div class="col-xs-12 align-center">
                <div class="preloader pl-size-xl">
                    <div class="spinner-layer pl-light-blue">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
