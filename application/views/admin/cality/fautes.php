<?php defined('BASEPATH') OR exit('No direct script access allowed');
    $identifiant = date('U').mt_rand();
    $total_nb_faute = 0;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="card card_m_b_10">
            <div class="header">
                <h2><?php echo $titre ?></h2>
                <ul class="header-dropdown m-r--5">
                    <button type="button" class="btn btn-primary waves-effect m-t--10" onclick="fautes_export_xl('<?php echo $taux ?>');" title="Export-Excel">
                        <i class="material-icons">file_download</i>
                        <span>Excel</span>
                    </button>
                </ul>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div id="graph_prc_fautes_<?php echo $identifiant ?>" class="grph_faute" my_fonct="init_graph_fautes_<?php echo $identifiant ?>">
                            <?php if($total < 1){ ?> <h4>Aucun pli contrôlé.</h4> <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Faute</th>
                                        <th>Nombre</th>
                                        <th>Taux</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($fautes as $key => $faute): ?>
                                    <tr>
                                        <td><?php echo ($key+1) ?></td>
                                        <td><?php echo $faute->err ?></td>
                                        <td><?php echo $faute->nb ?></td><?php $total_nb_faute += $faute->nb; ?>
                                        <td><?php echo (integer)$faute->prc ?>%</td>
                                        <td style="width:30%;">
                                            <div class="progress">
                                                <div class="progress-bar bg-green" role="progressbar" aria-valuenow="<?php echo $faute->prc ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $faute->prc ?>%;">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td colspan="2"><span class="font-bold">Total</span></td>
                                        <td colspan="2"><span class="font-bold"><?php echo $total_nb_faute ?></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if($total > 0){ ?>
<script>
    $(function() {
        try {
            init_graph_fautes_<?php echo $identifiant ?>();
        } catch (err) {
            console.log('Population zero!');
        }
    });
    function init_graph_fautes_<?php echo $identifiant ?>() {
        Highcharts.chart('graph_prc_fautes_<?php echo $identifiant ?>', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            colors: ['#7cb5ec', '#f00'],
            title: {
                text: 'Sur <?php echo $total ?><br>plis contr\364l\351s',
                align: 'center',
                verticalAlign: 'middle',
                y: 60
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white'
                        }
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%'],
                    size: '110%'
                }
            },
            series: [{
                type: 'pie',
                name: 'Plis',
                innerSize: '50%',
                data: [
                    ['OK (<?php echo ($sans) ?>)', <?php echo ($sans * 100 / $total) ?>],
                    ['KO (<?php echo ($avec) ?>)', <?php echo ($avec * 100 / $total) ?>]
                ]
            }]
        });
    }
</script>
<?php } ?>
