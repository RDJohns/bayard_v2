<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="icon-and-text-button-demo parti_bouton">
    <button type="button" class="btn bg-light-green waves-effect" data-toggle="modal" data-target="#add_group_modal">
        <i class="material-icons">group_add</i>
        <span>Nouveau groupe utilisateur</span>
    </button>
</div>

<div class="row clearfix">
    <div class="col-xs-12">
        <div class="card">
            
            <div class="header">
                <h2>Groupes &eacute;xistants&nbsp;
                    <button type="button" class="btn btn-default waves-effect" onclick="load_table();" data-toggle="tooltip" data-placement="top" title="Rafraichir">
                        <i class="material-icons">autorenew</i>
                    </button>
                </h2>
            </div>
            
            <div class="body table-responsive">
                <table id="group_admin_datatable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="align-center">Nom</th>
                            <th class="align-center">Crit&egrave;res</th>
                            <th class="align-center">Nombres Crit&egrave;res</th>
                            <th class="align-center">Nombres membres</th>
                            <th class="align-center">Actif</th>
                            <th class="align-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</div>

<!-- Modales -->
<div class="modal fade" id="add_group_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Ajout d'un nouveau groupe</h4>
            </div>
            <div class="modal-body">
                <form id="frm_add">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" id="add_gr_nom" class="form-control">
                            <label class="form-label">Nom</label>
                        </div>
                    </div>
                    <div class="switch m-t-15">
                        <label><input id="add_gr_actif" type="checkbox" checked><span class="lever"></span>Actif</label>
                    </div>
                    <div class="zone_sel_gr_poss m-t-25">
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="form-label">Regroupement de pli:</label>
                            </div>
                            <div class="col-lg-6 m-t-15">
                                <select class="form-control show-tick" id="sel_add_gr_poss_by" onchange="load_multi_select($(this).attr('id'));">
                                    <option value="typologie" selected>Par typologie de pli</option>
                                    <option value="soc">Par soci&eacute;t&eacute;</option>
                                    <option value="id_groupe_paiement">Par type de paiement</option>
                                </select>
                            </div>
                            <div class="col-lg-12 m-t-25">
                                <select id="sel_add_gr_poss" class="ms- sel_gr_poss" multiple="multiple">
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect" onclick="send_new_group();">Enregistrer</button>
                <button type="button" class="btn btn-success waves-effect" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>
<!---------->
<div class="modal fade" id="modif_group_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Modification de groupe</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" id="modif_gr_nom" class="form-control">
                            <label class="form-label">Nom</label>
                        </div>
                    </div>
                    <div class="switch m-t-15">
                        <label><input id="modif_gr_actif" type="checkbox" checked><span class="lever"></span>Actif</label>
                    </div>
                    <div class="zone_sel_gr_poss m-t-25">
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="form-label">Regroupement de pli:</label>
                            </div>
                            <div class="col-lg-6 m-t-15">
                                <select class="form-control show-tick" id="sel_modif_gr_poss_by" onchange="load_multi_select($(this).attr('id'));">
                                    <option value="typologie" selected>Par typologie de pli</option>
                                    <option value="soc">Par soci&eacute;t&eacute;</option>
                                    <option value="id_groupe_paiement">Par type de paiement</option>
                                </select>
                            </div>
                            <div class="col-lg-12 m-t-25">
                                <select id="sel_modif_gr_poss" class="ms- sel_gr_poss" multiple="multiple">
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="modif_gr_bt_ok" type="button" class="btn btn-primary waves-effect" onclick="">Enregistrer</button>
                <button type="button" class="btn btn-success waves-effect" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>
<!---------->
<div class="modal fade" id="membre_group_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Les membres du groupe <small id="ident_member_gr_modal"></small></h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="zone_sel_member m-t-25">
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="form-label">Op&eacute;rateurs:</label>
                            </div>
                            <div class="col-lg-12 m-t-25">
                                <select id="sel_member_gr_modal" class="ms-" multiple="multiple">
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="member_gr_bt_ok_modal" type="button" class="btn btn-primary waves-effect" onclick="">Enregistrer</button>
                <button type="button" class="btn btn-success waves-effect" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modales -->
