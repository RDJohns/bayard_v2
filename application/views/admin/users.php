<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="icon-and-text-button-demo parti_bouton">
    <button type="button" class="btn bg-light-green waves-effect" data-toggle="modal" data-target="#add_user_modal">
        <i class="material-icons">person_add</i>
        <span>Ajouter un utilisateur</span>
    </button>
    <button type="button" class="btn bg-light-blue waves-effect" onclick="app_wallboard_user.affiche_modal();">
        <i class="material-icons">view_list</i>
        <span>Activit&eacute;s</span>
    </button>
</div>

<div class="row clearfix">
    <div class="col-xs-12">
        <div class="card">
            
            <div class="header">
                <h2>Utilisateurs enregistrés&nbsp;
                    <button type="button" class="btn btn-default waves-effect" onclick="load_table();" data-toggle="tooltip" data-placement="top" title="Rafraichir">
                        <i class="material-icons">autorenew</i>
                    </button>
                </h2>
            </div>
            
            <div class="body table-responsive">
                <table id="user_admin_datatable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="align-center">Login</th>
                            <th class="align-center">Type</th>
                            <th class="align-center">Groupe</th>
                            <th class="align-center">Actif</th>
                            <th class="align-center">Matr.GPAO</th>
                            <th class="align-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</div>

<!-- Modales -->
<div class="modal fade" id="add_user_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Ajout d'un utilisateur</h4>
            </div>
            <div class="modal-body">
                <form id="frm_add">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" id="add_user_login" class="form-control no_space">
                            <label class="form-label">Identifiant</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" id="add_user_pass" class="form-control">
                            <label class="form-label">Mot de passe</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" id="add_user_matr" class="form-control nb">
                            <label class="form-label">Matr.GPAO</label>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-xs-12">
                            <p><b>Compte</b>:</p>
                            <select id="add_user_type" class="form-control show-tick" onchange="manage_view_modal_add_user();">
                                <?php foreach ($liste_types as $n_type => $type): ?>
                                    <option value="<?php echo $type->id_type_utilisateur ?>"> <?php echo $type->nom_type_utilisateur ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row clearfix add_op_only m-t-25">
                        <div class="col-xs-12">
                            <p><b>Niveau</b>:</p>
                            <select id="add_user_op_niveau" class="form-control show-tick">
                                <option value="1">Niveau 1</option>
                                <option value="2">Niveau 2</option>
                            </select>
                        </div>
                    </div>
                    <div class="row clearfix add_op_only m-t-25">
                        <div class="col-xs-12">
                            <p><b>Goupe utilisateur</b>:</p>
                            <select id="add_user_group" class="form-control show-tick">
                                <option value="-1" selected>Aucun</option>
                                <?php foreach ($liste_groupes as $groupe): ?>
                                    <option value="<?php echo $groupe->id_groupe_utilisateur ?>"> <?php echo $groupe->nom_groupe_utilisateur ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row clearfix add_op_push_only m-t-25">
                        <div class="col-xs-12">
                            <p><b>Acc&egrave;s op&eacute;rateur</b>:</p>
                            <select id="add_op_push_acces" class="form-control show-tick">
                                <?php foreach ($liste_type_acces_push as $acces_push): ?>
                                    <option value="<?php echo $acces_push->id ?>"> <?php echo $acces_push->comm ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="switch m-t-15">
                        <label><input id="add_user_actif" type="checkbox" checked><span class="lever"></span>Actif</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="traitement-ko"><input id="user-traitement-ko" type="checkbox"><span class="lever switch-col-indigo"></span>Accès à l'interface de traitement des KO</label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect" onclick="send_new_user();">Enregistrer</button>
                <button type="button" class="btn btn-success waves-effect" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>
<div class="hide_img" id="zone_img_temp"></div>
<!---------->
<div class="modal fade" id="modif_user_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Modification d'un utilisateur</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" id="modif_user_login" class="form-control no_space">
                            <label class="form-label">Identifiant</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" id="modif_user_pass" class="form-control">
                            <label class="form-label">Mot de passe</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line" id="frm_ln_modif_matr">
                            <input type="text" id="modif_user_matr" class="form-control nb">
                            <label class="form-label">Matr.GPAO</label>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-xs-12">
                            <select id="modif_user_type" class="form-control show-tick" onchange="manage_view_modal_modif_user();">
                                <?php foreach ($liste_types as $n_type => $type): ?>
                                    <option value="<?php echo $type->id_type_utilisateur ?>"> <?php echo $type->nom_type_utilisateur ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row clearfix modif_op_only m-t-25">
                        <div class="col-xs-12">
                            <p><b>Niveau</b>:</p>
                            <select id="modif_user_op_niveau" class="form-control show-tick">
                                <option value="1">Niveau 1</option>
                                <option value="2">Niveau 2</option>
                            </select>
                        </div>
                    </div>
                    <div class="row clearfix modif_op_only m-t-25">
                        <div class="col-xs-12">
                            <p><b>Goupe utilisateur</b>:</p>
                            <select id="modif_user_group" class="form-control show-tick">
                                <option value="-1">Aucun</option>
                                <?php foreach ($liste_groupes_all as $groupe_all): ?>
                                    <option value="<?php echo $groupe_all->id_groupe_utilisateur ?>" <?php echo ($groupe_all->supprime == '1' ? 'disabled' : '') ?> > <?php echo $groupe_all->nom_groupe_utilisateur ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row clearfix modif_op_push_only m-t-25">
                        <div class="col-xs-12">
                            <p><b>Acc&egrave;s op&eacute;rateur</b>:</p>
                            <select id="modif_op_push_acces" class="form-control show-tick">
                                <?php foreach ($liste_type_acces_push as $acces_push): ?>
                                    <option value="<?php echo $acces_push->id ?>"> <?php echo $acces_push->comm ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="switch m-t-15">
                        <label><input id="modif_user_actif" type="checkbox" checked><span class="lever"></span>Actif</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="traitement-ko"><input id="modif-user-traitement-ko" type="checkbox"><span class="lever switch-col-indigo"></span>Accès à l'interface de traitement des KO</label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="modif_user_bt_ok" type="button" class="btn btn-primary waves-effect" onclick="">Enregistrer</button>
                <button type="button" class="btn btn-success waves-effect" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade p-r-0" id="wallboard_user_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="width: 100% !important;">
        <div class="modal-content" id="content_wallboard_user_modal">
            <div class="modal-header p-b-0">
                <div class="row">
                    <div class="col-lg-3">
                        <h4 class="modal-title">Activit&eacute;s</h4>
                    </div>
                    <div class="col-lg-4 align-right">
                        <div class="input-group m-b-0">
                            <span class="input-group-addon">
                                <i class="material-icons">search</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control date" placeholder="Rechercher login ou matr" v-model.trim="s_login">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 align-left">
                        <button type="button" class="btn bg-light-blue waves-effect" :disabled="waiting" onclick="app_wallboard_user.reload_data();">
                            <i class="material-icons">autorenew</i>
                            <span>Recharger</span>
                        </button>
                    </div>
                    <div class="col-lg-2 align-right">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row" v-if="!waiting">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-2 align-center">
                                <h6>Connect&eacute;s <span class="label label-primary">{{ nbrs.connecte }} / {{ nbrs.nb }}</span></h6>
                            </div>
                            <div class="col-lg-5 align-center">
                                <h6>Op.courrier (en traitement/connect&eacute;) <span class="label label-primary">{{ nbrs.connecte_op_cr_ttr }} / {{ nbrs.connecte_op_cr }}</span></h6>
                            </div>
                            <div class="col-lg-5 align-center">
                                <h6>Op.Mail/SFTP (en traitement/connect&eacute;) <span class="label label-primary">{{ nbrs.connecte_op_fx_ttr }} / {{ nbrs.connecte_op_fx }}</span></h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20" v-if="nbrs.nb>0">
                        <table class="table m-b-0">
                            <thead>
                                <tr>
                                    <th class="tab_w_u_col_login">Login<small> [Matr]</small></th>
                                    <th class="tab_w_u_col_tp"></th>
                                    <th class="tab_w_u_col_con">Connect&eacute;<small> [IP]</small></th>
                                    <th class="tab_w_u_col_ocup">En traitement</th>
                                    <th class="tab_w_u_col_fx">Flux</th>
                                    <th class="tab_w_u_col_dt_recpt">R&eacute;ception</th>
                                    <th class="tab_w_u_col_soc">Pli<small> [Mouvements]</small></th>
                                    <th class="tab_w_u_col_act">Derni&egrave;re action<small> [Il y a]</small></th>
                                </tr>
                            </thead>
                        </table>
                        <div style="max-height: 67vh;overflow-x: auto;">
                            <table class="table table-striped">
                                <thead></thead>
                                <tbody v-html="data_lignes"></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12" v-if="nbrs.nb==0">
                        <div class="alert alert-info">
                            <strong>Serveur: </strong> Pas de données.
                        </div>
                    </div>
                </div>
                <div class="row" v-if="waiting">
                    <div class="col-xs-12 align-center">
                        <div class="preloader pl-size-xl">
                            <div class="spinner-layer pl-light-blue">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modales -->

<style>
    .p-r-0 {
        padding-right: 0px !important;
    }
    .m-b-0 {
        margin-bottom: 0px !important;
    }
    .p-b-0 {
        padding-bottom: 0px !important;
    }
</style>
