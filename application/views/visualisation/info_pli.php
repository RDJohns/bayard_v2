<?php
?>
<div class="admin-pro-accordion-wrap shadow-inner responsive-mg-b-30">
    <div class="panel-group edu-custon-design" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading accordion-head">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Pli #<?php echo $info_pli[0]->pli.' : '.$info_pli[0]->code_ged;  ?></a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse panel-ic collapse in">
                <div class="panel-body admin-panel-content animated bounce">
                    <ul class="doc-list">
                    <?php
                        foreach ($info_pli as $pli){ ?>
                            <li onclick="afficher_doc('<?php echo $pli->id_document; ?>')">&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $pli->type_document.' #'.$pli->id_document; ?></li>
                        <?php
                        }
                    ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
