<?php

if($list_export):
    echo "Date courrier,Lot_scan,Pli,Document,N Commande,N Client,Nom client,Prenom client,N tel client,Code postal,Statut\n";
    foreach ($list_export as $doc):
        echo $doc->date_courrier.
            ",".$doc->lot_scan.
            ",".$doc->pli.
            ",".$doc->type_document.
            ",".$doc->num_commande.
            ",".$doc->num_client.
            ",".$doc->nom_client.
            ",".$doc->prenom_client.
            ",".$doc->num_tel_client_pli.
            ",".$doc->code_postal.
            ",".$doc->traitement.
            "\n";
    endforeach;
endif;