<script>
    var url_site = '<?php echo site_url(); ?>';
</script>
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="logo-pro">
                </div>
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="header-top-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="header-top-wraper">
                            <div class="row">
                                <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                    <!--div class="menu-switcher-pro">
                                        <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                            <i class="educate-icon educate-nav"></i>
                                        </button>
                                    </div-->
									
                                </div>
                                <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                    <div class="header-top-menu tabl-d-n">
                                       
										<ul class="nav navbar-nav mai-top-nav">
										   <li style="align-text:center;">
                                               <img src="<?php echo base_url().'src/'; ?>img/logo-willemse.png" style="margin:10px 30px 0px -100px;" ></img>
                                           </li>
                                           <li class="nav-item dropdown res-dis-nn">
                                                 <a href="<?php echo site_url('visualisation/visualisation');?>" style="color:#E1DD70" class="nav-link dropdown-toggle" id="nav" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b>Visualisation</b><span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                <div class="dropdown-menu animated" aria-labelledby="nav">
                                                    <a class="dropdown-item" href="<?php echo site_url('visualisation/visualisation');?>">Plis</a>
                                                    <a class="dropdown-item" href="<?php echo site_url('visualisation/visualisation/production');?>">Production</a>
                                                </div>
											</li>
                                            <li class="nav-item dropdown res-dis-nn">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">Reporting <span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                <div role="menu" class="dropdown-menu">
                                                    <a class="dropdown-item" href="<?php echo site_url('statistics/statistics/stat_plis');?>" class="dropdown-item">Réception</a>
                                                    <a href="<?php echo site_url('statistics/statistics/stat_mensuel');?>" class="dropdown-item">Réception Mensuelle</a>
													<a href="<?php echo site_url('statistics/statistics/stat_traitement');?>" class="dropdown-item">Traitement</a>
													 <a class="dropdown-item" href="<?php echo site_url('production/production/view_chq');?>" class="dropdown-item">Chèque multiple</a>
													<a href="<?php echo site_url('production/production/view_typage');?>" class="dropdown-item">Typage</a>
													<a href="<?php echo site_url('production/production/visu_chq_tlmc');?>" class="dropdown-item">Suivi REB</a>
													 
                                                </div>
                                            </li>
											<li class="nav-item">
                                                <a href="<?php echo site_url('anomalie/anomalie/visu_anomalie');?>">Extraction</a>
                                            </li>
                                            
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <div class="header-right-info">
                                        <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                            <li class="nav-item">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                    <img src="<?php echo base_url().'src/'; ?>img/avatar.png" alt="" />
                                                    <span class="admin-name"><?php echo $this->session->userdata('login'); ?></span>
                                                    <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                                </a>
                                                <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                    <li onclick="deconnexion();"><a href="#"><span class="edu-icon edu-locked author-log-ic"></span>D&eacute;connexion</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- <li class="nav-item nav-setting-open"><a href="#" role="button" class="nav-link"><i class="educate-icon educate-menu"></i></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">

            <!--nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li>
                        <a class="has-arrow" href="#">
                            <span class="menu-click-non">
                                Résultat de recherche
                            </span>
                        </a>
                        <ul class="submenu-angle" aria-expanded="false">
                            <li>
                                <div class="row">
                                    <div id="result-rsrch">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav-->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px;">
                    <div class="admin-pro-accordion-wrap shadow-inner">
                        <div class="panel-group edu-custon-design" id="accordion2">
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-head">
                                    <a data-toggle="collapse" id="to-accordion1" href="#collapse4">
                                        <span class="panel-title" style='font-size:14px;font-family: "open sans","Helvetica Neue",Helvetica,Arial,sans-serif;font-weight: bold; '>
                                          <h5 style="color:#69B145"><i class="fa fa-search" style="color:#C62057"></i>&nbsp;Recherche</h5> 
                                        </span>
                                        <span class="dir-arrow">
                                            <i class="fa fa-angle-down"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapse4" class="panel-collapse panel-ic collapse in">
                                    <div class="panel-body admin-panel-content animated" style="background:#F6F8FA !important;">
                                        <div class="row" >
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="numcommande" id="numcommande" type="text" class="form-control" placeholder="N° commande" onkeypress="rech()" style="border:1px solid #ccc;height: 35px; border-radius: 3px;">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="numclient" id="numclient" type="number" class="form-control" placeholder="N° Client" onkeypress="rech()" style="border:1px solid #ccc;height: 35px; border-radius: 3px;">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="numtel" id="numtel" type="text" class="form-control" placeholder="N° Téléphone Client" onkeypress="rech()" style="border:1px solid #ccc;height: 35px; border-radius: 3px;">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="code_postal" id="codepostal" type="text" class="form-control" placeholder="Code Postal" onkeypress="rech()" style="border:1px solid #ccc;height: 35px; border-radius: 3px;">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <select id="statut" class="form-control" style="border:1px solid #ccc;height: 35px; border-radius: 3px;">
                                                        <option value="">----- Statut -----</option>
                                                        <?php
                                                        if($type_statut){
                                                            foreach ($type_statut as $statut){ ?>
                                                                <option value="<?php echo $statut->id_flag_traitement; ?>"><?php echo ($statut->id_flag_traitement == 2)? 'Clôturé' : $statut->traitement; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="nom_client" id="nom_client" type="text" class="form-control" placeholder="Nom du client" onkeypress="rech()" style="border:1px solid #ccc;height: 35px; border-radius: 3px;">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="prenom_client" id="prenom_client" type="text" class="form-control" placeholder="Pr&eacute;nom du client" onkeypress="rech()" style="border:1px solid #ccc;height: 35px; border-radius: 3px;">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" id="data_5">
                                                    <div class="input-daterange input-group" id="datepicker">
                                                        <input type="text" class="form-control" id="date_deb" name="start" placeholder="Date courrier" value="" readonly style="border:1px solid #ccc;height: 35px; border-radius: 3px;"/>
                                                        <span class="input-group-addon">&nbsp; au </span>
                                                        <input type="text" class="form-control" id="date_fin" name="end" placeholder="Date courrier" value="" readonly style="border:1px solid #ccc;height: 35px; border-radius: 3px;"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary waves-effect waves-light" onclick="recherche_doc()" id="bt_search"><i class="fa fa-search"></i>&nbsp;Rechercher</button>
                                            </div>
                                            <div class="row">
                                                <div style="text-align: center;">
                                                    <em id="error_message">*Veuillez au moins renseigner un champ</em><br/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-head">
                                    <a data-toggle="collapse" class="has-arrow" id="to-accordion2" href="#collapse5">
                                        <span class="panel-title" style='font-size:14px;font-family: "open sans","Helvetica Neue",Helvetica,Arial,sans-serif;font-weight: bold; '>
                                            <h5 style="color:#69B145"><i class="fa fa-file-text" style="color:#C62057"></i>&nbsp;Résultats de la recherche</h5>  
                                        </span>
                                        <span class="dir-arrow">
                                            <i class="fa fa-angle-up"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapse5" class="panel-collapse panel-ic collapse">
                                    <div class="panel-body admin-panel-content animated">
                                        <div class="row" id="result-rsrch" style="display: block;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="analytics-content" id="detail_stat">

                                                        <div class="body">
                                                            <div>
                                                                <table id="pli-table" class="table table-bordered table-striped table-hover dataTable">
                                                                    <!---table id="example" class="table table-striped table-bordered nowrap" style="width:100%;margin-bottom: 0px !important;"-->
                                                                    <thead  class="custom_font">

                                                                    <tr>
                                                                        <th>Date courrier</th>
                                                                        <th>Lot scan</th>
                                                                        <th>Pli</th>
                                                                        <th>Document</th>
                                                                        <th>N&deg; Commande</th>
                                                                        <th>N&deg; Client</th>
                                                                        <th>Nom du client</th>
                                                                        <th>Pr&eacute;nom du client</th>
                                                                        <th>N° tél client</th>
                                                                        <th>Code postal</th>
                                                                        <th>Statut</th>
                                                                        <th>Visualiser</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="custom_font">
                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <br/>
                                        </div>
                                        <div class="row" id="doc-row">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--div class="row hpanel widget-int-shape" id="doc-row">

            </div-->
        </div>
    </div>
</div>
<script>
 var users = <?php echo json_encode(array());?>;
</script>
