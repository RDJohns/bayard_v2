 <!-- jquery

    <?php
        $url_js = base_url().'src/';
        $url_adminbsb = base_url().'assets/adminbsb/';
    ?>
		============================================ -->
    <script>
        var chemin_site = '<?php echo site_url(); ?>';
    </script>
    <script src="<?php echo js_url('vendor/jquery-1.12.4.min'); ?>"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo js_url('bootstrap.min'); ?>"></script>
    <!-- wow JS
		============================================ -->
    <script src="<?php echo js_url('wow.min'); ?>"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="<?php echo js_url('jquery-price-slider'); ?>"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?php echo js_url('jquery.meanmenu'); ?>"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?php echo js_url('owl.carousel.min'); ?>"></script>
    <!-- sticky JS
		============================================ -->
    <script src="<?php echo js_url('jquery.sticky'); ?>"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo js_url('jquery.scrollUp.min'); ?>"></script>
    <!-- counterup JS
		============================================ -->
    <script src="<?php echo js_url('counterup/jquery.counterup.min'); ?>"></script>
    <script src="<?php echo js_url('counterup/waypoints.min'); ?>"></script>
    <script src="<?php echo js_url('counterup/counterup-active'); ?>"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?php echo js_url('scrollbar/jquery.mCustomScrollbar.concat.min'); ?>"></script>
    <script src="<?php echo js_url('scrollbar/mCustomScrollbar-active'); ?>"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="<?php echo js_url('metisMenu/metisMenu.min'); ?>"></script>
    <script src="<?php echo js_url('metisMenu/metisMenu-active'); ?>"></script>
    <!-- chosen JS
    ============================================ -->
    <script src="<?php echo js_url('chosen/jquery.dropdown');?>"></script>
    <script src="<?php echo js_url('chosen/typeAhead');?>"></script>


    <!-- select2 JS
    ============================================ -->
    <script src="<?php echo js_url('select2/select2.full.min');?>"></script>
    <script src="<?php echo js_url('select2/select2-active');?>"></script>
    <!-- data table JS
        ============================================ -->
    <script src="<?php echo js_url('data-table/bootstrap-table');?>"></script>
    <script src="<?php echo js_url('data-table/tableExport');?>"></script>
    <script src="<?php echo js_url('data-table/data-table-active');?>"></script>
    <script src="<?php echo js_url('data-table/bootstrap-table-editable');?>"></script>
    <script src="<?php echo js_url('data-table/bootstrap-editable');?>"></script>
    <script src="<?php echo js_url('data-table/bootstrap-table-resizable');?>"></script>
    <script src="<?php echo js_url('data-table/colResizable-1.5.source');?>"></script>
    <script src="<?php echo js_url('data-table/bootstrap-table-export');?>"></script>
    <!--  editable JS
        ============================================ -->
    <script src="<?php echo js_url('editable/jquery.mockjax');?>"></script>
    <script src="<?php echo js_url('editable/mock-active');?>"></script>
    <script src="<?php echo js_url('editable/select2');?>"></script>
    <script src="<?php echo js_url('editable/moment.min');?>"></script>
    <script src="<?php echo js_url('editable/bootstrap-datetimepicker');?>"></script>
    <script src="<?php echo js_url('editable/bootstrap-editable');?>"></script>
    <script src="<?php echo js_url('editable/xediable-active');?>"></script>
    <!-- morrisjs JS
		============================================ -->
    <!-- <script src="<?php echo js_url('morrisjs/raphael-min'); ?>"></script> -->
    <!-- <script src="<?php echo js_url('morrisjs/morris'); ?>"></script> -->
    <!-- <script src="<?php echo js_url('morrisjs/morris-active'); ?>"></script> -->
    <!-- morrisjs JS
		============================================ -->
    <script src="<?php echo js_url('sparkline/jquery.sparkline.min'); ?>"></script>
    <script src="<?php echo js_url('sparkline/jquery.charts-sparkline'); ?>"></script>
    <script src="<?php echo js_url('sparkline/sparkline-active'); ?>"></script>
    <!-- calendar JS
		============================================ -->
    <script src="<?php echo js_url('calendar/moment.min'); ?>"></script>
    <script src="<?php echo js_url('calendar/fullcalendar.min'); ?>"></script>
    <script src="<?php echo js_url('calendar/fullcalendar-active'); ?>"></script>
	<!-- notification JS
	============================================ -->
	<script src="<?php echo js_url('notifications/Lobibox'); ?>"></script>
	<script src="<?php echo js_url('notifications/notification-active'); ?>"></script>
    <!-- plugins JS
		============================================ -->
    <script src="<?php echo js_url('plugins'); ?>"></script>
    <!-- jquery132 JS
		============================================ -->
    <!--script src="<?php echo js_url('jquery-1.3.2'); ?>"></script-->
    <!-- quickflip JS
		============================================ -->
    <script src="<?php echo js_url('jquery.quickflip'); ?>"></script>
    <!-- fliptab JS
		============================================ -->
    <script src="<?php echo js_url('fliptab/fliptab'); ?>"></script>
    <!-- plugins JS
        ============================================ -->
    <script src="<?php echo js_url('notifications/Lobibox'); ?>"></script>
    <script src="<?php echo js_url('notifications/notification-active'); ?>"></script>
     <!-- plugins JS
            ============================================ -->
     <script src="<?php echo js_url('datapicker/bootstrap-datepicker'); ?>"></script>
     <script src="<?php echo js_url('datapicker/datepicker-active'); ?>"></script>
    <!-- plugins JS
        ============================================ -->
    <script src="<?php echo js_url('data-table/bootstrap-table'); ?>"></script>
    <script src="<?php echo js_url('data-table/tableExport'); ?>"></script>
    <script src="<?php echo js_url('data-table/data-table-active'); ?>"></script>
    <script src="<?php echo js_url('data-table/bootstrap-table-editable'); ?>"></script>
    <script src="<?php echo js_url('data-table/bootstrap-editable'); ?>"></script>
    <script src="<?php echo js_url('data-table/bootstrap-table-resizable'); ?>"></script>
    <script src="<?php echo js_url('data-table/colResizable-1.5.source'); ?>"></script>
    <script src="<?php echo js_url('data-table/bootstrap-table-export'); ?>"></script>
    <!-- main JS
		============================================ -->
	<script src="<?php echo $url_js.'js/main.js'; ?>"></script>
    <!-- visualisation JS
    ============================================ -->
    <script src="https://cdn.jsdelivr.net/npm/jquery-roadmap@1/dist/jquery.roadmap.min.js"></script>
    <script src="<?php echo $url_js; ?>plugins/nprogress/nprogress.js"></script>
     <script src="<?php echo $url_js.'js/visualisation/visualisation.js'; ?>"></script>
     <script src="<?php echo $url_js.'js/visualisation/head.js'; ?>"></script>
    
    <!-- tawk chat JS
		============================================ -->
    <!--script src="<?php echo js_url('tawk-chat'); ?>"></script-->
    <script src="<?php echo $url_js; ?>plugins/img_w/jquery.magnify.js"></script>

    <script src="<?php echo $url_js.'plugins/sweetalert.min.js'; ?>"></script>

     <script src="<?php echo $url_adminbsb.'plugins/jquery-datatable/jquery.dataTables.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/jquery-datatable/extensions/export/buttons.flash.min.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/jquery-datatable/extensions/export/jszip.min.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/jquery-datatable/extensions/export/pdfmake.min.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/jquery-datatable/extensions/export/vfs_fonts.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/jquery-datatable/extensions/export/buttons.html5.min.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/jquery-datatable/extensions/export/buttons.print.min.js'; ?>"></script>
    
     <!-- Morris Plugin Js -->
     <script src="<?php echo $url_adminbsb.'plugins/node-waves/waves.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/raphael/raphael.min.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'plugins/morrisjs/morris.js'; ?>"></script>
     <script src="<?php echo $url_adminbsb.'js/admin.js'; ?>"></script>
     <script src="<?php echo  base_url().'assets/js/'.'jquery.slimscroll.min.js'; ?>"></script>

 
</body>

</html>
