<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div>
        <div class="analytics-content" id="detail_stat">

            <div class="body">
                <div class="table-responsive" id="tab_production"  >
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable pli-table">
					 <thead>
                       <!--thead  class="custom_font"-->
                        <tr>
                            <th>Id pli</th>
                            <th>Pli</th>
                            <th>Type pli</th>
                            <th>Lot scan</th>
                            <th>Type Document/Code de document</th>
                            <th>Dernier statut</th>
                            <th>Responsable</th>
                            <th>Date dernier traitement</th>
                            <th>Date courrier</th>
                            <th>Détail</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        if ($prod) {
                            foreach ($prod as $value) { ?>
                        <?php if ($value->oid1 == $value->oid2) { ?>
                                    <tr>
                                        <td><?php echo $value->id_pli; ?></td>
                                        <td><?php echo $value->pli; ?></td>
                                        <td><?php echo $value->code_type_pli; ?></td>
                                        <td><?php echo $value->lot_scan ?></td>
                                        <td><?php echo $value->lib_type . ' #' . $value->id_document; ?></td>
                                        <td><?php echo $value->traitement; ?></div>
                                        <td>
                                            <?php if ($value->type_par == null) {
                                                echo $value->ttr_par;
                                            } else {
                                                echo $value->type_par;
                                            }; ?>

                                        </td>
                                        <td><?php echo date("d/m/y", strtotime($value->date_traitement)); ?></td>
                                        <td><?php echo date("d/m/Y", strtotime($value->date_courrier)); ?></td>
                                       
                                        <td style="text-align: center">
                                            <button class="btn btn-info wiew_detail"
                                                    onclick="afficher_histo('<?php echo $value->id_pli; ?>')">
                                                <i class="fa fa-eye"></i></button>
                                        </td>
                                    </tr>

                                    <?php
                                }
                            }
                        } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


