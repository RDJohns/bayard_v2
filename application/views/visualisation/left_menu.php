 <!-- Start Left menu area -->
    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="index.html"><img class="main-logo" src="<?php echo img_url('logo/logo.png'); ?>" alt="" /></a>
                <strong><a href="index.html"><img src="<?php echo img_url('logo/logosn.png'); ?>" alt="" /></a></strong>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                        <li class="active">
                            <a class="has-arrow" href="index.html">
								   <span class="educate-icon educate-home icon-wrap"></span>
								   <span class="mini-click-non">GED</span>
								</a>
                            <ul class="submenu-angle" aria-expanded="true">
                                <li><a title="Plis et Documents" href="index.html"><span class="mini-sub-pro">Plis et Documents</span></a></li>
                                <li><a title="Anomalis" href="<?php echo site_url('visualisation/VisualeAnomalis');?>"><span class="mini-sub-pro">Anomalis</span></a></li>
                            </ul>
                        </li>      
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
    <!-- End Left menu area -->