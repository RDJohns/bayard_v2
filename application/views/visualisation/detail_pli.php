<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
                <h5 style="color:#69B145"><i class="fa fa-archive" style="color:#C62057"></i> Détail du pli  #<?php echo $id ?></h5>
        </div>
            <div class="body">
                <div class="table-responsive" id="tab_details_plis"  >
                    <table class="table table-bordered table-striped table-hover dataTable detail_plis">
                        <thead class="detailpli">
                        <!--thead  class="custom_font"-->
                        <tr>
                            <th>Pli</th>
                            <th>Type pli</th>
                            <th>Lot scan</th>
                            <th>Type Document/Code de document</th>
                            <th>Statut</th>
                            <th>Responsable</th>
                            <th>Date  de traitement</th>
                            <th>Date courrier</th>
                        </tr>
                        </thead>
                        <tbody class ="bodypli">
                        <?php
                        if($pli){
                            foreach($pli as $value){ ?>
                                    <tr>
                                        <td><?php echo $value->id_pli; ?></td>
                                        <td><?php echo $value->code_type_pli; ?></td>
                                        <td><?php echo $value->lot_scan ?></td>
                                        <td><?php echo $value->lib_type . ' #' . $value->id_document; ?></td>
                                        <td><?php 
                                        if  ( $value->id_flag_traitement == 0 && $value->source == "controle") {
                                            echo "KO Typage"; 
                                        } else {
                                            echo $value->traitement; 
                                        };
                                                                                          
                                            ?></td>
                                        <td><?php if($value->type_par == null) {
                                                echo $value->ttr_par;
                                            } else {
                                                echo $value->type_par;
                                            }; ?></td>
                                        <td><?php echo date("d/m/y", strtotime($value->date_traitement)); ?></td>
                                        <td><?php echo date("d/m/Y", strtotime($value->date_courrier)); ?></td>
                                    </tr>
                                    <?php

                            }
                        } ?>

                        </tbody>
                    </table>
                </div>
            </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
           <h5 style="color:#69B145"><i class="fa fa-file" style="color:#C62057"></i> Documents du pli  #<?php echo $id?></h5>
        </div>


        <div class="body">
            <?php
            $var = explode(',', $pli[0]->id_document);
            $varType = explode(',', $pli[0]->lib_type);
            $len = count($var);
            for ($cpr = $len-1; $cpr >=0; $cpr--) {
                $doc = $this->mvisu->get_one_doc($var[$cpr]);
                ?>
                    <button type="button" img="trig_<?php echo $doc[0]->id_document ?>" class="btn btn-primary btn-lg aficherImg"><?php echo $varType[$cpr]?>#<?php echo $doc[0]->id_document ?></button>
                    <img class = "hidden" src="data:image/jpeg;base64, <?php echo $doc[0]->n_ima_base64_recto; ?>"  class="img_b64" id ="trig_<?php echo $doc[0]->id_document ?>" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$doc[0]->id_pli.' / Doc#'.$doc[0]->id_document.' / RECTO'; ?>" data-group="<?php echo $doc[0]->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $doc[0]->n_ima_base64_recto; ?>"/>
                    <img class = "hidden" src="data:image/jpeg;base64, <?php echo $doc[0]->n_ima_base64_verso; ?>"  class="img_b64" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$doc[0]->id_pli.' / Doc#'.$doc[0]->id_document.' / RECTO'; ?>" data-group="<?php echo $doc[0]->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $doc[0]->n_ima_base64_verso; ?>"/>
                <?php
            }
            ?>



        </div>
    </div>
</div>

<script>
    $('.aficherImg').on('click', function() {
        id = $(this).attr('img');
        $('#'+id).trigger('click');
    });
</script>
