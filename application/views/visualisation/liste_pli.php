<?php

?>
    <!--div class="row"-->
<?php

?>
    <!--/div-->

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div>
        <div class="analytics-content" id="detail_stat"> 

            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable pli-table">
                        <thead  class="custom_font">

                            <tr>
                                <th>Date courrier</th>
                                <th>Pli</th>
                                <th>Lot scan</th>
                                <th>Document</th>
                                <th>N° Commande</th>
                                <th>N° Client</th>
                                <th>Nom du client</th>
                                <th>N° tél client</th>
                                <th>Code postal</th>
                                <th>Statut</th>
                                <th>Visualiser</th>
                            </tr>
                        </thead>
                        <tbody class="custom_font">
                            <?php
                            if($liste_pli_doc){
                            foreach($liste_pli_doc as $value){ ?>

                                <tr>
                                    <td><?php echo date("d/m/Y", strtotime($value->date_courrier)); ?></td>
                                    <td><?php echo $value->pli; ?></td>
                                    <td><?php echo $value->lot_scan; ?></td>
                                    <td><?php echo $value->type_document. ' #' .$value->id_doc; ?></td>
                                    <td><?php echo $value->num_commande; ?></td>
                                    <td><?php echo $value->num_client; ?></td>
                                    <td><?php echo $value->nom_client; ?></td>
                                    <td><?php echo $value->num_tel_client_pli; ?></td>
                                    <td><?php echo $value->code_postal; ?></td>
                                    <td><?php echo $value->traitement; ?></td>
                                    <td style="text-align: center"><button class="btn btn-info" onclick="afficher_doc('<?php echo $value->id_doc; ?>')"><i class="fa fa-eye"></i></button></td>
                                </tr>

                            <?php
                                }
                            } ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


