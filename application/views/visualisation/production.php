<script>
    var url_site = '<?php echo site_url(); ?>';
</script>
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="logo-pro">
                </div>
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="header-top-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="header-top-wraper">
                            <div class="row">
                                <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                </div>
                                <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                    <div class="header-top-menu tabl-d-n">
                                        <ul class="nav navbar-nav mai-top-nav">
                                            <li style="align-text:center;">
                                                <img src="<?php echo base_url().'src/'; ?>img/logo-willemse.png" style="margin:10px 30px 0px -100px;" ></img>
                                            </li>

                                            <li class="nav-item dropdown res-dis-nn">
                                                <a href="<?php echo site_url('visualisation/visualisation');?>" style="color:#E1DD70" class="nav-link dropdown-toggle" id="nav" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b>Visualisation</b><span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                <div class="dropdown-menu animated" aria-labelledby="nav">
                                                    <a class="dropdown-item" href="<?php echo site_url('visualisation/visualisation');?>">Plis</a>
                                                    <a class="dropdown-item" href="<?php echo site_url('visualisation/visualisation/production');?>">Production</a>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown res-dis-nn">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">Reporting <span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                <div role="menu" class="dropdown-menu">
                                                    <a class="dropdown-item" href="<?php echo site_url('statistics/statistics/stat_plis');?>" class="dropdown-item">Réception</a>
                                                    <a href="<?php echo site_url('statistics/statistics/stat_mensuel');?>" class="dropdown-item">Réception Mensuelle</a>
													<a href="<?php echo site_url('statistics/statistics/stat_traitement');?>" class="dropdown-item">Traitement</a>
													 <a class="dropdown-item" href="<?php echo site_url('production/production/view_chq');?>" class="dropdown-item">Chèque multiple</a>
													<a href="<?php echo site_url('production/production/view_typage');?>" class="dropdown-item">Typage</a>
													 
                                                </div>
                                            </li>
                                            <li class="nav-item">
                                                <a href="<?php echo site_url('anomalie/anomalie/visu_anomalie');?>">Extraction</a>
                                            </li>
                                            
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <div class="header-right-info">
                                        <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                            <li class="nav-item">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                    <img src="<?php echo base_url().'src/'; ?>img/avatar.png" alt="" />
                                                    <span class="admin-name"><?php echo $this->session->userdata('login'); ?></span>
                                                    <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                                </a>
                                                <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                    <li onclick="deconnexion();"><a href="#"><span class="edu-icon edu-locked author-log-ic"></span>D&eacute;connexion</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- <li class="nav-item nav-setting-open"><a href="#" role="button" class="nav-link"><i class="educate-icon educate-menu"></i></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px;">
                    <div class="admin-pro-accordion-wrap shadow-inner">
                        <div class="panel-group edu-custon-design" id="accordion2">
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-head">
                                    <a data-toggle="collapse" id="to-accordion1" href="#collapse4">
                                        <span class="panel-title" style='font-size:14px;font-family: "open sans","Helvetica Neue",Helvetica,Arial,sans-serif;font-weight: bold; '>
                                          <h5 style="color:#69B145"><i class="fa fa-search" style="color:#C62057"></i>
											Suivi de production</h5>
                                        </span>
                                        <span class="dir-arrow">
                                            <i class="fa fa-angle-down"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapse4" class="panel-collapse panel-ic collapse in">
                                    <div class="panel-body admin-panel-content animated" style="background:#F6F8FA !important;">
                                        <div class="row" >
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="idplis" id="idplis" type="text" class="form-control input_theme" placeholder="Id pli">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="plis" id="plis" type="text" class="form-control input_theme" placeholder="Pli">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <?php
                                                    $userss = array();$c=0;
                                                    if ($users) {
                                                        foreach ($users as $utilisateur) { ?>
                                                         <?php
                                                            $userss[$c] = $utilisateur->login;
                                                            $c++;
                                                                    }
                                                                }
                                                                ?>
                                                <div  id="traite" class="users form-group">
                                                    <input name="user" id="traitePar" type="text" class="typeahead form-control input_theme" placeholder="Traité par">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="lotscan" id="lotscan" type="text" class="form-control input_theme" placeholder="Lot scan">
                                                </div>
                                                
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="typopli">
                                                     <select id="typologie" class="form-control input_theme" placeholder="--- Typologie de pli ---">
                                                        <option value=""> ---- Typologie de pli ----</option>
                                                        <?php
                                                        if ($typePlis) {
                                                            foreach ($typePlis as $typologie) { ?>
                                                                <option value="<?php echo $typologie->id_type_pli; ?>"><?php echo $typologie->code_type_pli; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="statpli">
                                                    <select id="statut" class="form-control input_theme" placeholder="---- Statut de pli ----">
                                                        <option value=""> ---- Statut de pli ----</option>
                                                        <?php
                                                        if ($statut) {
                                                            foreach ($statut as $stat) { ?>
                                                                <option value="<?php echo $stat->id_flag_traitement; ?>"><?php echo $stat->traitement; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group users">
                                                        <input name="typePar" id="typePar" type="text" class="typeahead form-control input_theme" placeholder="Typé par">
                                                    </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" id="data_5">
                                                    <div class="input-daterange input-group" id="datepicker">
                                                        <input type="text" class="form-control input_theme" id="date_deb_prod" name="start" placeholder="Date courrier" value="" readonly style="border:1px solid #ccc;height: 35px; border-radius: 3px;"/>
                                                        <span class="input-group-addon">&nbsp; au </span>
                                                        <input type="text" class="form-control" id="date_fin_prod" name="end" placeholder="Date courrier" value="" readonly style="border:1px solid #ccc;height: 35px; border-radius: 3px;"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" id="data_5">
                                                    <div class="input-daterange input-group" id="datepicker">
                                                        <input type="text" class="form-control input_theme" id="date_ttr" name="date_ttr" placeholder="Date traitement" value="" readonly />
                                                        <span class="input-group-addon">&nbsp; au </span>
                                                        <input type="text" class="form-control input_theme" id="date_fin_ttr" name="date_fin_ttr" placeholder="Date traitement" value="" readonly />
                                                        <!--<input type="text" class="form-control" id="date_ttr" name="date_ttr" placeholder="Date traitement" value="" readonly />
                                                    -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary waves-effect waves-light" onclick="recherche_prod()" id="bt_production"><i class="fa fa-search"></i>&nbsp;Rechercher</button>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle viderChamps" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Initialiser <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a flag="idplis" href="javascript:void(0);" class="init waves-effect waves-block">Id pli</a></li>
                                                        <li><a flag="plis" href="javascript:void(0);" class="init waves-effect waves-block">Pli</a></li>
                                                        <li><a flag="traitePar" href="javascript:void(0);" class="init waves-effect waves-block">Traité par</a></li>
                                                        <li><a flag="typePar" href="javascript:void(0);" class="init waves-effect waves-block">Typé par</a></li>
                                                        <li><a flag="lotscan" href="javascript:void(0);" class="init waves-effect waves-block">Lot scan</a></li>
                                                        <li><a flag="date_deb_prod" href="javascript:void(0);" class="init waves-effect waves-block">Date de courrier</a></li>
                                                        <li><a flag="date_ttr" href="javascript:void(0);" class="init waves-effect waves-block">Date de traitement</a></li>
                                                        <li><a href="javascript:void(0);" class="waves-effect waves-block infoChamp hide">Aucun champ à initialiser!!</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div style="text-align: left;">
                                                    <em id="error_message">*Veuillez au moins renseigner un champ</em><br/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-head">
                                    <a data-toggle="collapse" class="has-arrow" id="to-accordion2" href="#collapse5">
                                        <span class="panel-title" style='font-size:14px;font-family: "open sans","Helvetica Neue",Helvetica,Arial,sans-serif;font-weight: bold; '>
                                            <h5 style="color:#69B145"><i class="fa fa-file-text" style="color:#C62057"></i>
											Résultats de la recherche</h5>
                                        </span>
                                        <span class="dir-arrow">
                                            <i class="fa fa-angle-up"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapse5" class="panel-collapse panel-ic collapse">
                                    <div class="panel-body admin-panel-content animated">
                                       <!-- div pour afficher les résulats de la requette -->
                                        <div class="row" id="result-rsrch">

                                        </div>
                                        <br/>
                                        <div class="row" id="doc-row">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
 var users = <?php echo json_encode($userss);?>;
</script>
