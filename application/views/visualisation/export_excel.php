<?php
    $bordure = 'style="border-bottom:1px solid #cecece"';
    $bordure_text = 'style="border-bottom:1px solid #cecece;mso-number-format: \'\@\';"';
?>
<table>
    <thead>
        <tr>
            <th <?php echo $bordure; ?>>Date courrier</th>
            <th <?php echo $bordure; ?>>Lot scan</th>
            <th <?php echo $bordure; ?>>Pli</th>
            <th <?php echo $bordure; ?>>Document</th>
            <th <?php echo $bordure; ?>>N&deg; Commande</th>
            <th <?php echo $bordure; ?>>N&deg; Client</th>
            <th <?php echo $bordure; ?>>Nom du client</th>
            <th <?php echo $bordure; ?>>Pr&eacute;nom du client</th>
            <th <?php echo $bordure; ?>>N&deg; t&eacute;l client</th>
            <th <?php echo $bordure; ?>>Code postal</th>
            <th <?php echo $bordure; ?>>Statut</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if($list_export):
                foreach ($list_export as $doc): ?>
                    <tr>
                        <td <?php echo $bordure; ?>><?php echo $doc->date_courrier; ?></td>
                        <td <?php echo $bordure; ?>><?php echo $doc->lot_scan; ?></td>
                        <td <?php echo $bordure_text; ?>><?php echo $doc->pli; ?></td>
                        <td <?php echo $bordure; ?>><?php echo $doc->type_document; ?></td>
                        <td <?php echo $bordure; ?>><?php echo $doc->num_commande; ?></td>
                        <td <?php echo $bordure; ?>><?php echo $doc->num_client; ?></td>
                        <td <?php echo $bordure; ?>><?php echo $doc->nom_client; ?></td>
                        <td <?php echo $bordure; ?>><?php echo $doc->prenom_client; ?></td>
                        <td <?php echo $bordure; ?>><?php echo $doc->num_tel_client_pli; ?></td>
                        <td <?php echo $bordure; ?>><?php echo $doc->code_postal; ?></td>
                        <td <?php echo $bordure; ?>><?php echo $doc->traitement; ?></td>
                    </tr>
                <?php
                endforeach;
            endif;
        ?>
    </tbody>
</table>

