<?php
$strAnomlieCheque        = "";

$strListeIdDoc           = "";
$strAnomalieCorrige      = "";

$listeChequeCorrige      = array();



list($class,$rang)      = explode("mode_paiement_",$valueCheque);

$classChequePrecedant   = "mode_paiement_".$rang;
$rangSuivant            = (int)$rang+1;
$classChequeSuivant     = "mode_paiement_".$rangSuivant;

$chequeVerification     = array(); /*array("str"=>"","listeID"=>"array()");*/
if(intval($currentDoc) > 0 )
{
   $CI = &get_instance();
   $CI->load->library("ra/Verifier_Cheque",null,"vCheque");
   $chequeVerification = $CI->vCheque->VerifierChequeID(intval($currentDoc));

   if($chequeVerification)
   {
        $strAnomalieCorrige = $chequeVerification["str"];
        $listeChequeCorrige = $chequeVerification["listeID"];
    }
}


if((int)$rangSuivant < 2 )
{
    $style = "style='display:none'";
}
else
{
    $style = "style='display:inline'";
}

/* $strAnomlieCheque      .= '<select class="form-control c-anomalie-liste"  id="chAnomalie_'.$rangSuivant.'"  onchange="verifierAnomalieParRang('.$rangSuivant.');"  multiple data-live-search="true" multiple>'; */
$strAnomlieCheque      .= '<select class="form-control c-anomalie-liste"  id="chAnomalie_'.$rangSuivant.'"   multiple data-live-search="true" multiple>';
$strAnomlieCheque       .= '<option value="0" disabled="disabled">--Choisir une anomalie--</option>';
foreach ($anomalieCheque as $itemAnomalie)
{
    $disabled      = " ";
    /*data-subtext="French's"*/
    $dataSubtext   = " ";

    if(in_array(intval($itemAnomalie->id),$listeChequeCorrige ))
    {
        $disabled     = " disabled ";
        $dataSubtext  = ' data-subtext="<code style=\'color :red !important;\'>anomalie déjà corrigé par CDN</code>" ';
    }
    $strAnomlieCheque   .= '<option value="'.(int)$itemAnomalie->id.'" data-live-search="true" '.$disabled. '  '.$dataSubtext.'>'.$itemAnomalie->anomalie.'</option>';
}


$strAnomlieCheque       .= '</select>';




$disable = " ";
$strListeDocFirstSelect = "";

for($iDoc = 0; $iDoc < count($idDoc);$iDoc++)
{
    $idDocument = $idDoc[$iDoc];
    if(intval($idDocument) == intval($currentDoc))
    {
        $disable = " disabled ";
        $strListeIdDoc   .= '<option value="'.$idDocument.'" selected data-live-search="true">Doc. #'.$idDocument.'</option>';
    }
    else
    {
        $strListeIdDoc   .= '<option value="'.$idDocument.'" data-live-search="true">Doc. # '.$idDocument.'</option>';
    }
    
}

$strListeIdDocTemp   = '<select class="form-group form-control c-chqDoc"  '.$disable.' id="chqDoc_'.$rangSuivant.'"  onchange ="checkDocument('.$rangSuivant.')" data-live-search="true">';


if(trim($disable) != "disabled")
{
    $strListeDocFirstSelect = '<option value="0" selected data-live-search="true">--Choisir un document--</option>';
}

$strListeIDFinale = $strListeIdDocTemp.$strListeDocFirstSelect.$strListeIdDoc.'</select>';

$strListeDocFirstSelect = "";
$disable = " ";

$idAnomlieChequeListe = "modal-anomalie_chq_".$rangSuivant;

/*
$CI =&get_instance();              

 $CI->menu->menu($actif);   */


?>


<div class="row clearfix dynamique_cheque <?php echo $classChequeSuivant; ?>">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <span class="badge bg-purple">Chèque # <?php echo $rangSuivant.$doc; ?></span>
            <div class="body">
                <div class="form-group form-float">
                    <div class="form-line">
                            <?php echo $strListeIDFinale; ?>
                            <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">ID document : </label>
                    </div>
                </div>

                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" onblur="setMontant('chMontant_<?php echo $rangSuivant; ?>');" onkeypress="return isNumberKey(event,'chMontant_<?php echo $rangSuivant; ?>')" class="form-control chMontant" id="chMontant_<?php echo $rangSuivant; ?>"/>
                        <label class="form-label">Montant : </label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text"  maxlength="31" minlength="31" class="form-control chCmc7" id="chCmc7_<?php echo $rangSuivant; ?>" value="<?php echo $cmc7;?>"/>
                        <label class="form-label">CMC7 : </label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" maxlength="2" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" class="form-control chRlmc" id="chRlmc_<?php echo $rangSuivant; ?>"/>
                        <label class="form-label">RLMC : </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                       
                        <div class="form-group form-float">
                            <div class="form-line">
                                <?php echo $strAnomlieCheque; ?>
                                <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Anomalie : </label>
                            </div>
                        </div>
                        <!-- <br/> <span class="c-texte-sans-anomalie" style="font-weight: bold;" id="texte-sans-anomalie_<?php echo $rangSuivant;?>" >Chèque sans anomalie</span> --> 
                            
                        
                    </div>
                    <div class="col-sm-4 addCheque <?php echo $classChequeSuivant; ?>" >
                        <button type="button" tabindex="-1"  class="btn btn-primary btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Ajouter un chèque" onclick="ajouterCheque('<?php echo $classChequeSuivant; ?>,<?php echo "null"; ?>');">
                            <i class="material-icons"><b>plus_one</b></i>
                        </button>
                        <button type="button" <?php echo $style; ?>class="btn btn-small bg-blue-grey btn-circle waves-effect waves-circle waves-float" tabindex="-1" data-toggle="tooltip" data-placement="top" title="Supprimer ce chèque" onclick="supprimerCheque('<?php echo $classChequeSuivant; ?>');">
                            <i class="material-icons"><b>remove</b></i>
                        </button>
                       
                    </div>
                    <?php echo $strAnomalieCorrige ?>
                </div>

            </div>
        </div>
    </div>
</div>

