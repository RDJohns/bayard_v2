<?php
$strMagnify     = "<br/>";
$idPli          = 0;
ini_set('memory_limit', '-1');
$over_docs = count($resultat) > 20;
$num_doc = 0;
foreach ($resultat as $itemMagnify)
{
    $idPli          = (int)$itemMagnify->id_pli;
    $captionRecto   =  "";
    $captionVerso   =  "";
    $idDoc          =  0;
    $imageRecto     = "";
    $imageVerso     = "";
    $idDoc          = 0;
    $idDoc          =$itemMagnify->id_document;
    $num_doc++;
    if(trim($itemMagnify->n_ima_base64_recto) != "" && $itemMagnify->n_ima_base64_recto != null)
    {
        $captionRecto   = (trim($itemMagnify->n_ima_recto))== ""?"Pas de nom":trim($itemMagnify->n_ima_recto);
        $imageRecto     = $itemMagnify->n_ima_base64_recto;
        $captionRecto   .=" #pli ".$idPli." #doc ".$idDoc.' (recto)';
        $strMagnify     .='<a  class="magni img_b64" data-magnify="gallery" data-caption="'.$captionRecto.'" href="data:image/jpeg;base64,'.$imageRecto.'">';

        if ($over_docs) {
            $strMagnify     .='<i class="material-icons font-40" data-toggle="tooltip" data-placement="top" title="DOC#'.$num_doc.' recto">image</i>';
        } else {
            $strMagnify     .='<img class="thumbnail_image" src="data:image/jpeg;base64,'.$imageRecto.'" alt="">';
        }
        

        $strMagnify     .='</a>';
    }
    else
    {
        log_message('error', $this->session->userdata('infouser').' pas image recto #pli '.$idPli );
    }

    if(trim($itemMagnify->n_ima_base64_verso) != "" && $itemMagnify->n_ima_base64_verso != null)
    {
        $captionVerso   = (trim($itemMagnify->n_ima_verso))== ""?"Pas de nom":trim($itemMagnify->n_ima_verso);
        $imageVerso     = $itemMagnify->n_ima_base64_verso;
        $captionVerso   .=" #pli ".$idPli." #doc ".$idDoc.' (verso)';

        $strMagnify     .='<a  class="magni img_b64" data-magnify="gallery" data-caption="'.$captionVerso.'" href="data:image/jpeg;base64,'.$imageVerso.'">';
        if ($over_docs) {
            $strMagnify     .='<i class="material-icons font-40" data-toggle="tooltip" data-placement="top" title="DOC#'.$num_doc.' verso">image</i>';
        } else {
            $strMagnify     .='<img class="thumbnail_image" src="data:image/jpeg;base64,'.$imageVerso.'" alt="">';
        }
        $strMagnify     .='</a>';
    }
    else
    {
        log_message('error', $this->session->userdata('infouser').' pas image verso #pli '.$idPli );
    }
}
echo "<div class='alldoc' style='display: block !important;margin-left: auto !important;margin-right: auto !important;'>".$strMagnify."</div>";
/*
 * $( "#myelement" ).click(function() {
    if($('#another-element:visible').length)
        $('#another-element').hide("slide", { direction: "right" }, 1000);
    else
        $('#another-element').show("slide", { direction: "right" }, 1000);
});
 * */
?>

