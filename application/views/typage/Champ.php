<?php
    $strModePaiement    = "";
    $strTypologie       = "";
    /*$strMotifRejet      = "";*/
    $strSociete         = "";

    $strModePaiement    .= '<select class="form-group form-control mode_paiement_0" id="mode-paiement" onchange="verifModePaiement(\'mode_paiement_0\',\'\');" multiple data-live-search="true" autofocus>';
    $strTypologie       .= '<select class="form-group form-control select_typo" id="typologie" onchange="getProvenanceType();">';
    /*$strMotifRejet      .= '<select class="form-group form-control"  id="motifRejet" onchange="verifDescription();">';*/
    $strSociete         .= '<select class="form-group form-control"  onchange="getSociete();" id="infoSociete" name="infoSocieteName" data-live-search="true" autofocus>';

    $strTypologie       .= '<option value="0" selected disabled="disabled">--Choisir une typologie--</option>';
    $strSociete         .= '<option selected disabled="disabled">Choisir société</option>';

    if($mPaiement)
    {
        foreach ($mPaiement as $itemMpaiemnt)
        {
            $selected  = ' ';
            $strModePaiement    .= '<option value="'.$itemMpaiemnt->id_mode_paiement.'"  >'.$itemMpaiemnt->mode_paiement.'</option>';
        }
    }


    if($societe)
    {
        foreach ($societe as $itemSct)
        {
            $strSociete    .= '<option value="'.$itemSct->id.'"  >'.$itemSct->nom_societe.'</option>'; 
        }
    }

    $strModePaiement        .= '</select>';
    $strTypologie           .= '</select>';
    $strSociete             .= '</select>';
?>


<div class="split_right right annuler-koscan" id="top" style="display:none;">
    <div class="centered">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">

                          
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="infoDatamatrix"/>
                                    <label class="form-label">Infos datamatrix : </label>
                                </div>

                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="code-ecole-gci"/>
                                    <label class="form-label">Code école/GCI : </label>
                                </div>

                            </div>
                            <div class="form-group form-float">
                                <div class="form-line ctitrePli">
                                    <select id="titrePli">
                                        <option value="0" selected disabled="disabled">--Choisir un titre--</option>
                                    </select>
                                    <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Titre : </label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <?php echo $strSociete;?>
                                    <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Société : </label>
                                </div>

                            </div>

                    </div>
                </div>
            </div>
        </div>
<!--
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <form onsubmit="return false;" id="payeurForm">


                        <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" value="000000000000" class="form-control" minlength="-1" maxlength="12"  name="numPayeurName" id="numPayeur" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                                    <label class="form-label">Numéro payeur :</label>
                                </div>

                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control"  maxlength="30" id="nomPayeur" name="nomPayeurName"/>
                                    <label class="form-label">Nom payeur : </label>
                                </div>
                            </div>

                            <div class="form-group form-float" style="display:none;">
                                <div class="form-line">
                                    <input type="text" class="form-control" maxlength="10" id="cpPayeur"  name="cpPayeurName" >
                                    <label class="form-label">Code postal payeur : </label>
                                </div>

                            </div>
                            <div class="form-group form-float" style="display:none;">
                                <div class="form-line">
                                    <input type="text" class="form-control" maxlength="3"  id="cPaysPayeur" name="cPaysPayeurName"/>
                                    <label class="form-label">Code pays payeur : </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> -->

        <!--Info payeur ; abonné-->
    <!--    <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <span class="badge bg-teal"> # 1</span>
                    <div class="body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" value="000000000000" class="form-control" minlength="-1" maxlength="12" name="numPayeurName" id="numPayeur" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
                                <label class="form-label">Numéro payeur :</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" maxlength="30" id="nomPayeur" name="nomPayeurName" />
                                <label class="form-label">Nom payeur : </label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" value="000000000000" id="numAbonne" maxlength="12" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
                                <label class="form-label">Numéro abonné : </label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-line">
                                        <input type="text" class="form-control" id="codePromo" />
                                        <label class="form-label">Code promotion : </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top">
                                        <i class="material-icons"><b>plus_one</b></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div id="apres-info-payeur">

        </div>
        <!--fin Info payeur ; abonné-->

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="form-group form-float" style="display:none;">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="000000000000" id="numAbonne" maxlength="12" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                                    <label class="form-label">Numéro abonné : </label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line c-typo-pli">
                                    <?php echo $strTypologie; ?>
                                    <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Typologie : </label>
                                </div>
                            </div>
                          <div class="form-group form-float" style="display:none;">
                            <div class="form-line">
                                <!-- <input type="text" class="form-control" maxlength="12" id="codePromo" /> -->
                                <input type="text" class="form-control"  id="codePromo" />
                                <label class="form-label">Code promotion : </label>
                            </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-float c-mode-paie">
                                        <div class="form-line">
                                            <?php echo $strModePaiement; ?>
                                            <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Mode de paiement : </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="pli_rejet">

        </div>
        <!--
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <form onsubmit="return false;" >
                            <div class="form-group form-float">
                                    <div class="form-line c-provenance">
                                        <select id="provenance-id">
                                            <option value="0" selected disabled="disabled">--Choisir --</option>
                                        </select>
                                        <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Provenance : </label>
                                    </div>
                            </div>
                            <div class="form-group form-float" style="display:none;">
                                    <div class="form-line c-provenance-type"> 
                                        <select id="provenance-type-id" class="form-group form-control" data-live-search="true">>
                                            <option value="0" selected disabled="disabled"> --Choisir --</option>
                                            
                                        </select>
                                        <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Type provenance : </label>
                                    </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div> -->

        <div class="row clearfix" style="display:none;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <form onsubmit="return false;" id="abonnementForm">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" class="form-control" value="1" id="nbAbonment" name="nbAbonnementName"/>
                                    <label class="form-label">Nombre d'abonnement : </label>
                                </div>
                            </div>
                            <div class="form-group form-float" style="display:none;">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="typeCoupon"/>
                                    <label class="form-label">Type coupon : </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix motif_rejet">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <form onsubmit="return false;" style="display: none;" class="motif-rejet">
                          <!--  <div class="form-group form-float">
                                <div class="form-line">
                                    <?php /*echo $strMotifRejet; */?>

                                    <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Motif rejet : </label>MATC
                                </div>
                            </div> -->
                            <div class="form-group form-float">
                                <div class="form-line cDecriptionRejet" style="display: none">
                                    <textarea rows="4" class="form-control no-resize" id="decriptionRejet" name="decriptionRejetName"></textarea>
                                    <label class="form-label">Description rejet : </label>
                                </div>
                            </div>
                        </form>

                        <div id="boutonPli" >
                            <button title="Annuler le traitement du pli"  type="button" class="btn btn-danger waves-effect annuler-koscan" onclick="annulerPli();" style="display: none !important;">
                                <i class="material-icons">cancel</i>
                                <span>Annuler</span>
                            </button>

                            <button type="button" class="btn bg-teal waves-effect" onclick="validerPli();">
                                <i class="material-icons">verified_user</i>
                                <span>Valider</span>
                            </button>
                            
                            <div class="btn-group dropup ">
                                    <button type="button" class="btn bg-brown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="material-icons">error</i>&nbsp;&nbsp; Pli KO &nbsp;<span class="caret"></span>
                                    </button>
                                    
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:KOscanPli();" class=" waves-effect waves-block"><i class="material-icons">print</i>KO scan</a></li>
                                        <li><a href="javascript:KOinconnuTypage();" class=" waves-effect waves-block"><i class="material-icons">new_releases</i>KO inconnu</a></li>
                                        <li><a href="javascript:KOenAttenteTypage();" class=" waves-effect waves-block"><i class="material-icons">pause</i>KO en attente</a></li>
                                        <li><a href="javascript:KOdefinitif();" class=" waves-effect waves-block"><i class="material-icons">remove_circle</i>KO définitif</a></li>
                                    </ul>
                                </div>
                            
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="split_right right charger" style="background:white;">

</div>