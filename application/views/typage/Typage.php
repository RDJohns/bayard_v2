<?php
/**
 * Created by PhpStorm.
 * User: 9420
 * Date: 06/03/2019
 * Time: 11:35
 */
?>

<div id="" class="formTypage" style="position: fixed;bottom: -20px;width: 100%;">
    <div class="row clearfix" style="margin-right: 0 !important; margin-left: 0 !important;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header" style="padding: 9px !important;">
                    <h2>
                        Typage pli :#
                    </h2>

                </div>
                <div class="body">
                    <input type="hidden" id="nbrCheque" value="1">
                    <div class="">
                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <select class="form-group form-control" >
                                            <option value="0" selected disabled>Choisir société</option>
                                            <option value="10">Bayard</option>
                                            <option value="20">Milan</option>
                                        </select>
                                        <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Société</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">Info datamatrix</label>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">Titre</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">N° payeur</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">N° abonné</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">Code promotion</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-group form-control bootstrap-select">
                                            <option value="0" selected disabled>Choisir la typologie</option>
                                            <option value="10">Bayard</option>
                                            <option value="20">Milan</option>
                                        </select>
                                        <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Typologie</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <select class="form-group bootstrap-select">
                                            <option value="0" selected disabled>Choisir la mode de paiement</option>
                                            <option value="10">Bayard</option>
                                            <option value="20">Milan</option>
                                        </select>
                                        <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Mode de paiement</label>
                                    </div>
                                </div>
                            </div>


                        </div>



                    </div>
                    <div class="cheque_dynamique">

                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <span class="badge" style="margin-top: 10px !important;">Chèque #1 : </span>

                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">Montant</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">CMC7</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">RLMC</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <select class="form-group bootstrap-select">
                                            <option value="0" selected disabled>Choisir l'anomalie du chèque</option>
                                            <option value="10">Bayard</option>
                                            <option value="20">Milan</option>
                                        </select>
                                        <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Anomalie</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-default waves-effect" onclick="ajouterCheque();">
                                    <i class="material-icons">add_circle</i>
                                </button>
                            </div>

                        </div>
                    </div> <!-- cheque_dynamique !-->

                    <div class="pli_rejet">
                    </div>
                </div> <!-- body panel !-->

                <div class="row justify-content-md-center">
                    <div class="col-sm-2 col-sm-offset-2">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <select class="form-group bootstrap-select">
                                    <option value="0" selected disabled>Choisir l'anomalie du chèque</option>
                                    <option value="10">Bayard</option>
                                    <option value="20">Milan</option>
                                </select>
                                <label class="form-label" style="top: -10px;left: 0;font-size: 12px;z-index: 999;">Anomalie</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control">
                                <label class="form-label">RLMC</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2 col-sm-offset-4">
                        <div>
                            <button type="button" class="btn btn-success waves-effect">VALIDER</button>
                            <button type="button" class="btn btn-danger waves-effect">ANNULER</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






