<br/><br/><br/><br/>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="card">
        <div class="body bg-blue-grey">
            Vous-avez encore un pli en cours de saisie. #ID PLI: <?php echo (int)$idPliSaisie; ?>
            <p></p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: white !important;" href="<?php echo site_url()."/saisie/Saisie"?>">Cliquer ici pour terminer la saisie du pli</a>
        </div>
    </div>
</div>