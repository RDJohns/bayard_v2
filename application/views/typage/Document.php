<?php

    $titreMenu  = "";
    $listeDocu  = "";
    $infoDoc    = "";
   // var_dump($info);

    foreach ($info as $itemInfoDoc)
    {
        if((int)$itemInfoDoc->id_pli > 0)
        {
            $titreMenu  = " Pli #<span class='c-pli-id'>".(int)$itemInfoDoc->id_pli.'</span>';
            $listeDocu .='
                <li>
                    <a href="#" onclick="afficheDocID('.(int)$itemInfoDoc->id_document.');">
                        <i class="material-icons">insert_drive_file</i>
                        <span class="icon-name custom_metismenu">Document #'.$itemInfoDoc->id_document.'</span>
                     </a>
                </li>';
            $infoDoc       = " <u style='color: blue;'>Cliquer pour cacher/afficher tous les images</u>";
        }
        else
        {
            $titreMenu  = " Pas de pli";
            $listeDocu  = "";

        }
    }
?>
<ul class="metisFolder metismenu">
    <li class="mm-active">
        <a href="#" aria-expanded="true" onclick="afficheLesDocs();" class="rootDoc" style="">
            <div class="demo-google-material-icon">
                <i class="material-icons">folder</i>
                <span class="icon-name custom_metismenu"><?php echo $titreMenu."  ".$infoDoc; ?></span>
            </div>
        </a>
        <ul class="mm-collapse mm-show" style="">
           <?php echo $listeDocu; ?>
        </ul>
    </li>
</ul>
