<div class="modal fade" id="id-modal-cheque" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body modal-cheque">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-consigne" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header modal-col-red">
                <h4 class="modal-title" id="titre-consige-typage"></h4>
            </div>
            <div class="modal-body content-modal-consigne">
                
            </div>
            <div class="modal-footer">
                <button type="button" data-color="red" class="btn  bg-red waves-effect" data-dismiss="modal" onclick="luConsigne();">J'AI LU LA CONSIGNE</button>
            </div>
        </div>
    </div>
    </div>