<?php
$strMotifRejet      = "";
$strMotifAnomalie   = "";
$strMotifRejet      .= '<select class="form-group form-control"  id="motifRejet" onchange="verifDescription(1);">';
$strMotifAnomalie   .= '<select class="form-group form-control"  id="motifAnomalie" onchange="verifDescription(2);">';
$strMotifRejet      .= '<option value="0"  selected disabled="disabled">--Choisir un motif--</option>';
$strMotifAnomalie  .= '<option value="0"  selected disabled="disabled">--Choisir un motif--</option>';

foreach ($motifRejet as $itemMotifRejet)
{
    $strMotifRejet         .= '<option value="'.$itemMotifRejet->id.'"  >'.$itemMotifRejet->motif.'</option>';
    $strMotifAnomalie      .= '<option value="'.$itemMotifRejet->id.'"  >'.$itemMotifRejet->motif.'</option>';
}
$strMotifRejet              .= '</select>';
$strMotifAnomalie           .= '</select>';
?>

<div class="modal fade" id="ko-scan-rejet-pli" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="alert alert-danger c-error" style="display: none;">
                <strong>Erreur !</strong><span id="txt-error"></span>
            </div>
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Rejeter le pli # <span id="ko-scan-rejet-pli-id"></span></h4>
            </div>
            <div class="modal-body">
               Motif  KO  :  <?php echo $strMotifRejet ; ?>
                <br/>
                <div class="row cDecriptionRejet" style="display: none;">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                Description  :<span style="color: red;font-weight: bold;">(obligatoire)</span>
                                <textarea rows="4" class="form-control no-resize" id="ko-scan-description"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-teal waves-effect" onclick="rejeterPli();">Rejeter le pli</button>
                <button type="button" class="btn bg-blue-grey waves-effect" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="raccourci-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        
        <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Raccourcis</h4>
                </div>
                <div class="modal-body table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="align-center">Touches</th>
                                <th class="align-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td><kbd>Ctrl</kbd> + <kbd>Enter</kbd></td><td>Charger un pli / valider le traitement</td></tr>
                            <tr><td><kbd>Ctrl</kbd> + <kbd>backspace</kbd></td><td>Annuler le traitement</td></tr>
                            <tr><td><kbd>Ctrl</kbd> + <kbd>B</kbd></td><td>Choisir société Bayard</td></tr>
                            <tr><td><kbd>Ctrl</kbd> + <kbd>M</kbd></td><td>Choisir société Milan</td></tr>
                            <tr><td><kbd>Enter</kbd></td><td>Valider confirmation</td></tr>
                            <tr><td><kbd>Esc</kbd></td><td>Fermer le document-viewer / annuler confirmation</td></tr>
                            <tr><td><kbd>Ctrl</kbd> + <kbd>Shift</kbd></td><td>Afficher le document-viewer</td></tr>
                            <tr><td><kbd>Alt</kbd> + <kbd>+</kbd></td><td>Zoom document-viewer</td></tr>
                            <tr><td><kbd>Alt</kbd> + <kbd>-</kbd></td><td>De-zoom document-viewer</td></tr>
                            <tr><td><kbd>Ctrl</kbd> + <kbd>,</kbd></td><td>Rotation document-viewer</td></tr>
                            
                            
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                </div>
            </div>
        
    </div>
</div>






<div class="modal fade" id="ko-motif-rejet-pli" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="alert alert-danger c-error" style="display: none;">
                <strong>Erreur !</strong><span id="txt-error1"></span>
            </div>
            <div class="modal-header">
                <h4 class="modal-title" >Mettre en <span id="pli-ko-type" ></span> le pli # <span id="ko-pli-id"></span></h4>
            </div>
            <div class="modal-body">
                <!--<div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                Description du motif :<span style="color: red;font-weight: bold;">(obligatoire)</span>
                                <textarea rows="4" class="form-control no-resize" id="ko-motif-description"></textarea>
                            </div>
                        </div>
                    </div>
                </div> !-->
                Motif  KO  :  <?php echo $strMotifAnomalie ; ?>
                <br/>
                <div class="row cDecriptionRejet1" style="display: none;">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                Description  :<span style="color: red;font-weight: bold;">(obligatoire)</span>
                                <textarea rows="4" class="form-control no-resize" id="ko-motif-description"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-teal waves-effect" onclick="mettreKOPli();">Mettre KO le pli</button>
                <button type="button" class="btn bg-blue-grey waves-effect" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>