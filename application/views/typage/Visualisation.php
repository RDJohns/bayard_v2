<!-- test user (session, exist pli non ferme, asignation, access -->

<div class="split left">

    <div class="centered">
        <div class="row ">
            <div class="col-sm-6">
                <div class="arborescence slimScrollDivDoc" style="float: left;width: 90%;overflow-y: auto;height: 100px;overflow-x: hidden" >

                </div>
            </div>
            <div class="col-sm-6">
            <div class="form-group form-float charger" style="width: 175px !important; float: left !important; margin-right: 12px !important; margin-top: 4px !important;">
                <div class="form-line">
                    <select class="form-group form-control chosen-select" name="choixSocieteName" data-live-search="true" id="choixSociete" tabindex="-98">
                        <option value="0" <?php if((int)$this->session->userdata('choix_societe') == 0) echo " selected "; ?>>Bayard et Milan</option>
                        <option value="1" <?php if((int)$this->session->userdata('choix_societe') == 1) echo " selected "; ?>>Bayard</option>
                        <option value="2" <?php if((int)$this->session->userdata('choix_societe') == 2) echo " selected "; ?>>Milan</option>
                    </select>
                        <label class="form-label" style="top: 6px;left: -50px;z-index: 999;color: black;font-size: 13px;">Société : </label>
                </div>
               
            </div>
           
                <button type="button" class="btn btn-success waves-effect charger" onclick="chargerPli();" >
                    <i class="material-icons">refresh</i>
                    <span>Charger PLI</span>
                </button>
                <img onclick="visualiserConsigneTypage();" class="img-typage-consigne" title = "Cliquer sur l'image pour voir toutes les consignes" src="<?php echo base_url().'/assets/images/img_consigne.png'?>" alt="logo" style="width: 50px !important;cursor:pointer;display:none;">
                <div style="float: left;width: 90%;overflow-y: auto;height: 100px;overflow-x: hidden;display:none;" class="consigen-pli">
                
					<div class="alert bg-red">
						<span class="text-consigne">
							Consigne pour le traitement du pli# : 
							................
						</span>
						
					</div>
                </div>
              
				
              
                <div class="preloader pl-size-xs" style="display: none;">
                    <div class="spinner-layer pl-red-grey">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="liste_doc" style=" text-align: center !important;">
            <div class="image-set" style="display: inline-block !important;">

            </div>
            <div class="tab-document" style="display: none !important;">
                <div class="body">
                        <ul class="nav nav-tabs tab-col-pink" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#doc-recto" data-toggle="tab" aria-expanded="true">RECTO</a>
                            </li>
                            <li role="presentation">
                                <a href="#doc-verso" data-toggle="tab">VERSO</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="doc-recto">

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="doc-verso">


                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>

</div>






<!-- TODO modal toute consigne-->
<div class="modal fade" id="mdl_disp_motif_consigne_pli" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg_" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <div class="row">
               <div class="col-lg-8">
                  <h4 class="modal-title">Motif-consignes du Pli#<span id="mdl_disp_motif_consigne_pli_id_pli"></span></h4>
               </div>
               <div class="col-lg-4 align-right">
                  <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
               </div>
            </div>
         </div>
         <div class="modal-body" id="mdl_disp_motif_consigne_pli_body" style="max-height: 60vh; overflow-y: auto;"></div>
      </div>
   </div>
</div>
<!-- -->