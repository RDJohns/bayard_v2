<div class="container-fluid">
    <div style="margin-top: 100px;">
                <div class="row">
                    <div class="col-md-1 filtre" style="margin-top:10px;">Courrier du</div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="text" class="form-control nsaisi-date" id="nsaisi_date_debut" value="<?php echo date( 'd/m/Y', strtotime( 'first day of this month' ) );?>">
                            </div>
                            <span class="input-group-addon">au&nbsp;&nbsp;</span>
                            <div class="form-line">
                                <input type="text" class="form-control nsaisi-date" id="nsaisi_date_fin" value="<?php echo date( 'd/m/Y', strtotime( 'last day of this month' ) );?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <select id="choix-societe" class="form-control">
                            <option value="">-- Choix société --</option>
                            <option value="1">Bayard</option>
                            <option value="2">Milan</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary waves-effect" onclick="show_nosaisie()">Afficher</button>
                    </div>
                </div>
        <div class="row m-t-20">
            <div class="col-md-12">
                <div class="card" style="opacity: 0">
                    <div class="header">
                        <h2>Ch&egrave;ques non saisis</h2>
                    </div>
                    <div class="body">
                        <table id="table-nosaisie" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Date courrier</th>
                                <th>Id pli</th>
                                <th>Pli</th>
                                <th>Lot scan</th>
                                <th>Etape</th>
                                <th>Etat</th>
                                <th>Statut</th>
                                <th>CMC7</th>
                                <th>Montant</th>
                                <th>Statut ch&egrave;que</th>
                                <th>Commande</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="preloader nsaisi_preload" id="preloader_nsaisi" style="margin-left: 50%;">
            <div class="spinner-layer pl-black">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>