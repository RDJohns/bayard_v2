<div class="row">
    <div class="col-lg-12">
        <div class="card card_filtre">
            <div class="header">
                <h2>
                    CRITERES DE RECHERCHE
                    <!-- <small>Veuillez renseigner les champs selon les besoins</small> -->
                </h2>
            </div>
            <div class="body">
                <div class= "row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row date_picker">
                            <div class="col-xs-3 col_hx m-t-10"><b>Traitement saisie du : </b></div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_act_1" />
                                        <label class="form-label">debut</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_act_2" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <p><b>Société:</b></p>
                        <select class="form-control show-tick" id="sel_societe" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($societes as $soc): ?>
                                    <option value="<?php echo $soc->id?>" ><?php echo $soc->nom_societe ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <p><b>Etape:</b></p>
                        <select class="form-control show-tick" id="sel_etape" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($etapes as $etp): ?>
                                    <option value="<?php echo $etp->id_flag_traitement?>" ><?php echo $etp->traitement ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <p><b>Statut :</b></p>
                        <select class="form-control show-tick" id="sel_etat" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                        
                            <?php foreach ($etats as $etp): ?>
                                    <option value="<?php echo $etp->id_statut_saisie?>" ><?php echo $etp->libelle ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-2col-md-2 col-sm-2col-xs-2 col_hx">
                        <button type="button" class="btn btn-block btn-info btn-lg waves-effect m-t-20" onclick="load_especes();">
                            <i class="material-icons">search</i>
                            <span>Rechercher / Rafraichir</span>
                        </button>
                    </div>
                </div>
        </div>
    </div>
    <div class="row" id="search_especes">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                                <h2>
                                    RESULTATS
                                    <!-- <small>All pictures taken from <a href="https://unsplash.com/" target="_blank">unsplash.com</a></small> -->
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <!-- <li><a href="javascript:void(0);" onclick= "" class=" waves-effect waves-block">Retraiter tous</a></li> -->
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Annuler</a></li>
                                            <!-- <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li> -->
                                        </ul>
                                    </li>
                                </ul>
                    </div>
                <div class="body">
                    <table id="suivi_especes" class="table table-bordered table-striped table-hover" style="width: 100%;">
                        <thead>
                            <tr>
                                <th class="align-center">Date courrier</th>
                                <th class="align-center">Date Traitement saisie</th>
                                <th class="align-center">#ID pli</th>
                                <th class="align-center">Lot scan</th>
                                <th class="align-center">Commande</th>
                                <th class="align-center">Société</th>
                                <th class="align-center">Montant</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>