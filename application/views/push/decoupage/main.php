<br><br><br><br><!-- fin global -->

<div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 voir-liste-pli ged-reception" style="margin-top:10px;">
    <div class="card">
    <div class="header">
            <h5>FICHIER EN COURS DE TRAITEMENT</h5>
    </div>
        <div class="body">
            <div class="row">
                <div class="col-md-2">
                    <button type="button" class="btn btn-success waves-effect btn-charger-fichier" onclick="chargerFichier();"><i class="material-icons">autorenew</i>&nbsp;&nbsp;&nbsp;Charger un fichier </button>
                    <button style ="display:none;" type="button" class="btn btn-danger waves-effect btn-annuler-tmt" onclick="annulerTraitement();"><i class="material-icons">report_problem</i>&nbsp;&nbsp;&nbsp;Annnuler le traitement </button>
                </div>

                <div class="col-md-10 c-fichier" style="padding-left: 30px !important;"></div>

            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 voir-liste-pli ged-reception c-dropzone-decoupage" style="margin-top:-20px;display:none !important;">
    <div class="card">
        <div class="header">
            <h5>UPLOADER ICI LE(S) FICHIER(S) A PUBLIPOSTER</h5>
           <!-- <div class="row">
                <div class="col-md-2">
                    <span style="float:right">Type de fichier</span>
                </div>
                <div class="col-md-4">
                <select class="form-control show-tick" onchange="showDropzone();" multiple>
                    <option value="0" disabled >-- Sélectionner --</option>
                    <option value="1" selected>FLUX PUBLIPOSTES NORMAL</option>
                    <option value="2" selected>FLUX PUBLIPOSTES REJET KO</option>
                </select>
                </div>
            </div> -->
        </div>
        <div class="body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <span type="button" class="label label-info" style="font-size: 90%;padding: 5px;">&nbsp;&nbsp;&nbsp;FLUX PUBLIPOSTES NORMAL</span>

                     <span class="todepot_order">
                         <form action="#" class="dropzone hzScroll" id="decoupage" style="border: solid 1px #1f91f3  !important;">
                        </form>
                    </span>

                    </div>
                    
                    
                    
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                    <span type="button" class="label label-danger" style="font-size: 90%;padding: 5px;">&nbsp;&nbsp;&nbsp;FLUX PUBLIPOSTES REJET KO</span>

                     <span class="todepot_order_rejet">
                         <form action="#" class="dropzone hzScroll" id="decoupage-rejet-ko" style="border: solid 1px #fe0505 !important;">
                        </form>
                    </span>

                    </div>
                    
                </div>
            </div>
            <button type="button" style="position: relative;bottom: 35px;" class="btn btn-primary waves-effect" onclick="terminerDecoupage();"><i class="material-icons">done_all</i>&nbsp;&nbsp;&nbsp;Terminer le découpage</button>
        </div>
        
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 voir-fichier_decoupe" style="margin-top:-20px;">
    <div class="card">
        <div class="header">
            <h5>VISUALISATION DE TRAITEMENT</h5>
        </div>
        <div class="body">
            <table class="table table-bordered table-striped table-hover dataTable" id="table-decoupage">
                <thead>
                    <tr>
                        <th>ID Flux</th>
                        <th>Fichier origine</th>
                        <th>Fichier publiposté</th>
                        <th>Date de de traitement</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<div class="preloader-reception">
        <div class="preloader pl-size-xs"><div class="spinner-layer pl-indigo"><div class="circle-clipper left"><div class="circle"></div></div>
        <div class="circle-clipper right"><div class="circle"></div></div></div></div>
        <span>Chargement de données...</span>
</div>