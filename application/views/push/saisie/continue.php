<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row p-t-20">

    <div class="col-sm-8 col-xs-12 align-center" id="div_bt_next_flux">
        <h3 class="op_7">Lot saisie en cours&nbsp;
            <span class="label label-default"><?php echo $label_advantage ?></span>&nbsp;
            <button type="button" class="btn btn-default waves-effect" onclick="copyToClipboard('<?php echo $label_advantage ?>');">
                <i class="material-icons">content_copy</i>Copier
            </button>
        </h3>
        <h4 class="op_7">Flux enrégistrés <span class="label label-default"><?php echo $nb_pli_in_this_lot_saisie ?></span></h4>
        <h5 class="op_7"><?php echo $soc ?> <i class="material-icons font-10">label_outline</i> <?php echo $src ?> <i class="material-icons font-10">label_outline</i> <?php echo $typo ?></h5>
        <p class="">
            <button type="button" class="btn bg-blue btn-lg waves-effect" onclick="next_flux();">
                <i class="material-icons">assignment_returned</i>
                <span><?php echo $lbl_bouton ?></span>
            </button>
        </p>
    </div>

    <div class="col-sm-4 col-xs-12 align-center m-t-60">
        <button type="button" class="btn bg-orange btn-lg waves-effect" onclick="new_lot_saisie();">
            <i class="material-icons">content_paste</i>
            <span>Nouveau lot de saisie</span>
        </button>
    </div>
	
</div>

<script>
    
    function get_filters_value() {
        return {
            id_soc: '<?php echo $id_soc ?>'
            ,id_src: '<?php echo $id_source ?>'
            ,id_typo: '<?php echo $id_typologie ?>'
        };
    }

</script>

<style>
#div_bt_next_flux{
    border-right: 1px grey dashed;
}
</style>
