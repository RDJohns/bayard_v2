<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb breadcrumb-bg-blue-grey align-center">
            <li><i class="material-icons">account_balance</i> <?php echo $flux->nom_societe ?></li>
            <li><i class="material-icons">move_to_inbox</i> <?php echo $flux->src ?></li>
            <li><i class="material-icons">assignment</i> <?php echo $flux->typologie ?></li>
            <li class="active">
                <i class="material-icons">markunread</i> #
                <?php echo $flux->id_flux ?>
                <?php echo ($flux->date_reception == '' ? '' : ' ['.(new DateTime($flux->date_reception))->format('d/m/Y').']') ?>
                <?php echo ($flux->flag_important == 1 ? ' <small class="font-bold col-orange">[PRIO]</small>' : '') ?>
            </li>
        </ol>
    </div>
</div>

<div class="row m-l-0 m-r-0">

    <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 p-r-5 p-l-10">

        <div class="card card_fx m-b-0">
            <div class="body p-b-5">
                
                <div class="row">
                    <?php if ($flux->id_source == 1): ?>
                    <div class="col-lg-8">
                        <a href="<?php echo (site_url('push/saisie/ajax_push/download_mail/'.$flux->id_flux)) ?>" download="" title="t&eacute;l&eacute;charger">
                        <i class="material-icons font-15">file_download</i>
                            <i class="material-icons font-15">mail</i>
                            <?php echo $flux->nom_fichier ?>
                        </a>
                    </div>
                    <?php endif; ?>
                    <?php if ($flux->id_source == 2): ?>
                    <div class="col-lg-8">
                        <a href="<?php echo (site_url('push/saisie/ajax_push/download_sftp_file/'.$flux->id_flux)) ?>" download="" title="t&eacute;l&eacute;charger">
                        <i class="material-icons">file_download</i>
                            <i class="material-icons">insert_drive_file</i>
                            <?php echo $flux->filename_origin ?>
                        </a>
                    </div>
                    <?php endif; ?>
                    <div class="col-lg-4">
                    <?php if (!is_null($data_ke)): ?>
                        <span>
                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Consigne client">
                                <i class="material-icons" data-toggle="popover" data-placement="bottom" title="Consigne [<?php echo (new DateTime($data_ke->dt))->format('d/m/Y H:m') ?>]" data-content="<?php echo $data_ke->commentaire ?>">info_outline</i>
                            </a>
                        </span>
                        <?php if ($nb_files > 0): ?>
                        <span>
                            <a href="<?php echo site_url('push/saisie/retraitement_push/download_all_files_ke/'.$flux->id_flux) ?>" download="" data-toggle="tooltip" data-placement="top" title="T&eacute;l&eacute;charger fichier consigne">
                                <i class="material-icons">attachment</i>
                            </a>
                        </span>
                        <?php endif; ?>
                    <?php endif; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <p>
                            <b>Soci&eacute;t&eacute; :</b>
                            <select class="form-control show-tick sel_field field" id="field_soc" onchange="change_soc();">
                                <?php foreach ($socs as $soc): ?>
                                <option value="<?php echo $soc->id ?>" <?php echo ($flux->soc == $soc->id ? 'selected' : '') ?>><?php echo $soc->nom_societe ?></option>
                                <?php endforeach; ?>
                            </select>
                        </p>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group form-float m-b-0 m-t-20">
                            <div class="form-line">
                                <input type="text" class="form-control" id="field_entity" value="<?php echo $flux->entite ?>" />
                                <label class="form-label">Entit&eacute;</label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            <b>Typologie :</b>
                            <select class="form-control show-tick sel_field field" id="field_typo" disabled data-container="body" data-live-search="true" data-size="5" onchange="">
                                <?php foreach ($typos as $typo): ?>
                                <option value="<?php echo $typo->id_typologie ?>" <?php echo ($flux->id_typo == $typo->id_typologie ? 'selected' : '') ?>><?php echo $typo->typologie ?></option>
                                <?php endforeach; ?>
                            </select>
                        </p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            <b>Statut :</b>
                            <select class="form-control show-tick sel_field field" id="field_statut" data-container="body" onchange="change_state();">
                                <?php foreach ($statuts as $statut): ?>
                                <option value="<?php echo $statut->id_statut ?>" <?php echo ($default_statut == $statut->id_statut ? 'selected' : '') ?>><?php echo $statut->statut ?></option>
                                <?php endforeach; ?>
                            </select>
                        </p>
                    </div>
                </div>
                
                <?php if ($with_motif_escalade): ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group form-float m-b-5">
                            <div class="form-line">
                                <input type="text" class="form-control" id="field_motif_escalade" value="<?php echo $flux->motif_escalade ?>" />
                                <label class="form-label">Motif escalade</label>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                
                <?php if ($with_motif_transfert): ?>
                <div class="row line_motif_ko">
                    <div class="col-lg-12">
                        <div class="form-group form-float m-b-5">
                            <div class="form-line">
                                <input type="text" class="form-control" id="field_motif_transfert" value="<?php echo $flux->motif_transfert ?>" />
                                <label class="form-label">Motif KO</label>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                
                <?php if ($with_motif_ks): ?>
                <div class="row line_motif_ks">
                    <div class="col-lg-12">
                        <div class="form-group form-float m-b-5">
                            <div class="form-line">
                                <input type="text" class="form-control" id="field_motif_ks" value="<?php echo $flux->motif_ks ?>" />
                                <label class="form-label">Motif KO-transfert SRC (KS)</label>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                
                <div class="row <?php echo ($flux->id_src == 2 ? 'hidden' : '') ?>">
                    <div class="col-lg-12 m-t-5">
                        <label for="comment_ko_scan">Nom pi&egrave;ce jointe 
                            <button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float" onclick="add_pj();">
                                <i class="material-icons">add</i>
                            </button> :</label>
                        <div class="form-group m-b-5">
                            <div class="form-line">
                                <ul class="list-group" id="ul_list_pjs">
                                    <?php foreach ($pjs as $pj): ?>
                                    <li class="list-group-item" is_ajoute="<?php echo $pj->ajoute ?>" my_date="<?php echo $pj->date_injection ?>"><span class="nom_fichier_pj"><?php echo $pj->nom_fichier ?></span> <?php echo ($pj->ajoute == 1 ? '<span class="badge bg-pink m-t--5" onclick="del_pj($(this));"><i class="material-icons font-22 ico_modif_pj">clear</i></span><span class="badge bg-cyan m-t--5" onclick="edit_pj($(this));"><i class="material-icons font-22 ico_modif_pj">create</i></span>' : '') ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-lg-12">
                        <div class="btn-group dropup btn-block align-center">
                            <button type="button" class="btn bg-blue btn-lg waves-effect bt_ss" onclick="quick_save();" title="Enregistrement rapide du travail en cours">
                                <i class="material-icons">save</i>
                                <span>Enregistrer</span>
                            </button>           
                            <button type="button" class="btn bg-deep-orange btn-lg waves-effect bt_ss" onclick="terminer();" title="Valider et Terminer ce traitement">
                                <i class="material-icons">done_all</i>
                                <span>Terminer</span>
                            </button>           
                            <button type="button" class="btn bg-green btn-lg waves-effect bt_ss" onclick="annuler();" title="Annuler ce traitement">
                                <i class="material-icons">cancel</i>
                                <span>Annuler</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php if(!is_null($motif_consigne_flux)){ ?>
            <?php if(trim($motif_consigne_flux->motif_operateur_flux) != '' && FALSE){ ?>
                <div class="alert alert-danger m-b-0">
                    <strong>Op&eacute;rateur:</strong> <?php echo $motif_consigne_flux->motif_operateur_flux ?>
                </div>
            <?php } ?>
            <?php if(trim($motif_consigne_flux->consigne_client_flux) != ''){ ?>
                <div class="alert alert-info m-b-0">
                    <strong>Consigne:</strong> <?php echo $motif_consigne_flux->consigne_client_flux ?>
                </div>
            <?php } ?>
        <?php } ?>

    </div>

    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 p-l-5 p-r-10" id="div_liste_abonnement">

    <?php echo $view_abonnements ?>

    </div>

</div>

<script>
    ID_FLUX = '<?php echo $flux->id_flux ?>';
    ID_SRC = '<?php echo $flux->id_src ?>';
    ID_SOC = '<?php echo $flux->soc ?>';
</script>
