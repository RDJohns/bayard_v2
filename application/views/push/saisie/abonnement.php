<?php defined('BASEPATH') OR exit('No direct script access allowed');

$identifiant = date('U').mt_rand();

?>

<div class="card card_fx grp_field_abonnement" id="grp_field_abonnement_<?php echo $identifiant ?>" my_ident="<?php echo $identifiant ?>" >
    <div class="header bg-deep-purple">
        <h2>Abonnement <span class="numerotation_abonnement font-14"></span></h2>
        <ul class="header-dropdown m-r--5">
            <li>
                <a href="javascript:get_view_abonnement();">
                    <i class="material-icons">add</i>
                </a>
            </li>
            <li class="clear_abonnement">
                <a href="javascript:suppr_gr_field_abonnement('#grp_field_abonnement_<?php echo $identifiant ?>');">
                    <i class="material-icons">clear</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="body p-b-5">
        <div class="row">
            
            <div class="col-lg-6">
                <div class="form-group form-float m-b-5">
                    <div class="form-line">
                        <input type="text" class="form-control field field_abonnement nb oblig" id="field_abonnement_num_abon_<?php echo $identifiant ?>" value="<?php echo $abonnement->numero_abonne ?>" />
                        <label class="form-label">Numéro de l'abonn&eacute;</label>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6">
                <div class="form-group form-float m-b-5">
                    <div class="form-line">
                        <input type="text" class="form-control field field_abonnement" id="field_abonnement_nom_abon_<?php echo $identifiant ?>" value="<?php echo $abonnement->nom_abonne ?>" />
                        <label class="form-label">Nom de l'abonn&eacute;</label>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-5">
                <div class="form-group form-float m-b-5">
                    <div class="form-line">
                        <input type="text" class="form-control field field_abonnement nb oblig" id="field_abonnement_num_payeur_<?php echo $identifiant ?>" value="<?php echo $abonnement->numero_payeur ?>" />
                        <label class="form-label">Numéro du payeur</label>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-5">
                <div class="form-group form-float m-b-5">
                    <div class="form-line">
                        <input type="text" class="form-control field field_abonnement" id="field_abonnement_nom_payeur_<?php echo $identifiant ?>" value="<?php echo $abonnement->nom_payeur ?>" />
                        <label class="form-label">Nom du payeur</label>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-2">
                <div class="form-group form-float m-b-5">
                    <div class="form-line">
                        <input type="text" class="form-control field field_abonnement montant" id="field_abonnement_tarif_<?php echo $identifiant ?>" value="<?php echo u_montant($abonnement->tarif) ?>" onchange="$(this).val(u_montant($(this).val()));" />
                        <label class="form-label">Tarif</label>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6">
                <p>
                    <b>Titre :</b>
                    <select class="form-control show-tick sel_field field sel_field_abonnement field_abonnement_titre" id="field_abonnement_titre_<?php echo $identifiant ?>" data-container="body" data-live-search="true" data-size="5" onchange="numerotation_abonnement();">
                        <?php foreach ($titres as $titre): ?>
                        <option value="<?php echo $titre->id_titre ?>" <?php echo ($abonnement->titre == $titre->id_titre ? 'selected' : '') ?> title="<?php echo $titre->code ?>" data-subtext=" - <?php echo $titre->code ?>"><?php echo $titre->titre ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>
            
            <div class="col-lg-6">
                <p>
                    <b>Typologie :</b>
                    <select class="form-control show-tick sel_field field sel_field_abonnement field_abonnement_typo" id="field_abonnement_typo_<?php echo $identifiant ?>" data-container="body" data-live-search="true" data-size="5" onchange="gestion_multi_typo();">
                        <?php foreach ($typos as $typo): ?>
                        <option value="<?php echo $typo->id_typologie ?>" <?php echo ($abonnement->typologie_abonnement == $typo->id_typologie ? 'selected' : '') ?>><?php echo $typo->typologie ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>

            <div class="col-lg-6">
                <p>
                    <b>Statut :</b>
                    <select class="form-control show-tick sel_field field sel_field_abonnement" id="field_abonnement_statut_<?php echo $identifiant ?>" data-container="body" data-size="5" onchange="gestion_motif_ko_abonnement($(this).val(), 'dom_field_abonnement_motif_ko_<?php echo $identifiant ?>');">
                        <?php foreach ($statuts as $statut): ?>
                        <option value="<?php echo $statut->id_statut ?>" <?php echo ($abonnement->id_statut == $statut->id_statut ? 'selected' : '') ?>><?php echo $statut->statut ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>
            
            <div class="col-lg-6" id="dom_field_abonnement_motif_ko_<?php echo $identifiant ?>" <?php echo ($abonnement->id_statut == 1 ? 'style="display: none;"' : '') ?>>
                <div class="form-group form-float m-b-0 m-t-20">
                    <div class="form-line">
                        <input type="text" class="form-control field field_abonnement" id="field_abonnement_motif_ko_<?php echo $identifiant ?>" value="<?php echo $abonnement->motif_ko ?>" />
                        <label class="form-label">Motif KO</label>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
