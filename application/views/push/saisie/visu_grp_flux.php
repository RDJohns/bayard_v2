<?php defined('BASEPATH') OR exit('No direct script access allowed');
$totaux = 0;
?>

<?php if (count($grps) > 0): ?>
<div class="row row_list_regroupement" id="row_list_regroupement">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th class="align-center">Date r&eacute;ception</th>
                <th class="align-center">Soci&eacute;t&eacute;</th>
                <th class="align-center">Source</th>
                <th class="align-center">typologie</th>
                <th class="align-center">Quantit&eacute;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($grps as $grp): ?>
            <tr onclick="to_filter(<?php echo $grp->id_soc.','.$grp->id_src.','.$grp->id_typo ?>);" style="cursor: pointer;">
                <td><?php echo ($grp->dt == '' ? '' : (new DateTime($grp->dt))->format('d/m/Y')) ?></td>
                <td><?php echo $grp->soc ?></td>
                <td><?php echo $grp->src ?></td>
                <td><?php echo $grp->typo ?></td>
                <td class="align-center"><?php echo $grp->nb ?></td>
                <?php $totaux += $grp->nb; ?>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td class="align-center font-bold" colspan="4">TOTAL</td>
                <td class="align-center font-bold"><?php echo $totaux ?></td>
            </tr>
        </tbody>
    </table>
</div>
<?php else: ?>
<div class="row">
    <div class="col-xs-12 align-center">
        <div class="alert alert-info">
            Aucun flux disponible!
        </div>
    </div>
</div>
<?php endif; ?>

<script>
function to_filter(soc, src, typo){
    if(($('#form_filtre_saisie')).length > 0){
        $('#sel_filtre_id_societe').selectpicker('val', soc);
        $('#sel_filtre_id_source').selectpicker('val', src);
        $('#sel_filtre_id_typo').selectpicker('val', typo);
    }
}
</script>
