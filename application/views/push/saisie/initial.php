<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row p-t-20">

    <?php if ($with_return): ?>
    <div class="col-lg-12 align-left">
        <a href="<?php echo $link_init ?>" type="button" class="btn bg-green btn-circle waves-effect waves-circle waves-float" title="Revenir">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>
    <?php endif; ?>

    <div class="col-lg-12 align-center">
        <h2>
            <small>Nouveau lot saisie: </small>
            <span id="id_lot_saisie"><?php echo $label_advantage ?></span>&nbsp;
            <button type="button" class="btn btn-default waves-effect" onclick="copyToClipboard('<?php echo $label_advantage ?>');">
                <i class="material-icons">content_copy</i>Copier
            </button>
        </h2>
    </div>

    <div class="col-lg-12 align-center">
        <?php echo $filtres ?>
    </div>

	<div class="col-lg-12 align-center">
		<button type="button" class="btn bg-blue btn-lg waves-effect" onclick="load_flux();">
            <i class="material-icons">assignment_returned</i>
            <span><?php echo $lbl_bouton ?></span>
        </button>
    </div>
    
</div>

    <?php if (!empty($message)): ?>
    <div class="row m-t-30">
        <div class="col-xs-12 align-center">
            <div class="alert bg-orange">
                <?php echo $message ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
