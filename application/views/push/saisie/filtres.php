<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="card m-l-25 m-r-25">
    <div class="body">
        <div class="row" id="form_filtre_saisie">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <p><b>Soci&eacute;t&eacute;:</b></p>
                <select class="form-control show-tick sel_field" id="sel_filtre_id_societe" data-container="body">
                    <?php foreach ($societies as $society): ?>
                        <option value="<?php echo $society->id ?>" <?php echo ($society->id == $current_society ? 'selected' : '') ?> ><?php echo $society->nom_societe ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <p><b>Source:</b></p>
                <select class="form-control show-tick sel_field" id="sel_filtre_id_source" data-container="body">
                    <?php foreach ($sources as $source): ?>
                        <option value="<?php echo $source->id_source ?>" <?php echo ($source->id_source == $current_source ? 'selected' : '') ?> ><?php echo $source->source ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <p><b>Typologies:</b></p>
                <select class="form-control show-tick sel_field" id="sel_filtre_id_typo" data-container="body" data-size="8" data-live-search="true">
                    <?php foreach ($typologies as $typologie): ?>
                        <option value="<?php echo $typologie->id_typologie ?>" <?php echo ($typologie->id_typologie == $current_typologie ? 'selected' : '') ?> ><?php echo $typologie->typologie ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>
</div>

<script>
    function get_filters_value() {
        return {
            id_soc: $('#sel_filtre_id_societe').val()
            ,id_src: $('#sel_filtre_id_source').val()
            ,id_typo: $('#sel_filtre_id_typo').val()
        };
    }
</script>
