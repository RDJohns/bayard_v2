<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row row_histo_pli">
    <div class="timeline">
        <span class="timeline-label">
            <span class="label label-primary">Maintenant: <?php echo date('d/m/Y H:i:s') ?></span>
        </span>
        <?php foreach ($pli_events as $key => $pli_event): ?>
        <div class="timeline-item">
            <div class="timeline-point timeline-point-default">
                <i class="material-icons"><?php echo 'local_offer' ?></i>
            </div>
            <div class="timeline-event timeline-event-warning" >
                <div class="timeline-heading"><h4><?php echo $pli_event->label_action ?></h4></div>
                <div class="timeline-body">
                    <div><?php echo $pli_event->login ?></div>
                    <?php if ($pli_event->coms_trace != ''): ?>
                    <div class="font-italic font-10"><?php echo $pli_event->coms_trace ?></div>
                    <?php endif; ?>
                    <div class="align-right"><small><?php echo $pli_event->date_action ?></small></div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <span class="timeline-label">
            <span class="label label-primary">Nouveau pli: <?php echo $pli_event->date_reception ?></span>
        </span>
    </div>
</div>

<style>
    div.timeline-point i{
        font-size: 17px !important;
        margin-top: 3px !important;
        margin-left: 2px !important;
    }
</style>
