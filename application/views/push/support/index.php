<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card card_filtre">
            <div class="body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group form-float m-t-30">
                            <div class="form-line">
                                <input type="text" class="form-control" id="txt_recherche_id_pli" onkeyup="//load_table();" />
                                <label class="form-label">#ID flux</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Source :</b></p>
                       
                        <select class="form-control show-tick" id="sel_source" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                            <?php foreach ($source as $src): ?>
                                <option value="<?php echo $src->id_source ?>" ><?php echo $src->source ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Société:</b></p>
                        <select class="form-control show-tick" id="sel_societe" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                            <?php foreach ($societe as $soc): ?>
                                    <option value="<?php echo $soc->id?>" ><?php echo $soc->nom_societe ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Typologie:</b></p>
                        <select class="form-control show-tick" id="sel_typologie" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                            <?php foreach ($typologies as $typo): ?>
                                    <option value="<?php echo $typo->id_typologie?>" ><?php echo $typo->typologie ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Etape:</b></p>
                        <select class="form-control show-tick" id="sel_statut" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($statut as $stat): ?>
                                    <option value="<?php echo $stat->id_statut?>" ><?php echo $stat->statut ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Etat:</b></p>
                        <select class="form-control show-tick" id="sel_etat" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($etat as $et): ?>
                                    <option value="<?php echo $et->id_etat_pli?>" ><?php echo $et->libelle_etat_pli ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Saisi par:</b></p>
                        <select class="form-control show-tick" id="sel_op_saisie" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($user_saisie as $op_saisie): ?>
                                    <option value="<?php echo $op_saisie->id_utilisateur?>" ><?php echo $op_saisie->login ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Typé par:</b></p>
                        <select class="form-control show-tick" id="sel_op_typage" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($user_typage as $ust): ?>
                                    <option value="<?php echo $ust->id_utilisateur?>" ><?php echo $ust->login ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group form-float m-t-30">
                            <div class="form-line">
                                <input type="text" class="form-control" id="from_abonne"/>
                                <label class="form-label">Nom Abonné ou N°Abonné</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group form-float m-t-30">
                            <div class="form-line">
                                <input type="text" class="form-control" id="from_payeur"/>
                                <label class="form-label">Nom Payeur ou N°Payeur</label>
                            </div>
                        </div>
                    
                    </div>
                </div>
                <div class= "row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Réception du : </b></div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_act_1" />
                                        <label class="form-label">debut</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_act_2" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p><b>Motif blocage:</b></p>
                        <select class="form-control show-tick" id="select_motif" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                            <?php foreach ($motif as $mtfb): ?>
                                    <option value="<?php echo $mtfb->id_motif?>" ><?php echo  $mtfb->motif ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_hx">
                        <button type="button" class="btn btn-block btn-info btn-lg waves-effect m-t-20 btnsearch" onclick="load_table();">
                            <i class="material-icons">search</i>
                            <span>Rechercher / Rafraichir</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="search_result">
    <div class="col-lg-12">
        <div class="card">
            <div class="header">
                            <h2>
                                RESULTATS
                                <!-- <small>All pictures taken from <a href="https://unsplash.com/" target="_blank">unsplash.com</a></small> -->
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" onclick= "/*RetraiterTous()*/" class=" waves-effect waves-block">Retraiter tous</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Annuler</a></li>
                                         <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li> 
                                    </ul>
                                </li>
                            </ul> -->
                </div>
            <div class="body">
                <table id="support_push" class="table table-bordered table-striped table-hover" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Retraitement</th>
                            <th class="align-center">#ID Flux</th>
                            <th class="align-center">Source</th>
                            <th class="align-center">Soci&eacute;t&eacute;</th>
                            <th class="align-center">Typologie</th>
                            <th class="align-center">Etape</th>
                            <th class="align-center">Etat</th>
                            <th class="align-center">Motif transfert</th>
                            <th class="align-center">Motif escalade</th>
                            <th class="align-center">Saisi par</th>
                            <th class="align-center">Typ&eacute; par</th>
                            <th class="align-center">Nombre d'abo.</th>
                            <th class="align-center">Prioris&eacute;</th>
                            <th class="align-center">Date de réception</th>
                            <th class="align-center">Prioriser</th>
                            <th class="align-center">Action</th>
                            <th class="align-center">Initialiser</th>
                            <th class="align-center">Anomalie</th>
                            <!--<th class="align-center">Prioris&eacute;</th>
                            <th class="align-center">Typ&eacute; par</th>
                            <th class="align-center">Type</th>
                            <th class="align-center">Lot.Saisie</th>
                            <th class="align-center">Saisie par</th>
                            <th class="align-center">Control&eacute; par</th>
                            <th class="align-center">Retrait&eacute; par</th>                           
                            <th class="align-center">Consigne</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- modal liste abonnement -->
<div class="modal fade" id="mdl_display_abo" tabindex="-1" role="dialog">
    <div class="modal-dialog abo_flux" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id='abon_head'><span>Abonnement Flux#</span></h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid"> 
                    <div id="abo_body">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>
<!-- histo flux-->
<div class="modal fade" id="mdl_display_histo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title histo_title" id='histo_head'><span>Historique Flux#</span></h4>
            </div>
                <div class="modal-body histo_flux">
                    <div class="container-fluid"> 
                        <div id="histo_body">
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<!--- modal for anomalie --->
<div class="modal fade" id="mdl_display_ano" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><span>Anomalie Flux#</span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="ano_body" class="modal-body mod_ano">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>