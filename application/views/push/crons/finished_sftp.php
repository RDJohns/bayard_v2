<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!--
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title></title>
        <meta charset="UTF-8">
    </head>
    <body> -->
        <p><img src="data:image/png;base64,<?php echo $logo ?>" alt="Logo <?php echo $soc_name ?>"></p>
        <p>Bonjour,</p>
        <p>
            Merci de trouver ci-dessous l'état de traitement du fichier <span style="font-weight: bold;"><?php echo $nom_fichier_sftp ?></span>:&nbsp;
        </p>
        <p>
            <ul>
                <?php foreach ($abonnements as $key => $abonnement): ?>
                <li>abonn&eacute; n&deg; <?php echo $abonnement->numero_abonne ?>:&nbsp;
                <span style="font-weight: bold;color: <?php echo ($abonnement->id_stat == 1 ? 'green' : 'red') ?>;"><?php echo $abonnement->statut ?></span><?php echo ($abonnement->id_stat == 2 ? ' - <span style="font-style: italic">'.$abonnement->motif_ko.'</span>' : '') ?></li>    
                <?php endforeach; ?>
            </ul>
        </p>
        <p>
            Cordialement,<br>--<br><span style="font-weight: bold;">L'&eacute;quipe <?php echo $soc_name ?></span>
        </p>
    <!-- </body>
</html> -->
