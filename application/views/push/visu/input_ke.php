<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-header">
    <h4 class="modal-title">Consigne pour Flux#<?php echo $id_flux ?></h4>
</div>
<div class="modal-body" >
    <div class="row">
        <div class="col-lg-<?php echo (count($info_ke->fichier_ke) > 0 ? '6' : '12') ?>">
            <div class="row">
                <label for="comms_ke">Consigne:</label>
            </div>
            <div class="row">
            <textarea rows="1" class="form-control no-resize auto-growth" id="comms_ke" placeholder="Appuyer sur la touche <Entr&eacute;e> pour aller &agrave; la ligne..."><?php echo (is_null($info_ke->data_ke) ? '' : $info_ke->data_ke->commentaire) ?></textarea>
            </div>
        </div>
        <?php if (count($info_ke->fichier_ke) > 0): ?>
        <div class="col-lg-6" id="list_files_ke">
            <ul class="list-group">
                <?php foreach ($info_ke->fichier_ke as $key_f => $fichier): ?>
                <li class="list-group-item align-left">
                    <i class="material-icons bt_files_ke" title="Supprimer" onclick="del_old_file_ke('existed_file_ke_<?php echo $key_f ?>', this);">delete</i>&nbsp;
                    <a href="<?php echo (site_url('visual/visual/download_file_ke/'.$fichier->oid)) ?>" download="">
                        <i class="material-icons bt_files_ke" title="T&eacute;l&eacute;charger">file_download</i>
                    </a>
                    <span id="existed_file_ke_<?php echo $key_f ?>" class="name_existed_file_ke" my_oid="<?php echo $fichier->oid ?>">&nbsp;<?php echo $fichier->nom_orig ?> <small>(<?php echo byte_format($fichier->taille) ?>)</small></span>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>
    </div>
    <div class="row m-t-25">
        <div class="col-lg-12">
            <form action="<?php echo site_url('visualisation/visu_push/input_file_ke'); ?>" class="dropzone" method="post" enctype="multipart/form-data"  id="ke_file_insert">
                <div class="dz-message">
                    <div class="drag-icon-cph">
                        <i class="material-icons">attach_file</i>
                    </div>
                    <h3>D&eacute;posez ici les pi&egrave;ces jointes &agrave; ajouter.</h3>
                    <em>(Ou <strong>cliquez</strong> ici.)</em>
                </div>
                <div class="fallback">
                    <input name="file" type="file" multiple />
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-link waves-effect" onclick="send_data_ke('<?php echo $id_flux ?>');">VALIDER</button>
    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">ANNULER</button>
</div>
