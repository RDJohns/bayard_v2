<div class="row">
    <div class="col-lg-12">
        <div class="card card_filtre">
            <div class="body" style="padding: 0px 20px;">
                <input type="hidden" id="relation-client" value="<?php echo (int)$relation_client; ?>">
				   <div class= "row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_cstm">
                        <div class="row date_picker">
                            <div class="col-xs-3 col_hx m-t-10"><b>Réception du : </b></div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_act_1" />
                                        <label class="form-label">debut</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_act_2" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_cstm" style="margin:0px !important;">
						<div class="form-group form-float m-t-20">
                            <div class="form-line">
                                <input type="text" class="form-control" id="txt_recherche_id_pli" onkeyup="//load_table();" />
                                <label class="form-label">#ID flux</label>
                            </div>
                        </div>
                    </div>
						<!--div class="form-group form-float m-t-10">
                            <div class="form-line">
                                <input type="text" class="form-control" id="txt_recherche_id_pli" onkeyup="//load_table();" />
                                <label class="form-label">#ID flux</label>
                            </div>
                        </div-->
                 
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_cstm">
                        <div class="form-group form-float m-t-10">
                            <div class="form-line">
                                <input type="text" class="form-control" id="nom_fichier" onkeyup="//load_table();" />
                                <label class="form-label">#Nom fichier</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_cstm">
                        <div class="form-group form-float m-t-10">
                            <div class="form-line">
                                <input type="text" class="form-control" id="from_flux" onkeyup="//load_table();" />
                                <label class="form-label">#From flux</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_cstm">
                        <div class="form-group form-float m-t-10">
                            <div class="form-line">
                                <input type="text" class="form-control" id="from_name_flux" onkeyup="//load_table();" />
                                <label class="form-label">#From name flux</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_cstm">
                        <div class="form-group form-float m-t-10">
                            <div class="form-line">
                                <input type="text" class="form-control" id="objet_flux" onkeyup="//load_table();" />
                                <label class="form-label">#Objet flux</label>
                            </div>
                        </div>
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_cstm">
                        <div class="form-group form-float m-t-10">
                            <div class="form-line">
                                <input type="text" class="form-control" id="from_abonne"/>
                                <label class="form-label">Nom Abonné ou N°Abonné</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_cstm">
                        <div class="form-group form-float m-t-10">
                            <div class="form-line">
                                <input type="text" class="form-control" id="from_payeur"/>
                                <label class="form-label">Nom Payeur ou N°Payeur</label>
                            </div>
                        </div>
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="col-xs-3 col_hx m-t-10"><b>Société:</b></div>
                        <div class="col-xs-9 col_hx m-t-10">
						<select class="form-control show-tick" id="sel_societe" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                            <?php foreach ($societe as $soc): ?>
                                    <option value="<?php echo $soc->id?>" ><?php echo $soc->nom_societe ?></option>
                            <?php endforeach; ?>
                        </select>
						</div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                    <div class="col-xs-2 col_hx m-t-10"><b>Dossier :</b></div>
                        <div class="col-xs-4 col_hx m-t-10">
                        <select class="form-control show-tick" id="sel_dossier" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                            <?php foreach ($dossier as $src): ?>
                                <option value="<?php echo $src->nom_dossier ?>" ><?php echo $src->nom_dossier ?></option>
                            <?php endforeach; ?>
                        </select>
						</div>
                        <div class="col-xs-2 col_hx m-t-10"><b>Source :</b></div>
						 <div class="col-xs-4 col_hx m-t-10">
                        <select class="form-control show-tick" id="sel_source" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                            <?php foreach ($source as $src): ?>
                                <option value="<?php echo $src->id_source ?>" ><?php echo $src->source ?></option>
                            <?php endforeach; ?>
                        </select>
						</div>
                        
                    </div>
                </div>
                <div class = "row">
					
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx" >
                        <div class="col-xs-3 col_hx m-t-10"><b>Typologie:</b></div>
						<div class="col-xs-9 col_hx m-t-10">
                        <select class="form-control show-tick" id="sel_typologie" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                            <?php foreach ($typologies as $typo): ?>
                                    <option value="<?php echo $typo->id_typologie?>" ><?php echo $typo->typologie ?></option>
                            <?php endforeach; ?>
                        </select>
						</div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="col-xs-2 col_hx m-t-10"><b>Etape :</b></div>
                        <div class="col-xs-4 col_hx m-t-10">
                            <select class="form-control show-tick" id="sel_statut" multiple data-live-search="true" title="Tous" data-size="7" onchange="//load_table();" >
                               
                                    <?php $grp = ''; $ln = 0; ?>
                                    <?php foreach ($statut as $step): ?>
                                        <?php if($grp != $step->etape_lib){
                                            if($ln > 0){echo '</optgroup>';}
                                            $grp = $step->etape_lib;
                                            $ln++; ?>
                                            <optgroup label="<?php echo $step->etape_lib ?>">
                                        <?php } ?>
                                        <option value="<?php echo $step->id_statut ?>" data-subtext="<?php echo ' - ' . $step->etape_lib ?>"><?php echo $step->etat_lib ?></option>
                            <?php endforeach; ?>
                            </select>
						</div>
                        <div class="col-xs-2 col_hx m-t-10"><b>Statut :</b></div>
						 <div class="col-xs-4 col_hx m-t-10">
                         <select class="form-control show-tick" id="sel_etat" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                            <?php foreach ($etat as $src): ?>
                                <option value="<?php echo $src->id_etat_pli ?>" ><?php echo $src->etat ?></option>
                            <?php endforeach; ?>
                        </select>
						</div>
                        
                    </div>
                </div>
          
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:-30px 0px 20px 0px !important;">
                        <button type="button" class="btn btn-block btn-info btn-lg waves-effect m-t-10" onclick="load_data();">
                            <i class="material-icons">search</i>
                            <span>Rechercher / Rafraichir</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
        <div class="preloader" id="preloader_visu" style="display:none;">
            <div class="spinner-layer pl-black">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    
<div class="row" id="search_result">
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <table id="visu_push" class="table table-bordered table-striped table-hover" style="width: 100%;">
                    <thead>
                        <tr>
                            <th class="align-center" style="background-color: white;">#ID Flux</th>
                            <th class="align-center">Dossier</th>
                            <th class="align-center">Nom fichier</th>
                            <th class="align-center" style="min-width: 150px !important;">Objet flux</th>
                            <th class="align-center" style="min-width: 80px !important;">From flux</th>
                            <th class="align-center" style="min-width: 100px !important;">From name flux</th>
                            <th class="align-center">Société</th>
                            <th class="align-center">Source</th>
                            <th class="align-center" style="min-width: 80px !important;">Etape</th>
                            <th class="align-center" style="min-width: 80px !important;">Etat</th>
                            <th class="align-center" style="min-width: 80px !important;">Statut</th>
                            <th class="align-center">Typologie</th>
                            <th class="align-center">Dernier traitement</th>
                            <th class="align-center">Entit&eacute;</th>
                            <th class="align-center">Nombre d'abo.</th>
                            <th class="align-center">Motif blocage</th>
                            <th class="align-center">Piece Jointe</th>
                            <th class="align-center">Abonnement</th>
                            <th class="align-center">Anomalie</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modal liste abonnement -->
        <div class="modal fade" id="mdl_display_abo" tabindex="-1" role="dialog">
            <div class="modal-dialog modal_xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id='abo_head'><span>Abonnement Flux#</span></h4>
                    </div>
                        <div class="modal-body">
                            <div class="container-fluid"> 
                                <div id="abo_body">
                                </div>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                    </div>
                </div>
            </div>
        </div>

<!--- modal for anomalie --->
<div class="modal fade" id="mdl_display_ano" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><span>Anomalie Flux#</span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="ano_body" class="modal-body mod_ano">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!-- modal for ke edition -->
<div class="modal fade" id="mdl_input_ke" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>
