<?php if($type) { ?>
                <table id="visu_abo" class="table table-bordered table-striped table-hover" style="width: 100%;">
                    <thead>
                        <tr>
                            <th class="align-center">ID</th>
                            <th class="align-center">Titre</th>
                            <th class="align-center">Soci&eacute;t&eacute;</th>
                            <th class="align-center">Nom du fichier</th>
                            <th class="align-center">Typologie abonnement</th>
                            <th class="align-center">Numéro Abonné</th>
                            <th class="align-center">Nom Abonné</th>
                            <th class="align-center">Code postal Abonné</th>
                            <th class="align-center">Numéro payeur</th>
                            <th class="align-center">Nom payeur</th>
                            <th class="align-center">Code postal payeur</th>
                            <th class="align-center">Statut</th>
                            <th class="align-center">Motif KO</th>
                            <th class="align-center">Tarif</th>
                            <th class="align-center">Date de traitement</th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php foreach ($abos as $abo): ?>
                            <tr>
                                <td><?php echo $idflux ?></td>
                                <td><?php echo $abo->titre_abo ?></td>
                                <td><?php echo $societe ?></td>
                                <td><?php echo $fichier ?></td>
                                <td><?php echo $abo->typologie_abo ?></td>
                                <td><?php echo $abo->numero_abonne ?></td>
                                <td><?php echo $abo->nom_abonne ?></td>
                                <td><?php echo $abo->code_postal_abonne ?></td>
                                <td><?php echo $abo->numero_payeur ?></td>
                                <td><?php echo $abo->nom_payeur ?></td>             
                                <td><?php echo $abo->code_postal_payeur ?></td>
                                <td><?php echo $abo->stat_abo ?></td>
                                <td><?php echo $abo->motif_ko ?></td>
                                <td><?php echo $abo->tarif ?></td>
                                <td><?php echo $date_traitement ?></td>
                            </tr>
                            <?php endforeach; ?>
                        
                    </tbody>
                </table>
<?php } else { ?>

    <table id="visu_jointes" class="table table-bordered table-striped table-hover" style="width: 100%;">
    <thead>
        <tr>
            <th class="align-center">Id doc</th>
            <th class="align-center">Nom fichier</th>
            <th class="align-center">Date injection</th>
            
        </tr>
    </thead>
    <tbody>
            <?php foreach ($jointes as $joint): ?>
            <tr>
                <td><?php echo $joint->id_doc_joint ?></td>
                <td><?php echo $joint->nom_fichier_origine ?></td>
                <td><?php echo $joint->date_injection ?></td>
            </tr>
            <?php endforeach; ?>
        
    </tbody>
</table>
<?php } ?>