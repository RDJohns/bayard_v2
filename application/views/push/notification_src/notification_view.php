<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <style>

        body{font-family:Verdana,'Open Sans',sans-serif;font-size: 12px;}
        th,td{border-collapse:collapse; border: 1px solid #555e5e; padding: 6px;}
        blockquote{font-style: normal;margin-left: 32px;border-left: 4px solid #CCC;padding-left: 8px;}
        .soc-title{color : #666666;}
    </style>
</head>
<body>

Bonjour, <br><br>

Merci de trouver ci-joint un mail <?php echo strtolower($nom_societe); ?> à traiter.

<br>
<?php
    echo $notification_source_view;
?>

<br><br>

L'&eacute;quipe VIVETIC <br><br>

<img src="cid:logo_vivetic" alt="vivetic" border="0" />

</body>
</html>