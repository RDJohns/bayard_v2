<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <style>

        body{font-family:Verdana,'Open Sans',sans-serif;font-size: 12px;}
        th,td{border-collapse:collapse; border: 1px solid #555e5e; padding: 6px;}
        blockquote{font-style: normal;margin-left: 32px;border-left: 4px solid #CCC;padding-left: 8px;}
        .soc-title{color : #666666;}
    </style>
</head>
<body>

Bonjour, <br><br>

Nous vous retournons cette demande car nous ne pouvons la traiter.<br>
En effet,  nous vous rappelons que, conformément à la fiche contact, pour vos SAV, il faut s’adresser aux destinataires suivants :<br>
Bayard jeunesse : contact.mp@milan.fr<br>
Bayard adulte : collecteurs@bayard-presse.com<br>
Milan : contact.mp@milan.fr

<br><br>

L'&eacute;quipe <?= strtoupper($societe) ?> <br><br>

</body>
</html>