<?php 
   
        $strTypologie          = "";
        $strTypologie         .= '<select class="form-group form-control" id="typologie" style="max-height: 65px;" data-live-search="true" data-show-subtext="true">';
        $strTypologie         .= '<option value="0" selected disabled="disabled">--Choisir une typologie--</option>';
		
		
        foreach ($typologie as $itemTypologie)
        {
            $strTypologie      .= '<option value="'.(int)$itemTypologie->id_typologie.'" >'.$itemTypologie->typologie.'</option>';
        }
		
		
			
		
		if($piece)
		{
			$iPiece = 0;
			$strPiece ="";
			$btnAjouter = "";
			
			foreach($piece as $itemPiece)		
			{
				$btnAjouter = "";
				if($iPiece == 0)
				{
					$btnAjouter = '<button type="button" class="btn bg-blue waves-effect c-nom_0" style="float:left;" onclick="nomPieceJointe(\'nom_0\')"><b>Ajouter</b></button>&nbsp;&nbsp;';
				}
				
				$strPiece .='
						<div class="col-md-3" style="margin-bottom: 5px !important;">
							<b>Nom pièce jointe :</b>
							<div class="input-group">
								<div class="form-line">
								<textarea readonly rows="2" class="form-control no-resize" style="cursor: not-allowed;background: #00000066 !important;" placeholder="Saisir ici le nom de fichier ... ">'.$itemPiece->nom_fichier_origine.'</textarea>
									
								</div>
								'.$btnAjouter.'
								</div>
						</div>';
				$iPiece++;		
			}
			
		}
		else
		{
			$strPiece ='
						<div class="col-md-4 c-nom-piece-jointe nom_0" style="margin-bottom: 5px !important;">
							<b>Nom pièce jointe :</b>
							<div class="input-group">
								<div class="form-line">
								<textarea rows="2" class="form-control no-resize text-nom-piece" placeholder="Saisir ici le nom de fichier ... "></textarea>
									
								</div>
								<button type="button" class="btn bg-blue waves-effect c-nom_0" style="float:left;" onclick="nomPieceJointe(\'nom_0\')"><b>Ajouter</b></button>&nbsp;&nbsp;
								</div>
						</div>';
		}

       
$strTypologie           .= '</select>';
?>
<div class="col-md-3" style="margin-bottom: 5px !important;">
    <b>Societe :</b>
    <div class="c-typage-societe">
        <select class="form-control show-tick" tabindex="-98" id="typage-societe" onchange="getTypologie();">
            <option value="0" <?php if($societe == 0) echo ' selected ';  ?> >*</option>
            <option value="1" <?php if($societe == 1) echo ' selected ';  ?> >Bayard</option>
            <option value="2" <?php if($societe == 2) echo ' selected ';  ?> >Milan</option>
        </select>
    </div>
    <span id="error-societe" class="push-error" style="display:none; color: red;position: fixed;margin-top: 0px;">Choisir une société</span>
    

</div>
<div class="col-md-3" style="margin-bottom: 5px !important;">
    <b>Typologie : </b>
    <div class="input-group c-push-typologie">
        <?php echo $strTypologie; ?>
    </div>
    <span id="error-typologie" class="push-error" style="display:none; color: red;position: fixed;margin-top: -17px;">Choisir une typologie</span>
</div>


<div class="col-md-3" style="margin-bottom: 5px !important;">
    <b>Nombre abonnements :</b>
    <div class="input-group">
        
        <div class="form-line">
            <input type="number" class="form-control" min= "1" id="abonnement" value="1" step="any" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
        </div>
    </div>
</div>

<div class="col-md-3" style="margin-bottom: 5px !important;">
    <b>Entité : </b>
    <div class="input-group">
        <div class="form-line">
            <textarea rows="2" class="form-control no-resize" id="entite" placeholder="Saisir ici l'entité ... "></textarea>
        </div>
    </div>
</div>


<?php 
	 
	if($traitement < 2)
	{
			echo $strPiece;
	}
	
?>
<div class="liste-div-nom-piece">

</div>


                            
