<div id="push-main" style="background:white; padding-top: 70px;  height: 100%;">
    <div class="centered">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-md-2"><span class="libelle">Type de traitement :</span> </div> 
                            <div class="col-md-2 ">
                                <select class="form-control show-tick" tabindex="-98" id="filtre-societe">
                                    <option value="0" disabled >-- Choisir --</option>
                                    <option value="1">Mail</option>
                                    <option value="2">SFTP</option>
                                </select>
                            </div>
                            <div class="col-md-1 filtre"><span class="libelle"> Société : </span></div> 
                            <div class="col-md-2 filtre">
                                <select class="form-control show-tick" tabindex="-98" id="filtre-societe">
                                    <option value="0" disabled >-- Choisir --</option>
                                    <option value="1">Bayard</option>
                                    <option value="2">Milan</option>
                                </select>
                            </div>  
                            <div class="col-md-2 filtre">
                                <button type="button" class="btn btn-success waves-effect" onclick="chargerPli();" >
                                    <i class="material-icons">refresh</i>
                                    <span>Charger PLI</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>