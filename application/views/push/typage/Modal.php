<div class="modal fade" id="id-modal-motif-rejet" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
		<div class="modal-header bg-red titre-anomalie" style="padding: 10px; !important;font-weight: bold;">REJETER LE FLUX</div>
            <div class="modal-body modal-motif-rejet">
				  <div id="flux-content-rejet">
					
				  </div>
				  <br/>
                <b>Motif de rejet :<span style="color: red;">&nbsp;(*)</span></b>
				<div class="input-group">
					<div class="form-line">
						<textarea rows="8" class="form-control no-resize text-rejet-piece" placeholder="Saisir ici le motif de rejet ... "></textarea>
					</div>
					
				</div>
            </div>
            <div class="spinner-modal" style="display:none;background: #fff;position: fixed;top: 50%;left: calc(50% - 100px);width: 255px;height: 50px;border-radius: 6px;padding: 10px;border: 1px solid grey;z-index: 999;">
               Enregistrement et envoi mail ... <div class="preloader pl-size-xs"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>
            </div>
            <div class="modal-footer">
                <span style="color: red;">&nbsp;(*)</span> : Obligatoire
				<button type="button" class="btn bg-blue-grey waves-effect" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="rejeterFlux();">Rejeter</button>

            </div>
        </div>
    </div>
</div>


<!-- modal motif anaomalie -->
<div class="modal fade" id="id-modal-motif-anomalie" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
		<div class="modal-header bg-blue titre-anomalie" style="padding: 10px; !important;font-weight: bold;"></div>
            <div class="modal-body modal-motif-anomalie">
				  
				  <div id="flux-content-anomalie">
					
				  </div>
				  <br/>
				 <b>Motif anomalie : <span style="color: red;">&nbsp;(*)</span></b>
				<div class="input-group">
					<div class="form-line">
						<textarea rows="8" class="form-control no-resize text-anomalie" placeholder="Saisir ici le motif ... "></textarea>
					</div>
					
				</div>
                <div class="spinner-modal" style="display:none;background: #fff;position: fixed;top: 50%;left: calc(50% - 100px);width: 255px;height: 50px;border-radius: 6px;padding: 10px;border: 1px solid grey;z-index: 999;">
                    Enregistrement et envoi mail ... <div class="preloader pl-size-xs"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>
                </div>
            </div>
            <div class="modal-footer">
                <span style="color: red;">&nbsp;(*)</span> : Obligatoire
				<button type="button" class="btn bg-blue-grey waves-effect btn-annuler" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-danger waves-effect btn-annuler" onclick="envoiAnomalieFlux();">Envoyer</button>

            </div>
        </div>
    </div>
</div>

