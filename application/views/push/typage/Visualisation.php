<div id="push-main" style="background:white; padding-top: 70px;  height: 100%;">
    <div class="centered">
        <div class="row clearfix"> 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body" style="padding-bottom: 0px;padding-top: 11px;">
                        <div class="row">
                            <div class="col-md-2"><span class="libelle">Type de traitement :</span> </div> 
                            <div class="col-md-2 ">
                                <select class="form-control show-tick" tabindex="-98" id="filtre-traitement">
                                    <option value="0" disabled >-- Choisir --</option>
                                    <option value="1">Mail</option>
                                    <option value="2">SFTP</option>
                                </select>
                            </div>
                            <div class="col-md-1 filtre"><span class="libelle"> Société : </span></div> 
                            <div class="col-md-2 filtre">
                                <select class="form-control show-tick" tabindex="-98" id="filtre-societe">
                                    <option value="0" disabled >-- Choisir --</option>
                                    <option value="1">Bayard</option>
                                    <option value="2">Milan</option>
                                </select>
                            </div>  
							
                            <div class="col-md-2 filtre btn-typage-valide-push">	</button>
                                <button type="button" class="btn btn-success waves-effect" onclick="chargerPli();" >
                                    <i class="material-icons">refresh</i>
                                    <span>Charger Flux</span> 
                                
                            </div>
							<div class="col-md-2 filtre">
								<div id="loader-flux"></div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--end filtre-->

        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 etat-traitement"  style='display:none;'>
                <div class="card" style="/*min-height: calc(90vh - 175px);*/">
                    <div class="body" style="padding-bottom: 0px;padding-top: 11px;">
                    <div class="info-box-4" style="margin-bottom: 5px;box-shadow: none;border: 1px solid #dddddd">
                        <div class="content a-type">
                            <div class="content-mail" style="font-size: 12px;">
                                
                            </div>
                           
                        </div>
                    </div>
                    <div class="list-group etat-push">
                                
                               
                    </div>
                     <br/>
                    </div>
                </div>
            </div> <!--end etat tratitement-->

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 content-traitement" style='display:none;'>
            <div class="card" style="margin-bottom: 10px;">
                <div class="body">
                    <div class="content-body-sftp"></div>
                    <div class="content-body-mail"></div>
                        <hr/ style="margin-top: 0px;margin-bottom: 9px;">
                        <div class="alert bg-teal consigne-traitement-flux" style="display:none !important;">
                            <strong>Consigne pour le traitement : </strong>
                            <span class="consigne-traitement-flux" id="id-consigne-traitement-flux">

                            </span>
                        </div>
                      <!--
                        <button type="button" class="btn bg-black waves-effect waves-ligh btn-typage mail-sftp" onclick="modalAnomalieFlux('hp');">Hors périmètre</button>
                        <button type="button" class="btn btn-danger waves-effect btn-typage mail-sftp" onclick="modalRejetFlux();">Rejeter</button>
                        <button type="button" class="btn btn-warning waves-effect btn-typage mail-anomalie" onclick="modalAnomalieFlux('mail');">Anomalie mail</button>
                        <button type="button" class="btn bg-brown waves-effect btn-typage mail-anomalie" onclick="modalAnomalieFlux('pj');">Anomalie PJ</button>
						<button type="button" class="btn btn-warning waves-effect  btn-typage sftp-anomalie" onclick="modalAnomalieFlux('fichier');">Anomalie fichier</button>
                        -->
                      <!--  <button type="button" class="btn btn-info waves-effect  btn-typage sftp-anomalie" onclick="decouperFichier('fichier');">Découper le fichier</button>-->
                        <!-- <button type="button" class="btn btn-info waves-effect btn-typage">Annuler</button> -->
                    </div>
            </div>
            <div class="card" style="/*min-height: calc(90vh - 175px);*/">
                <div class="body" style="padding-bottom: 0px;padding-top: 11px;">
                    
                    <div class="content-champ">
                        <div class="row clearfix ajax-champ">
                            
                        
                        
                        </div>
                    </div>
                </div>
                
                </div> <!--end etat visualisation -->

                <div class="card btn-typage-valide-push" style="margin-top: -24px !important;">
                    <div class="body" style="margin-bottom: 5px;">
                    
                    <button type="button" class="btn btn-success waves-effect" onclick="validerPushTypage()"><i class="material-icons">verified_user</i><span>Valider</span></button>
					<button type="button" class="btn btn-danger waves-effect" onclick="annulerTypage();"><i class="material-icons">cancel</i><span>Annuler</span></button>
                      <!--liste KO-->
                    <div class="btn-group dropup ">
                        <button type="button" class="btn bg-brown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <i class="material-icons">error</i>&nbsp;&nbsp; Mettre KO le flux &nbsp;<span class="caret"></span>
                        </button>
                        
                        <ul class="dropdown-menu" style = "min-width: 212px !important;">
                            <li><a href="javascript:fluxKO(11,'KO : Mail anomalie');" class=" waves-effect waves-block mail-anomalie"><i class="material-icons">email</i>KO : Mail anomalie</a></li>
                            <li><a href="javascript:fluxKO(12,'KO : Anomalie PJ');" class=" waves-effect waves-block mail-anomalie"><i class="material-icons">blur_off</i>KO : Anomalie PJ</a></li>
                            <li><a href="javascript:fluxKO(14,'KO : Anomalie fichier');" class=" waves-effect waves-block sftp-anomalie"><i class="material-icons">report_problem</i>KO : Anomalie fichier</a></li>
                            <li><a href="javascript:fluxKO(15,'Rejet');" class=" waves-effect waves-block"><i class="material-icons">pan_tool</i>Rejeté</a></li>
                            <li><a href="javascript:fluxKO(13,'Hors périmètre');" class=" waves-effect waves-block"><i class="material-icons">location_disabled</i>Hors périmètre</a></li>

                            <li><a href="javascript:fluxKO(22,'KO inconnu');" class=" waves-effect waves-block"><i class="material-icons">new_releases</i>KO inconnu</a></li>
                            <li><a href="javascript:fluxKO(23,'KO en attente');" class=" waves-effect waves-block"><i class="material-icons">pause</i>KO en attente</a></li>
                            <li><a href="javascript:fluxKO(21,'KO définitif');" class=" waves-effect waves-block"><i class="material-icons">block</i>KO définitif</a></li>
                        </ul>
                    </div>
                    <!-- fin liste KO -->
                </div>
              
                </div>
				<input type="hidden" id="id-flux">

        </div> 

        


    </div>    
</div>
<div class="class-loader" style="display:none; z-index: 999;background: #ffffff;position: fixed;width: 250px;height: 51px;top: 1%;border-radius: 6px;left: 36%;border: 1px solid #77777794;padding: 11px;">
	Enregistrement et envoi mail... <div class="preloader pl-size-xs"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>
</div>