<?php defined('BASEPATH') OR exit('No direct script access allowed');
	$url_assets = base_url('assets/');
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>GED-BAYARD</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo $url_assets ?>images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="<?php echo $url_assets ?>font/font.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $url_assets ?>font/icon.css" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo $url_assets ?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo $url_assets ?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo $url_assets ?>plugins/animate-css/animate.css" rel="stylesheet" />
    
    <!-- JQuery DataTable Css -->
    <link href="<?php echo $url_assets ?>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- jspanel Css -->
    <link href="<?php echo $url_assets ?>plugins/jspanel/jspanel.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo $url_assets ?>css/style.css" rel="stylesheet">

    <style>
        .p-b-0 {
            padding-bottom: 0px !important;
        }
    </style>

</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">GED-<b>BAYARD<sub class="font-11"><span class="badge bg-orange">V2</span></sub></b></a>
            <small>Page de connexion</small>
        </div>
        <div class="card">
            <div class="body p-b-0">
                <form  id="login-frm" onsubmit="return false;">
                    <div class="msg">Identifiant et mot de passe</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" id="username" class="form-control elem_login" name="username" placeholder="Identifiant" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" id="password" class="form-control elem_login" name="password" placeholder="Mot de passe" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-block bg-pink waves-effect elem_login" onclick="connexion();">CONNEXION</button>
                        </div>
                    </div>
                </form>
                <div class="container-fluid">
                    <div class="row" id="div_notif_num_vs_ged"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/node-waves/waves.js"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/ui/notifications.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-validation/jquery.validate.js"></script>
    
    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.colVis.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    
    <!-- pjpanel -->
    <script src="<?php echo $url_assets; ?>plugins/jspanel/jspanel.js"></script>
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/contextmenu/jspanel.contextmenu.js"></script>
    <!-- <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/datepicker/jspanel.datepicker.js"></script> -->
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/hint/jspanel.hint.js"></script>
    <!-- <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/layout/jspanel.layout.js"></script> -->
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/modal/jspanel.modal.js"></script>
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/tooltip/jspanel.tooltip.js"></script>
    <script src="<?php echo $url_assets; ?>plugins/jspanel/extensions/dock/jspanel.dock.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo $url_assets ?>js/admin.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/examples/sign-in.js"></script>

    <script>
        var in_connexion = false;

        function connexion() {
            if(!in_connexion) {
                in_connexion = true;
                var login = $.trim($('#username').val());
    			var pswd = $.trim($('#password').val());
                if( login != '' &&  pswd != ''){
    				$.post('<?php echo site_url('login/connexion'); ?>', {login:btoa(login), pswd:btoa(pswd)}, function(from_server){
                        try {
                            var reponse = $.parseJSON(from_server);
                            if(reponse.code == 0){
                                window.location.href = reponse.data;
                            }else{
                                showNotification('alert-warning', '<i class="material-icons">warning</i> '+reponse.data, 'top', 'center', null, null);
                                in_connexion = false;
                            }
                        } catch (error) {
                            showNotification('alert-danger', '<i class="material-icons">error</i> Erreur serveur. (Merci de contacter les DEVSI)', 'top', 'center', null, null);
                            in_connexion = false;
                        }
    				});
    			}else{
                    showNotification('alert-warning', '<i class="material-icons">warning</i> Identifiant et mot de passe sont requis.', 'top', 'center', null, null);
                    in_connexion = false;
                }
            }
        }

        $(function () {
            in_connexion = false;
            $('.elem_login').keypress(function(e){
                if(e.keyCode == 13){
                    connexion();
                }
            });
            $('#login-frm').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                }
                ,unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                }
                ,errorPlacement: function (error, element) {
                    $(element).parents('.input-group').append(error);
                }
                ,rules:{
                    password:{
                        required: true
                    }
                    ,username:{
                        required: true
                    }
                }
            });
            load_notif_num_vs_ged();
        });

        function load_notif_num_vs_ged() {
            $('#div_notif_num_vs_ged').html('');
            $.post('<?php echo site_url('visual/notification'); ?>', {}, function(from_server){
                $('#div_notif_num_vs_ged').html(from_server);
            });
        }
        
        function load_detail_num_vs_ged() {
            $.post('<?php echo site_url('visual/notification/detail_num_vs_ged'); ?>', {}, function(reponse) {
                try {
                    var data = $.parseJSON(reponse);
                } catch (error) {
                    showNotification('alert-danger', '<i class="material-icons">error</i>Alerte: Erreur serveur.', 'top', 'center', null, null);
                    return '';
                }
                if (data.code == '1') {
                    jsPanel.create({
                        headerTitle: data.titre
                        ,headerToolbar: data.sous_titre
                        ,footerToolbar: data.footer
                        ,content: data.body
                        ,contentSize: {
                            width: '80%'
                            ,height: '80vh'
                        }
                        ,theme: 'light'
                        ,onbeforemaximize: function(panel) {
                            $('html,body').scrollTop(0);
                            return true;
                        }
                    });
                    $('.table_detail_nb_num_vs_ged').DataTable({
                        'pageLength': 10
                        ,'bFilter': false
                        ,dom: 'tip'
                        ,"ordering": false
                        ,'oLanguage': {
                            'sInfo': "de _START_ \340 _END_ sur _TOTAL_ lignes"
                        }
                        ,'fnDrawCallback': function(oSettings) {
                            $('[data-toggle="tooltip"]').tooltip();
                            //$('[data-toggle="popover"]').popover();
                            $(this).removeClass('table_detail_nb_num_vs_ged');
                        },
                    });
                }
            }).fail(function(){
                showNotification('alert-danger', '<i class="material-icons">error</i>Erreur serveur!.', 'top', 'right', null, null);
            });
        }

    </script>
</body>

</html>
