<div class="preloader-reception">
        <div class="preloader pl-size-xs"><div class="spinner-layer pl-indigo"><div class="circle-clipper left"><div class="circle"></div></div>
        <div class="circle-clipper right"><div class="circle"></div></div></div></div>
        <span>Chargement de données...</span>
</div>

<div class="modal fade" id="detail-click" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-pink">
                <h6 class="modal-title" id="detail-click-label"></h6>
            </div>
            <div class="modal-body">
                
<!-- Nav tabs -->
<ul class="nav nav-tabs tab-nav-right tab-col-pink" role="tablist">
	<li role="presentation" class="active"><a href="#info-gle" data-toggle="tab"><i class="material-icons">home</i>&nbsp;&nbsp;Information générale</a></li>
	<li role="presentation"><a href="#info-mvt" data-toggle="tab" onclick="afficherInformation('mvt');"><i class="material-icons">timeline</i>&nbsp;&nbsp;&nbsp;Mouvemement</a></li>
	<li role="presentation"><a href="#info-paie" data-toggle="tab" onclick="afficherInformation('paie');"><i class="material-icons">monetization_on</i>&nbsp;&nbsp;Paiement</a></li>
    <li role="presentation"><a href="#info-doc" data-toggle="tab" onclick="afficherInformation('doc');"><i class="material-icons">insert_drive_file</i>&nbsp;&nbsp;Tous les documents</a></li>
    <li role="presentation"><a href="#info-histo" data-toggle="tab" onclick="afficherInformation('histo');"><i class="material-icons">av_timer</i>&nbsp;&nbsp;Historique</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active ra-modal-content" id="info-gle">
		<b>Chargement ...</b>
		
        
	</div>
	<div role="tabpanel" class="tab-pane ra-modal-content" id="info-mvt">
        <b>Chargement ...</b>
	</div>
	<div role="tabpanel" class="tab-pane ra-modal-content" id="info-paie">
        <b>Chargement ...</b>
	</div>
	<div role="tabpanel" class="tab-pane ra-modal-content" id="info-doc">
        <b>Chargement ...</b>
    </div>
    <div role="tabpanel" class="tab-pane ra-modal-content" id="info-histo">
        <b>Chargement ...</b>
	</div>
</div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-blue-grey waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal detail flux-->
<div class="modal fade" id="detail-click-flux" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-pink">
                <h6 class="modal-title" id="detail-click-label-flux">c</h6>
            </div>
            <div class="modal-body">
                
<!-- Nav tabs -->
<ul class="nav nav-tabs tab-nav-right tab-col-pink" role="tablist">
	<li role="presentation" class="active"><a href="#info-gle-flux" data-toggle="tab"><i class="material-icons">home</i>&nbsp;&nbsp;Information générale</a></li>
	<li role="presentation"><a href="#info-abo-flux" data-toggle="tab"><i class="material-icons">assignment</i>&nbsp;&nbsp;&nbsp;Abonnement</a></li>
	<li role="presentation"><a href="#info-histo-flux" data-toggle="tab" ><i class="material-icons">av_timer</i>&nbsp;&nbsp;Historique</a></li>
</ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active ra-modal-content" id="info-gle-flux">
            <b>Chargement ...</b>
        </div>

        <div role="tabpanel" class="tab-pane ra-modal-content" id="info-abo-flux">
            <b>Chargement ...</b>
        </div>

        <div role="tabpanel" class="tab-pane ra-modal-content" id="info-histo-flux">
            <b>Chargement ...</b>
        </div>
    </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn bg-blue-grey waves-effect" data-dismiss="modal">FERMER</button>
        </div>
    </div>
    </div>
</div>
<!-- FIN Modal detail flux-->