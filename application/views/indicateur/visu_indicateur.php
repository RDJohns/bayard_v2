<script>
    var url_site = '<?php echo site_url(); ?>';
</script>
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="logo-pro">
                    <a href="index.html"><img class="main-logo" src="" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="header-top-area">
         </div> 
    </div>
    <br />
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="">
				<div class="sparkline13-hd" style="padding:5px 0px 0px 20px" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="main-sparkline13-hd">
						<div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin:0px 0px 20px 0px">
								<!--h1 style="color:#69B145"><i class="fa fa-envelope-square" style="color:#C62057"></i>
								Suivi des réceptions</h1-->  
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					<span class="pull-left" style="font-size:13px;margin-top:8px;margin-left:8px;">Réception du</span></div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left">
						<div class="form-group-cstm " nowrap="nowrap">
							<div class="input-daterange input-group" id="bs_datepicker_range_container">
								<div class="form-line">
									<input type="text" id="date_debut" name="date_debut" class="form-control" placeholder="Début de réception ..." value="<?php echo date("d/m/Y");?>">
								</div>
									<span class="input-group-addon">au</span>
								<div class="form-line">
									<input type="text" id="date_fin" name="date_fin" class="form-control" placeholder="Fin de réception..." value="<?php echo date("d/m/Y");?>">
								</div>
							 </div>								
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
						<div class="form-group-cstm">
							<div class="row clearfix c-select_soc">
							<select class="btn-group form-control show-tick" id="select_soc" name="select_soc">
								<option value="">-- Société --</option>
									
							</select>
							</div>	
						</div>
					</div>					
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
						<div class="form-group-cstm">
							<div class="row clearfix">
							<select class="btn-group form-control show-tick" id="select_granu" name="select_granu">
								<option value="" selected>-- Granularité --</option>
								<option value="j">Jour</option>
								<option value="s">Semaine</option>
								<option value="m">Mois</option>
									
							</select>
							</div>	
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						 <div class="form-group-cstm">
							
								<button class="btn btn-primary waves-effect waves-light" id="visu_indicateur" onclick="charger_indicateur_prod();">
								<i class="fa fa-search"></i>&nbsp;Rechercher</button>
								<!-- onclick="charger_indicateur_prod();charger_indicateur_global_global()" -->
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom:30px;">	
				</div>
				
								
				<div id="result-rsrch" style="display: block;">					
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="">
								<div class="sparkline13-graph">
										<div class="datatable-dashv1-list custom-datatable-overright">
											<div id="toolbar">
												<!--select class="form-control dt-tb">
													<option value="">Export Basic</option>
													<option value="all">Export All</option>
													<option value="selected">Export Selected</option>
												</select-->
											</div>
										
										</div>
								</div>
							</div>
						</div>					
				</div>
			</div>
		<!--/div-->
	</br>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
		<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
		<em id="error_message">*Veuillez saisir les criteres de recherche</em>
		</div>
	</div>

	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab5" style="margin-top:50px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
         <div class="analytics-content" id="detail_stat">
        
		 <div class="body">
         <div class="table-responsive">		
             <!-- <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="indicateur_pli"> -->
			
			</div>
         </div>
		</div>
		</div>
    </div>
	</div>
	
	<div class="row">
    <div class="col-lg-12">
        <div class="card" style="box-shadow: 0 2px 10px rgba(0, 0, 0, 0) !important;">
            <div class="body" style="margin-top:-50px;">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active" id="div1">
                                <a href="#typage" data-toggle="tab">
                                    <i class="material-icons">view_list</i> <span id="div_typage">GLOBAL</span>
                                    <button type="button" class="btn-default btn-circle waves-effect waves-circle" onclick="export_indicateur_prod();" title="Export-Excel" id="a">
                                        <i class="material-icons" id="down_global">file_download</i>
                                    </button>
									<div class="preloader pl-size-xs" id="preloader_ind_global" style="display:none;">
										<div class="spinner-layer pl-black">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>
									<div class="preloader pl-size-xs" id="preloader_global_export" style="display:none;">
										<div class="spinner-layer pl-cyan">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>									
                                </a>
                            </li>
                            <li role="presentation"  id="div2">
                                <a href="#saisie" data-toggle="tab">
                                    <i class="material-icons">view_headline</i> DETAIL PAR PLI
                                    <button type="button" class="btn-default btn-circle waves-effect waves-circle" onclick="export_indicateur_detail();" title="Export-Excel">
                                        <i class="material-icons" style="color:#999" id="down_detail">file_download</i>
                                    </button>
									<div class="preloader pl-size-xs" id="preloader_ind_detail" style="display:none;" >
										<div class="spinner-layer pl-black">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>
									<div class="preloader pl-size-xs" id="preloader_detail_export" style="display:none;">
										<div class="spinner-layer pl-cyan">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>	
                                </a>
                            </li>
                           </ul>
                
                       <!-- <div class="tab-content"></div>-->
						
						<div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="typage">
							
								<div class="row" id="indicateur_global">								
										<!--** repartition global typage ***-->
								</div>
                                							
								<div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="indicateur_pli">
											<thead  class="custom_font">
												<tr>
												<th style="width:30px">Réception du</th>												
												<th style="width:10px">Reçu</th>												
												<th style="width:10px">Typage fini</th>
												<th style="width:10px">Saisie finie</th>
												<th style="width:10px">Contrôle termin&eacute;</th>
												<th style="width:10px">Termin&eacute;</th>
												<th style="width:10px">D&eacute;tail</th>	
												</tr>
											</thead> 
											 <tbody class="custom_font">
											 </tbody>
															
										</table>
										</br>
										<div class="preloader pl-size-xs" id="preloader_ind_op" style="display:none;">
											<div class="spinner-layer pl-black">
												<div class="circle-clipper left">
													<div class="circle"></div>
												</div>
												<!--div class="circle-clipper right">
													<div class="circle"></div>
												</div-->
											</div>
										</div>
											
										<span id="tab_export" style="padding-left:10px;">
										<i class="material-icons">people</i> <span id="div_op">DETAIL PAR OP</span>
										<button type="button" class="btn-default btn-circle waves-effect waves-circle" onclick="export_indicateur_op();" title="Export-Excel" id="a">
											<i class="material-icons" id="down_global">file_download</i>
										</button>
										</span>
										<div class="preloader pl-size-xs" id="preloader_op_export" style="display:none;">
											<div class="spinner-layer pl-black">
												<div class="circle-clipper left">
													<div class="circle"></div>
												</div>
												<div class="circle-clipper right">
													<div class="circle"></div>
												</div>
											</div>
										</div>	
										 <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="indicateur_pli_op">
											<thead  class="custom_font">
												<tr>
												<th style="width:20px">Réception du</th>												
												<th style="width:10px">Reçu</th>
												<th style="width:20px">Opérateur</th>	
												<th style="width:10px">Typage fini</th>
												<th style="width:10px">Saisie finie</th>
												<th style="width:10px">Contrôle terminé</th>
												<th style="width:10px">Termin&eacute;</th>
												<!--th style="width:10px">D&eacute;tail</th-->	
												</tr>
											</thead> 
											 <tbody class="custom_font">
											 </tbody>
															
										</table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="saisie">
							
								<div class="row" id="indicateur_global_saisie">								
										<!--** repartition global saisie***-->
								</div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="indicateur_detail_pli">
                                            <thead>
                                                <tr>
                                                    <th>#ID.GED</th>
													<th>Typologie</th>				
													<th>Lot scan</th>													
													<th>Trait&eacute; typage</th>			
													<th>Typ&eacute; par</th>			
													<th>Trait&eacute; saisie</th>			
													<th>Saisie par</th>			
													<th>Trait&eacute; Contrôle</th>
													<th>Control&eacute; par</th>
													<th>Retrait&eacute; par</th>
													<th>Termin&eacute;</th>			
													<th>Courrier du</th>			
													
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                           </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	<input type="hidden" id="daty_courrier">
	<input type="hidden" id="date_detail_export">
	
    <div class="library-book-area mg-t-30">
        <div class="container-fluid">
        </div>
    </div>
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
    </div>
    <div class="courses-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
        <div class="footer-copyright-area" style="position: fixed !important; bottom:0 !important; width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright © 2018. All rights reserved DEV-SI VIVETIC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
