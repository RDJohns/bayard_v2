<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="index.html"><img class="main-logo" src="<?php //echo img_url('logo/logo.png'); ?>" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row" style="background-color:#3F51B5;">
                                    
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<a class="navbar-brand" href="">
											<img src="<?php echo base_url().'/assets/images/logo.png'?>" alt="logo" style="display: block;width: 36px !important;">
										</a>                                       
										 <ol class="breadcrumb" style="background-color:#3F51B5;font-size: 18px;margin: 6px 0px 0px 20px;">
											<li><a style="color: #fff;text-decoration: none;font-size: 15px;">
											GED - BAYARD</a></li>
											<li style="color: #fff;text-decoration: none;font-size: 15px;"><i class="fa fa-hourglass-1" style="color:#fff"></i> Indicateurs de production&nbsp;											
											</li>
										</ol>
									</div>
									<?php
										$CI =&get_instance();
										$actif =isset($menu_acif)?$menu_acif:null;
										$CI->menu->menu();
									?>
									<!--div-- class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
										 <ul class="nav navbar-nav navbar-right">
											<li class="dropdown">
												<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="padding-top: 0px; padding-bottom: 15px;">
													<i class="material-icons">view_headline</i>

												</a>
												<ul class="dropdown-menu">
													<li class="header" style="text-align: center !important;margin-left: 12px;">
														<?php  echo (trim($this->session->userdata('login')== ""? "non défini ":$this->session->userdata('login'))); ?>
													</li>
													<li class="body">
															<ul class="menu">                          
															
															<li>
																<a href="<?php  echo site_url("visual/visual"); ?>" >
																	<div class="icon-circle bg-cyan">
																		<i class="fa fa-eye"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Visualisation des courries</h4>
																		<p>
																			Visu détaillée des courriers
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php  echo site_url("visualisation/visu_push"); ?>" >
																	<div class="icon-circle bg-deep-purple">
																		<i class="fa fa-sign-in"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Visualisation des MAIL/SFTP</h4>
																		<p>
																			Visu détaillée des MAIL/SFTP
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php  echo site_url("reception/pli_flux"); ?>" >
																	<div class="icon-circle bg-light-green">
																		<i class="fa fa-envelope"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Réception</h4>
																		<p>
																			Suivi de la réception des plis/flux
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php  echo site_url("reception/pli"); ?>" >
																	<div class="icon-circle bg-teal">
																		<i class="fa fa-signal"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Suivi mensuel</h4>
																		<p>
																			Réception mensuelle
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php  echo site_url("statistics/statistics/suivi_solde"); ?>" >
																	<div class="icon-circle bg-pink-cstm">
																		<i class="fa fa-battery-half"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Suivi des soldes</h4>
																		<p>
																			Soldes par date de courrier
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php echo site_url("statistics/statistics_flux/stat_traitement"); ?>">
																	<div class="icon-circle bg-light-blue">
																		<i class="fa fa-tasks"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Traitement</h4>
																		<p>
																			Suivi des traitements
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php echo site_url("anomalie/anomalie/visu_anomalie"); ?>">
																	<div class="icon-circle bg-cyan-t">
																		<i class="fa fa-file-text"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Extraction</h4>
																		<p>
																			Extraction des plis
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php echo site_url("suivi/especes"); ?>">
																	<div class="icon-circle bg-amber">
																		<i class="fa fa-money"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Suivi des espèces</h4>
																		<p>
																			Espèces
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php echo site_url("provenance/pli"); ?>">
																	<div class="icon-circle bg-green">
																		<i class="fa fa-flag"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Suivi des provenances</h4>
																		<p>
																			Provenance des enveloppes
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php echo site_url("suivi/reliquat"); ?>">
																	<div class="icon-circle bg-lime">
																		<i class="fa fa-list"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Suivi des ch&egrave;ques non REB</h4>
																		<p>
																			Ch&egrave;ques non remis en banque
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php echo site_url("suivi/nosaisie"); ?>">
																	<div class="icon-circle bg-blue-grey">
																		<i class="fa fa-clipboard"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Suivi des ch&egrave;ques non saisis</h4>
																		<p>
																			Ch&egrave;ques non saisis à J+3
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php echo site_url("suivi/nosaisie"); ?>">
																	<div class="icon-circle bg-grenat">
																		<i class="fa fa-hourglass-1"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Indicateurs de production</h4>
																		<p>
																			
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="<?php echo site_url("indicateur/indicateur/visu_indicateur"); ?>">
																	<div class="icon-circle bg-cyan">
																		<i class="fa fa-industry"></i>
																	</div>
																	<div class="menu-info">
																		<h4>Suivi des réceptions</h4>
																		<p>
																			
																		</p>
																	</div>
																</a>
															</li>
															<li>
																<a href="#" onclick="deconnexion();">
																	<div class="icon-circle bg-red">
																		<i class="material-icons">input</i>
																	</div>
																	<div class="menu-info">
																		<h4>Deconnexion</h4>																	   
																	</div>
																</a>
															</li>
														</ul>
													</li>

												</ul>
											 </li>
										 </ul>
									</div-->
								</div>
									
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
	<div> 
	
	</div>
</div>
