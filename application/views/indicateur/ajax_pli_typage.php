<?php
//setlocale (LC_TIME, 'fr_FR');
$liste = '';
$sum_pli     = 0;
$sum_user 	 = 0;
$sum_tot_pli = 0;
	$liste .='<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="card">
			<div class="header">
				<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
					DMT par pli</span>
				<ul class="header-dropdown m-r--5">
					<li class="dropdown">
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="font-family:Arial,Tahoma,sans-serif;font-size:12px;">
							<i class="fa fa-caret-square-o-right">
								<span style="font-family:Arial,Tahoma,sans-serif;font-size:12px;" onclick="export_indicateur_pli_gbl_'.$action_ttt.'()">Exporter
								<div class="preloader pl-size-xs" id="preloader_pli_'.$action_ttt.'" style="display:none;">
										<div class="spinner-layer pl-green" >
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>
								</span>
							</i>
						</a>
						<!--<ul class="dropdown-menu pull-right">
							<li></li>
							                                    
						</ul>-->
					</li>
				</ul>
			</div>
			<div class="body">
				<div class="table-container-body" id="table_indicateur_pli_'.$action_ttt.'">
				<table id="pli_typage" class="table" style="table-layout: fixed;">
					<thead>
						<tr>
							<th  style="width:60%">#ID.GED</th>
							<th  style="width:20%">Durée (h)</th>
						  
						</tr>
					</thead>
				
					<tbody>';
						foreach($arr_pli as $k => $tab){
							$sum_pli += $tab["duree"]; 
						    $liste .= '<tr>
							 <td>#'.$tab["id_pli"].'</td>
							 <td>'.$tab["duree"].'</td>
							 
						</tr>';
							
					  }
					$liste .='</tbody>
					<tfoot >
						<tr>
							<th>Total</th>
							<th>'.number_format($sum_pli,3).'</th>					                             
						</tr>
					</tfoot>
				</table>
			</div>
			</div>
		</div>
	</div>
	
	
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="card">
			<div class="header">
				<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
					indicateur par opérateur</span>
				<ul class="header-dropdown m-r--5">
					<!--li class="dropdown">
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="font-family:Arial,Tahoma,sans-serif;font-size:12px;">
							<i class="fa fa-caret-square-o-right">
								<span style="font-family:Arial,Tahoma,sans-serif;font-size:12px;" onclick="export_indicateur_op_gbl_'.$action_ttt.'()">Exporter 
								<div class="preloader pl-size-xs" id="preloader_op_'.$action_ttt.'" style="display:none;">
										<div class="spinner-layer pl-green" >
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>
								</span>
							</i>
						</a>
					</li-->
				</ul>
			</div>
			<div class="body">
				<div class="table-container-body" id="table_indicateur_op_'.$action_ttt.'">
				<table id="op_typage" class="table">
					<thead>
						<tr>
							<th  style="width:40%">Opérateur</th>
							<th  style="width:20%">Durée</th>
							<th  style="width:20%">Nbr. pli</th>
							<th  style="width:20%">Cad/h</th>
						  
						</tr>
					</thead>
					<tbody>';
						foreach($arr_user as $k => $tab){
							
							$sum_user += $tab["duree"];
							$indicateur = $tab["nb"] / $tab["duree"];
							$sum_tot_pli +=  $tab["nb"];
						    $liste .= '<tr>
							 <td>'.$tab["login"].'</td>
							 <td>'.number_format($tab["duree"],3).'</td>
							 <td>'.$tab["nb"].'</td>
							 <td>'.number_format($indicateur,0).'</td>
							 
						</tr>';
							
					  }
					$liste .='</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th>'.$sum_user.'</th>					                             
							<th>'.$sum_tot_pli.'</th>					                             
							<th></th>					                             
						</tr>
					</tfoot>
				</table>
			</div>
			</div>
			
		</div>
	</div>
	<input type="hidden" id="hr_'.$action_ttt.'" value="'.$sum_user.'">
	
		 <!-- Answered heures -->
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" id="global_'.$action_ttt.'">
			<div class="card">
				<div class="body bg-cyan">
					<div class="font-bold m-b--35">TEMPS DE PRODUCTION</div>
					<ul class="dashboard-stat-list">
						<li>
							<i class="material-icons">style</i> TYPAGE
							<span class="pull-right"><b><span id="div_hr_typage">__</span></b><small> HEURES</small></span>
						</li>
						<li>
							<i class="material-icons">border_color</i> SAISIE
							<span class="pull-right"><b><span id="div_hr_saisie">__</span></b><small> HEURES</small></span>
						</li>
						<li>
							<i class="material-icons">done_all</i> CONTROLE
							<span class="pull-right""><b><span id="div_hr_controle">__</span></b> <small> HEURES</small></span>
						</li>
						<li>
							<i class="material-icons">pie_chart</i> TOTAL
							<span class="pull-right"><b><span id="div_hr_totale">__</span></b><small> HEURES</small></span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- #END# Answered heures -->
	
	<!-- #END#  -->';
	echo $liste;
	
?>