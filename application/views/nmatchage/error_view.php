<div class="alert alert-danger">
    <strong><i class="material-icons">error</i> Erreur!</strong> Une erreur est survenue lors de la pr&eacute;paration des donn&eacute;es.
</div>