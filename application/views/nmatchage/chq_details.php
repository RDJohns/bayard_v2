<div class="admintab-wrap edu-tab1">
    <?php
    foreach ($documents as $document) : ?>
        &nbsp;&nbsp;&nbsp;<img src="data:image/jpeg;base64, <?php echo $document->n_ima_base64_recto; ?>" class="img-rounded" alt="" width="250" height="200" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$document->id_pli.' / Doc#'.$document->id_document; ?>" data-group="<?php echo $document->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $document->n_ima_base64_recto; ?>">
        &nbsp;&nbsp;&nbsp;<img src="data:image/jpeg;base64, <?php echo $document->n_ima_base64_verso; ?>" class="img-rounded" alt="" width="250" height="200" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$document->id_pli.' / Doc#'.$document->id_document; ?>" data-group="<?php echo $document->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $document->n_ima_base64_verso; ?>">
    <?php
        endforeach;
    ?>
<!--ul class="nav nav-tabs custom-menu-wrap custon-tab-menu-style1 tab-menu-right">
        <li class="active">
            <a data-toggle="tab" href="#recto">
                <span class="edu-icon edu-analytics tab-custon-ic"></span>Recto
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#verso">
                <span class="edu-icon edu-analytics-arrow tab-custon-ic"></span>Verso
            </a>
        </li>
    </ul>
    <div class="tab-content" style="overflow: auto;">
        <div id="recto" class="tab-pane in active animated flipInY custon-tab-style1">
            <img src="data:image/jpeg;base64, <?php echo $document->n_ima_base64_recto; ?>"  class="img_b64" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$document->id_pli.' / Doc#'.$document->id_document.' / RECTO'; ?>" data-group="<?php echo $document->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $document->n_ima_base64_recto; ?>"/>
        </div>
        <div id="verso" class="tab-pane animated flipInY custon-tab-style1">
            <img src="data:image/jpeg;base64, <?php echo $document->n_ima_base64_verso; ?>"  class="img_b64" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$document->id_pli.' / Doc#'.$document->id_document.' / VERSO'; ?>" data-group="<?php echo $document->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $document->n_ima_base64_verso; ?>"/>
        </div>
    </div-->
</div>