<div class="container-fluid" style="margin-top: 100px">
    <p align="center" id="lib-progress" style="display: none;"><b>Matchage en cours...</b>
    </p>
    <div class="progress" style="display:none;">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            0%
        </div>
    </div>
    <div id="match-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>Interface matchage automatique</h2>
                    </div>
                    <div class="body">

                        <form id="upload-form" method="post" enctype="multipart/form-data">
                        <div class="row clearfix">
                            <div class="col-md-2">
                                <select class="form-control show-tick" id="id_societe" name="id_societe">
                                    <option value="">--- Choisir société ---</option>
                                    <?php foreach ($societe as $soc): ?>
                                        <option value="<?= $soc->id ?>"><?= $soc->nom_societe ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input type="file" name="userfile" size="20" id="userfile" />
                            </div>
                            <div>
                                <input type="checkbox" id="checkbox_nofile" class="filled-in" />
                                <label for="checkbox_nofile">Pas de fichier à importer <span style="color:red;">*</span></label>
                            </div>
                        </div>
                        <input type="button" id="send-form" class="btn btn-primary m-t-15 waves-effect" value="Matcher" />
                        <input type="button" id="validate-match" class="btn btn-success m-t-15 waves-effect" value="Valider matchage" />

                        </form>
                        <br />
                        <em id="upload-error" style="color:red;"></em>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p align="right">
                    <span style="color:red;">(*)</span> A cocher s'il n'y a pas de données à matcher pour la société
                </p>
            </div>
        </div>
    </div>
</div>
