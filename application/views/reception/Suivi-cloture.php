<div class="row clearfix suivi-typo ged-reception"  style="margin-top:20px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Plis clôturés</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:detailCloParJour();">Par jour</a></li>
                            <li><a href="javascript:detailCloParSemaine();">Par semaine</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="recp-clo" ></div>
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="caption"><span class="badge bg-teal">Par pli</span></div>
                        <div id="donut-cloture-pli" class="graph" style="height: 250px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="caption" style="float: right;"><span class="badge bg-black">Par mouvement</span></div>
                        <div id="donut-cloture-mv" class="graph" style="height: 250px;"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-clo-table" >
        <div class="card">
            <div class="header">
                <h5><span id="title-detail-clo"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-clo-table');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="clo-par-jour">

                </div>

            </div>
        </div>
    </div>
</div>

<div class="row clearfix suivi-typo ged-reception-flux"  style="margin-top:20px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Flux clôturés</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:detailCloParJourFlux();">Par jour</a></li>
                            <li><a href="javascript:detailCloParSemaineFlux();">Par semaine</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="recp-clo-flux" ></div>
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="caption"><span class="badge bg-teal">Par flux</span></div>
                        <div id="donut-cloture-flux" class="graph" style="height: 250px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="caption" style="float: right;"><span class="badge bg-black">Par abonnement</span></div>
                        <div id="donut-cloture-abnmt" class="graph" style="height: 250px;"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-clo-table-flux" >
        <div class="card">
            <div class="header">
                <h5><span id="title-detail-clo-flux"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-clo-table-flux');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="clo-par-jour-flux">

                </div>

            </div>
        </div>
    </div>
</div>



