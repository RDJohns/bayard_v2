<br><br><br><br>
<div id="global-filtre">


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1 filtre">Courrier du </div>
            <div class="col-md-3">
                <div class="input-daterange input-group" id="bs_datepicker_range_container">
                    <div class="form-line">
                        <input type="text" class="form-control recpt-date" id="date-debut" value="<?php echo date( 'd/m/Y', strtotime( 'monday this week' ) );?>">
                    </div>
                    <span class="input-group-addon">au&nbsp;&nbsp;</span>
                    <div class="form-line">
                        <input type="text" class="form-control recpt-date" id="date-fin" value="<?php echo date( 'd/m/Y', strtotime( 'friday this week' ) );?>">
                    </div>
                    
                </div>
            </div>
            <div class="col-md-2">
                <select class="form-control show-tick" tabindex="-98" id="filtre-societe">
                    <option value="0" disabled >-- Choisir une socièté --</option>
                    <option value="1">Bayard</option>
                    <option value="2">Milan</option>
                </select>
            </div>
            <div class="col-md-1">
                    <button class="btn btn-primary waves-effect waves-light" id="stat_rech" onclick="charger_plis();">&nbsp;Rechercher</button>
            </div>
            <span >
                <ol class="breadcrumb ged-reception">
                    <li id="tab_plis">
                        <a href="javascript:listePlis();">
                            <i class="material-icons">bubble_chart</i>
                            <i class=""></i> Voir la liste des plis
                        </a>
                    </li>
                    <li id="tab_stat">
                        <a onclick="voirSynthese();" style="cursor:pointer;">
                            <i class="material-icons">equalizer</i>
                            <i class=""></i> Voir la synthèse
                        </a>
                    </li>
                    <li id="tab_stat-2">
                        <a onclick="resultatInitial();" style="cursor:pointer;">
                            <i class="material-icons">pie_chart</i>
                            <i class=""></i> Voir le résultat initial
                        </a>
                    </li>
                </ol>
            </span>
        </div>
    </div>
    <div class="preloader-reception">
        <span>Chargement de données...</span>
    </div>



    <div class="container-fluid">
        <div class="row ged-reception">
        <div class="col-lg-2 col-lg-offset-1">
                <div class="info-box bg-cyan-t hover-expand-effect">
                    <div class="icon">
                        
                        <i class="material-icons">mail</i>
                    </div>
                    <div class="content">
                        <div class="text">Plis total</div>
                    
                        <span class="info-text">Nbr. Pli : <span id="pli_total"></span></span><br/>
                        <span class="info-text">Nbr. mvt : <span id="mouvement_total"></span></span><br/>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">compare_arrows</i>
                    </div>
                    <div class="content">
                        <div class="text">Non traité</div>
                        <span class="info-text">Nbr. Pli : <span id="nb_pli_non_traite"></span></span><br/>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">Clôturé</div>
                        

                        <span class="info-text">Nbr. Pli : <span id="pli_cloture"></span></span><br/>
                        <span class="info-text">Nbr. mvt : <span id="mvt_cloture"></span></span><br/>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">hourglass_full</i>
                    </div>
                    <div class="content">
                        <div class="text">En cours</div>
                        <span class="info-text">Nbr. Pli : <span id="nb_pli_en_cours"></span></span><br/>
                        <span class="info-text">Nbr. mvt : <span id="mouvement_en_cours"></span></span><br/>
                    
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="info-box bg-red7 hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">bug_report</i>
                    </div>
                    <div class="content">
                        <div class="text">Anomalie</div>
                        <span class="info-text">Nbr. Pli : <span id="nb_pli_anomalie"></span></span><br/>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- fin global -->
<input type="hidden" id="checkth">
<div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 voir-liste-pli ged-reception" style="margin-top:10px;">
    <div class="card">
    <div class="header">
            <h5><span>Données de réception des plis</span></h5>
            <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                    <a onclick="fermer('voir-liste-pli');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                        <i class="material-icons">close</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div id="liste-plis">
                <table class="table table-bordered table-striped table-hover dataTable table-liste" id="table-liste-plis">
                    <thead>
                        <tr>
                            <th>Réception du courrier</th>
                            <th>Date numérisation</th>
                            <th>Date du traitement</th>
                            <th>Ref. lot scan</th>
                            <th>pli</th>
                            <th>Typologie</th>
                            <th>Statut</th>
                            <th>Nbr. mouvement</th>
                            <th>ID lot de saisie</th>
                            <th>Moyen de paiement</th>
                            <th>ID Pli</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 voir-synthese ged-reception"  style="margin-top:10px;">
        <div class="card">
        <div class="header">
        <h5><span><h5><span>Synthèse par jour</span></h5></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('voir-synthese');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="synthese-journaliere">

                </div>
                
                
            </div>
        </div>
    
</div>
<div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 voir-synthese-semaine ged-reception"  style="margin-top:10px;">
        <div class="card">
        <div class="header">
                <h5><span><h5><span>Synthèse par semaine</span></h5></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('voir-synthese-semaine');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="synthese-semaine">

                </div>
                
            </div>
        </div>
    
</div>