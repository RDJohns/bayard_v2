<div class="row clearfix suivi-typo ged-reception"  style="margin-top:20px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Plis par typologie</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:detailTypologieParJour();">Par jour</a></li>
                            <li><a href="javascript:detailTypologieParSemaine();">Par semaine</a></li>

                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="area_chart" class="graph"></div>
                <div id="recp-typo" ></div>
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="caption"><span class="badge bg-teal">Par pli</span></div>
                        <div id="donut-typo-pli" class="graph" style="height: 250px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="caption" style="float: right;"><span class="badge bg-black">Par mouvement</span></div>
                        <div id="donut-typo-mv" class="graph" style="height: 250px;"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <!-- #END# Donut Chart -->
    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-typo-table" >
        <div class="card">
        <div class="header">
                <h5><span id="title-detail"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-typo-table');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="typologie-par-jour">

                </div>
               
            </div>
        </div>
    </div>
    
</div>

<div class="row clearfix suivi-typo ged-reception-flux"  style="margin-top:20px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Flux par typologie</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:detailTypologieParJourFlux();">Par jour</a></li>
                            <li><a href="javascript:detailTypologieParSemaineFlux();">Par semaine</a></li>

                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="area_chart" class="graph"></div>
                <div id="recp-typo-flux" ></div>
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="caption"><span class="badge bg-teal">Par flux</span></div>
                        <div id="donut-typo-flux" class="graph" style="height: 250px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="caption" style="float: right;"><span class="badge bg-black">Par abonnement</span></div>
                        <div id="donut-typo-abnmt" class="graph" style="height: 250px;"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <!-- #END# Donut Chart -->
    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-typo-table-flux" >
        <div class="card">
            <div class="header">
                <h5><span id="title-detail-flux"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-typo-table-flux');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="typologie-par-jour-flux">

                </div>

            </div>
        </div>
    </div>

</div>