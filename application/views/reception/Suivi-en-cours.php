<div class="row clearfix suivi-en-cours suivi-typo ged-reception"  style="margin-top:10px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Plis en cours</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:detailEncoursParJour();">Par jour</a></li>
                            <li><a href="javascript:detailEncoursParSemaine();">Par semaine</a></li>
                            <!--<li><a href="javascript:detailEncoursParTypologie();">Par typologie</a></li> !-->
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="recp-en-cours" ></div>
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="caption"><span class="badge bg-teal">Par pli</span></div>
                        <div id="donut-en-cours-pli" class="graph" style="height: 250px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="caption" style="float: right;"><span class="badge bg-black">Par mouvement</span></div>
                        <div id="donut-en-cours-mv" class="graph" style="height: 250px;"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-encours-table" >
        <div class="card">
        <div class="header">
                <h5><span id="title-detail-encours"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-encours-table');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="encours-par-jour">

                </div>
                
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-encours-typo-table" >
        <div class="card">
        <div class="header">
                <h5><span id="title-detail-typo-encours">En cours par typologie</span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-encours-typo-table');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="encours-par-typo">
                   
                </div>
                
            </div>
        </div>
    </div>
</div>

<div class="row clearfix suivi-en-cours suivi-typo ged-reception-flux"  style="margin-top:10px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Flux en cours</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:detailEncoursParJourFlux();">Par jour</a></li>
                            <li><a href="javascript:detailEncoursParSemaineFlux();">Par semaine</a></li>
                            <li><a href="javascript:detailEncoursParTypologieFlux();">Par typologie</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="recp-en-cours-flux" ></div>
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="caption"><span class="badge bg-teal">Par flux</span></div>
                        <div id="donut-en-cours-flux" class="graph" style="height: 250px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="caption" style="float: right;"><span class="badge bg-black">Par mouvement</span></div>
                        <div id="donut-en-cours-abnmt" class="graph" style="height: 250px;"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-encours-table-flux" >
        <div class="card">
            <div class="header">
                <h5><span id="title-detail-encours-flux"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-encours-table-flux');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="encours-par-jour-flux">

                </div>

            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-encours-typo-table-flux" >
        <div class="card">
            <div class="header">
                <h5><span id="title-detail-typo-encours-flux">En cours par typologie</span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-encours-typo-table-flux');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="encours-par-typo-flux">

                </div>

            </div>
        </div>
    </div>
</div>
