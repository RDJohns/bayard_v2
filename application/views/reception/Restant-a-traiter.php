
<div class="row clearfix restant-a-traiter suivi-typo ged-reception"  style="margin-top:20px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Plis restant à traiter</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:nonTraiteJour();">Par jour</a></li>
                            <li><a href="javascript:nonTraiteSemaine();">Par semaine</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="recp-rest-traiter" ></div>
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div id="donut-restant-mv" class="graph" style="height: 250px;"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-non-table" >
        <div class="card">
        <div class="header">
                <h5><span id="title-detail-non"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-non-table');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
        </div>
            <div class="body">
                <div id="non-par-jour">

                </div>
                
            </div>
        </div>
</div>
</div>

<!-- pli fermer-->

<div class="row clearfix pli-fermer suivi-typo ged-reception"  style="margin-top:20px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Pli avec anomalie / CI Editée / OK + CI</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                           
                            <li><a href="javascript:avecAnomalieJour();">Par jour</a></li>
                            <li><a href="javascript:avecAnomalieSemaine();">Par semaine</a></li>
                        
                            <!-- <li><a href="javascript:syntheseFermerSemaine();">Voir détail</a></li> -->
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="recp-pli-fermer" ></div> <!-- ON met ici les plis avec anomalie -->
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                       <div class="caption"><span class="badge bg-teal">Pli actuel</span></div>
                        <div id="donut-pli-fermer" class="graph" style="height: 250px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="caption" style="float: right;"><span class="badge bg-black">Pli passé en KO</span></div>
                        <div id="donut-pli-ko-passe" class="graph" style="height: 250px;"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-anomalie-table" > 
        <div class="card">
        <div class="header">
                <h5><span id="title-detail-anomalie-table"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-anomalie-table');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
                <div class="body">
                    <div id="anomalie-table"><!-- table détail anomalie -->
                        xxxxxXXXXxXx	
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-pli-passe-ko-table" > 
        <div class="card">
        <div class="header">
                <h5><span id="title-detail-pli-passe-ko-table"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-pli-passe-ko-table');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
                <div class="body">
                    <div id="pli-passe-ko-table">
                        xxxxxXXXXxXx	
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
<!-- pli fermer fin -->



<div class="row clearfix restant-a-traiter suivi-typo ged-reception-flux"  style="margin-top:20px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Flux restant à traiter</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:nonTraiteJourFlux();">Par jour</a></li>
                            <li><a href="javascript:nonTraiteSemaineFlux();">Par semaine</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="recp-rest-traiter-flux" ></div>
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div id="donut-restant-abnmt" class="graph" style="height: 250px;"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-non-table-flux" >
        <div class="card">
            <div class="header">
                <h5><span id="title-detail-non-flux"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-non-table-flux');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="non-par-jour-flux">

                </div>

            </div>
        </div>
    </div>


</div>

<div class="row clearfix flux-anomalie suivi-typo ged-reception-flux"  style="margin-top:20px;">
    <!-- Area Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Flux avec anomalie</h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">

                            <li><a href="javascript:fluxAnomalieJour();">Par jour</a></li>
                            <li><a href="javascript:fluxAnomalieSemaine();">Par semaine</a></li>

                            <!-- <li><a href="javascript:syntheseFermerSemaine();">Voir détail</a></li> -->
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="recp-flux-anomalie" ></div> <!-- ON met ici les plis avec anomalie -->
            </div>
        </div>
    </div>
    <!-- #END# Area Chart -->
    <!-- Donut Chart -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h5>Répartition en mode graphe</h5>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="caption"><span class="badge bg-teal">Actuel</span></div>
                        <div id="donut-anomalie-flux" class="graph" style="height: 250px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="caption" style="float: right;"><span class="badge bg-black">Cumul</span></div>
                        <div id="donut-ttlano-flux" class="graph" style="height: 250px;"></div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 detail-anomalie-table-flux" >
        <div class="card">
            <div class="header">
                <h5><span id="title-detail-anomalie-table-flux"></span></h5>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a onclick="fermer('detail-anomalie-table-flux');" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="flux-anomalie-table"><!-- table détail anomalie -->
                    xxxxxXXXXxXx
                </div>

            </div> 
        </div>
    </div>
</div>

<div  style="margin-top:20px;"></div>
