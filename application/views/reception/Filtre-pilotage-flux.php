<br><br><br><br>
<div id="global-filtre">
    <div class="container-fluid">
            <div class="row">
            <div class="col-md-3 filtre"> </div>
                <div class="col-md-1 filtre">Réception du </div>
                <div class="col-md-3">
                    <div class="input-daterange input-group" id="bs_datepicker_range_container">
                        <div class="form-line">
                            <input type="text" class="form-control recpt-date" id="date-debut" value="<?php echo date( 'd/m/Y', strtotime( 'yesterday' ) );?>">
                        </div>
                        <span class="input-group-addon">au&nbsp;&nbsp;</span>
                        <div class="form-line">
                            <input type="text" class="form-control recpt-date" id="date-fin" value="<?php echo date( 'd/m/Y');?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                        <button class="btn btn-primary waves-effect waves-light" id="stat_rech" onclick="exportPilotageFlux();">&nbsp;Exporter les données</button>
                </div>
            </div>
    </div>
 </div>

