<br><br><br><br>

<div id="global-filtre">
<div class="container-fluid">
   <div class="row">
      <div class="col-md-1" style="top: 8px !important;">Date saisie </div>
      <div class="col-md-2">
      <div class="form-group">
            <div class="form-line"  >
                 <input  type="text" id="date-saisie-fautes" class="form-control recpt-date" name="daterange" value="2018/07/01 - 2018/07/01" style="background-color: white !important;text-align:center" readonly/>
            </div>
        </div>
      </div>
      

      <div class="col-md-1" style="top: 8px !important;">Société</div>
      <div class="col-md-2">
         <select class="form-control show-tick" tabindex="-98" id="filtre-societe-fautes" data-live-search="true">
            <option value="0"   disabled>--Choisir une société--</option>
            <option value="1">Bayard</option>
            <option value="2">Milan</option>
         </select>
      </div>

      <div class="col-md-1" style="top: 8px !important;">Granularité</div>
      <div class="col-md-1">
         <select class="form-control show-tick" tabindex="-98" id="granularite-fautes" >
            <option value="0"   disabled>--Choisir l'étape du traitement--</option>
            <option value="j">Jour</option>
            <option value="s">Semaine</option>
            <!--<option value="m">Mois</option> -->
         </select>
      </div>
      <div class="col-md-4">
      <button class="btn btn-primary waves-effect waves-light" id="btn_fautes-rechercher" onclick="getFautes();">
           &nbsp;Rechercher
        </button>
        &nbsp;&nbsp;
      <!--  <button class="btn btn-warning waves-effect waves-light" id="visu_indicateur" >
           &nbsp;Réinitialiser
        </button> -->
      </div>
     
    </div>
    
    
    <!--  <div class="col-md-1">
         <button class="btn btn-primary waves-effect waves-light" id="stat_rech" onclick="afficherPliKO();">&nbsp;Rechercher</button>
      </div> -->
  
      
   </div>

</div>

            