


            
            
<div class="container-fluid fautes-ged">
    <div class="row clearfix">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                        <h5>Fautes par opérateur (globale)</h5>
                        </div>
                        <!--<div class="col-xs-12 col-sm-6 align-right">
                            <div class="switch panel-switch-btn">
                                <span class="m-r-10 font-12">REAL TIME</span>
                                <label>OFF<input type="checkbox" id="realtime" checked=""><span class="lever switch-col-cyan"></span>ON</label>
                            </div>
                        </div> !-->
                    </div>
                   
                </div>
                <div class="body">
                    <table class="table table-bordered table-striped table-hover table-fautes-op" id="tb-fautes-op-globale">
                        <thead>
                            <tr>
                                <th style="width:45%">Opérateur</th>
                                <th style="width:15%">Nbr. champ</th>
                                <th style="width:15%">Nbr. champ avec fautes</th>
                                <th style="width:15%">Taux de fautes</th>
                            </tr>
                        </thead>
                   </table>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                        <h5>Fautes par opérateur détaillée (%)</h5>
                        </div>
                        <!--<div class="col-xs-12 col-sm-6 align-right">
                            <div class="switch panel-switch-btn">
                                <span class="m-r-10 font-12">REAL TIME</span>
                                <label>OFF<input type="checkbox" id="realtime" checked=""><span class="lever switch-col-cyan"></span>ON</label>
                            </div>
                        </div> !-->
                    </div>
                    
                </div>
                <div class="body">
                   <div id="table-ope-detaillee">

                   </div>
                </div>
            </div>
        </div>

        
    </div>


    
</div>
    