


              
            
<div class="container-fluid fautes-ged">
    <div class="row clearfix">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12">
                        <h5>Répartition en mode graphe : Nombre des typologies avec faute</h5>
                        </div>
                        <!--<div class="col-xs-12 col-sm-6 align-right">
                            <div class="switch panel-switch-btn">
                                <span class="m-r-10 font-12">REAL TIME</span>
                                <label>OFF<input type="checkbox" id="realtime" checked=""><span class="lever switch-col-cyan"></span>ON</label>
                            </div>
                        </div> !-->
                    </div>
                    
                </div>
                <div class="body">
                   <div id="donut-typo-pli">
    
                   </div>
                </div>
            </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12">
                        <h5>Répartition en mode graphe : Nombre des champs avec faute</h5>
                        </div>
                        <!--<div class="col-xs-12 col-sm-6 align-right">
                            <div class="switch panel-switch-btn">
                                <span class="m-r-10 font-12">REAL TIME</span>
                                <label>OFF<input type="checkbox" id="realtime" checked=""><span class="lever switch-col-cyan"></span>ON</label>
                            </div>
                        </div> !-->
                    </div>
                   
                </div>
                <div class="body">
                    <div id="donut-champ-pli">
    
                    </div>
                </div>
            </div>
        </div>
       
        
    </div>


    
</div>
    