<div class="admintab-wrap edu-tab1">
    <?php
    foreach ($documents as $document) : ?>
        &nbsp;&nbsp;&nbsp;<img src="data:image/jpeg;base64, <?php echo $document->n_ima_base64_recto; ?>" class="img-rounded" alt="" width="250" height="200" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$document->id_pli.' / Doc#'.$document->id_document; ?>" data-group="<?php echo $document->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $document->n_ima_base64_recto; ?>">
        &nbsp;&nbsp;&nbsp;<img src="data:image/jpeg;base64, <?php echo $document->n_ima_base64_verso; ?>" class="img-rounded" alt="" width="250" height="200" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$document->id_pli.' / Doc#'.$document->id_document; ?>" data-group="<?php echo $document->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $document->n_ima_base64_verso; ?>">
        <?php
    endforeach;
    ?>
</div>