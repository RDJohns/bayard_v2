<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <style>

        body{font-family:Verdana,'Open Sans',sans-serif;font-size: 12px;}
        th,td{border-collapse:collapse; border: 1px solid #1A747A;}
        blockquote{font-style: normal;margin-left: 32px;border-left: 4px solid #CCC;padding-left: 8px;}
    </style>
</head>
<body>

Bonjour, <br><br>

Merci de trouver ci-joint le reporting hebdomadaire des ch&egrave;ques cadeaux
<?php if($week != null){
    echo 'de la semaine S'.$week.'.';
}else{
    $chaine = 'du mois ';
    if (in_array(strtolower($month[0]), array('a','o'))){
        $chaine .= 'd\''.$month;
    }
    else{
        $chaine .= 'de '.$month;
    }
    echo $chaine;
} ?>

<br><br>

L'&eacute;quipe VIVETIC <br><br>

<img src="cid:logo_vivetic" alt="vivetic" border="0" />

</body>
</html>