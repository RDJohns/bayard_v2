<div class="card displayer-content">
    <div class="header">
        <h2>Visualisation des documents</h2>
        <a class="header-dropdown m-r--5" onclick="close_displayer()" style="cursor: pointer;">
            <i class="material-icons">close</i>
        </a>
    </div>
    <div class="body">
        <div class="row">
            <div class="col-md-12 chq-displayer">

            </div>
        </div>
    </div>
</div>
<div class="info-chq">
    <div class="card">
        <div class="header">
            <h2>Ecart en montant</h2>
        </div>
        <div class="body">
            <div class="row">
                <div class="btn-content">
                    <button class="btn btn-info waves-effect" onclick="show_col()"> Afficher/cacher les colonnes </button>
                </div>
                <div class="col-md-12" style="overflow: auto;">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th colspan="5" class="base-id-adv">ADVANTAGE</th>
                            <th>&nbsp;</th>
                            <th colspan="10" class="base-id-vv">GED</th>
                        </tr>
                        <tr>
                            <th>Société</th>
                            <th>Montant</th>
                            <th>N&deg; Payeur</th>
                            <th>Code paiement</th>
                            <th class="hidden-column">Date lot</th>
                            <th>Nom lot</th>
                            <th class="hidden-column">Code utilisateur</th>
                            <th> <i class="material-icons">compare_arrows</i> </th>
                            <th>Id pli</th>
                            <th>Id lot saisie</th>
                            <th>N&deg; Payeur</th>
                            <th>Montant</th>
                            <th>Type chq cadeau</th>
                            <th>Statut pli</th>
                            <th class="hidden-column">Date saisie</th>
                            <th>Matricule saisie</th>
                            <th>Statut ch&egrave;que</th>
                            <th>Commentaire</th>
                            <th>Visualisation</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($tab_diff as $chq): ?>
                            <tr>
                                <td><?= $chq['societe'] ?></td>
                                <td>
                                    <span id="montadv-<?= $chq['id_pli'].$chq['id_doc'] ?>">
                                    <?= $chq['montant_adv'] ?>
                                    </span>
                                </td>
                                <td><?= $chq['num_payeur_adv'] ?></td>
                                <td><?= $chq['code_pmt_adv'] ?></td>
                                <td class="hidden-column"><?= $chq['date_lot_adv'] ?></td>
                                <td><?= $chq['nom_lot_adv'] ?></td>
                                <td class="hidden-column"><?= $chq['code_user_adv'] ?></td>
                                <td><?= $chq['diff'] ?></td>
                                <td><?= $chq['id_pli'] ?></td>
                                <td><?= $chq['id_lot_saisie'] ?></td>
                                <td><?= $chq['num_payeur_vv'] ?></td>
                                <td>
                                    <a onclick="change_montant('<?= $chq['id_pli'] ?>','<?= $chq['id_doc'] ?>')" id="montvv-<?= $chq['id_pli'].$chq['id_doc'] ?>" role="button">
                                        <?= $chq['montant_vv'] ?>
                                    </a>
                                </td>
                                <td>
                                    <select class="form-control chq-kdo" id="typekdo-<?= $chq['id_pli'].$chq['id_doc'] ?>">
                                    <?php
                                    foreach ($tab_ckd as $type){
                                        if($chq['id_kdo'] == $type->id)
                                            echo '<option value="'.$chq['id_kdo'].'" selected>'.$chq['type_chq_kdo'].'</option>';
                                        else
                                            echo '<option value="'.$type->id.'">'.$type->type_cheque_cadeau.'</option>';
                                    }
                                    ?>
                                    </select>
                                </td>
                                <td><?= $chq['statut_pli'] ?></td>
                                <td><?= $chq['date_saisie'] ?></td>
                                <td class="hidden-column"><?= $chq['matricule_saisie'] ?></td>
                                <td>
                                    <select class="form-control chq-state" id="status-<?= $chq['id_pli'].$chq['id_doc'] ?>" onchange="change_status('<?= $chq['id_pli'] ?>','<?= $chq['id_doc'] ?>')">
                                        <option value="">-- Choix --</option>
                                        <option value="1">Valid&eacute;</option>
                                        <option value="0">A reporter</option>
                                        <option value="3">Retirer</option>
                                    </select>
                                </td>
                                <td>
                                    <textarea class="form-control chq-comment" id="commvv-<?= $chq['id_pli'].$chq['id_doc'] ?>" onchange="change_comment('<?= $chq['id_pli'] ?>','<?= $chq['id_doc'] ?>')">
                                    </textarea>
                                </td>
                                <td style="text-align: center"><button class="btn btn-info waves-effect" onclick="show_cheque(<?= $chq['id_pli'] ?>)"><i class="material-icons">visibility</i></button></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                    <p>Nombre de chèques : <?= count($tab_diff) ?></p>
                </div>
                <div class="col-md-6" style="overflow: auto;">

                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <h2>Ch&egrave;que(s) absent(s) dans ADV</h2>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-md-12" style="overflow: auto;">
                    <table class="table table-hover table-bordered" style="table-layout: fixed;">
                        <thead>
                        <tr>
                            <th style="width: 5%">Id pli</th>
                            <th style="width: 6%">Id lot saisie</th>
                            <th style="width: 12%">N&deg; Payeur</th>
                            <th style="width: 6%">Montant</th>
                            <th style="width: 19%">Type chq cadeau</th>
                            <th style="width: 5%">Statut pli</th>
                            <th style="width: 10%">Date saisie</th>
                            <th style="width: 8%">Matricule saisie</th>
                            <th style="width: 11%">Statut ch&egrave;que</th>
                            <th style="width: 10%">Commentaire</th>
                            <th style="width: 8%">Visualisation</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($tab_vv as $chq):
                            ?>
                            <tr>
                                <td><?= $chq->id_pli ?></td>
                                <td><?= $chq->id_lot_saisie ?></td>
                                <td><?= str_replace(';',';<br>',$chq->numero_payeur) ?></td>
                                <td><?= $chq->montant ?></td>
                                <td>
                                    <select class="form-control chq-kdo" id="typekdoadv-<?= $chq->id_pli.$chq->id_doc ?>">
                                        <?php
                                        foreach ($tab_ckd as $type){
                                            if($chq->id_kdo == $type->id)
                                                echo '<option value="'.$chq->id_kdo.'" selected>'.$chq->type_chq_kdo.'</option>';
                                            else
                                                echo '<option value="'.$type->id.'">'.$type->type_cheque_cadeau.'</option>';
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td><?= $chq->statut_saisie ?></td>
                                <td><?= date('d/m/Y H:i:s', strtotime($chq->dt_enregistrement)) ?></td>
                                <td><?= $chq->saisie_par ?></td>
                                <td>
                                    <select class="form-control chq-state" id="statusadv-<?= $chq->id_pli.$chq->id_doc ?>" onchange="change_status_adv('<?= $chq->id_pli ?>','<?= $chq->id_doc ?>')">
                                        <option value="">-- Choix --</option>
                                        <option value="1">Valid&eacute;</option>
                                        <option value="0">A reporter</option>
                                        <option value="3">Retirer</option>
                                    </select>
                                </td>
                                <td>
                                    <textarea class="form-control chq-comment" id="commvv-<?=  $chq->id_pli.$chq->id_doc ?>" onchange="change_comment('<?= $chq->id_pli ?>','<?= $chq->id_doc ?>')">
                                    </textarea>
                                </td>
                                <td style="text-align: center">
                                    <button class="btn btn-info waves-effect" onclick="show_cheque(<?= $chq->id_pli ?>)"><i class="material-icons">visibility</i></button>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </table>
                    <p>Nombre de chèques : <?= count($tab_vv) ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <h2>Ch&egrave;que(s) absent(s) dans GED</h2>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-md-12" style="overflow: auto;">
                    <table id="tab-vv" class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Société</th>
                            <th>Montant</th>
                            <th>N&deg; Payeur</th>
                            <th>Code paiement</th>
                            <th>Date lot</th>
                            <th>Nom lot</th>
                            <th>Code utilisateur</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($tab_adv as $chq):
                            ?>
                            <tr>
                                <td><?= $chq->societe ?></td>
                                <td><?= $chq->montant ?></td>
                                <td><?= $chq->num_client ?></td>
                                <td><?= $chq->code_pmt ?></td>
                                <td><?= $chq->date_lot ?></td>
                                <td><?= $chq->nom_lot ?></td>
                                <td><?= $chq->code_utilisateur ?></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                    <p>Nombre de chèques : <?= count($tab_adv) ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <h2>Ch&egrave;que(s) CI dans GED</h2>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-md-12" style="overflow: auto;">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Id pli</th>
                            <th>Id lot saisie</th>
                            <th>N&deg; Payeur</th>
                            <th>Montant</th>
                            <th>Type chq cadeau</th>
                            <th>Statut pli</th>
                            <th>Date saisie</th>
                            <th>Matricule saisie</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($tab_ci as $chq):
                            ?>
                            <tr>
                                <td><?= $chq->id_pli ?></td>
                                <td><?= $chq->id_lot_saisie ?></td>
                                <td><?= $chq->numero_payeur ?></td>
                                <td><?= $chq->montant ?></td>
                                <td><?= $chq->type_chq_kdo ?></td>
                                <td><?= $chq->statut_saisie ?></td>
                                <td><?= $chq->dt_enregistrement ?></td>
                                <td><?= $chq->saisie_par ?></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                    <p>Nombre de chèques : <?= count($tab_ci) ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row m-b-20">
    <div class="col-md-4 text-center">
        <button type="button" class="btn btn-warning btn-lg waves-effect" onclick="redo_match()">
            <i class="material-icons">cached</i>
            <span>Refaire matchage</span>
        </button>
    </div>
    <!--div class="col-md-4 text-center">
        <button type="button" class="btn bg-blue btn-lg waves-effect" id="terminer" onclick="check_match()">
            <i class="material-icons">done</i>
            <span>Terminer matchage</span>
        </button>
        <button type="button" class="btn bg-cyan btn-lg waves-effect" id="exportation" onclick="export_match()">
            <i class="material-icons">file_download</i>
            <span>Exporter matchage</span>
        </button>
    </div-->
    <div class="col-md-4 text-center">
        <button type="button" class="btn bg-teal btn-lg waves-effect" id="validation" onclick="validate_match()">
            <i class="material-icons">check_circle</i>
            <span>Valider matchage</span>
        </button>
    </div>
</div>
<script>
    //var table = $("#tab-vv").DataTable();
</script>
