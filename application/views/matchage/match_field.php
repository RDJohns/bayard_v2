<div class="card displayer-content">
    <div class="header">
        <h2>Visualisation des documents</h2>
        <a class="header-dropdown m-r--5" onclick="close_displayer()" style="cursor: pointer;">
            <i class="material-icons">close</i>
        </a>
    </div>
    <div class="body">
        <div class="row">
            <div class="col-md-12 chq-displayer">

            </div>
        </div>
    </div>
</div>
<div class="info-chq">
    <div class="card">
        <div class="header">
            <h2>Ecart en montant</h2>
        </div>
        <div class="body">
            <div class="row">
                <div class="btn-content">
                    <button class="btn btn-info waves-effect" onclick="show_col()"> Afficher/cacher les colonnes </button>
                </div>
                <div class="col-md-12" style="overflow: auto;">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th colspan="5" class="base-id-adv">ADVANTAGE</th>
                                <th>&nbsp;</th>
                                <th colspan="9" class="base-id-vv">GED</th>
                            </tr>
                            <tr>
                                <th class="hidden-column">Num Cde</th>
                                <th>N&deg; Payeur</th>
                                <th class="hidden-column">Nom Payeur</th>
                                <th class="hidden-column">N&deg; Reçu</th>
                                <th class="hidden-column">Mode Pmt</th>
                                <th>N&deg; Pmt</th>
                                <th>Montant Pay&eacute;</th>
                                <th>Date lot<br>Contrôle</th>
                                <th>Lot de<br>Contrôle</th>
                                <th> <i class="material-icons">compare_arrows</i> </th>
                                <th>Id Pli</th>
                                <th>N&deg; Payeur</th>
                                <th class="hidden-column">Nom Payeur</th>
                                <th>N&deg; Abonn&eacute;</th>
                                <th>N&deg; CMC7</th>
                                <th>Montant Pay&eacute;</th>
                                <th class="hidden-column">Statut pli</th>
                                <th class="hidden-column">Date saisie</th>
                                <th>Id lot saisie</th>
                                <th class="hidden-column">Mle saisie</th>
                                <th>Statut ch&egrave;que</th>
                                <th>Commentaire</th>
                                <th>Visualiser</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($tab_diff as $chq): ?>
                                <tr>
                                    <td class="hidden-column"><?= $chq['num_cde'] ?></td>
                                    <td><?= $chq['num_payeur_adv'] ?></td>
                                    <td class="hidden-column"><?= $chq['nom_payeur_adv'] ?></td>
                                    <td class="hidden-column"><?= $chq['num_recu_adv'] ?></td>
                                    <td class="hidden-column"><?= $chq['mode_pmt_adv'] ?></td>
                                    <td><?= $chq['num_pmt_adv'] ?></td>
                                    <td>
                                        <span id="montadv-<?= $chq['id_chq'] ?>">
                                        <?= $chq['montant_pmt_adv'] ?>
                                        </span>
                                    </td>
                                    <td><?= $chq['ctg_dte'] ?></td>
                                    <td><?= $chq['ctg_nme'] ?></td>
                                    <td><?= $chq['diff'] ?></td>
                                    <td><?= $chq['id_pli'] ?></td>
                                    <td><?= $chq['num_payeur_vv'] ?></td>
                                    <td class="hidden-column"><?= $chq['nom_payeur_vv'] ?></td>
                                    <td><?= $chq['num_abonne_vv'] ?></td>
                                    <td><?= $chq['cmc7_vv'] ?></td>
                                    <td>
                                        <?php //if($chq['montant_pmt_adv'] < $chq['montant_vv']): ?>
                                        <a onclick="change_montant(<?= $chq['id_chq'] ?>)" id="montvv-<?= $chq['id_chq'] ?>" role="button"><?php //endif; ?>
                                        <?= $chq['montant_vv'] ?>
                                        <?php //if($chq['montant_pmt_adv'] < $chq['montant_vv']): ?>
                                        </a>
                                        <?php //endif; ?>
                                    </td>
                                    <td class="hidden-column"><?= $chq['statut_vv'] ?></td>
                                    <td class="hidden-column"><?= $chq['dt_enreg_vv'] ?></td>
                                    <td><?= $chq['id_lot_saisie'] ?></td>
                                    <td class="hidden-column"><?= $chq['saisie_par_vv'] ?></td>
                                    <td>
                                        <select class="form-control chq-state" id="status-<?= $chq['id_chq'] ?>" onchange="change_status(<?= $chq['id_chq'] ?>)">
                                            <option value="">-- Choix --</option>
                                            <?php foreach ($st_ecart as $statut): ?>
                                                <?php //if($statut->id_etat == 1){
                                                    //if($chq['montant_pmt_adv'] < $chq['montant_vv']){ ?>
                                                    <option value="<?= $statut->id_etat ?>"><?= $statut->libelle ?></option>
                                                <?php
                                                    //}
                                                //} else {
                                                    ?>
                                                    <!--option value="<?= $statut->id_etat ?>"><?= $statut->libelle ?></option-->
                                                    <?php
                                                //}
                                            endforeach;
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <textarea class="form-control chq-comment" id="commvv-<?= $chq['id_chq'] ?>" onchange="change_comment(<?= $chq['id_chq'] ?>)">
                                        </textarea>
                                    </td>
                                    <td><button class="btn btn-info waves-effect" onclick="show_cheque(<?= $chq['id_pli'] ?>)"><i class="material-icons">visibility</i></button></td>
                                </tr>
                        <?php
                            endforeach;
                        ?>
                        </tbody>
                        <!--tbody>
                            <tr>
                                <td class="hidden-column">Test</td>
                                <td>Test</td>
                                <td class="hidden-column">Test</td>
                                <td>Test</td>
                                <td class="hidden-column">Test</td>
                                <td class="hidden-column">Test</td>
                                <td class="hidden-column">Test</td>
                                <td>Test</td>
                                <td class="hidden-column">Test</td>
                                <td><span id="montadv-149">40</span></td>
                                <td>Test</td>
                                <td>Test</td>
                                <td>Test</td>
                                <td>Test</td>
                                <td>Test</td>
                                <td class="hidden-column">Test</td>
                                <td>Test</td>
                                <td>Test</td>
                                <td><a onclick="change_montant(149)" id="montvv-149">20</a></td>
                                <td class="hidden-column">Test</td>
                                <td class="hidden-column">Test</td>
                                <td class="hidden-column">Test</td>
                                <td>
                                    <select class="form-control chq-state" id="status-149" onchange="change_status(149)">
                                        <option value="">-- Choix --</option>
                                        <option value="1">Valid&eacute;</option>
                                        <option value="0">A reporter</option>
                                    </select>
                                </td>
                                <td>
                                    <textarea class="form-control chq-comment" id="commvv-149" onchange="change_comment(149)">
                                    </textarea>
                                </td>
                                <td><button class="btn btn-info waves-effect" onclick="show_cheque('21649')"><i class="material-icons">visibility</i></button></td>
                            </tr>
                        </tbody-->
                    </table>
                    <p>Nombre de chèques : <?= count($tab_diff) ?></p>
                </div>
                <div class="col-md-6" style="overflow: auto;">

                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <h2>Ch&egrave;que(s) absent(s) dans ADV</h2>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-md-12" style="overflow: auto;">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Id Pli</th>
                            <th>N&deg; Payeur</th>
                            <th>Nom Payeur</th>
                            <th>N&deg; Abonn&eacute;</th>
                            <th>N&deg; CMC7</th>
                            <th>Mtant Pay&eacute;</th>
                            <th>Statut pli</th>
                            <th>Date saisie</th>
                            <th>Id lot saisie</th>
                            <th>Matricule saisie</th>
                            <th>Statut ch&egrave;que</th>
                            <th>Commentaires</th>
                            <th>Visualiser</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($tab_vv as $chq):
                            ?>
                            <tr>
                                <td><?= $chq->id_pli ?></td>
                                <td><?= $chq->numero_payeur ?></td>
                                <td><?= $chq->nom_payeur ?></td>
                                <td><?= $chq->numero_abonne ?></td>
                                <td><?= $chq->cmc7 ?></td>
                                <td><?= $chq->montant ?></td>
                                <td><?= $chq->flag_traitement ?></td>
                                <td><?= date('d/m/Y H:i:s', strtotime($chq->dt_enregistrement)) ?></td>
                                <td><?= $chq->id_lot_saisie ?></td>
                                <td><?= $chq->saisie_par ?></td>
                                <td>
                                    <select class="form-control chq-state" id="statusadv-<?= $chq->id ?>" onchange="change_status_adv(<?= $chq->id ?>)">
                                        <option value="">-- Choix --</option>
                                        <?php foreach ($st_abs_adv as $statut): ?>
                                            <option value="<?= $statut->id_etat ?>"><?= $statut->libelle ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td>
                                    <textarea class="form-control chq-comment" id="commadv-<?= $chq->id ?>" onchange="change_comment_adv(<?= $chq->id ?>)">
                                    </textarea>
                                </td>
                                <td>
                                    <button class="btn btn-info waves-effect" onclick="show_cheque(<?= $chq->id_pli ?>)"><i class="material-icons">visibility</i></button>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        <!--tr>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>
                                <select class="form-control chq-state" id="statusadv-149" onchange="change_status_adv(149)">
                                    <option value="">-- Choix --</option>
                                    <?php foreach ($st_abs_adv as $statut): ?>
                                        <option value="<?= $statut->id_etat ?>"><?= $statut->libelle ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <textarea class="form-control chq-comment" id="commadv-149" onchange="change_comment_adv(149)">
                                </textarea>
                            </td>
                            <td>
                                <button class="btn btn-info waves-effect" onclick="show_cheque(<?= $chq->id_pli ?>)"><i class="material-icons">visibility</i></button>
                            </td>
                        </tr-->
                        </tbody>
                    </table>
                    <p>Nombre de chèques : <?= count($tab_vv) ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <h2>Ch&egrave;que(s) absent(s) dans GED</h2>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-md-12" style="overflow: auto;">
                    <table id="tab-vv" class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Num Cde</th>
                            <th>N&deg; Payeur</th>
                            <th>Nom Payeur</th>
                            <th>N&deg; Reçu</th>
                            <th>Mode Pmt</th>
                            <th>N&deg; Pmt</th>
                            <th>Montant Pay&eacute;</th>
                            <th>Date lot<br>de contôle</th>
                            <th>Lot de contrôle</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($tab_adv as $chq):
                            ?>
                            <tr>
                                <td><?= $chq->num_cde ?></td>
                                <td><?= $chq->num_payeur ?></td>
                                <td><?= $chq->nom_payeur ?></td>
                                <td><?= $chq->num_recu ?></td>
                                <td><?= $chq->mode_pmt ?></td>
                                <td><?= $chq->num_pmt ?></td>
                                <td><?= $chq->montant_paye ?></td>
                                <td><?= $chq->ctg_dte ?></td>
                                <td><?= $chq->ctg_nme ?></td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                    <p>Nombre de chèques : <?= count($tab_adv) ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <h2>Ch&egrave;que(s) CI dans GED</h2>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-md-12" style="overflow: auto;">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Id Pli</th>
                            <th>N&deg; Payeur</th>
                            <th>Nom Payeur</th>
                            <th>N&deg; Abonn&eacute;</th>
                            <th>N&deg; CMC7</th>
                            <th>Mtant Pay&eacute;</th>
                            <th>Statut pli</th>
                            <th>Date saisie</th>
                            <th>Matricule saisie</th>
                            <th>Changer &eacute;tat (OK/KO)</th>
                            <th>Commentaire</th>
                            <th>Visualiser</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($tab_ci as $chq):
                            ?>
                            <tr>
                                <td><?= $chq->id_pli ?></td>
                                <td><?= $chq->numero_payeur ?></td>
                                <td><?= $chq->nom_payeur ?></td>
                                <td><?= $chq->numero_abonne ?></td>
                                <td><?= $chq->cmc7 ?></td>
                                <td><?= $chq->montant ?></td>
                                <td><?= $chq->flag_traitement ?></td>
                                <td><?= $chq->dt_enregistrement ?></td>
                                <td><?= $chq->saisie_par ?></td>
                                <td>
                                    <select class="form-control chq-etat" id="etat-<?= $chq->id ?>" onchange="change_etat(<?= $chq->id ?>)">
                                        <option value="0">-- Choix --</option>
                                        <option value="1">OK</option>
                                        <option value="2">KO</option>
                                    </select>
                                </td>
                                <td>
                                    <textarea class="form-control chq-comment" id="commci-<?= $chq->id ?>" onchange="change_comment_adv(<?= $chq->id ?>)">
                                    </textarea>
                                </td>
                                <td>
                                    <button class="btn btn-info waves-effect" onclick="show_cheque(<?= $chq->id_pli ?>)"><i class="material-icons">visibility</i></button>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        <!--tr>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>Test</td>
                            <td>
                                <select class="form-control chq-etat" id="etat-149" onchange="change_etat(149)">
                                    <option value="0">-- Choix --</option>
                                    <option value="1">OK</option>
                                    <option value="2">KO</option>
                                </select>
                            </td>
                        </tr-->
                        </tbody>
                    </table>
                    <p>Nombre de chèques : <?= count($tab_ci) ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row m-b-20">
    <div class="col-md-4 text-center">
        <button type="button" class="btn btn-warning btn-lg waves-effect" onclick="redo_match('<?= $id_societe ?>')">
            <i class="material-icons">cached</i>
            <span>Refaire matchage</span>
        </button>
    </div>
    <div class="col-md-4 text-center">
        <button type="button" class="btn bg-blue btn-lg waves-effect" id="terminer" onclick="check_match('<?= $id_societe ?>')">
            <i class="material-icons">done</i>
            <span>Terminer matchage</span>
        </button>
        <button type="button" class="btn bg-cyan btn-lg waves-effect" id="exportation" onclick="export_match('<?= $id_societe ?>')">
            <i class="material-icons">file_download</i>
            <span>Exporter matchage</span>
        </button>
    </div>
    <div class="col-md-4 text-center">
        <button type="button" class="btn bg-teal btn-lg waves-effect" id="validation" onclick="validate_match('<?= $id_societe ?>')" disabled>
            <i class="material-icons">check_circle</i>
            <span>Valider matchage</span>
        </button>
    </div>
</div>
<script>
    //var table = $("#tab-vv").DataTable();
</script>