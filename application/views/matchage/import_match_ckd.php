<div class="container-fluid" style="margin-top: 100px">
    <p align="center" id="lib-progress" style="display: none;"><b>Matchage en cours...</b>
    </p>
    <div class="progress" style="display:none;">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
    0%
        </div>
    </div>
    <div id="match-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>Importer fichier XLSX</h2>
                    </div>
                    <div class="body">

                        <form id="upload-form" method="post" enctype="multipart/form-data">

                        <input type="file" name="userfile" size="20" />
                        <input type="button" id="send-form" class="btn btn-primary m-t-15 waves-effect" value="Matcher" />

                        </form>
                        <br />
                        <em id="upload-error" style="color:red;"></em>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
