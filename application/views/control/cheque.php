<?php defined('BASEPATH') OR exit('No direct script access allowed');
    if(!isset($identifiant) || is_null($identifiant)){
        $identifiant = 'chq_'.date('U').mt_rand().mt_rand();                                        
    };
    $etat_modif = is_numeric($etat_modif) && in_array($etat_modif, array(0,1)) ? $etat_modif : 0;
?>

<div class="card grp_field_cheque" id="grp_field_cheque_<?php echo $identifiant ?>" my_ident="<?php echo $identifiant ?>" >
    <div class="header bg-amber">
        <h2>Ch&egrave;que bancaire <small>#<span class="numerotation_cheque"></span></small></h2>
        <ul class="header-dropdown m-r--5 direct_only">
            <li>
                <a href="javascript:get_view_cheque();">
                    <i class="material-icons">add</i>
                </a>
            </li>
            <li class="clear_cheque">
                <a href="javascript:suppr_gr_field_cheque('#grp_field_cheque_<?php echo $identifiant ?>');">
                    <i class="material-icons">clear</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="body">
        <div class="row">
            
            <div class="col-lg-12">
                <input type="checkbox" id="field_chq_etr_<?php echo $identifiant ?>" class="filled-in chk-col-light-blue field field_cheque" <?php echo ($etranger == 1 ? 'checked' : '') ?> />
                <label for="field_chq_etr_<?php echo $identifiant ?>">Ch&egrave;que &eacute;tranger</label>
            </div>

            <div class="col-lg-10 p-r-0">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="p-r-0 form-control field field_cheque oblig field_saisie_ctrl" id="field_cmc7_<?php echo $identifiant ?>" value="<?php echo $cmc7 ?>" nom_field="cmc7_cheque" />
                        <label class="form-label">CMC7</label>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 p-l-1 p-r-0">
                <button type="button" class="btn btn-default waves-effect" onclick="copyToClipboard7cmc7('#field_cmc7_<?php echo $identifiant ?>');">
                    <i class="material-icons">filter_7</i>
                </button>
            </div>
            
            <div class="col-lg-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control field field_cheque nb field_saisie_ctrl" id="field_rlmc_<?php echo $identifiant ?>" value="<?php echo $rlmc ?>" nom_field="rlmc_cheque" />
                        <label class="form-label">RLMC</label>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control field field_cheque oblig montant field_saisie_ctrl" id="field_montant_<?php echo $identifiant ?>" value="<?php echo u_montant($montant) ?>" nom_field="montant_cheque" onchange="$(this).val(u_montant($(this).val()));" />
                        <label class="form-label">Montant</label>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control field field_cheque field_saisie_ctrl" id="field_nom_client_<?php echo $identifiant ?>" value="<?php echo $nom_client ?>" nom_field="nom_client_cheque" />
                        <label class="form-label">Nom client</label>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-12">
                <p>
                    <b>Anomalie :</b>
                    <select class="form-control show-tick sel_field field field_cheque sel_field_cheque sel_field_cheque_anomalie" id="field_anomalie_<?php echo $identifiant ?>" data-live-search="true" data-size="6" data-container="body" data-selected-text-format="count > 0" data-count-selected-text="{0} anomalie(s)" data-window-padding="[100,0,0,0]" multiple <?php echo ($id_etat_chq == 4 ? 'disabled title="REB"' : 'title="Aucune"') ?> >
                        <?php foreach ($anomalies as $anomalie): ?>
                            <?php if($id_etat_chq != 4 || (in_array($anomalie->id, $paiement_anomali) && !in_array($anomalie->id, $tab_id_anom_corrige))): ?>
                                <option value="<?php echo $anomalie->id ?>" <?php echo (in_array($anomalie->id, $paiement_anomali) && !in_array($anomalie->id, $tab_id_anom_corrige) ? 'selected' : '') ?> <?php echo (in_array($anomalie->id, $tab_id_anom_corrige) ? 'disabled data-subtext=" [déjà corrigée]"' : '') ?> ><?php echo $anomalie->anomalie ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>
            <input type="hidden" class="field field_cheque" name="" id="field_etat_chq_<?php echo $identifiant ?>" value="<?php echo $id_etat_chq ?>">
            <input type="hidden" class="field field_cheque" name="" id="field_etat_modif_chq_<?php echo $identifiant ?>" value="<?php echo $etat_modif ?>">
            
            <div class="col-lg-12">
                <p>
                    <b>Document correspondant :</b>
                    <select class="form-control show-tick sel_field field field_cheque sel_field_cheque field_doc_correspond field_saisie_ctrl" id="field_doc_<?php echo $identifiant ?>" data-live-search="true" data-size="5" nom_field="document_cheque" <?php echo ($id_doc_lock ? 'disabled' : '') ?> >
                        <option value="" <?php echo (($id_doc == '') ? 'selected' : '') ?> >Selectionnez ...</option>
                        <?php foreach ($docs as $n => $doc): ?>
                            <?php if(!$id_doc_lock || ($id_doc == $doc->id_document)): ?>
                                <option value="<?php echo $doc->id_document ?>" <?php echo (($id_doc == $doc->id_document/* || $first_doc*/) ? 'selected' : '') ?> >Document <?php echo ($n+1) ?> - #<?php echo $doc->id_document ?></option>
                            <?php endif; ?>
                        <?php //$first_doc = FALSE; ?>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>

        </div>
    </div>
</div>

<style>
    .sel_field_cheque_anomalie.bs-container.open{
        left: 10% !important;
    }
    .sel_field_cheque_anomalie.bs-container.open .dropdown-menu.open{
        z-index: 1091;
    }
</style>
