<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row p-t-100">
    <div class="col-xs-12 align-center">
        <div class="alert bg-orange">
            <?php echo $message ?>
        </div>
    </div>
</div>

<div class="row p-t-100">
	<div class="col-lg-12 align-center">
		<button type="button" class="btn bg-blue btn-lg waves-effect" onclick="load_pli();">
            <i class="material-icons">assignment_returned</i>
            <span><?php echo $lbl_bouton ?></span>
        </button>
	</div>
</div>
