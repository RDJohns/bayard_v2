<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="align-center"></th>
                <th class="align-center">Contr&ocirc;le</th>
                <!-- <th class="align-center">Validation</th> -->
                <th class="align-center">Retraitement</th>
                <th class="align-center font-bold">TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Disponibles</td>
                <td class="align-center"><?php echo $population->nb_ctrl_dispo ?></td>
                <!-- <td class="align-center"><?php //echo $population->nb_valid_dispo ?></td> -->
                <td class="align-center"><?php echo $population->nb_retr_dispo ?></td>
                <td class="align-center font-bold"><?php echo ($population->nb_ctrl_dispo /*+ $population->nb_valid_dispo*/ + $population->nb_retr_dispo) ?></td>
            </tr>
            <tr>
                <td>En cours</td>
                <td class="align-center"><?php echo $population->nb_ctrl_current ?></td>
                <!-- <td class="align-center"><?php //echo $population->nb_valid_current ?></td> -->
                <td class="align-center"><?php echo $population->nb_retr_current ?></td>
                <td class="align-center font-bold"><?php echo ($population->nb_ctrl_current /*+ $population->nb_valid_current*/ + $population->nb_retr_current) ?></td>
            </tr>
            <tr>
                <td>Mes traitements</td>
                <td class="align-center"><?php echo $activity->my_nb_ctrl ?></td>
                <!-- <td class="align-center"><?php //echo $activity->my_nb_valid ?></td> -->
                <td class="align-center"><?php echo $activity->my_nb_retr ?></td>
                <td class="align-center font-bold"><?php echo ($activity->my_nb_ctrl /*+ $activity->my_nb_valid*/ + $activity->my_nb_retr) ?></td>
            </tr>
        </tbody>
    </table>
</div>
