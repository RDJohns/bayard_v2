<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <?php if (count($motif_consignes) > 0): ?>
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Motif du KO</th>
                                <th>Consigne</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($motif_consignes as $key => $mtf_c): ?>
                                <tr>
                                    <td><?php echo $mtf_c->motif_operateur ?><br><small><?php echo ($mtf_c->dt_motif_ko == '' ? '' : (new DateTime($mtf_c->dt_motif_ko))->format('d/m/Y G:i:s')) ?></small></td>
                                    <td><?php echo $mtf_c->consigne_client ?><br><small><?php echo ($mtf_c->dt_consigne == '' ? '' : (new DateTime($mtf_c->dt_consigne))->format('d/m/Y G:i:s')) ?></small></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php else: ?>
                <div class="alert alert-info">
                    <strong>0</strong> motif-KO ou consigne.
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>