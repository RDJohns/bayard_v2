<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row p-t-100">
	<div class="col-lg-offset-3 col-md-offset-3 col-lg-3 col-md-3 col-sm-6 col-xs-12 align-right">
        <p class="align-left"><b>Traitement:</b></p>
        <select class="form-control show-tick sel_field" id="sel_traitement_valid_init" data-container="body">
            <option value="-1" <?php echo ($trtmt_valid == '-1' ? 'selected' : '' ) ?> >Tous</option>
            <?php foreach ($traitements as $traitement): ?>
                <option value="<?php echo $traitement->id_flag_traitement ?>" <?php echo ($trtmt_valid == $traitement->id_flag_traitement ? 'selected' : '' ) ?> ><?php echo $traitement->traitement ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 align-left">
		<button type="button" class="btn bg-blue btn-lg waves-effect m-t-20" onclick="load_pli();">
            <i class="material-icons">assignment_returned</i>
            <span><?php echo $lbl_bouton ?></span>
        </button>
	</div>
</div>
