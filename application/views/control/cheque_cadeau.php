<?php defined('BASEPATH') OR exit('No direct script access allowed');
    if(!isset($identifiant)){
        $identifiant = 'chqkd_'.date('U').mt_rand().mt_rand();                                        
    };
?>

<div class="card grp_field_cheque_kd" id="grp_field_cheque_kd_<?php echo $identifiant ?>" my_ident="<?php echo $identifiant ?>" >
    <div class="header bg-cyan">
        <h2>Ch&egrave;que cadeau <small>#<span class="numerotation_cheque_kd"></span></small></h2>
        <ul class="header-dropdown m-r--5">
            <li>
                <a href="javascript:get_view_cheque_kd();">
                    <i class="material-icons">add</i>
                </a>
            </li>
            <li class="clear_cheque_kd">
                <a href="javascript:suppr_gr_field_cheque_kd('#grp_field_cheque_kd_<?php echo $identifiant ?>');">
                    <i class="material-icons">clear</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="body">
        <div class="row">

            <div class="col-lg-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control field field_cheque_kd oblig_ montant field_saisie_ctrl" id="field_montant_kd_<?php echo $identifiant ?>" value="<?php echo u_montant($montant) ?>" nom_field="montant_cheque_cadeau" onchange="$(this).val(u_montant($(this).val()));" />
                        <label class="form-label">Montant</label>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-12">
                <p>
                    <b>Type :</b>
                    <select class="form-control show-tick sel_field field field_cheque_kd sel_field_cheque_kd field_saisie_ctrl" id="field_type_chq_kd_<?php echo $identifiant ?>" data-live-search="true" data-size="5" nom_field="type_cheque_cadeau">
                        <?php foreach ($choix_type_chq_kds as $choix_type_chq_kd): ?>
                        <option value="<?php echo $choix_type_chq_kd->id ?>" <?php echo ($type_chq_kd == $choix_type_chq_kd->id ? 'selected' : '') ?> ><?php echo $choix_type_chq_kd->type_cheque_cadeau ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>
            
            <div class="col-lg-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control field field_cheque_kd oblig_ field_saisie_ctrl_" id="field_code_barre_kd_<?php echo $identifiant ?>" value="<?php echo $code_barre ?>" nom_field="code_barre_cheque_cadeau" />
                        <label class="form-label">Code barre</label>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-12">
                <p>
                    <b>Document correspondant :</b>
                    <select class="form-control show-tick sel_field field field_cheque_kd sel_field_cheque_kd field_doc_correspond field_saisie_ctrl" id="field_doc_chq_kd_<?php echo $identifiant ?>" data-live-search="true" data-size="5" nom_field="document_cheque_cadeau">
                        <option value="" <?php echo (($id_doc == '') ? 'selected' : '') ?> >Selectionnez ...</option>
                        <?php foreach ($docs as $n => $doc): ?>
                        <option value="<?php echo $doc->id_document ?>" <?php echo (($id_doc == $doc->id_document) ? 'selected' : '') ?> >Document <?php echo ($n+1) ?> - #<?php echo $doc->id_document ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>

        </div>
    </div>
</div>
