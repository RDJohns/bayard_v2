<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style>
    .m-b-1{
        margin-bottom: 1px !important;
    }
	.sel_erreur_ctrl.bs-container.open, .sel_motif_ko.bs-container.open{
        left: 30% !important;
        top: 50% !important;
    }
    .sel_erreur_ctrl.bs-container.open .dropdown-menu.open, .sel_motif_ko.bs-container.open .dropdown-menu.open{
        z-index: 1091;
    }
</style>

<div class="row">
	<div class="col-lg-12">

		<div class="row">
			<ol class="breadcrumb breadcrumb-bg-blue-grey align-center">
				<li><i class="material-icons">account_balance</i> <?php echo $pli->soc ?></li>
				<li><i class="material-icons">assignment</i> <?php echo $pli->typo ?></li>
				<li><i class="material-icons">attach_money</i> <?php echo $pli->grpaie ?></li>
				<li class="active">
                    <i class="material-icons">markunread</i> #
                    <?php echo $pli->id_pli ?>
                    <?php echo ($pli->date_courrier == '' ? '' : ' ['.(new DateTime($pli->date_courrier))->format('d/m/Y').']') ?>
                    <?php echo ($pli->recommande == 1 ? ' <small class="font-bold col-orange">[PRIO]</small>' : '') ?>
                </li>
			</ol>
		</div>
		
		<div class="row">

			<div class="col-md-8 col-lg-9 p-r-0">
				<div class="row">
					<div class="col-lg-6">
						<select id="sel_view_doc" class="form-control show-tick" onchange="show_doc();">
							<?php foreach ($docs as $i_doc => $doc): ?>
							<option class="opt_doc" value="<?php echo $doc->id_document ?>" <?php echo ($i_doc == 0 ? 'selected' : '') ?> >Document <?php echo ($i_doc+1) ?> - #<?php echo $doc->id_document ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-lg-6">
						<h4>/ <?php echo count($docs) ?> Document<?php echo (count($docs) > 1 ? 's' : '') ?></h4>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 p-r-0" id="view_doc">
					</div>
				</div>
			</div>

			<div class="col-md-4 col-lg-3 p-l-0" id="parti_field">
				
				<?php if(!is_null($motif_consigne)){ ?>
					<?php if ($nb_motif_consign > 1): ?>
                        <button type="button" class="btn btn-block bg-cyan btn-block btn-xs waves-effect" data-toggle="modal" data-target="#mdl_disp_all_motif_consigne_pli">
                            <i class="material-icons">info</i> <?php echo $nb_motif_consign ?> consignes ...
                        </button>
                    <?php endif; ?>
					<?php if(trim($motif_consigne->motif_operateur) != ''){ ?>
						<div class="alert alert-danger m-b-1">
							<strong>Motif de KO:</strong> <?php echo $motif_consigne->motif_operateur ?>
						</div>
					<?php } ?>
					<?php if(trim($motif_consigne->consigne_client) != ''){ ?>
						<div class="alert alert-info m-b-1">
							<strong>Consigne:</strong> <?php echo $motif_consigne->consigne_client ?>
						</div>
					<?php } ?>
				<?php } ?>
				
				<div class="card">
					<div class="header bg-grey">
						<h2>Donn&eacute;es du pli <!-- small>#<?php echo $pli->id_pli ?></small --></h2>
					</div>
					<div class="body p-l-10 p-r-5">
					
						<div class="row hidden">
							<div class="col-lg-12">
								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="field_idefix" value="<?php echo $pli->idefix ?>" />
										<label class="form-label">IDEFIX</label>
									</div>
								</div>
							</div>
						</div>
					
						<div class="row">
							<div class="col-lg-12">
								<label for="field_data_matrix">Infos datamatrix :</label>
								<div class="form-group">
									<div class="form-line">
										<textarea rows="1" class="form-control no-resize auto-growth field" id="field_data_matrix" placeholder="Appuyer sur la touche <Entr&eacute;e> pour aller &agrave; la ligne..."><?php echo $data_pli->infos_datamatrix ?></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="row hidden">
							<div class="col-lg-12">
								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control field_saisie_ctrl" id="field_promotion" value="<?php echo $data_pli->code_promotion ?>" nom_field="code_promo_pli" />
										<label class="form-label">Code promotion</label>
									</div>
								</div>
							</div>
						</div>

						<!-- <div class="row">
							<div class="col-lg-12">
								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="field_type_coupon" value="<?php //echo $data_pli->type_coupon ?>" />
										<label class="form-label">Type coupon</label>
									</div>
								</div>
							</div>
						</div> -->

						<div class="row">
							<div class="col-lg-12">
								<p>
									<b>Soci&eacute;t&eacute; :</b>
									<select class="form-control show-tick sel_field field" id="field_soc" onchange="set_titre();get_typologie();">
										<?php foreach ($socs as $soc): ?>
										<option value="<?php echo $soc->id ?>" <?php echo ($data_pli->societe == $soc->id ? 'selected' : '') ?>><?php echo $soc->nom_societe ?></option>
										<?php endforeach; ?>
									</select>
								</p>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
								<p>
									<b>Typologie :</b>
									<select class="form-control show-tick sel_field field" id="field_typologie" data-live-search="true" data-size="5">
										<?php foreach ($typos as $typo): ?>
										<option value="<?php echo $typo->id ?>" data-subtext=" - <?php echo $typo->sous_lot ?>" <?php echo ($pli->typologie == $typo->id ? 'selected' : '') ?>><?php echo $typo->typologie ?></option>
										<?php endforeach; ?>
									</select>
								</p>
							</div>
						</div>

						<div class="row hidden">
							<div class="col-lg-12">
								<p>
									<b>Titre :</b>
									<select class="form-control show-tick sel_field field" id="field_main_titre" data-live-search="true" disabled  data-size="5">
										<?php foreach ($titres as $titre): ?>
										<option value="<?php echo $titre->id ?>" title="<?php echo $titre->code ?>" data-subtext=" - <?php echo $titre->code ?>"><?php echo $titre->titre ?></option>
										<?php endforeach; ?>
									</select>
								</p>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control field_saisie_ctrl" id="field_code_ecole" value="<?php echo $data_pli->code_ecole_gci ?>" nom_field="code_ecole_gci_pli" />
										<label class="form-label">Code école/GCI</label>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
				
				<div class="card">
					<div class="header bg-grey">
						<h2>Mouvements <!-- small></small --></h2>
					</div>
					<div class="body p-l-10 p-r-5" id="main_card_mvmnt">
						<?php echo $view_mvmnts ?>
					</div>
				</div>

				<div class="card">
					<div class="header bg-grey">
						<h2>Paiements <!-- small></small --></h2>
					</div>
					<div class="body p-l-10 p-r-5" id="section_paiement">

						<div class="row">
							<div class="col-lg-12">
								<p>
									<b>Mode de paiement :</b>
									<select class="form-control show-tick sel_field field field_saisie_ctrl" id="field_mode_paiement" data-live-search="true" multiple onchange="analyse_paiement();" nom_field="mode_paiement" <?php echo ($mode_saisie_batch ? 'disabled' : '') ?> data-selected-text-format="count > 1" data-count-selected-text="{0} modes de paiement" >
										<?php foreach ($mode_paiements as $mode_paiement): ?>
										<option value="<?php echo $mode_paiement->id_mode_paiement ?>" <?php echo (in_array($mode_paiement->id_mode_paiement, $paiements) ? 'selected' : '') ?>><?php echo $mode_paiement->mode_paiement ?></option>
										<?php endforeach; ?>
									</select>
								</p>
							</div>
						</div>
						
						<div class="row" id="row_montant_espece" <?php echo (in_array('5', $mode_paiements) ? '' : 'style="display: none;"') ?> >
							<div class="col-lg-12">
								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control montant field_saisie_ctrl" id="field_montant_espece" value="<?php echo u_montant($montant_esp) ?>" nom_field="montant_espece" onchange="$(this).val(u_montant($(this).val()));" />
										<label class="form-label">Montant esp&egrave;ces </label>
									</div>
								</div>
							</div>
						</div>
						
						<?php echo $view_cheques ?>
						<?php echo $view_cheques_kd ?>
					</div>
				</div>

				<div class="card">
					<div class="header bg-grey">
						<h2>Statut <!-- small></small --></h2>
					</div>
					<div class="body p-l-10 p-r-5">

						<div class="row">
							<div class="col-lg-12">
								<p>
									<b>Statut pli :</b>
									<select class="form-control show-tick sel_field field" id="field_statut_saisie" onchange="manage_etat();">
										<?php foreach ($statut_ctrl as $statut): ?>
										<option value="<?php echo $statut->id_statut_saisie ?>" <?php echo ($status_in == $statut->id_statut_saisie ? 'selected' : '') ?> ><?php echo $statut->libelle ?></option>
										<?php endforeach; ?>
									</select>
								</p>
							</div>
						</div>
						
						<div class="row if_ko">
							<div class="col-lg-12">
								<p>
									<b>Motif KO/CI :</b>
									<select class="form-control show-tick sel_field field sel_motif_ko" id="field_motif_ko" data-live-search="true" data-size="5" onchange="" data-container="body">
										<?php foreach ($ko_motifs as $motif): ?>
										<option value="<?php echo $motif->id_motif ?>" <?php echo ($data_pli->motif_ko == $motif->id_motif ? 'selected' : '') ?> title="<?php echo character_limiter($motif->libelle_motif, 30) ?>" ><?php echo $motif->libelle_motif ?></option>
										<?php endforeach; ?>
									</select>
								</p>
							</div>
						</div>
						
						<div class="row if_ko_autre">
							<div class="col-lg-12">
								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="field_autre_motif_ko" value="<?php echo $data_pli->autre_motif_ko ?>" />
										<label class="form-label">Autre motif KO/CI</label>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<p>
									<b>Nom circulaire :</b>
									<select class="form-control show-tick sel_field field" id="field_nom_circulaire" data-live-search="true" data-size="5">
										<option value="-1">aucune circulaire</option>
										<?php foreach ($circulaires as $circulaire): ?>
										<option value="<?php echo $circulaire->id ?>" <?php echo ($data_pli->nom_circulaire == $circulaire->id ? 'selected' : '') ?> ><?php echo $circulaire->circulaire ?></option>
										<?php endforeach; ?>
									</select>
								</p>
							</div>
						</div>
						
						<div class="row hidden">
							<div class="col-lg-12">
								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="field_nom_deleg" value="<?php echo $data_pli->nom_deleg ?>" />
										<label class="form-label">Nom d&eacute;l&eacute;gu&eacute;</label>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row hidden_"><!-- NOTE fichier CI -->
							<div class="col-lg-12">
								<label for="">Fichier circulaire</label>
								<div class="form-group">
									<?php if ($with_f_circ): ?>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 p-r-0">
											<p class="font-italic font-bold col-pink align-center font-15" id="current_f_circ" title="<?php echo $nom_f_circulaire ?>"><i class="material-icons">insert_drive_file</i><?php echo ellipsize($nom_f_circulaire, 20, .5) ?></p>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 align-center p-l-0">
											<a href="<?php echo $f_circulaire ?>" download="" type="button" class="btn bg-light-blue btn-circle waves-effect waves-circle waves-float" title="t&eacute;l&eacute;charger">
												<i class="material-icons">file_download</i>
											</a>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 align-center p-l-0">
											<button type="button" class="btn bg-light-green btn-circle waves-effect waves-circle waves-float" onclick="restore_f_circ();" title="Restaurer">
												<i class="material-icons">restore</i>
											</button>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 align-center p-l-0">
											<button type="button" class="btn bg-red btn-circle waves-effect waves-circle waves-float" onclick="delete_f_circ();" title="Supprimer">
												<i class="material-icons">delete</i>
											</button>
										</div>
									</div>
									<?php endif; ?>
									<div class="row">
										<div class="col-xs-8 col-sm-9 col-md-10 col-xs-10 p-r-0">
											<div class="form-line">
												<form id="form_fichier_circulaire" method="" action="" enctype="multipart/form-data">
													<input type="file" id="field_fichier_circulaire" name="fichier_circulaire" class="form-control field_file" onchange="select_f_circ();">
												</form>
											</div>
										</div>
										<div class="col-xs-4 col-sm-3 col-md-2 col-lg-2 align-center p-l-0">
											<button type="button" class="btn bg-red btn-circle waves-effect waves-circle waves-float" onclick="clear_f_circ();" title="Effacer">
												<i class="material-icons">clear</i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<input type="checkbox" id="field_msg_ind" class="filled-in chk-col-red" <?php echo ($data_pli->message_indechiffrable == 1 ? 'checked' : '') ?> />
								<label for="field_msg_ind">Annotation</label>
							</div>
							<div class="col-lg-12 hidden">
								<input type="checkbox" id="field_dmd_kdo" class="filled-in chk-col-red" <?php echo ($data_pli->dmd_kdo_fidelite_prim_suppl == 1 ? 'checked' : '') ?> />
								<label for="field_dmd_kdo">Demande cadeau de fid&eacute;lit&eacute; ou prime suppl&eacute;mentaire</label>
							</div>
							<div class="col-lg-12 hidden">
								<input type="checkbox" id="field_dmd_env_kdo" class="filled-in chk-col-red" <?php echo ($data_pli->dmd_envoi_kdo_adrss_diff == 1 ? 'checked' : '') ?> />
								<label for="field_dmd_env_kdo">Demande d'envoi du cadeau &agrave; une adresse diff&eacute;rente</label>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<p>
									<b>Image &agrave; d&eacute;sarchiver pour le pli en cours :</b>
									<select id="field_img_desar" class="form-control show-tick sel_field field" multiple data-selected-text-format="count > 1" data-count-selected-text="{0} Images">
										<?php foreach ($docs as $i_doc => $doc): ?>
										<option value="<?php echo $doc->id_document ?>" <?php echo ($doc->desarchive == 1 ? 'selected' : '') ?> >Document <?php echo ($i_doc+1) ?> - #<?php echo $doc->id_document ?></option>
										<?php endforeach; ?>
									</select>
								</p>
							</div>
						</div>
					
					</div>
				</div>

				<div class="card">
					<div class="header bg-grey">
						<h2>Contr&ocirc;le</h2>
					</div>
					<div class="body">

						<div class="row">
							<div class="col-lg-12">
								<p>
									<b>Cat&eacute;gorie d’erreur :</b>
									<select class="form-control show-tick sel_field field sel_erreur_ctrl" multiple id="field_erreur" data-live-search="true" data-size="5" data-selected-text-format="count > 1" data-count-selected-text="{0} erreurs" data-container="body">
										<?php foreach ($categ_erreurs as $n => $erreur): ?>
										<option value="<?php echo $erreur->id_erreur ?>" data-subtext="<?php echo $erreur->description ?>" ><?php echo $erreur->erreur ?></option>
										<?php endforeach; ?>
									</select>
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<label for="field_comm_erreur">Commentaire :</label>
								<div class="form-group">
									<div class="form-line">
										<textarea rows="1" class="form-control no-resize auto-growth field" id="field_comm_erreur" placeholder="Appuyer sur la touche <Entr&eacute;e> pour aller &agrave; la ligne..."></textarea>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="row direct_only">
					<div class="col-lg-6 align-center p-r-0 p-l-0">
						<button type="button" class="btn btn-block bg-deep-orange btn-lg waves-effect" onclick="enregistrer();">
							<i class="material-icons">save</i>
							<span>Enregistrer</span>
						</button>
					</div>
					<div class="col-lg-6 align-center p-r-0 p-l-0">
						<button type="button" class="btn btn-block bg-green btn-lg waves-effect" onclick="annuler();">
							<i class="material-icons">cancel</i>
							<span>Annuler</span>
						</button>
					</div>
				</div>

				<div class="row batch_only">
					<div class="col-lg-4 align-center p-r-0 p-l-0">
						<button type="button" class="btn btn-block bg-deep-orange btn-lg waves-effect" onclick="enregistrer();">
							<i class="material-icons">save</i>
							<span>Enregistrer</span>
						</button>
					</div>
					<div class="col-lg-4 align-center p-r-0 p-l-0">
						<button type="button" class="btn btn-block bg-green btn-lg waves-effect" onclick="annuler();">
							<i class="material-icons">cancel</i>
							<span>Annuler</span>
						</button>
					</div>
					<div class="col-lg-4 align-center p-r-0 p-l-0">
						<button type="button" class="btn btn-block bg-purple btn-lg waves-effect" onclick="mode_advantage();" title="Passer en mode saisie directe advantage">
							<i class="material-icons">call_split</i>
							<span>Mode Advantage</span>
						</button>
					</div>
				</div>

			</div>

		</div>

	</div>
</div>

<div class="modal fade" id="mdl_disp_all_motif_consigne_pli" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-lg-8">
                        <h4 class="modal-title">Motif-KO et consignes</h4>
                    </div>
                    <div class="col-lg-4 align-right">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                    </div>
                </div>
            </div>
            <div class="modal-body" style="max-height: 60vh; overflow-y: auto;">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($nb_motif_consign > 0): ?>
                            <div class="body table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Motif du KO</th>
                                            <th>Consignes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($motif_consignes as $key => $mtf_c): ?>
                                            <tr>
                                                <td><?php echo $mtf_c->motif_operateur ?><br><small><?php echo ($mtf_c->dt_motif_ko == '' ? '' : (new DateTime($mtf_c->dt_motif_ko))->format('d/m/Y G:i:s')) ?></small></td>
                                                <td><?php echo $mtf_c->consigne_client ?><br><small><?php echo ($mtf_c->dt_consigne == '' ? '' : (new DateTime($mtf_c->dt_consigne))->format('d/m/Y G:i:s')) ?></small></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php else: ?>
                            <div class="alert alert-info">
                                <strong>0</strong> motif-KO ou consigne.
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	ID_PLI = '<?php echo $pli->id_pli ?>';
	MODE_SAISIE_BATCH = <?php echo ($mode_saisie_batch ? 'true' : 'false') ?>;
	$(function(){
		showNotification_persiste('alert-info','<i class="material-icons">info_outline</i>MODE SAISIE <?php echo ($mode_saisie_batch ? 'BATCH' : 'ADVANTAGE') ?>', 'top', 'center', null, null);
		<?php if(!is_null($motif_consigne)){ ?>
			swal("Attention!", "Présence de consigne de traitement", "warning");
		<?php } ?>
    });
</script>
