<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row p-t-100">
    <div class="col-xs-12 align-center">
        <div class="alert bg-orange">
            <?php echo $message ?>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 align-right">
        <p class="align-left"><b>Typologie:</b></p>
        <select class="form-control show-tick sel_field" id="sel_traitement_ctrl_init"  data-live-search="true" data-container="body" data-size="5">
            <option value="-1" <?php echo ($trtmt_ctrl == -1 ? 'selected' : '') ?>>Toutes</option>
            <?php foreach ($filtres->typos as $typo): ?>
                <option value="<?php echo $typo->id ?>" <?php echo ($typo->id == $trtmt_ctrl ? 'selected' : '') ?> data-subtext="<?php echo $typo->nb.' plis' ?>" ><?php echo $typo->typologie ?></option>
            <?php endforeach; ?>
        </select>
    </div>
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 align-right">
        <p class="align-left"><b>Saisi par:</b></p>
        <select class="form-control show-tick sel_field" id="sel_user_ctrl_init"  data-live-search="true" data-container="body" data-size="5">
            <option value="-1" <?php echo ($user_ctrl == -1 ? 'selected' : '') ?>>Tous</option>
            <?php foreach ($filtres->users as $user): ?>
                <option value="<?php echo $user->id_utilisateur ?>" <?php echo ($user->id_utilisateur == $user_ctrl ? 'selected' : '') ?> ><?php echo $user->login.' ('.$user->matr_gpao.')' ?></option>
            <?php endforeach; ?>
        </select>
    </div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 align-center">
		<button type="button" class="btn bg-blue btn-lg waves-effect m-t-20" onclick="load_pli();">
            <i class="material-icons">assignment_returned</i>
            <span><?php echo $lbl_bouton ?></span>
        </button>
	</div>
</div>
