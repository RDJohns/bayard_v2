<?php defined('BASEPATH') OR exit('No direct script access allowed');
    if(!isset($identifiant)){
        $identifiant = 'mvmnt_'.date('U').mt_rand().mt_rand();                                        
    };
?>

<div class="card grp_field_mvmnt" id="grp_field_mvmnt_<?php echo $identifiant ?>" my_ident="<?php echo $identifiant ?>" >
    <div class="header bg-light-green">
        <h2>Mouvement <small>#<span class="numerotation_mvmnt"></span></small></h2>
        <ul class="header-dropdown m-r--5">
            <li>
                <a href="javascript:get_view_mvmnt();">
                    <i class="material-icons">add</i>
                </a>
            </li>
            <li class="clear_mvmnt">
                <a href="javascript:suppr_gr_field_mvmnt('#grp_field_mvmnt_<?php echo $identifiant ?>');">
                    <i class="material-icons">clear</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="body">

        <div class="row">

            <div class="col-lg-12">
                <p>
                    <b>Titre :</b>
                    <select class="form-control show-tick sel_field field sel_field_mvmnt field_saisie_ctrl" id="field_titre_mvmnt_<?php echo $identifiant ?>" data-live-search="true" data-size="5" onchange="numerotation_mvmnt();" nom_field="titre_mouvement">
                        <?php foreach ($titres as $titre): ?>
                        <option value="<?php echo $titre->id ?>" <?php echo ($ttr == $titre->id ? 'selected' : '') ?> title="<?php echo $titre->code ?>" data-subtext=" - <?php echo $titre->code ?>"><?php echo $titre->titre ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>
            <?php if($is_batch): ?>
            <div class="col-lg-12 batch_only">
                <div class="form-group input-group spinner" data-trigger="spinner">
                    <span class="input-group-addon">Quantit&eacute;:</span>
                    <div class="form-line">
                        <input type="text" class="form-control text-center field field_mvmnt oblig field_saisie_ctrl" data-rule="quantity" id="field_qunatity_nb_<?php echo $identifiant ?>" value="<?php echo (isset($batch->qty_ord) ? (is_numeric($batch->qty_ord) ? $batch->qty_ord : '1') : '1') ?>" nom_field="quantite_mouvement" />
                        <!-- <label class="form-label">Quantit&eacute;/Nombre</label> -->
                    </div>
                    <span class="input-group-addon">
                        <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                    </span>
                </div>
            </div>
            <?php endif; ?>

            <div class="col-lg-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control field field_mvmnt oblig field_saisie_ctrl" id="field_code_promo_<?php echo $identifiant ?>" value="<?php echo $code_promo ?>" onchange="load_from_data_promo($(this), 'sel_child_promo_<?php echo $identifiant ?>', 'field_code_promo_<?php echo $identifiant ?>', 'field_code_promo_choix_mvmnt_<?php echo $identifiant ?>', 'field_code_promo_produit_mvmnt_<?php echo $identifiant ?>');" nom_field="code_promo_mouvement" />
                        <label class="form-label">Code promotion</label>
                    </div>
                </div>
            </div>
            <?php if($is_batch): ?>
            <div class="col-lg-12 batch_only">
                <p>
                    <b>Code choix :</b>
                    <select class="form-control show-tick sel_field field sel_field_mvmnt sel_child_promo_<?php echo $identifiant ?> promo_choix field_saisie_ctrl" id="field_code_promo_choix_mvmnt_<?php echo $identifiant ?>" data-live-search="true" data-size="5" nom_field="code_choix_mouvement" data-container="body" onchange="load_from_data_promo_art('field_code_promo_<?php echo $identifiant ?>', 'field_code_promo_choix_mvmnt_<?php echo $identifiant ?>', 'field_code_promo_produit_mvmnt_<?php echo $identifiant ?>');" >
                        <?php foreach ($code_promos as $k_promo_choix => $promo_choix): ?>
                        <option value="<?php echo $promo_choix->code_choix_promo ?>" <?php echo (isset($batch->pmo_chc) ? ($promo_choix->code_choix_promo == $batch->pmo_chc ? 'selected' : '') : ($k_promo_choix == 0 ? 'selected' : '')) ?> data-subtext=" - <?php echo $promo_choix->code_choix_promo.' - '.$promo_choix->code_art ?>"><?php echo $promo_choix->lib_choix ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>
            <div class="col-lg-12 batch_only">
                <p>
                    <b>Code produit :</b>
                    <select class="form-control show-tick sel_field field sel_field_mvmnt sel_child_promo_<?php echo $identifiant ?> promo_produit field_saisie_ctrl" id="field_code_promo_produit_mvmnt_<?php echo $identifiant ?>" data-live-search="true" data-size="5" nom_field="code_produit_mouvement">
                        <?php foreach ($code_promos_art as $k_promo_produit => $promo_produit): ?>
                        <option value="<?php echo $promo_produit->code_art ?>" <?php echo (isset($batch->itm_num) ? ($promo_produit->code_art == $batch->itm_num ? 'selected' : '') : ($k_promo_produit == 0 ? 'selected' : '')) ?> data-subtext=" - <?php echo $promo_produit->code_choix_promo ?>" ><?php echo $promo_produit->code_art ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>
            <?php endif; ?>

            <div class="col-lg-12">
                <div class="input-group">
                    <span class="input-group-addon">#Abonn&eacute: </span>
                    <div class="form-line">
                        <input type="text" class="form-control field field_mvmnt nb oblig_ field_saisie_ctrl" id="field_num_abon_<?php echo $identifiant ?>" value="<?php echo $num_abonne ?>" my_twin="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_ctm_nbr" onkeyup="maj_twin($(this));" onchange="maj_twin($(this));" nom_field="num_abonne_mouvement" />
                        <!-- <label class="form-label">Numéro de l'abonné</label> -->
                    </div>
                    <?php if($is_batch): ?>
                    <span class="input-group-addon batch_only">
                        <button type="button" class="btn btn-default waves-effect" title="Charger depuis la base d'adresse client" data-toggle="modal" data-target="#mdl_mvmnt_abonne_<?php echo $identifiant ?>">
                            <i class="material-icons">create</i>
                        </button>
                    </span>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-lg-12 direct_only">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control field field_mvmnt oblig_ field_saisie_ctrl" id="field_nom_abon_<?php echo $identifiant ?>" value="<?php echo (isset($batch->atn_end) ? $batch->atn_end : $nom_abonne) ?>" nom_field="nom_abonne_mouvement" />
                        <label class="form-label">Nom de l'abonné</label>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="input-group">
                    <span class="input-group-addon">#Payeur: </span>
                    <div class="form-line">
                        <input type="text" class="form-control field field_mvmnt nb oblig_ field_saisie_ctrl" id="field_num_payeur_<?php echo $identifiant ?>" value="<?php echo $num_payeur ?>" my_twin="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_ctm_nbr" onkeyup="maj_twin($(this));" onchange="maj_twin($(this));" nom_field="num_payeur_mouvement" />
                        <!-- <label class="form-label">Numéro du payeur</label> -->
                    </div>
                    <?php if($is_batch): ?>
                    <span class="input-group-addon batch_only">
                        <button type="button" class="btn btn-default waves-effect" title="Charger depuis la base d'adresse client" data-toggle="modal" data-target="#mdl_mvmnt_payeur_<?php echo $identifiant ?>">
                            <i class="material-icons">create</i>
                        </button>
                    </span>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-lg-12 direct_only">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control field field_mvmnt oblig_ field_saisie_ctrl" id="field_nom_payeur_<?php echo $identifiant ?>" value="<?php echo (isset($batch->datn_end) ? $batch->datn_end : $nom_payeur) ?>" nom_field="nom_payeur_mouvement" />
                        <label class="form-label">Nom du payeur</label>
                    </div>
                </div>
            </div>

            <?php if($is_batch): ?>
            <div class="modal fade" id="mdl_mvmnt_payeur_<?php echo $identifiant ?>" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document" style="margin-top: 10px !important; margin-right: 5px !important;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Payeur</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?> nb" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_ctm_nbr" my_code="ctm_nbr" value="<?php echo $num_payeur ?>" my_twin="field_num_payeur_<?php echo $identifiant ?>" onkeyup="maj_twin($(this));" onchange="maj_twin($(this));" my_field_addr="field_adress_payeur_<?php echo $identifiant ?>" />
                                            <!-- <label class="form-label">Numéro</label> -->
                                        </div>
                                        <span class="input-group-addon">
                                            <button type="button" class="btn btn-default waves-effect" title="Charger depuis la base d'adresse client" onclick="load_from_data_addr_client('field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_', 'field_mvmnt_mdl_payeur_<?php echo $identifiant ?>');">
                                                <i class="material-icons">system_update_alt</i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12 align-right">
                                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal"><i class="material-icons">done</i>OK</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_ctm_ttl" my_code="ctm_ttl" value="<?php echo (isset($batch->dctm_ttl) ? $batch->dctm_ttl : '') ?>" />
                                            <label class="form-label">Civilité</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_atn_end" my_code="atn_end" value="<?php echo (isset($batch->datn_end) ? $batch->datn_end : '') ?>" />
                                            <label class="form-label">Nom</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_atn_1st" my_code="atn_1st" value="<?php echo (isset($batch->datn_1st) ? $batch->datn_1st : '') ?>" />
                                            <label class="form-label">Prénom</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_cmp_nme" my_code="cmp_nme" value="<?php echo (isset($batch->dcmp_nme) ? $batch->dcmp_nme : '') ?>" />
                                            <label class="form-label">Raison sociale</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_dpt_nme" my_code="dpt_nme" value="<?php echo (isset($batch->ddpt_nme) ? $batch->ddpt_nme : '') ?>" />
                                            <label class="form-label">Complément Raison sociale</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?> field_adress_payeur_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_str_1st" my_code="str_1st" value="<?php echo (isset($batch->dstr_1st) ? $batch->dstr_1st : '') ?>" />
                                            <label class="form-label">Complément de Voie</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?> field_adress_payeur_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_str_2nd" my_code="str_2nd" value="<?php echo (isset($batch->dstr_2nd) ? $batch->dstr_2nd : '') ?>" />
                                            <label class="form-label">Voie</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?> field_adress_payeur_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_str_3rd" my_code="str_3rd" value="<?php echo (isset($batch->dstr_3rd) ? $batch->dstr_3rd : '') ?>" />
                                            <label class="form-label">Lieu Dit / Boite Postale</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?> field_adress_payeur_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_zip_cde" my_code="zip_cde" value="<?php echo (isset($batch->dzip_cde) ? $batch->dzip_cde : '') ?>" />
                                            <label class="form-label">Code Postal</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?> field_adress_payeur_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_ctm_ste" my_code="ctm_ste" value="<?php echo (isset($batch->dctm_ste) ? $batch->dctm_ste : '') ?>" />
                                            <label class="form-label">Code Etat</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?> field_adress_payeur_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_ctm_cty" my_code="ctm_cty" value="<?php echo (isset($batch->dctm_cty) ? $batch->dctm_cty : '') ?>" />
                                            <label class="form-label">Ville</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?> field_adress_payeur_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_cun_typ" my_code="cun_typ" value="<?php echo (isset($batch->dcun_typ) ? $batch->dcun_typ : '') ?>" />
                                            <label class="form-label">Code Pays</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_pho_nbr" my_code="pho_nbr" value="<?php echo (isset($batch->dpho_nbr) ? $batch->dpho_nbr : '') ?>" />
                                            <label class="form-label">Téléphone Domicile</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_pho_nbr2" my_code="pho_nbr2" value="<?php echo (isset($batch->dpho_nbr2) ? $batch->dpho_nbr2 : '') ?>" />
                                            <label class="form-label">Téléphone Bureau</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_pho_nbr3" my_code="pho_nbr3" value="<?php echo (isset($batch->dpho_nbr3) ? $batch->dpho_nbr3 : '') ?>" />
                                            <label class="form-label">Téléphone Portable</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_bir_dte" my_code="bir_dte" value="<?php echo (isset($batch->dbir_dte) ? $batch->dbir_dte : '') ?>" />
                                            <label class="form-label">Date de Naissance AAAAMMJJ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Code Sexe :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_sex_cde" my_code="sex_cde">
                                            <option value="" <?php echo (isset($batch->dsex_cde) ? ($batch->dsex_cde == '' ? 'selected' : '') : 'selected') ?> title="Inconnu" data-subtext="Inconnu"></option>
                                            <option value="F" <?php echo (isset($batch->dsex_cde) ? ($batch->dsex_cde == 'F' ? 'selected' : '') : '') ?> title="Femme">Femme</option>
                                            <option value="M" <?php echo (isset($batch->dsex_cde) ? ($batch->dsex_cde == 'M' ? 'selected' : '') : '') ?> title="Homme">Homme</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_adr_emal" my_code="adr_emal" value="<?php echo (isset($batch->dadr_email) ? $batch->dadr_email : '') ?>" />
                                            <label class="form-label">Adresse Email</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection Adresse :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_pmo_adr" my_code="pmo_adr">
                                            <option value="" <?php echo (!isset($batch->dpmo_adr) || $batch->dpmo_adr == '' || is_null($batch->dpmo_adr) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->dpmo_adr) ? ($batch->dpmo_adr == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->dpmo_adr) ? ($batch->dpmo_adr == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->dpmo_adr) ? ($batch->dpmo_adr == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->dpmo_adr) ? ($batch->dpmo_adr == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection Téléphone (les 3) :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_pmo_phn" my_code="pmo_phn">
                                            <option value="" <?php echo (!isset($batch->dpmo_pho) || $batch->dpmo_pho == '' || is_null($batch->dpmo_pho) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->dpmo_pho) ? ($batch->dpmo_pho == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->dpmo_pho) ? ($batch->dpmo_pho == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->dpmo_pho) ? ($batch->dpmo_pho == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->dpmo_pho) ? ($batch->dpmo_pho == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection Fax :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_pmo_fax" my_code="pmo_fax">
                                            <option value="" <?php echo (!isset($batch->dpmo_fax) || $batch->dpmo_fax == '' || is_null($batch->dpmo_fax) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->dpmo_fax) ? ($batch->dpmo_fax == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->dpmo_fax) ? ($batch->dpmo_fax == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->dpmo_fax) ? ($batch->dpmo_fax == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->dpmo_fax) ? ($batch->dpmo_fax == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection Email :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_pmo_eml" my_code="pmo_eml">
                                            <option value="" <?php echo (!isset($batch->dpmo_eml) || $batch->dpmo_eml == '' || is_null($batch->dpmo_eml) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->dpmo_eml) ? ($batch->dpmo_eml == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->dpmo_eml) ? ($batch->dpmo_eml == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->dpmo_eml) ? ($batch->dpmo_eml == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->dpmo_eml) ? ($batch->dpmo_eml == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_promo" my_code="promo">
                                            <option value="" <?php echo (!isset($batch->dpromo) || $batch->dpromo == '' || is_null($batch->dpromo) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->dpromo) ? ($batch->dpromo == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->dpromo) ? ($batch->dpromo == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->dpromo) ? ($batch->dpromo == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->dpromo) ? ($batch->dpromo == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection SMS :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_payeur_<?php echo $identifiant ?>" id="field_mvmnt_mdl_payeur_<?php echo $identifiant ?>_pmo_sms" my_code="pmo_sms">
                                            <option value="" <?php echo (!isset($batch->dpmo_sms) || $batch->dpmo_sms == '' || is_null($batch->dpmo_sms) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->dpmo_sms) ? ($batch->dpmo_sms == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->dpmo_sms) ? ($batch->dpmo_sms == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->dpmo_sms) ? ($batch->dpmo_sms == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->dpmo_sms) ? ($batch->dpmo_sms == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="mdl_mvmnt_abonne_<?php echo $identifiant ?>" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document" style="margin-top: 10px !important; margin-right: 5px !important;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Abonn&eacute;</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?> nb" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_ctm_nbr" my_code="ctm_nbr" value="<?php echo $num_abonne ?>" my_twin="field_num_abon_<?php echo $identifiant ?>" onkeyup="maj_twin($(this));" onchange="maj_twin($(this));" my_field_addr="field_adress_abonne_<?php echo $identifiant ?>" />
                                            <!-- <label class="form-label">Numéro</label> -->
                                        </div>
                                        <span class="input-group-addon">
                                            <button type="button" class="btn btn-default waves-effect" title="Charger depuis la base d'adresse client" onclick="load_from_data_addr_client('field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_', 'field_mvmnt_mdl_abonne_<?php echo $identifiant ?>');">
                                                <i class="material-icons">system_update_alt</i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12 align-right">
                                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal"><i class="material-icons">done</i>OK</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_ctm_ttl" my_code="ctm_ttl" value="<?php echo (isset($batch->ctm_ttl) ? $batch->ctm_ttl : '') ?>" />
                                            <label class="form-label">Civilité</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_atn_end" my_code="atn_end" value="<?php echo (isset($batch->atn_end) ? $batch->atn_end : '') ?>" />
                                            <label class="form-label">Nom</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_atn_1st" my_code="atn_1st" value="<?php echo (isset($batch->atn_1st) ? $batch->atn_1st : '') ?>" />
                                            <label class="form-label">Prénom</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_cmp_nme" my_code="cmp_nme" value="<?php echo (isset($batch->cmp_nme) ? $batch->cmp_nme : '') ?>" />
                                            <label class="form-label">Raison sociale</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_dpt_nme" my_code="dpt_nme" value="<?php echo (isset($batch->dpt_nme) ? $batch->dpt_nme : '') ?>" />
                                            <label class="form-label">Complément Raison sociale</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?> field_adress_abonne_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_str_1st" my_code="str_1st" value="<?php echo (isset($batch->str_1st) ? $batch->str_1st : '') ?>" />
                                            <label class="form-label">Complément de Voie</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?> field_adress_abonne_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_str_2nd" my_code="str_2nd" value="<?php echo (isset($batch->str_2nd) ? $batch->str_2nd : '') ?>" />
                                            <label class="form-label">Voie</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?> field_adress_abonne_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_str_3rd" my_code="str_3rd" value="<?php echo (isset($batch->str_3rd) ? $batch->str_3rd : '') ?>" />
                                            <label class="form-label">Lieu Dit / Boite Postale</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?> field_adress_abonne_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_zip_cde" my_code="zip_cde" value="<?php echo (isset($batch->zip_cde) ? $batch->zip_cde : '') ?>" />
                                            <label class="form-label">Code Postal</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?> field_adress_abonne_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_ctm_ste" my_code="ctm_ste" value="<?php echo (isset($batch->ctm_ste) ? $batch->ctm_ste : '') ?>" />
                                            <label class="form-label">Code Etat</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?> field_adress_abonne_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_ctm_cty" my_code="ctm_cty" value="<?php echo (isset($batch->ctm_cty) ? $batch->ctm_cty : '') ?>" />
                                            <label class="form-label">Ville</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?> field_adress_abonne_<?php echo $identifiant ?> field_adress_mdl" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_cun_typ" my_code="cun_typ" value="<?php echo (isset($batch->cun_typ) ? $batch->cun_typ : '') ?>" />
                                            <label class="form-label">Code Pays</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_pho_nbr" my_code="pho_nbr" value="<?php echo (isset($batch->pho_nbr) ? $batch->pho_nbr : '') ?>" />
                                            <label class="form-label">Téléphone Domicile</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_pho_nbr2" my_code="pho_nbr2" value="<?php echo (isset($batch->pho_nbr2) ? $batch->pho_nbr2 : '') ?>" />
                                            <label class="form-label">Téléphone Bureau</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_pho_nbr3" my_code="pho_nbr3" value="<?php echo (isset($batch->pho_nbr3) ? $batch->pho_nbr3 : '') ?>" />
                                            <label class="form-label">Téléphone Portable</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_bir_dte" my_code="bir_dte" value="<?php echo (isset($batch->bir_dte) ? $batch->bir_dte : '') ?>" />
                                            <label class="form-label">Date de Naissance AAAAMMJJ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Code Sexe :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_sex_cde" my_code="sex_cde">
                                            <option value="" <?php echo (isset($batch->sex_cde) ? ($batch->sex_cde == '' ? 'selected' : '') : 'selected') ?> title="Inconnu" data-subtext="Inconnu"></option>
                                            <option value="F" <?php echo (isset($batch->sex_cde) ? ($batch->sex_cde == 'F' ? 'selected' : '') : '') ?> title="Femme">Femme</option>
                                            <option value="M" <?php echo (isset($batch->sex_cde) ? ($batch->sex_cde == 'M' ? 'selected' : '') : '') ?> title="Homme">Homme</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group  form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control field field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_adr_emal" my_code="adr_emal" value="<?php echo (isset($batch->adr_email) ? $batch->adr_email : '') ?>" />
                                            <label class="form-label">Adresse Email</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection Adresse :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_pmo_adr" my_code="pmo_adr">
                                            <option value="" <?php echo (!isset($batch->pmo_adr) || $batch->pmo_adr == '' || is_null($batch->pmo_adr) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->pmo_adr) ? ($batch->pmo_adr == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->pmo_adr) ? ($batch->pmo_adr == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->pmo_adr) ? ($batch->pmo_adr == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->pmo_adr) ? ($batch->pmo_adr == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection Téléphone (les 3) :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_pmo_phn" my_code="pmo_phn">
                                            <option value="" <?php echo (!isset($batch->pmo_pho) || $batch->pmo_pho == '' || is_null($batch->pmo_pho) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->pmo_pho) ? ($batch->pmo_pho == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->pmo_pho) ? ($batch->pmo_pho == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->pmo_pho) ? ($batch->pmo_pho == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->pmo_pho) ? ($batch->pmo_pho == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection Fax :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_pmo_fax" my_code="pmo_fax">
                                            <option value="" <?php echo (!isset($batch->pmo_fax) || $batch->pmo_fax == '' || is_null($batch->pmo_fax) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->pmo_fax) ? ($batch->pmo_fax == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->pmo_fax) ? ($batch->pmo_fax == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->pmo_fax) ? ($batch->pmo_fax == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->pmo_fax) ? ($batch->pmo_fax == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection Email :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_pmo_eml" my_code="pmo_eml">
                                            <option value="" <?php echo (!isset($batch->pmo_eml) || $batch->pmo_eml == '' || is_null($batch->pmo_eml) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->pmo_eml) ? ($batch->pmo_eml == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->pmo_eml) ? ($batch->pmo_eml == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->pmo_eml) ? ($batch->pmo_eml == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->pmo_eml) ? ($batch->pmo_eml == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_promo" my_code="promo">
                                            <option value="" <?php echo (!isset($batch->promo) || $batch->promo == '' || is_null($batch->promo) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->promo) ? ($batch->promo == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->promo) ? ($batch->promo == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->promo) ? ($batch->promo == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->promo) ? ($batch->promo == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <p>
                                        <b>Top prospection SMS :</b>
                                        <select class="form-control show-tick sel_field field sel_field_mvmnt_mdl field_mvmnt_mdl field_mvmnt_mdl_abonne_<?php echo $identifiant ?>" id="field_mvmnt_mdl_abonne_<?php echo $identifiant ?>_pmo_sms" my_code="pmo_sms">
                                            <option value="" <?php echo (!isset($batch->pmo_sms) || $batch->pmo_sms == '' || is_null($batch->pmo_sms) ? 'selected' : '') ?> title="Aucune valeur" data-subtext="">Aucune valeur</option>
                                            <option value="Y" <?php echo (isset($batch->pmo_sms) ? ($batch->pmo_sms == 'Y' ? 'selected' : '') : '') ?> title="prospctable sans restriction" data-subtext=" - Y">prospctable sans restriction</option>
                                            <option value="N" <?php echo (isset($batch->pmo_sms) ? ($batch->pmo_sms == 'N' ? 'selected' : '') : '') ?> title="non prospectable" data-subtext=" - N">non prospectable</option>
                                            <option value="I" <?php echo (isset($batch->pmo_sms) ? ($batch->pmo_sms == 'I' ? 'selected' : '') : '') ?> title="prospectable seulement ien interne" data-subtext=" - I">prospectable seulement ien interne</option>
                                            <option value="E" <?php echo (isset($batch->pmo_sms) ? ($batch->pmo_sms == 'E' ? 'selected' : '') : '') ?> title="prospectable seulement externe" data-subtext=" - E">prospectable seulement externe</option>
                                        </select>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>

        </div>

    </div>
</div>

<style>
    .promo_choix.bs-container.open{
        left: 25% !important;
        /*top: 80% !important;*/
    }
    .promo_choix.bs-container.open .dropdown-menu.open{
        z-index: 1091;
    }
</style>
