<br/><br/><br/>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
    <div class="card unlock" >
        <div class="body">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 0px !important;">
                    <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-primary">
                            <div class="panel-heading" role="tab" id="headingOne_1">
                                <h5 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="true" aria-controls="collapseOne_1" class="">
                                        Filtre pour la recherche
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_1" aria-expanded="true" style="">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <p>
                                                <b>#id pli : </b>
                                            </p>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" id="dev-id-pli">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <p>
                                                <b>Typé par : </b>
                                            </p>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" id="dev-typage-par">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <p>
                                                <b>Saisie par : </b>
                                            </p>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" id="dev-saisie-par">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <p>
                                                <b>Contrôlé par : </b>
                                            </p>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" id="dev-ctrl-par">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <br><button type="button" class="btn bg-blue waves-effect" onclick="recherchePli();">
                                                    <i class="material-icons">search</i>
                                                    <span>Appliquer</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row clearfix">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <div class="table-responsive">
                        <table class="display nowrap table-bordered table-striped table-hover deverouiller">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Etat</th>
                                <th>Typé par</th>
                                <th>Saisie par</th>
                                <th>Contrôlé par</th>
                                <th>Action</th>

                            </tr>
                            </thead>

                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
