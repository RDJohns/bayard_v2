<div class="modal fade" id="unlockModal" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Déverouillage pli #<span id="pli-unlock"></span></h4>
                <input type="hidden" name="" id="id-pli-dev">
            </div>
            <div class="modal-body">
                <div class="button-demo">
                    <button type="button" class="btn bg-indigo waves-effect dev dev-typage">Déverouiller typage</button>
                    <button type="button" class="btn bg-blue waves-effect dev dev-saisie">Déverouiller saisie</button>
                    <button type="button" class="btn bg-blue-grey waves-effect dev dev-ctrl">Déverouiller contrôle</button>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  waves-effect" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>