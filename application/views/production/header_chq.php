<?php defined('BASEPATH') OR exit('No direct script access allowed');
    $url_src = base_url().'src/';
?>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>GED BAYARD</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('bootstrap.min'); ?>">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('font-awesome.min'); ?>">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('owl.carousel'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('owl.theme'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('owl.transitions'); ?>">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('animate'); ?>">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('normalize'); ?>">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('meanmenu.min'); ?>">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('main'); ?>">
    <!-- educate icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('educate-custon-icon'); ?>">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('morrisjs/morris'); ?>">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('scrollbar/jquery.mCustomScrollbar.min'); ?>">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('metisMenu/metisMenu.min'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('metisMenu/metisMenu-vertical'); ?>">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('calendar/fullcalendar.min'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('calendar/fullcalendar.print.min'); ?>">
    <!-- touchspin CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('touchspin/jquery.bootstrap-touchspin.min');?>">
    <!-- forms CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('form/themesaller-forms');?>">
    <!-- select2 CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('select2/select2.min');?>">
    <!-- chosen CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('chosen/bootstrap-chosen');?>">
    <!-- ionRangeSlider CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('ionRangeSlider/ion.rangeSlider');?>">
    <link rel="stylesheet" href="<?php echo css_url('ionRangeSlider/ion.rangeSlider.skinFlat');?>">
   <!-- notifications CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('notifications/Lobibox.min'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('notifications/notifications'); ?>">
     <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('style'); ?>">
	<link rel="stylesheet" href="<?php echo $url_src; ?>css/user.css">
    <!-- modals CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('modals');?>">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('responsive'); ?>">
    <!-- fliptab CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('fliptab'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('datapicker/datepicker3'); ?>">
    
	<link rel="stylesheet" href="<?php echo css_url('datapicker/datepicker3'); ?>">
	<link rel="stylesheet" href="<?php echo css_url('animate'); ?>">
	<link rel="stylesheet" href="<?php echo css_url('owl.carousel'); ?>">
	<link rel="stylesheet" href="<?php echo css_url('owl.theme'); ?>">
	<link rel="stylesheet" href="<?php echo css_url('owl.transitions'); ?>">
	 
    <!-- modernizr JS
		============================================ -->
	 <!--link rel="stylesheet" href="<?php echo base_url().'assets/datatable/css/dataTables.bootstrap.min.css'; ?>">
	 <link rel="stylesheet" href="<?php echo base_url().'assets/datatable/css/fixedHeader.bootstrap.min.css'; ?>">
	 <link rel="stylesheet" href="<?php echo base_url().'assets/datatable/css/responsive.bootstrap.min.css'; ?>"-->
	 <link rel="stylesheet" href="<?php echo base_url().'assets/datatable/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css'; ?>">
	 
	 
   
	  <link rel="stylesheet" href="<?php echo base_url().'assets/adminbsb/plugins/bootstrap/css/bootstrap.css'; ?>">
	  <link rel="stylesheet" href="<?php echo base_url().'assets/adminbsb/plugins/node-waves/waves.css'; ?>">
	  <!--link rel="stylesheet" href="<?php //echo base_url().'assets/adminbsb/plugins/animate-css/animate.css'; ?>"-->
	  <link rel="stylesheet" href="<?php echo base_url().'assets/adminbsb/plugins/morrisjs/morris.css'; ?>">
	  <link rel="stylesheet" href="<?php echo base_url().'assets/adminbsb/css/style.css'; ?>">
	   <link rel="stylesheet" href="<?php echo base_url().'assets/adminbsb/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'; ?>">
	  <link rel="stylesheet" href="<?php echo base_url().'assets/adminbsb/css/themes/all-themes.css'; ?>">
	  <link rel="stylesheet" href="<?php echo base_url().'assets/adminbsb/css/fixedHeader.bootstrap.min.css'; ?>">
	 
	 
	<!-- JQuery DataTable Css -->
    
  
    <!--script src="<?php //echo js_url('vendor/modernizr-2.8.3.min'); ?>"></script>
    <script src="<?php //echo $url_src; ?>js/admin/users.js"></script-->
</head>
<body>
