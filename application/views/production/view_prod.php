<script>
    var url_site = '<?php echo site_url(); ?>';
</script>
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="logo-pro">
                    <a href="index.html"><img class="main-logo" src="<?php echo img_url('logo/logo.png'); ?>" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="header-top-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="header-top-wraper">
                            <div class="row">
                                <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                    <!--div class="menu-switcher-pro">
                                        <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                            <i class="educate-icon educate-nav"></i>
                                        </button>
                                    </div-->
                                </div>
                                <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                    <div class="header-top-menu tabl-d-n">
                                        <ul class="nav navbar-nav mai-top-nav">
											<li style="align-text:center;">
                                               <img src="<?php echo base_url().'src/'; ?>img/logo-willemse.png" style="margin:10px 30px 0px -85px;" ></img>
                                           </li>
                                            <li class="nav-item dropdown res-dis-nn">
                                                <a href="<?php echo site_url('visualisation/visualisation');?>" class="nav-link dropdown-toggle" id="nav" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Visualisation<span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                <div class="dropdown-menu animated" aria-labelledby="nav">
                                                    <a class="dropdown-item" href="<?php echo site_url('visualisation/visualisation');?>">Plis</a>
                                                    <a class="dropdown-item" href="<?php echo site_url('visualisation/visualisation/production');?>">Production</a>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown res-dis-nn">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"> Reporting<span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                <div role="menu" class="dropdown-menu">
                                                    <a class="dropdown-item" href="<?php echo site_url('statistics/statistics/stat_plis');?>" class="dropdown-item">Réception</a>
													<a href="<?php echo site_url('statistics/statistics/stat_mensuel');?>" class="dropdown-item">Réception Mensuelle</a>
                                                    <a href="<?php echo site_url('statistics/statistics/stat_traitement');?>" class="dropdown-item">Traitement</a>
                                                </div>
                                            </li>
											<li class="nav-item dropdown res-dis-nn">
                                                <a href="<?php echo site_url('anomalie/anomalie/visu_anomalie');?>">Extraction</a>
                                            </li>
											<li class="nav-item dropdown res-dis-nn">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle" style="color:#E1DD70"><b>Production </b><span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                <div role="menu" class="dropdown-menu">
                                                    <a class="dropdown-item" href="<?php echo site_url('production/production/view_chq');?>" class="dropdown-item">Chèque multiple</a>
													<a href="<?php echo site_url('production/production/view_typage');?>" class="dropdown-item">Typage</a>
													<!--a href="<?php echo site_url('production/production/view_prod');?>" class="dropdown-item">Suivi de production</a-->
                                                   <a href="<?php echo site_url('production/production/visu_chq_tlmc');?>" class="dropdown-item">Suivi Reb</a> 
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <div class="header-right-info">
                                        <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                            <li class="nav-item">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                    <img src="<?php echo base_url().'src/'; ?>img/avatar.png" alt="" />
                                                    <span class="admin-name"><?php echo $this->session->userdata('login'); ?></span>
                                                    <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                                </a>
                                                <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                    <li onclick="deconnexion();"><a href="#"><span class="edu-icon edu-locked author-log-ic"></span>D&eacute;connexion</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- <li class="nav-item nav-setting-open"><a href="#" role="button" class="nav-link"><i class="educate-icon educate-menu"></i></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="">
				<div class="sparkline13-hd" style="padding:20px 0px 0px 20px" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="main-sparkline13-hd">
						<div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin:0px 0px 20px 0px">
							<h1 style="color:#69B145"><i class="fa fa-envelope-square" style="color:#C62057"></i>
							Suivi de la production</h1>  
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<!--span  style="float: right;margin-top:10px;">
								<ol class="breadcrumb">
									
									<li id="tab_plis">
										<a href="javascript:void(0);">
											<i class="fa fa-envelope"></i> Voir la liste des plis
										</a>
									</li>									
									<li id="tab_stat">
										<a onclick="get_reception_traitement()" style="cursor:pointer;">
											<i class="fa fa-bar-chart"></i> Voir la synthèse
										</a>
									</li>
									<li id="tab_tout">
									    <a href="javascript:void(0);">
											<i class="fa fa-home"></i> Voir le résultat initial
										 </a>
									</li>
								</ol>
							</span-->
						</div>
						</div>
					</div>
					
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
						<span class="pull-right" style="font-size:13px;margin-top:8px;"> Courrier du </span></div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group" nowrap="nowrap">
								<div class="input-daterange input-group" id="datepicker">
								<input type="text" class="form-control" id="date_debut" name="date_debut" value="<?php echo date("d/m/Y");?>" style="border:1px solid #ccc;height: 35px; border-radius: 3px; padding-left: 5px;"/>
								<span class="input-group-addon">&nbsp;au</span>
								<input type="text" class="form-control" id="date_fin" name="date_fin" value="<?php echo date("d/m/Y");?>" style="border:1px solid #ccc;height: 35px; border-radius: 3px; padding-left: 5px;"/>
								</div>						
							</div>
						</div>						
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						 <div class="form-group">
							<em id="error_message">*Veuillez saisir les criteres de recherche</em>
								<button class="btn btn-primary waves-effect waves-light" id="stat_rech" onclick="charger_production()">
								<i class="fa fa-search"></i>&nbsp;Rechercher</button>
						</div>
					</div>
					
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
					</div> 
					<div id="result-rsrch" style="display: block;">					
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">En cours de développement
					    <div class="">
                            <div class="sparkline13-graph">
									<div class="datatable-dashv1-list custom-datatable-overright">
										<div id="toolbar">
											<!--select class="form-control dt-tb">
												<option value="">Export Basic</option>
												<option value="all">Export All</option>
												<option value="selected">Export Selected</option>
											</select-->
										</div>
									
									</div>
								</div>
							</div>
						</div>
					</div>
             
				
            </div>
        </div>
    </div>
	
	
	
    <div class="library-book-area mg-t-30">
        <div class="container-fluid">
        </div>
    </div>
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
    </div>
    <div class="courses-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
        <div class="footer-copyright-area" style="position: fixed !important; bottom:0 !important; width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright © 2018. All rights reserved DEV-SI VIVETIC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
