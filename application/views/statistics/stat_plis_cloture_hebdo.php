
<!--div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab21" display="display:none"-->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Détail hebdo des plis cloturés</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab23()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_detail_plis_cloture_hebdo = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:25%;text-align:center;">Semaine</th>
										<th  style="width:15%;text-align:center;">CHQ</th>
										<th  style="width:15%;text-align:center;">CB</th>
										<th  style="width:15%;text-align:center;">Espèces</th>
										<th  style="width:15%;text-align:center;">Mandat</th>
										<th  style="width:15%;text-align:center;">BDP</th>
										<th  style="width:15%;text-align:center;">BDP avec CHQ</th>
										<th  style="width:15%;text-align:center;">Autre</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                  
									$j1 = 0;
									$tab_plis_cltr_hebdo = array();
									foreach($liste_plis_cloture_par_date as $k_plis_cloture_par_date =>$tab_plis_cloture_par_date){
										
										/*$autre = $tab_plis_cloture_par_date["ok"] -  $tab_plis_cloture_par_date["bdc_avec_chq"] - $tab_plis_cloture_par_date["bdc_avec_cb"] - $tab_plis_cloture_par_date["bdc_avec_espece"]-$tab_plis_cloture_par_date["bdc_avec_mandat_facture"];
										*/
										
										
										$num_semaine = $liste_date["num"][$tab_plis_cloture_par_date["dt_event"]];
										
										if (empty($tab_plis_cltr_hebdo[$num_semaine]["CHQ"]))
											$tab_plis_cltr_hebdo[$num_semaine]["CHQ"] = $tab_plis_cloture_par_date["bdc_avec_chq"];
										else
										$tab_plis_cltr_hebdo[$num_semaine]["CHQ"] += $tab_plis_cloture_par_date["bdc_avec_chq"];
										
										if (empty($tab_plis_cltr_hebdo[$num_semaine]["CB"]))
											$tab_plis_cltr_hebdo[$num_semaine]["CB"] = $tab_plis_cloture_par_date["bdc_avec_cb"];
										else
										$tab_plis_cltr_hebdo[$num_semaine]["CB"] += $tab_plis_cloture_par_date["bdc_avec_cb"];
										
										if (empty($tab_plis_cltr_hebdo[$num_semaine]["Esp"]))
											$tab_plis_cltr_hebdo[$num_semaine]["Esp"] = $tab_plis_cloture_par_date["bdc_avec_espece"];
										else
										$tab_plis_cltr_hebdo[$num_semaine]["Esp"] += $tab_plis_cloture_par_date["bdc_avec_espece"];
										
										if (empty($tab_plis_cltr_hebdo[$num_semaine]["mandat"]))
											$tab_plis_cltr_hebdo[$num_semaine]["mandat"] = $tab_plis_cloture_par_date["bdc_avec_mandat_facture"];
										else
										$tab_plis_cltr_hebdo[$num_semaine]["mandat"] += $tab_plis_cloture_par_date["bdc_avec_mandat_facture"];
										
										if (empty($tab_plis_cltr_hebdo[$num_semaine]["bdp"]))
											$tab_plis_cltr_hebdo[$num_semaine]["bdp"] = $tab_plis_cloture_par_date["bdp"];
										else
										$tab_plis_cltr_hebdo[$num_semaine]["bdp"] += $tab_plis_cloture_par_date["bdp"];
									
										if (empty($tab_plis_cltr_hebdo[$num_semaine]["bdp"]))
											$tab_plis_cltr_hebdo[$num_semaine]["bdp"] = $tab_plis_cloture_par_date["bdp"];
										else
										$tab_plis_cltr_hebdo[$num_semaine]["bdp"] += $tab_plis_cloture_par_date["bdp"];
									
										if (empty($tab_plis_cltr_hebdo[$num_semaine]["bdp_chq"]))
											$tab_plis_cltr_hebdo[$num_semaine]["bdp_chq"] = $tab_plis_cloture_par_date["bdp_chq"];
										else
										$tab_plis_cltr_hebdo[$num_semaine]["bdp_chq"] += $tab_plis_cloture_par_date["bdp_chq"];
									
										if (empty($tab_plis_cltr_hebdo[$num_semaine]["autre"]))
											$tab_plis_cltr_hebdo[$num_semaine]["autre"] = $tab_plis_cloture_par_date["autre_plis"];
										else
										$tab_plis_cltr_hebdo[$num_semaine]["autre"] += $tab_plis_cloture_par_date["autre_plis"];
										
									
										
									}
									foreach($tab_plis_cltr_hebdo as $kpli_plis_cltr_hebdo => $vpli_plis_cltr_hebdo){ 
									    
										$content_detail_plis_cloture_hebdo .= '<tr>
                                        <td style="text-align:center;">'.$kpli_plis_cltr_hebdo.'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_hebdo["CHQ"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_hebdo["CB"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_hebdo["Esp"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_hebdo["mandat"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_hebdo["bdp"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_hebdo["bdp_chq"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_hebdo["autre"].'</td>
										<!--td style="text-align:center;"></td-->
										</tr>';
										
									}
								 $content_detail_plis_cloture_hebdo .='</tbody>
                            </table>';
							echo $content_detail_plis_cloture_hebdo;
								//echo "<pre>";print_r($tab_plis_cltr_hebdo);	echo "</pre>";
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	<!--/div-->
                           