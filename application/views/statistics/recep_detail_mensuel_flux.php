<div class="row clearfix">
    <!-- Task Info -->
    <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Réception par mois</span>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <a onclick="quitter_detail_mensuel()" style="cursor: pointer;">
                                <i class="fa fa-close"> </i></a>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <?php
                    $content_recep_detail_mois = '<table class="table table-bordered table-striped table-hover dataTable js-exportable"  style="width:100% !important;">  
                                <thead   class="custom_font">
									<tr>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Réception du</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Nombre de flux</th>																		
                                      
                                    </tr>
                                   
                                </thead>
                                <tbody   class="custom_font">';


                    foreach($liste_lot as $k_flux =>$tab_flux){
                        //préparaion des données pour les graphes reception global

                        $content_recep_detail_mois .= '<tr>
                                        <td style="text-align:center">'.$tab_flux["date_reception"].'</td>
										<td style="text-align:center">'.$tab_flux["nb_flux"].'</td>
									</tr>';

                    }
                    $content_recep_detail_mois .='</tbody>
                            </table>';
                    echo $content_recep_detail_mois;
                    ?>
                </div>
            </div>

        </div>
    </div>
    <!-- #END# Task Info -->
    <!-- Browser Usage -->

</div>