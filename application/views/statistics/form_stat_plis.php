<?php
ini_set('memory_limit', '-1');
 $url_src    = base_url().'src/';
 $url_js     = base_url().'src/';
 $date_debut = $_REQUEST["date_debut"];
 $date_fin   = $_REQUEST["date_fin"];
 
 

?>
	 <!--div class="row"-->
<?php

    foreach($liste_pli as $pli){
		$nouveau_percent  = 0;
		$encours_percent  = 0;
		$traite_percent   = 0;
		$anomalie_percent = 0;
		$pli_total =  $pli['nouveau']+$pli['encours']+$pli['traite']+$pli['anomalie'];
		if ($pli_total > 0){
			$nouveau_percent   = $pli['nouveau']*100 / $pli_total;
			$encours_percent   = $pli['encours']*100 / $pli_total;
			$traite_percent    = $pli['traite']*100 / $pli_total;
			$anomalie_percent  = $pli['anomalie']*100 / $pli_total;
			$nouveau_percent   = ($nouveau_percent > 0) ? number_format($nouveau_percent,2) : 0;
			$encours_percent   = ($encours_percent > 0) ? number_format($encours_percent,2) : 0;
			$traite_percent    = ($traite_percent > 0) ? number_format($traite_percent,2) : 0;
			$anomalie_percent  = ($anomalie_percent > 0) ? number_format($anomalie_percent,2) : 0;
		}
   }


?>

   <!-- Widgets -->
            <div class="row clearfix" style="text-align:center; margin-top:125px;">
               
				 <div class="col-lg-1 col-md-1 col-sm-6 col-xs-12">
				 </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan-t hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-envelope"></i>
						</div>
                        <div class="content">
                            <div class="text">Plis total</div>
                            <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo $pli_total;?></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-plus-square"></i>
                        </div>
                        <div class="content">
                            <div class="text">Non traité<span style="float:right; width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php echo $nouveau_percent;?> %</span></div>
                            <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo $pli['nouveau'];?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-check-square"></i>
                        </div>
                        <div class="content">
                            <div class="text">Cloturé<span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php echo $traite_percent;?> %</span></div>
                            <div class="number count-to" data-from="0" data-to="242" data-speed="1000" data-fresh-interval="20"><?php echo $pli['traite'];?></div>
                        </div>
                    </div>
                </div>
				 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-cogs"></i>
                        </div>
                        <div class="content">
                            <div class="text">En cours<span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php echo $encours_percent;?> %</span></div>
                            <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"><?php echo $pli['encours'];?></div>
                        </div>
                    </div>
                </div>
				 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-bug"></i>
                        </div>
                        <div class="content">
                            <div class="text">Anomalie<span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php echo $anomalie_percent;?> %</span></div>
                            <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $pli['anomalie'];?></div>
                        </div>
                    </div>
                </div>
			<div class="col-lg-1 col-md-1 col-sm-6 col-xs-12">
			</div>
			 
            </div>
			
			

	</br>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab_synthese_reception">
	</div>
	<div class="row" style="padding:10px 0px 0px 0px;">
	
	
	<input type="hidden" value="<?php echo $data_donut_document ;?>" id="graph_donut_doc"/>
	<input type="hidden" value="<?php echo $data_donut_plis_traite ;?>" id="graph_donut"/>
	<input type="hidden" value="<?php echo $data_donut_plis_ko ;?>" id="graph_donut_plis_ko"/>
	<input type="hidden" value="<?php echo $data_donut_plis_restant;?>" id="graph_donut_plis_restant"/>
	<script type="javascript">
		getMorris('donut', 'donut_chart');
	</script>
	
	<!--div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      
                    </div>
					 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                     
                    </div>
	</div-->

	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab1" >
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
								Nombre de plis avec moyen de paiement</span>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="font-family:Arial,Tahoma,sans-serif;font-size:12px;">
                                        <i class="fa fa-caret-square-o-right">
											<span style="font-family:Arial,Tahoma,sans-serif;font-size:12px;"> Voir Détail &nbsp;&nbsp;&nbsp;&nbsp;</span>
										</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a onclick="get_plis_avec_moyen_de_paiement_j()">Par jour</a></li>
                                        <li><a onclick="get_plis_avec_moyen_de_paiement_s()">Par semaine</a></li>                                      
                                        <!--li><a onclick="get_plis_avec_moyen_de_paiement_m()">Par mois</a></li-->                                      
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php $content_type_doc = '<table class="table">
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:60%">#Pli avec Paiement</th>
                                        <th  style="width:20%">Nbr.</th>
                                        <th  style="width:20%">%</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                
								$j1 = 0;
								foreach($data_document as $kdoct =>$tab_doc){
									
									$content_type_doc .= '<tr>
									<th scope="row" style="font-weight:normal;">'.$tab_doc["code"].'</th>
									<td>'.$tab_doc["nb"].'</td>
									<td>'.$tab_doc["pourcent"].' %</td>
									</tr>';
									
								}
								 $content_type_doc .=' </tbody>
								  <tfoot   class="custom_font">
                                    <tr>
                                        <th>Plis total</th>
                                        <th>'.$pli_total.'</th>
                                        <th></th>                                      
                                    </tr>
                                </tfoot>
                            </table>';
							echo $content_type_doc;
									
							?>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="body table-responsive">
                      <div class="card" >
						<div class="header">
							 <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Répartition en mode graphe </span>
						</div>
						<div class="body" style="/*padding:2px 2px 2px 2px*/">
							<div style="height: 200px;width:auto" id="donut_document_chart" class="graph">
							</div>
						</div>
					</div>
					</div>
                </div>
                <!-- #END# Browser Usage -->
            </div>
	</div>
	</div>
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab11" display="display:none">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Détail des plis avec moyen de paiement</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab11()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_type_doc_d = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:25%;text-align:center;">Date de traitement</th>
										<th  style="width:15%;text-align:center;">CHQ</th>
										<th  style="width:15%;text-align:center;">CB</th>
										<th  style="width:15%;text-align:center;">Autre MPT</th>
										<th  style="width:15%;text-align:center;">Non typé</th>
										<th  style="width:15%;text-align:center;">Autres plis</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                  
								$j1 = 0;
								foreach($liste_plis_recu_par_date as $k_plis_recu_par_date =>$tab_plis_recu_par_date){
									
									$content_type_doc_d .= '<tr>
									<td style="text-align:center;">'.$tab_plis_recu_par_date["dt_event"].'</td>
									<td style="text-align:center;">'.$tab_plis_recu_par_date["avec_paiement_chq"].'</td>
									<td style="text-align:center;">'.$tab_plis_recu_par_date["avec_paiement_cb"].'</td>
									<td style="text-align:center;">'.$tab_plis_recu_par_date["avec_paiement_autre"].'</td>
									<td style="text-align:center;">'.$tab_plis_recu_par_date["non_traite"].'</td>
									<td style="text-align:center;">'.$tab_plis_recu_par_date["autre_pli"].'</td>
									</tr>';
									
								}
								 $content_type_doc_d .='</tbody>
                            </table>';
							echo $content_type_doc_d;
									
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	</div>
                           
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab12" display="display:none">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Détail hebdo des plis avec moyen de paiement</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab12()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_type_doc_d = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:25%;text-align:center;">Traitement S</th>
										<th  style="width:15%;text-align:center;">CHQ</th>
										<th  style="width:15%;text-align:center;">CB</th>
										<th  style="width:15%;text-align:center;">Autre MPT</th>
										<th  style="width:15%;text-align:center;">Non typé</th>
										<th  style="width:15%;text-align:center;">Autres plis</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                  
									
									$tab_plis_sem = array();
									$tab_plis_ttt_hebdo = array();
									foreach($liste_plis_recu_par_date as $k_plis_recu_par_date =>$tab_plis_recu_par_date){
										
										$num_semaine = "";
										$num_semaine = $liste_date["num"][$tab_plis_recu_par_date["dt_event"]];
										
									
										if (empty($tab_plis_ttt_hebdo[$num_semaine]["CHQ"]))
											$tab_plis_ttt_hebdo[$num_semaine]["CHQ"] = $tab_plis_recu_par_date["avec_paiement_chq"];
										else
										$tab_plis_ttt_hebdo[$num_semaine]["CHQ"] += $tab_plis_recu_par_date["avec_paiement_chq"];
									
										if (empty($tab_plis_ttt_hebdo[$num_semaine]["CB"]))
											$tab_plis_ttt_hebdo[$num_semaine]["CB"] = $tab_plis_recu_par_date["avec_paiement_cb"];
										else
										$tab_plis_ttt_hebdo[$num_semaine]["CB"] += $tab_plis_recu_par_date["avec_paiement_cb"];
									
										if (empty($tab_plis_ttt_hebdo[$num_semaine]["autre"]))
											$tab_plis_ttt_hebdo[$num_semaine]["autre"] = $tab_plis_recu_par_date["avec_paiement_autre"];
										else
										$tab_plis_ttt_hebdo[$num_semaine]["autre"] += $tab_plis_recu_par_date["avec_paiement_autre"];
										
										if (empty($tab_plis_ttt_hebdo[$num_semaine]["non_traite"]))
											$tab_plis_ttt_hebdo[$num_semaine]["non_traite"] = $tab_plis_recu_par_date["non_traite"];
										else
										$tab_plis_ttt_hebdo[$num_semaine]["non_traite"] += $tab_plis_recu_par_date["non_traite"];
										
										if (empty($tab_plis_ttt_hebdo[$num_semaine]["autre_pli"]))
											$tab_plis_ttt_hebdo[$num_semaine]["autre_pli"] = $tab_plis_recu_par_date["autre_pli"];
										else
										$tab_plis_ttt_hebdo[$num_semaine]["autre_pli"] += $tab_plis_recu_par_date["autre_pli"];
										
									}
									
									foreach($tab_plis_ttt_hebdo as $kpli_mpt_hb => $vpli_mpt_hb){ 
									    
										$content_type_doc_d .= '<tr>
                                        <td style="text-align:center;">'.$kpli_mpt_hb.'</td>
										<td style="text-align:center;">'.$vpli_mpt_hb["CHQ"].'</td>
										<td style="text-align:center;">'.$vpli_mpt_hb["CB"].'</td>
										<td style="text-align:center;">'.$vpli_mpt_hb["autre"].'</td>
										<td style="text-align:center;">'.$vpli_mpt_hb["non_traite"].'</td>
										<td style="text-align:center;">'.$vpli_mpt_hb["autre_pli"].'</td>
										</tr>';
										
									}
								 $content_type_doc_d .='</tbody>
                            </table>';
							echo $content_type_doc_d;
									
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	</div>
                           
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab13" display="display:none">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Détail mensuel des plis avec moyen de paiement</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab13()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_pli_avec_mpt_mes = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">  
                                <thead class="custom_font">
                                    <tr>
                                        <th  style="width:25%;text-align:center;">Mois</th>
										<th  style="width:15%;text-align:center;">CHQ</th>
										<th  style="width:15%;text-align:center;">CB</th>
										<th  style="width:15%;text-align:center;">Autre MPT</th>
										<th  style="width:15%;text-align:center;">Non typé</th>
										<th  style="width:15%;text-align:center;">Autres plis</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';	
								   /*echo "<pre>";
                                   print_r($liste_plis_recu_par_date);
								   echo "</pre>";*/
									$tab_plis_ttt_mens = array();
									foreach($liste_plis_recu_par_date as $k_plis_ttt_par_date =>$tab_plis_ttt_par_date){
										
										$mois = $liste_mois["mois"][$tab_plis_ttt_par_date["dt_event"]];
										
										if (empty($tab_plis_ttt_mens[$mois]["CHQ"]))
											$tab_plis_ttt_mens[$mois]["CHQ"] = $tab_plis_ttt_par_date["avec_paiement_chq"];
										else
										$tab_plis_ttt_mens[$mois]["CHQ"]     += $tab_plis_ttt_par_date["avec_paiement_chq"];
									
										if (empty($tab_plis_ttt_mens[$mois]["CB"]))
											$tab_plis_ttt_mens[$mois]["CB"]   = $tab_plis_ttt_par_date["avec_paiement_cb"];
										else
										$tab_plis_ttt_mens[$mois]["CB"]       += $tab_plis_ttt_par_date["avec_paiement_cb"];
									
										if (empty($tab_plis_ttt_mens[$mois]["autre"]))
											$tab_plis_ttt_mens[$mois]["autre"] = $tab_plis_ttt_par_date["avec_paiement_autre"];
										else
										$tab_plis_ttt_mens[$mois]["autre"]    += $tab_plis_ttt_par_date["avec_paiement_autre"];
										
										if (empty($tab_plis_ttt_mens[$mois]["non_traite"]))
											$tab_plis_ttt_mens[$mois]["non_traite"] = $tab_plis_ttt_par_date["non_traite"];
										else
										$tab_plis_ttt_mens[$mois]["non_traite"] += $tab_plis_ttt_par_date["non_traite"];
										
										if (empty($tab_plis_ttt_mens[$mois]["autre_pli"]))
											$tab_plis_ttt_mens[$mois]["autre_pli"] = $tab_plis_ttt_par_date["autre_pli"];
										else
										$tab_plis_ttt_mens[$mois]["autre_pli"]    += $tab_plis_ttt_par_date["autre_pli"];
										
									}
									
									foreach($tab_plis_ttt_mens as $kpli_mpt_mens => $vpli_mpt_mens){ 
									    
										$content_pli_avec_mpt_mes .= '<tr>
                                        <td style="text-align:center;">'.$kpli_mpt_mens.'</td>
										<td style="text-align:center;">'.$vpli_mpt_mens["CHQ"].'</td>
										<td style="text-align:center;">'.$vpli_mpt_mens["CB"].'</td>
										<td style="text-align:center;">'.$vpli_mpt_mens["autre"].'</td>
										<td style="text-align:center;">'.$vpli_mpt_mens["non_traite"].'</td>
										<td style="text-align:center;">'.$vpli_mpt_mens["autre_pli"].'</td>
										</tr>';
										
									}
								 $content_pli_avec_mpt_mes .='</tbody>
                            </table>';
							echo $content_pli_avec_mpt_mes;
									
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	</div>
                           
	
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab2" >	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Nombre de plis CLOTURES</span>
                             <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="font-family:Arial,Tahoma,sans-serif;font-size:12px;">
                                        <i class="fa fa-caret-square-o-right">
											<span style="font-family:Arial,Tahoma,sans-serif;font-size:12px;"> Voir Détail &nbsp;&nbsp;&nbsp;&nbsp;</span>
										</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a onclick="get_plis_cloture_j()">Par jour</a></li>
                                        <li><a onclick="get_plis_cloture_s()">Par semaine</a></li>
                                        <!--li><a onclick="get_plis_cloture_m()">Par mois</a></li-->
                                        <li><a onclick="get_plis_cloture_produit()">Par produit</a></li>                                      
                                    </ul>
                                </li>
                            </ul>
							
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 
							
							$content_type_pli = '<table class="table">
                                <thead   class="custom_font">
                                    <tr>
                                        <th style="width:60%">#Pli</th>
                                        <th style="width:20%">Nbr.</th>
                                        <th style="width:20%">%</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';						
                                  
									$j = 0;
									//$tab_pli_traite_lib = array("BDC_CHQ"=>"BDC payé par CHQ","BDC_CB"=>"BDC payé par CB","BDC_ESP"=>"BDC payé par ESPECES","MANDAT"=>"BDC payé par MANDAT FACTURE","autre"=>"Autres type de plis","CLOTURE"=>"PLIS CLOTURES");
									$tab_pli_traite_lib = array("BDC_CHQ"=>"BDC payé par CHQ","BDC_CB"=>"BDC payé par CB","BDC_ESP"=>"BDC payé par ESPECES","MANDAT"=>"BDC payé par MANDAT FACTURE","BDP"=>"Bon de participation","BDP_CHQ"=>"Bon de participation avec CHQ","Autre"=>"Autre type de plis","CLOTURE"=>"PLIS CLOTURES");
									
									foreach($data_plis_cloture as $kpli_cloture =>$tab_pli_cloture){ 
										if ($tab_pli_cloture["code"] == "CLOTURE"){
											$content_type_pli .= '<tr>
											<th scope="row" style="font-weight:normal;"><b>'.$tab_pli_traite_lib[$kpli_cloture].'</b></th>
											<td><b>'.$tab_pli_cloture["nb"].'</b></td>
											<td><b>'.$tab_pli_cloture["pourcent"].'%</b></td>
											</tr>';
										}
										else{
										$content_type_pli .= '<tr>
                                        <td scope="row" style="font-weight:normal;">'.$tab_pli_traite_lib[$kpli_cloture].'</td>
										<!--div class="card"-->											 
											<!--/div-->
										<td>'.$tab_pli_cloture["nb"].'</td>
										<td>'.$tab_pli_cloture["pourcent"].'%</td>
										</tr>';
										}
									}
									  
                               
                               $content_type_pli .='<tr><th><b>Total plis traités</b></th>
							   <th><b>'.$total_plis_traites.'</b></th>
							   <th><b></b></th></tr>';
							   $content_type_pli .=' </tbody>';
                            $content_type_pli .='</table>';
							echo $content_type_pli;
									
							?>
                       </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="card">
								<div class="header">
									<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Répartition en mode graphe</span>
									<ul class="header-dropdown m-r--5">
										<li class="dropdown">
											
										</li>
									</ul>
								</div>
								<div class="body" style="/*padding:2px 2px 2px 2px*/">
									<div style="height: 200px;width:auto"  id="donut_chart" class="graph">
									</div>
								</div>
					</div>
                </div>
                <!-- #END# Browser Usage -->
            </div>
	</div>
	</div>
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab21" display="display:none">
	
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab23" display="display:none">
	
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab24" display="display:none">
	
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab22" display="display:none">
	
	</div>
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab3">	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card"> 
					<div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Nombre de plis KO</span>
                             <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="font-family:Arial,Tahoma,sans-serif;font-size:12px;">
                                        <i class="fa fa-caret-square-o-right">
											<span style="font-family:Arial,Tahoma,sans-serif;font-size:12px;"> Voir Détail &nbsp;&nbsp;&nbsp;&nbsp;</span>
										</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a onclick="get_plis_ko_j()">Par jour</a></li>
                                        <li><a onclick="get_plis_ko_s()">Par semaine</a></li>
                                        <!--li><a onclick="get_plis_ko_m()">Par mois</a></li-->
                                     </ul>
                                </li>
                            </ul>
							
                        </div>
                       
						 <div class="analytics-content" >
                        <div class="body">
                            <div class="table-responsive">
                            <?php 
							
							$content_type_pli_ko = '<table class="table">
                                <thead   class="custom_font">
                                    <tr>
                                        <th style="width:60%">#Pli</th>
                                        <th style="width:20%">Nbr.</th>
                                        <th style="width:20%">%</th>                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';						
                                  
									$j = 0;
									$tab_pli_ko_lib = array("ko_scan"=>"KO SCAN","ko_inconnu"=>"KO Inconnu","ko_call"=>"KO Call","ko_def"=>"KO Définitif","ko_circulaire"=>"KO circulaire","ko_en_attente"=>"KO en attente","ko_reclammation"=>"KO Réclammation","ko_litige"=>"KO Litige","KO"=>"PLIS KO");
									
									foreach($data_plis_ko as $kpli_ko =>$tab_pli_ko){ 
										if ($tab_pli_ko["code"] == "KO"){
											$content_type_pli_ko .= '<tr>
												<th scope="row" style="font-weight:normal;"><b>'.$tab_pli_ko_lib[$kpli_ko].'</b></th>
												<td><b>'.$tab_pli_ko["nb"].'</b></td>
												<td><b>'.$tab_pli_ko["pourcent"].'%</b></td>
											</tr>';
										}
										else{
											
											$content_type_pli_ko .= '<tr>';
											if($tab_pli_ko_lib[$kpli_ko] != 'KO Call'){
												$content_type_pli_ko .= '<td scope="row" style="font-weight:normal;">'.$tab_pli_ko_lib[$kpli_ko].'</td>';
												$content_type_pli_ko .='<td>'.$tab_pli_ko["nb"].'</td>
																		<td>'.$tab_pli_ko["pourcent"].'%</td>
												</tr>';
											}
											else{ 
												 $content_type_pli_ko .= '<td scope="row" style="font-weight:normal;">
															<a onclick="get_plis_ko_call()" style="cursor:pointer;">'.$tab_pli_ko_lib[$kpli_ko].'</a></td>';
												
												
												$content_type_pli_ko .='<td>'.$tab_pli_ko["nb"].'</td>
												<td>'.$tab_pli_ko["pourcent"].'%</td>
												</tr>';
												$content_type_pli_ko .='
												<tr><td colspan="3"><div id="detail_ko_call" style="text-align:right;">
															<table class="table table-striped" style="width:100%;display:none;">                   
															</table>
															</div>
												</td></tr>';
											}
										}
									}
									  
                               $content_type_pli_ko .='<tr><th><b>Total plis traités</b></th>
							   <th><b>'.$total_plis_traites.'</b></th>
							   <th><b></b></th></tr>';
							   $content_type_pli_ko .=' </tbody>';							   
                            $content_type_pli_ko .='</table>';
							echo $content_type_pli_ko;
									
							?>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="card">
								<div class="header">
									<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Répartition en mode graphe</span>
									<ul class="header-dropdown m-r--5">
										<li class="dropdown">
											
										</li>
									</ul>
								</div>
								<div class="body" style="/*padding:2px 2px 2px 2px*/">
									<div style="height: 200px;width:auto"  id="donut_plis_ko_chart" class="graph">
									</div>
								</div>
					</div>
                </div>
                <!-- #END# Browser Usage -->
            </div>
	</div>
	</div>
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab31" display="display:none">	
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab32" display="display:none">	
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab33" display="display:none">	
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab4">	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Nombre de plis restant à traiter</span>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 
							
							$content_type_pli_restant = '<table class="table">
                                <thead   class="custom_font">
                                    <tr>
                                        <th style="width:60%">#Pli</th>
                                        <th style="width:20%">Nbr.</th>
                                        <th style="width:20%">%</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';						
                                  
									$j = 0;
									$tab_pli_restant_lib = array("en_cours"=>"EN COURS","nouveau"=>"NON TRAITE","RESTANT"=>"PLIS RESTANT");
									
									foreach($data_plis_restant as $kpli_restant =>$tab_pli_restant){ 
										if ($tab_pli_restant["code"] == "RESTANT"){
											$content_type_pli_restant .= '<tr>
											<th scope="row" style="font-weight:normal;"><b>'.$tab_pli_restant_lib[$kpli_restant].'</b></th>
											<td><b>'.$tab_pli_restant["nb"].'</b></td>
											<td><b>'.$tab_pli_restant["pourcent"].'%</b></td>
											</tr>';
										}
										else{
										$content_type_pli_restant .= '<tr>
                                        <th scope="row" style="font-weight:normal;">'.$tab_pli_restant_lib[$kpli_restant].'</th>
										<td>'.$tab_pli_restant["nb"].'</td>
										<td>'.$tab_pli_restant["pourcent"].'%</td>
										</tr>';
										}
									}
									  
                               $content_type_pli_restant .='<tr><th><b>Total plis restant à traiter</b></th>
							   <th><b>'.$total_plis_restant.'</b></th>
							   <th><b></b></th></tr>';
							   
							   $content_type_pli_restant .=' </tbody>';							   
                               $content_type_pli_restant .='</table>';
							echo $content_type_pli_restant;
									
							?>
                       </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="card">
								<div class="header">
									<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Répartition en mode graphe</span>
									<ul class="header-dropdown m-r--5">
										<li class="dropdown">
											
										</li>
									</ul>
								</div>
								<div class="body" style="/*padding:2px 2px 2px 2px*/">
									<div style="height: 200px;width:auto"  id="donut_plis_restant_chart" class="graph">
									</div>
								</div>
					</div>
                </div>
                <!-- #END# Browser Usage -->
            </div>
	</div>
	</div>
	
<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab5"  style="margin-top: 10px;margin-bottom: 100px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
         <div class="analytics-content" id="detail_stat">
        
		 <div class="body">
         <div class="table-responsive">
		<table class="table table-bordered table-striped table-hover dataTable js-export-table" id="detail_pli">                      
		 <thead  class="custom_font">
		
            <tr>
                <th>Courrier du</th>
                <th>Numérisation du</th>
                <th>Lot Scan</th>
                <th>Pli</th>
                <th>Traitement du</th>
                <th>Type de pli</th>
                <th>Statut</th>
                <th>Nbr. Document</th>
                <th>Clé pli</th>
            </tr>
        </thead> 
         <tbody class="custom_font"></tbody>
                               	
		</table>
         </div>
         </div>
		</div>
		</div>
    </div>
	</div>
	
	
