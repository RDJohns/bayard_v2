
<!--div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab21" display="display:none"-->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Détail journalier des plis cloturés</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab21()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_detail_plis_cloture = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:25%;text-align:center;">Date</th>
										<th  style="width:15%;text-align:center;">CHQ</th>
										<th  style="width:15%;text-align:center;">CB</th>
										<th  style="width:15%;text-align:center;">Espèces</th>
										<th  style="width:15%;text-align:center;">Mandat</th>
										<th  style="width:15%;text-align:center;">BDP</th>
										<th  style="width:15%;text-align:center;">BDP avec CHQ</th>
										<th  style="width:15%;text-align:center;">Autre</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                  
									$j1 = 0;
									foreach($liste_plis_cloture_par_date as $k_plis_cloture_par_date =>$tab_plis_cloture_par_date){
										/*$autre = $tab_plis_cloture_par_date["ok"] -  $tab_plis_cloture_par_date["bdc_avec_chq"] - $tab_plis_cloture_par_date["bdc_avec_cb"] -$tab_plis_cloture_par_date["bdc_avec_espece"]-$tab_plis_cloture_par_date["bdc_avec_mandat_facture"];*/
										$autre = $tab_plis_cloture_par_date["autre_plis"];
										$content_detail_plis_cloture .= '<tr>
                                        <td style="text-align:center;">'.$tab_plis_cloture_par_date["dt_event"].'</td>
										<td style="text-align:center;">'.$tab_plis_cloture_par_date["bdc_avec_chq"].'</td>
										<td style="text-align:center;">'.$tab_plis_cloture_par_date["bdc_avec_cb"].'</td>
										<td style="text-align:center;">'.$tab_plis_cloture_par_date["bdc_avec_espece"].'</td>
										<td style="text-align:center;">'.$tab_plis_cloture_par_date["bdc_avec_mandat_facture"].'</td>
										<td style="text-align:center;">'.$tab_plis_cloture_par_date["bdp"].'</td>
										<td style="text-align:center;">'.$tab_plis_cloture_par_date["bdp_chq"].'</td>
										<td style="text-align:center;">'.$autre.'</td>
										</tr>';
										
									}
								 $content_detail_plis_cloture .='</tbody>
                            </table>';
							echo $content_detail_plis_cloture;
									
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	<!--/div-->
                           