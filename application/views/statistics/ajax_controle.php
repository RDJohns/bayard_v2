<!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
								Traitement des plis - Contrôle/Matchage</span>
                            <ul class="header-dropdown m-r--5">
                               <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_traitement_journalier()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 		
															
                                  if(count($list_solde_pli) > 0 && $mensuel == 0){
										$content_traitement_jr = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-controle">     
										<thead   class="custom_font">
											<tr>
												<th  style="width:20%">Traitement du</th>
												<th>OK</th>
												<th>KO Scan</th>
												<th>KO Définitif</th>
												<th>KO Inconnu</th>
												<th>Cloturé sans traitement</th>
												<th>Divisé</th>
												<th>Circulaire</th>
												<th>Cir. éditée</th>
												<th>Cir. envoyée</th>		
												<th>KO SRC</th>
												<th>KO en attente de consigne</th>
												<th>OK+Circulaire</th>	
												<th>KO en cours bayard</th>									  
																					  
											</tr>
											
										</thead>
										<tbody   class="custom_font">';
									$total_ci = $total_ano  = $total_saisie_ok =$total_saisie_ko = $total_saisie_ci = $total_ko_ks = $total_ko_ke = $total_ok_circ= $total_ko_bayard=0;
									$total_ko_scan = $total_ko_inc = $total_ko_aba = $total_divise = $total_kodef = $total_ci_edi = $total_ci_env = 0;
									//var_dump($list_solde_pli);die;
									foreach($list_solde_pli as $ksolde =>$list_solde){
											$total_saisie_ok += $list_solde["ok"];
											$total_ko_scan += $list_solde["ko_scan"];
											$total_ko_inc  += $list_solde["ko_inconnu"];
											$total_ko_aba  += $list_solde["ko_abandonne"];
											$total_divise  += $list_solde["divise"];
											$total_kodef   += $list_solde["kd"];
											$total_ci 	   += $list_solde["ci"];
											$total_ci_edi  += $list_solde["ko_ci_editee"];
											$total_ci_env  += $list_solde["ko_ci_envoyee"];
											$total_ko_ks  += $list_solde["ks"];
											$total_ko_ke  += $list_solde["ke"];
											$total_ok_circ   += $list_solde["ok_circ"];
											$total_ko_bayard   += $list_solde["ko_bayard"];
										$content_traitement_jr .='<tr>
											<td  style="width:20%">'.$ksolde.'</td>
											<td>'.$list_solde["ok"].'</td>
											<td>'.$list_solde["ko_scan"].'</td>
											<td>'.$list_solde["kd"].'</td>
											<td>'.$list_solde["ko_inconnu"].'</td>
											<td>'.$list_solde["ko_abandonne"].'</td>
											<td>'.$list_solde["divise"].'</td>
											<td>'.$list_solde["ci"].'</td>			 
											<td>'.$list_solde["ko_ci_editee"].'</td>
											<td>'.$list_solde["ko_ci_envoyee"].'</td>
											<td>'.$list_solde["ks"].'</td>
											<td>'.$list_solde["ke"].'</td>												
											<td>'.$list_solde["ok_circ"].'</td>												
											<td>'.$list_solde["ko_bayard"].'</td>												
																			  
														  
										</tr>';
									}
								
								$content_traitement_jr .='<tr>
											<th>Total</th>
											<th>'.$total_saisie_ok.'</th>
											<th>'.$total_ko_scan.'</th>
											<th>'.$total_kodef.'</th>
											<th>'.$total_ko_inc.'</th>
											<th>'.$total_ko_aba.'</th>
											<th>'.$total_divise.'</th>
											<th>'.$total_ci.'</th>		
											<th>'.$total_ci_edi.'</th>
											<th>'.$total_ci_env.'</th>
											<th>'.$total_ko_ks.'</th>
											<th>'.$total_ko_ke.'</th>
											<th>'.$total_ok_circ.'</th>
											<th>'.$total_ko_bayard.'</th>
													  
									</tr></tbody>									 
								</table>';
								echo $content_traitement_jr;
								
								/*echo "<pre>";	
										print_r($tab_pli_ttt);								
									echo "</pre>";	*/	
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->