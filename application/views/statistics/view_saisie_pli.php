<?php
ini_set('memory_limit', '-1');
 $url_src = base_url().'src/';
 $url_js = base_url().'src/';

?>

   <div class="row clearfix" style="text-align:center;">
	   
	
	 
	</div>
			

	</br>
	<?php
		foreach($list_societe as $ksoc=>$lib_societe){			
		
	?>
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="tab_hebdo">
	   <div class="row clearfix">
                <!-- Task Info -->
				 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
							<span style="font-size:20px;"><i class="fa fa-copy" style="color:#00A651"></i>&nbsp;</span>
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">
								<?php echo $lib_societe ;?> - Suivi des saisies</span>
                            <ul class="header-dropdown m-r--5">
                               <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_traitement()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 		
	
					
				 
                                  	//pli_saisie_adv_j_ci								
									if(count($liste_solde_j) > 0 ){
										$content_solde_mvt = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-saisie">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date courrier</th>
												<th style="width:10%">Réception</th>
												<th style="width:10%">Saisie J-- </th>
												<th style="width:10%">Saisie ce jour (A)</th>
												<th style="width:10%"><font color="#0E76BC">(A) OK</font></th>';
												if($source == 'courrier') 
												$content_solde_mvt .= '
												<th style="width:10%"><font color="#0E76BC">(A) CI</font></th>';
												$content_solde_mvt .= '<th style="width:10%"><font color="#0E76BC">(A) KO</font></th>
												<th style="width:10%">KO Scan</th>
												<th style="width:10%">KO Définitif</th>
												<th style="width:10%">KO Inconnu</th>
												<th style="width:10%">Cloturé sans traitement</th>
												<th style="width:10%">Divisé</th>
												<th style="width:10%">KO Circulaire</th>
												<th style="width:10%">Cir. éditée</th>
												<th style="width:10%">Cir. envoyée</th>
												<th style="width:10%">KO SRC</th>
												<th style="width:10%">En attente de consigne</th>
												<th style="width:10%">Ok+circulaire</th>
												<th style="width:10%">KO en cours bayard</th>
												
											</tr>
										</thead>
										<tbody   class="custom_font">';
									$total = $total_saisie_adv_jm  = $total_ano = $total_ci =  $total_saisie_j =$total_saisie_ok =$total_saisie_ci =$total_saisie_ko = 0;
									$total_ko_scan = $total_ko_def = $total_ko_inconnu = $total_ko_abondonne = $total_divise = $total_ko_circulaire =$total_ci_editee = $total_ci_envoyee = $total_ko_ks = $total_ko_ke = $total_ok_circ=$total_ko_bayard=0; 
									foreach($liste_solde_j as $ksolde =>$list){
										if($lib_societe == $list["nom_societe"]){
											
			
				 
											$ano = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["anomalie"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["anomalie"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["anomalie"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["anomalie"] : 0;
											
											$ci = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci"] : 0;
											
											$ko_scan = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_scan"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_scan"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_scan"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_scan"] : 0;
											
											$ko_def = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_def"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_def"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_def"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_def"] : 0;
											
											$ko_inconnu = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_inconnu"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_inconnu"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_inconnu"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_inconnu"] : 0;
											
											$ko_abondonne = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_abondonne"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_abondonne"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_abondonne"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_abondonne"] : 0;
											
											$divise = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["divise"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["divise"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["divise"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["divise"] : 0;
											
											$ko_circulaire = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_circulaire"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_circulaire"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_circulaire"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_circulaire"] : 0;
											
											$ci_editee = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci_editee"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci_editee"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci_editee"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci_editee"] : 0;
											
											$ci_envoyee = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci_envoyee"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci_envoyee"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci_envoyee"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ci_envoyee"] : 0;
											
											$ko_ks = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_ks"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_ks"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_ks"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_ks"] : 0;
											
											$ko_ke = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_ke"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_ke"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_ke"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_ke"] : 0;
											
											$ok_circ = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ok_circ"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ok_circ"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ok_circ"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ok_circ"] : 0;
											
											$ko_bayard = (!empty($liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_bayard"]) && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_bayard"] != null && $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_bayard"] != '') ? $liste_anomalie[$list["date_courrier"]][$list["nom_societe"]]["ko_bayard"] : 0;
											
											
											$total 					+= $list["pli_total"];
											$total_saisie_adv_jm 	+= $list["pli_saisie_adv_jmoins"];
											$total_ano 		+=  $ano;
											$total_ci 		+=  $ci;
											$total_ko_scan 	+=  $ko_scan;
											$total_ko_def 	+=  $ko_def;
											$total_ko_inconnu 	 +=  $ko_inconnu;
											$total_ko_abondonne  +=  $ko_abondonne;
											$total_divise  		 +=  $divise;
											$total_ko_circulaire +=  $ko_circulaire;
											$total_ci_editee     +=  $ci_editee;
											$total_ci_envoyee    +=  $ci_envoyee;
											$total_ko_ks   		 +=  $ko_ks;
											$total_ko_ke  		 +=  $ko_ke;
											$total_ok_circ 		 +=  $ok_circ;
											$total_ko_bayard 		 +=  $ko_bayard;
											$total_saisie_j 	 += $list["pli_saisie_adv_j"];
											$total_saisie_ok 	 += $list["pli_saisie_adv_j_ok"];
											$total_saisie_ko 	 +=  $list["pli_saisie_adv_j_ko"];
											if($source == 'courrier') $total_saisie_ci +=  $list["pli_saisie_adv_j_ci"];
											
											$content_solde_mvt .='<tr>
													<td>'.$list["date_courrier"].'</td>
													<td>'.$list["pli_total"].'</td>
													<td>'.$list["pli_saisie_adv_jmoins"].'</td>
													<td>'.$list["pli_saisie_adv_j"].'</td>
													<td>'.$list["pli_saisie_adv_j_ok"].'</td>';
											if($source == 'courrier') 
											$content_solde_mvt .='<td>'.$list["pli_saisie_adv_j_ci"].'</td>';
											$content_solde_mvt .='<td>'.$list["pli_saisie_adv_j_ko"].'</td>
													<td>'.$ko_scan.'</td>
													<td>'.$ko_def.'</td>
													<td>'.$ko_inconnu.'</td>
													<td>'.$ko_abondonne.'</td>
													<td>'.$divise.'</td>
													<td>'.$ko_circulaire.'</td>
													<td>'.$ci_editee.'</td>
													<td>'.$ci_envoyee.'</td>
													<td>'.$ko_ks.'</td>
													<td>'.$ko_ke.'</td>
													<td>'.$ok_circ.'</td>
													<td>'.$ko_bayard.'</td>
																																					  
															  
											</tr>';
										}
									}
								
								$content_solde_mvt .='<tr>
													<th>Total</th>
													<th>'.$total.'</th>
													<th>'.$total_saisie_adv_jm.'</th>
													<th>'.$total_saisie_j.'</th>
													<th>'.$total_saisie_ok.'</th>';
													if($source == 'courrier') 
								$content_solde_mvt .='<th>'.$total_saisie_ci.'</th>';
								$content_solde_mvt .='<th>'.$total_saisie_ko.'</th>
													<th>'.$total_ko_scan.'</th>
													<th>'.$total_ko_def.'</th>
													<th>'.$total_ko_inconnu.'</th>
													<th>'.$total_ko_abondonne.'</th>
													<th>'.$total_divise.'</th>
													<th>'.$total_ko_circulaire.'</th>
													<th>'.$total_ci_editee.'</th>
													<th>'.$total_ci_envoyee.'</th>
													<th>'.$total_ko_ks.'</th>
													<th>'.$total_ko_ke.'</th>
													<th>'.$total_ok_circ.'</th>
													<th>'.$total_ko_bayard.'</th>
															  
											</tr></tbody>									 
								</table>';
								echo $content_solde_mvt;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
				
				 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
							<span style="font-size:20px;"><i class="fa fa-list" style="color:#E39F00"></i>&nbsp;</span>
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">
								<?php echo $lib_societe ;?> - Suivi des mouvements</span> 
                            <ul class="header-dropdown m-r--5">
                               <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_traitement()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  									
									if(count($liste_solde_mvt_j) > 0 ){
										$content_solde_mvt = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-saisie">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date courrier</th>
												<th style="width:10%">Réception</th>
												<th style="width:10%">Saisie J--</th>
												<th style="width:10%">Saisie ce jour (A)</th>
												<th style="width:10%"><font color="#0E76BC">(A) OK</font></th>';
												if($source == 'courrier') 
												$content_solde_mvt .= '
												<th style="width:10%"><font color="#0E76BC">(A) CI</font></th>';
												$content_solde_mvt .= '<th style="width:10%"><font color="#0E76BC">(A) KO</font></th>
												<th style="width:10%">KO Scan</th>
												<th style="width:10%">KO Définitif</th>
												<th style="width:10%">KO Inconnu</th>
												<th style="width:10%">Cloturé sans traitement</th>
												<th style="width:10%">Divisé</th>
												<th style="width:10%">KO Circulaire</th>
												<th style="width:10%">Cir. éditée</th>
												<th style="width:10%">Cir. envoyée</th>
												<th style="width:10%">KO SRC</th>
												<th style="width:10%">En attente de consigne</th>
												<th style="width:10%">Ok+circulaire</th>
												<th style="width:10%">KO en cours bayard</th>
												
											</tr>
										</thead>
										<tbody   class="custom_font">';
									$total = $total_saisie_adv_jm = $total_a_saisir  = $total_hp =  $total_saisie_j =$total_saisie_ok =$total_saisie_ci = $total_saisie_ko = $total_ano_ano = $total_ci = 0;
									$total_ko_scan = $total_ko_def = $total_ko_inconnu = $total_ko_abondonne = $total_divise = $total_ko_circulaire =$total_ci_editee = $total_ci_envoyee = $total_ko_ks = $total_ko_ke = $total_ok_circ = $total_ko_bayard =0; 
									foreach($liste_solde_mvt_j as $ksolde =>$list_mvt){
										if($lib_societe == $list_mvt["nom_societe"]){
											
											$ano_ano = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['anomalie']) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['anomalie'] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['anomalie'] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['anomalie'] : 0;
											
											$ci_ci = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['ci']) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['ci'] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['ci'] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['ci'] : 0;
											
											$ko_scan = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_scan"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_scan"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_scan"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_scan"] : 0;
											
											$ko_def = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_def"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_def"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_def"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_def"] : 0;
											
											$ko_inconnu = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_inconnu"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_inconnu"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_inconnu"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_inconnu"] : 0;
											
											$ko_abondonne = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_abondonne"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_abondonne"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_abondonne"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_abondonne"] : 0;
											
											$divise = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["divise"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["divise"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["divise"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["divise"] : 0;
											
											
											$ko_circulaire = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_circulaire"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_circulaire"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_circulaire"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_circulaire"] : 0;
											
											$ci_editee = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ci_editee"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ci_editee"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ci_editee"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ci_editee"] : 0;
											
											$ci_envoyee = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ci_envoyee"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ci_envoyee"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ci_envoyee"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ci_envoyee"] : 0;
											
											$ko_ks = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_ks"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_ks"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_ks"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_ks"] : 0;
											
											$ko_ke = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_ke"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_ke"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_ke"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_ke"] : 0;

											$ok_circ = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ok_circ"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ok_circ"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ok_circ"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ok_circ"] : 0;

											$ko_bayard = (!empty($liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_bayard"]) && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_bayard"] != null && $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_bayard"] != '') ? $liste_anomalie_mvt[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]["ko_bayard"] : 0;
											
											
											
											$total 				+= $list_mvt["mvt_total"];
											$total_saisie_adv_jm 	+= $list_mvt["mvt_saisie_adv_jmoins"];
											$total_saisie_j 	+= $list_mvt["mvt_saisie_adv_j"];
											$total_saisie_ok 	+= $list_mvt["mvt_saisie_adv_j_ok"];
											$total_saisie_ko 	+=  $list_mvt["mvt_saisie_adv_j_ko"];
											$total_ano_ano 		+=  $ano_ano;
											$total_ci 	 		+=  $ci_ci;
											$total_ko_scan 	+=  $ko_scan;
											$total_ko_def 	+=  $ko_def;
											$total_ko_inconnu 	 +=  $ko_inconnu;
											$total_ko_abondonne  +=  $ko_abondonne;
											$total_divise  		 +=  $divise;
											$total_ko_circulaire +=  $ko_circulaire;
											$total_ci_editee     +=  $ci_editee;
											$total_ci_envoyee    +=  $ci_envoyee;
											$total_ko_ks   		 +=  $ko_ks;
											$total_ko_ke  		 +=  $ko_ke;
											$total_ok_circ  		 +=  $ok_circ;
											$total_ko_bayard  		 +=  $ko_bayard;
											
											if($source == 'courrier') 
												$total_saisie_ci += $list_mvt["mvt_saisie_adv_j_ci"];
											
											$content_solde_mvt .='<tr>
													<td>'.$list_mvt["date_courrier"].'</td>
													<td>'.$list_mvt["mvt_total"].'</td>
													<td>'.$list_mvt["mvt_saisie_adv_jmoins"].'</td>
													<td>'.$list_mvt["mvt_saisie_adv_j"].'</td>
													<td>'.$list_mvt["mvt_saisie_adv_j_ok"].'</td>';
											if($source == 'courrier') 
													$content_solde_mvt .='<td>'.$list_mvt["mvt_saisie_adv_j_ci"].'</td>';
													$content_solde_mvt .='<td>'.$list_mvt["mvt_saisie_adv_j_ko"].'</td>
													<td>'.$ko_scan.'</td>
													<td>'.$ko_def.'</td>
													<td>'.$ko_inconnu.'</td>
													<td>'.$ko_abondonne.'</td>
													<td>'.$divise.'</td>
													<td>'.$ko_circulaire.'</td>
													<td>'.$ci_editee.'</td>
													<td>'.$ci_envoyee.'</td>
													<td>'.$ko_ks.'</td>
													<td>'.$ko_ke.'</td>
													<td>'.$ok_circ.'</td>
													<td>'.$ko_bayard.'</td>
																								  
															  
											</tr>';
										}
									}
								 $content_solde_mvt .='<tr>
													<th>Total</th>
													<th>'.$total.'</th>
													<th>'.$total_saisie_adv_jm.'</th>
													<th>'.$total_saisie_j.'</th>
													<th>'.$total_saisie_ok.'</th>';
													if($source == 'courrier') 
													$content_solde_mvt .='<th>'.$total_saisie_ci.'</th>';
													$content_solde_mvt .='<th>'.$total_saisie_ko.'</th>
													<th>'.$total_ko_scan.'</th>
													<th>'.$total_ko_def.'</th>
													<th>'.$total_ko_inconnu.'</th>
													<th>'.$total_ko_abondonne.'</th>
													<th>'.$total_divise.'</th>
													<th>'.$total_ko_circulaire.'</th>
													<th>'.$total_ci_editee.'</th>
													<th>'.$total_ci_envoyee.'</th>
													<th>'.$total_ko_ks.'</th>
													<th>'.$total_ko_ke.'</th>
													<th>'.$total_ok_circ.'</th>
													<th>'.$total_ko_bayard.'</th>
															  
											</tr></tbody>								 
								</table>';
								echo $content_solde_mvt;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
				
				
                <!-- #END# Task Info -->
         </div>
	</div>
	</div>
	<?php
		}
	?>
	

	
