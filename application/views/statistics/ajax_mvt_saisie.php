	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="row clearfix">
	       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
								Traitement des mouvements - Saisie</span>
                            <ul class="header-dropdown m-r--5">
                               <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_traitement()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  	if(count($liste_solde_mvt_jr) > 0 ){ 
										$content_solde_mvt_jr = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-mvt">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Traitement du</th>
												<th style="width:10%"><font color="#0E76BC">Saisie finie OK</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie finie CI</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie finie KO</font></th>
												<th>KO Scan</th>
												<th>KO Définitif</th>
												<th>KO Inconnu</th>
												<th>Cloturé sans traitement</th>
												<th>Divisé</th>
												<th>Circulaire</th>
												<th>Cir. éditée</th>
												<th>Cir. envoyée</th>		
												<th>KO SRC</th>
												<th>KO en attente de consigne</th>	
												<th>OK+Circulaire</th>	
												<th>KO en cours bayard</th>	
												
											</tr>
										</thead>
										<tbody   class="custom_font">';
									$total_ano = $total_ci  = $total_saisie_ok =$total_saisie_ko = $total_saisie_ci = $total_ko_ks = $total_ko_ke = $total_ok_circ= $total_ko_bayard=0;
									$total_ko_scan = $total_ko_inc = $total_ko_aba = $total_divise = $total_kodef = $total_ci_edi = $total_ci_env = 0;
									//var_dump($liste_solde_mvt_jr);die;
									foreach($liste_solde_mvt_jr as $ksolde =>$list){

											$total_ci			+= $list["mvt_ci"];
											$total_ano 			+= $list["mvt_anomalie"];
											$total_saisie_ok 	+= $list["mvt_saisie_ok"];
											$total_saisie_ci 	+= $list["mvt_saisie_ci"];
											$total_saisie_ko    +=  $list["mvt_saisie_ko"];
											
											$total_ko_scan += $list["mvt_ko_scan"];
											$total_ko_inc  += $list["mvt_ko_inconnu"];
											$total_ko_aba  += $list["mvt_ko_abandonne"];
											$total_divise  += $list["mvt_divise"];
											$total_kodef   += $list["mvt_ko_def"];
											$total_ci 	   += $list["mvt_ko_circulaire"];
											$total_ci_edi  += $list["mvt_ko_ci_editee"];
											$total_ci_env  += $list["mvt_ko_ci_envoyee"];
											$total_ko_ks   += $list["mvt_ko_src"];
											$total_ko_ke   += $list["mvt_ko_ke"];
											$total_ok_circ   += $list["mvt_ok_circ"];
											$total_ko_bayard   += $list["mvt_ko_bayard"];
											
											$content_solde_mvt_jr .='<tr>
													<td>'.$list["dt_event"].'</td>
													<td>'.$list["mvt_saisie_ok"].'</td>
													<td>'.$list["mvt_saisie_ci"].'</td>
													<td>'.$list["mvt_saisie_ko"].'</td>
													<td>'.$list["mvt_ko_scan"].'</td>
													 <td>'.$list["mvt_ko_def"].'</td>
													 <td>'.$list["mvt_ko_inconnu"].'</td>
													 <td>'.$list["mvt_ko_abandonne"].'</td>
													 <td>'.$list["mvt_divise"].'</td>
													 <td>'.$list["mvt_ko_circulaire"].'</td>			 
													 <td>'.$list["mvt_ko_ci_editee"].'</td>
													 <td>'.$list["mvt_ko_ci_envoyee"].'</td>
													 <td>'.$list["mvt_ko_src"].'</td>
													 <td>'.$list["mvt_ko_ke"].'</td>														
													 <td>'.$list["mvt_ok_circ"].'</td>														
													 <td>'.$list["mvt_ko_bayard"].'</td>														
															  
											</tr>';
										
									}
								
								 	 $content_solde_mvt_jr .='<tr>
													<th>Total</th>
													<th>'.$total_saisie_ok.'</th>
													<th>'.$total_saisie_ci.'</th>
													<th>'.$total_saisie_ko.'</th>
													<th>'.$total_ko_scan.'</th>
													<th>'.$total_kodef.'</th>
													<th>'.$total_ko_inc.'</th>
													<th>'.$total_ko_aba.'</th>
													<th>'.$total_divise.'</th>
													<th>'.$total_ci.'</th>		
													<th>'.$total_ci_edi.'</th>
													<th>'.$total_ci_env.'</th>
													<th>'.$total_ko_ks.'</th>
													<th>'.$total_ko_ke.'</th>
													<th>'.$total_ok_circ.'</th>
													<th>'.$total_ko_bayard.'</th>
															  
											</tr></tbody>									 
								</table>';
								echo $content_solde_mvt_jr;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
			</div>
	</div>
	