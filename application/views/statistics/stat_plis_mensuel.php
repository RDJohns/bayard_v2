<?php
$url_src    = base_url().'src/';
$url_js     = base_url().'src/';
$date_debut = $_REQUEST["date_debut"];
$date_fin   = $_REQUEST["date_fin"];



?>
<!--div class="row"-->
<?php
$pli_total = 0;
$nouveau_percent  = $non_traite = 0;
$encours_percent  = $encours = 0;
$traite_percent   = $traite = 0;
$anomalie_percent = $anomalie = 0;
$ferme_percent    = $ferme = 0;
$divise_percent       = $divise = 0;
$ci_traite_percent    = $ci_traite = 0;

$ko_scan 		= $ko_definitif =0 ;
$ko_inconnu 	= $ko_src = 0;
$ko_en_attente  = $ko_circulaire = 0;
$ci_editee 		= 0;
$ok_ci 		    = 0;
$ko_encours 	= 0;
// non_traite => flag_traitement = 0 or null
// traité => pli cloturé ==> statut_saisie (8,10,24) + flag traitement (9,14)
// en cours  => f_reception_gloable
// total ==> nombre de pli total trouvé
// cloturé  ==> OK, CI envoyé, cloturé sans traitement, Divisé
foreach($liste_pli as $pli){
    $pli_total += $pli['total'];
	
    if ($pli['non_traite'] > 0) $non_traite += $pli['non_traite'];
    if ($pli['encours'] > 0)    $encours   += $pli['encours'];
    if ($pli['traite'] > 0)     $traite    += $pli['traite'];
    if ($pli['anomalie'] > 0)   $anomalie  += $pli['anomalie'];
    if ($pli['ferme'] > 0)      $ferme 	   += $pli['ferme'];
    if ($pli['divise'] > 0)     $divise    += $pli['divise'];
    if ($pli['ci_traite'] > 0)  $ci_traite += $pli['ci_traite'];
}

foreach($list_pli_ko as $pliko){

    if ($pliko['ko_scan'] > 0)    		$ko_scan += $pliko['ko_scan'];
    if ($pliko['ko_definitif'] > 0)    	$ko_definitif += $pliko['ko_definitif'];
    if ($pliko['ko_inconnu'] > 0)     	$ko_inconnu += $pliko['ko_inconnu'];
    if ($pliko['ko_src'] > 0)   		$ko_src += $pliko['ko_src'];
    if ($pliko['ko_en_attente'] > 0)    $ko_en_attente += $pliko['ko_en_attente'];
    if ($pliko['ko_circulaire'] > 0)    $ko_circulaire += $pliko['ko_circulaire'];
    if ($pliko['ci_editee'] > 0)  	    $ci_editee += $pliko['ci_editee'];
    if ($pliko['ok_ci'] > 0)  	        $ok_ci += $pliko['ok_ci'];
    if ($pliko['ko_encours'] > 0)  	    $ko_encours += $pliko['ko_encours'];
}

$nouveau_percent   = ($pli_total > 0) ? $non_traite*100 / $pli_total : 0 ;
$encours_percent   = ($pli_total > 0) ? $encours*100 / $pli_total: 0 ;
$traite_percent    = ($pli_total > 0) ? $traite*100 / $pli_total: 0 ;
$anomalie_percent  = ($pli_total > 0) ? $anomalie*100 / $pli_total: 0 ;
$ferme_percent     = ($pli_total > 0) ? $ferme*100 / $pli_total: 0 ;
$divise_percent    = ($pli_total > 0) ? $divise*100 / $pli_total: 0 ;
$ci_traite_percent = ($pli_total > 0) ? $ci_traite*100 / $pli_total: 0 ;

$nouveau_percent   = ($nouveau_percent > 0) ? number_format($nouveau_percent,2) : 0;
$encours_percent   = ($encours_percent > 0) ? number_format($encours_percent,2) : 0;
$traite_percent    = ($traite_percent > 0) ? number_format($traite_percent,2) : 0;
$anomalie_percent  = ($anomalie_percent > 0) ? number_format($anomalie_percent,2) : 0;
$ferme_percent     = ($ferme_percent > 0) ? number_format($ferme_percent,2) : 0;
$divise_percent    = ($divise_percent > 0) ? number_format($divise_percent,2) : 0;
$ci_traite_percent = ($ci_traite_percent > 0) ? number_format($ci_traite_percent,2) : 0;

$kscan_percent   = ($pli_total > 0) ? $ko_scan*100 / $pli_total : 0 ;
$kscan_percent   = ($kscan_percent > 0) ? number_format($kscan_percent,2) : 0;
$kdef_percent   = ($pli_total > 0) ? $ko_definitif*100 / $pli_total : 0 ;
$kdef_percent   = ($kdef_percent > 0) ? number_format($kdef_percent,2) : 0;
$kinc_percent   = ($pli_total > 0) ? $ko_inconnu*100 / $pli_total : 0 ;
$kinc_percent   = ($kinc_percent > 0) ? number_format($kinc_percent,2) : 0;
$ksrc_percent   = ($pli_total > 0) ? $ko_src*100 / $pli_total : 0 ;
$ksrc_percent   = ($ksrc_percent > 0) ? number_format($ksrc_percent,2) : 0;
$katt_percent   = ($pli_total > 0) ? $ko_en_attente*100 / $pli_total : 0 ;
$katt_percent   = ($katt_percent > 0) ? number_format($katt_percent,2) : 0;
$ci_percent   = ($pli_total > 0) ? $ko_circulaire*100 / $pli_total : 0 ;
$ci_percent   = ($ci_percent > 0) ? number_format($ci_percent,2) : 0;
$ci_edi_percent   = ($pli_total > 0) ? $ci_editee*100 / $pli_total : 0 ;
$ci_edi_percent   = ($ci_edi_percent > 0) ? number_format($ci_edi_percent,2) : 0;
$ok_ci_percent   = ($pli_total > 0) ? $ok_ci*100 / $pli_total : 0 ;
$ok_ci_percent   = ($ok_ci_percent > 0) ? number_format($ok_ci_percent,2) : 0;
$kencours_percent   = ($pli_total > 0) ? $ko_encours*100 / $pli_total : 0 ;
$kencours_percent   = ($kencours_percent > 0) ? number_format($kencours_percent,2) : 0;

?>

<!-- Widgets -->
<div class="row clearfix" style="margin-top:125px;text-align:center;text-align:center;">

    <!--div class="col-lg-1 col-md-1 col-sm-6 col-xs-12">
    </div-->
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-cyan-t hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-envelope"></i>
            </div>
            <div class="content">
                <div class="text">Plis total</div>
                <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo $pli_total;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-cyan hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-plus-square"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $nouveau_percent;?> %</span></div>
                <div class="text">Non traité<span ></div>
                <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo $non_traite;?></div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-orange hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-cogs"></i>
            </div>
            <div class="content">
				<div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $encours_percent;?> %</span></div>
                <div class="text">En cours</div>
                <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"><?php echo $encours;?></div>
            </div>
        </div>
    </div>
   <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-light-green hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-check-square"></i>
            </div>
            <div class="content">
                 <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $traite_percent;?> %</span></div>
				 
				 <div class="text">Cloturé</div>
                <div class="number count-to" data-from="0" data-to="242" data-speed="1000" data-fresh-interval="20"><?php echo $traite;?></div>
            </div>
        </div>
    </div>

    <!--div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-teal hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa "></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $ok_ci_percent;?> %</span></div>
                <div class="text">OK + CI</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ok_ci;?></div>
            </div>
        </div>
    </div-->
	 
	 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-green hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
				<div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $ci_edi_percent;?> %</span></div>
                <div class="text">Cir. éditée</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ci_editee;?></div>
            </div>
        </div>
    </div>
	
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
				<div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $kscan_percent;//$anomalie_percent;?> %</span></div>
                <div class="text">KO SCAN</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_scan;?></div>
            </div>
        </div>
    </div>
	 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
				<div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $kdef_percent;?> %</span></div>
                <div class="text">KO Définitif</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_definitif;?></div>
            </div>
        </div>
    </div>
	 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
				<div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $kinc_percent;?> %</span></div>
                <div class="text">KO Inconnu</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_inconnu;?></div>
            </div>
        </div>
    </div>
	 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
				<div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $ksrc_percent;?> %</span></div>
                <div class="text">KO SRC</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_src;?></div>
            </div>
        </div>
    </div>
	<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
				<div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $katt_percent;?> %</span></div>
                <div class="text">KO En attente</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_en_attente;?></div>
            </div>
        </div>
    </div>
	<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
				<div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $ci_percent;?> %</span></div>
                <div class="text">KO Circulaire</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_circulaire;?></div>
            </div>
        </div>
    </div>

    <!--div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $kencours_percent;?> %</span></div>
                <div class="text">KO En cours</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_encours;?></div>
            </div>
        </div>
    </div-->
	
    <!--div class="col-lg-1 col-md-1 col-sm-6 col-xs-12">
    </div-->
</div>
</br>
<div class="col-md-12 col-sm-12 col-xs-12" id="tab_menseuel_0" style="display:;margin-top:10px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12" >
                <div class="card">
                    <div class="header">
                        <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Réception - Suivi des plis</span>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <a onclick="quitter_tab_mensuel_0()" style="cursor: pointer;">
                                        <i class="fa fa-close"> </i></a>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <?php
							
                             $content_recep_mois = '<table style="width: 100%;" class="table table-bordered table-striped table-hover dataTable table-stat-mensuel-typologie">  
                                <thead   class="custom_font">
                                   <tr>
                                        <th  style="width:10%;text-align:center;"  rowspan="2">Réception du</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Total</th>
                                        <!--th  style="width:10%;text-align:center"  rowspan="2">En cours</th-->
										<th  style="width:10%;text-align:center"  colspan="2">Restant à traiter</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Cloturé</th>
										<!--th  style="width:10%;text-align:center"  rowspan="2">OK + CI</th-->
										<th  style="width:10%;text-align:center"  rowspan="2">KO Circulaire</th>										
										<th  style="width:10%;text-align:center"  rowspan="2">KO SCAN</th>
										<th  style="width:10%;text-align:center"  rowspan="2">KO Définitf</th>
										<th  style="width:10%;text-align:center"  rowspan="2">KO Inconnu</th>
										<th  style="width:10%;text-align:center"  rowspan="2">KO SRC</th>
										<th  style="width:10%;text-align:center"  rowspan="2">KO En attente</th>
										<!--th  style="width:10%;text-align:center"  rowspan="2">KO En cours</th-->
										<th  style="width:10%;text-align:center"  rowspan="2">CI </br>Editée</th>										
										<th  style="width:40%;text-align:center"  colspan="4">Détail Cloturé</th>										
                                      
                                    </tr>
                                    <tr>
                                       
                                        <th  style="width:10%;text-align:center">En cours</th>
                                        <th  style="width:10%;text-align:center">Non traité</th>
                                        <th  style="width:10%;text-align:center">OK</th>
										<th  style="width:10%;text-align:center">CI envoyé</th>
										<th  style="width:10%;text-align:center">Cloturé sans traitement</th>
										<th  style="width:10%;text-align:center">Divisé</th>																
										
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';

                            $j0 = 0;
                            $k =0;
                            //global
                            $data_set_cloture = array();
                            $data_set_encours = array();
                            $data_set_nontraite = array();
                            $data_set_anomalie_glb = array();
                            $data_set_labels  = array();
                                                       
							// $data_set_anomalie_glb = array();
                            $data_set_ko_circulaire = array();
                            $data_set_ko_scan = array();
                            $data_set_ko_def = array();
                            $data_set_ko_inc = array();
                            $data_set_ko_src = array();
                            $data_set_ko_att = array();
                            $data_set_ci_editee = array();
                            $data_set_labels  = array();
                            //ko
                            $data_set_ferme    = array();
                            $data_set_ok = array();
                            $data_set_ci_envoyee    = array();
                            $data_set_divise    = array();
			    
                           
                            foreach($liste_pli as $k_plis =>$tab_plis){
                                //préparaion des données pour les graphes reception global
                                if(intval($tab_plis["total"])>0){
                                    $data_set_cloture[$k]   = $tab_plis["traite"];
                                    $data_set_encours[$k]   = $tab_plis["encours"];
                                    $data_set_nontraite[$k] = $tab_plis["non_traite"];
                                   // $data_set_anomalie_glb[$k]  = $tab_plis["anomalie"];
                                    $data_set_labels[$k]    	= $tab_plis["lib_daty"];
                                    //préparaion des données pour les graphes Total KO
                                                                       
									//$data_set_anomalie_glb[$k]  = $tab_plis["anomalie"];
                                    $data_set_ko_circulaire[$k] = $tab_plis["ko_ko_circulaire"];
                                    $data_set_ko_scan[$k]     	= $tab_plis["ko_ko_scan"];
                                    $data_set_ko_def[$k]        = $tab_plis["ko_ko_definitif"];
                                    $data_set_ko_inc[$k]        = $tab_plis["ko_ko_inconnu"];
                                    $data_set_ko_src[$k]        = $tab_plis["ko_src"];
                                    $data_set_ko_att[$k]        = $tab_plis["ko_ko_en_attente"];
                                    $data_set_ci_editee[$k]     = $tab_plis["ci_ci_editee"];
                                    $data_set_labels[$k]    	= $tab_plis["lib_daty"];
                                    //préparaion des données pour les graphes Total KO
                                    $data_set_ferme[$k]     = $tab_plis["ko_ko_abandonne"];
                                    $data_set_ok[$k]   		= $tab_plis["ok"];
                                    $data_set_ci_envoyee[$k]= $tab_plis["ci_ci_envoyee"];
                                    $data_set_divise[$k]    = $tab_plis["divise"];
                                 
									$lib = ($granu == 'm') ? '<a style="cursor:pointer;" onclick="get_detail_mensuel(\''.$tab_plis["daty"].'\')">'.$tab_plis["lib_daty"].'</a>' : $tab_plis["lib_daty"];
                                    $k++;
                                    $content_recep_mois .= '<tr>
                                        <td style="text-align:center">'.$lib.'</td>
                                        <td style="text-align:center">'.$tab_plis["total"].'</td>
										<td style="text-align:center">'.$tab_plis["encours"].'</td>
										<td style="text-align:center">'.$tab_plis["non_traite"].'</td>
										 <td style="text-align:center">'.$tab_plis["traite"].'</td>										 
										<!--td style="text-align:center">'.$tab_plis["ok_ci_ci"].'</td-->
										<td style="text-align:center">'.$tab_plis["ko_ko_circulaire"].'</td>
										<td style="text-align:center">'.$tab_plis["ko_ko_scan"].'</td>
										<td style="text-align:center">'.$tab_plis["ko_ko_definitif"].'</td>
										<td style="text-align:center">'.$tab_plis["ko_ko_inconnu"].'</td>
										<td style="text-align:center">'.$tab_plis["ko_src"].'</td>
										<td style="text-align:center">'.$tab_plis["ko_ko_en_attente"].'</td>										
										<!--td style="text-align:center">'.$tab_plis["ko_ko_encours"].'</td-->										
										<td style="text-align:center">'.$tab_plis["ci_ci_editee"].'</td>
										<td style="text-align:center">'.$tab_plis["ok"].'</td>
										<td style="text-align:center">'.$tab_plis["ci_ci_envoyee"].'</td>
										<td style="text-align:center">'.$tab_plis["ko_ko_abandonne"].'</td>										
										<td style="text-align:center">'.$tab_plis["divise"].'</td>
										
										</tr>';
                                }
                            }
                            $content_recep_mois .='</tbody>
                            </table>';
                            echo $content_recep_mois;
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <!-- #END# Task Info -->
            <!-- Browser Usage -->

        </div>
    </div>
    </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="tab_detail_mensuel" style="display:none;margin-top:50px;" >
  </div>  


<div class="col-md-12 col-sm-12 col-xs-12" id="tab_menseuel_0" style="margin-top:50px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12" >
                <div class="card">
                    <div class="header">
                        <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Réception - Plis passés en KO</span>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <a onclick="quitter_tab_mensuel_0()" style="cursor: pointer;">
                                        <i class="fa fa-close"> </i></a>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <?php
                            $content_recep_mois = '<table style="width: 100%;" class="table table-bordered table-striped table-hover dataTable table-stat-mensuel-ko">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:10%;text-align:center;">Réception du</th>
                                        <th  style="width:10%;text-align:center">Total</th>
                                        <th  style="width:10%;text-align:center">KO Circulaire</th>										
										<th  style="width:10%;text-align:center">KO SCAN</th>
										<th  style="width:10%;text-align:center">KO Définitf</th>
										<th  style="width:10%;text-align:center">KO Inconnu</th>
										<th  style="width:10%;text-align:center">KO SRC</th>
										<th  style="width:10%;text-align:center">KO En attente</th>
										<!--th  style="width:10%;text-align:center">KO En cours</th-->
										<th  style="width:10%;text-align:center">CI </br>Editée</th>
										<th  style="width:10%;text-align:center">CI envoyé</th>
										<th  style="width:10%;text-align:center">Cloturé sans traitement</th>
										<th  style="width:10%;text-align:center">Divisé</th>																
										
                                    </tr>
                                  
                                </thead>
                                <tbody   class="custom_font">';

                            $j0 = 0;
                            $k  = 0;                           
							
                            foreach($list_histo_ko as $k_plis =>$tab_ko){ 
                                
                                if(intval($tab_ko["ko_circulaire"])>0 || intval($tab_ko["ko_scan"])>0 || intval($tab_ko["ko_definitif"])>0
								|| intval($tab_ko["ko_inconnu"])>0|| intval($tab_ko["ko_src"])>0|| intval($tab_ko["ko_en_attente"])>0|| intval($tab_ko["ci_editee"])>0
								|| intval($tab_ko["ci_envoyee"])>0|| intval($tab_ko["ko_abandonne"])>0|| intval($tab_ko["divise"])>0){
                                    
                                    
									$k++;
                                    $content_recep_mois .= '<tr>
                                        <td style="text-align:center">'.$tab_ko["lib_daty"].'</td>
                                        <td style="text-align:center">'.$tab_ko["pli_total"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_circulaire"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_scan"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_definitif"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_inconnu"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_src"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_en_attente"].'</td>										
										<!--td style="text-align:center">'.$tab_ko["ko_encours"].'</td-->										
										<td style="text-align:center">'.$tab_ko["ci_editee"].'</td>
										<td style="text-align:center">'.$tab_ko["ci_envoyee"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_abandonne"].'</td>										
										<td style="text-align:center">'.$tab_ko["divise"].'</td>
									</tr>';
                                }
                            }
                            $content_recep_mois .='</tbody>
                            </table>';
                            echo $content_recep_mois;
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <!-- #END# Task Info -->
            <!-- Browser Usage -->

        </div>
    </div>  
   
</div>



<div class="row" style="padding:10px 0px 0px 0px;"  style="margin-top:50px;">


    <input type="hidden" value="<?php //echo $data_donut_document ;?>" id="graph_donut_doc"/>
    <input type="hidden" value="<?php //echo $data_donut_plis_traite ;?>" id="graph_donut"/>
    <input type="hidden" value="<?php //echo $data_donut_plis_ko ;?>" id="graph_donut_plis_ko"/>
    <input type="hidden" value="<?php //echo $data_donut_plis_restant;?>" id="graph_donut_plis_restant"/>

    <div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab_pli_mpt_mesuel" display="display:">     

    <div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab_pli_cloture_mesuel" display="display:;" style="margin-bottom:50px;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12"  style="margin-top:50px;">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Plis <b>cloturés</b> par typologie</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <a onclick="quitter_pli_cloture_mensuel()" style="cursor: pointer;">
                                            <i class="fa fa-close"></i></a>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <?php 
								
                                $content_pli_cloture = '<table class="table table-bordered table-striped table-hover dataTable table-stat-mensuel-typologie" style="width: 100%;">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:10%;text-align:center;background: white !important;">Réception du</th>
										<th>Non d&eacute;finie</th>';
										for($jj=2; $jj<count($tab_typo)+2;$jj++){
											 $content_pli_cloture .= '<th>'.$tab_typo[$jj].'</th>';
										}
                                       
                                   $content_pli_cloture .= ' </tr>
                                </thead>
                                <tbody   class="custom_font">';
                                $arrInfo_2 = array();
								$arrInfo_3 = array();
								$arrInfo_4 = array();
								$arrInfo_5 = array();
								$arrInfo_6 = array();
								$arrInfo_7 = array();
								$arrInfo_8 = array();
								$arrInfo_9 = array();
								$arrInfo_10 = array();
								$arrInfo_11 = array();
								$arrInfo_12 = array();
								$arrInfo_13 = array();
								$arrInfo_14 = array();
								$arrInfo_15 = array();
								$arrInfo_16 = array();
								$arrInfo_17 = array();
								$arrInfo_18 = array();
								$arrInfo_19 = array();
								$arrInfo_20 = array();
								$arrInfo_21 = array();
								$arrInfo_22 = array();
								$arrInfo_23 = array();
								$arrInfo_24 = array();
								$arrInfo_25 = array();
								$arrInfo_26 = array();
								$arrInfo_27 = array();
								$mois_verif = ""; // pour vérifier si le mois a changé dans la liste des données
                                $k=0;
								
                                foreach($liste_pli_typo as $k_plis_cltr =>$tab_plis_cltr){
                                    if($tab_plis_cltr["info_0"] + $tab_plis_cltr["info_2"] +  $tab_plis_cltr["info_3"] +  $tab_plis_cltr["info_4"] +  $tab_plis_cltr["info_5"] +  $tab_plis_cltr["info_6"] +  $tab_plis_cltr["info_7"] +  $tab_plis_cltr["info_8"] +  $tab_plis_cltr["info_9"] +  $tab_plis_cltr["info_10"] +  $tab_plis_cltr["info_11"] +  $tab_plis_cltr["info_12"] +  $tab_plis_cltr["info_13"] +  $tab_plis_cltr["info_14"] +  $tab_plis_cltr["info_15"] +  $tab_plis_cltr["info_16"] +  $tab_plis_cltr["info_17"] +  $tab_plis_cltr["info_18"] +  $tab_plis_cltr["info_19"] +  $tab_plis_cltr["info_20"] +  $tab_plis_cltr["info_21"] +  $tab_plis_cltr["info_22"] +  $tab_plis_cltr["info_23"] +  $tab_plis_cltr["info_24"] +  $tab_plis_cltr["info_25"] +  $tab_plis_cltr["info_26"] +  $tab_plis_cltr["info_27"]>0) {
                                       
                                       /* $arrInfo_2[$k]   += $tab_plis_cltr["info_2"];
                                        $arrInfo_3[$k]   += $tab_plis_cltr["info_3"];
                                        $arrInfo_4[$k]   += $tab_plis_cltr["info_4"];
                                        $arrInfo_5[$k]   += $tab_plis_cltr["info_5"];
                                        $arrInfo_6[$k]   += $tab_plis_cltr["info_6"];
                                        $arrInfo_7[$k]   += $tab_plis_cltr["info_7"];
                                        $arrInfo_8[$k]   += $tab_plis_cltr["info_8"];
                                        $arrInfo_9[$k]   += $tab_plis_cltr["info_9"];
                                        $arrInfo_10[$k]   += $tab_plis_cltr["info_10"];
                                        $arrInfo_11[$k]   += $tab_plis_cltr["info_11"];
                                        $arrInfo_12[$k]   += $tab_plis_cltr["info_12"];
                                        $arrInfo_13[$k]   += $tab_plis_cltr["info_13"];
                                        $arrInfo_14[$k]   += $tab_plis_cltr["info_14"];
                                        $arrInfo_15[$k]   += $tab_plis_cltr["info_15"];
                                        $arrInfo_16[$k]   += $tab_plis_cltr["info_16"];
                                        $arrInfo_17[$k]   += $tab_plis_cltr["info_17"];
                                        $arrInfo_18[$k]   += $tab_plis_cltr["info_18"];
                                        $arrInfo_19[$k]   += $tab_plis_cltr["info_19"];
                                        $arrInfo_20[$k]   += $tab_plis_cltr["info_20"];
                                        $arrInfo_21[$k]   += $tab_plis_cltr["info_21"];
                                        $arrInfo_22[$k]   += $tab_plis_cltr["info_22"];
                                        $arrInfo_23[$k]   += $tab_plis_cltr["info_23"];
                                        $arrInfo_24[$k]   += $tab_plis_cltr["info_24"];
                                        $arrInfo_25[$k]   += $tab_plis_cltr["info_25"];
                                        $arrInfo_26[$k]   += $tab_plis_cltr["info_26"];
                                        $arrInfo_27[$k]   += $tab_plis_cltr["info_27"];*/

                                       
                                        //$k++;
                                        $content_pli_cloture .= '<tr>
                                        <td style="text-align:center;background: white !important;">' . $tab_plis_cltr["date_courrier"] . '</td>
                                        <td style="text-align:center">' .$tab_plis_cltr["info_0"]. '</td>
                                        <td style="text-align:center">' .$tab_plis_cltr["info_2"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_3"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_4"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_5"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_6"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_7"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_8"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_9"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_10"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_11"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_12"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_13"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_14"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_15"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_16"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_17"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_18"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_19"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_20"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_21"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_22"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_23"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_24"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_25"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_26"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_27"]. '</td>


										</tr>';
                                    }
                                }
                                $content_pli_cloture .='</tbody>
                            </table>';
                                echo $content_pli_cloture;
						
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            		
							
				<!-- fin Plis avec moyen de paiement-->
				<!-- debut BDC et autres plis cloturés-->
				<!--div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
					<div class="row clearfix">
						<div class="col-xs-9 col-sm-12 col-md-12 col-lg-12"  style="margin-top:50px;">
							<div class="card">
								<div class="header">
									<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Plis cloturés en mode graphe</span>
								</div>
								<div class="body">
									<canvas id="chart-plis-cloture" width="auto" height="auto"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div-->
				<!-- fin BDC et autres plis cloturés-->

            </div>
        </div>
    </div>

    <!-- Browser Usage -->
	
   <input type="hidden" id="typo" name="typo" value="<?php echo $lib_typo?>">
   <script>
	 
        //recep global
  //      var cloture = <?php echo json_encode($data_set_cloture);?>;
 //       var encours = <?php echo json_encode($data_set_encours);?>;
  //      var nontraite = <?php echo json_encode($data_set_nontraite);?>;
        //var anomalie = <?php echo json_encode($data_set_anomalie_glb);?>;
  //      var labels = <?php echo json_encode($data_set_labels);?>;
        
		//ko data   
	
/*
        var ko_circulaire = <?php echo json_encode($data_set_ko_circulaire);?>;
        var ko_scan = <?php echo json_encode($data_set_ko_scan);?>;
        var ko_def = <?php echo json_encode($data_set_ko_def);?>;
        var ko_inconnu = <?php echo json_encode($data_set_ko_inc);?>;
        var ko_src = <?php echo json_encode($data_set_ko_src);?>;
        var ko_ke = <?php echo json_encode($data_set_ko_att);?>;
        var ci_editee = <?php echo json_encode($data_set_ci_editee);?>;
        var labels = <?php echo json_encode($data_set_labels);?>;
        //ko data
        var ferme = <?php echo json_encode($data_set_ferme);?>;
        var ok = <?php echo json_encode($data_set_ok);?>;
        var ci_envoyee = <?php echo json_encode($data_set_ci_envoyee);?>;
        var divise = <?php echo json_encode($data_set_divise);?>;
    */   
         //plis cloturés
        //var arrInfo_2 = <?php echo json_encode($arrInfo_2);?>;
       // var arrInfo_3 = <?php echo json_encode($arrInfo_3);?>;
 /*       var arrInfo_2=<?php echo json_encode($arrInfo_2);?>;
		var arrInfo_3=<?php echo json_encode($arrInfo_3);?>;
		var arrInfo_4=<?php echo json_encode($arrInfo_4);?>;
		var arrInfo_5=<?php echo json_encode($arrInfo_5);?>;
		var arrInfo_6=<?php echo json_encode($arrInfo_6);?>;
		var arrInfo_7=<?php echo json_encode($arrInfo_7);?>;
		var arrInfo_8=<?php echo json_encode($arrInfo_8);?>;
		var arrInfo_9=<?php echo json_encode($arrInfo_9);?>;
		var arrInfo_10=<?php echo json_encode($arrInfo_10);?>;
		var arrInfo_11=<?php echo json_encode($arrInfo_11);?>;
		var arrInfo_12=<?php echo json_encode($arrInfo_12);?>;
		var arrInfo_13=<?php echo json_encode($arrInfo_13);?>;
		var arrInfo_14=<?php echo json_encode($arrInfo_14);?>;
		var arrInfo_15=<?php echo json_encode($arrInfo_15);?>;
		var arrInfo_16=<?php echo json_encode($arrInfo_16);?>;
		var arrInfo_17=<?php echo json_encode($arrInfo_17);?>;
		var arrInfo_18=<?php echo json_encode($arrInfo_18);?>;
		var arrInfo_19=<?php echo json_encode($arrInfo_19);?>;
		var arrInfo_20=<?php echo json_encode($arrInfo_20);?>;
		var arrInfo_21=<?php echo json_encode($arrInfo_21);?>;
		var arrInfo_22=<?php echo json_encode($arrInfo_22);?>;
		var arrInfo_23=<?php echo json_encode($arrInfo_23);?>;
		var arrInfo_24=<?php echo json_encode($arrInfo_24);?>;
		var arrInfo_25=<?php echo json_encode($arrInfo_25);?>;
		var arrInfo_26=<?php echo json_encode($arrInfo_26);?>;
		var arrInfo_27=<?php echo json_encode($arrInfo_27);?>;
*/
        
    </script>
    <!--script src="<?php // echo base_url().'src/js/statistics/graphe.js'; ?>"></script-->
