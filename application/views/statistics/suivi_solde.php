<script>
    var url_site = '<?php echo site_url(); ?>';
</script>
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="header-top-area">
         </div> 
    </div>
    <br />

    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="">
				<div class="sparkline13-hd" style="padding:5px 0px 0px 20px" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="main-sparkline13-hd">
					<div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin:0px 0px 20px 0px">
							<!--h1 style="color:#69B145"><i class="fa fa-envelope-square" style="color:#C62057"></i>
							Suivi des réceptions</h1-->  
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							
						</div>
						</div>
					</div>
					
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
						<span class="pull-right" style="font-size:13px;margin-top:8px;"> Courrier du </span></div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="form-group" nowrap="nowrap">
								<div class="input-daterange input-group" id="bs_datepicker_range_container">
									<div class="form-line">
										<input type="text" id="date_debut" name="date_debut" class="form-control" placeholder="" value="<?php echo date("d/m/Y");?>">
									</div>
										<span class="input-group-addon">au</span>
									<div class="form-line">
										<input type="text" id="date_fin" name="date_fin" class="form-control" placeholder="" value="<?php echo date("d/m/Y");?>">
									</div>
                                 </div>
												 
							</div>
						</div>		
					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
						 <div class="form-group">
							 <div class="row clearfix">
								<select class="btn-group bootstrap-select form-control show-tick" id="select_soc" name="select_soc">
									<option value="">-- Société --</option>
										
								</select>
							</div>	
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						 <div class="form-group">
							 <div class="row clearfix">
								<select class="btn-group bootstrap-select form-control show-tick" id="select_cat_typo" name="select_cat_typo">
																			
								</select>
							</div>	
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<input name="source_visu" value="courrier" type="radio" id="courrier" class="with-gap radio-col-cyan" checked />
						<label for="courrier" style="width:100px;">Courrier</label>
						<input name="source_visu" value="sftp" type="radio" id="sftp" class="with-gap radio-col-deep-orange" />
						<label for="sftp" style="width:100px;">SFTP</label>
						<input name="source_visu"  value="mail" type="radio" id="mail" class="with-gap radio-col-light-green" />
						<label for="mail">Mail</label>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
						 <div class="form-group">
							
								<button class="btn btn-primary waves-effect waves-light" id="stat_rech" onclick="suivi();">
								<i class="fa fa-search"></i>&nbsp;Rechercher</button>
								
						</div>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					</div>
					</div> 
					<div id="result-rsrch" style="display: block;">					
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					    <div class="">
                            <div class="sparkline13-graph">
									<div class="datatable-dashv1-list custom-datatable-overright">
										<div id="toolbar">
											
										</div>
									
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="result-rsrch-saisie" style="display: block;">					
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					    <div class="">
                            <div class="sparkline13-graph">
									<div class="datatable-dashv1-list custom-datatable-overright">
										<div id="toolbar">
											
										</div>
									
									</div>
								</div>
							</div>
						</div>
					</div>
				
            </div>
        </div>
    </div>
	</br>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
	<div class="col-lg-5"></div>	
		<div class="col-lg-2">
			<div class="alert bg-pink" id="message_alert" style="display:none;margin-bottom:0px !important;"></div>	
		</div>	
	<div class="col-lg-5"></div>	
	<div class="preloader" id="preloader_solde">
			<div class="spinner-layer pl-black">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
	</div>	
	</div>	
    <div class="library-book-area mg-t-30">
        <div class="container-fluid">
        </div>
    </div>
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
    </div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:50px">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="id_pli_statut_att"></div>		
	</div>
    <div class="courses-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
        <div class="footer-copyright-area" style="position: fixed !important; bottom:0 !important; width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright © 2018. All rights reserved DEV-SI VIVETIC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
