<?php
 $url_src    = base_url().'src/';
 $url_js     = base_url().'src/';
 $date_debut = $_REQUEST["date_debut"];
 $date_fin   = $_REQUEST["date_fin"];
 

?>
	 <!--div class="row"-->

	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab0" style="margin-bottom:50px;display: ">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Réception et traitement</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab0()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
							</ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							
							$content_type_recep_date = '<table class="table table-bordered table-striped table-hover dataTable js-export-table-synthese"  style="width:100% !important;">  
                                <thead   class="custom_font">
									<tr>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Date réception</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Nbr. total plis réçu</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Date dernier traitement</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total cloturé</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total En cours</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total non traité</th>
										<th  style="width:40%;text-align:center"  colspan="8">Total KO</th>										
                                      
                                    </tr>
                                    <tr>
                                       
                                        <th  style="width:10%;text-align:center">Total KO Scan</th>
										<th  style="width:10%;text-align:center">Total KO Inconnu</th>
										<th  style="width:10%;text-align:center">Total KO Call</th>
										<th  style="width:10%;text-align:center">Total KO Circulaire</th>
										<th  style="width:10%;text-align:center">Total KO En attente</th>
										<th  style="width:10%;text-align:center">Total KO Réclam<br>mation</th>
										<th  style="width:10%;text-align:center">Total KO Litige</th>
										<th  style="width:10%;text-align:center">Total KO Définitif</th>
										
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">'; 									
                                  
									$j0 = 0;
									foreach($liste_result_plis_traitement_date as $k_plis_ttt_par_date =>$tab_plis_ttt_par_date){
										
										$content_type_recep_date .= '<tr>
                                        <td style="text-align:center">'.$tab_plis_ttt_par_date["date_courrier"].'</td>
                                        <td style="text-align:center">'.$liste_result_plis_reception[$tab_plis_ttt_par_date["date_courrier"]].'</td>
                                        <td style="text-align:center">'.$tab_plis_ttt_par_date["date_dernier_traitement"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["cloture"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["encours"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["non_traite"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["ko_scan"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["ko_inconnu"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["ko_call"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["ko_circulaire"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["ko_en_attente"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["ko_reclammation"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["ko_litige"].'</td>
										<td style="text-align:center">'.$tab_plis_ttt_par_date["ko_def"].'</td>
										</tr>';
										
									}
								 $content_type_recep_date .='</tbody>
                            </table>';
							echo $content_type_recep_date;
							
							
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab01" style="margin-bottom:50px;display:">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Réception et traitement Hebdo</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab01()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_type_recep_sem = '<table class="table table-bordered table-striped table-hover dataTable js-export-table-synthese"  style="width:100% !important;">  
                                <thead   class="custom_font">
									<tr>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Semaine de réception</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Nbr. total plis réçu</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Sem Traitement</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Total cloturé</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total En cours</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total non traité</th>
										<th  style="width:40%;text-align:center"  colspan="8">Total KO</th>										
                                      
                                    </tr>
                                    <tr>
                                       
                                        <th  style="width:10%;text-align:center">Total KO Scan</th>
										<th  style="width:10%;text-align:center">Total KO Inconnu</th>
										<th  style="width:10%;text-align:center">Total KO Call</th>
										<th  style="width:10%;text-align:center">Total KO Circulaire</th>
										<th  style="width:10%;text-align:center">Total KO En attente</th>
										<th  style="width:10%;text-align:center">Total KO Réclam<br>mation</th>
										<th  style="width:10%;text-align:center">Total KO Litige</th>
										<th  style="width:10%;text-align:center">Total KO Définitif</th>
										
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">'; 									
                                  
									$j0  = 0;
									$tab_plis_recep_hebdo = array();
									$aux_rec      = 0;
									$aux_date_rec = "";
									$tab_col_span = array();
									/*echo "<pre>";
									print_r($liste_date["num"]);
									echo "</pre>";*/
									foreach($liste_result_plis_traitement_date as $k_plis_ttt_par_date =>$tab_plis_recep_par_sem)
									{
										//echo $tab_plis_recep_par_sem["date_dernier_traitement"].$tab_plis_recep_par_sem["date_courrier"]."</br>";
										
										$num_semaine     = $liste_date["num"][$tab_plis_recep_par_sem["date_courrier"]];
										$num_semaine_ttt = ($tab_plis_recep_par_sem["date_dernier_traitement"] != '' && !empty($tab_plis_recep_par_sem["date_dernier_traitement"]) ) ?$liste_date["num"][$tab_plis_recep_par_sem["date_dernier_traitement"]] : "non_traite";
										
										$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["date_courrier"] = $tab_plis_recep_par_sem["date_courrier"];
										$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["date_dernier_traitement"] = $tab_plis_recep_par_sem["date_dernier_traitement"];
										
										
																			
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["nb_recep"]))
										{
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["nb_recep"] = $liste_result_plis_reception[$tab_plis_recep_par_sem["date_courrier"]];
											$aux_rec      = $liste_result_plis_reception[$tab_plis_recep_par_sem["date_courrier"]];
											$aux_date_rec = $tab_plis_recep_par_sem["date_courrier"];
											
											
										}
										else
										{	
											if($aux_rec != $liste_result_plis_reception[$tab_plis_recep_par_sem["date_courrier"]] && $aux_date_rec != $tab_plis_recep_par_sem["date_courrier"])
											{
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["nb_recep"] += $liste_result_plis_reception[$tab_plis_recep_par_sem["date_courrier"]];
											$aux_rec 	  = $liste_result_plis_reception[$tab_plis_recep_par_sem["date_courrier"]];
											$aux_date_rec = $tab_plis_recep_par_sem["date_courrier"];
											
											
											}
									   }
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["cloture"]))
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["cloture"] = $tab_plis_recep_par_sem["cloture"];
										else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["cloture"] += $tab_plis_recep_par_sem["cloture"];
									
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["encours"]))
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["encours"] = $tab_plis_recep_par_sem["encours"];
										else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["encours"] += $tab_plis_recep_par_sem["encours"];
									
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["non_traite"]))
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["non_traite"] = $tab_plis_recep_par_sem["non_traite"];
										else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["non_traite"] += $tab_plis_recep_par_sem["non_traite"];
										
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_scan"]))
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_scan"] = $tab_plis_recep_par_sem["ko_scan"];
										else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_scan"] += $tab_plis_recep_par_sem["ko_scan"];
										
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_reclammation"]))
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_reclammation"] = $tab_plis_recep_par_sem["ko_reclammation"];
										else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_reclammation"] += $tab_plis_recep_par_sem["ko_reclammation"];
										
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_litige"]))
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_litige"] = $tab_plis_recep_par_sem["ko_litige"];
										else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_litige"] += $tab_plis_recep_par_sem["ko_litige"];
										
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_inconnu"]))
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_inconnu"] = $tab_plis_recep_par_sem["ko_inconnu"];
										else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_inconnu"] += $tab_plis_recep_par_sem["ko_inconnu"];
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_call"]))
												$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_call"] = $tab_plis_recep_par_sem["ko_call"];
											else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_call"] += $tab_plis_recep_par_sem["ko_call"];
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_circulaire"]))
												$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_circulaire"] = $tab_plis_recep_par_sem["ko_circulaire"];
											else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_circulaire"] += $tab_plis_recep_par_sem["ko_circulaire"];
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_en_attente"]))
												$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_en_attente"] = $tab_plis_recep_par_sem["ko_en_attente"];
											else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_en_attente"] += $tab_plis_recep_par_sem["ko_en_attente"];
										if (empty($tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_def"]))
												$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_def"] = $tab_plis_recep_par_sem["ko_def"];
											else
											$tab_plis_recep_hebdo[$num_semaine][$num_semaine_ttt]["ko_def"] += $tab_plis_recep_par_sem["ko_def"];
									}
									
									/*foreach($tab_plis_recep_hebdo as $k_plis_recep_hebdo =>$tab_plis_recep_hebdo){
										$content_type_recep_sem .= '<tr>
                                         <td style="text-align:center">'.$k_plis_recep_hebdo.'</td>';
										 
										 $content_type_recep_sem .= '
										 <td style="text-align:center">'.$tab_plis_recep_hebdo["nb_recep"].'</td>
										<td style="text-align:center;">'.$tab_plis_recep_hebdo["cloture"].'</td>
										<td style="text-align:center;">'.$tab_plis_recep_hebdo["encours"].'</td>
										<td style="text-align:center;">'.$tab_plis_recep_hebdo["non_traite"].'</td>
										<td style="text-align:center;">'.$tab_plis_recep_hebdo["ko_scan"].'</td>
										<td style="text-align:center;">'.$tab_plis_recep_hebdo["ko_inconnu"].'</td>
										<td style="text-align:center;">'.$tab_plis_recep_hebdo["ko_call"].'</td>
										<td style="text-align:center;">'.$tab_plis_recep_hebdo["ko_circulaire"].'</td>
										<td style="text-align:center;">'.$tab_plis_recep_hebdo["ko_en_attente"].'</td>
										<td style="text-align:center;">'.$tab_plis_recep_hebdo["ko_def"].'</td>
										</tr>';
										
									}
									
									*/
									$tab_reception     = $tab_plis_recep_hebdo;
									$tab_plis_recep_nb = array();
									foreach($tab_reception as $k_semrec =>$v_semrec)
									{
										foreach($v_semrec as $k_semrecp => $v_semrecp)
										{
											if(empty($tab_plis_recep_nb[$k_semrec]))
												$tab_plis_recep_nb[$k_semrec]  = $v_semrecp["nb_recep"];
											else $tab_plis_recep_nb[$k_semrec] = $v_semrecp["nb_recep"];
										}
									}
									
									foreach($tab_plis_recep_hebdo as $k_plis_recep_hebdo =>$tab_plis_recep){
									
										foreach($tab_plis_recep as $k_plis_tt => $tab_plis_ttt){
										$sem_ttt = ($k_plis_tt == 'non_traite') ? '' : $k_plis_tt;
										$content_type_recep_sem .= '<tr>
                                         <td style="text-align:center">'.$k_plis_recep_hebdo.'</td>';
										
										//<td style="text-align:center">'.$tab_plis_ttt[$nb_recep].'</td>
										$content_type_recep_sem .= '
										<td style="text-align:center">'.$tab_plis_recep_nb[$k_plis_recep_hebdo].'</td>
										<td style="text-align:center;">'.$sem_ttt.'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["cloture"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["encours"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["non_traite"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["ko_scan"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["ko_inconnu"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["ko_call"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["ko_circulaire"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["ko_en_attente"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["ko_reclammation"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["ko_litige"].'</td>
										<td style="text-align:center;">'.$tab_plis_ttt["ko_def"].'</td>
										</tr>';
										}
									}
								 $content_type_recep_sem .='</tbody>
                            </table>';
							echo $content_type_recep_sem;
							
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	</div>
                           
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab02" style="margin-bottom:50px;display: ">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Typologie par statut</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab02()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
							</ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							
							$content_type_pli_date = '<table class="table table-bordered table-striped table-hover dataTable js-export-table-synthese"  style="width:100% !important;">  
                                <thead   class="custom_font">
									<tr>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Typologie</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Nbr. total plis réçu</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Date dernier traitement</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total cloturé</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total En cours</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total non traité</th>
										<th  style="width:40%;text-align:center"  colspan="8">Total KO</th>										
                                      
                                    </tr>
                                    <tr>
                                       
                                        <th  style="width:10%;text-align:center">Total KO Scan</th>
										<th  style="width:10%;text-align:center">Total KO Inconnu</th>
										<th  style="width:10%;text-align:center">Total KO Call</th>
										<th  style="width:10%;text-align:center">Total KO Circulaire</th>
										<th  style="width:10%;text-align:center">Total KO En attente</th>
										<th  style="width:10%;text-align:center">Total KO Réclam<br>mation</th>
										<th  style="width:10%;text-align:center">Total KO Litige</th>
										<th  style="width:10%;text-align:center">Total KO Définitif</th>
										
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">'; 									
                                  
									$j0 = 0;
									foreach($liste_result_plis_typo_date as $k_plis_typo_par_date =>$tab_plis_typo_par_date){
										$total = $tab_plis_typo_par_date["cloture"]+$tab_plis_typo_par_date["encours"]+$tab_plis_typo_par_date["non_traite"]+$tab_plis_typo_par_date["ko_scan"]+$tab_plis_typo_par_date["ko_inconnu"]+$tab_plis_typo_par_date["ko_call"]+$tab_plis_typo_par_date["ko_circulaire"]+$tab_plis_typo_par_date["ko_en_attente"]+$tab_plis_typo_par_date["ko_def"];
										if ($total > 0) {
										$content_type_pli_date .= '<tr>
                                        <td style="text-align:center">'.$tab_plis_typo_par_date["code_type_pli"].'</td>
                                        <td style="text-align:center">'.$liste_result_plis_typo_reception[$tab_plis_typo_par_date["typologie"]].'</td>
                                        <td style="text-align:center">'.$tab_plis_typo_par_date["date_dernier_traitement"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["cloture"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["encours"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["non_traite"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["ko_scan"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["ko_inconnu"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["ko_call"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["ko_circulaire"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["ko_en_attente"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["ko_reclammation"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["ko_litige"].'</td>
										<td style="text-align:center">'.$tab_plis_typo_par_date["ko_def"].'</td>
										</tr>';
										}
									}
								 $content_type_pli_date .='</tbody>
                            </table>';
							echo $content_type_pli_date;
							
							
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	</div>
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab03" style="margin-bottom:50px;display: ">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Typologie par statut par hebdo</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab03()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
							</ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							
							$content_type_pli_hebdo = '<table class="table table-bordered table-striped table-hover dataTable js-export-table-synthese"  style="width:100% !important;">  
                                <thead   class="custom_font">
									<tr>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Semaine de réception</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Nbr. total plis réçu</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Semaine de traitement</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total cloturé</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total En cours</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Total non traité</th>
										<th  style="width:40%;text-align:center"  colspan="8">Total KO</th>										
                                      
                                    </tr>
                                    <tr>
                                       
                                        <th  style="width:10%;text-align:center">Total KO Scan</th>
										<th  style="width:10%;text-align:center">Total KO Inconnu</th>
										<th  style="width:10%;text-align:center">Total KO Call</th>
										<th  style="width:10%;text-align:center">Total KO Circulaire</th>
										<th  style="width:10%;text-align:center">Total KO En attente</th>
										<th  style="width:10%;text-align:center">Total KO Réclam<br>mation</th>
										<th  style="width:10%;text-align:center">Total KO Litige</th>
										<th  style="width:10%;text-align:center">Total KO Définitif</th>
										
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">'; 									
                                  
									$j0 = 0;
									$tab_typologie = array(); 
									foreach($liste_result_plis_typo_date as $k_plis_typo_par_date =>$tab_plis_typo_par_date){
										$total = $tab_plis_typo_par_date["cloture"]+$tab_plis_typo_par_date["encours"]+$tab_plis_typo_par_date["non_traite"]+$tab_plis_typo_par_date["ko_scan"]+$tab_plis_typo_par_date["ko_inconnu"]+$tab_plis_typo_par_date["ko_call"]+$tab_plis_typo_par_date["ko_circulaire"]+$tab_plis_typo_par_date["ko_en_attente"]+$tab_plis_typo_par_date["ko_def"];
										
									
										$tab_typologie[$tab_plis_typo_par_date["typologie"]] = $tab_plis_typo_par_date["code_type_pli"];
										
										
										$num_semaine_ttt = ($tab_plis_typo_par_date["date_dernier_traitement"] != '' && !empty($tab_plis_typo_par_date["date_dernier_traitement"]) ) ? $liste_date["num"][$tab_plis_typo_par_date["date_dernier_traitement"]] : "non_traite";
										
										if($tab_plis_typo_par_date["typologie"] == "" || empty($tab_plis_typo_par_date["typologie"]))
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["code"] = "Aucune";
									    else 
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["code"] = $tab_plis_typo_par_date["code_type_pli"];
										
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["cloture"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["cloture"] = $tab_plis_typo_par_date["cloture"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["cloture"] += $tab_plis_typo_par_date["cloture"];
									    }
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["encours"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["encours"] = $tab_plis_typo_par_date["encours"];							
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["encours"] += $tab_plis_typo_par_date["encours"];
									    }
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["non_traite"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["non_traite"] = $tab_plis_typo_par_date["non_traite"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["non_traite"] += $tab_plis_typo_par_date["non_traite"];
									    }
										
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_scan"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_scan"] = $tab_plis_typo_par_date["ko_scan"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_scan"] += $tab_plis_typo_par_date["ko_scan"];
									    }
										
										
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_reclammation"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_reclammation"] = $tab_plis_typo_par_date["ko_reclammation"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_reclammation"] += $tab_plis_typo_par_date["ko_reclammation"];
									    }
										
										
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_litige"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_litige"] = $tab_plis_typo_par_date["ko_litige"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_litige"] += $tab_plis_typo_par_date["ko_litige"];
									    }
										
										
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_inconnu"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_inconnu"] = $tab_plis_typo_par_date["ko_inconnu"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_inconnu"] += $tab_plis_typo_par_date["ko_inconnu"];
									    }
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_call"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_call"] = $tab_plis_typo_par_date["ko_call"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_call"] += $tab_plis_typo_par_date["ko_call"];
									    }
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_circulaire"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_circulaire"] = $tab_plis_typo_par_date["ko_circulaire"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_circulaire"] += $tab_plis_typo_par_date["ko_circulaire"];
									    }
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_en_attente"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_en_attente"] = $tab_plis_typo_par_date["ko_en_attente"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_en_attente"] += $tab_plis_typo_par_date["ko_en_attente"];
									    }
										if (empty($tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_def"]))
										{
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_def"] = $tab_plis_typo_par_date["ko_def"];								
											
										}
										else
										{	
											$tab_plis_typo_hebdo[$tab_plis_typo_par_date["typologie"]][$num_semaine_ttt]["ko_def"] += $tab_plis_typo_par_date["ko_def"];
									    }
									}
									
									
									foreach($tab_plis_typo_hebdo as $k_plis_typo_hebdo =>$tb_plis_typo_hebdo){											
									
									if(!empty($k_plis_typo_hebdo) && $k_plis_typo_hebdo != '' && $k_plis_typo_hebdo != null){
										foreach($tb_plis_typo_hebdo as $ktypo_hebdo =>$tb_typo_hebdo){	
										if ($ktypo_hebdo == 'non_traite') $ktypo_hebdo = "";
										$content_type_pli_hebdo .= '<tr>';	
										$content_type_pli_hebdo .= '<td style="text-align:center">'.$tab_typologie[$k_plis_typo_hebdo].'</td>
                                        <td style="text-align:center">'.$liste_result_plis_typo_reception[$k_plis_typo_hebdo].'</td>
                                        <td style="text-align:center">'.$ktypo_hebdo.'</td>
                                        <td style="text-align:center">'.$tb_typo_hebdo['cloture'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['encours'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['non_traite'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['ko_scan'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['ko_inconnu'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['ko_call'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['ko_circulaire'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['ko_en_attente'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['ko_reclammation'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['ko_litige'].'</td>
										<td style="text-align:center">'.$tb_typo_hebdo['ko_def'].'</td>';
										
										$content_type_pli_hebdo .= '</tr>';
										}
									 }
									}
									
								 $content_type_pli_hebdo .='</tbody>
                            </table>';
							echo $content_type_pli_hebdo;
							
							
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	</div>
