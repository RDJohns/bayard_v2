<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
								Traitement des abonnements - Saisie</span>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <a onclick="quitter_traitement()" style="cursor: pointer;">
                                        <i class="fa fa-close"> </i></a>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="body table-responsive">
                            <?php
                            if(count($liste_solde_mvt_jr) > 0 ){
                                $content_solde_mvt_jr = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-mvt" style="width: 100%;">     
										<thead   class="custom_font">
											<tr>
												<th style="width:10%;white-space:nowrap;">Traitement du</th>
                                                <th style="width:10%;white-space:nowrap;">Saisie finie</th>
                                                <th style="width:10%;white-space:nowrap;">Flux OK</th>
												<th style="width:10%;white-space:nowrap;">Hors p&eacute;rimetre</th>
												<th style="width:10%">Anomalie</th>
												<th style="width:10%">Escalade</th>
                                                <th style="width:10%;white-space:nowrap;">Transfert consigne</th>
                                                <th style="width:10%;white-space:nowrap;">KO définitif</th>
                                                <th style="width:10%;white-space:nowrap;">KO inconnu</th>
                                                <th style="width:10%;white-space:nowrap;">KO en attente</th>
                                                <th style="width:10%;white-space:nowrap;">KO en cours bayard</th>
												
											</tr>
										</thead>
										<tbody   class="custom_font">';
                                $total_hp = $total_sf  = $total_anomalie =$total_escalade = $total_tc = $totl_ko_def = $total_ko_inconnu = $total_ko_ettent = $total_abo_flux_ok =  $total_ko_bayard= 0;
                                foreach($liste_solde_mvt_jr as $ksolde =>$list){


                                    $total_hp 			+= $list["abo_hors_perimetre"];
                                    $total_sf 		    += $list["abo_saisie_finie"];
                                    $total_anomalie 	+= $list["abo_anomalie"];
                                    $total_escalade 	+= $list["abo_escalade"];
                                    $total_tc           +=  $list["abo_transfert_consigne"];
                                    $totl_ko_def        += $list["ko_def"];
                                    $total_ko_inconnu   += $list["ko_inconnu"];
                                    $total_ko_ettent    += $list["ko_ettent"];
                                    $total_ko_bayard    += $list["ko_bayard"];
                                    $total_abo_flux_ok    += $list["abo_flux_ok"];

                                    $content_solde_mvt_jr .='<tr>
													<td>'.$list["dt_event"].'</td>
                                                    <td>'.$list["abo_saisie_finie"].'</td>
                                                    <td>'.$list["abo_flux_ok"].'</td>
													<td>'.$list["abo_hors_perimetre"].'</td>
													<td>'.$list["abo_anomalie"].'</td>
													<td>'.$list["abo_escalade"].'</td>
													<td>'.$list["abo_transfert_consigne"].'</td>													
                                                    <td>'.$list["ko_def"].'</td>
                                                    <td>'.$list["ko_inconnu"].'</td>
                                                    <td>'.$list["ko_ettent"].'</td>
                                                    <td>'.$list["ko_bayard"].'</td>
											</tr>';

                                }

                                $content_solde_mvt_jr .=' </tbody><tfoot>';
                                $content_solde_mvt_jr .='<tr>
													<th style="text-align:center;">Total</th>
                                                    <th>'.$total_sf.'</th>
                                                    <th>'.$total_abo_flux_ok.'</th>
													<th>'.$total_hp.'</th>
													<th>'.$total_anomalie.'</th>
													<th>'.$total_escalade.'</th>
													<th>'.$total_tc.'</th>
                                                    <th>'.$totl_ko_def.'</th>
                                                    <th>'.$total_ko_inconnu.'</th>
                                                    <th>'.$total_ko_ettent.'</th>
                                                    <th>'.$total_ko_bayard.'</th>
											</tr></tfoot>									 
								</table>';
                                echo $content_solde_mvt_jr;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        