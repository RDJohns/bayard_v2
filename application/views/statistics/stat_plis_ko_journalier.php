
<!--div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab21" display="display:none"-->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Détail journalier des plis KO</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab31()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_detail_plis_ko = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:10%;text-align:center;">Date traitement</th>
										<th  style="width:15%;text-align:center;">KO Inconnu</th>
										<th  style="width:15%;text-align:center;">KO scan</th>
										<th  style="width:15%;text-align:center;">KO Call</th>
										<th  style="width:15%;text-align:center;">KO Circulaire</th>
										<th  style="width:15%;text-align:center;">KO en attente</th>
										<th  style="width:15%;text-align:center;">KO réclam-<br>mation</th>
										<th  style="width:15%;text-align:center;">KO litige</th>
										<th  style="width:15%;text-align:center;">KO Définitif</th>
									 
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                  
									$j1 = 0;
									foreach($liste_result_plis_ko as $k_plis_ko_par_date =>$tab_plis_ko_par_date){
										
										$content_detail_plis_ko .= '<tr>
                                        <td style="text-align:center;">'.$tab_plis_ko_par_date["dt_event"].'</td>
										<td style="text-align:center;">'.$tab_plis_ko_par_date["ko_inconnu"].'</td>
										<td style="text-align:center;">'.$tab_plis_ko_par_date["ko_scan"].'</td>
										<td style="text-align:center;">'.$tab_plis_ko_par_date["ko_call"].'</td>
										<td style="text-align:center;">'.$tab_plis_ko_par_date["ko_circulaire"].'</td>
										<td style="text-align:center;">'.$tab_plis_ko_par_date["ko_en_attente"].'</td>
										<td style="text-align:center;">'.$tab_plis_ko_par_date["ko_reclammation"].'</td>
										<td style="text-align:center;">'.$tab_plis_ko_par_date["ko_litige"].'</td>
										<td style="text-align:center;">'.$tab_plis_ko_par_date["ko_def"].'</td>
										
										</tr>';
										
									}
								 $content_detail_plis_ko .='</tbody>
                            </table>';
							echo $content_detail_plis_ko;
									
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	<!--/div-->
                           