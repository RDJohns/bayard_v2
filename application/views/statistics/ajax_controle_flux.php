  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">
								Traitement des flux - Saisie</span>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <a onclick="quitter_traitement()" style="cursor: pointer;">
                                        <i class="fa fa-close"> </i></a>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="body table-responsive">
                            <?php if(isset($liste_solde_jr)){
                            if(count($liste_solde_jr) > 0 ){
                                $content_solde_jr = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-controle">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Traitement du</th>
												<th style="width:10%"><font color="#0E76BC">Saisie finie</font></th>
												<th style="width:10%"><font color="#0E76BC">Hors p&eacute;rimetre</font></th>
												<th style="width:10%"><font color="#0E76BC">Anomalie</font></th>
												<th style="width:10%">Escalade</th>
												<th style="width:10%">Transfert consigne</th>
												
											</tr>
										</thead>
										<tbody   class="custom_font">';
                                $total_sf = $total_anomalie  = $total_hp =$total_escalade = $total_tc = 0;
                                foreach($liste_solde_jr as $ksolde =>$list){


                                    $total_sf 			+= $list["saisie_finie"];
                                    $total_hp		    += $list["hors_perimetre"];
                                    $total_anomalie 	+= $list["anomalie"];
                                    $total_escalade 	+= $list["escalade"];
                                    $total_tc    +=  $list["transfert_consigne"];

                                    $content_solde_jr .='<tr>
													<td>'.$list["dt_event"].'</td>
													<td>'.$list["saisie_finie"].'</td>
													<td>'.$list["hors_perimetre"].'</td>
													<td>'.$list["anomalie"].'</td>
													<td>'.$list["escalade"].'</td>
													<td>'.$list["transfert_consigne"].'</td>													
															  
											</tr>';

                                }

                                $content_solde_jr .=' </tbody><tfoot>';
                                $content_solde_jr .='<tr>
													<th></th>
													<th>'.$total_sf.'</th>
													<th>'.$total_hp.'</th>
													<th>'.$total_anomalie.'</th>
													<th>'.$total_escalade.'</th>
													<th>'.$total_tc.'</th>
															  
											</tr></tfoot>									 
								</table>';
                                echo $content_solde_jr;
                            }
                        }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        