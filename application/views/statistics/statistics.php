<script>
    var url_site = '<?php echo site_url(); ?>';
</script>
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="logo-pro">
                    <a href="index.html"><img class="main-logo" src="<?php echo img_url('logo/logo.png'); ?>" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="header-top-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="header-top-wraper">
                            <div class="row">
                                <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                    <!--div class="menu-switcher-pro">
                                        <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                            <i class="educate-icon educate-nav"></i>
                                        </button>
                                    </div-->
                                </div> 
                                <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                    <div class="header-top-menu tabl-d-n">
                                        <ul class="nav navbar-nav mai-top-nav">
                                            <li class="nav-item dropdown res-dis-nn">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">Visualisation <span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                <div role="menu" class="dropdown-menu animated zoomIn">
                                                    <a href="<?php echo site_url('visualisation/visualisation');?>" class="dropdown-item">Plis</a>
                                                    <a href="<?php echo site_url('visualisation/visualisation/visualise_anomalis');?>" class="dropdown-item">Anomalies</a>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown res-dis-nn">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">Statistiques <span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                                                <div role="menu" class="dropdown-menu animated zoomIn">
                                                     <a class="dropdown-item" href="<?php echo site_url('statistics/statistics/stat_plis');?>" class="dropdown-item">Plis</a>
                                                    <a href="<?php echo site_url('statistics/statistics/stat_traitement');?>" class="dropdown-item">Traitement</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <div class="header-right-info">
                                        <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                            <li class="nav-item">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                    <img src="<?php echo base_url().'src/'; ?>img/avatar.png" alt="" />
                                                    <span class="admin-name"><?php echo $this->session->userdata('login'); ?></span>
                                                    <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                                </a>
                                                <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                    <li onclick="deconnexion();"><a href="#"><span class="edu-icon edu-locked author-log-ic"></span>D&eacute;connexion</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- <li class="nav-item nav-setting-open"><a href="#" role="button" class="nav-link"><i class="educate-icon educate-menu"></i></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="hpanel widget-int-shape">
                        <div class="panel-body">
                            <div class="text-center content-bg-pro">
                                <h4>Recherche</h4>
                                <div class="form-group">
                                    <input name="numcommande" id="numcommande" type="number" class="form-control" placeholder="N° commande" onkeypress="rech()">
                                </div>
                                <div class="form-group">
                                    <input name="numclient" id="numclient" type="number" class="form-control" placeholder="N° Client" onkeypress="rech()">
                                </div>
                                <div class="form-group">
                                    <input name="nom_client" id="nom_client" type="text" class="form-control" placeholder="Nom" onkeypress="rech()">
                                </div>
                                <em id="error_message">*Veuillez au moins renseigner un champ</em>
                                <button class="btn btn-primary waves-effect waves-light" onclick="recherche_doc()"><i class="fa fa-search"></i>&nbsp;Rechercher</button>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div id="info-pli" class="hpanel widget-int-shape">

                    </div>
                </div>
                <div id="result-rsrch" style="display: block;">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Projects <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <div id="toolbar">
                                        <select class="form-control dt-tb">
                                            <option value="">Export Basic</option>
                                            <option value="all">Export All</option>
                                            <option value="selected">Export Selected</option>
                                        </select>
                                    </div>
                                    <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                           data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                        <tr>
                                            <th data-field="state" data-checkbox="true"></th>
                                            <th data-field="id">ID</th>
                                            <th data-field="name" data-editable="true">Task</th>
                                            <th data-field="email" data-editable="true">Email</th>
                                            <th data-field="phone" data-editable="true">Phone</th>
                                            <th data-field="complete">Completed</th>
                                            <th data-field="task" data-editable="true">Task</th>
                                            <th data-field="date" data-editable="true">Date</th>
                                            <th data-field="price" data-editable="true">Price</th>
                                            <th data-field="action">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td>1</td>
                                            <td>Web Development</td>
                                            <td>admin@uttara.com</td>
                                            <td>+8801962067309</td>
                                            <td class="datatable-ct"><span class="pie">1/6</span>
                                            </td>
                                            <td>10%</td>
                                            <td>Jul 14, 2017</td>
                                            <td>$5455</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>2</td>
                                            <td>Graphic Design</td>
                                            <td>fox@itpark.com</td>
                                            <td>+8801762067304</td>
                                            <td class="datatable-ct"><span class="pie">230/360</span>
                                            </td>
                                            <td>70%</td>
                                            <td>fab 2, 2017</td>
                                            <td>$8756</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>3</td>
                                            <td>Software Development</td>
                                            <td>gumre@hash.com</td>
                                            <td>+8801862067308</td>
                                            <td class="datatable-ct"><span class="pie">0.42/1.461</span>
                                            </td>
                                            <td>5%</td>
                                            <td>Seb 5, 2017</td>
                                            <td>$9875</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>4</td>
                                            <td>Woocommerce</td>
                                            <td>kyum@frok.com</td>
                                            <td>+8801962066547</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>Oct 10, 2017</td>
                                            <td>$3254</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>5</td>
                                            <td>Joomla</td>
                                            <td>jams@game.com</td>
                                            <td>+8801962098745</td>
                                            <td class="datatable-ct"><span class="pie">200,133</span>
                                            </td>
                                            <td>80%</td>
                                            <td>Nov 20, 2017</td>
                                            <td>$58745</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>6</td>
                                            <td>Wordpress</td>
                                            <td>flat@yem.com</td>
                                            <td>+8801962254781</td>
                                            <td class="datatable-ct"><span class="pie">0.42,1.051</span>
                                            </td>
                                            <td>30%</td>
                                            <td>Aug 25, 2017</td>
                                            <td>$789879</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>7</td>
                                            <td>Ecommerce</td>
                                            <td>hasan@wpm.com</td>
                                            <td>+8801962254863</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>July 17, 2017</td>
                                            <td>$21424</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>8</td>
                                            <td>Android Apps</td>
                                            <td>ATM@devep.com</td>
                                            <td>+8801962875469</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>June 11, 2017</td>
                                            <td>$78978</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>9</td>
                                            <td>Prestashop</td>
                                            <td>presta@Prest.com</td>
                                            <td>+8801962067524</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>May 9, 2017</td>
                                            <td>$45645</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>10</td>
                                            <td>Game Development</td>
                                            <td>Dev@game.com</td>
                                            <td>+8801962067457</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>April 5, 2017</td>
                                            <td>$4564545</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>11</td>
                                            <td>Angular Js</td>
                                            <td>gular@angular.com</td>
                                            <td>+8801962067124</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>Dec 1, 2017</td>
                                            <td>$645455</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>12</td>
                                            <td>Opencart</td>
                                            <td>open@cart.com</td>
                                            <td>+8801962067587</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>Jan 6, 2017</td>
                                            <td>$78978</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>13</td>
                                            <td>Education</td>
                                            <td>john@example.com</td>
                                            <td>+8801962067471</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>Feb 6, 2016</td>
                                            <td>$456456</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>14</td>
                                            <td>Construction</td>
                                            <td>mary@example.com</td>
                                            <td>+8801962012457</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>Jan 6, 2016</td>
                                            <td>$87978</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>15</td>
                                            <td>Real Estate</td>
                                            <td>july@example.com</td>
                                            <td>+8801962067309</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>Dec 1, 2016</td>
                                            <td>$454554</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>16</td>
                                            <td>Personal Regume</td>
                                            <td>john@example.com</td>
                                            <td>+8801962067306</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>May 9, 2016</td>
                                            <td>$564555</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>17</td>
                                            <td>Admin Template</td>
                                            <td>mary@example.com</td>
                                            <td>+8801962067305</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>June 11, 2016</td>
                                            <td>$454565</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>18</td>
                                            <td>FrontEnd</td>
                                            <td>july@example.com</td>
                                            <td>+8801962067304</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>May 9, 2015</td>
                                            <td>$456546</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>19</td>
                                            <td>Backend</td>
                                            <td>john@range.com</td>
                                            <td>+8801962067303</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>Feb 9, 2014</td>
                                            <td>$564554</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>20</td>
                                            <td>Java Advance</td>
                                            <td>lamon@ghs.com</td>
                                            <td>+8801962067302</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>July 6, 2014</td>
                                            <td>$789889</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>21</td>
                                            <td>Jquery Advance</td>
                                            <td>hasad@uth.com</td>
                                            <td>+8801962067301</td>
                                            <td class="datatable-ct"><span class="pie">2,7</span>
                                            </td>
                                            <td>15%</td>
                                            <td>Jun 6, 2013</td>
                                            <td>$4565656</td>
                                            <td class="datatable-ct"><i class="fa fa-check"></i>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="library-book-area mg-t-30">
        <div class="container-fluid">
        </div>
    </div>
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
    </div>
    <div class="courses-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
        <div class="footer-copyright-area" style="position: fixed !important; bottom:0 !important; width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright © 2018. All rights reserved DEV-SI VIVETIC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
