<?php

$list_pli = '<thead  class="custom_font">

	<tr>
		<th style="white-space:nowrap;">Traitement du</th>
		<th style="white-space:nowrap;">Courrier du</th>
		<th style="white-space:nowrap;">Abonnement J</th>
		<th>Cloturé</th>
		<th style="white-space:nowrap;">Saisie finie</th>
		<th style="white-space:nowrap;">Flux OK</th>
		<th>Anomalie</th>
		<th style="white-space:nowrap;">Hors périmètre</th>
		<th>Escalade</th>
		<th style="white-space:nowrap;">Transfert consigne</th>
		<th style="white-space:nowrap;">KO définitif</th>
		<th style="white-space:nowrap;">KO inconnu</th>
		<th style="white-space:nowrap;">KO en attente</th>
		<th style="white-space:nowrap;">KO en cours bayard</th>
	</tr>
</thead> 
 <tbody class="custom_font">';

$j = 0;
$total_abo = $total_clo = $total_sf = $total_anom = $total_hp = $total_esc = $total_ts = $totl_ko_def = $total_ko_inconnu = $total_ko_ettent = $total_flux_ok = $total_ko_bayard= 0;
for($j = 0; $j < count($list); $j++){
    //$total_mvt 	   += $list[$j]["nb_mvt"];
    $total_abo     += $list[$j]["abo_j"];
    $total_clo 	   += $list[$j]["cloture"];
    $total_sf      += $list[$j]["saisie_finie"];
    $total_anom    += $list[$j]["anomalie"];
    $total_hp      += $list[$j]["hors_perimetre"];
    $total_esc     += $list[$j]["escalade"];
	$total_ts      += $list[$j]["transfert_consigne"];
	$totl_ko_def     += $list[$j]["ko_def"];
	$total_ko_inconnu     += $list[$j]["ko_inconnu"];
	$total_ko_ettent     += $list[$j]["ko_ettent"];
	$total_flux_ok     += $list[$j]["flux_ok"];
	$total_ko_bayard     += $list[$j]["ko_bayard"];
    $list_pli .= '<tr>
			 <td>'.$list[$j]["dt_event"].'</td>
			 <td>'.$list[$j]["date_reception"].'</td>
			 <td>'.$list[$j]["abo_j"].'</td>
			 <td>'.$list[$j]["cloture"].'</td>
			 <td>'.$list[$j]["saisie_finie"].'</td>
			 <td>'.$list[$j]["flux_ok"].'</td>
			 <td>'.$list[$j]["anomalie"].'</td>
			 <td>'.$list[$j]["hors_perimetre"].'</td>
			 <td>'.$list[$j]["escalade"].'</td>
			 <td>'.$list[$j]["transfert_consigne"].'</td>
			 <td>'.$list[$j]["ko_def"].'</td>
			 <td>'.$list[$j]["ko_inconnu"].'</td>
			 <td>'.$list[$j]["ko_ettent"].'</td>
			 <td>'.$list[$j]["ko_bayard"].'</td>
		</tr>';


}


$list_pli .= '</tbody>
	<tfoot><tr>
		<!--th></th-->
		<th colspan="2" style="text-align:center;">Total</th>
		<th>'.$total_abo.'</th>
		<th>'.$total_clo.'</th>
		<th>'.$total_sf.'</th>
		<th>'.$total_flux_ok.'</th>
		<th>'.$total_anom.'</th>
		<th>'.$total_hp.'</th>
		<th>'.$total_esc.'</th>
		<th>'.$total_ts.'</th>
		<th>'.$totl_ko_def.'</th>
		<th>'.$total_ko_inconnu.'</th>
		<th>'.$total_ko_ettent.'</th>
		<th>'.$total_ko_bayard.'</th>
	</tr>
</tfoot> ';

echo $list_pli ;
?>



