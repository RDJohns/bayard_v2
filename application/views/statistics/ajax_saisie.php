	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="row clearfix">
	       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
								Traitement des plis - Saisie</span>
                            <ul class="header-dropdown m-r--5">
                               <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_traitement()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 	
							/*
						
							
							*/
						
                                  	if(count($liste_solde_jr) > 0 ){ 
										$content_solde_jr = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-saisie">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Traitement du</th>
												<th style="width:10%"><font color="#0E76BC">Saisie finie OK</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie finie CI</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie finie KO</font></th>
												<th>KO Scan</th>
												<th>KO Définitif</th>
												<th>KO Inconnu</th>
												<th>Cloturé sans traitement</th>
												<th>Divisé</th>
												<th>Circulaire</th>
												<th>Cir. éditée</th>
												<th>Cir. envoyée</th>		
												<th>KO SRC</th>
												<th>KO en attente de consigne</th>												
												<th>OK circulaire</th>												
												<th>KO en cours bayard</th>												
											</tr>
										</thead>
										<tbody   class="custom_font">';
										
									$total_ci = $total_ano  = $total_saisie_ok =$total_saisie_ko = $total_saisie_ci = $total_ko_ks = $total_ko_ke =$total_ok_circ= $total_ko_bayard=0;
									$total_ko_scan = $total_ko_inc = $total_ko_aba = $total_divise = $total_kodef = $total_ci_edi = $total_ci_env = 0;
									//var_dump($liste_solde_jr);die;
									foreach($liste_solde_jr as $ksolde =>$list){
										
											//$total_hp 			+= $list["hp"];
											$total_ano 		+= $list["anomalie"];
											$total_ci 		+= $list["ci"];
											$total_saisie_ok 	+= $list["pli_saisie_ok"];
											$total_saisie_ci 	+= $list["pli_saisie_ci"];
											$total_saisie_ko    +=  $list["pli_saisie_ko"];
											
											$total_ko_scan += $list["ko_scan"];
											$total_ko_inc  += $list["ko_inconnu"];
											$total_ko_aba  += $list["ko_abandonne"];
											$total_divise  += $list["divise"];
											$total_kodef   += $list["ko_def"];
											$total_ci 	   += $list["ko_circulaire"];
											$total_ci_edi  += $list["ko_ci_editee"];
											$total_ci_env  += $list["ko_ci_envoyee"];
											$total_ko_ks   += $list["ko_src"];
											$total_ko_ke   += $list["ko_ke"];
											$total_ok_circ   += $list["ok_circulaire"];
											$total_ko_bayard   += $list["ko_bayard"];
											
											$content_solde_jr .='<tr>
													 <td>'.$list["dt_event"].'</td>
													 <td>'.$list["pli_saisie_ok"].'</td>
													 <td>'.$list["pli_saisie_ci"].'</td>
													 <td>'.$list["pli_saisie_ko"].'</td>
													 <td>'.$list["ko_scan"].'</td>
													 <td>'.$list["ko_def"].'</td>
													 <td>'.$list["ko_inconnu"].'</td>
													 <td>'.$list["ko_abandonne"].'</td>
													 <td>'.$list["divise"].'</td>
													 <td>'.$list["ko_circulaire"].'</td>			 
													 <td>'.$list["ko_ci_editee"].'</td>
													 <td>'.$list["ko_ci_envoyee"].'</td>
													 <td>'.$list["ko_src"].'</td>
													 <td>'.$list["ko_ke"].'</td>													
													 <td>'.$list["ok_circulaire"].'</td>													
													 <td>'.$list["ko_bayard"].'</td>													
															  
											</tr>';
										
									}
								
								
								$content_solde_jr .='<tr>
													<th>Total</th>
													<th>'.$total_saisie_ok.'</th>
													<th>'.$total_saisie_ci.'</th>
													<th>'.$total_saisie_ko.'</th>
													<th>'.$total_ko_scan.'</th>
													<th>'.$total_kodef.'</th>
													<th>'.$total_ko_inc.'</th>
													<th>'.$total_ko_aba.'</th>
													<th>'.$total_divise.'</th>
													<th>'.$total_ci.'</th>		
													<th>'.$total_ci_edi.'</th>
													<th>'.$total_ci_env.'</th>
													<th>'.$total_ko_ks.'</th>
													<th>'.$total_ko_ke.'</th>
													<th>'.$total_ok_circ.'</th>
													<th>'.$total_ko_bayard.'</th>
															  
											</tr></tbody>									 
								</table>';
								echo $content_solde_jr;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
			</div>
	</div>
	
	
	
