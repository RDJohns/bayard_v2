<!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
								Traitement des mouvements - Contrôle/Matchage</span>
                            <ul class="header-dropdown m-r--5">
                               <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_traitement()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                 
									$j = 0;	
									$tab_plis = array();	
									/*if($mensuel == 0){
									for($j = 0; $j < count($list); $j++){
										if($list[$j]["statut"] == 'Anomalie')
										$list[$j]["statut"] = $list[$j]["type_ko"];
									
										$num_semaine = $liste_date["num"][$list[$j]["dt_event"]];
										$num_semaine_courrier = $liste_date["num"][$list[$j]["date_courrier"]];
										$tab_plis[$num_semaine][$num_semaine_courrier][$list[$j]["statut"]][$list[$j]["id_pli"]]  = $list[$j]["statut"];
										
									}
									}*/
									//var_dump($list_solde_mvt);die;
									if(count($list_solde_mvt) > 0 && $mensuel == 0){
										$content_solde_mvt = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-mvt-controle">     
										<thead   class="custom_font">
											<tr>
												<th  style="width:20%">Traitement du</th>
												<th>OK</th>
												<th>KO Scan</th>
												<th>KO Définitif</th>
												<th>KO Inconnu</th>
												<th>Cloturé sans traitement</th>
												<th>Divisé</th>
												<th>Circulaire</th>
												<th>Cir. éditée</th>
												<th>Cir. envoyée</th>		
												<th>KO SRC</th>
												<th>KO en attente de consigne</th>
												<th>OK+Circulaire</th>	
												<th>KO en cours bayard</th>	
											</tr>
										</thead>
										<tbody   class="custom_font">';
									$total_ci = $total_ano  = $total_saisie_ok =$total_saisie_ko = $total_saisie_ci = $total_ko_ks = $total_ko_ke = $total_ok_circ= $total_ko_bayard=0;
									$total_ko_scan = $total_ko_inc = $total_ko_aba = $total_divise = $total_kodef = $total_ci_edi = $total_ci_env = 0;
									foreach($list_solde_mvt as $ksolde_mvt =>$list_mvt){
											$total_saisie_ok += $list_mvt["ok"];
											$total_ko_scan += $list_mvt["ko_scan"];
											$total_ko_inc  += $list_mvt["ko_inconnu"];
											$total_ko_aba  += $list_mvt["ko_abandonne"];
											$total_divise  += $list_mvt["divise"];
											$total_kodef   += $list_mvt["kd"];
											$total_ci 	   += $list_mvt["ci"];
											$total_ci_edi  += $list_mvt["ko_ci_editee"];
											$total_ci_env  += $list_mvt["ko_ci_envoyee"];
											$total_ko_ks  += $list_mvt["ks"];
											$total_ko_ke  += $list_mvt["ke"];
											$total_ok_circ   += $list_mvt["ok_circ"];
											$total_ko_bayard   += $list_mvt["ko_bayard"];
										$content_solde_mvt .='<tr>
												<td  style="width:20%">'.$ksolde_mvt.'</td>
												<td>'.$list_mvt["ok"].'</td>
												<td>'.$list_mvt["ko_scan"].'</td>
												<td>'.$list_mvt["kd"].'</td>
												<td>'.$list_mvt["ko_inconnu"].'</td>
												<td>'.$list_mvt["ko_abandonne"].'</td>
												<td>'.$list_mvt["divise"].'</td>
												<td>'.$list_mvt["ci"].'</td>			 
												<td>'.$list_mvt["ko_ci_editee"].'</td>
												<td>'.$list_mvt["ko_ci_envoyee"].'</td>
												<td>'.$list_mvt["ks"].'</td>
												<td>'.$list_mvt["ke"].'</td>												  
												<td>'.$list_mvt["ok_circ"].'</td>												  
												<td>'.$list_mvt["ko_bayard"].'</td>												  
																					  
														  
										</tr>';
									}
								$content_solde_mvt .='<tr>
											<th>Total</th>
											<th>'.$total_saisie_ok.'</th>
											<th>'.$total_ko_scan.'</th>
											<th>'.$total_kodef.'</th>
											<th>'.$total_ko_inc.'</th>
											<th>'.$total_ko_aba.'</th>
											<th>'.$total_divise.'</th>
											<th>'.$total_ci.'</th>		
											<th>'.$total_ci_edi.'</th>
											<th>'.$total_ci_env.'</th>
											<th>'.$total_ko_ks.'</th>
											<th>'.$total_ko_ke.'</th>
											<th>'.$total_ok_circ.'</th>
											<th>'.$total_ko_bayard.'</th>
													  
									</tr></tbody>									 
								</table>';
								echo $content_solde_mvt;
								 
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->