
<!--div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab21" display="display:none"-->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Détail mensuel des plis cloturés</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab24()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_detail_plis_cloture_mens = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:25%;text-align:center;">Mois</th>
										<th  style="width:15%;text-align:center;">CHQ</th>
										<th  style="width:15%;text-align:center;">CB</th>
										<th  style="width:15%;text-align:center;">Espèces</th>
										<th  style="width:15%;text-align:center;">Mandat</th>
										<th  style="width:15%;text-align:center;">BDP</th>
										<th  style="width:15%;text-align:center;">Autre</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                  
									$j1 = 0;
									$tab_plis_cltr_mens = array();
									foreach($liste_plis_cloture_par_date as $k_plis_cloture_par_date =>$tab_plis_cloture_par_date){
										/*$autre = $tab_plis_cloture_par_date["ok"] -  $tab_plis_cloture_par_date["bdc_avec_chq"] - $tab_plis_cloture_par_date["bdc_avec_cb"] -$tab_plis_cloture_par_date["bdc_avec_espece"]-$tab_plis_cloture_par_date["bdc_avec_mandat_facture"];*/
										
										$mois = $liste_mois["mois"][$tab_plis_cloture_par_date["dt_event"]];
										
										if (empty($tab_plis_cltr_mens[$mois]["CHQ"]))
											$tab_plis_cltr_mens[$mois]["CHQ"] = $tab_plis_cloture_par_date["bdc_avec_chq"];
										else
										$tab_plis_cltr_mens[$mois]["CHQ"]     += $tab_plis_cloture_par_date["bdc_avec_chq"];
										if (empty($tab_plis_cltr_mens[$mois]["CB"]))
											$tab_plis_cltr_mens[$mois]["CB"] = $tab_plis_cloture_par_date["bdc_avec_cb"];
										else
										$tab_plis_cltr_mens[$mois]["CB"]     += $tab_plis_cloture_par_date["bdc_avec_cb"];
										if (empty($tab_plis_cltr_mens[$mois]["Esp"]))
											$tab_plis_cltr_mens[$mois]["Esp"] = $tab_plis_cloture_par_date["bdc_avec_espece"];
										else
										$tab_plis_cltr_mens[$mois]["Esp"]     += $tab_plis_cloture_par_date["bdc_avec_espece"];
										if (empty($tab_plis_cltr_mens[$mois]["mandat"]))
											$tab_plis_cltr_mens[$mois]["mandat"] = $tab_plis_cloture_par_date["bdc_avec_mandat_facture"];
										else
										$tab_plis_cltr_mens[$mois]["mandat"]     += $tab_plis_cloture_par_date["bdc_avec_mandat_facture"];
										
										if (empty($tab_plis_cltr_mens[$mois]["bdp"]))
											$tab_plis_cltr_mens[$mois]["bdp"] = $tab_plis_cloture_par_date["bdp"];
										else
										$tab_plis_cltr_mens[$mois]["bdp"]     += $tab_plis_cloture_par_date["bdp"];
									
										if (empty($tab_plis_cltr_mens[$mois]["autre"]))
											$tab_plis_cltr_mens[$mois]["autre"] = $tab_plis_cloture_par_date["autre_plis"];
										else
										$tab_plis_cltr_mens[$mois]["autre"]     += $tab_plis_cloture_par_date["autre_plis"];					
									}
									foreach($tab_plis_cltr_mens as $kpli_plis_cltr_mens => $vpli_plis_cltr_mens){ 
									    
										$content_detail_plis_cloture_mens .= '<tr>
                                        <td style="text-align:center;">'.$kpli_plis_cltr_mens.'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_mens["CHQ"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_mens["CB"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_mens["Esp"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_mens["mandat"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_mens["bdp"].'</td>
										<td style="text-align:center;">'.$vpli_plis_cltr_mens["autre"].'</td>
										<!--td style="text-align:center;"></td-->
										</tr>';
										
									}
								 $content_detail_plis_cloture_mens .='</tbody>
                            </table>';
							echo $content_detail_plis_cloture_mens;
									
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	<!--/div-->
                           