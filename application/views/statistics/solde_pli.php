<?php
ini_set('memory_limit', '-1');
 $url_src = base_url().'src/';
 $url_js = base_url().'src/';

?>

	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="tab_hebdo">
	   <div class="row clearfix" style="margin-bottom:10px">
                <!-- Task Info -->
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="card" style="border-color:#00BCD4 !important;">
                        <div class="header">
                            <span style="font-size:20px;"><i class="fa fa-envelope" style="color:#00BCD4"></i>&nbsp;</span>
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">Bayard - Courrier</span>
                           	<ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" onclick="exporter('1', 'Bayard_courrier', 'c')" class=" waves-effect waves-block">Exporter vers excel</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  								
									if(count($list_courrier_att_bayard) > 0 ){
										$content = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date courrier</th>
												<th style="width:7%">A typer</th>
												<th style="width:10%">En attente de saisie</th>
											</tr>
										</thead>
										<tbody   class="custom_font" style="font-size:9px;">';
									$total = $att_saisie = $a_typer = 0;
									foreach($list_courrier_att_bayard as $kpli =>$list_pli){
										
											//$total 		+= $list_pli["pli_total"];
											$att_saisie 	+= $list_pli["pli_att_saisie"];
											$a_typer 		+= $list_pli["pli_a_typer"];
											
											$content .='<tr>
													<td>'.$list_pli["date_courrier"].'</td>
													<td>'.$list_pli["pli_a_typer"].'</td>
													<td>'.$list_pli["pli_att_saisie"].'</td>
																  
											</tr>';
									
									}
								
								 $content .=' </tbody><tfoot>';
									$content .='<tr>
													<th>Total</th>
													<th>'.$a_typer.'</th>
													<th>'.$att_saisie.'</th>
																	  
											</tr></tfoot>								 
								</table>';
								echo $content;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <span style="font-size:20px;"><i class="fa fa-server" style="color:#FF5722"></i>&nbsp;</span>
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">Bayard - SFTP</span>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" onclick="exporter('1', 'Bayard_SFTP', 'se', '2' )" class=" waves-effect waves-block">Exporter vers excel</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  								
									if(count($list_sftp_att_bayard) > 0 ){
										$content = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date réception</th>
												<th style="width:10%">A typer</th>
												<th style="width:10%">En attente de saisie</th>
											</tr>
										</thead>
										<tbody   class="custom_font">';
									$total = $att_saisie = $a_typer = 0;
									foreach($list_sftp_att_bayard as $kpli =>$list_pli){
										
											$att_saisie 		+= $list_pli["pli_att_saisie"];
											$a_typer 			+= $list_pli["pli_a_typer"];
											
											$content .='<tr>
													<td>'.$list_pli["date_reception"].'</td>
													<td>'.$list_pli["pli_a_typer"].'</td>
													<td>'.$list_pli["pli_att_saisie"].'</td>
											</tr>';
									
									}
								
								 $content .=' </tbody><tfoot>';
									$content .='<tr>
													<th>Total</th>
													<th>'.$a_typer.'</th>
													<th>'.$att_saisie.'</th>
													</tr></tfoot>								 
								</table>';
								echo $content;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <span style="font-size:20px;"><i class="fa fa-at" style="color:#8BC34A"></i>&nbsp;</span>
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">Bayard - Mail</span>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);"  onclick="exporter('1', 'Bayard_mail', 'se', '1' )"class=" waves-effect waves-block">Exporter vers excel</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  								
									if(count($list_mail_att_bayard) > 0 ){
										$content = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date réception</th>
												<th style="width:10%">A typer</th>
												<th style="width:10%">En attente de saisie</th>
												</tr>
										</thead>
										<tbody   class="custom_font">';
									$total = $non_traite  = $a_typer = $att_saisie = 0;
									foreach($list_mail_att_bayard as $kpli =>$list_pli){
										
											
											$att_saisie 		+= $list_pli["pli_att_saisie"];
											$a_typer 			+= $list_pli["pli_a_typer"];
											$content .='<tr>
													<td>'.$list_pli["date_reception"].'</td>
													<td>'.$list_pli["pli_a_typer"].'</td>
													<td>'.$list_pli["pli_att_saisie"].'</td>
													</tr>';
									
									}
								
								 $content .=' </tbody><tfoot>';
									$content .='<tr>
													<th>Total</th>
													<th>'.$a_typer.'</th>
													<th>'.$att_saisie.'</th>
													</tr></tfoot>								 
								</table>';
								echo $content;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
      </div>
	  <div class="row clearfix" style="margin-bottom:100px">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="card" style="border-color:#00BCD4 !important;">
                        <div class="header">
                            <span style="font-size:20px;"><i class="fa fa-envelope" style="color:#00BCD4"></i>&nbsp;</span>
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">Milan - Courrier</span>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);"  onclick="exporter('2', 'Milan_courrier', 'c')" class=" waves-effect waves-block">Exporter vers excel</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  								
									if(count($list_courrier_att_milan) > 0 ){
										$content = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date courrier</th>
												<th style="width:7%">A typer</th>
												<th style="width:10%">En attente de saisie</th>
											</tr>
										</thead>
										<tbody   class="custom_font">';
									$total = $att_saisie = $a_typer = 0;
									foreach($list_courrier_att_milan as $kpli =>$list_pli){
										
											//$total 				+= $list_pli["pli_total"];
											$att_saisie 	+= $list_pli["pli_att_saisie"];
											$a_typer 		+= $list_pli["pli_a_typer"];
											
											$content .='<tr>
													<td>'.$list_pli["date_courrier"].'</td>
													<td>'.$list_pli["pli_a_typer"].'</td>
													<td>'.$list_pli["pli_att_saisie"].'</td>
																  
											</tr>';
									
									}
								
								 $content .=' </tbody><tfoot>';
									$content .='<tr>
													<th>Total</th>
													<th>'.$a_typer.'</th>
													<th>'.$att_saisie.'</th>
																	  
											</tr></tfoot>								 
								</table>';
								echo $content;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <span style="font-size:20px;"><i class="fa fa-server" style="color:#FF5722"></i>&nbsp;</span>
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">Milan - SFTP</span>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" onclick="exporter('2', 'Milan_SFTP', 'se', '2' )" class=" waves-effect waves-block">Exporter vers excel</a></li>
                                        <!-- <li><a href="javascript:void(0);" onclick="quitter_traitement()" class=" waves-effect waves-block">Fermer</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Annuler</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  								
									if(count($list_sftp_att_milan) > 0 ){
										$content = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date réception</th>
												<th style="width:10%">A typer</th>
												<th style="width:10%">En attente de saisie</th>
											</tr>
										</thead>
										<tbody   class="custom_font">';
									$total = $att_saisie = $a_typer = 0;
									foreach($list_sftp_att_milan as $kpli =>$list_pli){
										
											$att_saisie 		+= $list_pli["pli_att_saisie"];
											$a_typer 			+= $list_pli["pli_a_typer"];
											$content .='<tr>
													<td>'.$list_pli["date_reception"].'</td>
													<td>'.$list_pli["pli_a_typer"].'</td>
													<td>'.$list_pli["pli_att_saisie"].'</td>
											</tr>';
									
									}
								
								 $content .=' </tbody><tfoot>';
									$content .='<tr>
													<th>Total</th>
													<th>'.$a_typer.'</th>
													<th>'.$att_saisie.'</th>
													</tr></tfoot>								 
								</table>';
								echo $content;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <span style="font-size:20px;"><i class="fa fa-at" style="color:#8BC34A"></i>&nbsp;</span>
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">Milan - Mail</span>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" onclick="exporter('2', 'Milan_mail', 'se', '1' )" class=" waves-effect waves-block">Exporter vers excel</a></li>
                                        <!-- <li><a href="javascript:void(0);" onclick="quitter_traitement()" class=" waves-effect waves-block">Fermer</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Annuler</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  								
									if(count($list_mail_att_milan) > 0 ){
										$content = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date réception</th>
												<th style="width:10%">A typer</th>
												<th style="width:10%">En attente de saisie</th>
												</tr>
										</thead>
										<tbody   class="custom_font">';
									$total = $non_traite  = $a_typer = $att_saisie = 0;
									foreach($list_mail_att_milan as $kpli =>$list_pli){
										
											
											$att_saisie 		+= $list_pli["pli_att_saisie"];
											$a_typer 			+= $list_pli["pli_a_typer"];
											$content .='<tr>
													<td>'.$list_pli["date_reception"].'</td>
													<td>'.$list_pli["pli_a_typer"].'</td>
													<td>'.$list_pli["pli_att_saisie"].'</td>
													</tr>';
									
									}
								
								 $content .=' </tbody><tfoot>';
									$content .='<tr>
													<th>Total</th>
													<th>'.$a_typer.'</th>
													<th>'.$att_saisie.'</th>
													</tr></tfoot>								 
								</table>';
								echo $content;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
      
	  </div>
	</div>
	</div>
	