<?php
ini_set('memory_limit', '-1');
 $url_src = base_url().'src/';
 $url_js = base_url().'src/';

?>

   <div class="row clearfix" style="text-align:center;">
	   
	
	 
	</div>
			

	</br>
	<?php
		foreach($list_societe as $ksoc=>$lib_societe){			
		
	?>
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" style="margin-top:50px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="tab_hebdo">
	   <div class="row clearfix">
                	
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-size:20px;"><i class="fa fa-dashboard" style="color:#662D91"></i>&nbsp;</span>
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;"><?php echo $lib_societe ;?> - Solde des plis</span>
                            <ul class="header-dropdown m-r--5">
                               <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_traitement()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  								
									if(count($list_pli_sla) > 0 ){
										$content_solde_mvt = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-solde">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date courrier</th>
												<th style="width:10%">Réception</th>
												<th style="width:10%">Reste à typer</th>
												<th style="width:10%">A traiter</th>
												<th style="width:10%">KO Scan</th>
												<th style="width:10%">KO Définitif</th>
												<th style="width:10%">KO Inconnu</th>
												<th style="width:10%">Cloturé sans traitement</th>
												<th style="width:10%">Divisé</th>
												<th style="width:10%">KO Circulaire</th>
												<th style="width:10%">Cir. éditée</th>
												<th style="width:10%">Cir. envoyée</th>
												<th style="width:10%">KO SRC</th>
												<th style="width:10%">En attente de consigne</th>
												<th style="width:10%">OK+circulaire</th>
												<th style="width:10%">KO en cours bayard</th>
												<th style="width:10%"><font color="#0E76BC">En attente de saisie</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie J</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie J+1</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie J+2</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie J++</font></th>
												
											</tr>
										</thead>
										<tbody   class="custom_font">';
									$total = $non_traite = $total_a_saisir = $total_ano = $total_cis =  $total_ano_scan = $total_ano_ko_def = $total_att_saisie = $total_saisie_j =$total_saisie_j1 =$total_saisie_j2 =$total_saisie_jplus = 0;
									$total_ko_inconnu = $total_ko_src = $total_ko_ke = $total_ko_ci =  $total_ci = $total_ci_editee = $total_ci_envoyee = $total_abondonne = $total_divise = $total_ok_circ=$total_ko_bayard=0;

									foreach($list_pli_sla as $kpli =>$list_pli){
										if($lib_societe == $list_pli["nom_societe"]){
											
											$ano_scan = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_scan']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_scan'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_scan'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_scan'] : 0;
											
											$ano_ko_def = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_def']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_def'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_def'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_def'] : 0;
											
											$ano_ko_inconnu = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_inconnu']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_inconnu'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_inconnu'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_inconnu'] : 0;
											
											$ano_ko_src = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_src']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_src'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_src'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_src'] : 0;
											
											$ano_ko_ke = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ke']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ke'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ke'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ke'] : 0;
											
											$ano_ci = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci'] : 0;
											
											$ano_ci_editee = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci_editee']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci_editee'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci_editee'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci_editee'] : 0;
											
											$ano_ci_envoyee = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci_envoyee']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci_envoyee'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci_envoyee'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_ci_envoyee'] : 0;
											
											$ano_ko_abondonne = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_abondonne']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_abondonne'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_abondonne'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_abondonne'] : 0;
											
											$ano_divise = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['divise']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['divise'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['divise'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['divise'] : 0;

											$ano_ok_circ = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ok_circ']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ok_circ'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ok_circ'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ok_circ'] : 0;

											$ano_ko_bayard = (!empty($liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_bayard']) && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_bayard'] != null && $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_bayard'] != '') ? $liste_anomalie_sla[$list_pli["date_courrier"]][$list_pli["nom_societe"]]['ko_bayard'] : 0;
											
											$a_saisir = $list_pli["pli_total"] - $list_pli["pli_non_traite"] - $ano_scan - $ano_ko_def - $ano_ko_inconnu - $ano_ko_src - $ano_ko_ke - $ano_ci - $ano_ci_editee - $ano_ci_envoyee - $ano_ko_abondonne - $ano_divise;
											$ano = $ano_scan + $ano_ko_def + $ano_ko_inconnu + $ano_ko_src + $ano_ko_ke + $ano_ci  + $ano_ko_abondonne + $ano_divise;
											$ci =  $ano_ci_editee + $ano_ci_envoyee ;
											$_a_saisir = ($a_saisir < 0) ? 0 : $a_saisir;
											
											
											$total 				+= $list_pli["pli_total"];
											$non_traite 		+= $list_pli["pli_non_traite"];
											$total_a_saisir 	+= $_a_saisir;
											$total_ano_scan 	+= $ano_scan;
											$total_ano_ko_def 	+= $ano_ko_def;
											$total_ko_inconnu   += $ano_ko_inconnu ;
											$total_ko_src   	+= $ano_ko_src ;
											$total_ko_ke   	    += $ano_ko_ke ;
											$total_ci   	    += $ano_ci ;
											$total_ci_editee   	+= $ano_ci_editee ;
											$total_ci_envoyee   += $ano_ci_envoyee ;
											$total_abondonne   	+= $ano_ko_abondonne ;
											$total_divise   	+= $ano_divise ;
											$total_ok_circ   	+= $ano_ok_circ ;
											$total_ko_bayard  	+= $ano_ko_bayard ;
											$total_ano = $total_ano_scan+$total_ano_ko_def+$total_ko_inconnu+$total_ko_src+$total_ko_ke+$total_ci+$total_abondonne+$total_divise+$total_ok_circ+$total_ko_bayard;
											$total_cis = $total_ci_editee+$total_ci_envoyee;
											
											$total_att_saisie 	+= $list_pli["pli_att_saisie"];
											$total_saisie_j 	+= $list_pli["pli_saisie_j"];
											$total_saisie_j1 	+= $list_pli["pli_saisie_j_plus_1"];
											$total_saisie_j2 	+= $list_pli["pli_saisie_j_plus_2"];
											$total_saisie_jplus +=  $list_pli["pli_saisie_j_plus_plus"];
											
																							
											$content_solde_mvt .='<tr>
													<td>'.$list_pli["date_courrier"].'</td>
													<td>'.$list_pli["pli_total"].'</td>
													<td>'.$list_pli["pli_non_traite"].'</td>
													<td>'.$_a_saisir.'</td>
													<td>'.$ano_scan.'</td>
													<td>'.$ano_ko_def.'</td>
													<td>'.$ano_ko_inconnu.'</td>													
													<td>'.$ano_ko_abondonne.'</td>
													<td>'.$ano_divise.'</td>
													<td>'.$ano_ci.'</td>
													<td>'.$ano_ci_editee.'</td>
													<td>'.$ano_ci_envoyee.'</td>
													<td>'.$ano_ko_src.'</td>
													<td>'.$ano_ko_ke.'</td>													
													<td>'.$ano_ok_circ.'</td>													
													<td>'.$ano_ko_bayard.'</td>													
													<td>'.$list_pli["pli_att_saisie"].'</td>
													<td>'.$list_pli["pli_saisie_j"].'</td>
													<td>'.$list_pli["pli_saisie_j_plus_1"].'</td>
													<td>'.$list_pli["pli_saisie_j_plus_2"].'</td>
													<td>'.$list_pli["pli_saisie_j_plus_plus"].'</td>
															  
											</tr>';
										}
									}
								
								 	$content_solde_mvt .='<tr>
													<th>Total</th>
													<th>'.$total.'</th>
													<th>'.$non_traite.'</th>
													<th>'.$total_a_saisir.'</th>
													<th>'.$total_ano_scan.'</th>
													<th>'.$total_ano_ko_def.'</th>
													<th>'.$total_ko_inconnu.'</th>
													<th>'.$total_abondonne.'</th>
													<th>'.$total_divise.'</th>
													<th>'.$total_ci.'</th>
													<th>'.$total_ci_editee.'</th>
													<th>'.$total_ci_envoyee.'</th>
													<th>'.$total_ko_src.'</th>
													<th>'.$total_ko_ke.'</th>
													<th>'.$total_ok_circ.'</th>
													<th>'.$total_ko_bayard.'</th>
													<th>'.$total_att_saisie.'</th>
													<th>'.$total_saisie_j.'</th>
													<th>'.$total_saisie_j1.'</th>
													<th>'.$total_saisie_j2.'</th>
													<th>'.$total_saisie_jplus.'</th>
															  
											</tr></tbody>								 
								</table>';
								echo $content_solde_mvt;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
							<span style="font-size:20px;"><i class="fa fa-database" style="color:#F1592A"></i>&nbsp;</span>
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">
								<?php echo $lib_societe ;?> - Solde des <?php echo $module;?></span>
								
                            <ul class="header-dropdown m-r--5">
                               <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_traitement()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="body table-responsive">
                            <?php 									
                                  	$total_mvt = 0;								
									if(count($list_mvt_sla) > 0 ){
										$content_solde_mvt = '<table class="table table-bordered table-striped table-hover dataTable js-exportable-solde">     
										<thead   class="custom_font">
											<tr>
												<th style="width:7%">Date courrier</th>
												<th style="width:10%">Réception</th>
												<th style="width:10%">Reste à typer</th>
												<th style="width:10%">A traiter</th>
												<th style="width:10%">KO Scan</th>
												<th style="width:10%">KO Définitif</th>
												<th style="width:10%">KO Inconnu</th>
												<th style="width:10%">Cloturé sans traitement</th>
												<th style="width:10%">Divisé</th>
												<th style="width:10%">KO Circulaire</th>
												<th style="width:10%">Cir. éditée</th>
												<th style="width:10%">Cir. envoyée</th>
												<th style="width:10%">KO SRC</th>
												<th style="width:10%">En attente de consigne</th>											
												<th style="width:10%">Ok+circulaire</th>											
												<th style="width:10%">KO en cours bayard</th>											
												<th style="width:10%"><font color="#0E76BC">En attente de saisie</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie J</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie J+1</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie J+2</font></th>
												<th style="width:10%"><font color="#0E76BC">Saisie J++</font></th>
												
											</tr>
										</thead>
										<tbody   class="custom_font">';
									$total = $non_traite = $total_a_saisir = $total_ano_scan = $total_ano_ko_def = $total_att_saisie = $total_saisie_j =$total_saisie_j1 =$total_saisie_j2 =$total_saisie_jplus = 0;
									$total_ano_ko_inc = $total_ano_ko_src = $total_ano_ko_ke = $total_ano_ko_ci = $total_ano_ci_edi = $total_ano_ci_env = $total_ano_abo = $total_ano_div = $total_cis =$total_ok_circ = $total_ko_bayard = 0;
									foreach($list_mvt_sla as $kmvt =>$list_mvt){
										if($lib_societe == $list_mvt["nom_societe"]){
											
											$ano_scan = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_scan']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_scan'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_scan'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_scan'] : 0;							
							
											$ano_ko_def = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_def']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_def'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_def'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_def'] : 0;
											
											$ano_ko_inc = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_inconnu']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_inconnu'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_inconnu'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_inconnu'] : 0;
											
											$ano_ko_src = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_src']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_src'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_src'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_src'] : 0;
											
											$ano_ko_ke = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_ke']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_ke'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_ke'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_ke'] : 0;
											
											$ano_ko_ci = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_ci']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_ci'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_ci'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_ci'] : 0;
											
											$ano_ci_editee = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ci_editee']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ci_editee'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ci_editee'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ci_editee'] : 0;
											
											$ano_ci_envoyee = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ci_envoyee']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ci_envoyee'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ci_envoyee'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ci_envoyee'] : 0;
											
											$ano_abondonne = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_abondonne']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_abondonne'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_abondonne'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_abondonne'] : 0;
											
											$ano_divise = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_divise']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_divise'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_divise'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_divise'] : 0;
											//nouveau statut
											$ok_circ = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ok_circ']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ok_circ'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ok_circ'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ok_circ'] : 0;
											$ano_bayard = (!empty($liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_bayard']) && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_bayard'] != null && $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_bayard'] != '') ? $liste_anomalie_sla[$list_mvt["date_courrier"]][$list_mvt["nom_societe"]]['mvt_ko_bayard'] : 0;
											
											$a_saisir = $list_mvt["mvt_total"] - $list_mvt["mvt_non_traite"] - $ano_scan - $ano_ko_def - $ano_ko_inc - $ano_ko_src - $ano_ko_ke - $ano_ko_ci - $ano_ci_editee - $ano_ci_envoyee - $ano_abondonne - $ano_divise;
											$ano = $ano_scan + $ano_ko_def + $ano_ko_inc + $ano_ko_src + $ano_ko_ke + $ano_ko_ci + $ano_abondonne + $ano_divise;
											$ci =  $ano_ci_editee + $ano_ci_envoyee;
											$_a_saisir = ($a_saisir < 0) ? 0 : $a_saisir;
											
											$total 				+= $list_mvt["mvt_total"];
											$non_traite 		+= $list_mvt["mvt_non_traite"];
											$total_a_saisir 	+= $_a_saisir;
											$total_ano_scan 	+= $ano_scan;
											$total_ano_ko_def 	+= $ano_ko_def;
											$total_ano_ko_inc 	+= $ano_ko_inc;
											$total_ano_ko_src 	+= $ano_ko_src;
											$total_ano_ko_ke	+= $ano_ko_ke;
											$total_ano_ko_ci	+= $ano_ko_ci;
											$total_ano_ci_edi	+= $ano_ci_editee;
											$total_ano_ci_env	+= $ano_ci_envoyee;
											$total_ano_abo	    += $ano_abondonne;
											$total_ano_div	    += $ano_divise;
											$total_ok_circ	    += $ok_circ;
											$total_ko_bayard	    += $ano_bayard;
											$total_att_saisie 	+= $list_mvt["mvt_att_saisie"];
											$total_saisie_j 	+= $list_mvt["mvt_saisie_j"];
											$total_saisie_j1 	+= $list_mvt["mvt_saisie_j_plus_1"];
											$total_saisie_j2 	+= $list_mvt["mvt_saisie_j_plus_2"];
											$total_saisie_jplus +=  $list_mvt["mvt_saisie_j_plus_plus"];
											$total_mvt += $ano;
											$total_cis += $ano_ci_editee+$ano_ci_envoyee;
											
											$content_solde_mvt .='<tr>
													<td>'.$list_mvt["date_courrier"].'</td>
													<td>'.$list_mvt["mvt_total"].'</td>
													<td>'.$list_mvt["mvt_non_traite"].'</td>
													<td>'.$_a_saisir.'</td>
													<td>'.$ano_scan.'</td>
													<td>'.$ano_ko_def.'</td>
													<td>'.$ano_ko_inc.'</td>													
													<td>'.$ano_abondonne.'</td>
													<td>'.$ano_divise.'</td>
													<td>'.$ano_ko_ci.'</td>
													<td>'.$ano_ci_editee.'</td>
													<td>'.$ano_ci_envoyee.'</td>
													<td>'.$ano_ko_src.'</td>
													<td>'.$ano_ko_ke.'</td>		
													<td>'.$ok_circ.'</td>		
													<td>'.$ano_bayard.'</td>		
													<td>'.$list_mvt["mvt_att_saisie"].'</td>
													<td>'.$list_mvt["mvt_saisie_j"].'</td>
													<td>'.$list_mvt["mvt_saisie_j_plus_1"].'</td>
													<td>'.$list_mvt["mvt_saisie_j_plus_2"].'</td>
													<td>'.$list_mvt["mvt_saisie_j_plus_plus"].'</td>
															  
											</tr>';
										}
									}
								
								 $content_solde_mvt .='<tr>
													<th>Total</th>
													<th>'.$total.'</th>
													<th>'.$non_traite.'</th>
													<th>'.$total_a_saisir.'</th>
													<th>'.$total_ano_scan.'</th>
													<th>'.$total_ano_ko_def.'</th>
													<th>'.$total_ano_ko_inc.'</th>
													<th>'.$total_ano_abo.'</th>
													<th>'.$total_ano_div.'</th>
													<th>'.$total_ano_ko_ci.'</th>
													<th>'.$total_ano_ci_edi.'</th>
													<th>'.$total_ano_ci_env.'</th>
													<th>'.$total_ano_ko_src.'</th>
													<th>'.$total_ano_ko_ke.'</th>
													<th>'.$total_ok_circ.'</th>
													<th>'.$total_ko_bayard.'</th>
													<th>'.$total_att_saisie.'</th>
													<th>'.$total_saisie_j.'</th>
													<th>'.$total_saisie_j1.'</th>
													<th>'.$total_saisie_j2.'</th>
													<th>'.$total_saisie_jplus.'</th>
															  
											</tr></tbody>							 
								</table>';
								echo $content_solde_mvt;
								}
							?>
                        </div>
                        </div>
                    </div>
                </div>
				
				
           
               
         </div>
	</div>
	</div>
	<?php
		}
	?>
	

	
