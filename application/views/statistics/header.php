<?php defined('BASEPATH') OR exit('No direct script access allowed');
    $url_src = base_url().'src/';
?>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>GED BAYARD</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
    <!-- favicon
		============================================ -->
    <!-- <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico"> -->
    <!-- Google Fonts
		============================================ -->
    <link href="<?php echo base_url().'assets/font/font-customer.css'; ?>" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo css_url('bootstrap.min'); ?>">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('font-awesome.min'); ?>">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('owl.carousel'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('owl.theme'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('owl.transitions'); ?>">
    <!-- animate CSS
		============================================ -->
    <!--link rel="stylesheet" href="<?php echo css_url('animate'); ?>"-->
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('normalize'); ?>">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('meanmenu.min'); ?>">
	<!-- main CSS	============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('main'); ?>">
    <!-- educate icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('educate-custon-icon'); ?>">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('morrisjs/morris'); ?>">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('scrollbar/jquery.mCustomScrollbar.min'); ?>">
    <!-- Bootstrap CSS
		============================================ -->
		 <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('metisMenu/metisMenu.min'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('metisMenu/metisMenu-vertical'); ?>">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('calendar/fullcalendar.min'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('calendar/fullcalendar.print.min'); ?>">
    <!-- touchspin CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('touchspin/jquery.bootstrap-touchspin.min');?>">
    <!-- forms CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('form/themesaller-forms');?>">
	 <!-- select2 CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('select2/select2.min');?>">
    <!-- chosen CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('chosen/bootstrap-chosen');?>">
    <!-- ionRangeSlider CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('ionRangeSlider/ion.rangeSlider');?>">
    <link rel="stylesheet" href="<?php echo css_url('ionRangeSlider/ion.rangeSlider.skinFlat');?>">
   <!-- notifications CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('notifications/Lobibox.min'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('notifications/notifications'); ?>">
	  <!-- style CSS
		============================================ -->
    <!--link rel="stylesheet" href="<?php //echo css_url('style'); ?>"-->
	  <link rel="stylesheet" href="">
    <!-- modals CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('modals');?>">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('responsive'); ?>">
    <!-- fliptab CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo css_url('fliptab'); ?>">
    <link rel="stylesheet" href="<?php echo css_url('datapicker/datepicker3'); ?>">
    
	<link rel="stylesheet" href="<?php echo css_url('datapicker/datepicker3'); ?>">
	<link rel="stylesheet" href="<?php echo css_url('animate'); ?>">
	<link rel="stylesheet" href="<?php echo css_url('owl.carousel'); ?>">
	<link rel="stylesheet" href="<?php echo css_url('owl.theme'); ?>">
	<link rel="stylesheet" href="<?php echo css_url('owl.transitions'); ?>">
	
	<link rel="stylesheet" href="<?php echo base_url().'assets/font/font.css'; ?>">
	<link rel="stylesheet" href="<?php echo base_url().'assets/font/icon.css'; ?>">
	<link rel="stylesheet" href="<?php //echo base_url().'assets/plugins/bootstrap/css/bootstrap.min.css'?>" >
	 <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/node-waves/waves.css'?>" >
	 <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/animate-css/animate.css'?>" >
	 <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/animate-css/animate.css'?>" >
	 <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css'?>" >
	 <link rel="stylesheet" href="<?php echo base_url().'assets/css/themes/all-themes.css'?>" >
   <link rel="stylesheet" href="<?php echo base_url().'assets/css/custom/typage.css'?>" >
   <link rel="stylesheet" href="<?php echo base_url().'assets/css/custom/stat.css'?>" >
    <!-- modernizr JS
		============================================ -->
	 <!--link rel="stylesheet" href="<?php ///echo base_url().'assets/datatable/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css'; ?>"-->
	 
      <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/sweetalert/sweetalert.css'; ?>">
	  <!--link rel="stylesheet" href="<?php //echo base_url().'assets/plugins/bootstrap/css/bootstrap.css'; ?>"-->
	  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/node-waves/waves.css'; ?>">
	  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/morrisjs/morris.css'; ?>">
	  <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css'; ?>">
	   <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'; ?>">
	  <link rel="stylesheet" href="<?php echo base_url().'assets/css/themes/all-themes.css'; ?>">
	  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/bootstrap/css/bootstrap.css'; ?>">
      <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'; ?>">
      <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css'; ?>">
	  

	
</head>
<body>
