<br><br><br><br> <!-- test-->
<div id="global-filtre">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1 filtre">Réception du </div>
                <div class="col-md-3">
                    <div class="input-daterange input-group" id="bs_datepicker_range_container">
                        <div class="form-line">
                            <input type="text" class="form-control recpt-date" id="date-debut" value="<?php echo date( 'd/m/Y', strtotime( 'monday this week' ) );?>">
                        </div>
                        <span class="input-group-addon">au&nbsp;&nbsp;</span>
                        <div class="form-line">
                            <input type="text" class="form-control recpt-date" id="date-fin" value="<?php echo date( 'd/m/Y', strtotime( 'friday this week' ) );?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <select class="form-control show-tick" tabindex="-98" id="filtre-societe" multiple>
                        <option value="0"  >-- Toutes --</option>
                        <option value="1" selected>Bayard</option>
                        <option value="2"selected>Milan</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select class="form-control show-tick" tabindex="-98" id="filtre-source" multiple>
                        <option value="0"  >-- Tous --</option>
                        <option value="1" selected >Courrier</option>
                        <option value="2">Mail</option>
                        <option value="3">SFTP</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select class="form-control show-tick" tabindex="-98" id="filtre-location-ged" multiple>
                        <option value="0"  >-- Tous --</option>
                        <option value="1" selected>GED Mada</option>
                        <option value="2">GED CE</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary waves-effect waves-light" id="stat_rech" onclick="charger_plis(1);">&nbsp;Rechercher</button>
                    <!--<button class="btn btn-info waves-effect waves-light" id="stat_rech" onclick="charger_plis(2);">&nbsp;Export ID</button> -->
                </div>
            </div>
    </div>
    <div class="preloader-reception"><span>Chargement de données...</span></div>
</div>

<div id="table-stat-ged-ce-mada">

</div>