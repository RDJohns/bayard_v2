<?php
ini_set('memory_limit', '-1');
 $url_src = base_url().'src/';
 $url_js = base_url().'src/';
 $date_debut = $_REQUEST["date_debut"];
 $date_fin   = $_REQUEST["date_fin"];


$tab_pli_ko_lib = array("ok_advantage"=>"Saisie Finie","ko_scan"=>"KO SCAN","ko_abondonne"=>"Cloturé sans traitement","ko_circulaire"=>"Circulaire","ko_ks"=>"KO SRC","ko_ke"=>"KO EN ATTENTE","ko_def"=>"KO Définitif","ko_inconnu"=>"KO Inconnu","ci_editee"=>"Cir. Editée","ci_envoyee"=>"Cir. envoyée","divise"=>"Divisé","ko"=>"KO","cloture"=>"Cloturé","cloture_ok"=>"Cloturé OK","ko"=>"PLIS KO", "ok_circ" => "Ok+circulaire","ko_bayard" => "KO en cours bayard");

$tab_pli_ko_color = array("ok_advantage"=>"bg-light-green","ko_scan"=>"bg-red7","ko_abondonne"=>"bg-red","ko_circulaire"=>"bg-brown","ko_ks"=>"bg-red5","ko_ke"=>"bg-purple","ko_def"=>"bg-red2","ko_inconnu"=>"bg-pink","ci_editee"=>"bg-red6","ci_envoyee"=>"bg-red6","ko"=>"bg-red8","cloture"=>"bg-light-green","cloture_ok"=>"bg-indigo","divise"=>"bg-grey","ok_circ" => "bg-grey","ko_bayard" => "bg-red5");

$tab_font_awesome = array("ok_advantage"=>"fa fa-check","ko_scan"=>"fa fa-print","ko_abondonne"=>"fa fa-bug","ko_circulaire"=>"fa fa-bell","ko_ks"=>"fa fa-share-square","ko_ke"=>"fa fa-clock-o","ko_def"=>"fa fa-clock-o","ko_inconnu"=>"fa fa-bug","ci_editee"=>"fa fa-clock-o","ci_envoyee"=>"fa fa-clock-o","ko"=>"fa fa-clock-o","cloture"=>"fa fa-bug","cloture_ok"=>"fa fa fa-check","ko_def"=>"fa fa-exclamation-triangle","divise"=>"fa fa-check","ok_circ" => "fa fa-bug","ko_bayard" => "fa fa-check");

?>

   <!-- Widgets -->
	<div class="row clearfix" style="margin-top:125px;text-align:center;">
		<?php 
		//var_dump($tab_pli);die;
		$ii = 0;
		foreach($tab_pli as $kpli_ko =>$list_pli){  
			if($kpli_ko != "KO") { // index "KO" ==> le total des KO
			//if($ii < 4) {
				
		?>
		 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
			<div class="info-box <?php echo $tab_pli_ko_color[$kpli_ko] ;?> hover-expand-effect">
				<div class="icon" style="text-align:center">
					<i class="<?php echo $tab_font_awesome[$kpli_ko]?>"></i>
				</div>
				<div class="content">
					<div class="text"  style="padding-bottom:3px;"><?php echo $tab_pli_ko_lib[$kpli_ko];?><span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php //echo $anomalie_percent." %";?></span></div>
					<div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"style="font-size:13px;font-weight: bold;"><?php echo $list_pli["nb"]." - ".$tab_pli_mvt[$kpli_ko]["nb"];?></div>
				</div>
			</div>
		</div>
		<?php $ii++; //} 
			}
		}?>
	
		
	</div>
	<!--div class="row clearfix" style="text-align:center;">
	   
		 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
		 </div>
		
		<?php 
		/*$ii = 0;
		foreach($tab_pli as $kpli_ko =>$list_pli){  
			if($kpli_ko != "KO") { // index "KO" ==> le total des KO
			if($ii >= 4) {*/
		?>
		 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
			<div class="info-box <?php //echo $tab_pli_ko_color[$kpli_ko] ;?> hover-expand-effect">
				<div class="icon" style="text-align:center">
					<i class="<?php //echo $tab_font_awesome[$kpli_ko]?>"></i>
				</div>
				<div class="content">
					<div class="text" style="padding-bottom:3px;"><?php //echo $tab_pli_ko_lib[$kpli_ko];?><span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php //echo $anomalie_percent." %";?></span></div>
					<div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20" style="font-size:13px;padding-bottom:5px;font-weight: bold;"><?php //echo $list_pli["nb"]." - ".$tab_pli_mvt[$kpli_ko]["nb"];?></div>
				</div>
			</div>
		</div>
			<?php /*}
			$ii++; } 
		}*/?>
		<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
		</div>
	 
	</div-->
		
	</br>

	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="tab_typo">
	   <div class="row clearfix">
	       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card trait_typo">
                        <div class="header">
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;font-weight: bold;">
							Traitement cloturé par typologie</span>
                            <ul class="header-dropdown m-r--5">
                               <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_traitement_typologie()" style="cursor: pointer;"> 
									   <i class="fa fa-close"> </i></a>
									</a>                                   
                                </li>
                            </ul>
						</div>
                        <div class="body">
							<div class="body table-responsive">
								<table class="table table-bordered table-striped table-hover dataTable js-table-typo" id="traitement_typo">
								</table>
							</div>
						</div>
                    </div>
                </div>
			</div>
		</div>
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12 ajax-saisie">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-1 m-l--20">
				<div class="preloader pl-size-xs" id="preloader" style="display:none;margin-left:10px;">
					<div class="spinner-layer pl-green" >
						<div class="circle-clipper left">
								<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
								<div class="circle"></div>
						</div>
						</div>
				</div>
			</div>
			<div class="row clearfix" id="ajax_saisie">
	        </div>
		</div>
		
	</div>
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12 ajax-mvt-saisie">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-1 m-l--20">
				<div class="preloader pl-size-xs" id="preloader_saisie" style="display:none;margin-left:10px;margin-top:50px;">
					<div class="spinner-layer pl-green" >
						<div class="circle-clipper left">
								<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
								<div class="circle"></div>
						</div>
						</div>
				</div>
			</div>
			<div class="row clearfix" id="ajax_mvt_saisie">
	        </div>
		</div>
	</div>
	
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12 ajax-controle">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="tab_journalier">
		<div class="col-lg-1 m-l--20">
				<div class="preloader pl-size-xs" id="preloader_controle_pli" style="display:none;margin-left:10px;margin-top:50px;">
					<div class="spinner-layer pl-green" >
						<div class="circle-clipper left">
								<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
								<div class="circle"></div>
						</div>
						</div>
				</div>
		</div>
	   <div class="row clearfix" id="ajax_controle">                
        </div>
	</div>
	</div>
	
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12 ajax-mvt-controle">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="tab_hebdo">
		<div class="col-lg-1 m-l--20">
				<div class="preloader pl-size-xs" id="preloader_controle_mvt" style="display:none;margin-left:10px;margin-top:50px;">
					<div class="spinner-layer pl-green" >
						<div class="circle-clipper left">
								<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
								<div class="circle"></div>
						</div>
						</div>
				</div>
		</div>
	    <div class="row clearfix" id="ajax_mvt_controle">
        </div>
	</div>
	</div>
	
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" display="display:none;border-top:50px;"  id="tab_traitement_date">
	 <div class="row clearfix">
	 <!-- Task Info -->
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
					Traitement par date de courrier</span>
					</br><span style="font-family:Arial,Tahoma,sans-serif;font-size:11px;">
					<i>* :OK, CI Envoyée, Cloturé sans traitement, Divisé</i></span>
				<ul class="header-dropdown m-r--5">
				   <li class="dropdown">
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						   <a onclick="quitter_traitement_date()" style="cursor: pointer;"> 
						   <i class="fa fa-close"> </i></a>
						</a>                                   
					</li>
				</ul>
			</div>
			<div class="body">
				<div class="body table-responsive">
					<table class="table table-bordered table-striped table-hover dataTable js-table-mvt" id="traitement_date_pli">
					</table>
				</div>
			</div>
			</div>
		</div>
	 </div>
	</div>
	</div>
	
	<div class="preloader" id="preloader_traitement_dtl">
		<div class="spinner-layer pl-black">
			<div class="circle-clipper left">
				<div class="circle"></div>
			</div>
			<div class="circle-clipper right">
				<div class="circle"></div>
			</div>
		</div>
	</div>	
					
	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" style="margin-top:60px;margin-bottom:60px;">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
         <div class="analytics-content" id="detail_stat">
        
		 <div class="body">
         <div class="table-responsive">
		
		<table class="table table-bordered table-striped table-hover dataTable js-exportable" id="traitement_pli" >                      
		 <thead  class="custom_font">
		
            <tr>
                <th>Société</th>
				<th>Traitement du</th>
				<th>Courrier du</th>
				<th>Lot scan</th>
				<th>Pli</th>
				<th>Id. pli</th>
				<th>Mouve-</br>ment</th>
				<th>Etape</th>
                <th>Etat</th>
                <th>Statut</th>
				<th>Desc. </br>anomalie</th>
				<th>Typologie</th>
				<th>Mode de paiement</th>
				<th>Cmc7</th>
				<th>Montant</th>						
				<th>Rlmc</th>
				<th>Datamatrix</th>
				<th>Motif rejet</th>
				<th>Description rejet</th>
				<th>Titre</th>
				<!--th>Societe titre</th-->	
            </tr>
			
        </thead> 
         <tbody class="custom_font"></tbody>
                               	
		</table>
         </div>
         </div>
		</div>
		</div>
    </div>
	</div>
		

	
