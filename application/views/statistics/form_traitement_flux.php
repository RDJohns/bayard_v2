<?php
ini_set('memory_limit', '-1');
$url_src = base_url().'src/';
$url_js = base_url().'src/';
$date_debut = $_REQUEST["date_debut"];
$date_fin   = $_REQUEST["date_fin"];


$tab_pli_ko_lib = array("saisie_finie"=>"Saisie Finie","flux_ok"=>"Flux OK","hors_perimetre"=>"Hors périmètre","anomalie"=>"Anomalie","escalade"=>"Escalade","transfert_consigne"=>"Transfert consigne","ko_def"=>"KO definitif","ko_inconnu"=>"KO inconnu","ko_ettent"=>"KO en attente", "ko_bayard"=>"KO en cours bayard");

$tab_pli_ko_color = array("saisie_finie"=>"bg-light-green","flux_ok"=>"bg-red7","hors_perimetre"=>"bg-blue-grey","anomalie"=>"bg-red9","escalade"=>"bg-cyan","transfert_consigne"=>"bg-brown","ko_def"=>"bg-pink","ko_inconnu"=>"bg-purple","ko_ettent"=>"bg-red", "ko_bayard"=>"bg-light-green");

$tab_font_awesome = array("saisie_finie"=>"fa fa-check","flux_ok"=>"fa fa-print","hors_perimetre"=>"fa fa-exclamation-triangle","anomalie"=>"fa fa-bell","escalade"=>"fa fa-share-square","transfert_consigne"=>"fa fa-clock-o","ko_def"=>"fa fa-times","ko_inconnu"=>"fa fa-times-circle","ko_ettent"=>"fa fa-minus-circle", "ko_bayard"=>"fa fa-bell");

?>
<div style="margin-top:125px;">
<!-- Widgets -->
<div class="row clearfix" style="text-align:center;">

    <!--div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display:inline-block;">
    </!--div-->
    <!--div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
			<div class="info-box bg-cyan-t hover-expand-effect">
				<div class="icon" style="text-align:center">
					<i class="fa fa-check-square"></i>
				</div>
				<div class="content">
					<div class="text"  style="padding-bottom:3px;">Pli chargé pour traitement<span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"></span></div>
					<div class="number count-to" data-from="0" data-to="242" data-speed="1000" data-fresh-interval="20" style="font-size:13px; font-weight: bold;"><?php //echo $total." - ".$mvt_total;?></div>
				</div>
			</div>
		</div-->

    <?php

    $ii = 0;
    //var_dump($tab_pli);
    //var_dump($tab_pli_mvt);
    foreach($tab_pli as $kpli_ko =>$list_pli){
        if($kpli_ko != "KO") { // index "KO" ==> le total des KO
            //if($ii < 5) {
                ?>
                <div class="" style="display:inline-block;width:18%;margin-left:10px;margin-right:10px;">
                    <div class="info-box <?php echo $tab_pli_ko_color[$kpli_ko] ;?> hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="<?php echo $tab_font_awesome[$kpli_ko]?>"></i>
                        </div>
                        <div class="content">
                            <div class="text"  style="padding-bottom:3px;"><?php echo $tab_pli_ko_lib[$kpli_ko];?><span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php //echo $anomalie_percent." %";?></span></div>
                            <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20" style="font-size:13px;font-weight: bold;"><?php echo (isset($list_pli["nb"])?$list_pli["nb"]:'0')." - ".(isset($tab_pli_mvt[$kpli_ko]["nb"])?$tab_pli_mvt[$kpli_ko]["nb"]:'0');?></div>
                        </div>
                    </div>
                </div>
                <?php $ii++; //}
        }
    }?>
</div>
<!--div class="row clearfix" style="text-align:center;">

  

    <?php
    $ii = 0;
    foreach($tab_pli as $kpli_ko =>$list_pli){
        if($kpli_ko != "KO") { // index "KO" ==> le total des KO
            if($ii >= 5) {
                ?>
                <div class="" style="display:inline-block;width:18%;margin-left:20px;">
                    <div class="info-box <?php echo $tab_pli_ko_color[$kpli_ko] ;?> hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="<?php echo $tab_font_awesome[$kpli_ko]?>"></i>
                        </div>
                        <div class="content">
                            <div class="text" style="padding-bottom:3px;"><?php echo $tab_pli_ko_lib[$kpli_ko];?><span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php //echo $anomalie_percent." %";?></span></div>
                            <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20" style="font-size:13px;padding-bottom:5px;font-weight: bold;"><?php echo (isset($list_pli["nb"])?$list_pli["nb"]:'0')." - ".(isset($tab_pli_mvt[$kpli_ko]["nb"])?$tab_pli_mvt[$kpli_ko]["nb"]:'0');?></div>
                        </div>
                    </div>
                </div>
            <?php }
            $ii++; }
    }?>
    

</div-->
</div>

</br>

<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" style="/*margin-top:50px;*/">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row clearfix" id="ajax_saisie">
        </div>
    </div>
</div>
<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row clearfix"  id="ajax_mvt_saisie">
        </div>
    </div>
</div>
<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" display="display:none;border-top:50px;"  id="tab_traitement_date">
        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
				<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
					Traitement par date de courrier</span>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <a onclick="quitter_traitement_date()" style="cursor: pointer;">
                                        <i class="fa fa-close"> </i></a>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="body table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-table-mvt" id="traitement_date_pli" style="width: 100%;">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="preloader" id="preloader_traitement_dtl">
    <div class="spinner-layer pl-black">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div>
        <div class="circle-clipper right">
            <div class="circle"></div>
        </div>
    </div>
</div>

<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" style="margin-top:60px;margin-bottom:60px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
            <div class="analytics-content" id="detail_stat">

                <div class="body">
                    <div class="table-responsive">

                        <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="traitement_flux" >
                            <thead  class="custom_font">

                            <tr>
                                <th>Société</th>
                                <th>Traitement du</th>
                                <th>Courrier du</th>
                                <th>Sujet</th>
                                <th>Source</th>
                                <th>Id. flux</th>
                                <th>Date<br>saisie adv</th>
                                <th>Id lot<br>saisie flux</th>
                                <th>Typologie</th>
                                <th>Statut</th>
                                <th>Lot advantage</th>
                                <th>Nb<br>abonnement</th>
                                <!--th>Societe titre</th-->
                            </tr>

                            </thead>
                            <tbody class="custom_font"></tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



