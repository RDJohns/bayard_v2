
<!--div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab21" display="display:none"-->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Détail hebdo des plis KO</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab32()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_detail_plis_ko_hebdo = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:10%;text-align:center;">Traitement S</th>
										<th  style="width:15%;text-align:center;">KO Inconnu</th>
										<th  style="width:15%;text-align:center;">KO Scan</th>
										<th  style="width:15%;text-align:center;">KO Call</th>
										<th  style="width:15%;text-align:center;">KO Circulaire</th>
										<th  style="width:15%;text-align:center;">KO en attente</th>
										<th  style="width:15%;text-align:center;">KO réclam-<br>mation</th>
										<th  style="width:15%;text-align:center;">KO litige</th>
										<th  style="width:15%;text-align:center;">KO Définitif</th>
										                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                  
									$j1 = 0;
									$tab_plis_ko_hebdo = array();
									
									foreach($liste_result_plis_ko as $k_plis_ko_par_date =>$tab_plis_ko_par_date){
										
										
										$num_semaine = $liste_date["num"][$tab_plis_ko_par_date["dt_event"]];
										
										if (empty($tab_plis_ko_hebdo[$num_semaine]["ko_inconnu"]))
											$tab_plis_ko_hebdo[$num_semaine]["ko_inconnu"] = $tab_plis_ko_par_date["ko_inconnu"];
										else
										$tab_plis_ko_hebdo[$num_semaine]["ko_inconnu"] += $tab_plis_ko_par_date["ko_inconnu"];
										
										if (empty($tab_plis_ko_hebdo[$num_semaine]["ko_scan"]))
											$tab_plis_ko_hebdo[$num_semaine]["ko_scan"] = $tab_plis_ko_par_date["ko_scan"];
										else
										$tab_plis_ko_hebdo[$num_semaine]["ko_scan"] += $tab_plis_ko_par_date["ko_scan"];
										
										if (empty($tab_plis_ko_hebdo[$num_semaine]["ko_call"]))
											$tab_plis_ko_hebdo[$num_semaine]["ko_call"] = $tab_plis_ko_par_date["ko_call"];
										else
										$tab_plis_ko_hebdo[$num_semaine]["ko_call"] += $tab_plis_ko_par_date["ko_call"];
										
										if (empty($tab_plis_ko_hebdo[$num_semaine]["ko_def"]))
											$tab_plis_ko_hebdo[$num_semaine]["ko_def"] = $tab_plis_ko_par_date["ko_def"];
										else
										$tab_plis_ko_hebdo[$num_semaine]["ko_def"] += $tab_plis_ko_par_date["ko_def"];
										
										if (empty($tab_plis_ko_hebdo[$num_semaine]["ko_circulaire"]))
											$tab_plis_ko_hebdo[$num_semaine]["ko_circulaire"] = $tab_plis_ko_par_date["ko_circulaire"];
										else
										$tab_plis_ko_hebdo[$num_semaine]["ko_circulaire"] += $tab_plis_ko_par_date["ko_circulaire"];
									
										if (empty($tab_plis_ko_hebdo[$num_semaine]["ko_en_attente"]))
											$tab_plis_ko_hebdo[$num_semaine]["ko_en_attente"] = $tab_plis_ko_par_date["ko_en_attente"];
										else
										$tab_plis_ko_hebdo[$num_semaine]["ko_en_attente"] += $tab_plis_ko_par_date["ko_en_attente"];
									
										if (empty($tab_plis_ko_hebdo[$num_semaine]["ko_reclammation"]))
											$tab_plis_ko_hebdo[$num_semaine]["ko_reclammation"] = $tab_plis_ko_par_date["ko_reclammation"];
										else
										$tab_plis_ko_hebdo[$num_semaine]["ko_reclammation"] += $tab_plis_ko_par_date["ko_reclammation"];
									
										if (empty($tab_plis_ko_hebdo[$num_semaine]["ko_litige"]))
											$tab_plis_ko_hebdo[$num_semaine]["ko_litige"] = $tab_plis_ko_par_date["ko_litige"];
										else
										$tab_plis_ko_hebdo[$num_semaine]["ko_litige"] += $tab_plis_ko_par_date["ko_litige"];
									
									}
									
									foreach($tab_plis_ko_hebdo as $kpli_plis_ko_hebdo => $vpli_plis_ko_hebdo){ 
									    
										$content_detail_plis_ko_hebdo .= '<tr>
                                        <td style="text-align:center;">'.$kpli_plis_ko_hebdo.'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_hebdo["ko_inconnu"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_hebdo["ko_scan"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_hebdo["ko_call"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_hebdo["ko_circulaire"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_hebdo["ko_en_attente"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_hebdo["ko_reclammation"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_hebdo["ko_litige"].'</td>										
										<td style="text-align:center;">'.$vpli_plis_ko_hebdo["ko_def"].'</td>
										</tr>';
										
									}
								 $content_detail_plis_ko_hebdo .='</tbody>
                            </table>';
							echo $content_detail_plis_ko_hebdo;
								//echo "<pre>";print_r($tab_plis_ko_hebdo);	echo "</pre>";
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	<!--/div-->
                           