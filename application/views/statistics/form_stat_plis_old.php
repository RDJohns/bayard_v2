<?php
 $url_src = base_url().'src/';
 $url_js = base_url().'src/';
 $date_debut = $_REQUEST["date_debut"];
 $date_fin   = $_REQUEST["date_fin"];
 
 

?>

	 <!--div class="row"-->
<?php

    foreach($liste_pli as $pli){
		$nouveau_percent = 0;
		$encours_percent = 0;
		$traite_percent = 0;
		$anomalie_percent = 0;
		$pli_total =  $pli['nouveau']+$pli['encours']+$pli['traite']+$pli['anomalie'];
		if ($pli_total > 0){
			$nouveau_percent  = $pli['nouveau']*100 / $pli_total;
			$encours_percent  = $pli['encours']*100 / $pli_total;
			$traite_percent   = $pli['traite']*100 / $pli_total;
			$anomalie_percent = $pli['anomalie']*100 / $pli_total;
			$nouveau_percent  = ($nouveau_percent > 0) ? number_format($nouveau_percent,2) : 0;
			$encours_percent  = ($encours_percent > 0) ? number_format($encours_percent,2) : 0;
			$traite_percent   = ($traite_percent > 0) ? number_format($traite_percent,2) : 0;
			$anomalie_percent  = ($anomalie_percent > 0) ? number_format($anomalie_percent,2) : 0;
		}
   }

?>

   <!-- Widgets -->
            <div class="row clearfix" style="text-align:center;">
               
				 <div class="col-lg-1 col-md-1 col-sm-6 col-xs-12">
				 </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan-t hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-envelope"></i>
						</div>
                        <div class="content">
                            <div class="text">Plis total</div>
                            <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo $pli_total;?></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-plus-square"></i>
                        </div>
                        <div class="content">
                            <div class="text">Non traité<span style="float:right; width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php echo $nouveau_percent;?> %</span></div>
                            <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo $pli['nouveau'];?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-check-square"></i>
                        </div>
                        <div class="content">
                            <div class="text">Traité<span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php echo $traite_percent;?> %</span></div>
                            <div class="number count-to" data-from="0" data-to="242" data-speed="1000" data-fresh-interval="20"><?php echo $pli['traite'];?></div>
                        </div>
                    </div>
                </div>
				 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-cogs"></i>
                        </div>
                        <div class="content">
                            <div class="text">En cours<span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php echo $encours_percent;?> %</span></div>
                            <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"><?php echo $pli['encours'];?></div>
                        </div>
                    </div>
                </div>
				 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon" style="text-align:center">
                            <i class="fa fa-bug"></i>
                        </div>
                        <div class="content">
                            <div class="text">Anomalie<span style="float:right;width:50%;margin:-35px 0px 10px 10px;font-size:10px;"><?php echo $anomalie_percent;?> %</span></div>
                            <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $pli['anomalie'];?></div>
                        </div>
                    </div>
                </div>
			<div class="col-lg-1 col-md-1 col-sm-6 col-xs-12">
			</div>
			 
            </div>
			
			

	</br>
	<div class="row" style="padding:10px 0px 0px 17px;">
	
	<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
						
           <!-- Basic Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-size:11px;font-weight:bold;">Répartition des plis par Moyen de paiement</span>
                            <ul class="header-dropdown m-r--5">
                             </ul>
                        </div>
                        <div class="body table-responsive">
                            <?php $content_type_doc = '<table class="table">
                                <thead   class="custom_font">
                                    <tr>
                                        <th>#Pli avec</th>
                                        <th>Nbr.</th>
                                        <th>%</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                  
									$j1 = 0;
									foreach($data_document as $kdoct =>$tab_doc){
										
										$content_type_doc .= '<tr>
                                        <th scope="row" style="font-weight:normal;">'.$tab_doc["code"].'</th>
										<td>'.$tab_doc["nb"].'</td>
										<td>'.$tab_doc["pourcent"].' %</td>
										</tr>';
										
									}
								 $content_type_doc .=' </tbody>
								  <tfoot   class="custom_font">
                                    <tr>
                                        <th>Plis total</th>
                                        <th>'.$pli_total.'</th>
                                        <th></th>
                                      
                                    </tr>
                                </tfoot>
                            </table>';
							echo $content_type_doc;
									
							?>
                        </div>
                    </div>
                </div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card" >
						<div class="header">
							<span style="font-size:11px;font-weight:bold;">Plis par Moyen de paiement</span>
						</div>
						<div class="body" style="padding:2px 2px 2px 2px">
							<div style="height: 150px;width:auto" id="donut_document_chart" class="graph">
							</div>
						</div>
					</div>
                </div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-size:11px;font-weight:bold;">Plis BDC Cloturés</span>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons" style="font-size:10px;padding:50px 30px 0px 10px;"><?php // echo $data_plis_cloture["total"];?></i> 
                                    </a>
                                    <!--ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul-->
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <?php 
							
							$content_type_pli = '<table class="table">
                                <thead   class="custom_font">
                                    <tr>
                                        <th>#Type de pli</th>
                                        <th>Nbr.</th>
                                        <th>%</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';						
                                  
									$j = 0;
									$tab_pli_traite_lib = array("BDC_CHQ"=>"BDC payé par CHQ","BDC_CB"=>"BDC payé par CB","BDC_ESP"=>"BDC payé par ESPECES","MANDAT"=>"BDC payé par MANDAT FACTURE","CLOTURE"=>"PLIS CLOTURES");
									
									foreach($data_plis_cloture as $kpli_cloture =>$tab_pli_cloture){ 
										if ($tab_pli_cloture["code"] == "CLOTURE"){
											$content_type_pli .= '<tr>
											<th scope="row" style="font-weight:normal;"><b>'.$tab_pli_traite_lib[$kpli_cloture].'</b></th>
											<td><b>'.$tab_pli_cloture["nb"].'</b></td>
											<td><b>'.$tab_pli_cloture["pourcent"].'%</b></td>
											</tr>';
										}
										else{
										$content_type_pli .= '<tr>
                                        <th scope="row" style="font-weight:normal;">'.$tab_pli_traite_lib[$kpli_cloture].'</th>
										<td>'.$tab_pli_cloture["nb"].'</td>
										<td>'.$tab_pli_cloture["pourcent"].'%</td>
										</tr>';
										}
									}
									  
                               $content_type_pli .=' </tbody>
                            </table>';
							echo $content_type_pli;
									
							?>
                        </div>
                    </div>
                </div>
			</div>
				<div class="col">
                    <div class="row-lg-3 row-md-6 row-sm-6 row-xs-12">
                      
                    </div>
                       <!-- Donut Chart -->
								<!-- Donut Chart -->
							<div class="row-lg-3 row-md-6 row-sm-6 row-xs-12">
							<div class="card">
								<div class="header">
									<span style="font-size:11px;font-weight:bold;">Répartition des plis BDC Cloturés</span>
									<ul class="header-dropdown m-r--5">
										<li class="dropdown">
											<!--a href="repartition_par_doc('<?php //echo $date_debut;?>','<?php //echo $date_fin ?>')" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
												<i class="material-icons" style="font-size:10px;padding:10px 6px 0px 10px;">plus d'infos...</i>
											</a>
											<ul class="dropdown-menu pull-right">
												<li><a onclick="repartition_par_doc('<?php //echo $date_debut;?>','<?php //echo $date_fin ?>')">Voir le tableau</a></li>
												<li><a onclick="voir_graph('<?php //echo $date_debut;?>','<?php //echo $date_fin ?>')">voir le graphe</a></li>
												
											</ul-->
										</li>
									</ul>
								</div>
								<div class="body" style="padding:2px 2px 2px 2px">
									<div style="height: 250px;width:auto"  id="donut_chart" class="graph">
									</div>
								</div>
								</div>
							</div>			
							<input type="hidden" value="<?php echo $data_donut_plis_traite ;?>" id="graph_donut"/>
							<div class="row-lg-3 row-md-6 row-sm-6 row-xs-12">
							</div>	
							<input type="hidden" value="<?php echo $data_donut_document ;?>" id="graph_donut_doc"/>
                   </div>
       </div>
	
	
	<script type="javascript">
		getMorris('donut', 'donut_chart');
	</script>
	<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
         <div class="analytics-content" id="detail_stat">
        
		 <div class="body">
         <div class="table-responsive">
		<?php 
		
		$list_pli = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">                      
		 <thead  class="custom_font">
		
            <tr>
                <th>Pli</th>
                <th>Traitement du</th>
                <th>Statut</th>
                <th>Lot Scan</th>
                <th>Courrier du</th>
                <th>Numérisation du</th>
                <th>Document</th>
            </tr>
        </thead>
         <tbody class="custom_font">';
			 
				$j = 0;
				
				
				for($j = 0; $j < count($list); $j++){
					if($list[$j]["statut"] == 'Anomalie')
						$list[$j]["statut"] = $list[$j]["type_ko"];
				  $list_pli .= '<tr>
					 <td>'.$list[$j]["pli"].'</td>
					 <td>'.$list[$j]["dt_event"].'</td>
					 <td>'.$list[$j]["statut"].'</td>
					 <td>'.$list[$j]["lot_scan"].'</td>
					 <td>'.$list[$j]["date_courrier"].'</td>
					 <td>'.$list[$j]["date_numerisation"].'</td>
					 <td>'.$list[$j]["nb_doc"].'</td>
				</tr>';
					

				}
			
				
			$list_pli .= '</tbody>
                               	
		</table>';
			echo $list_pli ;
		 ?>
         </div>
         </div>
		</div>
		</div>
    </div>
	</div>
