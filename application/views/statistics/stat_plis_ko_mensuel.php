
<!--div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab21" display="display:none"-->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Détail mensuel des plis KO</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab33()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php
							$content_detail_plis_ko_mens = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:10%;text-align:center;">Mois</th>
										<th  style="width:15%;text-align:center;">KO Inconnu</th>
										<th  style="width:15%;text-align:center;">KO Scan</th>
										<th  style="width:15%;text-align:center;">KO Call</th>
										<th  style="width:15%;text-align:center;">KO Circulaire</th>
										<th  style="width:15%;text-align:center;">KO en attente</th>
										<th  style="width:15%;text-align:center;">KO Définitif</th>
                                      
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                  
									$j1 = 0;
									$tab_plis_ko_mens = array();
									foreach($liste_result_plis_ko as $k_plis_ko_par_date =>$tab_plis_ko_par_date){
										
										$mois = $liste_mois["mois"][$tab_plis_ko_par_date["dt_event"]];
										
										if (empty($tab_plis_ko_mens[$mois]["ko_inconnu"]))
											$tab_plis_ko_mens[$mois]["ko_inconnu"] = $tab_plis_ko_par_date["ko_inconnu"];
										else
										$tab_plis_ko_mens[$mois]["ko_inconnu"] += $tab_plis_ko_par_date["ko_inconnu"];
										
										if (empty($tab_plis_ko_mens[$mois]["ko_scan"]))
											$tab_plis_ko_mens[$mois]["ko_scan"] = $tab_plis_ko_par_date["ko_scan"];
										else
										$tab_plis_ko_mens[$mois]["ko_scan"] += $tab_plis_ko_par_date["ko_scan"];
										
										if (empty($tab_plis_ko_mens[$mois]["ko_call"]))
											$tab_plis_ko_mens[$mois]["ko_call"] = $tab_plis_ko_par_date["ko_call"];
										else
										$tab_plis_ko_mens[$mois]["ko_call"] += $tab_plis_ko_par_date["ko_call"];
										
										if (empty($tab_plis_ko_mens[$mois]["ko_def"]))
											$tab_plis_ko_mens[$mois]["ko_def"] = $tab_plis_ko_par_date["ko_def"];
										else
										$tab_plis_ko_mens[$mois]["ko_def"] += $tab_plis_ko_par_date["ko_def"];
									
										if (empty($tab_plis_ko_mens[$mois]["ko_circulaire"]))
											$tab_plis_ko_mens[$mois]["ko_circulaire"] = $tab_plis_ko_par_date["ko_circulaire"];
										else
										$tab_plis_ko_mens[$mois]["ko_circulaire"] += $tab_plis_ko_par_date["ko_circulaire"];
									
										if (empty($tab_plis_ko_mens[$mois]["ko_en_attente"]))
											$tab_plis_ko_mens[$mois]["ko_en_attente"] = $tab_plis_ko_par_date["ko_en_attente"];
										else
										$tab_plis_ko_mens[$mois]["ko_en_attente"] += $tab_plis_ko_par_date["ko_en_attente"];
																
									}
									foreach($tab_plis_ko_mens as $kpli_plis_ko_mens => $vpli_plis_ko_mens){ 
									    
										$content_detail_plis_ko_mens .= '<tr>
                                        <td style="text-align:center;">'.$kpli_plis_ko_mens.'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_mens["ko_inconnu"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_mens["ko_scan"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_mens["ko_call"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_mens["ko_circulaire"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_mens["ko_en_attente"].'</td>
										<td style="text-align:center;">'.$vpli_plis_ko_mens["ko_def"].'</td>
										</tr>';
										
									}
								 $content_detail_plis_ko_mens .='</tbody>
                            </table>';
							echo $content_detail_plis_ko_mens;
									
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	<!--/div-->
                           