<?php
$url_src    = base_url().'src/';
$url_js     = base_url().'src/';
$date_debut = $_REQUEST["date_debut"];
$date_fin   = $_REQUEST["date_fin"];

?>
<!--div class="row"-->
<?php
$pli_total = 0;
$nouveau_percent  = $non_traite = 0;
$encours_percent  = $encours = 0;
$traite_percent   = $traite = 0;
$anomalie_percent = $anomalie = 0;

$ko_mail = $ko_pj = 0;
$ko_fichier = $ko_src = 0;
$ko_definitif = $ko_inconnu = 0;
$ko_attente = $ko_rejete = $ko_hp = 0;


foreach($liste_pli as $pli){
    $pli_total +=  $pli['non_traite']+$pli['encours']+$pli['traite']+$pli['anomalie'];
    if ($pli['non_traite'] > 0) $non_traite += $pli['non_traite'];
    if ($pli['encours'] > 0)    $encours += $pli['encours'];
    if ($pli['traite'] > 0)     $traite += $pli['traite'];
    if ($pli['anomalie'] > 0)   $anomalie += $pli['anomalie'];
}

foreach ($list_pli_ko as $pliko){
    if ($pliko['ko_mail'] > 0)    		$ko_mail += $pliko['ko_mail'];
    if ($pliko['ko_pj'] > 0)    	    $ko_pj += $pliko['ko_pj'];
    if ($pliko['ko_fichier'] > 0)     	$ko_fichier += $pliko['ko_fichier'];
    if ($pliko['ko_src'] > 0)   		$ko_src += $pliko['ko_src'];
    if ($pliko['ko_definitif'] > 0)     $ko_definitif += $pliko['ko_definitif'];
    if ($pliko['ko_attente'] > 0)       $ko_attente += $pliko['ko_attente'];
    if ($pliko['ko_inconnu'] > 0)  	    $ko_inconnu += $pliko['ko_inconnu'];
    if ($pliko['ko_rejete'] > 0)  	    $ko_inconnu += $pliko['ko_rejete'];
    if ($pliko['ko_hp'] > 0)  	        $ko_inconnu += $pliko['ko_hp'];
}

$nouveau_percent   = ($pli_total > 0) ? $non_traite*100 / $pli_total : 0 ;
$encours_percent   = ($pli_total > 0) ? $encours*100 / $pli_total: 0 ;
$traite_percent    = ($pli_total > 0) ? $traite*100 / $pli_total: 0 ;
$anomalie_percent  = ($pli_total > 0) ? $anomalie*100 / $pli_total: 0 ;

$nouveau_percent   = ($nouveau_percent > 0) ? number_format($nouveau_percent,2) : 0;
$encours_percent   = ($encours_percent > 0) ? number_format($encours_percent,2) : 0;
$traite_percent    = ($traite_percent > 0) ? number_format($traite_percent,2) : 0;
$anomalie_percent  = ($anomalie_percent > 0) ? number_format($anomalie_percent,2) : 0;

$komail_perc        = ($pli_total > 0) ? number_format($ko_mail*100 / $pli_total,2) : 0 ;
$kopj_perc          = ($pli_total > 0) ? number_format($ko_pj*100 / $pli_total,2) : 0 ;
$kofichier_perc     = ($pli_total > 0) ? number_format($ko_fichier*100 / $pli_total,2) : 0 ;
$kosrc_perc         = ($pli_total > 0) ? number_format($ko_src*100 / $pli_total,2) : 0 ;
$kodefinitif_perc   = ($pli_total > 0) ? number_format($ko_definitif*100 / $pli_total,2) : 0 ;
$koinconnu_perc     = ($pli_total > 0) ? number_format($ko_inconnu*100 / $pli_total,2) : 0 ;
$koattente_perc     = ($pli_total > 0) ? number_format($ko_attente*100 / $pli_total,2) : 0 ;
$korejete_perc      = ($pli_total > 0) ? number_format($ko_rejete*100 / $pli_total,2) : 0 ;
$kohp_perc          = ($pli_total > 0) ? number_format($ko_hp*100 / $pli_total,2) : 0 ;

?>

<!-- Widgets -->
<div class="row clearfix" style="margin-top:125px;text-align:center;text-align:center;">

    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-cyan-t hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-envelope"></i>
            </div>
            <div class="content">
                <div class="text">Flux total</div>
                <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo $pli_total;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-cyan hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-plus-square"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $nouveau_percent;?> %</span></div>
                <div class="text">Non traité<span ></div>
                <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo $non_traite;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-light-green hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-check-square"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $traite_percent;?> %</span></div>

                <div class="text">Cloturé</div>
                <div class="number count-to" data-from="0" data-to="242" data-speed="1000" data-fresh-interval="20"><?php echo $traite;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-orange hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-cogs"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $encours_percent;?> %</span></div>
                <div class="text">En cours</div>
                <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"><?php echo $encours;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $kohp_perc;?> %</span></div>
                <div class="text">Hors P&eacute;rimetre</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_rejete;?></div>
            </div>
        </div>
    </div>
    <?php if($id_source == 1){ ?>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $komail_perc;?> %</span></div>
                <div class="text">Anomalie Mail</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_mail;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $kopj_perc;?> %</span></div>
                <div class="text">Anomalie PJ</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_pj;?></div>
            </div>
        </div>
    </div>
    <?php
    } else {
    ?>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon" style="text-align:center">
                    <i class="fa fa-bug"></i>
                </div>
                <div class="content">
                    <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $kofichier_perc;?> %</span></div>
                    <div class="text">Anomalie Fichier</div>
                    <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_fichier;?></div>
                </div>
            </div>
        </div>
    <?php }
    ?>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $kosrc_perc;?> %</span></div>
                <div class="text">KO SRC</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_src;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $kodefinitif_perc;?> %</span></div>
                <div class="text">KO D&eacute;finitif</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_definitif;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $koinconnu_perc;?> %</span></div>
                <div class="text">KO Inconnu</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_inconnu;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $koattente_perc;?> %</span></div>
                <div class="text">KO En Attente</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_attente;?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon" style="text-align:center">
                <i class="fa fa-bug"></i>
            </div>
            <div class="content">
                <div class="text" style="float:right; width:50%;margin:0px -12px 0px 90px;font-size:10px;"><?php echo $korejete_perc;?> %</span></div>
                <div class="text">Rejet&eacute;</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php echo $ko_rejete;?></div>
            </div>
        </div>
    </div>

</div>
</br>
<div class="col-md-12 col-sm-12 col-xs-12" id="tab_menseuel_0" style="display:;margin-top:10px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12" >
                <div class="card">
                    <div class="header">
                        <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Réception</span>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <a onclick="quitter_tab_mensuel_0()" style="cursor: pointer;">
                                        <i class="fa fa-close"> </i></a>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <?php
                            $col_source = '';
                            if($id_source == 1){
                                $col_source .= '<th rowspan="2">Anomalie Mail</th>
                                <th rowspan="2">Anomalie PJ</th>';
                            }
                            else{
                                $col_source .= '<th rowspan="2">Anomalie Fichier</th>';
                            }
                            $content_recep_mois = '<table class="table table-bordered table-striped table-hover dataTable js-exportable"  style="width:100% !important;">  
                                <thead   class="custom_font">
									<tr>
                                        <th  style="width:10%;text-align:center;"  rowspan="2">Réception du</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Total</th>
                                        <th  style="width:10%;text-align:center"  rowspan="2">Cloturé</th>
										<!--th  style="width:10%;text-align:center"  rowspan="2">En cours</th-->
										<th  style="width:10%;text-align:center"  colspan="2">Restant à traiter</th>
										<th  style="width:10%;text-align:center"  rowspan="2">Anomalie</th>
										'.$col_source.'
										<th style="width:10%;text-align:center;"  rowspan="2">KO SRC</th>
										<th style="width:10%;text-align:center;"  rowspan="2">KO Définitif</th>
										<th style="width:10%;text-align:center;"  rowspan="2">KO Inconnu</th>
										<th style="width:10%;text-align:center;"  rowspan="2">KO En Attente</th>
										<th style="width:10%;text-align:center;"  rowspan="2">Hors périmetre</th>
										<th style="width:10%;text-align:center;"  rowspan="2">Rejeté</th>
										<th style="width:40%;text-align:center"  colspan="2">Total Cloturé</th>										
                                      
                                    </tr>
                                    <tr>
                                       
                                        <th  style="width:10%;text-align:center">En cours</th>
                                        <th  style="width:10%;text-align:center">Non traité</th>
                                        <th  style="width:10%;text-align:center">Traité</th>
										<th  style="width:10%;text-align:center">Sans traitement</th>															
										
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';

                            $j0 = 0;
                            $k =0;
                            //global
                            $data_set_cloture = array();
                            $data_set_encours = array();
                            $data_set_nontraite = array();
                            $data_set_anomalie_glb = array();
                            $data_set_labels  = array();
                            //ko
                            $data_set_traite    = array();
                            $data_set_hp = array();
                            $data_set_rejete    = array();
                            //$data_set_kd    = array();

                            foreach($liste_pli as $k_plis =>$tab_plis){
                                //préparaion des données pour les graphes reception global
                                if(intval( $tab_plis["traite"])+intval( $tab_plis["encours"])+intval( $tab_plis["non_traite"])+intval($tab_plis["anomalie"])>0){
                                    $data_set_cloture[$k]   = $tab_plis["traite"];
                                    $data_set_encours[$k]   = $tab_plis["encours"];
                                    $data_set_nontraite[$k] = $tab_plis["non_traite"];
                                    $data_set_anomalie_glb[$k]  = $tab_plis["anomalie"];
                                    $data_set_labels[$k]    = $tab_plis["lib_daty"];
                                    //préparaion des données pour les graphes Total KO
                                    $data_set_traite[$k]    = $tab_plis["nb_flux_ok"];
                                    $data_set_hp[$k]   		= $tab_plis["nb_flux_hp"];
                                    $data_set_rejete[$k]      	= $tab_plis["nb_flux_rejete"];

                                    $totaly = intval( $tab_plis["traite"])+intval( $tab_plis["encours"])+intval( $tab_plis["non_traite"])+intval( $tab_plis["anomalie"]);
                                    $k++;
                                    $content_col = '';
                                    if($id_source == 1){
                                        $content_col .= '<td>'.$tab_plis['nb_ko_mail'].'</td>
                                        <td>'.$tab_plis['nb_ko_pj'].'</td>';
                                    }
                                    else{
                                        $content_col .= '<td>'.$tab_plis['nb_ko_fichier'].'</td>';
                                    }
                                    $content_recep_mois .= '<tr>
                                        <td style="text-align:center"><a style="cursor:pointer;" onclick="get_detail_mensuel(\''.$tab_plis["daty"].'\')">'.$tab_plis["lib_daty"].'</a></td>
                                        <td style="text-align:center">'.$totaly.'</td>
                                        <td style="text-align:center">'.$tab_plis["traite"].'</td>
										<td style="text-align:center">'.$tab_plis["encours"].'</td>
										<td style="text-align:center">'.$tab_plis["non_traite"].'</td>
										<td style="text-align:center">'.$tab_plis["anomalie"].'</td>
										'.$content_col.'
										<td style="text-align:center">'.$tab_plis["nb_ko_src"].'</td>
										<td style="text-align:center">'.$tab_plis["nb_ko_definitif"].'</td>
										<td style="text-align:center">'.$tab_plis["nb_ko_inconnu"].'</td>
										<td style="text-align:center">'.$tab_plis["nb_ko_attente"].'</td>
										<td style="text-align:center">'.$tab_plis["nb_ko_hp"].'</td>
										<td style="text-align:center">'.$tab_plis["nb_ko_rejete"].'</td>
										<td style="text-align:center">'.$tab_plis["nb_flux_ok"].'</td>
										<td style="text-align:center">'.$tab_plis["nb_flux_sans_traitement"].'</td>
										</tr>';
                                }
                            }
                            $content_recep_mois .='</tbody>
                            </table>';
                            echo $content_recep_mois;
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <!-- #END# Task Info -->
            <!-- Browser Usage -->

        </div>
    </div>
    <br>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="tab_detail_mensuel" style="display:none;" >
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12" id="tab_menseuel_0" style="margin-top:50px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12" >
                <div class="card">
                    <div class="header">
                        <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Réception - Flux passés en KO</span>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <a onclick="quitter_tab_mensuel_0()" style="cursor: pointer;">
                                        <i class="fa fa-close"> </i></a>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <?php
                            $col_par_source = '';
                            if($id_source == 1){
                                $col_par_source .= '<th  style="width:10%;text-align:center">Anomalie Mail</th>';
                                $col_par_source .= '<th  style="width:10%;text-align:center">Anomalie PJ</th>';
                            }
                            else{
                                $col_par_source .= '<th  style="width:10%;text-align:center">Anomalie Fichier</th>';
                            }
                            $content_recep_mois = '<table style="width: 100%;" class="table table-bordered table-striped table-hover dataTable table-stat-mensuel-ko">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:10%;text-align:center;">Réception du</th>
                                        <th  style="width:10%;text-align:center">Total</th>
                                        '.$col_par_source.' 
										<th  style="width:10%;text-align:center">KO SRC</th>
										<th  style="width:10%;text-align:center">KO Définitif</th>
										<th  style="width:10%;text-align:center">KO Inconnu</th>
										<th  style="width:10%;text-align:center">KO En attente</th>
										<th  style="width:10%;text-align:center">Hors périmetre</th>
										<th  style="width:10%;text-align:center">Rejeté</th>																
										
                                    </tr>
                                  
                                </thead>
                                <tbody   class="custom_font">';

                            $j0 = 0;
                            $k  = 0;
                            $col_detail_source = '';

                            foreach($list_histo_ko as $k_plis =>$tab_ko){

                                if(intval($tab_ko["ko_mail"])>0 || intval($tab_ko["ko_pj"])>0 || intval($tab_ko["ko_fichier"])>0
                                    || intval($tab_ko["ko_src"])>0|| intval($tab_ko["ko_definitif"])>0|| intval($tab_ko["ko_inconnu"])>0|| intval($tab_ko["ko_attente"])>0
                                    || intval($tab_ko["ko_hp"])>0|| intval($tab_ko["ko_rejete"])>0 ){


                                    $k++;
                                    if($id_source == 1){
                                        $col_detail_source .= '<td style="text-align:center">'.$tab_ko["ko_mail"].'</td>';
                                        $col_detail_source .= '<td style="text-align:center">'.$tab_ko["ko_pj"].'</td>';
                                    }
                                    else{
                                        $col_detail_source .= '<td style="text-align:center">'.$tab_ko["ko_fichier"].'</td>';
                                    }
                                    $content_recep_mois .= '<tr>
                                        <td style="text-align:center">'.$tab_ko["lib_daty"].'</td>
                                        <td style="text-align:center">'.$tab_ko["flux_total"].'</td>
										'.$col_detail_source.' 
										<td style="text-align:center">'.$tab_ko["ko_src"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_definitif"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_inconnu"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_attente"].'</td>										
										<td style="text-align:center">'.$tab_ko["ko_hp"].'</td>
										<td style="text-align:center">'.$tab_ko["ko_rejete"].'</td>
									</tr>';
                                }
                            }
                            $content_recep_mois .='</tbody>
                            </table>';
                            echo $content_recep_mois;
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <!-- #END# Task Info -->
            <!-- Browser Usage -->

        </div>
    </div>

</div>

<div class="row" style="padding:10px 0px 0px 0px;"  style="margin-top:50px;">


    <input type="hidden" value="<?php //echo $data_donut_document ;?>" id="graph_donut_doc"/>
    <input type="hidden" value="<?php //echo $data_donut_plis_traite ;?>" id="graph_donut"/>
    <input type="hidden" value="<?php //echo $data_donut_plis_ko ;?>" id="graph_donut_plis_ko"/>
    <input type="hidden" value="<?php //echo $data_donut_plis_restant;?>" id="graph_donut_plis_restant"/>

    <div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab_pli_mpt_mesuel" display="display:">


        <div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab_pli_cloture_mesuel" display="display:;" style="margin-bottom:50px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12"  style="margin-top:50px;">
                        <div class="card">
                            <div class="header">
                                <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Flux cloturés par typologie</span>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <a onclick="quitter_pli_cloture_mensuel()" style="cursor: pointer;">
                                                <i class="fa fa-close"></i></a>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="table-responsive">
                                    <?php
                                    $content_pli_cloture = '<table class="table table-bordered table-striped table-hover dataTable table-stat-mensuel-typologie">  
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:10%;text-align:center;background: white !important;">Réception du</th>';

                                    for($jj=1; $jj<=18;$jj++){
                                        $content_pli_cloture .= '<th>'.$tab_typo[$jj].'</th>';
                                    }

                                    for($jj=34; $jj<=36;$jj++){
                                        $content_pli_cloture .= '<th>'.$tab_typo[$jj].'</th>';
                                    }

                                    $content_pli_cloture .= ' </tr>
                                </thead>
                                <tbody   class="custom_font">';
                                    $arrInfo_1 = array();
                                    $arrInfo_2 = array();
                                    $arrInfo_3 = array();
                                    $arrInfo_4 = array();
                                    $arrInfo_5 = array();
                                    $arrInfo_6 = array();
                                    $arrInfo_7 = array();
                                    $arrInfo_8 = array();
                                    $arrInfo_9 = array();
                                    $arrInfo_10 = array();
                                    $arrInfo_11 = array();
                                    $arrInfo_12 = array();
                                    $arrInfo_13 = array();
                                    $arrInfo_14 = array();
                                    $arrInfo_15 = array();
                                    $arrInfo_16 = array();
                                    $arrInfo_17 = array();
                                    $arrInfo_18 = array();
                                    $arrInfo_19 = array();
                                    $arrInfo_20 = array();
                                    $arrInfo_21 = array();
                                    $mois_verif = ""; // pour vérifier si le mois a changé dans la liste des données
                                    $k=-1;
								
                                    foreach($liste_pli_typo as $k_plis_cltr =>$tab_plis_cltr){
                                        if($tab_plis_cltr["info_1"] +  $tab_plis_cltr["info_2"] +  $tab_plis_cltr["info_3"] +  $tab_plis_cltr["info_4"] +  $tab_plis_cltr["info_5"] +  $tab_plis_cltr["info_6"] +  $tab_plis_cltr["info_7"] +  $tab_plis_cltr["info_8"] +  $tab_plis_cltr["info_9"] +  $tab_plis_cltr["info_10"] +  $tab_plis_cltr["info_11"] +  $tab_plis_cltr["info_12"] +  $tab_plis_cltr["info_13"] +  $tab_plis_cltr["info_14"] +  $tab_plis_cltr["info_15"] +  $tab_plis_cltr["info_16"] +  $tab_plis_cltr["info_17"] +  $tab_plis_cltr["info_18"] +  $tab_plis_cltr["info_19"] +  $tab_plis_cltr["info_20"] +  $tab_plis_cltr["info_21"] >0) {
                                            $mois = date('m',strtotime($tab_plis_cltr["date_reception"]));
                                            if($mois != $mois_verif){  // si le mois courant est différent du mois précédent $k s'incrémente
                                                $k++;
                                                $mois_verif = $mois;
												$arrInfo_1[$k]    = 0;
												$arrInfo_2[$k]    = 0;
												$arrInfo_3[$k]    = 0;
												$arrInfo_4[$k]    = 0;
												$arrInfo_5[$k]    = 0;
												$arrInfo_6[$k]    = 0;
												$arrInfo_7[$k]    = 0;
												$arrInfo_8[$k]    = 0;
												$arrInfo_9[$k]    = 0;
												$arrInfo_10[$k]    = 0;
												$arrInfo_11[$k]    = 0;
												$arrInfo_12[$k]    = 0;
												$arrInfo_13[$k]    = 0;
												$arrInfo_14[$k]    = 0;
												$arrInfo_15[$k]    = 0;
												$arrInfo_16[$k]    = 0;
												$arrInfo_17[$k]    = 0;
												$arrInfo_18[$k]    = 0;
												$arrInfo_19[$k]    = 0;
												$arrInfo_20[$k]    = 0;
												$arrInfo_21[$k]    = 0;
                                            }
											
											/*echo "<pre>";
											echo $tab_plis_cltr["info_1"]."</br>";
											echo "</pre>";*/
                                            $arrInfo_1[$k]   += (empty($arrInfo_1[$k])) ?  0: $tab_plis_cltr["info_1"];
                                            $arrInfo_2[$k]   += $tab_plis_cltr["info_2"];
                                            $arrInfo_3[$k]   += $tab_plis_cltr["info_3"];
                                            $arrInfo_4[$k]   += $tab_plis_cltr["info_4"];
                                            $arrInfo_5[$k]   += $tab_plis_cltr["info_5"];
                                            $arrInfo_6[$k]   += $tab_plis_cltr["info_6"];
                                            $arrInfo_7[$k]   += $tab_plis_cltr["info_7"];
                                            $arrInfo_8[$k]   += $tab_plis_cltr["info_8"];
                                            $arrInfo_9[$k]   += $tab_plis_cltr["info_9"];
                                            $arrInfo_10[$k]   += $tab_plis_cltr["info_10"];
                                            $arrInfo_11[$k]   += $tab_plis_cltr["info_11"];
                                            $arrInfo_12[$k]   += $tab_plis_cltr["info_12"];
                                            $arrInfo_13[$k]   += $tab_plis_cltr["info_13"];
                                            $arrInfo_14[$k]   += $tab_plis_cltr["info_14"];
                                            $arrInfo_15[$k]   += $tab_plis_cltr["info_15"];
                                            $arrInfo_16[$k]   += $tab_plis_cltr["info_16"];
                                            $arrInfo_17[$k]   += $tab_plis_cltr["info_17"];
                                            $arrInfo_18[$k]   += $tab_plis_cltr["info_18"];
                                            $arrInfo_19[$k]   += $tab_plis_cltr["info_19"];
                                            $arrInfo_20[$k]   += $tab_plis_cltr["info_20"];
                                            $arrInfo_21[$k]   += $tab_plis_cltr["info_21"];

                                            //$k++;
                                            $content_pli_cloture .= '<tr>
                                        <td style="text-align:center;background: white !important;">' . $tab_plis_cltr["date_reception"] . '</td>
                                        <td style="text-align:center">' .$tab_plis_cltr["info_1"]. '</td>
                                        <td style="text-align:center">' .$tab_plis_cltr["info_2"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_3"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_4"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_5"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_6"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_7"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_8"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_9"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_10"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_11"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_12"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_13"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_14"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_15"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_16"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_17"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_18"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_19"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_20"]. '</td>
										<td style="text-align:center">' .$tab_plis_cltr["info_21"]. '</td>


										</tr>';
                                        }
                                    }
                                    $content_pli_cloture .='</tbody>
                            </table>';
                                    echo $content_pli_cloture;

                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Task Info -->


                    <!-- fin Plis avec moyen de paiement-->
                    <!-- debut BDC et autres plis cloturés-->
                    <!--div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <div class="row clearfix">
                            <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12"  style="margin-top:50px;">
                                <div class="card">
                                    <div class="header">
                                        <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">Flux cloturés en mode graphe</span>
                                    </div>
                                    <div class="body">
                                        <canvas id="chart-plis-cloture" width="auto" height="auto"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div-->
                    <!-- fin BDC et autres plis cloturés-->

                </div>
            </div>
        </div>

        <!-- Browser Usage -->

        <input type="hidden" id="typo" name="typo" value="<?php echo $lib_typo?>">
        <script>

            //recep global
            var cloture = <?php echo json_encode($data_set_cloture);?>;
            var encours = <?php echo json_encode($data_set_encours);?>;
            var nontraite = <?php echo json_encode($data_set_nontraite);?>;
            var anomalie = <?php echo json_encode($data_set_anomalie_glb);?>;
            var labels = <?php echo json_encode($data_set_labels);?>;
            //ko data
            var traite = <?php echo json_encode($data_set_traite);?>;
            var hp = <?php echo json_encode($data_set_hp);?>;
            var rejete = <?php echo json_encode($data_set_rejete);?>;

            //plis cloturés
            //var arrInfo_2 = <?php echo json_encode($arrInfo_2);?>;
            // var arrInfo_3 = <?php echo json_encode($arrInfo_3);?>;
            var arrInfo_1=<?php echo json_encode($arrInfo_1);?>;
            var arrInfo_2=<?php echo json_encode($arrInfo_2);?>;
            var arrInfo_3=<?php echo json_encode($arrInfo_3);?>;
            var arrInfo_4=<?php echo json_encode($arrInfo_4);?>;
            var arrInfo_5=<?php echo json_encode($arrInfo_5);?>;
            var arrInfo_6=<?php echo json_encode($arrInfo_6);?>;
            var arrInfo_7=<?php echo json_encode($arrInfo_7);?>;
            var arrInfo_8=<?php echo json_encode($arrInfo_8);?>;
            var arrInfo_9=<?php echo json_encode($arrInfo_9);?>;
            var arrInfo_10=<?php echo json_encode($arrInfo_10);?>;
            var arrInfo_11=<?php echo json_encode($arrInfo_11);?>;
            var arrInfo_12=<?php echo json_encode($arrInfo_12);?>;
            var arrInfo_13=<?php echo json_encode($arrInfo_13);?>;
            var arrInfo_14=<?php echo json_encode($arrInfo_14);?>;
            var arrInfo_15=<?php echo json_encode($arrInfo_15);?>;
            var arrInfo_16=<?php echo json_encode($arrInfo_16);?>;
            var arrInfo_17=<?php echo json_encode($arrInfo_17);?>;
            var arrInfo_18=<?php echo json_encode($arrInfo_18);?>;
            var arrInfo_19=<?php echo json_encode($arrInfo_19);?>;
            var arrInfo_20=<?php echo json_encode($arrInfo_20);?>;
            var arrInfo_21=<?php echo json_encode($arrInfo_21);?>;

        </script>
        <!--script src="<?php echo base_url().'src/js/statistics/graphe-flux.js'; ?>"></script-->
