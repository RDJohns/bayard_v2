
<!--div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab21" display="display:none"-->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;margin-right:70%;">KO Call par motif</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="detail_ko_call()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php  
							$liste_plis_ko_call = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:25%;text-align:center;">Motif</th>
										<th  style="width:15%;text-align:center;">Nbr.</th>										     
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                 
									
									foreach($liste_plis_ko_call_motif as $k_plis_ko_call=>$tab_plis_ko_call){
										
									$liste_plis_ko_call .= '<tr>
                                        <td style="text-align:center;">'.$tab_plis_ko_call["libelle_motif"].'</td>
										<td style="text-align:center;">'.$tab_plis_ko_call["nb"].'</td>
										</tr>';
										
									}
								 $liste_plis_ko_call .='</tbody>
                            </table>';
							echo $liste_plis_ko_call;
									
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	<!--/div-->
                           