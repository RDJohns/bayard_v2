
<!--div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab21" display="display:none"-->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	   <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;margin-right:70%;">BDC par produits commandés</span>
                              <ul class="header-dropdown m-r--5">
								<li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                       <a onclick="quitter_tab22()" style="cursor: pointer;"> 
									   <i class="fa fa-close"></i></a>
									</a>                                   
                                </li>
							</ul>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                            <?php  
							$tb_bdc_volume = '<table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead   class="custom_font">
                                    <tr>
                                        <th  style="width:25%;text-align:center;">Date de traitement</th>
                                        <th  style="width:25%;text-align:center;">Lot scan</th>
										<th  style="width:15%;text-align:center;">Nbr. de produits commandés</th>										     
                                    </tr>
                                </thead>
                                <tbody   class="custom_font">';									
                                 
									
									foreach($liste_bdc_volume as $k_bdc=>$tab_bdc){
										
									$tb_bdc_volume .= '<tr>
                                        <td style="text-align:center;">'.$tab_bdc["date_ttt"].'</td>
                                        <td style="text-align:center;">'.$tab_bdc["lot_scan"].'</td>
										<td style="text-align:center;">'.$tab_bdc["nb"].'</td>
										</tr>';
										
									}
								 $tb_bdc_volume .='</tbody>
                            </table>';
							echo $tb_bdc_volume;
									
							?>
							
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
              
            </div>
	</div>
	<!--/div-->
                           