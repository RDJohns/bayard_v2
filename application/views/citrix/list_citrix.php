 <br><br><br><br><br>
  <div class="icon-and-text-button-demo parti_bouton">
    <button type="button" class="btn bg-blue btn-lg waves-effect" data-toggle="modal" onclick="add_virements_citrix();">
        <span>Ajout virements sur citrix</span>
    </button>
    <!--<button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>-->
</div>
 <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 c-citrix1" style="margin-top: 5px;">
        <div class="card">
            <div class="header">
                    <h5><span><h5><span>Liste des virements citrix sur la GED</span></h5></span></h5>
                    <div id="citrix-table-liste"></div>
            </div>
            <div class="body body-citrix-liste">
                <div id="citrix-liste">
                <table class="table table-bordered table-striped table-hover dataTable table-liste" id="table-liste-citrix">
                    <thead>
                        <tr>
                            <tr>
                            <th><input type="checkbox" id="check-all"></th>                            
                            <th>Date enregistrement</th>
                            <th>Soci&eacute;t&eacute;</th>
                            <th>Num&eacute;ro abonn&eacute;</th>
                            <th>Nom abonn&eacute;</th>
                            <th>Montant</th>
                            <th>Commande</th>
                            <th>Statut</th>
                            <th>Code postal payeur</th>       
                            <th>Num&eacute;ro payeur</th>
                            <th>Nom payeur</th>
                            <th>Mode de paiement</th>
                            <th>Typologie</th>
                            <th>Action</th>
                        </tr>
                           
                           
                        </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
</div>

<script type="text/javascript">
var save_method; 
var table;

$(document).ready(function() {

    table = $('#table-liste-citrix').DataTable({ 

        "processing": true, 
        "serverSide": true, 
        "order": [], 

        "ajax": {
            "url": "<?php echo site_url('citrix/saisie_citrix/listeVirementsCitrix')?>",
            "type": "POST"
        },
        "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            { 
                "targets": [ -1 ], 
                "orderable": false, 
            },

        ],

    });

    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        //orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });



});

//Add virements citrix

function add_virements_citrix()
{
    save_method = 'add';
    $('#form')[0].reset(); 
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty(); 
    $('[name="societe"]').val("").change();
    $('[name="statut"]').val("").change();
    $('[name="mode_paiement"]').val("").change();
    $('[name="typologie"]').val("").change(); 
    $('#modal_form').modal('show'); 
    $('.modal-title').text('Nouveau virements sur citrix '); 

}

//Modif
function edit_virements_citrix(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty(); 


    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('citrix/saisie_citrix/ajax_edit')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="date_enregistrement"]').datepicker('update',data.date_enregistrement);
            $('[name="societe"]').val(data.societe).change();
            $('[name="numero_abonne"]').val(data.numero_abonne);
            $('[name="nom_abonne"]').val(data.nom_abonne);
            $('[name="montant"]').val(data.montant);
            $('[name="commande"]').val(data.commande);
            $('[name="statut"]').val(data.statut).change();
            /*selectItemByValue(document.getElementById('statut'),data.statut); 
            alert(data.statut);*/
            $('[name="code_postal_payeur"]').val(data.code_postal_payeur);
            $('[name="numero_payeur"]').val(data.numero_payeur);
            $('[name="nom_payeur"]').val(data.nom_payeur);
            $('[name="mode_paiement"]').val(data.mode_paiement).change();
            $('[name="typologie"]').val(data.typologie).change(); 
           /*selectItemByValue(document.getElementById('typologie'),data.typologie); 
            alert(data.typologie);*/
            $('#modal_form').modal('show'); 
            $('.modal-title').text('Modification virements sur citrix'); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error loading data');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); 
}

function save()
{
    $('#btnSave').text('save...'); 
    $('#btnSave').attr('disabled',true); 

    if(save_method == 'add') {
        url = "<?php echo site_url('citrix/saisie_citrix/ajax_add')?>";
    } else {
        url = "<?php echo site_url('citrix/saisie_citrix/ajax_update')?>";
    }

    // ajax adding data to database
    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        beforeSend:function(){
         return confirm("Voulez-vous enregistrer ce virements");
        },
        success: function(data)
        {

            if(data.status) 
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                }
            }
            $('#btnSave').text('save'); 
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); 
            $('#btnSave').attr('disabled',false); 

        }
    });
}

function selectItemByValue(elmnt, value){
    //document.getElementById(elmnt);
    for(var i=0; i < elmnt.options.length; i++)
    {
      if(elmnt.options[i].value === value) {
        elmnt.selectedIndex = i;
        break;
      }
    }
  }
</script>
<!--Modal forms add new virements citrix-->

<div class="modal fade" id="modal_form" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Nouveau virements sur citrix </h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form" novalidate="novalidate">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-group">
                        <label>Date de saisie</label>
                            <div class="form-group">
                                <input name="date_enregistrement" placeholder="<?php echo date('d/m/Y');?>"class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    <div class="form-group form-float">
                        <select class="form-control show-tick" tabindex="-98" name="societe" id="societe" required aria-required="true" aria-invalid="true">
                            <option value="">Choisir une socièté</option>
                            <option value="Bayard">Bayard</option>
                            <option value="Milan">Milan</option>
                        </select>
                    </div>
                     <div class="form-group form-float">
                        <div class="form-line">
                            <input type="number" name="numero_abonne" placeholder="Numéro abonné"id="numero_abonne" class="form-control" required aria-required="true" aria-invalid="true">
                            
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" name="nom_abonne" placeholder="Nom abonné" id="nom_abonne" class="form-control" required aria-required="true" aria-invalid="true">
                            
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="number" name="montant" placeholder="Montant" id="montant" class="form-control" required aria-required="true" aria-invalid="true">
                            
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" name="commande" placeholder="Commande" id="commande" class="form-control" required aria-required="true" aria-invalid="true">
                            
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <select class="form-control show-tick" tabindex="-98" name="statut" id="statut">
                            <option value="">Choisir statut</option>
                            <option value="Full">Full</option>
                            <option value="Traité">Trait&eacute;</option>
                        </select>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" name="code_postal_payeur" placeholder="Code postal" id="code_postal_payeur" class="form-control">
                            
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="number" name="numero_payeur" placeholder="Numéro payeur" id="numero_payeur" class="form-control">
                            
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" name="nom_payeur" placeholder="Nom payeur" id="nom_payeur" class="form-control">
                            
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <select class="form-control show-tick" tabindex="-98" name="mode_paiement" id="mode_paiement" required aria-required="true" aria-invalid="true">
                            <option value="">Choisir mode de paiement</option>
                            <option value="Chèque">Chèque</option>
                            <option value="Carte bancaire">Carte bancaire</option>
                        </select>
                    </div>
                    <div class="form-group form-float">
                        <select class="form-control show-tick" tabindex="-98" name="typologie" id="typologie" required aria-required="true" aria-invalid="true">
                            <option value="">Choisir typologie</option>
                            <option value="Virement bancaire">Virement bancaire</option>
                            <option value="Mobilité bancaire">Mobilit&eacute; bancaire</option>
                            <option value="Autres">Autres</option>
                        </select>
                    </div>  
                  
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnSave" onclick="save();">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>




