<br><br><br><br>
  <div class="container-fluid">
            <div class="col-lg-12">
        <div class="card card_filtre">
            <div class="body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-6">
                               
                                <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                     <span class="input-group-addon" style="width:26%;"><b style="float:left;">Date d'enregistrement </b></span>
                                    <div class="form-line">
                                        <input type="text" class="form-control provenance-date" id="date-debut" value="<?php echo date( 'd/m/Y', strtotime( 'monday this week' ) );?>">
                                    </div>
                                    <span class="input-group-addon">au&nbsp;&nbsp;</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control provenance-date" id="date-fin" value="<?php echo date( 'd/m/Y', strtotime( 'friday this week' ) );?>">
                                    </div>
                                
                                </div >
                            </div>
                            <div class="col-xs-6 ">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Soci&eacute;t&eacute; :</b></div>
                            <div class="col-xs-9">
                                <select class="form-control show-tick" tabindex="-98" id="filtre-societe">
                                    <option value="">-- Choisir une socièté --</option>
                                    <option value="Bayard">Bayard</option>
                                    <option value="Milan">Milan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Statut:</b></div>
                            <div class="col-xs-9">
                                <select class="form-control show-tick" tabindex="-98" id="filtre-statut">
                                    <option value="">-- Choisir statut --</option>
                                    <option value="Full">Full</option>
                                    <option value="Traité">Trait&eacute;</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b> Mode de paiement :</b></div>
                            <div class="col-xs-9 col_hx_">
                                 <select class="form-control show-tick" tabindex="-98" id="filtre-mpaiement">
                                    <option value="" >-- Choisir mode de paiement --</option>
                                    <option value="Chèque">Chèque</option>
                                    <option value="Carte bancaire">Carte bancaire</option>
                                 </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Typologie :</b></div>
                            <div class="col-xs-9 col_hx_">
                                  <select class="form-control show-tick" tabindex="-98" id="filtre-typologie">
                                    <option value="">-- Choisir typologie --</option>
                                    <option value="Virement bancaire">Virement bancaire</option>
                                    <option value="Mobilité bancaire">Mobilit&eacute; bancaire</option>
                                    <option value="Autres">Autres</option>
                                  </select>
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                       <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="hidden" class="form-control" id="txt_recherche_num_abonne" onkeyup="" />
                                <label class="form-label">Num&eacute;ro abonn&eacute;</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="hidden" class="form-control" id="txt_recherche_nom_abonne" onkeyup="" />
                                <label class="form-label">Nom abonn&eacute;</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="hidden" class="form-control" id="txt_recherche_montant" onkeyup="" />
                                <label class="form-label">Montant</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="hidden" class="form-control" id="txt_recherche_commande" onkeyup="" />
                                <label class="form-label">Commande</label>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_hx">
                        <button type="button" class="btn bg-blue btn-lg waves-effect" onclick="load_citrix();">
                            <i class="material-icons">search</i>
                            <span>Rechercher</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div>



 <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 c-citrix" style="margin-top: 5px;">
        <div class="card">
            <div class="header">
                    <h5><span><h5><span>Suivi des virements citrix sur la GED</span></h5></span></h5>
                    <div id="citrix-table-liste"></div>
            </div>
            <div class="body body-citrix-liste">
                <div id="citrix-liste">
                <table class="table table-bordered table-striped table-hover dataTable table-liste" id="table-liste-citrix">
                    <thead>
                        <tr>
                            <tr>
                            <th>Date enregistrement</th>
                            <th>Soci&eacute;t&eacute;</th>
                            <th>Num&eacute;ro abonn&eacute;</th>
                            <th>Nom abonn&eacute;</th>
                            <th>Montant</th>
                            <th>Commande</th>
                            <th>Statut</th>
                            <th>Code postal payeur</th>       
                            <th>Num&eacute;ro payeur</th>
                            <th>Nom payeur</th>
                            <th>Mode de paiement</th>
                            <th>Typologie</th>
                        </tr>
                         
                             
                        </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
</div>

