<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <dl class="dl-horizontal">
            <dt>Pli: </dt>
            <dd><?php echo $pli->pli ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Lot num&eacute;risation: </dt>
            <dd><?php echo $pli->lot_scan ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Commande: </dt>
            <dd><?php echo $pli->commande ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Date courrier: </dt>
            <dd><?php echo (new DateTime($pli->date_courrier))->format('d/m/Y') ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Date cr&eacute;ation: </dt>
            <dd><?php echo (new DateTime($pli->date_creation))->format('d/m/Y G:i:s') ?></dd>
        </dl>
        <?php if (trim($pli->id_lot_saisie) != ''): ?>
        <dl class="dl-horizontal">
            <dt>Date saisie: </dt>
            <dd><?php echo (new DateTime($pli->dt_enregistrement))->format('d/m/Y G:i:s') ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>ID#Lot saisie: </dt>
            <dd><?php echo $pli->id_lot_saisie ?></dd>
        </dl>
        <?php endif; ?>
        <dl class="dl-horizontal">
            <dt>&Eacute;tape: </dt>
            <dd><?php echo $pli->traitement ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Statut: </dt>
            <dd><?php echo $pli->libelle ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>D&eacute;l&eacute;gu&eacute;: </dt>
            <dd><?php echo $pli->nom_deleg ?></dd>
        </dl>
        <?php if (!empty($pli->circulaire)): ?>
            <dl class="dl-horizontal">
                <dt>Circulaire: </dt>
                <dd><?php echo $pli->circulaire ?></dd>
            </dl>
        <?php endif; ?>
        <dl class="dl-horizontal">
            <dt>Soci&eacute;t&eacute;: </dt>
            <dd><?php echo $pli->nom_societe ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Type: </dt>
            <dd><?php echo $pli->typo ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Titre: </dt>
            <dd><?php echo $pli->titre_ ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Code promotion: </dt>
            <dd><?php echo $pli->code_promotion ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Paiement: </dt>
            <dd><?php echo $paies ?></dd>
        </dl>
        <?php if ($with_f_ci): ?>
        <dl>
            <dt>Fichier CI: </dt>
            <dd>
                <?php echo $f_ci ?>
                &nbsp;
                <a href="<?php echo (site_url('control/ajax/download_circulaire/'.$pli->idpli)) ?>" download="" type="button" class="btn btn-circle waves-effect waves-circle waves-float" title="t&eacute;l&eacute;charger">
                    <i class="material-icons">file_download</i>
                </a>
            </dd>
        </dl>
        <?php endif; ?>
        <?php if ($pli->nb_pj > 0): ?>
        <dl>
            <dt>Image &agrave; d&eacute;sarchiver <small>( <?php echo $pli->nb_pj ?> x 2 )</small> : </dt>
            <dd>
                pjs_pli<?php echo $pli->id_pli ?>.zip
                &nbsp;
                <a href="<?php echo (site_url('visual/visual/download_pjs/'.$pli->idpli)) ?>" download="" type="button" class="btn btn-circle waves-effect waves-circle waves-float" title="t&eacute;l&eacute;charger">
                    <i class="material-icons">file_download</i>
                </a>
            </dd>
        </dl>
        <?php endif; ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <?php if (!is_null($motif_consigne)) { ?>
            <dl class="dl-horizontal">
                <dt>Motif KO: </dt>
                <dd><?php echo $motif_consigne->motif_operateur ?></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Consigne: </dt>
                <dd><?php echo $motif_consigne->consigne_client ?></dd>
            </dl>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <dl>
                    <dt>Documents: </dt>
                    <dd>
                        <div id="list_doc_pli" class="list-group">
                            <?php foreach ($docs as $n_doc => $doc): ?>
                            <button type="button" class="list-group-item align-center" onclick="load_img_doc('<?php echo $doc->id_document ?>');" data-toggle="tooltip" data-placement="right" title="Cliquez pour afficher"><i class="material-icons">search</i> Document <?php echo ($n_doc+1).' - #'.$doc->id_document ?></button>
                            <?php endforeach; ?>
                        </div>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <dl>
                    <dt>Ch&egrave;ques: </dt>
                    <dd>
                        <?php if (count($chqs) > 0): ?>
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach ($chqs as $key => $chq): ?>
                            <li role="presentation" class="<?php echo ($key == 0 ? 'active' : '') ?>" >
                                <a href="#body_chq_<?php echo $key ?>" data-toggle="tab">
                                    <i class="material-icons">local_atm</i>#<?php echo ($key+1) ?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content tab_cont_0">
                            <?php foreach ($chqs as $key => $chq): ?>
                            <div role="tabpanel" class="tab-pane fade <?php echo ($key == 0 ? 'in active' : '') ?>" id="body_chq_<?php echo $key ?>">
                                <dl class="dl-horizontal">
                                    <dt>Etranger: </dt>
                                    <dd><?php echo ($chq->etranger == '1' ? 'OUI' : 'NON') ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>CMC7: </dt>
                                    <dd><?php echo $chq->cmc7 ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Montant: </dt>
                                    <dd><?php echo $chq->montant ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>RLMC: </dt>
                                    <dd><?php echo $chq->rlmc ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Client: </dt>
                                    <dd><?php echo $chq->nom_client ?></dd>
                                </dl>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 p-t-20">
                <dl>
                    <dt>Ch&egrave;ques cadeaux: </dt>
                    <dd>
                        <?php if (count($chqs_kd) > 0): ?>
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach ($chqs_kd as $key_kd => $chq_kd): ?>
                            <li role="presentation" class="<?php echo ($key_kd == 0 ? 'active' : '') ?>" >
                                <a href="#body_chq_kd_<?php echo $key_kd ?>" data-toggle="tab">
                                    <i class="material-icons">card_giftcard</i>#<?php echo ($key_kd+1) ?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content tab_cont_0">
                            <?php foreach ($chqs_kd as $key_kd => $chq_kd): ?>
                            <div role="tabpanel" class="tab-pane fade <?php echo ($key_kd == 0 ? 'in active' : '') ?>" id="body_chq_kd_<?php echo $key_kd ?>">
                                <dl class="dl-horizontal">
                                    <dt>Montant: </dt>
                                    <dd><?php echo $chq_kd->montant ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Type: </dt>
                                    <dd><?php echo $chq_kd->type_cheque_cadeau ?></dd>
                                </dl>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
</div>
