<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid p-r-0 p-l-0">
    <?php foreach ($comments as $key_comment => $comment): ?>
        <div class="media">
            <div class="media-body">
                <h4 class="media-heading"><?php echo $comment->login ?> <small class="font-10">[<?php echo ($comment->dt_comment == '' ? '' : (new DateTime($comment->dt_comment))->format('d/m/Y G:i:s')) ?>]</small></h4>
                <p class="p-l-50">
                <?php echo nl2br(htmlspecialchars($comment->commentaire)) ?>
                </p>
            </div>
        </div>
    <?php endforeach; ?>
</div>