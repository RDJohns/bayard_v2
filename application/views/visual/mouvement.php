<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <dl>
        <dt>Mouvements: </dt>
        <dd>
        <?php if (count($mvmts) > 0): ?>
            <ul class="nav nav-tabs" role="tablist">
                <?php foreach ($mvmts as $key => $mvmt): ?>
                <li role="presentation" class="<?php echo ($mvmt->id_ == $activ_id ? 'active' : '') ?>" >
                    <a href="#body_mvmt_<?php echo $key ?>" data-toggle="tab">
                        <i class="material-icons">picture_in_picture_alt</i>#<?php echo ($key+1) ?>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
            <div class="tab-content">
                <?php foreach ($mvmts as $key => $mvmt): ?>
                <div role="tabpanel" class="tab-pane fade <?php echo ($mvmt->id_ == $activ_id ? 'in active' : '') ?>" id="body_mvmt_<?php echo $key ?>">
                    <p><b>ID : </b><?php echo $mvmt->id_ ?></p>
                    <p><b>Titre : </b><?php echo $mvmt->titre_ ?></p>
                    <p><b>Code promotion : </b><?php echo $mvmt->code_promo ?></p>
                    <!-- <dl class="dl-horizontal">
                        <dt>ID: </dt>
                        <dd><?php echo $mvmt->id_ ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Titre: </dt>
                        <dd><?php echo $mvmt->titre_ ?></dd>
                    </dl> -->
                    <div class="row">
                        <div class="col-xs-6">
                            <h4>Abonn&eacute; :</h4>
                            <dl class="dl-horizontal">
                                <dt>Num&eacute;ro: </dt>
                                <dd><?php echo $mvmt->numero_abonne ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Nom: </dt>
                                <dd><?php echo $mvmt->nom_abonne ?></dd>
                            </dl>
                            <!-- <dl class="dl-horizontal">
                                <dt>Code postal: </dt>
                                <dd><?php //echo $mvmt->code_postal_abonne ?></dd>
                            </dl> -->
                        </div>
                        <div class="col-xs-6">
                            <h4>Payeur :</h4>
                            <dl class="dl-horizontal">
                                <dt>Num&eacute;ro: </dt>
                                <dd><?php echo $mvmt->numero_payeur ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Nom: </dt>
                                <dd><?php echo $mvmt->nom_payeur ?></dd>
                            </dl>
                            <!-- <dl class="dl-horizontal">
                                <dt>Code postal: </dt>
                                <dd><?php //echo $mvmt->code_postal_payeur ?></dd>
                            </dl> -->
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        </dd>
    </dl>
</div>
