<?php defined('BASEPATH') OR exit('No direct script access allowed');
    $nb_chq = count($chqs);
    $view_chq = $nb_chq < 15;
    $nb_chq_kd = count($chqs_kd);
    $view_chq_kd = $nb_chq_kd < 15;
    $nb_mvmnt = count($mvmts);
    $tb_prio = array(
        '-1'=>'<sup>En stand-by</sup> <sub><i class="material-icons col-orange">pause_circle_outline</i></sub>'
        , '0'=>'<sup>Normale</sup> <sub><i class="material-icons col-green">play_circle_outline</i></sub>'
        , '1'=>'<sup>Prioris&eacute;</sup> <sub><i class="material-icons col-red">fast_forward</i></sub>');
?>

<div class="container-fluid">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="tab_pli<?php echo $ui_id ?>">
            <div class="table-responsive p-t-25">
                <table class="table table-hover table-striped">
                    <tbody>
                        <tr><th>Identifiant</th><td><?php echo $id_pli ?></td></tr>
                        <tr><th>Lot num&eacute;risation</th><td><?php echo $pli->lot_scan ?></td></tr>
                        <tr><th>pli</th><td><?php echo $pli->pli ?></td></tr>
                        <tr><th>Commande</th><td><?php echo $pli->commande ?></td></tr>
                        <tr><th>Date courrier</th><td><?php echo (new DateTime($pli->date_courrier))->format('d/m/Y') ?></td></tr>
                        <tr><th>Soci&eacute;t&eacute;</th><td><?php echo $pli->nom_societe ?></td></tr>
                        <tr><th>Etape</th><td><?php echo $pli->flgtt_etape ?></td></tr>
                        <tr><th>Etat</th><td><?php echo $pli->flgtt_etat ?></td></tr>
                        <tr><th>Statut</th><td><?php echo ($pli->flag_rewrite_status_client == 0 || $pli2->statut_saisie != 1 ? $pli->libelle : '') ?></td></tr>
                        <tr><th>Type</th><td><?php echo $pli->typo ?></td></tr>
                        <tr><th>Paiements</th><td><?php echo $paies ?></td></tr>
                        <tr><th>Priorit&eacute;</th><td><?php echo (isset($tb_prio[$pli->recommande]) ? $tb_prio[$pli->recommande] : '?') ?></td></tr>
                        <tr><th>Lot-saisie</th><td><?php echo $pli->id_lot_saisie ?></td></tr>
                        <tr><th>Date traitement</th><td><?php echo ($pli2->date_saisie != '' ? (new DateTime($pli2->date_saisie))->format('d/m/Y G:i:s') : '') ?></td></tr>
                        <tr><th>Saisie niveau</th><td><?php echo $pli2->niveau_saisie ?></td></tr>
                        <tr><th>Titre</th><td><?php echo $pli->titre_ ?></td></tr>
                        <tr><th>D&eacute;l&eacute;gu&eacute;</th><td><?php echo $pli->nom_deleg ?></td></tr>
                        <tr><th>Code école / GCI</th><td><?php echo $pli->code_ecole_gci ?></td></tr>
                        <tr><th>Nom Circulaire</th><td><?php echo $pli->circulaire ?></td></tr>
                        <tr><th>Fichier circulaire</th><td><?php echo '<a href="'.site_url('control/ajax/download_circulaire/'.$pli->idpli).'" download="" >'.$pli2->nom_orig_fichier_circulaire.'</a>' ?></td></tr>
                        <tr><th>Image &agrave; d&eacute;sarchiver<span class="badge bg-cyan font-9"><?php echo $pli->nb_pj ?></span></th><td><?php echo ($pli->nb_pj > 0 ? '<a href="'.site_url('visual/visual/download_pjs/'.$pli->idpli).'" download="" >('.$pli->nb_pj.'x2) images</a>' : '') ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade p-l-0 p-r-0" id="tab_docs<?php echo $ui_id ?>">
            <?php echo $docs ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_chqs<?php echo $ui_id ?>">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel-group" id="accordion_chq_<?php echo $ui_id ?>" role="tablist" aria-multiselectable="true">
                            <?php foreach ($chqs as $key => $chq): ?>
                                <div class="panel panel-col-cyan">
                                    <div class="panel-heading ttl_accord_modal" role="tab" id="heifing_chq_<?php echo $key ?><?php echo $ui_id ?>">
                                        <h4 class="panel-title">
                                            <a class="<?php echo ($key == 0 && ($nb_chq < 2 || !$view_chq) ? '' : 'collapsed') ?>" role="button" data-toggle="collapse" data-parent="#accordion_chq_<?php echo $ui_id ?>" href="#body_chq_<?php echo $key ?><?php echo $ui_id ?>" aria-expanded="<?php echo ($key == 0 && ($nb_chq < 2 || !$view_chq) ? 'true' : 'false') ?>" aria-controls="body_chq_<?php echo $key ?><?php echo $ui_id ?>">
                                                Ch&egrave;que#<?php echo ($key+1).'<small class="col-white">: cmc7#'.$chq->cmc7.' - doc#'.$chq->id_doc ?></small>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="body_chq_<?php echo $key ?><?php echo $ui_id ?>" class="panel-collapse collapse <?php echo ($key == 0 && ($nb_chq < 2 || !$view_chq) ? 'in' : '') ?>" role="tabpanel" aria-labelledby="heifing_chq_<?php echo $key ?><?php echo $ui_id ?>">
                                        <div class="panel-body p-t-0">
                                            <div class="row">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-striped">
                                                        <tbody>
                                                            <?php
                                                                $tab_anom = $chq->anomalie == '' ? array('') : explode('|', $chq->anomalie);
                                                                $nb_row_anom = count($tab_anom) > 0 ? count($tab_anom) : 1;
                                                            ?>
                                                            <tr><th>Etranger</th><td><?php echo ($chq->etranger == '1' ? 'OUI' : 'NON') ?></td></tr>
                                                            <tr><th>CMC7</th><td><?php echo $chq->cmc7 ?></td></tr>
                                                            <tr><th>Montant</th><td><?php echo $chq->montant ?></td></tr>
                                                            <tr><th>RLMC</th><td><?php echo $chq->rlmc ?></td></tr>
                                                            <tr><th>Client</th><td><?php echo $chq->nom_client ?></td></tr>
                                                            <tr><th>Etat</th><td><?php echo $chq->lbl_etat_chq ?></td></tr>
                                                            <tr>
                                                                <th rowspan="<?php echo $nb_row_anom ?>">Anomalies</th>
                                                                <?php foreach ($tab_anom as $key_anom => $anom): ?>
                                                                    <?php if($key_anom == 0): ?>
                                                                        <td><?php echo $anom ?></td>
                                                                    <?php else: ?>
                                                                        <tr><td><?php echo $anom ?></td></tr>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php if(is_numeric($chq->id_doc)) { ?>
                                                <div class="row">
                                                    <?php if($view_chq) { ?>
                                                        <div class="col-lg-6 m-b--20">
                                                            <img id="img_temp_recto_chq_<?php echo $chq->id.'_'.$key ?>" style="width: 100%;" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Recto" src="data:image/jpeg;base64,<?php echo (trim($chq->n_ima_base64_recto) == '' ? $empty_b64 : $chq->n_ima_base64_recto ); ?>" onclick="load_img_doc_face('<?php echo $chq->id_doc ?>');" />
                                                        </div>
                                                        <div class="col-lg-6 m-b--20">
                                                            <img id="img_temp_verso_chq_<?php echo $chq->id.'_'.$key ?>" style="width: 100%;" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Verso" src="data:image/jpeg;base64,<?php echo (trim($chq->n_ima_base64_verso) == '' ? $empty_b64 : $chq->n_ima_base64_verso ); ?>" onclick="load_img_doc_face('<?php echo $chq->id_doc ?>',1);" />
                                                        </div>
                                                    <?php } else { ?>
                                                        <button type="button" class="list-group-item align-center" onclick="load_img_doc_face('<?php echo $chq->id_doc ?>');" data-toggle="tooltip" data-placement="right" title="Cliquez pour afficher"><i class="material-icons">search</i> Cliquer pour afficher l'image correspondant au Ch&egrave;que <?php echo ($key+1).' - #'.$chq->id_doc ?></button>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_chq_kdx<?php echo $ui_id ?>">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel-group" id="accordion_chq_kd_<?php echo $ui_id ?>" role="tablist" aria-multiselectable="true">
                            <?php foreach ($chqs_kd as $key => $chq_kd): ?>
                                <div class="panel panel-col-cyan">
                                    <div class="panel-heading ttl_accord_modal" role="tab" id="heifing_chq_kd_<?php echo $key ?><?php echo $ui_id ?>">
                                        <h4 class="panel-title">
                                            <a class="<?php echo ($key == 0 && ($nb_chq_kd < 2 || !$view_chq_kd) ? '' : 'collapsed') ?>" role="button" data-toggle="collapse" data-parent="#accordion_chq_kd_<?php echo $ui_id ?>" href="#body_chq_kd_<?php echo $key ?><?php echo $ui_id ?>" aria-expanded="<?php echo ($key == 0 && ($nb_chq_kd < 2 || !$view_chq_kd) ? 'true' : 'false') ?>" aria-controls="body_chq_kd_<?php echo $key ?><?php echo $ui_id ?>">
                                                Ch&egrave;que cadeau#<?php echo ($key+1).'<small class="col-white">: doc#'.$chq_kd->id_doc ?></small>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="body_chq_kd_<?php echo $key ?><?php echo $ui_id ?>" class="panel-collapse collapse <?php echo ($key == 0 && ($nb_chq_kd < 2 || !$view_chq_kd) ? 'in' : '') ?>" role="tabpanel" aria-labelledby="heifing_chq_kd_<?php echo $key ?><?php echo $ui_id ?>">
                                        <div class="panel-body p-t-0">
                                            <div class="row">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-striped">
                                                        <tbody>
                                                            <tr><th>Montant</th><td><?php echo $chq_kd->montant ?></td></tr>
                                                            <tr><th>Type</th><td><?php echo $chq_kd->type_cheque_cadeau ?></td></tr>
                                                            <tr><th>Code barre</th><td><?php echo $chq_kd->code_barre ?></td></tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php if(is_numeric($chq_kd->id_doc)) { ?>
                                                <div class="row">
                                                    <?php if($view_chq_kd) { ?>
                                                        <div class="col-lg-6 m-b--20">
                                                            <img id="img_temp_recto_chq_<?php echo $chq_kd->id.'_'.$key ?>" style="width: 100%;" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Recto" src="data:image/jpeg;base64,<?php echo (trim($chq_kd->n_ima_base64_recto) == '' ? $empty_b64 : $chq_kd->n_ima_base64_recto ); ?>" onclick="load_img_doc_face('<?php echo $chq_kd->id_doc ?>');" />
                                                        </div>
                                                        <div class="col-lg-6 m-b--20">
                                                            <img id="img_temp_verso_chq_<?php echo $chq_kd->id.'_'.$key ?>" style="width: 100%;" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Verso" src="data:image/jpeg;base64,<?php echo (trim($chq_kd->n_ima_base64_verso) == '' ? $empty_b64 : $chq_kd->n_ima_base64_verso ); ?>" onclick="load_img_doc_face('<?php echo $chq_kd->id_doc ?>',1);" />
                                                        </div>
                                                    <?php } else { ?>
                                                        <button type="button" class="list-group-item align-center" onclick="load_img_doc_face('<?php echo $chq_kd->id_doc ?>');" data-toggle="tooltip" data-placement="right" title="Cliquez pour afficher"><i class="material-icons">search</i> Cliquer pour afficher l'image correspondant au Ch&egrave;que cadeau <?php echo ($key+1).' - #'.$chq_kd->id_doc ?></button>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_mvmnts<?php echo $ui_id ?>">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel-group" id="accordion_mvmnt_<?php echo $ui_id ?>" role="tablist" aria-multiselectable="true">
                            <?php foreach ($mvmts as $key => $mvmt): ?>
                                <div class="panel panel-col-cyan">
                                    <div class="panel-heading ttl_accord_modal" role="tab" id="heifing_mvmnt_<?php echo $key ?><?php echo $ui_id ?>">
                                        <h4 class="panel-title">
                                            <a class="<?php echo ($key == 0 ? '' : 'collapsed') ?>" role="button" data-toggle="collapse" data-parent="#accordion_mvmnt_<?php echo $ui_id ?>" href="#body_mvmnt_<?php echo $key ?><?php echo $ui_id ?>" aria-expanded="<?php echo ($key == 0 ? 'true' : 'false') ?>" aria-controls="body_mvmnt_<?php echo $key ?><?php echo $ui_id ?>">
                                                Mouvement#<?php echo ($key+1) ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="body_mvmnt_<?php echo $key ?><?php echo $ui_id ?>" class="panel-collapse collapse <?php echo ($key == 0 ? 'in' : '') ?>" role="tabpanel" aria-labelledby="heifing_mvmnt_<?php echo $key ?><?php echo $ui_id ?>">
                                        <div class="panel-body p-t-0">
                                            <div class="row">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-striped">
                                                        <tbody>
                                                            <tr><th>Titre</th><td><?php echo $mvmt->titre_ ?></td></tr>
                                                            <tr><th>Num abonn&eacute;e</th><td><?php echo $mvmt->numero_abonne ?></td></tr>
                                                            <tr><th>Nom abonn&eacute;e</th><td><?php echo $mvmt->nom_abonne ?></td></tr>
                                                            <tr><th>Num payeur</th><td><?php echo $mvmt->numero_payeur ?></td></tr>
                                                            <tr><th>Nom payeur</th><td><?php echo $mvmt->nom_payeur ?></td></tr>
                                                            <tr><th>Code promotion</th><td><?php echo $mvmt->code_promo ?></td></tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_histo<?php echo $ui_id ?>">
            <div class="container-fluid p-r-25 p-l-25 p-t-25 p-b-25 tab_pan_disp_pli">
                <?php echo $histo ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_lot_saisie<?php echo $ui_id ?>">
            <div class="table-responsive p-t-25">
                <?php if(!is_null($lot_saisie)) { ?>
                    <table class="table table-hover table-striped">
                        <tbody>
                            <tr><th>Identifiant</th><td><?php echo $pli->id_lot_saisie ?></td></tr>
                            <tr><th>Cr&eacute;ation</th><td><?php echo ($lot_saisie->dt_creat == '' ? '' : (new DateTime($lot_saisie->dt_creat))->format('d/m/Y H:i:s')) ?></td></tr>
                            <tr><th>Par</th><td><?php echo $lot_saisie->par ?></td></tr>
                            <tr><th>Soci&eacute;t&eacute;</th><td><?php echo $lot_saisie->soc ?></td></tr>
                            <tr><th>Typologie</th><td><?php echo $lot_saisie->typo ?></td></tr>
                            <tr><th>Groupe de paiement</th><td><?php echo $lot_saisie->paie ?></td></tr>
                            <tr><th>Nombre de plis</th><td><?php echo $lot_saisie->total ?></td></tr>
                            <?php if(count($pli_lot) > 0) { ?>
                                <tr>
                                    <th rowspan="<?php echo $lot_saisie->nbpli ?>">Plis<span class="badge bg-teal"><?php echo $lot_saisie->nbpli ?></span></th>
                                    <td><a href="javascript:detail_pli('<?php echo $pli_lot[0] ?>')">Pli#<?php echo $pli_lot[0] ?></a></td>
                                </tr>
                                <?php foreach ($pli_lot as $key => $p_l): ?>
                                    <?php if($key > 0) { ?>
                                        <tr><td><a href="javascript:detail_pli('<?php echo $p_l ?>')">Pli#<?php echo $p_l ?></a></td></tr>
                                    <?php } ?>
                                <?php endforeach; ?>
                            <?php } ?>
                            <?php if(count($pli_lot_bis) > 0) { ?>
                                <tr>
                                    <th rowspan="<?php echo $lot_saisie->plis_bis ?>">Plis<sup class="font-italic col-pink font-10">BIS</sup><span class="badge bg-teal"><?php echo $lot_saisie->nbpli_bis ?></span></th>
                                    <td><a href="javascript:detail_pli('<?php echo $pli_lot_bis[0] ?>')">Pli#<?php echo $pli_lot_bis[0] ?></a></td>
                                </tr>
                                <?php foreach ($pli_lot_bis as $key => $p_l_b): ?>
                                    <?php if($key > 0) { ?>
                                        <tr><td><a href="javascript:detail_pli('<?php echo $p_l_b ?>')">Pli#<?php echo $p_l_b ?></a></td></tr>
                                    <?php } ?>
                                <?php endforeach; ?>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_motif_cons<?php echo $ui_id ?>">
            <div class="body table-responsive p-t-25">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Motif du KO</th>
                            <th>Consigne</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($motifs_cons as $key => $mtf_c): ?>
                            <tr>
                                <td><?php echo $mtf_c->motif_operateur ?><br><small><?php echo ($mtf_c->dt_motif_ko == '' ? '' : (new DateTime($mtf_c->dt_motif_ko))->format('d/m/Y G:i:s')) ?></small></td>
                                <td><?php echo $mtf_c->consigne_client ?><br><small><?php echo ($mtf_c->dt_consigne == '' ? '' : (new DateTime($mtf_c->dt_consigne))->format('d/m/Y G:i:s')) ?></small></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_comments<?php echo $ui_id ?>">
            <div class="container-fluid p-t-25">
                <?php foreach ($comments as $key_comment => $comment): ?>
                    <div class="media">
                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $comment->login ?> <small class="font-10">[<?php echo ($comment->dt_comment == '' ? '' : (new DateTime($comment->dt_comment))->format('d/m/Y G:i:s')) ?>]</small></h4>
                            <p class="p-l-50">
                            <?php echo nl2br(htmlspecialchars($comment->commentaire)) ?>
                            </p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<style>
.tab_pan_disp_pli .row_histo_pli {
    max-height: none !important;
    overflow-y: hidden !important;
}
.ttl_accord_modal{
    background-color: beige;
}
</style>
