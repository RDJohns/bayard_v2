<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-12">
        <dl class="dl-horizontal">
            <dt>Cr&eacute;ation: </dt>
            <dd><?php echo ($lot->dt_creat == '' ? '' : (new DateTime($lot->dt_creat))->format('d/m/Y H:i:s')) ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Par: </dt>
            <dd><?php echo $lot->par ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Soci&eacute;t&eacute;: </dt>
            <dd><?php echo $lot->soc ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Typologie: </dt>
            <dd><?php echo $lot->typo ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Paiement: </dt>
            <dd><?php echo $lot->paie ?></dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>Pli total: </dt>
            <dd><?php echo $lot->total ?></dd>
        </dl>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <dl class="dl-horizontal">
            <dt><?php echo $lot->nbpli ?> pli<?php echo ($lot->nbpli > 1 ? 's' : '') ?>: </dt>
            <dd><?php echo $lot->plis ?></dd>
        </dl>
    </div>
    <div class="col-lg-6">
        <dl class="dl-horizontal">
            <dt><?php echo $lot->nbpli_bis ?> pli<?php echo ($lot->nbpli_bis > 1 ? 's' : '') ?><sup class="font-italic col-pink font-10">BIS</sup>: </dt>
            <dd><?php echo $lot->plis_bis ?></dd>
        </dl>
    </div>
</div>
