<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<ul class="nav nav-tabs tab-nav-right font-12" role="tablist" style="background-color: rgba(255,255,255,0.3);width: 100%;margin-bottom:-3px;">
    <li role="presentation" class="active"><a href="#tab_detail_nb_num_vs_ged_courrier" data-toggle="tab">Courrier</a></li>
    <li role="presentation"><a href="#tab_detail_nb_num_vs_ged_mail" data-toggle="tab">Mail</a></li>
    <!-- <li role="presentation"><a href="#tab_detail_nb_num_vs_ged_sftp" data-toggle="tab">SFTP</a></li> -->
</ul>