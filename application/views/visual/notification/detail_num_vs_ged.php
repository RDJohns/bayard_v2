<?php defined('BASEPATH') OR exit('No direct script access allowed');
    $icon_ok = '<i class="material-icons m-b-0 col-green">done</i>';
    function diff_num_vs_ged($diff){
        return '<span class="col-red">'.$diff.'</span>';
    }
?>

<div class="tab-content p-b-0" style="height: auto;">
    <div role="tabpanel" class="tab-pane fade in active" id="tab_detail_nb_num_vs_ged_courrier">
        <div class="table-responsives">
            <table class="table table-hover table-striped table_detail_nb_num_vs_ged" >
                <thead>
                    <tr>
                        <th></th>
                        <th>Lot scan</th>
                        <th class="align-center">Num&eacute;ris&eacute;s</th>
                        <th class="align-center">Int&eacute;gr&eacute;s</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dbs_courrier as $key => $db): ?>
                        <tr>
                            <th><?php echo ($db['date']->format('d/m/Y H:i:s')) ?></th>
                            <th><?php echo $db['lot'] ?></th>
                            <th class="align-center"><?php echo $db['nb_num'] ?></th>
                            <th class="align-center"><?php echo $db['nb_pli'] ?></th>
                            <th class="align-right p-b-0"><?php echo ($db['diff'] >= 0 ? $icon_ok : diff_num_vs_ged($db['diff'])) ?></th>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="tab_detail_nb_num_vs_ged_mail">
        <div class="table-responsives">
            <table class="table table-hover table-striped table_detail_nb_num_vs_ged" >
                <thead>
                    <tr>
                        <th></th>
                        <th>Boite mail</th>
                        <th class="align-center">R&eacute;ception</th>
                        <th class="align-center">Int&eacute;gr&eacute;s</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dbs_mail as $key => $db): ?>
                        <tr>
                            <th><?php echo ($db['date']->format('d/m/Y')) ?></th>
                            <th><?php echo $db['dossier'] ?></th>
                            <th class="align-center"><?php echo $db['nb_num'] ?></th>
                            <th class="align-center"><?php echo $db['nb_pli'] ?></th>
                            <th class="align-right p-b-0"><?php echo ($db['diff'] >= 0 ? $icon_ok : diff_num_vs_ged($db['diff'])) ?></th>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="tab_detail_nb_num_vs_ged_sftp">

    </div>
</div>