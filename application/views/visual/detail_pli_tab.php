<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<ul class="nav nav-tabs tab-nav-right font-12" role="tablist" style="background-color: rgba(255,255,255,0.3);width: 100%;margin-bottom:-3px;">
    <li role="presentation" class="active"><a href="#tab_pli<?php echo $ui_id ?>" data-toggle="tab">Pli</a></li>
    <li role="presentation"><a href="#tab_docs<?php echo $ui_id ?>" data-toggle="tab">Documents<span class="badge bg-cyan font-9"><?php echo $nb_docs ?></span></a></li>
    <li role="presentation"><a href="#tab_chqs<?php echo $ui_id ?>" data-toggle="tab">Ch&egrave;ques<span class="badge bg-cyan font-9"><?php echo count($chqs) ?></span></a></li>
    <li role="presentation"><a href="#tab_chq_kdx<?php echo $ui_id ?>" data-toggle="tab">Ch&egrave;ques cadeaux<span class="badge bg-cyan font-9"><?php echo count($chqs_kd) ?></span></a></li>
    <li role="presentation"><a href="#tab_mvmnts<?php echo $ui_id ?>" data-toggle="tab">Mouvements<span class="badge bg-cyan font-9"><?php echo count($mvmts) ?></span></a></li>
    <li role="presentation"><a href="#tab_histo<?php echo $ui_id ?>" data-toggle="tab">Historique</a></li>
    <li role="presentation"><a href="#tab_lot_saisie<?php echo $ui_id ?>" data-toggle="tab">Lot-saisie</a></li>
    <li role="presentation"><a href="#tab_motif_cons<?php echo $ui_id ?>" data-toggle="tab">Motif-consigne<span class="badge bg-cyan font-9"><?php echo count($motifs_cons) ?></span></a></li>
    <li role="presentation"><a href="#tab_comments<?php echo $ui_id ?>" data-toggle="tab">Commentaires<span class="badge bg-cyan font-9"><?php echo count($comments) ?></span></a></li>
</ul>