<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="row m-t-10">
    <div class="col-lg-12">
        <div class="card card_filtre">
            <div class="body">
            <input type="hidden" id="relation-client" value="<?php echo (int) $relation_client; ?>"> <!-- pour avoir un menu de hauteur de deux lignes Benja-->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>R&eacute;ception du : </b></div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field elem_filtre" id="dt_mail_1" />
                                        <label class="form-label">debut</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field elem_filtre" id="dt_mail_2" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Traitement du : </b></div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field elem_filtre" id="dt_act_1" />
                                        <label class="form-label">debut</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field elem_filtre" id="dt_act_2" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>&Eacute;tape :</b></div>
                            <div class="col-xs-9">
                                <select class="form-control show-tick elem_filtre" id="sel_step" multiple data-live-search="true" title="Toutes" data-size="7" onchange="">
                                    <?php $grp = ''; $ln = 0; ?>
                                    <?php foreach ($steps as $step) : ?>
                                        <?php if($grp != $step->flgtt_etape){
                                            if($ln > 0){echo '</optgroup>';}
                                            $grp = $step->flgtt_etape;
                                            $ln++; ?>
                                            <optgroup label="<?php echo $step->flgtt_etape ?>">
                                        <?php } ?>
                                        <option value="<?php echo $step->id_flag_traitement ?>" data-subtext="<?php echo ' - ' . $step->flgtt_etape ?>"><?php echo $step->flgtt_libelle_etape ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Statut :</b></div>
                            <div class="col-xs-9">
                                <select class="form-control show-tick elem_filtre" id="sel_state" multiple data-live-search="true" title="Tous" data-size="7" onchange="">
                                    <?php foreach ($status as $statu) : ?>
                                        <option value="<?php echo $statu->id_statut_saisie ?>"><?php echo $statu->libelle ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Soci&eacute;t&eacute; :</b></div>
                            <div class="col-xs-9 col_hx_">
                                <select class="form-control show-tick elem_filtre" id="sel_soc" multiple data-live-search="true" title="Toutes" data-size="7" onchange="">
                                    <?php foreach ($socities as $socity) : ?>
                                        <option value="<?php echo $socity->id ?>"><?php echo $socity->nom_societe ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx_">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Typologie :</b></div>
                            <div class="col-xs-9 col_hx_">
                                <select class="form-control show-tick elem_filtre" id="sel_typo" multiple data-live-search="true" title="Toutes" data-size="7" onchange="">
                                    <?php foreach ($types as $type) : ?>
                                        <option value="<?php echo $type->id ?>" data-subtext="<?php echo ' - ' . $type->sous_lot ?>"><?php echo $type->typologie ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Mode de paiement :</b></div>
                            <div class="col-xs-9 col_hx_">
                                <select class="form-control show-tick elem_filtre" id="sel_md_paie" multiple data-live-search="true" title="Toutes" data-size="7" onchange="">
                                    <?php foreach ($md_paiements as $md_paiement) : ?>
                                        <option value="<?php echo $md_paiement->id_mode_paiement ?>"><?php echo $md_paiement->mode_paiement ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control elem_filtre" id="txt_recherche_id_pli" onkeyup="" />
                                <label class="form-label">Rechercher: #ID Pli</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-4 col_hx m-t-10 align-right"><b>Niveau saisie:</b></div>
                            <div class="col-xs-8 col_hx_">
                            <select class="form-control show-tick elem_filtre" id="sel_niveau_saisie" multiple data-live-search="true" title="Tous" data-size="3" onchange="//load_table();" >
                                <option value="1" >Niveau 1</option>
                                <option value="2" >Niveau 2</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control elem_filtre" id="txt_recherche_pli" onkeyup="" />
                                <label class="form-label">Rechercher: Lot.Scan ou #Lot.saisie</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control elem_filtre" id="txt_recherche_payeur" onkeyup="" />
                                <label class="form-label">Rechercher: Nom.Payeur ou N°Payeur</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control elem_filtre" id="txt_recherche_abonne" onkeyup="" />
                                <label class="form-label">Rechercher: Nom.Abonn&eacute; ou N°Abonn&eacute;</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control elem_filtre" id="txt_recherche_cmc7" onkeyup="" />
                                <label class="form-label">Rechercher: CMC7</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_hx">
                        <button type="button" class="btn btn-block btn-info btn-lg waves-effect" onclick="load_table();">
                            <i class="material-icons">search</i>
                            <span>Rechercher / Rafraichir</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <table id="visu_pli_dtt" class="table table-bordered table-striped table-hover" data-toggle="tooltip" data-placement="top" title="Cliquez sur les liens en bleus pour afficher les détails.">
                    <thead>
                        <tr>
                            <th class="align-center">R&eacute;ception</th>
                            <th class="align-center">Soci&eacute;t&eacute;</th>
                            <th class="align-center">#ID</th>
                            <th class="align-center">Pli</th>
                            <th class="align-center">&Eacute;tape</th>
                            <th class="align-center">&Eacute;tat</th>
                            <th class="align-center">Statut</th>
                            <th class="align-center">Type</th>
                            <th class="align-center">Mouvements</th>
                            <th class="align-center">Paiement</th>
                            <th class="align-center">CMC7</th>
                            <th class="align-center">Commande</th>
                            <th class="align-center">Motif-KO</th>
                            <th class="align-center">Consigne</th>
                            <th class="align-center">Commentaires</th>
                            <th class="align-center">Lot.Saisie</th>
                            <th class="align-center">Lot.Scan</th>
                            <th class="align-center">Num&eacute;risation</th>
                            <th class="align-center">traitement</th>
                            <!-- <th class="align-center">Typage</th> -->
                            <th class="align-center">Niveau saisie</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="hide_img" id="zone_img_temp"></div>
<!-- modales -->
<div class="modal fade" id="mdl_display_mvmt" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pli#<span id="mdl_display_mvmt_title"></span></h4>
            </div>
            <div class="modal-body" id="mdl_display_mvmt_body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_display_pli" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pli#<span id="mdl_display_pli_title"></span></h4>
            </div>
            <div class="modal-body" id="mdl_display_pli_body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_display_histo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pli#<span id="mdl_display_histo_title"></span></h4>
            </div>
            <div class="modal-body" id="mdl_display_histo_body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_display_lot_saisie" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Lot saisie: <span id="mdl_display_lot_saisie_title"></span></h4>
            </div>
            <div class="modal-body" id="mdl_display_lot_saisie_body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_input_ke" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_comment_pli" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg_" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-lg-8">
                        <h4 class="modal-title">Pli#<span id="mdl_comment_pli_id_pli"></span></h4>
                    </div>
                    <div class="col-lg-4 align-right">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                    </div>
                </div>
            </div>
            <div class="modal-body" id="mdl_comment_pli_body" style="max-height: 60vh; overflow-y: auto;"></div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <div class="form-line">
                                <textarea rows="1" class="form-control no-resize auto-growth" placeholder="Votre commentaire..." id="new_comment_pli"></textarea>
                            </div>
                            <span class="input-group-addon">
                                <button type="button" class="btn btn-default bg-light-green_ btn-circle-lg waves-effect waves-circle waves-float" onclick="add_comment_pli();">
                                    <i class="material-icons">chat</i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdl_disp_comment_pli" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg_" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-lg-8">
                        <h4 class="modal-title">Commentaires sur le Pli#<span id="mdl_disp_comment_pli_id_pli"></span></h4>
                    </div>
                    <div class="col-lg-4 align-right">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                    </div>
                </div>
            </div>
            <div class="modal-body" id="mdl_disp_comment_pli_body" style="max-height: 60vh; overflow-y: auto;"></div>
        </div>
    </div>
</div>

<style>
    #visu_pli_dtt_length{
        display: inline !important;
        margin-left: 50px !important;
    }
</style>
