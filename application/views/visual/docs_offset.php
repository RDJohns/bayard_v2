<?php defined('BASEPATH') OR exit('No direct script access allowed');
    $init_grp = 0;
    $id_container = $all ? 'container_plis_offset_'.date('YmdHis').mt_rand() : $id_dom;
?>

<?php if($all) { ?>
<div class="container-fluid" id="<?php echo $id_container ?>">
<?php } ?>

    <?php if($total > $limit) { ?>
        <div class="row div_zone_pagination">
            <div class="col-lg-12 align-left">
                <div class="btn-toolbar btn_page_modal" role="toolbar" aria-label="Toolbar with button groups" style="display: inline-block;">
                <?php for($i=0; $i<$nb_btn; $i++){ ?>
                    <?php if($init_grp == 0) {
                        echo ($i > 0 ? '</div>' : '');
                    ?>
                        <div class="btn-group" role="group">
                    <?php } ?>
                    <button onclick="load_docs_offset(<?php echo $id_pli.','.($i*$limit).",'".$id_container."'" ?>);" type="button" class="btn <?php echo ($i == $btn_current ? 'btn-primary disabled_' : 'btn-default') ?> waves-effect"><?php echo ($i+1) ?></button>
                    <?php $init_grp = $init_grp == 3 ? 0 : $init_grp + 1; ?>
                <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if($total > $limit && FALSE) { ?>
        <!-- <div class="row div_zone_pagination_bottom">
            <div class="col-lg-12 align-left">
                <nav>
                    <ul class="pagination">
                        <?php for($i=0; $i<$nb_btn; $i++){ ?>
                            <li class="<?php echo ($i == $btn_current ? 'active disabled' : '') ?>"><a href="javascript:load_docs_offset(<?php echo $id_pli.','.($i*$limit).",'".$id_container."'" ?>);" class="waves-effect"><?php echo ($i+1) ?></a></li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </div> -->
    <?php } ?>

    <?php foreach ($docs as $key => $doc): ?>
    <div class="row <?php echo ($key == 0 ? 'm-t-40' : '') ?>">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header p-v-5">
                    <h2>Document N&deg;<?php echo $key+1+$offset ?><small>#<?php echo $doc->id_document.' - recto: '.$doc->n_ima_recto.' - verso: '.$doc->n_ima_verso.'' ?></small></h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-xs-6 m-b--20">
                            <img id="img_temp_recto_<?php echo $doc->id_pli.'_'.$key ?>" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Recto" src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_recto) == '' ? $empty_b64 : $doc->n_ima_base64_recto ); ?>" onclick="load_img_doc_face('<?php echo $doc->id_document ?>');" />
                        </div>
                        <div class="col-xs-6 m-b--20">
                            <img id="img_temp_verso_<?php echo $doc->id_pli.'_'.$key ?>" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Verso" src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_verso) == '' ? $empty_b64 : $doc->n_ima_base64_verso ); ?>" onclick="load_img_doc_face('<?php echo $doc->id_document ?>', 1);" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
<?php if($all) { ?>
</div>
<?php } ?>

<style>
    .img_docs{
        width: 100%;
        cursor: pointer;
    }
    .div_zone_pagination{
        position: fixed;
        z-index: 1;
        padding-top: 5px;
    }
    /*.btn_page_modal{
        padding-left: 150px;
    }*/
</style>
