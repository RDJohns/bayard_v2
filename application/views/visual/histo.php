<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row row_histo_pli">
    <div class="timeline">
        <span class="timeline-label">
            <span class="label label-primary">Maintenant: <?php echo date('d/m/Y H:i:s') ?></span>
        </span>
        <?php foreach ($pli_events as $key => $pli_event): ?>
        <div class="timeline-item">
            <div class="timeline-point timeline-point-default">
                <i class="material-icons"><?php echo ($pli_event->flag == 0 ? 'mode_edit' : 'local_offer') ?></i>
            </div>
            <div class="timeline-event timeline-event-<?php echo $pli_event->color ?>" >
                <div class="timeline-heading"><h4><?php echo $pli_event->header ?></h4></div>
                <div class="timeline-body">
                    <div><?php echo $pli_event->body ?></div>
                    <?php if ($pli_event->coms != ''): ?>
                    <div class="font-italic font-10"><?php echo $pli_event->coms ?></div>
                    <?php endif; ?>
                    <?php if ($pli_event->dt_motif_ko != ''): ?>
                        <div class="align-center">
                            <button type="button" class="btn btn-primary btn-xs waves-effect" data-trigger="focus" data-container="body" data-toggle="popover" data-placement="bottom" title="<?php echo $pli_event->dt_motif_ko ?>" data-content="<?php echo $pli_event->motif_ko ?>">
                                <i class="material-icons">info</i> Motifs
                            </button>
                            <?php if ($pli_event->consigne_ko != ''): ?>
                            <button type="button" class="btn btn-primary btn-xs waves-effect" data-trigger="focus" data-container="body" data-toggle="popover" data-placement="bottom" title="<?php echo $pli_event->dt_consigne ?>" data-content="<?php echo $pli_event->consigne_ko ?>">
                                <i class="material-icons">info</i> Consignes
                            </button>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <div class="align-right"><small><?php echo $pli_event->moment ?></small></div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <span class="timeline-label">
            <span class="label label-primary">Nouveau pli: <?php echo $dt_courrier ?></span>
        </span>
    </div>
</div>

<style>
    div.timeline-point i{
        font-size: 17px !important;
        margin-top: 3px !important;
        margin-left: 2px !important;
    }
</style>

<script>
    $(function () {
        $('[data-toggle="popover"]').popover()
    })
</script>
