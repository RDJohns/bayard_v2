<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid p-r-0 p-l-0">

    <?php if (count($motif_consignes) > 0): ?>
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li role="presentation" class="active"><a href="#tab_documents_pli" data-toggle="tab">Documents</a></li>
            <li role="presentation"><a href="#tab_motif_consigne_pli" data-toggle="tab">Motif-consignes</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="tab_documents_pli">
                <div style="max-height: 70vh; overflow-y: auto;">
                    <?php foreach ($docs as $key => $doc): ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-r-0 p-l-0">
                        <div class="card">
                            <div class="header p-v-5">
                                <h2>Document N&deg;<?php echo $key+1 ?><small>#<?php echo $doc->id_document ?></small></h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-lg-6 m-b--20">
                                        <img id="img_temp_recto_<?php echo $doc->id_pli.'_'.$key ?>" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Recto" src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_recto) == '' ? $empty_b64 : $doc->n_ima_base64_recto ); ?>" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$id_pli.'/ Document#'.$doc->id_document.' / RECTO'; ?>" data-group="<?php echo $id_pli ?>" data-src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_recto) == '' ? $empty_b64 : $doc->n_ima_base64_recto ); ?>" />
                                    </div>
                                    <div class="col-lg-6 m-b--20">
                                        <img id="img_temp_verso_<?php echo $doc->id_pli.'_'.$key ?>" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Verso" src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_verso) == '' ? $empty_b64 : $doc->n_ima_base64_verso ); ?>" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$id_pli.'/ Document#'.$doc->id_document.' / VERSO'; ?>" data-group="<?php echo $id_pli ?>" data-src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_verso) == '' ? $empty_b64 : $doc->n_ima_base64_verso ); ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_motif_consigne_pli">
                <div style="max-height: 70vh; overflow-y: auto;">
                    <?php if (count($motif_consignes) > 0): ?>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Motif du KO</th>
                                        <th>Consigne</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($motif_consignes as $key => $mtf_c): ?>
                                        <tr>
                                            <td><?php echo $mtf_c->motif_operateur ?><br><small><?php echo ($mtf_c->dt_motif_ko == '' ? '' : (new DateTime($mtf_c->dt_motif_ko))->format('d/m/Y G:i:s')) ?></small></td>
                                            <td><?php echo $mtf_c->consigne_client ?><br><small><?php echo ($mtf_c->dt_consigne == '' ? '' : (new DateTime($mtf_c->dt_consigne))->format('d/m/Y G:i:s')) ?></small></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-info">
                            <strong>0</strong> motif-KO ou consigne.
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div style="max-height: 77vh; overflow-y: auto;">
            <?php foreach ($docs as $key => $doc): ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-r-0 p-l-0">
                <div class="card">
                    <div class="header p-v-5">
                        <h2>Document N&deg;<?php echo $key+1 ?><small>#<?php echo $doc->id_document ?></small></h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-6 m-b--20">
                                <img id="img_temp_recto_<?php echo $doc->id_pli.'_'.$key ?>" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Recto" src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_recto) == '' ? $empty_b64 : $doc->n_ima_base64_recto ); ?>" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$id_pli.'/ Document#'.$doc->id_document.' / RECTO'; ?>" data-group="<?php echo $id_pli ?>" data-src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_recto) == '' ? $empty_b64 : $doc->n_ima_base64_recto ); ?>" />
                            </div>
                            <div class="col-lg-6 m-b--20">
                                <img id="img_temp_verso_<?php echo $doc->id_pli.'_'.$key ?>" class="thumbnail img_docs" data-toggle="tooltip" data-placement="top" title="Verso" src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_verso) == '' ? $empty_b64 : $doc->n_ima_base64_verso ); ?>" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$id_pli.'/ Document#'.$doc->id_document.' / VERSO'; ?>" data-group="<?php echo $id_pli ?>" data-src="data:image/jpeg;base64,<?php echo (trim($doc->n_ima_base64_verso) == '' ? $empty_b64 : $doc->n_ima_base64_verso ); ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    
</div>

<style>
.img_docs{
    width: 100%;
}
.m-b--20{
    margin-bottom: -20px !important;
}
.p-v-5{
    padding-top: 5px !important;
    padding-bottom: 5px !important;
}
</style>
