<?php
//setlocale (LC_TIME, 'fr_FR');
$liste = '';
$sum_pli  = 0;
$sum_user = 0;
$sum_data = 0;
foreach($arr_data as $k => $arr){
	foreach($arr as $k1 =>$tab){
		$sum_data += $tab["duree"];		
	}							
}
//$sum_data = number_format($sum_data,3);

	$liste .='	
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="card">
			<div class="header_cstm">
				<div class="row">
					<div class="col-lg-7">
						<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
							DMT par pli/TC >> '.number_format($sum_data,3).' h</span>
					</div>
					<div class="col-lg-4">
						<span style="font-family:Arial,Tahoma,sans-serif;font-size:16px;">
						<i class="fa fa-caret-square-o-right" onclick="export_dmt_gbl_gbl_global()">
							<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;cursor:pointer;" >
							Exporter								
							</span>
						</i>								
						</span>	
					</div>
					<div class="col-lg-1 m-l--20">
					<div class="preloader pl-size-xs" id="preloader_global_global_global" style="display:none;">
						<div class="spinner-layer pl-green" >
							<div class="circle-clipper left">
									<div class="circle"></div>
							</div>
							<div class="circle-clipper right">
									<div class="circle"></div>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="body">
				<div class="table-container-body" id="table_dmt_global_global">
				<table id="global_global" class="table" style="table-layout: fixed;">
					<thead>
						<tr>
							<th style="width:20%">
								<div class="col-sm-9" style="margin-top:-27px; margin-bottom:-20px;">
								<div class="form-group">
									<div class="form-line">
										<input type="text" style="border:none;width:100%;margin-top:7px;" placeholder="#Id.Ged" id="recherche_pli"/>
									</div>
								</div>
							</div>
							</th>
							<th  style="width:20%">Opérateur</th>
							<th  style="width:20%">Durée</th>							  
							</tr>
					</thead>
				
					<tbody>';
							foreach($arr_data as $k => $arr){
								foreach($arr as $k1 =>$tab){
									//$sum_data += $tab["duree"];
									$liste .= '<tr class="tr_recherche">
									 <td style="width:20%" class="td_recherche">#'.$tab["id_pli"].'</td>
									 <td style="width:20%">'.$tab["login"].'</td>
									 <td style="width:20%" class="v_recherche">'.number_format($tab["duree"],3).'</td>
									</tr>';
								}							
						  }
						$liste .='</tbody>
					<tfoot >
						<tr>
							<th>Total</th>
							<th></th>
							<th id="total_recherche">'.number_format($sum_data,3).'
							</th>					                             			                             
						</tr>
					</tfoot>
				</table>
			</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="total_init" value="'.number_format($sum_data,3).'">
	
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="card">
			<div class="header_cstm">
				<div class="row">
					<div class="col-lg-8">
						<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
							DMT par pli</span>
							</div>
							<div class="col-lg-3">
								<!--<span style="font-family:Arial,Tahoma,sans-serif;font-size:18px;">
									<i class="fa fa-caret-square-o-right" onclick="export_dmt_globalpli()">
										<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;cursor: pointer;" >
										Exporter								
										</span>
									</i>								
						</span>-->
					</div>
					<div class="col-lg-1 m-l--20">
					<div class="preloader pl-size-xs" id="preloader_globalpli" style="display:none;">
						<div class="spinner-layer pl-green" >
							<div class="circle-clipper left">
									<div class="circle"></div>
							</div>
							<div class="circle-clipper right">
									<div class="circle"></div>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="body">
				<div class="table-container-body" id="table_dmt_pli_global">
				<table id="pli_global" class="table" style="table-layout: fixed;">
					<thead>
						<tr>
							<th  style="width:60%">#ID.GED</th>
							<th  style="width:20%">Durée</th>
						  
						</tr>
					</thead>
				
					<tbody>';
						foreach($arr_pli as $k => $tab){
							$sum_pli += $tab["duree"]; 
						    $liste .= '<tr>
							 <td>#'.$tab["id_pli"].'</td>
							 <td>'.$tab["duree"].'</td>
							 
						</tr>';
							
					  }
					$liste .='</tbody>
					<tfoot >
						<tr>
							<th>Total</th>
							<th>'.number_format($sum_pli,3).'</th>					                             
						</tr>
					</tfoot>
				</table>
			</div>
			</div>
		</div>
	</div>
	
	
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="card">
			<div class="header_cstm">
				<div class="row">
					<div class="col-lg-8">
						<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;">
							DMT par opérateur</span>
							</div>
							<div class="col-lg-3">
								<!--<span style="font-family:Arial,Tahoma,sans-serif;font-size:18px;">
									<i class="fa fa-caret-square-o-right" onclick="export_dmt_globalop()">
										<span style="font-family:Arial,Tahoma,sans-serif;font-size:14px;cursor: pointer;" >
										Exporter								
										</span>
									</i>								
						</span>-->
					</div>
					<div class="col-lg-1 m-l--20">
					<div class="preloader pl-size-xs" id="preloader_globalop" style="display:none;">
						<div class="spinner-layer pl-green" >
							<div class="circle-clipper left">
									<div class="circle"></div>
							</div>
							<div class="circle-clipper right">
									<div class="circle"></div>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="body">
				<div class="table-container-body" id="table_dmt_op_global">
				<table id="op_global" class="table">
					<thead>
						<tr>
							<th  style="width:60%">Opérateur</th>
							<th  style="width:20%">Durée</th>
						  
						</tr>
					</thead>
					<tbody>';
						foreach($arr_user as $k => $tab){
							$sum_user += $tab["duree"];
						    $liste .= '<tr>
							 <td>'.$tab["login"].'</td>
							 <td>'.number_format($tab["duree"],3).'</td>
							 
						</tr>';
							
					  }
					$liste .='</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th>'.$sum_user.'</th>					                             
						</tr>
					</tfoot>
				</table>
			</div>
			</div>
		</div>
	</div>
	

	
	<!-- #END#  -->';
	echo $liste;
	
?>
<script >
$('#recherche_pli').bind('keyup',function (e){
	if(e.keyCode == 13) {
	
		var id_pli = $.trim(($('#recherche_pli').val()));	
		if (!id_pli) {
				$('tr.tr_recherche').show();
			} else {
				$('tr.tr_recherche').hide();
				$('.td_recherche:contains(#'+id_pli+')').parent('tr').show();
				
			}
		recalculer();
	}
	 
		
});

function recalculer(){
	var total = 0;
	var id_pli = $.trim(($('#recherche_pli').val()));
	if (!id_pli) {
		$('#total_recherche').html($('#total_init').val());		
	}
	else{
		$( '.v_recherche:visible').map(function() {
				total += ($( this ).html())*1;
			}) ;
		/*$('.tr_recherche').each(function(){					
			if($('.tr_recherche', this).text().search(new RegExp(id_pli)) == -1){
					var i = $('.v_recherche:contains(.)').html();	
					total = (i*1)+total;
			}
		});
		*/
		var tot = total;
		var locale = 'fr';
		var options = {style: 'decimal',  minimumFractionDigits: 3, maximumFractionDigits: 3};
		var formatter = new Intl.NumberFormat(locale, options);
		
		$("#total_recherche").html(formatter.format(tot));
	
	}   
}
</script>