<script>
    var url_site = '<?php echo site_url(); ?>';
</script>
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="logo-pro">
                    <a href="index.html"><img class="main-logo" src="" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="header-top-area">
         </div> 
    </div>
    <br />
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="">
				<div class="sparkline13-hd" style="padding:5px 0px 0px 20px" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="main-sparkline13-hd">
						<div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin:0px 0px 20px 0px">
								<!--h1 style="color:#69B145"><i class="fa fa-envelope-square" style="color:#C62057"></i>
								Suivi des réceptions</h1-->  
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					<span class="/*pull-right*/" style="font-size:13px;margin-top:8px;">Traitement du</span></div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group-cstm" nowrap="nowrap">
							<div class="input-daterange input-group" id="bs_datepicker_range_container">
								<div class="form-line">
									<input type="text" id="date_debut_ttt" name="date_debut_ttt" class="form-control" placeholder="Début traitement ..." value="<?php echo date("d/m/Y");?>">
								</div>
									<span class="input-group-addon">au</span>
								<div class="form-line">
									<input type="text" id="date_fin_ttt" name="date_fin_ttt" class="form-control" placeholder="Fin traitement..." value="<?php echo date("d/m/Y");?>">
								</div>
							 </div>								
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="form-group-cstm">
							<div class="row clearfix c-select_soc">
							<select class="btn-group form-control show-tick" id="select_soc" name="select_soc">
								<option value="">-- Société --</option>
									
							</select>
							</div>	
						</div>
					</div>					
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						 <div class="form-group-cstm">
							 <div class="row clearfix c-select_typo">
								<select class="form-control show-tick" data-live-search="true" id="select_typo" name="select_typo">
									<option value="">-- Typologie --</option>
										
								</select>
							</div>	
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="form-group-cstm">
							<div class="row clearfix c-select_op">
							<select class="btn-group bootstrap-select form-control show-tick" id="select_op" name= "select_op">
								<option value="">-- Opérateur --</option>
									
							</select>
							</div>	
						</div>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
						 <div class="form-group-cstm">
							
								<button class="btn btn-primary waves-effect waves-light" id="visu_dmt" onclick="charger_typage_dmt();">
								<i class="fa fa-search"></i>&nbsp;Rechercher</button>
								<!-- onclick="charger_typage_dmt();charger_dmt_global_global()" -->
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom:30px;">	
				</div>
				
								
				<div id="result-rsrch" style="display: block;">					
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="">
								<div class="sparkline13-graph">
										<div class="datatable-dashv1-list custom-datatable-overright">
											<div id="toolbar">
												<!--select class="form-control dt-tb">
													<option value="">Export Basic</option>
													<option value="all">Export All</option>
													<option value="selected">Export Selected</option>
												</select-->
											</div>
										
										</div>
								</div>
							</div>
						</div>					
				</div>
			</div>
		<!--/div-->
	</br>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
		<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
		<em id="error_message">*Veuillez saisir les criteres de recherche</em>
		</div>
	</div>

	<div class="col-offset-3 col-md-12 col-sm-12 col-xs-12" id="tab5" style="margin-top:50px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
         <div class="analytics-content" id="detail_stat">
        
		 <div class="body">
         <div class="table-responsive">		
             <!-- <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="dmt_pli"> -->
			
			</div>
         </div>
		</div>
		</div>
    </div>
	</div>
	
	<div class="row">
    <div class="col-lg-12">
        <div class="card" style="box-shadow: 0 2px 10px rgba(0, 0, 0, 0) !important;">
            <div class="body" style="margin-top:-50px;">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#typage" data-toggle="tab">
                                    <i class="material-icons">style</i> <span id="div_typage">TYPAGE</span>
                                    <button type="button" class="btn-default btn-circle waves-effect waves-circle" onclick="export_dmt_typage();" title="Export-Excel" id="a">
                                        <i class="material-icons">file_download</i>
                                    </button>
									<div class="preloader pl-size-xs" id="preloader_typage" style="display:none;">
										<div class="spinner-layer pl-black">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>
									<div class="preloader pl-size-xs" id="preloader_export_typage" style="display:none;">
										<div class="spinner-layer pl-cyan">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>									
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#saisie" data-toggle="tab">
                                    <i class="material-icons">border_color</i> SAISIE
                                    <button type="button" class="btn-default btn-circle waves-effect waves-circle" onclick="export_dmt_saisie();" title="Export-Excel">
                                        <i class="material-icons">file_download</i>
                                    </button>
									<div class="preloader pl-size-xs" id="preloader_saisie" style="display:none;" >
										<div class="spinner-layer pl-black">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>
									<div class="preloader pl-size-xs" id="preloader_export_saisie" style="display:none;">
										<div class="spinner-layer pl-cyan">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>	
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#ctrl" data-toggle="tab">
                                    <i class="material-icons">done_all</i> CONTR&Ocirc;LE
                                    <button type="button" class="btn-default btn-circle waves-effect waves-circle" onclick="export_dmt_controle();" title="Export-Excel">
                                        <i class="material-icons">file_download</i>
                                    </button>
									<div class="preloader pl-size-xs" id="preloader_controle" style="display:none;">
										<div class="spinner-layer pl-black">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>
									<div class="preloader pl-size-xs" id="preloader_export_controle" style="display:none;">
										<div class="spinner-layer pl-cyan">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>	
                                </a>
                            </li>
							 <li role="presentation">
                                <a href="#global" data-toggle="tab">
                                    <i class="material-icons">pie_chart</i> GLOBAL
                                    <button type="button" class="btn-default btn-circle waves-effect waves-circle" onclick="export_dmt_global_global();" title="Export-Excel">
                                        <i class="material-icons">file_download</i>
                                    </button>
									<div class="preloader pl-size-xs" id="preloader_global_global" style="display:none;">
										<div class="spinner-layer pl-black">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>
									<div class="preloader pl-size-xs" id="preloader_export_global_global" style="display:none;">
										<div class="spinner-layer pl-cyan">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div>
											<div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>	
                                </a>
                            </li>
                        </ul>
                
                       <!-- <div class="tab-content"></div>-->
						
						<div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="typage">
							
								<div class="row" id="dmt_global">								
										<!--** repartition global typage ***-->
								</div>
                                							
								<div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="dmt_pli">
											<thead  class="custom_font">
												<tr>
												<th>Opérateur</th>												
												<th>#ID.GED</th>
												<th>Société</th>
												<th>Traitement du</th>
												<th>Nb. Mouvement</th>				
												<th>Typologie</th>				
												<th>Mode de paiement</th>				
												<th>Lot scan</th>
												<th>Statut</th>
												<th>Typage du</th>
												<th  nowrap="nowrap">Début et fin de typage</th>
												<th>Durée en h</th>
												</tr>
											</thead> 
											 <tbody class="custom_font">
											 </tbody>
															
										</table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="saisie">
							
								<div class="row" id="dmt_global_saisie">								
										<!--** repartition global saisie***-->
								</div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="dmt_saisie_pli">
                                            <thead>
                                                <tr>
                                                    <th>Opérateur</th>												
													<th>#ID.GED</th>
													<th>Société</th>
													<th>Traitement du</th>
													<th>Nb. Mouvement</th>				
													<th>Typologie</th>				
													<th>Mode de paiement</th>				
													<th>Lot scan</th>
													<th>Statut</th>
													<th>Saisie du</th>
													<th nowrap="nowrap">Début et fin de saisie</th>
													<th>Durée en h</th>
													
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="ctrl">
							
								<div class="row" id="dmt_global_controle">								
										<!--** repartition global controle***-->
								</div>
								
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="dmt_controle_pli">
                                            <thead>
                                                <tr>
                                                    <th>Opérateur</th>												
													<th>#ID.GED</th>
													<th>Société</th>
													<th>Traitement du</th>
													<th>Nb. Mouvement</th>				
													<th>Typologie</th>				
													<th>Mode de paiement</th>				
													<th>Lot scan</th>
													<th>Statut</th>
													<th>Controle du</th>
													<th nowrap="nowrap">Début et fin de controle</th>
													<th>Durée en h</th>
												</tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
							<div role="tabpanel" class="tab-pane fade" id="global">
							
								<div class="row" id="dmt_global_global">								
										<!--** repartition global controle***-->
								</div>
								
                                <div class="row">
                                    <div class="col-lg-12">
									 <table class="table table-bordered table-striped table-hover dataTable js-export-table" style="width:100%" id="dmt_globalglobal">
                                            <thead>
                                                <tr>
											 <th>Courrier du</th>												
											<th>#ID.GED</th>
											<th>Société</th>
											<th>Nb. Mouvement</th>				
											<th>Typologie</th>				
											<th>Mode de paiement</th>				
											<th>Lot scan</th>
											<th>Statut</th>
											<th>Durée typage en h</th>
											<th>Durée saisie en h</th>
											<th>Durée controle en h</th>
											<th>Durée totale</th>
											</tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	
	
    <div class="library-book-area mg-t-30">
        <div class="container-fluid">
        </div>
    </div>
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
    </div>
    <div class="courses-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
        <div class="footer-copyright-area" style="position: fixed !important; bottom:0 !important; width: 100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright © 2018. All rights reserved DEV-SI VIVETIC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
