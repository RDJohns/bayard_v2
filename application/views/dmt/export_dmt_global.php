<?php
//setlocale (LC_TIME, 'fr_FR');
$liste = '';
$sum_pli  = 0;
$sum_user = 0;
$sum_tot_pli = 0;
	$liste .='
			<table>
				<tr>
					<td>					
						<table border="1"><tr><td bgcolor="#ccc">DMT par pli</td></tr></table>
						<table id="pli_typage" class="table" border="1" >
							<thead class="custom_font">
								<tr>
									<th  style="width:60%">#ID.GED</th>
									<th  style="width:20%">Dur&eacute;e</th>
								  
								</tr>
							</thead>
							
							<tbody class="custom_font">';
								foreach($arr_pli as $k => $tab){
									$sum_pli += $tab["duree"]; 
									$liste .= '<tr>
									 <td>#'.$tab["id_pli"].'</td>
									 <td>'.preg_replace('/\./m', ',', number_format($tab["duree"],3)).'</td>
									 
								</tr>';
									
							  }
							$liste .='</tbody>
							<tfoot   class="custom_font">
								<tr>
									<th>Total</th>
									<th>'.preg_replace('/\./m', ',', number_format($sum_pli,3)).'</th>					                             
								</tr>
							</tfoot>
						</table>
					
					</td>
					<td><table border="1"><tr><td>&nbsp;</td></tr></table></td>
					<td>
					
						<table border="1"><tr><td>DMT par op&eacute;rateur</td></tr></table>			
						<table id="op_typage" class="table" border="1">
							<thead   class="custom_font">
								<tr>
									<th  style="width:40%">Op&eacute;rateur</th>
									<th  style="width:20%">Dur&eacute;e</th>
									<th  style="width:20%">Nbr. pli</th>
									<th  style="width:20%">Cad/h</th>
								  
								</tr>
							</thead>
							
							<tbody   class="custom_font">';
								foreach($arr_user as $k => $tab){
									$sum_user += $tab["duree"];
									$dmt = $tab["nb"] / $tab["duree"];
									$sum_tot_pli +=  $tab["nb"];
									$liste .= '<tr>
									 <td>'.$tab["login"].'</td>
									 <td>'.preg_replace('/\./m', ',', number_format($tab["duree"],3)).'</td>
									 <td>'.$tab["nb"].'</td>
									<td>'.number_format($dmt,0).'</td>
									 
								</tr>';
									
							  }
							$liste .='</tbody>
							<tfoot   class="custom_font">
								<tr>
									<th>Total</th>
									<th>'.preg_replace('/\./m', ',', number_format($sum_user,3)).'</th>		
									<th>'.$sum_tot_pli.'</th>
									<th></th>										
								</tr>
							</tfoot>
						</table>
					
					</td>
				</tr>
			</table>
				
			
				
	';
	echo $liste;
	
?>