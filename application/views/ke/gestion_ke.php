<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card card_filtre">
            <div class="body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Courrier du : </b></div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_c_1" />
                                        <label class="form-label">debut</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_c_2" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="row">
                            <div class="col-xs-3 col_hx m-t-10"><b>Traitement du : </b></div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_t_1" />
                                        <label class="form-label">debut</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 align-center m-t-10">&agrave;</div>
                            <div class="col-xs-4 align-center">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control no_saisi dt_field" id="dt_t_2" />
                                        <label class="form-label">fin</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                    <div class="col-xs-3 col_hx m-t-10"><b>Société : </b></div>
                    <div class="col-xs-9 align-center">
                        <div class="form-group form-float">    
                           <select class="form-control show-tick" id="sel_societe" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                                <?php foreach ($societe as $soc): ?>
                                        <option value="<?php echo $soc->id?>" ><?php echo $soc->nom_societe ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div> 
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                    <div class="col-xs-3 col_hx m-t-10"><b>Typologie : </b></div>
                    <div class="col-xs-9 align-center">
                        <div class="form-group form-float">    
                        <select class="form-control show-tick" id="sel_typologie" multiple data-live-search="true" title="Toutes" data-size="7" onchange="//load_table();" >
                            <?php foreach ($typologies as $typo): ?>
                                    <option value="<?php echo $typo->id?>" ><?php echo $typo->typologie ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <div class="form-group form-float frm_find">
                            <div class="form-line">
                                <input type="text" class="form-control" id="txt_recherche_id_pli" onkeyup="//load_table();" />
                                <label class="form-label">#ID Pli</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col_hx">
                        <button type="button" class="btn btn-block btn-info btn-lg waves-effect m-t-20 bouton" onclick="load_table();">
                            <i class="material-icons">search</i>
                            <span>Rechercher / Rafraichir</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="search_result">
    <div class="col-lg-12">
        <div class="card">
            <div class="header">
                    RESULTATS
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="PrioriserTous()" style="float:right;margin-top:-5px;">Prioriser</button>    
            </div>
            <div class="body">
                <table id="ke" class="table table-bordered table-striped table-hover" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Retraitement</th>
                            <th class="align-center">#ID pli</th>
                            <th class="align-center">Soci&eacute;t&eacute;</th>
                            <th class="align-center">Typologie</th>
                            <th class="align-center">Date de courrier</th>
                            <th class="align-center">Date de traitement de mise en KE</th>
                            <th class="align-center">Lot Scan</th>
                            <th class="align-center">Motif KO</th>
                            <th class="align-center">Autre Motif KO</th>
                        </tr>
                    </thead>
                    <tbody>  

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>