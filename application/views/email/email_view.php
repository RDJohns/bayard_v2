<!DOCTYPE html>
<html>
<head>
	 <meta charset="utf-8" />
    <style>

        body{font-family:Verdana,'Open Sans',sans-serif;font-size: 12px;}
        th,td{border-collapse:collapse; border: 1px solid #1A747A;}
        blockquote{font-style: normal;margin-left: 32px;border-left: 4px solid #CCC;padding-left: 8px;}
    </style>
</head>
	<body>
		<img src="cid:logo_mail" alt="vivetic" border="0" />
		<?php
			
			$contentMail  = "<br/>";
			$lEquipe      = "";
			$typeAnomalie = "";
			$anomalie     = $bodyMail['anomalieType'];
			$typeTtmt     = $typeTtmt;
            $societe     = $societe;
			
			if($societe == 2)
			{
				$lEquipe = "L'équipe MILAN";
			}
			else
			{
				$lEquipe = "L'équipe BAYARD";
			}
			
			if($anomalie == 'mail')
			{
				$typeAnomalie = " que votre mail contient ";
			}
			elseif($anomalie == 'pj')
			{
				$typeAnomalie = " que le document envoyé contient ";
			}
			elseif($anomalie == 'fichier')
			{
				$fichier      = isset($bodyMail['fichier']) ? $bodyMail['fichier'] : "  ";
				$typeAnomalie = " que le fichier \"".$fichier."\"  contient ";
			}
			elseif($anomalie == 'hp')
			{
				if($typeTtmt == 2)
				{
					$typeAnomalie = "que votre fichier contient ";
				}
				else
				{
					$typeAnomalie = " que votre mail contient ";
				}
				
			}
			
			$contentMail .= "Bonjour,<br/>";
					$contentMail .= "Nous avons constaté ".$typeAnomalie." une anomalie : <br/>";
					$contentMail .= "<blockquote>".$bodyMail['anomalie']."</blockquote>";
					$contentMail .= "Pouvez-vous vérifier et nous renvoyer votre demande s’il vous plait ? <br/>";
					$contentMail .= "Cordialement, <br/>";
					$contentMail .= "--<br/>";
					$contentMail .= $lEquipe;
			echo "<br/><span style='font-size:13px;font-family:\"Verdana\",sans-serif;color:black;'>".$contentMail.'</span>';
		?>
		
		
	</body>
</html>