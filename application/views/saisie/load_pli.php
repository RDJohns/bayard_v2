<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row m-t-60">
    
    <div class="col-lg-8 div_primo align-center">
        <h3 class="op_7">Lot saisie en cours&nbsp;
            <span class="label label-default"><?php echo $current_id_lot_saisie ?></span>&nbsp;
            <button type="button" class="btn btn-default waves-effect" onclick="copyToClipboard('<?php echo $current_id_lot_saisie ?>');">
                <i class="material-icons">content_copy</i>Copier
            </button>
        </h3>
        <h5 class="op_7"><?php echo $data_lot_saisie->soc.' <i class="material-icons ico_small">keyboard_arrow_right</i> '.$data_lot_saisie->typologie.' <i class="material-icons ico_small">keyboard_arrow_right</i> '.$data_lot_saisie->groupe_paiement ?></h5>
        <h4 class="op_7">Nombre de pli enrégistré <span class="label label-default"><?php echo $nb_pli_in_current_lot_saisie ?></span></h4>
        <p>
            <input type="checkbox" id="rejet_batch_only" class="filled-in" <?php echo ($rejet_batch ? 'checked' : '') ?> />
            <label for="rejet_batch_only">Rejet&eacute; BATCH</label>
        </p>
        <?php if ($force_new_lot): ?>
            <div class="alert alert-info font-16">
                <strong>Nombre de plis atteint!</strong> Merci de cr&eacute;er un nouveau lot de saisie.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons">forward</i>
            </div>
        <?php else: ?>
            <p class="">
                <button type="button" class="btn bg-blue btn-lg waves-effect" onclick="get_pli();">
                    <i class="material-icons">assignment_returned</i>
                    <span>Recharger un pli pour ce lot</span>
                </button>
            </p>
        <?php endif; ?>
    </div>
    
    <div class="col-lg-4 div_second align-center m-t-30">
        <button type="button" class="btn bg-orange btn-lg waves-effect" onclick="new_lot_saisie();">
            <i class="material-icons">content_paste</i>
            <span>Nouveau lot de saisie</span>
        </button>
    </div>

</div>


<div id="div_loader" class="row m-t-25 cacher">
    <div class="col-xs-12 align-center">
        <div class="preloader pl-size-xl">
            <div class="spinner-layer pl-light-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function copyToClipboard(text){
        var dummy = document.createElement("input");
        document.body.appendChild(dummy);
        dummy.setAttribute('value', text);
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
        //showNotification('alert-info', '<i class="material-icons">content_copy</i> Info: "'.text.'" copi&eacute;.', 'bottom', 'right', null, null);
    }
</script>

<style>
    .div_primo{
        border-right: 1px grey dotted !important;
    }
    .v_aligne_m{
        vertical-align: middle !important;
    }
    .op_7{
        opacity: 0.7 !important;
    }
    .ico_small{
        font-size: 12px !important;
    }
</style>
