<?php defined('BASEPATH') or exit('No direct script access allowed');

if (!isset($additional_css)) {
    $additional_css = '';
}
if (!isset($additional_js)) {
    $additional_js = '';
}
if (!isset($page_titre)) {
    $page_titre = '';
}
if (!isset($contenu)) {
    $contenu = '';
}

$url_assets = base_url('assets/');

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>GED-BAYARD</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo $url_assets ?>images/favicon.ico?1" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="<?php echo $url_assets ?>font/font.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $url_assets ?>font/icon.css" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo $url_assets ?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo $url_assets ?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo $url_assets ?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Bootstrap Spinner Css -->
    <link href="<?php echo $url_assets ?>plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="<?php echo $url_assets ?>plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="<?php echo $url_assets ?>plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?php echo $url_assets ?>plugins/bootstrap-select/css/bootstrap-select.css?1" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo $url_assets ?>plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo $url_assets ?>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Sweetalert Css -->
    <link href="<?php echo $url_assets ?>plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- nprogress Css-->
    <link rel="stylesheet" href="<?php echo $url_assets ?>plugins/nprogress/nprogress.css">

    <!-- Dropzone Css -->
    <link href="<?php echo $url_assets ?>plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- magnify Css -->
    <link href="<?php echo $url_assets ?>plugins/magnify/jquery.magnify.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo $url_assets ?>css/style.css?1" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo $url_assets ?>css/themes/all-themes.css" rel="stylesheet" />

    <link href="<?php echo $url_assets ?>css/admin/page.css" rel="stylesheet" />

    <link href="<?php echo base_url() ?>src/template/css/font-awesome.min.css" rel="stylesheet" />
    <!-- my CSS -->
    <link href="<?php echo $url_assets ?>css/my_style.css?1" rel="stylesheet">
    <?php echo $additional_css; ?>

    <style>
        .dz-remove {
            color: white;
            background: #fb8888;
            font-weight: bold;
            border-radius: 24px;
            opacity: 0.4;
        }

        .dz-remove:hover {
            text-decoration: none;
            color: white;
            border-radius: 5px;
            background: #fc7070;
            opacity: 1;
        }

        a.dz-remove:link {
            text-decoration: none !important;
            color: white;
        }

        .cacher {
            display: none;
        }
    </style>

    <script>
        var chemin_site = '<?php echo site_url(); ?>';
        var date_fr_now = '<?php echo date('d/m/Y'); ?>';
        if (typeof MODE_SAISIE_BATCH == "undefined" || MODE_SAISIE_BATCH == null){
            MODE_SAISIE_BATCH = false;
        }
    </script>

</head>

<body class="theme-indigo">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Chargement...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <a class="navbar-brand" href="">
            <img src="<?php echo base_url().'/assets/images/logo.png'?>" alt="logo" style="display: block;width: 36px !important;">
        </a>
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                
                <a class="navbar-brand" href="javascript:void();"><?php echo ucfirst($page_titre); ?></a>
            </div>
            <?php
                $CI =&get_instance();
                //$actif =isset($menu_acif)?$menu_acif:null;
                $CI->menu->menu();
            ?>
            <!--div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">view_headline</i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header"><?php echo $login_user ?></li>
                            <li class="body">
                                <ul class="menu">
                                    <?php foreach ($links as $link) : ?>
                                        <li>
                                            <a href="<?php echo $link['lien'] ?>">
                                                <div class="icon-circle bg-blue">
                                                    <i class="material-icons">turned_in</i>
                                                </div>
                                                <div class="menu-info">
                                                    <h4><?php echo $link['titre'] ?></h4>
                                                </div>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                    <li>
                                        <a href="javascript:show_info_lot();">
                                            <div class="icon-circle bg-green">
                                                <i class="material-icons">info_outline</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Mes lots de saisie</h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:show_info_grp_pli();">
                                            <div class="icon-circle bg-green">
                                                <i class="material-icons">info_outline</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Regroupements des plis</h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:show_info_grp_pli_by_older();">
                                            <div class="icon-circle bg-green">
                                                <i class="material-icons">info_outline</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Regroupements des plis <br> par date-courrier</h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#mdl_display_raccourci">
                                            <div class="icon-circle bg-light-blue">
                                                <i class="material-icons">info</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Raccourcis</h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:deconnexion();">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">input</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Deconnexion</h4>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div-->
        </div>
    </nav>
    <!-- #Top Bar -->

    <section class="content">
        <div id="contenu" class="container-fluid m-t--30">

            <!-- Body -->
            <?php echo $contenu; ?>
            <!-- End Body -->

        </div>
    </section>

    <!-- modales -->
    <div class="modal fade" id="mdl_display_lot_saisie" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Mes lots de saisie</h4>
                </div>
                <div class="modal-body" id="mdl_display_lot_saisie_body">
                    <table id="lot_saisie_dtt" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="align-center">Date</th>
                                <th class="align-center">ID</th>
                                <th class="align-center">Soci&eacute;t&eacute;</th>
                                <th class="align-center">Typologie</th>
                                <th class="align-center">Paiement</th>
                                <th class="align-center">Nombre.Plis</th>
                                <th class="align-center">Plis</th>
                                <th class="align-center">Nombre.Plis<sup class="font-italic col-pink font-10">BIS</sup></th>
                                <th class="align-center">Plis<sup class="font-italic col-pink font-10">BIS</sup></th>
                                <th class="align-center">TOTAL pli</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdl_display_group_pli" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Regroupements disponibles</h4>
                </div>
                <div class="modal-body" id="mdl_display_group_pli_body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdl_display_group_pli_by_older" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Regroupements disponibles par date-courrier</h4>
                </div>
                <div class="modal-body" id="mdl_display_group_pli_by_older_body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdl_display_raccourci" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Raccourcis</h4>
                </div>
                <div class="modal-body table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="align-center">Touches</th>
                                <th class="align-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><kbd>Ctrl</kbd> + <kbd>Enter</kbd></td>
                                <td>Charger un pli / valider le traitement</td>
                            </tr>
                            <tr>
                                <td><kbd>Ctrl</kbd> + <kbd>backspace</kbd></td>
                                <td>Annuler le traitement</td>
                            </tr>
                            <tr>
                                <td><kbd>Alt</kbd> + <kbd>*</kbd></td>
                                <td>Recto / verso</td>
                            </tr>
                            <tr>
                                <td><kbd>Alt</kbd> + <kbd>Page-down</kbd></td>
                                <td>Document suivant</td>
                            </tr>
                            <tr>
                                <td><kbd>Alt</kbd> + <kbd>Page-up</kbd></td>
                                <td>Document pr&eacute;c&eacute;dent</td>
                            </tr>
                            <tr>
                                <td><kbd>Enter</kbd></td>
                                <td>Valider confirmation</td>
                            </tr>
                            <tr>
                                <td><kbd>Esc</kbd></td>
                                <td>Fermer le document-viewer / annuler confirmation</td>
                            </tr>
                            <tr>
                                <td><kbd>Ctrl</kbd> + <kbd>Shift</kbd></td>
                                <td>Afficher le document-viewer</td>
                            </tr>
                            <tr>
                                <td><kbd>Alt</kbd> + <kbd>+</kbd></td>
                                <td>Zoom document-viewer</td>
                            </tr>
                            <tr>
                                <td><kbd>Alt</kbd> + <kbd>-</kbd></td>
                                <td>De-zoom document-viewer</td>
                            </tr>
                            <tr>
                                <td><kbd>Ctrl</kbd> + <kbd>,</kbd></td>
                                <td>Rotation document-viewer</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FERMER</button>
                </div>
            </div>
        </div>
    </div>

    <div id="html_loader" class="hidden">
        <div class="row m-t-25">
            <div class="col-xs-12 align-center">
                <div class="preloader pl-size-xl">
                    <div class="spinner-layer pl-light-blue">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap-select/js/bootstrap-select.js?1"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap-notify/bootstrap-notify.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/node-waves/waves.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo $url_assets ?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-steps/jquery.steps.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/dropzone/dropzone.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo $url_assets ?>plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Select Plugin Js -->
    <!-- <script src="<?php echo $url_assets ?>plugins/bootstrap-select/js/bootstrap-select.js"></script> -->

    <!-- Multi Select Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- nprogress js -->
    <script src="<?php echo $url_assets; ?>plugins/nprogress/nprogress.js"></script>

    <!-- nprogress js -->
    <script src="<?php echo $url_assets; ?>plugins/magnify/jquery.magnify.js"></script>

    <!-- Morris Plugin Js >
    <script src="<?php echo base_url(); ?>template/plugins/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>template/plugins/morrisjs/morris.js"></script-->

    <!-- Demo Js -->
    <script src="<?php echo $url_assets ?>js/demo.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo $url_assets ?>js/admin.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/tables/jquery-datatable.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/ui/tooltips-popovers.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/ui/notifications.js"></script>
    <script src="<?php echo $url_assets ?>js/pages/forms/basic-form-elements.js"></script>

    <!-- my js -->
    <?php echo $additional_js; ?>
    <script>
        $(function() {
            main_init();
        });

        function main_init() {
            init_nsprogress();
            $('[data-toggle="tooltip"]').tooltip();
            $('body').on('keypress', '.no_saisi', function(e) {
                e.preventDefault();
                e.stopPropagation();
            });
            $('body').on('keypress', '.nb', function(e) {
                if (((e.charCode < 48) || (e.charCode > 57)) && (e.charCode != 0)) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
            $('body').on('keypress', '.montant', function(e) {
                if(((e.charCode<48)||(e.charCode>57))&&(e.charCode!=0)&&(e.charCode!=44)&&(e.charCode!=46)){
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
            $('body').on('keypress', '.spinner input', function(e) {
                if (((e.charCode < 48) || (e.charCode > 57)) && (e.charCode != 0)) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        }

        function init_nsprogress() {
            $(document).ajaxStart(function() {
                NProgress.start();
            });
            /*$(document).ajaxComplete(function() {
                NProgress.done();
            });
            $(document).ajaxSuccess(function() {
                NProgress.done();
            });*/
            $(document).ajaxStop(function() {
                NProgress.done();
            });
        }

        function deconnexion() {
            swal({
                title: "CONFIRMATION",
                text: "Deconnexion",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Annuler!",
                showLoaderOnConfirm: true,
                closeOnConfirm: false
            }, function() {
                window.location.href = chemin_site + '/login';
            });
        }
    </script>

    <!-- Jquery Spinner Plugin Js -->
    <script src="<?php echo $url_assets ?>plugins/jquery-spinner/js/jquery.spinner.js"></script>
    
    <?php if (isset($direct_load_pli) && $direct_load_pli): ?>
    <script>
        $(function() {
            /*$('#contenu').html('<div id="div_loader" class="row m-t-25">'
                +'<div class="col-xs-12 align-center">'
                +'<div class="preloader pl-size-xl">'
                +'<div class="spinner-layer pl-light-blue">'
                +'<div class="circle-clipper left">'
                +'<div class="circle"></div>'
                +'</div>'
                +'<div class="circle-clipper right">'
                +'<div class="circle"></div>'
                +'</div>'
                +'</div>'
                +'</div>'
                +'</div>'
                +'</div>');*/
            get_pli();
        });
    </script>	
    <?php endif; ?>

</body>

</html>
