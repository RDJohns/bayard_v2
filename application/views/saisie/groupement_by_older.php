<?php defined('BASEPATH') OR exit('No direct script access allowed');
    $small_titre_w = 'Rejet BATCH';
    if(!$only_nv1){
        $small_titre_w .= ' - Nv2';
    }
?>

<style>
    .ligne_saisie_dispo{
        cursor: pointer;
    }
</style>

<?php if (count($grps) > 0): ?>
<div class="row row_list_regroupement" id="row_list_regroupement">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th class="align-center">Acc&egrave;s</th>
                <th class="align-center">Date courrier</th>
                <th class="align-center">Soci&eacute;t&eacute;</th>
                <th class="align-center">Type de pli</th>
                <th class="align-center">Type de paiement</th>
                <th class="align-center">En attente de saisie<br><small>[<?php echo $small_titre_w ?>]</small></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($grps as $grp): ?>
            <?php $acces = in_array($grp->id_pos, $my_poss) ? "<span class=\"label label-success\">OUI</span>" : "<span class=\"label label-warning\">NON</span>"; ?>
            <?php
                $small_nb_w = $grp->nb_w_rj_cba;
                if(!$only_nv1){
                    $small_nb_w .= ' - ' . $grp->nb_n2;
                }
            ?>
            <tr class="ligne_saisie_dispo" onclick="to_filter(<?php echo $grp->id_soc.','.$grp->id_typologie.','.$grp->id_groupe_paiement ?>);">
                <td class="align-center"><?php echo $acces ?></td>
                <td><?php echo ($grp->dt_cr == '' ? '' : (new DateTime($grp->dt_cr))->format('d/m/Y')) ?></td>
                <td><?php echo $grp->soc ?></td>
                <td><?php echo $grp->typo ?></td>
                <td><?php echo $grp->paie ?></td>
                <td class="align-center"><?php echo $grp->nb_w . ' <small>['.$small_nb_w . ']</small>' ?></td>
                <?php $totaux_wait += $grp->nb_w; ?>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td class="align-center font-bold" colspan="5">TOTAL</td>
                <td class="align-center font-bold"><?php echo $totaux_wait ?></td>
            </tr>
        </tbody>
    </table>
</div>
<?php else: ?>
<div class="row">
    <div class="col-xs-12 align-center">
        <div class="alert alert-info">
            Aucun regroupement de pli disponible!
        </div>
    </div>
</div>
<?php endif; ?>

<script>
    
function to_filter(soc, typo, paie){
    if(($('#form_filtre_saisie')).length > 0){
        $('#sel_id_societe').selectpicker('val', soc);
        $('#sel_id_typologie').selectpicker('val', typo);
        $('#sel_id_gr_payement').selectpicker('val', paie);
    }
}
</script>
