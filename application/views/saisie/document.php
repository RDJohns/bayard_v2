<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-lg-12">
        
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li role="presentation" class="active tab_doc_ap"><a href="#recto" data-toggle="tab">RECTO</a></li>
            <li role="presentation" class="tab_doc_ap"><a href="#verso" data-toggle="tab" onclick="$('#recto').removeClass('bounceInUp').addClass('flipInY');">VERSO</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane animated bounceInUp active" id="recto">
                <div class="div_img_b_64">
                    <img src="data:image/jpeg;base64,<?php echo $doc_b64_recto; ?>" class="img_b64" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$doc->id_pli.' / Doc#'.$doc->id_document.' / RECTO'; ?>" data-group="<?php echo $doc->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $doc_b64_recto; ?>" />
                </div>
            </div>
            <div role="tabpanel" class="tab-pane animated flipInY" id="verso">
                <div class="div_img_b_64">
                    <img src="data:image/jpeg;base64,<?php echo $doc_b64_verso; ?>" class="img_b64" data-magnify="gallery" data-caption="<?php echo 'Pli#'.$doc->id_pli.' / Doc#'.$doc->id_document.' / VERSO'; ?>" data-group="<?php echo $doc->id_document; ?>" data-src="data:image/jpeg;base64,<?php echo $doc_b64_verso; ?>" />
                </div>
            </div>
        </div>

    </div>
</div>
