<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row m-t-30"></div>

<?php if ($have_lot_saisie): ?>
<div class="row">
    <div class="col-lg-12">
        <a href="<?php echo site_url('saisie/saisie') ?>" type="button" class="btn bg-green btn-circle waves-effect waves-circle waves-float" title="Revenir">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>
</div>
<?php endif; ?>

<?php if (!empty($msg_erreur)): ?>
    <div class="row">
        <div class="col-lg-offset-2 col-lg-8 align-center">
            <div class="alert bg-orange">
                <?php echo $msg_erreur ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-xs-12 align-center">
        <button type="button" class="btn bg-blue btn-lg waves-effect" onclick="get_lot_saisie_pli();">
            <i class="material-icons">content_paste</i>
            <span>Commencer un lot de saisie</span>
        </button>
    </div>
</div>
<div class="row align-center">
    <h2>
        <small>Identifiant du lot saisie: </small>
        <span id="id_lot_saisie"><?php echo $id_lot_saisie ?></span>&nbsp;
        <button type="button" class="btn btn-default waves-effect" onclick="copyToClipboard('<?php echo $id_lot_saisie ?>');">
            <i class="material-icons">content_copy</i>Copier
        </button>
    </h2>
</div>

<div class="row m-t-25">
    <div class="col-lg-12">
        <div class="card">
            <div class="header">
                <h2>
                    Filtres <small>Sp&eacute;cification du pli</small>
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:reset_filtre_pli();">Reinitialiser</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row" id="form_filtre_saisie">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <p><b>Soci&eacute;t&eacute;:</b></p>
                        <select class="form-control show-tick sel_field" id="sel_id_societe" title="S&eacute;lectionnez" data-container="body">
                            <?php foreach ($list_grp_pli_pos_soc as $societe): ?>
                                <option value="<?php echo $societe->id_soc ?>" <?php echo ($societe->id_soc == $current_societe ? 'selected' : '') ?> ><?php echo $societe->soc ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <p><b>Type de pli:</b></p>
                        <select class="form-control show-tick sel_field" id="sel_id_typologie" title="S&eacute;lectionnez"  data-live-search="true" data-container="body" data-size="5">
                            <?php foreach ($list_grp_pli_pos_typologie as $type): ?>
                                <option value="<?php echo $type->id_typologie ?>" <?php echo ($type->id_typologie == $current_typologie ? 'selected' : '') ?> ><?php echo $type->typologie ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <p><b>Type de paiement:</b></p>
                        <select class="form-control show-tick sel_field" id="sel_id_gr_payement" title="S&eacute;lectionnez" data-container="body">
                            <?php foreach ($list_grp_pli_pos_gr_payement as $paiement): ?>
                                <option value="<?php echo $paiement->id_groupe_paiement ?>" <?php echo ($paiement->id_groupe_paiement == $current_gr_payement ? 'selected' : '') ?> ><?php echo $paiement->groupe_paiement ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 align-center">
                        <input type="checkbox" id="rejet_batch_only" class="filled-in" <?php echo ($rejet_batch ? 'checked' : '') ?> />
                        <label for="rejet_batch_only">Rejet&eacute; BATCH</label>
                    </div>
                <div/>
                <?php if ($no_pli): ?>
                <div class="row">
                    <div class="col-xs-12 align-center">
                        <div class="alert bg-orange">
                            Aucun pli disponible pour vous avec cette sp&eacute;cification!
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div id="div_loader" class="row m-t-25 cacher">
    <div class="col-xs-12 align-center">
        <div class="preloader pl-size-xl">
            <div class="spinner-layer pl-light-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function copyToClipboard(text){
        var dummy = document.createElement("input");
        document.body.appendChild(dummy);
        dummy.setAttribute('value', text);
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
        //showNotification('alert-info', '<i class="material-icons">content_copy</i> Info: "'.text.'" copi&eacute;.', 'bottom', 'right', null, null);
    }
</script>
