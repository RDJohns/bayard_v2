<div class="container-fluid" style="margin-top: 100px">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Ch&egrave;ques KO VIPS</h2>
                </div>
                <div class="body">

                    <form id="search-form" method="post" enctype="multipart/form-data">
                        <div class="row clearfix">
                            <div class="col-md-3">
                                <select class="form-control show-tick" id="id_societe" name="id_societe">
                                    <option value="">--- Choisir soci&eacute;t&eacute; ---</option>
                                    <?php foreach ($societe as $soc): ?>
                                        <option value="<?= $soc->id ?>"><?= $soc->nom_societe ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control show-tick" id="id_etat" name="id_etat">
                                    <option value="">--- Choisir statut ch&egrave;que ---</option>
                                    <?php foreach ($etat as $item): ?>
                                        <option value="<?= $item->id_etat_chq ?>"><?= $item->lbl_etat_chq ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input class="form-control" type="text" id="cmc-txt" name="cmc-txt" placeholder="CMC7"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="button" id="etat-search" onclick="search_chq()" class="btn btn-primary waves-effect" value="Rechercher" />

                    </form>
                    <br />

                    <div class="table-responsive">
                        <table id="table-chq" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Date r&eacute;ception</th>
                                <th>ID Pli</th>
                                <th>Pli</th>
                                <th>Societe</th>
                                <th>Lot scan</th>
                                <th>Etape</th>
                                <th>Etat</th>
                                <th>Statut</th>
                                <th>Num&eacute;ro payeur</th>
                                <th>Nom payeur</th>
                                <th>CMC7</th>
                                <th>Montant</th>
                                <th>Date<br>initiale Reb</th>
                                <th>Statut ch&egrave;que</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
