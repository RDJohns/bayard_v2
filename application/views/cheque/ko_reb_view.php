<div class="modal fade" id="payeurModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="width: 350px;">
        <div class="modal-content">
            <div class="modal-header bg-teal">
                <h4 class="modal-title" id="smallModalLabel">Modification soci&eacute;t&eacute;/num&eacute;ro payeur/montant</h4>
                <h5 >Pli #<span id="pli_id"></span></h5>
                <input type="hidden" id="pli_id_form" value="">
                <input type="hidden" id="chq_id_form" value="">
            </div>
            <div class="modal-body">
                <h6 class="card-inside-title">Soci&eacute;t&eacute;</h6>
                <select class="form-control" id="societe-form">
                    <?php
                        if($societe)
                        foreach($societe as $soc){
                            echo '<option value="'.$soc->id.'">'.$soc->nom_societe.'</option>';
                        }
                    ?>
                </select>
                <h6 class="card-inside-title">CMC7</h6>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" class="form-control" placeholder="CMC7" id="cmc7-form" value="" disabled>
                    </div>
                </div>
                <h6 class="card-inside-title">Montant</h6>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" class="form-control" placeholder="montant" id="montant-form" value="">
                    </div>
                </div>
                <div id="content-payeur">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn waves-effect bg-cyan" onclick="save_modif_payeur()">OK</button>
                <button type="button" class="btn waves-effect bg-amber" data-dismiss="modal">ANNULER</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="margin-top: 100px">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Recherche</h2>
                </div>
                <div class="body">
                    <form id="search-form" method="post" enctype="multipart/form-data">
                        <div class="row clearfix">
                            <div class="col-md-3">
                                <select class="form-control show-tick" id="id_societe" name="id_societe">
                                    <option value="">--- Choisir soci&eacute;t&eacute; ---</option>
                                    <?php foreach ($societe as $soc): ?>
                                        <option value="<?= $soc->id ?>"><?= $soc->nom_societe ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <!--div class="col-md-3">
                                <select class="form-control show-tick" id="id_etat" name="id_etat">
                                    <option value="">--- Choisir statut ch&egrave;que ---</option>
                                    <?php foreach ($etat as $item): ?>
                                        <option value="<?= $item->id_etat_chq ?>"><?= $item->lbl_etat_chq ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div-->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input class="form-control" type="text" id="cmc-txt" name="cmc-txt" placeholder="CMC7"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="button" id="etat-search" onclick="search_chq()" class="btn btn-primary waves-effect" value="Rechercher" />

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Ch&egrave;ques KO GED</h2>
                </div>
                <div class="body">

                    <div class="table-responsive">
                        <table id="table-chq" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Date r&eacute;ception</th>
                                <th>ID Pli</th>
                                <th>Pli</th>
                                <th>Societe</th>
                                <th>Lot scan</th>
                                <th>Etape</th>
                                <th>Etat</th>
                                <th>Statut</th>
                                <th>Num&eacute;ro payeur</th>
                                <th>Nom payeur</th>
                                <th>CMC7</th>
                                <th>Montant</th>
                                <th>Date<br>initiale Reb</th>
                                <th>Statut ch&egrave;que</th>
                                <!--th>Libellé<br>KO REB</th-->
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Ch&egrave;ques KO ADVANTAGE</h2>
                </div>
                <div class="body">
                    <br />

                    <div class="table-responsive">
                        <table id="table-adv" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Date extraction</th>
                                <th>Soci&eacute;t&eacute;</th>
                                <th>Num&eacute;ro paiement</th>
                                <th>Montant</th>
                                <th>Num&eacute;ro payeur</th>
                                <th>Nom payeur</th>
                                <th>Statut</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
