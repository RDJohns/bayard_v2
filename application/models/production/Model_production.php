<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_production extends CI_Model{

    private $CI;

    private $ged;
    private $base_64;
    protected $select_reb        = array('date_courrier','lot_scan','pli','code_type_pli','lib_type','statut','type_ko','montant','cmc7','date_encaissement','id_pli','id_doc');
    protected $column_order_reb  = array('date_courrier','lot_scan','pli','code_type_pli','lib_type','statut','type_ko','montant','cmc7','date_encaissement','id_pli','id_doc');
    protected $column_search_reb = array('date_courrier','lot_scan','pli','lib_type');
    protected $order_reb         = array('date_courrier'=> 'asc','lot_scan'=> 'asc','pli'=> 'asc','lib_type'=> 'asc');

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }
	
	public function get_dates($date_debut, $date_fin){
		$sql = "SELECT liste_dates('".$date_debut."','".$date_fin."') as daty";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_dates_months($date_debut, $date_fin){
		$sql = "SELECT substr(liste_months::text,1,7) as date_mth FROM liste_months ('".$date_debut."','".$date_fin."')";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_week_of_date($date_debut){
		$sql = "SELECT extract('week' from '".$date_debut."'::date) as week ";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_month_of_date($date_debut){
		$sql = "SELECT concat(EXTRACT(YEAR FROM TIMESTAMP '".$date_debut."'),'-',EXTRACT(MONTH FROM TIMESTAMP '".$date_debut."')) as mois;";
		return $this->ged->query($sql)
							->result_array();
	}
	
	public function get_multi_cheque($date_debut, $date_fin){
		
		$where_like = $where_view_pli = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " and (date_courrier between '".$date_debut."' and '".$date_fin."' )";
				$where_view_pli .= " and (view_lot.date_courrier  between '".$date_debut."' and '".$date_fin."') ";
        }
		
				
		$sql = "
				SELECT
					data_plis.pli,
					data_plis.id_pli, 
					data_plis.typologie,
					type_ko ,
					flag_traitement,
					traitement,
					dt_event,
					data_plis.typage_par,
					lot_scan,
					date_courrier,
					date_numerisation,
					statut, 				
					code_type_pli,
					comment,
					libelle_motif,
					txt_anomalie,
					nb_doc,
					data_plis.id_pli,
					data_plis.id_document,
					data_plis.type_document,
					data_plis.lib_type,
					cmc7, 
					montant,
					montant_chco
				FROM
				(SELECT 
				view_pli.pli,
				view_pli.id_pli, 
				view_pli.typologie,
				case when (flag_traitement = 3 ) then 'KO inconnu' else 
				case when (flag_traitement = 4 ) then 'KO scan' else 
				case when (flag_traitement = 5 ) then 'KO réclammation' else 
				case when (flag_traitement = 6 ) then 'KO litige' else 
				case when (flag_traitement = 8 ) then 'KO Définitif' else 
				case when (flag_traitement = 9 ) then 'KO Call' else 
				case when (flag_traitement = 11 ) then 'KO Circulaire' else 
				case when (flag_traitement = 12 ) then 'KO En attente' else ''
				end end end end end end end end type_ko ,
				flag_traitement,
				traitement,
				dt_event::date,
				view_pli.typage_par,
				lot_scan,
				date_courrier::date,
				date_numerisation,
				case when (flag_traitement is null) or (flag_traitement = 0 and view_pli.typage_par is null) then 'Non traité' else 
				case when (flag_traitement = 0 and view_pli.typage_par is not null)  then 'En cours de typage' else
				case when (flag_traitement = 1 and view_pli.traite_par is null)  then 'Typé' else 
				case when (flag_traitement = 1 and view_pli.traite_par is not null)  then 'En cours de saisie' else 
				case when (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation < 3 and team_pli = 1)  then 'En cours de controle' else 
				case when (team_pli = 2 and flag_traitement = 2) or (flag_avec_bdc = 0 and flag_traitement = 2  and team_pli = 1) or (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation = 3 and team_pli = 1) then 'Cloturé' else 
				case when (flag_traitement = 3 or flag_traitement = 4 or flag_traitement = 5 or flag_traitement = 6 or flag_traitement = 8 or flag_traitement = 9 or flag_traitement = 11 or flag_traitement = 12)
				then 'Anomalie' else '' end end end end end end end as statut, 				
				case when code_type_pli is null then 'NT' else code_type_pli end as code_type_pli,
				case when comment is null then 'Non typé' else comment end as comment,
				libelle_motif,txt_anomalie,
				count(id_document) as nb_doc,
				/*code_type_pli,*/
				view_document.id_document,
				view_document.type_document,
				view_document.lib_type 

				from 
				( 
						SELECT distinct get_max_oid_pli_par_pli.id_pli,get_max_oid_pli_par_pli.oid,flag_traitement,traitement,dt_event
						from get_max_oid_pli_par_pli() 
						inner join histo_pli on histo_pli.oid = get_max_oid_pli_par_pli.oid
						left join view_pli on view_pli.id_pli = histo_pli.id_pli
						left join view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
						inner join view_flag_traitement on view_flag_traitement.id_flag_traitement = histo_pli.flag_traitement
						where 1 = 1  ".$where_like."				
				)
				as traitement
				right join view_pli on view_pli.id_pli = traitement.id_pli
				right join view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
				right join view_document on view_pli.id_pli = view_document.id_pli
				where 1=1 ".$where_view_pli." and view_pli.typologie in (6,19,32) and view_document.type_document in (2)
				group by view_pli.pli,view_pli.id_pli,flag_traitement,traitement,view_pli.typage_par,view_pli.traite_par,
				lot_scan,date_courrier,date_numerisation,dt_event::date,view_pli.typologie,code_type_pli,
				comment,libelle_motif,txt_anomalie,flag_avec_bdc,flag_validation,code_type_pli,view_document.id_document,  view_document.type_document,view_document.lib_type,team_pli
				) as data_plis
				LEFT JOIN
				(
				SELECT tab.id_pli,
						  id_doc, 
						  cmc7, 
						  montant,
						  montant_chco,
						  max_oid.oid as oid1
				FROM
				 (
						  SELECT 
						  ident_doc.id_pli,
						  ident_doc.id_doc, 
						  montant,
						  montant_chco,
						  cmc7
						  FROM
						  (
						  
						  SELECT distinct id_document as id_doc,view_pli.id_pli,n_ima_recto, n_ima_verso,code_type_pli,flag_ttt
						  FROM view_pli 
						  left join view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
						  left join view_document on view_document.id_pli = view_pli.id_pli
						  left join data_document on data_document.id_doc = view_document.id_document and id_champ = 2
						  inner join histo_pli on view_pli.id_pli = histo_pli.id_pli  
						  inner join get_max_oid_pli_par_pli() on get_max_oid_pli_par_pli.id_pli = histo_pli.id_pli
						  where 1=1   ".$where_like."     
						  ) as ident_doc left join
						  (

								  SELECT distinct id_document  as id_doc,view_pli.id_pli,
								  valeur as cmc7
								  FROM view_pli 
								  left join view_lot on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
								  left join view_document on view_document.id_pli = view_pli.id_pli
								  left join data_document on data_document.id_doc = view_document.id_document and id_champ = 6
								  inner join histo_pli on view_pli.id_pli = histo_pli.id_pli  
								  inner join get_max_oid_pli_par_pli() on get_max_oid_pli_par_pli.id_pli = histo_pli.id_pli
								  where 1=1   ".$where_like." 
								  
						
						  ) as cmc7 on ident_doc.id_doc = cmc7.id_doc and ident_doc.id_pli = cmc7.id_pli
						  left join 
						  (
								  SELECT distinct id_document  as id_doc,view_pli.id_pli,
								  valeur as montant
								  FROM view_pli 
								  left join view_lot on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
								  left join view_document on view_document.id_pli = view_pli.id_pli
								  left join data_document on data_document.id_doc = view_document.id_document and id_champ = 10
								  inner join histo_pli on view_pli.id_pli = histo_pli.id_pli  
								  inner join get_max_oid_pli_par_pli() on get_max_oid_pli_par_pli.id_pli = histo_pli.id_pli
								  where 1=1 ".$where_like." 
								  
						  ) as montant on ident_doc.id_doc = montant.id_doc and ident_doc.id_pli = montant.id_pli
						  left join 
						  (
						  
								  SELECT distinct id_document as id_doc,view_pli.id_pli,
								  valeur as montant_chco
								  FROM view_pli 
								  left join view_lot on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
								  left join view_document on view_document.id_pli = view_pli.id_pli
								  left join data_document on data_document.id_doc = view_document.id_document and id_champ = 14
								  inner join histo_pli on view_pli.id_pli = histo_pli.id_pli  
								  inner join get_max_oid_pli_par_pli() on get_max_oid_pli_par_pli.id_pli = histo_pli.id_pli
								  where 1=1   ".$where_like." 
								  
						  ) as montant_chco  on ident_doc.id_doc = montant_chco.id_doc and ident_doc.id_pli = montant_chco.id_pli
						  
					  ) as tab
				left join get_max_oid_pli_par_pli() as max_oid on max_oid.id_pli = tab.id_pli
				)as data_document on data_plis.id_pli = data_document.id_pli and data_plis.id_document = data_document.id_doc
				order by date_courrier, date_numerisation, lot_scan,data_plis.id_pli, id_doc asc";		
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";*/
		return $this->ged->query($sql)
							->result_array();
    
    }
    
	public function get_typage($dates,$typologie){
		
		$where_like = $where_view_pli = "";
		$query      = "";
		$date_col   = "";
		$col = "";
		foreach($dates as $kdaty=>$vdaty ){  
			$daty     = $vdaty['daty'];
			$date_col = str_replace("-","",$daty);
			$col .= ", nb_".$date_col ;
			$where_like.= " and date_courrier= '".$daty."'";
			$query .= " LEFT JOIN(				
				SELECT count(distinct get_max_oid_pli_par_pli.id_pli) as nb_".$date_col." ,1 flag,
				date_courrier::date date_courrier
				from get_max_oid_pli_par_pli() 
				inner join histo_pli on histo_pli.oid = get_max_oid_pli_par_pli.oid
				inner join view_pli on view_pli.id_pli = histo_pli.id_pli
				inner join view_lot on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
				inner join view_flag_traitement on view_flag_traitement.id_flag_traitement = histo_pli.flag_traitement
				where (date_courrier = '".$daty."' )
				and view_pli.typologie in (".$typologie.")
				group by date_courrier::date,flag
			) data_plis_".$date_col." ON typo.flag = data_plis_".$date_col.".flag";	
        }		
				
		$sql = "select typologie".$col."
				from ( select '".$typologie."' as typologie,1 flag
				)typo
				".$query;		
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";exit;*/
		return $this->ged->query($sql)
							->result_array();
    
    }
   public function get_data_print(){
		
						
		$sql = "SELECT 
					id, 
					libelle_wf, 
					type_saisie, 
					libelle_categorie, 
					composition_pli 
				FROM public.data_print
				WHERE composition_pli is not null order by id asc";		
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";exit;*/
		return $this->ged->query($sql)
							->result_array();
    
    }
	public function get_list_plis($date_debut, $date_fin){
		
		$where_like = $where_view_pli = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
				$where_view_pli .= " view_lot.date_courrier  between '".$date_debut."' and '".$date_fin."' ";
        }
		$sql = "SELECT 
			traitement.pli,
			traitement.id_pli, 
			typologie,
			case when (flag_traitement = 3 ) then 'KO inconnu' else 
			case when (flag_traitement = 4 ) then 'KO scan' else 
			case when (flag_traitement = 5 ) then 'KO réclammation' else 
			case when (flag_traitement = 6 ) then 'KO litige' else 
			case when (flag_traitement = 8 ) then 'KO Définitif' else 
			case when (flag_traitement = 9 ) then 'KO Call' else
			case when (flag_traitement = 11 ) then 'KO Ciculaire' else
			case when (flag_traitement = 12 ) then 'KO en attente' else ''
			end end end end end end end end type_ko ,
			flag_traitement,
			traitement,
			dt_event::date,
			typage_par,
			traitement.lot_scan,
			date_courrier::date,
			date_numerisation,
			case when (flag_traitement is null) or (flag_traitement = 0 and typage_par is null) then 'Non traité' else 
			case when (flag_traitement = 0 and typage_par is not null and typage_ok = 0)  then 'En cours de typage' else
			case when (traitement.flag_traitement = 0 and traite_par is null and  typage_ok = 1) or (flag_traitement = 1 and traite_par is null)  then 'Typé' else 
			case when (flag_traitement = 1 and traite_par is not null)  then 'En cours de saisie' else 
			case when (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation < 3 and team_pli = 1)   then 'En cours de controle' else 
			case when (team_pli = 2 and flag_traitement = 2) or (flag_avec_bdc = 0 and flag_traitement = 2  and team_pli = 1) or (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation = 3 and team_pli = 1)  then 'Cloturé' else 
			case when (flag_traitement = 3 or flag_traitement = 4 or flag_traitement = 5 or flag_traitement = 6 or flag_traitement = 8 or flag_traitement = 9 or flag_traitement = 11 or flag_traitement = 12)
			then 'Anomalie' else '' end end end end end end end as statut, 				
			case when code_type_pli is null then 'NT' else code_type_pli end as code_type_pli,
			case when comment is null then 'Non typé' else comment end as comment,
			count(id_document) as nb_doc
			from 
			( 
				SELECT tab1.id_pli,flag_traitement,traitement,dt_event,pli,typologie,typage_par,typage_ok,team_pli,lot_scan,date_courrier,date_numerisation,traite_par,flag_avec_bdc,flag_validation,code_type_pli,comment
				
				FROM
				(
					SELECT histo_pli.id_pli,max(histo_pli.oid) oid
					from histo_pli 
					left join view_pli on view_pli.id_pli = histo_pli.id_pli
					left join view_lot on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
					where histo_pli.id_pli in (select distinct view_pli.id_pli from view_pli inner join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  WHERE ".$where_like.")
					group by histo_pli.id_pli
						
				) tab1 inner join
				(
					SELECT histo_pli.id_pli,histo_pli.oid,flag_traitement,traitement,dt_event,view_pli.pli,view_pli.typologie,view_pli.typage_par,view_pli.typage_ok,view_pli.traite_par,view_pli.team_pli,view_lot.lot_scan,view_lot.date_courrier::date
					,view_lot.date_numerisation::date,view_pli.flag_avec_bdc,view_pli.flag_validation,code_type_pli,comment
					from histo_pli 
					left join view_flag_traitement on view_flag_traitement.id_flag_traitement = histo_pli.flag_traitement
					left join view_pli on view_pli.id_pli = histo_pli.id_pli
					left join view_lot on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
					where histo_pli.id_pli in (select distinct view_pli.id_pli from view_pli inner join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  WHERE ".$where_like." )
					
				)tab2 on tab1.oid = tab2.oid
			
			)
			as traitement
			
			right join view_document on traitement.id_pli = view_document.id_pli
			group by pli,traitement.id_pli,flag_traitement,traitement,typage_par,traite_par,
			lot_scan,date_courrier,date_numerisation,dt_event,typologie,code_type_pli,
			comment,typage_ok,flag_avec_bdc,flag_validation,team_pli
			order by date_courrier::date, date_numerisation, lot_scan,traitement.id_pli asc";		
				
		return $this->ged->query($sql)
							->result_array();
    }
	
	
	public function get_min_date_courrier($date_debut, $date_fin){
		
		$where_like = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	= " dt_event::date between '".$date_debut."' and '".$date_fin."' ";
				
        }
		$sql = "SELECT min (date_courrier::date) as date_courrier
			from histo_pli 
			left join view_flag_traitement on view_flag_traitement.id_flag_traitement = histo_pli.flag_traitement 
			left join view_pli on view_pli.id_pli = histo_pli.id_pli 
			left join view_lot on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
			where ".$where_like."
			";
		return $this->ged->query($sql)
							->result_array();	
	}
	public function get_dates_ttt($date_debut, $date_fin){
		$where_like = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	= " and date_courrier::date between '".$date_debut."' and '".$date_fin."' ";
				
        }
		$sql = "SELECT min(dt_event::date) date_min_ttt, max(dt_event::date) date_max_ttt
				FROM view_pli 
				left join view_lot on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
				inner join histo_pli on view_pli.id_pli = histo_pli.id_pli  
				where 1=1 ".$where_like."
			";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_datatables_reb($where_date,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatables_reb_sql($where_date,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->ged->limit($length, $start);
        $query = $this->ged->get();
        return $query->result();
    }

    public function count_filtered_reb($where_date,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatables_reb_sql($where_date,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->ged->get();
        return $query->num_rows();
		
    }

    public function count_all_reb($where_date)
    { 
       
        $this->ged->select($this->select_reb)
                ->from("(SELECT
					date_courrier,lot_scan,pli,code_type_pli,lib_type,montant,cmc7,result.id_pli,id_doc,	
					case when (flag_traitement is null) or (flag_traitement = 0 and typage_par is null) then 'Non traité' else 
					case when (flag_traitement = 0 and typage_par is not null and typage_ok = 0)  then 'En cours de typage' else
					case when (flag_traitement = 0 and traite_par is null and  typage_ok = 1) or (flag_traitement = 1 and traite_par is null)  then 'Typé' else 
					case when (flag_traitement = 1 and traite_par is not null)  then 'En cours de saisie' else 
					case when (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation < 3 and team_pli = 1)  then 'En cours de controle' else 
					case when (team_pli = 2 and flag_traitement = 2) or (flag_avec_bdc = 0 and flag_traitement = 2  and team_pli = 1) or (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation = 3 and team_pli = 1) then 'Cloturé' else 
					case when (flag_traitement = 3 or flag_traitement = 4 or flag_traitement = 5 or flag_traitement = 6 or flag_traitement = 8 or flag_traitement = 9 or flag_traitement = 11 or flag_traitement = 12)
					then 'Anomalie' else '' end end end end end end end as statut,
					case when (flag_traitement = 3 ) then 'KO inconnu' else 
					case when (flag_traitement = 4 ) then 'KO scan' else 
					case when (flag_traitement = 5 ) then 'KO réclammation' else 
					case when (flag_traitement = 6 ) then 'KO litige' else 
					case when (flag_traitement = 8 ) then 'KO Définitif' else 
					case when (flag_traitement = 9 ) then 'KO Call' else 
					case when (flag_traitement = 11 ) then 'KO Circulaire' else 
					case when (flag_traitement = 12 ) then 'KO En attente' else ''
					end end end end end end end end type_ko,date_encaissement
					FROM
					(
						SELECT distinct substr(view_lot.date_courrier::text,1,11) as 		      date_courrier,view_lot.lot_scan,view_pli.pli,code_type_pli,lib_type, 
						 data_document.valeur as montant,ddoc.valeur as cmc7,id_document  as id_doc,get_max_oid_pli_par_pli.id_pli,
						 view_pli.typage_par,view_pli.typage_ok,view_pli.traite_par,view_pli.flag_avec_bdc,view_pli.flag_validation,view_pli.team_pli,view_pli.flag_ttt flag_traitement,substr(ddoc_chq.valeur::text,1,11) as date_encaissement
						  FROM view_pli 
						  left join view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
						  left join view_document on view_document.id_pli = view_pli.id_pli
						  left join data_document on data_document.id_doc = view_document.id_document and id_champ = 10
						  left join data_document ddoc on ddoc.id_doc = view_document.id_document and ddoc.id_champ = 6
						  left join data_document ddoc_chq on ddoc_chq.id_doc = view_document.id_document and ddoc_chq.id_champ = 8
						  inner join histo_pli on view_pli.id_pli = histo_pli.id_pli  
						  inner join get_max_oid_pli_par_pli() on get_max_oid_pli_par_pli.id_pli = histo_pli.id_pli
						  where date_tlmc is null and type_document = 2
						  ".$where_date."
						   ) as result
					  ) as tab
                         
                        ");
        return $this->ged->count_all_results();
		
    }

    public function get_datatables_reb_sql($where_date,$post_order,$search ,$post_order_col,$post_order_dir){

         $this->ged->select($this->select_reb)
                 ->from("(SELECT
					date_courrier,lot_scan,pli,code_type_pli,lib_type,montant,cmc7,result.id_pli,id_doc,	
					case when (flag_traitement is null) or (flag_traitement = 0 and typage_par is null) then 'Non traité' else 
					case when (flag_traitement = 0 and typage_par is not null and typage_ok = 0)  then 'En cours de typage' else
					case when (flag_traitement = 0 and traite_par is null and  typage_ok = 1) or (flag_traitement = 1 and traite_par is null)  then 'Typé' else 
					case when (flag_traitement = 1 and traite_par is not null)  then 'En cours de saisie' else 
					case when (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation < 3 and team_pli = 1)  then 'En cours de controle' else 
					case when (team_pli = 2 and flag_traitement = 2) or (flag_avec_bdc = 0 and flag_traitement = 2  and team_pli = 1) or (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation = 3 and team_pli = 1) then 'Cloturé' else 
					case when (flag_traitement = 3 or flag_traitement = 4 or flag_traitement = 5 or flag_traitement = 6 or flag_traitement = 8 or flag_traitement = 9 or flag_traitement = 11 or flag_traitement = 12)
					then 'Anomalie' else '' end end end end end end end as statut,
					case when (flag_traitement = 3 ) then 'KO inconnu' else 
					case when (flag_traitement = 4 ) then 'KO scan' else 
					case when (flag_traitement = 5 ) then 'KO réclammation' else 
					case when (flag_traitement = 6 ) then 'KO litige' else 
					case when (flag_traitement = 8 ) then 'KO Définitif' else 
					case when (flag_traitement = 9 ) then 'KO Call' else 
					case when (flag_traitement = 11 ) then 'KO Circulaire' else 
					case when (flag_traitement = 12 ) then 'KO En attente' else ''
					end end end end end end end end type_ko,date_encaissement
					FROM
					(
						SELECT distinct substr(view_lot.date_courrier::text,1,11) as 		      date_courrier,view_lot.lot_scan,view_pli.pli,code_type_pli,lib_type, 
						 data_document.valeur as montant,ddoc.valeur as cmc7,id_document  as id_doc,get_max_oid_pli_par_pli.id_pli,
						 view_pli.typage_par,view_pli.typage_ok,view_pli.traite_par,view_pli.flag_avec_bdc,view_pli.flag_validation,view_pli.team_pli,view_pli.flag_ttt flag_traitement,substr(ddoc_chq.valeur::text,1,11)  as date_encaissement
						  FROM view_pli 
						  left join view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
						  left join view_document on view_document.id_pli = view_pli.id_pli
						  left join data_document on data_document.id_doc = view_document.id_document and id_champ = 10
						  left join data_document ddoc on ddoc.id_doc = view_document.id_document and ddoc.id_champ = 6
						  left join data_document ddoc_chq on ddoc_chq.id_doc = view_document.id_document and ddoc_chq.id_champ = 8
						  inner join histo_pli on view_pli.id_pli = histo_pli.id_pli  
						  inner join get_max_oid_pli_par_pli() on get_max_oid_pli_par_pli.id_pli = histo_pli.id_pli
						  where date_tlmc is null and type_document = 2
						  ".$where_date."
						  ) as result
					  ) as tab
                         ");

        $i = 0;
        foreach ($this->column_search_reb as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->like($item, $search);
                }
                else
                {
                    $this->ged->or_like($item, $search);
                }

                if(count($this->column_search_reb) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->ged->order_by($this->column_order_reb[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_reb))
        {
            $order = $this->order_reb;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
    }
	public function get_reb($date_debut,$date_fin){
		
		$where_date  = "";
		if($date_debut != '' && $date_fin != ''){
				$where_date 	.= " and (date_courrier between '".$date_debut."' and '".$date_fin."' )";
		 }
		$sql = " SELECT
				date_courrier,lot_scan,pli,code_type_pli,lib_type,montant,cmc7,result.id_pli,id_doc,	
				case when (flag_traitement is null) or (flag_traitement = 0 and typage_par is null) then 'Non traité' else 
				case when (flag_traitement = 0 and typage_par is not null and typage_ok = 0)  then 'En cours de typage' else
				case when (flag_traitement = 0 and traite_par is null and  typage_ok = 1) or (flag_traitement = 1 and traite_par is null)  then 'Typé' else 
				case when (flag_traitement = 1 and traite_par is not null)  then 'En cours de saisie' else 
				case when (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation < 3 and team_pli = 1)  then 'En cours de controle' else 
				case when (team_pli = 2 and flag_traitement = 2) or (flag_avec_bdc = 0 and flag_traitement = 2  and team_pli = 1) or (flag_avec_bdc = 1 and flag_traitement = 2 and flag_validation = 3 and team_pli = 1) then 'Cloturé' else 
				case when (flag_traitement = 3 or flag_traitement = 4 or flag_traitement = 5 or flag_traitement = 6 or flag_traitement = 8 or flag_traitement = 9 or flag_traitement = 11 or flag_traitement = 12)
				then 'Anomalie' else '' end end end end end end end as statut,
				case when (flag_traitement = 3 ) then 'KO inconnu' else 
				case when (flag_traitement = 4 ) then 'KO scan' else 
				case when (flag_traitement = 5 ) then 'KO réclammation' else 
				case when (flag_traitement = 6 ) then 'KO litige' else 
				case when (flag_traitement = 8 ) then 'KO Définitif' else 
				case when (flag_traitement = 9 ) then 'KO Call' else 
				case when (flag_traitement = 11 ) then 'KO Circulaire' else 
				case when (flag_traitement = 12 ) then 'KO En attente' else ''
				end end end end end end end end type_ko,
				date_encaissement
				FROM
				(
					SELECT distinct view_lot.date_courrier::date,view_lot.lot_scan,view_pli.pli,code_type_pli,lib_type, 
					data_document.valeur as montant,ddoc.valeur as cmc7,id_document  as id_doc,get_max_oid_pli_par_pli.id_pli, view_pli.typage_par,view_pli.typage_ok,view_pli.traite_par,view_pli.flag_avec_bdc,view_pli.flag_validation,
					view_pli.team_pli,view_pli.flag_ttt flag_traitement,ddoc_chq.valeur as date_encaissement
				  FROM view_pli 
				  left join view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
				  left join view_document on view_document.id_pli = view_pli.id_pli
				  left join data_document on data_document.id_doc = view_document.id_document and id_champ = 10
				  left join data_document ddoc on ddoc.id_doc = view_document.id_document and ddoc.id_champ = 6
				  left join data_document ddoc_chq on ddoc_chq.id_doc = view_document.id_document and ddoc_chq.id_champ = 8
				  inner join histo_pli on view_pli.id_pli = histo_pli.id_pli  
				  inner join get_max_oid_pli_par_pli() on get_max_oid_pli_par_pli.id_pli = histo_pli.id_pli
				  where date_tlmc is null and type_document = 2
				  ".$where_date."
				  ) as result
				  order by date_courrier , lot_scan asc ";	       		
		
				
		return $this->ged->query($sql)
							->result_array();
    
    }
}
