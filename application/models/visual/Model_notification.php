<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_notification extends CI_Model{
	
	private $CI;

	private $ged;
    private $base_64;
    
    private $tb_pli = TB_pli;
    private $tb_lotNum = TB_lotNumerisation;
    private $tb_suiviNum = TB_suiviNum;
    private $ftb_suiviNum = FTB_suiviNum;
    private $tb_suiviMail = TB_suiviMail;

    private $dt_today;
    private $dt_inf;
    
	public function __construct() {
		parent::__construct();
		$this->CI =& get_instance();
		$this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->dt_today = new DateTime();
        $this->dt_inf = new DateTime();
        $this->dt_inf->modify('-1 day');
    }

    public function num_vs_ged_courrier()
    {
        $datas = $this->nb_pli_courrier_num_vs_ged();
        $data_rep = array();
        $dt_index = new DateTime($this->dt_today->format('Y-m-d'));
        while ($dt_index->format('Ymd') >= $this->dt_inf->format('Ymd')) {
            $current_data = NULL;
            foreach ($datas as $key => $data) {
                $date = new DateTime($data->dt_num);
                if($date->format('Ymd') == $dt_index->format('Ymd')){
                    $current_data = $data;
                }
            }
            if(is_null($current_data)){
                $current_data = new stdClass();
                $current_data->dt_num = $dt_index->format('Y-m-d');
                $current_data->nb_num = 0;
                $current_data->nb_int = 0;
            }
            $date = new DateTime($current_data->dt_num);
            $day = array(
                'date' => $date
                ,'nb_num' => is_numeric($current_data->nb_num) ? $current_data->nb_num : 0
                ,'nb_pli' => is_numeric($current_data->nb_int) ? $current_data->nb_int : 0
                ,'data' => $current_data
            );
            $day['diff'] = $day['nb_pli'] - $day['nb_num'];
            array_push($data_rep, $day);
            $dt_index->modify('-1 day');
        }
        return $data_rep;
    }

    public function num_vs_ged_mail()
    {
        $datas = $this->nb_pli_mail_num_vs_ged();
        $data_rep = array();
        $dt_index = new DateTime($this->dt_today->format('Y-m-d'));
        while ($dt_index->format('Ymd') >= $this->dt_inf->format('Ymd')) {
            $current_data = NULL;
            foreach ($datas as $key => $data) {
                $date = new DateTime($data->dt_num);
                if($date->format('Ymd') == $dt_index->format('Ymd')){
                    $current_data = $data;
                }
            }
            if(is_null($current_data)){
                $current_data = new stdClass();
                $current_data->dt_num = $dt_index->format('Y-m-d');
                $current_data->nb_num = 0;
                $current_data->nb_int = 0;
            }
            $date = new DateTime($current_data->dt_num);
            $day = array(
                'date' => $date
                ,'nb_num' => is_numeric($current_data->nb_num) ? $current_data->nb_num : 0
                ,'nb_pli' => is_numeric($current_data->nb_int) ? $current_data->nb_int : 0
                ,'data' => $current_data
            );
            $day['diff'] = $day['nb_pli'] - $day['nb_num'];
            array_push($data_rep, $day);
            $dt_index->modify('-1 day');
        }
        return $data_rep;
    }

    public function detail_num_vs_ged_courrier()
    {
        $datas = $this->detail_courrier_num_and_ged();
        $data_rep = array();
        foreach ($datas as $key => $data) {
            $day = array(
                'date' => new DateTime($data->dt_num)
                ,'lot' => $data->lot_scan
                ,'nb_num' => is_numeric($data->nb_num) ? $data->nb_num : 0
                ,'nb_pli' => is_numeric($data->nb_int) ? $data->nb_int : 0
                ,'data' => $data
            );
            $day['diff'] = $day['nb_pli'] - $day['nb_num'];
            array_push($data_rep, $day);
        }
        return $data_rep;
    }

    public function detail_num_vs_ged_mail()
    {
        $datas = $this->detail_mail_num_and_ged();
        $data_rep = array();
        foreach ($datas as $key => $data) {
            $day = array(
                'date' => new DateTime($data->dt_num)
                ,'dossier' => $data->dossier
                ,'nb_num' => is_numeric($data->nb_num) ? $data->nb_num : 0
                ,'nb_pli' => is_numeric($data->nb_int) ? $data->nb_int : 0
                ,'data' => $data
            );
            $day['diff'] = $day['nb_pli'] - $day['nb_num'];
            array_push($data_rep, $day);
        }
        return $data_rep;
    }

    private function nb_pli_courrier_num_vs_ged()
    {
        return $this->ged
            ->select('date_numerisation::DATE dt_num', FALSE)
            ->select('SUM(nb_plis_numerise) nb_num')
            ->select('SUM(nb_plis_integre) nb_int')
            ->from($this->tb_suiviNum)
            ->where(" date_numerisation::DATE BETWEEN '".$this->dt_inf->format('Y-m-d')."' AND '".$this->dt_today->format('Y-m-d')."' ", NULL, FALSE)
            ->group_by('date_numerisation::DATE', FALSE)
            ->order_by('dt_num', 'DESC')
            ->get()->result();
    }

    private function detail_courrier_num_and_ged()
    {
        return $this->ged
            ->from($this->tb_suiviNum)
            ->where(" date_numerisation::DATE BETWEEN '".$this->dt_inf->format('Y-m-d')."' AND '".$this->dt_today->format('Y-m-d')."' ", NULL, FALSE)
            ->select('lot_scan')
            ->select('nb_plis_numerise nb_num')
            ->select('nb_plis_integre nb_int')
            ->select('date_numerisation dt_num')
            ->order_by('date_numerisation', 'DESC')
            ->get()->result();
    }
    
    private function nb_pli_mail_num_vs_ged()
    {
        return $this->ged
            ->select('date_mail::DATE dt_num', FALSE)
            ->select('SUM(nb_mail_boite) nb_num')
            ->select('SUM(nb_mails_integres) nb_int')
            ->from($this->tb_suiviMail)
            ->where(" date_mail::DATE BETWEEN '".$this->dt_inf->format('Y-m-d')."' AND '".$this->dt_today->format('Y-m-d')."' ", NULL, FALSE)
            ->group_by('date_mail::DATE', FALSE)
            ->order_by('date_mail', 'DESC')
            ->get()->result();
    }
    
    private function detail_mail_num_and_ged()
    {
        return $this->ged
            ->from($this->tb_suiviMail)
            ->where(" date_mail::DATE BETWEEN '".$this->dt_inf->format('Y-m-d')."' AND '".$this->dt_today->format('Y-m-d')."' ", NULL, FALSE)
            ->select('dossier')
            ->select('SUM(nb_mail_boite) nb_num')
            ->select('SUM(nb_mails_integres) nb_int')
            ->select('date_mail dt_num')
            ->group_by('date_mail')
            ->group_by('dossier')
            ->order_by('date_mail', 'DESC')
            ->order_by('dossier', 'ASC')
            ->get()->result();
    }

}
