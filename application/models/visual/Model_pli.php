<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pli extends CI_Model{
	
	private $CI;

	private $ged;
	private $base_64;

	private $tb_pli = TB_pli;
	private $vw_pli = Vw_pli;
	private $vw_doc = Vw_doc;
	private $vw_lastH = Vw_lastHisto;
	private $vw_lotN = Vw_lotNum;
	private $tb_flgTrt = TB_flgTrt;
	private $vw_flgTrt = Vw_flgTrt;
	private $tb_tpUser = TB_tpUser;
	private $tb_user = TB_user;
	private $vw_user = Vw_user;
	private $tb_doc = TB_document;
	private $tb_titre = TB_titre;
	private $tb_cheque = TB_cheque;
	private $vw_grPliPos = Vw_grPliPos;
	private $tb_groupe_pli = TB_groupe_pli;
	private $tb_data_pli = TB_data_pli;
	private $tb_soc = TB_soc;
	private $tb_mdPaie = TB_mode_paiement;
	private $tb_paiement = TB_paiement;
	private $tb_mvmnt = TB_mvmnt;
	private $tb_typo = TB_typo;
	private $tb_statS = TB_statS;
	private $tb_histo = TB_histo;
	private $tb_action = TB_action;
	private $tb_trace = TB_trace;
	private $tb_lotSaisie = TB_lotSaisie;
	private $ftb_doc = FTB_document;
	private $ftb_docImg = FTB_documentImg;
	private $ftb_lotNum = FTB_lotNumerisation;
	private $ftb_flgTrt = FTB_flgTrt;
	private $ftb_pli = FTB_pli;
	private $ftb_cheque = FTB_cheque;
	private $tb_lstCirc = TB_lstCirc;
	private $ftb_lstCirc = FTB_lstCirc;
	private $tb_chqKd = TB_chqKd;
	private $tb_paieChqKd = TB_paieChqKd;
	private $ftb_chqKd = FTB_chqKd;
	private $ftb_paieChqKd = FTB_paieChqKd;
	private $ftb_preData = FTB_preData;
	private $tb_dataKe = TB_dataKe;
	private $tb_fichierKe = TB_fichierKe;
	private $tb_motifConsigne = TB_motifConsigne;
	private $tb_commentsPli = TB_commentsPli;
	private $ftb_etatChq = FTB_etatChq;
	private $ftb_paieChqAnom = FTB_paieChqAnom;
	private $ftb_chqAnom = FTB_chqAnom;
	
	public function __construct() {
		parent::__construct();
		$this->CI =& get_instance();
		$this->ged = $this->load->database('ged', TRUE);
		$this->base_64 = $this->load->database('base_64', TRUE);
	}
	
	public function flag_traitement(){
		return $this->base_64
			->from($this->tb_flgTrt)
			->where('filter_display', 1)
			->order_by('ordre_affichage')
			->get()->result();
	}
	
	public function statut_saisie(){
		return $this->ged
			->from($this->tb_statS)
			->order_by('id_statut_saisie')
			->where('actif', 1)
			//->where('saisie', 1)
			->get()->result();
	}
	
	public function societe(){
		return $this->ged
			->from($this->tb_soc)
			->where('actif', 1)
			->order_by('nom_societe')
			->get()->result();
	}

	public function typologie(){
		return $this->ged
			->from($this->tb_typo)
			->order_by('typologie')
			->get()->result();
	}

	public function mode_paiement(){
		return $this->ged
			->from($this->tb_mdPaie)
			->order_by('mode_paiement')
			->get()->result();
	}

	public function pli_total(){
		return $this->base_64->from($this->tb_pli)->count_all_results();
	}
	
    public function get_plis($limit, $offset, $arg_find, $arg_ordo, $sens, $filtre, $nb_only = TRUE){ 
		$sql_sub_pli = $this->sql_pli($filtre, $nb_only);
		$sql_sub_mvmnt = $this->sql_mouvement_data($filtre, $nb_only);
		$sql_sub_cmc7 = $this->sql_cmc7($filtre, $nb_only);
		$sql_sub_pai = $this->sql_paiement_data($filtre, $nb_only);
		$sql_sub_motifko_consigne = $this->sql_last_motif_consigne();
		$sql_sub_nb_comms = $this->sql_nb_comms();
		$this->ged->reset_query();
		$this->ged
			->from('('.$sql_sub_pli.') sub_pli')
			//->join($this->ftb_flgTrt.' tbflgtrt', 'sub_pli.flg_trtm = tbflgtrt.id_flag_traitement', 'LEFT')
			->join($this->tb_statS.' tbstat', 'status = id_statut_saisie', 'LEFT')
			->join($this->tb_typo.' tbtypo', 'sub_pli.typo = tbtypo.id', 'LEFT')
			->join($this->tb_soc.' tbsoc', 'sub_pli.soc = tbsoc.id', 'LEFT')
			->join('('.$sql_sub_pai.') sub_paie', 'sub_pli.idpli = paie_id_pli', 'LEFT')
			->join('('.$sql_sub_motifko_consigne.') tbmotcons', 'tbmotcons.id_pli = sub_pli.idpli', 'LEFT')
			->join('('.$sql_sub_nb_comms.') tbnbcomms', 'tbnbcomms.id_pli_comment = sub_pli.idpli', 'LEFT');
		if (!$nb_only || $filtre['find_pr'] != '' || $filtre['find_ab'] != '') {
			$this->ged->join('('.$sql_sub_mvmnt.') sub_mvmt', 'sub_pli.idpli = mvmt_id_pli', 'LEFT');
		}
		if ($filtre['find_cmc7'] != '' || !$nb_only) {
			$join = $filtre['find_cmc7'] != '' ? 'INNER' : 'LEFT';
            $this->ged
                ->join('('.$sql_sub_cmc7.') sub_cmc7', 'sub_pli.idpli = cmc7_id_pli', $join)
                ->select('sub_cmc7.*');
		}
		if ($arg_find != '') {
			$this->ged
				->group_start()
					->like('lower(pli)', $arg_find)
				->group_end();
		}
        if ($nb_only) {
			return $this->ged->count_all_results();
		}
		$this->ged->order_by($arg_ordo, strtoupper($sens));
		if($arg_ordo == 'nb_paie'){
			$this->ged->order_by('paiements', strtoupper($sens));
		}
		$this->ged->order_by('sub_pli.idpli');
		return $this->ged
			->select('nb_mvmts mvmts')
			->select('paiements')
			->select('date_courrier::DATE dtcourrier', FALSE)
			->select('date_numerisation dtnum')
			->select('lot_scan lotScan')
			->select('tbsoc.nom_societe soc')
			->select('sub_pli.idpli idpli')
			->select('pli')
			->select('traitement etape')
			->select('id_flag_traitement id_flag_trtmnt')
			->select('flag_rewrite_status_client rewrite_status')
			->select('flgtt_etape')
			->select('flgtt_etat flgtt_etat_disp')
			->select('flgtt_flag_attente')
			->select('pli_nouveau')
			->select('par_ttmt_ko')
			->select('(CASE WHEN flag_rewrite_status_client <> 0 AND tbstat.id_statut_saisie IN (1) THEN \'\' ELSE tbstat.libelle END) etat')
			->select('tbstat.id_statut_saisie id_statut_saisie')
			->select('tbtypo.typologie typo')
			->select('tbtypo.sous_lot typo1')
			->select('id_lot_saisie lotss')
			->select('sub_pli.lot_saisie_bis lotss1')
			->select('(CASE WHEN id_lot_saisie IS NULL THEN \'\' ELSE (sub_pli.dt_enregistrement::DATE)::VARCHAR END) dttrt')
			->select('(CASE WHEN nb_paie IS NULL THEN 0 ELSE nb_paie END)::integer nb_paie')
			->select('sub_pli.commande id_com')
			->select('sub_pli.recommande rec')
			->select('sub_pli.ke_prio')
			->select('tbmotcons.motif_operateur')
			->select('tbmotcons.consigne_client')
			->select('sub_pli.typage')
			->select('tbnbcomms.nb_comms')
			->select('niveau_saisie')
			->limit($limit, $offset)
			->get()->result();
	}

	private function sql_last_motif_consigne()
	{
		$this->ged->reset_query();
		$sql_sub = $this->ged->from($this->tb_motifConsigne)
			->select('id_pli')
			->select('MAX(dt_motif_ko) dt_ko', FALSE)
			->group_by('id_pli')
			->get_compiled_select();
		$this->ged->reset_query();
		return $this->ged->from($this->tb_motifConsigne.' tb')
			->select('tb.*', FALSE)
			->join('('.$sql_sub.') sub', 'sub.id_pli = tb.id_pli AND sub.dt_ko = tb.dt_motif_ko', 'INNER')
			->get_compiled_select();
	}

	private function sql_nb_comms()
	{
		$this->ged->reset_query();
		return $this->ged->from($this->tb_commentsPli)
			->select('id_pli_comment')
			->select('COUNT(*) nb_comms', FALSE)
			->group_by('id_pli_comment')
			->get_compiled_select();
	}

	private function sql_pli($filtre, $nb_only){
		$sql_id_pli_filtre_paiement = $this->sql_paiement_filtre($filtre, $nb_only);
		$sql_id_pli_filtre_mvmnt = $this->sql_mouvement_filtre($filtre, $nb_only);
		$sql_id_pli_filtre_soc = $this->sql_soc_filtre($filtre, $nb_only);
		$sql_id_pli_filtre_typo = $this->sql_typo_filtre($filtre, $nb_only);
		$this->ged->reset_query();
		$this->ged->from($this->ftb_pli.' tbpli')
			->join($this->ftb_lotNum.' tblotn', 'tbpli.id_lot_numerisation = tblotn.id_lot_numerisation', 'LEFT')
			->join($this->tb_data_pli.' tbdata', 'tbpli.id_pli = tbdata.id_pli', 'LEFT')
			->join($this->ftb_flgTrt.' tbflgtrt', 'tbpli.flag_traitement = tbflgtrt.id_flag_traitement', 'LEFT');
		if(is_array($filtre['md_paie']) && count($filtre['md_paie']) > 0){
            $this->ged->where(' tbdata.id_pli in ('.$sql_id_pli_filtre_paiement.') ', NULL, FALSE);
        }
		if($filtre['find_pr'] != '' || $filtre['find_ab'] != ''){
            $this->ged->where(' tbdata.id_pli in ('.$sql_id_pli_filtre_mvmnt.') ', NULL, FALSE);
		}
		if(is_array($filtre['soc']) && count($filtre['soc']) > 0){
			$this->ged->where(' tbdata.id_pli in ('.$sql_id_pli_filtre_soc.') ', NULL, FALSE);
		}
		if (!empty($filtre['c_dt1'])) {
			$this->ged->where('date_courrier::DATE >= \''.$filtre['c_dt1'].'\' ', NULL, FALSE)->where('date_courrier IS NOT NULL', NULL, FALSE);
		}
		if (!empty($filtre['c_dt2'])) {
			$this->ged->where('date_courrier::DATE <= \''.$filtre['c_dt2'].'\' ', NULL, FALSE)->where('date_courrier IS NOT NULL', NULL, FALSE);
		}
		if (!empty($filtre['trt_dt1'])) {
			$this->ged->where('tbdata.dt_enregistrement::DATE >= \''.$filtre['trt_dt1'].'\' ', NULL, FALSE)->where('id_lot_saisie IS NOT NULL', NULL, FALSE);
		}
		if (!empty($filtre['trt_dt2'])) {
			$this->ged->where('tbdata.dt_enregistrement::DATE <= \''.$filtre['trt_dt2'].'\' ', NULL, FALSE)->where('id_lot_saisie IS NOT NULL', NULL, FALSE);
		}
		if(is_array($filtre['etape']) && count($filtre['etape']) > 0){
			$this->ged->where_in('tbpli.flag_traitement', $filtre['etape']);
		}
		if(is_array($filtre['etat']) && count($filtre['etat']) > 0){
			$this->ged->where_in('tbdata.statut_saisie', $filtre['etat'])
				->where('flag_rewrite_status_client', 0);
		}
		if(is_array($filtre['typo']) && count($filtre['typo']) > 0){
			$this->ged->where(' tbdata.id_pli in ('.$sql_id_pli_filtre_typo.') ', NULL, FALSE);
		}
		if ($filtre['find_id_pli'] != '') {
			$this->ged->like('(tbdata.id_pli::varchar)', $filtre['find_id_pli']);
		}
		if(is_array($filtre['niveau_saisie']) && count($filtre['niveau_saisie']) > 0){
            $this->ged->where_in('niveau_saisie', $filtre['niveau_saisie']);
        }
		if ($filtre['find_pli'] != '') {
			$this->ged
				->group_start()
					->like('lower(id_lot_saisie)', $filtre['find_pli'])
					->or_like('lower(lot_scan::varchar)', $filtre['find_pli'])
				->group_end();
		}
		return $this->ged
			->select('*')
			->select('tbpli.societe soc')
			->select('tbpli.id_pli idpli')
			->select('tbpli.flag_traitement flg_trtm')
			->select('tbpli.typologie typo')
			->select('tbdata.statut_saisie status')
			->select('(CASE WHEN flag_batch = -1 THEN \'\' ELSE (CASE WHEN flag_typage_auto = 1 THEN \'AUTO\' ELSE \'MANUEL\' END) END) typage')
			->get_compiled_select();
	}

	private function sql_soc_filtre($filtre, $nb_only){
		$this->ged->reset_query();
		return $this->ged
			->from($this->tb_data_pli)
			->where_in('societe', $filtre['soc'])
			->select('id_pli')
			->get_compiled_select();
	}

	private function sql_typo_filtre($filtre, $nb_only){
		$this->ged->reset_query();
		return $this->ged
			->from($this->ftb_pli)
			->where_in('typologie', $filtre['typo'])
			->select('id_pli')
			->get_compiled_select();
	}
	
	private function sql_mouvement_data($filtre, $nb_only){
		$this->ged->reset_query();
		if (!$nb_only) {
			//$this->ged->select('STRING_AGG(\'<span class="elem_activ" onclick="load_mvmt(\'\'\'||id::VARCHAR||\'\'\', \'\'\'||id_pli::VARCHAR||\'\'\');">#\'||id::VARCHAR||\'</span>\', \' / \' ORDER BY id) mvmts', FALSE);
			$this->ged->select('count(id) nb_mvmts', FALSE);
		}
		return $this->ged
			->select('id_pli mvmt_id_pli')
			->from($this->tb_mvmnt)
			->group_by('id_pli')
			->get_compiled_select();
	}

	private function sql_mouvement_filtre($filtre, $nb_only){
		$this->ged->reset_query();
		if ($filtre['find_pr'] != '') {
			$this->ged
				->group_start()
					->like('lower(nom_payeur)', $filtre['find_pr'])
					->or_like('lower(numero_payeur)', $filtre['find_pr'])
				->group_end();
		}
		if ($filtre['find_ab'] != '') {
			$this->ged
				->group_start()
					->like('lower(nom_abonne)', $filtre['find_ab'])
					->or_like('lower(numero_abonne)', $filtre['find_ab'])
				->group_end();
		}
		return $this->ged
			->select('id_pli')
			->from($this->tb_mvmnt)
			->get_compiled_select();
	}
	
    private function sql_cmc7($filtre, $nb_only){
        $this->ged->reset_query();
        $sql_sub_cheque = $this->ged
            ->select('id_pli chq_id_pli')
            ->select('STRING_AGG(cmc7, \', \' ORDER BY cmc7) cmc7s', FALSE)
            ->from($this->tb_cheque)
            ->group_by('id_pli')
            ->get_compiled_select();
        $this->ged->reset_query();
        $sql_sub_pre_cheque = $this->ged
            ->select('id_pli pre_chq_id_pli')
            ->select('STRING_AGG(cmc7, \', \' ORDER BY cmc7) pre_cmc7s', FALSE)
            ->from($this->ftb_doc.' tbdoc')
            ->join($this->ftb_preData.' tbpredata', 'tbpredata.id_document = tbdoc.id_document', 'LEFT')
            ->group_by('id_pli')
            ->get_compiled_select();
		$this->ged->reset_query();
		$valide_cmc7 = is_numeric(trim($filtre['find_cmc7'])) && (int)trim($filtre['find_cmc7']) * 1 > 1;
		if($filtre['find_cmc7'] != '' && $valide_cmc7){
			$this->ged
				->join('('.$sql_sub_pre_cheque.') sub_pre_chq', 'chq_id_pli = pre_chq_id_pli', 'LEFT')
				->select('pre_cmc7s');
		}
        $this->ged
            ->from('('.$sql_sub_cheque.') sub_chq')
            ->select('chq_id_pli cmc7_id_pli')
            ->select('cmc7s');
        if ($filtre['find_cmc7'] != '') {
            $this->ged
                ->group_start()
					->like('cmc7s', $filtre['find_cmc7']);
			if($valide_cmc7){
				$this->ged->or_like('pre_cmc7s', $filtre['find_cmc7']);
			}
			$this->ged->group_end();
        }
        return $this->ged->get_compiled_select();
    }

    private function sql_paiement_data($filtre, $nb_only){
        $this->ged->reset_query();
        $this->ged
            ->from($this->tb_paiement.' tbpai')
            ->join($this->tb_mdPaie.' md_paie', 'tbpai.id_mode_paiement = md_paie.id_mode_paiement', 'LEFT')
            ->group_by('tbpai.id_pli')
			->select('tbpai.id_pli paie_id_pli')
			->select('COUNT(md_paie.id_mode_paiement) nb_paie');
        if (!$nb_only) {
			$this->ged->select('STRING_AGG(md_paie.mode_paiement, \'|\' ORDER BY md_paie.id_groupe_paiement) paiements', FALSE);
        }
        return $this->ged->get_compiled_select();
    }

    private function sql_paiement_filtre($filtre, $nb_only){
        $this->ged->reset_query();
        $this->ged
            ->from($this->tb_paiement)
            ->where_in('id_mode_paiement', $filtre['md_paie']);
        return $this->ged->distinct()->select('id_pli')->get_compiled_select();
    }
	
	public function doc($id_doc){
		$data = $this->base_64
			->from($this->tb_doc)
			->where('id_document', $id_doc)
			->get()->result();
		return count($data) ? $data[0] : NULL;
	}
	
	public function docs($id_pli){
		$data = $this->base_64
			->from($this->tb_doc)
			->where('id_pli', $id_pli)
			->order_by('id_document')
			->get()->result();
		return count($data) > 0 ? $data : array();
	}
	
	public function mouvements($id_pli){
		return $this->ged
			->select('*')
			->select('tbmv.id id_')
			->select('tbtt.titre titre_')
			->where('id_pli', $id_pli)
			->from($this->tb_mvmnt.' tbmv')
			->join($this->tb_titre.' tbtt', 'tbtt.id = tbmv.titre', 'LEFT')
			->order_by('tbmv.id')
			->get()->result();
	}
	
    public function pli_view($id_pli){
        if(is_numeric($id_pli)){
			$sql_sub_nb_pj = $this->ged
				->select('id_pli pj_id_pli')
				->select('COUNT(id_document) nb_pj')
				->from($this->ftb_doc)
				->where('desarchive', 1)
				->group_by('id_pli')
				->get_compiled_select();
			$this->ged->reset_query();
            $pli = $this->ged
				->select('*')
				->select('tb_pli.id_pli idpli')
                ->select('tbtt.titre titre_')
				->select('tbtpo.typologie typo')
				->select('(CASE WHEN nb_pj IS NULL THEN 0 ELSE nb_pj END)::integer nb_pj')
                ->from($this->ftb_pli.' tb_pli')
                ->where('tb_pli.id_pli', $id_pli)
                ->join($this->ftb_flgTrt.' flg_ttr', 'tb_pli.flag_traitement = flg_ttr.id_flag_traitement', 'INNER')
                ->join($this->tb_data_pli.' tbdt_pli', 'tbdt_pli.id_pli = tb_pli.id_pli', 'LEFT')
                ->join($this->tb_soc.' tbsoc', 'tbsoc.id = tbdt_pli.societe', 'LEFT')
                ->join($this->tb_titre.' tbtt', 'tbtt.id = tbdt_pli.titre', 'LEFT')
                ->join($this->tb_typo.' tbtpo', 'tbtpo.id = tb_pli.typologie', 'LEFT')
                ->join($this->tb_statS.' tbstat', 'tbstat.id_statut_saisie = tbdt_pli.statut_saisie', 'LEFT')
				->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
				->join('('.$sql_sub_nb_pj.') sub_pj', 'tb_pli.id_pli = pj_id_pli', 'LEFT')
				->join($this->tb_lstCirc.' tblc', 'tbdt_pli.nom_circulaire = tblc.id', 'LEFT')
                ->get()->result();
            return count($pli) > 0 ? $pli[0] : NULL;
        }
        return NULL;
    }
	
    public function data_docs($id_pli){
        return $this->base_64
            ->select('id_document')
            ->from($this->tb_doc)
            ->where('id_pli', $id_pli)
            ->get()->result();
	}
	
    public function paiement_view($id_pli){
        $data =  $this->ged
            ->select('STRING_AGG(mode_paiement, \', \' ORDER BY mode_paiement) paie', FALSE)
            ->from($this->tb_paiement.' tbp')
            ->join($this->tb_mdPaie.' tbmd_p', 'tbp.id_mode_paiement = tbmd_p.id_mode_paiement', 'INNER')
            ->where('id_pli', $id_pli)
            ->group_by('id_pli')
            ->get()->result();
        return isset($data[0]) ? $data[0]->paie : '';
	}
	
    public function cheques($id_pli){
        return $this->ged
            ->where('id_pli', $id_pli)
            ->from($this->tb_cheque)
            ->get()->result();
	}
	
    public function cheques_kd($id_pli){
        return $this->ged
            ->where('id_pli', $id_pli)
			->from($this->tb_paieChqKd.' tb_chq')
			->join($this->tb_chqKd.' tb_tp', 'type_chq_kdo = tb_tp.id', 'INNER')
            ->get()->result();
	}
	
	public function pli($id_pli){
		$pli = $this->base_64
			->from($this->tb_pli)
			->where('id_pli', $id_pli)
			->get()->result();
		return count($pli) > 0 ? $pli[0] : NULL;
	}
	
    public function data_pli($id_pli){
        if(!is_numeric($id_pli)){
            return NULL;
        }
		$data_pli = $this->ged
			->select('tbdata.*', FALSE)
			->select('date_saisie')
			->select('pli')
			->select('lot_scan')
			->from($this->tb_data_pli.' tbdata')
			->join($this->ftb_pli.' tb_pli', 'tb_pli.id_pli = tbdata.id_pli', 'LEFT')
			->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
            ->where('tbdata.id_pli', $id_pli)
            ->get()->result();
        return count($data_pli) > 0 ? $data_pli[0] : NULL;
    }

	public function histo($id_pli){
		$sql_sub_action = $this->ged
			->select('tbact.id_action ident')
			->select('tbact.label_action lbl')
			->select('id_user par')
			->select('(split_part(date_action::varchar, \'.\', 1) || \'.0\')::timestamp without time zone moment')
			->select('coms_trace com')
			->select("'' motif_ko")
			->select("(null) dt_motif_ko", FALSE)
			->select("'' consigne_ko")
			->select("(null) dt_consigne", FALSE)
			->select('0 flag', FALSE)
			->from($this->tb_trace.' tbtrc')
			->join($this->tb_action.' tbact', 'tbtrc.id_action = tbact.id_action', 'INNER')
			->where('id_pli', $id_pli)
			->where('tbact.courrier', 1)
			->get_compiled_select();
		$this->ged->reset_query();
		$sql_sub_histo = $this->ged
			->select('tbh.flag_traitement ident')
			->select('(tbflg.flgtt_etape || \' (\' || tbflg.flgtt_etat || (CASE WHEN flag_rewrite_status_client <> 0 AND tbh.statut_pli IN (1) THEN \')\' ELSE (\' - \' || tbstats.libelle || \')\') END)) lbl')
			->select('utilisateur par')
			->select('dt_event moment')
			->select('comms com')
			->select("motif_operateur motif_ko")
			->select('dt_motif_ko dt_motif_ko', FALSE)
			->select("consigne_client consigne_ko")
			->select('dt_consigne dt_consigne', FALSE)
			->select('1 flag', FALSE)
			->from($this->tb_histo.' tbh')
			->join($this->ftb_flgTrt.' tbflg', 'tbh.flag_traitement = tbflg.id_flag_traitement', 'INNER')
			->join($this->tb_statS.' tbstats', 'tbh.statut_pli = tbstats.id_statut_saisie', 'INNER')
			->join($this->tb_motifConsigne.' tbmotifcons', 'tbmotifcons.link_histo = tbh.link_motif_consigne', 'LEFT')
			->where('tbh.id_pli', $id_pli)
			->get_compiled_select();
		$this->ged->reset_query();
		return  $this->ged
			->from('(('.$sql_sub_action.') UNION ('.$sql_sub_histo.')) tbe')
			->join($this->tb_user.' tbu', 'tbe.par = tbu.id_utilisateur', 'LEFT')
			->order_by('moment', 'DESC')
			->order_by('flag', 'ASC')
			->get()->result();
	}

	public function detail_lot_saisie($id_lot_saisie){
		$sql_sub_pli = $this->ged
			->select('id_lot_saisie id_lot')
			->select('COUNT(id_pli) nb_pli')
			->select('STRING_AGG(\'#\' || id_pli::VARCHAR , \' | \' ORDER BY id_pli) id_plis', FALSE)
			->from($this->vw_pli)
			->where('id_lot_saisie', $id_lot_saisie)
			->where('lot_saisie_bis', 0)
			->group_by('id_lot_saisie')
			->get_compiled_select();
		$this->ged->reset_query();
		$sql_sub_pli_bis = $this->ged
			->select('id_lot_saisie id_lot')
			->select('COUNT(id_pli) nb_pli')
			->select('STRING_AGG(\'#\' || id_pli::VARCHAR , \' | \' ORDER BY id_pli) id_plis', FALSE)
			->from($this->vw_pli)
			->where('id_lot_saisie', $id_lot_saisie)
			->where('lot_saisie_bis', 1)
			->group_by('id_lot_saisie')
			->get_compiled_select();
		$this->ged->reset_query();
		$data = $this->ged
            ->from($this->tb_lotSaisie.' tbl')
            ->join(' ('.$sql_sub_pli.') tbsub', 'identifiant = tbsub.id_lot', 'LEFT')
			->join(' ('.$sql_sub_pli_bis.') tbsub_bis', 'identifiant = tbsub_bis.id_lot', 'LEFT')
			->join($this->tb_user.' tbu', 'operateur = id_utilisateur', 'LEFT')
			->join($this->vw_grPliPos.' tbposs', 'tbl.id_group_pli_possible = tbposs.id_groupe_pli_possible', 'LEFT')
			->where('identifiant', $id_lot_saisie)
            ->select('dt dt_creat')
            ->select('identifiant id_lot')
            ->select('login par')
            ->select('soc soc')
            ->select('typologie typo')
            ->select('groupe_paiement paie')
            ->select('(CASE WHEN tbsub.nb_pli IS NULL THEN 0 ELSE tbsub.nb_pli END)::integer nbpli')
            ->select('tbsub.id_plis plis')
            ->select('(CASE WHEN tbsub_bis.nb_pli IS NULL THEN 0 ELSE tbsub_bis.nb_pli END)::integer nbpli_bis')
            ->select('tbsub_bis.id_plis plis_bis')
			->select('(CASE WHEN tbsub.nb_pli IS NULL THEN 0 ELSE tbsub.nb_pli END)::integer + (CASE WHEN tbsub_bis.nb_pli IS NULL THEN 0 ELSE tbsub_bis.nb_pli END)::integer total')
			->get()->result();
		return count($data) > 0 ? $data[0] : NULL;
	}
	
    public function ged_pjs($id_pli){
        return $this->base_64
            ->from($this->tb_doc)
            ->where('id_pli', $id_pli)
            ->where('desarchive', 1)
            ->order_by('date_creation', 'ASC')
            ->get()->result();
	}

	public function info_ke($id_pli){
		$data_ke_raw = $this->ged
			->from($this->tb_dataKe)
			->where('id_pli', $id_pli)
			->limit(1)
			->get()->result();
		$info_ke = new stdClass();
		$info_ke->data_ke = count($data_ke_raw) > 0 ? $data_ke_raw[0] : NULL;
		$info_ke->fichier_ke = $this->ged
			->select('*')
			->select('oid')
			->from($this->tb_fichierKe)
			->where('id_pli', $id_pli)
			->order_by('nom_orig')
			->get()->result();
		return $info_ke;
	}

	public function save_data_consigne_ke($data){
		try {
			$this->ged->trans_begin();
			if(count($data->deleted_oids) > 0){
				$this->ged->where_in('oid', $data->deleted_oids)->delete($this->tb_fichierKe);
			}
			if(count($data->fichier_ke) > 0){
				$this->ged->insert_batch($this->tb_fichierKe, $data->fichier_ke);
			}
			$this->ged->where('id_pli', $data->id_pli)->delete($this->tb_dataKe);
			$this->ged->insert($this->tb_dataKe, $data->data_pli_ke);
			$this->ged->where('id_pli', $data->id_pli)->set('ke_prio', 1)->update($this->tb_data_pli);
			if($this->ged->trans_status() === FALSE){
				$this->ged->trans_rollback();
				throw new Exception("Transaction failed");
			}else {
				$this->ged->trans_commit();
			}
		} catch (Exception $th) {
			$this->ged->trans_rollback();
			throw $th;
		}
	}

	public function fichier_ke($oid){
		$fichiers = $this->ged
			->from($this->tb_fichierKe)
			->where('oid', $oid)
			->get()->result();
		return count($fichiers) > 0 ? $fichiers[0] : NULL;
	}
	
    public function get_motif_consigne($id_pli)
    {
        $data = $this->ged
            ->from($this->tb_motifConsigne)
            ->where('id_pli', $id_pli)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
	}
	
    public function get_all_motif_consigne($id_pli)
    {
        $data = $this->ged
            ->from($this->tb_motifConsigne)
            ->where('id_pli', $id_pli)
            ->order_by('dt_motif_ko', 'ASC')
            ->get()->result();
        return count($data) > 0 ? $data : array();
    }
	
	public function comments($id_pli, $new_comment=NULL)
	{
		if(!is_numeric($id_pli)){
			return array();
		}
		if(is_array($new_comment) && count($new_comment) > 0){

			$this->ged->trans_begin();
			$this->ged->insert($this->tb_commentsPli, $new_comment);
            if ($this->ged->trans_status() === FALSE)
            {
        		$this->ged->trans_rollback();
            }
            else
            {
				$this->CI->histo->action(127, '', $id_pli);
				$this->ged->trans_commit();
            }
			
		}
		return $this->ged
			->from($this->tb_commentsPli.' tbcom')
			->join($this->tb_user.' tbuser', 'tbcom.commented_by = tbuser.id_utilisateur', 'INNER')
			->where('id_pli_comment', $id_pli)
			->order_by('dt_comment', 'DESC')
			->get()->result();
	}

	public function typo_pli($id_pli)
	{
		$data = $this->ged
			->from($this->tb_data_pli.' tb_data')
			->where('id_pli', $id_pli)
			->join($this->tb_typo.' tb_typo', 'tb_data.typologie = tb_typo.id', 'INNER')
			->select('tb_typo.typologie')
			->get()->result();
		return count($data) > 0 ? $data[0]->typologie : '';
	}
	
	public function visu_docs_offset($id_pli, $offset=0, $limit=25)
	{
		$data = $this->base_64
			->from($this->tb_doc)
			->where('id_pli', $id_pli)
			->order_by('id_document')
			->limit($limit, $offset)
			->get()->result();
		return count($data) > 0 ? $data : array();
	}

	public function nb_docs_pli($id_pli)
	{
		return $this->base_64
			->from($this->tb_doc)
			->where('id_pli', $id_pli)
			->count_all_results();
	}
	
	public function chqs_b64($id_pli)
	{
		$sql_sub_anom_chq = $this->base_64
			->select('id_doc id_doc_anom')
			->select('STRING_AGG(anomalie, \'|\' order by priorite desc) anomalie', FALSE)
			->from($this->ftb_paieChqAnom.' tpca')
			->join($this->ftb_chqAnom.' ta', 'ta.id = tpca.id_anomalie_chq', 'INNER')
			->group_by('id_doc')
			->get_compiled_select();
		$this->base_64->reset_query();
		return $this->base_64
			->where('tchq.id_pli', $id_pli)
			->from($this->ftb_cheque.' tchq')
			->join($this->tb_doc.' tdoc', 'tchq.id_doc = tdoc.id_document', 'INNER')
			->join($this->ftb_etatChq.' tetatchq', 'tetatchq.id_etat_chq = tchq.id_etat_chq', 'INNER')
			->join('('.$sql_sub_anom_chq.') sub_anom', 'id_doc_anom = tchq.id_doc', 'LEFT')
			->get()->result();
	}
	
	public function chqs_kd_b64($id_pli)
	{
		return $this->base_64
			->where('tchqkd.id_pli', $id_pli)
			->from($this->ftb_paieChqKd.' tchqkd')
			->join($this->ftb_chqKd.' tb_tp', 'type_chq_kdo = tb_tp.id', 'INNER')
			->join($this->tb_doc.' tdoc', 'tchqkd.id_doc = tdoc.id_document', 'INNER')
			->get()->result();
	}

}
