<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Model_Circulaire extends CI_Model
    {
        var $table         = 'view_liste_pli_circulaire';
        private $tb_doc = TB_document;
        private $tb_dataPli = TB_data_pli;
        private $CI;
        private $ged;
        private $base_64;

        function __construct()
        {
            parent::__construct();
            parent::__construct();
            $this->CI = &get_instance();
            $this->ged = $this->load->database('ged', TRUE);
            $this->base_64 = $this->load->database('base_64', TRUE);
    
           /* $this->load->library('portier');
            $this->portier->must_op();*/
        }

        /**
         * utiliser pour le server-side de datatable
         */
        
 
        public function getListePlisCirculaire($date1,$date2,$societe)
        {
            $sqlFinal = "SELECT *from public.view_liste_pli_circulaire where  date_enregistrement between '".$date1."' and '".$date2."' and societe = ".$societe." order by date_enregistrement";
    
            $query = $this->ged->query($sqlFinal);
            return $query->result();
            
        }

        public function ged_pjs($id_pli){
            return $this->base_64
                ->from($this->tb_doc)
                ->where('id_pli', $id_pli)
                ->where('desarchive', 1)
                ->order_by('date_creation', 'ASC')
                ->get()->result();
        }
                
        public function updateFlagCiAfterValidation($id_pli)
        {   return $this->ged
                   ->where('id_pli', $id_pli)
                   ->set('flag_ci', 1)
                   ->update($this->tb_dataPli); 
            
        }
       
    }