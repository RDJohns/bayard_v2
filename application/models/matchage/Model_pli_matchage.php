<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pli_matchage extends CI_Model{

    private $CI;

    private $ged;
    private $base_64;
    private $base_reb_temp;

    private $tb_pli = TB_pli;
    private $f_pli = FTB_pli;
    private $tb_cheque = TB_cheque;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_dataPli = TB_data_pli;
    private $tb_document = TB_document;
    private $tb_user = TB_user;
    private $tb_statut = TB_statS;
    private $tb_etat = TB_etatMatchage;
    private $tb_mTempBy = TB_matchTempBy;
    private $tb_mTempMi = TB_matchTempMi;
    private $tb_societe = TB_soc;
    private $vw_pli = Vw_pli;

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->base_reb_temp = $this->load->database('base_reb_temp', TRUE);
    }

    public function get_societe(){
        return $this->ged->select('*')
                        ->from($this->tb_societe)
                        ->order_by('id','asc')
                        ->get()
                        ->result();
    }

    public function get_info_chq($num_pmt,$num_pay,$id_soc){
        $info = $this->ged->select($this->vw_pli.'.id_pli, numero_payeur, nom_payeur, numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement,dt_enregistrement,login as saisie_par,'.$this->tb_cheque.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie,'.$this->vw_pli.'.id_lot_saisie')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where($this->vw_pli.'.flag_traitement > ', 4)
            ->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
                ->where('anomalie_cheque_id', 0)
                ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 1)
            ->where('match_societe', $id_soc)
            ->where('flag_suppr', 0)
            ->where($this->vw_pli.'.recommande != -1 ',null,false)
            //->where('trouve',0)
            ->like('cmc7', $num_pmt, 'after')
            ->like('mv.numero_payeur', $num_pay)
            ->get()
            ->result();

            if($info){
                $this->ged->where('id', $info[0]->id)
                            ->update($this->tb_cheque, array('trouve' => 1));
            }

            return $info;

    }

    public function get_info_chq_in_vv($num_pmt){
        return $this->ged->select($this->vw_pli.'.id_pli, numero_payeur, nom_payeur, numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement,dt_enregistrement,login as saisie_par,'.$this->tb_cheque.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie,'.$this->vw_pli.'.id_lot_saisie')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where($this->vw_pli.'.flag_traitement > ', 4)
            ->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
            ->where('anomalie_cheque_id', 0)
            ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 1)
            ->where('flag_suppr', 0)
            ->like('cmc7', $num_pmt, 'after')
            ->get()
            ->result();

    }

    public function get_not_in_adv($id_soc){
        return $this->ged->select($this->vw_pli.'.id_pli, numero_payeur, nom_payeur, numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement,dt_enregistrement,login as saisie_par,'.$this->tb_cheque.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie,'.$this->vw_pli.'.id_lot_saisie')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where($this->vw_pli.'.flag_traitement > ', 4)
            ->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
            ->where('anomalie_cheque_id', 0)
            ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 1)
            ->where('match_societe', $id_soc)
            ->where('flag_suppr', 0)
            ->where($this->vw_pli.'.recommande != -1 ', NULL, FALSE)
            ->where('statut_saisie != ', 3)
            ->where('trouve', 0)
            ->get()
            ->result();
    }

    public function get_liste_chq(){

        return $this->ged->select($this->vw_pli.'.id_pli,numero_payeur,nom_payeur,numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement, dt_enregistrement,login as saisie_par,'.$this->tb_cheque.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie')
                ->from($this->vw_pli)
                ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
                ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur, 
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
                ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
                ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
                ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
                //->where_in('flag_traitement', array(9,20,25,14,15))
                ->where('flag_traitement > ', 4)
                ->where_not_in('flag_traitement', array(16,21,24))
                //->where('anomalie_cheque_id', 0)
                ->group_start()
                    ->where('anomalie_cheque_id', 0)
                    ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
                ->group_end()
                ->where('etat_matchage', 1)
                ->where('flag_suppr', 0)
                ->get()
                ->result();

    }

    public function get_cmc_from_vv(){
         $list_cmc7 = $this->ged->select('substring(cmc7,1,7) as cmc7')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where($this->vw_pli.'.flag_traitement > ', 4)
            ->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
                ->where('anomalie_cheque_id', 0)
                ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 1)
            ->where('flag_suppr', 0)
            ->where('statut_saisie != ', 3)
            ->get()
            ->result();

         $arr_pmt  = array();

         foreach ($list_cmc7 as $cmc){
             array_push($arr_pmt, $cmc->cmc7);
         }

         return $arr_pmt;
    }

    public function get_absent_in_adv(){
        $list_cmc7 = $this->ged->select('substring(cmc7,1,7) as cmc7')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur, 
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where('flag_traitement > ', 4)
            ->where_not_in('flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
                ->where('anomalie_cheque_id', 0)
                ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 1)
            ->where('flag_suppr', 0)
            ->get()
            ->result();

        $arr_pmt  = array();

        foreach ($list_cmc7 as $cmc){
            array_push($arr_pmt, $cmc->cmc7);
        }

        return $arr_pmt;
    }

    public function get_doc($id_pli){
        return $this->base_64->select('id_document, id_pli, n_ima_base64_recto, n_ima_base64_verso')
                ->from($this->tb_document)
                ->where('id_pli', $id_pli)
                ->get()
                ->result();
    }

    public function update_chq($id, $data){
        return $this->ged->where('id', $id)
                         ->update($this->tb_cheque, $data);
    }

    public function update_comment($id, $data){
        return $this->ged->where('id', $id)
            ->update($this->tb_cheque, $data);
    }

    public function update_etat($id, $data){
        return $this->ged->where('id', $id)
            ->update($this->tb_cheque, $data);
    }

    public function test_date(){
        $date = date("Y-m-d");
        return $this->ged->select($this->vw_pli.'.id_pli,dt_enregistrement')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur, 
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            //->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->where('anomalie_cheque_id', 0)
            ->where('dt_enregistrement::date < \''.$date.'\'', null,false)
            ->order_by($this->tb_cheque.'.id_pli')
            ->get()
            ->result();
    }

    public function set_prematch_chq(){
        $date = date("Y-m-d");
        $tab_id_by = array();
        $tab_id_mi = array();

        $upd = false;

        $query = "SELECT pc.id,dp.societe FROM paiement_cheque pc 
                  INNER JOIN view_pli vp ON vp.id_pli = pc.id_pli 
                  INNER JOIN f_pli fp ON fp.id_pli = vp.id_pli 
                  INNER JOIN data_pli dp ON dp.id_pli = vp.id_pli 
                  WHERE vp.flag_traitement > 4 
                  AND vp.flag_traitement NOT IN (16,21,24) 
                  AND pc.etat_matchage = 0 
                  AND pc.flag_suppr = 0 
                  AND vp.recommande != -1 
                  AND (pc.anomalie_cheque_id = 0 OR pc.anomalie_cheque_id IS NULL) 
                  AND dp.dt_enregistrement::date < '".$date."' 
                  ORDER BY dp.societe ASC ";

        $res_query = $this->ged->query($query)->result();

        if($res_query){
            foreach ($res_query as $id){
                if($id->societe == 1){
                    array_push($tab_id_by, $id->id);
                }
                else{
                    array_push($tab_id_mi, $id->id);
                }

            }

            if(!empty($tab_id_by)) {
                $upd = $this->ged->where_in('id', $tab_id_by)
                    ->update($this->tb_cheque, array('etat_matchage' => 1, 'match_societe' => 1));
            }

            if(!empty($tab_id_mi)){
                $upd = $this->ged->where_in('id', $tab_id_mi)
                    ->update($this->tb_cheque, array('etat_matchage' => 1, 'match_societe' => 2));
            }
        }

        //$nb_upd = $this->ged->affected_rows();

        if($upd){
            $data = array(
                'date_traitement' => $date
            );

            $temp = $this->base_reb_temp->select('*')
                                        ->from('syncro_ged_bayard')
                                        ->where('date_traitement', $date)
                                        ->get()
                                        ->result();

            if(empty($temp)){
                $this->base_reb_temp->insert('syncro_ged_bayard', $data);
            }
            /*else{
                $this->base_reb_temp->where('date_traitement', $date)
                                    ->update('syncro_ged_bayard', array('flag' => 0));
            }*/

            return $upd;
        }
        else{
            return false;
        }

    }

    public function set_prematch_societe($id_soc){
        $date = date("Y-m-d");
        $tab_id = array();

        $upd = false;

        $query = "SELECT pc.id FROM paiement_cheque pc 
                  INNER JOIN view_pli vp ON vp.id_pli = pc.id_pli 
                  INNER JOIN f_pli fp ON fp.id_pli = vp.id_pli 
                  INNER JOIN data_pli dp ON dp.id_pli = vp.id_pli 
                  WHERE vp.flag_traitement > 4 
                  AND vp.flag_traitement NOT IN (16,21,24) 
                  AND pc.etat_matchage = 0 
                  AND pc.flag_suppr = 0 
                  AND vp.recommande != -1 
                  AND (pc.anomalie_cheque_id = 0 OR pc.anomalie_cheque_id IS NULL) 
                  AND dp.societe = ".$id_soc." 
                  AND dp.dt_enregistrement::date < '".$date."'";

        $res_query = $this->ged->query($query)->result();

        if($res_query){
            foreach ($res_query as $id){
                array_push($tab_id, $id->id);
            }

            $upd = $this->ged->where_in('id', $tab_id)
                ->update($this->tb_cheque, array('etat_matchage' => 1, 'match_societe' => $id_soc));
        }

        $data = array(
            'date_traitement' => $date
        );

        $temp = $this->base_reb_temp->select('*')
            ->from('syncro_ged_bayard')
            ->where('date_traitement', $date)
            ->get()
            ->result();

        if(empty($temp)){
            $this->base_reb_temp->insert('syncro_ged_bayard', $data);
        }

        return true;

    }

    public function update_etat_match($e_match,$id_soc){
        $date = date("Y-m-d");
        $query = null;

        switch ($e_match){
            case 0 : {
                $query = "UPDATE paiement_cheque 
                      SET etat_matchage = 1 
                      FROM view_pli, data_pli
                      WHERE paiement_cheque.id_pli = view_pli.id_pli 
                      AND paiement_cheque.id_pli = data_pli.id_pli 
                      AND view_pli.flag_traitement > 4 
                      AND view_pli.flag_traitement NOT IN (16,21,24) 
                      AND paiement_cheque.etat_matchage in (0,2,4) 
                      AND paiement_cheque.flag_suppr = 0 
                      AND view_pli.recommande != -1 
                      AND (paiement_cheque.anomalie_cheque_id = 0 OR paiement_cheque.anomalie_cheque_id IS NULL) 
                      AND data_pli.dt_enregistrement::date < '" . $date . "' 
                      AND paiement_cheque.match_societe = ".$id_soc." ";

                break;
            }
            case 1 : {
                $query = "UPDATE paiement_cheque 
                      SET etat_matchage = 2 
                      FROM view_pli, data_pli
                      WHERE paiement_cheque.id_pli = view_pli.id_pli 
                      AND paiement_cheque.id_pli = data_pli.id_pli 
                      AND view_pli.flag_traitement > 4 
                      AND view_pli.flag_traitement NOT IN (16,21,24) 
                      AND paiement_cheque.etat_matchage = 1 
                      AND view_pli.recommande != -1 
                      AND (paiement_cheque.anomalie_cheque_id = 0 OR paiement_cheque.anomalie_cheque_id IS NULL) 
                      AND data_pli.dt_enregistrement::date < '" . $date . "' 
                      AND paiement_cheque.match_societe = ".$id_soc." ";

                break;
            }
            case 2 : {
                $query = "UPDATE paiement_cheque 
                      SET etat_matchage = 3 
                      FROM view_pli, data_pli
                      WHERE paiement_cheque.id_pli = view_pli.id_pli 
                      AND paiement_cheque.id_pli = data_pli.id_pli 
                      AND view_pli.flag_traitement > 4 
                      AND view_pli.flag_traitement NOT IN (16,21,24) 
                      AND paiement_cheque.etat_matchage = 2 
                      AND view_pli.recommande != -1 
                      AND (paiement_cheque.anomalie_cheque_id = 0 OR paiement_cheque.anomalie_cheque_id IS NULL) 
                      AND data_pli.dt_enregistrement::date < '" . $date . "' 
                      AND paiement_cheque.match_societe = ".$id_soc." ";

                break;
            }
        }

        $res = $this->ged->query($query);
        $upd = $this->ged->affected_rows();

        if($res){
            //if($upd > 0){
            if($id_soc == 1){
                $flag_col = 'flag_by';
            }
            else{
                $flag_col = 'flag_mi';
            }
                if($e_match == 2){
                    $flag = 4;
                    $this->update_for_cloturation();

                    $this->ged->where('etat_matchage', 3)
                            ->where('flag_reb', 0)
                            ->update($this->tb_cheque, array('flag_reb' => 1)); // flag envoi reb 1

                    $this->base_reb_temp->where('date_traitement', $date)
                        ->update('syncro_ged_bayard', array($flag_col => $flag));

                    $this->check_flag_syncro($date,$flag);
                }
                if($e_match == 1){
                    $flag = 1;
                    $this->ged->where('etat_matchage', 4)
                            ->where('match_societe',$id_soc)
                    ->update($this->tb_cheque, array('flag_suppr' => 1));

                    $this->base_reb_temp->where('date_traitement', $date)
                        ->update('syncro_ged_bayard', array($flag_col => $flag));

                    $this->check_flag_syncro($date,$flag);
                }
                if($e_match == 0) {
                    $flag = 0;
                    $this->base_reb_temp->where('date_traitement', $date)
                        ->update('syncro_ged_bayard', array($flag_col => $flag));

                    $this->check_flag_syncro($date,$flag);
                }
            //}
            return '';
        }
        else{
            return 'Erreur de mise à jour';
        }

    }

    public function check_flag_syncro($date,$flag){
        $res = $this->base_reb_temp->select('*')
                    ->from('syncro_ged_bayard')
                    ->where('date_traitement', $date)
                    ->get()
                    ->result();

        if($res){
            foreach ($res as $value){
                if(($value->flag_by == $flag) && ($value->flag_mi == $flag)){
                    $this->base_reb_temp->where('date_traitement', $date)
                        ->update('syncro_ged_bayard', array('flag' => $flag));
                }
                else{
                    if($value->flag_by < $value->flag_mi){
                        $this->base_reb_temp->where('date_traitement', $date)
                            ->update('syncro_ged_bayard', array('flag' => $value->flag_by));
                    }
                    if($value->flag_by > $value->flag_mi){
                        $this->base_reb_temp->where('date_traitement', $date)
                            ->update('syncro_ged_bayard', array('flag' => $value->flag_mi));
                    }
                }
            }
        }
    }

    public function update_for_cloturation(){
        // liste des plis ayant été validés pour cloturation
        $valide = $this->ged->select('id_pli')
                            ->from($this->tb_cheque)
                            ->where('etat_matchage', 3)
                            ->where('flag_reb', 0)
                            ->group_by('id_pli')
                            ->get()
                            ->result();

        // liste des plis ayant des chèques retirés pour cloturation
        $retire = $this->ged->select('id_pli')
                            ->from($this->tb_cheque)
                            ->where('etat_matchage', 4)
                            ->where('flag_reb', 0)
                            ->where('flag_suppr', 1)
                            ->group_by('id_pli')
                            ->get()
                            ->result();

        $arr_id = array();

        if(!empty($valide) || !empty($retire)){ // mise à jour des plis en clôturé 26
            foreach ($valide as $pli){
                array_push($arr_id, $pli->id_pli);
            }

            foreach ($retire as $pli){
                array_push($arr_id, $pli->id_pli);
            }

            $data = array(
                'flag_traitement' => 26
            );

            $this->base_64->where_in('id_pli', $arr_id)
                        ->update($this->tb_pli, $data);
        }

        $this->CI->histo->plis($arr_id);

    }

    public function get_etat_ecart(){
        return $this->ged->select('id_etat,libelle')
                        ->from($this->tb_etat)
                        ->where('ecart',1)
                        ->order_by('id_etat','desc')
                        ->get()
                        ->result();
    }

    public function get_etat_abs_adv(){
        return $this->ged->select('id_etat,libelle')
                        ->from($this->tb_etat)
                        ->where('abs_advantage',1)
                        ->order_by('id_etat','asc')
                        ->get()
                        ->result();
    }

    public function get_chq_before_reb(){
        return $this->ged->select($this->vw_pli.'.id_pli, numero_payeur, nom_payeur, numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement,dt_enregistrement,login as saisie_par,'.$this->tb_cheque.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie,'.$this->vw_pli.'.id_lot_saisie,match_societe')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where('flag_traitement > ', 4)
            ->where_not_in('flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
            ->where('anomalie_cheque_id', 0)
            ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 2)
            ->where('flag_suppr', 0)
            ->where($this->vw_pli.'.recommande != -1 ', NULL, FALSE)
            ->get()
            ->result();

    }

    public function get_data_chq($id){
        return $this->ged->from($this->tb_cheque)
                        ->where('id', $id)
                        ->get()
                        ->result();
    }

    public function direct_validation($id_soc){
        $date = date("Y-m-d");
        $flag = 4;

        /*********** Début vérification s'il n'y pas encore de la date du jour dans syncro_ged_bayard ***********/

        $data = array(
            'date_traitement' => $date
        );

        $temp = $this->base_reb_temp->select('*')
            ->from('syncro_ged_bayard')
            ->where('date_traitement', $date)
            ->get()
            ->result();

        if(empty($temp)){
            //Ajout de la date du jour dans syncro_ged_bayard s'il n'y pas pas la date du jour
            $this->base_reb_temp->insert('syncro_ged_bayard', $data);
        }

        /********** Fin vérification date du jour syncro_ged_bayard *******/

        if($id_soc == 1){
            $flag_col = 'flag_by';
        }
        else{
            $flag_col = 'flag_mi';
        }

        $this->base_reb_temp->where('date_traitement', $date)
            ->update('syncro_ged_bayard', array($flag_col => $flag));

        $this->check_flag_syncro($date,$flag);

        return true;
    }

    public function empty_match_temp($id_soc){
        $this->ged->trans_begin();
        if($id_soc == 1){
            $this->ged->query("TRUNCATE TABLE ".$this->tb_mTempBy." ");
        }
        else{
            $this->ged->query("TRUNCATE TABLE ".$this->tb_mTempMi." ");
        }
        if ($this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            return false;
        }
        else
        {
            $this->ged->trans_commit();
            return true;
        }
    }

    public function fill_match_temp($filename,$id_soc){

        $fic = fopen("./match_depot/".$filename, "a+");

        $this->ged->trans_begin();

        $tab=fgetcsv($fic,1024,';'); //eviter en-tête

        while ($tab=fgetcsv($fic,1024,';')){
            $data = array(
                'num_cde' => $tab[0],
                'num_payeur' =>$tab[1],
                'nom_payeur' => utf8_encode($tab[2]),
                'num_recu' => $tab[3],
                'mode_pmt' => $tab[4],
                'num_pmt' => $tab[5],
                'montant_paye' => $tab[6],
                'ctg_dte' => $tab[7],
                'ctg_nme' => utf8_encode($tab[8]),
                'id_societe' => $id_soc
            );

            if($id_soc == 1){
                $this->ged->insert($this->tb_mTempBy, $data);
            }
            else{
                $this->ged->insert($this->tb_mTempMi, $data);
            }
        }

        if ($this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            return false;
        }
        else
        {
            $this->ged->trans_commit();
            return true;
        }
    }

    public function get_match_temp($id_soc){
        if($id_soc == 1){
            return $this->ged->select('*')
                    ->from($this->tb_mTempBy)
                    ->where('id_societe', $id_soc)
                    ->get()
                    ->result();
        }
        else{
            return $this->ged->select('*')
                ->from($this->tb_mTempMi)
                ->where('id_societe', $id_soc)
                ->get()
                ->result();
        }
    }

    public function update_trouve($tab_trouve){
        $this->ged->trans_begin();

        $this->ged->where_in('id', $tab_trouve)
                    ->update($this->tb_cheque, array('trouve' => 1));

        if ($this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            return false;
        }
        else
        {
            $this->ged->trans_commit();
            return true;
        }
    }

    /*public function update_manuel_cloturation(){
        $arr_pli = array(19656);

        $this->base_64->where_in('id_pli', $arr_pli)
            ->update($this->tb_pli, array('flag_traitement' => 26));
        $this->CI->histo->plis($arr_pli);
    }*/

}

