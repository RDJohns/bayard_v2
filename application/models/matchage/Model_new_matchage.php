<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_new_matchage extends CI_Model
{
    private $CI;

    private $ged;
    private $base_64;
    private $base_reb;

    private $tb_pli = TB_pli;
    private $f_pli = FTB_pli;
    private $tb_cheque = TB_cheque;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_dataPli = TB_data_pli;
    private $tb_document = TB_document;
    private $tb_user = TB_user;
    private $tb_statut = TB_statS;
    private $tb_etat = TB_etatMatchage;
    private $tb_mTempBy = TB_matchTempBy;
    private $tb_mTempMi = TB_matchTempMi;
    private $tb_societe = TB_soc;
    private $vw_pli = Vw_pli;
    private $tb_mBy = TB_mstRebBy;
    private $tb_mMi = TB_mstRebMi;
    private $tb_chq = 'cheque_saisie';

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->base_reb = $this->load->database('base_reb', TRUE);
        //$this->base_reb = $this->load->database('base_reb', TRUE);
    }

    public function get_societe(){
        return $this->ged->select('*')
                        ->from($this->tb_societe)
                        ->order_by('id','asc')
                        ->get()
                        ->result();
    }

    public function set_prematch_societe($id_soc){
        $date = date("Y-m-d");
        $tab_id = array();

        $upd = false;

        $query = "SELECT pc.id FROM cheque_saisie pc 
                    INNER JOIN view_pli vp ON vp.id_pli = pc.id_pli  
                    INNER JOIN data_pli dp ON dp.id_pli = vp.id_pli 
                    WHERE vp.recommande != -1 
                    AND date_init IS NULL 
                    AND pc.date_saisie::date < '".$date."' 
                    AND dp.societe = ".$id_soc." ";

        $res_query = $this->ged->query($query)->result();

        if($res_query){
            foreach ($res_query as $id){
                array_push($tab_id, $id->id);
            }

            $upd = $this->ged->where_in('id', $tab_id)
                ->update($this->tb_chq, array('date_init' => date('Y-m-d H:i:s')));
        }

        //$this->set_correct_to_reb($date,$id_soc); // envoyer les chèques corrigés à J-1 en REB

        $data = array(
            'date_traitement' => $date
        );

        $temp = $this->base_reb->select('*')
            ->from('syncro_ged_bayard_new')
            ->where('date_traitement', $date)
            ->get()
            ->result();

        if(empty($temp)){
            $this->base_reb->insert('syncro_ged_bayard_new', $data);
        }

        return true;

    }

    public function set_correct_to_reb($date,$id_soc){
        $sql = "UPDATE paiement_cheque SET matche_verification = 0 
                WHERE 
                id IN (SELECT pc.id FROM paiement_cheque pc 
                INNER JOIN view_pli vp ON vp.id_pli = pc.id_pli  
                INNER JOIN data_pli dp ON dp.id_pli = vp.id_pli 
                WHERE dp.flag_saisie = 1 
                AND pc.etat_matchage = 1  
                AND pc.id_etat_chq = 2  
                AND vp.recommande != -1 
                AND date_correction IS NOT NULL 
                AND flag_matche = 1 
                AND date_correction::date < '".$date."' 
                AND dp.societe = ".$id_soc." ) ";
        
        $this->ged->query($sql);
    }

    public function fill_table_mst($filename,$id_soc)
    {
        
        $fic = fopen("./match_depot/".$filename, "a+");

        $this->ged->trans_begin();

        $tab=fgetcsv($fic,1024,';'); //eviter en-tête

        while ($tab=fgetcsv($fic,1024,';')){
            $data = array(
                'num_cde' => $tab[0],
                'num_payeur' =>$tab[1],
                'nom_payeur' => utf8_encode($tab[2]),
                'num_recu' => $tab[3],
                'mode_pmt' => $tab[4],
                'num_pmt' => trim($tab[5]),
                'montant_paye' => $tab[6],
                'ctg_dte' => $tab[7],
                'ctg_nme' => utf8_encode($tab[8]),
                'id_societe' => $id_soc
            );

            if($id_soc == 1){
                $this->ged->insert($this->tb_mBy, $data);
            }
            else{
                $this->ged->insert($this->tb_mMi, $data);
            }
        }

        if ($this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            return false;
        }
        else
        {
            $this->ged->trans_commit();
            return true;
        }
    }

    public function get_match_temp($id_soc){
        $mst_reb_table = ($id_soc == 1)?$this->tb_mBy:$this->tb_mMi;

        $sql = "select m.id as id_mst, p.id, p.montant,substring(cmc7 from 0 for 8) cmc7, LPAD(m.num_pmt,7,'0') num_pmt, REPLACE (m.montant_paye,',','.')::double precision as montant_adv, m.num_cde, m.num_payeur, m.nom_payeur, m.num_recu, m.mode_pmt, m.num_pmt
                from cheque_saisie p
                inner join ".$mst_reb_table." m on LPAD(m.num_pmt,7,'0') = substring (cmc7 from 0 for 8)  and REPLACE (m.montant_paye,',','.')::double precision = REPLACE (m.montant_paye,',','.')::double precision 
                where REPLACE (m.montant_paye,',','.')::double precision = REPLACE(p.montant,',','.')::double precision
                and m.id_societe = ".$id_soc."
                /*and p.etat_matchage = 1*/
                and p.flag_matche = 0 
                and m.flag_matche = 0 
                /*and p.id_etat_chq = 2*/ 
                ";
                $query = $this->ged->query($sql);
                return $query->result();

    }

    public function get_match_temp_adv($id_soc){
        $mst_reb_table = ($id_soc == 1)?$this->tb_mBy:$this->tb_mMi;
        
        $sql = "select m.id as id_mst, p.id, p.montant,substring(cmc7 from 0 for 8) cmc7, LPAD(m.num_pmt,7,'0') num_pmt, REPLACE (m.montant_paye,',','.')::double precision as montant_adv, m.num_cde, m.num_payeur, m.nom_payeur, m.num_recu, m.mode_pmt, m.num_pmt, m.ctg_dte, m.ctg_nme 
                from cheque_saisie p
                inner join ".$mst_reb_table." m on LPAD(m.num_pmt,7,'0') = substring (cmc7 from 0 for 8)  and REPLACE (m.montant_paye,',','.')::double precision = REPLACE (m.montant_paye,',','.')::double precision 
                where REPLACE (m.montant_paye,',','.')::double precision != REPLACE(p.montant,',','.')::double precision
                and m.id_societe = ".$id_soc."
                /*and p.etat_matchage = 1*/
                and p.flag_matche = 0 
                and m.flag_matche = 0 
                /*and p.id_etat_chq = 2*/
                ";
                $query = $this->ged->query($sql);
                return $query->result();
        
    }

    /**
     * Get correspondance mst_reb_advantage et paiement ==>num_pmt, cmc7, montant
     */
    public function get_info_chq_corr($num_pmt,$num_pay,$id_soc,$id_mst){
        $info = $this->ged->select($this->tb_chq.'.id,'.$this->vw_pli.'.id_pli, numero_payeur, nom_payeur, numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement,dt_enregistrement,login as saisie_par,'.$this->tb_dataPli.'.statut_saisie,'.$this->vw_pli.'.id_lot_saisie,'.$this->tb_chq.'.id_etat_chq')
            ->from($this->vw_pli)
            ->join($this->tb_chq, $this->tb_chq.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            //->where($this->tb_dataPli.'.flag_saisie ', 1)
            //->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,24))
            /*->group_start()
                ->where('anomalie_cheque_id', 0)
                ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()*/
           // ->where('flag_matche', 0)
            ->where($this->tb_chq.'.match_societe', $id_soc)
            ->where($this->vw_pli.'.recommande != -1 ',null,false)
            ->like($this->tb_chq.'.cmc7', $num_pmt, 'after')
            ->like('mv.numero_payeur', $num_pay)
            ->get()
            ->result();
            if($info){
                $table_mst = ($id_soc == 1)?$this->tb_mBy:$this->tb_mMi;
                foreach($info as $value){
                    if((in_array($value->statut_saisie,array(1,6,7,9,10,11))) && ($value->id_etat_chq == 2)){
                        $this->ged->where('id', $value->id)
                            ->update($this->tb_chq, array('flag_matche' => 1));
                        $this->ged->where('id', $id_mst)
                            ->update($table_mst, array('flag_matche' => 1));
                    }
                    else{
                        $this->ged->where('id', $value->id)
                            ->update($this->tb_chq, array('ko_matchage' => 1,'flag_matche' => 0));
                    }

                }

            }
            return $info;

    }

    /**
     * Get chèque trouvé mais avec de montant different
     */

    public function get_info_chq($num_pmt,$num_pay,$id_soc){
        $info = $this->ged->select($this->vw_pli.'.id_pli, numero_payeur, nom_payeur, numero_abonne,'.$this->tb_chq.'.cmc7,'.$this->tb_chq.'.montant,'.$this->tb_statut.'.libelle as flag_traitement,dt_enregistrement,login as saisie_par,'.$this->tb_chq.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie,'.$this->vw_pli.'.id_lot_saisie')
            ->from($this->vw_pli)
            ->join($this->tb_chq, $this->tb_chq.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            ->join($this->tb_cheque, $this->tb_cheque.'.cmc7 = '.$this->tb_chq.'.cmc7','inner')
            //->where($this->tb_dataPli.'.flag_saisie ', 1)
            //->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,24))
            /*->group_start()
                ->where('anomalie_cheque_id', 0)
                ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()*/
           // ->where('flag_matche', 0)
            ->where($this->tb_chq.'.match_societe', $id_soc)
            ->where($this->vw_pli.'.recommande != -1 ',null,false)
            ->like($this->tb_chq.'.cmc7', $num_pmt, 'after')
            ->like('mv.numero_payeur', $num_pay)
            ->get()
            ->result();
            if($info){
                $this->ged->where('id', $info[0]->id)
                            ->update($this->tb_chq, array('trouve_ged' => 1,'ko_matchage' => 2));
            }
            return $info;

    }

    public function get_info_chq_in_vv($num_pmt){
        return $this->ged->select($this->vw_pli.'.id_pli, numero_payeur, nom_payeur, numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement,dt_enregistrement,login as saisie_par,'.$this->tb_cheque.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie,'.$this->vw_pli.'.id_lot_saisie')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where($this->vw_pli.'.flag_traitement > ', 4)
            ->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
            ->where('anomalie_cheque_id', 0)
            ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 1)
            ->where('flag_suppr', 0)
            ->like('cmc7', $num_pmt, 'after')
            ->get()
            ->result();

    }

    public function get_not_in_adv($id_soc){
        return $this->ged->select($this->vw_pli.'.id_pli, numero_payeur, nom_payeur, numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement,dt_enregistrement,login as saisie_par,'.$this->tb_cheque.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie,'.$this->vw_pli.'.id_lot_saisie')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where($this->vw_pli.'.flag_traitement > ', 4)
            ->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
            ->where('anomalie_cheque_id', 0)
            ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 1)
            ->where('match_societe', $id_soc)
            ->where('flag_suppr', 0)
            ->where($this->vw_pli.'.recommande != -1 ', NULL, FALSE)
            ->where($this->tb_dataPli.'.statut_saisie != ', 3)
            ->where('trouve', 0)
            ->get()
            ->result();
    }

    public function get_liste_chq(){

        return $this->ged->select($this->vw_pli.'.id_pli,numero_payeur,nom_payeur,numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement, dt_enregistrement,login as saisie_par,'.$this->tb_cheque.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie')
                ->from($this->vw_pli)
                ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
                ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur, 
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
                ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
                ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
                ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
                //->where_in('flag_traitement', array(9,20,25,14,15))
                ->where('flag_traitement > ', 4)
                ->where_not_in('flag_traitement', array(16,21,24))
                //->where('anomalie_cheque_id', 0)
                ->group_start()
                    ->where('anomalie_cheque_id', 0)
                    ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
                ->group_end()
                ->where('etat_matchage', 1)
                ->where('flag_suppr', 0)
                ->get()
                ->result();

    }

    public function get_cmc_from_vv(){
        $list_cmc7 = $this->ged->select('substring(cmc7,1,7) as cmc7')
           ->from($this->vw_pli)
           ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
           ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
               string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
               FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
           ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
           ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
           //->where_in('flag_traitement', array(9,20,25,14,15))
           ->where($this->vw_pli.'.flag_traitement > ', 4)
           ->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,24))
           //->where('anomalie_cheque_id', 0)
           ->group_start()
               ->where('anomalie_cheque_id', 0)
               ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
           ->group_end()
           ->where('etat_matchage', 1)
           ->where('flag_suppr', 0)
           ->where('statut_saisie != ', 3)
           ->get()
           ->result();

        $arr_pmt  = array();

        foreach ($list_cmc7 as $cmc){
            array_push($arr_pmt, $cmc->cmc7);
        }

        return $arr_pmt;
   }

   public function get_absent_in_adv(){
    $list_cmc7 = $this->ged->select('substring(cmc7,1,7) as cmc7')
        ->from($this->vw_pli)
        ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
        ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur, 
            string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
            FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
        ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
        //->where_in('flag_traitement', array(9,20,25,14,15))
        ->where('flag_traitement > ', 4)
        ->where_not_in('flag_traitement', array(16,21,24))
        //->where('anomalie_cheque_id', 0)
        ->group_start()
            ->where('anomalie_cheque_id', 0)
            ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
        ->group_end()
        ->where('etat_matchage', 1)
        ->where('flag_suppr', 0)
        ->get()
        ->result();

    $arr_pmt  = array();

    foreach ($list_cmc7 as $cmc){
        array_push($arr_pmt, $cmc->cmc7);
    }

    return $arr_pmt;
    }

    public function get_etat_ecart(){
        return $this->ged->select('id_etat,libelle')
                        ->from($this->tb_etat)
                        ->where('ecart',1)
                        ->order_by('id_etat','desc')
                        ->get()
                        ->result();
    }

    public function get_etat_abs_adv(){
        return $this->ged->select('id_etat,libelle')
                        ->from($this->tb_etat)
                        ->where('abs_advantage',1)
                        ->order_by('id_etat','asc')
                        ->get()
                        ->result();
    }

    public function get_data_chq($id){
        return $this->ged->from($this->tb_cheque)
                        ->where('id', $id)
                        ->get()
                        ->result();
    }

    public function update_chq($id, $data){
        $daty = date("Y-m-d H:i:s");
        return $this->ged->where('id', $id)
                         ->set('matche_verification', 2)
                         //->set('flag_matche', 1)
                         ->set('date_correction', $daty)
                         ->update($this->tb_chq, $data);
    }

    public function update_montant($id, $data){
        $daty = date("Y-m-d H:i:s");
        return $this->ged->where('id', $id)
                ->set('matche_verification', 2)
                ->set('montant', 1)
                ->set('date_correction', $daty)
                ->update($this->tb_chq, $data);
    }

    public function get_doc($id_pli){
    return $this->base_64->select('id_document, id_pli, n_ima_base64_recto, n_ima_base64_verso')
                ->from($this->tb_document)
                ->where('id_pli', $id_pli)
                ->get()
                ->result();
    }

    public function update_flag_syncro($id_soc){
        $date = date("Y-m-d");
        $flag = 1;
        $flag_col = ($id_soc == 1)?'flag_by':'flag_mi';
        return $this->base_reb->where('date_traitement', $date)
                        ->update('syncro_ged_bayard_new', array($flag_col => $flag));

        //$this->check_flag_syncro($date,$flag);
    }

    public function check_flag_syncro($date){
        $res = $this->base_reb->select('*')
                    ->from('syncro_ged_bayard_new')
                    ->where('date_traitement', $date)
                    ->get()
                    ->result();

        if($res){
            foreach ($res as $value){
                $flag = ($value->flag_by < $value->flag_mi)?$value->flag_by:$value->flag_mi;
                
                $this->base_reb->where('date_traitement', $date)
                    ->update('syncro_ged_bayard_new', array('flag' => $flag));
               
            }
        }
    }

    public function direct_validation($id_soc){
        $date = date("Y-m-d");
        $flag = 1;

        /*********** D�but v�rification s'il n'y pas encore de la date du jour dans syncro_ged_bayard ***********/

        $data = array(
            'date_traitement' => $date
        );

        $temp = $this->base_reb->select('*')
            ->from('syncro_ged_bayard_new')
            ->where('date_traitement', $date)
            ->get()
            ->result();

        if(empty($temp)){
            //Ajout de la date du jour dans syncro_ged_bayard_new s'il n'y pas pas la date du jour
            $this->base_reb->insert('syncro_ged_bayard_new', $data);
        }

        /********** Fin v�rification date du jour syncro_ged_bayard *******/

        if($id_soc == 1){
            $flag_col = 'flag_by';
        }
        else{
            $flag_col = 'flag_mi';
        }

        $this->base_reb->where('date_traitement', $date)
            ->update('syncro_ged_bayard_new', array($flag_col => $flag));

        //$this->check_flag_syncro($date);

        return true;
    }


}