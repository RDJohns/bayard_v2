<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_match_ckd extends CI_Model{

    private $CI;

    private $ged;
    private $base_64;
    private $base_reb_temp;

    private $tb_pli = TB_pli;
    private $tb_cheque = TB_paieChqKd;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_dataPli = TB_data_pli;
    private $tb_document = TB_document;
    private $tb_user = TB_user;
    private $tb_statut = TB_statS;
    private $tb_etatCkd = TB_etatCkd;
    private $tb_ckd = TB_chqKd;
    private $tb_matchTemp = TB_matchCkdTemp;
    private $vw_pli = Vw_pli;

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->base_reb_temp = $this->load->database('base_reb_temp', TRUE);
    }

    public function get_info_chq($societe,$num_pay,$code_pmt,$id_lot){
         $info = $this->ged->select($this->vw_pli.'.id_pli, 
         numero_payeur,montant,dt_enregistrement,
         matr_gpao as saisie_par,'.$this->tb_cheque.'.id_doc,
         '.$this->tb_statut.'.libelle as statut_saisie,'.$this->vw_pli.'.id_lot_saisie,
         '.$this->tb_etatCkd.'.id_doc,'.$this->tb_ckd.'.type_cheque_cadeau as type_chq_kdo,'.$this->tb_ckd.'.id as id_kdo')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement WHERE numero_payeur LIKE \'%'.$num_pay.'%\' GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_etatCkd, $this->tb_etatCkd.'.id_pli = '.$this->tb_cheque.'.id_pli AND '.$this->tb_etatCkd.'.id_doc = '.$this->tb_cheque.'.id_doc', 'inner')
            ->join($this->tb_ckd, $this->tb_cheque.'.type_chq_kdo = '.$this->tb_ckd.'.id', 'inner')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where('flag_traitement > ', 4)
            ->where_not_in('flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            /*->group_start()
            ->where('anomalie_cheque_id', 0)
            ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()*/
            ->where('etat_matchage', 1)
            ->where('flag_suppr', 0)
            ->where($this->tb_dataPli.'.societe', $societe)
            ->where($this->tb_ckd.'.type_cheque_cadeau', $code_pmt)
            ->like($this->vw_pli.'.id_lot_saisie', $id_lot)
            ->like('mv.numero_payeur', $num_pay)
            ->get()
            ->result();

         if(!empty($info)){
             foreach ($info as $value){
                $this->ged->where('id_pli', $value->id_pli)
                 ->where('id_doc', $value->id_doc)
                 ->update($this->tb_etatCkd, array('trouve' => 1));
             }
         }

         return $info;

    }

    public function get_not_in_adv(){
        return $this->ged->select($this->vw_pli.'.id_pli, 
        numero_payeur,montant,dt_enregistrement,
        matr_gpao as saisie_par,'.$this->tb_cheque.'.id_doc,
        '.$this->tb_statut.'.libelle as statut_saisie,'.$this->vw_pli.'.id_lot_saisie,
        '.$this->tb_etatCkd.'.id_doc,'.$this->tb_ckd.'.type_cheque_cadeau as type_chq_kdo,'.$this->tb_ckd.'.id as id_kdo')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_etatCkd, $this->tb_etatCkd.'.id_pli = '.$this->tb_cheque.'.id_pli AND '.$this->tb_etatCkd.'.id_doc = '.$this->tb_cheque.'.id_doc', 'inner')
            ->join($this->tb_ckd, $this->tb_cheque.'.type_chq_kdo = '.$this->tb_ckd.'.id', 'inner')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            ->where('flag_traitement > ', 4)
            ->where_not_in('flag_traitement', array(16,21,24))
            ->where('etat_matchage', 1)
            ->where('trouve', 0)
            ->get()
            ->result();
    }

    public function get_absent_in_adv(){
        $list_cmc7 = $this->ged->select('substring(cmc7,1,7) as cmc7')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur, 
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where('flag_traitement > ', 4)
            ->where_not_in('flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
            ->where('anomalie_cheque_id', 0)
            ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 1)
            ->where('flag_suppr', 0)
            ->get()
            ->result();

        $arr_pmt  = array();

        foreach ($list_cmc7 as $cmc){
            array_push($arr_pmt, $cmc->cmc7);
        }

        return $arr_pmt;
    }

    public function get_doc($id_pli){
        return $this->base_64->select('id_document, id_pli, n_ima_base64_recto, n_ima_base64_verso')
            ->from($this->tb_document)
            ->where('id_pli', $id_pli)
            ->get()
            ->result();
    }

    public function update_chq($id_pli, $id_doc, $state, $montant, $typekdo){

        $this->ged->trans_begin();

        if($state != ''){
            $this->ged->where('id_pli', $id_pli)
                        ->where('id_doc', $id_doc)
                        ->update($this->tb_etatCkd, array('etat_matchage' => $state));
        }

        if($state == '1'){
            if($montant != ''){
                $this->ged->where('id_pli', $id_pli)
                    ->where('id_doc', $id_doc)
                    ->update($this->tb_cheque, array('montant' => $montant));
            }
            if($typekdo != ''){
                $this->ged->where('id_pli', $id_pli)
                    ->where('id_doc', $id_doc)
                    ->update($this->tb_cheque, array('type_chq_kdo' => $typekdo));
            }
        }

        if ($this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            return false;
        }
        else
        {
            $this->ged->trans_commit();
            return true;
        }

    }

    public function update_comment($id_pli, $id_doc, $data){
        return $this->ged->where('id_pli', $id_pli)
            ->where('id_doc', $id_doc)
            ->update($this->tb_etatCkd, $data);
    }

    public function update_etat($id, $data){
        return $this->ged->where('id', $id)
            ->update($this->tb_cheque, $data);
    }

    public function test_date(){
        $date = date("Y-m-d");
        return $this->ged->select($this->vw_pli.'.id_pli,dt_enregistrement')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur, 
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            //->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->where('anomalie_cheque_id', 0)
            ->where('dt_enregistrement::date < \''.$date.'\'', null,false)
            ->order_by($this->tb_cheque.'.id_pli')
            ->get()
            ->result();
    }

    public function set_prematch_chq(){
        $date_sem_prec = date('Y-m-d', strtotime('last Sunday'));
        $query = "
                  INSERT INTO ".$this->tb_etatCkd." (id_pli, id_doc, etat_matchage) (SELECT pcc.id_pli, pcc.id_doc, 1 FROM ".$this->tb_cheque." pcc 
                  LEFT JOIN ".$this->vw_pli." vp ON vp.id_pli = pcc.id_pli 
                  LEFT JOIN ".$this->tb_dataPli." dp ON dp.id_pli = pcc.id_pli 
                  LEFT JOIN ".$this->tb_etatCkd." ecc ON (ecc.id_pli = pcc.id_pli AND ecc.id_doc = pcc.id_doc) 
                  WHERE ecc.id_pli IS NULL AND ecc.id_doc IS NULL 
                  AND dt_enregistrement::date <= '".$date_sem_prec."' 
                  AND vp.flag_traitement > 4 
                  AND vp.flag_traitement NOT IN (16,21,24))
                  ";

        $this->ged->query($query);

        $this->ged->where('etat_matchage',0)
                    ->where('date_matchage IS NULL', null,false)
                    ->update($this->tb_etatCkd, array('etat_matchage' => 1));
    }

    public function update_etat_match($e_match){
        $date = date("Y-m-d");
        $query = null;

        switch ($e_match){
            case 0 : $query = "UPDATE ".$this->tb_etatCkd." SET etat_matchage = 1   
                               WHERE etat_matchage = 0 AND date_matchage IS NULL";
                break;
            case 1 : $query = "";
                break;
            case 2 : $query = "UPDATE ".$this->tb_etatCkd." SET etat_matchage = 2, date_matchage = NOW()  
                               WHERE etat_matchage = 1 AND date_matchage IS NULL";
                break;
        }

        $res = $this->ged->query($query);
        //$upd = $this->ged->affected_rows();

        if($res){
            if($e_match == 2){
                $query = "UPDATE ".$this->tb_etatCkd." SET date_matchage = NOW()  
                               WHERE etat_matchage = 3 AND date_matchage IS NULL";
                $this->ged->query($query);
            }
            return '';
        }
        else{
            return 'Erreur de mise à jour';
        }

    }

    public function update_for_cloturation(){
        // liste des plis ayant été validés pour cloturation
        $valide = $this->ged->select('id_pli')
            ->from($this->tb_cheque)
            ->where('etat_matchage', 3)
            ->where('flag_reb', 0)
            ->group_by('id_pli')
            ->get()
            ->result();

        // liste des plis ayant des chèques retirés pour cloturation
        $retire = $this->ged->select('id_pli')
            ->from($this->tb_cheque)
            ->where('etat_matchage', 4)
            ->where('flag_reb', 0)
            ->where('flag_suppr', 1)
            ->group_by('id_pli')
            ->get()
            ->result();

        $arr_id = array();

        if(!empty($valide) || !empty($retire)){ // mise à jour des plis en clôturé 26
            foreach ($valide as $pli){
                array_push($arr_id, $pli->id_pli);
            }

            foreach ($retire as $pli){
                array_push($arr_id, $pli->id_pli);
            }

            $data = array(
                'flag_traitement' => 26
            );

            $this->base_64->where_in('id_pli', $arr_id)
                ->update($this->tb_pli, $data);
        }

        $this->CI->histo->plis($arr_id);

    }

    public function get_etat_ecart(){
        return $this->ged->select('id_etat,libelle')
            ->from($this->tb_etat)
            ->where('ecart',1)
            ->order_by('id_etat','desc')
            ->get()
            ->result();
    }

    public function get_etat_abs_adv(){
        return $this->ged->select('id_etat,libelle')
            ->from($this->tb_etat)
            ->where('abs_advantage',1)
            ->order_by('id_etat','asc')
            ->get()
            ->result();
    }

    public function get_chq_before_reb(){
        return $this->ged->select($this->vw_pli.'.id_pli, numero_payeur, nom_payeur, numero_abonne,cmc7,montant,'.$this->tb_statut.'.libelle as flag_traitement,dt_enregistrement,login as saisie_par,'.$this->tb_cheque.'.id,'.$this->tb_cheque.'.id_doc,'.$this->tb_dataPli.'.statut_saisie,'.$this->vw_pli.'.id_lot_saisie')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = '.$this->tb_dataPli.'.statut_saisie')
            ->join($this->tb_user, $this->tb_user.'.id_utilisateur = '.$this->vw_pli.'.saisie_par', 'left')
            //->where_in('flag_traitement', array(9,20,25,14,15))
            ->where('flag_traitement > ', 4)
            ->where_not_in('flag_traitement', array(16,21,24))
            //->where('anomalie_cheque_id', 0)
            ->group_start()
            ->where('anomalie_cheque_id', 0)
            ->or_where('anomalie_cheque_id IS NULL', NULL, FALSE)
            ->group_end()
            ->where('etat_matchage', 2)
            ->where('flag_suppr', 0)
            ->get()
            ->result();

    }

    public function get_data_chq($id){
        return $this->ged->from($this->tb_cheque)
            ->where('id', $id)
            ->get()
            ->result();
    }

    public function get_type_kdo(){
        return $this->ged->select('id,type_cheque_cadeau')
            ->from($this->tb_ckd)
            ->where('actif',1)
            ->order_by('id','asc')
            ->get()
            ->result();
    }

    public function empty_match_temp(){
        $this->ged->trans_begin();
        $this->ged->query("TRUNCATE TABLE ".$this->tb_matchTemp." ");
        if ($this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            return false;
        }
        else
        {
            $this->ged->trans_commit();
            return true;
        }
    }

    public function fill_match_temp($filename){

        $this->load->library('excel');

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);

        $objPHPExcel = $objReader->load("./ckd_depot/".$filename);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        $rows = array();

        $this->ged->trans_begin();

        for ($row = 2; $row <= $highestRow; ++$row){

            $societe = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
            $montant_adv = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
            $num_payeur_adv = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
            $code_pmt_adv = $objWorksheet->getCellByColumnAndRow(17, $row)->getValue();
            $date_lot_adv = $objWorksheet->getCellByColumnAndRow(18, $row)->getValue();
            $nom_lot_adv = $objWorksheet->getCellByColumnAndRow(19, $row)->getValue();
            $code_user_adv = $objWorksheet->getCellByColumnAndRow(20, $row)->getValue();

            $number = preg_replace('~\D~', '', $nom_lot_adv);
            $string = preg_replace('/[^a-zA-Z]/', '', $nom_lot_adv);

            $nom_lot_adv = $number.'_'.$string;

            $data = array(
                'societe' => $societe,
                'montant' => $montant_adv,
                'num_client' => $num_payeur_adv,
                'code_pmt' => $code_pmt_adv,
                'date_lot' => $date_lot_adv,
                'nom_lot' => $nom_lot_adv,
                'code_utilisateur' => $code_user_adv
            );

            $this->ged->insert($this->tb_matchTemp, $data);
        }

        if ($this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            return false;
        }
        else
        {
            $this->ged->trans_commit();
            return true;
        }
    }

    public function get_match_temp(){
        return $this->ged->select('*')
            ->from($this->tb_matchTemp)
            ->get()
            ->result();
    }

    public function get_recap_reporting_ckd($date1,$date2){

        $where_date = '';

        if($date2 == ''){
            $where_date = " date_matchage::date = '".$date1."' ";
        }
        else{
            $where_date = " date_matchage::date between '".$date1."' AND '".$date2."' ";
        }

        $query = "
            SELECT cmd.commande, lire.nb as nb_lire, lire.montant as mt_lire, 
            cadhoc.nb as nb_cadhoc, cadhoc.montant as mt_cadhoc,
            cado.nb as nb_cado, cado.montant as mt_cado,
            tirgrp.nb as nb_tirgrp, tirgrp.montant as mt_tirgrp,
            accor.nb as nb_accor, accor.montant as mt_accor 
            FROM 
            (
                SELECT vl.commande FROM view_lot vl 
                INNER JOIN view_pli vp ON vp.id_lot_numerisation = vl.id_lot_numerisation 
                INNER JOIN ".$this->tb_cheque." pct ON pct.id_pli = vp.id_pli 
                INNER JOIN ".$this->tb_etatCkd." ecc ON (pct.id_pli = ecc.id_pli AND pct.id_doc = ecc.id_doc) 
                LEFT JOIN f_pli fp ON vp.id_pli = fp.id_pli 
                WHERE ".$where_date."
                AND etat_matchage = 2 
                GROUP BY vl.commande 
            ) as cmd 
            LEFT JOIN 
            (
                SELECT vl.commande, COUNT(*) as nb, SUM(montant::float) as montant FROM ".$this->tb_cheque." pct
                INNER JOIN ".$this->tb_etatCkd." ecc ON (pct.id_pli = ecc.id_pli AND pct.id_doc = ecc.id_doc)
                INNER JOIN view_pli vp ON vp.id_pli = pct.id_pli 
                LEFT JOIN f_pli fp ON vp.id_pli = fp.id_pli 
                INNER JOIN view_lot vl ON vl.id_lot_numerisation = vp.id_lot_numerisation 
                WHERE type_chq_kdo = 1 
                AND ".$where_date." 
                AND etat_matchage = 2 
                GROUP BY vl.commande
            ) as lire ON lire.commande = cmd.commande 
            LEFT JOIN
            ( 
                SELECT vl.commande, COUNT(*) as nb, SUM(montant::float) as montant FROM ".$this->tb_cheque." pct
                INNER JOIN ".$this->tb_etatCkd." ecc ON (pct.id_pli = ecc.id_pli AND pct.id_doc = ecc.id_doc)
                INNER JOIN view_pli vp ON vp.id_pli = pct.id_pli 
                LEFT JOIN f_pli fp ON vp.id_pli = fp.id_pli 
                INNER JOIN view_lot vl ON vl.id_lot_numerisation = vp.id_lot_numerisation 
                WHERE type_chq_kdo = 2 
                AND ".$where_date." 
                AND etat_matchage = 2 
                GROUP BY vl.commande
            ) as cadhoc ON cadhoc.commande = cmd.commande
            LEFT JOIN
            ( 
                SELECT vl.commande, COUNT(*) as nb, SUM(montant::float) as montant FROM ".$this->tb_cheque." pct
                INNER JOIN ".$this->tb_etatCkd." ecc ON (pct.id_pli = ecc.id_pli AND pct.id_doc = ecc.id_doc)
                INNER JOIN view_pli vp ON vp.id_pli = pct.id_pli 
                LEFT JOIN f_pli fp ON vp.id_pli = fp.id_pli 
                INNER JOIN view_lot vl ON vl.id_lot_numerisation = vp.id_lot_numerisation 
                WHERE type_chq_kdo = 3 
                AND ".$where_date." 
                AND etat_matchage = 2 
                GROUP BY vl.commande
            ) as cado ON cado.commande = cmd.commande 
            LEFT JOIN
            ( 
                SELECT vl.commande, COUNT(*) as nb, SUM(montant::float) as montant FROM ".$this->tb_cheque." pct
                INNER JOIN ".$this->tb_etatCkd." ecc ON (pct.id_pli = ecc.id_pli AND pct.id_doc = ecc.id_doc)
                INNER JOIN view_pli vp ON vp.id_pli = pct.id_pli 
                LEFT JOIN f_pli fp ON vp.id_pli = fp.id_pli 
                INNER JOIN view_lot vl ON vl.id_lot_numerisation = vp.id_lot_numerisation 
                WHERE type_chq_kdo = 4 
                AND ".$where_date." 
                AND etat_matchage = 2 
                GROUP BY vl.commande
            ) as tirgrp ON tirgrp.commande = cmd.commande 
            LEFT JOIN
            ( 
                SELECT vl.commande, COUNT(*) as nb, SUM(montant::float) as montant FROM ".$this->tb_cheque." pct
                INNER JOIN ".$this->tb_etatCkd." ecc ON (pct.id_pli = ecc.id_pli AND pct.id_doc = ecc.id_doc)
                INNER JOIN view_pli vp ON vp.id_pli = pct.id_pli 
                LEFT JOIN f_pli fp ON vp.id_pli = fp.id_pli 
                INNER JOIN view_lot vl ON vl.id_lot_numerisation = vp.id_lot_numerisation 
                WHERE type_chq_kdo = 5 
                AND ".$where_date." 
                AND etat_matchage = 2 
                GROUP BY vl.commande
            ) as accor ON accor.commande = cmd.commande 
            ORDER BY cmd.commande ASC
        ";

        $res_query = $this->ged->query($query)->result();

        return $res_query;
    }

    public function get_detail_reporting_ckd($date1,$date2){

        $where_date = '';

        if($date2 == ''){
            $where_date = " date_matchage::date = '".$date1."' ";
        }
        else{
            $where_date = " date_matchage::date between '".$date1."' AND '".$date2."' ";
        }

        $query = "
            SELECT commande, vp.id_pli, lot_scan, vp.id_lot_saisie, (n_ima_recto || ';' || n_ima_verso)::text as nom_image, date_courrier, date_matchage, montant FROM view_pli vp 
            INNER JOIN view_lot vl ON vl.id_lot_numerisation = vp.id_lot_numerisation 
            INNER JOIN ".$this->tb_cheque." pct ON pct.id_pli = vp.id_pli 
            LEFT JOIN f_pli fp ON fp.id_pli = vp.id_pli 
            INNER JOIN ".$this->tb_etatCkd." ecc ON (pct.id_pli = ecc.id_pli AND pct.id_doc = ecc.id_doc) 
            INNER JOIN f_document_si fd ON pct.id_doc = fd.id_document 
            WHERE etat_matchage = 2 
            AND ".$where_date." 
            ORDER BY commande ASC 
        ";

        $res_query = $this->ged->query($query)->result();

        return $res_query;
    }

    /*public function update_manuel_cloturation(){
        $arr_pli = array(19656);

        $this->base_64->where_in('id_pli', $arr_pli)
            ->update($this->tb_pli, array('flag_traitement' => 26));
        $this->CI->histo->plis($arr_pli);
    }*/

}

