<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pli_saisie extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    
    private $tb_pli = TB_pli;
    private $tb_soc = TB_soc;
    private $tb_user = TB_user;
    private $tb_titre = TB_titre;
    private $tb_cheque = TB_cheque;
    private $tb_chqSaisie = TB_chequeSaisie;
    private $tb_chequeAnoCorrige = TB_chequeAnoCorrige;
    private $tb_etatChq = TB_etatChq;
    private $tb_paieChqAnomali = TB_paieChqAnom;
    private $tb_histoChq = TB_histoChq;
    private $tb_tpUser = TB_tpUser;
    private $tb_lstCirc = TB_lstCirc;
    private $tb_paiement = TB_paiement;
    private $vw_grPliPos = Vw_grPliPos;
    private $tb_lotSaisie = TB_lotSaisie;
    private $tb_mdPaie = TB_mode_paiement;
    private $tb_assignation = TB_assignation;
    private $tb_groupe_pli = TB_groupe_pli;
    private $tb_data_pli = TB_data_pli;
    private $tb_anomChq = TB_anomChq;
    private $tb_motifKo = TB_motifKo;
    private $tb_grUser = TB_grUser;
    private $tb_doc = TB_document;
    private $tb_statS = TB_statS;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_mvmntSaisie = TB_mvmntSaisie;
    private $tb_typo = TB_typo;
    private $vw_pli = Vw_pli;
    private $ftb_pli = FTB_pli;
    private $ftb_lotNum = FTB_lotNumerisation;
    private $tb_mtfRj = TB_mtfRj;
    private $tb_mtfKoScn = TB_mtfKoScn;
    private $tb_chqKd = TB_chqKd;
    private $tb_paieChqKd = TB_paieChqKd;
    private $tb_paieEsp = TB_paieEsp;
    private $fvw_pliCourrier = FVW_pliCourrier;
    private $tb_cba = TB_cba;
    private $tb_saisieCtrlField = TB_saisiCtrlField;
    private $tb_preData = TB_preData;
    private $tb_motifConsigne = TB_motifConsigne;
    private $tb_assignationPli = TB_assignationPli;

    private $nb_inc_id_lot = 0;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function piocher_pli_for_saisie($id_utilisateur, $id_view_groupe_pli_possible=NULL, $rejet_batch=FALSE){
        if(is_null($id_view_groupe_pli_possible) or empty($id_view_groupe_pli_possible)){
            return NULL;
        }
        $pli = $this->current_pli_for_saisie($id_utilisateur, $id_view_groupe_pli_possible, $rejet_batch);
        if (is_null($pli)) {
            $pli = $this->get_pli_for_saisie($id_utilisateur, $id_view_groupe_pli_possible, $rejet_batch);
        }
        return $pli;
    }

    private function current_pli_for_saisie($id_utilisateur, $id_view_groupe_pli_possible, $rejet_batch){
        $data = $this->ged
            ->select('tb_pli.*')
            ->select('tb_gr_pli.*')
            ->select('tb_gr_poss.*')
            ->select('tb_gr_poss.typologie typo')
            ->from($this->fvw_pliCourrier.' tb_pli')
            ->where('tb_pli.flag_traitement', 4)
            ->where('saisie_par', $id_utilisateur)
            ->join($this->tb_groupe_pli.' tb_gr_pli', 'tb_pli.id_groupe_pli = tb_gr_pli.id_groupe_pli', 'INNER')
            ->where('id_view_groupe_pli_possible', $id_view_groupe_pli_possible)
            ->join($this->vw_grPliPos.' tb_gr_poss', 'tb_gr_pli.id_view_groupe_pli_possible = tb_gr_poss.id_groupe_pli_possible', 'INNER')
            ->join($this->tb_data_pli.' tb_data_pli', 'tb_pli.id_pli = tb_data_pli.id_pli', 'INNER')
            ->where('tb_data_pli.statut_saisie', 1)
            ->where('recommande != -1 ', NULL, FALSE)
            ->where('flag_rejet_batch', ($rejet_batch ? 1 : 0))
            ->limit(1)
            ->get()->result();
        if (count($data) > 0) {
            $this->CI->histo->action(10, '', $data[0]->id_pli);
            $this->CI->histo->pli($data[0]->id_pli);
            return $data[0];
        } else {
            $this->unlocked_pli_saisie_for_user($id_utilisateur);//unlock all saisie pli for me
            return NULL;
        }
        
    }

    private function get_pli_for_saisie($id_utilisateur, $id_view_groupe_pli_possible, $rejet_batch){
        $reserved_plis = $this->ged
            ->select('id_pli')
            ->from($this->tb_assignationPli)
            ->where('operateur != ', $id_utilisateur)
            ->where('traitement', 3)
            ->get_compiled_select();
        $this->ged->reset_query();

        $plis_for_me = $this->ged
            ->select('id_pli id_pli_for_me')
            ->select('operateur me')
            ->from($this->tb_assignationPli)
            ->where('operateur', $id_utilisateur)
            ->where('traitement', 3)
            ->get_compiled_select();
        $this->ged->reset_query();

        $this->ged->order_by('COALESCE(me, 0) DESC');
        if($this->CI->session->niveau_op == 1){
            $this->ged->where('tb_data_pli.niveau_saisie', 1);
        }else{
            $this->ged->order_by('tb_data_pli.niveau_saisie', 'DESC');
        }

        $data = $this->ged
            ->select('tb_pli.*')
            ->select('tb_gr_pli.*')
            ->select('tb_gr_poss.*')
            ->select('tb_gr_poss.typologie typo')
            // ->select('tb_pli_for_me.*')
            ->from($this->fvw_pliCourrier.' tb_pli')
            ->where('tb_pli.flag_traitement', 3)
            ->join($this->tb_groupe_pli.' tb_gr_pli', 'tb_pli.id_groupe_pli = tb_gr_pli.id_groupe_pli', 'INNER')
            ->where('tb_gr_pli.id_view_groupe_pli_possible', $id_view_groupe_pli_possible)
            ->join($this->tb_assignation.' tb_assign', 'tb_gr_pli.id_view_groupe_pli_possible = tb_assign.id_view_groupe_pli_possible', 'INNER')
            ->join($this->tb_grUser.' tb_gr_user', 'tb_assign.id_groupe_utilisateur = tb_gr_user.id_groupe_utilisateur', 'INNER')
            ->join($this->tb_user.' tb_user', 'tb_gr_user.id_groupe_utilisateur = tb_user.id_groupe_utilisateur', 'INNER')
            ->join($this->tb_data_pli.' tb_data_pli', 'tb_pli.id_pli = tb_data_pli.id_pli', 'INNER')
            ->join('('.$plis_for_me.') tb_pli_for_me', 'id_pli_for_me = tb_pli.id_pli', 'LEFT')
            ->where('tb_data_pli.statut_saisie', 1)
            ->where('id_utilisateur', $id_utilisateur)
            ->join($this->vw_grPliPos.' tb_gr_poss', 'tb_gr_pli.id_view_groupe_pli_possible = tb_gr_poss.id_groupe_pli_possible', 'INNER')
            ->where('recommande != -1 ', NULL, FALSE)
            ->where('flag_rejet_batch', ($rejet_batch ? 1 : 0))
            ->where(' tb_pli.id_pli NOT IN ('.$reserved_plis.') ', NULL, FALSE)
            // ->order_by('COALESCE(me, 0) DESC')
            ->order_by('recommande', 'DESC')
            ->order_by('tb_pli.date_courrier', 'ASC')
            ->order_by('id_regroupement', 'ASC')
            ->order_by('tb_pli.id_pli', 'ASC')
            ->limit(1)
            ->get()->result();
            
        if(count($data) > 0){
            $pli = $data[0];
            if($this->locked_pli_for_saisie($id_utilisateur, $pli->id_pli)){
                $this->CI->histo->action(4, '', $pli->id_pli);
                $this->CI->histo->pli($pli->id_pli);
                return $pli;
            }else{
                return $this->get_pli_for_saisie($id_utilisateur, $id_view_groupe_pli_possible, $rejet_batch);
            }
        }
        return NULL;
    }

    private function locked_pli_for_saisie($id_utilisateur, $id_pli){
        $data_lock_pli = array(
            'flag_traitement' => 4
            ,'saisie_par' => $id_utilisateur
        );
        $data_lock_datapli = array(
            'flag_traitement' => 4
        );
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        $this->base_64
            ->where('id_pli', $id_pli)
            ->where('flag_traitement', 3)
            ->set($data_lock_pli)
            ->update($this->tb_pli);
        $this->ged
            ->where('id_pli', $id_pli)
            ->where('statut_saisie', 1)
            ->set($data_lock_datapli)
            ->update($this->tb_data_pli);
        if($this->base_64->affected_rows() < 1 || $this->ged->affected_rows() < 1 || $this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            return FALSE;
        }else{
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            return TRUE;
        }
    }

    public function gr_pli_pos_for_user($id_utilisateur, $spec=NULL){
        if(is_null($spec)){
            $this->ged->select('tb_gr_pli_pos.*')
                ->order_by('tb_gr_pli_pos.typologie')
                ->order_by('tb_gr_pli_pos.soc')
                ->order_by('tb_gr_pli_pos.id_groupe_paiement');
        }else{
            $this->ged
                ->distinct()
                ->select('tb_gr_pli_pos.id_'.$spec)
                ->select('tb_gr_pli_pos.'.$spec)
                ->order_by($spec);
        }
        return $this->ged
            ->from($this->tb_user.' tb_user')
            ->where('id_utilisateur', $id_utilisateur)
            ->join($this->tb_grUser.' tb_gr_user', 'tb_gr_user.id_groupe_utilisateur = tb_user.id_groupe_utilisateur', 'INNER')
            ->join($this->tb_assignation.' tb_assign', 'tb_assign.id_groupe_utilisateur = tb_gr_user.id_groupe_utilisateur', 'INNER')
            ->join($this->vw_grPliPos.' tb_gr_pli_pos', 'tb_assign.id_view_groupe_pli_possible = tb_gr_pli_pos.id_groupe_pli_possible', 'INNER')
            ->get()->result();
    }

    public function unlocked_pli_saisie_for_user($id_utilisateur){
        $data_plis_locked = $this->base_64
            ->from($this->tb_pli)
            ->where('flag_traitement', 4)
            ->where('saisie_par', $id_utilisateur)
            ->get()->result();
        foreach ($data_plis_locked as $pli_locked) {
            $this->unlocked_saisie($pli_locked->id_pli);
        }
    }

    public function unlocked_saisie($id_pli){
        $data_lock_pli = array(
            'saisie_par' => NULL
            ,'flag_traitement' => 3
        );
        $data_lock_datapli = array(
            'flag_traitement' => 3
        );
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        $this->base_64
            ->where('id_pli', $id_pli)
            ->where('flag_traitement', 4)
            ->set($data_lock_pli)
            ->update($this->tb_pli);
        $this->ged
            ->where('id_pli', $id_pli)
            ->where('statut_saisie', 1)
            ->set($data_lock_datapli)
            ->update($this->tb_data_pli);
        $modification = $this->base_64->affected_rows() + $this->ged->affected_rows();
        if($modification != 2 || $this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'saisie> model_pli_saisie> unlocked> deverrouilage saisie de pli#'.$id_pli.' produisant '.$modification.' modification =>'.$this->base_64->last_query());
            return FALSE;
        }else {
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            $this->CI->histo->action(12, 'annulation saisie', $id_pli);
            $this->CI->histo->pli($id_pli);
            return TRUE;
        }
    }

    public function last_lot_saisie_today($id_utilisateur){
        $data = $this->ged
            ->select('*')
            ->from($this->tb_lotSaisie.' tb_ls')
            ->where('operateur', $id_utilisateur)
            ->where('dt::DATE = NOW()::DATE', NULL, FALSE)
            ->join($this->vw_grPliPos.' gr_pos', 'tb_ls.id_group_pli_possible = gr_pos.id_groupe_pli_possible', 'INNER')
            ->select('tb_ls.id_group_pli_possible id_gr_poss')
            ->order_by('dt', 'DESC')
            ->limit(1)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }

    public function last_lot_saisie($id_utilisateur){
        $data = $this->ged
            ->select('*')
            ->from($this->tb_lotSaisie.' tb_ls')
            ->where('operateur', $id_utilisateur)
            ->join($this->vw_grPliPos.' gr_pos', 'tb_ls.id_group_pli_possible = gr_pos.id_groupe_pli_possible', 'INNER')
            ->select('tb_ls.id_group_pli_possible id_gr_poss')
            ->order_by('dt', 'DESC')
            ->limit(1)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }

    private function id_lot_exist($id_lot){
        $data = $this->ged
            ->from($this->tb_lotSaisie)
            ->where('identifiant', $id_lot)
            ->get()->result();
        return count($data) > 0;
    }

    private function id_lot_inc($num_prop, $id_user, $login){
        if($this->nb_inc_id_lot > 100){
            log_message('error', 'saisie> model_pli_saisie> id_lot_inc> imossible de generer un id_lot_saisie user#'.$id_user);
            return NULL;
        }
        $prop_id_lot_saisie = $num_prop.'_'.$login;
        if($this->id_lot_exist($prop_id_lot_saisie)){
            $this->nb_inc_id_lot++;
            $num_prop++;
            return $this->id_lot_inc($num_prop, $id_user, $login);
        }else{
            return $prop_id_lot_saisie;
        }
    }

    public function id_lot_saisie_gen($id_user, $login){
        if(!is_numeric($id_user)){
            log_message('error', 'saisie> model_pli_saisie> id_lot_saisie_gen> erreur id_user non numeric:'.$id_user);
            return NULL;
        }
        $last_saisie = $this->last_lot_saisie($id_user);
        if(is_null($last_saisie)){
            return '1_'.$login;
        }else{
            $tab_ident = explode( '_', $last_saisie->identifiant);
            if(count($tab_ident) < 2){
                log_message('error', 'saisie> model_pli_saisie> id_lot_saisie_gen> last id_lot_saisie non conforme:'.$last_saisie->identifiant.', user#'.$id_user);
                return NULL;
            }
            if(is_numeric($tab_ident[0])){
                $this->nb_inc_id_lot = 0;
                return $this->id_lot_inc(($tab_ident[0] +1 ), $id_user, $login);
            }else{
                log_message('error', 'saisie> model_pli_saisie> id_lot_saisie_gen> id_lot_saisie non conforme:'.$last_saisie->identifiant.', user#'.$id_user);
                return NULL;
            }
        }
    }

    public function nb_pli_in_lot_saisie($id_lot_saisie){
        return $this->base_64
            ->from($this->tb_pli)
            ->where('id_lot_saisie', $id_lot_saisie)
            ->count_all_results();
    }

    public function documents($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->base_64
            ->from($this->tb_doc)
            ->where('id_pli', $id_pli)
            ->order_by('id_document')
            ->get()->result();
    }

    public function id_documents($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->base_64
            ->select('id_document')
            ->from($this->tb_doc)
            ->where('id_pli', $id_pli)
            ->order_by('id_document')
            ->get()->result();
    }

    public function document($id_doc){
        return $this->base_64
            ->from($this->tb_doc)
            ->where('id_document', $id_doc)
            ->get()->result();
    }

    public function societe(){
        return $this->ged
            ->from($this->tb_soc)
            ->where('actif', 1)
            ->order_by('nom_societe')
            ->get()->result();
    }

    public function typologie($id_soc=1){
        $id_soc = is_numeric($id_soc) ? $id_soc : 1;
        return $this->ged
            ->distinct()
            ->select($this->tb_typo.'.*')
            ->from($this->vw_grPliPos)
            ->join($this->tb_typo, 'id_typologie = id', 'INNER')
            ->order_by($this->tb_typo.'.typologie')
            ->where('id_soc', $id_soc)
            ->get()->result();
    }

    public function titre($id_soc=NULL, $titre_i=FALSE){
        if(is_numeric($id_soc)){
            $this->ged->where('societe_id', $id_soc);
        }
        if ($titre_i) {
            $this->ged->where('titre_i', 1);
        }
        return $this->ged
            ->from($this->tb_titre)
            ->order_by('titre')
            ->where('actif', 1)
            ->get()->result();
    }

    public function mode_paiement(){
        return $this->ged
            ->from($this->tb_mdPaie)
            ->order_by('mode_paiement')
            ->where('actif', 1)
            ->get()->result();
    }

    public function anom_cheque(){
        return $this->ged
            ->from($this->tb_anomChq)
            ->order_by('anomalie')
            ->where('flag', 1)
            ->get()->result();
    }

    public function anom_cheque_tab(){
        $data = $this->ged
            ->from($this->tb_anomChq.' tb_anom')
            ->join($this->tb_etatChq.' tb_stat', 'tb_anom.id_etat_chq = tb_stat.id_etat_chq', 'INNER')
            ->where('tb_anom.flag', 1)
            ->select('tb_anom.id')
            ->select('tb_anom.id_etat_chq')
            ->select('tb_stat.priorite prio_stat')
            ->select('tb_anom.priorite prio_anom')
            ->select('tb_anom.etat_pli')
            ->select('tb_anom.anomalie label_anom')
            ->get()->result();
        $rep = array();
        foreach ($data as $key => $value) {
            $rep[$value->id] = $value;
        }
        return $rep;
    }
    
    public function anom_cheque_etat(){
        $data = $this->ged
            ->from($this->tb_anomChq)
            ->order_by('id')
            ->get()->result();
        $rep = array();
        foreach ($data as $key => $value) {
            $rep[$value->id] = $value->id_etat_chq;
        }
        return $rep;
    }
    
    public function cheque_etat_tab(){
        $data = $this->ged
            ->from($this->tb_etatChq)
            ->get()->result();
        $rep = array();
        foreach ($data as $key => $value) {
            $rep[$value->id_etat_chq] = $value;
        }
        return $rep;
    }

    public function paiement_anomali($id_doc)
    {
        if(!is_numeric($id_doc)){
            return array();
        }
        $data = $this->ged
            ->from($this->tb_paieChqAnomali)
            ->where('id_doc', $id_doc)
            ->get()->result();
        $rep = array();
        foreach ($data as $key => $value) {
            array_push($rep, $value->id_anomalie_chq);
        }
        return $rep;
    }
    
    public function paiement_chq($id_doc)
    {
        if(!is_numeric($id_doc)){
            return NULL;
        }
        $data = $this->ged
            ->from($this->tb_cheque)
            ->where('id_doc', $id_doc)
            ->limit(1)
            ->get()->result();
        return count($data) == 1 ? $data[0] : NULL;
    }

    public function paiement_chq_ano_corrige($id_doc)
    {
        if(!is_numeric($id_doc)){
            return array();
        }
        return $this->ged
            ->from($this->tb_chequeAnoCorrige)
            ->where('id_doc', $id_doc)
            ->get()->result();
    }
    
    public function type_chq_kd(){
        return $this->ged
            ->from($this->tb_chqKd)
            ->order_by('type_cheque_cadeau')
            ->where('actif', 1)
            ->get()->result();
    }

    public function mvmnt_of_pli($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->ged
            ->from($this->tb_mvmnt)
            ->where('id_pli', $id_pli)
            ->order_by('id')
            ->get()->result();
    }

    public function statut_saisie($data_pli = NULL){
        if(isset($data_pli->flag_batch) && $data_pli->flag_batch == '1'){
            $this->ged->where_not_in('id_statut_saisie', array(11));
        }
        return $this->ged
            ->from($this->tb_statS)
            ->order_by('id_statut_saisie')
            ->where('actif', 1)
            ->where('saisie', 1)
            ->get()->result();
    }

    public function motif_ko(){
        return $this->ged
            ->from($this->tb_motifKo)
            ->order_by('libelle_motif')
            ->where('actif', 1)
            ->get()->result();
    }

    public function liste_circulaires(){
        return $this->ged
            ->from($this->tb_lstCirc)
            ->order_by('circulaire')
            ->where('actif', 1)
            ->get()->result();
    }

    public function data_pli($id_pli){
        if(!is_numeric($id_pli)){
            return NULL;
        }
        $data_pli = $this->ged
            ->from($this->tb_data_pli)
            ->where('id_pli', $id_pli)
            ->get()->result();
        return count($data_pli) > 0 ? $data_pli[0] : NULL;
    }

    public function my_paiement($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        $data = $this->ged->from($this->tb_paiement)->where('id_pli', $id_pli)->get()->result();
        $rep = array();
        foreach ($data as $paie) {
            array_push($rep, $paie->id_mode_paiement);
        }
        return $rep;
    }
    
    public function paie_esp($id_pli){
        if(is_numeric($id_pli)){
            $data = $this->ged->from($this->tb_paieEsp)->where('id_pli', $id_pli)->get()->result();
            return count($data) > 0 ? $data[0]->montant : '0';
        }
        return '0';
    }

    public function cheques($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->ged->from($this->tb_cheque)->where('id_pli', $id_pli)->order_by('id_doc')->get()->result();
    }
    
    public function cheques_kd($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->ged->from($this->tb_paieChqKd)->where('id_pli', $id_pli)->order_by('id_doc')->get()->result();
    }

    public function pli($id_pli){
        if(is_numeric($id_pli)){
            $pli = $this->base_64
                ->from($this->tb_pli)
                ->where('id_pli', $id_pli)
                ->get()->result();
            return count($pli) > 0 ? $pli[0] : NULL;
        }
        return NULL;
    }

    public function save_sasie($data_saisie){
        $id_pli = $data_saisie->id_pli;
        $check = $this->check_before_update($data_saisie);
        if($check[0]){
            $pli = $check[1];
            $lot_saisie = $this->lot_saisie($data_saisie->id_lot_saisie);
            $this->ged->trans_begin();
            $this->base_64->trans_begin();
            try {
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paiement);
                if (count($data_saisie->paiements) > 0) {
                    $this->ged->insert_batch($this->tb_paiement, $data_saisie->paiements);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieEsp);
                if (count($data_saisie->paie_esp) > 0) {
                    $this->ged->insert($this->tb_paieEsp, $data_saisie->paie_esp);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_mvmnt);
                if (count($data_saisie->mvmnts) > 0) {
                    $this->ged->insert_batch($this->tb_mvmnt, $data_saisie->mvmnts);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_cheque);
                if (count($data_saisie->cheques) > 0) {
                    $this->ged->insert_batch($this->tb_cheque, $data_saisie->cheques);
                }
                $this->ged
                    ->where('id_pli', $id_pli)
                    ->where_not_in('id_etat_chq', array(4))
                    ->delete($this->tb_chqSaisie);
                if (count($data_saisie->chq_saisie) > 0) {
                    $this->ged->insert_batch($this->tb_chqSaisie, $data_saisie->chq_saisie);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqAnomali);
                if (count($data_saisie->paiement_chq_anomalies) > 0) {
                    $this->ged->insert_batch($this->tb_paieChqAnomali, $data_saisie->paiement_chq_anomalies);
                }
                if (count($data_saisie->histo_cheque) > 0) {
                    $this->ged->insert_batch($this->tb_histoChq, $data_saisie->histo_cheque);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqKd);
                if (count($data_saisie->cheques_cadeaux) > 0) {
                    $this->ged->insert_batch($this->tb_paieChqKd, $data_saisie->cheques_cadeaux);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_saisieCtrlField);
                if (count($data_saisie->data_saisie_ctrl) > 0) {
                    $this->ged->insert_batch($this->tb_saisieCtrlField, $data_saisie->data_saisie_ctrl);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_mvmntSaisie);
                if (count($data_saisie->mvmvnts_saisie) > 0) {
                    $this->ged->insert_batch($this->tb_mvmntSaisie, $data_saisie->mvmvnts_saisie);
                }
                //$this->ged->where('id_pli', $id_pli)->delete($this->tb_motifConsigne);
                if(!is_null($data_saisie->motif_consigne)) {
                    $this->ged->insert($this->tb_motifConsigne, $data_saisie->motif_consigne);
                }
                if($data_saisie->saisie_batch && in_array($data_saisie->statut, array(1))){
                    $this->ged->where('id_pli', $id_pli)->delete($this->tb_cba);
                    $this->ged->insert_batch($this->tb_cba, $data_saisie->data_to_cba);
                }
                $this->ged
                    ->set($data_saisie->data_pli)
                    ->set('dt_enregistrement', 'NOW()', FALSE)
                    ->set('ke_prio', 0)
                    ->where('id_pli', $id_pli)
                    ->update($this->tb_data_pli);
                if($this->ged->affected_rows() < 1){
                    $this->ged->trans_rollback();
                    $this->base_64->trans_rollback();
                    log_message('error', 'models> saisie> model_pli_saisie> save_sasie: MAJ data_pli en erreur');
                    return array(FALSE, 'Erreur serveur!');
                }
                if(is_null($lot_saisie)){
                    $this->ged->insert($this->tb_lotSaisie, array(
                        'identifiant' => $data_saisie->id_lot_saisie
                        ,'operateur' => $data_saisie->op
                        ,'id_group_pli_possible' => $data_saisie->id_group_pli_possible
                    ));
                }else {
                    if($lot_saisie->operateur != $data_saisie->op){
                        $this->ged->trans_rollback();
                        $data_verif_old_user = $this->ged
                            ->where('id_utilisateur', $lot_saisie->operateur)
                            ->where('supprime', 1)
                            ->from($this->tb_user)
                            ->get()->result();
                        if(count($data_verif_old_user) > 0){
                            $this->ged
                                ->where('operateur', $lot_saisie->operateur)
                                ->set('operateur', $data_saisie->op)
                                ->update($this->tb_lotSaisie);
                        }
                        return array(FALSE, 'Enregistrement échoué: Identifiant lot saisie en doublon!');
                    }
                }
                $this->base_64->set('desarchive', 0)->where('id_pli', $id_pli)->update($this->tb_doc);
                if(is_array($data_saisie->img_dezip) && count($data_saisie->img_dezip) > 0){
                    $this->base_64
                        ->set('desarchive', 1)
                        ->where('id_pli', $id_pli)
                        ->where_in('id_document', $data_saisie->img_dezip)
                        ->update($this->tb_doc);
                }
                $this->base_64
                    ->set($data_saisie->pli)
                    ->set('date_saisie', 'NOW()', FALSE)
                    ->where('id_pli', $id_pli)
                    ->update($this->tb_pli);
                if($this->ged->affected_rows() < 1){
                    $this->ged->trans_rollback();
                    $this->base_64->trans_rollback();
                    log_message('error', 'models> saisie> model_pli_saisie> save_sasie: MAJ pli en erreur');
                    return array(FALSE, 'Erreur serveur!');
                }
            } catch (Exception $exc) {
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                log_message('error', 'models> saisie> model_pli_saisie> save_saisie: '.$exc->getMessage());
                return array(FALSE, 'Erreur serveur!');
            }
            if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                return array(FALSE, 'Enregistrement échoué!');
            }else{
                $this->ged->trans_commit();
                $this->base_64->trans_commit();
                $this->CI->histo->action($data_saisie->action_saisie, '', $id_pli);
                $this->CI->histo->pli($id_pli, ($data_saisie->link_motif_consigne == '' ? '' : 'KO/CI'), $data_saisie->link_motif_consigne);
                return array(TRUE, '');
            }
        }
        return $check;
    }

    public function ko_save_sasie($data_saisie){
        $id_pli = $data_saisie->id_pli;
        $check = $this->check_before_update($data_saisie);
        if($check[0]){
            $pli = $check[1];
            $lot_saisie = $this->lot_saisie($data_saisie->id_lot_saisie);
            $this->ged->trans_begin();
            $this->base_64->trans_begin();
            try {
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paiement);
                if (count($data_saisie->paiements) > 0) {
                    $this->ged->insert_batch($this->tb_paiement, $data_saisie->paiements);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieEsp);
                if (count($data_saisie->paie_esp) > 0) {
                    $this->ged->insert($this->tb_paieEsp, $data_saisie->paie_esp);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_mvmnt);
                if (count($data_saisie->mvmnts) > 0) {
                    $this->ged->insert_batch($this->tb_mvmnt, $data_saisie->mvmnts);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_cheque);
                if (count($data_saisie->cheques) > 0) {
                    $this->ged->insert_batch($this->tb_cheque, $data_saisie->cheques);
                }
                $this->ged
                    ->where('id_pli', $id_pli)
                    ->where_not_in('id_etat_chq', array(4))
                    ->delete($this->tb_chqSaisie);
                if (count($data_saisie->chq_saisie) > 0) {
                    $this->ged->insert_batch($this->tb_chqSaisie, $data_saisie->chq_saisie);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqAnomali);
                if (count($data_saisie->paiement_chq_anomalies) > 0) {
                    $this->ged->insert_batch($this->tb_paieChqAnomali, $data_saisie->paiement_chq_anomalies);
                }
                if (count($data_saisie->histo_cheque) > 0) {
                    $this->ged->insert_batch($this->tb_histoChq, $data_saisie->histo_cheque);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqKd);
                if (count($data_saisie->cheques_cadeaux) > 0) {
                    $this->ged->insert_batch($this->tb_paieChqKd, $data_saisie->cheques_cadeaux);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_mvmntSaisie);
                if (count($data_saisie->mvmvnts_saisie) > 0) {
                    $this->ged->insert_batch($this->tb_mvmntSaisie, $data_saisie->mvmvnts_saisie);
                }
                //$this->ged->where('id_pli', $id_pli)->delete($this->tb_motifConsigne);
                if(!is_null($data_saisie->motif_consigne)) {
                    $this->ged->insert($this->tb_motifConsigne, $data_saisie->motif_consigne);
                }
                $this->ged
                    ->set($data_saisie->data_pli)
                    //->set('dt_enregistrement', 'NOW()', FALSE)
                    ->set('ke_prio', 0)
                    ->set('in_cba', 0)
                    ->where('id_pli', $id_pli)
                    ->update($this->tb_data_pli);
                if($this->ged->affected_rows() < 1){
                    $this->ged->trans_rollback();
                    $this->base_64->trans_rollback();
                    log_message('error', 'models> saisie> model_pli_saisie> ko_save_sasie: MAJ data_pli en erreur');
                    return array(FALSE, 'Erreur serveur!');
                }
                if(is_null($lot_saisie)){
                    $this->ged->insert($this->tb_lotSaisie, array(
                        'identifiant' => $data_saisie->id_lot_saisie
                        ,'operateur' => $data_saisie->op
                        ,'id_group_pli_possible' => $data_saisie->id_group_pli_possible
                    ));
                }else {
                    if($lot_saisie->operateur != $data_saisie->op){
                        $this->ged->trans_rollback();
                        $data_verif_old_user = $this->ged
                            ->where('id_utilisateur', $lot_saisie->operateur)
                            ->where('supprime', 1)
                            ->from($this->tb_user)
                            ->get()->result();
                        if(count($data_verif_old_user) > 0){
                            $this->ged
                                ->where('operateur', $lot_saisie->operateur)
                                ->set('operateur', $data_saisie->op)
                                ->update($this->tb_lotSaisie);
                        }
                        return array(FALSE, 'Enregistrement échoué: Identifiant lot saisie en doublon!');
                    }
                }
                $this->base_64->set('desarchive', 0)->where('id_pli', $id_pli)->update($this->tb_doc);
                if(is_array($data_saisie->img_dezip) && count($data_saisie->img_dezip) > 0){
                    $this->base_64
                        ->set('desarchive', 1)
                        ->where('id_pli', $id_pli)
                        ->where_in('id_document', $data_saisie->img_dezip)
                        ->update($this->tb_doc);
                }
                $this->base_64
                    ->set($data_saisie->pli)
                    ->set('date_saisie', 'NOW()', FALSE)
                    ->where('id_pli', $id_pli)->update($this->tb_pli);
                if($this->ged->affected_rows() < 1){
                    $this->ged->trans_rollback();
                    $this->base_64->trans_rollback();
                    log_message('error', 'models> saisie> model_pli_saisie> ko_save_sasie: MAJ pli en erreur');
                    return array(FALSE, 'Erreur serveur!');
                }
            } catch (Exception $exc) {
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                log_message('error', 'models> saisie> model_pli_saisie> ko_save_saisie: '.$exc->getMessage());
                return array(FALSE, 'Erreur serveur!');
            }
            if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                return array(FALSE, 'Enregistrement échoué!');
            }else{
                $this->ged->trans_commit();
                $this->base_64->trans_commit();
                $this->CI->histo->action($data_saisie->action_saisie, '', $id_pli);
                $this->CI->histo->pli($id_pli,'ko',$data_saisie->link_motif_consigne);
                return array(TRUE, '');
            }
        }
        return $check;
    }

    public function quick_save($data_saisie){
        $id_pli = $data_saisie->id_pli;
        $check = $this->check_before_update($data_saisie);
        if($check[0]){
            $pli = $check[1];
            $this->ged->trans_begin();
            $this->base_64->trans_begin();
            try {
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paiement);
                if (count($data_saisie->paiements) > 0) {
                    $this->ged->insert_batch($this->tb_paiement, $data_saisie->paiements);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieEsp);
                if (count($data_saisie->paie_esp) > 0) {
                    $this->ged->insert($this->tb_paieEsp, $data_saisie->paie_esp);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_mvmnt);
                if (count($data_saisie->mvmnts) > 0) {
                    $this->ged->insert_batch($this->tb_mvmnt, $data_saisie->mvmnts);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_cheque);
                if (count($data_saisie->cheques) > 0) {
                    $this->ged->insert_batch($this->tb_cheque, $data_saisie->cheques);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqAnomali);
                if (count($data_saisie->paiement_chq_anomalies) > 0) {
                    $this->ged->insert_batch($this->tb_paieChqAnomali, $data_saisie->paiement_chq_anomalies);
                }
                if (count($data_saisie->histo_cheque) > 0) {
                    $this->ged->insert_batch($this->tb_histoChq, $data_saisie->histo_cheque);
                }
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqKd);
                if (count($data_saisie->cheques_cadeaux) > 0) {
                    $this->ged->insert_batch($this->tb_paieChqKd, $data_saisie->cheques_cadeaux);
                }
                $this->ged
                    ->set($data_saisie->data_pli)
                    ->where('id_pli', $id_pli)
                    ->update($this->tb_data_pli);
                if($this->ged->affected_rows() < 1){
                    $this->ged->trans_rollback();
                    $this->base_64->trans_rollback();
                    log_message('error', 'models> saisie> model_pli_saisie> quick_save: MAJ data_pli en erreur');
                    return array(FALSE, 'Erreur serveur!');
                }
                $this->base_64->set('desarchive', 0)->where('id_pli', $id_pli)->update($this->tb_doc);
                if(is_array($data_saisie->img_dezip) && count($data_saisie->img_dezip) > 0){
                    $this->base_64
                        ->set('desarchive', 1)
                        ->where('id_pli', $id_pli)
                        ->where_in('id_document', $data_saisie->img_dezip)
                        ->update($this->tb_doc);
                }
                $this->base_64->set($data_saisie->pli)->where('id_pli', $id_pli)->update($this->tb_pli);
                if($this->ged->affected_rows() < 1){
                    $this->ged->trans_rollback();
                    $this->base_64->trans_rollback();
                    log_message('error', 'models> saisie> model_pli_saisie> quick_save: MAJ pli en erreur');
                    return array(FALSE, 'Erreur serveur!');
                }
            } catch (Exception $exc) {
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                log_message('error', 'models> saisie> model_pli_saisie> quick_save: '.$exc->getMessage());
                return array(FALSE, 'Erreur serveur!');
            }
            if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                return array(FALSE, 'Enregistrement échoué!');
            }else{
                $this->ged->trans_commit();
                $this->base_64->trans_commit();
                $this->CI->histo->action(79, 'Saisie: enregistrement partiel', $id_pli);
                //$this->CI->histo->pli($id_pli);
                return array(TRUE, '');
            }
        }
        return $check;
    }

    public function check_before_update($data_saisie){
        $id_pli = $data_saisie->id_pli;
        $pli = $this->pli($id_pli);
        if(is_null($pli)){
            return array(FALSE, 'Pli introuvable!');
        }
        if($pli->saisie_par != $data_saisie->op || $pli->flag_traitement != 4){
            return array(FALSE, 'Ce pli ne vous est plus assigné!');
        }
        if($pli->recommande == -1){
            return array(FALSE, 'Ce pli a été mis en Stand-by par un administrateur!');
        }
        return array(TRUE, $pli);
    }

    private function lot_saisie($id_lot_saisie){
        $lot_saisie = $this->ged
            ->from($this->tb_lotSaisie)
            ->where('identifiant', $id_lot_saisie)
            ->get()->result();
        return count($lot_saisie) > 0 ? $lot_saisie[0] : NULL;
    }

    public function id_grp_paie_for_paiement($paiements){
        if(is_array($paiements) && count($paiements) > 0){
            $data = $this->ged
                ->from($this->tb_mdPaie)
                ->where_in('id_mode_paiement', $paiements)
                ->order_by('id_mode_paiement')
                ->get()->result();
            return count($data) > 0 ? $data[0]->id_groupe_paiement : 0;
        }
        return 0;
    }

    public function redirect_to_saisi_direct($id_pli){
        $this->ged->trans_begin();
        // $this->base_64->trans_begin();
        // $this->base_64
        //     ->set('flag_traitement', 3)
        //     ->set('saisie_par', NULL)
        //     ->where('id_pli', $id_pli)
        //     ->where('flag_traitement', 4)
        //     ->where('saisie_par', $this->CI->session->id_utilisateur)
        //     ->update($this->tb_pli);
        $this->ged
            // ->set('flag_traitement', 3)
            ->set('flag_batch', 0)
            ->where('id_pli', $id_pli)
            ->update($this->tb_data_pli);
        if($this->ged->trans_status() === FALSE /*|| $this->base_64->trans_status() === FALSE || $this->base_64->affected_rows() < 1*/){
            $this->ged->trans_rollback();
            // $this->base_64->trans_rollback();
            return FALSE;
        }else{
            $this->ged->trans_commit();
            // $this->base_64->trans_commit();
            $this->CI->histo->action(108, 'saisie batch', $id_pli);
            $this->CI->histo->pli($id_pli, 'redirection saisie directe');
            return TRUE;
        }
    }

    public function vers_saisie_n2($id_pli)
    {
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        $this->base_64
            ->set('flag_traitement', 3)
            ->set('saisie_par', NULL)
            ->where('id_pli', $id_pli)
            ->where('flag_traitement', 4)
            ->where('saisie_par', $this->CI->session->id_utilisateur)
            ->update($this->tb_pli);
        $this->ged
            ->set('flag_traitement', 3)
            ->set('niveau_saisie', 2)
            ->where('id_pli', $id_pli)
            ->update($this->tb_data_pli);
        if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE || $this->base_64->affected_rows() < 1){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            return FALSE;
        }else{
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            $this->CI->histo->action(134, 'vers saisie niveau 2', $id_pli);
            $this->CI->histo->pli($id_pli, 'vers saisie niveau 2');
            return TRUE;
        }
    }

    /*
    public function hors_perim($id_pli){
        $this->base_64
            ->set('flag_traitement', 21)
            ->where('id_pli', $id_pli)
            ->update($this->tb_pli);
        if($this->base_64->affected_rows() > 0){
            $this->ged
                ->set('statut_saisie', 2)
                ->where('id_pli', $id_pli)
                ->update($this->tb_data_pli);
            $this->CI->histo->pli($id_pli, 'HP saisie');
            $this->CI->histo->action(73, 'saisie', $id_pli);
            return TRUE;
        }
        return FALSE;
    }
    
    public function ko_scan($id_pli, $data_motif){
        $this->base_64
            ->set('flag_traitement', 16)
            ->where('id_pli', $id_pli)
            ->where('flag_traitement', 4)
            ->where('saisie_par', $this->CI->session->id_utilisateur)
            ->update($this->tb_pli);
        if($this->base_64->affected_rows() > 0){
            $this->ged
                ->set('statut_saisie', 2)
                ->where('id_pli', $id_pli)
                ->update($this->tb_data_pli);
            $this->ged->where('pli_id', $id_pli)->delete($this->tb_mtfKoScn);
            $this->ged->insert($this->tb_mtfKoScn, $data_motif);
            $this->CI->histo->pli($id_pli, 'KO-scan saisie');
            $this->CI->histo->action(74, 'saisie', $id_pli);
            return TRUE;
        }
        return FALSE;
    }*/

    public function motif_rejet(){
        return $this->ged
            ->from($this->tb_mtfRj)
            ->where('flag', 1)
            ->order_by('motif')
            ->get()->result();
    }

    public function get_old_values_ctrl_saisie($id_pli)
    {
        $old_values = array();
        $data = $this->ged
            ->from($this->tb_saisieCtrlField)
            ->where('id_pli', $id_pli)
            ->get()->result();
        foreach ($data as $key => $value) {
            $old_values[$value->id_field] = $value;
        }
        return $old_values;
    }

    public function get_predata($id_pli)
    {
        return $this->base_64
            ->where('id_pli', $id_pli)
            ->from($this->tb_doc.' tb_doc')
            ->join($this->tb_preData.' tb_pre', 'tb_doc.id_document = tb_pre.id_document', 'INNER')
            ->select('tb_pre.*')
            ->get()->result();
    }

    public function get_id_titre($code_titre)
    {
        $data = $this->ged
            ->from($this->tb_titre)
            ->where('code', strtoupper($code_titre))
            ->get()->result();
        return count($data) > 0 ? $data[0]->id : NULL;
    }

    public function get_motif_consigne($id_pli)
    {
        $data = $this->ged
            ->from($this->tb_motifConsigne)
            ->where('id_pli', $id_pli)
            ->order_by('dt_motif_ko', 'DESC')
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }
    
    public function get_all_motif_consigne($id_pli)
    {
        $data = $this->ged
            ->from($this->tb_motifConsigne)
            ->where('id_pli', $id_pli)
            ->order_by('dt_motif_ko', 'ASC')
            ->get()->result();
        return count($data) > 0 ? $data : array();
    }
    
    public function nb_motif_consigne($id_pli)
    {
        return $this->ged
            ->from($this->tb_motifConsigne)
            ->where('id_pli', $id_pli)
            ->count_all_results();
    }

    public function get_label_motif_ko($id_motif)
    {
        if(!is_numeric($id_motif)){
            return '';
        }
        $data = $this->ged
            ->from($this->tb_motifKo)
            ->where('id_motif', $id_motif)
            ->get()->result();
        return count($data) > 0 ? $data[0]->libelle_motif.', ' : '';
    }

    public function anom_chq_bloquant()
    {
        return $this->ged
            ->select('id')
            ->from($this->tb_anomChq)
            ->where_in('id_etat_chq', array(3))
            // ->where_in('id', array(23,24,26,27))
            ->get()->result();
    }

}
