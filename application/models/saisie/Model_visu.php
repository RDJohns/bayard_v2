<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_visu extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    
    private $tb_pli = TB_pli;
    private $tb_soc = TB_soc;
    private $tb_user = TB_user;
    private $tb_titre = TB_titre;
    private $tb_cheque = TB_cheque;
    private $tb_tpUser = TB_tpUser;
    private $tb_lstCirc = TB_lstCirc;
    private $tb_paiement = TB_paiement;
    private $vw_grPliPos = Vw_grPliPos;
    private $tb_lotSaisie = TB_lotSaisie;
    private $tb_mdPaie = TB_mode_paiement;
    private $tb_assignation = TB_assignation;
    private $tb_groupe_pli = TB_groupe_pli;
    private $tb_data_pli = TB_data_pli;
    private $tb_anomChq = TB_anomChq;
    private $tb_motifKo = TB_motifKo;
    private $tb_grUser = TB_grUser;
    private $tb_doc = TB_document;
    private $tb_statS = TB_statS;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_typo = TB_typo;
    private $vw_pli = Vw_pli;
    private $ftb_pli = FTB_pli;
    private $ftb_lotNum = FTB_lotNumerisation;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function my_lot_total(){
        $id_user = $this->CI->session->id_utilisateur;
        return $this->ged
            ->from($this->tb_lotSaisie)
            ->where('operateur', $id_user)
            ->count_all_results();
    }

    public function lot_saisie($limit, $offset, $arg_find, $arg_ordo, $sens, $filtre, $nb_only = TRUE){
        $id_user = $this->CI->session->id_utilisateur;
        $sql_sub = $this->ged
            ->select('identifiant id_lot')
            ->select('COUNT(id_pli) nb_plis', FALSE)
            ->select('STRING_AGG(\'#\' || id_pli::VARCHAR, \', \' ORDER BY id_pli) plis', FALSE)
            ->from($this->tb_lotSaisie.' tblot')
            ->where('operateur', $id_user)
            ->join($this->ftb_pli.' tbpli', 'id_lot_saisie=identifiant', 'INNER')
            ->where('lot_saisie_bis', 0)
            ->group_by('identifiant')
            ->get_compiled_select();
		$this->ged->reset_query();
        $sql_sub_bis = $this->ged
            ->select('identifiant id_lot')
            ->select('COUNT(id_pli) nb_plis', FALSE)
            ->select('STRING_AGG(\'#\' || id_pli::VARCHAR, \', \' ORDER BY id_pli) plis', FALSE)
            ->from($this->tb_lotSaisie.' tblot')
            ->where('operateur', $id_user)
            ->join($this->ftb_pli.' tbpli', 'id_lot_saisie=identifiant', 'INNER')
            ->where('lot_saisie_bis', 1)
            ->group_by('identifiant')
            ->get_compiled_select();
        $this->ged->reset_query();
        $this->ged
            ->from($this->tb_lotSaisie.' tb')
            ->where('operateur', $id_user)
            ->join(' ('.$sql_sub.') tbsub', 'identifiant = tbsub.id_lot', 'LEFT')
            ->join(' ('.$sql_sub_bis.') tbsub_bis', 'identifiant = tbsub_bis.id_lot', 'LEFT')
            ->join($this->vw_grPliPos.' tbposs', 'tb.id_group_pli_possible = tbposs.id_groupe_pli_possible', 'LEFT')
            ->select('dt dt_creat')
            ->select('identifiant id_lot')
            ->select('(CASE WHEN tbsub.nb_plis IS NULL THEN 0 ELSE tbsub.nb_plis END)::integer nbplis')
            ->select('tbsub.plis plis')
            ->select('(CASE WHEN tbsub_bis.nb_plis IS NULL THEN 0 ELSE tbsub_bis.nb_plis END)::integer nbplis_bis')
            ->select('tbsub_bis.plis plis_bis')
            ->select('(CASE WHEN tbsub.nb_plis IS NULL THEN 0 ELSE tbsub.nb_plis END)::integer + (CASE WHEN tbsub_bis.nb_plis IS NULL THEN 0 ELSE tbsub_bis.nb_plis END)::integer total')
            ->select('soc soc')
            ->select('typologie typo')
            ->select('groupe_paiement paie');
        if(!empty(trim($arg_find))){
            $this->ged
                ->group_start()
                    ->like('lower(identifiant)', $arg_find)
                    ->or_like('tbsub.plis', $arg_find)
                    ->or_like('tbsub_bis.plis', $arg_find)
                    ->or_like('lower(soc)', $arg_find)
                    ->or_like('lower(typologie)', $arg_find)
                    ->or_like('lower(groupe_paiement)', $arg_find)
                ->group_end();
        }
        if(isset($filtre['dt']) && !empty(trim($filtre['dt']))){
            $this->ged->where('dt::DATE', trim($filtre['dt']), FALSE);
        }
        if (!$nb_only) {
            $this->ged
                ->order_by($arg_ordo, strtoupper($sens))
                ->limit($limit, $offset);
			return $this->ged->get()->result();
        }
        return $this->ged->count_all_results();
    }

    /*public function group_pli(){
        $sql_sub_w = $this->ged
            ->select('tbgr.id_groupe_pli id_gr')
            ->select('COUNT(id_pli) nb_plis', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->where('flag_traitement', 3)
            ->group_by('tbgr.id_groupe_pli')
            ->get_compiled_select();
		$this->ged->reset_query();
        $sql_sub_s = $this->ged
            ->select('tbgr.id_groupe_pli id_gr')
            ->select('COUNT(id_pli) nb_plis', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->where('flag_traitement', 4)
            ->group_by('tbgr.id_groupe_pli')
            ->get_compiled_select();
        $this->ged->reset_query();
        return $this->ged
            ->from($this->tb_groupe_pli.' tbgr')
            ->join(' ('.$sql_sub_w.') tbw', 'tbw.id_gr = tbgr.id_groupe_pli', 'LEFT')
            ->join(' ('.$sql_sub_s.') tbs', 'tbs.id_gr = tbgr.id_groupe_pli', 'LEFT')
            ->join($this->vw_grPliPos.' tbpos', 'tbpos.id_groupe_pli_possible = tbgr.id_view_groupe_pli_possible', 'INNER')
            ->select('tbpos.id_groupe_pli_possible id_pos')
            ->select('soc')
            ->select('typologie typo')
            ->select('groupe_paiement paie')
            ->select('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer nb_w')
            ->select('(CASE WHEN tbs.nb_plis IS NULL THEN 0 ELSE tbs.nb_plis END)::integer nb_s')
            ->select('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer + (CASE WHEN tbs.nb_plis IS NULL THEN 0 ELSE tbs.nb_plis END)::integer total')
            ->where('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer > 0', NULL, FALSE)
            ->or_where('(CASE WHEN tbs.nb_plis IS NULL THEN 0 ELSE tbs.nb_plis END)::integer > 0', NULL, FALSE)
            ->order_by('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer DESC')
            ->get()->result();
    }*/

    public function group_pli1(){
        if($this->CI->session->id_type_utilisateur == 2 && $this->CI->session->niveau_op == 1){
            $this->ged->where('tb_dtpli.niveau_saisie', 1);
        }
        $sql_sub_w = $this->ged
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(tbpli.id_pli) nb_plis', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->join($this->tb_data_pli.' tb_dtpli', 'tb_dtpli.id_pli = tbpli.id_pli', 'INNER')
            ->where('tbpli.flag_traitement', 3)
            ->where('tb_dtpli.statut_saisie', 1)
            ->where('recommande != -1 ', NULL, FALSE)
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();

        if($this->CI->session->id_type_utilisateur == 2 && $this->CI->session->niveau_op == 1){
            $this->ged->where('tb_dtpli.niveau_saisie', 1);
        }
        $sql_sub_w_rj_cba = $this->ged
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(tbpli.id_pli) nb_plis', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->join($this->tb_data_pli.' tb_dtpli', 'tb_dtpli.id_pli = tbpli.id_pli', 'INNER')
            ->where('tbpli.flag_traitement', 3)
            ->where('tb_dtpli.statut_saisie', 1)
            ->where('flag_rejet_batch', 1)
            ->where('recommande != -1 ', NULL, FALSE)
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();

        if($this->CI->session->id_type_utilisateur == 2 && $this->CI->session->niveau_op == 1){
            $this->ged->where('tb_dtpli.niveau_saisie', 1);
        }
        $sql_sub_s = $this->ged
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(tbpli.id_pli) nb_plis', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->join($this->tb_data_pli.' tb_dtpli', 'tb_dtpli.id_pli = tbpli.id_pli', 'INNER')
            ->where('tbpli.flag_traitement', 4)
            ->where('tb_dtpli.statut_saisie', 1)
            ->where('recommande != -1 ', NULL, FALSE)
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();

        if($this->CI->session->id_type_utilisateur == 2 && $this->CI->session->niveau_op == 1){
            $this->ged->where('tb_dtpli.niveau_saisie', 1);
        }
        $sql_sub_w_n2 = $this->ged
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(tbpli.id_pli) nb_plis', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->join($this->tb_data_pli.' tb_dtpli', 'tb_dtpli.id_pli = tbpli.id_pli', 'INNER')
            ->where('tbpli.flag_traitement', 3)
            ->where('tb_dtpli.statut_saisie', 1)
            ->where('tb_dtpli.niveau_saisie', 2)
            ->where('recommande != -1 ', NULL, FALSE)
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();

        return $this->ged
            ->from($this->vw_grPliPos.' tbpos')
            ->join(' ('.$sql_sub_w.') tbw', 'tbw.id_gr_poss = tbpos.id_groupe_pli_possible', 'LEFT')
            ->join(' ('.$sql_sub_w_rj_cba.') tbw_rj_cba', 'tbw_rj_cba.id_gr_poss = tbpos.id_groupe_pli_possible', 'LEFT')
            ->join(' ('.$sql_sub_s.') tbs', 'tbs.id_gr_poss = tbpos.id_groupe_pli_possible', 'LEFT')
            ->join(' ('.$sql_sub_w_n2.') tbn2', 'tbn2.id_gr_poss = tbpos.id_groupe_pli_possible', 'LEFT')
            ->select('id_soc')
            ->select('id_typologie')
            ->select('id_groupe_paiement')
            ->select('tbpos.id_groupe_pli_possible id_pos')
            ->select('soc')
            ->select('typologie typo')
            ->select('groupe_paiement paie')
            ->select('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer nb_w')
            ->select('(CASE WHEN tbw_rj_cba.nb_plis IS NULL THEN 0 ELSE tbw_rj_cba.nb_plis END)::integer nb_w_rj_cba')
            ->select('(CASE WHEN tbs.nb_plis IS NULL THEN 0 ELSE tbs.nb_plis END)::integer nb_s')
            ->select('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer + (CASE WHEN tbs.nb_plis IS NULL THEN 0 ELSE tbs.nb_plis END)::integer total')
            ->select('(COALESCE(tbn2.nb_plis, 0)) nb_n2')
            ->where('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer > 0', NULL, FALSE)
            ->or_where('(CASE WHEN tbs.nb_plis IS NULL THEN 0 ELSE tbs.nb_plis END)::integer > 0', NULL, FALSE)
            ->order_by('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer DESC')
            ->order_by('soc')
            ->order_by('typologie')
            ->order_by('groupe_paiement')
            ->get()->result();
    }

    public function group_pli1_by_older(){
        if($this->CI->session->id_type_utilisateur == 2 && $this->CI->session->niveau_op == 1){
            $this->ged->where('tb_dtpli.niveau_saisie', 1);
        }
        $sql_sub_s = $this->ged
            ->select('tblotn.date_courrier::date dt_cr', FALSE)
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(tbpli.id_pli) nb_w', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->join($this->ftb_lotNum.' tblotn', 'tbpli.id_lot_numerisation = tblotn.id_lot_numerisation', 'INNER')
            ->join($this->tb_data_pli.' tb_dtpli', 'tb_dtpli.id_pli = tbpli.id_pli', 'INNER')
            ->where('tbpli.flag_traitement', 3)
            ->where('tb_dtpli.statut_saisie', 1)
            ->where('recommande != -1 ', NULL, FALSE)
            ->group_by('tblotn.date_courrier')
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();

        if($this->CI->session->id_type_utilisateur == 2 && $this->CI->session->niveau_op == 1){
            $this->ged->where('tb_dtpli.niveau_saisie', 1);
        }
        $sql_sub_s_rj_cba = $this->ged
            ->select('tblotn.date_courrier::date dt_cr', FALSE)
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(tbpli.id_pli) nb_w', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->join($this->ftb_lotNum.' tblotn', 'tbpli.id_lot_numerisation = tblotn.id_lot_numerisation', 'INNER')
            ->join($this->tb_data_pli.' tb_dtpli', 'tb_dtpli.id_pli = tbpli.id_pli', 'INNER')
            ->where('tbpli.flag_traitement', 3)
            ->where('tb_dtpli.statut_saisie', 1)
            ->where('flag_rejet_batch', 1)
            ->where('recommande != -1 ', NULL, FALSE)
            ->group_by('tblotn.date_courrier')
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();
        
        if($this->CI->session->id_type_utilisateur == 2 && $this->CI->session->niveau_op == 1){
            $this->ged->where('tb_dtpli.niveau_saisie', 1);
        }
        $sql_sub_w_n2 = $this->ged
            ->select('tblotn.date_courrier::date dt_cr', FALSE)
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(tbpli.id_pli) nb_plis', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->join($this->ftb_lotNum.' tblotn', 'tbpli.id_lot_numerisation = tblotn.id_lot_numerisation', 'INNER')
            ->join($this->tb_data_pli.' tb_dtpli', 'tb_dtpli.id_pli = tbpli.id_pli', 'INNER')
            ->where('tbpli.flag_traitement', 3)
            ->where('tb_dtpli.statut_saisie', 1)
            ->where('tb_dtpli.niveau_saisie', 2)
            ->where('recommande != -1 ', NULL, FALSE)
            ->group_by('tblotn.date_courrier')
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();

        return $this->ged
            ->select('id_soc')
            ->select('id_typologie')
            ->select('id_groupe_paiement')
            ->select('tbs.dt_cr')
            ->select('tbs.id_gr_poss id_pos')
            ->select('soc')
            ->select('tbpos.typologie typo')
            ->select('groupe_paiement paie')
            ->select('tbs.nb_w')
            ->select('(CASE WHEN tbs_rj_cba.nb_w IS NULL THEN 0 ELSE tbs_rj_cba.nb_w END)::integer nb_w_rj_cba')
            ->select('(COALESCE(tbn2.nb_plis, 0)) nb_n2')
            ->from($this->vw_grPliPos.' tbpos')
            ->join(' ('.$sql_sub_s.') tbs', 'tbs.id_gr_poss = tbpos.id_groupe_pli_possible', 'LEFT')
            ->join(' ('.$sql_sub_s_rj_cba.') tbs_rj_cba', 'tbs_rj_cba.id_gr_poss = tbpos.id_groupe_pli_possible AND tbs_rj_cba.dt_cr = tbs.dt_cr', 'LEFT')
            ->join(' ('.$sql_sub_w_n2.') tbn2', 'tbn2.id_gr_poss = tbpos.id_groupe_pli_possible AND tbn2.dt_cr = tbs.dt_cr', 'LEFT')
            ->where('tbs.nb_w > ', 0)
            ->where('tbs.nb_w IS NOT NULL ', NULL, FALSE)
            ->order_by('tbs.dt_cr', 'ASC')
            ->order_by('tbs.nb_w', 'DESC')
            ->order_by('soc')
            ->order_by('tbpos.typologie')
            ->order_by('groupe_paiement')
            ->get()->result();
    }

}
