<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cba extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;

    private $tb_pli = TB_pli;
    private $tb_data_pli = TB_data_pli;
    private $tb_doc = TB_document;
    private $tb_cba = TB_cba;
    private $tb_addrCba = TB_addrCba;
    private $tb_promoCba = TB_promoCba;
    private $tb_rejetCbaFl = TB_rejetCbaFl;
    private $tb_rejetCbaLn = TB_rejetCbaLn;

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function data_pli($id_pli)
    {
        if(is_numeric($id_pli)){
            $pli = $this->ged
                ->from($this->tb_data_pli)
                ->where('id_pli', $id_pli)
                ->get()->result();
            return count($pli) > 0 ? $pli[0] : NULL;
        }
        return NULL;
    }

    public function name_doc($id_doc)
    {
        if(is_numeric($id_doc)){
            $data = $this->base_64
                ->select('n_ima_recto')
                ->from($this->tb_doc)
                ->where('id_document', $id_doc)
                ->get()->result();
            return count($data) > 0 ? $data[0]->n_ima_recto : '';
        }
        return '';
    }

    public function update_cba($data, $id_pli)
    {
        if ($this->no_sent($id_pli)) {
            $this->ged->trans_begin();
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_cba);
            $this->ged->insert_batch($this->tb_cba, $data);
            $this->ged->set('in_cba', 1)->where('id_pli', $id_pli)->update($this->tb_data_pli);
            if($this->ged->trans_status() === FALSE){
                $this->ged->trans_rollback();
                log_message('error', 'cba=> libraries/saisie/Data_cba save_cba: update cba failed pli#'.$id_pli);
                return FALSE;
            }else{
                $this->ged->trans_commit();
                return TRUE;
            }
        }
    }

    public function save_cba($data, $id_pli)
    {
        $this->ged->trans_begin();
        $this->ged->insert_batch($this->tb_cba, $data);
        $this->ged->set('in_cba', 1)->where('id_pli', $id_pli)->update($this->tb_data_pli);
        if($this->ged->trans_status() === FALSE){
            $this->ged->trans_rollback();
            log_message('error', 'cba=> libraries/saisie/Data_cba save_cba: save cba failed pli#'.$id_pli);
            return FALSE;
        }else{
            $this->ged->trans_commit();
            return TRUE;
        }
    }

    public function no_sent($id_pli)//pas encore envoye
    {
        $data = $this->ged
            ->where('id_pli', $id_pli)
            ->where_in('flag_sent', array(1,-1))
            ->from($this->tb_cba)
            ->get()->result();
        return count($data) < 1;
    }

    public function remove_cba($id_pli)
    {
        if ($this->no_sent($id_pli)) {
            $this->ged->trans_begin();
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_cba);
            $this->ged->set('in_cba', 0)->where('id_pli', $id_pli)->update($this->tb_data_pli);
            if($this->ged->trans_status() === FALSE){
                $this->ged->trans_rollback();
                log_message('error', 'cba=> libraries/saisie/Data_cba remove_cba: delete cba failed pli#'.$id_pli);
                return FALSE;
            }else{
                $this->ged->trans_commit();
                return TRUE;
            }
        }
    }

    public function get_data_client($num_client)
    {
        $data = $this->ged
            ->where("ctm_nbr::integer = $num_client", NULL, FALSE)
            ->from($this->tb_addrCba)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;//$this->ged->last_query();
    }

    public function get_clients_name($array_num = array())
    {
        if(is_array($array_num) && count($array_num) > 0){
            return $this->ged
                ->select('ctm_nbr')
                ->select('atn_end')
                ->where_in("ctm_nbr::integer", $array_num, FALSE)
                ->from($this->tb_addrCba)
                ->get()->result();
        }
        return array();
    }

    public function get_promo($code_promo)
    {
        if(is_null($code_promo)){return array();}
        $data = $this->ged
            ->where('code_promo', $code_promo)
            ->where_not_in('top_reab', array('C', 'c'))
            ->where_in('liste_val', array('Y','y','V','v'))
            ->from($this->tb_promoCba)
            ->order_by('lib_choix')
            ->get()->result();
        return count($data) > 0 ? $data : array();
    }

    public function get_promo_art($code_promo, $code_chx_prom)
    {
        if(is_null($code_promo) || is_null($code_chx_prom)){return array();}
        $data = $this->ged
            ->where('code_promo', $code_promo)
            ->where_not_in('top_reab', array('C', 'c'))
            ->where_in('liste_val', array('Y','y','V','v'))
            ->where('code_choix_promo', $code_chx_prom)
            ->from($this->tb_promoCba)
            ->order_by('code_art')
            ->get()->result();
        return count($data) > 0 ? $data : array();
    }

    public function get_promo_choix_prod($promo)
    {
        $data = $this->ged
            ->where('code_promo', $promo->code_promo)
            ->where('code_choix_promo', $promo->code_choix)
            ->where('code_art', $promo->code_produit)
            ->from($this->tb_promoCba)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }

    public function verif_data_batch($payeur, $abonne, $promo, $only_chq=TRUE)
    {
        $rep = array('is_ok' => TRUE, 'msg' => '');
        $data_payeur_missed = FALSE;
        $data_abonne_missed = FALSE;
        if(is_numeric($payeur->ctm_nbr)){
            if(is_null($this->get_data_client($payeur->ctm_nbr))){
                //$rep['msg'] .= $rep['is_ok'] ? '' : ', ';
                $rep['is_ok'] = FALSE;
                $rep['msg'] .= 'payeur#'.$payeur->ctm_nbr.' introuvable';
            }
        }else{
            if(trim($payeur->atn_end) == '' && trim($payeur->atn_1st) == '' && !$data_payeur_missed){
                //$rep['msg'] .= $rep['is_ok'] ? '' : ', ';
                $rep['is_ok'] = FALSE;
                $rep['msg'] .= 'données payeur manquantes';
                $data_payeur_missed = TRUE;
            }
        }
        if(is_numeric($abonne->ctm_nbr)){
            if(is_null($this->get_data_client($abonne->ctm_nbr))){
                $rep['msg'] .= $rep['is_ok'] ? '' : ', ';
                $rep['is_ok'] = FALSE;
                $rep['msg'] .= 'abonné#'.$abonne->ctm_nbr.' introuvable';
            }
        }else{
            if(trim($abonne->atn_end) == '' && trim($abonne->atn_1st) == '' && !$data_abonne_missed){
                $rep['msg'] .= $rep['is_ok'] ? '' : ', ';
                $rep['is_ok'] = FALSE;
                $rep['msg'] .= 'données abonné manquantes';
                $data_abonne_missed = TRUE;
            }
        }
        if(count($this->get_promo($promo->code_promo)) < 1){
            $rep['msg'] .= $rep['is_ok'] ? '' : ', ';
            $rep['is_ok'] = FALSE;
            $rep['msg'] .= 'code_promotion#'.$promo->code_promo.' introuvable';
        }else{
            $data_promo = $this->get_promo_choix_prod($promo);
            if(is_null($data_promo)){
                $rep['msg'] .= $rep['is_ok'] ? '' : ', ';
                $rep['is_ok'] = FALSE;
                $rep['msg'] .= 'combinaison code_promotion#'.$promo->code_promo.', code_choix#'.$promo->code_choix.', code_produit#'.$promo->code_produit.' introuvable';
            }else{
                $rn_flg = trim($data_promo->top_reab);
                if (in_array($rn_flg, array('C','c')) && $only_chq) {
                    $rep['msg'] .= $rep['is_ok'] ? '' : ', ';
                    $rep['is_ok'] = FALSE;
                    $rep['msg'] .= 'offre non payable par chèque (code_promotion#'.$promo->code_promo.', code_choix#'.$promo->code_choix.', code_produit#'.$promo->code_produit.')';
                }else{
                    $val_lst = trim($data_promo->liste_val);
                    if (!in_array($val_lst, array('Y','y','V','v'))) {
                        $rep['msg'] .= $rep['is_ok'] ? '' : ', ';
                        $rep['is_ok'] = FALSE;
                        $rep['msg'] .= 'offre invalide (code_promotion#'.$promo->code_promo.', code_choix#'.$promo->code_choix.', code_produit#'.$promo->code_produit.')';
                    }
                }
            }
        }
        return $rep;
    }

    public function mode_directe($id_pli)
    {
        if($this->no_sent($id_pli)) {
            $this->ged->trans_begin();
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_cba);
            $this->ged
                ->set('in_cba', 0)
                ->set('flag_batch', 0)
                ->where('id_pli', $id_pli)->update($this->tb_data_pli);
            if($this->ged->trans_status() === FALSE){
                $this->ged->trans_rollback();
                log_message('error', 'cba=> libraries/saisie/Data_cba mode_directe: mode direct failed pli#'.$id_pli);
                return 'Erreur serveur DB';
            }else{
                $this->ged->trans_commit();
                $this->CI->histo->action(116, 'mode traitement advantage', $id_pli);
                return 0;
            }
        }else{
            $this->ged
                ->set('in_cba', 0)
                ->set('flag_batch', 0)
                ->where('id_pli', $id_pli)->update($this->tb_data_pli);
            return 1;
        }
    }

    public function is_traited_file($fl)
    {
        $nb = $this->ged
            ->where('nom', $fl)
            ->from($this->tb_rejetCbaFl)
            ->count_all_results();
        return $nb > 0;
    }

    public function appli_rejet_batch($data)
    {//var_dump($data);return TRUE;
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        try {
            $this->ged->insert($this->tb_rejetCbaFl, $data['rejet_cba_files']);
            if (count($data['rejet_cba_lignes']) > 0) {
                $this->ged->insert_batch($this->tb_rejetCbaLn, $data['rejet_cba_lignes']);
            }
            if (count($data['id_plis_ko']) > 0) {
                $this->ged
                    ->set('flag_rejet_batch', 1)
                    ->set('flag_batch', 0)
                    ->set('in_cba', 0)
                    ->set('statut_saisie', 1)
                    ->set('flag_traitement', 3)
                    ->set('flag_saisie', 0)
                    ->set('flag_retour_batch', 1)
                    ->where_in('id_pli', $data['id_plis_ko'])
                    ->update($this->tb_data_pli);
                $this->base_64
                    ->set('statut_saisie', 1)
                    ->set('flag_traitement', 3)
                    ->set('saisie_par', NULL)
                    ->set('traite_par', 0)
                    ->set('id_lot_saisie', NULL)
                    ->set('id_decoupage', NULL)
                    ->set('flag_echantillon', 0)
                    ->set('lot_saisie_bis', 0)
                    ->set('controle_par', NULL)
                    ->set('retraite_par', NULL)
                    ->set('sorti_saisie', NULL)
                    ->set('in_decoup', 0)
                    ->set('date_saisie', NULL)
                    ->set('date_controle', NULL)
                    ->set('date_termine', NULL)
                    ->where_in('id_pli', $data['id_plis_ko'])
                    ->update($this->tb_pli);
            }
            if (count($data['id_plis_ok']) > 0) {
                $this->ged
                    ->set('flag_retour_batch', 1)
                    ->where_in('id_pli', $data['id_plis_ok'])
                    ->update($this->tb_data_pli);
            }
            if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                log_message('error', 'models> saisie> model_cba> appli_rejet_batch: transaction failed');
                return FALSE;
            }else{
                $this->ged->trans_commit();
                $this->base_64->trans_commit();
                $this->CI->histo->action_plis(115, 'rejet batch', $data['id_plis_ko']);
                $this->CI->histo->plis($data['id_plis_ko'], 'rejet batch');
                return TRUE;
            }
        } catch (Exception $exc) {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> saisie> model_cba> appli_rejet_batch: '.$exc->getMessage());
            return FALSE;
        }
        return FALSE;
    }

}
