<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_base_reb extends CI_Model
{
    private $base_reb_temp;
    private $base_reb;
    private $tb_reb_temp = TB_reb_temp;
    private $tb_base_reb = TB_base_reb;


    public function __construct()
    {
    	parent::__construct();
        $this->base_reb_temp = $this->load->database('base_reb_temp', TRUE);
        $this->base_reb = $this->load->database('base_reb', TRUE);

    }

     	public function get_reb_from_temp(){
		$this->base_reb_temp
				->select("*")
				->from($this->tb_reb_temp);
				//->where('vpc.etat_machage', 3);
		
		$r= $this->base_reb_temp->get()->result();
        //var_dump($r);
        return $r;
		
	}

    public function save_reb($data, $id_doc){
    $reponse = FALSE;
        if(!$this->existe_reb($data)){

            //var_dump("ok : ".$id_doc);
            if($this->base_reb->insert('remise_en_banque_msc_new', $data)){
                log_message('error', 'info=> model_cron> save_reb -> enregistrement: pli#'.$data['id_pli'].'> doc#'.$id_doc
                        .'\n\t, montant:'.$data['montant']
                        .'\n\t, numero_serie:'.$data['numero_serie']
                        .'\n\t, zib:'.$data['zib']
                        .'\n\t, numero_compte:'.$data['numero_compte']
                        .'\n\t, commande:'.$data['commande']);
                //$this->update_reb_doc($id_doc, $data['id_pli']);
                $reponse = TRUE;
            }else{
                log_message('error', 'info=> model_cron> save_reb -> erreur enregistrement: pli#'.$data['id_pli'].'> doc#'.$id_doc);
            }
        }else{
            //var_dump("ko : ".$id_doc);
            log_message('error', 'info=> model_cron> save_reb -> already existed line pli#'.$data['id_pli'].', doc#'.$id_doc
                    //.'\n\t, traitement:'.$data['traitement']
                    .'\n\t, dt: '.$data['date_creation']);
        }
        return $reponse;
    }

    private function existe_reb($data){
        $where = array(
            'idenveloppe' => $data['idenveloppe']
            ,'id_pli' => $data['id_pli']
            //,'cmc7' => $data['cmc7']
        );
        $dt_hr_creation = explode(' ', $data['date_creation']);
        $dt_creation = isset($dt_hr_creation[0]) ? $dt_hr_creation[0] : '';
        return $this->base_reb
                ->where($where)
                ->group_start()
                    ->where('traitement', $data['traitement'])
                    ->or_like('date_creation', $dt_creation, 'after')
                ->group_end()
                ->from('remise_en_banque_msc_new')
                ->count_all_results() > 0;
    }

    public function get_nb_volume(){
        return $this->base_reb
            ->like('date_creation', date('Y-m-d'), 'after')
            ->from('remise_en_banque_msc_new')
            ->count_all_results();
    }

    public function save_volume($data){
        $this->base_reb->where('date', date('Y-m-d'))->delete('volume_ged_bayard_reb');
        $this->base_reb->insert('volume_ged_bayard_reb', $data);
        
    }

}