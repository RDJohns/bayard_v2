<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Model_cron_reb_temp extends CI_Model
{
	private $CI;

    private $ged;
    private $base_64;
    private $base_reb_temp;
    private $base_reb;

    private $wv_pcheque = Vw_pcheque;
    private $tb_document = TB_document;
    private $tb_pli = TB_pli;
    private $tb_lotNum = TB_lotNumerisation;
    private $wv_d_pli = Vw_dataPli;
    private $wv_typo = Vw_typologie;
    private $wv_statSaisie = Vw_statutSaisie;
    private $wv_societe = Vw_Societe;
    private $tb_cheque = TB_cheque;
    private $tb_synchro = TB_synchro;
    private $tb_reb_temp = TB_reb_temp;

    public function __construct()
    {
    	parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->base_reb_temp = $this->load->database('base_reb_temp', TRUE);
        $this->base_reb = $this->load->database('base_reb', TRUE);
    }

    	public function get_pli_from_ged(){
		$this->base_64
				->select('doc.id_document id_doc')
				->select('doc.id_pli id_pli')
				->select('doc.fichier_tri fichier_tri')
				->select('doc.n_ima_recto n_ima_recto')
				->select('doc.n_ima_verso n_ima_verso')
				->select('pli.pli pli')
				->select('pli.flag_traitement flag_traitement')	
				->select('pli.date_tri date_tri')
				->select('vpc.cmc7 cmc7')
				->select('vpc.montant montant')
				->select('lnm.lot_scan lot_scan')
				->select('lnm.commande commande')
				->select('vdp.statut_saisie statut_saisie')
				->select('vdp.dt_enregistrement')
				->select('vdp.societe')
				->select('vt.typologie')
				->select('vpc.etat_reb')
				->select('vst.libelle');
				
		$this->base_64
				->from($this->tb_document.' doc')
				->join($this->tb_pli.' pli', 'pli.id_pli = doc.id_pli', 'INNER')
				->join($this->wv_pcheque.' vpc', 'vpc.id_doc = doc.id_document', 'LEFT')
				->join($this->tb_lotNum.' lnm', 'lnm.id_lot_numerisation = pli.id_lot_numerisation', 'INNER')
				->join($this->wv_d_pli.' vdp', 'vdp.id_pli = pli.id_pli', 'INNER')
				->join($this->wv_typo.' vt', 'vt.id = pli.typologie', 'INNER')
				->join($this->wv_statSaisie.' vst','vst.id_statut_saisie = vdp.statut_saisie', 'INNER' )
				//->where('doc.type_document', 2)
				->where_not_in('pli.flag_traitement', array(16, 21, 24))
				->where('vdp.dt_enregistrement::date < \''.date('Y-m-d').'\'', null,false)
				->where('vpc.etat_matchage', 2)
				->where('vpc.flag_reb', 0);
		return $this->base_64->get()->result();
			
	}

	public function save_reb($data, $id_pli){
	$reponse = FALSE;
		if(!$this->existe_reb($data)){
			if($this->base_reb_temp->insert('remise_en_banque_msc_new', $data)){
				log_message_reb('error', 'info=> model_cron> save_reb -> enregistrement: pli#'.$data['id_pli'].'> doc#'.$id_pli
						.'\n\t, montant:'.$data['montant']
						.'\n\t, numero_serie:'.$data['numero_serie']
						.'\n\t, zib:'.$data['zib']
						.'\n\t, numero_compte:'.$data['numero_compte']
						.'\n\t, commande:'.$data['commande']);
				
				$reponse = TRUE;
			}else{
				log_message_reb('error', 'info=> model_cron> save_reb -> erreur enregistrement: pli#'.$data['id_pli'].'> doc#'.$id_pli);
			}
		}else{
			//var_dump("ko : ".$id_doc);
			log_message_reb('error', 'info=> model_cron> save_reb -> already existed line pli#'.$data['id_pli'].', doc#'.$id_pli
					//.'\n\t, traitement:'.$data['traitement']
					.'\n\t, dt: '.$data['date_creation']);
		}
		return $reponse;
   	}

   	/*public function save_remise_en_banque($data, $id_pli){
	$reponse = FALSE;
	//var_dump($this->existe_base_reb($data));
		if(!$this->existe_base_reb($data)){
			if($this->base_reb->insert('remise_en_banque_msc_new', $data)){
				log_message('error', 'info=> model_cron> save_reb -> enregistrement: pli#'.$data['id_pli'].'>;'
						.'\n\t, montant:'.$data['montant']
						.'\n\t, numero_serie:'.$data['numero_serie']
						.'\n\t, zib:'.$data['zib']
						.'\n\t, numero_compte:'.$data['numero_compte']
						.'\n\t, commande:'.$data['commande']);
				$this->update_flag_cheque();
				//var_dump($r);
				$reponse = TRUE;
			}else{
				log_message('error', 'info=> model_cron> save_reb -> erreur enregistrement: pli#'.$data['id_pli'].'> ;');
			}
		}else{
			//var_dump("ko : ".$id_doc);
			log_message('error', 'info=> model_cron> save_reb -> already existed line pli#'.$data['id_pli'].', doc#'.$id_pli
					//.'\n\t, traitement:'.$data['traitement']
					.'\n\t, dt: '.$data['date_creation']);
		}
		return $reponse;
   	}*/

   	private function existe_reb($data){
		$where = array(
			'idenveloppe' => $data['idenveloppe']
			,'id_pli' => $data['id_pli']
			,'numero_serie' =>$data['numero_serie']
			,'zib' => $data['zib']
			,'numero_compte' => $data['numero_compte']

			//,'cmc7' => $data['cmc7']
		);
		//var_dump($where_base);
		return $this->base_reb_temp
		        ->select("CONCAT(numero_serie,zib,numero_compte)")
				->from('remise_en_banque_msc_new')
				->where($where)
				->count_all_results() > 0;
	}

   	/*private function existe_base_reb($data){
		$where_base = array(
			'idenveloppe' => $data['idenveloppe']
			,'id_pli' => $data['id_pli']
			,'numero_serie' =>$data['numero_serie']
			,'zib' => $data['zib']
			,'numero_compte' => $data['numero_compte']
			
		);
		return $this->base_reb
		        ->select("CONCAT(numero_serie,zib,numero_compte)")
		        ->from('remise_en_banque_msc_new')
				->where($where_base)
				->count_all_results() > 0;
	}*/

	public function update_flag_cheque(){
		return $this->ged
		     ->where('etat_matchage', 3)
			 ->set('flag_reb', 1)
			 ->update($this->tb_cheque); 
	}

	public function update_synchro(){
		return $this->base_reb_temp
			  ->where('date(date_traitement) =  \''.date('Y-m-d').'\'', null,false)
			  ->set('flag',2)
			  ->update($this->tb_synchro);
	}

	/*public function delete_table_flag_0(){
	    return $this->base_reb_temp->empty_table($this->tb_reb_temp);
	}*/

	public function get_flag_synchro(){
		$this->base_reb_temp
			 ->select('flag')
			 ->from($this->tb_synchro)
			 ->where('date(date_traitement) =  \''.date('Y-m-d').'\'', null,false);
		return $this->base_reb_temp->get()->result();	
	}  

    /*public function get_reb_from_temp(){
    	$this->base_reb_temp
    		 ->select('*')
    		 ->from($this->tb_reb_temp);
   		return $this->base_reb_temp->get()->result();		 
    }*/
    public function get_nb_volume(){
        	return $this->base_reb_temp
            ->like('date_creation', date('Y-m-d'), 'after')
            ->from('remise_en_banque_msc_new')
            ->count_all_results();
    }

    public function save_volume($data){
        $this->base_reb->where('date', date('Y-m-d'))->delete('volume_ged_bayard_reb');
        $this->base_reb->insert('volume_ged_bayard_reb', $data);
        
    }

    /*public function update_synchro(){
		return $this->base_reb_temp
			  ->where('date(date_traitement) =  \''.date('Y-m-d').'\'', null,false)
			  ->set('flag',2)
			  ->update($this->tb_synchro);
	}*/

}
