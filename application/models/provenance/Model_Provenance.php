<?php

class Model_Provenance extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;
    
    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }
    
    public function recapProvenance($date1,$date2,$societe)
    {
        $sql    = "SELECT distinct *from public.f_provenance_recap('".$date1."','".$date2."',".$societe.") order by provenance;";
        $query  = $this->ged->query($sql);
        return $query->result();
    }
    
    public function provenanceParDateMilan($date1,$date2)
    {
        $sql    = "SELECT distinct *from public.f_provenance_par_date_milan('".$date1."','".$date2."') order by date_courrier;";
        $query  = $this->ged->query($sql);
        return $query->result();
    }

    public function provenanceParDateBayard($date1,$date2)
    {
        $sql    = "SELECT distinct *from public.f_provenance_par_date_bayard('".$date1."','".$date2."') order by date_courrier;";
        $query  = $this->ged->query($sql);
        return $query->result();
    }
    public function provenanceParTypologieBayard($date1,$date2)
    {
        $sql    = "SELECT distinct *from public.f_provenance_typologie_bayard('".$date1."','".$date2."') order by provenance;";
        $query  = $this->ged->query($sql);
        return $query->result();
    }
    public function provenanceParTypologieMilan($date1,$date2)
    {
        $sql    = "SELECT distinct *from public.f_provenance_typologie_milan('".$date1."','".$date2."') order by provenance;";
        $query  = $this->ged->query($sql);
        return $query->result();
    }
    public function provenanceParTypologie($date1,$date2,$societe)
    {
        $sql    = "SELECT distinct *from public.f_provenance_typologie('".$date1."','".$date2."',".(int)$societe.");";
        
        $query  = $this->ged->query($sql);

        return $query->result();
    }
    
}