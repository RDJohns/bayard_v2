<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_statistics extends CI_Model{

    private $CI;

    private $ged;
    private $base_64; 
    private $ged_flux; 
	protected $SELECT            = array('pli','id_pli','typologie','type_ko','flag_traitement','traitement','dt_event','lot_scan','date_courrier','date_numerisation','statut','code_type_pli','comment','nb_doc');
    protected $column_order      = array('date_courrier','date_numerisation','lot_scan','pli','dt_event','comment','statut','nb_doc','id_pli','typologie','type_ko','flag_traitement','traitement','code_type_pli');
    protected $column_search     = array('date_courrier','date_numerisation','dt_event','lot_scan','comment','statut');
    protected $order             = array('date_courrier' => 'asc','date_numerisation' => 'asc','lot_scan' => 'asc','id_pli' => 'asc');
	
	protected $select_ttt        = array('nom_societe','dt_event','date_courrier','lot_scan','pli','statut','libelle','txt_anomalie','libelle_motif','type_ko','typologie','mode_paiement','cmc7','montant','rlmc','infos_datamatrix','motif_rejet','description_rejet','titre_titre','titre_societe_nom','nb_mvt','id_pli');
    protected $column_order_ttt  = array('nom_societe','dt_event','date_courrier','lot_scan','pli','statut','libelle','txt_anomalie','libelle_motif','type_ko','typologie','mode_paiement','cmc7','montant','rlmc','infos_datamatrix','motif_rejet','description_rejet','titre_titre','titre_societe_nom','nb_mvt','id_pli');
    protected $column_search_ttt = array('nom_societe','dt_event','date_courrier','lot_scan','pli','statut','libelle','txt_anomalie','libelle_motif','type_ko','typologie','mode_paiement','cmc7','montant','rlmc','infos_datamatrix','motif_rejet','description_rejet','titre_titre','titre_societe_nom','nb_mvt','id_pli');
    protected $order_ttt         = array('dt_event'=> 'asc','date_courrier'=> 'asc','lot_scan'=> 'asc','pli'=> 'asc','statut'=> 'asc','libelle'=> 'asc');

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->ged_flux = $this->load->database('ged_flux', TRUE);
    }
	
	
	public function get_dates($date_debut, $date_fin){
		$sql = "SELECT liste_dates('".$date_debut."','".$date_fin."') as daty";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_dates_months($date_debut, $date_fin){
		$sql = "SELECT substr(liste_months::text,1,7) as date_mth FROM liste_months ('".$date_debut."','".$date_fin."')";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_week_of_date($date_debut){
		$sql = "SELECT extract('week' FROM '".$date_debut."'::date) as week ";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_month_of_date($date_debut){
		$sql = "SELECT concat(EXTRACT(YEAR FROM TIMESTAMP '".$date_debut."'),'-',EXTRACT(MONTH FROM TIMESTAMP '".$date_debut."')) as mois;";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_last_month($date_debut){
		$sql = "SELECT (date '".$date_debut."' + interval '1 month')::date as date_fin ";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_list_traitement($date_debut, $date_fin, $id_societe){
		
		$where_date = $where_view_pli = "";
		if($date_debut != '' && $date_fin != ''){
				$where_date 	.= " view_histo_pli_oid.dt_event::date between '".$date_debut."' AND '".$date_fin."' ";
				
        }
		if($id_societe != ''){
				$where_date 	.= " AND view_pli_stat.societe = '".$id_societe."' ";
				
        }
		
		$sql = "
				SELECT 
				traitement.id_pli,pli,flag_traitement,traitement,lot_scan,typologie,code_type_pli,statut, pli_avec_chq,
				id_societe,
				nom_societe,
				type_ko,
				typage_par,
				txt_anomalie,
				libelle_motif,
				comment,
				date_courrier::date,
				traitement.dt_event,
				montant,
				cmc7,
				rlmc,
				anomalie_cheque_id,
				id_mode_paiement,			
				mode_paiement,			
				actif as mode_paiement_actif,
				titre,			
				societe_vf,
				infos_datamatrix,
				code_promotion,
				motif_rejet,
				description_rejet,
				statut_saisie,
				motif_ko,
				autre_motif_ko,
				nom_circulaire,
				fichier_circulaire,
				message_indechiffrable,
				dmd_kdo_fidelite_prim_suppl,
				dmd_envoi_kdo_adrss_diff,
				type_coupon,
				id_erreur,
				comm_erreur,
				nom_orig_fichier_circulaire,
				titre_id,
				titre_titre,
				titre_societe_id,
				titre_societe_nom
				FROM ( 
			 
			 
					SELECT 
						view_histo_pli_oid.id_pli, 
						view_histo_pli_oid.oid,
						view_pli_stat.flag_traitement,
						traitement,
						typologie as code_type_pli,
						view_histo_pli_oid.dt_event::date,
						CASE WHEN pli_non_traite = 1 THEN 'Non traité' ELSE 
						CASE WHEN pli_en_cours_typage = 1 THEN 'En cours de typage' ELSE
						CASE WHEN pli_typage_termine = 1 THEN 'Typage terminé'  ELSE 
						CASE WHEN pli_en_cours_saisie = 1 THEN 'En cours de saisie'  ELSE
						CASE WHEN pli_saisie_terminee = 1 THEN 'Saisie terminé' ELSE 						
						CASE WHEN pli_decoupe = 1 THEN 'Découpé' ELSE					
						CASE WHEN pli_echantillonage_encours = 1 THEN 'Echantillonage en cours' ELSE					
						CASE WHEN pli_controle_ech_termine = 1 THEN 'Echantillonage termine' ELSE					
						CASE WHEN pli_en_cours_controle = 1 THEN 'En cours de controle' ELSE					
						CASE WHEN pli_validation_en_cours = 1 THEN 'En cours de validation' ELSE 						
						CASE WHEN pli_correction_en_cours = 1 THEN 'En cours de correction' ELSE 						
						CASE WHEN pli_matchage_termine = 1 THEN 'Matchage terminé' ELSE
						CASE WHEN pli_ferme = 1 THEN 'Fermé' ELSE
						CASE WHEN pli_cloture = 1 THEN 'Cloturé' ELSE
						CASE WHEN pli_ko_scan = 1 THEN 'KO scan' ELSE 						
						CASE WHEN pli_ko_saisie = 1 THEN 'KO saisie' ELSE 						
						CASE WHEN pli_hors_perimetre = 1 THEN 'Hors périmètre' ELSE 						
						CASE WHEN pli_ko_ks = 1 THEN 'KS' ELSE 						
						CASE WHEN pli_ko_ke = 1 THEN 'KE' ELSE 						
						CASE WHEN pli_ko_kd = 1 THEN 'KD' ELSE 						
						CASE WHEN pli_ci = 1 THEN 'CI' ELSE 						
						CASE WHEN pli_ko = 1 THEN 'KO' ELSE ''						
						END END END END END END END END END END END END END END END END END END END END END END as statut,
						'' as comment,
						flag_chq as pli_avec_chq,
						CASE WHEN pli_ko_scan = 1 THEN 'KO scan' ELSE 
						CASE WHEN pli_ko_saisie = 1 THEN 'KO saisie' ELSE 
						CASE WHEN pli_hors_perimetre = 1 THEN 'Hors périmètre' ELSE ''
						END END END type_ko,
						typage_par,
						lot_scan, 
						date_courrier, 
						pli, 
						typologie,
						view_pli_stat.societe as id_societe,
						view_pli_stat.nom_societe,
						paiement_cheque.montant,
						paiement_cheque.cmc7,
						paiement_cheque.rlmc,
						paiement_cheque.anomalie_cheque_id,
						mode_paiement.id_mode_paiement,			
						mode_paiement.mode_paiement,			
						mode_paiement.actif,
						data_pli.infos_datamatrix,			
						data_pli.titre,			
						data_pli.societe societe_vf,
						data_pli.code_promotion,
						data_pli.motif_rejet,
						data_pli.description_rejet,
						data_pli.statut_saisie,
						data_pli.motif_ko,
						data_pli.autre_motif_ko,
						data_pli.nom_circulaire,
						data_pli.fichier_circulaire,
						data_pli.message_indechiffrable,
						data_pli.dmd_kdo_fidelite_prim_suppl,
						data_pli.dmd_envoi_kdo_adrss_diff,
						data_pli.type_coupon,
						data_pli.id_erreur,
						data_pli.comm_erreur,
						data_pli.nom_orig_fichier_circulaire,
						motif_ko.libelle_motif as libelle_motif,			
						motif_ko_scan.description as txt_anomalie,
						titres.code as titre_id,
						titres.titre as titre_titre,
						titres.societe_id as titre_societe_id,
						societe.nom_societe as titre_societe_nom
															
					FROM view_histo_pli_oid
					INNER join view_pli_stat on view_pli_stat.id_pli = view_histo_pli_oid.id_pli
					LEFT JOIN data_pli on view_pli_stat.id_pli = data_pli.id_pli 
					LEFT JOIN paiement on paiement.id_pli = view_pli_stat.id_pli  
					LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli AND paiement.id_mode_paiement = 1
					LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
					LEFT JOIN motif_ko on motif_ko.id_motif = data_pli.motif_ko
					LEFT JOIN motif_ko_scan on data_pli.id_pli = motif_ko_scan.pli_id
					LEFT JOIN titres on titres.id = data_pli.titre
					LEFT JOIN societe on societe.id = titres.societe_id
					WHERE ".$where_date." 						
				
			) as traitement 
			WHERE    (
				(flag_traitement in (9,14,15,20,25) AND pli_avec_chq is null) 
				or ( flag_traitement = 26 AND pli_avec_chq is not null) 
				or ( flag_traitement = 18  ) 
				or ( flag_traitement = 24  ) 
				or (flag_traitement in (16,17,21)) 
				or (flag_traitement in (23, 25))
			)
			ORDER BY dt_event, date_courrier,lot_scan,id_pli  asc
			";
		/*echo "<pre>";
		print_r($sql);
		echo "</pre>";exit;	*/
		return $this->ged->query($sql)
							->result_array();
    }
    public function get_list_plis($date_debut, $date_fin){
		
		$where_like = $where_view_pli = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " date_courrier between '".$date_debut."' AND '".$date_fin."' ";
				$where_view_pli .= " view_lot.date_courrier  between '".$date_debut."' AND '".$date_fin."' ";
        }
		$sql = "";		
			/*echo "<pre>";	
			print_r($sql);
			echo "</pre>";exit;	*/
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_list_document($date_debut, $date_fin){
		
		$where_like = $where_view_pli = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " date_courrier between '".$date_debut."' AND '".$date_fin."' ";
				$where_view_pli .= " date_courrier  between '".$date_debut."' AND '".$date_fin."' ";
        }
		
		$sql = "";	
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";exit;*/
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_list_plis_traite($date_debut, $date_fin){
		
		$where_like = $where_view_pli = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " date_courrier between '".$date_debut."' AND '".$date_fin."' ";
				$where_view_pli .= " view_lot.date_courrier  between '".$date_debut."' AND '".$date_fin."' ";
        }
		
		$sql = "";	
		 /* echo "<pre>";
        print_r($sql);
        echo "</pre>";exit;*/
		return $this->ged->query($sql)
							->result_array();
    }
	
	
    public function get_stat_plis( $date_debut, $date_fin){
        
		$where_like = $where_view_pli = "";

        /*if($numcmd != ''){
            $where_like .= " valeur = '".$numcmd."' ";
        }        
		*/
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " date_courrier between '".$date_debut."' AND '".$date_fin."' ";
				$where_view_pli .= " view_lot.date_courrier  between '".$date_debut."' AND '".$date_fin."' ";
        }

      	$sql = ""	;		
       /* echo "<pre>";
        print_r($sql);
        echo "</pre>";exit;*/
		return $this->ged->query($sql)
							->result_array();

    }

	public function get_plis_recu_par_date($date_debut, $date_fin){
        
		$where_like = $where_view_pli = "";
       
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " date_courrier between '".$date_debut."' AND '".$date_fin."' ";
				$where_view_pli .= " date_courrier  between '".$date_debut."' AND '".$date_fin."' ";
        }

      	$sql = ""	;		
        
		return $this->ged->query($sql)
							->result_array();

    }   
    
	
	public function get_reception_par_date($date_debut, $date_fin){
        /* Lss plis parents qui ont été dupliqués sont flag_ttt à 10 => à ne plus compter */
		$where_like = "";        
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " date_courrier between '".$date_debut."' AND '".$date_fin."' ";
	    }

      	$sql = "SELECT count(distinct view_pli.id_pli) as nb_plis,date_courrier::date
			FROM view_lot
			INNER join view_pli on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
			WHERE ".$where_like."
			GROUP BY date_courrier::date
			ORDER BY date_courrier asc"	;		
        
		return $this->ged->query($sql)
							->result_array();

    }
	public function get_reception_typologie_par_date($date_debut, $date_fin){
        /* Lss plis parents qui ont été dupliqués sont flag_ttt à 10 => à ne plus compter */      	
		$where_like = "";        
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " date_courrier between '".$date_debut."' AND '".$date_fin."' ";
	    }
		$sql = "SELECT count(distinct view_pli.id_pli) as nb_plis,view_pli.typologie
			FROM view_lot
			INNER join view_pli on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
			WHERE ".$where_like."
			GROUP BY view_pli.typologie
			ORDER BY view_pli.typologie asc"	;		
        
		return $this->ged->query($sql)
							->result_array();

    }
	public function get_stat_traitement( $date_debut, $date_fin,$id_societe){
        $WHERE = $whereClause = "";
		if($id_societe != '' && $id_societe != null){
			$WHERE = " AND view_pli_stat.societe = '".$id_societe."'";
			$whereClause = " AND societe = '".$id_societe."'";
		}
	
      	$sql = "
				SELECT
					CASE WHEN
					sum(pli_traite)+sum(pli_ke) is null
					THEN 0 ELSE
					sum(pli_traite)+sum(pli_ke) END as pli_total,
					CASE WHEN SUM(pli_traite) is null THEN 0 ELSE SUM(pli_traite) END as traite,
					CASE WHEN SUM(pli_ok_advantage) is null THEN 0 ELSE SUM(pli_ok_advantage) END as pli_ok_advantage,
					CASE WHEN SUM(pli_ko_scan) is null THEN 0 ELSE SUM(pli_ko_scan) END as pli_ko_scan,
					CASE WHEN SUM(pli_hors_perimetre) is null THEN 0 ELSE SUM(pli_hors_perimetre) END as pli_hors_perimetre,
					CASE WHEN SUM(pli_ks) is null THEN 0 ELSE SUM(pli_ks) END as pli_ks,
					CASE WHEN SUM(pli_kd) is null THEN 0 ELSE SUM(pli_kd) END as pli_kd,
					CASE WHEN SUM(pli_ci) is null THEN 0 ELSE SUM(pli_ci) END as pli_ci,
					CASE WHEN SUM(pli_ok) is null THEN 0 ELSE SUM(pli_ok) END as pli_ok,
					CASE WHEN SUM(pli_ke) is null THEN 0 ELSE SUM(pli_ke) END as pli_ke
				FROM
				(
					SELECT 
					CASE WHEN pli_cloture = 1   THEN count(distinct id_pli) ELSE 0 END as pli_traite,
					CASE WHEN pli_ok = 1   THEN count(distinct id_pli) ELSE 0 END as pli_ok,
					CASE WHEN (date_saisie_advantage between '".$date_debut."' AND '".$date_fin."') AND id_lot_saisie is not null  THEN count(distinct id_pli) ELSE 0 END as pli_ok_advantage,
					CASE WHEN pli_ko_scan = 1  THEN count(distinct id_pli) ELSE 0 END as pli_ko_scan,						 
					CASE WHEN pli_hors_perimetre = 1 THEN count(id_pli) ELSE 0 END as pli_hors_perimetre,
					CASE WHEN pli_ko_ks = 1 THEN count(id_pli) ELSE 0 END as pli_ks,
					CASE WHEN pli_ko_ke = 1 THEN count(id_pli) ELSE 0 END as pli_ke,
					CASE WHEN pli_ko_kd = 1  THEN count( id_pli) ELSE 0 END as pli_kd,
					CASE WHEN pli_ci = 1 THEN count(distinct id_pli) ELSE 0 END as pli_ci
					
					FROM 
					(
						SELECT 
							view_pli_stat.id_pli, 
							view_pli_stat.flag_traitement ,
							view_pli_stat.dt_event::date,
							pli_cloture,
							pli_ok,
							pli_ko_scan,
							pli_hors_perimetre,
							pli_ko_ks,
							pli_ko_kd,
							pli_ko_ke,
							pli_ci,
							id_lot_saisie,
							date_saisie_advantage							
						FROM view_pli_stat
						WHERE 
						view_pli_stat.dt_event::date between '".$date_debut."' AND '".$date_fin."' ".$WHERE."						
					  )
					 as traitement 
					 GROUP BY flag_traitement,flag_traitement,pli_cloture,pli_ok, pli_ko_scan, pli_hors_perimetre, pli_ko_ks, pli_ko_kd, pli_ko_ke, pli_ci,id_lot_saisie,date_saisie_advantage
				 ) as result "	;		
        
		/*echo "<pre>";
		print_r($sql);
		echo "</pre>";exit;
		*/
		
		return $this->ged->query($sql)
							->result_array();

    }
	
	public function get_stat_solde( $date_debut, $date_fin,$id_societe,$rubrique){
        $WHERE = $clauseWhere = "";
		
		if($date_debut != '' && $date_fin != ''){
				$clauseWhere 	.= " histo_pli.dt_event::date between '".$date_debut."' AND '".$date_fin."' ";				
		  }
		if($id_societe != ''){
				$clauseWhere .= " AND data_pli.societe = '".$id_societe."' ";
			
        }
		/*
		SELECT distinct
				histo_pli.dt_event::date,	
				histo_pli.id_pli, 
				histo_pli.flag_traitement,
				traitement,
				typologie as code_type_pli,
				pli,
				typologie,
				flag_chq as pli_avec_chq,
				pli_non_traite,
				pli_cloture,
				pli_ko,
				pli_ko_scan,
				pli_hors_perimetre,
				pli_ko_ks,
				pli_ko_ke,
				pli_ko_kd,
				pli_ci,
				pli_en_cours,
				id_lot_saisie,
				date_saisie_advantage,
				nb_mvt,
				flag_chq,
				data_pli.statut_saisie,
				view_pli_stat.societe
				FROM histo_pli
				INNER JOIN view_pli_traitement ON view_pli_traitement.id  = histo_pli.id
				INNER join view_pli_stat on view_pli_stat.id_pli = histo_pli.id_pli
				LEFT JOIN data_pli on view_pli_stat.id_pli = data_pli.id_pli 
				LEFT JOIN view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = data_pli.id_pli 
				WHERE ".$clauseWhere."
		*/	

		$sql="SELECT	
			dt_event as date_j,societe,
			CASE WHEN SUM(pli_traite) is null THEN 0 ELSE SUM(pli_traite) END as traite,
			CASE WHEN SUM(ok) is null THEN 0 ELSE SUM(ok) END as ok,
			CASE WHEN SUM(ko) is null THEN 0 ELSE SUM(ko) END as ko,
			CASE WHEN SUM(ko_scan) is null THEN 0 ELSE SUM(ko_scan) END as ko_scan,
			CASE WHEN SUM(hors_perimetre) is null THEN 0 ELSE SUM(hors_perimetre) END as hors_perimetre,
			CASE WHEN SUM(ko_ks) is null THEN 0 ELSE SUM(ko_ks) END as ks,
			CASE WHEN SUM(ko_ke) is null THEN 0 ELSE SUM(ko_ke) END as ke,
			CASE WHEN SUM(ko_def) is null THEN 0 ELSE SUM(ko_def) END as kd,
			CASE WHEN SUM(ko_circulaire) is null THEN 0 ELSE SUM(ko_circulaire) END as ci,
			CASE WHEN SUM(ok_advantage) is null THEN 0 ELSE SUM(ok_advantage) END as ok_advantage,
			sum(nb_mvt) nb_mvt
			
		FROM
		(
			SELECT 
				 id_pli, flag_traitement,typologie,
				 CASE WHEN (flag_traitement = ANY (ARRAY[9, 14, 15]) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL  AND statut_saisie = 1) THEN 1 ELSE 0 END as ok,
				 CASE WHEN (flag_traitement = ANY (ARRAY[16, 21])) OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6])) THEN count(distinct id_pli) ELSE 0 END as ko, 						 
				 CASE WHEN (flag_traitement = 16)  THEN count(distinct id_pli) ELSE 0 END as ko_scan,						 
				 CASE WHEN (flag_traitement = 21) THEN count(id_pli) ELSE 0 END as hors_perimetre,
				 CASE WHEN (flag_traitement = 18 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 4)) THEN 1 ELSE 0 END as ko_ks,
				 CASE WHEN (flag_traitement = 19 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 5)) THEN 1 ELSE 0 END as ko_ke,
				 CASE WHEN (flag_traitement = 20 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 6]))) THEN 1 ELSE 0 END as ko_def,
				 CASE WHEN (flag_traitement = 25 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = 3)) THEN 1 ELSE 0 END as ko_circulaire,						 
				 CASE WHEN (date_saisie_advantage between '".$date_debut."' AND '".$date_fin."' ) AND id_lot_saisie is not null AND flag_traitement != 26 THEN count(distinct id_pli) ELSE 0 END as ok_advantage,				 
				 CASE WHEN  (flag_traitement = ANY (ARRAY[9, 14, 15, 18]) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL )OR (flag_traitement = 20) OR (flag_traitement = 25) THEN count(distinct id_pli) ELSE 0 END as pli_traite,
				 CASE WHEN flag_traitement != 26 THEN nb_mvt ELSE 0 END nb_mvt,
				 dt_event,
				 societe
			FROM
			(
			
				SELECT distinct
				histo_pli.dt_event::date,	
				histo_pli.id_pli, 
				histo_pli.flag_traitement,
				typologie,
				id_lot_saisie,
				data_pli.dt_enregistrement::date AS date_saisie_advantage,
				nb_mvt,
				view_pli_avec_cheque_new.id_pli AS flag_chq,
				data_pli.statut_saisie,
				data_pli.societe
				FROM histo_pli
				INNER JOIN view_pli_traitement ON view_pli_traitement.id  = histo_pli.id
				INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
				LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli 
				LEFT JOIN view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = data_pli.id_pli 
				LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli  
				WHERE ".$clauseWhere."
			) as traitement
			GROUP BY id_pli,flag_traitement,typologie,nb_mvt,dt_event,id_lot_saisie,date_saisie_advantage,flag_chq,statut_saisie,societe

		) as result GROUP BY dt_event,societe
		ORDER BY dt_event asc
		";
		/*		echo "<pre>";
				print_r($sql);
				echo "</pre>";exit;
		*/		
		return $this->ged->query($sql)
							->result_array();

    }
	
	public function get_stat_solde_mvt( $date_debut, $date_fin,$id_societe,$rubrique){
        $WHERE = $clauseWhere = "";
		
		
		if($date_debut != '' && $date_fin != ''){
				$clauseWhere 	.= " histo_pli.dt_event::date between '".$date_debut."' AND '".$date_fin."' ";				
		  }
		if($id_societe != ''){
				$clauseWhere .= " AND data_pli.societe = '".$id_societe."' ";
			
        }
		
		$sql="SELECT	
			dt_event as date_j,societe,
			CASE WHEN SUM(pli_traite) is null THEN 0 ELSE SUM(pli_traite) END as traite,
			CASE WHEN SUM(ok) is null THEN 0 ELSE SUM(ok) END as ok,
			CASE WHEN SUM(ko) is null THEN 0 ELSE SUM(ko) END as ko,
			CASE WHEN SUM(ko_scan) is null THEN 0 ELSE SUM(ko_scan) END as ko_scan,
			CASE WHEN SUM(hors_perimetre) is null THEN 0 ELSE SUM(hors_perimetre) END as hors_perimetre,
			CASE WHEN SUM(ko_ks) is null THEN 0 ELSE SUM(ko_ks) END as ks,
			CASE WHEN SUM(ko_ke) is null THEN 0 ELSE SUM(ko_ke) END as ke,
			CASE WHEN SUM(ko_def) is null THEN 0 ELSE SUM(ko_def) END as kd,
			CASE WHEN SUM(ko_circulaire) is null THEN 0 ELSE SUM(ko_circulaire) END as ci,
			CASE WHEN SUM(ok_advantage) is null THEN 0 ELSE SUM(ok_advantage) END as ok_advantage,
			sum(nb_mvt) nb_mvt
			
		FROM
		(
			SELECT 
				 id_pli, flag_traitement,typologie,
				 CASE WHEN (flag_traitement = ANY (ARRAY[9, 14, 15]) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL  AND statut_saisie = 1) THEN sum(nb_mvt) ELSE 0 END as ok,
				 CASE WHEN (flag_traitement = ANY (ARRAY[16, 21])) OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6])) THEN sum(nb_mvt) ELSE 0 END as ko, 						 
				 CASE WHEN (flag_traitement = 16)  THEN sum(nb_mvt) ELSE 0 END as ko_scan,						 
				 CASE WHEN (flag_traitement = 21) THEN sum(nb_mvt) ELSE 0 END as hors_perimetre,
				 CASE WHEN (flag_traitement = 18 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 4)) THEN sum(nb_mvt) ELSE 0 END as ko_ks,
				 CASE WHEN (flag_traitement = 19 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 5)) THEN sum(nb_mvt) ELSE 0 END as ko_ke,
				 CASE WHEN (flag_traitement = 20 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 6]))) THEN sum(nb_mvt) ELSE 0 END as ko_def,
				 CASE WHEN (flag_traitement = 25 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = 3)) THEN sum(nb_mvt) ELSE 0 END as ko_circulaire,						 
				 CASE WHEN (date_saisie_advantage between '".$date_debut."' AND '".$date_fin."' ) AND id_lot_saisie is not null AND flag_traitement != 26 THEN sum(nb_mvt) ELSE 0 END as ok_advantage,
				 
				 CASE WHEN  (flag_traitement = ANY (ARRAY[9, 14, 15, 18]) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL )OR (flag_traitement = 20) OR (flag_traitement = 25) THEN sum(nb_mvt) ELSE 0 END as pli_traite,
				 CASE WHEN flag_traitement != 26 THEN sum(nb_mvt) ELSE 0 END as nb_mvt,				 
				 dt_event,
				 societe
			FROM
			(
			
				SELECT distinct
				histo_pli.dt_event::date,	
				histo_pli.id_pli, 
				histo_pli.flag_traitement,
				typologie as code_type_pli,
				pli,
				typologie,
				id_lot_saisie,
				data_pli.dt_enregistrement::date AS date_saisie_advantage,
				nb_mvt,
				view_pli_avec_cheque_new.id_pli AS flag_chq,
				data_pli.statut_saisie,
				data_pli.societe
				FROM histo_pli
				INNER JOIN view_pli_traitement ON view_pli_traitement.id  = histo_pli.id
				INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
				LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli 
				LEFT JOIN view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = data_pli.id_pli
				LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli  
				WHERE ".$clauseWhere."
			) as traitement
			GROUP BY id_pli,flag_traitement,typologie,nb_mvt,dt_event,id_lot_saisie,date_saisie_advantage,flag_chq,statut_saisie,societe

		) as result GROUP BY dt_event,societe
		ORDER BY dt_event asc
		";
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";exit;
				*/
		return $this->ged->query($sql)
							->result_array();

    }
	
	
	public function get_stat_solde_initial( $date_debut,$id_societe,$rubrique){
        $WHERE = $ClauseWhere = "";
		if($id_societe != ""){
			$ClauseWhere = " AND societe = '".$id_societe."'";
		}
		$sql= "SELECT
					sum(pli_non_traite_reception_j) as pli_non_traite_reception_j,
					sum(pli_non_traite_reception_jmoins) as pli_non_traite_reception_jmoins
				FROM
				(SELECT 
					pli_non_traite_reception_j, 
					pli_non_traite_reception_jmoins
				FROM solde_plis_mvt_journalier
				WHERE 
					date_j = ('".$date_debut."'::date- interval '1 day')::date 
					".$ClauseWhere."
					AND rubrique = ".$rubrique."
			    ) as tab ";
			/*echo "<pre>";
			print_r($sql);
			echo "</pre>";exit;*/
			
		return $this->ged->query($sql)
							->result_array();

    }
	
	
	public function get_traitement_ko($date_debut, $date_fin,$id_societe){
		
		$where_date = $where_date_total = $where_soc="";
		$where_date_adv = "";
		if($date_debut != '' && $date_fin != ''){
				$where_date 	.= " histo_pli.dt_event::date between '".$date_debut."' AND '".$date_fin."' ";
				$where_date_total 	.= " date_courrier::date between '".$date_debut."' AND '".$date_fin."' ";
				$where_date_adv = " between '".$date_debut."' AND '".$date_fin."'";
		  }
		if($id_societe != ''){
				$where_soc   = " AND data_pli.societe = '".$id_societe."' ";
				$where_date_total .= " AND view_pli.societe = '".$id_societe."' ";
				
        }
		
		$sql="SELECT	
					CASE WHEN SUM(pli_traite) is null THEN 0 ELSE SUM(pli_traite) END as traite,
					CASE WHEN SUM(ok) is null THEN 0 ELSE SUM(ok) END as ok,
					CASE WHEN SUM(ko) is null THEN 0 ELSE SUM(ko) END as ko,
					CASE WHEN SUM(ko_scan) is null THEN 0 ELSE SUM(ko_scan) END as ko_scan,
					CASE WHEN SUM(hors_perimetre) is null THEN 0 ELSE SUM(hors_perimetre) END as hors_perimetre,
					CASE WHEN SUM(ko_ks) is null THEN 0 ELSE SUM(ko_ks) END as ko_ks,
					CASE WHEN SUM(ko_ke) is null THEN 0 ELSE SUM(ko_ke) END as ko_ke,
					CASE WHEN SUM(ko_def) is null THEN 0 ELSE SUM(ko_def) END as ko_def,
					CASE WHEN SUM(ko_circulaire) is null THEN 0 ELSE SUM(ko_circulaire) END as ko_circulaire,
					CASE WHEN SUM(saisie_j) is null THEN 0 ELSE SUM(saisie_j) END as ok_advantage,
					CASE WHEN SUM(cloture_ok) is null THEN 0 ELSE SUM(cloture_ok) END as cloture_ok,
					CASE WHEN SUM(mvt_ok) is null THEN 0 ELSE SUM(mvt_ok) END as mvt_ok,
					CASE WHEN SUM(mvt_ko) is null THEN 0 ELSE SUM(mvt_ko) END as mvt_ko,
					CASE WHEN SUM(mvt_ko_scan) is null THEN 0 ELSE SUM(mvt_ko_scan) END as mvt_ko_scan,
					CASE WHEN SUM(mvt_hors_perimetre) is null THEN 0 ELSE SUM(mvt_hors_perimetre) END as mvt_hors_perimetre,
					CASE WHEN SUM(mvt_ko_ks) is null THEN 0 ELSE SUM(mvt_ko_ks) END as mvt_ko_ks,
					CASE WHEN SUM(mvt_ko_ke) is null THEN 0 ELSE SUM(mvt_ko_ke) END as mvt_ko_ke,
					CASE WHEN SUM(mvt_ko_def) is null THEN 0 ELSE SUM(mvt_ko_def) END as mvt_ko_def,
					CASE WHEN SUM(mvt_ko_circulaire) is null THEN 0 ELSE SUM(mvt_ko_circulaire) END as mvt_ko_circulaire,
					CASE WHEN SUM(mvt_cloture_ok) is null THEN 0 ELSE SUM(mvt_cloture_ok) END as mvt_cloture_ok,
					CASE WHEN SUM(mvt_ok_advantage) is null THEN 0 ELSE SUM(mvt_ok_advantage) END as mvt_ok_advantage,
					CASE WHEN SUM(mvt_pli_traite) is null THEN 0 ELSE SUM(mvt_pli_traite) END as mvt_pli_traite,
						(
							
							SELECT count(distinct data_pli.id_pli)||','||sum(nb_mvt) as pli_total 
							FROM histo_pli
							INNER join data_pli on data_pli.id_pli = histo_pli.id_pli
							LEFT JOIN view_nb_mouvement_pli on data_pli.id_pli = view_nb_mouvement_pli.id_pli
							WHERE dt_event::date ".$where_date_adv.$where_soc." 
						) as pli_total_en_traitement
				FROM
				(
					SELECT id_pli, flag_traitement,typologie,
						 CASE WHEN  ((flag_traitement = ANY (ARRAY[9, 14, 15, 18])) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL)  OR flag_traitement = 20 OR flag_traitement = 25  THEN count(distinct id_pli) ELSE 0 END as ok,
						 CASE WHEN (flag_traitement = ANY (ARRAY[16, 21])) /*OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND (statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6])))*/ THEN count(distinct id_pli) ELSE 0 END as ko, 						 
						 CASE WHEN (flag_traitement = 16)   THEN count(distinct id_pli) ELSE 0 END as ko_scan,						 
						 CASE WHEN (flag_traitement = 21) THEN count(id_pli) ELSE 0 END as hors_perimetre,
						 CASE WHEN (flag_traitement = 18 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 4)) THEN count(id_pli) ELSE 0 END as ko_ks,
						 CASE WHEN (flag_traitement = 19 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 5)) THEN count(id_pli) ELSE 0 END as ko_ke,
						 CASE WHEN (flag_traitement = 20 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 6])))  THEN count( id_pli) ELSE 0 END as ko_def,
						 CASE WHEN (flag_traitement = 25 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = 3))  THEN count(distinct id_pli) ELSE 0 END as ko_circulaire,
						 CASE WHEN (flag_traitement = ANY (ARRAY[9, 14, 15]) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL  AND statut_saisie = 1)  THEN count(distinct id_pli) ELSE 0 END as cloture_ok,
						 CASE WHEN ((flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) AND flag_chq IS NULL ) OR (flag_traitement = 26 AND flag_chq IS NOT NULL) OR flag_traitement = 25 THEN count(distinct id_pli) ELSE 0 END as pli_traite,
						CASE WHEN ((flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL)  OR flag_traitement = 25 THEN sum(nb_mvt) ELSE 0 END as mvt_ok,
						CASE WHEN (flag_traitement = ANY (ARRAY[16, 21])) /*OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND (statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6])))*/ THEN sum(nb_mvt) ELSE 0 END as mvt_ko, 						 
						CASE WHEN (flag_traitement = 16)   THEN sum(nb_mvt) ELSE 0 END as mvt_ko_scan,						 
						CASE WHEN (flag_traitement = 21) THEN sum(nb_mvt) ELSE 0 END as mvt_hors_perimetre,
						CASE WHEN (flag_traitement = 18 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 4)) THEN sum(nb_mvt) ELSE 0 END as mvt_ko_ks,
						CASE WHEN (flag_traitement = 19 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 5)) THEN sum(nb_mvt) ELSE 0 END as mvt_ko_ke,
						CASE WHEN (flag_traitement = 20 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 6])))  THEN sum( nb_mvt) ELSE 0 END as mvt_ko_def,
						CASE WHEN (flag_traitement = 25  OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = 3))  THEN sum(nb_mvt) ELSE 0 END as mvt_ko_circulaire,
						CASE WHEN (flag_traitement = ANY (ARRAY[9, 14, 15]) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL  AND statut_saisie = 1)  THEN sum(nb_mvt) ELSE 0 END as mvt_cloture_ok,
						CASE WHEN date_saisie_advantage  ".$where_date_adv." AND id_lot_saisie is not null AND flag_traitement != 26 THEN sum(nb_mvt) ELSE 0 END as mvt_ok_advantage,
						CASE WHEN  ((flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) AND flag_chq IS NULL ) OR (flag_traitement = 26 AND flag_chq IS NOT NULL) OR flag_traitement = 25 THEN sum(nb_mvt) ELSE 0 END as mvt_pli_traite,
						CASE WHEN (date_saisie_advantage ".$where_date_adv.") AND id_lot_saisie is not null AND flag_traitement != 26 THEN count(distinct id_pli) ELSE 0 END as saisie_j
						 
					FROM
					(
					
						SELECT distinct
						histo_pli.dt_event::date,	
						histo_pli.id_pli, 
						histo_pli.flag_traitement,
						f_pli.typologie as code_type_pli,
						pli,
						f_pli.typologie,
						view_pli_avec_cheque_new.id_pli as pli_avec_chq,
						id_lot_saisie,
						data_pli.dt_enregistrement::date as date_saisie_advantage,
						nb_mvt,
						view_pli_avec_cheque_new.id_pli AS flag_chq,
						data_pli.statut_saisie,
						data_pli.societe
						FROM histo_pli
						INNER JOIN f_histo_pli_by_date_by_societe('''".$date_debut."''' , '''".$date_fin."''',".$id_societe.") ON f_histo_pli_by_date_by_societe.id  = histo_pli.id 
						INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
						INNER JOIN data_pli on histo_pli.id_pli = data_pli.id_pli 
						LEFT JOIN view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = data_pli.id_pli
						LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli
						WHERE ".$where_date.$where_soc." 		
								
					) as traitement
					GROUP BY id_pli,pli,flag_traitement,typologie,id_lot_saisie,date_saisie_advantage,statut_saisie,flag_chq
				) as result";
		/*echo "<pre>";
		print_r($sql);
		echo "</pre>";exit;
		*/
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_stat_hebdo($sem_debut, $sem_fin,$id_societe){
		
		$sql = "	SELECT
						semaine AS lib_daty,
						date_debut AS daty,
						SUM(total) total,
						SUM(non_traite) non_traite,
						SUM(encours) encours,
						SUM(cloture) traite,
						SUM(anomalie) anomalie,
						SUM(ko_scan) ko_ko_scan,
						SUM(ko_circulaire) ko_ko_circulaire,
						SUM(ko_src) ko_src,
						SUM(ko_definitif) ko_ko_definitif,
						SUM(ko_inconnu) ko_ko_inconnu,
						SUM(ko_en_attente) ko_ko_en_attente,
						SUM(ko_abandonne) ko_ko_abandonne,
						SUM(ko_abandonne) ferme,
						SUM(ci_editee) ci_ci_editee,
						SUM(ci_envoyee) ci_ci_envoyee,		
						SUM(ci_envoyee) ci_traite,		
						SUM(divise) divise,	
						SUM(ok) ok	
					FROM
					(
						SELECT semaine,date_debut,
							CASE WHEN rubrique = 'Total' THEN SUM(nb_pli) ELSE 0 END AS total,
							CASE WHEN rubrique = 'Non traité' THEN SUM(nb_pli) ELSE 0 END AS non_traite,
							CASE WHEN rubrique = 'En cours' THEN SUM(nb_pli) ELSE 0 END AS encours,
							CASE WHEN rubrique = 'Cloturé' THEN SUM(nb_pli) ELSE 0 END AS cloture,
							CASE WHEN rubrique = 'Anomalie' THEN SUM(nb_pli) ELSE 0 END AS anomalie,
							CASE WHEN rubrique = 'ko scan' THEN SUM(nb_pli) ELSE 0 END AS ko_scan,
							CASE WHEN rubrique = 'ko circualire' THEN SUM(nb_pli) ELSE 0 END AS ko_circulaire,
							CASE WHEN rubrique = 'ko src' THEN SUM(nb_pli) ELSE 0 END AS ko_src,
							CASE WHEN rubrique = 'ko definitif' THEN SUM(nb_pli) ELSE 0 END AS ko_definitif,
							CASE WHEN rubrique = 'ko inconnu' THEN SUM(nb_pli) ELSE 0 END AS ko_inconnu,
							CASE WHEN rubrique = 'ko en attente' THEN SUM(nb_pli) ELSE 0 END AS ko_en_attente,
							CASE WHEN rubrique = 'ko abandonné' THEN SUM(nb_pli) ELSE 0 END AS ko_abandonne,
							CASE WHEN rubrique = 'ci éditée' THEN SUM(nb_pli) ELSE 0 END AS ci_editee,
							CASE WHEN rubrique = 'ci envoyée' THEN SUM(nb_pli) ELSE 0 END AS ci_envoyee,
							CASE WHEN rubrique = 'divise' THEN SUM(nb_pli) ELSE 0 END AS divise	,		
							CASE WHEN rubrique = 'OK' THEN SUM(nb_pli) ELSE 0 END AS ok			
						FROM stat_hebdo
						WHERE titre = 'GLOBAL' 
						AND societe = ".$id_societe." and date_debut  between '".$sem_debut."' and '".$sem_fin."'
						GROUP BY rubrique, semaine,date_debut
					) as global
					GROUP BY semaine, date_debut
					ORDER BY semaine asc
				";
		return $this->ged->query($sql)
							->result_array();
    }
	public function get_stat_mensuel( $date_debut, $date_fin ,$id_soc){
		
       $sql = "SELECT 	concat( /*date_part('month',data_daty.daty)*/
							CASE WHEN date_part('month',data_daty.daty) = 1 THEN 'Janvier' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 2 THEN 'Février' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 3 THEN 'Mars' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 4 THEN 'Avril' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 5 THEN 'Mai' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 6 THEN 'Juin' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 7 THEN 'Juillet' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 8 THEN 'Aout' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 9 THEN 'Septembre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 10 THEN 'Octobre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 11 THEN 'Novembre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 12 THEN 'Décembre' END
							END END END END END END END END END END END
							,' ',date_part('year',data_daty.daty)) as lib_daty,
							data_daty.daty,
                            CASE WHEN data_global.total is null THEN 0 ELSE data_global.total::integer END as total,
                            CASE WHEN data_global.non_traite is null THEN 0 ELSE data_global.non_traite::integer END as non_traite,
                            CASE WHEN encours is null THEN 0 ELSE encours::integer END,
                            CASE WHEN traite is null THEN 0 ELSE traite::integer END,
							CASE WHEN anomalie is null THEN 0 ELSE anomalie::integer END,	
							CASE WHEN ferme is null THEN 0 ELSE ferme::integer END,	
							CASE WHEN divise is null THEN 0 ELSE divise::integer END,	
							CASE WHEN ci_traite is null THEN 0 ELSE ci_traite::integer END,	
							data_global.ok,data_global.ko,data_global.nb_pli_ci,data_global.nb_pli_ks,data_global.nb_pli_kd,data_global.mouvement_ok,data_global.mouvement_ano,data_global.mouvement_ci, data_global.mouvement_ks,data_global.mouvement_kd,							
							nb_pli_2,nb_pli_3,nb_pli_4,nb_pli_5,nb_pli_6,nb_pli_7,nb_pli_8,nb_pli_9,nb_pli_10,
							nb_pli_11,nb_pli_12,nb_pli_13,nb_pli_14,nb_pli_15,nb_pli_16,nb_pli_17,nb_pli_18,nb_pli_19,nb_pli_20,
							nb_pli_21,nb_pli_22,nb_pli_23,nb_pli_24,nb_pli_25,nb_pli_26,nb_pli_27,
							nb_mvt_2,nb_mvt_3,nb_mvt_4,nb_mvt_5,nb_mvt_6,nb_mvt_7,nb_mvt_8,nb_mvt_9,nb_mvt_10,
							nb_mvt_11,nb_mvt_12,nb_mvt_13,nb_mvt_14,nb_mvt_15,nb_mvt_16,nb_mvt_17,nb_mvt_18,nb_mvt_19,nb_mvt_20,
							nb_mvt_21,nb_mvt_22,nb_mvt_23,nb_mvt_24,nb_mvt_25,nb_mvt_26,nb_mvt_27,							
							nb_cltr_pli_2,nb_cltr_pli_3,nb_cltr_pli_4,nb_cltr_pli_5,nb_cltr_pli_6,nb_cltr_pli_7,nb_cltr_pli_8,nb_cltr_pli_9,nb_cltr_pli_10,							nb_cltr_pli_11,nb_cltr_pli_12,nb_cltr_pli_13,nb_cltr_pli_14,nb_cltr_pli_15,nb_cltr_pli_16,nb_cltr_pli_17,nb_cltr_pli_18,nb_cltr_pli_19,nb_cltr_pli_20,
							nb_cltr_pli_21,nb_cltr_pli_22,nb_cltr_pli_23,nb_cltr_pli_24,nb_cltr_pli_25,nb_cltr_pli_26,nb_cltr_pli_27,		nb_cltr_mvt_2,nb_cltr_mvt_3,nb_cltr_mvt_4,nb_cltr_mvt_5,nb_cltr_mvt_6,nb_cltr_mvt_7,nb_cltr_mvt_8,nb_cltr_mvt_9,nb_cltr_mvt_10,						nb_cltr_mvt_11,nb_cltr_mvt_12,nb_cltr_mvt_13,nb_cltr_mvt_14,nb_cltr_mvt_15,nb_cltr_mvt_16,nb_cltr_mvt_17,nb_cltr_mvt_18,nb_cltr_mvt_19,nb_cltr_mvt_20,
							nb_cltr_mvt_21,nb_cltr_mvt_22,nb_cltr_mvt_23,nb_cltr_mvt_24,nb_cltr_mvt_25,nb_cltr_mvt_26,nb_cltr_mvt_27,nb_att_en_cours,nb_controle_en_cours,nb_saisie_en_cours,nb_typage_en_cours,nb_rettt_en_cours,mvt_att_en_cours,mvt_controle_en_cours,mvt_saisie_en_cours,mvt_typage_en_cours,mvt_rettt_en_cours,
							nb_non_traite,nb_en_cours,mvt_non_traite,mvt_en_cours, 
							ko_ko_scan,
							ko_ko_circulaire,
							ko_src,
							ko_ko_definitif,
							ko_ko_inconnu,
							ko_ko_en_attente,
							ko_ko_abandonne,
							ci_ci_editee,
							ci_ci_envoyee,
							ok_ci_ci,
							ko_ko_encours
						FROM
						 (
							SELECT liste_months as daty FROM liste_months ('".$date_debut."','".$date_fin."')
						 ) as data_daty
						LEFT JOIN 
						(	SELECT
								daty,
								SUM(total) total,
								SUM(non_traite) non_traite,
								SUM(encours) encours,
								SUM(traite) traite,
								SUM(anomalie) anomalie,
								SUM(divise) divise,
								SUM(ferme) ferme,
								SUM(ci_traite) ci_traite,
								SUM(ok) ok,
								SUM(ko) ko,
								SUM(nb_pli_ci) nb_pli_ci,
								SUM(nb_pli_ks) nb_pli_ks,
								SUM(nb_pli_kd) nb_pli_kd,
								SUM(mouvement_ok) mouvement_ok,
								SUM(mouvement_ano) mouvement_ano,
								SUM(mouvement_ci) mouvement_ci,
								SUM(mouvement_ks) mouvement_ks,
								SUM(mouvement_kd) mouvement_kd
							FROM
							(
								SELECT daty,
								CASE WHEN rubrique ='Total' THEN nb_pli::integer ELSE 0 END  as total,
								CASE WHEN rubrique ='Non traité' THEN nb_pli::integer ELSE 0 END  as non_traite,
								CASE WHEN rubrique ='Encours' THEN nb_pli::integer ELSE 0 END as encours,
								CASE WHEN rubrique ='Cloturé' THEN nb_pli::integer ELSE 0 END as traite,
								CASE WHEN rubrique ='Anomalie' THEN nb_pli::integer ELSE 0 END as anomalie,
								CASE WHEN rubrique ='Divisé' THEN nb_pli::integer ELSE 0 END as divise,
								CASE WHEN rubrique ='CI traité' THEN nb_pli::integer ELSE 0 END as ci_traite,
								CASE WHEN rubrique ='Fermé' THEN nb_pli::integer ELSE 0 END as ferme,								
								CASE WHEN rubrique ='Traité OK' THEN nb_pli::integer ELSE 0 END as ok,				
								CASE WHEN rubrique ='KO' THEN nb_pli::integer ELSE 0 END as ko,				
								CASE WHEN rubrique ='Traité CI' THEN nb_pli::integer ELSE 0 END as nb_pli_ci,				
								CASE WHEN rubrique ='Traité KS' THEN nb_pli::integer ELSE 0 END as nb_pli_ks,				
								CASE WHEN rubrique ='Traité KD' THEN nb_pli::integer ELSE 0 END as nb_pli_kd,								
								CASE WHEN rubrique ='Traité OK' THEN nb_mvt::integer ELSE 0 END as mouvement_ok,				
								CASE WHEN rubrique ='KO' THEN nb_mvt::integer ELSE 0 END as mouvement_ano,				
								CASE WHEN rubrique ='Traité CI' THEN nb_mvt::integer ELSE 0 END as mouvement_ci,				
								CASE WHEN rubrique ='Traité KS' THEN nb_mvt::integer ELSE 0 END as mouvement_ks,				
								CASE WHEN rubrique ='Traité KD' THEN nb_mvt::integer ELSE 0 END as mouvement_kd					
								FROM stat_mensuel
								WHERE titre = 'GLOBAL' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = ".$id_soc."
								GROUP BY rubrique, daty,nb_pli,nb_mvt
							) as global
							GROUP BY daty
						) as data_global on data_daty.daty = data_global.daty
						LEFT JOIN (
							SELECT 
								daty,
								nb_pli_2,nb_pli_3,nb_pli_4,nb_pli_5,nb_pli_6,nb_pli_7,nb_pli_8,nb_pli_9,nb_pli_10,
								nb_pli_11,nb_pli_12,nb_pli_13,nb_pli_14,nb_pli_15,nb_pli_16,nb_pli_17,nb_pli_18,nb_pli_19,nb_pli_20,
								nb_pli_21,nb_pli_22,nb_pli_23,nb_pli_24,nb_pli_25,nb_pli_26,nb_pli_27,
								nb_mvt_2,nb_mvt_3,nb_mvt_4,nb_mvt_5,nb_mvt_6,nb_mvt_7,nb_mvt_8,nb_mvt_9,nb_mvt_10,
								nb_mvt_11,nb_mvt_12,nb_mvt_13,nb_mvt_14,nb_mvt_15,nb_mvt_16,nb_mvt_17,nb_mvt_18,nb_mvt_19,nb_mvt_20,
								nb_mvt_21,nb_mvt_22,nb_mvt_23,nb_mvt_24,nb_mvt_25,nb_mvt_26,nb_mvt_27
							FROM 			
							(
								SELECT '".$date_debut."' ::date as daty,
								split_part(data_2, ',', 3) as nb_pli_2,split_part(data_2, ',', 4) as nb_mvt_2,
								split_part(data_3, ',', 3) as nb_pli_3,split_part(data_3, ',', 4) as nb_mvt_3,
								split_part(data_4, ',', 3) as nb_pli_4,split_part(data_4, ',', 4) as nb_mvt_4,
								split_part(data_5, ',', 3) as nb_pli_5,split_part(data_5, ',', 4) as nb_mvt_5,
								split_part(data_6, ',', 3) as nb_pli_6,split_part(data_6, ',', 4) as nb_mvt_6,
								split_part(data_7, ',', 3) as nb_pli_7,split_part(data_7, ',', 4) as nb_mvt_7,
								split_part(data_8, ',', 3) as nb_pli_8,split_part(data_8, ',', 4) as nb_mvt_8,
								split_part(data_9, ',', 3) as nb_pli_9,split_part(data_9, ',', 4) as nb_mvt_9,
								split_part(data_10, ',', 3) as nb_pli_10,split_part(data_10, ',', 4) as nb_mvt_10,
								split_part(data_11, ',', 3) as nb_pli_11,split_part(data_11, ',', 4) as nb_mvt_11,
								split_part(data_12, ',', 3) as nb_pli_12,split_part(data_12, ',', 4) as nb_mvt_12,
								split_part(data_13, ',', 3) as nb_pli_13,split_part(data_13, ',', 4) as nb_mvt_13,
								split_part(data_14, ',', 3) as nb_pli_14,split_part(data_14, ',', 4) as nb_mvt_14,
								split_part(data_15, ',', 3) as nb_pli_15,split_part(data_15, ',', 4) as nb_mvt_15,
								split_part(data_16, ',', 3) as nb_pli_16,split_part(data_16, ',', 4) as nb_mvt_16,
								split_part(data_17, ',', 3) as nb_pli_17,split_part(data_17, ',', 4) as nb_mvt_17,
								split_part(data_18, ',', 3) as nb_pli_18,split_part(data_18, ',', 4) as nb_mvt_18,
								split_part(data_19, ',', 3) as nb_pli_19,split_part(data_19, ',', 4) as nb_mvt_19,
								split_part(data_20, ',', 3) as nb_pli_20,split_part(data_20, ',', 4) as nb_mvt_20,
								split_part(data_21, ',', 3) as nb_pli_21,split_part(data_21, ',', 4) as nb_mvt_21,
								split_part(data_22, ',', 3) as nb_pli_22,split_part(data_22, ',', 4) as nb_mvt_22,
								split_part(data_23, ',', 3) as nb_pli_23,split_part(data_23, ',', 4) as nb_mvt_23,
								split_part(data_24, ',', 3) as nb_pli_24,split_part(data_24, ',', 4) as nb_mvt_24,
								split_part(data_25, ',', 3) as nb_pli_25,split_part(data_25, ',', 4) as nb_mvt_25,
								split_part(data_26, ',', 3) as nb_pli_26,split_part(data_26, ',', 4) as nb_mvt_26,
								split_part(data_27, ',', 3) as nb_pli_27,split_part(data_27, ',', 4) as nb_mvt_27
								FROM f_stat_mensuel_par_bloc_soc_typo('".$id_soc."','Plis par typologie', '".$date_debut."' , '".$date_fin."')
								
							) as data_plis_par_typo
							
						) as data_plis_typo on data_daty.daty = data_plis_typo.daty
						LEFT JOIN (
							SELECT 
								daty,
								nb_cltr_pli_2,nb_cltr_pli_3,nb_cltr_pli_4,nb_cltr_pli_5,nb_cltr_pli_6,nb_cltr_pli_7,nb_cltr_pli_8,nb_cltr_pli_9,nb_cltr_pli_10,								
								nb_cltr_pli_11,nb_cltr_pli_12,nb_cltr_pli_13,nb_cltr_pli_14,nb_cltr_pli_15,nb_cltr_pli_16,nb_cltr_pli_17,nb_cltr_pli_18,nb_cltr_pli_19,nb_cltr_pli_20,								
								nb_cltr_pli_21,nb_cltr_pli_22,nb_cltr_pli_23,nb_cltr_pli_24,nb_cltr_pli_25,nb_cltr_pli_26,nb_cltr_pli_27,	nb_cltr_mvt_2,nb_cltr_mvt_3,nb_cltr_mvt_4,nb_cltr_mvt_5,nb_cltr_mvt_6,nb_cltr_mvt_7,nb_cltr_mvt_8,nb_cltr_mvt_9,nb_cltr_mvt_10,nb_cltr_mvt_11,nb_cltr_mvt_12,nb_cltr_mvt_13,nb_cltr_mvt_14,nb_cltr_mvt_15,nb_cltr_mvt_16,nb_cltr_mvt_17,nb_cltr_mvt_18,nb_cltr_mvt_19,nb_cltr_mvt_20,nb_cltr_mvt_21,nb_cltr_mvt_22,nb_cltr_mvt_23,nb_cltr_mvt_24,nb_cltr_mvt_25,nb_cltr_mvt_26,nb_cltr_mvt_27
							FROM 			
							(
								SELECT '".$date_debut."' ::date as daty,
								split_part(data_2, ',', 3) as nb_cltr_pli_2,split_part(data_2, ',', 4) as nb_cltr_mvt_2,
								split_part(data_3, ',', 3) as nb_cltr_pli_3,split_part(data_3, ',', 4) as nb_cltr_mvt_3,
								split_part(data_4, ',', 3) as nb_cltr_pli_4,split_part(data_4, ',', 4) as nb_cltr_mvt_4,
								split_part(data_5, ',', 3) as nb_cltr_pli_5,split_part(data_5, ',', 4) as nb_cltr_mvt_5,
								split_part(data_6, ',', 3) as nb_cltr_pli_6,split_part(data_6, ',', 4) as nb_cltr_mvt_6,
								split_part(data_7, ',', 3) as nb_cltr_pli_7,split_part(data_7, ',', 4) as nb_cltr_mvt_7,
								split_part(data_8, ',', 3) as nb_cltr_pli_8,split_part(data_8, ',', 4) as nb_cltr_mvt_8,
								split_part(data_9, ',', 3) as nb_cltr_pli_9,split_part(data_9, ',', 4) as nb_cltr_mvt_9,
								split_part(data_10, ',', 3) as nb_cltr_pli_10,split_part(data_10, ',', 4) as nb_cltr_mvt_10,
								split_part(data_11, ',', 3) as nb_cltr_pli_11,split_part(data_11, ',', 4) as nb_cltr_mvt_11,
								split_part(data_12, ',', 3) as nb_cltr_pli_12,split_part(data_12, ',', 4) as nb_cltr_mvt_12,
								split_part(data_13, ',', 3) as nb_cltr_pli_13,split_part(data_13, ',', 4) as nb_cltr_mvt_13,
								split_part(data_14, ',', 3) as nb_cltr_pli_14,split_part(data_14, ',', 4) as nb_cltr_mvt_14,
								split_part(data_15, ',', 3) as nb_cltr_pli_15,split_part(data_15, ',', 4) as nb_cltr_mvt_15,
								split_part(data_16, ',', 3) as nb_cltr_pli_16,split_part(data_16, ',', 4) as nb_cltr_mvt_16,
								split_part(data_17, ',', 3) as nb_cltr_pli_17,split_part(data_17, ',', 4) as nb_cltr_mvt_17,
								split_part(data_18, ',', 3) as nb_cltr_pli_18,split_part(data_18, ',', 4) as nb_cltr_mvt_18,
								split_part(data_19, ',', 3) as nb_cltr_pli_19,split_part(data_19, ',', 4) as nb_cltr_mvt_19,
								split_part(data_20, ',', 3) as nb_cltr_pli_20,split_part(data_20, ',', 4) as nb_cltr_mvt_20,
								split_part(data_21, ',', 3) as nb_cltr_pli_21,split_part(data_21, ',', 4) as nb_cltr_mvt_21,
								split_part(data_22, ',', 3) as nb_cltr_pli_22,split_part(data_22, ',', 4) as nb_cltr_mvt_22,
								split_part(data_23, ',', 3) as nb_cltr_pli_23,split_part(data_23, ',', 4) as nb_cltr_mvt_23,
								split_part(data_24, ',', 3) as nb_cltr_pli_24,split_part(data_24, ',', 4) as nb_cltr_mvt_24,
								split_part(data_25, ',', 3) as nb_cltr_pli_25,split_part(data_25, ',', 4) as nb_cltr_mvt_25,
								split_part(data_26, ',', 3) as nb_cltr_pli_26,split_part(data_26, ',', 4) as nb_cltr_mvt_26,
								split_part(data_27, ',', 3) as nb_cltr_pli_27,split_part(data_27, ',', 4) as nb_cltr_mvt_27
								FROM f_stat_mensuel_par_bloc_soc_typo('".$id_soc."','Plis cloturés', '".$date_debut."' , '".$date_fin."')
							) as data_plis_clos
							
						) as data_plis_clotures on data_daty.daty = data_plis_clotures.daty
						LEFT JOIN (
							SELECT 
								daty,
								sum(nb_att_en_cours) as nb_att_en_cours,
								sum(nb_controle_en_cours) as nb_controle_en_cours,
								sum(nb_saisie_en_cours) as nb_saisie_en_cours,
								sum(nb_typage_en_cours) as nb_typage_en_cours,
								sum(nb_rettt_en_cours) as nb_rettt_en_cours,
								sum(mvt_att_en_cours) as mvt_att_en_cours,
								sum(mvt_controle_en_cours) as mvt_controle_en_cours,
								sum(mvt_saisie_en_cours) as mvt_saisie_en_cours,
								sum(mvt_typage_en_cours) as mvt_typage_en_cours,
								sum(mvt_rettt_en_cours) as mvt_rettt_en_cours
							FROM
							(SELECT daty,
								CASE WHEN rubrique ='En attente consignes' THEN nb_pli::integer ELSE 0 END  as nb_att_en_cours,
								CASE WHEN rubrique ='En cours de contrôle' THEN nb_pli::integer ELSE 0 END as nb_controle_en_cours,
								CASE WHEN rubrique ='En cours de saisie' THEN nb_pli::integer ELSE 0 END as nb_saisie_en_cours,
								CASE WHEN rubrique ='En cours de typage' THEN nb_pli::integer ELSE 0 END as nb_typage_en_cours,
								CASE WHEN rubrique ='Retraitement' THEN nb_pli::integer ELSE 0 END as nb_rettt_en_cours,
								CASE WHEN rubrique ='En attente consignes' THEN nb_mvt::integer ELSE 0 END  as mvt_att_en_cours,
								CASE WHEN rubrique ='En cours de contrôle' THEN nb_mvt::integer ELSE 0 END as mvt_controle_en_cours,
								CASE WHEN rubrique ='En cours de saisie' THEN nb_mvt::integer ELSE 0 END as mvt_saisie_en_cours,
								CASE WHEN rubrique ='En cours de typage' THEN nb_mvt::integer ELSE 0 END as mvt_typage_en_cours,
								CASE WHEN rubrique ='Retraitement' THEN nb_mvt::integer ELSE 0 END as mvt_rettt_en_cours
								
								FROM stat_mensuel
								WHERE titre = 'Plis en cours' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = '".$id_soc."'
								GROUP BY rubrique, daty,nb_pli,nb_mvt
							)as data_plis_encours
							GROUP BY daty
						) as data_pli_en_cours on data_daty.daty = data_pli_en_cours.daty
						LEFT JOIN (
							SELECT 
								daty,sum(nb_non_traite) nb_non_traite,sum(nb_en_cours) nb_en_cours,
								sum(mvt_non_traite) mvt_non_traite,sum(mvt_en_cours) mvt_en_cours
							FROM 			
							(
								SELECT daty,
								CASE WHEN rubrique ='Non traité' THEN nb_pli::integer ELSE 0 END  as nb_non_traite,
								CASE WHEN rubrique ='En cours' THEN nb_pli::integer ELSE 0 END as nb_en_cours,
								CASE WHEN rubrique ='Non traité' THEN nb_mvt::integer ELSE 0 END  as mvt_non_traite,
								CASE WHEN rubrique ='En cours' THEN nb_mvt::integer ELSE 0 END as mvt_en_cours
								
								FROM stat_mensuel
								WHERE titre = 'Restant à traiter' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = '".$id_soc."'
								GROUP BY rubrique, daty,nb_pli,nb_mvt
							) as data_restant
							GROUP BY daty
						) as data_restant on data_daty.daty = data_restant.daty
						LEFT JOIN 
						(	SELECT
								daty,
								SUM(ko_scan) ko_ko_scan,
								SUM(ko_circulaire) ko_ko_circulaire,
								SUM(ko_src) ko_src,
								SUM(ko_definitif) ko_ko_definitif,
								SUM(ko_inconnu) ko_ko_inconnu,
								SUM(ko_en_attente) ko_ko_en_attente,
								SUM(ko_abandonne) ko_ko_abandonne,
								SUM(ci_editee) ci_ci_editee,
								SUM(ci_envoyee) ci_ci_envoyee,
								SUM(ok_ci) ok_ci_ci,
								SUM(ko_encours) ko_ko_encours
							FROM
							(
								SELECT daty,
								CASE WHEN rubrique ='KO SCAN' THEN nb_pli::integer ELSE 0 END  as ko_scan,
								CASE WHEN rubrique ='KO CI' THEN nb_pli::integer ELSE 0 END as ko_circulaire,
								CASE WHEN rubrique ='KO KS' THEN nb_pli::integer ELSE 0 END as ko_src,
								CASE WHEN rubrique ='KO KD' THEN nb_pli::integer ELSE 0 END as ko_definitif,
								CASE WHEN rubrique ='KO INCONNU' THEN nb_pli::integer ELSE 0 END as ko_inconnu,
								CASE WHEN rubrique ='KO EN ATTENTE' THEN nb_pli::integer ELSE 0 END as ko_en_attente,
								CASE WHEN rubrique ='KO ABANDONNE' THEN nb_pli::integer ELSE 0 END as ko_abandonne,								
								CASE WHEN rubrique ='CI EDITEE' THEN nb_pli::integer ELSE 0 END as ci_editee,								
								CASE WHEN rubrique ='CI ENVOYEE' THEN nb_pli::integer ELSE 0 END as ci_envoyee,								
								CASE WHEN rubrique ='OK CI' THEN nb_pli::integer ELSE 0 END as ok_ci,								
								CASE WHEN rubrique ='KO EN COURS BAYARD' THEN nb_pli::integer ELSE 0 END as ko_encours								
												
								FROM stat_mensuel
								WHERE titre = 'PLI KO' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = ".$id_soc."
								GROUP BY rubrique, daty,nb_pli
							) as global
							GROUP BY daty
						) as data_ko on data_daty.daty = data_ko.daty
						ORDER BY data_daty.daty asc
				"	;		
      
		return $this->ged->query($sql)
							->result_array();

    }
	public function get_stat_journalier($jr_debut, $jr_fin,$id_societe){
		
		$sql = "	SELECT
				date_courrier AS lib_daty,
				date_courrier AS daty,
				SUM(total) total,
				SUM(non_traite) non_traite,
				SUM(encours) encours,
				SUM(cloture) traite,
				SUM(anomalie) anomalie,
				SUM(ko_scan) ko_ko_scan,
				SUM(ko_circulaire) ko_ko_circulaire,
				SUM(ko_src) ko_src,
				SUM(ko_definitif) ko_ko_definitif,
				SUM(ko_inconnu) ko_ko_inconnu,
				SUM(ko_en_attente) ko_ko_en_attente,
				SUM(ko_abandonne) ko_ko_abandonne,
				SUM(ko_abandonne) ferme,
				SUM(ci_editee) ci_ci_editee,
				SUM(ci_envoyee) ci_ci_envoyee,		
				SUM(ci_envoyee) ci_traite,		
				SUM(divise) divise,	
				SUM(ok) ok	
			FROM
			(
				SELECT date_courrier::date date_courrier,
					count(distinct f_pli.id_pli) AS total,
					CASE WHEN  data_pli.flag_traitement = 0 or data_pli.flag_traitement is null THEN count(distinct f_pli.id_pli)  ELSE 0 END  AS non_traite,
					CASE WHEN (data_pli.flag_traitement = ANY (ARRAY[1, 3, 4, 7, 8, 11, 13])) OR data_pli.flag_traitement = 2 AND data_pli.statut_saisie = 1 OR data_pli.flag_traitement = 5 AND data_pli.statut_saisie = 1 OR data_pli.flag_traitement = 6 AND data_pli.statut_saisie = 1 THEN count(distinct f_pli.id_pli) ELSE 0 END AS encours,
					CASE WHEN (data_pli.flag_traitement  in(9,14) and data_pli.statut_saisie = 1) or (data_pli.statut_saisie in (8,10,24)) THEN count(distinct f_pli.id_pli) ELSE 0 END AS cloture,
					CASE WHEN (data_pli.statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7]) ) THEN count(distinct f_pli.id_pli) ELSE 0 END AS anomalie,
					CASE WHEN data_pli.statut_saisie = 2 THEN count(distinct f_pli.id_pli) ELSE 0 END AS ko_scan,
					CASE WHEN data_pli.statut_saisie = 3 THEN count(distinct f_pli.id_pli) ELSE 0 END AS ko_definitif,
					CASE WHEN data_pli.statut_saisie = 4 THEN count(distinct f_pli.id_pli) ELSE 0 END AS ko_inconnu,
					CASE WHEN data_pli.statut_saisie = 5 THEN count(distinct f_pli.id_pli) ELSE 0 END AS ko_src,
					CASE WHEN data_pli.statut_saisie = 6 THEN count(distinct f_pli.id_pli) ELSE 0 END AS ko_en_attente,
					CASE WHEN data_pli.statut_saisie = 7 THEN count(distinct f_pli.id_pli) ELSE 0 END AS ko_circulaire,
					CASE WHEN data_pli.statut_saisie = 8 THEN count(distinct f_pli.id_pli) ELSE 0 END AS ko_abandonne,
					CASE WHEN data_pli.statut_saisie = 9 THEN count(distinct f_pli.id_pli) ELSE 0 END AS ci_editee,
					CASE WHEN data_pli.statut_saisie = 10 THEN count(distinct f_pli.id_pli) ELSE 0 END AS ci_envoyee,
					CASE WHEN data_pli.statut_saisie = 24 THEN count(distinct f_pli.id_pli) ELSE 0 END AS divise	,		
					CASE WHEN data_pli.flag_traitement  in (9,14) and data_pli.statut_saisie = 1  THEN count(distinct f_pli.id_pli) ELSE 0 END AS ok			
				FROM data_pli
				inner join f_pli on f_pli.id_pli = data_pli.id_pli 
				inner join f_lot_numerisation on f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation 
				WHERE data_pli.societe = ".$id_societe." and date_courrier::date  between '".$jr_debut."' and '".$jr_fin."'
				GROUP BY data_pli.statut_saisie, date_courrier,data_pli.flag_traitement
			) as global
			GROUP BY  date_courrier::date
			ORDER BY date_courrier asc
				";
		return $this->ged->query($sql)
							->result_array();
    }

    public function get_stat_journalier_flux($jr_debut, $jr_fin,$id_societe,$id_source){
        $sql = "SELECT
				date_reception AS lib_daty,
				date_reception AS daty,
				SUM(total) total,
				SUM(non_traite) non_traite,
				SUM(encours) encours,
				SUM(cloture) traite,
				SUM(anomalie) anomalie,
				SUM(ko_mail) nb_ko_mail,
				SUM(ko_pj) nb_ko_pj,
				SUM(ko_fichier) nb_ko_fichier,
				SUM(ko_src) nb_ko_src,
				SUM(ko_definitif) nb_ko_definitif,
				SUM(ko_inconnu) nb_ko_inconnu,
				SUM(ko_attente) nb_ko_attente,
				SUM(ko_rejete) nb_ko_rejete,
				SUM(ko_rejete) nb_flux_rejete,
				SUM(ko_hp) nb_ko_hp,	
				SUM(ko_hp) nb_flux_hp,	
				SUM(ok) nb_flux_ok,
				SUM(sans_traitement) nb_flux_sans_traitement	
			FROM
			(
				SELECT date_reception::date date_reception,
					count(distinct flux.id_flux) AS total,
					CASE WHEN  flux.statut_pli = 0 or flux.statut_pli is null THEN count(distinct flux.id_flux)  ELSE 0 END  AS non_traite,
					CASE WHEN (flux.statut_pli = ANY (ARRAY[1,2,3,4,5,6,8,9,10])) AND flux.etat_pli_id = 1 THEN count(distinct flux.id_flux) ELSE 0 END AS encours,
					CASE WHEN (flux.statut_pli = 7 and flux.etat_pli_id = 1) or (flux.etat_pli_id = 100) THEN count(distinct flux.id_flux) ELSE 0 END AS cloture,
					CASE WHEN (flux.etat_pli_id = ANY (ARRAY[11,12,14,19,21,22,23,15,13]) ) THEN count(distinct flux.id_flux) ELSE 0 END AS anomalie,
					CASE WHEN flux.etat_pli_id = 100 THEN count(distinct flux.id_flux) ELSE 0 END AS sans_traitement,
					CASE WHEN flux.etat_pli_id = 11 THEN count(distinct flux.id_flux) ELSE 0 END AS ko_mail,
					CASE WHEN flux.etat_pli_id = 12 THEN count(distinct flux.id_flux) ELSE 0 END AS ko_pj,
					CASE WHEN flux.etat_pli_id = 14 THEN count(distinct flux.id_flux) ELSE 0 END AS ko_fichier,
					CASE WHEN flux.etat_pli_id = 19 THEN count(distinct flux.id_flux) ELSE 0 END AS ko_src,
					CASE WHEN flux.etat_pli_id = 21 THEN count(distinct flux.id_flux) ELSE 0 END AS ko_definitif,
					CASE WHEN flux.etat_pli_id = 22 THEN count(distinct flux.id_flux) ELSE 0 END AS ko_inconnu,
					CASE WHEN flux.etat_pli_id = 23 THEN count(distinct flux.id_flux) ELSE 0 END AS ko_attente,
					CASE WHEN flux.etat_pli_id = 15 THEN count(distinct flux.id_flux) ELSE 0 END AS ko_rejete,
					CASE WHEN flux.etat_pli_id = 13 THEN count(distinct flux.id_flux) ELSE 0 END AS ko_hp,		
					CASE WHEN flux.statut_pli in (7) and flux.etat_pli_id = 1  THEN count(distinct flux.id_flux) ELSE 0 END AS ok			
				FROM flux
				WHERE flux.societe = ".$id_societe." and flux.id_source = ".$id_source." and date_reception::date  between '".$jr_debut."' and '".$jr_fin."'
				GROUP BY flux.etat_pli_id, date_reception,flux.statut_pli
			) as global
			GROUP BY  date_reception::date
			ORDER BY date_reception asc";
        
        return $this->ged_flux->query($sql)
                                ->result_array();
    }
	
	public function get_ko_mensuel( $date_debut, $date_fin ,$id_soc,$granu){
		/* tantely*/
		if($granu == 'm'){
        $sql = "SELECT 	concat( /*date_part('month',data_daty.daty)*/
							CASE WHEN date_part('month',data_daty.daty) = 1 THEN 'Janvier' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 2 THEN 'Février' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 3 THEN 'Mars' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 4 THEN 'Avril' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 5 THEN 'Mai' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 6 THEN 'Juin' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 7 THEN 'Juillet' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 8 THEN 'Aout' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 9 THEN 'Septembre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 10 THEN 'Octobre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 11 THEN 'Novembre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 12 THEN 'Décembre' END
							END END END END END END END END END END END
							,' ',date_part('year',data_daty.daty)) as lib_daty,
							data_daty.daty,
                            CASE WHEN ko_scan is null THEN 0 ELSE ko_scan::integer END,
                            CASE WHEN ko_circulaire is null THEN 0 ELSE ko_circulaire::integer END,
							CASE WHEN ko_src is null THEN 0 ELSE ko_src::integer END,	
							CASE WHEN ko_definitif is null THEN 0 ELSE ko_definitif::integer END,	
							CASE WHEN ko_inconnu is null THEN 0 ELSE ko_inconnu::integer END,	
							CASE WHEN ko_en_attente is null THEN 0 ELSE ko_en_attente::integer END,
							CASE WHEN ko_abandonne is null THEN 0 ELSE ko_abandonne::integer END,
							CASE WHEN ci_editee is null THEN 0 ELSE ci_editee::integer END,
							CASE WHEN ci_envoyee is null THEN 0 ELSE ci_envoyee::integer END,
							CASE WHEN ok_ci is null THEN 0 ELSE ok_ci::integer END,
							CASE WHEN ko_encours is null THEN 0 ELSE ko_encours::integer END
						FROM
						 (
							SELECT liste_months as daty FROM liste_months ('".$date_debut."','".$date_fin."')
						 ) as data_daty
						LEFT JOIN 
						(	SELECT
								daty,
								SUM(ko_scan) ko_scan,
								SUM(ko_circulaire) ko_circulaire,
								SUM(ko_src) ko_src,
								SUM(ko_definitif) ko_definitif,
								SUM(ko_inconnu) ko_inconnu,
								SUM(ko_en_attente) ko_en_attente,
								SUM(ko_abandonne) ko_abandonne,
								SUM(ci_editee) ci_editee,
								SUM(ci_envoyee) ci_envoyee,
								SUM(ok_ci) ok_ci,
								SUM(ko_encours) ko_encours
							FROM
							(
								SELECT daty,
								CASE WHEN rubrique ='KO SCAN' THEN nb_pli::integer ELSE 0 END  as ko_scan,
								CASE WHEN rubrique ='KO CI' THEN nb_pli::integer ELSE 0 END as ko_circulaire,
								CASE WHEN rubrique ='KO KS' THEN nb_pli::integer ELSE 0 END as ko_src,
								CASE WHEN rubrique ='KO KD' THEN nb_pli::integer ELSE 0 END as ko_definitif,
								CASE WHEN rubrique ='KO INCONNU' THEN nb_pli::integer ELSE 0 END as ko_inconnu,
								CASE WHEN rubrique ='KO EN ATTENTE' THEN nb_pli::integer ELSE 0 END as ko_en_attente,
								CASE WHEN rubrique ='KO ABANDONNE' THEN nb_pli::integer ELSE 0 END as ko_abandonne,								
								CASE WHEN rubrique ='CI EDITEE' THEN nb_pli::integer ELSE 0 END as ci_editee,								
								CASE WHEN rubrique ='CI ENVOYEE' THEN nb_pli::integer ELSE 0 END as ci_envoyee,								
								CASE WHEN rubrique ='OK CI' THEN nb_pli::integer ELSE 0 END as ok_ci,								
								CASE WHEN rubrique ='KO EN COURS BAYARD' THEN nb_pli::integer ELSE 0 END as ko_encours								
												
								FROM stat_mensuel
								WHERE titre = 'PLI KO' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = ".$id_soc."
								GROUP BY rubrique, daty,nb_pli
							) as global
							GROUP BY daty
						) as data_global on data_daty.daty = data_global.daty
						ORDER BY data_daty.daty asc"	;
       
		}
		if($granu == 's'){
		
			$sql = " SELECT semaine as lib_daty,	
				coalesce (ko_scan,0) ko_scan,
				coalesce (ko_circulaire,0) ko_circulaire,
				coalesce (ko_src,0) ko_src,
				coalesce (ko_definitif,0) ko_definitif,
				coalesce (ko_inconnu,0) ko_inconnu,
				coalesce (ko_en_attente,0) ko_en_attente,
				coalesce (ko_abandonne,0)ko_abandonne ,
				coalesce (ci_editee,0) ci_editee,
				coalesce (ci_envoyee,0) ci_envoyee 
				FROM
				 (
					SELECT daty,'S'||week as semaine  FROM liste_weeks ('".$date_debut."','".$date_fin."')
				 ) as data_daty
				LEFT JOIN 
				(	SELECT
						date_debut,
						SUM(ko_scan) ko_scan,
						SUM(ko_circulaire) ko_circulaire,
						SUM(ko_src) ko_src,
						SUM(ko_definitif) ko_definitif,
						SUM(ko_inconnu) ko_inconnu,
						SUM(ko_en_attente) ko_en_attente,
						SUM(ko_abandonne) ko_abandonne,
						SUM(ci_editee) ci_editee,
						SUM(ci_envoyee) ci_envoyee,
						SUM(ok_ci) ok_ci,
						SUM(ko_encours) ko_encours
					FROM
					(
						SELECT date_debut,
						CASE WHEN rubrique ='ko scan' THEN nb_pli::integer ELSE 0 END  as ko_scan,
						CASE WHEN rubrique ='ko circualire' THEN nb_pli::integer ELSE 0 END as ko_circulaire,
						CASE WHEN rubrique ='ko src' THEN nb_pli::integer ELSE 0 END as ko_src,
						CASE WHEN rubrique ='ko definitif' THEN nb_pli::integer ELSE 0 END as ko_definitif,
						CASE WHEN rubrique ='ko inconnu' THEN nb_pli::integer ELSE 0 END as ko_inconnu,
						CASE WHEN rubrique ='ko en attente' THEN nb_pli::integer ELSE 0 END as ko_en_attente,
						CASE WHEN rubrique ='ko abandonné' THEN nb_pli::integer ELSE 0 END as ko_abandonne,								
						CASE WHEN rubrique ='ci éditée' THEN nb_pli::integer ELSE 0 END as ci_editee,								
						CASE WHEN rubrique ='ci envoyée' THEN nb_pli::integer ELSE 0 END as ci_envoyee,								
						CASE WHEN rubrique ='ok ci' THEN nb_pli::integer ELSE 0 END as ok_ci,								
						CASE WHEN rubrique ='ko en cours' THEN nb_pli::integer ELSE 0 END as ko_encours							
										
						FROM stat_hebdo
						WHERE titre = 'GLOBAL' 
						AND date_debut between '".$date_debut."' and '".$date_fin."'
						AND societe = ".$id_soc."
						GROUP BY rubrique, date_debut,nb_pli
					) as global
					GROUP BY date_debut
				) as data_global on data_daty.daty = data_global.date_debut
				group by semaine,ko_scan,ko_circulaire, ko_src, ko_definitif,ko_inconnu,ko_en_attente,ko_abandonne,ci_editee,ci_envoyee
				ORDER BY semaine asc";			
		}
		if($granu == 'j'){
			$sql ="
				SELECT 	daty as lib_daty,	
					coalesce (ko_scan,0) ko_scan,
					coalesce (ko_circulaire,0) ko_circulaire,
					coalesce (ko_src,0) ko_src,
					coalesce (ko_definitif,0) ko_definitif,
					coalesce (ko_inconnu,0) ko_inconnu,
					coalesce (ko_en_attente,0) ko_en_attente,
					coalesce (ko_abandonne,0)ko_abandonne ,
					coalesce (ci_editee,0) ci_editee,
					coalesce (ci_envoyee,0) ci_envoyee 
				FROM
				 (
					SELECT liste_dates as daty FROM liste_dates ('".$date_debut."' , '".$date_fin."')
				 ) as data_daty
				LEFT JOIN 
				(	SELECT
						date_debut,
						SUM(ko_scan) ko_scan,
						SUM(ko_circulaire) ko_circulaire,
						SUM(ko_src) ko_src,
						SUM(ko_definitif) ko_definitif,
						SUM(ko_inconnu) ko_inconnu,
						SUM(ko_en_attente) ko_en_attente,
						SUM(ko_abandonne) ko_abandonne,
						SUM(ci_editee) ci_editee,
						SUM(ci_envoyee) ci_envoyee
					FROM
					(
						SELECT date_courrier as date_debut,
						CASE WHEN pli_ko_scan = 1  THEN 1 ELSE 0 END as ko_scan,
						CASE WHEN pli_ko_circulaire = 1  THEN 1 ELSE 0 END as ko_circulaire,
						CASE WHEN pli_ko_src = 1  THEN 1 ELSE 0 END as ko_src,
						CASE WHEN pli_ko_definitif = 1  THEN 1 ELSE 0 END as ko_definitif,
						CASE WHEN pli_ko_inconnu = 1 THEN 1 ELSE 0 END as ko_inconnu,
						CASE WHEN pli_en_attente = 1 THEN 1 ELSE 0 END as ko_en_attente,
						CASE WHEN pli_ferme = 1 THEN 1 ELSE 0 END as ko_abandonne,								
						CASE WHEN pli_ko_ci_editee = 1 THEN 1 ELSE 0 END as ci_editee,								
						CASE WHEN pli_ci_envoyee = 1 THEN 1 ELSE 0 END as ci_envoyee								
										
						FROM  pli_stat
						WHERE  date_courrier::date between '".$date_debut."' and '".$date_fin."'
						AND societe = ".$id_soc."
						
					) as global
					GROUP BY date_debut
				) as data_global on data_daty.daty = data_global.date_debut
				group by daty,ko_scan,ko_circulaire, ko_src, ko_definitif,ko_inconnu,ko_en_attente,ko_abandonne,ci_editee,ci_envoyee
				ORDER BY daty asc
			";
		}
		 /*echo "<pre>";
		print_r($sql);
        echo "</pre>";exit;
		*/
        return $this->ged->query($sql)
            ->result_array();
	}

    public function get_ko_mensuel_flux( $date_debut, $date_fin ,$id_soc,$source,$granu){
	    $sql = '';
        if($granu == 'm'){
        $sql = "SELECT 	concat( /*date_part('month',data_daty.daty)*/
							CASE WHEN date_part('month',data_daty.daty) = 1 THEN 'Janvier' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 2 THEN 'Février' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 3 THEN 'Mars' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 4 THEN 'Avril' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 5 THEN 'Mai' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 6 THEN 'Juin' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 7 THEN 'Juillet' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 8 THEN 'Aout' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 9 THEN 'Septembre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 10 THEN 'Octobre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 11 THEN 'Novembre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 12 THEN 'Décembre' END
							END END END END END END END END END END END
							,' ',date_part('year',data_daty.daty)) as lib_daty,
							data_daty.daty,
                            CASE WHEN ko_mail is null THEN 0 ELSE ko_mail::integer END,
                            CASE WHEN ko_pj is null THEN 0 ELSE ko_pj::integer END,
							CASE WHEN ko_fichier is null THEN 0 ELSE ko_fichier::integer END,	
							CASE WHEN ko_src is null THEN 0 ELSE ko_src::integer END,	
							CASE WHEN ko_definitif is null THEN 0 ELSE ko_definitif::integer END,	
							CASE WHEN ko_inconnu is null THEN 0 ELSE ko_inconnu::integer END,
							CASE WHEN ko_attente is null THEN 0 ELSE ko_attente::integer END,
							CASE WHEN ko_rejete is null THEN 0 ELSE ko_rejete::integer END,
							CASE WHEN ko_hp is null THEN 0 ELSE ko_hp::integer END
						FROM
						 (
							SELECT liste_months as daty FROM liste_months ('".$date_debut."','".$date_fin."')
						 ) as data_daty
						LEFT JOIN 
						(	SELECT
								daty,
								SUM(ko_mail) ko_mail,
								SUM(ko_pj) ko_pj,
								SUM(ko_fichier) ko_fichier,
								SUM(ko_src) ko_src,
								SUM(ko_definitif) ko_definitif,
								SUM(ko_inconnu) ko_inconnu,
								SUM(ko_attente) ko_attente,
								SUM(ko_rejete) ko_rejete,
								SUM(ko_hp) ko_hp
							FROM
							(
								SELECT daty,
								CASE WHEN rubrique ='KO MAIL' THEN nb_flux::integer ELSE 0 END  as ko_mail,
								CASE WHEN rubrique ='KO PJ' THEN nb_flux::integer ELSE 0 END as ko_pj,
								CASE WHEN rubrique ='KO FICHIER' THEN nb_flux::integer ELSE 0 END as ko_fichier,
								CASE WHEN rubrique ='KO SRC' THEN nb_flux::integer ELSE 0 END as ko_src,
								CASE WHEN rubrique ='KO DEFINITIF' THEN nb_flux::integer ELSE 0 END as ko_definitif,
								CASE WHEN rubrique ='KO INCONNU' THEN nb_flux::integer ELSE 0 END as ko_inconnu,
								CASE WHEN rubrique ='KO EN ATTENTE' THEN nb_flux::integer ELSE 0 END as ko_attente,								
								CASE WHEN rubrique ='KO REJETE' THEN nb_flux::integer ELSE 0 END as ko_rejete,								
								CASE WHEN rubrique ='KO HP' THEN nb_flux::integer ELSE 0 END as ko_hp 								
												
								FROM stat_mensuel
								WHERE titre = 'FLUX KO' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = ".$id_soc." 
								AND source = ".$source." 
								GROUP BY rubrique, daty,nb_flux
							) as global
							GROUP BY daty
						) as data_global on data_daty.daty = data_global.daty
						ORDER BY data_daty.daty asc"	;
        }
        if($granu == 's'){
            $sql = "SELECT 	semaine as lib_daty,	
					coalesce (ko_mail,0) ko_mail,
					coalesce (ko_pj,0) ko_pj,
					coalesce (ko_fichier,0) ko_fichier,
					coalesce (ko_src,0) ko_src,
					coalesce (ko_definitif,0) ko_definitif,
					coalesce (ko_inconnu,0) ko_inconnu,
					coalesce (ko_attente,0) ko_attente ,
					coalesce (ko_rejete,0) ko_rejete,
					coalesce (ko_hp,0) ko_hp 
                FROM
                (
                  SELECT daty,'S'||week as semaine FROM liste_weeks('".$date_debut."','".$date_fin."')
                ) as data_daty 
                LEFT JOIN 
                (
                SELECT
                    date_debut,
                    SUM(ko_mail) ko_mail,
                    SUM(ko_pj) ko_pj,
                    SUM(ko_fichier) ko_fichier,
                    SUM(ko_src) ko_src,
                    SUM(ko_definitif) ko_definitif,
                    SUM(ko_inconnu) ko_inconnu,
                    SUM(ko_attente) ko_attente,
                    SUM(ko_rejete) ko_rejete,
                    SUM(ko_hp) ko_hp
                    FROM (
                    SELECT date_debut,
                    CASE WHEN rubrique ='KO MAIL' THEN nb_flux::integer ELSE 0 END  as ko_mail,
                    CASE WHEN rubrique ='KO PJ' THEN nb_flux::integer ELSE 0 END as ko_pj,
                    CASE WHEN rubrique ='KO FICHIER' THEN nb_flux::integer ELSE 0 END as ko_fichier,
                    CASE WHEN rubrique ='KO SRC' THEN nb_flux::integer ELSE 0 END as ko_src,
                    CASE WHEN rubrique ='KO DEFINITIF' THEN nb_flux::integer ELSE 0 END as ko_definitif,
                    CASE WHEN rubrique ='KO INCONNU' THEN nb_flux::integer ELSE 0 END as ko_inconnu,
                    CASE WHEN rubrique ='KO EN ATTENTE' THEN nb_flux::integer ELSE 0 END as ko_attente,								
                    CASE WHEN rubrique ='KO REJETE' THEN nb_flux::integer ELSE 0 END as ko_rejete,								
                    CASE WHEN rubrique ='KO HP' THEN nb_flux::integer ELSE 0 END as ko_hp 								
                                    
                    FROM stat_hebdo
                    WHERE titre = 'FLUX KO' 
                    AND date_debut between '".$date_debut."' AND '".$date_fin."'
                    AND societe = ".$id_soc."
                    AND source = ".$source."
                    GROUP BY rubrique, date_debut,nb_flux
                ) as global
                GROUP BY date_debut) as data_global ON data_daty.daty = data_global.date_debut 
                group by semaine,ko_mail,ko_pj, ko_fichier, ko_src,ko_definitif,ko_inconnu,ko_attente,ko_rejete,ko_hp
				ORDER BY semaine asc";
        }
        if($granu == 'j'){
            $sql = "SELECT 	daty as lib_daty,	
					coalesce (ko_mail,0) ko_mail,
					coalesce (ko_pj,0) ko_pj,
					coalesce (ko_fichier,0) ko_fichier,
					coalesce (ko_src,0) ko_src,
					coalesce (ko_definitif,0) ko_definitif,
					coalesce (ko_inconnu,0) ko_inconnu,
					coalesce (ko_attente,0) ko_attente ,
					coalesce (ko_rejete,0) ko_rejete,
					coalesce (ko_hp,0) ko_hp 
				FROM
				 (
					SELECT liste_dates as daty FROM liste_dates ('".$date_debut."' , '".$date_fin."')
				 ) as data_daty
				LEFT JOIN 
				(	SELECT 
					    date_debut,
					    SUM(ko_mail) ko_mail,
					    SUM(ko_pj) ko_pj,
					    SUM(ko_fichier) ko_fichier,
					    SUM(ko_src) ko_src,
					    SUM(ko_definitif) ko_definitif,
					    SUM(ko_inconnu) ko_inconnu,
					    SUM(ko_attente) ko_attente,
					    SUM(ko_rejete) ko_rejete,
					    SUM(ko_hp) ko_hp
					FROM
					(
						SELECT date_reception::date as date_debut,
								CASE WHEN etat_pli_id = 11 THEN 1 ELSE 0 END  as ko_mail,
								CASE WHEN etat_pli_id = 12 THEN 1 ELSE 0 END as ko_pj,
								CASE WHEN etat_pli_id = 14 THEN 1 ELSE 0 END as ko_fichier,
								CASE WHEN etat_pli_id = 19 THEN 1 ELSE 0 END as ko_src,
								CASE WHEN etat_pli_id = 21 THEN 1 ELSE 0 END as ko_definitif,
								CASE WHEN etat_pli_id = 22 THEN 1 ELSE 0 END as ko_inconnu,
								CASE WHEN etat_pli_id = 23 THEN 1 ELSE 0 END as ko_attente,								
								CASE WHEN etat_pli_id = 15 THEN 1 ELSE 0 END as ko_rejete,								
								CASE WHEN etat_pli_id = 13 THEN 1 ELSE 0 END as ko_hp 									
										
						FROM  flux 
						WHERE  date_reception::date between '".$date_debut."' and '".$date_fin."'
						AND societe = ".$id_soc." 
						AND id_source = ".$source." 
						
					) as global
					GROUP BY date_debut
				) as data_global on data_daty.daty = data_global.date_debut
				group by daty,ko_mail,ko_pj, ko_fichier, ko_src,ko_definitif,ko_inconnu,ko_attente,ko_rejete,ko_hp
				ORDER BY daty asc";
        }
        /*echo "<pre>";
		print_r($sql);
        echo "</pre>";exit;
		*/
        return $this->ged_flux->query($sql)
            ->result_array();
    }

    public function get_stat_mensuel_flux( $date_debut, $date_fin ,$id_soc,$id_source){

        $sql = "SELECT 	concat( /*date_part('month',data_daty.daty)*/
							CASE WHEN date_part('month',data_daty.daty) = 1 THEN 'Janvier' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 2 THEN 'Février' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 3 THEN 'Mars' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 4 THEN 'Avril' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 5 THEN 'Mai' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 6 THEN 'Juin' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 7 THEN 'Juillet' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 8 THEN 'Aout' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 9 THEN 'Septembre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 10 THEN 'Octobre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 11 THEN 'Novembre' ELSE
							CASE WHEN date_part('month',data_daty.daty) = 12 THEN 'Décembre' END
							END END END END END END END END END END END
							,' ',date_part('year',data_daty.daty)) as lib_daty,
							data_daty.daty,
                            CASE WHEN data_global.non_traite is null THEN 0 ELSE data_global.non_traite::integer END as non_traite,
                            CASE WHEN encours is null THEN 0 ELSE encours::integer END,
                            CASE WHEN traite is null THEN 0 ELSE traite::integer END,
							CASE WHEN anomalie is null THEN 0 ELSE anomalie::integer END,data_global.nb_flux_ok,data_global.nb_flux_hp,data_global.nb_flux_rejete,data_global.abonnement_ok,data_global.abonnement_hp,data_global.abonnement_rejete,							
							nb_flux_1,nb_flux_2,nb_flux_3,nb_flux_4,nb_flux_5,nb_flux_6,nb_flux_7,nb_flux_8,nb_flux_9,nb_flux_10,
							nb_flux_11,nb_flux_12,nb_flux_13,nb_flux_14,nb_flux_15,nb_flux_16,nb_flux_17,nb_flux_18,nb_flux_19,nb_flux_20,
							nb_flux_21,
							nb_abo_1,nb_abo_2,nb_abo_3,nb_abo_4,nb_abo_5,nb_abo_6,nb_abo_7,nb_abo_8,nb_abo_9,nb_abo_10,
							nb_abo_11,nb_abo_12,nb_abo_13,nb_abo_14,nb_abo_15,nb_abo_16,nb_abo_17,nb_abo_18,nb_abo_19,nb_abo_20,
							nb_abo_21,							
							nb_cltr_flux_1,nb_cltr_flux_2,nb_cltr_flux_3,nb_cltr_flux_4,nb_cltr_flux_5,nb_cltr_flux_6,nb_cltr_flux_7,nb_cltr_flux_8,nb_cltr_flux_9,nb_cltr_flux_10,nb_cltr_flux_11,nb_cltr_flux_12,nb_cltr_flux_13,nb_cltr_flux_14,nb_cltr_flux_15,nb_cltr_flux_16,nb_cltr_flux_17,nb_cltr_flux_18,nb_cltr_flux_19,nb_cltr_flux_20,
							nb_cltr_flux_21,		
							nb_cltr_abo_1,nb_cltr_abo_2,nb_cltr_abo_3,nb_cltr_abo_4,nb_cltr_abo_5,nb_cltr_abo_6,nb_cltr_abo_7,nb_cltr_abo_8,nb_cltr_abo_9,nb_cltr_abo_10,nb_cltr_abo_11,nb_cltr_abo_12,nb_cltr_abo_13,nb_cltr_abo_14,nb_cltr_abo_15,nb_cltr_abo_16,nb_cltr_abo_17,nb_cltr_abo_18,nb_cltr_abo_19,nb_cltr_abo_20,
							nb_cltr_abo_21,nb_encours_typage, nb_att_niv1,nb_encours_niv1,nb_att_niv2,nb_encours_niv2,nb_att_consigne,nb_encours_rettt, abo_encours_typage,abo_att_niv1,abo_encours_niv1,abo_att_niv2,abo_encours_niv2,abo_att_consigne,abo_encours_rettt ,
							nb_non_traite,nb_en_cours,abo_non_traite,abo_en_cours,
							nb_ko_mail,nb_ko_pj,nb_ko_fichier,nb_ko_src,nb_ko_definitif,nb_ko_attente,nb_ko_inconnu,nb_ko_rejete,nb_ko_hp,nb_flux_sans_traitement  
						FROM
						 (
							SELECT liste_months as daty FROM liste_months ('".$date_debut."','".$date_fin."')
						 ) as data_daty
						LEFT JOIN 
						(	SELECT
								daty,
								SUM(non_traite) non_traite,
								SUM(encours) encours,
								SUM(traite) traite,
								SUM(anomalie) anomalie,
								SUM(nb_flux_ok) nb_flux_ok,
								SUM(nb_flux_hp) nb_flux_hp,
								SUM(nb_flux_rejete) nb_flux_rejete,
								SUM(nb_flux_sans_traitement) nb_flux_sans_traitement,
								SUM(abonnement_ok) abonnement_ok,
								SUM(abonnement_hp) abonnement_hp,
								SUM(abonnement_rejete) abonnement_rejete 
							FROM
							(
								SELECT daty,
								CASE WHEN rubrique ='Non traité' THEN nb_flux::integer ELSE 0 END  as non_traite,
								CASE WHEN rubrique ='Encours' THEN nb_flux::integer ELSE 0 END as encours,
								CASE WHEN rubrique ='Cloturé' THEN nb_flux::integer ELSE 0 END as traite,
								CASE WHEN rubrique ='Anomalie' THEN nb_flux::integer ELSE 0 END as anomalie,				
								CASE WHEN rubrique ='Traité OK' THEN nb_flux::integer ELSE 0 END as nb_flux_ok,				
								CASE WHEN rubrique ='Traité HP' THEN nb_flux::integer ELSE 0 END as nb_flux_hp,				
								CASE WHEN rubrique ='Traité Rejeté' THEN nb_flux::integer ELSE 0 END as nb_flux_rejete,
								CASE WHEN rubrique ='Sans traitement' THEN nb_flux::integer ELSE 0 END as nb_flux_sans_traitement,											
								CASE WHEN rubrique ='Traité OK' THEN nb_abo::integer ELSE 0 END as abonnement_ok,				
								CASE WHEN rubrique ='Traité HP' THEN nb_abo::integer ELSE 0 END as abonnement_hp,				
								CASE WHEN rubrique ='Traité Rejeté' THEN nb_abo::integer ELSE 0 END as abonnement_rejete  				
								FROM stat_mensuel
								WHERE titre = 'GLOBAL' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = ".$id_soc." AND source = ".$id_source." 
								GROUP BY rubrique, daty,nb_flux,nb_abo
							) as global
							GROUP BY daty
						) as data_global on data_daty.daty = data_global.daty
						LEFT JOIN (
							SELECT 
								daty,
								nb_flux_1,nb_flux_2,nb_flux_3,nb_flux_4,nb_flux_5,nb_flux_6,nb_flux_7,nb_flux_8,nb_flux_9,nb_flux_10,
								nb_flux_11,nb_flux_12,nb_flux_13,nb_flux_14,nb_flux_15,nb_flux_16,nb_flux_17,nb_flux_18,nb_flux_19,nb_flux_20,
								nb_flux_21,
								nb_abo_1,nb_abo_2,nb_abo_3,nb_abo_4,nb_abo_5,nb_abo_6,nb_abo_7,nb_abo_8,nb_abo_9,nb_abo_10,
								nb_abo_11,nb_abo_12,nb_abo_13,nb_abo_14,nb_abo_15,nb_abo_16,nb_abo_17,nb_abo_18,nb_abo_19,nb_abo_20,
								nb_abo_21 
							FROM 			
							(
								SELECT '".$date_debut."' ::date as daty,
								split_part(data_1, ',', 3) as nb_flux_1,split_part(data_1, ',', 4) as nb_abo_1,
								split_part(data_2, ',', 3) as nb_flux_2,split_part(data_2, ',', 4) as nb_abo_2,
								split_part(data_3, ',', 3) as nb_flux_3,split_part(data_3, ',', 4) as nb_abo_3,
								split_part(data_4, ',', 3) as nb_flux_4,split_part(data_4, ',', 4) as nb_abo_4,
								split_part(data_5, ',', 3) as nb_flux_5,split_part(data_5, ',', 4) as nb_abo_5,
								split_part(data_6, ',', 3) as nb_flux_6,split_part(data_6, ',', 4) as nb_abo_6,
								split_part(data_7, ',', 3) as nb_flux_7,split_part(data_7, ',', 4) as nb_abo_7,
								split_part(data_8, ',', 3) as nb_flux_8,split_part(data_8, ',', 4) as nb_abo_8,
								split_part(data_9, ',', 3) as nb_flux_9,split_part(data_9, ',', 4) as nb_abo_9,
								split_part(data_10, ',', 3) as nb_flux_10,split_part(data_10, ',', 4) as nb_abo_10,
								split_part(data_11, ',', 3) as nb_flux_11,split_part(data_11, ',', 4) as nb_abo_11,
								split_part(data_12, ',', 3) as nb_flux_12,split_part(data_12, ',', 4) as nb_abo_12,
								split_part(data_13, ',', 3) as nb_flux_13,split_part(data_13, ',', 4) as nb_abo_13,
								split_part(data_14, ',', 3) as nb_flux_14,split_part(data_14, ',', 4) as nb_abo_14,
								split_part(data_15, ',', 3) as nb_flux_15,split_part(data_15, ',', 4) as nb_abo_15,
								split_part(data_16, ',', 3) as nb_flux_16,split_part(data_16, ',', 4) as nb_abo_16,
								split_part(data_17, ',', 3) as nb_flux_17,split_part(data_17, ',', 4) as nb_abo_17,
								split_part(data_18, ',', 3) as nb_flux_18,split_part(data_18, ',', 4) as nb_abo_18,
								split_part(data_19, ',', 3) as nb_flux_19,split_part(data_19, ',', 4) as nb_abo_19,
								split_part(data_20, ',', 3) as nb_flux_20,split_part(data_20, ',', 4) as nb_abo_20,
								split_part(data_21, ',', 3) as nb_flux_21,split_part(data_21, ',', 4) as nb_abo_21 
								FROM f_stat_mensuel_par_bloc_soc_typo('".$id_soc."','Flux par typologie', '".$date_debut."' , '".$date_fin."', '".$id_source."')
								
							) as data_flux_par_typo
							
						) as data_flux_typo on data_daty.daty = data_flux_typo.daty
						LEFT JOIN (
							SELECT 
								daty,
								nb_cltr_flux_1,nb_cltr_flux_2,nb_cltr_flux_3,nb_cltr_flux_4,nb_cltr_flux_5,nb_cltr_flux_6,nb_cltr_flux_7,nb_cltr_flux_8,nb_cltr_flux_9,nb_cltr_flux_10,nb_cltr_flux_11,nb_cltr_flux_12,nb_cltr_flux_13,nb_cltr_flux_14,nb_cltr_flux_15,nb_cltr_flux_16,nb_cltr_flux_17,nb_cltr_flux_18,nb_cltr_flux_19,nb_cltr_flux_20,nb_cltr_flux_21,nb_cltr_abo_1,nb_cltr_abo_2,nb_cltr_abo_3,nb_cltr_abo_4,nb_cltr_abo_5,nb_cltr_abo_6,nb_cltr_abo_7,nb_cltr_abo_8,nb_cltr_abo_9,nb_cltr_abo_10,nb_cltr_abo_11,nb_cltr_abo_12,nb_cltr_abo_13,nb_cltr_abo_14,nb_cltr_abo_15,nb_cltr_abo_16,nb_cltr_abo_17,nb_cltr_abo_18,nb_cltr_abo_19,nb_cltr_abo_20,nb_cltr_abo_21 
							FROM 			
							(
								SELECT '".$date_debut."' ::date as daty,
								split_part(data_1, ',', 3) as nb_cltr_flux_1,split_part(data_1, ',', 4) as nb_cltr_abo_1,
								split_part(data_2, ',', 3) as nb_cltr_flux_2,split_part(data_2, ',', 4) as nb_cltr_abo_2,
								split_part(data_3, ',', 3) as nb_cltr_flux_3,split_part(data_3, ',', 4) as nb_cltr_abo_3,
								split_part(data_4, ',', 3) as nb_cltr_flux_4,split_part(data_4, ',', 4) as nb_cltr_abo_4,
								split_part(data_5, ',', 3) as nb_cltr_flux_5,split_part(data_5, ',', 4) as nb_cltr_abo_5,
								split_part(data_6, ',', 3) as nb_cltr_flux_6,split_part(data_6, ',', 4) as nb_cltr_abo_6,
								split_part(data_7, ',', 3) as nb_cltr_flux_7,split_part(data_7, ',', 4) as nb_cltr_abo_7,
								split_part(data_8, ',', 3) as nb_cltr_flux_8,split_part(data_8, ',', 4) as nb_cltr_abo_8,
								split_part(data_9, ',', 3) as nb_cltr_flux_9,split_part(data_9, ',', 4) as nb_cltr_abo_9,
								split_part(data_10, ',', 3) as nb_cltr_flux_10,split_part(data_10, ',', 4) as nb_cltr_abo_10,
								split_part(data_11, ',', 3) as nb_cltr_flux_11,split_part(data_11, ',', 4) as nb_cltr_abo_11,
								split_part(data_12, ',', 3) as nb_cltr_flux_12,split_part(data_12, ',', 4) as nb_cltr_abo_12,
								split_part(data_13, ',', 3) as nb_cltr_flux_13,split_part(data_13, ',', 4) as nb_cltr_abo_13,
								split_part(data_14, ',', 3) as nb_cltr_flux_14,split_part(data_14, ',', 4) as nb_cltr_abo_14,
								split_part(data_15, ',', 3) as nb_cltr_flux_15,split_part(data_15, ',', 4) as nb_cltr_abo_15,
								split_part(data_16, ',', 3) as nb_cltr_flux_16,split_part(data_16, ',', 4) as nb_cltr_abo_16,
								split_part(data_17, ',', 3) as nb_cltr_flux_17,split_part(data_17, ',', 4) as nb_cltr_abo_17,
								split_part(data_18, ',', 3) as nb_cltr_flux_18,split_part(data_18, ',', 4) as nb_cltr_abo_18,
								split_part(data_19, ',', 3) as nb_cltr_flux_19,split_part(data_19, ',', 4) as nb_cltr_abo_19,
								split_part(data_20, ',', 3) as nb_cltr_flux_20,split_part(data_20, ',', 4) as nb_cltr_abo_20,
								split_part(data_21, ',', 3) as nb_cltr_flux_21,split_part(data_21, ',', 4) as nb_cltr_abo_21 
								FROM f_stat_mensuel_par_bloc_soc_typo('".$id_soc."','Flux cloturés', '".$date_debut."' , '".$date_fin."', '".$id_source."')
							) as data_flux_clos
							
						) as data_flux_clotures on data_daty.daty = data_flux_clotures.daty
						LEFT JOIN (
							SELECT 
								daty,
								sum(nb_encours_typage) as nb_encours_typage,
								sum(nb_att_niv1) as nb_att_niv1,
								sum(nb_encours_niv1) as nb_encours_niv1,
								sum(nb_att_niv2) as nb_att_niv2,
								sum(nb_encours_niv2) as nb_encours_niv2,
								sum(nb_att_consigne) as nb_att_consigne,
								sum(nb_encours_rettt) as nb_encours_rettt,
								sum(abo_encours_typage) as abo_encours_typage,
								sum(abo_att_niv1) as abo_att_niv1,
								sum(abo_encours_niv1) as abo_encours_niv1,
								sum(abo_att_niv2) as abo_att_niv2,
								sum(abo_encours_niv2) as abo_encours_niv2,
								sum(abo_att_consigne) as abo_att_consigne,
								sum(abo_encours_rettt) as abo_encours_rettt 
							FROM
							(SELECT daty,
								CASE WHEN rubrique ='En cours de typage' THEN nb_flux::integer ELSE 0 END  as nb_encours_typage,
								CASE WHEN rubrique ='En attente de saisie niveau 1' THEN nb_flux::integer ELSE 0 END as nb_att_niv1,
								CASE WHEN rubrique ='En cours de saisie niveau 1' THEN nb_flux::integer ELSE 0 END as nb_encours_niv1,
								CASE WHEN rubrique ='En attente de saisie niveau 2' THEN nb_flux::integer ELSE 0 END as nb_att_niv2,
								CASE WHEN rubrique ='En cours de saisie niveau 2' THEN nb_flux::integer ELSE 0 END as nb_encours_niv2,
								CASE WHEN rubrique ='En attente consigne' THEN nb_abo::integer ELSE 0 END  as nb_att_consigne,
								CASE WHEN rubrique ='En cours de retraitement' THEN nb_abo::integer ELSE 0 END as nb_encours_rettt,
								CASE WHEN rubrique ='En cours de typage' THEN nb_abo::integer ELSE 0 END  as abo_encours_typage,
								CASE WHEN rubrique ='En attente de saisie niveau 1' THEN nb_abo::integer ELSE 0 END as abo_att_niv1,
								CASE WHEN rubrique ='En cours de saisie niveau 1' THEN nb_abo::integer ELSE 0 END as abo_encours_niv1,
								CASE WHEN rubrique ='En attente de saisie niveau 2' THEN nb_abo::integer ELSE 0 END as abo_att_niv2,
								CASE WHEN rubrique ='En cours de saisie niveau 2' THEN nb_abo::integer ELSE 0 END as abo_encours_niv2,
								CASE WHEN rubrique ='En attente consigne' THEN nb_abo::integer ELSE 0 END  as abo_att_consigne,
								CASE WHEN rubrique ='En cours de retraitement' THEN nb_abo::integer ELSE 0 END as abo_encours_rettt 
								
								FROM stat_mensuel
								WHERE titre = 'Flux en cours' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = '".$id_soc."' AND source = '".$id_source."' 
								GROUP BY rubrique, daty,nb_flux,nb_abo
							)as data_flux_encours
							GROUP BY daty
						) as data_pli_en_cours on data_daty.daty = data_pli_en_cours.daty
						LEFT JOIN (
							SELECT 
								daty,sum(nb_non_traite) nb_non_traite,sum(nb_en_cours) nb_en_cours,
								sum(abo_non_traite) abo_non_traite,sum(abo_en_cours) abo_en_cours
							FROM 			
							(
								SELECT daty,
								CASE WHEN rubrique ='Non traité' THEN nb_flux::integer ELSE 0 END  as nb_non_traite,
								CASE WHEN rubrique ='En cours' THEN nb_flux::integer ELSE 0 END as nb_en_cours,
								CASE WHEN rubrique ='Non traité' THEN nb_abo::integer ELSE 0 END  as abo_non_traite,
								CASE WHEN rubrique ='En cours' THEN nb_abo::integer ELSE 0 END as abo_en_cours
								
								FROM stat_mensuel
								WHERE titre = 'Restant à traiter' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = '".$id_soc."' AND source = '".$id_source."' 
								GROUP BY rubrique, daty,nb_flux,nb_abo
							) as data_restant
							GROUP BY daty
						) as data_restant on data_daty.daty = data_restant.daty
						LEFT JOIN (
							SELECT 
								daty,sum(nb_ko_mail) nb_ko_mail,sum(nb_ko_pj) nb_ko_pj,
								sum(nb_ko_fichier) nb_ko_fichier,sum(nb_ko_src) nb_ko_src,
								sum(nb_ko_definitif) nb_ko_definitif,sum(nb_ko_inconnu) nb_ko_inconnu,
								sum(nb_ko_attente) nb_ko_attente,sum(nb_ko_rejete) nb_ko_rejete,
								sum(nb_ko_hp) nb_ko_hp,
								sum(abo_ko_mail) abo_ko_mail,sum(abo_ko_pj) abo_ko_pj,
								sum(abo_ko_fichier) abo_ko_fichier,sum(abo_ko_src) abo_ko_src,
								sum(abo_ko_definitif) abo_ko_definitif,sum(abo_ko_inconnu) abo_ko_inconnu,
								sum(abo_ko_attente) abo_ko_attente,sum(abo_ko_rejete) abo_ko_rejete,
								sum(abo_ko_hp) abo_ko_hp 
							FROM 			
							(
								SELECT daty,
								CASE WHEN rubrique ='KO MAIL' THEN nb_flux::integer ELSE 0 END  as nb_ko_mail,
								CASE WHEN rubrique ='KO PJ' THEN nb_flux::integer ELSE 0 END as nb_ko_pj,
								CASE WHEN rubrique ='KO FICHIER' THEN nb_flux::integer ELSE 0 END  as nb_ko_fichier,
								CASE WHEN rubrique ='KO SRC' THEN nb_flux::integer ELSE 0 END as nb_ko_src,
								CASE WHEN rubrique ='KO DEFINITIF' THEN nb_flux::integer ELSE 0 END  as nb_ko_definitif,
								CASE WHEN rubrique ='KO INCONNU' THEN nb_flux::integer ELSE 0 END as nb_ko_inconnu,
								CASE WHEN rubrique ='KO EN ATTENTE' THEN nb_flux::integer ELSE 0 END  as nb_ko_attente,
								CASE WHEN rubrique ='KO REJETE' THEN nb_flux::integer ELSE 0 END as nb_ko_rejete,
								CASE WHEN rubrique ='KO HP' THEN nb_flux::integer ELSE 0 END  as nb_ko_hp,
								CASE WHEN rubrique ='KO MAIL' THEN nb_abo::integer ELSE 0 END  as abo_ko_mail,
								CASE WHEN rubrique ='KO PJ' THEN nb_abo::integer ELSE 0 END as abo_ko_pj,
								CASE WHEN rubrique ='KO FICHIER' THEN nb_abo::integer ELSE 0 END  as abo_ko_fichier,
								CASE WHEN rubrique ='KO SRC' THEN nb_abo::integer ELSE 0 END as abo_ko_src,
								CASE WHEN rubrique ='KO DEFINITIF' THEN nb_abo::integer ELSE 0 END  as abo_ko_definitif,
								CASE WHEN rubrique ='KO INCONNU' THEN nb_abo::integer ELSE 0 END as abo_ko_inconnu,
								CASE WHEN rubrique ='KO EN ATTENTE' THEN nb_abo::integer ELSE 0 END  as abo_ko_attente,
								CASE WHEN rubrique ='KO REJETE' THEN nb_abo::integer ELSE 0 END as abo_ko_rejete,
								CASE WHEN rubrique ='KO HP' THEN nb_abo::integer ELSE 0 END  as abo_ko_hp 
								
								FROM stat_mensuel
								WHERE titre = 'FLUX KO' 
								AND daty between '".$date_debut."' AND '".$date_fin."'
								AND societe = '".$id_soc."' AND source = '".$id_source."' 
								GROUP BY rubrique, daty,nb_flux,nb_abo
							) as data_ko
							GROUP BY daty
						) as data_ko on data_daty.daty = data_ko.daty
						ORDER BY data_daty.daty asc"	;
        /*echo "<pre>";
		print_r($sql);
        echo "</pre>";exit;
		*/
        return $this->ged_flux->query($sql)
            ->result_array();

    }

    public function get_stat_hebdo_flux($date_debut,$date_fin,$id_soc,$id_source){
        $sql = "SELECT semaine as lib_daty,date_debut as daty,
                SUM(total) total,
                SUM(non_traite) non_traite, 
                SUM(encours) encours, 
                SUM(cloture) traite, 
                SUM(anomalie) anomalie, 
                SUM(traite) nb_flux_ok, 
                SUM(hp) nb_flux_hp, 
                SUM(rejete) nb_flux_rejete, 
                SUM(rejete) nb_flux_sans_traitement, 
                SUM(ko_mail) nb_ko_mail, 
                SUM(ko_pj) nb_ko_pj, 
                SUM(ko_fichier) nb_ko_fichier, 
                SUM(ko_src) nb_ko_src, 
                SUM(ko_definitif) nb_ko_definitif, 
                SUM(ko_inconnu) nb_ko_inconnu, 
                SUM(ko_attente) nb_ko_attente, 
                SUM(ko_rejete) nb_ko_rejete,
                SUM(ko_hp) nb_ko_hp  
                FROM (
                SELECT semaine,date_debut,
                    CASE WHEN rubrique = 'Total' THEN SUM(nb_flux) ELSE 0 END AS total,
                    CASE WHEN rubrique = 'Non traité' THEN SUM(nb_flux) ELSE 0 END AS non_traite,
                    CASE WHEN rubrique = 'Encours' THEN SUM(nb_flux) ELSE 0 END AS encours,
                    CASE WHEN rubrique = 'Cloturé' THEN SUM(nb_flux) ELSE 0 END AS cloture,
                    CASE WHEN rubrique = 'Anomalie' THEN SUM(nb_flux) ELSE 0 END AS anomalie,
                    CASE WHEN rubrique = 'Traité OK' THEN SUM(nb_flux) ELSE 0 END AS traite,
                    CASE WHEN rubrique = 'Sans traitement' THEN SUM(nb_flux) ELSE 0 END AS sans_traitement,
                    CASE WHEN rubrique = 'Traité HP' THEN SUM(nb_flux) ELSE 0 END AS hp,
                    CASE WHEN rubrique = 'Traité Rejeté' THEN SUM(nb_flux) ELSE 0 END AS rejete,
                    CASE WHEN rubrique = 'KO MAIL' THEN SUM(nb_flux) ELSE 0 END AS ko_mail,
                    CASE WHEN rubrique = 'KO PJ' THEN SUM(nb_flux) ELSE 0 END AS ko_pj,
                    CASE WHEN rubrique = 'KO FICHIER' THEN SUM(nb_flux) ELSE 0 END AS ko_fichier,
                    CASE WHEN rubrique = 'KO SRC' THEN SUM(nb_flux) ELSE 0 END AS ko_src,
                    CASE WHEN rubrique = 'KO DEFINITIF' THEN SUM(nb_flux) ELSE 0 END AS ko_definitif,
                    CASE WHEN rubrique = 'KO INCONNU' THEN SUM(nb_flux) ELSE 0 END AS ko_inconnu,
                    CASE WHEN rubrique = 'KO EN ATTENTE' THEN SUM(nb_flux) ELSE 0 END AS ko_attente,
                    CASE WHEN rubrique = 'KO REJETE' THEN SUM(nb_flux) ELSE 0 END AS ko_rejete,
                    CASE WHEN rubrique = 'KO HP' THEN SUM(nb_flux) ELSE 0 END AS ko_hp 
                                
                FROM stat_hebdo
                WHERE titre = 'GLOBAL' 
                AND societe = ".$id_soc." and source = ".$id_source." AND date_debut  between '".$date_debut."' and '".$date_fin."'
                GROUP BY rubrique, semaine,date_debut
                ) as data_global 
                GROUP BY semaine, daty 
                ORDER BY semaine ASC 
                ";

        return $this->ged_flux->query($sql)
                                ->result_array();
    }
	public function get_stat_mois_typologlie( $date_debut, $date_fin ,$id_soc,$id_source){

        if($id_source == '3'){
           $sql = "SELECT mois,
					mois as date_courrier ,
					sum(coalesce(info_0 ,0)) as info_0 ,
					sum(coalesce(info_2 ,0)) as info_2 ,
					sum(coalesce(info_3 ,0)) as info_3 ,
					sum(coalesce(info_4 ,0)) as info_4 ,
					sum(coalesce(info_5 ,0)) as info_5 ,
					sum(coalesce(info_6 ,0)) as info_6 ,
					sum(coalesce(info_7 ,0)) as info_7 ,
					sum(coalesce(info_8 ,0)) as info_8 ,
					sum(coalesce(info_9 ,0)) as info_9 ,
					sum(coalesce(info_10 ,0)) as info_10 ,
					sum(coalesce(info_11 ,0)) as info_11 ,
					sum(coalesce(info_12 ,0)) as info_12 ,
					sum(coalesce(info_13 ,0)) as info_13 ,
					sum(coalesce(info_14 ,0)) as info_14 ,
					sum(coalesce(info_15 ,0)) as info_15 ,
					sum(coalesce(info_16 ,0)) as info_16 ,
					sum(coalesce(info_17 ,0)) as info_17 ,
					sum(coalesce(info_18 ,0)) as info_18 ,
					sum(coalesce(info_19 ,0)) as info_19 ,
					sum(coalesce(info_20 ,0)) as info_20 ,
					sum(coalesce(info_21 ,0)) as info_21 ,
					sum(coalesce(info_22 ,0)) as info_22 ,
					sum(coalesce(info_23 ,0)) as info_23 ,
					sum(coalesce(info_24 ,0)) as info_24 ,
					sum(coalesce(info_25 ,0)) as info_25 ,
					sum(coalesce(info_26 ,	0)) as info_26 ,
					sum(coalesce(info_27 ,	0)) as info_27 
					FROM
					(
						SELECT  liste_dates as daty ,
						CASE WHEN date_part('month',liste_dates) = 1 THEN 'Janvier ' || substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 2 THEN 'Février ' || substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 3 THEN 'Mars ' || substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 4 THEN 'Avril '|| substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 5 THEN 'Mai '|| substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 6 THEN 'Juin ' || substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 7 THEN 'Juillet ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 8 THEN 'Aout ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 9 THEN 'Septembre ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 10 THEN 'Octobre ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 11 THEN 'Novembre ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 12 THEN 'Décembre ' || substr(liste_dates::character varying,1,4)  END
						END END END END END END END END END END END as mois 
						from public.liste_dates( '".$date_debut."', (SELECT (date '".$date_fin."' + interval '1 month' - interval '1 day')::date))
					) as data_date
					left join(
					SELECT date_courrier, info_0 , info_2 , info_3 , info_4 , info_5 , info_6 , info_7 , info_8 , info_9 , info_10 , info_11 , info_12 , info_13 , info_14 , info_15 , info_16 , info_17 , info_18 , info_19 , info_20 , info_21 , info_22 , info_23 , info_24 , info_25 , info_26 , info_27 
					FROM f_pli_cloture_detail('".$date_debut."' ,(SELECT (date '".$date_fin."' + interval '1 month' - interval '1 day')::date), ".$id_soc.")
					 ) as data_pli ON data_date.daty = data_pli.date_courrier
					 GROUP BY mois
					 ORDER BY date_courrier ASC 
					"	;
                       
            return $this->ged->query($sql)
                                ->result_array();
        }
        else{
            $sql = "SELECT mois,
					mois as date_reception ,
					sum(coalesce(info_1 ,0)) as info_1 ,
					sum(coalesce(info_2 ,0)) as info_2 ,
					sum(coalesce(info_3 ,0)) as info_3 ,
					sum(coalesce(info_4 ,0)) as info_4 ,
					sum(coalesce(info_5 ,0)) as info_5 ,
					sum(coalesce(info_6 ,0)) as info_6 ,
					sum(coalesce(info_7 ,0)) as info_7 ,
					sum(coalesce(info_8 ,0)) as info_8 ,
					sum(coalesce(info_9 ,0)) as info_9 ,
					sum(coalesce(info_10 ,0)) as info_10 ,
					sum(coalesce(info_11 ,0)) as info_11 ,
					sum(coalesce(info_12 ,0)) as info_12 ,
					sum(coalesce(info_13 ,0)) as info_13 ,
					sum(coalesce(info_14 ,0)) as info_14 ,
					sum(coalesce(info_15 ,0)) as info_15 ,
					sum(coalesce(info_16 ,0)) as info_16 ,
					sum(coalesce(info_17 ,0)) as info_17 ,
					sum(coalesce(info_18 ,0)) as info_18 ,
					sum(coalesce(info_19 ,0)) as info_19 ,
					sum(coalesce(info_20 ,0)) as info_20 ,
					sum(coalesce(info_21 ,0)) as info_21 
					FROM
					(
						SELECT  liste_dates as daty ,
						CASE WHEN date_part('month',liste_dates) = 1 THEN 'Janvier ' || substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 2 THEN 'Février ' || substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 3 THEN 'Mars ' || substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 4 THEN 'Avril '|| substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 5 THEN 'Mai '|| substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 6 THEN 'Juin ' || substr(liste_dates::character varying,1,4) ELSE
						CASE WHEN date_part('month',liste_dates) = 7 THEN 'Juillet ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 8 THEN 'Aout ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 9 THEN 'Septembre ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 10 THEN 'Octobre ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 11 THEN 'Novembre ' || substr(liste_dates::character varying,1,4)  ELSE
						CASE WHEN date_part('month',liste_dates) = 12 THEN 'Décembre ' || substr(liste_dates::character varying,1,4)  END
						END END END END END END END END END END END as mois 
						from public.liste_dates( '".$date_debut."', (SELECT (date '".$date_fin."' + interval '1 month' - interval '1 day')::date))
					) as data_date
					left join(
					SELECT date_reception, info_1 , info_2 , info_3 , info_4 , info_5 , info_6 , info_7 , info_8 , info_9 , info_10 , info_11 , info_12 , info_13 , info_14 , info_15 , info_16 , info_17 , info_18 , info_19 , info_20 , info_21  
					FROM f_flux_cloture_detail('".$date_debut."' ,(SELECT (date '".$date_fin."' + interval '1 month' - interval '1 day')::date), ".$id_soc.",".$id_source.")
					 ) as data_pli ON data_date.daty = data_pli.date_reception
					 GROUP BY mois
					 ORDER BY date_reception ASC 
					"	;

            return $this->ged_flux->query($sql)
                ->result_array();
        }
     
    }
	public function get_stat_mensuel_typologlie( $date_debut, $date_fin ,$id_soc,$id_source){

        if($id_source == '3'){
           $sql = "	SELECT date_courrier, info_0 , info_2 , info_3 , info_4 , info_5 , info_6 , info_7 , info_8 , info_9 , info_10 , info_11 , info_12 , info_13 , info_14 , info_15 , info_16 , info_17 , info_18 , info_19 , info_20 , info_21 , info_22 , info_23 , info_24 , info_25 , info_26 , info_27 
           FROM f_pli_cloture_detail( '".$date_debut."' ,(SELECT (date '".$date_fin."' + interval '1 month' - interval '1 day')::date), ".$id_soc.") ORDER BY date_courrier ASC "	;
            /*echo "<pre>";
            print_r($sql);
            echo "</pre>";
            exit;
            */
            return $this->ged->query($sql)
                                ->result_array();
        }
        ELSE{
            $sql = "	SELECT date_reception, info_1 , info_2 , info_3 , info_4 , info_5 , info_6 , info_7 , info_8 , info_9 , info_10 , info_11 , info_12 , info_13 , info_14 , info_15 , info_16 , info_17 , info_18 , info_19 , info_20 , info_21  
           FROM f_flux_cloture_detail( '".$date_debut."' ,(SELECT (date '".$date_fin."' + interval '1 month' - interval '1 day')::date), ".$id_soc.",".$id_source.") ORDER BY date_reception ASC "	;
            /*echo "<pre>";
            print_r($sql);
            echo "</pre>";
            exit;
            */
            return $this->ged_flux->query($sql)
                ->result_array();
        }
    }
	public function get_stat_hebdo_typologlie( $date_debut, $date_fin ,$id_soc,$id_source,$granu)
    {
        if ($id_source == '3'){
            $sql = "	SELECT  
				week as date_courrier,
				sum(coalesce(info_0 ,0)) as info_0 ,
				sum(coalesce(info_2 ,0)) as info_2 ,
				sum(coalesce(info_3 ,0)) as info_3 ,
				sum(coalesce(info_4 ,0)) as info_4 ,
				sum(coalesce(info_5 ,0)) as info_5 ,
				sum(coalesce(info_6 ,0)) as info_6 ,
				sum(coalesce(info_7 ,0)) as info_7 ,
				sum(coalesce(info_8 ,0)) as info_8 ,
				sum(coalesce(info_9 ,0)) as info_9 ,
				sum(coalesce(info_10 ,0)) as info_10 ,
				sum(coalesce(info_11 ,0)) as info_11 ,
				sum(coalesce(info_12 ,0)) as info_12 ,
				sum(coalesce(info_13 ,0)) as info_13 ,
				sum(coalesce(info_14 ,0)) as info_14 ,
				sum(coalesce(info_15 ,0)) as info_15 ,
				sum(coalesce(info_16 ,0)) as info_16 ,
				sum(coalesce(info_17 ,0)) as info_17 ,
				sum(coalesce(info_18 ,0)) as info_18 ,
				sum(coalesce(info_19 ,0)) as info_19 ,
				sum(coalesce(info_20 ,0)) as info_20 ,
				sum(coalesce(info_21 ,0)) as info_21 ,
				sum(coalesce(info_22 ,0)) as info_22 ,
				sum(coalesce(info_23 ,0)) as info_23 ,
				sum(coalesce(info_24 ,0)) as info_24 ,
				sum(coalesce(info_25 ,0)) as info_25 ,
				sum(coalesce(info_26 ,0)) as info_26 ,
				sum(coalesce(info_27 ,0)) as info_27 

				FROM
				(
				SELECT 'S '||week as week, daty from public.liste_weeks( '" . $date_debut . "', '" . $date_fin . "')
				) as data_date
				left join(
				SELECT date_courrier, info_0 , info_2 , info_3 , info_4 , info_5 , info_6 , info_7 , info_8 , info_9 , info_10 , info_11 , info_12 , info_13 , info_14 , info_15 , info_16 , info_17 , info_18 , info_19 , info_20 , info_21 , info_22 , info_23 , info_24 , info_25 , info_26 , info_27 
				FROM f_pli_cloture_detail( '" . $date_debut . "' ,'" . $date_fin . "'," . $id_soc . ") 
				 ) as data_pli ON data_date.daty = data_pli.date_courrier
				 GROUP BY week
				 ORDER BY week ASC ";
        /*
        echo "<pre>";
        print_r($sql);
        echo "</pre>";
        */

        return $this->ged->query($sql)
            ->result_array();
        }
        else{
            $sql = "	SELECT  
				week as date_reception,
				sum(coalesce(info_1 ,0)) as info_1 ,
				sum(coalesce(info_2 ,0)) as info_2 ,
				sum(coalesce(info_3 ,0)) as info_3 ,
				sum(coalesce(info_4 ,0)) as info_4 ,
				sum(coalesce(info_5 ,0)) as info_5 ,
				sum(coalesce(info_6 ,0)) as info_6 ,
				sum(coalesce(info_7 ,0)) as info_7 ,
				sum(coalesce(info_8 ,0)) as info_8 ,
				sum(coalesce(info_9 ,0)) as info_9 ,
				sum(coalesce(info_10 ,0)) as info_10 ,
				sum(coalesce(info_11 ,0)) as info_11 ,
				sum(coalesce(info_12 ,0)) as info_12 ,
				sum(coalesce(info_13 ,0)) as info_13 ,
				sum(coalesce(info_14 ,0)) as info_14 ,
				sum(coalesce(info_15 ,0)) as info_15 ,
				sum(coalesce(info_16 ,0)) as info_16 ,
				sum(coalesce(info_17 ,0)) as info_17 ,
				sum(coalesce(info_18 ,0)) as info_18 ,
				sum(coalesce(info_19 ,0)) as info_19 ,
				sum(coalesce(info_20 ,0)) as info_20 ,
				sum(coalesce(info_21 ,0)) as info_21 

				FROM
				(
				SELECT 'S '||week as week, daty from public.liste_weeks( '" . $date_debut . "', '" . $date_fin . "')
				) as data_date
				left join(
				SELECT date_reception, info_1 , info_2 , info_3 , info_4 , info_5 , info_6 , info_7 , info_8 , info_9 , info_10 , info_11 , info_12 , info_13 , info_14 , info_15 , info_16 , info_17 , info_18 , info_19 , info_20 , info_21  
				FROM f_flux_cloture_detail( '" . $date_debut . "' ,'" . $date_fin . "'," . $id_soc . ",".$id_source.") 
				 ) as data_pli ON data_date.daty = data_pli.date_reception
				 GROUP BY week
				 ORDER BY week ASC ";

            return $this->ged_flux->query($sql)
                ->result_array();
        }
		
    }
	public function get_stat_jour_typologlie( $date_debut, $date_fin ,$id_soc,$id_source,$granu)
    {
        if ($id_source == '3'){
            $sql = "	SELECT  
				daty as date_courrier,
				sum(coalesce(info_0 ,0)) as info_0 ,
				sum(coalesce(info_2 ,0)) as info_2 ,
				sum(coalesce(info_3 ,0)) as info_3 ,
				sum(coalesce(info_4 ,0)) as info_4 ,
				sum(coalesce(info_5 ,0)) as info_5 ,
				sum(coalesce(info_6 ,0)) as info_6 ,
				sum(coalesce(info_7 ,0)) as info_7 ,
				sum(coalesce(info_8 ,0)) as info_8 ,
				sum(coalesce(info_9 ,0)) as info_9 ,
				sum(coalesce(info_10 ,0)) as info_10 ,
				sum(coalesce(info_11 ,0)) as info_11 ,
				sum(coalesce(info_12 ,0)) as info_12 ,
				sum(coalesce(info_13 ,0)) as info_13 ,
				sum(coalesce(info_14 ,0)) as info_14 ,
				sum(coalesce(info_15 ,0)) as info_15 ,
				sum(coalesce(info_16 ,0)) as info_16 ,
				sum(coalesce(info_17 ,0)) as info_17 ,
				sum(coalesce(info_18 ,0)) as info_18 ,
				sum(coalesce(info_19 ,0)) as info_19 ,
				sum(coalesce(info_20 ,0)) as info_20 ,
				sum(coalesce(info_21 ,0)) as info_21 ,
				sum(coalesce(info_22 ,0)) as info_22 ,
				sum(coalesce(info_23 ,0)) as info_23 ,
				sum(coalesce(info_24 ,0)) as info_24 ,
				sum(coalesce(info_25 ,0)) as info_25 ,
				sum(coalesce(info_26 ,0)) as info_26 ,
				sum(coalesce(info_27 ,0)) as info_27 

				FROM
				(
				SELECT daty from public.liste_weeks( '" . $date_debut . "', '" . $date_fin . "')
				) as data_date
				left join(
				SELECT date_courrier, info_0 , info_2 , info_3 , info_4 , info_5 , info_6 , info_7 , info_8 , info_9 , info_10 , info_11 , info_12 , info_13 , info_14 , info_15 , info_16 , info_17 , info_18 , info_19 , info_20 , info_21 , info_22 , info_23 , info_24 , info_25 , info_26 , info_27 
				FROM f_pli_cloture_detail( '" . $date_debut . "' ,'" . $date_fin . "'," . $id_soc . ") 
				 ) as data_pli ON data_date.daty = data_pli.date_courrier
				 GROUP BY daty
				 ORDER BY daty ASC ";


        return $this->ged->query($sql)
            ->result_array();
        }
        else{
            $sql = "	SELECT  
				daty as date_recpetion,
				sum(coalesce(info_1 ,0)) as info_1 ,
				sum(coalesce(info_2 ,0)) as info_2 ,
				sum(coalesce(info_3 ,0)) as info_3 ,
				sum(coalesce(info_4 ,0)) as info_4 ,
				sum(coalesce(info_5 ,0)) as info_5 ,
				sum(coalesce(info_6 ,0)) as info_6 ,
				sum(coalesce(info_7 ,0)) as info_7 ,
				sum(coalesce(info_8 ,0)) as info_8 ,
				sum(coalesce(info_9 ,0)) as info_9 ,
				sum(coalesce(info_10 ,0)) as info_10 ,
				sum(coalesce(info_11 ,0)) as info_11 ,
				sum(coalesce(info_12 ,0)) as info_12 ,
				sum(coalesce(info_13 ,0)) as info_13 ,
				sum(coalesce(info_14 ,0)) as info_14 ,
				sum(coalesce(info_15 ,0)) as info_15 ,
				sum(coalesce(info_16 ,0)) as info_16 ,
				sum(coalesce(info_17 ,0)) as info_17 ,
				sum(coalesce(info_18 ,0)) as info_18 ,
				sum(coalesce(info_19 ,0)) as info_19 ,
				sum(coalesce(info_20 ,0)) as info_20 ,
				sum(coalesce(info_21 ,0)) as info_21  

				FROM
				(
				SELECT daty from public.liste_weeks( '" . $date_debut . "', '" . $date_fin . "')
				) as data_date
				left join(
				SELECT date_reception, info_1 , info_2 , info_3 , info_4 , info_5 , info_6 , info_7 , info_8 , info_9 , info_10 , info_11 , info_12 , info_13 , info_14 , info_15 , info_16 , info_17 , info_18 , info_19 , info_20 , info_21  
				FROM f_flux_cloture_detail( '" . $date_debut . "' ,'" . $date_fin . "'," . $id_soc . ",".$id_source.") 
				 ) as data_pli ON data_date.daty = data_pli.date_reception
				 GROUP BY daty
				 ORDER BY daty ASC ";


            return $this->ged_flux->query($sql)
                ->result_array();
        }
        
    }
	public function get_typologlie($id_source){
		if($id_source == '3'){
           $sql = "	SELECT id, typologie FROM typologie ORDER BY id asc"	;
           return $this->ged->query($sql)
                                ->result_array();
        }
        ELSE{
            $sql = "	SELECT id_typologie as id, typologie FROM typologie ORDER BY id asc"	;
            return $this->ged_flux->query($sql)
                ->result_array();
        }

    }
	
	public function get_histo_ko( $date_debut, $date_fin ,$id_soc){

        $sql = "SELECT 	concat( /*date_part('month',data_daty.daty)*/
					CASE WHEN date_part('month',data_daty.daty) = 1 THEN 'Janvier' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 2 THEN 'Février' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 3 THEN 'Mars' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 4 THEN 'Avril' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 5 THEN 'Mai' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 6 THEN 'Juin' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 7 THEN 'Juillet' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 8 THEN 'Aout' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 9 THEN 'Septembre' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 10 THEN 'Octobre' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 11 THEN 'Novembre' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 12 THEN 'Décembre' END
					END END END END END END END END END END END
					,' ',date_part('year',data_daty.daty)) as lib_daty,
					data_daty.daty,
					CASE WHEN ko_scan is null THEN 0 ELSE ko_scan::integer END,
					CASE WHEN ko_circulaire is null THEN 0 ELSE ko_circulaire::integer END,
					CASE WHEN ko_src is null THEN 0 ELSE ko_src::integer END,	
					CASE WHEN ko_definitif is null THEN 0 ELSE ko_definitif::integer END,	
					CASE WHEN ko_inconnu is null THEN 0 ELSE ko_inconnu::integer END,	
					CASE WHEN ko_en_attente is null THEN 0 ELSE ko_en_attente::integer END,
					CASE WHEN ko_abandonne is null THEN 0 ELSE ko_abandonne::integer END,
					CASE WHEN ci_editee is null THEN 0 ELSE ci_editee::integer END,
					CASE WHEN ci_envoyee is null THEN 0 ELSE ci_envoyee::integer END,
					CASE WHEN divise is null THEN 0 ELSE divise::integer END,
					CASE WHEN ko_encours is null THEN 0 ELSE ko_encours::integer END,
					coalesce(nb_pli,0) as pli_total,
					coalesce(nb_mvt,0) as mvt_total
				FROM
				 (
					SELECT liste_months as daty FROM liste_months ('".$date_debut."','".$date_fin."')
				 ) as data_daty
				LEFT JOIN 
				(	SELECT
						daty,
						SUM(ko_scan) ko_scan,
						SUM(ko_circulaire) ko_circulaire,
						SUM(ko_src) ko_src,
						SUM(ko_definitif) ko_definitif,
						SUM(ko_inconnu) ko_inconnu,
						SUM(ko_en_attente) ko_en_attente,
						SUM(ko_abandonne) ko_abandonne,
						SUM(ci_editee) ci_editee,
						SUM(ci_envoyee) ci_envoyee,
						SUM(divise) divise,
						SUM(ko_encours) ko_encours
					FROM
					(
						SELECT (substr(dt_courrier::character varying,1,7)||'-01')::date as daty,
						CASE WHEN statut_pli = 2 THEN count(distinct id_pli)::integer ELSE 0 END  as ko_scan,
						CASE WHEN statut_pli = 3 THEN count(distinct id_pli)::integer ELSE 0 END as ko_definitif,
						CASE WHEN statut_pli = 4 THEN count(distinct id_pli)::integer ELSE 0 END as ko_inconnu,
						CASE WHEN statut_pli = 5 THEN count(distinct id_pli)::integer ELSE 0 END as ko_src,
						CASE WHEN statut_pli = 6 THEN count(distinct id_pli)::integer ELSE 0 END as ko_en_attente,
						CASE WHEN statut_pli = 7 THEN count(distinct id_pli)::integer ELSE 0 END as ko_circulaire,
						CASE WHEN statut_pli = 8 THEN count(distinct id_pli)::integer ELSE 0 END as ko_abandonne,								
						CASE WHEN statut_pli = 9 THEN count(distinct id_pli)::integer ELSE 0 END as ci_editee,								
						CASE WHEN statut_pli = 10 THEN count(distinct id_pli)::integer ELSE 0 END as ci_envoyee	,							
						CASE WHEN statut_pli = 24 THEN count(distinct id_pli)::integer ELSE 0 END as divise,							
						CASE WHEN statut_pli = 12 THEN count(distinct id_pli)::integer ELSE 0 END as ko_encours														
										
						FROM histo_pli
						WHERE dt_courrier::date between '".$date_debut."' AND ('".$date_fin."'::date+ INTERVAL '1 month'-INTERVAL '1 day') AND societe_h = ".$id_soc."
						GROUP BY histo_pli.statut_pli,daty
					) as global
					GROUP BY daty
				) as data_global on data_daty.daty = data_global.daty
				LEFT JOIN
				(
					SELECT  nb_pli,nb_mvt,daty
					FROM stat_mensuel
					where daty between '".$date_debut."' AND '".$date_fin."' and societe = ".$id_soc."
					and titre = 'GLOBAL' and rubrique ='Total'
					
				) as data_pli on data_daty.daty = data_pli.daty
				ORDER BY data_daty.daty asc";
			/*echo "<pre>";
			print_r($sql);
			echo "</pre>";exit;
			*/
		
        return $this->ged->query($sql)
            ->result_array();
	}

	public function get_histo_ko_flux($date_debut,$date_fin,$id_soc,$id_source){
	    $sql = "SELECT 	concat( /*date_part('month',data_daty.daty)*/
					CASE WHEN date_part('month',data_daty.daty) = 1 THEN 'Janvier' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 2 THEN 'Février' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 3 THEN 'Mars' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 4 THEN 'Avril' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 5 THEN 'Mai' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 6 THEN 'Juin' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 7 THEN 'Juillet' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 8 THEN 'Aout' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 9 THEN 'Septembre' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 10 THEN 'Octobre' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 11 THEN 'Novembre' ELSE
					CASE WHEN date_part('month',data_daty.daty) = 12 THEN 'Décembre' END
					END END END END END END END END END END END
					,' ',date_part('year',data_daty.daty)) as lib_daty,
					data_daty.daty,
					CASE WHEN ko_mail is null THEN 0 ELSE ko_mail::integer END,
					CASE WHEN ko_pj is null THEN 0 ELSE ko_pj::integer END,
					CASE WHEN ko_fichier is null THEN 0 ELSE ko_fichier::integer END,	
					CASE WHEN ko_src is null THEN 0 ELSE ko_src::integer END,	
					CASE WHEN ko_definitif is null THEN 0 ELSE ko_definitif::integer END,	
					CASE WHEN ko_inconnu is null THEN 0 ELSE ko_inconnu::integer END,
					CASE WHEN ko_attente is null THEN 0 ELSE ko_attente::integer END,
					CASE WHEN ko_rejete is null THEN 0 ELSE ko_rejete::integer END,
					CASE WHEN ko_hp is null THEN 0 ELSE ko_hp::integer END,
					coalesce(nb_flux,0) as flux_total,
					coalesce(nb_abo,0) as abo_total
				FROM
				 (
					SELECT liste_months as daty FROM liste_months ('".$date_debut."','".$date_fin."')
				 ) as data_daty
				LEFT JOIN 
				(	SELECT
						daty,
						SUM(ko_mail) ko_mail,
						SUM(ko_pj) ko_pj,
						SUM(ko_fichier) ko_fichier,
						SUM(ko_src) ko_src,
						SUM(ko_definitif) ko_definitif,
						SUM(ko_inconnu) ko_inconnu,
						SUM(ko_attente) ko_attente,
						SUM(ko_rejete) ko_rejete,
						SUM(ko_hp) ko_hp
					FROM
					(
						SELECT (substr(dt_recept::character varying,1,7)||'-01')::date as daty,
						CASE WHEN etat_flux_h = 11 THEN count(distinct id_flux)::integer ELSE 0 END  as ko_mail,
						CASE WHEN etat_flux_h = 12 THEN count(distinct id_flux)::integer ELSE 0 END as ko_pj,
						CASE WHEN etat_flux_h = 14 THEN count(distinct id_flux)::integer ELSE 0 END as ko_fichier,
						CASE WHEN etat_flux_h = 19 THEN count(distinct id_flux)::integer ELSE 0 END as ko_src,
						CASE WHEN etat_flux_h = 21 THEN count(distinct id_flux)::integer ELSE 0 END as ko_definitif,
						CASE WHEN etat_flux_h = 22 THEN count(distinct id_flux)::integer ELSE 0 END as ko_inconnu,
						CASE WHEN etat_flux_h = 23 THEN count(distinct id_flux)::integer ELSE 0 END as ko_attente,								
						CASE WHEN etat_flux_h = 15 THEN count(distinct id_flux)::integer ELSE 0 END as ko_rejete,								
						CASE WHEN etat_flux_h = 13 THEN count(distinct id_flux)::integer ELSE 0 END as ko_hp							
										
						FROM traitement
						WHERE dt_recept::date between '".$date_debut."' AND ('".$date_fin."'::date+ INTERVAL '1 month'-INTERVAL '1 day') AND societe_h = ".$id_soc." AND source_h = ".$id_source." 
						GROUP BY traitement.etat_flux_h,daty
					) as global
					GROUP BY daty
				) as data_global on data_daty.daty = data_global.daty
				LEFT JOIN
				(
					SELECT  nb_flux,nb_abo,daty
					FROM stat_mensuel
					where daty between '".$date_debut."' AND '".$date_fin."' and societe = ".$id_soc." and source = ".$id_source." 
					and titre = 'GLOBAL' and rubrique ='Total'
					
				) as data_flux on data_daty.daty = data_flux.daty
				ORDER BY data_daty.daty asc";
	    
	    return $this->ged_flux->query($sql)
                                ->result_array();
    }
   
	
    public function get_histo_ko_hebdo( $sem_debut, $sem_fin ,$id_soc){

        $sql = "SELECT
					lib_daty, 
					SUM(ko_scan) ko_scan,
					SUM(ko_circulaire) ko_circulaire,
					SUM(ko_src) ko_src,
					SUM(ko_definitif) ko_definitif,
					SUM(ko_inconnu) ko_inconnu,
					SUM(ko_en_attente) ko_en_attente,
					SUM(ko_abandonne) ko_abandonne,
					SUM(ci_editee) ci_editee,
					SUM(ci_envoyee) ci_envoyee,
					SUM(divise) divise,
					pli_total,
					mvt_total
				FROM 
				(SELECT data_daty.semaine as lib_daty,
					data_daty.daty,
					CASE WHEN ko_scan is null THEN 0 ELSE ko_scan::integer END,
					CASE WHEN ko_circulaire is null THEN 0 ELSE ko_circulaire::integer END,
					CASE WHEN ko_src is null THEN 0 ELSE ko_src::integer END,	
					CASE WHEN ko_definitif is null THEN 0 ELSE ko_definitif::integer END,	
					CASE WHEN ko_inconnu is null THEN 0 ELSE ko_inconnu::integer END,	
					CASE WHEN ko_en_attente is null THEN 0 ELSE ko_en_attente::integer END,
					CASE WHEN ko_abandonne is null THEN 0 ELSE ko_abandonne::integer END,
					CASE WHEN ci_editee is null THEN 0 ELSE ci_editee::integer END,
					CASE WHEN ci_envoyee is null THEN 0 ELSE ci_envoyee::integer END,
					CASE WHEN divise is null THEN 0 ELSE divise::integer END,
					coalesce(nb_pli,0) as pli_total,
					coalesce(nb_mvt,0) as mvt_total
				FROM
				 (
					SELECT daty,'S'||week as semaine  FROM liste_weeks ('".$sem_debut."','".$sem_fin."')
				 ) as data_daty
				LEFT JOIN 
				(	SELECT
						daty, 
						SUM(ko_scan) ko_scan,
						SUM(ko_circulaire) ko_circulaire,
						SUM(ko_src) ko_src,
						SUM(ko_definitif) ko_definitif,
						SUM(ko_inconnu) ko_inconnu,
						SUM(ko_en_attente) ko_en_attente,
						SUM(ko_abandonne) ko_abandonne,
						SUM(ci_editee) ci_editee,
						SUM(ci_envoyee) ci_envoyee,
						SUM(divise) divise
					FROM
					(
						SELECT dt_courrier as daty,
						CASE WHEN statut_pli = 2 THEN count(distinct id_pli)::integer ELSE 0 END  as ko_scan,
						CASE WHEN statut_pli = 3 THEN count(distinct id_pli)::integer ELSE 0 END as ko_definitif,
						CASE WHEN statut_pli = 4 THEN count(distinct id_pli)::integer ELSE 0 END as ko_inconnu,
						CASE WHEN statut_pli = 5 THEN count(distinct id_pli)::integer ELSE 0 END as ko_src,
						CASE WHEN statut_pli = 6 THEN count(distinct id_pli)::integer ELSE 0 END as ko_en_attente,
						CASE WHEN statut_pli = 7 THEN count(distinct id_pli)::integer ELSE 0 END as ko_circulaire,
						CASE WHEN statut_pli = 8 THEN count(distinct id_pli)::integer ELSE 0 END as ko_abandonne,								
						CASE WHEN statut_pli = 9 THEN count(distinct id_pli)::integer ELSE 0 END as ci_editee,								
						CASE WHEN statut_pli = 10 THEN count(distinct id_pli)::integer ELSE 0 END as ci_envoyee	,							
						CASE WHEN statut_pli = 24 THEN count(distinct id_pli)::integer ELSE 0 END as divise						
										
						FROM histo_pli
						WHERE dt_courrier::date between '".$sem_debut."' AND '".$sem_fin."'
						AND societe_h = ".$id_soc."
						GROUP BY histo_pli.statut_pli,dt_courrier
					) as global
					GROUP BY daty
				) as data_global on data_daty.daty = data_global.daty
				LEFT JOIN
				(
					SELECT  nb_pli,nb_mvt,date_debut,semaine
					FROM stat_hebdo
					where date_debut between '".$sem_debut."' AND '".$sem_fin."' and societe = ".$id_soc."
					and titre = 'GLOBAL' and rubrique ='Total'
					
				) as data_pli on data_daty.semaine = data_pli.semaine
			) as tab 
			GROUP BY lib_daty,pli_total,mvt_total
			ORDER BY lib_daty asc"	;
        /*echo "<pre>";
		print_r($sql);
        echo "</pre>";exit;*/
		
        return $this->ged->query($sql)
            ->result_array();
	}

	public function get_histo_ko_hebdo_flux($sem_debut,$sem_fin,$id_soc,$id_source){
        $sql = "SELECT lib_daty,
                SUM(ko_mail) ko_mail,
                SUM(ko_pj) ko_pj,
                SUM(ko_fichier) ko_fichier,
                SUM(ko_src) ko_src,
                SUM(ko_definitif) ko_definitif,
                SUM(ko_inconnu) ko_inconnu,
                SUM(ko_attente) ko_attente,
                SUM(ko_rejete) ko_rejete,
                SUM(ko_hp) ko_hp
                FROM 
                (SELECT 	
                    data_daty.semaine as lib_daty,
                    data_daty.daty,
                    CASE WHEN ko_mail is null THEN 0 ELSE ko_mail::integer END,
                    CASE WHEN ko_pj is null THEN 0 ELSE ko_pj::integer END,
                    CASE WHEN ko_fichier is null THEN 0 ELSE ko_fichier::integer END,	
                    CASE WHEN ko_src is null THEN 0 ELSE ko_src::integer END,	
                    CASE WHEN ko_definitif is null THEN 0 ELSE ko_definitif::integer END,	
                    CASE WHEN ko_inconnu is null THEN 0 ELSE ko_inconnu::integer END,
                    CASE WHEN ko_attente is null THEN 0 ELSE ko_attente::integer END,
                    CASE WHEN ko_rejete is null THEN 0 ELSE ko_rejete::integer END,
                    CASE WHEN ko_hp is null THEN 0 ELSE ko_hp::integer END,
                    coalesce(nb_flux,0) as flux_total,
                    coalesce(nb_abo,0) as abo_total
                FROM
                 (
                    SELECT daty,'S'||week as semaine  FROM liste_weeks ('".$sem_debut."','".$sem_fin."')
                 ) as data_daty
                LEFT JOIN 
                (	SELECT
                        daty,
                        SUM(ko_mail) ko_mail,
                        SUM(ko_pj) ko_pj,
                        SUM(ko_fichier) ko_fichier,
                        SUM(ko_src) ko_src,
                        SUM(ko_definitif) ko_definitif,
                        SUM(ko_inconnu) ko_inconnu,
                        SUM(ko_attente) ko_attente,
                        SUM(ko_rejete) ko_rejete,
                        SUM(ko_hp) ko_hp
                    FROM
                    (
                        SELECT (substr(dt_recept::character varying,1,7)||'-01')::date as daty,
                        CASE WHEN etat_flux_h = 11 THEN count(distinct id_flux)::integer ELSE 0 END  as ko_mail,
                        CASE WHEN etat_flux_h = 12 THEN count(distinct id_flux)::integer ELSE 0 END as ko_pj,
                        CASE WHEN etat_flux_h = 14 THEN count(distinct id_flux)::integer ELSE 0 END as ko_fichier,
                        CASE WHEN etat_flux_h = 19 THEN count(distinct id_flux)::integer ELSE 0 END as ko_src,
                        CASE WHEN etat_flux_h = 21 THEN count(distinct id_flux)::integer ELSE 0 END as ko_definitif,
                        CASE WHEN etat_flux_h = 22 THEN count(distinct id_flux)::integer ELSE 0 END as ko_inconnu,
                        CASE WHEN etat_flux_h = 23 THEN count(distinct id_flux)::integer ELSE 0 END as ko_attente,								
                        CASE WHEN etat_flux_h = 15 THEN count(distinct id_flux)::integer ELSE 0 END as ko_rejete,								
                        CASE WHEN etat_flux_h = 13 THEN count(distinct id_flux)::integer ELSE 0 END as ko_hp							
                                        
                        FROM traitement
                        WHERE dt_recept::date between '".$sem_debut."' AND ('$sem_fin'::date+ INTERVAL '1 month'-INTERVAL '1 day') AND societe_h = ".$id_soc." AND source_h = ".$id_source." 
                        GROUP BY traitement.etat_flux_h,daty
                    ) as global
                    GROUP BY daty
                ) as data_global on data_daty.daty = data_global.daty
                LEFT JOIN
                (
                    SELECT  nb_flux,nb_abo,date_debut,semaine
                    FROM stat_hebdo 
                    where date_debut between '".$sem_debut."' AND '".$sem_fin."' and societe = ".$id_soc." and source = ".$id_source." 
                    and titre = 'GLOBAL' and rubrique ='Total'
                    
                ) as data_flux on data_daty.semaine = data_flux.semaine
                ) as tab 
                GROUP BY lib_daty,flux_total,abo_total 
                ORDER BY lib_daty asc";
        
        return $this->ged_flux->query($sql)
                                ->result_array();
    }
   
	public function get_histo_ko_journalier( $jr_debut, $jr_fin ,$id_soc){

        $sql = "SELECT 	data_daty.daty as lib_daty,
					data_daty.daty,
					CASE WHEN ko_scan is null THEN 0 ELSE ko_scan::integer END,
					CASE WHEN ko_circulaire is null THEN 0 ELSE ko_circulaire::integer END,
					CASE WHEN ko_src is null THEN 0 ELSE ko_src::integer END,	
					CASE WHEN ko_definitif is null THEN 0 ELSE ko_definitif::integer END,	
					CASE WHEN ko_inconnu is null THEN 0 ELSE ko_inconnu::integer END,	
					CASE WHEN ko_en_attente is null THEN 0 ELSE ko_en_attente::integer END,
					CASE WHEN ko_abandonne is null THEN 0 ELSE ko_abandonne::integer END,
					CASE WHEN ci_editee is null THEN 0 ELSE ci_editee::integer END,
					CASE WHEN ci_envoyee is null THEN 0 ELSE ci_envoyee::integer END,
					CASE WHEN divise is null THEN 0 ELSE divise::integer END,
					coalesce(nb_pli,0) as pli_total
				FROM
				 (
					SELECT liste_dates as daty  FROM liste_dates ('".$jr_debut."','".$jr_fin."')
				 ) as data_daty
				LEFT JOIN 
				(	SELECT
						date_courrier, 
						SUM(ko_scan) ko_scan,
						SUM(ko_circulaire) ko_circulaire,
						SUM(ko_src) ko_src,
						SUM(ko_definitif) ko_definitif,
						SUM(ko_inconnu) ko_inconnu,
						SUM(ko_en_attente) ko_en_attente,
						SUM(ko_abandonne) ko_abandonne,
						SUM(ci_editee) ci_editee,
						SUM(ci_envoyee) ci_envoyee,
						SUM(divise) divise
					FROM
					(
						SELECT dt_courrier::date as date_courrier,
						CASE WHEN statut_pli = 2 THEN count(distinct id_pli)::integer ELSE 0 END  as ko_scan,
						CASE WHEN statut_pli = 3 THEN count(distinct id_pli)::integer ELSE 0 END as ko_definitif,
						CASE WHEN statut_pli = 4 THEN count(distinct id_pli)::integer ELSE 0 END as ko_inconnu,
						CASE WHEN statut_pli = 5 THEN count(distinct id_pli)::integer ELSE 0 END as ko_src,
						CASE WHEN statut_pli = 6 THEN count(distinct id_pli)::integer ELSE 0 END as ko_en_attente,
						CASE WHEN statut_pli = 7 THEN count(distinct id_pli)::integer ELSE 0 END as ko_circulaire,
						CASE WHEN statut_pli = 8 THEN count(distinct id_pli)::integer ELSE 0 END as ko_abandonne,								
						CASE WHEN statut_pli = 9 THEN count(distinct id_pli)::integer ELSE 0 END as ci_editee,								
						CASE WHEN statut_pli = 10 THEN count(distinct id_pli)::integer ELSE 0 END as ci_envoyee	,							
						CASE WHEN statut_pli = 24 THEN count(distinct id_pli)::integer ELSE 0 END as divise						
										
						FROM histo_pli
						WHERE dt_courrier::date between '".$jr_debut."' AND '".$jr_fin."'
						AND societe_h = ".$id_soc."
						GROUP BY dt_courrier::date,statut_pli 
					) as global
					GROUP BY date_courrier
				) as data_global on data_daty.daty = data_global.date_courrier
				LEFT JOIN
				(
					SELECT  nb_pli, date_courrier::date as date_courrier
					FROM lot_mensuel
					where date_courrier between '".$jr_debut."' AND '".$jr_fin."' and societe = ".$id_soc."
					
				) as data_pli on data_daty.daty = data_pli.date_courrier
				ORDER BY data_daty.daty asc"	;
        /*echo "<pre>";
		print_r($sql);
        echo "</pre>";exit;*/
		
        return $this->ged->query($sql)
            ->result_array();
	}

	public function get_histo_ko_journalier_flux($jr_debut, $jr_fin ,$id_soc,$id_source){
        $sql = "SELECT 	
					data_daty.daty as lib_daty,
					data_daty.daty,
					CASE WHEN ko_mail is null THEN 0 ELSE ko_mail::integer END,
					CASE WHEN ko_pj is null THEN 0 ELSE ko_pj::integer END,
					CASE WHEN ko_fichier is null THEN 0 ELSE ko_fichier::integer END,	
					CASE WHEN ko_src is null THEN 0 ELSE ko_src::integer END,	
					CASE WHEN ko_definitif is null THEN 0 ELSE ko_definitif::integer END,	
					CASE WHEN ko_inconnu is null THEN 0 ELSE ko_inconnu::integer END,
					CASE WHEN ko_attente is null THEN 0 ELSE ko_attente::integer END,
					CASE WHEN ko_rejete is null THEN 0 ELSE ko_rejete::integer END,
					CASE WHEN ko_hp is null THEN 0 ELSE ko_hp::integer END,
					coalesce(nb_flux,0) as flux_total
				FROM
				 (
					SELECT liste_dates as daty  FROM liste_dates ('".$jr_debut."','".$jr_fin."')
				 ) as data_daty
				LEFT JOIN 
				(	SELECT
						date_reception,
						SUM(ko_mail) ko_mail,
						SUM(ko_pj) ko_pj,
						SUM(ko_fichier) ko_fichier,
						SUM(ko_src) ko_src,
						SUM(ko_definitif) ko_definitif,
						SUM(ko_inconnu) ko_inconnu,
						SUM(ko_attente) ko_attente,
						SUM(ko_rejete) ko_rejete,
						SUM(ko_hp) ko_hp
					FROM
					(
						SELECT dt_recept::date as date_reception,
						CASE WHEN etat_flux_h = 11 THEN count(distinct id_flux)::integer ELSE 0 END  as ko_mail,
						CASE WHEN etat_flux_h = 12 THEN count(distinct id_flux)::integer ELSE 0 END as ko_pj,
						CASE WHEN etat_flux_h = 14 THEN count(distinct id_flux)::integer ELSE 0 END as ko_fichier,
						CASE WHEN etat_flux_h = 19 THEN count(distinct id_flux)::integer ELSE 0 END as ko_src,
						CASE WHEN etat_flux_h = 21 THEN count(distinct id_flux)::integer ELSE 0 END as ko_definitif,
						CASE WHEN etat_flux_h = 22 THEN count(distinct id_flux)::integer ELSE 0 END as ko_inconnu,
						CASE WHEN etat_flux_h = 23 THEN count(distinct id_flux)::integer ELSE 0 END as ko_attente,								
						CASE WHEN etat_flux_h = 15 THEN count(distinct id_flux)::integer ELSE 0 END as ko_rejete,								
						CASE WHEN etat_flux_h = 13 THEN count(distinct id_flux)::integer ELSE 0 END as ko_hp							
										
						FROM traitement
						WHERE dt_recept::date between '".$jr_debut."' AND ('".$jr_fin."'::date+ INTERVAL '1 month'-INTERVAL '1 day') AND societe_h = ".$id_soc." AND source_h = ".$id_source." 
						GROUP BY dt_recept::date,traitement.etat_flux_h
					) as global
					GROUP BY date_reception 
				) as data_global on data_daty.daty = data_global.date_reception
				LEFT JOIN
				(
					SELECT  nb_flux,date_reception::date as date_reception 
					FROM lot_mensuel  
					where date_reception between '".$jr_debut."' AND '$jr_fin' and societe = ".$id_soc." and source = ".$id_source." 
					
				) as data_flux on data_daty.daty = data_flux.date_reception
            ";

        return $this->ged_flux->query($sql)
                                ->result_array();
    }
   
	public function get_min_max_reception_menusel ( $date_debut, $date_fin){
		
		$sql = "SELECT 
				CASE WHEN min(date_reception) < '".$date_debut."' THEN min(date_reception) ELSE '".$date_debut."'::date END date_reception_min , 
				max(date_reception) date_reception_max 
				FROM public.traitement_mensuel
				WHERE date_traitement between  '".$date_debut."' AND '".$date_fin."'  ";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_traitement_mensuel( $date_debut, $date_fin, $date_reception_min){
        
		list ($yy,$mm,$dd) = explode("-",$date_reception_min);
		$date_reception_min = $dd."/".$mm."/".$yy;
		$sql = "SELECT 	data_daty.daty,	
					data.mois_reception,
					mois_traitement,traite,en_cours,
					ko_scan,
					ko_inconnu,
					ko_def,
					ko_call,
					ko_circulaire,
					ko_en_attente,
					ko_reclammation,
					ko_litige,
					non_traite,
					nb_plis_recu,
					nb_non_type_plis_recu
					FROM
					(
						SELECT substr(liste_months::text,1,7) as daty FROM liste_months ('".$date_debut."' , '".$date_fin."') 
					) as data_daty
					LEFT JOIN
					(
						SELECT  substr(date_traitement::text,1,7) as mois_traitement,
							substr(date_reception::text,1,7) as mois_reception,
							sum(traite) as traite, 
							sum(en_cours) as en_cours, 
							sum(ko_scan) as ko_scan, 
							sum(ko_inconnu) as ko_inconnu, 
							sum(ko_def) as ko_def, 
							sum(ko_call) as ko_call, 
							sum(ko_circulaire) as ko_circulaire, 
							sum(ko_en_attente) as ko_en_attente,
							sum(ko_reclammation) as ko_reclammation,
							sum(ko_litige) as ko_litige,
							sum(non_traite) as non_traite
						  
					FROM public.traitement_mensuel
					WHERE date_traitement between '".$date_debut."' AND '".$date_fin."'
					GROUP BY substr(date_reception::text,1,7),substr(date_traitement::text,1,7)
						
					) 
					as data on data.mois_traitement = data_daty.daty 
					LEFT JOIN
					(
						SELECT  
							substr(date_reception::text,1,7) as mois_reception,
							sum(nb_plis_recu) nb_plis_recu,
							sum(nb_non_type_plis_recu) nb_non_type_plis_recu
						FROM
						(    
							 SELECT  distinct date_reception ,			
							nb_plis_recu, 
							nb_non_type_plis_recu		  
							FROM public.traitement_mensuel
							WHERE date_reception between '".$date_reception_min."' AND '".$date_fin."'
						) as reception
						GROUP BY substr(date_reception::text,1,7)
						
					) 
					as data_recep on data_recep.mois_reception = data.mois_reception 
					ORDER BY daty, mois_traitement asc";
					
		return $this->ged->query($sql)
							->result_array();	
	}
	
	public function get_min_date_courrier($date_debut, $date_fin){
		
		$where_like = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	= " dt_event::date between '".$date_debut."' AND '".$date_fin."' ";
				
        }
		$sql = "SELECT min (date_courrier::date) as date_courrier
			FROM histo_pli 
			LEFT JOIN view_flag_traitement on view_flag_traitement.id_flag_traitement = histo_pli.flag_traitement 
			LEFT JOIN view_pli on view_pli.id_pli = histo_pli.id_pli 
			LEFT JOIN view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
			WHERE ".$where_like."
			";
		return $this->ged->query($sql)
							->result_array();	
	}
	public function get_dates_ttt($date_debut, $date_fin){
		$where_like = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	= " AND date_courrier::date between '".$date_debut."' AND '".$date_fin."' ";
				
        }
		$sql = "SELECT min(dt_event::date) date_min_ttt, max(dt_event::date) date_max_ttt
				FROM view_pli 
				LEFT JOIN view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
				INNER join histo_pli on view_pli.id_pli = histo_pli.id_pli  
				WHERE 1=1 ".$where_like."
			";
		return $this->ged->query($sql)
							->result_array();
	}
	public function get_lot_mensuel($date_debut, $date_fin, $id_societe){
		$where_date = "";
		$where_soc  = "";
		if($date_debut != '' && $date_fin != ''){
				$where_date 	= " AND date_courrier::date >= '".$date_debut."' AND date_courrier::date < '".$date_fin."' ";
				$where_soc      = " AND societe = '".$id_societe."' ";
        }
		$sql = "SELECT date_courrier, nb_pli
				FROM lot_mensuel 
				WHERE 1=1 ".$where_date." ".$where_soc." 
				ORDER BY  date_courrier::date asc
			";
		return $this->ged->query($sql)
							->result_array();
	}

    public function get_lot_mensuel_flux($date_debut, $date_fin,$id_societe,$id_source){
        $where_date = "";
        $where_soc_source = "";
        if($date_debut != '' && $date_fin != ''){
            $where_date 	    = " AND date_reception::date >= '".$date_debut."' AND date_reception::date < '".$date_fin."' ";
            $where_soc_source   = " AND societe = '".$id_societe."' AND source = '".$id_source."' ";
        }
        $sql = "SELECT date_reception, nb_flux
				FROM lot_mensuel 
				WHERE 1=1 ".$where_date." ".$where_soc_source." 
				ORDER BY  date_reception::date asc
			";
        return $this->ged_flux->query($sql)
            ->result_array();
    }
	
	public function get_datatables($where_date_courrier,$where_date_courrier_lot,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatable_sql($where_date_courrier,$where_date_courrier_lot,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->ged->limit($length, $start);
        $query = $this->ged->get();
        return $query->result();
    }

    public function count_filtered($where_date_courrier,$where_date_courrier_lot,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_sql($where_date_courrier,$where_date_courrier_lot,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->ged->get();
        return $query->num_rows();
		
    }

    public function count_all($where_date_courrier,$where_date_courrier_lot)
    { 
       
        $this->ged->SELECT($this->SELECT)
                ->FROM("(SELECT 
						pli,
						traitement.id_pli, 
						typologie,
						CASE WHEN (flag_traitement = 3 ) THEN 'KO inconnu' ELSE 
						CASE WHEN (flag_traitement = 4 ) THEN 'KO scan' ELSE 
						CASE WHEN (flag_traitement = 5 ) THEN 'KO réclammation' ELSE 
						CASE WHEN (flag_traitement = 6 ) THEN 'KO litige' ELSE 
						CASE WHEN (flag_traitement = 8 ) THEN 'KO Définitif' ELSE 
						CASE WHEN (flag_traitement = 9 ) THEN 'KO Call' ELSE
						CASE WHEN (flag_traitement = 11 ) THEN 'KO Ciculaire' ELSE
						CASE WHEN (flag_traitement = 12 ) THEN 'KO en attente' ELSE ''
						END END END END END END END END type_ko ,
						flag_traitement,
						traitement,
						dt_event,
						lot_scan,
						date_courrier,
						date_numerisation,
						CASE WHEN (flag_traitement is null) or (flag_traitement = 0 ) THEN 'Non traité' ELSE 
						CASE WHEN (flag_traitement = 0 )  THEN 'En cours de typage' ELSE
						CASE WHEN (traitement.flag_traitement = 0 ) or (flag_traitement = 1 )  THEN 'Typage terminé' ELSE 
						CASE WHEN (flag_traitement = 1 )  THEN 'En cours de saisie' ELSE 
						CASE WHEN ( flag_traitement = 2 )  THEN 'En cours de controle' ELSE 
						CASE WHEN (traitement. flag_traitement = 2) THEN 'Cloturé' ELSE 
						CASE WHEN (flag_traitement = 3 or flag_traitement = 4 or flag_traitement = 5 or flag_traitement = 6 or flag_traitement = 8 or flag_traitement = 9 or flag_traitement = 11 or flag_traitement = 12)
						THEN 'Anomalie' ELSE '' END END END END END END END as statut, 				
						CASE WHEN code_type_pli is null THEN 'NT' ELSE code_type_pli END as code_type_pli,
						CASE WHEN comment is null THEN 'Non typé' ELSE comment END as comment,
						count(distinct id_document) as nb_doc
						
						FROM 
						( 
							SELECT tab1.id_pli,flag_traitement,traitement,substring(dt_event::text FROM 0 for 11) as dt_event ,pli,typologie,lot_scan,	substring(date_courrier::text FROM 0 for 11) as date_courrier ,substring(date_numerisation::text FROM 0 for 11) as date_numerisation,code_type_pli,comment
							
							FROM
							(
								SELECT histo_pli.id_pli,max(histo_pli.oid) oid
								FROM histo_pli 
								LEFT JOIN view_pli on view_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
								WHERE histo_pli.id_pli in (SELECT distinct view_pli.id_pli 
								FROM view_pli INNER join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  WHERE  ".$where_date_courrier." )
								GROUP BY histo_pli.id_pli
									
							) tab1 INNER join
							(
								SELECT histo_pli.id_pli,histo_pli.oid,histo_pli.flag_traitement,traitement,dt_event,
								view_pli.pli,view_pli.typologie,view_lot.lot_scan,
								view_lot.date_courrier,view_lot.date_numerisation,'xxxx' as code_type_pli,'no comment' as comment
								FROM histo_pli 
								LEFT JOIN view_flag_traitement on view_flag_traitement.id_flag_traitement = histo_pli.flag_traitement
								LEFT JOIN view_pli on view_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_lot on  view_lot.id_lot_numerisation = view_pli.id_lot_numerisation
								WHERE histo_pli.id_pli in (SELECT distinct view_pli.id_pli FROM view_pli INNER join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  WHERE  ".$where_date_courrier." )
								
							)tab2 on tab1.oid = tab2.oid
							union
							(
							SELECT 
								view_pli.id_pli,histo_pli.flag_traitement,'Non traité' as traitement,'0001-01-01'::text as dt_event,
								view_pli.pli,view_pli.typologie,view_lot.lot_scan,	substring(date_courrier::text FROM 0 for 11) as date_courrier ,substring(date_numerisation::text FROM 0 for 11) as date_numerisation,'yyyy' as code_type_pli,'no comment' as comment
								 FROM view_pli 
								 INNER join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  
								 LEFT JOIN histo_pli on histo_pli.id_pli = view_pli.id_pli 
								 WHERE ".$where_date_courrier." AND histo_pli.id_pli is null
								
							)
						
						)
						as traitement
						LEFT JOIN view_document on traitement.id_pli = view_document.id_pli						
						GROUP BY pli,traitement.id_pli,flag_traitement,traitement,
						lot_scan,date_courrier,date_numerisation,dt_event,typologie,code_type_pli,
						comment
						) as tab
                         ");
        return $this->ged->count_all_results();
		
    }

    public function get_datatable_sql($where_date_courrier,$where_date_courrier_lot,$post_order,$search ,$post_order_col,$post_order_dir){

         $this->ged->SELECT($this->SELECT)
                 ->FROM("");

        $i = 0;
        foreach ($this->column_search as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->like($item, $search);
                }
                ELSE
                {
                    $this->ged->or_like($item, $search);
                }

                if(count($this->column_search) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->ged->order_by($this->column_order[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order))
        {
            $order = $this->order;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
    }
	
	 public function get_list_newplis($date_debut, $date_fin){
		
		$where_like = $where_view_pli = "";
		if($date_debut != '' && $date_fin != ''){
				$where_like 	.= " date_courrier between '".$date_debut."' AND '".$date_fin."' ";
				$where_view_pli .= " view_lot.date_courrier  between '".$date_debut."' AND '".$date_fin."' ";
        }
		$sql = "SELECT 
						view_pli.id_pli,view_pli.id_pli,view_pli.typologie,'' as type_ko,
						histo_pli.flag_traitement,'Non traité' as traitement,dt_event::date,view_pli.typage_par,
						lot_scan,date_courrier::date,date_numerisation,'Non traité' as statut,
						0 code_type_pli,null as comment,null as nb_doc,null vol_produit
						 FROM view_pli 
						 INNER join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  
						 LEFT JOIN histo_pli on histo_pli.id_pli = view_pli.id_pli 
						 WHERE  ".$where_like." AND histo_pli.id_pli is null
				    ORDER BY date_courrier::date, date_numerisation, lot_scan,view_pli.id_pli asc";		
			/*echo "<pre>";	
			print_r($sql);
			echo "</pre>";exit;	*/
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_datatables_traitement($where_date_traitement,$f_clause_where,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatable_ttt_sql($where_date_traitement,$f_clause_where,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->ged->limit($length, $start);
        $query = $this->ged->get();
        return $query->result();
    }

    public function count_filtered_traitement($where_date_traitement,$f_clause_where,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_ttt_sql($where_date_traitement,$f_clause_where,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->ged->get();
        return $query->num_rows();
		
    }

    public function count_all_traitement($where_date_traitement,$f_clause_where)
    { 
      /* 
		CASE WHEN pli_en_cours_typage = 1 THEN 'En cours - En cours de typage' ELSE 
		CASE WHEN pli_typage_termine = 1 THEN 'En cours - Typage terminé'  ELSE 
		CASE WHEN pli_regroupe = 1 THEN 'En cours - Regroupé'  ELSE
		CASE WHEN pli_en_cours_saisie = 1 THEN 'En cours - En cours de saisie'  ELSE
		CASE WHEN pli_saisie_terminee = 1 THEN 'En cours - Saisie terminé' ELSE 						
		CASE WHEN pli_decoupe = 1 THEN 'En cours - Découpé' ELSE					
		CASE WHEN pli_echantillonage_encours = 1 THEN 'Echantillonage en cours' ELSE					
		CASE WHEN pli_controle_ech_termine = 1 THEN 'En cours - Echantillonage terminé' ELSE					
		CASE WHEN pli_en_cours_controle = 1 or pli_ci_saisie = 1 THEN 'En cours de controle' ELSE					
		CASE WHEN pli_validation_en_cours = 1 THEN 'En cours de validation' ELSE 						
		CASE WHEN pli_correction_en_cours = 1 THEN 'En cours de correction' ELSE
		CASE WHEN pli_correction_terminee = 1 THEN 'En cours - Correction terminée' ELSE 						
		CASE WHEN pli_validation_terminee = 1 THEN 'En cours - Validation terminée' ELSE 				
		CASE WHEN pli_matchage_termine = 1 THEN 'Matchage terminé' ELSE
		CASE WHEN pli_ferme = 1 THEN 'Fermé' ELSE
		CASE WHEN pli_ko_scan = 1 THEN 'KO scan' ELSE 						
		CASE WHEN pli_ko_saisie = 1 THEN 'KO saisie' ELSE 						
		CASE WHEN pli_hors_perimetre = 1 THEN 'Hors périmètre' ELSE 						
		CASE WHEN pli_ko_ks = 1 THEN 'KS' ELSE 						
		CASE WHEN pli_ko_ke = 1 THEN 'KE' ELSE 						
		CASE WHEN pli_ko_kd = 1 THEN 'KD' ELSE 						
		CASE WHEN pli_ci = 1 THEN 'CI' ELSE 
		CASE WHEN pli_ok = 1 THEN 'OK' ELSE 
		CASE WHEN pli_ko = 1 THEN 'KO' ELSE ''						
		END END END END END END END END END END END END END END END END END END END END END END END END END as statut,
		 */ 
        $this->ged->SELECT($this->select_ttt)
                ->FROM("(SELECT
							nom_societe,
							dt_event,
							date_courrier,
							lot_scan,
							pli,
							statut,
							libelle,
							txt_anomalie,
							libelle_motif,
							type_ko,
							typologie,
							titre_titre,
							string_agg(mode_paiement,', ') as mode_paiement,											
							string_agg(cmc7,', ') as cmc7,											
							string_agg(montant,', ') as montant,											
							string_agg(rlmc,', ') as rlmc,											
							CASE WHEN infos_datamatrix is null or infos_datamatrix = '' THEN string_agg(infos_datamatrix,'') ELSE string_agg(infos_datamatrix,', ') END as infos_datamatrix,
							string_agg(motif_rejet,', ') as motif_rejet,
							string_agg(description_rejet,', ') as description_rejet,
							titre_societe_nom,
							nb_mvt,
							id_pli
						FROM 
                        (SELECT distinct
							nom_societe,
							traitement.dt_event,
							date_courrier,
							lot_scan,
							pli,
							CASE WHEN statut = libelle THEN 'Cloturé' ELSE statut END as statut,
							libelle,
							txt_anomalie,
							libelle_motif,
							type_ko,
							typologie,
							titre_titre,
							string_agg(mode_paiement,', ') mode_paiement,											
							string_agg(cmc7,', ') cmc7,											
							string_agg(montant,', ') montant,											
							string_agg(rlmc,', ')rlmc ,											
							CASE WHEN infos_datamatrix is null or infos_datamatrix = '' THEN string_agg(infos_datamatrix,'') ELSE string_agg(infos_datamatrix,', ') END as infos_datamatrix,
							motif_rejet,
							description_rejet,
							titre_societe_nom,
							nb_mvt,
							id_pli
							FROM ( 
							 
							 
									SELECT 
										histo_pli.id_pli::text as id_pli, 
										histo_pli.flag_traitement,
										typologie as code_type_pli,
										substring(histo_pli.dt_event::text FROM 0 for 11) as dt_event,
										CASE WHEN (histo_pli.flag_traitement = 0 or histo_pli.flag_traitement is null) THEN 'Non traité' ELSE
										CASE WHEN (histo_pli.flag_traitement >= 1 AND histo_pli.flag_traitement <= 8) 
										OR (histo_pli.flag_traitement >= 10 AND histo_pli.flag_traitement <= 13 )
										OR (histo_pli.flag_traitement = ANY (ARRAY[9, 14, 15, 18, 20])) AND view_pli_avec_cheque_new.id_pli IS NOT NULL 
										OR (histo_pli.flag_traitement = ANY (ARRAY[17, 19])) 
										OR (histo_pli.flag_traitement = ANY (ARRAY[23, 22])) THEN 'En cours 'ELSE						
										CASE WHEN (histo_pli.flag_traitement = 22) THEN 'Matchage terminé' ELSE
										CASE WHEN (histo_pli.flag_traitement = 24) THEN 'Fermé' ELSE
										CASE WHEN (histo_pli.flag_traitement = 16) THEN 'KO scan' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 17) THEN 'KO saisie' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 21) THEN 'Hors périmètre' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 18 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie= 4)) THEN 'KS' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 19 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie= 5)) THEN 'KE' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 20 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 6]))) THEN 'KD' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 25 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie = 3)) THEN 'CI' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = ANY (ARRAY[9, 14, 15]) AND view_pli_avec_cheque_new.id_pli IS NULL) OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL  AND statut_saisie = 1) THEN 'OK' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = ANY (ARRAY[16, 21])) OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6])) THEN 'KO' ELSE ''						
										END END END END END END END END END END END  END END as statut,
										view_pli_avec_cheque_new.id_pli as pli_avec_chq,
										CASE WHEN (histo_pli.flag_traitement = 16) THEN 'KO scan' ELSE 
										CASE WHEN (histo_pli.flag_traitement = 17) THEN 'KO saisie' ELSE 
										CASE WHEN (histo_pli.flag_traitement = 21) THEN 'Hors périmètre' ELSE ''
										END END END type_ko,
										typage_par,
										lot_scan, 
										substring(date_courrier::text FROM 0 for 11) as date_courrier, 
										pli, 
										(typologie::text) as typologie,
										data_pli.societe as id_societe,
										societe.nom_societe,
										paiement_cheque.montant,
										paiement_cheque.cmc7,
										paiement_cheque.rlmc,
										paiement_cheque.anomalie_cheque_id,
										mode_paiement.id_mode_paiement,			
										mode_paiement.mode_paiement,			
										mode_paiement.actif,
										data_pli.infos_datamatrix,			
										data_pli.titre,			
										data_pli.societe societe_vf,
										data_pli.code_promotion,
										(data_pli.motif_rejet::text) as motif_rejet,
										data_pli.description_rejet,
										data_pli.statut_saisie,
										data_pli.motif_ko,
										data_pli.autre_motif_ko,
										data_pli.nom_circulaire,
										data_pli.fichier_circulaire,
										data_pli.message_indechiffrable,
										data_pli.dmd_kdo_fidelite_prim_suppl,
										data_pli.dmd_envoi_kdo_adrss_diff,
										data_pli.type_coupon,
										data_pli.id_erreur,
										data_pli.comm_erreur,
										data_pli.nom_orig_fichier_circulaire,
										motif_ko.libelle_motif as libelle_motif,			
										motif_ko_scan.description as txt_anomalie,
										titres.code as titre_id,
										titres.titre as titre_titre,
										titres.societe_id as titre_societe_id,
										societe.nom_societe as titre_societe_nom,
										statut_saisie.libelle,
										nb_mvt::text as nb_mvt
																			
									FROM histo_pli
									INNER JOIN f_histo_pli_by_date_by_societe(".$f_clause_where.") f ON f.id  = histo_pli.id
									LEFT JOIN data_pli on f.id_pli = data_pli.id_pli 
									INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
									INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation  = f_lot_numerisation.id_lot_numerisation
									LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
									LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli AND paiement.id_mode_paiement = 1
									LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
									LEFT JOIN motif_ko on motif_ko.id_motif = data_pli.motif_ko
									LEFT JOIN motif_ko_scan on data_pli.id_pli = motif_ko_scan.pli_id
									LEFT JOIN titres on titres.id = data_pli.titre
									LEFT JOIN societe on societe.id = titres.societe_id
									LEFT JOIN view_nb_mouvement_pli on data_pli.id_pli = view_nb_mouvement_pli.id_pli
									LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
									LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli
									WHERE  ".$where_date_traitement."
								
							) as traitement 
							GROUP BY  infos_datamatrix,txt_anomalie,nom_societe,dt_event,date_courrier,lot_scan,pli,statut,motif_rejet,description_rejet,type_ko,typologie,traitement,txt_anomalie,libelle_motif,titre_titre,titre_societe_nom,nb_mvt,id_pli,libelle) as resultat
							group by nom_societe,dt_event,date_courrier,lot_scan,pli,statut,libelle,txt_anomalie,libelle_motif,type_ko,typologie,titre_titre,infos_datamatrix,titre_societe_nom,nb_mvt,id_pli
	
						) as tab
                        ");
        return $this->ged->count_all_results();
		
    }

    public function get_datatable_ttt_sql($where_date_traitement,$f_clause_where,$post_order,$search ,$post_order_col,$post_order_dir){
			/*		
				CASE WHEN pli_en_cours_typage = 1 THEN 'En cours - En cours de typage' ELSE 
				CASE WHEN pli_typage_termine = 1 THEN 'En cours - Typage terminé'  ELSE 
				CASE WHEN pli_regroupe = 1 THEN 'En cours - Regroupé'  ELSE
				CASE WHEN pli_en_cours_saisie = 1 THEN 'En cours - En cours de saisie'  ELSE
				CASE WHEN pli_saisie_terminee = 1 THEN 'En cours - Saisie terminé' ELSE 						
				CASE WHEN pli_decoupe = 1 THEN 'En cours - Découpé' ELSE					
				CASE WHEN pli_echantillonage_encours = 1 THEN 'Echantillonage en cours' ELSE					
				CASE WHEN pli_controle_ech_termine = 1 THEN 'En cours - Echantillonage terminé' ELSE					
				CASE WHEN pli_en_cours_controle = 1 or pli_ci_saisie = 1 THEN 'En cours de controle' ELSE					
				CASE WHEN pli_validation_en_cours = 1 THEN 'En cours de validation' ELSE 						
				CASE WHEN pli_correction_en_cours = 1 THEN 'En cours de correction' ELSE
				CASE WHEN pli_correction_terminee = 1 THEN 'En cours - Correction terminée' ELSE 						
				CASE WHEN pli_validation_terminee = 1 THEN 'En cours - Validation terminée' ELSE 				
				CASE WHEN pli_matchage_termine = 1 THEN 'Matchage terminé' ELSE
				CASE WHEN pli_ferme = 1 THEN 'Fermé' ELSE
				CASE WHEN pli_ko_scan = 1 THEN 'KO scan' ELSE 						
				CASE WHEN pli_ko_saisie = 1 THEN 'KO saisie' ELSE 						
				CASE WHEN pli_hors_perimetre = 1 THEN 'Hors périmètre' ELSE 						
				CASE WHEN pli_ko_ks = 1 THEN 'KS' ELSE 						
				CASE WHEN pli_ko_ke = 1 THEN 'KE' ELSE 						
				CASE WHEN pli_ko_kd = 1 THEN 'KD' ELSE 						
				CASE WHEN pli_ci = 1 THEN 'CI' ELSE 
				CASE WHEN pli_ok = 1 THEN 'OK' ELSE 
				CASE WHEN pli_ko = 1 THEN 'KO' ELSE ''						
				END END END END END END END END END END END END END END END END END END END END END END END END END as statut,
									
			*/
         $this->ged->SELECT($this->select_ttt)
                 ->FROM("(SELECT
							nom_societe,
							dt_event,
							date_courrier,
							lot_scan,
							pli,
							statut,
							libelle,
							txt_anomalie,
							libelle_motif,
							type_ko,
							typologie,
							titre_titre,
							string_agg(mode_paiement,', ') as mode_paiement,											
							string_agg(cmc7,', ') as cmc7,											
							string_agg(montant,', ') as montant,											
							string_agg(rlmc,', ') as rlmc,											
							CASE WHEN infos_datamatrix is null or infos_datamatrix = '' THEN string_agg(infos_datamatrix,'') ELSE string_agg(infos_datamatrix,', ') END as infos_datamatrix,
							string_agg(motif_rejet,', ') as motif_rejet,
							string_agg(description_rejet,', ') as description_rejet,
							titre_societe_nom,
							nb_mvt,
							id_pli
						FROM 
						(
						SELECT distinct
							nom_societe,
							traitement.dt_event,
							date_courrier,
							lot_scan,
							pli,
							CASE WHEN statut = libelle THEN 'Cloturé' ELSE statut END as statut,
							libelle,
							txt_anomalie,
							libelle_motif,
							type_ko,
							typologie,
							titre_titre,
							string_agg(mode_paiement,', ') mode_paiement,											
							string_agg(cmc7,', ') cmc7,											
							string_agg(montant,', ') montant,											
							string_agg(rlmc,', ')rlmc ,											
							CASE WHEN infos_datamatrix is null or infos_datamatrix = '' THEN string_agg(infos_datamatrix,'') ELSE string_agg(infos_datamatrix,', ') END as infos_datamatrix,
							motif_rejet,
							description_rejet,
							titre_societe_nom,
							nb_mvt,
							id_pli
							FROM ( 
							 
							 
									SELECT 
										histo_pli.id_pli::text as id_pli, 
										histo_pli.flag_traitement,
										typologie.id as code_type_pli,
										substring(histo_pli.dt_event::text FROM 0 for 11) as dt_event,
										CASE WHEN (histo_pli.flag_traitement = 0 or histo_pli.flag_traitement is null) THEN 'Non traité' ELSE
										CASE WHEN (histo_pli.flag_traitement >= 1 AND histo_pli.flag_traitement <= 8) 
										OR (histo_pli.flag_traitement >= 10 AND histo_pli.flag_traitement <= 13 )
										OR (histo_pli.flag_traitement = ANY (ARRAY[9, 14, 15, 18, 20])) AND view_pli_avec_cheque_new.id_pli IS NOT NULL 
										OR (histo_pli.flag_traitement = ANY (ARRAY[17, 19])) 
										OR (histo_pli.flag_traitement = ANY (ARRAY[23, 22])) THEN 'En cours 'ELSE						
										CASE WHEN (histo_pli.flag_traitement = 22) THEN 'Matchage terminé' ELSE
										CASE WHEN (histo_pli.flag_traitement = 24) THEN 'Fermé' ELSE
										CASE WHEN (histo_pli.flag_traitement = 16) THEN 'KO scan' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 17) THEN 'KO saisie' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 21) THEN 'Hors périmètre' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 18 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie= 4)) THEN 'KS' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 19 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie= 5)) THEN 'KE' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 20 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 6]))) THEN 'KD' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = 25 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie = 3)) THEN 'CI' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = ANY (ARRAY[9, 14, 15]) AND view_pli_avec_cheque_new.id_pli IS NULL) OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL  AND statut_saisie = 1) THEN 'OK' ELSE 						
										CASE WHEN (histo_pli.flag_traitement = ANY (ARRAY[16, 21])) OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6])) THEN 'KO' ELSE ''						
										END END END END END END END END END END END  END END as statut,
										view_pli_avec_cheque_new.id_pli as pli_avec_chq,
										CASE WHEN (histo_pli.flag_traitement = 16) THEN 'KO scan' ELSE 
										CASE WHEN (histo_pli.flag_traitement = 17) THEN 'KO saisie' ELSE 
										CASE WHEN (histo_pli.flag_traitement = 21) THEN 'Hors périmètre' ELSE ''
										END END END type_ko,
										typage_par,
										lot_scan, 
										substring(date_courrier::text FROM 0 for 11) as date_courrier, 
										pli, 
										(typologie.typologie::text) as typologie,
										data_pli.societe as id_societe,
										societe.nom_societe,
										paiement_cheque.montant,
										paiement_cheque.cmc7,
										paiement_cheque.rlmc,
										paiement_cheque.anomalie_cheque_id,
										mode_paiement.id_mode_paiement,			
										mode_paiement.mode_paiement,			
										mode_paiement.actif,
										data_pli.infos_datamatrix,			
										data_pli.titre,			
										data_pli.societe societe_vf,
										data_pli.code_promotion,
										(data_pli.motif_rejet::text) as motif_rejet,
										data_pli.description_rejet,
										data_pli.statut_saisie,
										data_pli.motif_ko,
										data_pli.autre_motif_ko,
										data_pli.nom_circulaire,
										data_pli.fichier_circulaire,
										data_pli.message_indechiffrable,
										data_pli.dmd_kdo_fidelite_prim_suppl,
										data_pli.dmd_envoi_kdo_adrss_diff,
										data_pli.type_coupon,
										data_pli.id_erreur,
										data_pli.comm_erreur,
										data_pli.nom_orig_fichier_circulaire,
										motif_ko.libelle_motif as libelle_motif,			
										motif_ko_scan.description as txt_anomalie,
										titres.code as titre_id,
										titres.titre as titre_titre,
										titres.societe_id as titre_societe_id,
										societe.nom_societe as titre_societe_nom,
										statut_saisie.libelle,
										nb_mvt::text as nb_mvt
																			
									FROM histo_pli
									INNER JOIN f_histo_pli_by_date_by_societe(".$f_clause_where.") f ON f.id  = histo_pli.id
									LEFT JOIN data_pli on f.id_pli = data_pli.id_pli 
									INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
									INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation  = f_lot_numerisation.id_lot_numerisation
									LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
									LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli AND paiement.id_mode_paiement = 1
									LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
									LEFT JOIN motif_ko on motif_ko.id_motif = data_pli.motif_ko
									LEFT JOIN motif_ko_scan on data_pli.id_pli = motif_ko_scan.pli_id
									LEFT JOIN titres on titres.id = data_pli.titre
									LEFT JOIN societe on societe.id = titres.societe_id
									LEFT JOIN view_nb_mouvement_pli on data_pli.id_pli = view_nb_mouvement_pli.id_pli
									LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
									LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli
									LEFT JOIN typologie on typologie.id = f_pli.typologie
									WHERE  ".$where_date_traitement."
								
							) as traitement 
							GROUP BY  infos_datamatrix,txt_anomalie,nom_societe,dt_event,date_courrier,lot_scan,pli,statut,motif_rejet,description_rejet,type_ko,typologie,traitement,txt_anomalie,libelle_motif,titre_titre,titre_societe_nom,nb_mvt,id_pli,libelle
							) as resultat group by nom_societe,dt_event,date_courrier,lot_scan,pli,statut,libelle,txt_anomalie,libelle_motif,type_ko,typologie,titre_titre,infos_datamatrix,titre_societe_nom,nb_mvt,id_pli

						) as tab
					 ");

        $i = 0;
        foreach ($this->column_search_ttt as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->like($item, $search);
                }
                ELSE
                {
                    $this->ged->or_like($item, $search);
                }

                if(count($this->column_search_ttt) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->ged->order_by($this->column_order_ttt[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_ttt))
        {
            $order = $this->order_ttt;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
    }
	
	public function get_traitement_excel($where_date_traitement, $f_clause_where)
    { 
     
         $sql = "SELECT
				nom_societe,dt_event,date_courrier,lot_scan,pli,statut,libelle,txt_anomalie,libelle_motif,type_ko,typologie,titre_titre,string_agg(mode_paiement,', ') as mode_paiement,											
				string_agg(cmc7,', ') as cmc7,											
				string_agg(montant,', ') as montant,											
				string_agg(rlmc,', ') as rlmc,											
				CASE WHEN infos_datamatrix is null or infos_datamatrix = '' THEN string_agg(infos_datamatrix,'') ELSE string_agg(infos_datamatrix,', ') END as infos_datamatrix,
				string_agg(motif_rejet,', ') as motif_rejet,
				string_agg(description_rejet,', ') as description_rejet,
				titre_societe_nom,nb_mvt,autre_motif_ko,id_pli
				FROM 
				(SELECT distinct
					nom_societe,
					traitement.dt_event,
					date_courrier,
					lot_scan,
					pli,
					CASE WHEN statut = libelle THEN 'Cloturé' ELSE statut END as statut,
					libelle,
					txt_anomalie,
					libelle_motif,
					type_ko,
					typologie,
					titre_titre,
					string_agg(mode_paiement,', ') mode_paiement,											
					string_agg(cmc7,', ') cmc7,											
					string_agg(montant,', ') montant,											
					string_agg(rlmc,', ')rlmc ,											
					CASE WHEN infos_datamatrix is null or infos_datamatrix = '' THEN string_agg(infos_datamatrix,'') ELSE string_agg(infos_datamatrix,', ') END as infos_datamatrix,
					motif_rejet,
					description_rejet,
					titre_societe_nom,
					nb_mvt,
					autre_motif_ko,
					id_pli
					FROM ( 
					 
					 
							SELECT 
								histo_pli.id_pli::text as id_pli, 
								histo_pli.flag_traitement,
								typologie.id as code_type_pli,
								substring(histo_pli.dt_event::text FROM 0 for 11) as dt_event,
								CASE WHEN (histo_pli.flag_traitement = 0 or histo_pli.flag_traitement is null) THEN 'Non traité' ELSE
								CASE WHEN (histo_pli.flag_traitement >= 1 AND histo_pli.flag_traitement <= 8) 
								OR (histo_pli.flag_traitement >= 10 AND histo_pli.flag_traitement <= 13 )
								OR (histo_pli.flag_traitement = ANY (ARRAY[9, 14, 15, 18, 20])) AND view_pli_avec_cheque_new.id_pli IS NOT NULL 
								OR (histo_pli.flag_traitement = ANY (ARRAY[17, 19])) 
								OR (histo_pli.flag_traitement = ANY (ARRAY[23, 22])) THEN 'En cours 'ELSE						
								CASE WHEN (histo_pli.flag_traitement = 22) THEN 'Matchage terminé' ELSE
								CASE WHEN (histo_pli.flag_traitement = 24) THEN 'Fermé' ELSE
								CASE WHEN (histo_pli.flag_traitement = 16) THEN 'KO scan' ELSE 						
								CASE WHEN (histo_pli.flag_traitement = 17) THEN 'KO saisie' ELSE 						
								CASE WHEN (histo_pli.flag_traitement = 21) THEN 'Hors périmètre' ELSE 						
								CASE WHEN (histo_pli.flag_traitement = 18 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie= 4)) THEN 'KS' ELSE 						
								CASE WHEN (histo_pli.flag_traitement = 19 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie= 5)) THEN 'KE' ELSE 						
								CASE WHEN (histo_pli.flag_traitement = 20 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 6]))) THEN 'KD' ELSE 						
								CASE WHEN (histo_pli.flag_traitement = 25 OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie = 3)) THEN 'CI' ELSE 						
								CASE WHEN (histo_pli.flag_traitement = ANY (ARRAY[9, 14, 15]) AND view_pli_avec_cheque_new.id_pli IS NULL) OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL  AND statut_saisie = 1) THEN 'OK' ELSE 						
								CASE WHEN (histo_pli.flag_traitement = ANY (ARRAY[16, 21])) OR (histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6])) THEN 'KO' ELSE ''						
								END END END END END END END END END END END  END END as statut,
								view_pli_avec_cheque_new.id_pli as pli_avec_chq,
								CASE WHEN (histo_pli.flag_traitement = 16) THEN 'KO scan' ELSE 
								CASE WHEN (histo_pli.flag_traitement = 17) THEN 'KO saisie' ELSE 
								CASE WHEN (histo_pli.flag_traitement = 21) THEN 'Hors périmètre' ELSE ''
								END END END type_ko,
								typage_par,
								lot_scan, 
								substring(date_courrier::text FROM 0 for 11) as date_courrier, 
								pli, 
								typologie.typologie as typologie,
								data_pli.societe as id_societe,
								societe.nom_societe,
								paiement_cheque.montant,
								paiement_cheque.cmc7,
								paiement_cheque.rlmc,
								paiement_cheque.anomalie_cheque_id,
								mode_paiement.id_mode_paiement,			
								mode_paiement.mode_paiement,			
								mode_paiement.actif,
								data_pli.infos_datamatrix,			
								data_pli.titre,			
								data_pli.societe societe_vf,
								data_pli.code_promotion,
								(data_pli.motif_rejet::text) as motif_rejet,
								data_pli.description_rejet,
								data_pli.statut_saisie,
								data_pli.motif_ko,
								data_pli.autre_motif_ko,
								data_pli.nom_circulaire,
								data_pli.fichier_circulaire,
								data_pli.message_indechiffrable,
								data_pli.dmd_kdo_fidelite_prim_suppl,
								data_pli.dmd_envoi_kdo_adrss_diff,
								data_pli.type_coupon,
								data_pli.id_erreur,
								data_pli.comm_erreur,
								data_pli.nom_orig_fichier_circulaire,
								motif_ko.libelle_motif as libelle_motif,			
								motif_ko_scan.description as txt_anomalie,
								titres.code as titre_id,
								titres.titre as titre_titre,
								titres.societe_id as titre_societe_id,
								societe.nom_societe as titre_societe_nom,
								statut_saisie.libelle,
								nb_mvt::text as nb_mvt
																	
							FROM histo_pli
							INNER JOIN f_histo_pli_by_date_by_societe(".$f_clause_where.") f ON f.id  = histo_pli.id
							LEFT JOIN data_pli on f.id_pli = data_pli.id_pli 
							INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation  = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
							LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli AND paiement.id_mode_paiement = 1
							LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
							LEFT JOIN motif_ko on motif_ko.id_motif = data_pli.motif_ko
							LEFT JOIN motif_ko_scan on data_pli.id_pli = motif_ko_scan.pli_id
							LEFT JOIN titres on titres.id = data_pli.titre
							LEFT JOIN societe on societe.id = data_pli.societe
							LEFT JOIN view_nb_mouvement_pli on data_pli.id_pli = view_nb_mouvement_pli.id_pli
							LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
							LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli
							LEFT JOIN typologie on typologie.id = f_pli.typologie
							WHERE  ".$where_date_traitement."
					) as traitement 
					GROUP BY  infos_datamatrix,txt_anomalie,nom_societe,dt_event,date_courrier,lot_scan,pli,statut,motif_rejet,description_rejet,type_ko,typologie,traitement,txt_anomalie,libelle_motif,titre_titre,titre_societe_nom,nb_mvt,autre_motif_ko,id_pli,libelle
					) as resultat group by nom_societe,dt_event,date_courrier,lot_scan,pli,statut,libelle,txt_anomalie,libelle_motif,type_ko,typologie,titre_titre,infos_datamatrix,titre_societe_nom,nb_mvt,autre_motif_ko,id_pli
					ORDER BY nom_societe,dt_event,date_courrier ASC
			";
			/*echo "<pre>";
			print_r($sql);
			echo "</pre>";exit;
			*/
			  return $this->ged->query($sql)
					->result_array();
		
    }

	public function get_pli_restant_a_valider (){
		
		$sql = "SELECT count(id_pli) as nb FROM pli
			WHERE flag_validation = 1 
			AND flag_ctrl = 0 
			AND valideur IS NULL 
			AND flag_traitement = 2";
		return $this->base_64->query($sql)
							->result_array();
	}
	public function get_pli_restant_a_corriger (){
		
		$sql = "SELECT count(id_pli) as nb  FROM pli 
				WHERE flag_validation = 2 
				AND flag_ctrl = 0 
				AND valideur IS NULL 
				AND flag_traitement = 2
				AND flag_avec_bdc = 1 ";
		return $this->base_64->query($sql)
							->result_array();
	}
	public function get_societe (){
		
		$sql = "SELECT id, nom_societe FROM societe ORDER BY id asc
				";
		return $this->ged->query($sql)
							->result_array();
	}

    public function get_source (){

        $sql = "SELECT id_source, source FROM source WHERE actif = 1 ORDER BY id_source asc
				";
        return $this->ged_flux->query($sql)
            ->result_array();
    }

   public function get_categorie (){

        $sql = "SELECT id_categorie, libelle FROM categorie_typologie ORDER BY id_categorie asc
				";
        return $this->ged->query($sql)
            ->result_array();
    }
	public function get_traitement_typologie($date_debut, $date_fin,$id_societe){
		
		$clauseWhere = $clauseWhereSociete = "";
		if($date_debut != '' && $date_fin != ''){
				$clauseWhere 	.= "AND (dt_event::date between '".$date_debut."' AND '".$date_fin."') ";
		  }
		if($id_societe != ''){
				$clauseWhereSociete .= " AND f_pli.societe = '".$id_societe."' ";	
		}
		
		$sql_old = "
		SELECT date_courrier,typologie,dt_event,SUM(nb_mvt) as nb_mvt,SUM(nb_pli) as nb_pli 
		FROM (
				select f_lot_numerisation.date_courrier::date,typologie.typologie,histo.dt_event::date,mvt_nbr as nb_mvt,count(f_pli.id_pli) AS nb_pli
				from f_pli 
				left join f_lot_numerisation on f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
				left join data_pli on data_pli.id_pli = f_pli.id_pli
				left join societe on societe.id = f_pli.societe
				left join typologie on typologie.id = f_pli.typologie
				inner join
					(
							SELECT id, view_pli_traitement.id_pli, view_pli_traitement.flag_traitement, dt_event
							FROM f_pli
							INNER JOIN f_histo_pli_by_date_event('".$date_debut."' , '".$date_fin."') view_pli_traitement ON view_pli_traitement.id_pli = f_pli.id_pli
							where (view_pli_traitement.flag_traitement in (5,6,7,8,9,11,13,14) AND f_pli.statut_saisie in (1,8,10,24))
							".$clauseWhere."
					) histo on histo.id_pli = f_pli.id_pli
				where f_pli.flag_traitement in (5,6,7,8,9,11,13,14) AND f_pli.statut_saisie in (1,8,10,24) ".$clauseWhereSociete."
				GROUP BY f_pli.id_pli,f_lot_numerisation.date_courrier,typologie.typologie,histo.dt_event,mvt_nbr
			) as typo
			GROUP BY date_courrier,typologie,dt_event";
		
		//lenteur de req pour sql_omd alors remplacer par sql
		$sql = "
		select date_courrier,typologie,dt_event,SUM(nb_mvt) as nb_mvt,SUM(nb_pli) as nb_pli  from
				(
						select * from (
							select f_lot_numerisation.date_courrier::date,typologie.typologie,mvt_nbr as nb_mvt,count(f_pli.id_pli) AS nb_pli,f_pli.id_pli
											from f_pli 
											left join f_lot_numerisation on f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
											left join data_pli on data_pli.id_pli = f_pli.id_pli
											left join societe on societe.id = f_pli.societe
											left join typologie on typologie.id = f_pli.typologie
							where f_pli.flag_traitement in (5,6,7,8,9,11,13,14) AND f_pli.statut_saisie in (1,8,10,24)   $clauseWhereSociete 
							GROUP BY f_pli.id_pli,f_lot_numerisation.date_courrier,typologie.typologie,mvt_nbr order by id_pli
						) as res
				inner join
				(

								SELECT 
											id, view_pli_traitement.id_pli, view_pli_traitement.flag_traitement, dt_event 
											from f_histo_pli_by_date_event('".$date_debut."' , '".$date_fin."') view_pli_traitement
											inner join f_pli on f_pli.id_pli = view_pli_traitement.id_pli
											where view_pli_traitement.flag_traitement in(5,6,7,8,9,11,13,14) AND f_pli.statut_saisie in (1,8,10,24) order by id_pli

				)hst on hst.id_pli = res.id_pli
				)as typo
		GROUP BY date_courrier,typologie,dt_event
		";
		
		// echo "<pre>";
		// print_r($sql);
		// echo "</pre>";exit;
		
		return $this->ged->query($sql)
							->result_array();
	}
	

	public function get_traitement_bydate($date_debut, $date_fin,$id_societe){
		
		$clauseWhere = $clauseWhereCmplt = "";
		if($date_debut != '' && $date_fin != ''){
				$clauseWhere 	.= " histo_pli.dt_event::date between '".$date_debut."' AND '".$date_fin."' ";
				//$clauseWhereCmplt 	.= " date_courrier::date between '".$date_debut."' AND '".$date_fin."' ";
		  }
		if($id_societe != ''){
				$clauseWhere .= " AND data_pli.societe = '".$id_societe."' ";
				//$clauseWhereCmplt .= " AND view_pli.societe = '".$id_societe."' ";
				
        }
		
		$sql = "SELECT	
					dt_event,
					date_courrier,
					CASE WHEN SUM(pli_traite) is null THEN 0 ELSE SUM(pli_traite) END as traite,
					CASE WHEN SUM(ok) is null THEN 0 ELSE SUM(ok) END as ok,
					CASE WHEN SUM(ko) is null THEN 0 ELSE SUM(ko) END as ko,
					CASE WHEN SUM(ko_scan) is null THEN 0 ELSE SUM(ko_scan) END as ko_scan,
					CASE WHEN SUM(hors_perimetre) is null THEN 0 ELSE SUM(hors_perimetre) END as hors_perimetre,
					CASE WHEN SUM(ko_ks) is null THEN 0 ELSE SUM(ko_ks) END as ko_ks,
					CASE WHEN SUM(ko_ke) is null THEN 0 ELSE SUM(ko_ke) END as ko_ke,
					CASE WHEN SUM(ko_def) is null THEN 0 ELSE SUM(ko_def) END as ko_def,
					CASE WHEN SUM(ko_circulaire) is null THEN 0 ELSE SUM(ko_circulaire) END as ko_circulaire,
					CASE WHEN SUM(ok_advantage) is null THEN 0 ELSE SUM(ok_advantage) END as ok_advantage,
					sum(nb_mvt) nb_mvt
					
				FROM
				(
					SELECT 
						 id_pli,id_pli, flag_traitement,typologie,
						 CASE WHEN (flag_traitement = ANY (ARRAY[9, 14, 15]) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL  AND statut_saisie = 1) THEN 1 ELSE 0 END as ok,
						 CASE WHEN (flag_traitement = ANY (ARRAY[16, 21])) OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6])) THEN count(distinct id_pli) ELSE 0 END as ko, 						 
						 CASE WHEN (flag_traitement = 16)  THEN count(distinct id_pli) ELSE 0 END as ko_scan,						 
						 CASE WHEN (flag_traitement = 21) THEN count(id_pli) ELSE 0 END as hors_perimetre,
						 CASE WHEN (flag_traitement = 18 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 4)) THEN 1 ELSE 0 END as ko_ks,
						 CASE WHEN (flag_traitement = 19 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie= 5)) THEN 1 ELSE 0 END as ko_ke,
						 CASE WHEN (flag_traitement = 20 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = ANY (ARRAY[2, 6]))) THEN 1 ELSE 0 END as ko_def,
						 CASE WHEN (flag_traitement = 25 OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND statut_saisie = 3)) THEN 1 ELSE 0 END as ko_circulaire,						 
						 CASE WHEN (date_saisie_advantage between '".$date_debut."' AND '".$date_fin."') AND id_lot_saisie is not null AND flag_traitement != 26 THEN count(distinct id_pli) ELSE 0 END as ok_advantage,
						 
						 CASE WHEN  (flag_traitement = ANY (ARRAY[9, 14, 15, 18]) AND flag_chq IS NULL) OR (flag_traitement = 26 AND flag_chq IS NOT NULL AND dt_event between '".$date_debut."' AND '".$date_fin."' ) OR (flag_traitement = 20 AND dt_event between '".$date_debut."' AND '".$date_fin."') OR (flag_traitement = 25 AND dt_event between '".$date_debut."' AND '".$date_fin."') THEN count(distinct id_pli) ELSE 0 END as pli_traite,
						 CASE WHEN flag_traitement != 26 THEN nb_mvt ELSE 0 END nb_mvt,
						 dt_event,
						 date_courrier
					FROM
					(
					
						SELECT distinct
						histo_pli.dt_event::date,		
						f_lot_numerisation.date_courrier::date,
						histo_pli.id_pli, 
						histo_pli.flag_traitement,
						typologie as code_type_pli,
						pli,
						typologie,						
						id_lot_saisie,
						data_pli.dt_enregistrement::date AS date_saisie_advantage,
						nb_mvt,
						view_pli_avec_cheque_new.id_pli AS flag_chq,
						data_pli.statut_saisie,
						data_pli.societe
						FROM histo_pli
						INNER JOIN view_pli_traitement ON view_pli_traitement.id  = histo_pli.id
						INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
						INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation  = f_lot_numerisation.id_lot_numerisation
						LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli 
						LEFT JOIN view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = data_pli.id_pli 
						LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli 
						WHERE  ".$clauseWhere." 								
					) as traitement
					GROUP BY id_pli,pli,flag_traitement,typologie,nb_mvt,dt_event,date_courrier,id_lot_saisie,date_saisie_advantage,flag_chq,statut_saisie

				) as result GROUP BY dt_event,date_courrier
				ORDER BY dt_event,date_courrier asc

		";	
		/*echo "<pre>";
		print_r($sql);
		echo "</pre>";exit;
		*/
		
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_suivi_solde($date_j, $id_societe){
      $where_soc = "";
	  if($id_societe != '') $where_soc = " AND societe = '".$id_societe."'";
	  $sql = "	SELECT
					societe,nom_societe, date_courrier,pli_total,pli_saisie_adv_jmoins,stock_initial,pli_saisie_adv_j,stock_final,
					pli_saisie_adv_j_ok,pli_saisie_adv_j_ci,pli_saisie_adv_j_ko
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_courrier,pli_total/*, pli_en_prod_j*/, 			
					coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins, 
					coalesce(pli_total,0)-coalesce(pli_saisie_adv_jmoins,0) as stock_initial, 
					coalesce(pli_saisie_adv_j,0) as pli_saisie_adv_j,
					coalesce(pli_total,0)-coalesce(pli_saisie_adv_j,0)-coalesce(pli_saisie_adv_jmoins,0) as stock_final,
					coalesce(pli_saisie_adv_j_ok,0) as pli_saisie_adv_j_ok,
					coalesce(pli_saisie_adv_j_ci,0) as pli_saisie_adv_j_ci,
					coalesce(pli_saisie_adv_j_ko, 0) as pli_saisie_adv_j_ko
					FROM 
					(
						SELECT date_courrier::date as date_courrier,  count(id_pli) as pli_total ,societe
						FROM view_pli_stat 
						WHERE concat(date_courrier::date,societe)::text
							in (SELECT concat(date_courrier::date,societe)
							FROM  view_pli_stat
							WHERE 
								dt_event::date = '".$date_j."' ".$where_soc." 
							GROUP BY date_courrier,societe
							)
						GROUP BY date_courrier::date,societe
					
					) as data_total

					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,view_pli_stat.societe, nom_societe, COUNT(id_pli) as pli_en_prod_j
						FROM  view_pli_stat
						WHERE 
							dt_event::date = '".$date_j."' ".$where_soc." 
						GROUP BY societe,nom_societe, date_courrier
					
					)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier AND data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						
						Select adv_total.date_courrier,adv_total.societe,
							coalesce(pli_saisie_adv_j,0) pli_saisie_adv_j,
							coalesce(pli_saisie_adv_j_ok,0) pli_saisie_adv_j_ok,
							coalesce(pli_saisie_adv_j_ci,0) pli_saisie_adv_j_ci,
							coalesce(pli_saisie_adv_j_ko,0) pli_saisie_adv_j_ko
						FROM	
						(
						SELECT date_courrier::date as date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_j
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
						AND id_lot_saisie is not null
						GROUP BY date_courrier,societe
						)as adv_total
						LEFT JOIN
						(
						SELECT date_courrier::date as date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_j_ok
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
						AND id_lot_saisie is not null AND statut_saisie in (1)
						GROUP BY date_courrier,societe
						) as adv_ok on adv_ok.date_courrier = adv_total.date_courrier AND adv_ok.societe = adv_total.societe
						LEFT JOIN(
						SELECT date_courrier::date,societe, count(id_lot_saisie) as pli_saisie_adv_j_ci
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc."  
						AND id_lot_saisie is not null AND statut_saisie in (3)
						GROUP BY date_courrier,societe
						) as adv_ci on adv_ci.date_courrier = adv_total.date_courrier AND adv_ci.societe = adv_total.societe
						LEFT JOIN(
						SELECT date_courrier::date,societe, count(id_lot_saisie) as pli_saisie_adv_j_ko
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc."  
						AND id_lot_saisie is not null AND statut_saisie not in (1,3)
						GROUP BY date_courrier,societe
						)adv_ko on adv_ko.date_courrier = adv_total.date_courrier AND adv_ko.societe = adv_total.societe
					
					)as data_saisie_advantage on data_saisie_advantage.date_courrier = data_ttt_j.date_courrier AND data_saisie_advantage.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_jmoins
						FROM view_pli_stat
						WHERE date_saisie_advantage < '".$date_j."' ".$where_soc." 
						AND id_lot_saisie is not null
						GROUP BY date_courrier,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier AND data_saisie_advantage1.societe = data_ttt_j.societe
					
					WHERE pli_en_prod_j is not null
				)as data
				ORDER BY societe,date_courrier  asc
			";
					 
				return $this->ged->query($sql)
							->result_array();

    }
	
	public function get_suivi_solde_courrier($date_deb, $date_fin, $date_j, $id_societe){
      $where_soc = "";
	  if($id_societe != '') $where_soc = " AND pli.societe = '".$id_societe."'";
	  
				$sql = "SELECT
							societe,nom_societe, date_courrier,pli_total,pli_saisie_adv_jmoins,stock_initial,pli_saisie_adv_j,stock_final,
							pli_saisie_adv_j_ok,pli_saisie_adv_j_ci,pli_saisie_adv_j_ko
						FROM
						(

							SELECT data_total.societe,nom_societe, data_total.date_courrier,pli_total/*, pli_en_prod_j*/, 			
							coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins, 
							coalesce(pli_total,0)-coalesce(pli_saisie_adv_jmoins,0) as stock_initial, 
							coalesce(pli_saisie_adv_j,0) as pli_saisie_adv_j,
							coalesce(pli_total,0)-coalesce(pli_saisie_adv_j,0)-coalesce(pli_saisie_adv_jmoins,0) as stock_final,
							coalesce(pli_saisie_adv_j_ok,0) as pli_saisie_adv_j_ok,
							coalesce(pli_saisie_adv_j_ci,0) as pli_saisie_adv_j_ci,
							coalesce(pli_saisie_adv_j_ko, 0) as pli_saisie_adv_j_ko
							FROM 
							(
								SELECT date_courrier::date as date_courrier ,pli.societe, COUNT(pli.id_pli) as pli_total
								FROM pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN view_societe ON view_societe.id = pli.societe
								WHERE 
									date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
								GROUP BY pli.societe,nom_societe, date_courrier
							) as data_total

							LEFT JOIN
							(
								SELECT date_courrier::date as date_courrier ,pli.societe, nom_societe, COUNT(pli.id_pli) as pli_en_prod_j
								FROM pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN view_societe ON view_societe.id = pli.societe
								WHERE 
									date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
								GROUP BY pli.societe,nom_societe, date_courrier
							
							)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier AND data_total.societe = data_ttt_j.societe
							LEFT JOIN
							(
								
								Select adv_total.date_courrier,adv_total.societe,
									coalesce(pli_saisie_adv_j,0) pli_saisie_adv_j,
									coalesce(pli_saisie_adv_j_ok,0) pli_saisie_adv_j_ok,
									coalesce(pli_saisie_adv_j_ci,0) pli_saisie_adv_j_ci,
									coalesce(pli_saisie_adv_j_ko,0) pli_saisie_adv_j_ko
								FROM	
								(
									SELECT date_courrier::date as date_courrier,pli.societe, count(distinct pli.id_pli) as pli_saisie_adv_j
									FROM pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
									WHERE (dt_enregistrement::date =  '".$date_j."' or dt_batch::date =  '".$date_j."') ".$where_soc."  
									AND flag_saisie = 1
									GROUP BY date_courrier,pli.societe
								)as adv_total
								LEFT JOIN
								(
									SELECT date_courrier::date as date_courrier,pli.societe, count(distinct pli.id_pli) as pli_saisie_adv_j_ok
									FROM pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
									WHERE (dt_enregistrement::date =  '".$date_j."' or dt_batch::date =  '".$date_j."') ".$where_soc."  
									AND flag_saisie = 1 and pli.statut_saisie = 1
									GROUP BY date_courrier,pli.societe
								) as adv_ok on adv_ok.date_courrier = adv_total.date_courrier AND adv_ok.societe = adv_total.societe
								LEFT JOIN(
									SELECT date_courrier::date,pli.societe, count(distinct pli.id_pli) as pli_saisie_adv_j_ci
									FROM pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
									WHERE (dt_enregistrement::date =  '".$date_j."' or dt_batch::date =  '".$date_j."') ".$where_soc."  
									AND flag_saisie = 1 and pli.statut_saisie in(7,9,10)
									GROUP BY date_courrier,pli.societe
								) as adv_ci on adv_ci.date_courrier = adv_total.date_courrier AND adv_ci.societe = adv_total.societe
								LEFT JOIN(
									SELECT date_courrier::date,pli.societe, count(distinct pli.id_pli) as pli_saisie_adv_j_ko
									FROM pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
									WHERE (dt_enregistrement::date =  '".$date_j."' or dt_batch::date =  '".$date_j."') ".$where_soc."  
									AND flag_saisie = 1 and pli.statut_saisie in(2,3,4,5,6,8)
									GROUP BY date_courrier,pli.societe
								)adv_ko on adv_ko.date_courrier = adv_total.date_courrier AND adv_ko.societe = adv_total.societe 
							
							)as data_saisie_advantage on data_saisie_advantage.date_courrier = data_ttt_j.date_courrier AND data_saisie_advantage.societe = data_ttt_j.societe
							LEFT JOIN
							(
								SELECT date_courrier,pli.societe, count(distinct pli.id_pli) as pli_saisie_adv_jmoins
								FROM pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
								WHERE (dt_enregistrement::date < '".$date_j."' or dt_batch::date < '".$date_j."') 
								AND date_courrier::date between '".$date_deb."' AND '".$date_fin."'".$where_soc." 
								AND flag_saisie = 1
								GROUP BY date_courrier,pli.societe
								
								
							)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier AND data_saisie_advantage1.societe = data_ttt_j.societe
							
							WHERE pli_en_prod_j is not null
						)as data
						ORDER BY societe,date_courrier  asc
					";
				
				
				return $this->base_64->query($sql)
							->result_array();

    }
	
	public function get_reception_anomalie($date_j, $id_societe){
		 if($id_societe != '') $where_soc = " AND f_pli.societe = '".$id_societe."'";
		$sql = "
				SELECT coalesce(anomalie,0) as anomalie,
						coalesce(ci,0) as ci,
						coalesce(ko_scan,0) as ko_scan,
						coalesce(ko_def,0) as ko_def,
						coalesce(ko_inconnu,0) as ko_inconnu,
						coalesce(ko_ks,0) as ko_ks,
						coalesce(ko_ke,0) as ko_ke,
						coalesce(ko_circulaire,0) as ko_circulaire,
						coalesce(ko_abondonne,0) as ko_abondonne,
						coalesce(divise,0) as divise,
						coalesce(ci_editee,0) as ci_editee,
						coalesce(ci_envoyee,0) as ci_envoyee,
						coalesce(ok_circ,0) as ok_circ,
						coalesce(ko_bayard,0) as ko_bayard,
						data_soc.id as societe,
						CASE WHEN date_courrier is null THEN '".$date_j."' ::date ELSE date_courrier END as date_courrier
					FROM 
					(
						SELECT id, societe
						FROM societe
					) as data_soc
					LEFT JOIN 
					(
						SELECT
							societe,date_courrier, sum(anomalie) anomalie, sum(ci) as ci,
							sum(ko_scan) as ko_scan,
							sum(ko_def) as ko_def,
							sum(ko_inconnu) as ko_inconnu,
							sum(ko_ks) as ko_ks,
							sum(ko_ke) as ko_ke,
							sum(ko_circulaire) as ko_circulaire,
							sum(ko_abondonne) as ko_abondonne,
							sum(divise) as divise,
							sum(ci_editee) as ci_editee,
							sum(ci_envoyee) as ci_envoyee,
							sum(ok_circ) as ok_circ,
							sum(ko_bayard) as ko_bayard
						FROM
						(
							SELECT 
								case when f_pli.statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7]) then count(id_pli) else 0 end as anomalie,
								case when f_pli.statut_saisie = ANY (ARRAY[9, 10]) then count(id_pli) else 0 end as ci,
								case when statut_saisie  = 2   then count(distinct id_pli) else 0 end as ko_scan,					 
								 case when statut_saisie = 3  then count( id_pli) else 0 end as ko_def,
								 case when statut_saisie = 4  then count( id_pli) else 0 end as ko_inconnu,
								 case when statut_saisie = 5 then count(id_pli) else 0 end as ko_ks,
								 case when statut_saisie = 6 then count(id_pli) else 0 end as ko_ke,
								 case when  statut_saisie = 7  then count(distinct id_pli) else 0 end as ko_circulaire,
								 case when statut_saisie = 8  then count( id_pli) else 0 end as ko_abondonne,						 
								 case when statut_saisie = 9  then count(distinct id_pli) else 0 end as ci_editee,
								 case when statut_saisie = 10  then count(distinct id_pli) else 0 end as ci_envoyee,
								 case when statut_saisie = 24  then count(distinct id_pli) else 0 end as divise,
								 case when statut_saisie = 11  then count(distinct id_pli) else 0 end as ok_circ,
								 case when statut_saisie = 12  then count(distinct id_pli) else 0 end as ko_bayard,								 
								societe.id as societe,date_courrier::date as date_courrier
							FROM f_pli 
							INNER JOIN f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
							LEFT JOIN societe on societe.id = f_pli.societe 
							WHERE date_courrier::date = '".$date_j."' ".$where_soc."
							AND f_pli.statut_saisie in (2, 3, 4, 5, 6, 7, 8, 9, 10,24)				
							GROUP BY societe.id,date_courrier::date,f_pli.statut_saisie 
						) as tab GROUP BY societe,date_courrier
					) as data on data.societe = data_soc.id
					
				";
			
			/*echo "<pre>";		 
			print_r($sql);
			echo "</pre>";	exit;
			*/
			
		return $this->ged->query($sql)
							->result_array();
	}
		
	public function get_suivi_solde_mvt($date_j, $id_societe){
      $where_soc = "";
	  if($id_societe != '') $where_soc = " AND societe = '".$id_societe."'";
	  $sql = "	SELECT 
					societe,nom_societe, date_courrier,mvt_total,mvt_saisie_adv_jmoins,
					stock_initial,mvt_saisie_adv_j,stock_final, mvt_saisie_adv_j_ok,mvt_saisie_adv_j_ci,mvt_saisie_adv_j_ko 
					FROM (
						SELECT data_total.societe,nom_societe, data_total.date_courrier,mvt_total/*, mvt_en_prod_j*/, 
						coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins, 
						coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_initial, 
						coalesce(mvt_saisie_adv_j,0) as mvt_saisie_adv_j, 
						coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_j,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_final, 
						coalesce(mvt_saisie_adv_j_ok,0) as mvt_saisie_adv_j_ok, 
						coalesce(mvt_saisie_adv_j_ci,0) as mvt_saisie_adv_j_ci, 
						coalesce(mvt_saisie_adv_j_ko, 0) as mvt_saisie_adv_j_ko 
						FROM ( 

							SELECT date_courrier::date as date_courrier, 
							sum(nb_mvt) as mvt_total ,societe 
							FROM view_pli_stat 
							LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
							WHERE concat(date_courrier::date,societe)::text in 
								(SELECT concat(date_courrier::date,societe) 
								FROM view_pli_stat 
								WHERE dt_event::date = '".$date_j."' ".$where_soc." 
								GROUP BY date_courrier,societe ) GROUP BY date_courrier::date,societe 
						) as data_total 
						LEFT JOIN 
						( 
							SELECT date_courrier::date as date_courrier ,view_pli_stat.societe, nom_societe, 
							sum(nb_mvt) as mvt_en_prod_j
							 FROM view_pli_stat
							 LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
							 WHERE dt_event::date = '".$date_j."' ".$where_soc." 
							 GROUP BY societe,nom_societe, date_courrier 
						 )as data_ttt_j 
						 on data_total.date_courrier = data_ttt_j.date_courrier 
						 AND data_total.societe = data_ttt_j.societe 
						 LEFT JOIN 
						 ( 
								Select adv_total.date_courrier,adv_total.societe, 
								coalesce(mvt_saisie_adv_j,0) mvt_saisie_adv_j, coalesce(mvt_saisie_adv_j_ok,0) mvt_saisie_adv_j_ok, 
								coalesce(mvt_saisie_adv_j_ci,0) mvt_saisie_adv_j_ci, 
								coalesce(mvt_saisie_adv_j_ko,0) mvt_saisie_adv_j_ko 
								FROM (
									SELECT date_courrier::date as date_courrier,societe, 
									sum(nb_mvt) as mvt_saisie_adv_j 
									FROM view_pli_stat
									LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
									WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
									AND id_lot_saisie is not null 
									GROUP BY date_courrier,societe 
								)as adv_total 
								LEFT JOIN 
								( 
								SELECT date_courrier::date as date_courrier,societe, 
								sum(nb_mvt) as mvt_saisie_adv_j_ok 
								FROM view_pli_stat 
								LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
								WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
								AND id_lot_saisie is not null AND statut_saisie in (1) 
								GROUP BY date_courrier,societe 
								) as adv_ok on adv_ok.date_courrier = adv_total.date_courrier 
								AND adv_ok.societe = adv_total.societe 
								LEFT JOIN(
								SELECT date_courrier::date,societe, sum(nb_mvt) as mvt_saisie_adv_j_ci 
								FROM view_pli_stat 
								LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
								WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
								AND id_lot_saisie is not null AND statut_saisie in (3) 
								GROUP BY date_courrier,societe 
								) as adv_ci on adv_ci.date_courrier = adv_total.date_courrier 
								AND adv_ci.societe = adv_total.societe 
								LEFT JOIN(
								SELECT date_courrier::date,societe, sum(nb_mvt) as mvt_saisie_adv_j_ko 
								FROM view_pli_stat 
								LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
								WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
								AND id_lot_saisie is not null 
								AND statut_saisie not in (1,3) GROUP BY date_courrier,societe 
								)adv_ko on adv_ko.date_courrier = adv_total.date_courrier 
								AND adv_ko.societe = adv_total.societe 
						)as data_saisie_advantage 
						on data_saisie_advantage.date_courrier = data_ttt_j.date_courrier 
						AND data_saisie_advantage.societe = data_ttt_j.societe 
						LEFT JOIN 
						(
						 SELECT date_courrier,societe, sum(nb_mvt) as mvt_saisie_adv_jmoins 
						 FROM view_pli_stat 
						 LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
						 WHERE date_saisie_advantage < '".$date_j."' ".$where_soc." 
						 AND id_lot_saisie is not null GROUP BY date_courrier,societe 
						 )as data_saisie_advantage1 
						 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier 
						 AND data_saisie_advantage1.societe = data_ttt_j.societe 
						 WHERE mvt_en_prod_j is not null 
					)as data ORDER BY societe,date_courrier asc 

			";
					 
				return $this->ged->query($sql)
							->result_array();

    }
	
	public function get_suivi_solde_mvt_courrier($date_deb, $date_fin, $date_j, $id_societe){
      $where_soc = "";
	  if($id_societe != '') $where_soc = " AND pli.societe = '".$id_societe."'";
	
				$sql="
				SELECT
					societe,nom_societe, date_courrier,mvt_total,mvt_saisie_adv_jmoins,stock_initial,mvt_saisie_adv_j,stock_final,
					mvt_saisie_adv_j_ok,mvt_saisie_adv_j_ci,mvt_saisie_adv_j_ko
				FROM
				(

					SELECT data_total.societe,nom_societe, data_total.date_courrier,mvt_total, 			
					coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins, 
					coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_initial, 
					coalesce(mvt_saisie_adv_j,0) as mvt_saisie_adv_j,
					coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_j,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_final,
					coalesce(mvt_saisie_adv_j_ok,0) as mvt_saisie_adv_j_ok,
					coalesce(mvt_saisie_adv_j_ci,0) as mvt_saisie_adv_j_ci,
					coalesce(mvt_saisie_adv_j_ko, 0) as mvt_saisie_adv_j_ko
					FROM 
					(
						SELECT date_courrier::date as date_courrier ,pli.societe, count(f_mouvement.id)  as mvt_total
						FROM pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
						INNER JOIN view_societe ON view_societe.id = pli.societe
						WHERE 
							date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						GROUP BY pli.societe,nom_societe, date_courrier
					) as data_total

					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,pli.societe, nom_societe, count(f_mouvement.id)  as mvt_en_prod_j
						FROM pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
						INNER JOIN view_societe ON view_societe.id = pli.societe
						WHERE 
							date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."  
						GROUP BY pli.societe,nom_societe, date_courrier
					
					)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier AND data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						
						Select adv_total.date_courrier,adv_total.societe,
							coalesce(mvt_saisie_adv_j,0) mvt_saisie_adv_j,
							coalesce(mvt_saisie_adv_j_ok,0) mvt_saisie_adv_j_ok,
							coalesce(mvt_saisie_adv_j_ci,0) mvt_saisie_adv_j_ci,
							coalesce(mvt_saisie_adv_j_ko,0) mvt_saisie_adv_j_ko
						FROM	
						(
							SELECT date_courrier::date as date_courrier,pli.societe,  count(f_mouvement.id) as mvt_saisie_adv_j
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
							LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
							WHERE (dt_enregistrement::date =  '".$date_j."' or dt_batch::date =  '".$date_j."') ".$where_soc."  
							AND flag_saisie = 1
							GROUP BY date_courrier,pli.societe
						)as adv_total
						LEFT JOIN
						(
							SELECT date_courrier::date as date_courrier,pli.societe, count(f_mouvement.id)  as mvt_saisie_adv_j_ok
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
							LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
							WHERE (dt_enregistrement::date =  '".$date_j."' or dt_batch::date =  '".$date_j."') ".$where_soc."  
							AND ( flag_saisie = 1 and pli.statut_saisie = 1)
							GROUP BY date_courrier,pli.societe
						) as adv_ok on adv_ok.date_courrier = adv_total.date_courrier AND adv_ok.societe = adv_total.societe
						LEFT JOIN(
							SELECT date_courrier::date,pli.societe, count(f_mouvement.id)  as mvt_saisie_adv_j_ci
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
							LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
							WHERE (dt_enregistrement::date =  '".$date_j."' or dt_batch::date =  '".$date_j."') ".$where_soc."  
							AND (flag_saisie = 1 and pli.statut_saisie in(7,9,10))
							GROUP BY date_courrier,pli.societe
						) as adv_ci on adv_ci.date_courrier = adv_total.date_courrier AND adv_ci.societe = adv_total.societe
						LEFT JOIN(
							SELECT date_courrier::date,pli.societe, count(f_mouvement.id)  as mvt_saisie_adv_j_ko
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
							LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
							WHERE (dt_enregistrement::date =  '".$date_j."' or dt_batch::date =  '".$date_j."') ".$where_soc."  
							AND (flag_saisie = 1 and pli.statut_saisie in(2,3,4,5,6,8))
							GROUP BY date_courrier,pli.societe
						)adv_ko on adv_ko.date_courrier = adv_total.date_courrier AND adv_ko.societe = adv_total.societe
					
					)as data_saisie_advantage on data_saisie_advantage.date_courrier = data_ttt_j.date_courrier AND data_saisie_advantage.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_courrier,pli.societe, count(f_mouvement.id)  as mvt_saisie_adv_jmoins
						FROM pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
						LEFT JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
						WHERE (dt_enregistrement::date < '".$date_j."' or dt_batch::date < '".$date_j."') 
						AND date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						AND flag_saisie = 1
						GROUP BY date_courrier,pli.societe
						
					)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier AND data_saisie_advantage1.societe = data_ttt_j.societe
					
					WHERE mvt_en_prod_j is not null
				)as data
				ORDER BY societe,date_courrier  asc
			";
			
			/*
			echo "<pre>";	 
			print_r($sql);
			echo "</pre>";exit;	
			*/
			
			return $this->base_64->query($sql)
						->result_array();

    }

	public function get_reception_anomalie_mvt($date_j, $id_societe){
		 if($id_societe != '') $where_soc = " AND f_pli.societe = '".$id_societe."'";
		/*$sql = "SELECT coalesce(anomalie,0) as anomalie, 
					data_soc.id as societe,
					CASE WHEN date_courrier is null THEN '".$date_j."'::date ELSE date_courrier END as date_courrier
				FROM 
				(
					SELECT id, societe
					FROM societe
				) as data_soc
				LEFT JOIN 
				(
					SELECT sum(nb_mvt) as anomalie,societe.id as societe,date_courrier::date as date_courrier
						FROM view_pli_stat
						LEFT JOIN societe on societe.id = view_pli_stat.societe
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 						
						WHERE date_courrier::date = '".$date_j."' ".$where_soc."
						AND flag_traitement in (16,21)						
					GROUP BY societe.id,date_courrier::date
				) as data on data.societe = data_soc.id
				";
		*/
		$sql ="SELECT coalesce(anomalie,0) as anomalie, 
				coalesce(ci,0) as ci,
				coalesce(ko_scan,0) as ko_scan,
				coalesce(ko_def,0) as ko_def,
				coalesce(ko_inconnu,0) as ko_inconnu,
				coalesce(ko_ks,0) as ko_ks,
				coalesce(ko_ke,0) as ko_ke,
				coalesce(ko_circulaire,0) as ko_circulaire,
				coalesce(ko_abondonne,0) as ko_abondonne,
				coalesce(divise,0) as divise,
				coalesce(ci_editee,0) as ci_editee,
				coalesce(ci_envoyee,0) as ci_envoyee,
				coalesce(ok_circ,0) as ok_circ,
				coalesce(ko_bayard,0) as ko_bayard,
				data_soc.id as societe,
				CASE WHEN date_courrier is null THEN '".$date_j."'::date ELSE date_courrier END as date_courrier
			FROM 
			(
				SELECT id, societe
				FROM societe
			) as data_soc
			LEFT JOIN 
			(
				SELECT
					societe,date_courrier, sum(anomalie) anomalie, sum(ci) as ci,
					sum(ko_scan) as ko_scan,
					sum(ko_def) as ko_def,
					sum(ko_inconnu) as ko_inconnu,
					sum(ko_ks) as ko_ks,
					sum(ko_ke) as ko_ke,
					sum(ko_circulaire) as ko_circulaire,
					sum(ko_abondonne) as ko_abondonne,
					sum(divise) as divise,
					sum(ci_editee) as ci_editee,
					sum(ci_envoyee) as ci_envoyee,
					sum(ok_circ) as ok_circ,
					sum(ko_bayard) as ko_bayard
					FROM
					(
						SELECT 
						case when f_pli.statut_saisie = ANY (ARRAY[9, 10]) then count(mouvement.id_pli) else 0 end as ci,
						case when f_pli.statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7]) then count(mouvement.id_pli) else 0 end as anomalie,
						 case when statut_saisie = 2  then  count(mouvement.id) else 0 end as ko_scan,					 
						 case when statut_saisie = 3  then  count(mouvement.id)else 0 end as ko_def,
						 case when statut_saisie = 4  then  count(mouvement.id) else 0 end as ko_inconnu,
						 case when statut_saisie = 5 then   count(mouvement.id) else 0 end as ko_ks,
						 case when statut_saisie = 6 then   count(mouvement.id) else 0 end as ko_ke,
						 case when  statut_saisie = 7  then  count(mouvement.id) else 0 end as ko_circulaire,
						 case when statut_saisie = 8  then   count(mouvement.id) else 0 end as ko_abondonne,						 
						 case when statut_saisie = 9  then   count(mouvement.id) else 0 end as ci_editee,
						 case when statut_saisie = 10  then  count(mouvement.id) else 0 end as ci_envoyee,
						 case when statut_saisie = 24  then  count(mouvement.id) else 0 end as divise,
						 case when statut_saisie = 11  then  count(mouvement.id) else 0 end as ok_circ,
						 case when statut_saisie = 12  then  count(mouvement.id) else 0 end as ko_bayard,
						societe.id as societe,date_courrier::date as date_courrier
						FROM f_pli 
						INNER JOIN f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
						LEFT JOIN societe on societe.id = f_pli.societe
						LEFT JOIN  mouvement on mouvement.id_pli = f_pli.id_pli 						
						WHERE date_courrier::date = '".$date_j."' ".$where_soc."
						AND statut_saisie in (2, 3, 4, 5, 6, 7, 8, 9, 10,24)						
						GROUP BY societe.id,date_courrier::date,f_pli.statut_saisie 
						) as tab GROUP BY societe,date_courrier
			) as data on data.societe = data_soc.id
		";
		return $this->ged->query($sql)
							->result_array();
	}
		
	public function get_solde_courrier_sla($date_deb, $date_fin, $date_j, $id_societe){
      $where_soc = ""; 
	  if($id_societe != '') $where_soc = " AND view_data_pli.societe = '".$id_societe."'";
	 
			$sql ="
				SELECT
					societe,nom_societe, date_courrier,pli_total,
					pli_saisie_j,pli_saisie_j_plus_1,pli_saisie_j_plus_2,pli_saisie_j_plus_plus,
					pli_non_traite,pli_att_saisie
				FROM
				(

					SELECT data_total.societe,nom_societe, data_total.date_courrier,pli_total, 			
					coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins,  
					coalesce(pli_saisie_j, 0) as pli_saisie_j,
					coalesce(pli_saisie_j_plus_1, 0) as pli_saisie_j_plus_1,
					coalesce(pli_saisie_j_plus_2, 0) as pli_saisie_j_plus_2,
					coalesce(pli_saisie_j_plus_plus, 0) as pli_saisie_j_plus_plus,
					coalesce(pli_non_traite, 0) as pli_non_traite,
					coalesce(pli_att_saisie, 0) as pli_att_saisie
					
					FROM 
					(
						SELECT date_courrier::date as date_courrier,  count(pli.id_pli) as pli_total ,pli.societe
						FROM pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
						WHERE date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 			
						GROUP BY date_courrier::date,pli.societe
					
					) as data_total
					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier,  count(pli.id_pli) as pli_non_traite ,pli.societe
						FROM pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
						INNER JOIN view_societe ON view_societe.id = pli.societe
						WHERE date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."  
						AND (pli.flag_traitement = 0 or pli.flag_traitement is null)
						GROUP BY date_courrier::date,pli.societe
					
					) as data_non_traite on data_total.date_courrier = data_non_traite.date_courrier AND data_total.societe = data_non_traite.societe
					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,pli.societe, COUNT(pli.id_pli) as pli_att_saisie
						FROM pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
						INNER JOIN view_societe ON view_societe.id = pli.societe
						WHERE 
							date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."  
							AND flag_saisie = 0
							AND pli.flag_traitement in (1,2,3,4)
						GROUP BY pli.societe,nom_societe, date_courrier					
					)as data_att_saisie on data_total.date_courrier = data_att_saisie.date_courrier AND data_total.societe = data_att_saisie.societe
					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,pli.societe, nom_societe, COUNT(pli.id_pli) as pli_en_prod_j
						FROM pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
						INNER JOIN view_societe ON view_societe.id = pli.societe
						WHERE 
							date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						GROUP BY pli.societe,nom_societe, date_courrier
					
					)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier AND data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_courrier::date,pli.societe, 
						case when flag_saisie = 1 then count(flag_saisie) else 0 end as pli_saisie_adv_jmoins
						FROM pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						LEFT JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
						/*LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli*/
						WHERE (dt_enregistrement < '".$date_j."' or dt_batch < '".$date_j."' ) 
						AND date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						AND flag_saisie = 1
						GROUP BY flag_saisie,date_courrier::date,pli.societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier AND data_saisie_advantage1.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT
							date_courrier ,societe, sum(pli_saisie_j) as pli_saisie_j,sum(pli_saisie_j_plus_1) as pli_saisie_j_plus_1,
							sum(pli_saisie_j_plus_2) as pli_saisie_j_plus_2,sum(pli_saisie_j_plus_plus) as pli_saisie_j_plus_plus
						FROM
						(
							SELECT date_courrier::date as date_courrier,pli.societe,
							CASE WHEN ( date_courrier::date = dt_enregistrement::date)  OR ( date_courrier::date = dt_batch::date and flag_batch = 1)
							THEN count(distinct pli.id_pli) ELSE 0
							END as pli_saisie_j,
							CASE WHEN extract(dow FROM (date_courrier::date+ INTERVAL '1 day')::date)::integer = 0 
								THEN 
									CASE WHEN ( (date_courrier::date+ INTERVAL '2 day')::date = dt_enregistrement::date) OR 
									( (date_courrier::date+ INTERVAL '2 day')::date = dt_batch::date and flag_batch = 1) 
									THEN count(distinct pli.id_pli) ELSE 0
									END
								ELSE 
									CASE WHEN ( (date_courrier::date+ INTERVAL '1 day')::date = dt_enregistrement::date) OR 
									( (date_courrier::date+ INTERVAL '1 day')::date = dt_batch::date and flag_batch = 1) 
									THEN count(distinct pli.id_pli) ELSE 0
									END
							END as pli_saisie_j_plus_1,
							CASE WHEN extract(dow FROM (date_courrier::date+ INTERVAL '2 day')::date) = 0 
								THEN 
									CASE WHEN ( (date_courrier::date+ INTERVAL '3 day')::date = dt_enregistrement::date) 
									 OR 
									( (date_courrier::date+ INTERVAL '3 day')::date = dt_batch::date and flag_batch = 1)
									THEN count(distinct pli.id_pli) ELSE 0
									END
								ELSE 
									CASE WHEN ( (date_courrier::date+ INTERVAL '2 day')::date = dt_enregistrement::date) OR 
									( (date_courrier::date+ INTERVAL '2 day')::date = dt_batch::date and flag_batch = 1) 
									THEN count(distinct pli.id_pli) ELSE 0
									END
							END as pli_saisie_j_plus_2,

							CASE WHEN extract(dow FROM (date_courrier::date+ INTERVAL '2 day')::date) = 0 
								THEN 
									CASE WHEN ( (date_courrier::date+ INTERVAL '3 day')::date < dt_enregistrement::date) OR 
									( (date_courrier::date+ INTERVAL '3 day')::date < dt_batch::date and flag_batch = 1) 
									THEN count(distinct pli.id_pli) ELSE 0
									END
								ELSE 
									CASE WHEN ( (date_courrier::date+ INTERVAL '2 day')::date < dt_enregistrement::date) OR 
									( (date_courrier::date+ INTERVAL '2 day')::date < dt_batch::date and flag_batch = 1)
									THEN count(distinct pli.id_pli) ELSE 0
									END
							END as pli_saisie_j_plus_plus			
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							LEFT JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
							LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
							WHERE date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
							AND ( flag_saisie = 1 )
							GROUP BY date_courrier,pli.societe,dt_enregistrement::date,dt_batch::date,flag_batch
						) as data_saisie
						GROUP BY date_courrier ,societe
					)as data_saisie_jplus on data_saisie_jplus.date_courrier = data_ttt_j.date_courrier AND data_saisie_jplus.societe = data_ttt_j.societe
					
					WHERE pli_en_prod_j is not null
				)as data
				ORDER BY societe,date_courrier  asc
				";
			/*echo "<pre>";		 
			print_r($sql);
			echo "</pre>";	exit;
			*/
			
				return $this->base_64->query($sql)
							->result_array();

    }

	public function get_suivi_mvt_courrier_sla($date_deb, $date_fin, $date_j, $id_societe){
      $where_soc = "";
	  if($id_societe != '') $where_soc = " AND view_data_pli.societe = '".$id_societe."'";
		
				$sql="SELECT
						societe,nom_societe, date_courrier,mvt_total,mvt_saisie_adv_jmoins,
						mvt_saisie_j,mvt_saisie_j_plus_1,mvt_saisie_j_plus_2,mvt_saisie_j_plus_plus,
						mvt_non_traite,mvt_att_saisie
					FROM
					(

						SELECT data_total.societe,nom_societe, data_total.date_courrier,mvt_total, 			
						coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins, 
						coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_initial, 
						coalesce(mvt_saisie_j, 0) as mvt_saisie_j,
						coalesce(mvt_saisie_j_plus_1, 0) as mvt_saisie_j_plus_1,
						coalesce(mvt_saisie_j_plus_2, 0) as mvt_saisie_j_plus_2,
						coalesce(mvt_saisie_j_plus_plus, 0) as mvt_saisie_j_plus_plus,
						coalesce(mvt_non_traite, 0) as mvt_non_traite,
						coalesce(mvt_att_saisie, 0) as mvt_att_saisie
						FROM 
						(
							SELECT date_courrier::date as date_courrier,  count(f_mouvement.id) as mvt_total ,pli.societe
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
							INNER JOIN view_societe ON view_societe.id = pli.societe
							LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
							WHERE date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 		
							GROUP BY date_courrier::date,pli.societe
						
						) as data_total
						LEFT JOIN
						(
							SELECT date_courrier::date as date_courrier, count(f_mouvement.id) as mvt_non_traite ,pli.societe
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
							INNER JOIN view_societe ON view_societe.id = pli.societe
							LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
							WHERE 
									date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
									AND (pli.flag_traitement = 0 or pli.flag_traitement is null)
							GROUP BY date_courrier::date,pli.societe
						
						) as data_non_traite on data_total.date_courrier = data_non_traite.date_courrier AND data_total.societe = data_non_traite.societe					
						LEFT JOIN
						(
							SELECT date_courrier::date as date_courrier ,pli.societe, count(f_mouvement.id) as mvt_att_saisie
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
							INNER JOIN view_societe ON view_societe.id = pli.societe
							LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
							WHERE 
								date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."   
								AND flag_saisie = 0
								AND pli.flag_traitement in (1,2,3,4)
							GROUP BY pli.societe,nom_societe, date_courrier					
						)as data_att_saisie on data_total.date_courrier = data_att_saisie.date_courrier AND data_total.societe = data_att_saisie.societe
						LEFT JOIN
						(
							SELECT date_courrier::date as date_courrier ,pli.societe, nom_societe, count(f_mouvement.id) as mvt_en_prod_j
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
							INNER JOIN view_societe ON view_societe.id = pli.societe
							LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
							WHERE 
								date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
							GROUP BY pli.societe,nom_societe, date_courrier
						
						)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier AND data_total.societe = data_ttt_j.societe
						LEFT JOIN
						(
							SELECT date_courrier,pli.societe,sum(mvt_nbr) as mvt_saisie_adv_jmoins
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
							INNER JOIN view_societe ON view_societe.id = pli.societe
							/*LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli*/
							WHERE dt_enregistrement < '".$date_j."' ".$where_soc."
							AND date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
							AND flag_saisie = 1
							GROUP BY date_courrier,pli.societe
						
						)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier AND data_saisie_advantage1.societe = data_ttt_j.societe
						LEFT JOIN
						(
							SELECT
								date_courrier ,societe, sum(mvt_saisie_j) as mvt_saisie_j,
								sum(mvt_saisie_j_plus_1) as mvt_saisie_j_plus_1,
								sum(mvt_saisie_j_plus_2) as mvt_saisie_j_plus_2,
								sum(mvt_saisie_j_plus_plus) as mvt_saisie_j_plus_plus
							FROM
							(	
								SELECT date_courrier::date as date_courrier,pli.societe, 
								CASE WHEN ( date_courrier::date = dt_enregistrement::date) OR ( date_courrier::date = dt_batch::date and flag_batch = 1)
								THEN count(f_mouvement.id) ELSE 0
								END as mvt_saisie_j,
								CASE WHEN extract(dow FROM (date_courrier::date+ INTERVAL '1 day')::date)::integer = 0 
									THEN 
										CASE WHEN ( (date_courrier::date+ INTERVAL '2 day')::date = dt_enregistrement::date)  OR 
									( (date_courrier::date+ INTERVAL '2 day')::date = dt_batch::date and flag_batch = 1)
										THEN count(f_mouvement.id) ELSE 0
										END
									ELSE 
										CASE WHEN ( (date_courrier::date+ INTERVAL '1 day')::date = dt_enregistrement::date)  OR 
									( (date_courrier::date+ INTERVAL '1 day')::date = dt_batch::date and flag_batch = 1)
										THEN count(f_mouvement.id) ELSE 0
										END
								END as mvt_saisie_j_plus_1,
								CASE WHEN extract(dow FROM (date_courrier::date+ INTERVAL '2 day')::date) = 0 
									THEN 
										CASE WHEN ( (date_courrier::date+ INTERVAL '3 day')::date = dt_enregistrement::date) OR 
									( (date_courrier::date+ INTERVAL '3 day')::date = dt_batch::date and flag_batch = 1)
										THEN count(f_mouvement.id) ELSE 0
										END
									ELSE 
										CASE WHEN ( (date_courrier::date+ INTERVAL '2 day')::date = dt_enregistrement::date) OR 
									( (date_courrier::date+ INTERVAL '2 day')::date = dt_batch::date and flag_batch = 1) 
										THEN count(f_mouvement.id) ELSE 0
										END
								END as mvt_saisie_j_plus_2,

								CASE WHEN extract(dow FROM (date_courrier::date+ INTERVAL '2 day')::date) = 0 
									THEN 
										CASE WHEN ( (date_courrier::date+ INTERVAL '3 day')::date < dt_enregistrement::date)  OR 
									( (date_courrier::date+ INTERVAL '3 day')::date < dt_batch::date and flag_batch = 1)
										THEN count(f_mouvement.id) ELSE 0
										END
									ELSE 
										CASE WHEN ( (date_courrier::date+ INTERVAL '2 day')::date < dt_enregistrement::date) OR 
									( (date_courrier::date+ INTERVAL '2 day')::date < dt_batch::date and flag_batch = 1)
										THEN count(f_mouvement.id) ELSE 0
										END
								END as mvt_saisie_j_plus_plus
								FROM pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN view_data_pli ON view_data_pli.id_pli = pli.id_pli
								INNER JOIN view_societe ON view_societe.id = pli.societe
								LEFT JOIN  f_mouvement on f_mouvement.id_pli = pli.id_pli
								WHERE date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
								 AND  flag_saisie = 1 
								GROUP BY date_courrier,pli.societe,dt_enregistrement,dt_batch::date,flag_batch
							) as data_saisie
							GROUP BY date_courrier ,societe
						)as data_saisie_jplus on data_saisie_jplus.date_courrier = data_ttt_j.date_courrier AND data_saisie_jplus.societe = data_ttt_j.societe
						
						WHERE mvt_en_prod_j is not null
					)as data
					ORDER BY societe,date_courrier  asc";
				
				/*echo "<pre>";		 
				print_r($sql);
				echo "</pre>";	exit;
				*/
				
				return $this->base_64->query($sql)
							->result_array();

	}
	//anomalie courrier
	public function get_reception_anomalie_sla($date_j, $id_societe){ 
		 if($id_societe != '') $where_soc = " AND f_pli.societe = '".$id_societe."'";
		$sql = "SELECT societe,date_courrier,
				sum(ko_scan) as ko_scan,
				sum(ko_def) as ko_def,
				sum(ko_inconnu) as ko_inconnu,
				sum(ko_src) as ko_src,
				sum(ko_ke) as ko_ke,
				sum(ko_ci) as ko_ci,
				sum(ko_ci_editee) as ko_ci_editee,
				sum(ko_ci_envoyee) as ko_ci_envoyee,
				sum(ko_abondonne) as ko_abondonne,
				sum(divise) as divise,
				sum(ok_circ) as ok_circ,
				sum(ko_bayard) as ko_bayard,
				sum(mvt_ko_scan) as mvt_ko_scan,
				sum(mvt_def) as mvt_ko_def,
				sum(mvt_ko_inconnu) as mvt_ko_inconnu,
				sum(mvt_src) as mvt_ko_src,
				sum(mvt_ke) as mvt_ko_ke,
				sum(mvt_ci) as mvt_ko_ci,
				sum(mvt_ci_editee) as mvt_ci_editee,
				sum(mvt_ci_envoyee) as mvt_ci_envoyee,
				sum(mvt_abondonne) as mvt_abondonne,
				sum(mvt_divise) as mvt_divise,
				sum(mvt_ok_circ) as mvt_ok_circ,
				sum(mvt_ko_bayard) as mvt_ko_bayard
				
				FROM 
				(
					SELECT coalesce(ko_scan,0) as ko_scan, coalesce(ko_def,0) as ko_def, 
							coalesce(ko_inconnu,0) as ko_inconnu, coalesce(ko_src,0) as ko_src, 
							coalesce(ko_ke,0) as ko_ke, coalesce(ko_ci,0) as ko_ci, coalesce(ko_abondonne,0) as ko_abondonne,
							coalesce(divise,0) as divise,							
							coalesce(ok_circ,0) as ok_circ,							
							coalesce(ko_bayard,0) as ko_bayard,							
							coalesce(ko_ci_editee,0) as ko_ci_editee, coalesce(ko_ci_envoyee,0) as ko_ci_envoyee, 
							coalesce(mvt_ko_scan,0) as mvt_ko_scan, coalesce(mvt_def,0) as mvt_def, 
							coalesce(mvt_ko_inconnu,0) as mvt_ko_inconnu, coalesce(mvt_src,0) as mvt_src, 
							coalesce(mvt_ke,0) as mvt_ke, coalesce(mvt_ci,0) as mvt_ci, 
							coalesce(mvt_ci_editee,0) as mvt_ci_editee, coalesce(mvt_ci_envoyee,0) as mvt_ci_envoyee,
							coalesce(mvt_abondonne,0) as mvt_abondonne,
							coalesce(mvt_divise,0) as mvt_divise,
							coalesce(mvt_ok_circ,0) as mvt_ok_circ,
							coalesce(mvt_ko_bayard,0) as mvt_ko_bayard,
							data_soc.id as societe,
							CASE WHEN date_courrier is null THEN '".$date_j."'::date ELSE date_courrier END as date_courrier
						FROM 
						(
							SELECT id, societe
							FROM societe
						) as data_soc
						LEFT JOIN 
						(
							
							SELECT societe.id as societe,date_courrier::date as date_courrier,
							CASE WHEN f_pli.statut_saisie = 2 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_scan,
							CASE WHEN f_pli.statut_saisie = 3 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_def,
							CASE WHEN f_pli.statut_saisie = 4 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_inconnu,
							CASE WHEN f_pli.statut_saisie = 5 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_src,
							CASE WHEN f_pli.statut_saisie = 6 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_ke,
							CASE WHEN f_pli.statut_saisie = 7 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_ci,
							CASE WHEN f_pli.statut_saisie = 8 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_abondonne,
							CASE WHEN f_pli.statut_saisie = 9 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_ci_editee,
							CASE WHEN f_pli.statut_saisie = 10 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_ci_envoyee,
							CASE WHEN f_pli.statut_saisie = 24 THEN count(distinct f_pli.id_pli) ELSE 0 END as divise,
							CASE WHEN f_pli.statut_saisie = 11 THEN count(distinct f_pli.id_pli) ELSE 0 END as ok_circ,
							CASE WHEN f_pli.statut_saisie = 12 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_bayard,
							CASE WHEN f_pli.statut_saisie = 2 THEN sum(mvt_nbr) ELSE 0 END as mvt_ko_scan,
							CASE WHEN f_pli.statut_saisie = 3 THEN sum(mvt_nbr) ELSE 0 END as mvt_def,
							CASE WHEN f_pli.statut_saisie = 4 THEN sum(mvt_nbr) ELSE 0 END as mvt_ko_inconnu,
							CASE WHEN f_pli.statut_saisie = 5 THEN sum(mvt_nbr) ELSE 0 END as mvt_src,
							CASE WHEN f_pli.statut_saisie = 6 THEN sum(mvt_nbr) ELSE 0 END as mvt_ke,
							CASE WHEN f_pli.statut_saisie = 7 THEN sum(mvt_nbr) ELSE 0 END as mvt_ci,
							CASE WHEN f_pli.statut_saisie = 8 THEN sum(mvt_nbr) ELSE 0 END as mvt_abondonne,
							CASE WHEN f_pli.statut_saisie = 9 THEN sum(mvt_nbr) ELSE 0 END as mvt_ci_editee,
							CASE WHEN f_pli.statut_saisie = 10 THEN sum(mvt_nbr) ELSE 0 END as mvt_ci_envoyee,
							CASE WHEN f_pli.statut_saisie = 24 THEN sum(mvt_nbr) ELSE 0 END as mvt_divise,
							CASE WHEN f_pli.statut_saisie = 11 THEN sum(mvt_nbr) ELSE 0 END as mvt_ok_circ,
							CASE WHEN f_pli.statut_saisie = 12 THEN sum(mvt_nbr) ELSE 0 END as mvt_ko_bayard
							FROM f_pli
							INNER JOIN f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
							/*LEFT JOIN  mouvement on mouvement.id_pli = f_pli.id_pli */
							LEFT JOIN  data_pli on data_pli.id_pli = f_pli.id_pli
							LEFT JOIN societe on societe.id = f_pli.societe 
							WHERE date_courrier::date = '".$date_j."' ".$where_soc."
							AND f_pli.flag_traitement in (2, 3, 4, 5, 6, 7, 8, 9, 10,24)								
							GROUP BY societe.id,date_courrier::date,f_pli.flag_traitement,f_pli.statut_saisie
						) as data on data.societe = data_soc.id
				) as res
				GROUP BY  societe,date_courrier
				ORDER BY  societe,date_courrier asc
				";
		   
		/*echo "<pre>";	
		print_r($sql);
		echo "</pre>";	
		exit;*/
			
		return $this->ged->query($sql)
							->result_array();
	}
	
	public function get_saisie_anomalie_courrier($date_j, $id_societe, $id_categorie){
		$where_soc = $where_cat = "";
		if($id_societe != '') $where_soc = " AND f_pli.societe = '".$id_societe."'";
		if($id_categorie  !='*') $where_cat .= " AND categorie_typologie.id_categorie = '".$id_categorie."'";
	
		$sql = "SELECT societe,date_courrier,id_categorie,
				sum(ko_scan) as ko_scan,
				sum(ko_def) as ko_def,
				sum(ko_inconnu) as ko_inconnu,
				sum(ko_src) as ko_src,
				sum(ko_ke) as ko_ke,
				sum(ko_ci) as ko_ci,
				sum(ok_circ) as ok_circ,
				sum(ko_bayard) as ko_bayard,
				sum(ko_ci_editee) as ko_ci_editee,
				sum(ko_ci_envoyee) as ko_ci_envoyee,
				sum(ko_abondonne) as ko_abondonne,
				sum(ko_scan)+sum(ko_def) +sum(ko_inconnu) +sum(ko_src)+sum(ko_ke) +sum(ko_ci) +sum(ko_abondonne) as anomalie,
				sum(ko_ci_editee) + sum(ko_ci_envoyee) as ci,
				sum(mvt_ko_scan) as mvt_ko_scan,
				sum(mvt_def) as mvt_ko_def,
				sum(mvt_ko_inconnu) as mvt_ko_inconnu,
				sum(mvt_src) as mvt_ko_src,
				sum(mvt_src) as mvt_ok_circ,
				sum(mvt_src) as mvt_ko_bayard,
				sum(mvt_ke) as mvt_ko_ke,
				sum(mvt_ci) as mvt_ko_ci,
				sum(mvt_ci_editee) as mvt_ci_editee,
				sum(mvt_ci_envoyee) as mvt_ci_envoyee,
				sum(mvt_abondonne) as mvt_abondonne,
				sum(mvt_ko_scan) +sum(mvt_def) +sum(mvt_ko_inconnu) +sum(mvt_src) +sum(mvt_ke) +sum(mvt_ci) +sum(mvt_abondonne) as mvt_anomalie,
				sum(mvt_ci_editee)+sum(mvt_ci_envoyee)  as mvt_ci
				
				FROM 
				(
					SELECT 
							coalesce(ko_scan,0) as ko_scan, coalesce(ko_def,0) as ko_def, 
							coalesce(ko_inconnu,0) as ko_inconnu, coalesce(ko_src,0) as ko_src, 
							coalesce(ko_ke,0) as ko_ke, coalesce(ko_ci,0) as ko_ci, coalesce(ko_abondonne,0) as ko_abondonne, 
							coalesce(ko_ci_editee,0) as ko_ci_editee, coalesce(ko_ci_envoyee,0) as ko_ci_envoyee,
							coalesce(mvt_ko_scan,0) as mvt_ko_scan, coalesce(mvt_def,0) as mvt_def, 
							coalesce(mvt_ko_inconnu,0) as mvt_ko_inconnu, coalesce(mvt_src,0) as mvt_src, 
							coalesce(mvt_ke,0) as mvt_ke, coalesce(mvt_ci,0) as mvt_ci, 
							coalesce(mvt_ci_editee,0) as mvt_ci_editee, coalesce(mvt_ci_envoyee,0) as mvt_ci_envoyee,
							coalesce(mvt_abondonne,0) as mvt_abondonne, 														
							coalesce(mvt_ok_circ,0) as mvt_ok_circ, 														
							coalesce(mvt_ko_bayard,0) as mvt_ko_bayard, 														
							coalesce(ok_circ,0) as ok_circ, 														
							coalesce(ko_bayard,0) as ko_bayard, 														
							data_soc.id as societe,
							CASE WHEN date_courrier is null THEN '".$date_j."'::date ELSE date_courrier END as date_courrier,
							CASE WHEN id_categorie is null THEN 0 ELSE id_categorie END as id_categorie
						FROM 
						(
							SELECT id, societe
							FROM societe
						) as data_soc
						LEFT JOIN 
						(
							SELECT societe.id as societe,date_courrier::date as date_courrier,categorie_typologie.id_categorie,
								CASE WHEN f_pli.statut_saisie = 2 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_scan,
								CASE WHEN f_pli.statut_saisie = 3 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_def,
								CASE WHEN f_pli.statut_saisie = 4 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_inconnu,
								CASE WHEN f_pli.statut_saisie = 5 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_src,
								CASE WHEN f_pli.statut_saisie = 6 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_ke,
								CASE WHEN f_pli.statut_saisie = 7 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_ci,
								CASE WHEN f_pli.statut_saisie = 8 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_abondonne,
								CASE WHEN f_pli.statut_saisie = 9 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_ci_editee,
								CASE WHEN f_pli.statut_saisie = 10 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_ci_envoyee,
								CASE WHEN f_pli.statut_saisie = 11 THEN count(distinct f_pli.id_pli) ELSE 0 END as ok_circ,
								CASE WHEN f_pli.statut_saisie = 12 THEN count(distinct f_pli.id_pli) ELSE 0 END as ko_bayard,
								CASE WHEN f_pli.statut_saisie = 2 THEN count(mouvement.id_pli) ELSE 0 END as mvt_ko_scan,
								CASE WHEN f_pli.statut_saisie = 3 THEN count(mouvement.id_pli) ELSE 0 END as mvt_def,
								CASE WHEN f_pli.statut_saisie = 4 THEN count(mouvement.id_pli) ELSE 0 END as mvt_ko_inconnu,
								CASE WHEN f_pli.statut_saisie = 5 THEN count(mouvement.id_pli) ELSE 0 END as mvt_src,
								CASE WHEN f_pli.statut_saisie = 6 THEN count(mouvement.id_pli) ELSE 0 END as mvt_ke,
								CASE WHEN f_pli.statut_saisie = 7 THEN count(mouvement.id_pli) ELSE 0 END as mvt_ci,
								CASE WHEN f_pli.statut_saisie = 8 THEN count(mouvement.id_pli) ELSE 0 END as mvt_abondonne,
								CASE WHEN f_pli.statut_saisie = 9 THEN count(mouvement.id_pli) ELSE 0 END as mvt_ci_editee,
								CASE WHEN f_pli.statut_saisie = 10 THEN count(mouvement.id_pli) ELSE 0 END as mvt_ci_envoyee,								
								CASE WHEN f_pli.statut_saisie = 11 THEN count(mouvement.id_pli) ELSE 0 END as mvt_ok_circ,								
								CASE WHEN f_pli.statut_saisie = 12 THEN count(mouvement.id_pli) ELSE 0 END as mvt_ko_bayard								
								FROM f_pli
								INNER JOIN f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
								LEFT JOIN typologie on f_pli.typologie = typologie.id
								LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
								LEFT JOIN  mouvement on mouvement.id_pli = f_pli.id_pli
								LEFT JOIN societe on societe.id = f_pli.societe 
								WHERE date_courrier::date ='".$date_j."' ".$where_soc." ".$where_cat."
								AND f_pli.statut_saisie in (2, 3, 4, 5, 6, 7, 8, 9, 10)							
							GROUP BY societe.id,date_courrier::date,f_pli.statut_saisie,categorie_typologie.id_categorie
						) as data on data.societe = data_soc.id
				) as res
				GROUP BY  societe,date_courrier,id_categorie
				ORDER BY  societe,date_courrier,id_categorie asc
				";
		    			
			
		return $this->ged->query($sql)
							->result_array();
	}
	
	/* suivi des saisies des courriers par pli et typologie*/
	public function get_saisie_typologie_sftp($date_deb, $date_fin, $date_j, $id_societe,$id_categorie, $id_src){
      $where_soc = $where_cat = $where_cat_total = ""; 
	  if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
	  if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
	 // if($id_categorie  !='*') $where_cat .= " AND categorie_typologie.id_categorie = '".$id_categorie."'";
	   if($id_categorie  =='*') {
			 // $where_cat .= " AND view_categorie_typologie.id_categorie = '".$id_categorie."'";
		  }ELSE {
			  if($id_categorie > 0) 
			  {
					$where_cat .= " AND categorie_typologie.id_categorie = '".$id_categorie."'";
					 $where_cat_total .= " AND categorie_typologie.id_categorie = '".$id_categorie."'";
			  }
			  ELSE 
			  {
			   $where_cat .= " AND categorie_typologie.id_categorie = '0' ";
			   $where_cat_total .= " AND categorie_typologie.id_categorie is null ";
			  }
		  }
	  
	  $sql = "SELECT
					societe,libelle, id_categorie ,date_reception as date_courrier,pli_total,
					pli_saisie_j,pli_saisie_j_plus_1,pli_saisie_j_plus_2,pli_saisie_j_plus_plus,
					pli_non_traite,pli_att_saisie,nom_societe
				FROM
				(

					SELECT data_total.societe, data_total.date_reception,pli_total, 
					coalesce(pli_saisie_j, 0) as pli_saisie_j,
					coalesce(pli_saisie_j_plus_1, 0) as pli_saisie_j_plus_1,
					coalesce(pli_saisie_j_plus_2, 0) as pli_saisie_j_plus_2,
					coalesce(pli_saisie_j_plus_plus, 0) as pli_saisie_j_plus_plus,
					coalesce(pli_non_traite, 0) as pli_non_traite,
					coalesce(pli_att_saisie, 0) as pli_att_saisie,
					data_total.nom_societe,
					data_total.libelle,
					data_total.id_categorie
					
					FROM 
					(
						SELECT tab_categorie_dates.daty as date_reception,tab_categorie_dates.id_categorie,tab_categorie_dates.libelle,tab_categorie_dates.societe, 
						tab_categorie_dates.nom_societe,coalesce(pli_total,0) as pli_total
							FROM 
							(
							SELECT date_reception::date as date_reception,flux.societe, nom_societe,
							CASE WHEN categorie_typologie.id_categorie is null THEN 0 ELSE categorie_typologie.id_categorie END as id_categorie,
							count(id_flux) as pli_total
							FROM flux
							LEFT JOIN typologie on flux.typologie = typologie.id_typologie
							LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							INNER join societe on societe.id = flux.societe
							WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat_total."  					
							GROUP BY date_reception::date,flux.societe, nom_societe,categorie_typologie.id_categorie
							)as data RIGHT JOIN (
								SELECT id_categorie, libelle, daty,".$id_societe." as societe,nom_societe
								FROM
								f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
							AND data.societe = tab_categorie_dates.societe
					
					) as data_total
					LEFT JOIN
					(
						SELECT tab_categorie_dates.daty as date_courrier,tab_categorie_dates.id_categorie, tab_categorie_dates.societe, coalesce(pli_non_traite,0) pli_non_traite
						FROM
						(							
							SELECT
								date_reception,	sum(pli_non_traite) as pli_non_traite,0 as id_categorie,societe
							FROM		
							(
							SELECT date_reception::date as date_reception,societe,categorie_typologie.id_categorie,
							CASE WHEN categorie_typologie.id_categorie is null THEN	count(id_flux) ELSE 0 END as pli_non_traite 
							FROM flux
							LEFT JOIN typologie on flux.typologie = typologie.id_typologie
							LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							WHERE date_reception::date between'".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat_total."
							GROUP BY date_reception::date,societe,categorie_typologie.id_categorie
							) as tab1
							where id_categorie is null
							GROUP by date_reception,id_categorie,societe
						)as data RIGHT JOIN (
							SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
							FROM
							f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
						) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
						AND data.societe = tab_categorie_dates.societe
					
					) as data_non_traite on data_total.date_reception = data_non_traite.date_courrier AND data_total.societe = data_non_traite.societe AND data_total.id_categorie = data_non_traite.id_categorie
					LEFT JOIN
					(	
						SELECT tab_categorie_dates.daty as date_reception,tab_categorie_dates.societe,nom_societe,tab_categorie_dates.id_categorie,libelle, coalesce(pli_att_saisie,0) as pli_att_saisie
						FROM
						(
						SELECT date_reception::date as date_reception ,flux.societe,
						CASE WHEN categorie_typologie.id_categorie is null THEN 0 ELSE categorie_typologie.id_categorie END as id_categorie,
						COUNT(flux.id_flux) as pli_att_saisie
						FROM  flux
						LEFT JOIN typologie on flux.typologie = typologie.id_typologie
						LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						WHERE 
							date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat."     
							AND id_lot_saisie_flux is null
							AND flux.statut_pli in (1,2,3,4,5,6)
						GROUP BY societe, date_reception::date,categorie_typologie.id_categorie)
						as data RIGHT JOIN (
							SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
							FROM
							f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
						) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
						AND data.societe = tab_categorie_dates.societe	
									
					)as data_att_saisie on data_total.date_reception = data_att_saisie.date_reception AND data_total.societe = data_att_saisie.societe AND data_total.id_categorie = data_att_saisie.id_categorie
					LEFT JOIN
					(
						SELECT tab_categorie_dates.daty as date_reception ,tab_categorie_dates.societe,nom_societe,tab_categorie_dates.id_categorie,tab_categorie_dates.libelle, Coalesce(pli_en_prod_j,0) pli_en_prod_j
						FROM  
						(
						SELECT date_reception::date as date_reception ,flux.societe,categorie_typologie.id_categorie, COUNT(id_flux) as pli_en_prod_j
						FROM  flux
						LEFT JOIN typologie on flux.typologie = typologie.id_typologie
						LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						WHERE 
							date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat."  
						GROUP BY societe, date_reception::date,categorie_typologie.id_categorie
						)
						as data RIGHT JOIN (
							SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
							FROM
							f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
						) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
						AND data.societe = tab_categorie_dates.societe	
					
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception AND data_total.societe = data_ttt_j.societe AND data_total.id_categorie = data_ttt_j.id_categorie
					LEFT JOIN
					(

						SELECT
						tab_categorie_dates.daty as date_reception,
						tab_categorie_dates.societe,
						tab_categorie_dates.nom_societe,
						tab_categorie_dates.libelle,
						tab_categorie_dates.id_categorie,  
						coalesce(pli_saisie_j,0) pli_saisie_j,
						coalesce(pli_saisie_j_plus_1,0) pli_saisie_j_plus_1,
						coalesce(pli_saisie_j_plus_2,0) pli_saisie_j_plus_2, 
						coalesce(pli_saisie_j_plus_plus,0) pli_saisie_j_plus_plus
						FROM
						(
							SELECT
								data_saisie.date_reception ,data_saisie.societe,data_saisie.libelle, data_saisie.id_categorie,
								sum(pli_saisie_j) as pli_saisie_j,sum(pli_saisie_j_plus_1) as pli_saisie_j_plus_1,
								sum(pli_saisie_j_plus_2) as pli_saisie_j_plus_2,sum(pli_saisie_j_plus_plus) as pli_saisie_j_plus_plus
							FROM
							(
								SELECT date_reception::date as date_reception,flux.societe,libelle,categorie_typologie.id_categorie,
								CASE WHEN ( date_reception::date = dt::date) 
								THEN count(id_lot_saisie_flux) ELSE 0
								END as pli_saisie_j,
								CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '1 day')::date)::integer = 0 
									THEN 
										CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) 
										THEN count(id_lot_saisie_flux) ELSE 0
										END
									ELSE 
										CASE WHEN ( (date_reception::date+ INTERVAL '1 day')::date = dt::date) 
										THEN count(id_lot_saisie_flux) ELSE 0
										END
								END as pli_saisie_j_plus_1,
								CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 
									THEN 
										CASE WHEN ( (date_reception::date+ INTERVAL '3 day')::date = dt::date) 
										THEN count(id_lot_saisie_flux) ELSE 0
										END
									ELSE 
										CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) 
										THEN count(id_lot_saisie_flux) ELSE 0
										END
								END as pli_saisie_j_plus_2,

								CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 
									THEN 
										CASE WHEN ( (date_reception::date+ INTERVAL '3 day')::date < dt::date) 
										THEN count(id_lot_saisie_flux) ELSE 0
										END
									ELSE 
										CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date < dt::date) 
										THEN count(id_lot_saisie_flux) ELSE 0
										END
								END as pli_saisie_j_plus_plus
								FROM flux
								LEFT JOIN typologie on flux.typologie = typologie.id_typologie				
								LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
								LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
								WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat." 
								AND id_lot_saisie_flux is not null
								GROUP BY date_reception::date,societe,libelle,dt::date,categorie_typologie.id_categorie
								) as data_saisie
							   GROUP BY date_reception ,societe,libelle,id_categorie
							)
							as data RIGHT JOIN (
								SELECT id_categorie, libelle, daty,".$id_societe." as societe,nom_societe
								FROM
								f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
							AND data.societe = tab_categorie_dates.societe
						
					)as data_saisie_jplus on data_saisie_jplus.date_reception = data_ttt_j.date_reception AND data_saisie_jplus.societe = data_ttt_j.societe AND data_saisie_jplus.id_categorie = data_ttt_j.id_categorie
					
					WHERE pli_en_prod_j is not null
				)as data
				Where pli_total > 0
				ORDER BY societe,date_reception  ASC	
			";
			
			/*echo "<pre>";
			print_r($sql);
			echo "</pre>";exit;
			*/
			return $this->ged_flux->query($sql)
							->result_array();

    }

	/* suivi des saisies des courriers par mouvement et typologie*/
	public function get_saisie_typologie_mvt_sftp($date_deb, $date_fin, $date_j, $id_societe,$id_categorie,$id_src){
	   $where_soc = $where_cat = $where_cat_total = "";
	   if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
	   if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
	   if($id_categorie  =='*') {
			 // $where_cat .= " AND view_categorie_typologie.id_categorie = '".$id_categorie."'";
		  }ELSE {
			  if($id_categorie > 0) 
			  {
					$where_cat .= " AND categorie_typologie.id_categorie = '".$id_categorie."'";
					 $where_cat_total .= " AND categorie_typologie.id_categorie = '".$id_categorie."'";
			  }
			  ELSE 
			  {
			   $where_cat .= " AND categorie_typologie.id_categorie = '0' ";
			   $where_cat_total .= " AND categorie_typologie.id_categorie is null ";
			  }
		  }
	   
		$sql = "SELECT
					societe,libelle, id_categorie ,date_reception as date_courrier,mvt_total,
					mvt_saisie_j,mvt_saisie_j_plus_1,mvt_saisie_j_plus_2,mvt_saisie_j_plus_plus,
					mvt_non_traite,mvt_att_saisie,nom_societe
				FROM
				(

					SELECT data_total.societe, data_total.date_reception,mvt_total, 
					coalesce(mvt_saisie_j, 0) as mvt_saisie_j,
					coalesce(mvt_saisie_j_plus_1, 0) as mvt_saisie_j_plus_1,
					coalesce(mvt_saisie_j_plus_2, 0) as mvt_saisie_j_plus_2,
					coalesce(mvt_saisie_j_plus_plus, 0) as mvt_saisie_j_plus_plus,
					coalesce(mvt_non_traite, 0) as mvt_non_traite,
					coalesce(mvt_att_saisie, 0) as mvt_att_saisie,
					data_total.nom_societe,
					data_total.libelle,
					data_total.id_categorie
					
					FROM 
					(
						SELECT tab_categorie_dates.daty as date_reception,tab_categorie_dates.id_categorie,tab_categorie_dates.libelle,tab_categorie_dates.societe, 
						tab_categorie_dates.nom_societe,coalesce(mvt_total,0) as mvt_total
							FROM 
							(
							SELECT date_reception::date as date_reception,flux.societe,
							CASE WHEN categorie_typologie.id_categorie is null THEN 0 ELSE categorie_typologie.id_categorie END as id_categorie,
							nom_societe,sum(nb_abo) as mvt_total
							FROM flux
							LEFT JOIN typologie on flux.typologie = typologie.id_typologie
							LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							INNER join societe on societe.id = flux.societe
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat_total."  					
							GROUP BY date_reception::date,flux.societe, nom_societe,categorie_typologie.id_categorie
							)as data RIGHT JOIN (
								SELECT id_categorie, libelle, daty,".$id_societe." as societe,nom_societe
								FROM
								f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
							AND data.societe = tab_categorie_dates.societe
					
					) as data_total
					LEFT JOIN
					(
						SELECT tab_categorie_dates.daty as date_courrier,tab_categorie_dates.id_categorie, tab_categorie_dates.societe, coalesce(mvt_non_traite,0) mvt_non_traite
						FROM
						(
							SELECT
								date_reception,	sum(mvt_non_traite) as mvt_non_traite,0 as id_categorie,societe
							FROM		
							(
							SELECT date_reception::date as date_reception,societe,
							CASE WHEN categorie_typologie.id_categorie is null THEN 0 ELSE categorie_typologie.id_categorie END as id_categorie, 
							sum(nb_abo) as mvt_non_traite 
							FROM flux
							LEFT JOIN typologie on flux.typologie = typologie.id_typologie
							LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							WHERE date_reception::date between'".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat_total."
							AND (statut_pli = 0 or statut_pli is null)
							GROUP BY date_reception::date,societe,categorie_typologie.id_categorie
							) as tab1
							where id_categorie is null
							GROUP by date_reception,id_categorie,societe
						)as data RIGHT JOIN (
							SELECT id_categorie, libelle, daty,".$id_societe." as societe,nom_societe
							FROM
							f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
						) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
						AND data.societe = tab_categorie_dates.societe
					
					) as data_non_traite on data_total.date_reception = data_non_traite.date_courrier AND data_total.societe = data_non_traite.societe AND data_total.id_categorie = data_non_traite.id_categorie
					LEFT JOIN
					(	
						SELECT tab_categorie_dates.daty as date_reception,tab_categorie_dates.societe,nom_societe,tab_categorie_dates.id_categorie,libelle, coalesce(mvt_att_saisie,0) as mvt_att_saisie
						FROM
						(
							SELECT date_reception::date as date_reception ,flux.societe,
							CASE WHEN categorie_typologie.id_categorie is null THEN 0 ELSE categorie_typologie.id_categorie END as id_categorie,
							sum(nb_abo) as mvt_att_saisie
							FROM  flux
							LEFT JOIN typologie on flux.typologie = typologie.id_typologie
							LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							WHERE 
								date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat."     
								AND id_lot_saisie_flux is null
								AND flux.statut_pli in (1,2,3,4,5,6)
							GROUP BY societe, date_reception::date,categorie_typologie.id_categorie
						)
						as data RIGHT JOIN (
							SELECT id_categorie, libelle, daty,".$id_societe." as societe,nom_societe
							FROM
							f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
						) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
						AND data.societe = tab_categorie_dates.societe	
									
					)as data_att_saisie on data_total.date_reception = data_att_saisie.date_reception AND data_total.societe = data_att_saisie.societe AND data_total.id_categorie = data_att_saisie.id_categorie
					LEFT JOIN
					(
						SELECT tab_categorie_dates.daty as date_reception ,tab_categorie_dates.societe,nom_societe,tab_categorie_dates.id_categorie,tab_categorie_dates.libelle, Coalesce(pli_en_prod_j,0) pli_en_prod_j
						FROM  
						(
							SELECT date_reception::date as date_reception ,flux.societe,categorie_typologie.id_categorie, sum(nb_abo) as pli_en_prod_j
							FROM  flux
							LEFT JOIN typologie on flux.typologie = typologie.id_typologie
							LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							WHERE 
								date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat."  
							GROUP BY societe, date_reception::date,categorie_typologie.id_categorie
						)
						as data RIGHT JOIN (
							SELECT id_categorie, libelle, daty,".$id_societe." as societe,nom_societe
							FROM
							f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
						) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
						AND data.societe = tab_categorie_dates.societe	
					
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception AND data_total.societe = data_ttt_j.societe AND data_total.id_categorie = data_ttt_j.id_categorie
					LEFT JOIN
					(

						SELECT
						tab_categorie_dates.daty as date_reception,
						tab_categorie_dates.societe,
						tab_categorie_dates.nom_societe,
						tab_categorie_dates.libelle,
						tab_categorie_dates.id_categorie,  
						sum(mvt_saisie_j) mvt_saisie_j,
						sum(mvt_saisie_j_plus_1) mvt_saisie_j_plus_1,
						sum(mvt_saisie_j_plus_2) mvt_saisie_j_plus_2, 
						sum(mvt_saisie_j_plus_plus) mvt_saisie_j_plus_plus
						FROM
						(
							SELECT
								data_saisie.date_reception ,data_saisie.societe,data_saisie.libelle, data_saisie.id_categorie,
								sum(mvt_saisie_j) as mvt_saisie_j,sum(mvt_saisie_j_plus_1) as mvt_saisie_j_plus_1,
								sum(mvt_saisie_j_plus_2) as mvt_saisie_j_plus_2,sum(mvt_saisie_j_plus_plus) as mvt_saisie_j_plus_plus
							FROM
							(
								SELECT date_reception::date as date_reception,flux.societe,libelle,categorie_typologie.id_categorie,
								CASE WHEN ( date_reception::date = dt::date) 
								THEN sum(nb_abo) ELSE 0
								END as mvt_saisie_j,
								CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '1 day')::date)::integer = 0 
									THEN 
										CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) 
										THEN sum(nb_abo) ELSE 0
										END
									ELSE 
										CASE WHEN ( (date_reception::date+ INTERVAL '1 day')::date = dt::date) 
										THEN sum(nb_abo) ELSE 0
										END
								END as mvt_saisie_j_plus_1,
								CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 
									THEN 
										CASE WHEN ( (date_reception::date+ INTERVAL '3 day')::date = dt::date) 
										THEN sum(nb_abo) ELSE 0
										END
									ELSE 
										CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) 
										THEN sum(nb_abo) ELSE 0
										END
								END as mvt_saisie_j_plus_2,

								CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 
									THEN 
										CASE WHEN ( (date_reception::date+ INTERVAL '3 day')::date < dt::date) 
										THEN sum(nb_abo) ELSE 0
										END
									ELSE 
										CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date < dt::date) 
										THEN sum(nb_abo) ELSE 0
										END
								END as mvt_saisie_j_plus_plus
								FROM flux
								LEFT JOIN typologie on flux.typologie = typologie.id_typologie				
								LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
								LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
								LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
								WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat." 
								AND id_lot_saisie_flux is not null
								GROUP BY date_reception::date,societe,libelle,dt::date,categorie_typologie.id_categorie
								) as data_saisie
							   GROUP BY date_reception ,societe,libelle,id_categorie
							)
							as data RIGHT JOIN (
								SELECT id_categorie, libelle, daty,".$id_societe." as societe,nom_societe
								FROM
								f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.",".$id_src.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_reception = tab_categorie_dates.daty
							AND data.societe = tab_categorie_dates.societe
							Group by tab_categorie_dates.daty ,tab_categorie_dates.societe,tab_categorie_dates.nom_societe,tab_categorie_dates.libelle,tab_categorie_dates.id_categorie
					)as data_saisie_jplus on data_saisie_jplus.date_reception = data_ttt_j.date_reception AND data_saisie_jplus.societe = data_ttt_j.societe AND data_saisie_jplus.id_categorie = data_ttt_j.id_categorie
					
					WHERE pli_en_prod_j is not null
				)as data
					Where mvt_total > 0
				ORDER BY societe,date_reception  ASC";
		/*echo "<pre>";		 
		print_r($sql);
		echo "</pre>";	
		exit;
		*/
		
		return $this->ged_flux->query($sql)->result_array();
		}

	/* suivi des saisies des sftp par pli et typologie*/
	public function get_saisie_typologie_courrier($date_deb, $date_fin, $date_j, $id_societe, $id_categorie){
      $where_soc = $where_cat = $where_view_cat = $where_cat_total = "";
	  if($id_societe != '') $where_soc = " AND pli.societe = '".$id_societe."'";
	 
	  if($id_categorie  =='*') {
			 // $where_cat .= " AND view_categorie_typologie.id_categorie = '".$id_categorie."'";
		  }ELSE {
			  if($id_categorie > 0) 
			  {
					 $where_cat .= " AND f_categorie_typologie.id_categorie = '".$id_categorie."'";
					 $where_view_cat .= " AND view_categorie_typologie.id_categorie = '".$id_categorie."'";
					 $where_cat_total .= " AND view_categorie_typologie.id_categorie = '".$id_categorie."'";
			  }
			  ELSE 
			  {
			   $where_cat .= " AND f_categorie_typologie.id_categorie = '0' ";
			   $where_view_cat .= " AND view_categorie_typologie.id_categorie = '0' ";
			   $where_cat_total .= " AND view_categorie_typologie.id_categorie is null ";
			  }
		  }
	  
	
			$sql ="
				SELECT
					societe,nom_societe,id_categorie, libelle, date_courrier,pli_total,
					pli_saisie_j,pli_saisie_j_plus_1,pli_saisie_j_plus_2,pli_saisie_j_plus_plus,
					pli_non_traite,pli_att_saisie
				FROM
				(

					SELECT data_total.societe,data_total.nom_societe, data_total.date_courrier, 			
					coalesce(pli_total,0) as pli_total,
					coalesce(pli_saisie_j, 0) as pli_saisie_j,
					coalesce(pli_saisie_j_plus_1, 0) as pli_saisie_j_plus_1,
					coalesce(pli_saisie_j_plus_2, 0) as pli_saisie_j_plus_2,
					coalesce(pli_saisie_j_plus_plus, 0) as pli_saisie_j_plus_plus,
					coalesce(pli_non_traite, 0) as pli_non_traite,
					coalesce(pli_att_saisie, 0) as pli_att_saisie,
					data_total.id_categorie,
					data_total.libelle
					
					FROM 
					(
						SELECT tab_categorie_dates.daty as date_courrier,tab_categorie_dates.libelle,tab_categorie_dates.id_categorie ,tab_categorie_dates.societe, tab_categorie_dates.nom_societe,coalesce(pli_total,0)as pli_total
						FROM 
						(
						SELECT date_courrier::date as date_courrier, libelle,
						CASE WHEN view_typlogie.id_categorie is null THEN 0 ELSE view_typlogie.id_categorie END as id_categorie, count(pli.id_pli) as pli_total ,pli.societe,view_societe.nom_societe
						FROM pli
						LEFT JOIN view_typlogie on pli.typologie = view_typlogie.id
						LEFT JOIN view_categorie_typologie on view_categorie_typologie.id_categorie = view_typlogie.id_categorie
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN view_societe ON view_societe.id = pli.societe
						WHERE date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat_total."   			
						GROUP BY date_courrier::date,pli.societe,libelle,view_typlogie.id_categorie,view_societe.nom_societe
						)
						as data
						RIGHT JOIN (
								SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
								FROM
								f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
							AND data.societe = tab_categorie_dates.societe
					
					) as data_total
					LEFT JOIN
					(
						SELECT tab_categorie_dates.daty as date_courrier,tab_categorie_dates.id_categorie, tab_categorie_dates.societe, pli_non_traite
						FROM
						(							
							SELECT
								date_courrier,	sum(pli_non_traite) as pli_non_traite,0 as id_categorie,societe
							FROM		
							(SELECT date_courrier::date as date_courrier,  count(pli.id_pli) as pli_non_traite,view_categorie_typologie.id_categorie, pli.societe
							FROM pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN view_societe ON view_societe.id = pli.societe
							LEFT JOIN view_typlogie on pli.typologie = view_typlogie.id
							LEFT JOIN view_categorie_typologie on view_categorie_typologie.id_categorie = view_typlogie.id_categorie
							WHERE date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat_total."
							GROUP BY date_courrier::date,pli.societe,view_categorie_typologie.id_categorie
							) as tab1
							where id_categorie is null
							GROUP by date_courrier,id_categorie,societe
			
						)
						as data
						RIGHT JOIN (
							SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
							FROM
							f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
						) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
						AND data.societe = tab_categorie_dates.societe
					
					) as data_non_traite on data_total.date_courrier = data_non_traite.date_courrier AND data_total.societe = data_non_traite.societe AND data_total.id_categorie = data_non_traite.id_categorie
					LEFT JOIN
					(
						SELECT daty as date_courrier ,tab_categorie_dates.societe, tab_categorie_dates.id_categorie, tab_categorie_dates.libelle,tab_categorie_dates.nom_societe,
						coalesce(pli_att_saisie,0) pli_att_saisie
						FROM
						(
							SELECT date_courrier::date as date_courrier ,pli.societe,view_categorie_typologie.id_categorie, COUNT(pli.id_pli) as pli_att_saisie/*,string_agg(pli.id_pli::text,',') as p*/
							FROM pli
							LEFT JOIN view_typlogie on pli.typologie = view_typlogie.id
							INNER JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
							LEFT JOIN view_categorie_typologie on view_categorie_typologie.id_categorie = view_typlogie.id_categorie
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN view_societe ON view_societe.id = pli.societe
							WHERE 
								date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_view_cat." 
								AND flag_saisie = 0
								AND pli.flag_traitement in (1,2,3,4)
							GROUP BY pli.societe,nom_societe, date_courrier,view_categorie_typologie.id_categorie
							)as data
							RIGHT JOIN (
								SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
								FROM
								f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
							AND data.societe = tab_categorie_dates.societe	
						
					)as data_att_saisie on data_total.date_courrier = data_att_saisie.date_courrier AND data_total.societe = data_att_saisie.societe AND data_total.id_categorie = data_att_saisie.id_categorie
					LEFT JOIN
					(
						SELECT tab_categorie_dates.daty as date_courrier ,tab_categorie_dates.societe,tab_categorie_dates.id_categorie,libelle, tab_categorie_dates.nom_societe, 
						coalesce(pli_en_prod_j,0) pli_en_prod_j
						FROM
						(
							SELECT date_courrier::date as date_courrier ,pli.societe,view_categorie_typologie.id_categorie, nom_societe, COUNT(pli.id_pli) as pli_en_prod_j
							FROM pli
							LEFT JOIN view_typlogie on pli.typologie = view_typlogie.id
							LEFT JOIN view_categorie_typologie on view_categorie_typologie.id_categorie = view_typlogie.id_categorie
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN view_societe ON view_societe.id = pli.societe
							WHERE 
								date_courrier::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_view_cat."   
							GROUP BY pli.societe,nom_societe, date_courrier,view_categorie_typologie.id_categorie
						)as data
							RIGHT JOIN (
								SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
								FROM
								f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
							AND data.societe = tab_categorie_dates.societe	
					
					)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier AND data_total.societe = data_ttt_j.societe AND data_total.id_categorie = data_ttt_j.id_categorie
					LEFT JOIN
					(
						SELECT
						/*date_courrier ,*/
						tab_categorie_dates.daty as date_courrier,
						tab_categorie_dates.societe,
						tab_categorie_dates.nom_societe,
						tab_categorie_dates.libelle,
						tab_categorie_dates.id_categorie,  
						coalesce(pli_saisie_j,0) pli_saisie_j,
						coalesce(pli_saisie_j_plus_1,0) pli_saisie_j_plus_1,
						coalesce(pli_saisie_j_plus_2,0) pli_saisie_j_plus_2, 
						coalesce(pli_saisie_j_plus_plus,0) pli_saisie_j_plus_plus
						FROM
						(	
						SELECT
							date_courrier ,societe,id_categorie, sum(pli_saisie_j) as pli_saisie_j,sum(pli_saisie_j_plus_1) as pli_saisie_j_plus_1,
							sum(pli_saisie_j_plus_2) as pli_saisie_j_plus_2,sum(pli_saisie_j_plus_plus) as pli_saisie_j_plus_plus
							FROM
							( 
							
								SELECT data_pli.date_courrier,societe,id_categorie,
									CASE WHEN ( data_pli.date_courrier::date = dt_enregistrement::date OR 
												data_pli.date_courrier::date = dt_batch::date) and flag_saisie = 1
												THEN count(distinct data_pli.id_pli) ELSE 0
												END as pli_saisie_j,
									CASE WHEN extract(dow FROM (data_pli.date_courrier::date+ INTERVAL '1 day')::date)::integer = 0 
									THEN 
											   CASE WHEN (( data_pli.date_courrier::date+ INTERVAL '2 day')::date = dt_enregistrement::date OR 
												 (data_pli.date_courrier::date+ INTERVAL '2 day')::date = dt_batch::date) and flag_saisie = 1
											   THEN count(distinct data_pli.id_pli) ELSE 0
											   END
									ELSE 
											   CASE WHEN  ((data_pli.date_courrier::date+ INTERVAL '1 day')::date = dt_enregistrement::date OR 
												(data_pli.date_courrier::date+ INTERVAL '1 day')::date = dt_batch::date) and flag_saisie = 1
											   THEN count(distinct data_pli.id_pli) ELSE 0
											   END
									END as pli_saisie_j_plus_1,
									CASE WHEN extract(dow FROM (data_pli.date_courrier::date+ INTERVAL '2 day')::date) = 0 
									THEN 
											   CASE WHEN ( (data_pli.date_courrier::date+ INTERVAL '3 day')::date = dt_enregistrement::date OR 
												 (data_pli.date_courrier::date+ INTERVAL '3 day')::date = dt_batch::date) and flag_saisie = 1
											   THEN count(distinct data_pli.id_pli) ELSE 0
											   END
									ELSE 
											   CASE WHEN ( (data_pli.date_courrier::date+ INTERVAL '2 day')::date = dt_enregistrement::date OR 
												 (data_pli.date_courrier::date+ INTERVAL '2 day')::date = dt_batch::date) and flag_saisie = 1 
											   THEN count(distinct data_pli.id_pli) ELSE 0
											   END
									END as pli_saisie_j_plus_2,

									CASE WHEN extract(dow FROM (data_pli.date_courrier::date+ INTERVAL '2 day')::date) = 0 
									THEN 
											   CASE WHEN ( (data_pli.date_courrier::date+ INTERVAL '3 day')::date < dt_enregistrement::date  OR 
												 (data_pli.date_courrier::date+ INTERVAL '3 day')::date < dt_batch::date) and flag_saisie = 1
											   THEN count(distinct data_pli.id_pli) ELSE 0
											   END
									ELSE 
											   CASE WHEN ( (data_pli.date_courrier::date+ INTERVAL '2 day')::date < dt_enregistrement::date OR 
												 (data_pli.date_courrier::date+ INTERVAL '2 day')::date < dt_batch::date) and flag_saisie = 1
										   
											   THEN count(distinct data_pli.id_pli) ELSE 0
											   END
									END as pli_saisie_j_plus_plus 
							
								FROM
								 (
									 SELECT pli.id_pli,date_courrier::date as date_courrier,pli.societe,f_categorie_typologie.id_categorie
									 FROM pli
									 INNER JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli									 
									 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
									 INNER JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie

									 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat."
									 AND flag_saisie = 1
									 ORDER BY pli.id_pli asc
								) as data_pli
								 LEFT JOIN
								 (
									 SELECT pli.id_pli,count(f_mouvement.id_pli) as nb_mvt
									 FROM pli                             
									 INNER JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli
									 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
									 INNER JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
									 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat."
									 GROUP BY pli.id_pli  
									 ORDER BY pli.id_pli asc
									   
								) as data_mvt on data_pli.id_pli = data_mvt.id_pli
								  LEFT JOIN (
										   SELECT f_data_pli.id_pli,
										   dt_enregistrement::date,dt_batch::date,flag_saisie,
										   date_courrier::date
										   FROM pli                             
										   INNER JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
										   INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
										   LEFT JOIN f_typologie on pli.typologie = f_typologie.id
										   INNER JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
										   WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat." 
											AND flag_saisie = 1									   
										   ORDER BY pli.id_pli asc                                                                                                                                      
								)as data_saisie_jour on data_saisie_jour.id_pli = data_pli.id_pli
								GROUP BY data_pli.date_courrier, dt_enregistrement,societe,id_categorie,dt_batch::date,flag_saisie
								
							) as data_saisie
							GROUP BY date_courrier ,societe,id_categorie
						) as data
						RIGHT JOIN (
							SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
							FROM
							f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
						) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
						AND data.societe = tab_categorie_dates.societe			
					)
					as data_saisie_jplus on data_saisie_jplus.date_courrier = data_ttt_j.date_courrier AND data_saisie_jplus.societe = data_ttt_j.societe
					AND data_saisie_jplus.id_categorie = data_ttt_j.id_categorie
					WHERE pli_en_prod_j is not null
				)as data
				Where pli_total > 0
				ORDER BY societe,date_courrier,id_categorie  asc
				";
			/*echo "<pre>";		 
			print_r($sql);
			echo "</pre>";	exit;*/
			
			return $this->base_64->query($sql)
							->result_array();

    }

	/* suivi des saisies des sftp par pli et typologie*/
	public function get_saisie_typologie_mvt_courrier($date_deb, $date_fin, $date_j, $id_societe, $id_categorie){
         $where_soc = $where_cat = $where_view_cat = $where_cat_total = "";
	    if($id_societe != '') $where_soc = " AND pli.societe = '".$id_societe."'";
	    if($id_categorie  =='*') {
			 // $where_cat .= " AND view_categorie_typologie.id_categorie = '".$id_categorie."'";
		  }ELSE {
			  if($id_categorie > 0) 
			  {
					$where_cat .= " AND f_categorie_typologie.id_categorie = '".$id_categorie."'";
					//$where_view_cat .= " AND f_categorie_typologie.id_categorie = '".$id_categorie."'";
					$where_cat_total .= " AND f_categorie_typologie.id_categorie = '".$id_categorie."'";
			  }
			  ELSE 
			  {
			   $where_cat .= " AND f_categorie_typologie.id_categorie = '0' ";
			   //$where_view_cat  .= " AND f_categorie_typologie.id_categorie = '0' ";
			   $where_cat_total .= " AND f_categorie_typologie.id_categorie is null ";
			  }
		  }
	  
		 
				$sql="SELECT
						societe,nom_societe,id_categorie,libelle, date_courrier,mvt_total,
						mvt_saisie_j,mvt_saisie_j_plus_1,mvt_saisie_j_plus_2,mvt_saisie_j_plus_plus,
						mvt_non_traite,mvt_att_saisie
					FROM
					(

						SELECT data_total.societe,data_total.nom_societe, data_total.date_courrier,
						coalesce(mvt_total,0) as mvt_total,
						coalesce(mvt_saisie_j, 0) as mvt_saisie_j,
						coalesce(mvt_saisie_j_plus_1, 0) as mvt_saisie_j_plus_1,
						coalesce(mvt_saisie_j_plus_2, 0) as mvt_saisie_j_plus_2,
						coalesce(mvt_saisie_j_plus_plus, 0) as mvt_saisie_j_plus_plus,
						coalesce(mvt_non_traite, 0) as mvt_non_traite,
						coalesce(mvt_att_saisie, 0) as mvt_att_saisie,
						data_total.id_categorie,
						data_total.libelle
					
						FROM 
						(
							SELECT tab_categorie_dates.daty as date_courrier,tab_categorie_dates.libelle,tab_categorie_dates.id_categorie ,tab_categorie_dates.societe, tab_categorie_dates.nom_societe,coalesce(mvt_total,0)as mvt_total
							FROM 
							(
								SELECT
								   data_pli.date_courrier::date AS date_courrier, sum(nb_mvt) as mvt_total,societe,nom_societe,
								   libelle, 
								   CASE
										WHEN id_categorie IS NULL THEN 0
										ELSE id_categorie
								  END AS id_categorie
								  FROM
								 (
									 SELECT pli.id_pli,date_courrier::date as date_courrier,pli.societe,f_categorie_typologie.id_categorie,nom_societe,
									 f_categorie_typologie.libelle
									 FROM pli
									 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
									 LEFT JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
									 INNER JOIN f_societe ON f_societe.id = pli.societe	
									 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat_total." 	 
									 ORDER BY pli.id_pli asc
								) as data_pli
								 LEFT JOIN
								 (
									 SELECT pli.id_pli,count(f_mouvement.id_pli) as nb_mvt
									 FROM pli 
									 INNER JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli
									 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
									 LEFT JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
									 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat_total."
									 GROUP BY pli.id_pli  
									 ORDER BY pli.id_pli asc
									   
								) as data_mvt on data_pli.id_pli = data_mvt.id_pli
								GROUP BY data_pli.date_courrier, societe,id_categorie,nom_societe,societe,libelle
							)as data
							RIGHT JOIN (
									SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
									FROM
									f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
								AND data.societe = tab_categorie_dates.societe
						
						) as data_total
						LEFT JOIN
						(
							SELECT tab_categorie_dates.daty as date_courrier,tab_categorie_dates.id_categorie, tab_categorie_dates.societe, mvt_non_traite
							FROM(
																
								SELECT
									date_courrier,sum(mvt_non_traite) as mvt_non_traite,0 as id_categorie,societe
								FROM		
								(
									SELECT
									   data_pli.date_courrier::date AS date_courrier, sum(nb_mvt) as mvt_non_traite,id_categorie,societe
									  FROM
									 (
										 SELECT pli.id_pli,date_courrier::date as date_courrier,pli.societe,f_categorie_typologie.id_categorie
										 FROM pli                             
										 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
										 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
										 LEFT JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
										 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat_total." 
										 ORDER BY pli.id_pli asc
									) as data_pli
									 LEFT JOIN
									 (
										 SELECT pli.id_pli,count(f_mouvement.id_pli) as nb_mvt
										 FROM pli                             
										 INNER JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli
										 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
										 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
										 LEFT JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
										 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc." ".$where_cat_total."
										 GROUP BY pli.id_pli  
										 ORDER BY pli.id_pli asc										   
									) as data_mvt on data_pli.id_pli = data_mvt.id_pli
									GROUP BY data_pli.date_courrier, societe,id_categorie
						
								) as tab1
								where id_categorie is null
								GROUP by date_courrier,id_categorie,societe
			
								)as data
								RIGHT JOIN (
										SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
										FROM
										f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
									) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
									AND data.societe = tab_categorie_dates.societe
						
						) as data_non_traite on data_total.date_courrier = data_non_traite.date_courrier AND data_total.societe = data_non_traite.societe AND data_total.id_categorie = data_non_traite.id_categorie					
						LEFT JOIN
						(
							SELECT daty as date_courrier ,tab_categorie_dates.societe, tab_categorie_dates.id_categorie, tab_categorie_dates.libelle,tab_categorie_dates.nom_societe,
							coalesce(mvt_att_saisie,0) mvt_att_saisie
							FROM
							(
								SELECT
									   data_pli.date_courrier::date AS date_courrier, sum(nb_mvt) as mvt_att_saisie,id_categorie,societe,nom_societe
									  FROM
									 (
										 SELECT pli.id_pli,date_courrier::date as date_courrier,pli.societe,f_categorie_typologie.id_categorie,nom_societe
										 FROM pli
										 INNER JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli                             
										 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
										 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
										 LEFT JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
										 LEFT JOIN f_societe ON f_societe.id = pli.societe	
										 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat." 
										 AND flag_saisie = 0
										 AND pli.flag_traitement IN (1, 2, 3, 4)										 
										 ORDER BY pli.id_pli asc
									) as data_pli
									 LEFT JOIN
									 (
										 SELECT pli.id_pli,count(f_mouvement.id_pli) as nb_mvt
										 FROM pli 
										 INNER JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli                                  
										 INNER JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli
										 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
										 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
										 LEFT JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
										 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat." 
										 AND flag_saisie = 0
										 AND pli.flag_traitement IN (1, 2, 3, 4)	
										 GROUP BY pli.id_pli  
										 ORDER BY pli.id_pli asc
										   
									) as data_mvt on data_pli.id_pli = data_mvt.id_pli
									GROUP BY data_pli.date_courrier, societe,id_categorie,nom_societe,societe
								
							)as data
							RIGHT JOIN (
								SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
								FROM
								f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
							AND data.societe = tab_categorie_dates.societe	
						)as  data_att_saisie on data_total.date_courrier = data_att_saisie.date_courrier AND data_total.societe = data_att_saisie.societe AND data_total.id_categorie = data_att_saisie.id_categorie
						LEFT JOIN
						(
							SELECT tab_categorie_dates.daty as date_courrier ,tab_categorie_dates.societe,tab_categorie_dates.id_categorie,libelle, tab_categorie_dates.nom_societe, 
							coalesce(mvt_en_prod_j,0) mvt_en_prod_j
							FROM
							(
								SELECT
									 data_pli.date_courrier::date AS date_courrier, sum(nb_mvt) as mvt_en_prod_j,id_categorie,societe,nom_societe
									 FROM
									 (
										 SELECT pli.id_pli,date_courrier::date as date_courrier,pli.societe,f_categorie_typologie.id_categorie,nom_societe
										 FROM pli                             
										 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
										 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
										 LEFT JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
										 INNER JOIN f_societe ON f_societe.id = pli.societe	
										 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat."   
										 ORDER BY pli.id_pli asc
									) as data_pli
									 LEFT JOIN
									 (
										 SELECT pli.id_pli,count(f_mouvement.id_pli) as nb_mvt
										 FROM pli                             
										 INNER JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli
										 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
										 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
										 LEFT JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
										 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat."  
										 GROUP BY pli.id_pli  
										 ORDER BY pli.id_pli asc
										   
									) as data_mvt on data_pli.id_pli = data_mvt.id_pli
									GROUP BY data_pli.date_courrier, societe,id_categorie,nom_societe,societe
									
							)as data
								RIGHT JOIN (
									SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
									FROM
									f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
								) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
								AND data.societe = tab_categorie_dates.societe	
						
						)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier AND data_total.societe = data_ttt_j.societe AND data_total.id_categorie = data_ttt_j.id_categorie
						LEFT JOIN
						(
							SELECT
								tab_categorie_dates.daty as date_courrier,
								tab_categorie_dates.societe,
								tab_categorie_dates.nom_societe,
								tab_categorie_dates.libelle,
								tab_categorie_dates.id_categorie, 
								coalesce(mvt_saisie_j,0) as mvt_saisie_j,
								coalesce(mvt_saisie_j_plus_1,0) as mvt_saisie_j_plus_1,
								coalesce(mvt_saisie_j_plus_2,0) as mvt_saisie_j_plus_2,
								coalesce(mvt_saisie_j_plus_plus,0) as mvt_saisie_j_plus_plus
							FROM
							(	
								SELECT
								date_courrier ,societe,id_categorie, sum(mvt_saisie_j) as mvt_saisie_j,sum(mvt_saisie_j_plus_1) as mvt_saisie_j_plus_1,
								sum(mvt_saisie_j_plus_2) as mvt_saisie_j_plus_2,sum(mvt_saisie_j_plus_plus) as mvt_saisie_j_plus_plus
								FROM
								(
									SELECT data_pli.date_courrier,societe,id_categorie,
										CASE WHEN ( data_pli.date_courrier::date = dt_enregistrement::date OR 
												data_pli.date_courrier::date = dt_batch::date) and flag_saisie = 1 
													THEN sum(nb_mvt) ELSE 0
													END as mvt_saisie_j,
										CASE WHEN extract(dow FROM (data_pli.date_courrier::date+ INTERVAL '1 day')::date)::integer = 0 
										THEN 
												   CASE WHEN (( data_pli.date_courrier::date+ INTERVAL '2 day')::date = dt_enregistrement::date OR 
												 (data_pli.date_courrier::date+ INTERVAL '2 day')::date = dt_batch::date) and flag_saisie = 1 
												   THEN sum(nb_mvt) ELSE 0
												   END
										ELSE 
												   CASE WHEN ((data_pli.date_courrier::date+ INTERVAL '1 day')::date = dt_enregistrement::date OR 
												(data_pli.date_courrier::date+ INTERVAL '1 day')::date = dt_batch::date) and flag_saisie = 1
												   THEN sum(nb_mvt) ELSE 0
												   END
										END as mvt_saisie_j_plus_1,
										CASE WHEN extract(dow FROM (data_pli.date_courrier::date+ INTERVAL '2 day')::date) = 0 
										THEN 
												   CASE WHEN ( (data_pli.date_courrier::date+ INTERVAL '3 day')::date = dt_enregistrement::date OR 
												 (data_pli.date_courrier::date+ INTERVAL '3 day')::date = dt_batch::date) and flag_saisie = 1 
												   THEN sum(nb_mvt) ELSE 0
												   END
										ELSE 
												   CASE WHEN ( (data_pli.date_courrier::date+ INTERVAL '2 day')::date = dt_enregistrement::date OR 
												 (data_pli.date_courrier::date+ INTERVAL '2 day')::date = dt_batch::date) and flag_saisie = 1  
												   THEN sum(nb_mvt) ELSE 0
												   END
										END as mvt_saisie_j_plus_2,

										CASE WHEN extract(dow FROM (data_pli.date_courrier::date+ INTERVAL '2 day')::date) = 0 
										THEN 
												   CASE WHEN ( (data_pli.date_courrier::date+ INTERVAL '3 day')::date < dt_enregistrement::date  OR 
												 (data_pli.date_courrier::date+ INTERVAL '3 day')::date < dt_batch::date) and flag_saisie = 1
												   THEN sum(nb_mvt) ELSE 0
												   END
										ELSE 
												   CASE WHEN ( (data_pli.date_courrier::date+ INTERVAL '2 day')::date < dt_enregistrement::date OR 
												 (data_pli.date_courrier::date+ INTERVAL '2 day')::date < dt_batch::date) and flag_saisie = 1
												   THEN sum(nb_mvt) ELSE 0
												   END
										END as mvt_saisie_j_plus_plus     
									FROM
									 (
										 SELECT pli.id_pli,date_courrier::date as date_courrier,pli.societe,f_categorie_typologie.id_categorie
										 FROM pli                             
										 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
										 INNER JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
										 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
										 INNER JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
										 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat." 
										 AND flag_saisie = 1
										 ORDER BY pli.id_pli asc
									) as data_pli
									 LEFT JOIN
									 (
										 SELECT pli.id_pli,count(f_mouvement.id_pli) as nb_mvt
										 FROM pli                             
										 INNER JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli
										 INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
										 LEFT JOIN f_typologie on pli.typologie = f_typologie.id
										 INNER JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
										 WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat." 
										 GROUP BY pli.id_pli  
										 ORDER BY pli.id_pli asc
										   
									) as data_mvt on data_pli.id_pli = data_mvt.id_pli
									  LEFT JOIN (
											   SELECT f_data_pli.id_pli,
											   dt_enregistrement::date,dt_batch::date,flag_saisie,
												date_courrier::date
											   FROM pli                             
											   INNER JOIN f_data_pli ON pli.id_pli = f_data_pli.id_pli
											   INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											   LEFT JOIN f_typologie on pli.typologie = f_typologie.id
											   INNER JOIN f_categorie_typologie on f_categorie_typologie.id_categorie = f_typologie.id_categorie
											   WHERE date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ".$where_soc."  ".$where_cat." 
											   AND flag_saisie = 1
											   ORDER BY pli.id_pli asc                                                                                                                                      
									)as data_saisie_jour on data_saisie_jour.id_pli = data_pli.id_pli
									GROUP BY data_pli.date_courrier, dt_enregistrement,societe,id_categorie,dt_batch::date,flag_saisie

								) as data_saisie
								GROUP BY date_courrier ,societe,id_categorie
							) as data
							RIGHT JOIN (
								SELECT id_categorie, libelle, daty,".$id_societe."  as societe,nom_societe
								FROM
								f_get_date_typologie_societe('".$date_deb."' , '".$date_fin."',".$id_societe.")
							) as tab_categorie_dates on data.id_categorie = tab_categorie_dates.id_categorie AND data.date_courrier = tab_categorie_dates.daty
							AND data.societe = tab_categorie_dates.societe	
							
						)	as data_saisie_jplus on data_saisie_jplus.date_courrier = data_ttt_j.date_courrier AND data_saisie_jplus.societe = data_ttt_j.societe
							AND data_saisie_jplus.id_categorie = data_ttt_j.id_categorie
						WHERE mvt_en_prod_j is not null
					)as data
					WHERE mvt_total > 0 
					ORDER BY societe,date_courrier,id_categorie  asc
					";
				/*echo "<pre>";		 
				print_r($sql);
				echo "</pre>";	exit;
				*/				
				return $this->base_64->query($sql)
							->result_array();

    }	
	
	public function get_solde_sftp_sla($date_deb, $date_fin, $date_j, $id_societe, $id_src){
      $where_soc = ""; 
	  if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
	  if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
	  
	  $sql = "SELECT
					societe, date_reception as date_courrier,pli_total,
					pli_saisie_j,pli_saisie_j_plus_1,pli_saisie_j_plus_2,pli_saisie_j_plus_plus,
					pli_non_traite,pli_att_saisie,nom_societe
				FROM
				(
	  
					SELECT data_total.societe, data_total.date_reception,pli_total/*, pli_en_prod_j*/, 			
					coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins,  
					coalesce(pli_saisie_j, 0) as pli_saisie_j,
					coalesce(pli_saisie_j_plus_1, 0) as pli_saisie_j_plus_1,
					coalesce(pli_saisie_j_plus_2, 0) as pli_saisie_j_plus_2,
					coalesce(pli_saisie_j_plus_plus, 0) as pli_saisie_j_plus_plus,
					coalesce(pli_non_traite, 0) as pli_non_traite,
					coalesce(pli_att_saisie, 0) as pli_att_saisie,
					nom_societe
					
					FROM 
					(
						SELECT date_reception::date as date_reception,  count(id_flux) as pli_total  ,flux.societe, nom_societe
						FROM flux
						INNER join societe on societe.id = flux.societe
						WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 					
						GROUP BY date_reception::date,flux.societe, nom_societe
					
					) as data_total
					LEFT JOIN
					(
						SELECT date_reception::date as date_reception,  count(id_flux) as pli_non_traite ,societe
						FROM flux 
						WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						AND (statut_pli = 0 or statut_pli is null)
						GROUP BY date_reception::date,societe
					
					) as data_non_traite on data_total.date_reception = data_non_traite.date_reception AND data_total.societe = data_non_traite.societe
					LEFT JOIN
					(
						SELECT date_reception::date as date_reception ,flux.societe, COUNT(flux.id_flux) as pli_att_saisie
						FROM  flux
						WHERE 
							date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
							AND id_lot_saisie_flux is null
							AND flux.statut_pli in (1,2,3,4,5,6)
						GROUP BY societe, date_reception::date					
					)as data_att_saisie on data_total.date_reception = data_att_saisie.date_reception AND data_total.societe = data_att_saisie.societe
					LEFT JOIN
					(
						SELECT date_reception::date as date_reception ,flux.societe, COUNT(id_flux) as pli_en_prod_j
						FROM  flux
						WHERE 
							date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						GROUP BY societe, date_reception::date
					
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception AND data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_reception::date as date_reception,societe, count(id_lot_saisie_flux) as pli_saisie_adv_jmoins
						FROM flux
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						WHERE dt::date < '".$date_j."' ".$where_soc." 
						AND id_lot_saisie_flux is not null
						GROUP BY date_reception::date,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_reception = data_ttt_j.date_reception AND data_saisie_advantage1.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT
							date_reception ,societe, sum(pli_saisie_j) as pli_saisie_j,sum(pli_saisie_j_plus_1) as pli_saisie_j_plus_1,
							sum(pli_saisie_j_plus_2) as pli_saisie_j_plus_2,sum(pli_saisie_j_plus_plus) as pli_saisie_j_plus_plus
						FROM
						(
							SELECT date_reception::date as date_reception,societe,
							CASE WHEN ( date_reception::date = dt::date) 
							THEN count(id_lot_saisie_flux) ELSE 0
							END as pli_saisie_j,
							CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '1 day')::date)::integer = 0 
								THEN 
									CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) 
									THEN count(id_lot_saisie_flux) ELSE 0
									END
								ELSE 
									CASE WHEN ( (date_reception::date+ INTERVAL '1 day')::date = dt::date) 
									THEN count(id_lot_saisie_flux) ELSE 0
									END
							END as pli_saisie_j_plus_1,
							CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 
								THEN 
									CASE WHEN ( (date_reception::date+ INTERVAL '3 day')::date = dt::date) 
									THEN count(id_lot_saisie_flux) ELSE 0
									END
								ELSE 
									CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) 
									THEN count(id_lot_saisie_flux) ELSE 0
									END
							END as pli_saisie_j_plus_2,

							CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 
								THEN 
									CASE WHEN ( (date_reception::date+ INTERVAL '3 day')::date < dt::date) 
									THEN count(id_lot_saisie_flux) ELSE 0
									END
								ELSE 
									CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date < dt::date) 
									THEN count(id_lot_saisie_flux) ELSE 0
									END
							END as pli_saisie_j_plus_plus
							FROM flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
							AND id_lot_saisie_flux is not null
							GROUP BY date_reception::date,societe,dt::date
						) as data_saisie
						GROUP BY date_reception ,societe
					)as data_saisie_jplus on data_saisie_jplus.date_reception = data_ttt_j.date_reception AND data_saisie_jplus.societe = data_ttt_j.societe
					
					WHERE pli_en_prod_j is not null
				)as data
				ORDER BY societe,date_reception  ASC				
	  
			";
			
			/*echo "<pre>";
			print_r($sql);
			echo "</pre>";exit;*/
			return $this->ged_flux->query($sql)
							->result_array();

    }

	public function get_suivi_mvt_sftp_sla($date_deb, $date_fin, $date_j, $id_societe,$id_src){
      $where_soc = "";
	   if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
	   if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
	  $sql = "
			SELECT
					societe, date_reception as date_courrier,mvt_total,
					mvt_saisie_j,mvt_saisie_j_plus_1,mvt_saisie_j_plus_2,mvt_saisie_j_plus_plus,
					mvt_non_traite,mvt_att_saisie,nom_societe
				FROM
				(
	  
					SELECT data_total.societe, data_total.date_reception,mvt_total/*, mvt_en_prod_j*/, 			
					coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins,  
					coalesce(mvt_saisie_j, 0) as mvt_saisie_j,
					coalesce(mvt_saisie_j_plus_1, 0) as mvt_saisie_j_plus_1,
					coalesce(mvt_saisie_j_plus_2, 0) as mvt_saisie_j_plus_2,
					coalesce(mvt_saisie_j_plus_plus, 0) as mvt_saisie_j_plus_plus,
					coalesce(mvt_non_traite, 0) as mvt_non_traite,
					coalesce(mvt_att_saisie, 0) as mvt_att_saisie,
					nom_societe
					FROM 
					(
						SELECT date_reception::date as date_reception, 
						sum(nb_abo) as mvt_total ,flux.societe,nom_societe
						FROM flux
						INNER join societe on societe.id = flux.societe
						LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
						WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."
						GROUP BY date_reception::date,flux.societe, nom_societe
					) as data_total
					LEFT JOIN
					(
					SELECT date_reception::date as date_reception, sum(nb_abo) as mvt_non_traite ,societe
					FROM flux
					LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
					WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."
					AND (statut_pli = 0 or statut_pli is null)
					GROUP BY date_reception::date,societe
					) 
					as data_non_traite on data_total.date_reception = data_non_traite.date_reception 
					AND data_total.societe = data_non_traite.societe
					LEFT JOIN
					(
					SELECT date_reception::date as date_reception ,flux.societe, sum(nb_abo) as mvt_att_saisie
					FROM  flux
					LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
					WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."
					AND id_lot_saisie_flux is null
					AND flux.statut_pli in (1,2,3,4,5,6)
					GROUP BY societe, date_reception::date
					)as 
					data_att_saisie on data_total.date_reception = data_att_saisie.date_reception 
					AND data_total.societe = data_att_saisie.societe
					LEFT JOIN(
					SELECT date_reception::date as date_reception ,flux.societe, sum(nb_abo) as mvt_en_prod_j
					FROM  flux
					LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
					WHERE 
					date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."
					GROUP BY societe, date_reception::date
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception 
					AND data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(SELECT date_reception::date as date_reception,societe, sum(nb_abo) as mvt_saisie_adv_jmoins
					FROM flux
					LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
					LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
					WHERE dt::date < '".$date_j."' ".$where_soc." AND id_lot_saisie_flux is not null
					GROUP BY date_reception::date,societe
					)as data_saisie_advantage1 
					on data_saisie_advantage1.date_reception = data_ttt_j.date_reception 
					AND data_saisie_advantage1.societe = data_ttt_j.societe
					LEFT JOIN
					(
					SELECT	date_reception ,societe, sum(mvt_saisie_j) as mvt_saisie_j,
					sum(mvt_saisie_j_plus_1) as mvt_saisie_j_plus_1,
					sum(mvt_saisie_j_plus_2) as mvt_saisie_j_plus_2,
					sum(mvt_saisie_j_plus_plus) as mvt_saisie_j_plus_plus
					FROM
					(
						SELECT date_reception::date as date_reception,societe,CASE WHEN ( date_reception::date = dt::date) 
						THEN sum(nb_abo) ELSE 0	END as mvt_saisie_j,
						CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '1 day')::date)::integer = 0 
							THEN CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) THEN sum(nb_abo) ELSE 0 END
							ELSE CASE WHEN ( (date_reception::date+ INTERVAL '1 day')::date = dt::date) THEN sum(nb_abo) ELSE 0	END	END as mvt_saisie_j_plus_1,
							CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 THEN 
								CASE WHEN ( (date_reception::date+ INTERVAL '3 day')::date = dt::date) THEN sum(nb_abo) ELSE 0
							END	ELSE 
								CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) THEN sum(nb_abo) ELSE 0
							END	END as mvt_saisie_j_plus_2,
							CASE WHEN extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 THEN 
								CASE WHEN ( (date_reception::date+ INTERVAL '3 day')::date < dt::date) THEN sum(nb_abo) ELSE 0
								END	ELSE 
								CASE WHEN ( (date_reception::date+ INTERVAL '2 day')::date < dt::date) THEN sum(nb_abo) ELSE 0
							END	END as mvt_saisie_j_plus_plus
						FROM flux
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
						WHERE date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc."  
						AND id_lot_saisie_flux is not null
						GROUP BY date_reception::date,societe,dt::date
						) as data_saisie
						GROUP BY date_reception ,societe
					)as data_saisie_jplus on data_saisie_jplus.date_reception = data_ttt_j.date_reception 
					AND data_saisie_jplus.societe = data_ttt_j.societe
					WHERE mvt_en_prod_j is not null
					)as data
					ORDER BY societe,date_reception::date  asc";
			/*echo "<pre>";		 
			print_r($sql);
			echo "</pre>";	
			exit;
			*/
			
			return $this->ged_flux->query($sql)->result_array();
			}
	
	public function get_reception_flux_anomalie_sla($date_j, $id_societe, $id_src){
		if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
		if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
		$sql = "SELECT societe,date_reception::date as date_courrier,
				sum(ko_scan) as ko_scan,
				sum(hors_peri) as hors_peri,
				sum(mvt_ko_scan) as mvt_ko_scan,
				sum(mvt_hors_peri) as mvt_hors_peri
				FROM 
				(
					SELECT coalesce(ko_scan,0) as ko_scan, coalesce(hors_peri,0) as hors_peri, 
							coalesce(mvt_ko_scan,0) as mvt_ko_scan, coalesce(mvt_hors_peri,0) as mvt_hors_peri, 
							data_soc.id as societe,
							CASE WHEN date_reception::date is null THEN '".$date_j."'::date ELSE date_reception::date END as date_reception
						FROM 
						(
							SELECT id, societe
							FROM societe
						) as data_soc
						LEFT JOIN 
						(
							SELECT societe.id as societe,date_reception::date as date_reception,
								CASE WHEN flux.statut_pli = ANY (ARRAY[11, 12, 14,15,19,21,22,23]) THEN count(distinct flux.id_flux) ELSE 0 END as ko_scan,
								CASE WHEN flux.statut_pli = 13 THEN count(distinct flux.id_flux) ELSE 0 END as hors_peri,
								CASE WHEN flux.statut_pli = ANY (ARRAY[11, 12, 14,15,19,21,22,23])  THEN sum(nb_abo) ELSE 0 END as mvt_ko_scan,
								CASE WHEN flux.statut_pli = 13 THEN sum(nb_abo) ELSE 0 END as mvt_hors_peri
								FROM flux
								LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
								LEFT JOIN societe on societe.id = flux.societe 
								WHERE date_reception::date = '".$date_j."' ".$where_soc."
								AND flux.statut_pli in (11,12,13,14,15,19,21,22,23)								
							GROUP BY societe.id,date_reception::date,flux.statut_pli
						) as data on data.societe = data_soc.id
				) as res
				GROUP BY  societe,date_reception::date
				ORDER BY  societe,date_reception::date asc
				";
		    /*echo "<pre>";		 
			print_r($sql);
			echo "</pre>";	
			exit;*/
			
			
		return $this->ged_flux->query($sql)
							->result_array();
	}
	
	public function get_saisie_anomalie_flux($date_j, $id_societe, $id_src, $id_categorie){
		$where_soc = $where_cat = '';
		if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
		if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
		if($id_categorie  !='*') $where_cat .= " AND categorie_typologie.id_categorie = '".$id_categorie."'";
		
		$sql = "SELECT societe,id_categorie,date_reception::date as date_courrier,
				sum(ko_scan) as ko_scan,
				sum(hors_peri) as hors_peri,
				sum(mvt_ko_scan) as mvt_ko_scan,
				sum(mvt_hors_peri) as mvt_hors_peri
				FROM 
				(
					SELECT coalesce(ko_scan,0) as ko_scan, coalesce(hors_peri,0) as hors_peri, 
							coalesce(mvt_ko_scan,0) as mvt_ko_scan, coalesce(mvt_hors_peri,0) as mvt_hors_peri, 
							data_soc.id as societe,
							CASE WHEN id_categorie is null THEN 0 ELSE id_categorie END as id_categorie,
							CASE WHEN date_reception::date is null THEN '".$date_j."'::date ELSE date_reception::date END as date_reception
						FROM 
						(
							SELECT id, societe
							FROM societe
						) as data_soc
						LEFT JOIN 
						(
							SELECT societe.id as societe,date_reception::date as date_reception,categorie_typologie.id_categorie,
								CASE WHEN flux.statut_pli = ANY (ARRAY[11, 12, 14,15,19,21,22,23]) THEN count(distinct flux.id_flux) ELSE 0 END as ko_scan,
								CASE WHEN flux.statut_pli = 13 THEN count(distinct flux.id_flux) ELSE 0 END as hors_peri,
								CASE WHEN flux.statut_pli = ANY (ARRAY[11, 12, 14,15,19,21,22,23])  THEN sum(nb_abo) ELSE 0 END as mvt_ko_scan,
								CASE WHEN flux.statut_pli = 13 THEN sum(nb_abo) ELSE 0 END as mvt_hors_peri
								FROM flux
								LEFT JOIN typologie on flux.typologie = typologie.id_typologie
								LEFT JOIN categorie_typologie on categorie_typologie.id_categorie = typologie.id_categorie
								LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
								LEFT JOIN societe on societe.id = flux.societe 
								WHERE date_reception::date = '".$date_j."' ".$where_soc." ".$where_cat."
								AND flux.statut_pli in (11,12,13,14,15,19,21,22,23)								
							GROUP BY societe.id,date_reception::date,flux.statut_pli,categorie_typologie.id_categorie
						) as data on data.societe = data_soc.id
				) as res
				GROUP BY  societe,date_reception::date,id_categorie
				ORDER BY  societe,date_reception::date,id_categorie asc
				";
		    /*echo "<pre>";		 
			print_r($sql);
			echo "</pre>";	
			exit;
			*/
			
			
		return $this->ged_flux->query($sql)
							->result_array();
	}
	
	public function get_suivi_solde_sftp($date_deb, $date_fin, $date_j, $id_societe,$id_src){
      $where_soc = "";
	  if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
	  if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
	  $sql = "	SELECT
					societe,nom_societe, date_reception as date_courrier,pli_total,pli_saisie_adv_jmoins,pli_saisie_adv_j,
					pli_saisie_adv_j_ok,pli_saisie_adv_j_ko
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_reception,pli_total, 			
					coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins, 
					coalesce(pli_saisie_adv_j,0) as pli_saisie_adv_j,
					coalesce(pli_saisie_adv_j_ok,0) as pli_saisie_adv_j_ok,
					coalesce(pli_saisie_adv_j_ko, 0) as pli_saisie_adv_j_ko
					FROM 
					(
						SELECT date_reception::date as date_reception,  count(id_flux) as pli_total  ,flux.societe, nom_societe
						FROM flux
						INNER join societe on societe.id = flux.societe
						WHERE 
						date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						GROUP BY date_reception::date,flux.societe, nom_societe
					
					) as data_total

					LEFT JOIN
					(
						SELECT date_reception::date as date_reception ,flux.societe, COUNT(id_flux) as pli_en_prod_j
						FROM  flux
						WHERE 
							date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						GROUP BY societe,date_reception::date
					
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception AND data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						
						Select adv_total.date_reception,adv_total.societe,
							coalesce(pli_saisie_adv_j,0) pli_saisie_adv_j,
							coalesce(pli_saisie_adv_j_ok,0) pli_saisie_adv_j_ok,
							coalesce(pli_saisie_adv_j_ko,0) pli_saisie_adv_j_ko
						FROM	
						(
							SELECT date_reception::date as date_reception,societe, count(id_lot_saisie_flux) as pli_saisie_adv_j
							FROM flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							WHERE dt::date = '".$date_j."' ".$where_soc." 
							AND id_lot_saisie_flux is not null
							GROUP BY date_reception::date,societe
						)as adv_total
						LEFT JOIN
						(
							SELECT date_reception::date as date_reception,societe, count(id_lot_saisie_flux) as pli_saisie_adv_j_ok
							FROM flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							/*LEFT JOIN abonnement on abonnement.id_flux = flux.id_flux
							LEFT JOIN statut_abonnement on statut_abonnement.id_statut = abonnement.id_statut*/
							WHERE dt::date = '".$date_j."' ".$where_soc." 
							AND id_lot_saisie_flux is not null AND flux.etat_pli_id in ( 1, 100)  
							GROUP BY date_reception::date,societe
						) as adv_ok on adv_ok.date_reception = adv_total.date_reception AND adv_ok.societe = adv_total.societe
						LEFT JOIN(
							SELECT date_reception::date,societe, count(id_lot_saisie_flux) as pli_saisie_adv_j_ko
							FROM flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							/*LEFT JOIN abonnement on abonnement.id_flux = flux.id_flux
							LEFT JOIN statut_abonnement on statut_abonnement.id_statut = abonnement.id_statut*/
							WHERE dt::date = '".$date_j."' ".$where_soc."  
							AND id_lot_saisie_flux is not null AND flux.etat_pli_id in (11,12,14,15,19,21,22,23)
							GROUP BY date_reception::date,societe
						)adv_ko on adv_ko.date_reception = adv_total.date_reception AND adv_ko.societe = adv_total.societe
					
					)as data_saisie_advantage on data_saisie_advantage.date_reception = data_ttt_j.date_reception AND data_saisie_advantage.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_reception::date,societe, count(id_lot_saisie_flux) as pli_saisie_adv_jmoins
						FROM flux
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						WHERE dt::date < '".$date_j."' ".$where_soc." 
						AND id_lot_saisie_flux is not null
						GROUP BY date_reception::date,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_reception = data_ttt_j.date_reception AND data_saisie_advantage1.societe = data_ttt_j.societe
					
					WHERE pli_en_prod_j is not null
				)as data
				ORDER BY societe,date_reception  asc
			";
				 
				return $this->ged_flux->query($sql)
							->result_array();

    }
	public function get_suivi_solde_mvt_sftp($date_deb, $date_fin, $date_j, $id_societe,$id_src){
      $where_soc = "";
	  if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
	  if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
	  $sql = "
			SELECT
					societe,nom_societe, date_reception as date_courrier,mvt_total,mvt_saisie_adv_jmoins,mvt_saisie_adv_j,
					mvt_saisie_adv_j_ok,mvt_saisie_adv_j_ko
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_reception,mvt_total, 			
					coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins, 
					coalesce(mvt_saisie_adv_j,0) as mvt_saisie_adv_j,
					coalesce(mvt_saisie_adv_j_ok,0) as mvt_saisie_adv_j_ok,
					coalesce(mvt_saisie_adv_j_ko, 0) as mvt_saisie_adv_j_ko
					FROM 
					(
						SELECT date_reception::date as date_reception,  count(id_flux) as mvt_total  ,flux.societe, nom_societe
						FROM flux
						INNER join societe on societe.id = flux.societe
						WHERE 
						date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						GROUP BY date_reception::date,flux.societe, nom_societe

					) as data_total

					LEFT JOIN
					(
						SELECT date_reception::date as date_reception ,flux.societe, sum(nb_abo) as mvt_en_prod_j
						FROM  flux
						LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
						INNER join societe on societe.id = flux.societe
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						WHERE 
							date_reception::date between '".$date_deb."' AND '".$date_fin."' ".$where_soc." 
						GROUP BY flux.societe, date_reception::date
					
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception AND data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						
						Select adv_total.date_reception,adv_total.societe,
							coalesce(mvt_saisie_adv_j,0) mvt_saisie_adv_j,
							coalesce(mvt_saisie_adv_j_ok,0) mvt_saisie_adv_j_ok,
							coalesce(mvt_saisie_adv_j_ko,0) mvt_saisie_adv_j_ko
						FROM	
						(
							SELECT date_reception::date as date_reception,societe, sum(nb_abo) as mvt_saisie_adv_j
							FROM flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							WHERE dt::date = '".$date_j."' ".$where_soc." 
							AND id_lot_saisie_flux is not null
							GROUP BY date_reception::date,societe
						)as adv_total
						LEFT JOIN
						(
							SELECT date_reception::date as date_reception,societe,  sum(nb_abo) as mvt_saisie_adv_j_ok
							FROM flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							/*LEFT JOIN abonnement on abonnement.id_flux = flux.id_flux
							LEFT JOIN statut_abonnement on statut_abonnement.id_statut = abonnement.id_statut*/
							WHERE dt::date = '".$date_j."' ".$where_soc." 
							AND id_lot_saisie_flux is not null AND flux.etat_pli_id in ( 1, 100)  
							GROUP BY date_reception::date,societe
						) as adv_ok on adv_ok.date_reception = adv_total.date_reception AND adv_ok.societe = adv_total.societe
						LEFT JOIN(
							SELECT date_reception::date,societe,  sum(nb_abo) as mvt_saisie_adv_j_ko
							FROM flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							/*LEFT JOIN abonnement on abonnement.id_flux = flux.id_flux
							LEFT JOIN statut_abonnement on statut_abonnement.id_statut = abonnement.id_statut*/
							WHERE dt::date = '".$date_j."' ".$where_soc."  
							AND id_lot_saisie_flux is not null AND flux.etat_pli_id in (11,12,14,15,19,21,22,23)
							GROUP BY date_reception::date,societe
						)adv_ko on adv_ko.date_reception = adv_total.date_reception AND adv_ko.societe = adv_total.societe
					
					)as data_saisie_advantage on data_saisie_advantage.date_reception = data_ttt_j.date_reception AND data_saisie_advantage.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_reception::date,societe, sum(nb_abo) as mvt_saisie_adv_jmoins
						FROM flux
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
						WHERE dt::date < '".$date_j."' ".$where_soc." 
						AND id_lot_saisie_flux is not null
						GROUP BY date_reception::date,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_reception = data_ttt_j.date_reception AND data_saisie_advantage1.societe = data_ttt_j.societe
					
					WHERE mvt_en_prod_j is not null
				)as data
				ORDER BY societe,date_reception  asc";
			
				return $this->ged_flux->query($sql)
							->result_array();

    }

	public function get_reception_anomalie_sftp($date_j, $id_societe, $id_src){
		if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
		if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
		$sql = "SELECT coalesce(anomalie,0) as anomalie, 
					data_soc.id as societe,
					CASE WHEN date_reception is null THEN '".$date_j."'::date ELSE date_reception END as date_courrier
				FROM 
				(
					SELECT id, societe
					FROM societe
				) as data_soc
				LEFT JOIN 
				(
					SELECT count(id_flux) as anomalie,flux.societe as societe,date_reception::date as date_reception
						FROM flux
						LEFT JOIN societe on societe.id = flux.societe 
						WHERE date_reception::date = '".$date_j."' ".$where_soc."
						AND flux.statut_pli in (11,12,13,14,15,19,21,22,23)						
					GROUP BY flux.societe,date_reception::date
				) as data on data.societe = data_soc.id
				
				";
				
	
		return $this->ged_flux->query($sql)
							->result_array();
	}
	
	public function get_reception_anomalie_sftp_abo($date_j, $id_societe, $id_src){
		if($id_societe != '') $where_soc = " AND flux.societe = '".$id_societe."'";
		if($id_src != '') $where_soc .= " AND flux.id_source = '".$id_src."'";
		$sql = "SELECT coalesce(anomalie,0) as anomalie, 
				data_soc.id as societe,
						CASE WHEN date_reception is null THEN '".$date_j."'::date ELSE date_reception END as date_courrier
					FROM 
					(
						SELECT id, societe
						FROM societe
					) as data_soc
					LEFT JOIN 
					(
						SELECT sum(nb_abo) as anomalie,societe.id as societe,date_reception::date as date_reception
							FROM flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							LEFT JOIN societe on societe.id = flux.societe
							WHERE  date_reception::date = '".$date_j."' ".$where_soc."
							AND flux.statut_pli in (11,12,13,14,15,19,21,22,23)
						GROUP BY societe.id,date_reception::date
					) as data on data.societe = data_soc.id 
						";
		return $this->ged_flux->query($sql)
							->result_array();
	}
	
	public function get_saisie_par_date_traitement($date_debut, $date_fin, $id_societe){
		
		$sql = "SELECT data_global.dt_event,data_global.societe,pli_saisie_ok,pli_saisie_ci,pli_saisie_ko,
					coalesce(ko_scan,0) as ko_scan,
					coalesce(hp,0) as hp
					from 
					(
						SELECT distinct dt_event::date as dt_event,societe
						FROM histo_pli
						INNER JOIN f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''')  ON f_histo_pli_by_date.id  = histo_pli.id
						LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli
						WHERE  histo_pli.dt_event::date between '".$date_debut."' AND '".$date_fin."' AND societe = '".$id_societe."'
					) as data_global
					LEFT JOIN
					(
						SELECT dt_event, societe,
						sum(pli_saisie_ok) as pli_saisie_ok,
						sum(pli_saisie_ci) as pli_saisie_ci,
						sum(pli_saisie_ko) as pli_saisie_ko
						from 
						(
							SELECT 
								id_pli,dt_event, societe,
								CASE WHEN date_saisie_advantage  between '".$date_debut."' AND '".$date_fin."' AND id_lot_saisie is not null AND statut_saisie = 1 AND flag_traitement != 26 THEN 1 ELSE 0 END as pli_saisie_ok,
								CASE WHEN date_saisie_advantage  between '".$date_debut."' AND '".$date_fin."' AND id_lot_saisie is not null AND statut_saisie = 3 AND flag_traitement != 26 THEN 1 ELSE 0 END as pli_saisie_ci,
								CASE WHEN date_saisie_advantage  between '".$date_debut."' AND '".$date_fin."' AND id_lot_saisie is not null AND statut_saisie not in (1,3) AND flag_traitement != 26 THEN 1 ELSE 0 END as pli_saisie_ko
							from
							(
								SELECT 
									distinct histo_pli.dt_event::date ,
									data_pli.societe, 
									id_lot_saisie,
									data_pli.dt_enregistrement::date as date_saisie_advantage,
									data_pli.statut_saisie as statut_saisie,
									histo_pli.id_pli,
									histo_pli.flag_traitement
								FROM histo_pli
								INNER JOIN f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') ON f_histo_pli_by_date.id  = histo_pli.id
								INNER join view_pli_stat on view_pli_stat.id_pli = histo_pli.id_pli
								LEFT JOIN data_pli on view_pli_stat.id_pli = data_pli.id_pli
								WHERE   histo_pli.dt_event::date between '".$date_debut."' AND '".$date_fin."'  AND data_pli.societe = '".$id_societe."'
								
							) as tab
							GROUP BY id_pli,dt_event::date,date_saisie_advantage,id_lot_saisie,statut_saisie,societe,flag_traitement
						) as ok
						GROUP BY dt_event::date,societe
					) as data_saisie on data_saisie.dt_event = data_global.dt_event AND data_saisie.societe = data_global.societe
					LEFT JOIN
					(
					SELECT
						societe, dt_event, sum(ko_scan) as ko_scan ,sum(hp) as hp
						FROM
						(
							SELECT data_pli.societe as societe,histo_pli.dt_event::date as dt_event,
							CASE WHEN histo_pli.flag_traitement = 16 THEN coalesce(count(distinct histo_pli.id_pli),0) ELSE 0 END as ko_scan,
							CASE WHEN histo_pli.flag_traitement = 21 THEN coalesce(count(distinct histo_pli.id_pli),0) ELSE 0 END as hp
								FROM histo_pli
								INNER JOIN f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') ON f_histo_pli_by_date.id  = histo_pli.id						
								INNER join view_pli_stat on view_pli_stat.id_pli = histo_pli.id_pli
								LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli
								WHERE histo_pli.dt_event::date between  '".$date_debut."' AND '".$date_fin."' AND data_pli.societe = '".$id_societe."'
								AND histo_pli.flag_traitement in (16,21)						
							GROUP BY data_pli.societe,histo_pli.dt_event::date,histo_pli.flag_traitement
						) as res GROUP BY societe,dt_event
						
					) as data_anomalie
					on data_global.dt_event = data_anomalie.dt_event AND data_global.societe = data_anomalie.societe
					ORDER BY data_global.dt_event asc";
		/*echo "<pre>";
		print_r($sql);
		echo "</pre>";exit;
		*/			
		return $this->ged->query($sql)
							->result_array();
		
	}
	
	public function get_saisie_mvt_par_date_traitement($date_debut, $date_fin, $id_societe){
		
		$sql = "SELECT data_global.dt_event,data_global.societe,mvt_saisie_ok,mvt_saisie_ci,mvt_saisie_ko,
					coalesce(ko_scan,0) as mvt_ko_scan,
					coalesce(hp,0) as mvt_hp
					from 
					(
						SELECT distinct dt_event::date as dt_event,societe
						FROM histo_pli
						INNER JOIN f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''')  ON f_histo_pli_by_date.id  = histo_pli.id
						LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli
						WHERE  histo_pli.dt_event::date between '".$date_debut."' AND '".$date_fin."' AND societe = '".$id_societe."'
					) as data_global
					LEFT JOIN
					(
						SELECT dt_event, societe,
						sum(mvt_saisie_ok) as mvt_saisie_ok,
						sum(mvt_saisie_ci) as mvt_saisie_ci,
						sum(mvt_saisie_ko) as mvt_saisie_ko
						from 
						(
							SELECT 
								id_pli,dt_event, societe,
								CASE WHEN date_saisie_advantage  between '".$date_debut."' AND '".$date_fin."' AND id_lot_saisie is not null AND statut_saisie = 1 AND flag_traitement != 26 THEN sum(nb_mvt) ELSE 0 END as mvt_saisie_ok,
								CASE WHEN date_saisie_advantage  between '".$date_debut."' AND '".$date_fin."' AND id_lot_saisie is not null AND statut_saisie = 3 AND flag_traitement != 26 THEN sum(nb_mvt) ELSE 0 END as mvt_saisie_ci,
								CASE WHEN date_saisie_advantage  between '".$date_debut."' AND '".$date_fin."' AND id_lot_saisie is not null AND statut_saisie not in (1,3) AND flag_traitement != 26 THEN sum(nb_mvt) ELSE 0 END as mvt_saisie_ko
							from
							(
								SELECT 
									distinct histo_pli.dt_event::date ,
									data_pli.societe, 
									id_lot_saisie,
									data_pli.dt_enregistrement::date as date_saisie_advantage,
									data_pli.statut_saisie as statut_saisie,
									histo_pli.id_pli,
									histo_pli.flag_traitement,
									nb_mvt
								FROM histo_pli
								INNER JOIN f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') ON f_histo_pli_by_date.id  = histo_pli.id
								INNER join view_pli_stat on view_pli_stat.id_pli = histo_pli.id_pli
								LEFT JOIN data_pli on view_pli_stat.id_pli = data_pli.id_pli
								LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = histo_pli.id_pli
								WHERE   histo_pli.dt_event::date between '".$date_debut."' AND '".$date_fin."'  AND data_pli.societe = '".$id_societe."'
								
							) as tab
							GROUP BY id_pli,dt_event::date,date_saisie_advantage,id_lot_saisie,statut_saisie,societe,flag_traitement
						) as ok
						GROUP BY dt_event::date,societe
					) as data_saisie on data_saisie.dt_event = data_global.dt_event AND data_saisie.societe = data_global.societe
					LEFT JOIN
					(
					SELECT
						societe, dt_event, sum(ko_scan) as ko_scan ,sum(hp) as hp
						FROM
						(
							SELECT data_pli.societe as societe,histo_pli.dt_event::date as dt_event,
							CASE WHEN histo_pli.flag_traitement = 16 THEN coalesce(sum(nb_mvt),0) ELSE 0 END as ko_scan,
							CASE WHEN histo_pli.flag_traitement = 21 THEN coalesce(sum(nb_mvt),0) ELSE 0 END as hp
								FROM histo_pli
								INNER JOIN f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') ON f_histo_pli_by_date.id  = histo_pli.id						
								INNER join view_pli_stat on view_pli_stat.id_pli = histo_pli.id_pli
								LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli
								LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = histo_pli.id_pli
								WHERE histo_pli.dt_event::date between  '".$date_debut."' AND '".$date_fin."' AND data_pli.societe = '".$id_societe."'
								AND histo_pli.flag_traitement in (16,21)						
							GROUP BY data_pli.societe,histo_pli.dt_event::date,histo_pli.flag_traitement
						) as res GROUP BY societe,dt_event
						
					) as data_anomalie
					on data_global.dt_event = data_anomalie.dt_event AND data_global.societe = data_anomalie.societe
					ORDER BY data_global.dt_event asc";
		/*echo "<pre>";
		print_r($sql);
		echo "</pre>";exit;
		*/			
		return $this->ged->query($sql)
							->result_array();
		
	}
	
	
	public function get_pli_courrier_att_saisie($id_societe){
	
		$sql="SELECT date_courrier, societe,
				sum(pli_a_typer) as pli_a_typer, 
				sum(pli_att_saisie) as pli_att_saisie 
				FROM 
				(
					SELECT date_courrier::date as date_courrier ,pli.societe,
					CASE WHEN pli.flag_traitement=0 or pli.flag_traitement is null THEN COUNT(distinct pli.id_pli)  ELSE 0 END as pli_a_typer,
					CASE WHEN pli.flag_traitement = ANY (ARRAY[1,2,3,4]) AND id_lot_saisie is null THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_att_saisie
					FROM  pli
					INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
					LEFT JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli
					INNER JOIN f_societe ON f_societe.id = pli.societe	
					WHERE 
						pli.societe = '".$id_societe."' 
						AND (pli.flag_traitement in (0,1,2,3,4) or pli.flag_traitement is null) 
						AND coalesce(pli.statut_saisie,0) NOT IN (3,8,9,10,4) 
					GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,id_lot_saisie
					having count(pli.id_pli)>0
				) as res
				GROUP BY date_courrier,societe
				ORDER BY date_courrier,societe ASC";
		return $this->base_64->query($sql)
								->result_array();
	}
	public function get_pli_sftp_att_saisie($id_societe, $id_source){
		
		$sql="SELECT date_reception,societe, 
		sum(pli_a_typer) as pli_a_typer, 
		sum(pli_att_saisie) as pli_att_saisie
		FROM 
		(
		SELECT date_reception::date as date_reception ,flux.societe, 
			CASE WHEN flux.statut_pli = 0 or flux.statut_pli  is null THEN COUNT(flux.id_flux) ELSE 0 END as pli_a_typer ,
			CASE WHEN flux.statut_pli > 0 AND id_lot_saisie_flux is null THEN COUNT(flux.id_flux) ELSE 0 END as pli_att_saisie
			FROM  flux
			WHERE 
				flux.societe = '".$id_societe."' AND flux.id_source = '".$id_source."' 
				AND id_lot_saisie_flux is null
				AND flux.statut_pli in (0,1,2,3,4,5,6) AND flux.etat_pli_id not in (15,100) 
			GROUP BY societe, date_reception::date,flux.statut_pli,id_lot_saisie_flux
			having count(flux.id_flux)>0
			) as res
			group by societe, date_reception
			ORDER BY societe, date_reception::date ASC
			";
		return $this->ged_flux->query($sql)
								->result_array();
	}

	//exporter excell
	public function exporterExcel($soc)
	{
		//var_dump($soc);die;
		$sql = "select id_pli, flag_traitement as flag,typologie.typologie,flgtt_etape as etape,flgtt_etat as etat,
		CASE WHEN id_flag_traitement = ANY (ARRAY[2,5,6,9,14]) THEN statut_saisie.libelle ELSE '' END as statut,
		to_char(f_lot_numerisation.date_courrier::date, 'dd-mm-yyyy') as dc
		from f_pli 
		left join typologie on typologie.id = f_pli.typologie
		left join f_lot_numerisation on f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
		left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
		left join statut_saisie on statut_saisie.id_statut_saisie = f_pli.statut_saisie 
		where flag_traitement = 0 AND societe= ".$soc." AND id_lot_saisie IS NULL AND coalesce(statut_saisie,0) NOT IN (3,8,9,10,4) order by f_lot_numerisation.date_courrier::date ASC";
		return $this->ged->query($sql)->result();
	}

	public function exporterExcel2($soc)
	{
		$sql = "select id_pli, flag_traitement as flag,typologie.typologie,flgtt_etape as etape,flgtt_etat as etat,
		CASE WHEN id_flag_traitement = ANY (ARRAY[2,5,6,9,14]) THEN statut_saisie.libelle ELSE '' END as statut,
		to_char(f_lot_numerisation.date_courrier::date, 'dd-mm-yyyy') as dc
		from f_pli 
		left join typologie on typologie.id = f_pli.typologie
		left join f_lot_numerisation on f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
		left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
		left join statut_saisie on statut_saisie.id_statut_saisie = f_pli.statut_saisie 
		where flag_traitement = ANY (ARRAY[1,2,3,4]) AND societe = ".$soc." AND id_lot_saisie IS NULL AND coalesce(statut_saisie,0) NOT IN (3,8,9,10,4) order by f_lot_numerisation.date_courrier::date ASC";
		return $this->ged->query($sql)->result();
	}

	//sftp
	public function sftp($soc, $source, $where)
	{
		// if($clause) {
		// 	$where .= " AND typologie.typologie is not null";
		// }
		$sql = "select 
                id_flux as id_pli,
              
                typologie.typologie as typologie,
                statut_pli.etape_lib as etape,
                statut_pli.etat_lib as etat,
                CASE WHEN statut_pli.id_statut = ANY(ARRAY[2,4,6]) THEN etat_pli.libelle_etat_pli ELSE '' END as statut,
                to_char(date_reception::date, 'dd-mm-yyyy') as dc
               
            from flux 
                left join source on flux.id_source = source.id_source
                left join typologie on typologie.id_typologie = flux.typologie
                left join statut_pli on statut_pli.id_statut = flux.statut_pli
                left join etat_pli on etat_pli.id_etat_pli = flux.etat_pli_id
      where flux.societe = ".$soc." AND flux.id_source = ".$source." AND flux.etat_pli_id not in( 15,100) ".$where." order by date_reception::date ASC";
		
		return $this->ged_flux->query($sql)->result();
	}
	
	public function get_end_month_date($date){
        $sql = "SELECT to_char( ('".$date."'::date+ INTERVAL '1 month'-INTERVAL '1 day')::date, 'DD/MM/YYYY') as date_end";		
        return $this->ged->query($sql)
							->result_array();

    }
	
	public function get_num_date($date){
		$sql = "SELECT EXTRACT(DOW FROM TIMESTAMP '".$date."'::date) as num_date";		
        return $this->ged->query($sql)
							->result_array();

    }
	public function get_date_first($date, $j){
		$sql = "SELECT to_char(('".$date."'::date - INTERVAL '".($j-1)." days')::date, 'DD/MM/YYYY') as daty";		
        return $this->ged->query($sql)
							->result_array();

    }
	public function get_date_last($date, $j){
		$sql = "SELECT to_char(('".$date."'::date - INTERVAL '".$j." days')::date, 'DD/MM/YYYY') as daty";		
        return $this->ged->query($sql)
							->result_array();

    }


}
