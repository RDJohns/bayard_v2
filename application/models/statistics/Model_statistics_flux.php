<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_statistics_flux extends CI_Model{

    private $CI;

    private $ged;
    private $base_64;
    private $ged_flux;
    protected $SELECT            = array('pli','id_pli','typologie','type_ko','flag_traitement','traitement','dt_event','lot_scan','date_courrier','date_numerisation','statut','etape','code_type_pli','comment','nb_doc');
    protected $column_order      = array('date_courrier','date_numerisation','lot_scan','pli','dt_event','comment','statut','etape','nb_doc','id_pli','typologie','type_ko','flag_traitement','traitement','code_type_pli');
    protected $column_search     = array('date_courrier','date_numerisation','dt_event','lot_scan','comment','statut','etape');
    protected $order             = array('date_courrier' => 'asc','date_numerisation' => 'asc','lot_scan' => 'asc','id_pli' => 'asc');

    protected $select_ttt        = array('nom_societe','dt_event','date_courrier','lot_scan','pli','statut','etape','txt_anomalie','libelle_motif','type_ko','typologie','mode_paiement','cmc7','montant','rlmc','infos_datamatrix','motif_rejet','description_rejet','titre_titre','titre_societe_nom','nb_mvt','id_pli','flgtt_etape','flgtt_etat','flgtt_etat_disp','etat');
    protected $select_ttt_flux   = array('nom_societe','source','sujet','id_flux','date_reception','date_dernier_traitement','date_saisie_adv','id_lot_saisie_flux','typologie','statut_pli','lot_advantage','nb_abo');
    protected $column_order_ttt  = array('nom_societe','dt_event','date_courrier','lot_scan','pli','statut','etape','txt_anomalie','libelle_motif','type_ko','typologie','mode_paiement','cmc7','montant','rlmc','infos_datamatrix','motif_rejet','description_rejet','titre_titre','titre_societe_nom','nb_mvt','id_pli');
    protected $column_order_ttt_flux = array('nom_societe','source','sujet','id_flux','date_reception','date_dernier_traitement','date_saisie_adv','id_lot_saisie_flux','typologie','statut_pli','lot_advantage','nb_abo');
    protected $column_search_ttt = array('nom_societe','dt_event','date_courrier','lot_scan','pli','statut','etape','txt_anomalie','libelle_motif','type_ko','typologie','mode_paiement','cmc7','montant','rlmc','infos_datamatrix','motif_rejet','description_rejet','titre_titre','titre_societe_nom','nb_mvt','id_pli');
    protected $column_search_ttt_flux = array('nom_societe','source','sujet','id_flux','date_reception','date_dernier_traitement','date_saisie_adv','id_lot_saisie_flux','typologie','statut_pli','lot_advantage','nb_abo');
    protected $order_ttt         = array('dt_event'=> 'asc','date_courrier'=> 'asc','lot_scan'=> 'asc','pli'=> 'asc','statut'=> 'asc','etape'=> 'asc','libelle'=> 'asc');
    protected $order_ttt_flux    = array('date_reception'=> 'asc','date_dernier_traitement'=> 'asc');

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->ged_flux = $this->load->database('ged_flux', TRUE);
    }


    public function get_dates($date_debut, $date_fin){
        $sql = "SELECT liste_dates('".$date_debut."','".$date_fin."') as daty";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_dates_months($date_debut, $date_fin){
        $sql = "SELECT substr(liste_months::text,1,7) as date_mth FROM liste_months ('".$date_debut."','".$date_fin."')";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_week_of_date($date_debut){
        $sql = "SELECT extract('week' FROM '".$date_debut."'::date) as week ";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_month_of_date($date_debut){
        $sql = "SELECT concat(EXTRACT(YEAR FROM TIMESTAMP '".$date_debut."'),'-',EXTRACT(MONTH FROM TIMESTAMP '".$date_debut."')) as mois;";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_last_month($date_debut){
        $sql = "SELECT (date '".$date_debut."' + interval '1 month')::date as date_fin ";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_list_traitement($date_debut, $date_fin, $id_societe){

         $where_date = $where_view_pli = $where = "";
        if($date_debut != '' && $date_fin != ''){
            //$where_date 	.= "histo_pli.dt_event::date between '".$date_debut."' and '".$date_fin."' ";
            $where_date 	.= "and view_pli_traitement.dt_event::date between '".$date_debut."' and '".$date_fin."' ";

        }
        if($id_societe != ''){
           // $where 	.= " and f_pli.societe = '".$id_societe."' ";
            $where 	.= " and data_pli.societe = '".$id_societe."' ";

        }
		

        $sql = "	SELECT
						f_pli.id_pli, 
						histo_pli.flag_traitement,
						f_pli.flag_traitement as traitement,
						typologie.typologie as code_type_pli,
						histo_pli.dt_event::date,
						case when histo_pli.flag_traitement IS NULL then 'Non traité' else 
						statut_saisie.libelle end as statut,
						view_pli_traitement.flag_traitement as id_etape,
						f_flag_traitement.traitement as etape,
						'' as comment,
						case when data_pli.avec_chq = 1 then data_pli.id_pli else null end AS pli_avec_chq,
						--view_pli_avec_cheque_new.id_pli as pli_avec_chq,
						case when data_pli.statut_saisie in (2,3,4,5,6,7,9) then statut_saisie.libelle else '' end type_ko,
						f_pli.typage_par,
						f_lot_numerisation.lot_scan, 
						f_lot_numerisation.date_courrier, 
						f_pli.pli, 
						typologie.typologie,
						societe.id as id_societe,
						societe.societe as nom_societe,
						paiement_cheque.montant,
						paiement_cheque.cmc7,
						paiement_cheque.rlmc,
						paiement_cheque.anomalie_cheque_id,
						mode_paiement.id_mode_paiement,			
						mode_paiement.mode_paiement,			
						mode_paiement.actif as mode_paiement_actif,
						data_pli.infos_datamatrix,			
						data_pli.titre,			
						data_pli.societe societe_vf,
						data_pli.code_promotion,
						data_pli.motif_rejet,
						data_pli.description_rejet,
						data_pli.statut_saisie,
						data_pli.motif_ko,
						data_pli.autre_motif_ko,
						data_pli.nom_circulaire,
						data_pli.fichier_circulaire,
						data_pli.message_indechiffrable,
						data_pli.dmd_kdo_fidelite_prim_suppl,
						data_pli.dmd_envoi_kdo_adrss_diff,
						data_pli.type_coupon,
						data_pli.id_erreur,
						data_pli.comm_erreur,
						data_pli.nom_orig_fichier_circulaire,
						motif_ko.libelle_motif as libelle_motif,			
						motif_ko_scan.description as txt_anomalie,
						titres.code as titre_id,
						titres.titre as titre_titre,
						titres.societe_id as titre_societe_id,
						societe.nom_societe as titre_societe_nom
															
					FROM histo_pli
					INNER JOIN f_histo_pli_by_last_date_event('".$date_debut."' , '".$date_fin."') view_pli_traitement ON view_pli_traitement.id  = histo_pli.id
					INNER JOIN f_pli ON f_pli.id_pli  = view_pli_traitement.id_pli
					INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
					LEFT JOIN typologie ON typologie.id = f_pli.typologie
					LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli
					LEFT JOIN titres on titres.id = data_pli.titre
					LEFT JOIN societe on societe.id = data_pli.societe
					LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = f_pli.statut_saisie
					LEFT JOIN f_flag_traitement on f_flag_traitement.id_flag_traitement = view_pli_traitement.flag_traitement /* Add last*/
					/*LEFT JOIN view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = data_pli.id_pli */
					--LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli 
					LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
					LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
					LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
					LEFT JOIN motif_ko on motif_ko.id_motif = data_pli.motif_ko
					LEFT JOIN motif_ko_scan on data_pli.id_pli = motif_ko_scan.pli_id
					WHERE 1=1 ".$where_date." ".$where." 						
					
			ORDER BY histo_pli.dt_event, f_lot_numerisation.date_courrier,f_lot_numerisation.lot_scan,f_pli.id_pli  asc
			";
       /* echo "<pre>";
		print_r($sql);
		echo "</pre>";exit;
		*/
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_list_plis($date_debut, $date_fin){

        $where_like = $where_view_pli = "";
        if($date_debut != '' && $date_fin != ''){
            $where_like 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
            $where_view_pli .= " view_lot.date_courrier  between '".$date_debut."' and '".$date_fin."' ";
        }
        $sql = "";
        /*echo "<pre>";
        print_r($sql);
        echo "</pre>";exit;	*/
        return $this->ged->query($sql)
            ->result_array();
    }

    public function get_list_document($date_debut, $date_fin){

        $where_like = $where_view_pli = "";
        if($date_debut != '' && $date_fin != ''){
            $where_like 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
            $where_view_pli .= " date_courrier  between '".$date_debut."' and '".$date_fin."' ";
        }

        $sql = "";
        /*echo "<pre>";
        print_r($sql);
        echo "</pre>";exit;*/
        return $this->ged->query($sql)
            ->result_array();
    }

    public function get_list_plis_traite($date_debut, $date_fin){

        $where_like = $where_view_pli = "";
        if($date_debut != '' && $date_fin != ''){
            $where_like 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
            $where_view_pli .= " view_lot.date_courrier  between '".$date_debut."' and '".$date_fin."' ";
        }

        $sql = "";
        /* echo "<pre>";
       print_r($sql);
       echo "</pre>";exit;*/
        return $this->ged->query($sql)
            ->result_array();
    }


    public function get_stat_plis( $date_debut, $date_fin){

        $where_like = $where_view_pli = "";

        /*if($numcmd != ''){
            $where_like .= " valeur = '".$numcmd."' ";
        }
		*/
        if($date_debut != '' && $date_fin != ''){
            $where_like 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
            $where_view_pli .= " view_lot.date_courrier  between '".$date_debut."' and '".$date_fin."' ";
        }

        $sql = ""	;
        /* echo "<pre>";
         print_r($sql);
         echo "</pre>";exit;*/
        return $this->ged->query($sql)
            ->result_array();

    }

    public function get_plis_recu_par_date($date_debut, $date_fin){

        $where_like = $where_view_pli = "";

        if($date_debut != '' && $date_fin != ''){
            $where_like 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
            $where_view_pli .= " date_courrier  between '".$date_debut."' and '".$date_fin."' ";
        }

        $sql = ""	;

        return $this->ged->query($sql)
            ->result_array();

    }


    public function get_reception_par_date($date_debut, $date_fin){
        /* Lss plis parents qui ont été dupliqués sont flag_ttt à 10 => à ne plus compter */
        $where_like = "";
        if($date_debut != '' && $date_fin != ''){
            $where_like 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
        }

        $sql = "SELECT count(distinct view_pli.id_pli) as nb_plis,date_courrier::date
			FROM view_lot
			INNER join view_pli on view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
			WHERE ".$where_like."
			GROUP BY date_courrier::date
			ORDER BY date_courrier asc"	;

        return $this->ged->query($sql)
            ->result_array();

    }
    public function get_reception_typologie_par_date($date_debut, $date_fin){
        /* Lss plis parents qui ont été dupliqués sont flag_ttt à 10 => à ne plus compter */
        $where_like = "";
        if($date_debut != '' && $date_fin != ''){
            $where_like 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
        }
        $sql = "SELECT count(distinct view_pli.id_pli) as nb_plis,view_pli.typologie
			FROM view_lot
			INNER join view_pli on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
			WHERE ".$where_like."
			GROUP BY view_pli.typologie
			ORDER BY view_pli.typologie asc"	;

        return $this->ged->query($sql)
            ->result_array();

    }
    public function get_stat_traitement( $date_debut, $date_fin,$id_societe,$source){
        $WHERE = $whereClause = "";
        if($source == '3'){
            if($id_societe != '' && $id_societe != null){
                $WHERE = " and view_pli_stat.societe = '".$id_societe."'";
                $whereClause = " and societe = '".$id_societe."'";
            }

            $sql = "
                    SELECT
                        case when
                        sum(pli_traite)+sum(pli_ke) is null
                        then 0 ELSE
                        sum(pli_traite)+sum(pli_ke) END as pli_total,
                        case when SUM(pli_traite) is null then 0 else SUM(pli_traite) end as traite,
                        case when SUM(pli_ok_advantage) is null then 0 else SUM(pli_ok_advantage) end as pli_ok_advantage,
                        case when SUM(pli_ko_scan) is null then 0 else SUM(pli_ko_scan) end as pli_ko_scan,
                        case when SUM(pli_hors_perimetre) is null then 0 else SUM(pli_hors_perimetre) end as pli_hors_perimetre,
                        case when SUM(pli_ks) is null then 0 else SUM(pli_ks) end as pli_ks,
                        case when SUM(pli_kd) is null then 0 else SUM(pli_kd) end as pli_kd,
                        case when SUM(pli_ci) is null then 0 else SUM(pli_ci) end as pli_ci,
                        case when SUM(pli_ok) is null then 0 else SUM(pli_ok) end as pli_ok,
                        case when SUM(pli_ke) is null then 0 else SUM(pli_ke) end as pli_ke
                    FROM
                    (
                        SELECT 
                        case when pli_cloture = 1   then count(distinct id_pli) else 0 end as pli_traite,
                        case when pli_ok = 1   then count(distinct id_pli) else 0 end as pli_ok,
                        case when (date_saisie_advantage between '".$date_debut."' and '".$date_fin."') and id_lot_saisie is not null  then count(distinct id_pli) else 0 end as pli_ok_advantage,
                        case when pli_ko_scan = 1  then count(distinct id_pli) else 0 end as pli_ko_scan,						 
                        case when pli_hors_perimetre = 1 then count(id_pli) else 0 end as pli_hors_perimetre,
                        case when pli_ko_ks = 1 then count(id_pli) else 0 end as pli_ks,
                        case when pli_ko_ke = 1 then count(id_pli) else 0 end as pli_ke,
                        case when pli_ko_kd = 1  then count( id_pli) else 0 end as pli_kd,
                        case when pli_ci = 1 then count(distinct id_pli) else 0 end as pli_ci
                        
                        FROM 
                        (
                            SELECT 
                                view_pli_stat.id_pli, 
                                view_pli_stat.flag_traitement ,
                                view_pli_stat.dt_event::date,
                                pli_cloture,
                                pli_ok,
                                pli_ko_scan,
                                pli_hors_perimetre,
                                pli_ko_ks,
                                pli_ko_kd,
                                pli_ko_ke,
                                pli_ci,
                                id_lot_saisie,
                                date_saisie_advantage							
                            FROM view_pli_stat
                            WHERE 
                            view_pli_stat.dt_event::date between '".$date_debut."' and '".$date_fin."' ".$WHERE."						
                          )
                         as traitement 
                         GROUP BY flag_traitement,flag_traitement,pli_cloture,pli_ok, pli_ko_scan, pli_hors_perimetre, pli_ko_ks, pli_ko_kd, pli_ko_ke, pli_ci,id_lot_saisie,date_saisie_advantage
                     ) as result "	;

            /*echo "<pre>";
            print_r($sql);
            echo "</pre>";exit;
            */

            return $this->ged->query($sql)
                ->result_array();
        }

    }

	//traitement des plis controle/matchage
    public function get_stat_solde( $date_debut, $date_fin,$id_societe,$rubrique){
        $WHERE = $clauseWhere = "";

        if($date_debut != '' && $date_fin != ''){
            $clauseWhere 	.= " view_pli_traitement.dt_event::date between '".$date_debut."' and '".$date_fin."' ";
        }
        if($id_societe != ''){
            $clauseWhere .= " and data_pli.societe = '".$id_societe."' ";

        }
      

        $sql="SELECT	
			dt_event as date_j,societe,
			case when SUM(pli_traite) is null then 0 else SUM(pli_traite) end as traite,
			case when SUM(ok) is null then 0 else SUM(ok) end as ok,
			case when SUM(ko) is null then 0 else SUM(ko) end as ko,
			case when SUM(ko_scan) is null then 0 else SUM(ko_scan) end as ko_scan,
			case when SUM(ko_ks) is null then 0 else SUM(ko_ks) end as ks,
			case when SUM(ko_ke) is null then 0 else SUM(ko_ke) end as ke,
			case when SUM(ko_def) is null then 0 else SUM(ko_def) end as kd,
			case when SUM(ko_inconnu) is null then 0 else SUM(ko_inconnu) end as ko_inconnu,
			case when SUM(ko_abondonne) is null then 0 else SUM(ko_abondonne) end as ko_abondonne,
			case when SUM(ko_circulaire) is null then 0 else SUM(ko_circulaire) end as ci,
			case when SUM(ci_editee) is null then 0 else SUM(ci_editee) end as ci_editee,
			case when SUM(ci_envoyee) is null then 0 else SUM(ci_envoyee) end as ci_envoyee,
			case when SUM(divise) is null then 0 else SUM(divise) end as divise,
			case when SUM(ok_ci) is null then 0 else SUM(ok_ci) end as ok_ci,
			case when SUM(ko_bayard) is null then 0 else SUM(ko_bayard) end as ko_bayard,
			case when SUM(ok_advantage) is null then 0 else SUM(ok_advantage) end as ok_advantage,
			sum(nb_mvt) nb_mvt
			
		FROM
		(
			SELECT 
				id_pli, flag_traitement,typologie,
				 case when (flag_traitement in (9,14)  and statut_saisie = 1) then count(distinct id_pli) else 0 end as ok,
				 case when statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7, 9])  then count(distinct id_pli) else 0 end as ko, 						 
				 case when statut_saisie = 2   then count(distinct id_pli) else 0 end as ko_scan,					 
				 case when statut_saisie = 3  then count( id_pli) else 0 end as ko_def,
				 case when statut_saisie = 4  then count( id_pli) else 0 end as ko_inconnu,
				 case when statut_saisie = 5 then count(id_pli) else 0 end as ko_ks,
				 case when statut_saisie = 6 then count(id_pli) else 0 end as ko_ke,
				 case when  statut_saisie = 7  then count(distinct id_pli) else 0 end as ko_circulaire,
				 case when statut_saisie = 8  then count( id_pli) else 0 end as ko_abondonne,						 
				 case when statut_saisie = 9  then count(distinct id_pli) else 0 end as ci_editee,
				 case when statut_saisie = 10  then count(distinct id_pli) else 0 end as ci_envoyee,
				 case when statut_saisie = 24  then count(distinct id_pli) else 0 end as divise,
				 case when statut_saisie = 11  then count(distinct id_pli) else 0 end as ok_ci,
			     case when statut_saisie = 12  then count(distinct id_pli) else 0 end as ko_bayard,
				 case when (flag_saisie = 1 AND  date_saisie_advantage between '".$date_debut."' and '".$date_fin."')  OR (flag_saisie = 1 and dt_batch between '".$date_debut."' and '".$date_fin."') then count(distinct id_pli) else 0 end as ok_advantage,				 
				 case when (flag_traitement  in(9,14)  and statut_saisie = 1 /*AND  dt_event between '".$date_debut."' and '".$date_fin."'*/)
						or (statut_saisie in (8,10,24)  /*AND  dt_event between '".$date_debut."' and '".$date_fin."'*/)						
						then count(distinct id_pli) else 0 end as pli_traite,
				 case when statut_saisie not in (8,24) then nb_mvt else 0 end nb_mvt,
				 dt_event,
				 societe 
				 
			FROM
			(
			
				SELECT distinct
				histo_pli.dt_event::date,
				dt_batch,
				flag_saisie,				
				histo_pli.id_pli, 
				histo_pli.flag_traitement,
				f_pli.typologie,
				id_lot_saisie,
				data_pli.dt_enregistrement::date AS date_saisie_advantage,
				mvt_nbr as nb_mvt,
				case when data_pli.avec_chq = 1 then data_pli.id_pli else null end AS flag_chq,
				--view_pli_avec_cheque_new.id_pli AS flag_chq,
				data_pli.statut_saisie,
				data_pli.societe
				FROM f_histo_pli_by_last_date_event('".$date_debut."' , '".$date_fin."' ) view_pli_traitement
				INNER JOIN histo_pli ON view_pli_traitement.id  = histo_pli.id and view_pli_traitement.id_pli  = histo_pli.id_pli 
				INNER JOIN f_pli ON f_pli.id_pli  = view_pli_traitement.id_pli
				LEFT JOIN data_pli on view_pli_traitement.id_pli = data_pli.id_pli  
				--LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli  
				WHERE ".$clauseWhere."
				and f_pli.flag_traitement in (7,8,9,11,13,14)
			) as traitement
			GROUP BY id_pli,flag_traitement,typologie,nb_mvt,dt_event,id_lot_saisie,date_saisie_advantage,flag_chq,statut_saisie,societe,dt_batch,flag_saisie

		) as result GROUP BY dt_event,societe
		ORDER BY dt_event asc
		";
		/*echo "<pre>";
		print_r($sql);
		echo "</pre>";exit;*/
        
        return $this->ged->query($sql)
            ->result_array();

    }

	//traitement mouvement controle/matchage
    public function get_stat_solde_mvt( $date_debut, $date_fin,$id_societe,$rubrique){
        $WHERE = $clauseWhere = "";


        if($date_debut != '' && $date_fin != ''){
           // $clauseWhere 	.= " histo_pli.dt_event::date between '".$date_debut."' and '".$date_fin."' ";
            $clauseWhere 	.= " view_pli_traitement.dt_event::date between '".$date_debut."' and '".$date_fin."' ";
        }
        if($id_societe != ''){
            $clauseWhere .= " and data_pli.societe = '".$id_societe."' ";

        }
		
        $sql="SELECT	
			dt_event as date_j,societe,
			case when SUM(pli_traite) is null then 0 else SUM(pli_traite) end as traite,
			case when SUM(ok) is null then 0 else SUM(ok) end as ok,
			case when SUM(ko) is null then 0 else SUM(ko) end as ko,
			case when SUM(ko_scan) is null then 0 else SUM(ko_scan) end as ko_scan,
			case when SUM(ko_ks) is null then 0 else SUM(ko_ks) end as ks,
			case when SUM(ko_ke) is null then 0 else SUM(ko_ke) end as ke,
			case when SUM(ko_def) is null then 0 else SUM(ko_def) end as kd,
			case when SUM(ko_circulaire) is null then 0 else SUM(ko_circulaire) end as ci,
			case when SUM(ko_inconnu) is null then 0 else SUM(ko_inconnu) end as ko_inconnu,
			case when SUM(ko_abondonne) is null then 0 else SUM(ko_abondonne) end as ko_abondonne,
			case when SUM(ci_editee) is null then 0 else SUM(ci_editee) end as ci_editee,
			case when SUM(ci_envoyee) is null then 0 else SUM(ci_envoyee) end as ci_envoyee,
			case when SUM(divise) is null then 0 else SUM(divise) end as divise,
			case when SUM(ok_advantage) is null then 0 else SUM(ok_advantage) end as ok_advantage,
			case when SUM(ok_ci) is null then 0 else SUM(ok_ci) end as ok_ci,
			case when SUM(ko_bayard) is null then 0 else SUM(ko_bayard) end as ko_bayard,
			sum(nb_mvt) nb_mvt
			
		FROM
		(
			SELECT 
				 id_pli, flag_traitement,typologie,
				 case when  (flag_traitement in (9,14)  and statut_saisie = 1) then  sum(mvt_nbr) else 0 end as ok,
				 case when statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7, 9])  then  sum(mvt_nbr) else 0 end as ko, 						 
				 case when statut_saisie = 2   then  sum(mvt_nbr) else 0 end as ko_scan,					 
				 case when statut_saisie = 3  then  sum(mvt_nbr)else 0 end as ko_def,
				 case when statut_saisie = 4  then  sum(mvt_nbr) else 0 end as ko_inconnu,
				 case when statut_saisie = 5 then  sum(mvt_nbr) else 0 end as ko_ks,
				 case when statut_saisie = 6 then  sum(mvt_nbr) else 0 end as ko_ke,
				 case when  statut_saisie = 7  then  sum(mvt_nbr) else 0 end as ko_circulaire,
				 case when statut_saisie = 8  then  sum(mvt_nbr) else 0 end as ko_abondonne,						 
				 case when statut_saisie = 9  then  sum(mvt_nbr) else 0 end as ci_editee,
				 case when statut_saisie = 10  then  sum(mvt_nbr)else 0 end as ci_envoyee,
				 case when statut_saisie = 24  then  sum(mvt_nbr)else 0 end as divise,
				 case when statut_saisie = 11  then count(distinct id_pli) else 0 end as ok_ci,
				 case when statut_saisie = 12  then count(distinct id_pli) else 0 end as ko_bayard,
				 case when (flag_saisie = 1 AND  date_saisie_advantage between '".$date_debut."' and '".$date_fin."')  OR (flag_saisie = 1 and dt_batch between '".$date_debut."' and '".$date_fin."') then  sum(mvt_nbr) else 0 end as ok_advantage,				 
				 case when (flag_traitement  in(9,14) and statut_saisie = 1 /* AND  dt_event between '".$date_debut."' and '".$date_fin."'*/)
						or (statut_saisie in (8,10,24) /*AND  dt_event between '".$date_debut."' and '".$date_fin."'*/)
						then  sum(mvt_nbr) else 0 end as pli_traite,
				 case when statut_saisie not in (8,24) then mvt_nbr else 0 end nb_mvt,
				 dt_event,
				 societe 
				 
			FROM
			(
			
				SELECT distinct
					histo_pli.dt_event::date,
					dt_batch,
					flag_saisie,				
					histo_pli.id_pli, 
					histo_pli.flag_traitement,
					f_pli.typologie,
					id_lot_saisie,
					data_pli.dt_enregistrement::date AS date_saisie_advantage,
					mvt_nbr,
					case when data_pli.avec_chq = 1 then data_pli.id_pli else null end  AS flag_chq,
					--view_pli_avec_cheque_new.id_pli AS flag_chq,
					data_pli.statut_saisie,
					data_pli.societe
				FROM f_histo_pli_by_last_date_event('".$date_debut."' , '".$date_fin."' ) view_pli_traitement
				INNER JOIN histo_pli ON view_pli_traitement.id  = histo_pli.id and view_pli_traitement.id_pli  = histo_pli.id_pli 
				INNER JOIN f_pli ON f_pli.id_pli  = view_pli_traitement.id_pli
				LEFT JOIN data_pli on view_pli_traitement.id_pli = data_pli.id_pli 
				---LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli  
				WHERE ".$clauseWhere."
				and f_pli.flag_traitement in (7,8,9,11,13,14)
				
				
			) as traitement
			GROUP BY id_pli,flag_traitement,typologie,nb_mvt,dt_event,id_lot_saisie,date_saisie_advantage,flag_chq,statut_saisie,societe,flag_saisie,dt_batch

		) as result GROUP BY dt_event,societe
		ORDER BY dt_event asc
		";
       /* echo "<pre>";
        print_r($sql);
        echo "</pre>";exit;*/
        
        return $this->ged->query($sql)
            ->result_array();

    }


    public function get_stat_solde_initial( $date_debut,$id_societe,$rubrique){
        $WHERE = $ClauseWhere = "";
        if($id_societe != ""){
            $ClauseWhere = " and societe = '".$id_societe."'";
        }
        $sql= "SELECT
					sum(pli_non_traite_reception_j) as pli_non_traite_reception_j,
					sum(pli_non_traite_reception_jmoins) as pli_non_traite_reception_jmoins
				FROM
				(SELECT 
					pli_non_traite_reception_j, 
					pli_non_traite_reception_jmoins
				FROM solde_plis_mvt_journalier
				WHERE 
					date_j = ('".$date_debut."'::date- interval '1 day')::date 
					".$ClauseWhere."
					and rubrique = ".$rubrique."
			    ) as tab ";
        /*echo "<pre>";
        print_r($sql);
        echo "</pre>";exit;*/

        return $this->ged->query($sql)
            ->result_array();

    }


    public function get_traitement_ko($date_debut, $date_fin,$id_societe,$source){

        if($source == '3'){
        $where_date = $where_date_total = $where_soc="";
        $where_date_adv = "";
        if($date_debut != '' && $date_fin != ''){
            $where_date 	.= " f.dt_event::date between '".$date_debut."' and '".$date_fin."' ";
            $where_date_total 	.= " date_courrier::date between '".$date_debut."' and '".$date_fin."' ";
            $where_date_adv = " between '".$date_debut."' and '".$date_fin."'";
        }
        if($id_societe != ''){
            $where_soc   = " and data_pli.societe = '".$id_societe."' ";
            $where_date_total .= " and view_pli.societe = '".$id_societe."' ";
            $soc = $id_societe;
        }

        $sql="SELECT	
					case when SUM(pli_traite) is null then 0 else SUM(pli_traite) end as traite,
					case when SUM(ok) is null then 0 else SUM(ok) end as ok,
					case when SUM(ko) is null then 0 else SUM(ko) end as ko,
					case when SUM(ko_scan) is null then 0 else SUM(ko_scan) end as ko_scan,
					case when SUM(ko_ks) is null then 0 else SUM(ko_ks) end as ko_ks,
					case when SUM(ko_ke) is null then 0 else SUM(ko_ke) end as ko_ke,
					case when SUM(ko_def) is null then 0 else SUM(ko_def) end as ko_def,
					case when SUM(ko_inconnu) is null then 0 else SUM(ko_inconnu) end as ko_inconnu,
					case when SUM(ko_abondonne) is null then 0 else SUM(ko_abondonne) end as ko_abondonne,
					case when SUM(ko_circulaire) is null then 0 else SUM(ko_circulaire) end as ko_circulaire,
					case when SUM(ci_editee) is null then 0 else SUM(ci_editee) end as ci_editee,
					case when SUM(ci_envoyee) is null then 0 else SUM(ci_envoyee) end as ci_envoyee,
					case when SUM(divise) is null then 0 else SUM(divise) end as divise,
					case when SUM(saisie_j) is null then 0 else SUM(saisie_j) end as ok_advantage,
					case when SUM(ok_circ) is null then 0 else SUM(ok_circ) end as ok_circ,
					case when SUM(ko_bayard) is null then 0 else SUM(ko_bayard) end as ko_bayard,
					case when SUM(cloture_ok) is null then 0 else SUM(cloture_ok) end as cloture_ok,
					case when SUM(mvt_ok) is null then 0 else SUM(mvt_ok) end as mvt_ok,
					case when SUM(mvt_ko) is null then 0 else SUM(mvt_ko) end as mvt_ko,
					case when SUM(mvt_ko_scan) is null then 0 else SUM(mvt_ko_scan) end as mvt_ko_scan,
					case when SUM(mvt_ko_ks) is null then 0 else SUM(mvt_ko_ks) end as mvt_ko_ks,
					case when SUM(mvt_ko_ke) is null then 0 else SUM(mvt_ko_ke) end as mvt_ko_ke,
					case when SUM(mvt_ko_def) is null then 0 else SUM(mvt_ko_def) end as mvt_ko_def,
					case when SUM(mvt_ko_inconnu) is null then 0 else SUM(mvt_ko_inconnu) end as mvt_ko_inconnu,
					case when SUM(mvt_ko_abondonne) is null then 0 else SUM(mvt_ko_abondonne) end as mvt_ko_abondonne,
					case when SUM(mvt_ko_circulaire) is null then 0 else SUM(mvt_ko_circulaire) end as mvt_ko_circulaire,
					case when SUM(mvt_ci_editee) is null then 0 else SUM(mvt_ci_editee) end as mvt_ci_editee,
					case when SUM(mvt_ci_envoyee) is null then 0 else SUM(mvt_ci_envoyee) end as mvt_ci_envoyee,
					case when SUM(mvt_divise) is null then 0 else SUM(mvt_divise) end as mvt_divise,
					case when SUM(mvt_cloture_ok) is null then 0 else SUM(mvt_cloture_ok) end as mvt_cloture_ok,
					case when SUM(mvt_ok_advantage) is null then 0 else SUM(mvt_ok_advantage) end as mvt_ok_advantage,
					case when SUM(mvt_ko_bayard) is null then 0 else SUM(mvt_ko_bayard) end as mvt_ko_bayard,
					case when SUM(mvt_ok_circ) is null then 0 else SUM(mvt_ok_circ) end as mvt_ok_circ,
					case when SUM(mvt_pli_traite) is null then 0 else SUM(mvt_pli_traite) end as mvt_pli_traite,
						(
							SELECT count(distinct data_pli.id_pli)||','||sum(nb_mvt) as pli_total 
							FROM histo_pli
							INNER join data_pli on data_pli.id_pli = histo_pli.id_pli
							LEFT JOIN view_nb_mouvement_pli on data_pli.id_pli = view_nb_mouvement_pli.id_pli
							WHERE dt_event::date ".$where_date_adv.$where_soc." 
						) as pli_total_en_traitement
				FROM
				(
					SELECT id_pli, flag_traitement,typologie,
						 case when (flag_traitement in (9,14) and statut_saisie = 1) then count(distinct id_pli) else 0 end as ok,
						 case when statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7, 9])  then count(distinct id_pli) else 0 end as ko, 						 
						 case when statut_saisie = 2   then count(distinct id_pli) else 0 end as ko_scan,
						 case when statut_saisie = 3  then count( id_pli) else 0 end as ko_def,
						 case when statut_saisie = 4  then count( id_pli) else 0 end as ko_inconnu,
						 case when statut_saisie = 5 then count(id_pli) else 0 end as ko_ks,
						 case when statut_saisie = 6 then count(id_pli) else 0 end as ko_ke,
						 case when  statut_saisie = 7  then count(distinct id_pli) else 0 end as ko_circulaire,
						 case when statut_saisie = 8  then count( id_pli) else 0 end as ko_abondonne,						 
						 case when statut_saisie = 9  then count(distinct id_pli) else 0 end as ci_editee,
						 case when statut_saisie = 10  then count(distinct id_pli) else 0 end as ci_envoyee,
						 case when statut_saisie = 24  then count(distinct id_pli) else 0 end as divise,
						 case when statut_saisie = 11  then count(distinct id_pli) else 0 end as ok_circ,
						 case when statut_saisie = 12  then count(distinct id_pli) else 0 end as ko_bayard,
						 case when (flag_traitement in (9,14) and statut_saisie = 1)  then count(distinct id_pli) else 0 end as cloture_ok,
						case when (flag_traitement in (9,14) and statut_saisie = 1)
						or ( statut_saisie in (8,10,24)) then count(distinct id_pli) else 0 end as pli_traite,
						case when (flag_traitement in (9,14) and statut_saisie = 1) then sum(nb_mvt) else 0 end as mvt_ok,
						case when statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7, 9]) then sum(nb_mvt) else 0 end as mvt_ko, 						 
						case when statut_saisie = 2   then sum(nb_mvt) else 0 end as mvt_ko_scan,
						case when statut_saisie = 3  then sum( nb_mvt) else 0 end as mvt_ko_def,
						case when statut_saisie = 4  then sum( nb_mvt) else 0 end as mvt_ko_inconnu,
						case when statut_saisie = 5 then sum(nb_mvt) else 0 end as mvt_ko_ks,
						case when statut_saisie = 6 then sum(nb_mvt) else 0 end as mvt_ko_ke,
						case when statut_saisie = 7  then sum(nb_mvt) else 0 end as mvt_ko_circulaire,
						case when statut_saisie = 8  then sum( nb_mvt) else 0 end as mvt_ko_abondonne,
						case when statut_saisie = 9  then sum(nb_mvt) else 0 end as mvt_ci_editee,
						case when statut_saisie = 10  then sum(nb_mvt) else 0 end as mvt_ci_envoyee,
						case when statut_saisie = 24  then sum(nb_mvt) else 0 end as mvt_divise,
						case when (flag_traitement in (9,14) and statut_saisie = 1) then sum(nb_mvt) else 0 end as mvt_cloture_ok,
						case when flag_saisie = 1 and (date_saisie_advantage  ".$where_date_adv.") or  (dt_batch ".$where_date_adv.")
						then sum(nb_mvt) else 0 end as mvt_ok_advantage,
						case when ( flag_traitement  in(9,14)  and statut_saisie = 1)
						or ( statut_saisie in (8,10,24)) then sum(nb_mvt) else 0 end as mvt_pli_traite,
						case when flag_saisie = 1 and (date_saisie_advantage ".$where_date_adv." or dt_batch ".$where_date_adv." ) then count(distinct id_pli) else 0 end as saisie_j,
						case when statut_saisie = 11  then sum(nb_mvt) else 0 end as mvt_ok_circ,
						case when statut_saisie = 12 then sum(nb_mvt) else 0 end as mvt_ko_bayard
						 
					FROM (
					
						SELECT distinct
							f.dt_event::date,
							dt_batch,							
							f.id_pli, 
							f.flag_traitement,
							f_pli.typologie as code_type_pli,
							pli,
							f_pli.typologie,
							--view_pli_avec_cheque_new.id_pli as pli_avec_chq,
							case when data_pli.avec_chq = 1 then data_pli.id_pli else null end AS pli_avec_chq,
							data_pli.dt_enregistrement::date as date_saisie_advantage,
							mvt_nbr as nb_mvt,
							--view_pli_avec_cheque_new.id_pli AS flag_chq,
							case when data_pli.avec_chq = 1 then data_pli.id_pli else null end  AS flag_chq,
							data_pli.statut_saisie,
							flag_saisie,
							data_pli.societe
						FROM f_histo_pli_by_last_date_event('".$date_debut."' , '".$date_fin."') f
					    INNER JOIN histo_pli ON f.id  = histo_pli.id and f.id_pli  = histo_pli.id_pli 
					    INNER JOIN f_pli ON f_pli.id_pli  = f.id_pli
					    INNER JOIN data_pli on f.id_pli = data_pli.id_pli 
						WHERE ".$where_date.$where_soc." 	
						
						
								
					) as traitement
					GROUP BY id_pli,pli,flag_traitement,typologie,date_saisie_advantage,statut_saisie,flag_chq,flag_saisie,dt_batch
				) as result";
        //echo "<pre>";
        //print_r($sql);
        //echo "</pre>";exit;
		
        
        return $this->ged->query($sql)
            ->result_array();
        }
        else{

        if($id_societe != '' && $id_societe != null && $source != '' && $source != null){
            $WHERE = " and societe = '".$id_societe."' and flux.id_source = ".$source ;
        }

       /* $sql = "
                    SELECT
                sum(saisie_finie) as saisie_finie,
                sum(flux_ok) as flux_ok,
                sum(hors_perimetre) as hors_perimetre,
                sum(anomalie) as anomalie,
                sum(escalade) as escalade,
                sum(transfert_consigne) as transfert_consigne,
                sum(abo_saisie_finie) as abo_saisie_finie,
                sum(abo_ok) as abo_ok,
                sum(abo_hors_perimetre) as abo_hors_perimetre,
                sum(abo_anomalie) as abo_anomalie,
                sum(abo_escalade) as abo_escalade,
                sum(abo_transfert_consigne) as abo_transfert_consigne
                FROM
                (              
                        SELECT id_flux, 
                                       case when statut_pli = 7 and id_lot_saisie_flux is not null and date_saisie between '".$date_debut."' and '".$date_fin."'  then 1 else 0 end as saisie_finie,
                                       case when statut_pli = 7 then 1 else 0 end as flux_ok,
                                       case when statut_pli = 13 then 1 else 0 end as hors_perimetre,
                                       case when statut_pli = ANY (ARRAY[11, 12, 14, 15]) then 1 else 0 end as anomalie,
                                       case when statut_pli = ANY (ARRAY[4, 5]) then 1 else 0 end as escalade,
                                       case when statut_pli = ANY (ARRAY[8, 9, 10]) then 1 else 0 end as transfert_consigne,
                                       case when statut_pli = 7 and id_lot_saisie_flux is not null and date_saisie between '".$date_debut."' and '".$date_fin."'  then nb_abo else 0 end as abo_saisie_finie,
                                       case when statut_pli = 7 then nb_abo else 0 end as abo_ok,
                                       case when statut_pli = 13 then nb_abo else 0 end as abo_hors_perimetre,
                                       case when statut_pli = ANY (ARRAY[11, 12, 14, 15]) then nb_abo else 0 end as abo_anomalie,
                                       case when statut_pli = ANY (ARRAY[4, 5]) then nb_abo else 0 end as abo_escalade,
                                       case when statut_pli = ANY (ARRAY[8, 9, 10]) then nb_abo else 0 end as abo_transfert_consigne
                        FROM
                        (
                        SELECT  traitement.id_flux, flag_anomalie_pj, flag_hors_perimetre, 
                               flag_traitement_niveau, view_traitement_oid.statut_pli, 
                               flux.typologie,id_lot_saisie_flux,
                               view_traitement_oid.date_traitement::date as dt_event,
                               view_lot_saisie_flux.dt::date as date_saisie,
                               nb_abo
                          FROM public.flux
                          inner join traitement on flux.id_flux = traitement.id_flux
                          inner join view_traitement_oid on traitement.oid = view_traitement_oid.oid
                          left join view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
                          left join view_lot_saisie_flux on view_lot_saisie_flux.id = flux.id_lot_saisie_flux
                          where traitement.date_traitement::date between '".$date_debut."' and '".$date_fin."' 
                          ".$WHERE." 
                          ) as data_flux
                          GROUP BY id_flux,statut_pli,id_lot_saisie_flux,date_saisie,nb_abo
                  ) as data
			"	;*/
		$sql = "SELECT
					sum(saisie_finie) as saisie_finie,
					sum(flux_ok) as flux_ok,
					sum(hors_perimetre) as hors_perimetre,
					sum(anomalie) as anomalie,
					sum(escalade) as escalade,
					sum(transfert_consigne) as transfert_consigne,
					sum(ko_def) as ko_def,
					sum(ko_inconnu) as ko_inconnu,
					sum(ko_ettent) as ko_ettent,
					sum(abo_saisie_finie) as abo_saisie_finie,
					sum(abo_ok) as abo_ok,
					sum(abo_hors_perimetre) as abo_hors_perimetre,
					sum(abo_anomalie) as abo_anomalie,
					sum(abo_escalade) as abo_escalade,
					sum(abo_transfert_consigne) as abo_transfert_consigne,
					sum(abo_ko_def) as abo_ko_def,
					sum(abo_ko_inconnu) as abo_ko_inconnu,
					sum(abo_ko_ettent) as abo_ko_ettent,
					sum(ko_bayard) as ko_bayard,
					sum(abo_ko_bayard) as abo_ko_bayard
				FROM
				(              
						SELECT id_flux, 
							case when statut_pli = 7 and id_lot_saisie_flux is not null and date_saisie between '$date_debut' and '$date_fin'  then 1 else 0 end as saisie_finie,
							case when statut_pli = ANY (ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) and etat_pli_id = 1 then 1 else 0 end as flux_ok,
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 13  then 1 else 0 end as hors_perimetre,
							case when statut_pli =  ANY (ARRAY[2, 7]) and etat_pli_id = ANY (ARRAY[11, 12, 14, 15]) then 1 else 0 end as anomalie,
							case when statut_pli = ANY (ARRAY[4, 5]) then 1 else 0 end as escalade,
							case when statut_pli = ANY (ARRAY[8, 9, 10]) then 1 else 0 end as transfert_consigne,
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 21 then 1 else 0 end as ko_def,
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 22 then 1 else 0 end as ko_inconnu,
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 23 then 1 else 0 end as ko_ettent,
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 24 then 1 else 0 end as ko_bayard,
							case when statut_pli = 7 and id_lot_saisie_flux is not null and date_saisie between '$date_debut' and '$date_fin'  then nb_abo else 0 end as abo_saisie_finie,
							case when statut_pli = ANY (ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) and etat_pli_id = 1 then nb_abo else 0 end as abo_ok,
							case when statut_pli = 13 then nb_abo else 0 end as abo_hors_perimetre,
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = ANY (ARRAY[11, 12, 14, 15]) then nb_abo else 0 end as abo_anomalie,
							case when statut_pli = ANY (ARRAY[4, 5]) then nb_abo else 0 end as abo_escalade,
							case when statut_pli = ANY (ARRAY[8, 9, 10]) then nb_abo else 0 end as abo_transfert_consigne,
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 21 then nb_abo else 0 end as abo_ko_def,
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 22 then nb_abo else 0 end as abo_ko_inconnu,
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 23 then nb_abo else 0 end as abo_ko_ettent,	 
							case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 24 then nb_abo else 0 end as abo_ko_bayard	 

						FROM
						(
						SELECT  traitement.id_flux, flag_anomalie_pj, flag_hors_perimetre, 
							flag_traitement_niveau,
							flux.statut_pli, --traitement.statut_pli, 

							flux.typologie,id_lot_saisie_flux,
							traitement.date_traitement::date as dt_event,

							view_lot_saisie_flux.dt::date as date_saisie,
							nb_abo,/*etat_flux_h as*/ etat_pli_id,id_traitement
						FROM public.flux
						inner join traitement on flux.id_flux = traitement.id_flux and id_traitement = (select id_traitement from traitement where id_flux = flux.id_flux order by id_traitement desc limit 1)
						left join view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
						left join view_lot_saisie_flux on view_lot_saisie_flux.id = flux.id_lot_saisie_flux
						where traitement.date_traitement::date between '$date_debut' and '$date_fin' 
						$WHERE --and societe = 1 and flux.id_source = 1
						) as data_flux
						GROUP BY id_flux,statut_pli,id_lot_saisie_flux,date_saisie,nb_abo,etat_pli_id
				) as data";

        /*echo "<pre>";
        print_r($sql);
        echo "</pre>";exit;*/


        return $this->ged_flux->query($sql)
            ->result_array();
        }
    }

    public function get_stat_mensuel( $date_debut, $date_fin ,$id_soc){

        $sql = "SELECT 	concat( /*date_part('month',data_daty.daty)*/
							case when date_part('month',data_daty.daty) = 1 then 'Janvier' else
							case when date_part('month',data_daty.daty) = 2 then 'Février' else
							case when date_part('month',data_daty.daty) = 3 then 'Mars' else
							case when date_part('month',data_daty.daty) = 4 then 'Avril' else
							case when date_part('month',data_daty.daty) = 5 then 'Mai' else
							case when date_part('month',data_daty.daty) = 6 then 'Juin' else
							case when date_part('month',data_daty.daty) = 7 then 'Juillet' else
							case when date_part('month',data_daty.daty) = 8 then 'Aout' else
							case when date_part('month',data_daty.daty) = 9 then 'Septembre' else
							case when date_part('month',data_daty.daty) = 10 then 'Octobre' else
							case when date_part('month',data_daty.daty) = 11 then 'Novembre' else
							case when date_part('month',data_daty.daty) = 12 then 'Décembre' end
							end end end end end end end end end end end
							,' ',date_part('year',data_daty.daty)) as lib_daty,
							data_daty.daty,
                            case when data_global.non_traite is null then 0 else data_global.non_traite::integer end as non_traite,
                            case when encours is null then 0 else encours::integer end,
                            case when traite is null then 0 else traite::integer end,
							case when anomalie is null then 0 else anomalie::integer end,							data_global.nb_pli_ok,data_global.nb_pli_ko,data_global.nb_pli_ci,data_global.nb_pli_ks,data_global.nb_pli_kd,data_global.mouvement_ok,data_global.mouvement_ano,data_global.mouvement_ci, data_global.mouvement_ks,data_global.mouvement_kd,							
							nb_pli_2,nb_pli_3,nb_pli_4,nb_pli_5,nb_pli_6,nb_pli_7,nb_pli_8,nb_pli_9,nb_pli_10,
							nb_pli_11,nb_pli_12,nb_pli_13,nb_pli_14,nb_pli_15,nb_pli_16,nb_pli_17,nb_pli_18,nb_pli_19,nb_pli_20,
							nb_pli_21,nb_pli_22,nb_pli_23,nb_pli_24,nb_pli_25,nb_pli_26,nb_pli_27,
							nb_mvt_2,nb_mvt_3,nb_mvt_4,nb_mvt_5,nb_mvt_6,nb_mvt_7,nb_mvt_8,nb_mvt_9,nb_mvt_10,
							nb_mvt_11,nb_mvt_12,nb_mvt_13,nb_mvt_14,nb_mvt_15,nb_mvt_16,nb_mvt_17,nb_mvt_18,nb_mvt_19,nb_mvt_20,
							nb_mvt_21,nb_mvt_22,nb_mvt_23,nb_mvt_24,nb_mvt_25,nb_mvt_26,nb_mvt_27,							nb_cltr_pli_2,nb_cltr_pli_3,nb_cltr_pli_4,nb_cltr_pli_5,nb_cltr_pli_6,nb_cltr_pli_7,nb_cltr_pli_8,nb_cltr_pli_9,nb_cltr_pli_10,							nb_cltr_pli_11,nb_cltr_pli_12,nb_cltr_pli_13,nb_cltr_pli_14,nb_cltr_pli_15,nb_cltr_pli_16,nb_cltr_pli_17,nb_cltr_pli_18,nb_cltr_pli_19,nb_cltr_pli_20,
							nb_cltr_pli_21,nb_cltr_pli_22,nb_cltr_pli_23,nb_cltr_pli_24,nb_cltr_pli_25,nb_cltr_pli_26,nb_cltr_pli_27,		nb_cltr_mvt_2,nb_cltr_mvt_3,nb_cltr_mvt_4,nb_cltr_mvt_5,nb_cltr_mvt_6,nb_cltr_mvt_7,nb_cltr_mvt_8,nb_cltr_mvt_9,nb_cltr_mvt_10,						nb_cltr_mvt_11,nb_cltr_mvt_12,nb_cltr_mvt_13,nb_cltr_mvt_14,nb_cltr_mvt_15,nb_cltr_mvt_16,nb_cltr_mvt_17,nb_cltr_mvt_18,nb_cltr_mvt_19,nb_cltr_mvt_20,
							nb_cltr_mvt_21,nb_cltr_mvt_22,nb_cltr_mvt_23,nb_cltr_mvt_24,nb_cltr_mvt_25,nb_cltr_mvt_26,nb_cltr_mvt_27,nb_att_en_cours,nb_controle_en_cours,nb_saisie_en_cours,nb_typage_en_cours,nb_rettt_en_cours,mvt_att_en_cours,mvt_controle_en_cours,mvt_saisie_en_cours,mvt_typage_en_cours,mvt_rettt_en_cours,
							nb_non_traite,nb_en_cours,mvt_non_traite,mvt_en_cours
						FROM
						 (
							SELECT liste_months as daty FROM liste_months ('".$date_debut."','".$date_fin."')
						 ) as data_daty
						LEFT JOIN 
						(	SELECT
								daty,
								SUM(non_traite) non_traite,
								SUM(encours) encours,
								SUM(traite) traite,
								SUM(anomalie) anomalie,
								SUM(nb_pli_ok) nb_pli_ok,
								SUM(nb_pli_ko) nb_pli_ko,
								SUM(nb_pli_ci) nb_pli_ci,
								SUM(nb_pli_ks) nb_pli_ks,
								SUM(nb_pli_kd) nb_pli_kd,
								SUM(mouvement_ok) mouvement_ok,
								SUM(mouvement_ano) mouvement_ano,
								SUM(mouvement_ci) mouvement_ci,
								SUM(mouvement_ks) mouvement_ks,
								SUM(mouvement_kd) mouvement_kd
							FROM
							(
								SELECT daty,
								case when rubrique ='Non traité' then nb_pli::integer else 0 end  as non_traite,
								case when rubrique ='Encours' then nb_pli::integer else 0 end as encours,
								case when rubrique ='Cloturé' then nb_pli::integer else 0 end as traite,
								case when rubrique ='Anomalie' then nb_pli::integer else 0 end as anomalie,				
								case when rubrique ='Traité OK' then nb_pli::integer else 0 end as nb_pli_ok,				
								case when rubrique ='KO Scan HP' then nb_pli::integer else 0 end as nb_pli_ko,				
								case when rubrique ='Traité CI' then nb_pli::integer else 0 end as nb_pli_ci,				
								case when rubrique ='Traité KS' then nb_pli::integer else 0 end as nb_pli_ks,				
								case when rubrique ='Traité KD' then nb_pli::integer else 0 end as nb_pli_kd,								
								case when rubrique ='Traité OK' then nb_mvt::integer else 0 end as mouvement_ok,				
								case when rubrique ='KO Scan HP' then nb_mvt::integer else 0 end as mouvement_ano,				
								case when rubrique ='Traité CI' then nb_mvt::integer else 0 end as mouvement_ci,				
								case when rubrique ='Traité KS' then nb_mvt::integer else 0 end as mouvement_ks,				
								case when rubrique ='Traité KD' then nb_mvt::integer else 0 end as mouvement_kd					
								FROM stat_mensuel
								WHERE titre = 'GLOBAL' 
								and daty between '".$date_debut."' and '".$date_fin."'
								and societe = ".$id_soc."
								GROUP BY rubrique, daty,nb_pli,nb_mvt
							) as global
							GROUP BY daty
						) as data_global on data_daty.daty = data_global.daty
						LEFT JOIN (
							SELECT 
								daty,
								nb_pli_2,nb_pli_3,nb_pli_4,nb_pli_5,nb_pli_6,nb_pli_7,nb_pli_8,nb_pli_9,nb_pli_10,
								nb_pli_11,nb_pli_12,nb_pli_13,nb_pli_14,nb_pli_15,nb_pli_16,nb_pli_17,nb_pli_18,nb_pli_19,nb_pli_20,
								nb_pli_21,nb_pli_22,nb_pli_23,nb_pli_24,nb_pli_25,nb_pli_26,nb_pli_27,
								nb_mvt_2,nb_mvt_3,nb_mvt_4,nb_mvt_5,nb_mvt_6,nb_mvt_7,nb_mvt_8,nb_mvt_9,nb_mvt_10,
								nb_mvt_11,nb_mvt_12,nb_mvt_13,nb_mvt_14,nb_mvt_15,nb_mvt_16,nb_mvt_17,nb_mvt_18,nb_mvt_19,nb_mvt_20,
								nb_mvt_21,nb_mvt_22,nb_mvt_23,nb_mvt_24,nb_mvt_25,nb_mvt_26,nb_mvt_27
							FROM 			
							(
								SELECT '".$date_debut."' ::date as daty,
								split_part(data_2, ',', 3) as nb_pli_2,split_part(data_2, ',', 4) as nb_mvt_2,
								split_part(data_3, ',', 3) as nb_pli_3,split_part(data_3, ',', 4) as nb_mvt_3,
								split_part(data_4, ',', 3) as nb_pli_4,split_part(data_4, ',', 4) as nb_mvt_4,
								split_part(data_5, ',', 3) as nb_pli_5,split_part(data_5, ',', 4) as nb_mvt_5,
								split_part(data_6, ',', 3) as nb_pli_6,split_part(data_6, ',', 4) as nb_mvt_6,
								split_part(data_7, ',', 3) as nb_pli_7,split_part(data_7, ',', 4) as nb_mvt_7,
								split_part(data_8, ',', 3) as nb_pli_8,split_part(data_8, ',', 4) as nb_mvt_8,
								split_part(data_9, ',', 3) as nb_pli_9,split_part(data_9, ',', 4) as nb_mvt_9,
								split_part(data_10, ',', 3) as nb_pli_10,split_part(data_10, ',', 4) as nb_mvt_10,
								split_part(data_11, ',', 3) as nb_pli_11,split_part(data_11, ',', 4) as nb_mvt_11,
								split_part(data_12, ',', 3) as nb_pli_12,split_part(data_12, ',', 4) as nb_mvt_12,
								split_part(data_13, ',', 3) as nb_pli_13,split_part(data_13, ',', 4) as nb_mvt_13,
								split_part(data_14, ',', 3) as nb_pli_14,split_part(data_14, ',', 4) as nb_mvt_14,
								split_part(data_15, ',', 3) as nb_pli_15,split_part(data_15, ',', 4) as nb_mvt_15,
								split_part(data_16, ',', 3) as nb_pli_16,split_part(data_16, ',', 4) as nb_mvt_16,
								split_part(data_17, ',', 3) as nb_pli_17,split_part(data_17, ',', 4) as nb_mvt_17,
								split_part(data_18, ',', 3) as nb_pli_18,split_part(data_18, ',', 4) as nb_mvt_18,
								split_part(data_19, ',', 3) as nb_pli_19,split_part(data_19, ',', 4) as nb_mvt_19,
								split_part(data_20, ',', 3) as nb_pli_20,split_part(data_20, ',', 4) as nb_mvt_20,
								split_part(data_21, ',', 3) as nb_pli_21,split_part(data_21, ',', 4) as nb_mvt_21,
								split_part(data_22, ',', 3) as nb_pli_22,split_part(data_22, ',', 4) as nb_mvt_22,
								split_part(data_23, ',', 3) as nb_pli_23,split_part(data_23, ',', 4) as nb_mvt_23,
								split_part(data_24, ',', 3) as nb_pli_24,split_part(data_24, ',', 4) as nb_mvt_24,
								split_part(data_25, ',', 3) as nb_pli_25,split_part(data_25, ',', 4) as nb_mvt_25,
								split_part(data_26, ',', 3) as nb_pli_26,split_part(data_26, ',', 4) as nb_mvt_26,
								split_part(data_27, ',', 3) as nb_pli_27,split_part(data_27, ',', 4) as nb_mvt_27
								FROM f_stat_mensuel_par_bloc_soc_typo('".$id_soc."','Plis par typologie', '".$date_debut."' , '".$date_fin."')
								
							) as data_plis_par_typo
							
						) as data_plis_typo on data_daty.daty = data_plis_typo.daty
						LEFT JOIN (
							SELECT 
								daty,
								nb_cltr_pli_2,nb_cltr_pli_3,nb_cltr_pli_4,nb_cltr_pli_5,nb_cltr_pli_6,nb_cltr_pli_7,nb_cltr_pli_8,nb_cltr_pli_9,nb_cltr_pli_10,								nb_cltr_pli_11,nb_cltr_pli_12,nb_cltr_pli_13,nb_cltr_pli_14,nb_cltr_pli_15,nb_cltr_pli_16,nb_cltr_pli_17,nb_cltr_pli_18,nb_cltr_pli_19,nb_cltr_pli_20,								nb_cltr_pli_21,nb_cltr_pli_22,nb_cltr_pli_23,nb_cltr_pli_24,nb_cltr_pli_25,nb_cltr_pli_26,nb_cltr_pli_27,	nb_cltr_mvt_2,nb_cltr_mvt_3,nb_cltr_mvt_4,nb_cltr_mvt_5,nb_cltr_mvt_6,nb_cltr_mvt_7,nb_cltr_mvt_8,nb_cltr_mvt_9,nb_cltr_mvt_10,nb_cltr_mvt_11,nb_cltr_mvt_12,nb_cltr_mvt_13,nb_cltr_mvt_14,nb_cltr_mvt_15,nb_cltr_mvt_16,nb_cltr_mvt_17,nb_cltr_mvt_18,nb_cltr_mvt_19,nb_cltr_mvt_20,nb_cltr_mvt_21,nb_cltr_mvt_22,nb_cltr_mvt_23,nb_cltr_mvt_24,nb_cltr_mvt_25,nb_cltr_mvt_26,nb_cltr_mvt_27
							FROM 			
							(
								SELECT '".$date_debut."' ::date as daty,
								split_part(data_2, ',', 3) as nb_cltr_pli_2,split_part(data_2, ',', 4) as nb_cltr_mvt_2,
								split_part(data_3, ',', 3) as nb_cltr_pli_3,split_part(data_3, ',', 4) as nb_cltr_mvt_3,
								split_part(data_4, ',', 3) as nb_cltr_pli_4,split_part(data_4, ',', 4) as nb_cltr_mvt_4,
								split_part(data_5, ',', 3) as nb_cltr_pli_5,split_part(data_5, ',', 4) as nb_cltr_mvt_5,
								split_part(data_6, ',', 3) as nb_cltr_pli_6,split_part(data_6, ',', 4) as nb_cltr_mvt_6,
								split_part(data_7, ',', 3) as nb_cltr_pli_7,split_part(data_7, ',', 4) as nb_cltr_mvt_7,
								split_part(data_8, ',', 3) as nb_cltr_pli_8,split_part(data_8, ',', 4) as nb_cltr_mvt_8,
								split_part(data_9, ',', 3) as nb_cltr_pli_9,split_part(data_9, ',', 4) as nb_cltr_mvt_9,
								split_part(data_10, ',', 3) as nb_cltr_pli_10,split_part(data_10, ',', 4) as nb_cltr_mvt_10,
								split_part(data_11, ',', 3) as nb_cltr_pli_11,split_part(data_11, ',', 4) as nb_cltr_mvt_11,
								split_part(data_12, ',', 3) as nb_cltr_pli_12,split_part(data_12, ',', 4) as nb_cltr_mvt_12,
								split_part(data_13, ',', 3) as nb_cltr_pli_13,split_part(data_13, ',', 4) as nb_cltr_mvt_13,
								split_part(data_14, ',', 3) as nb_cltr_pli_14,split_part(data_14, ',', 4) as nb_cltr_mvt_14,
								split_part(data_15, ',', 3) as nb_cltr_pli_15,split_part(data_15, ',', 4) as nb_cltr_mvt_15,
								split_part(data_16, ',', 3) as nb_cltr_pli_16,split_part(data_16, ',', 4) as nb_cltr_mvt_16,
								split_part(data_17, ',', 3) as nb_cltr_pli_17,split_part(data_17, ',', 4) as nb_cltr_mvt_17,
								split_part(data_18, ',', 3) as nb_cltr_pli_18,split_part(data_18, ',', 4) as nb_cltr_mvt_18,
								split_part(data_19, ',', 3) as nb_cltr_pli_19,split_part(data_19, ',', 4) as nb_cltr_mvt_19,
								split_part(data_20, ',', 3) as nb_cltr_pli_20,split_part(data_20, ',', 4) as nb_cltr_mvt_20,
								split_part(data_21, ',', 3) as nb_cltr_pli_21,split_part(data_21, ',', 4) as nb_cltr_mvt_21,
								split_part(data_22, ',', 3) as nb_cltr_pli_22,split_part(data_22, ',', 4) as nb_cltr_mvt_22,
								split_part(data_23, ',', 3) as nb_cltr_pli_23,split_part(data_23, ',', 4) as nb_cltr_mvt_23,
								split_part(data_24, ',', 3) as nb_cltr_pli_24,split_part(data_24, ',', 4) as nb_cltr_mvt_24,
								split_part(data_25, ',', 3) as nb_cltr_pli_25,split_part(data_25, ',', 4) as nb_cltr_mvt_25,
								split_part(data_26, ',', 3) as nb_cltr_pli_26,split_part(data_26, ',', 4) as nb_cltr_mvt_26,
								split_part(data_27, ',', 3) as nb_cltr_pli_27,split_part(data_27, ',', 4) as nb_cltr_mvt_27
								FROM f_stat_mensuel_par_bloc_soc_typo('".$id_soc."','Plis cloturés', '".$date_debut."' , '".$date_fin."')
							) as data_plis_clos
							
						) as data_plis_clotures on data_daty.daty = data_plis_clotures.daty
						LEFT JOIN (
							SELECT 
								daty,
								sum(nb_att_en_cours) as nb_att_en_cours,
								sum(nb_controle_en_cours) as nb_controle_en_cours,
								sum(nb_saisie_en_cours) as nb_saisie_en_cours,
								sum(nb_typage_en_cours) as nb_typage_en_cours,
								sum(nb_rettt_en_cours) as nb_rettt_en_cours,
								sum(mvt_att_en_cours) as mvt_att_en_cours,
								sum(mvt_controle_en_cours) as mvt_controle_en_cours,
								sum(mvt_saisie_en_cours) as mvt_saisie_en_cours,
								sum(mvt_typage_en_cours) as mvt_typage_en_cours,
								sum(mvt_rettt_en_cours) as mvt_rettt_en_cours
							FROM
							(SELECT daty,
								case when rubrique ='En attente consignes' then nb_pli::integer else 0 end  as nb_att_en_cours,
								case when rubrique ='En cours de contrôle' then nb_pli::integer else 0 end as nb_controle_en_cours,
								case when rubrique ='En cours de saisie' then nb_pli::integer else 0 end as nb_saisie_en_cours,
								case when rubrique ='En cours de typage' then nb_pli::integer else 0 end as nb_typage_en_cours,
								case when rubrique ='Retraitement' then nb_pli::integer else 0 end as nb_rettt_en_cours,
								case when rubrique ='En attente consignes' then nb_mvt::integer else 0 end  as mvt_att_en_cours,
								case when rubrique ='En cours de contrôle' then nb_mvt::integer else 0 end as mvt_controle_en_cours,
								case when rubrique ='En cours de saisie' then nb_mvt::integer else 0 end as mvt_saisie_en_cours,
								case when rubrique ='En cours de typage' then nb_mvt::integer else 0 end as mvt_typage_en_cours,
								case when rubrique ='Retraitement' then nb_mvt::integer else 0 end as mvt_rettt_en_cours
								
								FROM stat_mensuel
								WHERE titre = 'Plis en cours' 
								and daty between '".$date_debut."' and '".$date_fin."'
								and societe = '".$id_soc."'
								GROUP BY rubrique, daty,nb_pli,nb_mvt
							)as data_plis_encours
							GROUP BY daty
						) as data_pli_en_cours on data_daty.daty = data_pli_en_cours.daty
						LEFT JOIN (
							SELECT 
								daty,sum(nb_non_traite) nb_non_traite,sum(nb_en_cours) nb_en_cours,
								sum(mvt_non_traite) mvt_non_traite,sum(mvt_en_cours) mvt_en_cours
							FROM 			
							(
								SELECT daty,
								case when rubrique ='Non traité' then nb_pli::integer else 0 end  as nb_non_traite,
								case when rubrique ='En cours' then nb_pli::integer else 0 end as nb_en_cours,
								case when rubrique ='Non traité' then nb_mvt::integer else 0 end  as mvt_non_traite,
								case when rubrique ='En cours' then nb_mvt::integer else 0 end as mvt_en_cours
								
								FROM stat_mensuel
								WHERE titre = 'Restant à traiter' 
								and daty between '".$date_debut."' and '".$date_fin."'
								and societe = '".$id_soc."'
								GROUP BY rubrique, daty,nb_pli,nb_mvt
							) as data_restant
							GROUP BY daty
						) as data_restant on data_daty.daty = data_restant.daty
						ORDER BY data_daty.daty asc
				"	;
        /*echo "<pre>";
		print_r($sql);
        echo "</pre>";exit;
		*/
        return $this->ged->query($sql)
            ->result_array();

    }


    public function get_stat_mensuel_typologlie( $date_debut, $date_fin ,$id_soc){

        $sql = "	SELECT date_courrier, info_0 , info_2 , info_3 , info_4 , info_5 , info_6 , info_7 , info_8 , info_9 , info_10 , info_11 , info_12 , info_13 , info_14 , info_15 , info_16 , info_17 , info_18 , info_19 , info_20 , info_21 , info_22 , info_23 , info_24 , info_25 , info_26 , info_27 
	   FROM f_pli_cloture_detail( '".$date_debut."' ,(SELECT (date '".$date_fin."' + interval '1 month' - interval '1 day')::date), ".$id_soc.")"	;
        /*echo "<pre>";
		print_r($sql);
        echo "</pre>";
		exit;
		*/
        return $this->ged->query($sql)
            ->result_array();

    }
    public function get_typologlie(){

        $sql = "	SELECT id, typologie FROM typologie ORDER BY id asc"	;
        return $this->ged->query($sql)
            ->result_array();

    }
	 public function get_statut_saisie(){
	 $sql = "	SELECT id_statut_saisie, libelle FROM statut_saisie ORDER BY libelle asc"	;
        return $this->ged->query($sql)
            ->result_array();

    }
    public function get_min_max_reception_menusel ( $date_debut, $date_fin){

        $sql = "SELECT 
				case when min(date_reception) < '".$date_debut."' then min(date_reception) else '".$date_debut."'::date end date_reception_min , 
				max(date_reception) date_reception_max 
				FROM public.traitement_mensuel
				WHERE date_traitement between  '".$date_debut."' and '".$date_fin."'  ";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_traitement_mensuel( $date_debut, $date_fin, $date_reception_min){

        list ($yy,$mm,$dd) = explode("-",$date_reception_min);
        $date_reception_min = $dd."/".$mm."/".$yy;
        $sql = "SELECT 	data_daty.daty,	
					data.mois_reception,
					mois_traitement,traite,en_cours,
					ko_scan,
					ko_inconnu,
					ko_def,
					ko_call,
					ko_circulaire,
					ko_en_attente,
					ko_reclammation,
					ko_litige,
					non_traite,
					nb_plis_recu,
					nb_non_type_plis_recu
					FROM
					(
						SELECT substr(liste_months::text,1,7) as daty FROM liste_months ('".$date_debut."' , '".$date_fin."') 
					) as data_daty
					LEFT JOIN
					(
						SELECT  substr(date_traitement::text,1,7) as mois_traitement,
							substr(date_reception::text,1,7) as mois_reception,
							sum(traite) as traite, 
							sum(en_cours) as en_cours, 
							sum(ko_scan) as ko_scan, 
							sum(ko_inconnu) as ko_inconnu, 
							sum(ko_def) as ko_def, 
							sum(ko_call) as ko_call, 
							sum(ko_circulaire) as ko_circulaire, 
							sum(ko_en_attente) as ko_en_attente,
							sum(ko_reclammation) as ko_reclammation,
							sum(ko_litige) as ko_litige,
							sum(non_traite) as non_traite
						  
					FROM public.traitement_mensuel
					WHERE date_traitement between '".$date_debut."' and '".$date_fin."'
					GROUP BY substr(date_reception::text,1,7),substr(date_traitement::text,1,7)
						
					) 
					as data on data.mois_traitement = data_daty.daty 
					LEFT JOIN
					(
						SELECT  
							substr(date_reception::text,1,7) as mois_reception,
							sum(nb_plis_recu) nb_plis_recu,
							sum(nb_non_type_plis_recu) nb_non_type_plis_recu
						FROM
						(    
							 SELECT  distinct date_reception ,			
							nb_plis_recu, 
							nb_non_type_plis_recu		  
							FROM public.traitement_mensuel
							WHERE date_reception between '".$date_reception_min."' and '".$date_fin."'
						) as reception
						GROUP BY substr(date_reception::text,1,7)
						
					) 
					as data_recep on data_recep.mois_reception = data.mois_reception 
					ORDER BY daty, mois_traitement asc";

        return $this->ged->query($sql)
            ->result_array();
    }

    public function get_min_date_courrier($date_debut, $date_fin, $source){

        $where_like = "";
        if($source =='3'){
            if($date_debut != '' && $date_fin != ''){
                $where_like 	= " dt_event::date between '".$date_debut."' and '".$date_fin."' ";

            }
            $sql = "SELECT min (date_courrier::date) as date_courrier
                FROM histo_pli 
                LEFT JOIN view_flag_traitement on view_flag_traitement.id_flag_traitement = histo_pli.flag_traitement 
                LEFT JOIN view_pli on view_pli.id_pli = histo_pli.id_pli 
                LEFT JOIN view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
                WHERE ".$where_like."
                ";
            return $this->ged->query($sql)
                ->result_array();
        }
        else{
            if($date_debut != '' && $date_fin != ''){
                $where_like 	= " date_traitement::date between '".$date_debut."' and '".$date_fin."' ";

            }
            $sql = "SELECT min (date_reception::date) as date_courrier 
                FROM traitement 
                LEFT JOIN statut_pli ON statut_pli.id_statut = traitement.statut_pli 
                LEFT JOIN flux on flux.id_flux = traitement.id_flux 
                WHERE ".$where_like."
                ";
            return $this->ged_flux->query($sql)
                ->result_array();
        }
    }
    public function get_dates_ttt($date_debut, $date_fin){
        $where_like = "";
        if($date_debut != '' && $date_fin != ''){
            $where_like 	= " and date_courrier::date between '".$date_debut."' and '".$date_fin."' ";

        }
        $sql = "SELECT min(dt_event::date) date_min_ttt, max(dt_event::date) date_max_ttt
				FROM view_pli 
				LEFT JOIN view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
				INNER join histo_pli on view_pli.id_pli = histo_pli.id_pli  
				WHERE 1=1 ".$where_like."
			";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_lot_mensuel($date_debut, $date_fin){
        $where_date = "";
        if($date_debut != '' && $date_fin != ''){
            $where_date 	= " and date_courrier::date >= '".$date_debut."' and date_courrier::date < '".$date_fin."' ";

        }
        $sql = "SELECT date_courrier, nb_pli
				FROM lot_mensuel 
				WHERE 1=1 ".$where_date."
				ORDER BY  date_courrier::date asc
			";
        return $this->ged->query($sql)
            ->result_array();
    }

    public function get_datatables($where_date_courrier,$where_date_courrier_lot,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatable_sql($where_date_courrier,$where_date_courrier_lot,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
            $this->ged->limit($length, $start);
        $query = $this->ged->get();
        return $query->result();
    }

    public function count_filtered($where_date_courrier,$where_date_courrier_lot,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_sql($where_date_courrier,$where_date_courrier_lot,$post_order,$search,$post_order_col,$post_order_dir);

        $query = $this->ged->get();
        return $query->num_rows();

    }

    public function count_all($where_date_courrier,$where_date_courrier_lot)
    {

        $this->ged->SELECT($this->SELECT)
            ->FROM("(SELECT 
						pli,
						traitement.id_pli, 
						typologie,
						case when (flag_traitement = 3 ) then 'KO inconnu' else 
						case when (flag_traitement = 4 ) then 'KO scan' else 
						case when (flag_traitement = 5 ) then 'KO réclammation' else 
						case when (flag_traitement = 6 ) then 'KO litige' else 
						case when (flag_traitement = 8 ) then 'KO Définitif' else 
						case when (flag_traitement = 9 ) then 'KO Call' else
						case when (flag_traitement = 11 ) then 'KO Ciculaire' else
						case when (flag_traitement = 12 ) then 'KO en attente' else ''
						end end end end end end end end type_ko ,
						flag_traitement,
						traitement,
						dt_event,
						lot_scan,
						date_courrier,
						date_numerisation,
						case when (flag_traitement is null) or (flag_traitement = 0 ) then 'Non traité' else 
						case when (flag_traitement = 0 )  then 'En cours de typage' else
						case when (traitement.flag_traitement = 0 ) or (flag_traitement = 1 )  then 'Typage terminé' else 
						case when (flag_traitement = 1 )  then 'En cours de saisie' else 
						case when ( flag_traitement = 2 )  then 'En cours de controle' else 
						case when (traitement. flag_traitement = 2) then 'Cloturé' else 
						case when (flag_traitement = 3 or flag_traitement = 4 or flag_traitement = 5 or flag_traitement = 6 or flag_traitement = 8 or flag_traitement = 9 or flag_traitement = 11 or flag_traitement = 12)
						then 'Anomalie' else '' end end end end end end end as statut, 				
						case when code_type_pli is null then 'NT' else code_type_pli end as code_type_pli,
						case when comment is null then 'Non typé' else comment end as comment,
						count(distinct id_document) as nb_doc
						
						FROM 
						( 
							SELECT tab1.id_pli,flag_traitement,traitement,substring(dt_event::text FROM 0 for 11) as dt_event ,pli,typologie,lot_scan,	substring(date_courrier::text FROM 0 for 11) as date_courrier ,substring(date_numerisation::text FROM 0 for 11) as date_numerisation,code_type_pli,comment
							
							FROM
							(
								SELECT histo_pli.id_pli,max(histo_pli.oid) oid
								FROM histo_pli 
								LEFT JOIN view_pli on view_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_lot on  view_pli.id_lot_numerisation = view_lot.id_lot_numerisation
								WHERE histo_pli.id_pli in (SELECT distinct view_pli.id_pli 
								FROM view_pli INNER join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  WHERE  ".$where_date_courrier." )
								GROUP BY histo_pli.id_pli
									
							) tab1 INNER join
							(
								SELECT histo_pli.id_pli,histo_pli.oid,histo_pli.flag_traitement,traitement,dt_event,
								view_pli.pli,view_pli.typologie,view_lot.lot_scan,
								view_lot.date_courrier,view_lot.date_numerisation,'xxxx' as code_type_pli,'no comment' as comment
								FROM histo_pli 
								LEFT JOIN view_flag_traitement on view_flag_traitement.id_flag_traitement = histo_pli.flag_traitement
								LEFT JOIN view_pli on view_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_lot on  view_lot.id_lot_numerisation = view_pli.id_lot_numerisation
								WHERE histo_pli.id_pli in (SELECT distinct view_pli.id_pli FROM view_pli INNER join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  WHERE  ".$where_date_courrier." )
								
							)tab2 on tab1.oid = tab2.oid
							union
							(
							SELECT 
								view_pli.id_pli,histo_pli.flag_traitement,'Non traité' as traitement,'0001-01-01'::text as dt_event,
								view_pli.pli,view_pli.typologie,view_lot.lot_scan,	substring(date_courrier::text FROM 0 for 11) as date_courrier ,substring(date_numerisation::text FROM 0 for 11) as date_numerisation,'yyyy' as code_type_pli,'no comment' as comment
								 FROM view_pli 
								 INNER join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  
								 LEFT JOIN histo_pli on histo_pli.id_pli = view_pli.id_pli 
								 WHERE ".$where_date_courrier." and histo_pli.id_pli is null
								
							)
						
						)
						as traitement
						LEFT JOIN view_document on traitement.id_pli = view_document.id_pli						
						GROUP BY pli,traitement.id_pli,flag_traitement,traitement,
						lot_scan,date_courrier,date_numerisation,dt_event,typologie,code_type_pli,
						comment
						) as tab
                         ");
        return $this->ged->count_all_results();

    }

    public function get_datatable_sql($where_date_courrier,$where_date_courrier_lot,$post_order,$search ,$post_order_col,$post_order_dir){

        $this->ged->SELECT($this->SELECT)
            ->FROM("");

        $i = 0;
        foreach ($this->column_search as $item)
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->like($item, $search);
                }
                else
                {
                    $this->ged->or_like($item, $search);
                }

                if(count($this->column_search) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order))
        {
            $this->ged->order_by($this->column_order[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order))
        {
            $order = $this->order;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_list_newplis($date_debut, $date_fin){

        $where_like = $where_view_pli = "";
        if($date_debut != '' && $date_fin != ''){
            $where_like 	.= " date_courrier between '".$date_debut."' and '".$date_fin."' ";
            $where_view_pli .= " view_lot.date_courrier  between '".$date_debut."' and '".$date_fin."' ";
        }
        $sql = "SELECT 
						view_pli.id_pli,view_pli.id_pli,view_pli.typologie,'' as type_ko,
						histo_pli.flag_traitement,'Non traité' as traitement,dt_event::date,view_pli.typage_par,
						lot_scan,date_courrier::date,date_numerisation,'Non traité' as statut,
						0 code_type_pli,null as comment,null as nb_doc,null vol_produit
						 FROM view_pli 
						 INNER join view_lot on view_lot.id_lot_numerisation = view_pli.id_lot_numerisation  
						 LEFT JOIN histo_pli on histo_pli.id_pli = view_pli.id_pli 
						 WHERE  ".$where_like." and histo_pli.id_pli is null
				    ORDER BY date_courrier::date, date_numerisation, lot_scan,view_pli.id_pli asc";
        /*echo "<pre>";
        print_r($sql);
        echo "</pre>";exit;	*/
        return $this->ged->query($sql)
            ->result_array();
    }

    public function get_datatables_traitement($where_date_traitement,$f_clause_where,$length,$start,$post_order,$search,$post_order_col,$post_order_dir,$source){
        $this->get_datatable_ttt_sql($where_date_traitement,$f_clause_where,$post_order,$search,$post_order_col,$post_order_dir,$source );
        if($source == '3'){
            if($length != -1)
                $this->ged->limit($length, $start);
            $query = $this->ged->get();
        }
        else{
            if($length != -1)
                $this->ged_flux->limit($length, $start);
            $query = $this->ged_flux->get();
        }
        return $query->result();
    }

    public function count_filtered_traitement($where_date_traitement,$f_clause_where,$post_order,$search,$post_order_col,$post_order_dir,$source)
    {
        $this->get_datatable_ttt_sql($where_date_traitement,$f_clause_where,$post_order,$search,$post_order_col,$post_order_dir,$source);

        if($source == '3'){
            $query = $this->ged->get();
        }
        else{
            $query = $this->ged_flux->get();
        }
        return $query->num_rows();

    }

    public function count_all_traitement($where_date_traitement,$f_clause_where,$source)
    {
        
        if($source == '3'){
        $this->ged->SELECT($this->select_ttt)
            ->FROM("(SELECT
							nom_societe,
							dt_event,
							date_courrier,
							lot_scan,
							pli,
							statut,
							etape,
							libelle,
							txt_anomalie,
							libelle_motif,
							type_ko,
							typologie,
							titre_titre,
							string_agg(mode_paiement,', ') as mode_paiement,											
							string_agg(cmc7,', ') as cmc7,											
							string_agg(montant,', ') as montant,											
							string_agg(rlmc,', ') as rlmc,											
							case when infos_datamatrix is null or infos_datamatrix = '' then string_agg(infos_datamatrix,'') else string_agg(infos_datamatrix,', ') end as infos_datamatrix,
							string_agg(motif_rejet,', ') as motif_rejet,
							string_agg(description_rejet,', ') as description_rejet,
							titre_societe_nom,
							nb_mvt,
							id_pli
						FROM 
                        (SELECT distinct
							nom_societe,
							traitement.dt_event,
							date_courrier,
							lot_scan,
							pli,
							statut,
							etape,
							libelle,
							txt_anomalie,
							libelle_motif,
							type_ko,
							typologie,
							titre_titre,
							string_agg(mode_paiement,', ') mode_paiement,											
							string_agg(cmc7,', ') cmc7,											
							string_agg(montant,', ') montant,											
							string_agg(rlmc,', ')rlmc ,											
							case when infos_datamatrix is null or infos_datamatrix = '' then string_agg(infos_datamatrix,'') else string_agg(infos_datamatrix,', ') end as infos_datamatrix,
							motif_rejet,
							description_rejet,
							titre_societe_nom,
							nb_mvt,
							id_pli
							FROM ( 
							 
							 
									SELECT 
										histo_pli.id_pli::text as id_pli, 
										histo_pli.flag_traitement,
										f_pli.typologie as code_type_pli,
										substring(histo_pli.dt_event::text FROM 0 for 11) as dt_event,
										/*case when (f_pli.statut_saisie = 0 or f_pli.statut_saisie is null) then 'Non traité' else
										 case  when (data_pli.flag_traitement  in(9,14)  and data_pli.statut_saisie = 1)  or (data_pli.statut_saisie in (8,10,24))
												then 'Cloturé' 
												else statut_saisie.libelle end end as statut,
												*/
										case when (f_pli.statut_saisie = 0 or f_pli.statut_saisie is null) then 'Non traité' else statut_saisie.libelle end as statut,
										f_flag_traitement.traitement as etape,
										view_pli_avec_cheque_new.id_pli as pli_avec_chq,
										case when (f_pli.statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7]) ) then statut_saisie.libelle else ''  end type_ko,
										typage_par,
										lot_scan, 
										substring(date_courrier::text FROM 0 for 11) as date_courrier, 
										pli, 
										(f_pli.typologie::text) as typologie,
										data_pli.societe as id_societe,
										societe.nom_societe,
										paiement_cheque.montant,
										paiement_cheque.cmc7,
										paiement_cheque.rlmc,
										paiement_cheque.anomalie_cheque_id,
										mode_paiement.id_mode_paiement,			
										mode_paiement.mode_paiement,			
										mode_paiement.actif,
										data_pli.infos_datamatrix,			
										data_pli.titre,			
										data_pli.societe societe_vf,
										data_pli.code_promotion,
										(data_pli.motif_rejet::text) as motif_rejet,
										data_pli.description_rejet,
										data_pli.statut_saisie,
										data_pli.motif_ko,
										data_pli.autre_motif_ko,
										data_pli.nom_circulaire,
										data_pli.fichier_circulaire,
										data_pli.message_indechiffrable,
										data_pli.dmd_kdo_fidelite_prim_suppl,
										data_pli.dmd_envoi_kdo_adrss_diff,
										data_pli.type_coupon,
										data_pli.id_erreur,
										data_pli.comm_erreur,
										data_pli.nom_orig_fichier_circulaire,
										motif_ko.libelle_motif as libelle_motif,			
										motif_ko_scan.description as txt_anomalie,
										titres.code as titre_id,
										titres.titre as titre_titre,
										titres.societe_id as titre_societe_id,
										societe.nom_societe as titre_societe_nom,
										statut_saisie.libelle,
										mvt_nbr::text as nb_mvt
																			
									FROM histo_pli
									INNER JOIN f_histo_pli_by_date_by_societe(".$f_clause_where.") f ON f.id  = histo_pli.id
									LEFT JOIN data_pli on f.id_pli = data_pli.id_pli 
									INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
									INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation  = f_lot_numerisation.id_lot_numerisation
									LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
									LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
									LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
									LEFT JOIN motif_ko on motif_ko.id_motif = data_pli.motif_ko
									LEFT JOIN motif_ko_scan on data_pli.id_pli = motif_ko_scan.pli_id
									LEFT JOIN titres on titres.id = data_pli.titre
									LEFT JOIN societe on societe.id = titres.societe_id
									/*LEFT JOIN view_nb_mouvement_pli on data_pli.id_pli = view_nb_mouvement_pli.id_pli*/
									LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
									LEFT JOIN f_flag_traitement on f_flag_traitement.id_flag_traitement = data_pli.flag_traitement
									LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli
									WHERE  ".$where_date_traitement."
								
							) as traitement 
							GROUP BY  infos_datamatrix,txt_anomalie,nom_societe,dt_event,date_courrier,lot_scan,pli,statut,etape,motif_rejet,description_rejet,type_ko,typologie,traitement,txt_anomalie,libelle_motif,titre_titre,titre_societe_nom,nb_mvt,id_pli,libelle) as resultat
							group by nom_societe,dt_event,date_courrier,lot_scan,pli,statut,etape,libelle,txt_anomalie,libelle_motif,type_ko,typologie,titre_titre,infos_datamatrix,titre_societe_nom,nb_mvt,id_pli
	
						) as tab
                        ");
        return $this->ged->count_all_results();
        }
        else{
            $this->ged_flux->SELECT($this->select_ttt_flux)
                ->FROM("(SELECT  
                societe.nom_societe,
                source.source,
                case when (flux.id_source = 1) then objet_flux else filename_origin end as sujet,
                traitement.id_flux, 
                (date_reception::date) as date_reception,
                (view_traitement_oid.date_traitement)::date as date_dernier_traitement,       
                (view_lot_saisie_flux.dt::date) as date_saisie_adv,
                id_lot_saisie_flux,
                typologie.typologie,
                case when (traitement.statut_pli = 7 and id_lot_saisie_flux is not null and view_lot_saisie_flux.dt::date = view_traitement_oid.date_traitement::date)
                then 'Saisie finie'
                else 
                case when traitement.statut_pli = 7 and view_lot_saisie_flux.dt::date = view_traitement_oid.date_traitement::date then 
                'cloture' else
                case when traitement.statut_pli = 13 then 'hors_perimetre' else  
                case when traitement.statut_pli = ANY (ARRAY[11, 12, 14, 15]) then 'Anomalie' else 
                case when traitement.statut_pli = ANY (ARRAY[4, 5]) then 'Escaladé' else 
                case when traitement.statut_pli = ANY (ARRAY[8, 9, 10]) then 'transfert_consigne' else
                case when traitement.statut_pli = 0 then 'Non traité' else
                'En cours'                                                           
                end end end end end end end as statut_pli,
                lot_advantage,
       nb_abo
  FROM public.flux
  inner join traitement on flux.id_flux = traitement.id_flux
  left join typologie on typologie.id_typologie = flux.typologie
  inner join view_traitement_oid on traitement.oid = view_traitement_oid.oid
  left join view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
  left join view_lot_saisie_flux on view_lot_saisie_flux.id = flux.id_lot_saisie_flux
  left join societe on societe.id = flux.societe
  left join source on source.id_source = flux.id_source
  where ".$where_date_traitement." 
) as tab ");
            return $this->ged_flux->count_all_results();
        }

    }
	//charger datatable traitement courrier
    public function get_datatable_ttt_sql($where_date_traitement,$f_clause_where,$post_order,$search ,$post_order_col,$post_order_dir,$source){
        
        if($source == '3'){
        $this->ged->SELECT($this->select_ttt)
            ->FROM("(SELECT
							nom_societe,
							dt_event,
							date_courrier,
							lot_scan,
							pli,
							statut,
							etape,
							txt_anomalie,
							libelle_motif,
							type_ko,
							typologie,
							titre_titre,
							string_agg(mode_paiement,', ') as mode_paiement,											
							string_agg(cmc7,', ') as cmc7,											
							string_agg(montant,', ') as montant,											
							string_agg(rlmc,', ') as rlmc,											
							case when infos_datamatrix is null or infos_datamatrix = '' then string_agg(infos_datamatrix,'') else string_agg(infos_datamatrix,', ') end as infos_datamatrix,
							string_agg(motif_rejet,', ') as motif_rejet,
							string_agg(description_rejet,', ') as description_rejet,
							titre_societe_nom,
							nb_mvt,
							id_pli,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						FROM 
                        (SELECT distinct
							nom_societe,
							traitement.dt_event,
							date_courrier,
							lot_scan,
							pli,
							statut,
							etape,
							libelle,
							txt_anomalie,
							libelle_motif,
							type_ko,
							typologie,
							titre_titre,
							string_agg(mode_paiement,', ') mode_paiement,											
							string_agg(cmc7,', ') cmc7,											
							string_agg(montant,', ') montant,											
							string_agg(rlmc,', ')rlmc ,											
							case when infos_datamatrix is null or infos_datamatrix = '' then string_agg(infos_datamatrix,'') else string_agg(infos_datamatrix,', ') end as infos_datamatrix,
							motif_rejet,
							description_rejet,
							titre_societe_nom,
							nb_mvt,
							id_pli,
							flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
							FROM ( 
							 
							 
									SELECT 
										histo_pli.id_pli::text as id_pli, 
										histo_pli.flag_traitement,
										f_pli.typologie as code_type_pli,
										substring(histo_pli.dt_event::text FROM 0 for 11) as dt_event,
										/*case when (f_pli.statut_saisie = 0 or f_pli.statut_saisie is null) then 'Non traité' else
										 case  when (data_pli.flag_traitement  in(9,14)  and data_pli.statut_saisie = 1) or (data_pli.statut_saisie in (8,10,24))
												then 'Cloturé' 
												else statut_saisie.libelle end end as statut,
												*/
										case when (f_pli.statut_saisie = 0 or f_pli.statut_saisie is null) then 'Non traité' else statut_saisie.libelle end as statut,
										f_flag_traitement.traitement as etape,
										
										f_flag_traitement.flgtt_etape,
										f_flag_traitement.flgtt_etat,
										CASE WHEN (flgtt_flag_attente = 1 AND par_ttmt_ko = 1) THEN 'A traiter' ELSE flgtt_etat END as  flgtt_etat_disp,
										CASE WHEN flag_rewrite_status_client <> 0 THEN ' ' ELSE statut_saisie.libelle END as etat,
										view_pli_avec_cheque_new.id_pli as pli_avec_chq,
										case when (f_pli.statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7]) ) then statut_saisie.libelle else ''  end type_ko, 
										typage_par,
										lot_scan, 
										substring(date_courrier::text FROM 0 for 11) as date_courrier, 
										pli, 
										(typologie.typologie::text) as typologie,
										data_pli.societe as id_societe,
										societe.nom_societe,
										paiement_cheque.montant,
										paiement_cheque.cmc7,
										paiement_cheque.rlmc,
										paiement_cheque.anomalie_cheque_id,
										mode_paiement.id_mode_paiement,			
										mode_paiement.mode_paiement,			
										mode_paiement.actif,
										data_pli.infos_datamatrix,			
										data_pli.titre,			
										data_pli.societe societe_vf,
										data_pli.code_promotion,
										(data_pli.motif_rejet::text) as motif_rejet,
										data_pli.description_rejet,
										data_pli.statut_saisie,
										data_pli.motif_ko,
										data_pli.autre_motif_ko,
										data_pli.nom_circulaire,
										data_pli.fichier_circulaire,
										data_pli.message_indechiffrable,
										data_pli.dmd_kdo_fidelite_prim_suppl,
										data_pli.dmd_envoi_kdo_adrss_diff,
										data_pli.type_coupon,
										data_pli.id_erreur,
										data_pli.comm_erreur,
										data_pli.nom_orig_fichier_circulaire,
										motif_ko.libelle_motif as libelle_motif,			
										motif_ko_scan.description as txt_anomalie,
										titres.code as titre_id,
										titres.titre as titre_titre,
										titres.societe_id as titre_societe_id,
										societe.nom_societe as titre_societe_nom,
										statut_saisie.libelle,
										mvt_nbr::text as nb_mvt
																			
									FROM histo_pli
									INNER JOIN f_histo_pli_by_date_by_societe(".$f_clause_where.") f ON f.id  = histo_pli.id
									LEFT JOIN data_pli on f.id_pli = data_pli.id_pli 
									INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
									INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation  = f_lot_numerisation.id_lot_numerisation
									LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
									LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
									LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
									LEFT JOIN motif_ko on motif_ko.id_motif = data_pli.motif_ko
									LEFT JOIN motif_ko_scan on data_pli.id_pli = motif_ko_scan.pli_id
									LEFT JOIN titres on titres.id = data_pli.titre
									LEFT JOIN societe on societe.id = titres.societe_id
									LEFT JOIN typologie on typologie.id = f_pli.typologie
									LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = f_pli.statut_saisie
									/*LEFT JOIN view_nb_mouvement_pli on data_pli.id_pli = view_nb_mouvement_pli.id_pli*/
									LEFT JOIN f_flag_traitement on f_flag_traitement.id_flag_traitement = data_pli.flag_traitement
									LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli
									WHERE  ".$where_date_traitement."
								
							) as traitement 
							GROUP BY  infos_datamatrix,txt_anomalie,nom_societe,dt_event,date_courrier,lot_scan,pli,statut,etape,motif_rejet,description_rejet,type_ko,typologie,traitement,txt_anomalie,libelle_motif,titre_titre,titre_societe_nom,nb_mvt,id_pli,libelle,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat) as resultat
							group by nom_societe,dt_event,date_courrier,lot_scan,pli,statut,etape,libelle,txt_anomalie,libelle_motif,type_ko,typologie,titre_titre,infos_datamatrix,titre_societe_nom,nb_mvt,id_pli,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
	
						) as tab
                        ");

        $i = 0;
        foreach ($this->column_search_ttt as $item)
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->like($item, $search);
                }
                else
                {
                    $this->ged->or_like($item, $search);
                }

                if(count($this->column_search_ttt) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order))
        {
            $this->ged->order_by($this->column_order_ttt[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_ttt))
        {
            $order = $this->order_ttt;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
        }
        else{
            $this->ged_flux->SELECT($this->select_ttt_flux)
                ->FROM("(SELECT  
                societe.nom_societe,
                source.source,
                case when (flux.id_source = 1) then objet_flux else filename_origin end as sujet,
                traitement.id_flux, 
                (date_reception::date) as date_reception,
                (view_traitement_oid.date_traitement)::date as date_dernier_traitement,       
                (view_lot_saisie_flux.dt::date) as date_saisie_adv,
                id_lot_saisie_flux,
                typologie.typologie,
                case when (traitement.statut_pli = 7 and id_lot_saisie_flux is not null and view_lot_saisie_flux.dt::date = view_traitement_oid.date_traitement::date)
                then 'Saisie finie'
                else 
                case when traitement.statut_pli = 7 and view_lot_saisie_flux.dt::date = view_traitement_oid.date_traitement::date then 
                'cloture' else
                case when traitement.statut_pli = 13 then 'hors_perimetre' else  
                case when traitement.statut_pli = ANY (ARRAY[11, 12, 14, 15]) then 'Anomalie' else 
                case when traitement.statut_pli = ANY (ARRAY[4, 5]) then 'Escaladé' else 
                case when traitement.statut_pli = ANY (ARRAY[8, 9, 10]) then 'transfert_consigne' else
                case when traitement.statut_pli = 0 then 'Non traité' else
                'En cours'                                                           
                end end end end end end end as statut_pli,
                lot_advantage,
       nb_abo
  FROM public.flux
  inner join traitement on flux.id_flux = traitement.id_flux
  left join typologie on typologie.id_typologie = flux.typologie
  inner join view_traitement_oid on traitement.oid = view_traitement_oid.oid
  left join view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
  left join view_lot_saisie_flux on view_lot_saisie_flux.id = flux.id_lot_saisie_flux
  left join societe on societe.id = flux.societe
  left join source on source.id_source = flux.id_source
  where ".$where_date_traitement." 
) as tab ");

            $i = 0;
            foreach ($this->column_search_ttt_flux as $item)
            {
                if($search)
                {
                    if($i===0)
                    {
                        $this->ged_flux->group_start();
                        $this->ged_flux->like($item, $search);
                    }
                    else
                    {
                        $this->ged_flux->or_like($item, $search);
                    }

                    if(count($this->column_search_ttt_flux) - 1 == $i)
                        $this->ged_flux->group_end();
                }
                $i++;
            }

            if(isset($post_order))
            {
                $this->ged_flux->order_by($this->column_order_ttt_flux[$post_order_col], $post_order_dir);
            }
            elseif (isset($this->order_ttt_flux))
            {
                $order = $this->order_ttt_flux;
                $this->ged_flux->order_by(key($order), $order[key($order)]);
            }
        }
	}
	//export excel courrier 
    public function get_traitement_excel($where_date_traitement, $f_clause_where,$source)
    {
        if($source == '3'){
            $sql = "SELECT
                    nom_societe,dt_event,date_courrier,lot_scan,pli,statut,etape,libelle,txt_anomalie,libelle_motif,type_ko,typologie,titre_titre,string_agg(mode_paiement,', ') as mode_paiement,											
                    string_agg(cmc7,', ') as cmc7,											
                    string_agg(montant,', ') as montant,											
                    string_agg(rlmc,', ') as rlmc,											
                    case when infos_datamatrix is null or infos_datamatrix = '' then string_agg(infos_datamatrix,'') else string_agg(infos_datamatrix,', ') end as infos_datamatrix,
                    string_agg(motif_rejet,', ') as motif_rejet,
                    string_agg(description_rejet,', ') as description_rejet,
                    titre_societe_nom,nb_mvt,autre_motif_ko,id_pli,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
                    FROM 
                    (SELECT distinct
                        nom_societe,
                        traitement.dt_event,
                        date_courrier,
                        lot_scan,
                        pli,
                        statut,
                        etape,
                        libelle,
                        txt_anomalie,
                        libelle_motif,
                        type_ko,
                        typologie,
                        titre_titre,
                        string_agg(mode_paiement,', ') mode_paiement,											
                        string_agg(cmc7,', ') cmc7,											
                        string_agg(montant,', ') montant,											
                        string_agg(rlmc,', ')rlmc ,											
                        case when infos_datamatrix is null or infos_datamatrix = '' then string_agg(infos_datamatrix,'') else string_agg(infos_datamatrix,', ') end as infos_datamatrix,
                        motif_rejet,
                        description_rejet,
                        titre_societe_nom,
                        nb_mvt,
                        autre_motif_ko,
                        id_pli,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
                        FROM ( 
                         
                         
                                SELECT 
                                    histo_pli.id_pli::text as id_pli, 
                                    histo_pli.flag_traitement,
                                    typologie.id as code_type_pli,
                                    substring(histo_pli.dt_event::text FROM 0 for 11) as dt_event,
                                   /* case when (f_pli.statut_saisie = 0 or f_pli.statut_saisie is null) then 'Non traité' else
										 case  when (data_pli.flag_traitement  in(9,14)  and data_pli.statut_saisie = 1 ) or (data_pli.statut_saisie in (8,10,24))
												then 'Cloturé' 
												else statut_saisie.libelle end end as statut,
												*/
									case when (f_pli.statut_saisie = 0 or f_pli.statut_saisie is null) then 'Non traité' else statut_saisie.libelle end as statut,
									f_flag_traitement.traitement as etape,
									view_pli_avec_cheque_new.id_pli as pli_avec_chq,
									case when (f_pli.statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7]) ) then statut_saisie.libelle else ''  end type_ko, 
                                    typage_par,
                                    lot_scan, 
                                    substring(date_courrier::text FROM 0 for 11) as date_courrier, 
                                    pli, 
                                    typologie.typologie as typologie,
                                    data_pli.societe as id_societe,
                                    societe.nom_societe,
                                    paiement_cheque.montant,
                                    paiement_cheque.cmc7,
                                    paiement_cheque.rlmc,
                                    paiement_cheque.anomalie_cheque_id,
                                    mode_paiement.id_mode_paiement,			
                                    mode_paiement.mode_paiement,			
                                    mode_paiement.actif,
                                    data_pli.infos_datamatrix,			
                                    data_pli.titre,			
                                    data_pli.societe societe_vf,
                                    data_pli.code_promotion,
                                    (data_pli.motif_rejet::text) as motif_rejet,
                                    data_pli.description_rejet,
                                    data_pli.statut_saisie,
                                    data_pli.motif_ko,
                                    data_pli.autre_motif_ko,
                                    data_pli.nom_circulaire,
                                    data_pli.fichier_circulaire,
                                    data_pli.message_indechiffrable,
                                    data_pli.dmd_kdo_fidelite_prim_suppl,
                                    data_pli.dmd_envoi_kdo_adrss_diff,
                                    data_pli.type_coupon,
                                    data_pli.id_erreur,
                                    data_pli.comm_erreur,
                                    data_pli.nom_orig_fichier_circulaire,
                                    motif_ko.libelle_motif as libelle_motif,			
                                    motif_ko_scan.description as txt_anomalie,
                                    titres.code as titre_id,
                                    titres.titre as titre_titre,
                                    titres.societe_id as titre_societe_id,
                                    societe.nom_societe as titre_societe_nom,
                                    statut_saisie.libelle,
                                    mvt_nbr::text as nb_mvt,
									f_flag_traitement.flgtt_etape,
									f_flag_traitement.flgtt_etat,
									CASE WHEN (flgtt_flag_attente = 1 AND par_ttmt_ko = 1) THEN 'A traiter' ELSE flgtt_etat END as  flgtt_etat_disp,
									CASE WHEN flag_rewrite_status_client <> 0 THEN ' ' ELSE statut_saisie.libelle END as etat
                                                                        
                                FROM histo_pli
                                INNER JOIN f_histo_pli_by_date_by_societe(".$f_clause_where.") f ON f.id  = histo_pli.id
                                LEFT JOIN data_pli on f.id_pli = data_pli.id_pli 
                                INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
                                INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation  = f_lot_numerisation.id_lot_numerisation
                                LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
                                LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
                                LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
                                LEFT JOIN motif_ko on motif_ko.id_motif = data_pli.motif_ko
                                LEFT JOIN motif_ko_scan on data_pli.id_pli = motif_ko_scan.pli_id
                                LEFT JOIN titres on titres.id = data_pli.titre
                                LEFT JOIN societe on societe.id = data_pli.societe
                                /*LEFT JOIN view_nb_mouvement_pli on data_pli.id_pli = view_nb_mouvement_pli.id_pli*/
                                LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN f_flag_traitement on f_flag_traitement.id_flag_traitement = data_pli.flag_traitement
                                LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli
                                LEFT JOIN typologie on typologie.id = f_pli.typologie
                                WHERE  ".$where_date_traitement."
                        ) as traitement 
                        GROUP BY  infos_datamatrix,txt_anomalie,nom_societe,dt_event,date_courrier,lot_scan,pli,statut,etape,motif_rejet,description_rejet,type_ko,typologie,traitement,txt_anomalie,libelle_motif,titre_titre,titre_societe_nom,nb_mvt,autre_motif_ko,id_pli,libelle,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
                        ) as resultat group by nom_societe,dt_event,date_courrier,lot_scan,pli,statut,etape,libelle,txt_anomalie,libelle_motif,type_ko,typologie,titre_titre,infos_datamatrix,titre_societe_nom,nb_mvt,autre_motif_ko,id_pli,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
                        ORDER BY nom_societe,dt_event,date_courrier ASC
                ";
			return $this->ged->query($sql)
                ->result_array();
        }
        else{
            $sql = "SELECT  
                societe.nom_societe,
                source.source,
                case when (flux.id_source = 1) then objet_flux else filename_origin end as sujet,
                traitement.id_flux, 
                (date_reception::date) as date_reception,
                (view_traitement_oid.date_traitement)::date as date_dernier_traitement,       
                (view_lot_saisie_flux.dt::date) as date_saisie_adv,
                id_lot_saisie_flux,
                typologie.typologie,
                case when (traitement.statut_pli = 7 and id_lot_saisie_flux is not null and view_lot_saisie_flux.dt::date = view_traitement_oid.date_traitement::date)
                then 'Saisie finie'
                else 
                case when traitement.statut_pli = 7 and view_lot_saisie_flux.dt::date = view_traitement_oid.date_traitement::date then 
                'cloture' else
                case when traitement.statut_pli = 13 then 'hors_perimetre' else  
                case when traitement.statut_pli = ANY (ARRAY[11, 12, 14, 15]) then 'Anomalie' else 
                case when traitement.statut_pli = ANY (ARRAY[4, 5]) then 'Escaladé' else 
                case when traitement.statut_pli = ANY (ARRAY[8, 9, 10]) then 'transfert_consigne' else
                case when traitement.statut_pli = 0 then 'Non traité' else
                'En cours'                                                           
                end end end end end end end as statut_pli,
                lot_advantage,
                nb_abo
              FROM public.flux
              inner join traitement on flux.id_flux = traitement.id_flux
              left join typologie on typologie.id_typologie = flux.typologie
              inner join view_traitement_oid on traitement.oid = view_traitement_oid.oid
              left join view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
              left join view_lot_saisie_flux on view_lot_saisie_flux.id = flux.id_lot_saisie_flux
              left join societe on societe.id = flux.societe
              left join source on source.id_source = flux.id_source
              where ".$where_date_traitement." ORDER BY nom_societe,date_dernier_traitement,date_reception ASC";
            
            return $this->ged_flux->query($sql)
                ->result_array();
        }

    }

    public function get_pli_restant_a_valider (){

        $sql = "SELECT count(id_pli) as nb FROM pli
			WHERE flag_validation = 1 
			AND flag_ctrl = 0 
			AND valideur IS NULL 
			AND flag_traitement = 2";
        return $this->base_64->query($sql)
            ->result_array();
    }
    public function get_pli_restant_a_corriger (){

        $sql = "SELECT count(id_pli) as nb  FROM pli 
				WHERE flag_validation = 2 
				AND flag_ctrl = 0 
				AND valideur IS NULL 
				AND flag_traitement = 2
				AND flag_avec_bdc = 1 ";
        return $this->base_64->query($sql)
            ->result_array();
    }
    public function get_societe (){

        $sql = "SELECT id, nom_societe FROM societe ORDER BY id asc
				";
        return $this->ged->query($sql)
            ->result_array();
    }

    public function get_source (){

        $sql = "SELECT id_source, source FROM source ORDER BY id_source asc
				";
        return $this->ged_flux->query($sql)
            ->result_array();
    }

    public function get_traitement_bydate($date_debut, $date_fin,$id_societe,$source){

        $clauseWhere = $clauseWhereCmplt = "";
        if($source == '3'){
            if($date_debut != '' && $date_fin != ''){
                $clauseWhere 	.= " histo_pli.dt_event::date between '".$date_debut."' and '".$date_fin."' ";
                //$clauseWhereCmplt 	.= " date_courrier::date between '".$date_debut."' and '".$date_fin."' ";
            }
            if($id_societe != ''){
                $clauseWhere .= " and data_pli.societe = '".$id_societe."' ";
                //$clauseWhereCmplt .= " and view_pli.societe = '".$id_societe."' ";

            }

            $sql = "SELECT	
                        dt_event,
                        date_courrier,
                        case when SUM(pli_traite) is null then 0 else SUM(pli_traite) end as traite,
                        case when SUM(ok) is null then 0 else SUM(ok) end as ok,
                        case when SUM(ko) is null then 0 else SUM(ko) end as ko,
                        case when SUM(ko_scan) is null then 0 else SUM(ko_scan) end as ko_scan,
                        case when SUM(ko_def) is null then 0 else SUM(ko_def) end as ko_def,
                        case when SUM(ko_inconnu) is null then 0 else SUM(ko_inconnu) end as ko_inconnu,
                        case when SUM(ko_ks) is null then 0 else SUM(ko_ks) end as ko_ks,
                        case when SUM(ko_ke) is null then 0 else SUM(ko_ke) end as ko_ke,
                        case when SUM(ko_abondonne) is null then 0 else SUM(ko_abondonne) end as ko_abondonne,
                        case when SUM(ko_circulaire) is null then 0 else SUM(ko_circulaire) end as ko_circulaire,
                        case when SUM(ko_ci_editee) is null then 0 else SUM(ko_ci_editee) end as ko_ci_editee,
                        case when SUM(ko_ci_envoyee) is null then 0 else SUM(ko_ci_envoyee) end as ko_ci_envoyee,
                        case when SUM(divise) is null then 0 else SUM(divise) end as divise,
                        case when SUM(ok_advantage) is null then 0 else SUM(ok_advantage) end as ok_advantage,
						case when SUM(ok_ci) is null then 0 else SUM(ok_ci) end as ok_ci,
						case when SUM(ko_bayard) is null then 0 else SUM(ko_bayard) end as ko_bayard,
                        sum(nb_mvt) nb_mvt
                        
                    FROM
                    (
                        SELECT 
                             id_pli,id_pli, flag_traitement,typologie,
                             case when (flag_traitement  in(9,14) and statut_saisie = 1) then 1 else 0 end as ok,
                             case when statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6, 7, 9, 10]) then count(distinct id_pli) else 0 end as ko, 						 
                             case when (statut_saisie = 2)  then count(distinct id_pli) else 0 end as ko_scan,
                             case when (statut_saisie = 3) then 1 else 0 end as ko_def,
                             case when (statut_saisie = 4) then 1 else 0 end as ko_inconnu,							 
                             case when (statut_saisie = 5) then 1 else 0 end as ko_ks,
                             case when (statut_saisie = 6) then 1 else 0 end as ko_ke,
                             case when (statut_saisie = 7) then 1 else 0 end as ko_circulaire,						 
                             case when (statut_saisie = 8) then 1 else 0 end as ko_abondonne,						 
                             case when (statut_saisie = 9) then 1 else 0 end as ko_ci_editee,						 
                             case when (statut_saisie = 10) then 1 else 0 end as ko_ci_envoyee,						 
                             case when (statut_saisie = 24) then 1 else 0 end as divise,
							 case when statut_saisie = 11  then 1 else 0 end as ok_ci,
				           case when statut_saisie = 12  then 1 else 0 end as ko_bayard,						 
                             case when ( flag_saisie = 1 and date_saisie_advantage between '".$date_debut."' and '".$date_fin."')  OR 
							 (flag_saisie = 1 AND  dt_batch between '".$date_debut."' and '".$date_fin."') 
							 then count(distinct id_pli) else 0 end as ok_advantage,
                             case when (flag_traitement  in(9,14) and statut_saisie = 1 /*AND dt_event between '".$date_debut."' and '".$date_fin."'*/ )
							 or (statut_saisie in (8,10,24) /*AND dt_batch between '".$date_debut."' and '".$date_fin."' */) then count(distinct id_pli) else 0 end as pli_traite,
                            case when statut_saisie not in(8,24) then nb_mvt else 0 end nb_mvt,
                             dt_event,
                             date_courrier
                        FROM
                        (
                        
                            SELECT distinct
                            histo_pli.dt_event::date,		
                            f_lot_numerisation.date_courrier::date,
                            histo_pli.id_pli, 
                            histo_pli.flag_traitement,
                            f_pli.typologie as code_type_pli,
                            pli,
                            f_pli.typologie,						
                            id_lot_saisie,
                            data_pli.dt_enregistrement::date AS date_saisie_advantage,
                            case when (data_pli.dt_enregistrement::date = f_lot_numerisation.date_courrier::date or data_pli.dt_batch::date = f_lot_numerisation.date_courrier::date ) then  
                            mvt_nbr else 0 end as nb_mvt,
							case when data_pli.avec_chq = 1 then data_pli.id_pli else null end  AS flag_chq,
                            --view_pli_avec_cheque_new.id_pli AS flag_chq,
                            data_pli.statut_saisie,
							flag_saisie,
                            dt_batch,
                            data_pli.societe
							FROM f_histo_pli_by_last_date_event('".$date_debut."' , '".$date_fin."') histo_pli 
							INNER JOIN f_pli ON f_pli.id_pli  = histo_pli.id_pli
                            INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation  = f_lot_numerisation.id_lot_numerisation
                            LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli 
                           -- LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli 
                            WHERE  ".$clauseWhere." 								
                        ) as traitement
                        GROUP BY id_pli,pli,flag_traitement,typologie,nb_mvt,dt_event,date_courrier,id_lot_saisie,date_saisie_advantage,flag_chq,statut_saisie,flag_saisie,dt_batch
    
                    ) as result GROUP BY dt_event,date_courrier
                    ORDER BY dt_event,date_courrier asc
    
            ";
            // echo "<pre>";
            // print_r($sql);
            // echo "</pre>";exit;
            

            return $this->ged->query($sql)
                ->result_array();
        }
        else{
            if($date_debut != '' && $date_fin != ''){
                $clauseWhere 	.= " traitement.date_traitement::date between '".$date_debut."' and '".$date_fin."' ";
                //$clauseWhereCmplt 	.= " date_courrier::date between '".$date_debut."' and '".$date_fin."' ";
            }
            if($id_societe != ''){
                $clauseWhere .= " and societe = '".$id_societe."' and flux.id_source = ".$source;
                //$clauseWhereCmplt .= " and view_pli.societe = '".$id_societe."' ";

            }

            $sql = "SELECT
                dt_event,date_reception,
                 sum(abo_j) as abo_j,
                 sum(cloture) as cloture,
                sum(saisie_finie) as saisie_finie, 
				sum(flux_ok) as flux_ok,
                sum(anomalie) as anomalie,             
                sum(hors_perimetre) as hors_perimetre,                
                sum(escalade) as escalade,
                sum(transfert_consigne) as transfert_consigne,
				sum(ko_def) as ko_def,
				sum(ko_inconnu) as ko_inconnu,
				sum(ko_ettent) as ko_ettent,
				sum(ko_bayard) as ko_bayard
               
                FROM
                (              
                    SELECT id_flux, dt_event,date_reception,
                                   case when statut_pli = 7 and id_lot_saisie_flux is not null and date_saisie = dt_event then 1 else 0 end as saisie_finie,
                                   /*case when statut_pli = 7 and date_saisie = dt_event then 1 else 0 end as cloture,
                                   case when statut_pli = 13 then 1 else 0 end as hors_perimetre,
                                   case when statut_pli = ANY (ARRAY[11, 12, 14, 15]) then 1 else 0 end as anomalie,*/
								   case when statut_pli = ANY (ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) and etat_pli_id = 1 then 1 else 0 end as flux_ok,
								   case when statut_pli = ANY (ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) and etat_pli_id = 1 and date_saisie = dt_event then 1 else 0 end as cloture,
                                   case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 13  then 1 else 0 end as hors_perimetre,
                                   case when statut_pli =  ANY (ARRAY[2, 7]) and etat_pli_id = ANY (ARRAY[11, 12, 14, 15]) then 1 else 0 end as anomalie,
                                   case when statut_pli = ANY (ARRAY[4, 5]) then 1 else 0 end as escalade,
                                   case when statut_pli = ANY (ARRAY[8, 9, 10]) then 1 else 0 end as transfert_consigne,
                                   case when (statut_pli = 7 and id_lot_saisie_flux is not null and date_saisie = dt_event) 
								   		OR  (statut_pli = ANY (ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) and etat_pli_id = 1 ) 
										OR statut_pli = 13 
										OR (statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = ANY (ARRAY[11, 12, 14, 15]))
										OR statut_pli = ANY (ARRAY[4, 5])
										OR statut_pli = ANY (ARRAY[8, 9, 10])
										OR (statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 21 )
										OR (statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 22 )
										OR (statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 23 )
										then nb_abo else 0 end as abo_j,
								   case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 21 then 1 else 0 end as ko_def,
									case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 22 then 1 else 0 end as ko_inconnu,
									case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 23 then 1 else 0 end as ko_ettent,
									case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 24 then 1 else 0 end as ko_bayard
                    FROM
                    (
                    SELECT  traitement.id_flux, flag_anomalie_pj, flag_hors_perimetre, 
                           flag_traitement_niveau, flux.statut_pli,--view_traitement_oid.statut_pli, 
                           flux.typologie,id_lot_saisie_flux,
                           view_traitement_oid.date_traitement::date as dt_event,
                           date_reception::date as date_reception,
                           view_lot_saisie_flux.dt::date as date_saisie,
                           nb_abo,/*etat_flux_h as*/ etat_pli_id,id_traitement
                      FROM public.flux
                      inner join traitement on flux.id_flux = traitement.id_flux and id_traitement = (select id_traitement from traitement where id_flux = flux.id_flux order by id_traitement desc limit 1)
                      inner join view_traitement_oid on traitement.oid = view_traitement_oid.oid
                      left join view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
                      left join view_lot_saisie_flux on view_lot_saisie_flux.id = flux.id_lot_saisie_flux
                      where ".$clauseWhere." 
                      ) as data_flux
                      GROUP BY id_flux,statut_pli,id_lot_saisie_flux,date_saisie,date_reception,dt_event,nb_abo,etat_pli_id
                ) as data
                  GROUP BY dt_event,date_reception

            ";
            /*echo "<pre>";
            print_r($sql);
            echo "</pre>";exit;
            */

            return $this->ged_flux->query($sql)
                ->result_array();
        }
    }

    public function get_traitement_typologie($date_debut, $date_fin,$id_societe){
		
		$clauseWhere = $clauseWhereSociete = "";
		if($date_debut != '' && $date_fin != ''){
				$clauseWhere 	.= "and (dt_event::date between '".$date_debut."' and '".$date_fin."') ";
		  }
		if($id_societe != ''){
				$clauseWhereSociete .= " and f_pli.societe = '".$id_societe."' ";	
		}
		
		$sql = "
		SELECT date_courrier,typologie,dt_event,SUM(nb_mvt) as nb_mvt,SUM(nb_pli) as nb_pli 
		FROM (
				select f_lot_numerisation.date_courrier::date,typologie.typologie,histo.dt_event::date,view_nb_mouvement_pli.nb_mvt,count(f_pli.id_pli) AS nb_pli
				from f_pli 
				left join f_lot_numerisation on f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
				left join view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
				left join societe on societe.id = f_pli.societe
				left join typologie on typologie.id = f_pli.typologie
				inner join
					(
							SELECT id, id_pli, flag_traitement, dt_event
							FROM f_histo_pli_by_last_date_event('".$date_debut."' , '".$date_fin."') view_pli_traitement 
							/*FROM view_pli_traitement*/
							where (flag_traitement >= 5 and flag_traitement not in (16,17,18,19,20,21,24))
							".$clauseWhere."
					) histo on histo.id_pli = f_pli.id_pli
				where f_pli.flag_traitement >= 5 and f_pli.flag_traitement NOT IN (16, 17, 18, 19,20,21,24) ".$clauseWhereSociete."
				GROUP BY f_pli.id_pli,f_lot_numerisation.date_courrier,typologie.typologie,histo.dt_event,view_nb_mouvement_pli.nb_mvt
			) as typo
			GROUP BY date_courrier,typologie,dt_event";
		
		/*echo "<pre>";
		print_r($sql);
		echo "</pre>";exit;
		*/
		return $this->ged->query($sql)
							->result_array();
	}
	

	public function get_suivi_solde($date_j, $id_societe){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and societe = '".$id_societe."'";
        $sql = "	SELECT
					societe,nom_societe, date_courrier,pli_total,pli_saisie_adv_jmoins,stock_initial,pli_saisie_adv_j,stock_final,
					pli_saisie_adv_j_ok,pli_saisie_adv_j_ci,pli_saisie_adv_j_ko
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_courrier,pli_total/*, pli_en_prod_j*/, 			
					coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins, 
					coalesce(pli_total,0)-coalesce(pli_saisie_adv_jmoins,0) as stock_initial, 
					coalesce(pli_saisie_adv_j,0) as pli_saisie_adv_j,
					coalesce(pli_total,0)-coalesce(pli_saisie_adv_j,0)-coalesce(pli_saisie_adv_jmoins,0) as stock_final,
					coalesce(pli_saisie_adv_j_ok,0) as pli_saisie_adv_j_ok,
					coalesce(pli_saisie_adv_j_ci,0) as pli_saisie_adv_j_ci,
					coalesce(pli_saisie_adv_j_ko, 0) as pli_saisie_adv_j_ko
					FROM 
					(
						SELECT date_courrier::date as date_courrier,  count(id_pli) as pli_total ,societe
						FROM view_pli_stat 
						WHERE concat(date_courrier::date,societe)::text
							in (SELECT concat(date_courrier::date,societe)
							FROM  view_pli_stat
							WHERE 
								dt_event::date = '".$date_j."' ".$where_soc." 
							GROUP BY date_courrier,societe
							)
						GROUP BY date_courrier::date,societe
					
					) as data_total

					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,view_pli_stat.societe, nom_societe, COUNT(id_pli) as pli_en_prod_j
						FROM  view_pli_stat
						WHERE 
							dt_event::date = '".$date_j."' ".$where_soc." 
						GROUP BY societe,nom_societe, date_courrier
					
					)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier and data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						
						Select adv_total.date_courrier,adv_total.societe,
							coalesce(pli_saisie_adv_j,0) pli_saisie_adv_j,
							coalesce(pli_saisie_adv_j_ok,0) pli_saisie_adv_j_ok,
							coalesce(pli_saisie_adv_j_ci,0) pli_saisie_adv_j_ci,
							coalesce(pli_saisie_adv_j_ko,0) pli_saisie_adv_j_ko
						FROM	
						(
						SELECT date_courrier::date as date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_j
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null
						GROUP BY date_courrier,societe
						)as adv_total
						LEFT JOIN
						(
						SELECT date_courrier::date as date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_j_ok
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null and statut_saisie in (1)
						GROUP BY date_courrier,societe
						) as adv_ok on adv_ok.date_courrier = adv_total.date_courrier and adv_ok.societe = adv_total.societe
						LEFT JOIN(
						SELECT date_courrier::date,societe, count(id_lot_saisie) as pli_saisie_adv_j_ci
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc."  
						and id_lot_saisie is not null and statut_saisie in (3)
						GROUP BY date_courrier,societe
						) as adv_ci on adv_ci.date_courrier = adv_total.date_courrier and adv_ci.societe = adv_total.societe
						LEFT JOIN(
						SELECT date_courrier::date,societe, count(id_lot_saisie) as pli_saisie_adv_j_ko
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc."  
						and id_lot_saisie is not null and statut_saisie not in (1,3)
						GROUP BY date_courrier,societe
						)adv_ko on adv_ko.date_courrier = adv_total.date_courrier and adv_ko.societe = adv_total.societe
					
					)as data_saisie_advantage on data_saisie_advantage.date_courrier = data_ttt_j.date_courrier and data_saisie_advantage.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_jmoins
						FROM view_pli_stat
						WHERE date_saisie_advantage < '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null
						GROUP BY date_courrier,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier and data_saisie_advantage1.societe = data_ttt_j.societe
					
					WHERE pli_en_prod_j is not null
				)as data
				ORDER BY societe,date_courrier  asc
			";

        return $this->ged->query($sql)
            ->result_array();

    }

    public function get_suivi_solde_courrier($date_deb, $date_fin, $date_j, $id_societe){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and societe = '".$id_societe."'";
        $sql = "	SELECT
					societe,nom_societe, date_courrier,pli_total,pli_saisie_adv_jmoins,stock_initial,pli_saisie_adv_j,stock_final,
					pli_saisie_adv_j_ok,pli_saisie_adv_j_ci,pli_saisie_adv_j_ko
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_courrier,pli_total/*, pli_en_prod_j*/, 			
					coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins, 
					coalesce(pli_total,0)-coalesce(pli_saisie_adv_jmoins,0) as stock_initial, 
					coalesce(pli_saisie_adv_j,0) as pli_saisie_adv_j,
					coalesce(pli_total,0)-coalesce(pli_saisie_adv_j,0)-coalesce(pli_saisie_adv_jmoins,0) as stock_final,
					coalesce(pli_saisie_adv_j_ok,0) as pli_saisie_adv_j_ok,
					coalesce(pli_saisie_adv_j_ci,0) as pli_saisie_adv_j_ci,
					coalesce(pli_saisie_adv_j_ko, 0) as pli_saisie_adv_j_ko
					FROM 
					(
						SELECT date_courrier::date as date_courrier,  count(id_pli) as pli_total ,societe
						FROM view_pli_stat 
						WHERE concat(date_courrier::date,societe)::text
							in (SELECT concat(date_courrier::date,societe)
							FROM  view_pli_stat
							WHERE 
								date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
							GROUP BY date_courrier,societe
							)
						GROUP BY date_courrier::date,societe
					
					) as data_total

					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,view_pli_stat.societe, nom_societe, COUNT(id_pli) as pli_en_prod_j
						FROM  view_pli_stat
						WHERE 
							date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						GROUP BY societe,nom_societe, date_courrier
					
					)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier and data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						
						Select adv_total.date_courrier,adv_total.societe,
							coalesce(pli_saisie_adv_j,0) pli_saisie_adv_j,
							coalesce(pli_saisie_adv_j_ok,0) pli_saisie_adv_j_ok,
							coalesce(pli_saisie_adv_j_ci,0) pli_saisie_adv_j_ci,
							coalesce(pli_saisie_adv_j_ko,0) pli_saisie_adv_j_ko
						FROM	
						(
						SELECT date_courrier::date as date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_j
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null
						GROUP BY date_courrier,societe
						)as adv_total
						LEFT JOIN
						(
						SELECT date_courrier::date as date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_j_ok
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null and statut_saisie in (1)
						GROUP BY date_courrier,societe
						) as adv_ok on adv_ok.date_courrier = adv_total.date_courrier and adv_ok.societe = adv_total.societe
						LEFT JOIN(
						SELECT date_courrier::date,societe, count(id_lot_saisie) as pli_saisie_adv_j_ci
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc."  
						and id_lot_saisie is not null and statut_saisie in (3)
						GROUP BY date_courrier,societe
						) as adv_ci on adv_ci.date_courrier = adv_total.date_courrier and adv_ci.societe = adv_total.societe
						LEFT JOIN(
						SELECT date_courrier::date,societe, count(id_lot_saisie) as pli_saisie_adv_j_ko
						FROM view_pli_stat
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc."  
						and id_lot_saisie is not null and statut_saisie not in (1,3)
						GROUP BY date_courrier,societe
						)adv_ko on adv_ko.date_courrier = adv_total.date_courrier and adv_ko.societe = adv_total.societe
					
					)as data_saisie_advantage on data_saisie_advantage.date_courrier = data_ttt_j.date_courrier and data_saisie_advantage.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_jmoins
						FROM view_pli_stat
						WHERE date_saisie_advantage < '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null
						GROUP BY date_courrier,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier and data_saisie_advantage1.societe = data_ttt_j.societe
					
					WHERE pli_en_prod_j is not null
				)as data
				ORDER BY societe,date_courrier  asc
			";

        return $this->ged->query($sql)
            ->result_array();

    }

    public function get_reception_anomalie($date_j, $id_societe){
        if($id_societe != '') $where_soc = " and view_pli_stat.societe = '".$id_societe."'";
        $sql = "SELECT coalesce(anomalie,0) as anomalie, 
					data_soc.id as societe,
					case when date_courrier is null then '".$date_j."'::date else date_courrier end as date_courrier
				FROM 
				(
					SELECT id, societe
					FROM societe
				) as data_soc
				LEFT JOIN 
				(
					SELECT count(id_pli) as anomalie,societe.id as societe,date_courrier::date as date_courrier
						FROM view_pli_stat
						LEFT JOIN societe on societe.id = view_pli_stat.societe 
						WHERE date_courrier::date = '".$date_j."' ".$where_soc."
						and flag_traitement in (16,21)						
					GROUP BY societe.id,date_courrier::date
				) as data on data.societe = data_soc.id
				";
        return $this->ged->query($sql)
            ->result_array();
    }

    public function get_suivi_solde_mvt($date_j, $id_societe){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and societe = '".$id_societe."'";
        $sql = "	SELECT 
					societe,nom_societe, date_courrier,mvt_total,mvt_saisie_adv_jmoins,
					stock_initial,mvt_saisie_adv_j,stock_final, mvt_saisie_adv_j_ok,mvt_saisie_adv_j_ci,mvt_saisie_adv_j_ko 
					FROM (
						SELECT data_total.societe,nom_societe, data_total.date_courrier,mvt_total/*, mvt_en_prod_j*/, 
						coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins, 
						coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_initial, 
						coalesce(mvt_saisie_adv_j,0) as mvt_saisie_adv_j, 
						coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_j,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_final, 
						coalesce(mvt_saisie_adv_j_ok,0) as mvt_saisie_adv_j_ok, 
						coalesce(mvt_saisie_adv_j_ci,0) as mvt_saisie_adv_j_ci, 
						coalesce(mvt_saisie_adv_j_ko, 0) as mvt_saisie_adv_j_ko 
						FROM ( 

							SELECT date_courrier::date as date_courrier, 
							sum(nb_mvt) as mvt_total ,societe 
							FROM view_pli_stat 
							LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
							WHERE concat(date_courrier::date,societe)::text in 
								(SELECT concat(date_courrier::date,societe) 
								FROM view_pli_stat 
								WHERE dt_event::date = '".$date_j."' ".$where_soc." 
								GROUP BY date_courrier,societe ) GROUP BY date_courrier::date,societe 
						) as data_total 
						LEFT JOIN 
						( 
							SELECT date_courrier::date as date_courrier ,view_pli_stat.societe, nom_societe, 
							sum(nb_mvt) as mvt_en_prod_j
							 FROM view_pli_stat
							 LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
							 WHERE dt_event::date = '".$date_j."' ".$where_soc." 
							 GROUP BY societe,nom_societe, date_courrier 
						 )as data_ttt_j 
						 on data_total.date_courrier = data_ttt_j.date_courrier 
						 and data_total.societe = data_ttt_j.societe 
						 LEFT JOIN 
						 ( 
								Select adv_total.date_courrier,adv_total.societe, 
								coalesce(mvt_saisie_adv_j,0) mvt_saisie_adv_j, coalesce(mvt_saisie_adv_j_ok,0) mvt_saisie_adv_j_ok, 
								coalesce(mvt_saisie_adv_j_ci,0) mvt_saisie_adv_j_ci, 
								coalesce(mvt_saisie_adv_j_ko,0) mvt_saisie_adv_j_ko 
								FROM (
									SELECT date_courrier::date as date_courrier,societe, 
									sum(nb_mvt) as mvt_saisie_adv_j 
									FROM view_pli_stat
									LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
									WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
									and id_lot_saisie is not null 
									GROUP BY date_courrier,societe 
								)as adv_total 
								LEFT JOIN 
								( 
								SELECT date_courrier::date as date_courrier,societe, 
								sum(nb_mvt) as mvt_saisie_adv_j_ok 
								FROM view_pli_stat 
								LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
								WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
								and id_lot_saisie is not null and statut_saisie in (1) 
								GROUP BY date_courrier,societe 
								) as adv_ok on adv_ok.date_courrier = adv_total.date_courrier 
								and adv_ok.societe = adv_total.societe 
								LEFT JOIN(
								SELECT date_courrier::date,societe, sum(nb_mvt) as mvt_saisie_adv_j_ci 
								FROM view_pli_stat 
								LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
								WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
								and id_lot_saisie is not null and statut_saisie in (3) 
								GROUP BY date_courrier,societe 
								) as adv_ci on adv_ci.date_courrier = adv_total.date_courrier 
								and adv_ci.societe = adv_total.societe 
								LEFT JOIN(
								SELECT date_courrier::date,societe, sum(nb_mvt) as mvt_saisie_adv_j_ko 
								FROM view_pli_stat 
								LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
								WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
								and id_lot_saisie is not null 
								and statut_saisie not in (1,3) GROUP BY date_courrier,societe 
								)adv_ko on adv_ko.date_courrier = adv_total.date_courrier 
								and adv_ko.societe = adv_total.societe 
						)as data_saisie_advantage 
						on data_saisie_advantage.date_courrier = data_ttt_j.date_courrier 
						and data_saisie_advantage.societe = data_ttt_j.societe 
						LEFT JOIN 
						(
						 SELECT date_courrier,societe, sum(nb_mvt) as mvt_saisie_adv_jmoins 
						 FROM view_pli_stat 
						 LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
						 WHERE date_saisie_advantage < '".$date_j."' ".$where_soc." 
						 and id_lot_saisie is not null GROUP BY date_courrier,societe 
						 )as data_saisie_advantage1 
						 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier 
						 and data_saisie_advantage1.societe = data_ttt_j.societe 
						 WHERE mvt_en_prod_j is not null 
					)as data ORDER BY societe,date_courrier asc 

			";

        return $this->ged->query($sql)
            ->result_array();

    }

    public function get_suivi_solde_mvt_courrier($date_deb, $date_fin, $date_j, $id_societe){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and societe = '".$id_societe."'";
        $sql = "
			SELECT
					societe,nom_societe, date_courrier,mvt_total,mvt_saisie_adv_jmoins,stock_initial,mvt_saisie_adv_j,stock_final,
					mvt_saisie_adv_j_ok,mvt_saisie_adv_j_ci,mvt_saisie_adv_j_ko
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_courrier,mvt_total/*, mvt_en_prod_j*/, 			
					coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins, 
					coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_initial, 
					coalesce(mvt_saisie_adv_j,0) as mvt_saisie_adv_j,
					coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_j,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_final,
					coalesce(mvt_saisie_adv_j_ok,0) as mvt_saisie_adv_j_ok,
					coalesce(mvt_saisie_adv_j_ci,0) as mvt_saisie_adv_j_ci,
					coalesce(mvt_saisie_adv_j_ko, 0) as mvt_saisie_adv_j_ko
					FROM 
					(
						SELECT date_courrier::date as date_courrier,  sum(nb_mvt) as mvt_total ,societe
						FROM view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
						WHERE concat(date_courrier::date,societe)::text
							in (SELECT concat(date_courrier::date,societe)
							FROM  view_pli_stat
							WHERE 
								date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
							GROUP BY date_courrier,societe
							)
						GROUP BY date_courrier::date,societe
					
					) as data_total

					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,view_pli_stat.societe, nom_societe, sum(nb_mvt) as mvt_en_prod_j
						FROM  view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE 
							date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						GROUP BY societe,nom_societe, date_courrier
					
					)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier and data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						
						Select adv_total.date_courrier,adv_total.societe,
							coalesce(mvt_saisie_adv_j,0) mvt_saisie_adv_j,
							coalesce(mvt_saisie_adv_j_ok,0) mvt_saisie_adv_j_ok,
							coalesce(mvt_saisie_adv_j_ci,0) mvt_saisie_adv_j_ci,
							coalesce(mvt_saisie_adv_j_ko,0) mvt_saisie_adv_j_ko
						FROM	
						(
						SELECT date_courrier::date as date_courrier,societe, sum(nb_mvt) as mvt_saisie_adv_j
						FROM view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null
						GROUP BY date_courrier,societe
						)as adv_total
						LEFT JOIN
						(
						SELECT date_courrier::date as date_courrier,societe, sum(nb_mvt) as mvt_saisie_adv_j_ok
						FROM view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null and statut_saisie in (1)
						GROUP BY date_courrier,societe
						) as adv_ok on adv_ok.date_courrier = adv_total.date_courrier and adv_ok.societe = adv_total.societe
						LEFT JOIN(
						SELECT date_courrier::date,societe, sum(nb_mvt) as mvt_saisie_adv_j_ci
						FROM view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc."  
						and id_lot_saisie is not null and statut_saisie in (3)
						GROUP BY date_courrier,societe
						) as adv_ci on adv_ci.date_courrier = adv_total.date_courrier and adv_ci.societe = adv_total.societe
						LEFT JOIN(
						SELECT date_courrier::date,societe, sum(nb_mvt) as mvt_saisie_adv_j_ko
						FROM view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE date_saisie_advantage = '".$date_j."' ".$where_soc."  
						and id_lot_saisie is not null and statut_saisie not in (1,3)
						GROUP BY date_courrier,societe
						)adv_ko on adv_ko.date_courrier = adv_total.date_courrier and adv_ko.societe = adv_total.societe
					
					)as data_saisie_advantage on data_saisie_advantage.date_courrier = data_ttt_j.date_courrier and data_saisie_advantage.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_courrier,societe, sum(nb_mvt) as mvt_saisie_adv_jmoins
						FROM view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE date_saisie_advantage < '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null
						GROUP BY date_courrier,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier and data_saisie_advantage1.societe = data_ttt_j.societe
					
					WHERE mvt_en_prod_j is not null
				)as data
				ORDER BY societe,date_courrier  asc";

        return $this->ged->query($sql)
            ->result_array();

    }

    public function get_reception_anomalie_mvt($date_j, $id_societe){
        if($id_societe != '') $where_soc = " and view_pli_stat.societe = '".$id_societe."'";
        $sql = "SELECT coalesce(anomalie,0) as anomalie, 
					data_soc.id as societe,
					case when date_courrier is null then '".$date_j."'::date else date_courrier end as date_courrier
				FROM 
				(
					SELECT id, societe
					FROM societe
				) as data_soc
				LEFT JOIN 
				(
					SELECT sum(nb_mvt) as anomalie,societe.id as societe,date_courrier::date as date_courrier
						FROM view_pli_stat
						LEFT JOIN societe on societe.id = view_pli_stat.societe
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 						
						WHERE date_courrier::date = '".$date_j."' ".$where_soc."
						and flag_traitement in (16,21)						
					GROUP BY societe.id,date_courrier::date
				) as data on data.societe = data_soc.id
				";
        return $this->ged->query($sql)
            ->result_array();
    }

    public function get_solde_courrier_sla($date_deb, $date_fin, $date_j, $id_societe){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and societe = '".$id_societe."'";
        $sql = "	SELECT
					societe,nom_societe, date_courrier,pli_total,
					pli_saisie_j,pli_saisie_j_plus_1,pli_saisie_j_plus_2,pli_saisie_j_plus_plus,
					pli_non_traite,pli_att_saisie
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_courrier,pli_total/*, pli_en_prod_j*/, 			
					coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins,  
					coalesce(pli_saisie_j, 0) as pli_saisie_j,
					coalesce(pli_saisie_j_plus_1, 0) as pli_saisie_j_plus_1,
					coalesce(pli_saisie_j_plus_2, 0) as pli_saisie_j_plus_2,
					coalesce(pli_saisie_j_plus_plus, 0) as pli_saisie_j_plus_plus,
					coalesce(pli_non_traite, 0) as pli_non_traite,
					coalesce(pli_att_saisie, 0) as pli_att_saisie
					
					FROM 
					(
						SELECT date_courrier::date as date_courrier,  count(id_pli) as pli_total ,societe
						FROM view_pli_stat 
						WHERE concat(date_courrier::date,societe)::text
							in (SELECT concat(date_courrier::date,societe)
							FROM  view_pli_stat
							WHERE 
								date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
							GROUP BY date_courrier,societe
							)
						GROUP BY date_courrier::date,societe
					
					) as data_total
					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier,  count(id_pli) as pli_non_traite ,societe
						FROM view_pli_stat 
						WHERE date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						and (flag_traitement = 0 or flag_traitement is null)
						GROUP BY date_courrier::date,societe
					
					) as data_non_traite on data_total.date_courrier = data_non_traite.date_courrier and data_total.societe = data_non_traite.societe
					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,view_pli_stat.societe, COUNT(view_pli_stat.id_pli) as pli_att_saisie
						FROM  view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE 
							date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc."  
							and id_lot_saisie is null
							and view_pli_stat.flag_traitement in (1,2,3,4)
						GROUP BY societe,nom_societe, date_courrier					
					)as data_att_saisie on data_total.date_courrier = data_att_saisie.date_courrier and data_total.societe = data_att_saisie.societe
					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,view_pli_stat.societe, nom_societe, COUNT(id_pli) as pli_en_prod_j
						FROM  view_pli_stat
						WHERE 
							date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						GROUP BY societe,nom_societe, date_courrier
					
					)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier and data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_courrier,societe, count(id_lot_saisie) as pli_saisie_adv_jmoins
						FROM view_pli_stat
						WHERE date_saisie_advantage < '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null
						GROUP BY date_courrier,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier and data_saisie_advantage1.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT
							date_courrier ,societe, sum(pli_saisie_j) as pli_saisie_j,sum(pli_saisie_j_plus_1) as pli_saisie_j_plus_1,
							sum(pli_saisie_j_plus_2) as pli_saisie_j_plus_2,sum(pli_saisie_j_plus_plus) as pli_saisie_j_plus_plus
						FROM
						(
							SELECT date_courrier::date as date_courrier,societe,
							case when ( date_courrier::date = date_saisie_advantage::date) 
							then count(id_lot_saisie) else 0
							end as pli_saisie_j,
							case when extract(dow FROM (date_courrier::date+ INTERVAL '1 day')::date)::integer = 0 
								then 
									case when ( (date_courrier::date+ INTERVAL '2 day')::date = date_saisie_advantage::date) 
									then count(id_lot_saisie) else 0
									end
								else 
									case when ( (date_courrier::date+ INTERVAL '1 day')::date = date_saisie_advantage::date) 
									then count(id_lot_saisie) else 0
									end
							end as pli_saisie_j_plus_1,
							case when extract(dow FROM (date_courrier::date+ INTERVAL '2 day')::date) = 0 
								then 
									case when ( (date_courrier::date+ INTERVAL '3 day')::date = date_saisie_advantage::date) 
									then count(id_lot_saisie) else 0
									end
								else 
									case when ( (date_courrier::date+ INTERVAL '2 day')::date = date_saisie_advantage::date) 
									then count(id_lot_saisie) else 0
									end
							end as pli_saisie_j_plus_2,

							case when extract(dow FROM (date_courrier::date+ INTERVAL '2 day')::date) = 0 
								then 
									case when ( (date_courrier::date+ INTERVAL '3 day')::date < date_saisie_advantage::date) 
									then count(id_lot_saisie) else 0
									end
								else 
									case when ( (date_courrier::date+ INTERVAL '2 day')::date < date_saisie_advantage::date) 
									then count(id_lot_saisie) else 0
									end
							end as pli_saisie_j_plus_plus
							FROM view_pli_stat
							LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
							WHERE date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
							and id_lot_saisie is not null
							GROUP BY date_courrier,societe,date_saisie_advantage
						) as data_saisie
						GROUP BY date_courrier ,societe
					)as data_saisie_jplus on data_saisie_jplus.date_courrier = data_ttt_j.date_courrier and data_saisie_jplus.societe = data_ttt_j.societe
					
					WHERE pli_en_prod_j is not null
				)as data
				ORDER BY societe,date_courrier  asc
			";
        /*echo "<pre>";
        print_r($sql);
        echo "</pre>";
        */
        return $this->ged->query($sql)
            ->result_array();

    }

    public function get_suivi_mvt_courrier_sla($date_deb, $date_fin, $date_j, $id_societe){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and societe = '".$id_societe."'";
        $sql = "
			SELECT
					societe,nom_societe, date_courrier,mvt_total,mvt_saisie_adv_jmoins
					mvt_saisie_j,mvt_saisie_j_plus_1,mvt_saisie_j_plus_2,mvt_saisie_j_plus_plus,
					mvt_non_traite,mvt_att_saisie
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_courrier,mvt_total/*, mvt_en_prod_j*/, 			
					coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins, 
					coalesce(mvt_total,0)-coalesce(mvt_saisie_adv_jmoins,0) as stock_initial, 
					coalesce(mvt_saisie_j, 0) as mvt_saisie_j,
					coalesce(mvt_saisie_j_plus_1, 0) as mvt_saisie_j_plus_1,
					coalesce(mvt_saisie_j_plus_2, 0) as mvt_saisie_j_plus_2,
					coalesce(mvt_saisie_j_plus_plus, 0) as mvt_saisie_j_plus_plus,
					coalesce(mvt_non_traite, 0) as mvt_non_traite,
					coalesce(mvt_att_saisie, 0) as mvt_att_saisie
					FROM 
					(
						SELECT date_courrier::date as date_courrier,  sum(nb_mvt) as mvt_total ,societe
						FROM view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli 
						WHERE concat(date_courrier::date,societe)::text
							in (SELECT concat(date_courrier::date,societe)
							FROM  view_pli_stat
							WHERE 
								date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
							GROUP BY date_courrier,societe
							)
						GROUP BY date_courrier::date,societe
					
					) as data_total
					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier, sum(nb_mvt) as mvt_non_traite ,societe
						FROM view_pli_stat 
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE 
								date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
								and (flag_traitement = 0 or flag_traitement is null)
						GROUP BY date_courrier::date,societe
					
					) as data_non_traite on data_total.date_courrier = data_non_traite.date_courrier and data_total.societe = data_non_traite.societe					
					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,view_pli_stat.societe,  sum(nb_mvt) as mvt_att_saisie
						FROM  view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE 
							date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc."  
							and id_lot_saisie is null
							and view_pli_stat.flag_traitement in (1,2,3,4)
						GROUP BY societe,nom_societe, date_courrier					
					)as data_att_saisie on data_total.date_courrier = data_att_saisie.date_courrier and data_total.societe = data_att_saisie.societe
					LEFT JOIN
					(
						SELECT date_courrier::date as date_courrier ,view_pli_stat.societe, nom_societe, sum(nb_mvt) as mvt_en_prod_j
						FROM  view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE 
							date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						GROUP BY societe,nom_societe, date_courrier
					
					)as data_ttt_j on data_total.date_courrier = data_ttt_j.date_courrier and data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_courrier,societe, sum(nb_mvt) as mvt_saisie_adv_jmoins
						FROM view_pli_stat
						LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
						WHERE date_saisie_advantage < '".$date_j."' ".$where_soc." 
						and id_lot_saisie is not null
						GROUP BY date_courrier,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_courrier = data_ttt_j.date_courrier and data_saisie_advantage1.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT
							date_courrier ,societe, sum(mvt_saisie_j) as mvt_saisie_j,
							sum(mvt_saisie_j_plus_1) as mvt_saisie_j_plus_1,
							sum(mvt_saisie_j_plus_2) as mvt_saisie_j_plus_2,
							sum(mvt_saisie_j_plus_plus) as mvt_saisie_j_plus_plus
						FROM
						(	
							SELECT date_courrier::date as date_courrier,societe, 
							case when ( date_courrier::date = date_saisie_advantage::date) 
							then sum(nb_mvt) else 0
							end as mvt_saisie_j,
							case when extract(dow FROM (date_courrier::date+ INTERVAL '1 day')::date)::integer = 0 
								then 
									case when ( (date_courrier::date+ INTERVAL '2 day')::date = date_saisie_advantage::date) 
									then sum(nb_mvt) else 0
									end
								else 
									case when ( (date_courrier::date+ INTERVAL '1 day')::date = date_saisie_advantage::date) 
									then sum(nb_mvt) else 0
									end
							end as mvt_saisie_j_plus_1,
							case when extract(dow FROM (date_courrier::date+ INTERVAL '2 day')::date) = 0 
								then 
									case when ( (date_courrier::date+ INTERVAL '3 day')::date = date_saisie_advantage::date) 
									then sum(nb_mvt) else 0
									end
								else 
									case when ( (date_courrier::date+ INTERVAL '2 day')::date = date_saisie_advantage::date) 
									then sum(nb_mvt) else 0
									end
							end as mvt_saisie_j_plus_2,

							case when extract(dow FROM (date_courrier::date+ INTERVAL '2 day')::date) = 0 
								then 
									case when ( (date_courrier::date+ INTERVAL '3 day')::date < date_saisie_advantage::date) 
									then sum(nb_mvt) else 0
									end
								else 
									case when ( (date_courrier::date+ INTERVAL '2 day')::date < date_saisie_advantage::date) 
									then sum(nb_mvt) else 0
									end
							end as mvt_saisie_j_plus_plus
							FROM view_pli_stat
							LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
							WHERE date_courrier::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
							and id_lot_saisie is not null
							GROUP BY date_courrier,societe,date_saisie_advantage
						) as data_saisie
						GROUP BY date_courrier ,societe
					)as data_saisie_jplus on data_saisie_jplus.date_courrier = data_ttt_j.date_courrier and data_saisie_jplus.societe = data_ttt_j.societe
					
					WHERE mvt_en_prod_j is not null
				)as data
				ORDER BY societe,date_courrier  asc";

        return $this->ged->query($sql)
            ->result_array();

    }
    public function get_reception_anomalie_sla($date_j, $id_societe){
        if($id_societe != '') $where_soc = " and view_pli_stat.societe = '".$id_societe."'";
        $sql = "SELECT societe,date_courrier,
				sum(ko_scan) as ko_scan,
				sum(hors_peri) as hors_peri,
				sum(mvt_ko_scan) as mvt_ko_scan,
				sum(mvt_hors_peri) as mvt_hors_peri
				FROM 
				(
					SELECT coalesce(ko_scan,0) as ko_scan, coalesce(hors_peri,0) as hors_peri, 
							coalesce(mvt_ko_scan,0) as mvt_ko_scan, coalesce(mvt_hors_peri,0) as mvt_hors_peri, 
							data_soc.id as societe,
							case when date_courrier is null then '".$date_j."'::date else date_courrier end as date_courrier
						FROM 
						(
							SELECT id, societe
							FROM societe
						) as data_soc
						LEFT JOIN 
						(
							SELECT societe.id as societe,date_courrier::date as date_courrier,
								case when view_pli_stat.flag_traitement = 16 then count(distinct view_pli_stat.id_pli) else 0 end as ko_scan,
								case when view_pli_stat.flag_traitement = 21 then count(distinct view_pli_stat.id_pli) else 0 end as hors_peri,
								case when view_pli_stat.flag_traitement = 16 then sum(nb_mvt) else 0 end as mvt_ko_scan,
								case when view_pli_stat.flag_traitement = 21 then sum(nb_mvt) else 0 end as mvt_hors_peri
								FROM view_pli_stat
								LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
								LEFT JOIN societe on societe.id = view_pli_stat.societe 
								WHERE date_courrier::date = '".$date_j."' ".$where_soc."
								and view_pli_stat.flag_traitement in (16,21)								
							GROUP BY societe.id,date_courrier::date,view_pli_stat.flag_traitement
						) as data on data.societe = data_soc.id
				) as res
				GROUP BY  societe,date_courrier
				ORDER BY  societe,date_courrier asc
				";
        /*echo "<pre>";
        print_r($sql);
        echo "</pre>";
        exit;
        */

        return $this->ged->query($sql)
            ->result_array();
    }

    public function get_solde_sftp_sla($date_deb, $date_fin, $date_j, $id_societe, $id_src){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and flux.societe = '".$id_societe."'";
        if($id_src != '') $where_soc .= " and flux.id_source = '".$id_src."'";

        $sql = "SELECT
					societe, date_reception as date_courrier,pli_total,
					pli_saisie_j,pli_saisie_j_plus_1,pli_saisie_j_plus_2,pli_saisie_j_plus_plus,
					pli_non_traite,pli_att_saisie,nom_societe
				FROM
				(
	  
					SELECT data_total.societe, data_total.date_reception,pli_total/*, pli_en_prod_j*/, 			
					coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins,  
					coalesce(pli_saisie_j, 0) as pli_saisie_j,
					coalesce(pli_saisie_j_plus_1, 0) as pli_saisie_j_plus_1,
					coalesce(pli_saisie_j_plus_2, 0) as pli_saisie_j_plus_2,
					coalesce(pli_saisie_j_plus_plus, 0) as pli_saisie_j_plus_plus,
					coalesce(pli_non_traite, 0) as pli_non_traite,
					coalesce(pli_att_saisie, 0) as pli_att_saisie,
					nom_societe
					
					FROM 
					(
						SELECT date_reception::date as date_reception,  count(id_flux) as pli_total  ,flux.societe, nom_societe
						FROM flux
						INNER join societe on societe.id = flux.societe
						WHERE date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 					
						GROUP BY date_reception::date,flux.societe, nom_societe
					
					) as data_total
					LEFT JOIN
					(
						SELECT date_reception::date as date_reception,  count(id_flux) as pli_non_traite ,societe
						FROM flux 
						WHERE date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						and (statut_pli = 0 or statut_pli is null)
						GROUP BY date_reception::date,societe
					
					) as data_non_traite on data_total.date_reception = data_non_traite.date_reception and data_total.societe = data_non_traite.societe
					LEFT JOIN
					(
						SELECT date_reception::date as date_reception ,flux.societe, COUNT(flux.id_flux) as pli_att_saisie
						FROM  flux
						WHERE 
							date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
							and id_lot_saisie_flux is null
							and flux.statut_pli in (1,2,3,4,5,6)
						GROUP BY societe, date_reception::date					
					)as data_att_saisie on data_total.date_reception = data_att_saisie.date_reception and data_total.societe = data_att_saisie.societe
					LEFT JOIN
					(
						SELECT date_reception::date as date_reception ,flux.societe, COUNT(id_flux) as pli_en_prod_j
						FROM  flux
						WHERE 
							date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						GROUP BY societe, date_reception::date
					
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception and data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_reception::date as date_reception,societe, count(id_lot_saisie_flux) as pli_saisie_adv_jmoins
						FROM flux
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						WHERE dt::date < '".$date_j."' ".$where_soc." 
						and id_lot_saisie_flux is not null
						GROUP BY date_reception::date,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_reception = data_ttt_j.date_reception and data_saisie_advantage1.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT
							date_reception ,societe, sum(pli_saisie_j) as pli_saisie_j,sum(pli_saisie_j_plus_1) as pli_saisie_j_plus_1,
							sum(pli_saisie_j_plus_2) as pli_saisie_j_plus_2,sum(pli_saisie_j_plus_plus) as pli_saisie_j_plus_plus
						FROM
						(
							SELECT date_reception::date as date_reception,societe,
							case when ( date_reception::date = dt::date) 
							then count(id_lot_saisie_flux) else 0
							end as pli_saisie_j,
							case when extract(dow FROM (date_reception::date+ INTERVAL '1 day')::date)::integer = 0 
								then 
									case when ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) 
									then count(id_lot_saisie_flux) else 0
									end
								else 
									case when ( (date_reception::date+ INTERVAL '1 day')::date = dt::date) 
									then count(id_lot_saisie_flux) else 0
									end
							end as pli_saisie_j_plus_1,
							case when extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 
								then 
									case when ( (date_reception::date+ INTERVAL '3 day')::date = dt::date) 
									then count(id_lot_saisie_flux) else 0
									end
								else 
									case when ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) 
									then count(id_lot_saisie_flux) else 0
									end
							end as pli_saisie_j_plus_2,

							case when extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 
								then 
									case when ( (date_reception::date+ INTERVAL '3 day')::date < dt::date) 
									then count(id_lot_saisie_flux) else 0
									end
								else 
									case when ( (date_reception::date+ INTERVAL '2 day')::date < dt::date) 
									then count(id_lot_saisie_flux) else 0
									end
							end as pli_saisie_j_plus_plus
							FROM flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							WHERE date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
							and id_lot_saisie_flux is not null
							GROUP BY date_reception::date,societe,dt::date
						) as data_saisie
						GROUP BY date_reception ,societe
					)as data_saisie_jplus on data_saisie_jplus.date_reception = data_ttt_j.date_reception and data_saisie_jplus.societe = data_ttt_j.societe
					
					WHERE pli_en_prod_j is not null
				)as data
				ORDER BY societe,date_reception  ASC				
	  
			";
        return $this->ged_flux->query($sql)
            ->result_array();

    }

    public function get_suivi_mvt_sftp_sla($date_deb, $date_fin, $date_j, $id_societe,$id_src){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and flux.societe = '".$id_societe."'";
        if($id_src != '') $where_soc .= " and flux.id_source = '".$id_src."'";
        $sql = "
			SELECT
					societe, date_reception as date_courrier,mvt_total,
					mvt_saisie_j,mvt_saisie_j_plus_1,mvt_saisie_j_plus_2,mvt_saisie_j_plus_plus,
					mvt_non_traite,mvt_att_saisie,nom_societe
				FROM
				(
	  
					SELECT data_total.societe, data_total.date_reception,mvt_total/*, mvt_en_prod_j*/, 			
					coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins,  
					coalesce(mvt_saisie_j, 0) as mvt_saisie_j,
					coalesce(mvt_saisie_j_plus_1, 0) as mvt_saisie_j_plus_1,
					coalesce(mvt_saisie_j_plus_2, 0) as mvt_saisie_j_plus_2,
					coalesce(mvt_saisie_j_plus_plus, 0) as mvt_saisie_j_plus_plus,
					coalesce(mvt_non_traite, 0) as mvt_non_traite,
					coalesce(mvt_att_saisie, 0) as mvt_att_saisie,
					nom_societe
					FROM 
					(
						SELECT date_reception::date as date_reception, 
						sum(nb_abo) as mvt_total ,flux.societe,nom_societe
						FROM flux
						INNER join societe on societe.id = flux.societe
						LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
						WHERE date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc."
						GROUP BY date_reception::date,flux.societe, nom_societe
					) as data_total
					LEFT JOIN
					(
					SELECT date_reception::date as date_reception, sum(nb_abo) as mvt_non_traite ,societe
					FROM flux
					LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
					WHERE date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc."
					and (statut_pli = 0 or statut_pli is null)
					GROUP BY date_reception::date,societe
					) 
					as data_non_traite on data_total.date_reception = data_non_traite.date_reception 
					and data_total.societe = data_non_traite.societe
					LEFT JOIN
					(
					SELECT date_reception::date as date_reception ,flux.societe, sum(nb_abo) as mvt_att_saisie
					FROM  flux
					LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
					WHERE date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc."
					and id_lot_saisie_flux is null
					and flux.statut_pli in (1,2,3,4,5,6)
					GROUP BY societe, date_reception::date
					)as 
					data_att_saisie on data_total.date_reception = data_att_saisie.date_reception 
					and data_total.societe = data_att_saisie.societe
					LEFT JOIN(
					SELECT date_reception::date as date_reception ,flux.societe, sum(nb_abo) as mvt_en_prod_j
					FROM  flux
					LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
					WHERE 
					date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc."
					GROUP BY societe, date_reception::date
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception 
					and data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(SELECT date_reception::date as date_reception,societe, sum(nb_abo) as mvt_saisie_adv_jmoins
					FROM flux
					LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
					LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
					WHERE dt::date < '".$date_j."' ".$where_soc." and id_lot_saisie_flux is not null
					GROUP BY date_reception::date,societe
					)as data_saisie_advantage1 
					on data_saisie_advantage1.date_reception = data_ttt_j.date_reception 
					and data_saisie_advantage1.societe = data_ttt_j.societe
					LEFT JOIN
					(
					SELECT	date_reception ,societe, sum(mvt_saisie_j) as mvt_saisie_j,
					sum(mvt_saisie_j_plus_1) as mvt_saisie_j_plus_1,
					sum(mvt_saisie_j_plus_2) as mvt_saisie_j_plus_2,
					sum(mvt_saisie_j_plus_plus) as mvt_saisie_j_plus_plus
					FROM
					(
						SELECT date_reception::date as date_reception,societe,case when ( date_reception::date = dt::date) 
						then sum(nb_abo) else 0	end as mvt_saisie_j,
						case when extract(dow FROM (date_reception::date+ INTERVAL '1 day')::date)::integer = 0 
							then case when ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) then sum(nb_abo) else 0 end
							else case when ( (date_reception::date+ INTERVAL '1 day')::date = dt::date) then sum(nb_abo) else 0	end	end as mvt_saisie_j_plus_1,
							case when extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 then 
								case when ( (date_reception::date+ INTERVAL '3 day')::date = dt::date) then sum(nb_abo) else 0
							end	else 
								case when ( (date_reception::date+ INTERVAL '2 day')::date = dt::date) then sum(nb_abo) else 0
							end	end as mvt_saisie_j_plus_2,
							case when extract(dow FROM (date_reception::date+ INTERVAL '2 day')::date) = 0 then 
								case when ( (date_reception::date+ INTERVAL '3 day')::date < dt::date) then sum(nb_abo) else 0
								end	else 
								case when ( (date_reception::date+ INTERVAL '2 day')::date < dt::date) then sum(nb_abo) else 0
							end	end as mvt_saisie_j_plus_plus
						FROM flux
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
						WHERE date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc."  
						and id_lot_saisie_flux is not null
						GROUP BY date_reception::date,societe,dt::date
						) as data_saisie
						GROUP BY date_reception ,societe
					)as data_saisie_jplus on data_saisie_jplus.date_reception = data_ttt_j.date_reception 
					and data_saisie_jplus.societe = data_ttt_j.societe
					WHERE mvt_en_prod_j is not null
					)as data
					ORDER BY societe,date_reception::date  asc";
        /*echo "<pre>";
        print_r($sql);
        echo "</pre>";
        exit;
        */

        return $this->ged_flux->query($sql)->result_array();
    }

    public function get_reception_flux_anomalie_sla($date_j, $id_societe, $id_src){
        if($id_societe != '') $where_soc = " and flux.societe = '".$id_societe."'";
        if($id_src != '') $where_soc .= " and flux.id_source = '".$id_src."'";
        $sql = "SELECT societe,date_reception::date as date_courrier,
				sum(ko_scan) as ko_scan,
				sum(hors_peri) as hors_peri,
				sum(mvt_ko_scan) as mvt_ko_scan,
				sum(mvt_hors_peri) as mvt_hors_peri
				FROM 
				(
					SELECT coalesce(ko_scan,0) as ko_scan, coalesce(hors_peri,0) as hors_peri, 
							coalesce(mvt_ko_scan,0) as mvt_ko_scan, coalesce(mvt_hors_peri,0) as mvt_hors_peri, 
							data_soc.id as societe,
							case when date_reception::date is null then '".$date_j."'::date else date_reception::date end as date_reception
						FROM 
						(
							SELECT id, societe
							FROM societe
						) as data_soc
						LEFT JOIN 
						(
							SELECT societe.id as societe,date_reception::date as date_reception,
								case when flux.statut_pli = ANY (ARRAY[11, 12, 14]) then count(distinct flux.id_flux) else 0 end as ko_scan,
								case when flux.statut_pli = 13 then count(distinct flux.id_flux) else 0 end as hors_peri,
								case when flux.statut_pli = ANY (ARRAY[11, 12, 14])  then sum(nb_abo) else 0 end as mvt_ko_scan,
								case when flux.statut_pli = 13 then sum(nb_abo) else 0 end as mvt_hors_peri
								FROM flux
								LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
								LEFT JOIN societe on societe.id = flux.societe 
								WHERE date_reception::date = '".$date_j."' ".$where_soc."
								and flux.statut_pli in (11,12,13,14)								
							GROUP BY societe.id,date_reception::date,flux.statut_pli
						) as data on data.societe = data_soc.id
				) as res
				GROUP BY  societe,date_reception::date
				ORDER BY  societe,date_reception::date asc
				";
        /*echo "<pre>";
        print_r($sql);
        echo "</pre>";
        exit;
        */

        return $this->ged_flux->query($sql)
            ->result_array();
    }


    public function get_suivi_solde_sftp($date_deb, $date_fin, $date_j, $id_societe,$id_src){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and flux.societe = '".$id_societe."'";
        if($id_src != '') $where_soc .= " and flux.id_source = '".$id_src."'";
        $sql = "	SELECT
					societe,nom_societe, date_reception as date_courrier,pli_total,pli_saisie_adv_jmoins,pli_saisie_adv_j,
					pli_saisie_adv_j_ok,pli_saisie_adv_j_ko
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_reception,pli_total, 			
					coalesce(pli_saisie_adv_jmoins,0) as pli_saisie_adv_jmoins, 
					coalesce(pli_saisie_adv_j,0) as pli_saisie_adv_j,
					coalesce(pli_saisie_adv_j_ok,0) as pli_saisie_adv_j_ok,
					coalesce(pli_saisie_adv_j_ko, 0) as pli_saisie_adv_j_ko
					FROM 
					(
						SELECT date_reception::date as date_reception,  count(id_flux) as pli_total  ,flux.societe, nom_societe
						FROM flux
						INNER join societe on societe.id = flux.societe
						WHERE 
						date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						GROUP BY date_reception::date,flux.societe, nom_societe
					
					) as data_total

					LEFT JOIN
					(
						SELECT date_reception::date as date_reception ,flux.societe, COUNT(id_flux) as pli_en_prod_j
						FROM  flux
						WHERE 
							date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						GROUP BY societe,date_reception::date
					
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception and data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						
						Select adv_total.date_reception,adv_total.societe,
							coalesce(pli_saisie_adv_j,0) pli_saisie_adv_j,
							coalesce(pli_saisie_adv_j_ok,0) pli_saisie_adv_j_ok,
							coalesce(pli_saisie_adv_j_ko,0) pli_saisie_adv_j_ko
						FROM	
						(
							SELECT date_reception::date as date_reception,societe, count(id_lot_saisie_flux) as pli_saisie_adv_j
							FROM flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							WHERE dt::date = '".$date_j."' ".$where_soc." 
							and id_lot_saisie_flux is not null
							GROUP BY date_reception::date,societe
						)as adv_total
						LEFT JOIN
						(
							SELECT date_reception::date as date_reception,societe, count(id_lot_saisie_flux) as pli_saisie_adv_j_ok
							FROM flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							LEFT JOIN abonnement on abonnement.id_flux = flux.id_flux
							LEFT JOIN statut_abonnement on statut_abonnement.id_statut = abonnement.id_statut
							WHERE dt::date = '".$date_j."' ".$where_soc." 
							and id_lot_saisie_flux is not null and abonnement.id_statut in (1)
							GROUP BY date_reception::date,societe
						) as adv_ok on adv_ok.date_reception = adv_total.date_reception and adv_ok.societe = adv_total.societe
						LEFT JOIN(
							SELECT date_reception::date,societe, count(id_lot_saisie_flux) as pli_saisie_adv_j_ko
							FROM flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							LEFT JOIN abonnement on abonnement.id_flux = flux.id_flux
							LEFT JOIN statut_abonnement on statut_abonnement.id_statut = abonnement.id_statut
							WHERE dt::date = '".$date_j."' ".$where_soc."  
							and id_lot_saisie_flux is not null and abonnement.id_statut in (2)
							GROUP BY date_reception::date,societe
						)adv_ko on adv_ko.date_reception = adv_total.date_reception and adv_ko.societe = adv_total.societe
					
					)as data_saisie_advantage on data_saisie_advantage.date_reception = data_ttt_j.date_reception and data_saisie_advantage.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_reception::date,societe, count(id_lot_saisie_flux) as pli_saisie_adv_jmoins
						FROM flux
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						WHERE dt::date < '".$date_j."' ".$where_soc." 
						and id_lot_saisie_flux is not null
						GROUP BY date_reception::date,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_reception = data_ttt_j.date_reception and data_saisie_advantage1.societe = data_ttt_j.societe
					
					WHERE pli_en_prod_j is not null
				)as data
				ORDER BY societe,date_reception  asc
			";

        return $this->ged_flux->query($sql)
            ->result_array();

    }
    public function get_suivi_solde_mvt_sftp($date_deb, $date_fin, $date_j, $id_societe,$id_src){
        $where_soc = "";
        if($id_societe != '') $where_soc = " and flux.societe = '".$id_societe."'";
        if($id_src != '') $where_soc .= " and flux.id_source = '".$id_src."'";
        $sql = "
			SELECT
					societe,nom_societe, date_reception as date_courrier,mvt_total,mvt_saisie_adv_jmoins,mvt_saisie_adv_j,
					mvt_saisie_adv_j_ok,mvt_saisie_adv_j_ko
				FROM
				(
	  
					SELECT data_total.societe,nom_societe, data_total.date_reception,mvt_total, 			
					coalesce(mvt_saisie_adv_jmoins,0) as mvt_saisie_adv_jmoins, 
					coalesce(mvt_saisie_adv_j,0) as mvt_saisie_adv_j,
					coalesce(mvt_saisie_adv_j_ok,0) as mvt_saisie_adv_j_ok,
					coalesce(mvt_saisie_adv_j_ko, 0) as mvt_saisie_adv_j_ko
					FROM 
					(
						SELECT date_reception::date as date_reception,  count(id_flux) as mvt_total  ,flux.societe, nom_societe
						FROM flux
						INNER join societe on societe.id = flux.societe
						WHERE 
						date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						GROUP BY date_reception::date,flux.societe, nom_societe

					) as data_total

					LEFT JOIN
					(
						SELECT date_reception::date as date_reception ,flux.societe, sum(nb_abo) as mvt_en_prod_j
						FROM  flux
						LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
						INNER join societe on societe.id = flux.societe
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						WHERE 
							date_reception::date between '".$date_deb."' and '".$date_fin."' ".$where_soc." 
						GROUP BY flux.societe, date_reception::date
					
					)as data_ttt_j on data_total.date_reception = data_ttt_j.date_reception and data_total.societe = data_ttt_j.societe
					LEFT JOIN
					(
						
						Select adv_total.date_reception,adv_total.societe,
							coalesce(mvt_saisie_adv_j,0) mvt_saisie_adv_j,
							coalesce(mvt_saisie_adv_j_ok,0) mvt_saisie_adv_j_ok,
							coalesce(mvt_saisie_adv_j_ko,0) mvt_saisie_adv_j_ko
						FROM	
						(
							SELECT date_reception::date as date_reception,societe, sum(nb_abo) as mvt_saisie_adv_j
							FROM flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							WHERE dt::date = '".$date_j."' ".$where_soc." 
							and id_lot_saisie_flux is not null
							GROUP BY date_reception::date,societe
						)as adv_total
						LEFT JOIN
						(
							SELECT date_reception::date as date_reception,societe, sum(nb_abo) as mvt_saisie_adv_j_ok
							FROM flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							LEFT JOIN abonnement on abonnement.id_flux = flux.id_flux
							LEFT JOIN statut_abonnement on statut_abonnement.id_statut = abonnement.id_statut
							WHERE dt::date = '".$date_j."' ".$where_soc." 
							and id_lot_saisie_flux is not null and abonnement.id_statut in (1)
							GROUP BY date_reception::date,societe
						) as adv_ok on adv_ok.date_reception = adv_total.date_reception and adv_ok.societe = adv_total.societe
						LEFT JOIN(
							SELECT date_reception::date,societe, sum(nb_abo) as mvt_saisie_adv_j_ko
							FROM flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
							LEFT JOIN abonnement on abonnement.id_flux = flux.id_flux
							LEFT JOIN statut_abonnement on statut_abonnement.id_statut = abonnement.id_statut
							WHERE dt::date = '".$date_j."' ".$where_soc."  
							and id_lot_saisie_flux is not null and abonnement.id_statut in (2)
							GROUP BY date_reception::date,societe
						)adv_ko on adv_ko.date_reception = adv_total.date_reception and adv_ko.societe = adv_total.societe
					
					)as data_saisie_advantage on data_saisie_advantage.date_reception = data_ttt_j.date_reception and data_saisie_advantage.societe = data_ttt_j.societe
					LEFT JOIN
					(
						SELECT date_reception::date,societe, sum(nb_abo) as mvt_saisie_adv_jmoins
						FROM flux
						LEFT JOIN lot_saisie_flux on lot_saisie_flux.id = flux.id_lot_saisie_flux
						LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
						WHERE dt::date < '".$date_j."' ".$where_soc." 
						and id_lot_saisie_flux is not null
						GROUP BY date_reception::date,societe
					
					)as data_saisie_advantage1 on data_saisie_advantage1.date_reception = data_ttt_j.date_reception and data_saisie_advantage1.societe = data_ttt_j.societe
					
					WHERE mvt_en_prod_j is not null
				)as data
				ORDER BY societe,date_reception  asc";

        return $this->ged_flux->query($sql)
            ->result_array();

    }

    public function get_reception_anomalie_sftp($date_j, $id_societe, $id_src){
        if($id_societe != '') $where_soc = " and flux.societe = '".$id_societe."'";
        if($id_src != '') $where_soc .= " and flux.id_source = '".$id_src."'";
        $sql = "SELECT coalesce(anomalie,0) as anomalie, 
					data_soc.id as societe,
					case when date_reception is null then '".$date_j."'::date else date_reception end as date_courrier
				FROM 
				(
					SELECT id, societe
					FROM societe
				) as data_soc
				LEFT JOIN 
				(
					SELECT count(id_flux) as anomalie,flux.societe as societe,date_reception::date as date_reception
						FROM flux
						LEFT JOIN societe on societe.id = flux.societe 
						WHERE date_reception::date = '".$date_j."' ".$where_soc."
						and flux.statut_pli in (11,12,13,14)						
					GROUP BY flux.societe,date_reception::date
				) as data on data.societe = data_soc.id
				
				";


        return $this->ged_flux->query($sql)
            ->result_array();
    }

    public function get_reception_anomalie_sftp_abo($date_j, $id_societe, $id_src){
        if($id_societe != '') $where_soc = " and flux.societe = '".$id_societe."'";
        if($id_src != '') $where_soc .= " and flux.id_source = '".$id_src."'";
        $sql = "SELECT coalesce(anomalie,0) as anomalie, 
				data_soc.id as societe,
						case when date_reception is null then '".$date_j."'::date else date_reception end as date_courrier
					FROM 
					(
						SELECT id, societe
						FROM societe
					) as data_soc
					LEFT JOIN 
					(
						SELECT sum(nb_abo) as anomalie,societe.id as societe,date_reception::date as date_reception
							FROM flux
							LEFT JOIN  view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
							LEFT JOIN societe on societe.id = flux.societe
							WHERE  date_reception::date = '".$date_j."' ".$where_soc."
							and flux.statut_pli in (11,12,13,14)
						GROUP BY societe.id,date_reception::date
					) as data on data.societe = data_soc.id
						";
        return $this->ged_flux->query($sql)
            ->result_array();
    }

	//requette traitement>traitement des plis saisie
    public function get_saisie_par_date_traitement($date_debut, $date_fin, $id_societe, $source){
	
        if($source == '3'){
           $sql = "SELECT data_global.dt_event,data_global.societe,pli_saisie_ok,pli_saisie_ci,pli_saisie_ko,
                        coalesce(ko_scan,0) as ko_scan,
                        coalesce(ko_inconnu,0) as ko_inconnu,
                        coalesce(ko_def,0) as ko_def,
                        coalesce(ko_ke,0) as ko_ke,
                        coalesce(ko_src,0) as ko_src,
                        coalesce(ko_circulaire,0) as ko_circulaire,
                        coalesce(ko_ci_editee,0) as ko_ci_editee,
                        coalesce(ko_ci_envoyee,0) as ko_ci_envoyee,
                        coalesce(ko_abandonne,0) as ko_abandonne,
                        coalesce(divise,0) as divise,
						coalesce(ko_scan,0)+coalesce(ko_inconnu,0)+coalesce(ko_def,0)+coalesce(ko_ke,0)+coalesce(ko_src,0)+coalesce(ko_circulaire,0)  as anomalie,
						coalesce(ko_ci_editee,0)+coalesce(ko_ci_envoyee,0) as ci,
						coalesce(ok_circulaire,0)+coalesce(ok_circulaire,0) as ok_circulaire,
						coalesce(ko_bayard,0)+coalesce(ko_bayard,0) as ko_bayard
                        from 
                        (
                            select 
									distinct dt_event::date as dt_event,case when societe is not null then (data_pli.societe)  end societe
								from
								(
								select 
									histo_pli.dt_event::date,
									histo_pli.id_pli,id
									
								from histo_pli 
								where histo_pli.dt_event::date between '".$date_debut."' and '".$date_fin."'
								)histo_pli 
								inner join 
								(
								select * from f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') 
								)as res1 on res1.id = histo_pli.id 
								
								
								left join
								(
								select 
									id_pli,
									data_pli.societe
								from data_pli
									where societe  = '".$id_societe."'
								)as data_pli on data_pli.id_pli = histo_pli.id_pli
								
								group by dt_event,societe
                        ) as data_global
                        LEFT JOIN
                        (
                            SELECT dt_event, societe,
                            sum(pli_saisie_ok) as pli_saisie_ok,
                            sum(pli_saisie_ci) as pli_saisie_ci,
                            sum(pli_saisie_ko) as pli_saisie_ko
                            from 
                            (
                                SELECT 
                                    id_pli,dt_event, societe,                                    
									case when flag_saisie = 1 and (( statut_saisie = 1 and date_saisie_advantage  between '".$date_debut."' and '".$date_fin."') or  (statut_saisie = 1 and dt_batch  between '".$date_debut."' and '".$date_fin."')) then 1 else 0 end as pli_saisie_ok,
                                    case when flag_saisie = 1 and ((statut_saisie in (7,9,10) and date_saisie_advantage  between '".$date_debut."' and '".$date_fin."') or  (statut_saisie in (7,9,10) and dt_batch  between '".$date_debut."' and '".$date_fin."')) then 1 else 0 end as pli_saisie_ci,
                                    case when flag_saisie = 1 and ((statut_saisie in (2,3,4,5,6,8,24) and date_saisie_advantage  between '".$date_debut."' and '".$date_fin."') or  (statut_saisie in (2,3,4,5,6,8,24) and dt_batch  between '".$date_debut."' and '".$date_fin."')) then 1 else 0 end as pli_saisie_ko
								
                                from
                                (
											select 
												distinct histo_pli.dt_event::date ,
												data_pli.societe, 
												id_lot_saisie,
												data_pli.dt_enregistrement::date as date_saisie_advantage,
												data_pli.statut_saisie as statut_saisie,
												histo_pli.id_pli,
												histo_pli.flag_traitement,
												flag_saisie,
												data_pli.dt_batch


											from 
											        (
														select * from f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') 
													)as res1
													left JOIN
													(
														select 
															histo_pli.dt_event::date,
															histo_pli.id_pli,
															histo_pli.flag_traitement,
															id
														from histo_pli 
														where histo_pli.dt_event::date between '".$date_debut."' and '".$date_fin."'
													)histo_pli on res1.id_pli = histo_pli.id_pli  and res1.id  = histo_pli.id 
													inner join
													(
														select 
															id_pli, id_lot_saisie
															from f_pli
														where societe = '".$id_societe."'
													)as f_pli on f_pli.id_pli = histo_pli.id_pli
													left join
													(
														select 
															id_pli,
															dt_enregistrement,
															data_pli.societe,
															dt_batch,
															flag_saisie,
															data_pli.statut_saisie as statut_saisie
														from data_pli
															where societe = '".$id_societe."'
											        )as data_pli on data_pli.id_pli = f_pli.id_pli
                                ) as tab
                                GROUP BY id_pli,dt_event::date,date_saisie_advantage,id_lot_saisie,statut_saisie,societe,flag_traitement,flag_saisie,dt_batch
                            ) as ok
                            GROUP BY dt_event::date,societe
                        ) as data_saisie on data_saisie.dt_event = data_global.dt_event and data_saisie.societe = data_global.societe
                        LEFT JOIN
                        (
                        SELECT
                            societe, dt_event, sum(ko_scan) as ko_scan ,sum(ko_def) as ko_def,sum(ko_inconnu) as ko_inconnu,sum(ko_abandonne) as ko_abandonne,sum(divise) as divise,
							sum(ko_src) as ko_src,sum(ko_ke) as ko_ke,sum(ko_circulaire) as ko_circulaire,sum(ko_ci_envoyee) as ko_ci_envoyee,
							sum(ko_ci_editee) as ko_ci_editee,
							sum(ok_circulaire) as ok_circulaire,
							sum(ko_bayard) as ko_bayard
                            FROM
                            (
                                SELECT data_pli.societe as societe,histo_pli.dt_event::date as dt_event,
                                case when data_pli.statut_saisie = 2 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_scan,
                                case when data_pli.statut_saisie = 3 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_def,
                                case when data_pli.statut_saisie = 4 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_inconnu,
                                case when data_pli.statut_saisie = 5 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_src,
                                case when data_pli.statut_saisie = 6 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_ke,
                                case when data_pli.statut_saisie = 7 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_circulaire,
                                case when data_pli.statut_saisie = 9 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_ci_envoyee,
                                case when data_pli.statut_saisie = 10 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_ci_editee,
                                case when data_pli.statut_saisie = 8 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_abandonne,
                                case when data_pli.statut_saisie = 24 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as divise,
								case when data_pli.statut_saisie = 11 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ok_circulaire,
                                case when data_pli.statut_saisie = 12 then coalesce(count(distinct histo_pli.id_pli),0) else 0 end as ko_bayard
                                    FROM f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') 
                                    INNER JOIN histo_pli ON f_histo_pli_by_date.id  = histo_pli.id and f_histo_pli_by_date.id_pli  = histo_pli.id_pli
                                    --INNER JOIN f_pli  ON f_histo_pli_by_date.id_pli  = f_pli.id_pli                                   
                                    LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli
                                    --INNER JOIN view_statut_pli ON data_pli.id_pli  = view_statut_pli.id_pli
                                    WHERE histo_pli.dt_event::date between  '".$date_debut."' and '".$date_fin."' and data_pli.societe = '".$id_societe."'
                                    and data_pli.statut_saisie not in (1) 							
                                GROUP BY data_pli.societe,histo_pli.dt_event::date,histo_pli.flag_traitement,data_pli.statut_saisie
                            ) as res GROUP BY societe,dt_event
                            
                        ) as data_anomalie
                        on data_global.dt_event = data_anomalie.dt_event and data_global.societe = data_anomalie.societe
						where data_anomalie.societe is not null
                        ORDER BY data_global.dt_event asc";
            //  echo "<pre>";
            //  print_r($sql);
            //  echo "</pre>";exit;
            return $this->ged->query($sql)
                ->result_array();
        }
        else{
            $sql = "SELECT
                dt_event,
                sum(saisie_finie) as saisie_finie,
				sum(flux_ok) as flux_ok,
                sum(hors_perimetre) as hors_perimetre,
                sum(anomalie) as anomalie,
                sum(escalade) as escalade,
                sum(transfert_consigne) as transfert_consigne,
				sum(ko_def) as ko_def,
				sum(ko_inconnu) as ko_inconnu,
				sum(ko_ettent) as ko_ettent,
				sum(ko_bayard) as ko_bayard
                FROM
                (              
                                SELECT id_flux, dt_event,
                                               case when statut_pli = 7 and id_lot_saisie_flux is not null and date_saisie = dt_event then 1 else 0 end as saisie_finie,
											   case when statut_pli = ANY (ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) and etat_pli_id = 1 then 1 else 0 end as flux_ok,
                                              /* case when statut_pli = 13 then 1 else 0 end as hors_perimetre,
                                               case when statut_pli = ANY (ARRAY[11, 12, 14, 15]) then 1 else 0 end as anomalie,*/
											   case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 13  then 1 else 0 end as hors_perimetre,
                                               case when statut_pli =  ANY (ARRAY[2, 7]) and etat_pli_id = ANY (ARRAY[11, 12, 14, 15]) then 1 else 0 end as anomalie,
                                               case when statut_pli = ANY (ARRAY[4, 5]) then 1 else 0 end as escalade,
                                               case when statut_pli = ANY (ARRAY[8, 9, 10]) then 1 else 0 end as transfert_consigne,
											   case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 21 then 1 else 0 end as ko_def,
												case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 22 then 1 else 0 end as ko_inconnu,
												case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 23 then 1 else 0 end as ko_ettent,
												case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 24 then 1 else 0 end as ko_bayard
                                FROM
                                (
                                SELECT  traitement.id_flux, flag_anomalie_pj, flag_hors_perimetre, 
                                       flag_traitement_niveau, flux.statut_pli, --view_traitement_oid.statut_pli, 
                                       flux.typologie,id_lot_saisie_flux,
                                       view_traitement_oid.date_traitement::date as dt_event,
                                       view_lot_saisie_flux.dt::date as date_saisie,
                                       nb_abo,/*etat_flux_h as*/ etat_pli_id,id_traitement
                                  FROM public.flux
                                  inner join traitement on flux.id_flux = traitement.id_flux and id_traitement = (select id_traitement from traitement where id_flux = flux.id_flux order by id_traitement desc limit 1)
                                  inner join view_traitement_oid on traitement.oid = view_traitement_oid.oid
                                  left join view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
                                  left join view_lot_saisie_flux on view_lot_saisie_flux.id = flux.id_lot_saisie_flux
                                  where traitement.date_traitement::date between '".$date_debut."' and '".$date_fin."'  
                                  and societe = '".$id_societe."' 
                                  and flux.id_source = ".$source." 
                                  ) as data_flux
                                  GROUP BY id_flux,statut_pli,id_lot_saisie_flux,date_saisie,dt_event,nb_abo,etat_pli_id
                  ) as data
                  GROUP BY dt_event
                  ";

            return $this->ged_flux->query($sql)
                ->result_array();
        }

    }

	//traitement des mouvements - saisie
    public function get_saisie_mvt_par_date_traitement($date_debut, $date_fin, $id_societe, $source){

        if($source == '3'){
            $sql = "SELECT data_global.dt_event,data_global.societe,mvt_saisie_ok,mvt_saisie_ci,mvt_saisie_ko,
                        coalesce(ko_scan,0) as mvt_ko_scan,
                        coalesce(ko_def,0) as mvt_ko_def,
						coalesce(ko_ke,0) as mvt_ko_ke,
                        coalesce(ko_src,0) as mvt_ko_src,
                        coalesce(ko_circulaire,0) as mvt_ko_circulaire,
                        coalesce(ko_ci_editee,0) as mvt_ko_ci_editee,
                        coalesce(ko_ci_envoyee,0) as mvt_ko_ci_envoyee,
                        coalesce(ko_abandonne,0) as mvt_ko_abandonne,
                        coalesce(divise,0) as mvt_divise,
						coalesce(ko_inconnu,0) as mvt_ko_inconnu,
						coalesce(ko_scan,0)+coalesce(ko_inconnu,0)+coalesce(ko_def,0)+coalesce(ko_ke,0)+coalesce(ko_src,0)+coalesce(ko_circulaire,0)  as mvt_anomalie,
						coalesce(ko_ci_editee,0)+coalesce(ko_ci_envoyee,0) as mvt_ci,
						coalesce(ok_circ,0) as mvt_ok_circ,
                        coalesce(ko_bayard,0) as mvt_ko_bayard
                        from 
                        (
                            select 
									distinct dt_event::date as dt_event,case when societe is not null then (data_pli.societe)  end societe
								from
								(
								select 
									histo_pli.dt_event::date,
									histo_pli.id_pli,id
									
								from histo_pli 
								where histo_pli.dt_event::date between '".$date_debut."' and '".$date_fin."'
								)histo_pli 
								inner join 
								(
								select * from f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') 
								)as res1 on res1.id = histo_pli.id 
								
								
								left join
								(
								select 
									id_pli,
									data_pli.societe
								from data_pli
									where societe  = '".$id_societe."'
								)as data_pli on data_pli.id_pli = histo_pli.id_pli
								
								group by dt_event,societe
                        ) as data_global
                        LEFT JOIN
                        (
                            SELECT dt_event, societe,
                            sum(mvt_saisie_ok) as mvt_saisie_ok,
                            sum(mvt_saisie_ci) as mvt_saisie_ci,
                            sum(mvt_saisie_ko) as mvt_saisie_ko
                            from 
                            (
                                SELECT 
                                    id_pli,dt_event, societe,
                                    case when flag_saisie = 1 and (( statut_saisie = 1 and date_saisie_advantage  between '".$date_debut."' and '".$date_fin."') or  (statut_saisie = 1 and dt_batch  between '".$date_debut."' and '".$date_fin."')) then sum(nb_mvt) else 0 end as mvt_saisie_ok,
                                    case when flag_saisie = 1 and ((statut_saisie in (7,9,10) and date_saisie_advantage  between '".$date_debut."' and '".$date_fin."') or  (statut_saisie in (7,9,10) and dt_batch  between '".$date_debut."' and '".$date_fin."')) then sum(nb_mvt) else 0 end as mvt_saisie_ci,
                                    case when flag_saisie = 1 and ((statut_saisie in (2,3,4,5,6,8,24) and date_saisie_advantage  between '".$date_debut."' and '".$date_fin."') or  (statut_saisie in (2,3,4,5,6,8,24) and dt_batch  between '".$date_debut."' and '".$date_fin."')) then sum(nb_mvt) else 0 end as mvt_saisie_ko
                                from
                                (
                                   	select 
												distinct histo_pli.dt_event::date ,
												data_pli.societe, 
												id_lot_saisie,
												data_pli.dt_enregistrement::date as date_saisie_advantage,
												data_pli.statut_saisie as statut_saisie,
												histo_pli.id_pli,
												histo_pli.flag_traitement,
												flag_saisie,
												data_pli.dt_batch,
												mvt_nbr as nb_mvt
											from 
											        (
														select * from f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') 
													)as res1
													left JOIN
													(
														select 
															histo_pli.dt_event::date,
															histo_pli.id_pli,
															histo_pli.flag_traitement,
															id
														from histo_pli 
														where histo_pli.dt_event::date between '".$date_debut."' and '".$date_fin."'
													)histo_pli on res1.id_pli = histo_pli.id_pli  and res1.id  = histo_pli.id 
													inner join
													(
														select 
															id_pli, id_lot_saisie
															from f_pli
														where societe = '".$id_societe."'
													)as f_pli on f_pli.id_pli = histo_pli.id_pli
													left join
													(
														select 
															id_pli,
															dt_enregistrement,
															data_pli.societe,
															dt_batch,
															flag_saisie,
															data_pli.statut_saisie as statut_saisie,
															mvt_nbr
														from data_pli
															where societe = '".$id_societe."'
											        )as data_pli on data_pli.id_pli = f_pli.id_pli
									
                                ) as tab
                                GROUP BY id_pli,dt_event::date,date_saisie_advantage,id_lot_saisie,statut_saisie,societe,flag_traitement,flag_saisie, dt_batch
                            ) as ok
                            GROUP BY dt_event::date,societe
                        ) as data_saisie on data_saisie.dt_event = data_global.dt_event and data_saisie.societe = data_global.societe
                        LEFT JOIN
                        (
                        SELECT
                            societe, dt_event, sum(ko_scan) as ko_scan ,sum(ko_def) as ko_def,
							sum(ko_inconnu) as ko_inconnu,sum(ko_abandonne) as ko_abandonne,sum(divise) as divise,
							sum(ko_src) as ko_src,sum(ko_ke) as ko_ke,sum(ko_circulaire) as ko_circulaire,
							sum(ko_ci_envoyee) as ko_ci_envoyee,sum(ko_ci_editee) as ko_ci_editee,
                            sum(ok_circ) as ok_circ,sum(ko_bayard) as ko_bayard
                            FROM
                            (
                                 SELECT data_pli.societe as societe,histo_pli.dt_event::date as dt_event,
                                case when data_pli.statut_saisie = 2 then coalesce(sum(mvt_nbr),0) else 0 end as ko_scan,
                                case when data_pli.statut_saisie = 3 then coalesce(sum(mvt_nbr),0) else 0 end as ko_def,
                                case when data_pli.statut_saisie = 4 then coalesce(sum(mvt_nbr),0) else 0 end as ko_inconnu,
                                case when data_pli.statut_saisie = 5 then coalesce(sum(mvt_nbr),0) else 0 end as ko_src,
                                case when data_pli.statut_saisie = 6 then coalesce(sum(mvt_nbr),0) else 0 end as ko_ke,
                                case when data_pli.statut_saisie = 7 then coalesce(sum(mvt_nbr),0) else 0 end as ko_circulaire,
                                case when data_pli.statut_saisie = 9 then coalesce(sum(mvt_nbr),0) else 0 end as ko_ci_envoyee,
                                case when data_pli.statut_saisie = 10 then coalesce(sum(mvt_nbr),0) else 0 end as ko_ci_editee,
                                case when data_pli.statut_saisie = 8 then coalesce(sum(mvt_nbr),0) else 0 end as ko_abandonne,
                                case when data_pli.statut_saisie = 24 then coalesce(sum(mvt_nbr),0) else 0 end as divise,
                                case when data_pli.statut_saisie = 11 then coalesce(sum(mvt_nbr),0) else 0 end as ok_circ,
								case when data_pli.statut_saisie = 12 then coalesce(sum(mvt_nbr),0) else 0 end as ko_bayard
                                    FROM f_histo_pli_by_date('''".$date_debut."''' , '''".$date_fin."''') 
                                    INNER JOIN histo_pli ON f_histo_pli_by_date.id  = histo_pli.id and f_histo_pli_by_date.id_pli  = histo_pli.id_pli
                                    --INNER JOIN f_pli  ON f_histo_pli_by_date.id_pli  = f_pli.id_pli                                   
                                    LEFT JOIN data_pli on histo_pli.id_pli = data_pli.id_pli
                                   -- INNER JOIN view_statut_pli ON data_pli.id_pli  = view_statut_pli.id_pli
                                    WHERE histo_pli.dt_event::date between  '".$date_debut."' and '".$date_fin."' and data_pli.societe = '".$id_societe."'
                                    and data_pli.statut_saisie not in (1) 							
                                GROUP BY data_pli.societe,histo_pli.dt_event::date,histo_pli.flag_traitement,data_pli.statut_saisie
                            ) as res GROUP BY societe,dt_event
                            
							
                        ) as data_anomalie
                        on data_global.dt_event = data_anomalie.dt_event and data_global.societe = data_anomalie.societe
						where data_anomalie.societe is not null
                        ORDER BY data_global.dt_event asc";
            //  echo "<pre>";
            //  print_r($sql);
            //  echo "</pre>";exit;
            
            return $this->ged->query($sql)
                ->result_array();
        }
        else{
            $sql = "SELECT
                dt_event,
                sum(saisie_finie) as abo_saisie_finie,
				sum(abo_flux_ok) as abo_flux_ok,
                sum(hors_perimetre) as abo_hors_perimetre,
                sum(anomalie) as abo_anomalie,
                sum(escalade) as abo_escalade,
                sum(transfert_consigne) as abo_transfert_consigne,
				sum(ko_def) as ko_def,
				sum(ko_inconnu) as ko_inconnu,
				sum(ko_ettent) as ko_ettent,
				sum(ko_bayard) as ko_bayard

                FROM
                (              
                                SELECT id_flux, dt_event,
                                               case when statut_pli = 7 and id_lot_saisie_flux is not null and date_saisie = dt_event then nb_abo else 0 end as saisie_finie,
                                               /*case when statut_pli = 13 then 1 else 0 end as hors_perimetre,
                                               case when statut_pli = ANY (ARRAY[11, 12, 14, 15]) then nb_abo else 0 end as anomalie,*/
											   case when statut_pli = ANY (ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) and etat_pli_id = 1 then nb_abo else 0 end as abo_flux_ok,
											   case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 13  then nb_abo else 0 end as hors_perimetre,
											   case when statut_pli =  ANY (ARRAY[2, 7]) and etat_pli_id = ANY (ARRAY[11, 12, 14, 15]) then nb_abo else 0 end as anomalie,
                                               case when statut_pli = ANY (ARRAY[4, 5]) then nb_abo else 0 end as escalade,
                                               case when statut_pli = ANY (ARRAY[8, 9, 10]) then nb_abo else 0 end as transfert_consigne,
												case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 21 then nb_abo else 0 end as ko_def,
												case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 22 then nb_abo else 0 end as ko_inconnu,
												case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 23 then nb_abo else 0 end as ko_ettent,
												case when statut_pli = ANY (ARRAY[2, 7]) and etat_pli_id = 24 then nb_abo else 0 end as ko_bayard
                                FROM
                                (
                                SELECT  traitement.id_flux, flag_anomalie_pj, flag_hors_perimetre, 
                                       flag_traitement_niveau, flux.statut_pli, --view_traitement_oid.statut_pli,
                                       flux.typologie,id_lot_saisie_flux,
                                       view_traitement_oid.date_traitement::date as dt_event,
                                       view_lot_saisie_flux.dt::date as date_saisie,
                                       case when nb_abo is not null then nb_abo else 0 end nb_abo,/*etat_flux_h as*/ etat_pli_id,id_traitement
                                  FROM public.flux
                                  inner join traitement on flux.id_flux = traitement.id_flux and id_traitement = (select id_traitement from traitement where id_flux = flux.id_flux order by id_traitement desc limit 1)
                                  inner join view_traitement_oid on traitement.oid = view_traitement_oid.oid
                                  left join view_nb_abonnement on view_nb_abonnement.id_flux = flux.id_flux
                                  left join view_lot_saisie_flux on view_lot_saisie_flux.id = flux.id_lot_saisie_flux
                                  where traitement.date_traitement::date between '".$date_debut."' and '".$date_fin."' 
                                  and societe = '".$id_societe."' 
                                  and flux.id_source = ".$source." 
                                  ) as data_flux
                                  GROUP BY id_flux,statut_pli,id_lot_saisie_flux,date_saisie,dt_event,nb_abo,etat_pli_id
                  ) as data
                  GROUP BY dt_event
            ";
            return $this->ged_flux->query($sql)
                ->result_array();
        }

    }


    public function get_pli_courrier_att_saisie($id_societe){

        $sql="SELECT date_courrier, societe,
			sum(pli_a_typer) as pli_a_typer, 
			sum(pli_att_saisie) as pli_att_saisie 
			FROM 
			(
			SELECT date_courrier::date as date_courrier ,view_pli_stat.societe,
			CASE WHEN view_pli_stat.flag_traitement=0 or view_pli_stat.flag_traitement is null THEN COUNT(view_pli_stat.id_pli)  ELSE 0 END as pli_a_typer,
			CASE WHEN view_pli_stat.flag_traitement = ANY (ARRAY[1,2,3,4]) and id_lot_saisie is null THEN COUNT(view_pli_stat.id_pli) ELSE 0 END as pli_att_saisie
			FROM  view_pli_stat
			LEFT JOIN  view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = view_pli_stat.id_pli
			WHERE 
				societe = '".$id_societe."' 
				and (view_pli_stat.flag_traitement in (0,1,2,3,4) or flag_traitement is null)
			GROUP BY societe,nom_societe, date_courrier, view_pli_stat.flag_traitement,id_lot_saisie
			having count(view_pli_stat.id_pli)>0
			) as res
			GROUP BY date_courrier,societe
			ORDER BY date_courrier,societe ASC";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_pli_sftp_att_saisie($id_societe, $id_source){

        $sql="SELECT date_reception,societe, 
		sum(pli_a_typer) as pli_a_typer, 
		sum(pli_att_saisie) as pli_att_saisie
		FROM 
		(
		SELECT date_reception::date as date_reception ,flux.societe, 
			case when flux.statut_pli = 0 or flux.statut_pli  is null then COUNT(flux.id_flux) else 0 end as pli_a_typer ,
			case when flux.statut_pli > 0 and id_lot_saisie_flux is null then COUNT(flux.id_flux) else 0 end as pli_att_saisie
			FROM  flux
			WHERE 
				flux.societe = '".$id_societe."' and flux.id_source = '".$id_source."' 
				and id_lot_saisie_flux is null
				and flux.statut_pli in (0,1,2,3,4,5,6)
			GROUP BY societe, date_reception::date,flux.statut_pli,id_lot_saisie_flux
			having count(flux.id_flux)>0
			) as res
			group by societe, date_reception
			ORDER BY societe, date_reception::date ASC
			";
        return $this->ged_flux->query($sql)
            ->result_array();
    }

    //exporter excell
    public function exporterExcel($soc)
    {
        //var_dump($soc);die;
        $sql = "select id_pli, flag_traitement as flag,typologie.typologie,f_flag_traitement.traitement as flag_traitement from f_pli 
		left join typologie on typologie.id = f_pli.flag_traitement
		left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
		where flag_traitement = 0 and societe= ".$soc;
        return $this->ged->query($sql)->result();
    }

    public function exporterExcel2($soc)
    {
        $sql = "select id_pli, flag_traitement as flag,typologie.typologie,f_flag_traitement.traitement as flag_traitement from f_pli 
		left join typologie on typologie.id = f_pli.flag_traitement
		left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
		where flag_traitement = ANY (ARRAY[1,2,3,4]) and societe =".$soc;
        return $this->ged->query($sql)->result();
    }

    //sftp
    public function sftp($soc, $source, $where)
    {
        // if($clause) {
        // 	$where .= " and typologie.typologie is not null";
        // }
        $sql = "
		select 
                id_flux as id_pli,
              
                typologie.typologie as typologie,
                statut_pli.statut as flag_traitement
               
            from flux 
                left join source on flux.id_source = source.id_source
                left join typologie on typologie.id_typologie = flux.typologie
                left join statut_pli on statut_pli.id_statut = flux.statut_pli
      where flux.societe = ".$soc." and flux.id_source = ".$source." ".$where;

        return $this->ged_flux->query($sql)->result();
    }

}
