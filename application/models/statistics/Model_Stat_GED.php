<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Stat_GED extends CI_Model{

    private $CI;

    private $ged;
    private $base_64; 
    private $ged_flux; 
    private $ged_ce;
	

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->ged_ce = $this->load->database('ged_ce', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->ged_flux = $this->load->database('ged_flux', TRUE);
    }

    public function dataCourrier($societe,$where,$whereCourrier,$conn)
    {
       $sqlCourrierMada = "
       select ttmt.id_flag_traitement,ttmt.traitement,
       case when id_pli is null then 0 else id_pli end as id_pli,
       case when nb_mouvement is null then 0 else nb_mouvement end as nb_mouvement
       from f_flag_traitement ttmt
       left join (
            SELECT case when id_flag_traitement is null then 0 else id_flag_traitement end as id_flag_traitement,sum(nb_mouvement) nb_mouvement, count(id_pli) id_pli
                FROM (
                    SELECT case when id_flag_traitement is null then 0 else id_flag_traitement end as id_flag_traitement,nb_mouvement nb_mouvement,  id_pli
                FROM public.view_pli_stat 
                where 
                date_courrier::date between  '".$this->session->userdata('dateDebut')."' and '".$this->session->userdata('datefin')."'  
                and societe in (".$societe.") ".$where."  ".$whereCourrier."   
            ) nombre  group by id_flag_traitement
         ) as data on  ttmt.id_flag_traitement = data.id_flag_traitement order by ttmt.id_flag_traitement"; 
        
        
        return $this->$conn->query($sqlCourrierMada)->result();
    }
    
    public function dataFlux($societe,$where,$location,$type)
    {
        $sqlMail = "
            select id_statut,statut,
            case when id_flux is null then 0 else id_flux end as id_flux,
                    case when nb_abonnement is null then 0 else nb_abonnement end as nb_abonnement
                    from statut_pli 
                    left join (
                        SELECT count(id_flux) id_flux, statut_pli,sum(nb_abonnement) nb_abonnement
                    FROM public.flux 
                    where 
                    date_reception::date between  '".$this->session->userdata('dateDebut')."' and '".$this->session->userdata('datefin')."'  
                    and id_source = ".intval($type)." and societe in (".$societe.") ".$where."  ".$location." 
                    GROUP BY statut_pli
                    ) flux on flux.statut_pli = statut_pli.id_statut order by statut_pli.id_statut";
        // echo "<pre>".$sqlMail."</pre>";     
        // $this->fluxID($societe,$where,$location,$type);
        return $this->ged_flux->query($sqlMail)->result();
    }

    public function courrierID($societe,$where,$whereCourrier,$conn)
    {
        $sqlCourrierMada = "
        select 
        id_pli,
        case when ttmt.id_flag_traitement = 0 then 'Stock Typage'
        when ttmt.id_flag_traitement = 1 then 'En cours de typage'
        when ttmt.id_flag_traitement in(16,21) then 'Anomalie a traiter'
        when ttmt.id_flag_traitement in(3) then 'Stock Saisie'
        when ttmt.id_flag_traitement in(4) then 'Saisie en cours'			
        when ttmt.id_flag_traitement in(5) then 'Saisie OK'	
        when ttmt.id_flag_traitement in(17) then 'Saisie KO'
        when ttmt.id_flag_traitement in(23) then 'Saisie CI'
        when ttmt.id_flag_traitement in(26) then 'Reb'
        when ttmt.id_flag_traitement in(24) then 'Fermé'
        when ttmt.id_flag_traitement in(6,7) then 'Stock Ctrl'
        when ttmt.id_flag_traitement in(8) then 'Contrôle en cours'
        when ttmt.id_flag_traitement in(10) then 'KO et CI en attente validation'
        when ttmt.id_flag_traitement in(12) then 'KO et CI en cours de validation'
        when ttmt.id_flag_traitement in(15) then 'KO et CI correction OK'
        when ttmt.id_flag_traitement in(19) then 'KE (att cons)'
        when ttmt.id_flag_traitement in(11) then 'KE en attente de correction'
        when ttmt.id_flag_traitement in(13) then 'KE en cours de retraiteM'
        when ttmt.id_flag_traitement in(14) then 'KE correction OK'
        when ttmt.id_flag_traitement in(20) then 'KD définitif'
        when ttmt.id_flag_traitement in(18) then 'KS SRC'
        when ttmt.id_flag_traitement in(9) then 'Ctrl OK'
        when ttmt.id_flag_traitement in(25) then 'Ctrl CI' end as comment
           from f_flag_traitement ttmt
           left join (
                SELECT case when id_flag_traitement is null then 0 else id_flag_traitement end as id_flag_traitement, id_pli
                    FROM (
                        SELECT case when id_flag_traitement is null then 0 else id_flag_traitement end as id_flag_traitement,nb_mouvement nb_mouvement,  id_pli
                    FROM public.view_pli_stat 
                    where 
                    date_courrier::date between  '".$this->session->userdata('dateDebut')."' and '".$this->session->userdata('datefin')."'  
                and societe in (".$societe.") ".$where."  ".$whereCourrier."  
                ) nombre  
             ) as data on  ttmt.id_flag_traitement = data.id_flag_traitement where id_pli is not null order by ttmt.id_flag_traitement";
             
        return $this->$conn->query($sqlCourrierMada)->result();
    }

    public function fluxID($societe,$where,$location,$type)
    {
        $sqlMail = "
        select 	   
            id_flux id_pli,
            case when id_statut in (0) then 'Stock Typage'
            when id_statut in (1) then 'En cours de typage'
            when id_statut in (11) then 'Anomalie à traiter'
            when id_statut in (12,13,14,15) then 'Anomalie Def'
            when id_statut in (2,4) then 'Stock Saisie'
            when id_statut in (3,5,6) then 'Saisie en cours'
            when id_statut in (7) then 'Saisie OK'
            when id_statut in (7) then 'Saisie OK'
            when id_statut in (8) then 'ESCALADE'
            when id_statut in (10) then 'KE en attente de correction'
            when id_statut in (14) then 'KE correction OK'
            when id_statut in (20) then 'KD définitif'
            when id_statut in (19) then 'KS SRC' 
            else 'Autre'
            end as comment
        from statut_pli 
            left join (
            SELECT id_flux, statut_pli,id_source
            FROM public.flux 
            where 
            date_reception::date between  '".$this->session->userdata('dateDebut')."'  and '".$this->session->userdata('datefin')."'  
            and id_source = ".intval($type)." 
            and societe in (".$societe.") ".$where."  ".$location." 
            ) flux on flux.statut_pli = statut_pli.id_statut 
            where id_flux is not null order by statut_pli.id_statut ";
        
            // $this->debug($sqlMail);    
        return $this->ged_flux->query($sqlMail)->result();
    }

    function debug($sql)
    {
        echo "<pre>".print_r($sql)."</pre>";
    }

    public function GetStatCourrier($conn)
    {
        $sql = "
            SELECT
            sum( pli_) as pli_total,
            sum( mvt_nbr) as mvt_total,
            sum( stock_typage) as stock_typage,
            sum( typage_en_cours) as typage_en_cours,
            sum( typage_ok_typage) as typage_ok_typage,
            sum( typage_ok_typage_mvt) as typage_ok_typage_mvt,
            sum( stock_attente_saisie) as stock_attente_saisie,
            sum( stock_attente_saisie_mvt) as stock_attente_saisie_mvt,
            sum( en_cours_saisie) as en_cours_saisie,
            sum( en_cours_saisie_mvt) as en_cours_saisie_mvt,
            sum( saisie_ok) as saisie_ok,
            sum( saisie_ok_mvt) as saisie_ok_mvt,
            sum( stock_ctrl) as stock_ctrl,
            sum( stock_ctrl_mvt) as stock_ctrl_mvt,
            sum( en_cours_ctrl) as en_cours_ctrl,
            sum( en_cours_ctrl_mvt) as en_cours_ctrl_mvt,
            sum( clo_apres_ctrl) as clo_apres_ctrl,
            sum( sans_apres_ctrl_mvt) as sans_apres_ctrl_mvt,
            sum( clo_sans_ctrl) as clo_sans_ctrl,
            sum( clo_sans_ctrl_mvt) as clo_sans_ctrl_mvt,
            sum( ko_scan) as ko_scan,
            sum( ko_scan_mvt) as ko_scan_mvt,
            sum( ko_kd) as ko_kd,
            sum( ko_kd_mvt) as ko_kd_mvt,
            sum( ko_kinc) as ko_kinc,
            sum( ko_kinc_mvt) as ko_kinc_mvt,
            sum( ko_src) as ko_src,
            sum( ko_src_mvt) as ko_src_mvt,
            sum( ko_en_attt) as ko_en_attt,
            sum( ko_en_attt_mvt) as ko_en_attt_mvt,
            sum( ko_cir) as ko_cir,
            sum( ko_cir_mvt) as ko_cir_mvt,
            sum( clo_sans_tt) as clo_sans_tt,
            sum( clo_sans_tt_mvt) as clo_sans_tt_mvt,
            sum( edit) as edit,
            sum( cir_edit_mvt) as cir_edit_mvt,
            sum( cir_envoyee) as cir_envoyee,
            sum( cir_envoyee_mvt) as cir_envoyee_mvt,
            sum( ok_cir) as ok_cir,
            sum( ok_cir_mvt) as ok_cir_mvt,
            sum( ko_en_cours_by) as ko_en_cours_by,
            sum( ko_en_cours_by_mvt) as ko_en_cours_by_mvt,
            sum( divise) as divise,
            sum( divise_mvt	) as divise_mvt
            from(
            SELECT 
            1 pli_,data_pli.id_pli,coalesce(data_pli.mvt_nbr,0) mvt_nbr,
            CASE WHEN coalesce( data_pli.flag_traitement,0) < 1  THEN 1 ELSE 0 END AS stock_typage,
            CASE WHEN (coalesce( data_pli.flag_traitement,0) = 1) THEN 1 ELSE 0 END AS typage_en_cours,
            CASE WHEN (coalesce( data_pli.flag_traitement,0) = 2 ) and coalesce( data_pli.statut_saisie,0) = 1 THEN 1 ELSE 0 END AS typage_ok_typage,
            CASE WHEN (coalesce( data_pli.flag_traitement,0) = 2 ) and coalesce( data_pli.statut_saisie,0) = 1 THEN data_pli.mvt_nbr ELSE 0 END AS typage_ok_typage_mvt, 
            CASE WHEN data_pli.flag_traitement in(3) and  data_pli.statut_saisie = 1  then 1 else 0 end as stock_attente_saisie,
            CASE WHEN data_pli.flag_traitement in(3) and  data_pli.statut_saisie = 1  then data_pli.mvt_nbr else 0 end as stock_attente_saisie_mvt,
            case when (data_pli.flag_traitement = 4 and data_pli.statut_saisie = 1 ) then 1 else 0 end as en_cours_saisie,
            case when (data_pli.flag_traitement = 4 and data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr else 0 end as en_cours_saisie_mvt,
            case when (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
            or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1) then 1 else 0  end as saisie_ok,
            case when (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
            or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1) then data_pli.mvt_nbr else 0  end as saisie_ok_mvt,
            case when (data_pli.flag_traitement = 7  and  data_pli.statut_saisie = 1 ) then 1 else 0  end as stock_ctrl,
            case when (data_pli.flag_traitement = 7  and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr else 0  end as stock_ctrl_mvt,
            case when data_pli.flag_traitement in(8,11,13) and data_pli.statut_saisie = 1 then 1 else 0  end as en_cours_ctrl,
            case when data_pli.flag_traitement in(8,11,13) and data_pli.statut_saisie = 1 then 1 else 0  end as en_cours_ctrl_mvt,
            case when data_pli.flag_traitement in(9)  then 1 else 0  end as clo_apres_ctrl,
            case when data_pli.flag_traitement in(9)  then data_pli.mvt_nbr else 0  end as sans_apres_ctrl_mvt,
            case when data_pli.flag_traitement in(14)  then 1 else 0  end as clo_sans_ctrl,
            case when data_pli.flag_traitement in(14)  then data_pli.mvt_nbr else 0  end as clo_sans_ctrl_mvt,
            
            case when data_pli.statut_saisie = 2  then 1 else 0  end as ko_scan,
            case when data_pli.statut_saisie = 2  then data_pli.mvt_nbr else 0  end as ko_scan_mvt,
            case when data_pli.statut_saisie = 3  then 1 else 0  end as ko_kd,
            case when data_pli.statut_saisie = 3  then data_pli.mvt_nbr else 0  end as ko_kd_mvt,
            case when data_pli.statut_saisie = 4  then 1 else 0  end as ko_kinc,
            case when data_pli.statut_saisie = 4  then data_pli.mvt_nbr else 0  end as ko_kinc_mvt,
            case when data_pli.statut_saisie = 5  then 1 else 0  end as ko_src,
            case when data_pli.statut_saisie = 5  then data_pli.mvt_nbr else 0  end as ko_src_mvt,
            case when data_pli.statut_saisie = 6  then 1 else 0  end as ko_en_attt,
            case when data_pli.statut_saisie = 6  then data_pli.mvt_nbr else 0  end as ko_en_attt_mvt,
            case when data_pli.statut_saisie = 7  then 1 else 0  end as ko_cir,
            case when data_pli.statut_saisie = 7  then data_pli.mvt_nbr else 0  end as ko_cir_mvt,
            case when data_pli.statut_saisie = 8  then 1 else 0  end as clo_sans_tt,
            case when data_pli.statut_saisie = 8  then data_pli.mvt_nbr else 0  end as clo_sans_tt_mvt,
            case when data_pli.statut_saisie = 9  then 1 else 0  end as edit,
            case when data_pli.statut_saisie = 9  then data_pli.mvt_nbr else 0  end as cir_edit_mvt,
            case when data_pli.statut_saisie = 10  then 1 else 0  end as cir_envoyee,
            case when data_pli.statut_saisie = 10  then data_pli.mvt_nbr else 0  end as cir_envoyee_mvt,
            case when data_pli.statut_saisie = 11  then 1 else 0  end as ok_cir,
            case when data_pli.statut_saisie = 11  then data_pli.mvt_nbr else 0  end as ok_cir_mvt,
            case when data_pli.statut_saisie = 12  then 1 else 0  end as ko_en_cours_by,
            case when data_pli.statut_saisie = 12  then data_pli.mvt_nbr else 0  end as ko_en_cours_by_mvt,
            case when data_pli.statut_saisie = 24  then 1 else 0  end as divise,
            case when data_pli.statut_saisie = 24  then data_pli.mvt_nbr else 0  end as divise_mvt
            FROM f_pli
            JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
            LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
            where date_courrier::date between  '".$this->session->userdata('dateDebut')."' and '".$this->session->userdata('datefin')."'  
            and data_pli.societe in (".$this->session->userdata('societe').")) stat_ged";
            return $this->$conn->query($sql)->result();
    }
    
    public function GetSftpMail($source)
    {
        $sql = "
        SELECT sum(mail_sftp.flux) AS flux_total,
        sum(mail_sftp.flux_stock_typage) AS flux_stock_typage,
        sum(mail_sftp.flux_en_cours_typage) AS flux_en_cours_typage,
        sum(mail_sftp.flux_type) AS flux_type,
        sum(mail_sftp.flux_en_cours_nv1) AS flux_en_cours_nv1,
        sum(mail_sftp.flux_escalade) AS flux_escalade,
        sum(mail_sftp.flux_en_cours_nv2) AS flux_en_cours_nv2,
        sum(mail_sftp.flux_traite) AS flux_traite,
        sum(mail_sftp.flux_trans_cons) AS flux_trans_cons,
        sum(mail_sftp.flux_recep_cons) AS flux_recep_cons,
        sum(mail_sftp.flux_en_cours_retraite) AS flux_en_cours_retraite,
        sum(mail_sftp.flux_mail_anom) AS flux_mail_anom,
        sum(mail_sftp.flux_anom_pj) AS flux_anom_pj,
        sum(mail_sftp.flux_hors_perim) AS flux_hors_perim,
        sum(mail_sftp.flux_anom_fic) AS flux_anom_fic,
        sum(mail_sftp.flux_rejete) AS flux_rejete,
        sum(mail_sftp.flux_cloture) AS flux_cloture,
        sum(mail_sftp.flux_cloture_sans_ttmt) AS flux_cloture_sans_ttmt,
        sum(mail_sftp.flux_stock_typage_nb_abo) AS flux_stock_typage_nb_abo,
        sum(mail_sftp.flux_en_cours_typage_nb_abo) AS flux_en_cours_typage_nb_abo,
        sum(mail_sftp.flux_type_nb_abo) AS flux_type_nb_abo,
        sum(mail_sftp.flux_en_cours_nv1_nb_abo) AS flux_en_cours_nv1_nb_abo,
        sum(mail_sftp.flux_escalade_nb_abo) AS flux_escalade_nb_abo,
        sum(mail_sftp.flux_en_cours_nv2_nb_abo) AS flux_en_cours_nv2_nb_abo,
        sum(mail_sftp.flux_traite_nb_abo) AS flux_traite_nb_abo,
        sum(mail_sftp.flux_trans_cons_nb_abo) AS flux_trans_cons_nb_abo,
        sum(mail_sftp.flux_recep_cons_nb_abo) AS flux_recep_cons_nb_abo,
        sum(mail_sftp.flux_en_cours_retraite_nb_abo) AS flux_en_cours_retraite_nb_abo,
        sum(mail_sftp.flux_mail_anom_nb_abo) AS flux_mail_anom_nb_abo,
        sum(mail_sftp.flux_anom_pj_nb_abo) AS flux_anom_pj_nb_abo,
        sum(mail_sftp.flux_hors_perim_nb_abo) AS flux_hors_perim_nb_abo,
        sum(mail_sftp.flux_anom_fic_nb_abo) AS flux_anom_fic_nb_abo,
        sum(mail_sftp.flux_rejete_nb_abo) AS flux_rejete_nb_abo,
        sum(mail_sftp.flux_cloture_nb_abo) AS flux_cloture_nb_abo,
        sum(mail_sftp.flux_cloture_sans_ttmt_nb_abo) AS flux_cloture_sans_ttmt_nb_abo,
        sum(mail_sftp.nb_abonnement) AS nb_abonnement_total,
        sum(mail_sftp.flux_anom_src) AS flux_anom_src,
        sum(mail_sftp.flux_ko_definitif) AS flux_ko_definitif,
        sum(mail_sftp.flux_ko_inconnu) AS flux_ko_inconnu,
        sum(mail_sftp.flux_ko_attente) AS flux_ko_attente,
        sum(mail_sftp.flux_anom_src_nb_abo) AS flux_anom_src_nb_abo,
        sum(mail_sftp.flux_ko_definitif_nb_abo) AS flux_ko_definitif_nb_abo,
        sum(mail_sftp.flux_ko_inconnu_nb_abo) AS flux_ko_inconnu_nb_abo,
        sum(mail_sftp.flux_ko_attente_nb_abo) AS flux_ko_attente_nb_abo
        FROM ( SELECT flux.societe,
         1 AS flux,
             CASE WHEN flux.statut_pli = 0 OR flux.statut_pli IS NULL THEN 1 ELSE 0 END AS  flux_stock_typage,
             CASE WHEN flux.statut_pli = 1 THEN 1 ELSE 0 END AS  flux_en_cours_typage,
             CASE WHEN flux.statut_pli = 2  AND etat_pli_id = 1 THEN 1 ELSE 0 END AS  flux_type,
             CASE WHEN flux.statut_pli = 3 THEN 1 ELSE 0 END AS  flux_en_cours_nv1,
             CASE WHEN flux.statut_pli = 4 THEN 1 ELSE 0 END AS  flux_escalade,
             CASE WHEN flux.statut_pli = 5 THEN 1 ELSE 0 END AS  flux_en_cours_nv2,
             CASE WHEN flux.statut_pli = 7 AND flux.etat_pli_id = 1 THEN 1 ELSE 0 END AS  flux_traite,
             CASE WHEN flux.statut_pli = 8 THEN 1 ELSE 0 END AS  flux_trans_cons,
             CASE WHEN flux.statut_pli = 9 THEN 1 ELSE 0 END AS  flux_recep_cons,
             CASE WHEN flux.statut_pli = 10 THEN 1 ELSE 0 END AS  flux_en_cours_retraite,
             CASE WHEN flux.etat_pli_id = 11 THEN 1 ELSE 0 END AS  flux_mail_anom,
             CASE WHEN flux.etat_pli_id = 12 THEN 1 ELSE 0 END AS  flux_anom_pj,
             CASE WHEN flux.etat_pli_id = 13 THEN 1 ELSE 0 END AS  flux_hors_perim,
             CASE WHEN flux.etat_pli_id = 14 THEN 1 ELSE 0 END AS  flux_anom_fic,
             CASE WHEN flux.etat_pli_id = 15 THEN 1 ELSE 0 END AS  flux_rejete,
             CASE WHEN flux.statut_pli = 7 AND flux.etat_pli_id = 1 THEN 1 ELSE 0 END AS  flux_cloture,
             CASE WHEN flux.etat_pli_id = 100 AND flux.etat_pli_id = 1 THEN 1 ELSE 0 END AS  flux_cloture_sans_ttmt,
             CASE WHEN flux.statut_pli = 0 OR flux.statut_pli IS NULL THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_stock_typage_nb_abo,
             CASE WHEN flux.statut_pli = 1 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_en_cours_typage_nb_abo,
             CASE WHEN flux.statut_pli = 2  AND etat_pli_id = 1 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_type_nb_abo,
             CASE WHEN flux.etat_pli_id = 19 THEN 1 ELSE 0 END AS  flux_anom_src,
             CASE WHEN flux.etat_pli_id = 21 THEN 1 ELSE 0 END AS  flux_ko_definitif,
             CASE WHEN flux.etat_pli_id = 22 THEN 1 ELSE 0 END AS  flux_ko_inconnu,
             CASE WHEN flux.etat_pli_id = 23 THEN 1 ELSE 0 END AS  flux_ko_attente,
             CASE WHEN flux.etat_pli_id = 19 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_anom_src_nb_abo,
             CASE WHEN flux.etat_pli_id = 21 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_ko_definitif_nb_abo,
             CASE WHEN flux.etat_pli_id = 22 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_ko_inconnu_nb_abo,
             CASE WHEN flux.etat_pli_id = 23 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_ko_attente_nb_abo,
             CASE WHEN flux.statut_pli = 3 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_en_cours_nv1_nb_abo,
             CASE WHEN flux.statut_pli = 4 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_escalade_nb_abo,
             CASE WHEN flux.statut_pli = 5 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_en_cours_nv2_nb_abo,
             CASE WHEN flux.statut_pli = 7 AND flux.etat_pli_id = 1 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_traite_nb_abo,
             CASE WHEN flux.statut_pli = 8 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_trans_cons_nb_abo,
             CASE WHEN flux.statut_pli = 9 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_recep_cons_nb_abo,
             CASE WHEN flux.statut_pli = 10 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_en_cours_retraite_nb_abo,
             CASE WHEN flux.etat_pli_id = 11 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_mail_anom_nb_abo,
             CASE WHEN flux.etat_pli_id = 12 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_anom_pj_nb_abo,
             CASE WHEN flux.etat_pli_id = 13 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_hors_perim_nb_abo,
             CASE WHEN flux.etat_pli_id = 14 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_anom_fic_nb_abo,
             CASE WHEN flux.etat_pli_id = 15 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_rejete_nb_abo,
             CASE WHEN flux.statut_pli = 7 AND flux.etat_pli_id = 1 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_cloture_nb_abo,
             CASE WHEN flux.etat_pli_id = 100 AND flux.etat_pli_id = 1 THEN COALESCE(flux.nb_abonnement, 0) ELSE 0 END AS  flux_cloture_sans_ttmt_nb_abo,
         COALESCE(flux.nb_abonnement, 0) AS nb_abonnement
        FROM flux
       WHERE flux.date_reception::date between '".$this->session->userdata('dateDebut')."' and '".$this->session->userdata('datefin')."' AND flux.societe in( ".$this->session->userdata('societe').") AND flux.id_source = ".$source.") mail_sftp;";
          return $this->ged_flux->query($sql)->result();
    }

}
