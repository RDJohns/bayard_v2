<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_finished extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    private $push;

    private $tb_soc = TB_soc;
    private $ftb_soc = FTB_soc;
    private $tb_source = TB_sources;
    private $tb_typologieFlux = TB_typologieFlux;
    private $tb_flux = TB_flux;
    private $tb_lstMail = TB_lstMail;
    private $tb_lstDossier = TB_lstDossier;
    private $tb_abonmnt = TB_abonmnt;
    private $tb_statutAb = TB_statutAb;
    private $tb_pjs = TB_pjs;
    private $tb_socFx = TB_socFx;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
    }

    public function get_data($dt=NULL){
        $dt = is_null($dt) ? date('Y-m-d') : $dt;
        return $this->push
            ->from($this->tb_flux.' tbfx')
            ->join($this->tb_socFx.' tbsoc', 'tbfx.societe = tbsoc.id', 'INNER')
            ->join($this->tb_lstMail.' tbmail', 'tbmail.libelle = tbfx.sous_dossier', 'LEFT')
            ->join($this->tb_lstDossier.' tbsftp', 'tbsftp.nom_dossier = tbfx.sous_dossier', 'LEFT')
            ->where('statut_pli', 7)
            ->where('dt_traitement::DATE = \''.$dt.'\'', NULL, FALSE)
            ->select('tbfx.societe id_soc')

            ->select('tbmail.mail mail_login')
            ->select('tbmail.mdp mail_mdp')
            ->select('tbfx.from_flux mail_to')
            ->select('tbmail.mail mail_from')
            ->select('tbmail.libelle mail_from_name')

            ->select('tbsoc.mail sftp_login')
            ->select('tbsoc.mdp sftp_mdp')
            ->select('tbsftp.destinataire_mail sftp_to')
            ->select('tbsoc.mail sftp_from')
            ->select('tbsftp.nom_dossier sftp_from_name')

            ->select('*')
            ->get()->result();
    }

    public function pjs($id_flux){
        return $this->push
            ->from($this->tb_pjs)
            ->where('id_flux', $id_flux)
            ->order_by('ajoute', 'ASC')
            ->order_by('date_injection', 'ASC')
            ->get()->result();
    }
    
    public function abonnements($id_flux){
        return $this->push
            ->from($this->tb_abonmnt.' tbab')
            ->join($this->tb_statutAb.' tbsta', 'tbsta.id_statut = tbab.id_statut', 'INNER')
            ->where('id_flux', $id_flux)
            ->order_by('id')
            ->select('*')
            ->select('tbsta.id_statut id_stat')
            ->get()->result();
    }

}
