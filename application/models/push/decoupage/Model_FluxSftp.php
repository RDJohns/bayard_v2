<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_FluxSftp extends CI_Model
{

    private $CI;
    private $ged;
    private $base_64;
    private $ged_push;

    private $tbFlux                         = "flux";
    private $aDecouper                      = "view_flux_sftp_a_decouper";
    private $encoursDecoupage               = "view_fichier_en_cours_decoupage";
    private $fichierTemporaireDecoupage     = "fichier_temporaire_decoupage";
    private $logFileDecoupage               = "log_file_decoupage";
    private $currentUser                    = 0;
    private $fichierPublipostes             = "view_fichier_publipostes";
    private $fluxPubliposte                 = "flux_publiposte";

    function __construct()
    {
        parent::__construct();

        $this->CI           = &get_instance();
        $this->ged          = $this->load->database('ged', TRUE);
        $this->base_64      = $this->load->database('base_64', TRUE);
        $this->ged_push     = $this->load->database('ged_push', TRUE);
        $this->currentUser  = (int)$this->session->userdata('id_utilisateur');

    }

    public function getFluxADecoupe()
    {
        $getEncours = $this->getFichierEncoursDecoupage();

        $sqlUpdate = "UPDATE public.flux SET statut_pli = 17, decoupe_par = ".(int)$this->currentUser." WHERE id_flux = (SELECT id_flux FROM public.view_flux_sftp_a_decouper limit 1) ";

        if($getEncours)
        {
            return $getEncours;
        }
        else
        {
            $query  = $this->ged_push->query($sqlUpdate);
            $update = $this->ged_push->affected_rows();
            if($update > 0 )
            {
                return $this->getFichierEncoursDecoupage();
                // maka id flux
            }
            return null;
        }
    }

    public function getFichierEncoursDecoupage()
    {
        return $this->ged_push
            ->select('*')
            ->from($this->encoursDecoupage)
            ->where('decoupe_par',(int)$this->currentUser)
            ->get()->result_array();
    }

   public function annulerDecoupage($idFlux)
    {
        $this->ged_push->where('id_flux', (int)$idFlux);
        $this->ged_push->update($this->tbFlux, array("decoupe_par"=>0,"statut_pli"=>16));
        
        $verif = $this->getFichierEncoursDecoupage();
        if(count($verif) <= 0 )
        {
            $this->CI->histo->action(84, "", $idFlux);
            
            $this->ged_push->where('id_flux_parent', (int)$idFlux);
            $this->ged_push->delete($this->fluxPubliposte);
            return (int)$idFlux;
        }
        return 0;
    }

   public function insertNomFichier($data,$dataLog)
   {
        $insertion = $this->ged_push->insert($this->fichierTemporaireDecoupage, $data);
        $this->insertionLogFile($dataLog);
        return $insertion;
   }
   public function getFichierServeur($string)
    {
        return $this->ged_push
            ->select('*')
            ->from($this->fichierTemporaireDecoupage)
            ->where('nom_fichier_client',$string)
            ->get()->result();
    }
    public function deleteInDB($file)
    {
        $this->ged_push->where('nom_fichier_dans_serveur', $file);
        $this->ged_push->update($this->fichierTemporaireDecoupage, array("action"=>"suppresion"));

        $dataLog["user_id"]                     = (int)$this->currentUser;
        $dataLog["ip"]                          = $this->input->ip_address();
        $dataLog["action"]                      = "suppression";
        $dataLog["nom_fichier_dans_serveur"]    = $file;
        
        $this->insertionLogFile($dataLog);
    }

    public function insertionLogFile($data)
    {
        $this->ged_push->insert($this->logFileDecoupage, $data);
    }
    public function getLocation($idFlux)
    {
        return $this->ged_push
            ->select('*')
            ->from($this->tbFlux)
            ->where('id_flux',(int)$idFlux)
            ->get()->result();
    } 

    public function fichierPublipostes()
    {
        return $this->ged_push
            ->select('*')
            ->from($this->fichierPublipostes)
            ->where('user_id',(int)$this->currentUser)
            ->get()->result();
    }
    public function updateFlux($idFlux,$data,$dataFlux)
    {
        $this->ged_push->trans_begin();
        $this->ged_push->where('id_flux', $idFlux);
        $this->ged_push->update($this->tbFlux, array("statut_pli"=>18));
        
        $this->ged_push->insert($this->fluxPubliposte, $data);

        if(is_array($dataFlux) && count($dataFlux) > 0 )
        {
            $sqlFlux = $this->insererFlux($dataFlux);
            $this->ged_push->query($sqlFlux);
        }

        if ($this->ged_push->trans_status() === FALSE)
        {
            $this->ged_push->trans_rollback();
            return 0;
        }
        else
        {
            $this->ged_push->trans_commit();
            return 1;
        }
    }
    public function  insererFlux($data)
    {
        $idFlux         = $data["id_flux"];
        $statutPli      = $data["statut_pli"];
        $nomFichier     = $data["nom_fichier"]; /* nom crypte io*/
        $filenameOrigin = $data["filename_origin"];
        $emplacement    = $data["emplacement"]; /* REJET_KO/xyz.xyz, DECOUPAGE/MILAN/TELEVENTE_DPI/xyz.xyz */

        $sql = "INSERT INTO public.flux(dossier_flux,societe,id_source,sous_dossier,decoupe_par,id_flux_parent,statut_pli,nom_fichier,filename_origin,emplacement)
                SELECT dossier_flux,societe,id_source,sous_dossier,decoupe_par,".$idFlux.", ".$statutPli.", '".$nomFichier."', '".$filenameOrigin."', '".$emplacement."' FROM public.flux where id_flux =".(int)$idFlux;
        return $sql;
    }

    public function publiposter($idFlux)
    {
        $data = array();
        $data = array("statut_pli"=>16,"demande_decoupe_par"=>$this->currentUser,"type_par"=>null);

        $this->ged_push->where('id_flux',(int)$idFlux);
        $this->ged_push->update($this->tbFlux,$data);

        if($this->ged_push->affected_rows() > 0)
        {
            $this->CI->histo->action(80, "", $idFlux);
            return 1;
        }
        return 0;
    }
}
/**test git */