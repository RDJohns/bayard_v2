<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_LIBSftp extends CI_Model
{

    private $CI;
    private $ged_push;
    private $tbFlux             = "flux";
    private $fluxPubliposte     = "flux_publiposte";
    private $colonne            = "
                    id_flux, objet_flux, from_flux, fromname_flux, to_flux, dossier_flux,
                    uidmail, date_reception, flag_anomalie_pj, flag_hors_perimetre,
                    flag_traitement_niveau, flag_important, flag_retraitement, statut_pli,
                    typologie, nb_abonnement, entite, societe, id_source, type_par,
                    id_motif_blocage, description_motif, nom_fichier, filename_eml_origine,
                    date_envoi_mail, dossier, sous_dossier, emplacement, saisie_par,
                    nb_retraitement, id_lot_saisie_flux, lot_saisie_bis, date_lot_saisie,
                    motif_escalade, motif_transfert, nom_pjs, filename_origin, dt_quick_save,
                    mail_reception_typage, dt_traitement, sftp_envoi_mail_traitement,
                    toname_flux, decoupe_par, demande_decoupe_par, motif_ks ";

    function __construct()
    {
        parent::__construct();

        $this->CI           = &get_instance();
		$this->ged_push     = $this->load->database('ged_push', TRUE);
    }

    
    public function getFlux($idFlux)
    {
        return $this->ged_push
            ->select('*')
            ->from($this->tbFlux)
            ->where('id_flux',(int)$idFlux)
            ->get()->result();
    }

    public function moveFlux()
    {
        $sql = "INSERT INTO public.flux_parent(".$this->colonne.")  SELECT ".$this->colonne." FROM public.flux where  statut_pli = 18 ";
        $sqlDelete  = " DELETE FROM public.flux where  statut_pli = 18";

        $this->ged_push->trans_begin();
        $this->ged_push->query($sql);
        $this->ged_push->query($sqlDelete);
        if ($this->ged_push->trans_status() === FALSE)
        {
            $this->ged_push->trans_rollback();
        }
        else
        {
            $this->ged_push->trans_commit();
        }
    }

    public function fichierPubliposter($data)
    {
        return $this->ged_push->select("flux_publiposte.*,flux_parent.emplacement,flux_parent.nom_fichier,flux_parent.filename_origin")
             ->from("flux_publiposte")
             ->where($data)   
             ->join("flux_parent",'flux_parent.id_flux = flux_publiposte.id_flux_parent','left')->get()->result();  
                
    }
}
/**test git */