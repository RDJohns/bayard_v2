<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Model_FichierUser extends CI_Model 
    {
        
        private $ged_push;
        private $currentUser;

        protected $table        = "view_fichier_publipostes";
        protected $columnOrder  = array("id_flux","fichier_origine","fichier","date_action","type_publiposte");
        
        function __construct()
        {
            parent::__construct();
            $this->ged_push     = $this->load->database('ged_push', TRUE);
            $this->currentUser  = (int)$this->session->userdata('id_utilisateur');
        }

        public function fichierPublipostes()
        {
            $this->_get_query();
            if(isset($_POST['length']) && $_POST['length'] < 1) {
                $_POST['length']= '5';
            } else
            $_POST['length']= $_POST['length'];

            if(isset($_POST['start']) && $_POST['start'] > 1) {
                $_POST['start']= $_POST['start'];
            }
            
            $this->ged_push->limit($_POST['length'], $_POST['start']);
            $query = $this->ged_push->get();
            return $query->result();
        }

        private function _get_query()
        {
            $this->ged_push->from($this->table);
            $i = 0;
            $this->ged_push->where("user_id",(int)$this->currentUser);
            foreach ($this->columnOrder as $emp) 
            {
                if(isset($_POST['search']['value']) && !empty($_POST['search']['value'])){
                    $_POST['search']['value'] = $_POST['search']['value'];
                } else
                    $_POST['search']['value'] = '';
                if($_POST['search']['value']) 
                {
                    if($i===0) 
                    {
                        $this->ged_push->group_start();
                        $this->ged_push->like($emp, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->ged_push->or_like($emp, $_POST['search']['value']);
                    }

                    if(count($this->columnOrder) - 1 == $i) 
                        $this->ged_push->group_end(); 
                }
                $i++;
            }

            if(isset($_POST['order'])) 
            {
                $this->ged_push->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->ged_push->order_by(key($order), $order[key($order)]);
            }
        }

        function countFiltered()
        {
            $this->_get_query();
            $query = $this->ged_push->get();
            return $query->num_rows();
        }

        public function countAll()
        {
            $this->ged_push->from($this->table);
            $this->ged_push->where("user_id",(int)$this->currentUser);
            return $this->ged_push->count_all_results();
        }
    }
/**test git */