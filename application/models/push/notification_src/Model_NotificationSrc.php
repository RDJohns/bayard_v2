<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_NotificationSrc extends CI_Model
{

    private $CI;

    private $tb_flux = TB_flux;
    private $tb_traceMail = TB_traceMailSrc;

    public function __construct()
    {
        parent::__construct();
        $this->CI       = &get_instance();
        $this->ged      = $this->load->database('ged', TRUE);
        $this->base_64  = $this->load->database('base_64', TRUE);
        $this->ged_push = $this->load->database('ged_push', TRUE);

    }

    public function getSociete(){
        return $this->ged_push
            ->select('*')
            ->from('societe')
            ->get()
            ->result();
    }

    public function getDestinataire($soc){
        return $this->ged_push
            ->select('*')
            ->from('destinataire_transfert_src')
            ->where('societe', $soc)
            ->get()
            ->result();
    }

    public function getSource(){
        return $this->ged_push
                ->select('*')
                ->from('source')
                ->get()
                ->result();
    }

    public function getSrc($dest, $source){
        $query = "SELECT flux.id_flux, motif_ks, objet_flux,titre.dest_transfert_src_id, emplacement|| '/' || nom_fichier AS fichier,nom_fichier, LOWER(from_flux) as from_flux, LOWER(to_flux) as to_flux
                  FROM flux 
                  LEFT JOIN trace_mail_src ON flux.id_flux = trace_mail_src.id_flux 
                  LEFT JOIN view_traitement_oid ON view_traitement_oid.id_flux = flux.id_flux 
                  INNER JOIN ( SELECT id_flux, titre FROM abonnement GROUP BY id_flux,titre ORDER BY id_flux asc) as abonnement ON flux.id_flux = abonnement.id_flux 
                  LEFT JOIN titre ON titre.id_titre = abonnement.titre   
                  WHERE flux.etat_pli_id = 19 
                  AND trace_mail_src.id_flux IS NULL 
                  AND dest_transfert_src_id = ".$dest." 
                  AND flux.id_source = ".$source." 
                  GROUP BY flux.id_flux, motif_ks, objet_flux,titre.dest_transfert_src_id, fichier,nom_fichier,from_flux,to_flux 
                  ORDER BY flux.id_flux ASC ";

        return $this->ged_push
                    ->query($query)->result();
    }

    public function getSrc_test($dest, $source){
        $query = "SELECT flux.id_flux, motif_ks, objet_flux,titre.dest_transfert_src_id, emplacement|| '/' || nom_fichier AS fichier,nom_fichier, LOWER(from_flux) as from_flux, LOWER(to_flux) as to_flux
                  FROM flux 
                  LEFT JOIN trace_mail_src ON flux.id_flux = trace_mail_src.id_flux 
                  LEFT JOIN view_traitement_oid ON view_traitement_oid.id_flux = flux.id_flux 
                  INNER JOIN ( SELECT id_flux, titre FROM abonnement GROUP BY id_flux,titre ORDER BY id_flux asc) as abonnement ON flux.id_flux = abonnement.id_flux 
                  LEFT JOIN titre ON titre.id_titre = abonnement.titre   
                  WHERE flux.etat_pli_id = 19 
                  AND flux.id_flux IN (112471,
114671,
113880,
114886,
114905,
115100,
115146,
115156,
115193,
115608,
115602,
115708,
115672,
115882,
115853,
115985,
115986,
116017,
115987,
115957,
116160,
116219,
114593,
115894,
116626,
116584,
116628,
116790,
116832,
117088,
117200,
117343,
117587,
117330,
117241,
117345,
117589,
118529,
118830,
118530,
118636,
118855,
118915,
118959,
118914,
118955,
118960,
119021,
119216,
119048,
119124,
119120,
119140,
119291,
119210,
119404)
                  AND trace_mail_src.id_flux IS NULL 
                  AND dest_transfert_src_id = ".$dest." 
                  AND flux.id_source = ".$source." 
                  GROUP BY flux.id_flux, motif_ks, objet_flux,titre.dest_transfert_src_id, fichier,nom_fichier,from_flux,to_flux 
                  ORDER BY flux.id_flux ASC 
                  --LIMIT 5 ";

        return $this->ged_push
            ->query($query)->result();
    }

    public function add_trace_cron($module_nom,$nom_fichier,$date_jour,$couple,$statut){
        $data = array(
            'module_nom' => $module_nom,
            'fichier_nom' => $nom_fichier,
            'date_creation' => $date_jour,
            'couple' => $couple,
            'statut' => $statut
        );

        $this->ged_push->insert('etat_crons', $data);
    }

    public function add_trace_src($id_flux){

        $this->ged_push->trans_begin();

        /*foreach ($obj as $arr_flux){
            foreach ($arr_flux as $flux){
                $data = array(
                    'id_flux' => $flux->id_flux,
                );

                $this->ged_push->insert($this->tb_traceMail, $data);
            }
        }*/

        $this->ged_push->insert($this->tb_traceMail,array('id_flux' => $id_flux));

        if ($this->ged_push->trans_status() === FALSE)
        {
            $this->ged_push->trans_rollback();
            return false;
        }
        else
        {
            $this->ged_push->trans_commit();
            return true;
        }
    }

}