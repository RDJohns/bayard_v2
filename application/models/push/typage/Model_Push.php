<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Model_Push extends CI_Model
    {
        var $table         = 'view_liste_pli_circulaire';
        private $CI;
        private $ged;
        private $base_64;
        private $ged_push;

		private $tbFlux                     = "flux";
		private $tbReception                = "view_liste_pj_type";
		private $tbTypologie                = "view_typologie";
		private $tbFluxMailNonType          = "view_flux_mail_non_type";
		private $tbFluxSftpNonType          = "view_sftp_non_type";
		private $tbPJ                       = "pieces_jointes";
        private $anomalieFlux               = "anomalie_flux";
        private $motifConsigneFlux          = "motif_consigne_flux";
        
        function __construct()
        {
            parent::__construct();
            $this->CI       = &get_instance();
            $this->ged      = $this->load->database('ged', TRUE);
            $this->base_64  = $this->load->database('base_64', TRUE);
            $this->ged_push = $this->load->database('ged_push', TRUE);

        }

        public function listeEtatMail( $societe,$libelle)
        {
            $sql = " SELECT 
                        CASE WHEN nb_flux IS NULL THEN 0 ELSE nb_flux END AS nb_flux,
                        libelle
                FROM liste_mail
                LEFT JOIN
                (SELECT count(id_flux) nb_flux,
                        sous_dossier boite_mail
                    FROM public.flux
                    WHERE id_source = 1 and statut_pli in (1,0) and societe = ".(int)$societe."
                    AND  flag_important >= 0 and ( type_par = 0 or type_par is null or type_par = ".(int)$this->session->userdata('id_utilisateur').") GROUP BY sous_dossier) AS bm ON bm.boite_mail = liste_mail.libelle
                WHERE type = 1
                AND actif = 1 and societe_id = ".(int)$societe."
                ORDER BY libelle asc";

            $query =  $this->ged_push->query($sql);
            $this->saveLog('liste etat Mail');
            return $query->result();
        }
		
		public function listeSftp( $societe,$libelle)
        {
             $sql = " select nom_dossier,CASE WHEN nb_flux IS NULL THEN 0 ELSE nb_flux END AS nb_flux,societe from liste_dossier
					left join (
						SELECT count(id_flux) nb_flux,societe,
                        sous_dossier
                    FROM public.flux
                    WHERE id_source = 2 and statut_pli in (1,0) and societe in (".(int)$societe.",0)
                    AND  flag_important >= 0 and ( type_par = 0 or type_par is null or type_par = ".(int)$this->session->userdata('id_utilisateur').")
                    GROUP BY sous_dossier,societe
				) as sftp on sftp.sous_dossier = liste_dossier.nom_dossier ORDER BY nom_dossier asc";

            $query =  $this->ged_push->query($sql);

            $this->saveLog('liste Sftp');

            return $query->result();
        }
		
        /*1-verifier si efa manananana flux ve (par type ttmt), de alaina iny sinon maka vao²*/
        public function getFlux($societe,$traitement,$libelle)
        {
            /*maka vao²*/

            $myFlux = $this->getMyFlux();

            if( $myFlux <= 0 )
            {
                $sql = "
                UPDATE public.flux
                SET statut_pli = 1 , type_par = ".(int)$this->session->userdata('id_utilisateur')." 
                 WHERE id_flux = (SELECT id_flux
                 FROM public.view_flux where sous_dossier ilike '%".trim($libelle)."%' and societe in( ".(int)$societe.",0) and id_source = ".(int)$traitement."
                order by flag_important desc, date_reception asc,id_flux limit 1)";
    
                
                $query  = $this->ged_push->query($sql);
                $update = $this->ged_push->affected_rows();
                if($update > 0 )
                {
                    $this->saveLog( ' getFlux');

                    return $this->getMyFlux();
                }

                $this->saveLog('Pas de flux');

                return 0;
            }

            $this->saveLog('Pas de flux');
            return $myFlux;

           
            
        }
        
        public function getMyFlux()
        {
            $row  =  $this->ged_push /*misy pli anazy ao*/
            ->select('*')
            ->from($this->tbFlux)
            ->where('statut_pli',1)
            ->where('type_par',(int)$this->session->userdata('id_utilisateur'))
            ->get()->result();

            if($row)
            {
                $this->CI->histo->flux((int)$row[0]->id_flux,'');
               $this->CI->histo->action(65, "Typage flux", (int)$row[0]->id_flux);
                return (int)$row[0]->id_flux;
            }
            return 0;
        }
		
		 public function getAllMyFlux()
        {
            return $this->ged_push 
            ->select('*')
            ->from($this->tbFlux)
            ->where('statut_pli',1)
            ->where('flag_important >=',0)
            ->where('type_par',(int)$this->session->userdata('id_utilisateur'))
            ->get()->result();

           
        }
		
        public function afficherFlux($idFlux)
        {
            return html_escape($this->ged_push
            ->select('*')
            ->from($this->tbFlux)
            ->where('statut_pli',1)
            ->where('type_par',(int)$this->session->userdata('id_utilisateur'))
            ->where('id_flux',(int)$idFlux)
            ->get()->result_array());
        }
        public function annulerTypage()
        {
            
            $currentFlux = $this->getAllMyFlux();
            $arrayCurrent   = array(0);
            if($currentFlux)
            {
                foreach($currentFlux as $itemCurrent)
                {
                    $arrayCurrent[] =  $itemCurrent->id_flux;
                    $this->saveLog( ' annulation flux #'.$itemCurrent->id_flux);
                   $this->CI->histo->action(70, "Typage flux", $itemCurrent->id_flux);
                }

                $sql = "
                UPDATE public.flux
                SET statut_pli = 0 , 
                    typologie = 0,
                    nb_abonnement = NULL, 
                    entite = NULL, 
                    type_par = NULL, 
                    id_motif_blocage = NULL,
                    etat_pli_id = 1
                    WHERE type_par =".(int)$this->session->userdata('id_utilisateur')." and statut_pli = 1";
                    
                    $query  = $this->ged_push->query($sql);
              $this->CI->histo->flux_tab($arrayCurrent,'');
            }
						
			
            return $this->getMyFlux();    
        }

        public function getFluxMailAtraiter($societe,$traitement,$libelle)
        {
            $flux = null;
            $this->annulerTypage();
            $fluxID = $this->getFlux($societe,$traitement,$libelle);

            if($fluxID > 0)
            {
                $flux = $this->afficherFlux($fluxID);
            }

            return $flux;

        }

        public function getFile($fluxID) 
        {
            return $this->ged_push 
            ->select('*')
            ->from($this->tbFlux)
            ->where('id_flux',(int)$fluxID)
            ->get()->result();
        }

        public function getTypologie($societe,$traitement)
        {
            if((int)$societe == 0)
			{
				return $this->ged_push 
				->select('id_typologie,typologie')
				->distinct('id_typologie')
				->from($this->tbTypologie)
				->where('actif',1)
				->where_in('societe',array(1,2))
				->where('traitement',(int)$traitement)
				->get()->result();
			}
			return $this->ged_push 
            ->select('*')
            ->from($this->tbTypologie)
            ->where('actif',1)
            ->where('societe',(int)$societe)
            ->where('traitement',(int)$traitement)
            ->get()->result();
        }
		
		public function getMailInfo($idFlux)
		{
			return $this->ged_push 
            ->select('*')
            ->from($this->tbFluxMailNonType)
			->where('id_flux',(int)$idFlux)
            ->get()->result();
		}
		public function getMailInfoSftp($idFlux)
		{
			return $this->ged_push 
            ->select('*')
            ->from($this->tbFluxSftpNonType)
			->where('id_flux',(int)$idFlux)
            ->get()->result();
		}

        /*SELECT id_flux,societe
  FROM public.flux;
*/

		public function getSociete($fluxID)
		{
			 return $this->ged_push 
            ->select('id_flux,societe,id_source')
            ->from($this->tbFlux)
            ->where('id_flux',(int)$fluxID)
            ->get()->result();
		}
		public function getPJ($fluxID)
		{
			 return $this->ged_push 
            ->select('*')
            ->from($this->tbPJ)
            ->where('id_flux',(int)$fluxID)
            ->get()->result();
		}
		public function savePJ($data)
		{
            $id = (int)$data['id_flux'];
		    $this->deletePJ($id);
		    return $this->ged_push->insert_batch($this->tbPJ, $data);
		}
		
		public function saveUpdatePushTypage($data,$id)
		{
			$this->ged_push->where("id_flux",$id);
			
            $update =  $this->ged_push->update($this->tbFlux, $data);
            $this->CI->histo->flux((int)$id,'');

            $this->saveLog( ' saveUpdatePushTypage flux #'.$id);
            $this->CI->histo->action(66, "Typage flux", $id);
            return $update;
			
		}
		
		
		
		public function saveAnomalie($data,$tbFlux,$dataMotifConsigneFlux,$whereUpdate) 
		{
             /*$sql ="DELETE FROM public.motif_consigne_flux where id_flux = ".intval($whereUpdate)."; ";
                
            $query =  $this->ged_push->query($sql);*/
            $link_motif_consigne = intval($whereUpdate).date('YmdHis');
            $dataMotifConsigneFlux["link_traitement"] =  $link_motif_consigne;
            $this->ged_push->trans_begin();
			$this->ged_push->insert($this->anomalieFlux, $data); 
            
            $this->ged_push->insert($this->motifConsigneFlux,$dataMotifConsigneFlux);
			$this->ged_push->where("id_flux",$whereUpdate);
			$this->ged_push->update($this->tbFlux, $tbFlux); 
            
            
			if ($this->ged_push->trans_status() === FALSE)
			{
					$this->ged_push->trans_rollback();
					return 0;
			}
			else
			{
                    $this->ged_push->trans_commit();
                    $this->CI->histo->flux($whereUpdate,'anomalie', $link_motif_consigne);
                    $this->saveLog( ' saveAnomalie flux #'.$whereUpdate);
                    $this->CI->histo->action(67, "Typage flux", $whereUpdate);
					return 1;
			}
			
		}
        public function deleteAnomalie($id)
        {
            $this->ged_push->delete($this->anomalieFlux, array("flux_id"=>$id));
        }

        public function deletePJ($id)
        {
            $this->ged_push->delete($this->tbPJ, array("id_flux"=>$id,"ajoute"=>1));
        }

		public function updateFlagMail($whereUpdate)
        {
            $this->ged_push->where("id_flux",$whereUpdate);
            $this->ged_push->update($this->tbFlux, array("mail_reception_typage"=>1));

            $this->saveLog("update Flag Mail #".$whereUpdate);
        }
		
		public function getFluxIDType($fluxID)
		{
			 return $this->ged_push 
            ->select('*')
            ->from($this->tbReception)
            ->where('id_flux',(int)$fluxID)
            ->get()->result();
		}

        public function saveLog($msg)
        {
            $info = $this->session->userdata('info_personnel');
            log_message_push('error', $info.' > Model Typage flux >'.$msg);
        }

        public function standBy($idFlux)
        {
            return $this->ged_push
                ->select('*')
                ->from($this->tbFlux)
                ->where('id_flux',(int)$idFlux)
                ->where('flag_important',-1)
                ->get()->result();
        }
        public function getConsigneClient($idFlux)
        {
            $sql ="
                SELECT  id_flux, string_agg(consigne_client_flux,'; ') motif_operateur
                FROM public.motif_consigne_flux where id_flux = ".intval($idFlux)."
                group by id_flux ";
                
            $query =  $this->ged_push->query($sql);
            return $query->result();
        }
    }