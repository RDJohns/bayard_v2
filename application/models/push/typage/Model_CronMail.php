<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_CronMail extends CI_Model
{
    var $table                          = 'view_liste_pli_circulaire';
    private $CI;
    private $ged;
    private $base_64;
    private $ged_push;

    private $tbFlux                     = "flux";
    private $tbReception                = "view_liste_pj_type";
    private $tbTypologie                = "view_typologie";
    private $tbFluxMailNonType          = "view_flux_mail_non_type";
    private $tbFluxSftpNonType          = "view_sftp_non_type";
    private $tbPJ                       = "pieces_jointes";
    private $anomalieFlux               = "anomalie_flux";

    function __construct()
    {
        parent::__construct();
        $this->CI       = &get_instance();
        $this->ged      = $this->load->database('ged', TRUE);
        $this->base_64  = $this->load->database('base_64', TRUE);
        $this->ged_push = $this->load->database('ged_push', TRUE);

    }

    public function getFluxType()
    {
        return $this->ged_push
        ->select('id_flux')
            ->from($this->tbFlux)
            ->where('mail_reception_typage',0)
            ->where('statut_pli',2)
            ->get()->result();
    }
    public function getFluxAnomalieType()
    {
        return $this->ged_push
            ->select('id_flux,id_source')
            ->from($this->tbFlux)
            ->where('mail_reception_typage',0)
           ->where_in('statut_pli',array(11,12,13,14))
            ->get()->result();
    }

    public function getMailInfo($idFlux)
    {
        $sql =" SELECT flux.id_flux,
                flux.societe,
                flux.objet_flux,
                flux.from_flux,
                flux.fromname_flux,
                flux.sous_dossier,
                mail.mail,
                mail.mdp,
                flux.id_source, 
                flux.statut_pli,
                anomalie_flux.anomalie
               FROM flux
                 LEFT JOIN ( SELECT liste_mail.id,
                        liste_mail.mail,
                        liste_mail.libelle,
                        liste_mail.mdp,
                        liste_mail.type,
                        liste_mail.societe_id,
                        liste_mail.actif
                       FROM liste_mail
                      WHERE liste_mail.actif = 1) mail ON mail.libelle::text = flux.sous_dossier::text 
                      LEFT JOIN anomalie_flux on anomalie_flux.flux_id = flux.id_flux
              WHERE flux.id_source = 1 AND flux.statut_pli in (11,12,13,14) and flux.mail_reception_typage = 0 and flux.id_flux =".(int)$idFlux;

        $query = $this->ged_push->query($sql);
        return $query->result();

    }

    public function getMailInfoSftp($idFlux)
    {
        $sql = "SELECT flux.id_flux,
                flux.societe,
                flux.sous_dossier,
                flux.filename_origin,
                mail.id,
                mail.nom_dossier,
                mail.extraction_automatique,
                mail.envoi_anomalie_fichier,
                mail.envoi_mail_hors_p,
                mail.envoi_accuse_reception,
                mail.envoi_accuse_traitement,
                mail.societe_milan,
                mail.societe_bayard,
                mail.actif,
                mail.mail_test,
                mail.destinataire_mail,
                societe.mail,
                societe.mdp,
                societe.nom_societe,
                flux.id_source,
                flux.statut_pli,
                anomalie_flux.anomalie
               FROM flux
                 LEFT JOIN ( SELECT liste_dossier.id,
                        liste_dossier.nom_dossier,
                        liste_dossier.extraction_automatique,
                        liste_dossier.envoi_anomalie_fichier,
                        liste_dossier.envoi_mail_hors_p,
                        liste_dossier.envoi_accuse_reception,
                        liste_dossier.envoi_accuse_traitement,
                        liste_dossier.societe_milan,
                        liste_dossier.societe_bayard,
                        liste_dossier.actif,
                        liste_dossier.mail_test,
                        liste_dossier.destinataire_mail
                       FROM liste_dossier) mail ON mail.nom_dossier::text = flux.sous_dossier::text
                 LEFT JOIN societe ON societe.id = flux.societe 
		 LEFT JOIN anomalie_flux on anomalie_flux.flux_id = flux.id_flux
              WHERE flux.id_source = 2 AND flux.statut_pli in (11,12,13,14) and flux.mail_reception_typage = 0 and flux.id_flux =".(int)$idFlux;;

        $query = $this->ged_push->query($sql);
        return $query->result();
    }

    public function getAnomalie($fluxID)
    {
        return $this->ged_push
            ->select('*')
            ->from($this->anomalieFlux)
            ->where('flux_id',(int)$fluxID)
            ->get()->result();
    }

    public function saveTraceMail($data)
    {
        $this->ged_push->insert('trace_envoi_mail', $data);
    }
    



}