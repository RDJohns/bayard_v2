<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_flux extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    private $push;

    private $tb_soc = TB_soc;
    private $tb_source = TB_sources;
    private $tb_typologieFlux = TB_typologieFlux;
    private $tb_lotSaisieFlux = TB_lotSaisieFlux;
    private $vw_lotSaisieFlux = VW_lotSaisieFlux;
    private $tb_flux = TB_flux;
    private $tb_titreFx = TB_titreFx;
    private $tb_abonmnt = TB_abonmnt;
    private $tb_statutAb = TB_statutAb;
    private $tb_pjs = TB_pjs;
    private $tb_anomaFx = TB_anomalieFlux;
    private $tb_socFx = TB_socFx;
    private $tb_motifConsFx = TB_motifConsigneFx;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
    }

    public function societies(){
        return $this->ged
            ->from($this->tb_socFx)
            ->where('actif', 1)
            ->order_by('nom_societe')
            ->get()->result();
    }

    public function sources(){
        return $this->push
            ->from($this->tb_source)
            ->where('actif', 1)
            ->order_by('source')
            ->get()->result();
    }

    public function typologies($options=NULL, $all=FALSE){
        $opt_dispo = array('mail', 'sftp', 'bayard', 'milan');
        foreach ($opt_dispo as $option) {
            if(isset($options[$option]) && !is_null($options[$option])){
                if($options[$option]){
                    $this->push->where($option, 1);
                }
            }
        }
        if(!$all){
            $this->push->where('id_typologie !=', 1);
        }
        $data = $this->push
            ->from($this->tb_typologieFlux)
            ->where('actif', 1)
            ->order_by('typologie')
            ->get()->result();
        //log_message('error', $this->push->last_query());
        return $data;
    }

    public function last_lot_saisie($id_utilisateur, $today=TRUE){
        if($today){
            $this->push->where('dt::DATE = NOW()::DATE', NULL, FALSE);
        }
        $data = $this->push
            ->select('*')
            ->from($this->vw_lotSaisieFlux)
            ->where('id_user', $id_utilisateur)
            ->order_by('dt', 'DESC')
            ->limit(1)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }

    public function nb_flux_for_lot_saisie($id_lot_saisie){
        return $this->push
            ->from($this->tb_flux)
            ->where('id_lot_saisie_flux', $id_lot_saisie)
            ->count_all_results();
    }

    public function titres($societe=NULL, $all=FALSE){
        if(is_numeric($societe)){
            $this->push->where('societe', $societe);
        }
        if(!$all){
            $this->push->where('id_titre !=', 1);
        }
        return $this->push
            ->where('actif', 1)
            ->from($this->tb_titreFx)
            ->order_by('titre')
            ->get()->result();
    }

    public function my_abonnements($id_flux){
        return $this->push
            ->from($this->tb_abonmnt)
            ->where('id_flux', $id_flux)
            ->order_by('id')
            ->get()->result();
    }

    public function statut_abonnement(){
        return $this->push
            ->from($this->tb_statutAb)
            ->order_by('id_statut')
            ->get()->result();
    }

    public function my_pjs($id_flux){
        return $this->push
            ->from($this->tb_pjs)
            ->where('id_flux', $id_flux)
            ->order_by('ajoute', 'ASC')
            ->order_by('date_injection', 'ASC')
            ->get()->result();
    }

    public function motif_consigne($id_flux){
        $data = $this->push
            ->from($this->tb_motifConsFx)
            ->where('id_flux', $id_flux)
            ->order_by('dt_motif_ko', 'DESC')
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }

    public function quick_save($data){
        $this->push->trans_begin();
        try {
            $this->push
                ->set($data->data_flux)
                ->set('dt_quick_save', 'NOW()', FALSE)
                ->where('id_flux', $data->id_flux)
                ->update($this->tb_flux);
            $this->push->where('id_flux', $data->id_flux)->delete($this->tb_abonmnt);
            if(count($data->data_abonnement) > 0){
                $this->push->insert_batch($this->tb_abonmnt, $data->data_abonnement);
            }
            $this->push->where('id_flux', $data->id_flux)->where('ajoute', 1)->delete($this->tb_pjs);
            if(count($data->data_pjs) > 0){
                $this->push->insert_batch($this->tb_pjs, $data->data_pjs);
            }
        } catch (Exception $exc) {
            $this->push->trans_rollback();
            log_message('error', 'models> push> saisie> model_flux> quick_save:'.$exc->getMessage());
            return FALSE;
        }
        if($this->push->trans_status() === FALSE){
            $this->push->trans_rollback();
            log_message('error', 'Sauvegard rapide échouée!=> id_flux: '.$data->id_flux.',  operateur: '.$this->CI->session->id_utilisateur);
            return FALSE;
        }else{
            $this->push->trans_commit();
            return TRUE;
        }
    }

    public function simple_save($data){
        $this->push->trans_begin();
        try {
            $this->push
                ->set($data->data_flux)
                ->where('id_flux', $data->id_flux)
                ->update($this->tb_flux);
            $this->push->where('id_flux', $data->id_flux)->delete($this->tb_abonmnt);
            if(count($data->data_abonnement) > 0){
                $this->push->insert_batch($this->tb_abonmnt, $data->data_abonnement);
            }
            $this->push->where('id_flux', $data->id_flux)->where('ajoute', 1)->delete($this->tb_pjs);
            if(count($data->data_pjs) > 0){
                $this->push->insert_batch($this->tb_pjs, $data->data_pjs);
            }
            if(isset($data->motif_consigne_flux)){
                // $this->push->where('id_flux', $data->id_flux)->delete($this->tb_motifConsFx);
                if(is_array($data->motif_consigne_flux) && count($data->motif_consigne_flux) > 0){
                    $this->push->insert($this->tb_motifConsFx, $data->motif_consigne_flux);
                }
            }
        } catch (Exception $exc) {
            $this->push->trans_rollback();
            log_message('error', 'models> push> saisie> model_flux> simple_save:'.$exc->getMessage());
            return FALSE;
        }
        if($this->push->trans_status() === FALSE){
            $this->push->trans_rollback();
            log_message('error', 'Simple sauvegard échouée!=> id_flux: '.$data->id_flux.',  operateur: '.$this->CI->session->id_utilisateur.', for state: '.$data->data_flux->statut_pli);
            return FALSE;
        }else{
            $this->push->trans_commit();
            if(isset($data->link_traitement_motif_consigne)){
                $this->CI->histo->flux($data->id_flux, 'ko', $data->link_traitement_motif_consigne);
            }else{
                $this->CI->histo->flux($data->id_flux);
            }
            return TRUE;
        }
    }

    public function save_traitement($data, $lot_saisie){
        $existe_lot_saisie = $this->push
            ->from($this->tb_lotSaisieFlux)
            ->where('num', $lot_saisie->num)
            ->where('login', $lot_saisie->login)
            ->count_all_results() > 0;
        $this->push->trans_begin();
        try {
            if(isset($lot_saisie->id) && is_numeric($lot_saisie->id)){
                $data->data_flux['id_lot_saisie_flux'] = $lot_saisie->id;
            }else{
                if($existe_lot_saisie){
                    log_message('error', 'Lot saisie en doublon : '.$lot_saisie->num.'_'.$lot_saisie->login.', op: '.$lot_saisie->id_user);
                    return array(FALSE, 'Lot saisie en doublon!');
                }
                $sql_lot_saisie = $this->push->set($lot_saisie)->get_compiled_insert($this->tb_lotSaisieFlux) . ' RETURNING id ';
                $retour_insert = $this->push->query($sql_lot_saisie)->result();
                $data->data_flux['id_lot_saisie_flux'] = $retour_insert[0]->id;
            }
            $this->push->where('id_flux', $data->id_flux)->delete($this->tb_motifConsFx);
            $this->push
                ->set($data->data_flux)
                ->set('dt_traitement', date('Y-m-d H:i:s'))
                ->where('id_flux', $data->id_flux)
                ->update($this->tb_flux);
            $this->push->where('id_flux', $data->id_flux)->delete($this->tb_abonmnt);
            if(count($data->abonnements) > 0){
                $this->push->insert_batch($this->tb_abonmnt, $data->abonnements);
            }
            $this->push->where('id_flux', $data->id_flux)->where('ajoute', 1)->delete($this->tb_pjs);
            if(count($data->pjs) > 0){
                $this->push->insert_batch($this->tb_pjs, $data->pjs);
            }
        } catch (Exception $exc) {
            $this->push->trans_rollback();
            log_message('error', 'models> push> saisie> model_flux> save_traitement:'.$exc->getMessage());
            return array(FALSE, 'Erreur serveur!');
        }
        if($this->push->trans_status() === FALSE){
            $this->push->trans_rollback();
            log_message('error', 'push> saisie> model_flux> save_traitement> Simple sauvegard échouée!=> id_flux: '.$data->id_flux.',  operateur: '.$this->CI->session->id_utilisateur.', for state: '.$data->data_flux->statut_pli);
            return array(FALSE, 'Enregistrement échoué!');
        }else{
            $this->push->trans_commit();
            $this->CI->histo->action(57, '', $data->id_flux);
            $this->CI->histo->flux($data->id_flux);
            return array(TRUE, '');
        }
    }

    public function anomalie_save($data){
        $this->push->trans_begin();
        try {
            $this->push
                ->set($data->data_flux)
                ->where('id_flux', $data->id_flux)
                ->update($this->tb_flux);
            $modif = $this->push->affected_rows();
            $this->push->where('flux_id', $data->id_flux)->delete($this->tb_anomaFx);
            $this->push->insert($this->tb_anomaFx, $data->data_anomalie);
        } catch (Exception $exc) {
            $this->push->trans_rollback();
            log_message('error', 'models> push> saisie> model_flux> anomalie_save:'.$exc->getMessage());
            return FALSE;
        }
        if($this->push->trans_status() === FALSE or $modif < 1){
            $this->push->trans_rollback();
            log_message('error', 'push> saisie> model_flux> anomalie_save> sauvegard anomalie échouée!=> id_flux: '.$data->id_flux.',  operateur: '.$this->CI->session->id_utilisateur.', for state: '.$data->data_flux->statut_pli);
            return FALSE;
        }else{
            $this->push->trans_commit();
            $this->CI->histo->flux($data->id_flux);
            return TRUE;
        }
    }

    public function flux_for_me($id_flux){
        return $this->push
            ->where('id_flux', $id_flux)
            ->where('saisie_par', $this->CI->session->id_utilisateur)
            ->where_in('statut_pli', array(3, 5, 10))
            ->from($this->tb_flux)
            ->count_all_results() > 0;
    }

    public function flux_line($id_flux){
        if(!is_numeric($id_flux)){
            return NULL;
        }
        $data = $this->push
            ->where('id_flux', $id_flux)
            ->from($this->tb_flux)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }
    
    public function lot_saisie($limit, $offset, $arg_find, $arg_ordo, $sens, $filtre, $nb_only = TRUE){
        $id_user = $this->CI->session->id_utilisateur;
        $sql_sub = $this->push
            ->select('tblot.id id_lot')
            ->select('COUNT(id_flux) nb_flux', FALSE)
            ->select('STRING_AGG(\'#\' || id_flux::VARCHAR, \', \' ORDER BY id_flux) flux', FALSE)
            ->from($this->tb_lotSaisieFlux.' tblot')
            ->where('id_user', $id_user)
            ->join($this->tb_flux.' tbfx', 'tblot.id=id_lot_saisie_flux', 'INNER')
            ->where('lot_saisie_bis', 0)
            ->group_by('tblot.id')
            ->get_compiled_select();
		$this->push->reset_query();
        $sql_sub_bis = $this->push
            ->select('tblot.id id_lot')
            ->select('COUNT(id_flux) nb_flux_bis', FALSE)
            ->select('STRING_AGG(\'#\' || id_flux::VARCHAR, \', \' ORDER BY id_flux) flux_bis', FALSE)
            ->from($this->tb_lotSaisieFlux.' tblot')
            ->where('id_user', $id_user)
            ->join($this->tb_flux.' tbfx', 'tblot.id=id_lot_saisie_flux', 'INNER')
            ->where('lot_saisie_bis', 1)
            ->group_by('tblot.id')
            ->get_compiled_select();
        $this->push->reset_query();
        $this->push
            ->from($this->vw_lotSaisieFlux.' tblot')
            ->where('id_user', $id_user)
            ->join(' ('.$sql_sub.') tbsub', 'tblot.id = tbsub.id_lot', 'LEFT')
            ->join(' ('.$sql_sub_bis.') tbsub_bis', 'tblot.id = tbsub.id_lot', 'LEFT')
            ->select('dt')
            ->select('lot_advantage identifiant')
            ->select('(CASE WHEN nb_flux IS NULL THEN 0 ELSE nb_flux END)::integer nb_flux')
            ->select('flux')
            ->select('(CASE WHEN nb_flux_bis IS NULL THEN 0 ELSE nb_flux_bis END)::integer nb_flux_bis')
            ->select('flux_bis')
            ->select('(CASE WHEN nb_flux IS NULL THEN 0 ELSE nb_flux END)::integer + (CASE WHEN nb_flux_bis IS NULL THEN 0 ELSE nb_flux_bis END)::integer total')
            ->select('nom_societe soc')
            ->select('source src')
            ->select('typologie typo');
        if(!empty(trim($arg_find))){
            $this->push
                ->group_start()
                    ->like('lower(lot_advantage)', $arg_find)
                    ->or_like('flux', $arg_find)
                    ->or_like('flux_bis', $arg_find)
                    ->or_like('lower(nom_societe)', $arg_find)
                    ->or_like('lower(source)', $arg_find)
                    ->or_like('lower(typologie)', $arg_find)
                ->group_end();
        }
        if(isset($filtre['dt']) && !empty(trim($filtre['dt']))){
            $this->push->where('dt::DATE', trim($filtre['dt']), FALSE);
        }
        if (!$nb_only) {
            $this->push
                ->order_by($arg_ordo, strtoupper($sens))
                ->limit($limit, $offset);
			return $this->push->get()->result();
        }
        return $this->push->count_all_results();
    }
    
    public function my_lot_total(){
        $id_user = $this->CI->session->id_utilisateur;
        return $this->push
            ->from($this->tb_lotSaisieFlux)
            ->where('id_user', $id_user)
            ->count_all_results();
    }

}
