<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_saisie1 extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    private $push;

    private $tb_soc = TB_soc;
    private $tb_source = TB_sources;
    private $ftb_soc = FTB_soc;
    private $tb_typologieFlux = TB_typologieFlux;
    private $tb_lotSaisieFlux = TB_lotSaisieFlux;
    private $vw_lotSaisieFlux = VW_lotSaisieFlux;
    private $tb_flux = TB_flux;
    private $tb_statutPli = TB_statutPli;
    private $tb_socFx = TB_socFx;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
    }

    public function piocher_flux($id_utilisateur, $filtres){
        $flux = $this->current_flux($id_utilisateur, $filtres);
        if(is_null($flux)){
            $flux = $this->get_a_flux($id_utilisateur, $filtres);
        }
        return $flux;
    }

    private function current_flux($id_utilisateur, $filtres){
        $data = $this->push
            ->from($this->tb_flux.' tbfx')
            ->join($this->tb_socFx.' tbsoc', 'tbfx.societe = tbsoc.id', 'INNER')
            ->join($this->tb_source.' tbsrc', 'tbsrc.id_source = tbfx.id_source', 'INNER')
            ->join($this->tb_typologieFlux.' tbtypo', 'tbtypo.id_typologie = tbfx.typologie', 'INNER')
            ->where('saisie_par', $id_utilisateur)
            ->where('statut_pli', 3)
            ->where('tbfx.societe', $filtres['current_society'])
            ->where('tbfx.id_source', $filtres['current_source'])
            ->where('tbfx.typologie', $filtres['current_typologie'])
            ->order_by('flag_important', 'DESC')
            ->order_by('date_reception', 'ASC')
            ->order_by('id_flux', 'ASC')
            ->select('*')
            ->select('tbfx.id_source id_src')
            ->select('tbfx.societe soc')
            ->select('tbsrc.source src')
            ->select('tbtypo.typologie typo')
            ->select('tbfx.typologie id_typo')
            ->get()->result();
        if(count($data) > 0){
            $this->CI->histo->action(62, '', $data[0]->id_flux);
            $this->CI->histo->flux($data[0]->id_flux);
            return $data[0];
        }else{
            $this->clear_niveau($id_utilisateur);
            return NULL;
        }
    }

    private function get_a_flux($id_utilisateur, $filtres){
        $data = $this->push
            ->from($this->tb_flux.' tbfx')
            ->join($this->tb_socFx.' tbsoc', 'tbfx.societe = tbsoc.id', 'INNER')
            ->join($this->tb_source.' tbsrc', 'tbsrc.id_source = tbfx.id_source', 'INNER')
            ->join($this->tb_typologieFlux.' tbtypo', 'tbtypo.id_typologie = tbfx.typologie', 'INNER')
            ->where('saisie_par IS NULL', NULL, FALSE)
            ->where('statut_pli', 2)
            ->where('etat_pli_id', 1)
            ->where('tbfx.societe', $filtres['current_society'])
            ->where('tbfx.id_source', $filtres['current_source'])
            ->where('tbfx.typologie', $filtres['current_typologie'])
            ->order_by('flag_important', 'DESC')
            ->order_by('date_reception', 'ASC')
            ->order_by('id_flux', 'ASC')
            ->select('*')
            ->select('tbfx.id_source id_src')
            ->select('tbfx.societe soc')
            ->select('tbsrc.source src')
            ->select('tbtypo.typologie typo')
            ->select('tbfx.typologie id_typo')
            ->get()->result();
        if(count($data) > 0){
            $flux = $data[0];
            if($this->locking($id_utilisateur, $flux->id_flux)){
                $this->CI->histo->action(50, '', $flux->id_flux);
                $this->CI->histo->flux($flux->id_flux);
                return $flux;
            }else {
                return $this->get_a_flux($id_utilisateur, $filtres);
            }
        }
        return NULL;
    }

    private function locking($id_utilisateur, $id_flux){
        $data_lock = array(
            'statut_pli' => 3
            ,'saisie_par' => $id_utilisateur
        );
        $this->push
            ->where('id_flux', $id_flux)
            ->where('statut_pli', 2)
            ->set($data_lock)
            ->update($this->tb_flux);
        return $this->push->affected_rows() > 0;
    }

    private function clear_niveau($id_utilisateur){
        $flux_locked = $this->push
            ->from($this->tb_flux)
            ->where('statut_pli', 3)
            ->where('saisie_par', $id_utilisateur)
            ->get()->result();
        foreach ($flux_locked as $flux) {
            $this->unlock($flux->id_flux);
        }
    }

    public function unlock($id_flux){
        $data_modif = array(
            'saisie_par' => NULL
            ,'statut_pli' => 2
        );
        $this->push
            ->where('id_flux', $id_flux)
            ->where('statut_pli', 3)
            ->set($data_modif)
            ->update($this->tb_flux);
        if($this->push->affected_rows() == 1){
            $this->CI->histo->action(51, 'Automatique', $id_flux);
            $this->CI->histo->flux($id_flux, 'Automatique');
            return TRUE;
        }else{
            log_message('error', 'models> push> saisie> model_saisie1> unlock: unlocking failed '.$this->push->last_query());
            return FALSE;
        }
    }

    public function output_status($source){
        if($source == 1){
            $this->push->where_not_in('id_statut', array(14));
        }elseif ($source == 2) {
            $this->push->where_not_in('id_statut', array(11, 12));
        }
        return $this->push
            ->from($this->tb_statutPli)
            ->where('sortie_n1', 1)
            ->order_by('id_statut')
            ->get()->result();
    }

	public function unlock_cancel($id_flux){
		$data_modif = array(
            'statut_pli' => 2
            ,'saisie_par' => NULL
        );
        $this->push
            ->where('id_flux', $id_flux)
            ->where('statut_pli', 3)
            ->where('saisie_par', $this->CI->session->id_utilisateur)
            ->set($data_modif)
            ->update($this->tb_flux);
        return $this->push->affected_rows() > 0;
    }
    
    public function group_fx_by_older(){
        return $this->push
            ->from($this->tb_flux.' tbfx')
            ->join($this->tb_socFx.' tbsoc', 'tbfx.societe = tbsoc.id', 'INNER')
            ->join($this->tb_source.' tbsrc', 'tbsrc.id_source = tbfx.id_source', 'INNER')
            ->join($this->tb_typologieFlux.' tbtypo', 'tbtypo.id_typologie = tbfx.typologie', 'INNER')
            ->where('statut_pli', 2)
            ->where('etat_pli_id', 1)
            ->group_by('date_reception::DATE', FALSE)
            ->group_by('tbsoc.nom_societe')
            ->group_by('tbsrc.source')
            ->group_by('tbtypo.typologie')
            ->select('date_reception::DATE dt', FALSE)
            ->select('tbsoc.nom_societe soc')
            ->select('tbsrc.source src')
            ->select('tbtypo.typologie typo')
            ->select('COUNT(id_flux) nb', FALSE)
            ->select('tbsoc.id id_soc')
            ->select('tbsrc.id_source id_src')
            ->select('tbtypo.id_typologie id_typo')
            ->group_by('tbsoc.id')
            ->group_by('tbsrc.id_source')
            ->group_by('tbtypo.id_typologie')
            ->order_by('dt', 'ASC')
            ->order_by('nb', 'DESC')
            ->order_by('soc')
            ->order_by('src')
            ->order_by('typo')
            ->get()->result();
    }

}
