<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_echantillonnage extends CI_Model{

    private $CI;

    private $ged;
    private $base_64;

    private $tb_pli = TB_pli;
    private $tb_decoupage = TB_decoupage;
    private $tb_dataPli = TB_data_pli;
    private $vw_dataPli = Vw_dataPli;

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    //*************Découpage**************//

    public function init_dt_decoupage($taux){
        $query = "INSERT INTO ".$this->tb_decoupage." (date_decoupage,taux) VALUES (DEFAULT,".$taux.") ";
        $this->base_64->query($query);
        return $this->base_64->insert_id();
    }

    public function get_pli_to_cut_crga(){ // plis pour catégories commande réseau + groupeurs annulation
        return $this->base_64
            ->select('string_agg('.$this->tb_pli.'.id_pli::text,\',\')::text as id_pli,saisie_par,typologie')
            ->from($this->tb_pli)
            ->join($this->vw_dataPli, $this->tb_pli.'.id_pli = '.$this->vw_dataPli.'.id_pli')
            //->where('id_decoupage IS NULL',null,false)
            ->where_in($this->tb_pli.'.flag_traitement',array(5,6))
            ->where_in($this->vw_dataPli.'.statut_saisie', array(1))
            ->where_in($this->tb_pli.'.typologie', array(6,7,8,9,10,11,12,13,14,15,17,18,30,31)) // ajout 17,18 Institutionnel dans le groupe le 18/11/2019
            ->group_by(array('saisie_par','typologie'))
            ->get()
            ->result();
    }

    public function get_pli_to_cut_autres(){ // plis pour autres catégories
        return $this->base_64
            ->select('string_agg('.$this->tb_pli.'.id_pli::text,\',\')::text as id_pli,saisie_par,typologie')
            ->from($this->tb_pli)
            ->join($this->vw_dataPli, $this->tb_pli.'.id_pli = '.$this->vw_dataPli.'.id_pli')
            //->where('id_decoupage IS NULL',null,false)
            ->where_in($this->tb_pli.'.flag_traitement',array(5,6))
            ->where_in($this->vw_dataPli.'.statut_saisie', array(1))
            ->where_not_in($this->tb_pli.'.typologie', array(6,7,8,9,10,11,12,13,14,15,17,18,30,31))
            ->group_by(array('saisie_par','typologie'))
            ->get()
            ->result();
    }

    public function update_decoupage_in_pli($taux){
        $list_pli_cut = null;
        if($taux == 1) $list_pli_cut = $this->get_pli_to_cut_crga();
        if($taux == 3) $list_pli_cut = $this->get_pli_to_cut_autres();

        $list_plis = array();

        $this->base_64->trans_begin();

        foreach($list_pli_cut as $pli){
            $id_decoupage = $this->init_dt_decoupage($taux);
            $tab_id       = explode(',',$pli->id_pli);
            $data = array(
                'id_decoupage' => $id_decoupage,
                'flag_traitement' => 7,
                'in_decoup' => 1
            );
            /*echo $id_decoupage.'<br>';
            var_dump($tab_id);
            echo 'ten percent ('.$taux.'%):<br>';*/

            $this->base_64->where_in('id_pli',$tab_id)
                            ->update($this->tb_pli,$data);

            $this->ged->where_in('id_pli',$tab_id)
                            ->update($this->tb_dataPli,array('flag_traitement' => 7));
            $ten_percent = $this->take_ten_percent($tab_id);
            /*var_dump($ten_percent);
            echo '<br>';*/
            $this->base_64->where_in('id_pli',$ten_percent)
                            ->update($this->tb_pli,array('flag_echantillon'=>1));
            $list_plis = array_merge($list_plis,$tab_id); // historisation des plis
        }

        if($this->base_64->trans_status() === FALSE){
            $this->base_64->trans_rollback();
            return 'Découpage échoué';
        }
        else{
            $this->base_64->trans_commit();
            $this->CI->histo->plis($list_plis);
            /*$this->CI->histo->plis($tab_id_sample);*/
            return '';
        }

    }

    //************Echantillon***********//

    public function take_ten_percent($pli_cut){
        $nb_pli_cut  = count($pli_cut);
        if($nb_pli_cut <= 10){
            $nb_echantillon = 1;
        }
        else{
            $nb_echantillon = round($nb_pli_cut*10/100);
        }
        $rand_keys = (array)array_rand($pli_cut,$nb_echantillon);

        $arr_pli_sample = array();

        for($i = 0; $i < count($rand_keys); $i++){
            array_push($arr_pli_sample, $pli_cut[$rand_keys[$i]]);
        }

        return $arr_pli_sample;

    }

    public function take_all_percent($id_decoupage){
        $pli_cut = $this->base_64
            ->select('id_pli')
            ->from($this->tb_pli)
            ->where('id_decoupage', $id_decoupage)
            ->get()
            ->result();

        //$nb_pli_cut  = count($pli_cut);

        $arr_pli_sample = array();

        foreach($pli_cut as $pli){
            array_push($arr_pli_sample,$pli->id_pli);
        }

        return $arr_pli_sample;

    }

    public function verif_heure(){
        $sql = "SELECT id_decoupage, EXTRACT(EPOCH FROM (now() - date_decoupage ))/3600 as diff_time_hours, verrou 
                FROM decoupage 
                WHERE id_decoupage = (SELECT MAX(id_decoupage) FROM decoupage)";

        $result = $this->base_64->query($sql)->result();

        if(($result[0]->verrou == 0) && ($result[0]->diff_time_hours >= 1)){
            $this->base_64->where('id_decoupage', $result[0]->id_decoupage)
                            ->update('decoupage', array('verrou' => 1));

            return true;
        }
        else{
            return false;
        }
    }

}

