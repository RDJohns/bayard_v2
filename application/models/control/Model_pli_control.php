<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pli_control extends CI_Model{

	private $CI;

	private $ged;
	private $base_64;

	private $tb_pli = TB_pli;
	private $tb_soc = TB_soc;
	private $tb_user = TB_user;
	private $tb_titre = TB_titre;
	private $tb_cheque = TB_cheque;
	private $tb_tpUser = TB_tpUser;
	private $tb_categEr = TB_categEr;
	private $tb_lstCirc = TB_lstCirc;
	private $tb_paiement = TB_paiement;
	private $vw_grPliPos = Vw_grPliPos;
	private $tb_lotSaisie = TB_lotSaisie;
	private $tb_mdPaie = TB_mode_paiement;
	private $tb_assignation = TB_assignation;
	private $tb_groupe_pli = TB_groupe_pli;
	private $tb_data_pli = TB_data_pli;
	private $tb_anomChq = TB_anomChq;
	private $tb_motifKo = TB_motifKo;
	private $tb_grPaie = TB_grPaie;
	private $tb_grUser = TB_grUser;
	private $tb_doc = TB_document;
	private $tb_statS = TB_statS;
	private $tb_mvmnt = TB_mvmnt;
	private $tb_typo = TB_typo;
	private $vw_pli = Vw_pli;
	private $vw_lotN = Vw_lotNum;
	private $ftb_pli = FTB_pli;
	private $ftb_lotNum = FTB_lotNumerisation;
	private $tb_assignationPli = TB_assignationPli;

	public function __construct() {
		parent::__construct();
		$this->CI =& get_instance();
		$this->ged = $this->load->database('ged', TRUE);
		$this->base_64 = $this->load->database('base_64', TRUE);
	}

	public function piocher_pli($id_user, $filtre){
		$pli = NULL;
		if (!is_numeric($id_user)) {
			return $pli;
		}
		$pli = $this->current_pli($id_user, $filtre);
		if(is_null($pli)){
			$this->clear_current_pli($id_user);
			$pli = $this->get_pli($id_user, $filtre);
			if(!is_null($pli)){
				$this->CI->histo->action(15, '', $pli->id_pli);
			}
		}else {
            $this->CI->histo->action(18, '', $pli->id_pli);
		}
		return $pli;
	}

	private function filtre_ctrl($filtre_ctrl){
		$typo = isset($filtre_ctrl['typo']) ? $filtre_ctrl['typo'] : -1;
		$user = isset($filtre_ctrl['user']) ? $filtre_ctrl['user'] : -1;
		if($typo != '-1' && is_numeric($typo)){
			$this->ged->where('tb_typo.id', $typo);
		}
		if($user != '-1' && is_numeric($user)){
			$this->ged->where('tb_pli.saisie_par', $user);
		}
	}

	private function current_pli($id_user, $filtre_ctrl){
		$this->filtre_ctrl($filtre_ctrl);
		$data = $this->ged
			->select('tb_pli.*')
			->select('tblotN.lot_scan')
			->select('tb_soc.nom_societe soc')
			->select('tb_typo.typologie typo')
			->select('tb_grp.commentaire_groupe_paiement grpaie')
			->select('tblotN.date_courrier')
			->from($this->ftb_pli.' tb_pli')
			->join($this->tb_typo.' tb_typo', 'tb_typo.id=tb_pli.typologie', 'INNER')
			->join($this->tb_paiement.' tb_paie', 'tb_paie.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_mdPaie.' tb_mdp', 'tb_mdp.id_mode_paiement=tb_paie.id_mode_paiement', 'INNER')
			->join($this->tb_grPaie.' tb_grp', 'tb_grp.id_groupe_paiement=tb_mdp.id_groupe_paiement', 'INNER')
			->join($this->tb_data_pli.' tb_data', 'tb_data.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_soc.' tb_soc', 'tb_soc.id=tb_data.societe', 'INNER')
			->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
			->where('tb_pli.flag_traitement', 8)
			->where('tb_data.statut_saisie', 1)
			->where('controle_par', $id_user)
			->where('recommande != -1 ', NULL, FALSE)
			->order_by('recommande', 'DESC')
			->order_by('tblotN.date_courrier', 'ASC')
			->order_by('id_decoupage')
			->order_by('id_pli')
			->order_by('tb_grp.id_groupe_paiement')
			->limit(1)
			->get()->result();
		return count($data) > 0 ? $data[0] : NULL;
	}

	private function get_pli($id_user, $filtre_ctrl){
		$reserved_plis = $this->ged
            ->select('id_pli')
            ->from($this->tb_assignationPli)
            ->where('operateur != ', $id_user)
            ->where('traitement', 7)
            ->get_compiled_select();
        $this->ged->reset_query();
        $plis_for_me = $this->ged
            ->select('id_pli id_pli_for_me')
            ->select('operateur me')
            ->from($this->tb_assignationPli)
            ->where('operateur', $id_user)
            ->where('traitement', 7)
            ->get_compiled_select();
        $this->ged->reset_query();
		$this->filtre_ctrl($filtre_ctrl);
		$data = $this->ged
			->select('tb_pli.*')
			->select('tblotN.lot_scan')
			->select('tb_soc.nom_societe soc')
			->select('tb_typo.typologie typo')
			->select('tb_grp.commentaire_groupe_paiement grpaie')
			->select('tblotN.date_courrier')
			->from($this->ftb_pli.' tb_pli')
			->join($this->tb_typo.' tb_typo', 'tb_typo.id=tb_pli.typologie', 'INNER')
			->join($this->tb_paiement.' tb_paie', 'tb_paie.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_mdPaie.' tb_mdp', 'tb_mdp.id_mode_paiement=tb_paie.id_mode_paiement', 'INNER')
			->join($this->tb_grPaie.' tb_grp', 'tb_grp.id_groupe_paiement=tb_mdp.id_groupe_paiement', 'INNER')
			->join($this->tb_data_pli.' tb_data', 'tb_data.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_soc.' tb_soc', 'tb_soc.id=tb_data.societe', 'INNER')
			->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
			->join('('.$plis_for_me.') tb_pli_for_me', 'id_pli_for_me = tb_pli.id_pli', 'LEFT')
			->where('tb_pli.flag_traitement', 7)
			->where('flag_echantillon', 1)
			->where('tb_data.statut_saisie', 1)
			->where('recommande != -1 ', NULL, FALSE)
			->where(' tb_pli.id_pli NOT IN ('.$reserved_plis.') ', NULL, FALSE)
            ->order_by('COALESCE(me, 0) DESC')
			->order_by('recommande', 'DESC')
			->order_by('tblotN.date_courrier', 'ASC')
			->order_by('id_decoupage')
			->order_by('id_pli')
			->order_by('tb_grp.id_groupe_paiement')
			->limit(1)
			->get()->result();
		if (count($data) > 0) {
			$pli = $data[0];
			return $this->locked_pli($id_user, $pli->id_pli) ? $pli : $this->get_pli($id_user, $filtre_ctrl);
		} else {
			return NULL;
		}
	}

	private function locked_pli($id_user, $id_pli){
		if(!is_numeric($id_pli)){
			return FALSE;
		}
		$data = array(
			'flag_traitement' => 8
			,'controle_par' => $id_user
		);
		$data_datapli = array(
            'flag_traitement' => 8
		);
		$this->ged->trans_begin();
        $this->base_64->trans_begin();
		$this->base_64
			->where('id_pli', $id_pli)
			->where('flag_traitement', 7)
			->set($data)
			->update($this->tb_pli);
		$this->ged
			->where('id_pli', $id_pli)
			->where('statut_saisie', 1)
            ->set($data_datapli)
            ->update($this->tb_data_pli);
		if($this->base_64->affected_rows() < 1 || $this->ged->affected_rows() < 1 || $this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
			$this->ged->trans_rollback();
			$this->base_64->trans_rollback();
			return FALSE;
		}else{
			$this->ged->trans_commit();
			$this->base_64->trans_commit();
			return TRUE;
		}
	}
	
	private function clear_current_pli($id_user){
		$plis = $this->base_64
			->from($this->tb_pli)
			->where('flag_traitement', 8)
			->where('controle_par', $id_user)
			->get()->result();
		foreach ($plis as $key => $pli) {
			$this->unlock_auto($id_user, $pli->id_pli);
		}
	}

	private function unlock_auto($id_user, $id_pli){
		$data_modif_pli = array(
            'controle_par' => NULL
            ,'flag_traitement' => 7
		);
		$data_datapli = array(
            'flag_traitement' => 7
		);
		$this->ged->trans_begin();
        $this->base_64->trans_begin();
        $this->base_64
			->where('id_pli', $id_pli)
			->where('flag_traitement', 8)
			->where('controle_par', $id_user)
            ->set($data_modif_pli)
			->update($this->tb_pli);
		$this->ged
			->where('id_pli', $id_pli)
			->where('statut_saisie', 1)
            ->set($data_datapli)
            ->update($this->tb_data_pli);
        $modification = $this->base_64->affected_rows() + $this->ged->affected_rows();
		if($modification != 2 || $this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'control> model_pli_control> unlock_auto> deverrouilage automatique controle de pli#'.$id_pli.' produisant '.$modification.' modification =>'.$this->base_64->last_query());
            return FALSE;
        }else {
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            $this->CI->histo->action(26, 'deverrouilage AUTO', $id_pli);
            $this->CI->histo->pli($id_pli);
            return TRUE;
        }
	}

	public function categorie_erreur(){
		return $this->ged
            ->from($this->tb_categEr)
            ->order_by('erreur')
            ->where('actif', 1)
            ->get()->result();
	}

	public function unlocked($id_pli){
		$data_modif_pli = array(
            'controle_par' => NULL
            ,'flag_traitement' => 7
		);
		$data_datapli = array(
            'flag_traitement' => 7
		);
		$this->ged->trans_begin();
        $this->base_64->trans_begin();
        $this->base_64
            ->where('id_pli', $id_pli)
            ->where('flag_traitement', 8)
            ->set($data_modif_pli)
			->update($this->tb_pli);
		$this->ged
			->where('id_pli', $id_pli)
			->where('statut_saisie', 1)
            ->set($data_datapli)
            ->update($this->tb_data_pli);
        $modification = $this->base_64->affected_rows() + $this->ged->affected_rows();
		if($modification != 2 || $this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'control> model_pli_control> unlocked> deverrouilage control de pli#'.$id_pli.' produisant '.$modification.' modification =>'.$this->base_64->last_query());
            return FALSE;
        }else {
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            $this->CI->histo->action(26, '', $id_pli);
            $this->CI->histo->pli($id_pli);
            return TRUE;
        }
	}

	public function get_data_filtre(){
		$flag_traitement_cible = array(7/*, 8*/);
		$id_user = $this->CI->session->id_utilisateur;
		$reponse = new stdClass();
		$reponse->typos = $this->ged
			->select('tbtypo.id, tbtypo.typologie')
			->select('COUNT(id_pli) nb', FALSE)
			->from($this->tb_typo.' tbtypo')
			->join($this->ftb_pli.' tb_pli', 'tbtypo.id = tb_pli.typologie', 'LEFT')
			->where('actif', 1)
			->where('flag_echantillon', 1)
			->group_start()
				->where_in('flag_traitement', $flag_traitement_cible)
				->or_group_start()
					->where_in('flag_traitement', array(8))
					->where('controle_par', $id_user)
				->group_end()
			->group_end()
			->group_by('tbtypo.id, tbtypo.typologie')
			->order_by('tbtypo.typologie')
			->get()->result();
		$reponse->users = $this->ged
			->distinct()
			->select('tbu.*')
			->from($this->tb_user.' tbu')
			->join($this->ftb_pli.' tb_pli', 'tbu.id_utilisateur = tb_pli.saisie_par', 'INNER')
			->where('id_type_utilisateur', 2)
			->where('flag_echantillon', 1)
			->group_start()
				->where_in('flag_traitement', $flag_traitement_cible)
				->or_group_start()
					->where_in('flag_traitement', array(8))
					->where('controle_par', $id_user)
				->group_end()
			->group_end()
			->order_by('login')
			->get()->result();
		$reponse->typos = count($reponse->typos) > 0 ? $reponse->typos : array();
		$reponse->users = count($reponse->users) > 0 ? $reponse->users : array();
		return $reponse;
	}
	
}
