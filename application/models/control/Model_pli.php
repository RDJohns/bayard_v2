<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pli extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    
    private $tb_pli = TB_pli;
    private $tb_soc = TB_soc;
    private $tb_user = TB_user;
    private $tb_titre = TB_titre;
    private $tb_cheque = TB_cheque;
    private $tb_chequeAnoCorrige = TB_chequeAnoCorrige;
    private $tb_etatChq = TB_etatChq;
    private $tb_paieChqAnomali = TB_paieChqAnom;
    private $tb_histoChq = TB_histoChq;
    private $tb_tpUser = TB_tpUser;
    private $tb_categEr = TB_categEr;
    private $tb_ctrlPliEr = TB_ctrlPliEr;
    private $tb_lstCirc = TB_lstCirc;
    private $tb_paiement = TB_paiement;
    private $vw_grPliPos = Vw_grPliPos;
    private $tb_lotSaisie = TB_lotSaisie;
    private $tb_mdPaie = TB_mode_paiement;
    private $tb_assignation = TB_assignation;
    private $tb_groupe_pli = TB_groupe_pli;
    private $tb_data_pli = TB_data_pli;
    private $tb_decoup = TB_decoupage;
    private $tb_anomChq = TB_anomChq;
    private $tb_motifKo = TB_motifKo;
    private $tb_grUser = TB_grUser;
    private $tb_doc = TB_document;
    private $tb_statS = TB_statS;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_typo = TB_typo;
    private $vw_pli = Vw_pli;
    private $tb_trace = TB_trace;
    private $vw_lotN = Vw_lotNum;
    private $ftb_pli = FTB_pli;
	private $ftb_lotNum = FTB_lotNumerisation;
	private $tb_chqKd = TB_chqKd;
	private $tb_paieChqKd = TB_paieChqKd;
	private $tb_paieEsp = TB_paieEsp;
    private $ftb_data_pli = FTB_data_pli;
    private $tb_cba = TB_cba;
    private $tb_saisieCtrlField = TB_saisiCtrlField;
    private $tb_motifConsigne = TB_motifConsigne;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function documents($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->base_64
            ->from($this->tb_doc)
            ->where('id_pli', $id_pli)
            ->order_by('id_document')
            ->get()->result();
    }
    
    public function id_documents($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->base_64
            ->select('id_document')
            ->from($this->tb_doc)
            ->where('id_pli', $id_pli)
            ->order_by('id_document')
            ->get()->result();
    }

    public function document($id_doc){
        return $this->base_64
            ->from($this->tb_doc)
            ->where('id_document', $id_doc)
            ->get()->result();
    }

    public function societe(){
        return $this->ged
            ->from($this->tb_soc)
            ->where('actif', 1)
            ->order_by('nom_societe')
            ->get()->result();
    }

    public function typologie($id_soc=1){
        $id_soc = is_numeric($id_soc) ? $id_soc : 1;
        return $this->ged
            ->distinct()
            ->select($this->tb_typo.'.*')
            ->from($this->vw_grPliPos)
            ->join($this->tb_typo, 'id_typologie = id', 'INNER')
            ->order_by($this->tb_typo.'.typologie')
            ->where('id_soc', $id_soc)
            ->get()->result();
    }

    public function titre($id_soc=NULL, $titre_i=FALSE){
        if(is_numeric($id_soc)){
            $this->ged->where('societe_id', $id_soc);
        }
        if ($titre_i) {
            $this->ged->where('titre_i', 1);
        }
        return $this->ged
            ->from($this->tb_titre)
            ->order_by('titre')
            ->where('actif', 1)
            ->get()->result();
    }

    public function mode_paiement(){
        return $this->ged
            ->from($this->tb_mdPaie)
            ->order_by('mode_paiement')
            ->where('actif', 1)
            ->get()->result();
    }

    public function anom_cheque(){
        return $this->ged
            ->from($this->tb_anomChq)
            ->order_by('anomalie')
            ->where('flag', 1)
            ->get()->result();
    }

    public function anom_cheque_tab(){
        $data = $this->ged
            ->from($this->tb_anomChq.' tb_anom')
            ->join($this->tb_etatChq.' tb_stat', 'tb_anom.id_etat_chq = tb_stat.id_etat_chq', 'INNER')
            ->where('tb_anom.flag', 1)
            ->select('tb_anom.id')
            ->select('tb_anom.id_etat_chq')
            ->select('tb_stat.priorite prio_stat')
            ->select('tb_anom.priorite prio_anom')
            ->select('tb_anom.etat_pli')
            ->select('tb_anom.anomalie label_anom')
            ->get()->result();
        $rep = array();
        foreach ($data as $key => $value) {
            $rep[$value->id] = $value;
        }
        return $rep;
    }
    
    public function anom_cheque_etat(){
        $data = $this->ged
            ->from($this->tb_anomChq)
            ->order_by('id')
            ->get()->result();
        $rep = array();
        foreach ($data as $key => $value) {
            $rep[$value->id] = $value->id_etat_chq;
        }
        return $rep;
    }

    public function cheque_etat_tab(){
        $data = $this->ged
            ->from($this->tb_etatChq)
            ->get()->result();
        $rep = array();
        foreach ($data as $key => $value) {
            $rep[$value->id_etat_chq] = $value;
        }
        return $rep;
    }
    
    public function paiement_anomali($id_doc)
    {
        if(!is_numeric($id_doc)){
            return array();
        }
        $data = $this->ged
            ->from($this->tb_paieChqAnomali)
            ->where('id_doc', $id_doc)
            ->get()->result();
        $rep = array();
        foreach ($data as $key => $value) {
            array_push($rep, $value->id_anomalie_chq);
        }
        return $rep;
    }

    public function paiement_chq($id_doc)
    {
        if(!is_numeric($id_doc)){
            return NULL;
        }
        $data = $this->ged
            ->from($this->tb_cheque)
            ->where('id_doc', $id_doc)
            ->limit(1)
            ->get()->result();
        return count($data) == 1 ? $data[0] : NULL;
    }

    public function paiement_chq_ano_corrige($id_doc)
    {
        if(!is_numeric($id_doc)){
            return array();
        }
        return $this->ged
            ->from($this->tb_chequeAnoCorrige)
            ->where('id_doc', $id_doc)
            ->get()->result();
    }

    public function type_chq_kd(){
        return $this->ged
            ->from($this->tb_chqKd)
            ->order_by('type_cheque_cadeau')
            ->where('actif', 1)
            ->get()->result();
    }

    public function mvmnt_of_pli($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->ged
            ->from($this->tb_mvmnt)
            ->where('id_pli', $id_pli)
            ->order_by('id')
            ->get()->result();
    }
    
    public function statut_retraitement($data_pli = NULL){
        if(isset($data_pli->flag_batch) && $data_pli->flag_batch == '1'){
            $this->ged->where_not_in('id_statut_saisie', array(11));
        }
        return $this->ged
            ->from($this->tb_statS)
            ->order_by('id_statut_saisie')
            ->where('actif', 1)
            ->where('retraitement', 1)
            ->get()->result();
    }

    public function statut_control($data_pli = NULL){
        if(isset($data_pli->flag_batch) && $data_pli->flag_batch == '1'){
            $this->ged->where_not_in('id_statut_saisie', array(11));
        }
        return $this->ged
            ->from($this->tb_statS)
            ->order_by('id_statut_saisie')
            ->where('actif', 1)
            ->where('control', 1)
            ->get()->result();
    }

    public function motif_ko(){
        return $this->ged
            ->from($this->tb_motifKo)
            ->order_by('libelle_motif')
            ->where('actif', 1)
            ->get()->result();
    }

    public function categorie_erreur(){
        return $this->ged
            ->from($this->tb_categEr)
            ->order_by('erreur')
            ->where('actif', 1)
            ->get()->result();
    }

    public function liste_circulaires(){
        return $this->ged
            ->from($this->tb_lstCirc)
            ->order_by('circulaire')
            ->where('actif', 1)
            ->get()->result();
    }

    public function data_pli_only($id_pli){
        if(!is_numeric($id_pli)){
            return NULL;
        }
        $data_pli = $this->ged
            ->from($this->tb_data_pli)
            ->where('id_pli', $id_pli)
            ->get()->result();
        return count($data_pli) > 0 ? $data_pli[0] : NULL;
    }

    public function data_pli($id_pli){
        if(!is_numeric($id_pli)){
            return NULL;
        }
        $data_pli = $this->ged
            ->select('tbdata.*', FALSE)
            ->select('pli')
            ->select('lot_scan')
            ->from($this->tb_data_pli.' tbdata')
            ->join($this->ftb_pli.' tb_pli', 'tb_pli.id_pli = tbdata.id_pli', 'LEFT')
            ->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
            ->where('tbdata.id_pli', $id_pli)
            ->get()->result();
        return count($data_pli) > 0 ? $data_pli[0] : NULL;
    }

    public function my_paiement($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        $data = $this->ged->from($this->tb_paiement)->where('id_pli', $id_pli)->get()->result();
        $rep = array();
        foreach ($data as $paie) {
            array_push($rep, $paie->id_mode_paiement);
        }
        return $rep;
    }

    public function paie_esp($id_pli){
        if(is_numeric($id_pli)){
            $data = $this->ged->from($this->tb_paieEsp)->where('id_pli', $id_pli)->get()->result();
            return count($data) > 0 ? $data[0]->montant : '0';
        }
        return '0';
    }

    public function cheques($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->ged->from($this->tb_cheque)->where('id_pli', $id_pli)->order_by('id_doc')->get()->result();
    }

    public function cheques_kd($id_pli){
        if(!is_numeric($id_pli)){
            return array();
        }
        return $this->ged->from($this->tb_paieChqKd)->where('id_pli', $id_pli)->order_by('id_doc')->get()->result();
    }

    public function pli($id_pli){
        if(is_numeric($id_pli)){
            $pli = $this->base_64
                ->from($this->tb_pli)
                ->where('id_pli', $id_pli)
                ->get()->result();
            return count($pli) > 0 ? $pli[0] : NULL;
        }
        return NULL;
    }
    
    public function save($data_ctrl){
        $id_pli = $data_ctrl->id_pli;
        $pli = $this->pli($id_pli);
        if(is_null($pli)){
            return array(FALSE, 'Pli introuvable!');
        }
        $responsable = $data_ctrl->is_control ? $pli->controle_par : $pli->retraite_par;
        if($responsable != $data_ctrl->op || $pli->flag_traitement != $data_ctrl->old_flg){
            return array(FALSE, 'Ce pli ne vous est plus assigné!');
        }
        if($pli->recommande == -1){
            return array(FALSE, 'Ce pli est en Stand-by!');
        }
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        try {
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_paiement);
            if (count($data_ctrl->paiements) > 0) {
                $this->ged->insert_batch($this->tb_paiement, $data_ctrl->paiements);
            }
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieEsp);
            if (count($data_ctrl->paie_esp) > 0) {
                $this->ged->insert($this->tb_paieEsp, $data_ctrl->paie_esp);
            }
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_mvmnt);
            if (count($data_ctrl->mvmnts) > 0) {
                $this->ged->insert_batch($this->tb_mvmnt, $data_ctrl->mvmnts);
            }
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_cheque);
            if (count($data_ctrl->cheques) > 0) {
                $this->ged->insert_batch($this->tb_cheque, $data_ctrl->cheques);
            }
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqAnomali);
            if (count($data_ctrl->cheques_anomalies) > 0) {
                $this->ged->insert_batch($this->tb_paieChqAnomali, $data_ctrl->cheques_anomalies);
            }
            if (count($data_ctrl->histo_cheque) > 0) {
                $this->ged->insert_batch($this->tb_histoChq, $data_ctrl->histo_cheque);
            }
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqKd);
            if (count($data_ctrl->cheques_cadeaux) > 0) {
                $this->ged->insert_batch($this->tb_paieChqKd, $data_ctrl->cheques_cadeaux);
            }
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_saisieCtrlField);
            if (count($data_ctrl->data_saisie_ctrl) > 0) {
                $this->ged->insert_batch($this->tb_saisieCtrlField, $data_ctrl->data_saisie_ctrl);
            }
            if($data_ctrl->saisie_batch){
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_cba);
                $this->ged->insert_batch($this->tb_cba, $data_ctrl->data_to_cba);
            }
            $this->ged->set($data_ctrl->data_pli)->set('ke_prio', 0)->where('id_pli', $id_pli)->update($this->tb_data_pli);
            $this->base_64->set('desarchive', 0)->where('id_pli', $id_pli)->update($this->tb_doc);
            $this->ged->where('id_pli', $id_pli)->delete($this->tb_ctrlPliEr);
            if(count($data_ctrl->controle_pli_erreur) > 0) {
                $this->ged->insert_batch($this->tb_ctrlPliEr, $data_ctrl->controle_pli_erreur);
            }
            //$this->ged->where('id_pli', $id_pli)->delete($this->tb_motifConsigne);
            if(!is_null($data_ctrl->motif_consigne)) {
                $this->ged->insert($this->tb_motifConsigne, $data_ctrl->motif_consigne);
            }
            if(is_array($data_ctrl->img_dezip) && count($data_ctrl->img_dezip) > 0){
                $this->base_64
                    ->set('desarchive', 1)
                    ->where('id_pli', $id_pli)
                    ->where_in('id_document', $data_ctrl->img_dezip)
                    ->update($this->tb_doc);
            }
            $this->base_64
                ->set($data_ctrl->pli)
                ->set('date_controle', 'NOW()', FALSE)
                ->where('id_pli', $id_pli)
                ->update($this->tb_pli);
        } catch (Exception $exc) {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> control> model_pli> save: '.$exc->getMessage());
            return array(FALSE, 'Erreur serveur!');
        }
        if($this->ged->trans_status() && $this->base_64->trans_status()){
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            return array(TRUE, '');
        }else{
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            return array(FALSE, 'Enregistrement échoué!');
        }
    }

    private function decoupage_active(){//return id_decoup,taux,date,nb_pop des decoupages sans echantillon non controlé mais avec pop90 en attente
        $sql_sub = $this->base_64->distinct()
            ->from($this->tb_pli)
			->select('id_decoupage')
			->where('flag_echantillon', 1)//echantillons
            ->where_in('flag_traitement', array(7, 8))//en attente ou en cours de controle
            ->where('statut_saisie', 1)
			->get_compiled_select();
		$this->base_64->reset_query();
        $sql_sub_pop = $this->base_64
            ->from($this->tb_pli)
			->select('id_decoupage')
            ->select('COUNT(id_pli) nb_pop')
            ->group_by('id_decoupage')
			->get_compiled_select();//nb total population/decoupage
		$this->base_64->reset_query();
        return $this->base_64->distinct()
            ->select('tb_dec.id_decoupage id_dec')
            ->select('date_decoupage')
            ->select('tb_dec.taux taux')
            ->select('nb_pop')
            ->from($this->tb_decoup.' tb_dec')
            ->join($this->tb_pli.' tb_pli', 'tb_dec.id_decoupage = tb_pli.id_decoupage', 'INNER')
            ->join('('.$sql_sub_pop.') sub_pop', 'tb_dec.id_decoupage = sub_pop.id_decoupage', 'INNER')
            ->where('flag_traitement', 7)//en attente de controle
            ->where('flag_echantillon', 0)//population non echantillon
            ->where('tb_dec.id_decoupage NOT IN ('.$sql_sub.')', NULL, FALSE)
            ->order_by('date_decoupage')
            ->get()->result();
    }

    private function my90($id_decoup){//:id_pli in array
        $plis = $this->base_64
            ->from($this->tb_pli)
            ->where('id_decoupage', $id_decoup)
            ->where('flag_traitement', 7)
            ->where('flag_echantillon', 0)
            ->get()->result();
        $tab_id_pli = array();
        foreach ($plis as $pli) {
            array_push($tab_id_pli, $pli->id_pli);
        }
        return $tab_id_pli;
    }

    private function nb_erreur($id_decoup){
        return $this->ged
            ->distinct()
            ->select('id_pli')
            ->from($this->tb_ctrlPliEr)
            ->where('id_decoupage', $id_decoup)
            ->count_all_results();
    }

    public function verif_control(){
        $decs = $this->decoupage_active();
        foreach ($decs as $dec) {
            $flg = 9;//control ok
            //$action_ctrl = 21;
            $seuil = $dec->taux;//3% ou 1% selon le cas
            if(!is_numeric($seuil)){
                log_message('error', 'models> control> model_pli> verif_control: seuil d\'erreur non numerique >'.$seuil.'< id_decoupage#'.$dec->id_dec);
                continue;
            }
            $prc_faute = $this->nb_erreur($dec->id_dec) * 100 / $dec->nb_pop;
            if($prc_faute > $seuil){
                $flg = 11;//ko echantillon -> retraitement
                //$action_ctrl = 8;
            }
            $id_pli90 = $this->my90($dec->id_dec);
            if(count($id_pli90) > 0){
                $this->base_64->trans_begin();
                $this->ged->trans_begin();
                /*if($flg == 9){
                    $this->ged->set('statut_saisie', 1);
                }else{
                    $this->base_64->set('sorti_saisie', $flg);
                    $this->ged->set('statut_saisie', 2);
                }*/
                $this->base_64
                    ->where_in('id_pli', $id_pli90)
                    ->set('flag_traitement', $flg)
                    ->update($this->tb_pli);
                /*$this->base_64
                    ->where('id_decoupage', $dec->id_dec)
                    ->set('in_decoup', 0)
                    ->update($this->tb_pli);*/
                $this->ged
                    ->where_in('id_pli', $id_pli90)
                    ->set('flag_traitement', $flg)
                    ->update($this->tb_data_pli);
                if($this->base_64->trans_status() === FALSE || $this->ged->trans_status() === FALSE){
                    $this->ged->trans_rollback();
                    $this->base_64->trans_rollback();
                    log_message('error', 'application 90% pli apres controle echantillon echoue, id_decoup#'.$dec->id_dec);
                }else{
                    $this->base_64->trans_commit();
                    $this->ged->trans_commit();
                    $this->CI->histo->plis($id_pli90, 'controle faute: '.$prc_faute.'% sur '.$seuil.'% id_decoup#'.$dec->id_dec);
                    log_message('info', 'controle termine id_decoup#'.$dec->id_dec.' avec '.$prc_faute.'% sur '.$seuil.'% -> flg:'.$flg);
                    //$this->CI->histo->action_plis($action_ctrl, 'automatique apres controle echantillon', $id_pli90);
                }
            }
        }
    }

    public function population(){
        $sql_sub_control_dispo = $this->base_64
            ->select('COUNT(id_pli) nb_ctrl_dispo', FALSE)
            ->from($this->tb_pli)
            ->where('flag_traitement', 7)
            ->where('flag_echantillon', 1)
            ->where('statut_saisie', 1)
			->get_compiled_select();
		$this->base_64->reset_query();
        /*$sql_sub_validation_dispo = $this->base_64
            ->select('COUNT(id_pli) nb_valid_dispo', FALSE)
            ->from($this->tb_pli)
            ->where_in('flag_traitement', array(17, 23))
			->get_compiled_select();
		$this->base_64->reset_query();*/
        $sql_sub_retraitement_dispo = $this->base_64
            ->select('COUNT(tbpli.id_pli) nb_retr_dispo', FALSE)
            ->from($this->tb_pli.' tbpli')
            ->join($this->ftb_data_pli.' tbdata', 'tbdata.id_pli = tbpli.id_pli', 'INNER')
            ->where('tbpli.flag_traitement', 11)
            ->where('tbdata.statut_saisie', 1)
            // ->where('ke_prio', 1)
			->get_compiled_select();
        $this->base_64->reset_query();
        // $sql_sub_retraitement_total = $this->base_64
        //     ->select('COUNT(id_pli) nb_retr_tot', FALSE)
        //     ->from($this->tb_pli)
        //     ->where_in('flag_traitement',  array(17, 13))
        //     ->where('statut_saisie', 1)
		// 	->get_compiled_select();
        // $this->base_64->reset_query();
        $sql_sub_control_current = $this->base_64
            ->select('COUNT(id_pli) nb_ctrl_current', FALSE)
            ->from($this->tb_pli)
            ->where('flag_traitement', 8)
            ->where('statut_saisie', 1)
			->get_compiled_select();
		$this->base_64->reset_query();
        /*$sql_sub_validation_current = $this->base_64
            ->select('COUNT(id_pli) nb_valid_current', FALSE)
            ->from($this->tb_pli)
            ->where('flag_traitement', 12)
			->get_compiled_select();
		$this->base_64->reset_query();*/
        $sql_sub_retraitement_current = $this->base_64
            ->select('COUNT(id_pli) nb_retr_current', FALSE)
            ->from($this->tb_pli)
            ->where('flag_traitement', 13)
            ->where('statut_saisie', 1)
			->get_compiled_select();
        $this->base_64->reset_query();
        $data = $this->base_64
            ->select('('.$sql_sub_control_dispo.')', FALSE)
            // ->select('('.$sql_sub_validation_dispo.')', FALSE)
            ->select('('.$sql_sub_retraitement_dispo.')', FALSE)
            // ->select('('.$sql_sub_retraitement_total.')', FALSE)
            ->select('('.$sql_sub_control_current.')', FALSE)
            // ->select('('.$sql_sub_validation_current.')', FALSE)
            ->select('('.$sql_sub_retraitement_current.')', FALSE)
            ->get()->result();
        return $data[0];
    }

    public function activity($id_user){
        $sql_sub_sub_ctrl = $this->ged
            ->distinct()
            ->select('id_pli')
            ->from($this->tb_trace)
            ->where('id_user', $id_user)
            ->where('date_action::DATE', date('\'Y-m-d\''), FALSE)
            ->where_in('id_action', array(21, 22, 23, 24, 25, 102, 103))
            ->get_compiled_select();
		$this->ged->reset_query();
        $sql_sub_my_control = $this->ged
            ->select('COUNT(id_pli) my_nb_ctrl', FALSE)
            ->from('('.$sql_sub_sub_ctrl.') sub2')
			->get_compiled_select();
        $this->ged->reset_query();
        /*$sql_sub_sub_val = $this->ged
            ->distinct()
            ->select('id_pli')
            ->from($this->tb_trace)
            ->where('id_user', $id_user)
            ->where('date_action::DATE', date('\'Y-m-d\''), FALSE)
            ->where_in('id_action', array(34, 35, 36, 37, 38))
            ->get_compiled_select();
		$this->ged->reset_query();
        $sql_sub_my_validation = $this->ged
            ->select('COUNT(id_pli) my_nb_valid', FALSE)
            ->from('('.$sql_sub_sub_val.') sub2')
			->get_compiled_select();
        $this->ged->reset_query();*/
        $sql_sub_sub_ret = $this->ged
            ->distinct()
            ->select('id_pli')
            ->from($this->tb_trace)
            ->where('id_user', $id_user)
            ->where('date_action::DATE', date('\'Y-m-d\''), FALSE)
            ->where_in('id_action', array(29, 30, 31, 32, 33, 100, 101))
            ->get_compiled_select();
		$this->ged->reset_query();
        $sql_sub_my_retraitement = $this->ged
            ->select('COUNT(id_pli) my_nb_retr', FALSE)
            ->from('('.$sql_sub_sub_ret.') sub2')
			->get_compiled_select();
        $this->ged->reset_query();
        $data = $this->ged
            ->select('('.$sql_sub_my_control.')', FALSE)
            // ->select('('.$sql_sub_my_validation.')', FALSE)
            ->select('('.$sql_sub_my_retraitement.')', FALSE)
            ->get()->result();
        return $data[0];
    }

    public function get_commande_cba_mvmnt($mvmnt_ui_id)
    {
        $data = $this->ged
            ->from($this->tb_cba)
            ->where('mvmnt_ui_id', $mvmnt_ui_id)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }
    
    public function get_motif_consigne($id_pli)
    {
        $data = $this->ged
            ->from($this->tb_motifConsigne)
            ->where('id_pli', $id_pli)
            ->order_by('dt_motif_ko', 'DESC')
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }
    
    public function get_all_motif_consigne($id_pli)
    {
        $data = $this->ged
            ->from($this->tb_motifConsigne)
            ->where('id_pli', $id_pli)
            ->order_by('dt_motif_ko', 'ASC')
            ->get()->result();
        return count($data) > 0 ? $data : array();
    }
    
    public function nb_motif_consigne($id_pli)
    {
        return $this->ged
            ->from($this->tb_motifConsigne)
            ->where('id_pli', $id_pli)
            ->count_all_results();
    }
    
    public function get_label_motif_ko($id_motif)
    {
        if(!is_numeric($id_motif)){
            return '';
        }
        $data = $this->ged
            ->from($this->tb_motifKo)
            ->where('id_motif', $id_motif)
            ->get()->result();
        return count($data) > 0 ? $data[0]->libelle_motif.', ' : '';
    }

}
