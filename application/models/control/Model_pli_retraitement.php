<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pli_retraitement extends CI_Model{

	private $CI;

	private $ged;
	private $base_64;

	private $tb_pli = TB_pli;
	private $tb_soc = TB_soc;
	private $tb_user = TB_user;
	private $tb_titre = TB_titre;
	private $tb_cheque = TB_cheque;
	private $tb_tpUser = TB_tpUser;
	private $tb_categEr = TB_categEr;
	private $tb_lstCirc = TB_lstCirc;
	private $tb_paiement = TB_paiement;
	private $vw_grPliPos = Vw_grPliPos;
	private $tb_lotSaisie = TB_lotSaisie;
	private $tb_mdPaie = TB_mode_paiement;
	private $tb_assignation = TB_assignation;
	private $tb_groupe_pli = TB_groupe_pli;
	private $tb_data_pli = TB_data_pli;
	private $tb_anomChq = TB_anomChq;
	private $tb_motifKo = TB_motifKo;
	private $tb_grPaie = TB_grPaie;
	private $tb_grUser = TB_grUser;
	private $tb_doc = TB_document;
	private $tb_statS = TB_statS;
	private $tb_mvmnt = TB_mvmnt;
	private $tb_typo = TB_typo;
	private $vw_pli = Vw_pli;
	private $vw_lotN = Vw_lotNum;
	private $ftb_pli = FTB_pli;
	private $ftb_lotNum = FTB_lotNumerisation;
	private $tb_dataKe = TB_dataKe;
	private $tb_fichierKe = TB_fichierKe;
	private $tb_assignationPli = TB_assignationPli;

    private $can_treated = array(/*11, 18,*/ 11);

	public function __construct() {
		parent::__construct();
		$this->CI =& get_instance();
		$this->ged = $this->load->database('ged', TRUE);
		$this->base_64 = $this->load->database('base_64', TRUE);
	}

	public function piocher_pli($id_user){
		$pli = NULL;
		if (!is_numeric($id_user)) {
			return $pli;
		}
		$pli = $this->current_pli($id_user);
		if(is_null($pli)){
			$this->clear_current_pli($id_user);
			$pli = $this->get_pli($id_user);
			if(!is_null($pli)){
            	$this->CI->histo->action(17, '', $pli->id_pli);
			}
		}else {
            $this->CI->histo->action(20, '', $pli->id_pli);
		}
		return $pli;
	}

	private function current_pli($id_user){
		$data = $this->ged
			->select('tb_pli.*')
			->select('tblotN.lot_scan')
			->select('tb_soc.nom_societe soc')
			->select('tb_typo.typologie typo')
			->select('tb_grp.commentaire_groupe_paiement grpaie')
			->select('tblotN.date_courrier')
			->from($this->ftb_pli.' tb_pli')
			->join($this->tb_typo.' tb_typo', 'tb_typo.id=tb_pli.typologie', 'INNER')
			->join($this->tb_paiement.' tb_paie', 'tb_paie.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_mdPaie.' tb_mdp', 'tb_mdp.id_mode_paiement=tb_paie.id_mode_paiement', 'INNER')
			->join($this->tb_grPaie.' tb_grp', 'tb_grp.id_groupe_paiement=tb_mdp.id_groupe_paiement', 'INNER')
			->join($this->tb_data_pli.' tb_data', 'tb_data.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_soc.' tb_soc', 'tb_soc.id=tb_data.societe', 'INNER')
			->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
			->where('tb_pli.flag_traitement', 13)
			->where('tb_data.statut_saisie', 1)
			->where('retraite_par', $id_user)
			->where('recommande != -1 ', NULL, FALSE)
			// ->where('ke_prio', 1)
			->order_by('recommande', 'DESC')
			->order_by('tblotN.date_courrier', 'ASC')
			->order_by('id_pli')
			->order_by('tb_grp.id_groupe_paiement')
			->limit(1)
			->get()->result();
		return count($data) > 0 ? $data[0] : NULL;
	}

	private function get_pli($id_user){
		$reserved_plis = $this->ged
            ->select('id_pli')
            ->from($this->tb_assignationPli)
            ->where('operateur != ', $id_user)
            ->where('traitement', 7)
            ->get_compiled_select();
        $this->ged->reset_query();
        $plis_for_me = $this->ged
            ->select('id_pli id_pli_for_me')
            ->select('operateur me')
            ->from($this->tb_assignationPli)
            ->where('operateur', $id_user)
            ->where('traitement', 7)
            ->get_compiled_select();
        $this->ged->reset_query();
		$data = $this->ged
			->select('tb_pli.*')
			->select('tblotN.lot_scan')
			->select('tb_soc.nom_societe soc')
			->select('tb_typo.typologie typo')
			->select('tb_grp.commentaire_groupe_paiement grpaie')
			->select('tblotN.date_courrier')
			->from($this->ftb_pli.' tb_pli')
			->join($this->tb_typo.' tb_typo', 'tb_typo.id=tb_pli.typologie', 'INNER')
			->join($this->tb_paiement.' tb_paie', 'tb_paie.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_mdPaie.' tb_mdp', 'tb_mdp.id_mode_paiement=tb_paie.id_mode_paiement', 'INNER')
			->join($this->tb_grPaie.' tb_grp', 'tb_grp.id_groupe_paiement=tb_mdp.id_groupe_paiement', 'INNER')
			->join($this->tb_data_pli.' tb_data', 'tb_data.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_soc.' tb_soc', 'tb_soc.id=tb_data.societe', 'INNER')
			->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
			->join('('.$plis_for_me.') tb_pli_for_me', 'id_pli_for_me = tb_pli.id_pli', 'LEFT')
			->where_in('tb_pli.flag_traitement', $this->can_treated)
			->where('recommande != -1 ', NULL, FALSE)
			->where('tb_data.statut_saisie', 1)
			// ->where('ke_prio', 1)
			->where(' tb_pli.id_pli NOT IN ('.$reserved_plis.') ', NULL, FALSE)
            ->order_by('COALESCE(me, 0) DESC')
			->order_by('recommande', 'DESC')
			->order_by('tblotN.date_courrier', 'ASC')
			->order_by('id_pli')
			->order_by('tb_grp.id_groupe_paiement')
			->limit(1)
			->get()->result();
		if (count($data) > 0) {
			$pli = $data[0];
			return $this->locked_pli($id_user, $pli->id_pli) ? $pli : $this->get_pli($id_user);
		} else {
			return NULL;
		}
	}

	private function locked_pli($id_user, $id_pli){
		if(!is_numeric($id_pli)){
			return FALSE;
		}
		$data = array(
			'flag_traitement' => 13
			,'retraite_par' => $id_user
		);
		$data_datapli = array(
            'flag_traitement' => 13
		);
		$this->ged->trans_begin();
        $this->base_64->trans_begin();
		$this->base_64
			->where('id_pli', $id_pli)
			->where_in('flag_traitement', $this->can_treated)
			->set($data)
			->update($this->tb_pli);
		$this->ged
			->where('id_pli', $id_pli)
			->where('statut_saisie', 1)
            ->set($data_datapli)
            ->update($this->tb_data_pli);
		if($this->base_64->affected_rows() < 1 || $this->ged->affected_rows() < 1 || $this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
			$this->ged->trans_rollback();
			$this->base_64->trans_rollback();
			return FALSE;
		}else{
			$this->ged->trans_commit();
			$this->base_64->trans_commit();
			return TRUE;
		}
	}

	private function clear_current_pli($id_user){
		$plis = $this->base_64
			->from($this->tb_pli)
			->where('flag_traitement', 13)
			->where('retraite_par', $id_user)
			->get()->result();
		foreach ($plis as $key => $pli) {
			$this->unlock_auto($id_user, $pli->id_pli);
		}
	}

	private function unlock_auto($id_user, $id_pli){
		$data_modif_pli = array(
            'retraite_par' => NULL
            ,'flag_traitement' => 11
		);
		$data_datapli = array(
            'flag_traitement' => 11
		);
		$this->ged->trans_begin();
        $this->base_64->trans_begin();
        $this->base_64
			->where('id_pli', $id_pli)
			->where('flag_traitement', 13)
			->where('retraite_par', $id_user)
            ->set($data_modif_pli)
            ->update($this->tb_pli);
		$this->ged
			->where('id_pli', $id_pli)
			->where('statut_saisie', 1)
			->set($data_datapli)
			->update($this->tb_data_pli);
		$modification = $this->base_64->affected_rows() + $this->ged->affected_rows();
		if($modification != 2 || $this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'control> model_pli_retraitement> unlock_auto> deverrouilage automatique retraitement de pli#'.$id_pli.' produisant '.$modification.' modification =>'.$this->base_64->last_query());
            return FALSE;
        }else {
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            $this->CI->histo->action(28, 'deverrouilage AUTO', $id_pli);
            $this->CI->histo->pli($id_pli);
            return TRUE;
        }
	}
	
	public function unlocked($id_pli){
		$data_modif_pli = array(
            'retraite_par' => NULL
            ,'flag_traitement' => 11
		);
		$data_datapli = array(
            'flag_traitement' => 11
		);
		$this->ged->trans_begin();
        $this->base_64->trans_begin();
        $this->base_64
			->where('id_pli', $id_pli)
			->where('flag_traitement', 13)
            ->set($data_modif_pli)
			->update($this->tb_pli);
		$this->ged
			->where('id_pli', $id_pli)
			->where('statut_saisie', 1)
            ->set($data_datapli)
            ->update($this->tb_data_pli);
        $modification = $this->base_64->affected_rows() + $this->ged->affected_rows();
		if($modification != 2 || $this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'control> model_pli_retraitement> unlocked> deverrouilage retraitement de pli#'.$id_pli.' produisant '.$modification.' modification =>'.$this->base_64->last_query());
            return FALSE;
        }else {
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            $this->CI->histo->action(28, '', $id_pli);
            $this->CI->histo->pli($id_pli);
            return TRUE;
        }
	}
	
	public function info_ke($id_pli){
		$data_ke_raw = $this->ged
			->from($this->tb_dataKe)
			->where('id_pli', $id_pli)
			->limit(1)
			->get()->result();
		$info_ke = new stdClass();
		$info_ke->data_ke = NULL;//count($data_ke_raw) > 0 ? $data_ke_raw[0] : NULL;
		$files = array();//$this->ged->from($this->tb_fichierKe)->where('id_pli', $id_pli)->get()->result();
		$info_ke->nb_fichier_ke = count($files);
		return $info_ke;
	}

	public function files_ke($id_pli){
		return $this->ged
			->select('*')
			->select('oid')
			->from($this->tb_fichierKe)
			->where('id_pli', $id_pli)
			->order_by('nom_orig')
			->get()->result();
	}
	
}
