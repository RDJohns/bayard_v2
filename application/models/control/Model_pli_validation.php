<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pli_validation extends CI_Model{

	private $CI;

	private $ged;
	private $base_64;

	private $tb_pli = TB_pli;
	private $tb_soc = TB_soc;
	private $tb_user = TB_user;
	private $tb_titre = TB_titre;
	private $tb_cheque = TB_cheque;
	private $tb_tpUser = TB_tpUser;
	private $tb_categEr = TB_categEr;
	private $tb_lstCirc = TB_lstCirc;
	private $tb_paiement = TB_paiement;
	private $vw_grPliPos = Vw_grPliPos;
	private $tb_lotSaisie = TB_lotSaisie;
	private $tb_mdPaie = TB_mode_paiement;
	private $tb_assignation = TB_assignation;
	private $tb_groupe_pli = TB_groupe_pli;
	private $tb_data_pli = TB_data_pli;
	private $tb_anomChq = TB_anomChq;
	private $tb_motifKo = TB_motifKo;
	private $tb_grPaie = TB_grPaie;
	private $tb_grUser = TB_grUser;
	private $tb_doc = TB_document;
	private $tb_statS = TB_statS;
	private $tb_mvmnt = TB_mvmnt;
	private $tb_typo = TB_typo;
	private $vw_pli = Vw_pli;
	private $vw_lotN = Vw_lotNum;
	private $ftb_pli = FTB_pli;
	private $ftb_lotNum = FTB_lotNumerisation;
	private $tb_flgTrt = TB_flgTrt;
    
    private $can_validate = array(/*10,*/ 17, 23);

	public function __construct() {
		parent::__construct();
		$this->CI =& get_instance();
		$this->ged = $this->load->database('ged', TRUE);
		$this->base_64 = $this->load->database('base_64', TRUE);
	}

	public function choix_init(){
		return $this->base_64
			->from($this->tb_flgTrt)
			->where_in('id_flag_traitement', $this->can_validate)
			->order_by('traitement', 'ASC')
			->get()->result();
	}

	public function piocher_pli($id_user, $trtmt_valid='-1'){
		$pli = NULL;
		if (!is_numeric($id_user)) {
			return $pli;
		}
		$pli = $this->current_pli($id_user, $trtmt_valid);
		if(is_null($pli)){
			$this->unlock_all($id_user);
			$pli = $this->get_pli($id_user, $trtmt_valid);
			if(!is_null($pli)){
            	$this->CI->histo->action(16, '', $pli->id_pli);
			}
		}else {
            $this->CI->histo->action(19, '', $pli->id_pli);
		}
		return $pli;
	}

	private function current_pli($id_user, $trtmt_valid){
		if($trtmt_valid != '-1' && is_numeric($trtmt_valid)){
			$this->ged->where('sorti_saisie', $trtmt_valid);
		}
		$data = $this->ged
			->select('tb_pli.*')
			->select('tblotN.lot_scan')
			->select('tb_soc.nom_societe soc')
			->select('tb_typo.typologie typo')
			->select('tb_grp.commentaire_groupe_paiement grpaie')
			->select('tblotN.date_courrier')
			->from($this->ftb_pli.' tb_pli')
			->join($this->tb_typo.' tb_typo', 'tb_typo.id=tb_pli.typologie', 'INNER')
			->join($this->tb_paiement.' tb_paie', 'tb_paie.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_mdPaie.' tb_mdp', 'tb_mdp.id_mode_paiement=tb_paie.id_mode_paiement', 'INNER')
			->join($this->tb_grPaie.' tb_grp', 'tb_grp.id_groupe_paiement=tb_mdp.id_groupe_paiement', 'INNER')
			->join($this->tb_data_pli.' tb_data', 'tb_data.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_soc.' tb_soc', 'tb_soc.id=tb_data.societe', 'INNER')
			->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
			->where('flag_traitement', 12)
			->where('controle_par', $id_user)
			->where('recommande != -1 ', NULL, FALSE)
			->order_by('recommande', 'DESC')
			->order_by('tblotN.date_courrier', 'ASC')
			->order_by('flag_traitement')
			->order_by('id_pli')
			->order_by('tb_grp.id_groupe_paiement')
			->get()->result();
		return count($data) > 0 ? $data[0] : NULL;
	}

	private function get_pli($id_user, $trtmt_valid){
		if($trtmt_valid != '-1' && is_numeric($trtmt_valid)){
			$this->ged->where('sorti_saisie', $trtmt_valid);
		}
		$data = $this->ged
			->select('tb_pli.*')
			->select('tblotN.lot_scan')
			->select('tb_soc.nom_societe soc')
			->select('tb_typo.typologie typo')
			->select('tb_grp.commentaire_groupe_paiement grpaie')
			->select('tblotN.date_courrier')
			->from($this->ftb_pli.' tb_pli')
			->join($this->tb_typo.' tb_typo', 'tb_typo.id=tb_pli.typologie', 'INNER')
			->join($this->tb_paiement.' tb_paie', 'tb_paie.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_mdPaie.' tb_mdp', 'tb_mdp.id_mode_paiement=tb_paie.id_mode_paiement', 'INNER')
			->join($this->tb_grPaie.' tb_grp', 'tb_grp.id_groupe_paiement=tb_mdp.id_groupe_paiement', 'INNER')
			->join($this->tb_data_pli.' tb_data', 'tb_data.id_pli=tb_pli.id_pli', 'INNER')
			->join($this->tb_soc.' tb_soc', 'tb_soc.id=tb_data.societe', 'INNER')
			->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
			->where_in('flag_traitement', $this->can_validate)
			->where('recommande != -1 ', NULL, FALSE)
			->order_by('recommande', 'DESC')
			->order_by('tblotN.date_courrier', 'ASC')
			->order_by('flag_traitement')
			->order_by('id_pli')
			->order_by('tb_grp.id_groupe_paiement')
			->get()->result();
		if (count($data) > 0) {
			$pli = $data[0];
			return $this->locked_pli($id_user, $pli) ? $pli : $this->get_pli($id_user, $trtmt_valid);
		} else {
			return NULL;
		}
	}

	private function locked_pli($id_user, $pli){
		$id_pli = $pli->id_pli;
		if(!is_numeric($id_pli)){
			return FALSE;
		}
		$data = array(
			'flag_traitement' => 12
			,'controle_par' => $id_user
		);
		if(!in_array($pli->sorti_saisie, array(17, 23))){
			$data['sorti_saisie'] = 17;
		}
		$this->base_64
			->where('id_pli', $id_pli)
			->where_in('flag_traitement', $this->can_validate)
			->set($data)
			->update($this->tb_pli);
		return $this->base_64->affected_rows() > 0;
	}

	private function unlock_all($id_user){
		$this->base_64
			->set('flag_traitement', 'sorti_saisie', FALSE)
			->set('controle_par', NULL)
			->where('flag_traitement', 12)
			->where('controle_par', $id_user)
			->where_in('sorti_saisie', $this->can_validate)
			->update($this->tb_pli);
	}
	
	public function unlocked($id_pli){
		$plis = $this->base_64
			->from($this->tb_pli)
			->where('id_pli', $id_pli)
			->get()->result();
		if(count($plis) < 1){
			log_message('error', 'control> model_pli_validation> unlocked> pli introuvable pli#'.$id_pli);
			return FALSE;
		}
		$old_flag = empty($plis[0]->sorti_saisie) || !in_array($plis[0]->sorti_saisie, $this->can_validate) ? 17 : $plis[0]->sorti_saisie;
		$data_modif = array(
            'controle_par' => NULL
            ,'flag_traitement' => $old_flag
        );
        $this->base_64
			->where('id_pli', $id_pli)
			->where('flag_traitement', 12)
            ->set($data_modif)
            ->update($this->tb_pli);
        $modification = $this->base_64->affected_rows();
        if($modification == 1){
            $this->CI->histo->action(27, '', $id_pli);
            $this->CI->histo->pli($id_pli);
            return TRUE;
        }else {
            log_message('error', 'control> model_pli_validation> unlocked> deverrouilage retraitement de pli#'.$id_pli.' produisant '.$modification.' modification =>'.$this->base_64->last_query());
            return FALSE;
        }
	}
	
}
