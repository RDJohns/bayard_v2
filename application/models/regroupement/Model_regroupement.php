<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_regroupement extends CI_Model{

    private $CI;

    private $ged;
    private $base_64;

    private $tb_pli = TB_pli;
    private $tb_paiement = TB_paiement;
    private $tb_modePaiement = TB_mode_paiement;
    private $tb_dataPli = TB_data_pli;
    private $tb_regroupement = TB_regroupement;
    private $tb_groupePli = TB_groupe_pli;

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function init_dt_regroupement(){
        date_default_timezone_set('Africa/Nairobi');
        $data = array(
            'dt_regroupement' => date('Y-m-d h:i:sa')
        );

        $this->ged
             ->insert($this->tb_regroupement, $data);
        return $this->ged->insert_id();

    }

    public function set_group_pli($id_regroupement){
        $list_pli = $this->get_pli_to_group();
        $arr_id_pli = array();
        foreach ($list_pli as $pli){
            array_push($arr_id_pli, $pli->id_pli);
            $typologie          = $pli->typologie;
            $societe            = $this->get_societe_pli($pli->id_pli);
            //$societe            = $societe[0]->societe;
            $groupe_paiement    = $this->get_group_paiement_pli($pli->id_pli);
            $groupe_paiement    = $groupe_paiement[0]->id_groupe_paiement;
            $id_groupe_possible = $typologie.'-'.$groupe_paiement.'-'.$societe;
            $id_groupe_pli      = $this->add_groupe_pli($id_regroupement, $id_groupe_possible);
            $this->update_id_groupe_pli($pli->id_pli, $id_groupe_pli);
        }
        $this->CI->histo->plis($arr_id_pli);
    }

    public function get_pli_to_group(){
        return $this->base_64
                    ->select('id_pli,typologie')
                    ->from($this->tb_pli)
                    //->where('id_groupe_pli IS NULL',null,false)
                    ->where('flag_traitement',2)
                    ->where('statut_saisie',1)
                    //->where('id_pli',450)
                    //->where('typage_par IS NOT NULL',null,false)
                    ->get()
                    ->result();
    }

    public function get_group_paiement_pli($id_pli){
        return $this->ged
                    ->select_min('id_groupe_paiement')
                    ->from($this->tb_paiement)
                    ->join($this->tb_modePaiement.' mp', 'mp.id_mode_paiement = '.$this->tb_paiement.'.id_mode_paiement')
                    ->where('id_pli',$id_pli)
                    ->get()
                    ->result();
    }

    public function get_societe_pli($id_pli){
        $soc = $this->ged
                    ->select('id_pli,societe')
                    ->from($this->tb_dataPli)
                    ->where('id_pli', $id_pli)
                    ->get()
                    ->result();

        if($soc){
            return $soc[0]->societe;
        }else{
            $societe = $this->set_societe_data_pli($id_pli);
            return $societe;
        }
    }

    public function set_societe_data_pli($id_pli){
        $pli = $this->base_64
                    ->select('id_pli,societe')
                    ->from($this->tb_pli)
                    ->where('id_pli', $id_pli)
                    ->get()
                    ->result();

        $societe = $pli[0]->societe;

        $data = array(
            'id_pli' => $id_pli,
            'societe' => $societe
        );

        $this->ged->insert($this->tb_dataPli, $data);

        return $societe;
    }

    public function add_groupe_pli($id_regroup, $id_gp_poss){
        $data = array(
            'id_regroupement' => $id_regroup,
            'id_view_groupe_pli_possible' => $id_gp_poss
        );

        $regroup_exist = $this->ged
                                ->select('*')
                                ->from($this->tb_groupePli)
                                ->where('id_regroupement', $id_regroup)
                                ->where('id_view_groupe_pli_possible', $id_gp_poss)
                                ->get()
                                ->result();

        if($regroup_exist){
            return $regroup_exist[0]->id_groupe_pli;
        }

        $this->ged
                ->insert($this->tb_groupePli, $data);

        return $this->ged->insert_id();

    }

    public function update_id_groupe_pli($id_pli, $id_groupe_pli){
        $data = array(
            'id_groupe_pli' => $id_groupe_pli,
            'flag_traitement' => 3
        );

        $this->base_64->where('id_pli', $id_pli);
        $this->base_64->update($this->tb_pli, $data);

        $this->ged->where('id_pli', $id_pli);
        $this->ged->update($this->tb_dataPli, array('flag_traitement' => 3));
    }

}
