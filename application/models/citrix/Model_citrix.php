<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Model_citrix extends CI_Model
    {
        var $table         = 'view_saisie_citrix';
        var $tb_citrix     = 'saisie_citrix';
        var $column_order = array(null, 'societe', 'nom_abonne', 'nom_payeur', null);
        var $column_search = array('societe','nom_abonne','nom_payeur'); 
        var $order = array('id' => 'desc'); 
        private $ged;
        private $base_64;

        function __construct()
        {
            parent::__construct();
            parent::__construct();
            $this->ged = $this->load->database('ged', TRUE);
            $this->base_64 = $this->load->database('base_64', TRUE);
        }

        private function _get_datatables_query()
        {
                
                $this->ged->from($this->tb_citrix);

                $i = 0;
            
                foreach ($this->column_search as $item) 
                {
                    if($_POST['search']['value']) 
                    {
                        
                        if($i===0) 
                        {
                            $this->ged->group_start(); 
                            $this->ged->like($item, $_POST['search']['value']);
                        }
                        else
                        {
                            $this->ged->or_like($item, $_POST['search']['value']);
                        }

                        if(count($this->column_search) - 1 == $i) 
                            $this->ged->group_end(); 
                    }
                    $i++;
                }
                
                if(isset($_POST['order'])) 
                {
                    $this->ged->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
                } 
                else if(isset($this->order))
                {
                    $order = $this->order;
                    $this->ged->order_by(key($order), $order[key($order)]);
                }
        }

        function get_datatables()
        {
            $this->_get_datatables_query();
            if($_POST['length'] != -1)
            $this->ged->limit($_POST['length'], $_POST['start']);
            $query = $this->ged->get();
            return $query->result();
        }


        function count_filtered_list()
        {
            $this->_get_datatables_query();
            $query = $this->ged->get();
            return $query->num_rows();
        }

        public function count_all_list()
        {
            $this->ged->from($this->tb_citrix);
            return $this->ged->count_all_results();
        }

        public function get_by_id($id)
        {
            $this->ged->from($this->tb_citrix);
            $this->ged->where('id',$id);
            $query = $this->ged->get();

            return $query->row();
        }

        public function save($data)
        {
            $this->ged->insert($this->tb_citrix, $data);
            return $this->ged->insert_id();
        }

        public function update($where, $data)
        {
            $this->ged->update($this->tb_citrix, $data, $where);
            return $this->ged->affected_rows();
        }

        private function _get_query_citrix()
        {
            $this->ged->from($this->table);

            $i = 0;
            $date_deb = date_tobd( $_POST['dateDebut']);
            $date_fin = date_tobd( $_POST['dateFin']);
            $societe  = $_POST['societe'];
            $statut = $_POST['statut'];
            $paiement = $_POST['paiement'];
            $typologie = $_POST['typologie'];
            $this->ged->where('date_enregistrement>=',$date_deb);
            $this->ged->where('date_enregistrement<=',$date_fin);
            $this->ged->where('societe',$societe);
            $this->ged->where('statut' ,$statut);
            $this->ged->where('mode_paiement ',$paiement);
            $this->ged->where('typologie' ,$typologie);

            if(isset($_POST['order'])) 
            {
                $this->ged->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->ged->order_by(key($order), $order[key($order)]);
            }
        }

        function get_citrix()
        {
            $this->_get_query_citrix();
            if(isset($_POST['length']) && $_POST['length'] < 1) {
                $_POST['length']= '10';
            } else
                $_POST['length']= $_POST['length'];

            if(isset($_POST['start']) && $_POST['start'] > 1) {
                $_POST['start']= $_POST['start'];
            }      
            $this->ged->limit($_POST['length'], $_POST['start']);
            $query = $this->ged->get();
            return $query->result();
        }

        public function count_all()
        {
            $this->ged->from($this->table);
            $date_deb = date_tobd( $_POST['dateDebut']);
            $date_fin = date_tobd( $_POST['dateFin']);
            $societe  = $_POST['societe'];
            $statut = $_POST['statut'];
            $paiement = $_POST['paiement'];
            $typologie = $_POST['typologie'];
            $this->ged->where('statut' ,$statut);
            $this->ged->where('mode_paiement',$mode_paiement);
            $this->ged->where('typologie' ,$typologie);
            $this->ged->where('date_enregistrement>=',$date_deb);
            $this->ged->where('date_enregistrement<=',$date_fin);
            return $this->ged->count_all_results();
        }

        function count_filtered()
        {
            $this->_get_query_citrix();
            $query = $this->ged->get();
            return $query->num_rows();
        }

        public function exportCitrix($date1,$date2,$societe,$statut,$paiement,$typologie)
        {
            $sqlFinal = "SELECT *from public.view_saisie_citrix where  date_enregistrement between '".$date1."' and '".$date2."' and societe = '".$societe."'  and statut = '".$statut."' and mode_paiement = '".$paiement."' and typologie = '".$typologie."'  order by societe";
    
            $query = $this->ged->query($sqlFinal);
            return $query->result();
            
        }

        public function get_all_virements_citrix()
        {
            $this->ged->from('saisie_citrix');
            $query=$this->ged->get();
            return $query->result();
        }

    }