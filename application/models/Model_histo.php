<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_histo extends CI_Model{

	private $CI;

	private $ged;
    private $base_64;
    private $push;

    private $tb_pli = TB_pli;
    private $ftb_dataPli = FTB_data_pli;
    private $tb_histo = TB_histo;
    private $tb_trace = TB_trace;
    private $tb_act = TB_action;
    private $tb_flux = TB_flux;
    private $tb_histoFx = TB_histoFx;
    private $tb_lotNum = TB_lotNumerisation;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
    }

    public function save_histo($user, $id_pli, $comms, $pli, $link_motif_consigne=''){
        $this->ged->insert($this->tb_histo, array(
            'id_pli' => $id_pli
            ,'utilisateur' => $user
            ,'flag_traitement' => $pli->flag_traitement
            ,'statut_pli' => $pli->statut_saisie
            ,'comms' => $comms
            ,'dt_courrier' => $pli->date_courrier
            ,'societe_h' => $pli->societe
            ,'typologie_h' => $pli->typologie
            ,'week_event' => date('W')
            ,'month_event' => date('m')
            ,'year_event' => date('Y')
            ,'day_event' => date('d')
            ,'week_day_event' => date('N')
            ,'link_motif_consigne' => $link_motif_consigne
        ));
        // $this->fill_histo();
    }

    private function fill_histo(){
        $histos = $this->ged
            ->select('*')
            ->select('oid')
            ->from($this->tb_histo)
            ->where('dt_courrier IS NULL', NULL, FALSE)
            ->get()->result();
        foreach ($histos as $key => $histo) {
            $pli = $this->pli($histo->id_pli);
            $dt_event = new DateTime($histo->dt_event);
            $this->ged
                ->set('dt_courrier', $pli->date_courrier)
                ->set('societe_h', $pli->societe)
                ->set('typologie_h', $pli->typologie)
                ->set('week_event', $dt_event->format('W'))
                ->set('month_event', $dt_event->format('m'))
                ->set('year_event', $dt_event->format('Y'))
                ->set('day_event', $dt_event->format('d'))
                ->set('week_day_event', $dt_event->format('N'))
                ->where('oid', $histo->oid)
                ->update($this->tb_histo);
        }
    }

    public function pli($id_pli){
        $plis = $this->base_64
            ->from($this->tb_pli.' pli')
            ->where('pli.id_pli', $id_pli)
            ->join($this->ftb_dataPli.' data_pli', 'pli.id_pli = data_pli.id_pli', 'INNER')
            ->join($this->tb_lotNum.' lotnum', 'lotnum.id_lot_numerisation = pli.id_lot_numerisation', 'INNER')
            ->get()->result();
        return count($plis) > 0 ? $plis[0] : NULL;
    }

    public function last_histo($id_pli){
        $histos = $this->ged
            ->from($this->tb_histo)
            ->where('id_pli', $id_pli)
            ->order_by('dt_event', 'DESC')
            ->get()->result();
        return count($histos) > 0 ? $histos[0] : NULL;
    }

    public function save_act($id_user, $id_act, $com, $id_pli){
        $id_couple = NULL;
        if(!is_null($id_user)) {
            $this->regul($id_user, $id_act);
            $last = $this->last_act_pli($id_user, $id_pli);
            if(!is_null($last)){
                if(isset($last[0]) && is_numeric($last[0]->oid)){
                    $id_couple = $last[0]->oid;
                }
            }
        }
        if($this->valid_act($id_act)){
            $this->ged->insert($this->tb_trace, array(
                'id_user' => $id_user
                ,'id_action' => $id_act
                ,'coms_trace' => $com
                ,'id_pli' => $id_pli
                ,'id_couple' => $id_couple
                ,'ip' => $this->CI->input->ip_address()
            ));
        }else {
            log_message('error', 'model_histo> save_act> unexpected id_action: '.$id_act);
        }
    }

    private function regul($id_user, $id_act){
        if($id_act == 1){
            $last = $this->last_act($id_user);
            if(!is_null($last)){
                if($last[0]->id_action != 2){
                    $last_dtm = new DateTime($last[0]->date_action);
                    $last_dtm->modify('+1 second');
                    $id_couple = NULL;
                    if(isset($last[1])){
                        if(is_numeric($last[1]->oid)){
                            $id_couple = $last[1]->oid;
                        }
                    }
                    $this->ged->insert($this->tb_trace, array(
                        'date_action' => $last_dtm->format('d-m-Y G:i:s')
                        ,'id_user' => $id_user
                        ,'id_action' => 2
                        ,'coms_trace' => 'Auto logout'
                        ,'id_couple' => $id_couple
                    ));
                }
            }
        }
    }

    private function last_act($id_user){
        $acts = $this->ged
            ->select('*')
            ->select('oid')
            ->from($this->tb_trace)
            ->where('id_user', $id_user)
            ->order_by('date_action', 'DESC')
            ->get()->result();
        return count($acts) > 0 ? $acts : NULL;
    }

    private function last_act_pli($id_user, $id_pli){
        $acts = $this->ged
            ->select('*')
            ->select('oid')
            ->from($this->tb_trace)
            ->where('id_user', $id_user)
            ->where('id_pli', $id_pli)
            ->where('id_pli IS NOT NULL', NULL, FALSE)
            ->order_by('date_action', 'DESC')
            ->get()->result();
        return count($acts) > 0 ? $acts : NULL;
    }

    private function valid_act($id_act){
        return $this->ged
            ->from($this->tb_act)
            ->where('id_action', $id_act)
            ->count_all_results() > 0;
    }
    
    public function save_histo_fx($user, $id_flux, $comms, $flux, $link_motif_consigne_flux=''){
        $this->push->insert($this->tb_histoFx, array(
            'id_flux' => $id_flux
            ,'statut_pli' => $flux->statut_pli
            ,'user_id' => $user
            ,'niveau_traitement' => $flux->flag_traitement_niveau + $flux->flag_retraitement
            ,'comms' => $comms
            ,'dt_recept' => $flux->date_reception
            ,'societe_h' => $flux->societe
            ,'typologie_h' => $flux->typologie
            ,'week_event' => date('W')
            ,'month_event' => date('m')
            ,'year_event' => date('Y')
            ,'day_event' => date('d')
            ,'week_day_event' => date('N')
            ,'etat_flux_h' => $flux->etat_pli_id
            ,'source_h' => $flux->id_source
            ,'link_motif_consigne_flux' => $link_motif_consigne_flux
        ));
        //$this->fill_histo_fx();
    }
    
    private function fill_histo_fx(){
        $histos = $this->push
            ->select('*')
            ->select('oid')
            ->from($this->tb_histoFx)
            ->where('dt_recept IS NULL', NULL, FALSE)
            ->get()->result();
        foreach ($histos as $key => $histo) {
            $flux = $this->flux($histo->id_flux);
            $dt_event = new DateTime($histo->date_traitement);
            $this->push
                ->set('dt_recept', $flux->date_reception)
                ->set('societe_h', $flux->societe)
                ->set('typologie_h', $flux->typologie)
                ->set('week_event', $dt_event->format('W'))
                ->set('month_event', $dt_event->format('m'))
                ->set('year_event', $dt_event->format('Y'))
                ->set('day_event', $dt_event->format('d'))
                ->set('week_day_event', $dt_event->format('N'))
                ->set('etat_flux_h', $flux->etat_pli_id)
                ->where('oid', $histo->oid)
                ->update($this->tb_histoFx);
        }
    }
    
    public function last_histo_fx($id_flux){
        $histos = $this->push
            ->from($this->tb_histoFx)
            ->where('id_flux', $id_flux)
            ->order_by('date_traitement', 'DESC')
            ->get()->result();
        return count($histos) > 0 ? $histos[0] : NULL;
    }
    
    public function flux($id_flux){
        $data = $this->push
            ->where('id_flux', $id_flux)
            ->from($this->tb_flux)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }

}
