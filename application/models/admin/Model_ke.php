<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_ke extends CI_Model{
    private $CI;
    private $tb_flux = TB_flux;
    //private $tb_source = TB_Source;
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
    }
   

    //function pour recuperer les sociétés: BY ou My
    public function getSociete()
    {
        $sql = "
            select id, nom_societe from societe
            where actif = 1        
        ";
        $query = $this->ged->query($sql);

        return $query->result();

    }

    //function pour recuperer typologies
    public function getTypologie()
    {
        $sql = "
            select id, typologie from typologie
            where actif = 1
        ";
        $query = $this->ged->query($sql);

        return $query->result();

    }


    //recuperation des plis selon les criteres de recherche
    public function getPli($where, $lenght, $start)
    {
        $where_limit = "";
        $da = new DateTime();
        if($lenght != -1) {
            if($lenght != null and $start != null) {

                $where_limit = "
                    LIMIT ".$lenght."
                    OFFSET ".$start."
                ";
            };
        } else {
            $where_limit = "
                LIMIT null
                OFFSET null
            ";
        } 
        $sql = "            
        select f_pli.id_pli,societe.nom_societe,typologie.typologie,histo.dt_event::date,f_lot_numerisation.date_courrier::date,f_lot_numerisation.lot_scan,data_pli.autre_motif_ko,motif_ko.libelle_motif  
        from f_pli
        left  join data_pli on f_pli.id_pli = data_pli.id_pli 
        left  join motif_ko on  data_pli.motif_ko = motif_ko.id_motif
        left join f_lot_numerisation on f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
        left join societe on societe.id = f_pli.societe
        left join typologie on typologie.id = f_pli.typologie
        inner join
        (
          select histo_pli.id_pli,max(f.dt_event) as dt_event from histo_pli 
            inner  join view_histo_pli_oid f on f.oid = histo_pli.oid 
          where f.flag_traitement = 19 group by histo_pli.id_pli
          
        ) histo on histo.id_pli = f_pli.id_pli
        where f_pli.flag_traitement = 19 and ke_prio =0 
        ".$where." order by id_pli ".$where_limit;
        //echo($sql);die;
        $query = $this->ged->query($sql);
        return $query->result();
    }

    //compter les pli
    public function getTotal($where, $lenght, $start)
    {
     
            $where_limit = "
                LIMIT null
                OFFSET null
            ";
            $da = new DateTime();
       
        $sql = "            
            select count(*) from f_pli
                left  join data_pli on f_pli.id_pli = data_pli.id_pli 
                left  join motif_ko on  data_pli.motif_ko = motif_ko.id_motif
                left join f_lot_numerisation on f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
                left join societe on societe.id = f_pli.societe
                left join typologie on typologie.id = f_pli.typologie
                inner join
                    (
                        select histo_pli.id_pli,max(f.dt_event) as dt_event from histo_pli 
                            inner  join view_histo_pli_oid f on f.oid = histo_pli.oid 
                        where f.flag_traitement = 19 group by histo_pli.id_pli
                        
                    ) histo on histo.id_pli = f_pli.id_pli
            where f_pli.flag_traitement = 19 and ke_prio =0 
        ".$where." ".$where_limit;
        
        $query = $this->ged->query($sql);
        return $query->result();
    }

    //update ke_prio to 1
    function prioriserPli($id_pli)
    {
        
        if(is_array($id_pli)) {            
            $sql = $this->updatePli(implode(',',$id_pli));
           
        } else {
            $sql =  $this->updatePli($id_pli);
        }
    }

    //update pli
    function updatePli($id)
    {
        $sql = " update data_pli set ke_prio = 1 where id_pli in(".$id.") and ke_prio = 0";
        $query = $this->ged->query($sql);
        $this->CI->histo->action(78, '', $id);
        return $query;
    }

    
    
}
