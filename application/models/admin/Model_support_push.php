<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_support_push extends CI_Model{
    private $CI;
    private $tb_flux = TB_flux;
    //private $tb_source = TB_Source;
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged_push', TRUE);
    }

    //function pour recuperer les source: FTP ou MAIL
    public function getSource()
    {
        $sql = "
            select id_source, source from source
            where actif = 1        
        ";
        $query = $this->ged->query($sql);

        return $query->result();

    }
    public function getDossier()
    {
        $sql = "select *from ( SELECT libelle nom_dossier FROM public.liste_mail union 
        SELECT nom_dossier FROM public.liste_dossier) as dossier order by nom_dossier";
        $query = $this->ged->query($sql);

        return $query->result();
    }

    //function pour recuperer les sociétés: BY ou My
    public function getSociete()
    {
        $sql = "
            select id, nom_societe from f_societe
            where actif = 1        
        ";
        $query = $this->ged->query($sql);

        return $query->result();

    }

    //function pour recuperer typologies
    public function getTypologie()
    {
        $sql = "
            select id_typologie, typologie from typologie
            where actif = 1
        ";
        $query = $this->ged->query($sql);

        return $query->result();

    }

    //function pour recuperer statut
    public function getStatut()
    {
        $sql = "
            select * from statut_pli where statut_lib <> '' order by id_statut 
          
        ";
        $query = $this->ged->query($sql);

        return $query->result();

    }

    //function pour recuperer user flux
    public function getUserFlux($type_acces)
    {
        $sql = "
            select id_utilisateur, login from f_ged_user 
            left join operateur_acces as opa on opa.id_user = f_ged_user.id_utilisateur where id_type_acces in(".$type_acces.") and id_type_utilisateur = 5          
        ";
        $query = $this->ged->query($sql);

        return $query->result();

    }

    //function pour recuperer les motifs blocage
    public function getMotifBlocage()
    {
        $sql = "
            select id_motif, motif from motif_blocage                 
        ";
        $query = $this->ged->query($sql);

        return $query->result();

    }

    //recuperation des flux selon les criteres de recherche
    public function getFlux($where, $lenght, $start)
    {
        $where_limit = "";
        $da = new DateTime();
       // var_dump($da->format('Y-m-d'));die;
        if($lenght != -1) {
            if($lenght != null and $start != null) {

                $where_limit = "
                    LIMIT ".$lenght."
                    OFFSET ".$start."
                ";
            };
        } else {
            $where_limit = "
                LIMIT null
                OFFSET null
            ";
        } 
        // if ($where == '') {
        //     $where = "and date_reception::date = '".$da->format('Y-m-d')."'";
        // }
//var_dump($where);die;
        $sql = "            
        select 
            id_flux,
            source.id_source,
            source.source, 
            societe.societe,
            typologie.typologie,
            statut_pli.id_statut,
            statut_pli.statut,
            motif,
            op_typage.login op_typage,
            op_saisi.login op_saisie,
            flag_important,
            date_reception::date,
            motif_escalade,
            motif_transfert,
            sum(nbr_abo) nbr_abo,
            string_agg(distinct abon.numero_payeur,',') numero_payeur,string_agg( distinct abon.numero_abonne,',') numero_abonne,
            string_agg( distinct abon.nom_abonne,',') nom_abonne,
            string_agg(distinct abon.nom_payeur,',') nom_payeur,
            etat_pli_id,
            etat_pli.libelle_etat_pli as lib_etat
        from flux 
        left join etat_pli on etat_pli.id_etat_pli = flux.etat_pli_id
        left join source on flux.id_source = source.id_source
        left join f_societe societe on flux.societe = societe.id
        left join typologie on typologie.id_typologie = flux.typologie
        left join statut_pli on statut_pli.id_statut = flux.statut_pli
        left join motif_blocage on motif_blocage.id_motif = flux.id_motif_blocage
        left join f_ged_user op_typage on op_typage.id_utilisateur = flux.type_par
        left join f_ged_user op_saisi on op_saisi.id_utilisateur = flux.saisie_par
        left join
        (
        select count(id_flux) as nbr_abo ,id_flux as flux_abo,nom_abonne,numero_abonne,nom_payeur,numero_payeur from abonnement group by id_flux,abonnement.nom_abonne,abonnement.numero_abonne,nom_payeur,numero_payeur 
        ) abon on abon.flux_abo = flux.id_flux
        where 1 = 1
    ".$where." 
    group by flux.id_flux,source.id_source,societe.societe,typologie.typologie,statut_pli.id_statut,motif_blocage.motif,op_typage.login,op_saisi.login,libelle_etat_pli 
    order by id_flux ".$where_limit;

        
        $query = $this->ged->query($sql);
        return $query->result();
    }

    //compter les flux
    public function getTotal($where, $lenght, $start)
    {
     
            $where_limit = "
                LIMIT null
                OFFSET null
            ";
            $da = new DateTime();
            // if ($where == '') {
            //     $where = "and date_reception::date = '".$da->format('Y-m-d')."'";
            // }
       
        $sql = "            
            select count(distinct flux.id_flux) from flux 
                left join source on flux.id_source = source.id_source
                left join f_societe societe on flux.societe = societe.id
                left join typologie on typologie.id_typologie = flux.typologie
                left join statut_pli on statut_pli.id_statut = flux.statut_pli
                left join motif_blocage on motif_blocage.id_motif = flux.id_motif_blocage
                left join f_ged_user on f_ged_user.id_utilisateur = flux.saisie_par
                left join abonnement abon on abon.id_flux =  flux.id_flux
                
            where 1 = 1
        ".$where." ".$where_limit;
        //var_dump($sql);die;
        $query = $this->ged->query($sql);
        return $query->result();
    }

    //update flag important d'un flux lors du clic sur prioriser
    function prioriserFlux($id)
    {
        if($id) {
            $sql = " update flux set flag_important = 1 where id_flux = ".$id;
            $query = $this->ged->query($sql);
        }
    }

    //update flag 8 en 9 pour traitement niveau 3
    function retraiterFlux($id_flux)
    {
        $this->load->library('session');
        $user = $this->session->userdata['id_utilisateur'];
        //var_dump(implode(',', $id_flux));die;

        if(is_array($id_flux)) {
            //$verif = "select count(*) from flux where statut_pli = 9"
            
            $sql = $this->updateFlux(implode(',',$id_flux));
            if($sql) {
               $count = count($id_flux);
                for($cp =0; $cp<$count; $cp++) {
                  //$this->insertTtr($id_flux[$cp],'9','3',$user);
                  $this->CI->histo->action(72, 'Dépuis support', $id_flux[$cp]);
                  $this->CI->histo->flux($id_flux[$cp], 'Dépuis support');
                }
            }
        } else {
            $sql =  $this->updateFlux($id_flux);
            if($sql) {
                $this->CI->histo->action(72, 'Dépuis support', $id_flux);
                $this->CI->histo->flux($id_flux, 'Dépuis support');
            }
        }
    }

    //update flux
    function updateFlux($id)
    {
        $sql = " update flux set statut_pli = 9 where id_flux in(".$id.") and statut_pli = 8";
        $query = $this->ged->query($sql);
        return $query;
    }

    //isertion into traitememnt pour historique
    function insertTtr($id_flux, $flag, $niveau, $user)
    {
        $sql = " insert into traitement (id_flux,statut_pli,user_id,niveau_traitement) values (".$id_flux."
        ,".$flag.",".$user.",".$niveau.")";
       //var_dump($sql);die;
        $query = $this->ged->query($sql);
    }

    //verifier flux
    public function pli($id_pli){
        if(is_numeric($id_pli)){
            $pli = $this->ged
                ->from('flux')
                ->where('id_flux', $id_pli)
                ->get()->result();
            return count($pli) > 0 ? $pli[0] : NULL;
        }
        return NULL;
    }

    //unlock niveau 1 flg 3 vers type 2
    public function unlockNiv1($id_flux)
    {
        $data_modif = array(
            'saisie_par' => NULL
            ,'statut_pli' => 2
        );
        $this->ged
            ->where('id_flux', $id_flux)
            ->where('statut_pli', 3)
            ->set($data_modif)
            ->update($this->tb_flux);
        if($this->ged->affected_rows() == 1){
            $this->CI->histo->action(51, 'Deverrouillage support', $id_flux);
            $this->CI->histo->flux($id_flux, 'Deverrouillage support niveau1');
            return TRUE;
        }else{
            log_message('error', 'models> push> saisie> model_saisie1> unlock: unlocking failed '.$this->ged->last_query());
            return FALSE;
        }
    }

     //unlock niveau 2 flag 5 vers escalade flg 4
    public function unlockNiveau2($id_flux)
    {
        $data_modif = array(
            'saisie_par' => NULL
            ,'statut_pli' => 4
        );
        $this->ged
            ->where('id_flux', $id_flux)
            ->where('statut_pli', 5)
            ->set($data_modif)
            ->update($this->tb_flux);
        if($this->ged->affected_rows() == 1){
            $this->CI->histo->action(53, 'Deverrouillage support niveau 2 vers escalade', $id_flux);
            $this->CI->histo->flux($id_flux, 'Deverrouillage support niveau 2 vers escalade');
            return TRUE;
        }else{
            log_message('error', 'models> push> saisie> model_saisie1> unlock: unlocking failed '.$this->ged->last_query());
            return FALSE;
        }
    }

    //deverrouillage flag 10 en cours de retraitement vers reception consigne
    public function unlockRetraitement($id_flux){
        $data_modif = array(
            'saisie_par' => NULL
            ,'statut_pli' => 9
        );
        $this->ged
            ->where('id_flux', $id_flux)
            ->where('statut_pli', 10)
            ->set($data_modif)
            ->update($this->tb_flux);
        if($this->ged->affected_rows() == 1){
            $this->CI->histo->action(55, 'Deverrouillage retraitement vers reception consigne', $id_flux);
            $this->CI->histo->flux($id_flux, 'Deverrouillage retraitement vers reception consigne');
            return TRUE;
        }else{
            log_message('error', 'models> push> saisie> model_saisie1> unlock: unlocking failed '.$this->ged->last_query());
            return FALSE;
        }
    }
    
    //deverrouillage typage 
    public function unlockTypage($id_flux)
    {
     
            $data_modif = array(
            
                'statut_pli' => 0
                ,'typologie' => 0
                ,'nb_abonnement' =>NULL
                ,'entite' =>NULL
                ,'type_par' =>NULL
                ,'id_motif_blocage' =>NULL
           );

            $this->ged
            ->where('id_flux', $id_flux)
            ->where('statut_pli', 1)
            ->set($data_modif)
            ->update($this->tb_flux);
            if($this->ged->affected_rows() == 1){
                $this->CI->histo->action(70, 'Annulaiton typage support', $id_flux);
                $this->CI->histo->flux($id_flux, 'Annulaiton typage support');
                return TRUE;
            }else{
                log_message('error', 'models> push> saisie> model_saisie1> unlock: unlocking failed '.$this->ged->last_query());
                return FALSE;
            }
    }
    
    //function initialiser un flux
    public function initialiserFlux($id_flux, $with_user=TRUE)
    {
        $this->load->library('session');
        if($with_user){
            $user = $this->session->userdata['id_utilisateur'];
            $this->ged->where('statut_pli !=', 7);
        }else{
            $user = 219;
        }
        $data_modif = array(

            'flag_anomalie_pj' => 0
            ,'flag_hors_perimetre' => 0
            ,'flag_traitement_niveau' => 0
            ,'flag_important' => 0
            ,'flag_retraitement' => 0
            ,'statut_pli' => 0
            ,'typologie' => 0
            ,'nb_abonnement' => NULL
            ,'type_par' => NULL
            ,'id_motif_blocage' => NULL
            ,'description_motif' => ''
            ,'saisie_par' => NULL
            ,'nb_retraitement' => 0
            ,'id_lot_saisie_flux' => NULL
            ,'lot_saisie_bis' => 0
            ,'date_lot_saisie' => NULL
            ,'motif_escalade' => ''
            ,'motif_transfert' => ''
            ,'dt_quick_save'=> NULL
            ,'mail_reception_typage' => 0
            ,'dt_traitement' => NULL
            ,'sftp_envoi_mail_traitement' => 0
        );
        $this->ged
        ->where('id_flux', $id_flux)
        ->set($data_modif)
        ->update($this->tb_flux);

        if($this->ged->affected_rows() == 1){
            $this->CI->histo->action(75, 'Réinitialisation manueelle depuis support par '.$user, $id_flux);
            $this->CI->histo->flux($id_flux, 'Initialisation manuelle depuis support push!!!');
            return TRUE;
        }else{
            log_message('error', 'models> admin>Model_support_push initialiserFlux: init failed '.$this->ged->last_query());
            return FALSE;
        }
    }
    //historique flux
    public function getHistoriqueFlux($id_flux)
    {
        $sql = "
            select id_pli,label_action,coms_trace,date_action,id_user,login,statut_pli,TO_CHAR(date_reception::date, 'dd mon YYYY') as date_reception from f_trace
            left join f_tb_action on f_tb_action.id_action = f_trace.id_action
            left join f_ged_user on f_ged_user.id_utilisateur = f_trace.id_user
            left join flux on flux.id_flux = f_trace.id_pli
            where id_pli = ".$id_flux."
            order by date_action desc
        ";
        $query = $this->ged->query($sql);
        return $query->result();
    }

    public function getIdFluxAbonnne($abonne){
        $sql = "select distinct id_flux from abonnement where nom_abonne ilike '%".$abonne."%' or numero_abonne ilike '%".$abonne."%'"; 
        $query = $this->ged->query($sql);
        return $query->result();

    }
    public function getIdFluxPayeur($payeur){
        $sql = "select distinct id_flux from abonnement where nom_payeur ilike '%".$payeur."%' or numero_payeur ilike '%".$payeur."%'";
        $query = $this->ged->query($sql);
        return $query->result();

    }

     //function pour recuperer etat flux
    public function getEtat()
    {
        $sql = "
            select id_etat_pli, libelle_etat_pli from etat_pli
                 
         ";
        $query = $this->ged->query($sql);
        return $query->result();
    }
}
