<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_users extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    private $push;
    
    private $tb_user = TB_user;
    private $tb_tpUser = TB_tpUser;
    private $tb_grUser = TB_grUser;
    private $tb_tpOpAcces = TB_tpOpAcces;
    private $ftb_opAcces = FTB_opAcces;
    private $tb_opAcces = TB_opAcces;
    private $tb_action = TB_action;
    private $tb_trace = TB_trace;
    private $tb_data_pli = TB_data_pli;
    private $tb_soc = TB_soc;
    private $tb_paiement = TB_paiement;
    private $tb_typo = TB_typo;
    private $tb_grPaie = TB_grPaie;
    private $tb_flux = TB_flux;
    private $tb_fxTypo = FTB_fxTypologie;
    private $ftb_pliCourrier = FVW_pliCourrier;

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
    }

    
    public function nb_all_user(){
		return $this->ged
            ->where('supprime', 0)
            ->from($this->tb_user.' tbu')
            ->where('tbtp.actif', 1)
            ->join($this->tb_tpUser.' tbtp', 'tbu.id_type_utilisateur = tbtp.id_type_utilisateur', 'INNER')
            ->count_all_results();
    }
    
    public function get_users($limit, $offset, $arg_find, $arg_ordo, $sens, $filtre, $nb_only = TRUE){
        $this->ged
            ->from($this->tb_user.' tbu')
            ->join($this->tb_tpUser.' tbtp', 'tbu.id_type_utilisateur = tbtp.id_type_utilisateur', 'INNER')
            ->join($this->tb_grUser.' tbgr', 'tbu.id_groupe_utilisateur = tbgr.id_groupe_utilisateur', 'LEFT')
            ->join($this->ftb_opAcces.' tbopacces', 'tbopacces.id_user = tbu.id_utilisateur', 'LEFT')
            ->where('tbu.supprime', 0)
            ->where('tbtp.actif', 1)
            ->select('id_utilisateur id')
            ->select('login')
            ->select('tbu.id_type_utilisateur id_tp')
            ->select('nom_type_utilisateur tp')
            ->select('pass')
            ->select('nom_groupe_utilisateur gr')
            ->select('tbu.id_groupe_utilisateur id_gr')
            ->select('tbu.actif user_actif')
            ->select('tbu.matr_gpao matricule')
            ->select('avec_traitement_ko')
            ->select('niveau_op')
            ->select('id_type_acces');
            

        if ($arg_find != '') {
            $this->ged
                ->group_start()
                    ->like('lower(nom_type_utilisateur)', $arg_find)
                    ->or_like('lower(nom_groupe_utilisateur)', $arg_find)
                    ->or_like('lower(login)', $arg_find)
                    ->or_like('(id_utilisateur::varchar)', $arg_find)
                    ->or_like('(tbu.matr_gpao::varchar)', $arg_find)
                ->group_end();
        }
        if (!$nb_only) {
            $this->ged
                ->order_by($arg_ordo, strtoupper($sens))
                ->limit($limit, $offset);
			return $this->ged->get()->result();
        }
        return $this->ged->count_all_results();
    }

    public function list_type_user(){
        return $this->ged
            ->from($this->tb_tpUser)
            ->where('actif', 1)
            ->order_by('nom_type_utilisateur')
            ->get()->result();
    }

    public function get_user($id_user){
        $data = $this->ged->from($this->tb_user)->where('id_utilisateur', $id_user)->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }

    public function suppr($id_user){
        $user = $this->get_user($id_user);
        if(!is_null($user)){
            $this->ged->set('login', $user->login.'_deleted_'.date('YmdHis'));
        }
        $this->ged
            ->set('supprime', 1)
            ->where('id_utilisateur', $id_user)
            ->update($this->tb_user);
        return $this->ged->affected_rows() > 0;
    }

    public function exist_login($login, $id_user=NULL){
        if(!is_null($id_user)){
            $this->ged->where('id_utilisateur !=', $id_user);
        }
        $data = $this->ged
            ->from($this->tb_user)
            ->group_start()
                ->where('login', $login)
                ->or_like('login', $login.'_deleted_20', 'after')
            ->group_end()
            ->get()->result();
        return count($data) > 0;
    }

    public function new_user($data, $id_acces){
        $this->ged->trans_begin();
        $this->push->trans_begin();
        $sql_insert = $this->ged->set($data)->get_compiled_insert($this->tb_user) . '  RETURNING id_utilisateur ';
        $data = $this->ged->query($sql_insert)->result();
        $new_id = $this->ged->affected_rows() > 0 ? $data[0]->id_utilisateur : NULL;
        if(!is_null($id_acces) && is_numeric($new_id)) {
            $this->push->where('id_user', $new_id)->delete($this->tb_opAcces);
            $this->push
                ->set('id_user', $new_id)
                ->set('id_type_acces', $id_acces)
                ->insert($this->tb_opAcces);
        }
        if(!is_numeric($new_id) || $this->ged->trans_status() === FALSE || $this->push->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->push->trans_rollback();
            return FALSE;
        }else{
            $this->ged->trans_commit();
            $this->push->trans_commit();
            return TRUE;
        }
    }

    public function modifier($id, $data, $id_acces){
        $this->ged->trans_begin();
        $this->push->trans_begin();
        $this->ged->set($data)->where('id_utilisateur', $id)->update($this->tb_user);
        $this->push->where('id_user', $id)->delete($this->tb_opAcces);
        if(!is_null($id_acces)) {
            $this->push
                ->set('id_user', $id)
                ->set('id_type_acces', $id_acces)
                ->insert($this->tb_opAcces);
        }
        if($this->ged->affected_rows() != 1 || $this->ged->trans_status() === FALSE || $this->push->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->push->trans_rollback();
            return FALSE;
        }else{
            $this->ged->trans_commit();
            $this->push->trans_commit();
            return TRUE;
        }
    }

    public function list_grp_user($all=TRUE){
        if(!$all){
            $this->ged->where('supprime', 0);
        }
        return $this->ged
            ->from($this->tb_grUser)
            ->order_by('nom_groupe_utilisateur')
            ->get()->result();
    }
    
    public function type_acces_push(){
        return $this->push
            ->from($this->tb_tpOpAcces)
            ->where('actif', 1)
            ->order_by('id')
            ->get()->result();
    }

    public function wallboard_activity($s_login='')
    {
        $sql_sub_login = $this->ged
            ->from($this->tb_trace)
            ->where_in('id_action', array(1,2))
            ->group_by('id_user')
            ->select('id_user')
            ->select('MAX(date_action) dt')
            ->get_compiled_select();
        $this->ged->reset_query();
        $sql_login = $this->ged
            ->from($this->tb_trace.' trace')
            ->join('('.$sql_sub_login.') sub_log', 'sub_log.id_user = trace.id_user and trace.date_action = sub_log.dt', 'INNER')
            ->select('trace.id_user, trace.date_action dt_log, id_action id_log, ip ip_log')
            ->get_compiled_select();
        $this->ged->reset_query();
        $sql_sub_act = $this->ged
            ->from($this->tb_trace)
            ->group_by('id_user')
            ->select('id_user')
            ->select('MAX(date_action) dt')
            ->get_compiled_select();
        $this->ged->reset_query();
        $sql_act = $this->ged
            ->from($this->tb_trace.' trace')
            ->join('('.$sql_sub_act.') sub_act', 'sub_act.id_user = trace.id_user and trace.date_action = sub_act.dt', 'INNER')
            ->select('trace.id_user, sub_act.dt dt_act, trace.id_action id_act, trace.id_pli id_pli_act')
            ->select("(case when (sub_act.dt::timestamp without time zone + interval '8 hours') < NOW()  then 2 else 1 end) id_log_1", FALSE)
            ->select("(sub_act.dt::timestamp without time zone + interval '8 hours') dt_log_1", FALSE)
            ->select("NOW() - trace.date_action::timestamp without time zone duree", FALSE)
            ->get_compiled_select();
        $this->ged->reset_query();
        if(trim($s_login) != '' && !is_null($s_login)){
            $this->ged
                ->group_start()
                    ->like('LOWER(u.login)', strtolower(trim($s_login)))
                    ->or_like('(u.matr_gpao::varchar)', strtolower(trim($s_login)))
                ->group_end();
        }
        $main_sql = $this->ged
            ->from($this->tb_user.' u')
            ->join($this->tb_tpUser.' tu', 'u.id_type_utilisateur=tu.id_type_utilisateur','INNER')
            ->join('('.$sql_login.') lg', 'lg.id_user = u.id_utilisateur', 'LEFT')
            ->join('('.$sql_act.') act', 'act.id_user = u.id_utilisateur', 'LEFT')
            ->join($this->tb_action.' tb_action', 'tb_action.id_action=id_act', 'LEFT')
            ->join($this->tb_data_pli.' data_pli', 'data_pli.id_pli = act.id_pli_act', 'LEFT')
            ->join($this->ftb_pliCourrier.' f_pli_courrier', 'f_pli_courrier.id_pli = data_pli.id_pli and tb_action.courrier = 1 and tb_action.flux = 0', 'LEFT')
            ->join($this->tb_typo.' typologie', 'typologie.id = data_pli.typologie', 'LEFT')
            ->join($this->tb_paiement.' paiement', 'paiement.id_pli = data_pli.id_pli and id_mode_paiement = 2', 'LEFT')
            ->join($this->tb_grPaie.' groupe_paiement', '_correctif1_ = groupe_paiement.id_groupe_paiement', 'LEFT')
            ->join($this->tb_flux.' flux', 'flux.id_flux = act.id_pli_act and tb_action.flux = 1 and tb_action.courrier = 0', 'LEFT')
            ->join($this->tb_fxTypo.' flux_typologie', 'flux_typologie.id_typologie = flux.typologie', 'LEFT')
            ->join($this->tb_soc.' societe', 'societe.id = COALESCE(data_pli.societe, flux.societe)', 'LEFT')
            ->where('u.actif', 1)
            ->where('u.supprime', 0)
            ->where('tu.actif', 1)
            ->order_by('case when COALESCE(lg.id_log, 2) = 1 and COALESCE(act.id_log_1, 2) = 1 then 1 else 2 end')
            ->order_by('COALESCE(dt_act::varchar, \'0\') DESC')
            ->order_by('u.login', 'ASC')
            ->select('u.id_utilisateur id_u')
            ->select('u.login')
            ->select('u.matr_gpao')
            ->select('tu.id_type_utilisateur id_type_u')
            ->select('tu.nom_type_utilisateur type_u')
            ->select('lg.ip_log')
            ->select('label_action')
            ->select('act.id_pli_act')
            ->select('case when COALESCE(lg.id_log, 2) = 1 and COALESCE(act.id_log_1, 2) = 1 then 1 else 0 end connecte')
            ->select('case when lg.id_log = 2 then act.dt_act when dt_log_1 is not null then dt_log_1 else null end dt_log_abs', FALSE)
            ->select('duree, tb_action.courrier, tb_action.flux, deb_trtment occup, type_trtment')
            ->select('COALESCE(typologie.typologie, flux_typologie.typologie) typologie, societe.nom_societe')
            ->select('case when tb_action.flux = 0 and tb_action.courrier = 1 then commentaire_groupe_paiement else null end paiements', FALSE)
            ->select("case when tb_action.flux = 1 and tb_action.courrier = 0 then (case when flux_typologie.mail = 1 then 'Mail' else 'SFTP' end) when tb_action.flux = 0 and tb_action.courrier = 1 then 'Courrier' else null end flux", FALSE)
            ->select("case when tb_action.flux = 1 and tb_action.courrier = 0 then (case when flux_typologie.mail = 1 then 2 else 3 end) when tb_action.flux = 0 and tb_action.courrier = 1 then 1 else null end id_flux", FALSE)
            ->select('COALESCE(mvt_nbr, nb_abonnement, 0) mouvement')
            ->select('COALESCE(f_pli_courrier.date_courrier, flux.date_reception, null) dt_reception')
            ->get_compiled_select();
        $this->ged->reset_query();
        $correctif1 = ' (CASE WHEN avec_chq = 1 then 1 when avec_chq = 0 and paiement.id_paiement is not null then 2 else 3 end) ';
        $main_sql = str_replace('"_correctif1_"', $correctif1, $main_sql);
        return $this->ged->query($main_sql)->result();
    }

    public function histo_user($id_user, $limit, $offset, $nb_only = TRUE)
    {
        $this->ged
            ->from($this->tb_trace.' trace')
            ->join($this->tb_action.' tb_action', 'tb_action.id_action = trace.id_action', 'INNER')
            ->where('id_user', $id_user);
        if($nb_only){
            return $this->ged->count_all_results();
        }else{
            return $this->ged
                ->order_by('date_action', 'DESC')
                ->limit($limit, $offset)
                ->select('date_action, label_action, coms, id_pli, ip')
                ->select("case when tb_action.flux = 1 and tb_action.courrier = 0 then 'Mail/SFTP' when tb_action.flux = 0 and tb_action.courrier = 1 then 'Courrier' else null end flux", FALSE)
                ->select("case when tb_action.flux = 1 and tb_action.courrier = 0 then 2 when tb_action.flux = 0 and tb_action.courrier = 1 then 1 else null end id_flux", FALSE)
                ->get()->result();
        }
    }

    public function reset_data_user($id_user=NULL)
    {
        $id_user = is_null($id_user) ? $this->CI->session->id_utilisateur : $id_user;
        if(!is_numeric($id_user)){
            return NULL;
        }
        $data = $this->ged
            ->from($this->tb_user)
            ->where('id_utilisateur', $id_user)
            ->limit(1)
            ->get()->result();
        if(count($data) == 1){
            $this->CI->session->niveau_op = $data[0]->niveau_op;
        }else{
            $this->CI->session->niveau_op = 1;
        }
    }

}
