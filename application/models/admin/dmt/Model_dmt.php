<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dmt extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    private $push;

    private $tb_pli = TB_pli;
    private $tb_flgTrt = TB_flgTrt;
    private $tb_soc = TB_soc;
    private $tb_mdPaie = TB_mode_paiement;
    private $tb_user = TB_user;
    private $vw_typoCrFx = VW_typoCrFx;
    private $vw_statusCrFx = VW_statusCrFx;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
    }
    
    public function flag_traitement(){
        return $this->base_64
            ->from($this->tb_flgTrt)
            ->where('filter_display', 1)
            ->order_by('traitement')
            ->get()->result();
    }
    
	public function societe(){
		return $this->ged
			->from($this->tb_soc)
			->where('actif', 1)
			->order_by('nom_societe')
			->get()->result();
    }
    
	public function mode_paiement(){
		return $this->ged
			->from($this->tb_mdPaie)
			->order_by('mode_paiement')
			->get()->result();
    }
    
    public function ops(){
        return $this->ged
            ->from($this->tb_user)
            ->where_in('id_type_utilisateur', array(2, 3, 5))
            ->order_by('login')
            ->get()->result();
    }
    
    public function typo(){
        return $this->push
            ->from($this->vw_typoCrFx)
            ->order_by('titre')
            ->order_by('source')
            ->get()->result();
    }
    
    public function status(){
        return $this->push
            ->from($this->vw_statusCrFx)
            ->order_by('titre')
            ->order_by('source')
            ->get()->result();
    }

}
