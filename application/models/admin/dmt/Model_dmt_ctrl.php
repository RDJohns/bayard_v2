<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dmt_ctrl extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    private $push;

    private $tb_mdPaie = TB_mode_paiement;
    private $tb_paiement = TB_paiement;
    private $vw_dureeCtrl = VW_dureeCtrl;

    private $intresting_action_cr;
    private $intresting_action_fx;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
        // $this->intresting_action_cr = array(21, 22, 23, 25, 24, 29, 33, 30, 31, 32, 34, 38, 35, 36, 37);
        $this->intresting_action_cr = array(21,22,23,24,25,29,30,31,32,33,100,101,102,103,124,125);
    }
    
    public function lns_total(){
        return $this->ged
            ->from($this->vw_dureeCtrl)
            ->where_in('id_act', $this->intresting_action_cr)
            ->count_all_results();
    }
    
    public function get_lns($data, $nb_only = TRUE){

        // if (!$nb_only) {
		// 	$this->ged->select('STRING_AGG(md_paie.mode_paiement, \', \' ORDER BY md_paie.id_groupe_paiement) paiements', FALSE);
		// }
		// $this->ged
		// 	->select('paie.id_pli id_pli')
		// 	->select('COUNT(paie.id_mode_paiement) nb_paie')
		// 	->from($this->tb_paiement.' paie')
		// 	->join($this->tb_mdPaie.' md_paie', 'paie.id_mode_paiement = md_paie.id_mode_paiement', 'INNER')
		// 	->group_by('paie.id_pli');
		// if(is_array($data['filtre']['paies']) && count($data['filtre']['paies']) > 0){
		// 	$this->ged
		// 		->join($this->tb_paiement.' paie_1', 'paie_1.id_pli = paie.id_pli', 'INNER')
		// 		->where_in('paie_1.id_mode_paiement', $data['filtre']['paies']);
		// }
		// $sql_sub_paiement = $this->ged->get_compiled_select();
        // $this->ged->reset_query();

        $spec_src0 = FALSE;
        if(isset($data['filtre']['typos']->f_0) && is_array($data['filtre']['typos']->f_0) && count($data['filtre']['typos']->f_0) > 0){
            $this->ged->where_in('id_typo', $data['filtre']['typos']->f_0);
            $spec_src0 = TRUE;
        }
        if(isset($data['filtre']['status']->f_0) && is_array($data['filtre']['status']->f_0) && count($data['filtre']['status']->f_0) > 0){
            $this->ged->where_in('etape', $data['filtre']['status']->f_0);
            $spec_src0 = TRUE;
        }
        // if (!$nb_only) {
        //     $this->ged->select('subp.paiements paiements');
        // }
        // if(is_array($data['filtre']['paies']) && count($data['filtre']['paies']) > 0){
        //     $this->ged->select('subp.nb_paie nb_paie');
        // }
        $this->where_dt($data);
        $this->ged
            ->select('vcr.*')
            ->from($this->vw_dureeCtrl.' vcr');
            // ->join(' ('.$sql_sub_paiement.') subp', 'vcr.id_ged = subp.id_pli', 'LEFT')
            // ->where_in('id_act', $this->intresting_action_cr);

        $spec_src1_2 = FALSE;
        if(isset($data['filtre']['typos']->f_1) && is_array($data['filtre']['typos']->f_1) && count($data['filtre']['typos']->f_1) > 0){
            $spec_src1_2 = TRUE;
        }
        if(isset($data['filtre']['status']->f_1) && is_array($data['filtre']['status']->f_1) && count($data['filtre']['status']->f_1) > 0){
            $spec_src1_2 = TRUE;
        }
        if(!$spec_src0 && $spec_src1_2){
            $this->ged->where_in('id_source', array(1, 2));
        }
        // if(is_array($data['filtre']['paies']) && count($data['filtre']['paies']) > 0){
        //     $this->ged
        //         ->where('nb_paie IS NOT NULL', NULL, FALSE)
        //         ->where('nb_paie > ', 0, FALSE)
        //         ->where('id_source', 0);
		// }
        if(is_array($data['filtre']['socs']) && count($data['filtre']['socs']) > 0){
            $this->ged->where_in('id_soc', $data['filtre']['socs']);
		}
        if(is_array($data['filtre']['srcs']) && count($data['filtre']['srcs']) > 0){
            $this->ged->where_in('id_source', $data['filtre']['srcs']);
		}
        if(is_array($data['filtre']['ops']) && count($data['filtre']['ops']) > 0){
            $this->ged->where_in('id_user', $data['filtre']['ops']);
        }
        if ($data['filtre']['id_ged'] != '') {
			$this->ged->where('id_ged', $data['filtre']['id_ged']);
		}
        if ($data['filtre']['id_cmd'] != '') {
			$this->ged->like('lower(cmd)', $data['filtre']['id_cmd']);
		}
        if (!$nb_only) {
            if($data['excel']){
                if($data['type'] == 2){
                    $main_sql = $this->ged->get_compiled_select();
                    $this->ged->reset_query();
                    return $this->ged
                        ->from('('.$main_sql.') sub_main')
                        ->group_by('login')
                        ->group_by('matr_gpao')
                        ->order_by('login', 'ASC')
                        ->select('login')
                        ->select('matr_gpao')
                        ->select('SUM(duree) duree', FALSE)
                        ->get()->result();
                }else{
                    $this->ged->order_by('id_ged', 'ASC');
                }
                return $this->ged->get()->result();
            }
            $this->ged
                ->order_by($data['arg_ordo'], strtoupper($data['sens']))
                ->limit($data['limit'], $data['offset']);
            //echo $this->ged->get_compiled_select();
            return $this->ged->get()->result();
        }
        return $this->ged->count_all_results();
    }
    
    private function where_dt($data){
        // if (!empty($data['filtre']['dt_act_1'])) {
        //     $this->ged
        //         ->group_start()
        //             ->where('dt_deb::DATE >= \''.$data['filtre']['dt_act_1'].'\' ', NULL, FALSE)
        //             ->or_where('dt_fin::DATE >= \''.$data['filtre']['dt_act_1'].'\' ', NULL, FALSE)
        //         ->group_end();
		// }
        // if (!empty($data['filtre']['dt_act_2'])) {
        //     $this->ged
        //         ->group_start()
        //             ->where('dt_deb::DATE <= \''.$data['filtre']['dt_act_2'].'\' ', NULL, FALSE)
        //             ->or_where('dt_fin::DATE <= \''.$data['filtre']['dt_act_2'].'\' ', NULL, FALSE)
        //         ->group_end();
        // }
        if (!empty($data['filtre']['dt_act_1'])) {
            $this->ged
                ->where('dt_fin::DATE >= \''.$data['filtre']['dt_act_1'].'\' ', NULL, FALSE);
        }
        if (!empty($data['filtre']['dt_act_2'])) {
            $this->ged
                ->where('dt_fin::DATE <= \''.$data['filtre']['dt_act_2'].'\' ', NULL, FALSE);
        }
    }

}
