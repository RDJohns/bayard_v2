<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_support extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    private $reb_temp;
    private $reb;

    private $tb_pli = TB_pli;
    private $vw_pli = Vw_pli;
    private $tb_flgTrt = TB_flgTrt;
    private $tb_lotNum = TB_lotNumerisation;
    private $vw_flgTrt = Vw_flgTrt;
    private $tb_tpUser = TB_tpUser;
    private $tb_user = TB_user;
    private $vw_user = Vw_user;
    private $tb_doc = TB_document;
    private $tb_titre = TB_titre;
    private $tb_cheque = TB_cheque;
    private $tb_chqSaisie = TB_chequeSaisie;
    private $vw_grPliPos = Vw_grPliPos;
    private $tb_groupe_pli = TB_groupe_pli;
    private $tb_data_pli = TB_data_pli;
    private $tb_soc = TB_soc;
    private $tb_mdPaie = TB_mode_paiement;
    private $tb_paiement = TB_paiement;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_mvmntSaisie = TB_mvmntSaisie;
    private $tb_typo = TB_typo;
    private $tb_statS = TB_statS;
    private $vw_datapli = Vw_dataPli;
    private $vw_lotN = Vw_lotNum;
    private $ftb_user = FTB_user;
    private $ftb_datapli = FTB_data_pli;
    private $ftb_statuS = FTB_statuS;
    private $ftb_flgTrt = FTB_flgTrt;
    private $ftb_pli = FTB_pli;
    private $ftb_lotNum = FTB_lotNumerisation;
    private $ftb_cheque = FTB_cheque;
    private $ftb_paie = FTB_paie;
    private $ftb_mdPaie = FTB_mdPaie;
    private $ftb_typo = FTB_typo;
    private $tb_lstCirc = TB_lstCirc;
    private $ftb_lstCirc = FTB_lstCirc;
    private $ftb_motifCons = FTB_motifCons;
    private $tb_action = TB_action;
    private $tb_trace = TB_trace;
    private $tb_histo = TB_histo;
    private $ftb_soc = FTB_soc;
    private $tb_mtfRj = TB_mtfRj;
    private $tb_mtfKoScn = TB_mtfKoScn;
    private $tb_chqKd = TB_chqKd;
	private $tb_paieChqKd = TB_paieChqKd;
    private $tb_preData = TB_preData;
    private $tb_paieEsp = TB_paieEsp;
    private $tb_reb_tp = TB_rebTp;
    private $tb_reb = TB_reb;
    private $tb_motifConsigne = TB_motifConsigne;
    private $tb_paieChqAnom = TB_paieChqAnom;
    private $tb_saisiCtrlField = TB_saisiCtrlField;
    private $tb_assignationPli = TB_assignationPli;
    private $tb_histoAssignationPli = TB_histoAssignationPli;

    private $exclus_pli = array(/*6, 7, 8, 10, 11*/);
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        // $this->reb_temp = $this->load->database('base_reb_temp', TRUE);
        // $this->reb = $this->load->database('base_reb', TRUE);
    }

    public function flag_traitement(){
        return $this->base_64
            ->from($this->tb_flgTrt)
            ->where('filter_display', 1)
            ->order_by('ordre_affichage')
            ->get()->result();
    }

    public function status(){
        return $this->ged
            ->from($this->tb_statS)
            ->where('actif', 1)
            ->order_by('id_statut_saisie')
            //->order_by('libelle')
            ->get()->result();
    }

    public function user($type=NULL){
        if(is_null($type)){
            $this->ged->where_in('id_type_utilisateur', array(2, 3));
        }else{
            $this->ged->where('id_type_utilisateur', $type);
        }
        return $this->ged
            ->from($this->tb_user)
            ->where('supprime', 0)
            ->order_by('login')
            ->get()->result();
    }
    
    public function get_plis($limit, $offset, $arg_find, $arg_ordo, $sens, $filtre, $nb_only = TRUE){
        $sql_sub_pli = $this->sql_pli($filtre, $nb_only);
        $sql_sub_pai = $this->sql_paiement_data($filtre, $nb_only);
        $sql_sub_cmc7 = $this->sql_cmc7($filtre, $nb_only);
        $sql_sub_nb_pj = $this->sql_nb_pjs();
        $sql_sub_nb_motif_cons = $this->sql_nb_motif_cons();
        $this->base_64->reset_query();
        $this->base_64
            ->from('('.$sql_sub_pli.') sub_pli')
            //->join($this->tb_flgTrt.' tbflg', 'sub_pli.flag_traitement = id_flag_traitement', 'INNER')
            ->join($this->tb_lotNum.' tblotn', 'sub_pli.id_lot_numerisation = tblotn.id_lot_numerisation', 'INNER')
            ->join($this->ftb_user.' tbu_ty', 'tbu_ty.id_utilisateur = typage_par', 'LEFT')
            ->join($this->ftb_user.' tbu_ss', 'tbu_ss.id_utilisateur = saisie_par', 'LEFT')
            ->join($this->ftb_user.' tbu_ct', 'tbu_ct.id_utilisateur = controle_par', 'LEFT')
            ->join($this->ftb_user.' tbu_rt', 'tbu_rt.id_utilisateur = retraite_par', 'LEFT')
            ->join($this->ftb_datapli.' tbdp', 'tbdp.id_pli = sub_pli.id_pli', 'LEFT')
            ->join($this->ftb_statuS.' tbstatus', 'tbstatus.id_statut_saisie = tbdp.statut_saisie', 'LEFT')
            ->join($this->ftb_typo.' tbtypo', 'sub_pli.typologie = tbtypo.id', 'LEFT')
            ->join($this->ftb_lstCirc.' tblc', 'tbdp.nom_circulaire = tblc.id', 'LEFT')
            ->join($this->ftb_soc.' tbsoc', 'sub_pli.societe = tbsoc.id', 'LEFT')
            ->join('('.$sql_sub_pai.') sub_paie', 'sub_pli.id_pli = paie_id_pli', 'LEFT');
        if ($filtre['find_cmc7'] != '' || !$nb_only) {
            $join = $filtre['find_cmc7'] != '' ? 'INNER' : 'LEFT';
            $this->base_64
                ->join('('.$sql_sub_cmc7.') sub_cmc7', 'sub_pli.id_pli = cmc7_id_pli', $join)
                ->select('sub_cmc7.*');
        }
        if (!$nb_only) {
            $this->base_64
                ->join('('.$sql_sub_nb_pj.') sub_nb_pjs', 'sub_pli.id_pli = pj_id_pli', 'LEFT')
                ->select('(CASE WHEN nb_pj IS NULL THEN 0 ELSE nb_pj END)::integer nb_pj')
                ->select('paiements');
            $this->base_64
                ->join('('.$sql_sub_nb_motif_cons.') sub_nb_motif_cons', 'sub_pli.id_pli = nb_motif_cons_id_pli', 'LEFT')
                ->select('(CASE WHEN nb_motif_cons IS NULL THEN 0 ELSE nb_motif_cons END)::integer nb_motif_cons');
        }
        if ($filtre['find_lots'] != '') {
			$this->base_64->like('lower(id_lot_saisie)', $filtre['find_lots']);
        }
        if ($filtre['find_lotn'] != '') {
            $this->base_64
                ->group_start()
                    ->like('lot_scan', $filtre['find_lotn'])
                    ->or_like('lower(tblotn.commande)', $filtre['find_lotn'])
                ->group_end();
        }
        if(is_array($filtre['niveau_saisie']) && count($filtre['niveau_saisie']) > 0){
            $this->base_64->where_in('niveau_saisie', $filtre['niveau_saisie']);
        }
        if ($arg_find != '') {
            $this->base_64
                ->group_start()
                    ->or_like('lower(pli)', $arg_find)
                ->group_end();
        }
        if($nb_only){
            return $this->base_64->count_all_results();
        }
        return $this->base_64
            ->select('sub_pli.id_pli id_pli')
            ->select('pli')
            ->select('lot_scan lot_num')
            ->select('traitement flg')
            ->select('sub_pli.id_flag_traitement id_flg')
            ->select('tbdp.statut_saisie status')
            ->select('tbstatus.libelle lib_status')
            ->select('tbstatus.code code_status')
            ->select('flgtt_etape')
            ->select('flgtt_etat flgtt_etat_disp')
            ->select('(CASE WHEN flag_rewrite_status_client <> 0 AND tbdp.statut_saisie IN (1) THEN \'\' ELSE tbstatus.libelle END) etat')
            ->select('recommande rec')
            ->select('tbu_ty.login ty_par')
            ->select('tbtypo.typologie typo')
            ->select('id_lot_saisie lot_ss')
            ->select('lot_saisie_bis lot_ss_bis')
            ->select('tbu_ss.login ss_par')
            ->select('tbu_ct.login ct_par')
            ->select('tbu_rt.login rt_par')
            ->select('sub_pli.traitement')
            ->select('in_decoup')
            ->select('fichier_circulaire')
            ->select('circulaire')
            ->select('tbsoc.nom_societe')
            ->select('tblotn.commande id_com')
            ->select('tbdp.nom_orig_fichier_circulaire')
            ->select('tbdp.niveau_saisie')
            ->order_by($arg_ordo, strtoupper($sens))
            ->order_by('sub_pli.id_pli')
            ->limit($limit, $offset)
            ->get()->result();
    }

    private function sql_pli($filtre, $nb_only){
        $sql_id_pli_filtre_paiement = $this->sql_paiement_filtre($filtre, $nb_only);
        $this->base_64->reset_query();
        $this->base_64->from($this->tb_pli.' tbpli')
            ->join($this->tb_flgTrt.' tbflg', 'tbpli.flag_traitement = id_flag_traitement', 'INNER');
        if(is_array($filtre['md_paie']) && count($filtre['md_paie']) > 0){
            $this->base_64->where(' id_pli in ('.$sql_id_pli_filtre_paiement.') ', NULL, FALSE);
        }
        if(is_array($filtre['etat']) && count($filtre['etat']) > 0){
            $this->base_64->where_in('flag_traitement', $filtre['etat']);
        }
        if(count($this->exclus_pli) > 0){
            $this->base_64->where_not_in('flag_traitement', $this->exclus_pli);
        }
        if(is_array($filtre['status']) && count($filtre['status']) > 0){
            $this->base_64->where_in('statut_saisie', $filtre['status'])
                ->where('flag_rewrite_status_client', 0);
        }
        if(is_array($filtre['soc']) && count($filtre['soc']) > 0){
			$this->base_64->where_in('societe', $filtre['soc']);
        }
        if(is_array($filtre['typo']) && count($filtre['typo']) > 0){
			$this->base_64->where_in('typologie', $filtre['typo']);
        }
        if(is_array($filtre['ty_par']) && count($filtre['ty_par']) > 0){
            $this->base_64->where_in('typage_par', $filtre['ty_par']);
        }
        if(is_array($filtre['ss_par']) && count($filtre['ss_par']) > 0){
            $this->base_64->where_in('saisie_par', $filtre['ss_par']);
        }
        if(is_array($filtre['ct_par']) && count($filtre['ct_par']) > 0){
            $this->base_64->where_in('controle_par', $filtre['ct_par']);
        }
        if(is_array($filtre['rt_par']) && count($filtre['rt_par']) > 0){
            $this->base_64->where_in('retraite_par', $filtre['rt_par']);
        }
        if ($filtre['find_idpli'] != '') {
			$this->base_64->like('(id_pli::varchar)', $filtre['find_idpli']);
        }
        return $this->base_64->get_compiled_select();
    }

    private function sql_paiement_data($filtre, $nb_only){
        $this->base_64->reset_query();
        $this->base_64
            ->from($this->ftb_paie.' tbpai')
            ->join($this->ftb_mdPaie.' md_paie', 'tbpai.id_mode_paiement = md_paie.id_mode_paiement', 'INNER')
            ->group_by('tbpai.id_pli')
            ->select('tbpai.id_pli paie_id_pli');
        if (!$nb_only) {
			$this->base_64->select('STRING_AGG(md_paie.mode_paiement, \'|\' ORDER BY md_paie.id_groupe_paiement) paiements', FALSE);
        }
        return $this->base_64->get_compiled_select();
    }

    private function sql_paiement_filtre($filtre, $nb_only){
        $this->base_64->reset_query();
        $this->base_64
            ->from($this->ftb_paie)
            ->where_in('id_mode_paiement', $filtre['md_paie']);
        return $this->base_64->distinct()->select('id_pli')->get_compiled_select();
    }

    private function sql_nb_pjs(){
        $this->base_64->reset_query();
        return $this->base_64
            ->select('id_pli pj_id_pli')
            ->select('COUNT(id_document) nb_pj')
            ->from($this->tb_doc)
            ->where('desarchive', 1)
            ->group_by('id_pli')
            ->get_compiled_select();
    }

    private function sql_nb_motif_cons(){
        $this->base_64->reset_query();
        return $this->base_64
            ->select('id_pli nb_motif_cons_id_pli')
            ->select('COUNT(id) nb_motif_cons')
            ->from($this->ftb_motifCons)
            ->group_by('id_pli')
            ->get_compiled_select();
    }

    private function sql_cmc7($filtre, $nb_only){
        $this->base_64->reset_query();
        $sql_sub_cheque = $this->base_64
            ->select('id_pli chq_id_pli')
            ->select('STRING_AGG(cmc7, \', \' ORDER BY cmc7) cmc7s', FALSE)
            ->from($this->ftb_cheque)
            ->group_by('id_pli')
            ->get_compiled_select();
        $this->base_64->reset_query();
        $sql_sub_pre_cheque = $this->base_64
            ->select('id_pli pre_chq_id_pli')
            ->select('STRING_AGG(cmc7, \', \' ORDER BY cmc7) pre_cmc7s', FALSE)
            ->from($this->tb_doc.' tbdoc')
            ->join($this->tb_preData.' tbpredata', 'tbpredata.id_document = tbdoc.id_document', 'LEFT')
            ->group_by('id_pli')
            ->get_compiled_select();
        $this->base_64->reset_query();
        $this->base_64
            ->from('('.$sql_sub_cheque.') sub_chq')
            ->join('('.$sql_sub_pre_cheque.') sub_pre_chq', 'chq_id_pli = pre_chq_id_pli', 'LEFT')
            ->select('chq_id_pli cmc7_id_pli')
            ->select('cmc7s')
            ->select('pre_cmc7s');
        if ($filtre['find_cmc7'] != '') {
            $this->base_64
                ->group_start()
                    ->like('cmc7s', $filtre['find_cmc7'])
                    ->or_like('pre_cmc7s', $filtre['find_cmc7'])
                ->group_end();
        }
        return $this->base_64->get_compiled_select();
    }

    public function pli_total(){
        if(count($this->exclus_pli) > 0){
            $this->base_64->where_not_in('flag_traitement', $this->exclus_pli);
        }
        return $this->base_64
            ->from($this->tb_pli)
            ->count_all_results();
    }

    public function prio($id_pli, $rec){
        $this->base_64
            ->set('recommande', $rec)
            ->where('id_pli', $id_pli)
            ->update($this->tb_pli);
    }

    public function stand_by($id_pli, $rec){
        $this->base_64
            ->set('recommande', $rec)
            ->where('id_pli', $id_pli)
            ->update($this->tb_pli);
        if($rec == -1){
            $this->CI->histo->action(76, 'support', $id_pli);
        }else{
            $this->CI->histo->action(77, 'support', $id_pli);
        }
    }

    public function reinit($id_pli, $flg_tt){
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        $this->base_64
            ->set('flag_traitement', 0)
            ->set('statut_saisie', 1)
            ->set('recommande', 0)
            ->set('typologie', 0)
            ->set('id_groupe_pli', NULL)
            ->set('typage_par', NULL)
            ->set('saisie_par', NULL)
            ->set('traite_par', 0)
            ->set('id_lot_saisie', NULL)
            ->set('id_decoupage', NULL)
            ->set('flag_echantillon', 0)
            ->set('lot_saisie_bis', 0)
            ->set('controle_par', NULL)
            ->set('retraite_par', NULL)
            ->set('sorti_saisie', NULL)
            ->set('in_decoup', 0)
            ->set('date_tri', NULL)
            ->set('date_tlmc', NULL)
            ->set('date_typage', NULL)
            ->set('date_saisie', NULL)
            ->set('date_controle', NULL)
            ->set('date_termine', NULL)
            ->set('date_reb', NULL)
            ->set('flag_suppr_def_db', 0)
            ->set('dt_suppr_def_db', NULL)
            ->where('id_pli', $id_pli)
            ->where('flag_traitement', $flg_tt)
            ->update($this->tb_pli);
        if($this->base_64->affected_rows() == 1){
            try {
                $this->ged
                    ->set('statut_saisie', 1)
                    ->set('motif_rejet', NULL)
                    ->set('description_rejet', NULL)
                    ->set('motif_ko', NULL)
                    ->set('titre', NULL)
                    ->set('autre_motif_ko', '')
                    ->set('nom_circulaire', NULL)
                    ->set('fichier_circulaire', '')
                    ->set('message_indechiffrable', NULL)
                    ->set('dmd_kdo_fidelite_prim_suppl', NULL)
                    ->set('dmd_envoi_kdo_adrss_diff', NULL)
                    ->set('type_coupon', '')
                    ->set('id_erreur', NULL)
                    ->set('comm_erreur', '')
                    ->set('nom_orig_fichier_circulaire', '')
                    ->set('flag_solde_cloture', 0)
                    ->set('nom_deleg', '')
                    ->set('flag_ci', 0)
                    ->set('ke_prio', 0)
                    ->set('flag_traitement', 0)
                    ->set('typologie', 0)
                    ->set('flag_batch', 0)
                    ->set('mvt_nbr', 0)
                    ->set('avec_chq', 0)
                    ->set('code_ecole_gci', '')
                    ->set('flag_rejet_batch', 0)
                    ->set('date_envoi_ci', NULL)
                    ->set('motif_ci', NULL)
                    ->set('flag_saisie', 0)
                    ->set('dt_batch', NULL)
                    //->set('flag_retour_batch', 0)
                    ->set('flag_typage_auto', 0)
                    ->set('pli_nouveau', 1)
                    ->set('par_ttmt_ko', 0)
                    ->set('date_dernier_statut_ko', NULL)
                    ->set('statut_avant_ko_encours', NULL)
                    ->set('niveau_saisie', 1)
                    ->where('id_pli', $id_pli)
                    ->update($this->tb_data_pli);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paiement);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieEsp);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_mvmnt);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_cheque);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqKd);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_motifConsigne);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_paieChqAnom);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_saisiCtrlField);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_chqSaisie);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_mvmntSaisie);
                $this->ged->where('id_pli', $id_pli)->delete($this->tb_assignationPli);
            } catch (Exception $exc) {
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                log_message('error', 'models> admin> model_support> reinit: '.$exc->getMessage());
                return FALSE;
            }
            if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                return FALSE;
            }else{
                $this->ged->trans_commit();
                $this->base_64->trans_commit();
                $this->CI->histo->action(42, 'support', $id_pli);
                $this->CI->histo->pli($id_pli, 'reinitialisation support');
                return TRUE;
            }
        }
        return FALSE;
    }

    
    public function is_ce($ce=TRUE){
        // $this->tb_volume_reb = TB_volumeReb;
        $this->tb_reb_tp = TB_rebTp;
        $this->tb_reb = TB_reb;
        // $this->tb_syncro = TB_syncro;
        if($ce){
            // $this->tb_volume_reb = TB_volumeRebCe;
            $this->tb_reb_tp = TB_rebCeTp;
            $this->tb_reb = TB_rebCe;
            // $this->tb_syncro = TB_syncroCe;
        }
    }

    // public function with_cheque_reb($id_pli){
    //     $pli = $this->reb
    //         ->where('id_pli', $id_pli)
    //         ->from($this->tb_reb)
    //         ->get()->result();
    //     if(count($pli) > 0){
    //         return trim($pli[0]->traitement) == 'OK';
    //     }
    //     return FALSE;
    // }

    public function with_chq_saisie_reb($id_pli) {
        $data = $this->ged
            ->where('id_pli', $id_pli)
            ->where('date_reb IS NOT NULL', NULL, FALSE)
            ->from($this->tb_chqSaisie)
            ->count_all_results();
        return $data > 0;
    }

    public function is_divised($id_pli){
        $pli_fils = $this->base_64
            ->from($this->tb_pli)
            ->where('pli_parent', $id_pli)
            ->count_all_results();
        return $pli_fils > 0;
    }
    
    public function reinit_extra($tab_id_pli){
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        // $this->reb_temp->trans_begin();
        // $this->reb->trans_begin();
        try {
            $this->base_64
                ->set('flag_traitement', 0)
                ->set('statut_saisie', 1)
                ->set('recommande', 0)
                ->set('typologie', 0)
                ->set('id_groupe_pli', NULL)
                ->set('typage_par', NULL)
                ->set('saisie_par', NULL)
                ->set('traite_par', 0)
                ->set('id_lot_saisie', NULL)
                ->set('id_decoupage', NULL)
                ->set('flag_echantillon', 0)
                ->set('lot_saisie_bis', 0)
                ->set('controle_par', NULL)
                ->set('retraite_par', NULL)
                ->set('sorti_saisie', NULL)
                ->set('in_decoup', 0)
                ->set('date_tri', NULL)
                ->set('date_tlmc', NULL)
                ->set('date_typage', NULL)
                ->set('date_saisie', NULL)
                ->set('date_controle', NULL)
                ->set('date_termine', NULL)
                ->set('date_reb', NULL)
                ->set('flag_suppr_def_db', 0)
                ->set('dt_suppr_def_db', NULL)
                ->where_in('id_pli', $tab_id_pli)
                ->update($this->tb_pli);
            $this->ged
                ->set('statut_saisie', 1)
                ->set('motif_rejet', NULL)
                ->set('description_rejet', NULL)
                ->set('motif_ko', NULL)
                ->set('titre', NULL)
                ->set('autre_motif_ko', '')
                ->set('nom_circulaire', NULL)
                ->set('fichier_circulaire', '')
                ->set('message_indechiffrable', NULL)
                ->set('dmd_kdo_fidelite_prim_suppl', NULL)
                ->set('dmd_envoi_kdo_adrss_diff', NULL)
                ->set('type_coupon', '')
                ->set('id_erreur', NULL)
                ->set('comm_erreur', '')
                ->set('nom_orig_fichier_circulaire', '')
                ->set('flag_solde_cloture', 0)
                ->set('nom_deleg', '')
                ->set('flag_ci', 0)
                ->set('ke_prio', 0)
                ->set('mvt_nbr', 0)
                ->set('avec_chq', 0)
                ->set('flag_traitement', 0)
                ->set('typologie', 0)
                ->set('flag_batch', 0)
                ->set('code_ecole_gci', '')
                ->set('flag_rejet_batch', 0)
                ->set('date_envoi_ci', NULL)
                ->set('motif_ci', NULL)
                ->set('flag_saisie', 0)
                ->set('dt_batch', NULL)
                //->set('flag_retour_batch', 0)
                ->set('flag_typage_auto', 0)
                ->set('pli_nouveau', 1)
                ->set('par_ttmt_ko', 0)
                ->set('date_dernier_statut_ko', NULL)
                ->set('statut_avant_ko_encours', NULL)
                ->set('niveau_saisie', 1)
                ->where_in('id_pli', $tab_id_pli)
                ->update($this->tb_data_pli);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_paiement);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_paieEsp);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_mvmnt);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_cheque);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_paieChqKd);
            // $this->reb_temp->where_in('id_pli', $tab_id_pli)->delete($this->tb_reb_tp);
            // $this->reb->where_in('id_pli', $tab_id_pli)->delete($this->tb_reb);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_motifConsigne);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_paieChqAnom);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_saisiCtrlField);
            /*$this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_histo);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_trace);*/
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_chqSaisie);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_mvmntSaisie);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_assignationPli);
        } catch (Exception $exc) {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            // $this->reb_temp->trans_rollback();
            // $this->reb->trans_rollback();
            log_message('error', 'models> admin> model_support> reinit_extra: '.$exc->getMessage());
            return FALSE;
        }
        if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE/* || $this->reb_temp->trans_status() === FALSE || $this->reb->trans_status() === FALSE*/){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            // $this->reb_temp->trans_rollback();
            // $this->reb->trans_rollback();
            log_message('error', 'models> admin> model_support> reinit_extra => failed');
            return FALSE;
        }else {
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            // $this->reb_temp->trans_commit();
            // $this->reb->trans_commit();
            $this->CI->histo->action_plis(42, 'reinitialisation exceptionnelle', $tab_id_pli);
            $this->CI->histo->plis($tab_id_pli, 'reinitialisation exceptionnelle');
            return TRUE;
        }
    }

    public function abandon($id_pli, $flg_tt){
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        try {
            $this->ged
                ->set('statut_saisie', 8)
                ->set('flag_traitement', 14)
                ->where('id_pli', $id_pli)
                ->update($this->tb_data_pli);
            $this->base_64
                ->set('statut_saisie', 8)
                ->set('saisie_par', NULL)
                ->set('flag_traitement', 14)
                ->where('id_pli', $id_pli)
                ->where('flag_traitement', $flg_tt)
                ->update($this->tb_pli);
            if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE || $this->base_64->affected_rows() < 1){
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                return array(FALSE, 'Le pli a changé d\'état!');
            }else{
                $this->ged->trans_commit();
                $this->base_64->trans_commit();
                $this->CI->histo->action(43, 'support', $id_pli);
                $this->CI->histo->pli($id_pli, 'abandon-pli support');
                return array(TRUE, '');
            }
        } catch (Exception $exc) {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> admin> model_support> abandon: '.$exc->getMessage());
            return array(FALSE, 'Erreur serveur!');
        }
    }

    /*public function ko_scan($id_pli){
        $this->base_64
            ->set('flag_traitement', 16)
            ->where('id_pli', $id_pli)
            ->update($this->tb_pli);
        if($this->base_64->affected_rows() > 0){
            $this->ged
                ->set('statut_saisie', 2)
                ->where('id_pli', $id_pli)
                ->update($this->tb_data_pli);
            $this->CI->histo->pli($id_pli, 'KO-scan support');
            $this->CI->histo->action(40, 'support', $id_pli);
            return TRUE;
        }
        return FALSE;
    }*/
    
    public function ko_scan($id_pli, $flg_tt_now, $data_motif){
        $motif = $data_motif['motif_rejet_id'] == 4 ? '' : $this->CI->model_pli_saisie->get_label_motif_ko($data_motif['motif_rejet_id']);
        $link_motif_consigne = $id_pli.date('YmdHis');
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        try {
            $this->base_64
                ->set('statut_saisie', 2)
                ->where('id_pli', $id_pli)
                ->where('flag_traitement', $flg_tt_now)
                // ->where('statut_saisie', 1)
                ->update($this->tb_pli);
            $this->ged
                ->set('statut_saisie', 2)
                ->where('id_pli', $id_pli)
                ->update($this->tb_data_pli);
            // $this->ged->where('pli_id', $id_pli)->delete($this->tb_mtfKoScn);
            $this->ged->insert($this->tb_mtfKoScn, $data_motif);
            // $this->ged->where('id_pli', $id_pli)->delete($this->tb_motifConsigne);
            $this->ged->insert($this->tb_motifConsigne, array(
				'id_pli' => $id_pli
                ,'motif_operateur' => $data_motif['description'] == '' ? $motif : $motif.$data_motif['description']
                ,'id_flg_trtmnt' => $flg_tt_now
				,'id_stt_ss' => 2
				,'link_histo' => $link_motif_consigne
			));
            if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE || $this->base_64->affected_rows() < 1){
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                return FALSE;
            }else{
                $this->ged->trans_commit();
                $this->base_64->trans_commit();
                $this->CI->histo->action(40, 'support', $id_pli);
                $this->CI->histo->pli($id_pli, 'KO-scan support', $link_motif_consigne);
                return TRUE;
            }
        } catch (Exception $exc) {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> admin> model_support> ko_scan: '.$exc->getMessage());
            return FALSE;
        }
    }

    public function hors_perim($id_pli, $flg_tt){
        $this->base_64
            ->set('flag_traitement', 21)
            ->where('id_pli', $id_pli)
            ->where('flag_traitement', $flg_tt)
            ->update($this->tb_pli);
        if($this->base_64->affected_rows() > 0){
            $this->ged
                ->set('statut_saisie', 2)
                ->where('id_pli', $id_pli)
                ->update($this->tb_data_pli);
                $this->CI->histo->action(41, 'support', $id_pli);
            $this->CI->histo->pli($id_pli, 'HP support');
            return TRUE;
        }
        return FALSE;
    }

    public function recuperation_ks($id_pli, $flg_tt){
        $this->base_64
            ->set('flag_traitement', 17)
            ->where('id_pli', $id_pli)
            ->where('flag_traitement', $flg_tt)
            ->update($this->tb_pli);
        if($this->base_64->affected_rows() > 0){
            $this->ged
                ->set('statut_saisie', 2)
                ->where('id_pli', $id_pli)
                ->update($this->tb_data_pli);
            $this->CI->histo->action(45, 'recup KS support', $id_pli);
            $this->CI->histo->pli($id_pli, 'recup KS support');
            return TRUE;
        }
        return FALSE;
    }

    public function data_pli($id_pli){
        $data = $this->base_64
            ->where('id_pli', $id_pli)
            ->from($this->tb_pli)
            ->get()->result();
        return count($data) ? $data[0] : NULL;
    }

    public function data_docs($id_pli){
        return $this->base_64
            ->select('id_document')
            ->from($this->tb_doc)
            ->where('id_pli', $id_pli)
            ->get()->result();
    }

    public function doc($id_doc){
        $data = $this->base_64
            ->from($this->tb_doc)
            ->where('id_document', $id_doc)
            ->get()->result();
        return count($data) ? $data[0] : NULL;
    }

    public function pli($id_pli){
        if(is_numeric($id_pli)){
            $pli = $this->base_64
                ->from($this->tb_pli)
                ->where('id_pli', $id_pli)
                ->get()->result();
            return count($pli) > 0 ? $pli[0] : NULL;
        }
        return NULL;
    }

    public function pli_view($id_pli){
        if(is_numeric($id_pli)){
            $pli = $this->ged
                ->select('*')
                ->select('tbtt.titre titre_')
                ->select('tbtpo.typologie typo')
                ->from($this->ftb_pli.' tb_pli')
                ->where('tb_pli.id_pli', $id_pli)
                ->join($this->ftb_flgTrt.' flg_ttr', 'tb_pli.flag_traitement = flg_ttr.id_flag_traitement', 'INNER')
                ->join($this->tb_data_pli.' tbdt_pli', 'tbdt_pli.id_pli = tb_pli.id_pli', 'LEFT')
                ->join($this->tb_soc.' tbsoc', 'tbsoc.id = tbdt_pli.societe', 'LEFT')
                ->join($this->tb_titre.' tbtt', 'tbtt.id = tbdt_pli.titre', 'LEFT')
                ->join($this->tb_typo.' tbtpo', 'tbtpo.id = tb_pli.typologie', 'LEFT')
                ->join($this->tb_statS.' tbstat', 'tbstat.id_statut_saisie = tbdt_pli.statut_saisie', 'LEFT')
                ->join($this->ftb_lotNum.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
                ->join($this->tb_lstCirc.' tblc', 'tbdt_pli.nom_circulaire = tblc.id', 'LEFT')
                ->get()->result();
            return count($pli) > 0 ? $pli[0] : NULL;
        }
        return NULL;
    }

    public function paiement_view($id_pli){
        $data =  $this->ged
            ->select('STRING_AGG(mode_paiement, \', \' ORDER BY mode_paiement) paie', FALSE)
            ->from($this->tb_paiement.' tbp')
            ->join($this->tb_mdPaie.' tbmd_p', 'tbp.id_mode_paiement = tbmd_p.id_mode_paiement', 'INNER')
            ->where('id_pli', $id_pli)
            ->group_by('id_pli')
            ->get()->result();
        return isset($data[0]) ? $data[0]->paie : '';
    }

    public function cheques($id_pli){
        return $this->ged
            ->where('id_pli', $id_pli)
            ->from($this->tb_cheque)
            ->get()->result();
    }
    
    public function cheques_kd($id_pli){
        return $this->ged
            ->where('id_pli', $id_pli)
			->from($this->tb_paieChqKd.' tb_chq')
			->join($this->tb_chqKd.' tb_tp', 'type_chq_kdo = tb_tp.id', 'INNER')
            ->get()->result();
	}

    public function mouvements($id_pli){
        return $this->ged
            ->select('*')
            ->select('tbtt.titre titre_')
            ->where('id_pli', $id_pli)
            ->from($this->tb_mvmnt.' tbmv')
            ->join($this->tb_titre.' tbtt', 'tbtt.id = tbmv.titre', 'LEFT')
            ->get()->result();
    }

    public function division($id_pli, $fils, $flg_tr){
        $pli = $this->pli($id_pli);
        if(is_null($pli)){
            return array(FALSE, 'Pli introuvable!');
        }
        if($flg_tr != $pli->flag_traitement){
            return array(FALSE, 'Le pli a changé d\'état!');
        }
        $this->base_64->trans_begin();
        $this->ged->trans_begin();
        $id_pli_fils = array();
        foreach ($fils as $key => $docs) {
            $sql_insert = $this->base_64
                ->set(array(
                    'pli' => $pli->pli.'_'.$key
                    ,'id_lot_numerisation' => $pli->id_lot_numerisation
                    ,'date_creation' => $pli->date_creation
                    ,'societe' => $pli->societe
                    ,'statut_saisie' => 1
                    ,'flag_traitement' => 0
                    ,'pli_parent' => $id_pli
                ))->get_compiled_insert($this->tb_pli);
            $sql_insert .= ' RETURNING id_pli ';
            $data_insert = $this->base_64->query($sql_insert)->result();
            array_push($id_pli_fils, $data_insert[0]->id_pli);
            $this->base_64
                ->set('id_pli', $data_insert[0]->id_pli)
                ->where_in('id_document', $docs)
                ->update($this->tb_doc);
            $this->ged->insert($this->tb_data_pli, array(
                'id_pli' => $data_insert[0]->id_pli
                ,'societe' => $pli->societe
                ,'statut_saisie' => 1
                ,'flag_traitement' => 0
            ));
        }
        $this->base_64
            ->set('statut_saisie', 24)
            ->set('flag_traitement', 14)
            ->where('id_pli', $id_pli)
            ->update($this->tb_pli);
        $this->ged
            ->set('statut_saisie', 24)
            ->set('flag_traitement', 14)
            ->where('id_pli', $id_pli)
            ->update($this->tb_data_pli);
        if($this->base_64->trans_status() === FALSE || $this->ged->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            return array(FALSE, 'Division échouée!');
        }else{
            $this->base_64->trans_commit();
            $this->ged->trans_commit();
            $this->CI->histo->pli($id_pli, 'division');
            $this->CI->histo->action(44, '', $id_pli);
            $this->CI->histo->plis($id_pli_fils, 'fils pli#'.$id_pli);
            return array(TRUE, '');
        }
    }

    public function unlock($id_pli, $old_flag, $new_flag){
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        try {
            $this->ged
                ->set('flag_traitement', $new_flag)
                ->where('id_pli', $id_pli)
                ->where('statut_saisie', 1)
                ->update($this->tb_data_pli);
            $this->base_64
                ->where('id_pli', $id_pli)
                ->where('flag_traitement', $old_flag)
                ->set('flag_traitement', $new_flag)
                ->update($this->tb_pli);
            if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE || $this->base_64->affected_rows() < 1 || $this->ged->affected_rows() < 1){
                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                return FALSE;
            }else{
                $this->ged->trans_commit();
                $this->base_64->trans_commit();
                return TRUE;
            }
        } catch (Exception $exc) {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> admin> model_support> unlock: '.$exc->getMessage());
            return FALSE;
        }
    }

    public function ged_data_pli($id_pli){
        $data = $this->ged
            ->select('tbdata.*', FALSE)
            ->select('pli')
            ->select('lot_scan')
            ->from($this->tb_data_pli.' tbdata')
            ->join($this->vw_pli.' tb_pli', 'tb_pli.id_pli = tbdata.id_pli', 'LEFT')
			->join($this->vw_lotN.' tblotN', 'tb_pli.id_lot_numerisation = tblotN.id_lot_numerisation', 'LEFT')
            ->where('tbdata.id_pli', $id_pli)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }

    public function ged_pjs($id_pli){
        return $this->base_64
            ->from($this->tb_doc)
            ->where('id_pli', $id_pli)
            ->where('desarchive', 1)
            ->order_by('date_creation', 'ASC')
            ->get()->result();
    }
    
	public function societe(){
		return $this->ged
			->from($this->tb_soc)
			->where('actif', 1)
			->order_by('nom_societe')
			->get()->result();
	}
    
	public function typologie(){
		return $this->ged
			->from($this->tb_typo)
			->order_by('typologie')
			->get()->result();
	}

	public function mode_paiement(){
		return $this->ged
			->from($this->tb_mdPaie)
			->order_by('mode_paiement')
			->get()->result();
    }
    
    public function group_pli1(){
        $sql_sub_w = $this->ged
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(id_pli) nb_plis', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->where('flag_traitement', 3)
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();
        $sql_sub_s = $this->ged
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(id_pli) nb_plis', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->where('flag_traitement', 4)
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();
        return $this->ged
            ->from($this->vw_grPliPos.' tbpos')
            ->join(' ('.$sql_sub_w.') tbw', 'tbw.id_gr_poss = tbpos.id_groupe_pli_possible', 'LEFT')
            ->join(' ('.$sql_sub_s.') tbs', 'tbs.id_gr_poss = tbpos.id_groupe_pli_possible', 'LEFT')
            ->select('tbpos.id_groupe_pli_possible id_pos')
            ->select('soc')
            ->select('typologie typo')
            ->select('groupe_paiement paie')
            ->select('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer nb_w')
            ->select('(CASE WHEN tbs.nb_plis IS NULL THEN 0 ELSE tbs.nb_plis END)::integer nb_s')
            ->select('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer + (CASE WHEN tbs.nb_plis IS NULL THEN 0 ELSE tbs.nb_plis END)::integer total')
            ->where('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer > 0', NULL, FALSE)
            ->or_where('(CASE WHEN tbs.nb_plis IS NULL THEN 0 ELSE tbs.nb_plis END)::integer > 0', NULL, FALSE)
            ->order_by('(CASE WHEN tbw.nb_plis IS NULL THEN 0 ELSE tbw.nb_plis END)::integer DESC')
            ->order_by('soc')
            ->order_by('typologie')
            ->order_by('groupe_paiement')
            ->get()->result();
    }

    public function group_pli1_by_older(){
        $sql_sub_s = $this->ged
            ->select('tblotn.date_courrier::date dt_cr', FALSE)
            ->select('tbgr.id_view_groupe_pli_possible id_gr_poss')
            ->select('COUNT(id_pli) nb_w', FALSE)
            ->from($this->tb_groupe_pli.' tbgr')
            ->join($this->ftb_pli.' tbpli', 'tbgr.id_groupe_pli = tbpli.id_groupe_pli', 'INNER')
            ->join($this->ftb_lotNum.' tblotn', 'tbpli.id_lot_numerisation = tblotn.id_lot_numerisation', 'INNER')
            ->where('flag_traitement', 3)
            ->group_by('tblotn.date_courrier')
            ->group_by('tbgr.id_view_groupe_pli_possible')
            ->get_compiled_select();
        $this->ged->reset_query();
        return $this->ged
            ->select('dt_cr')
            ->select('id_gr_poss id_pos')
            ->select('soc')
            ->select('tbpos.typologie typo')
            ->select('groupe_paiement paie')
            ->select('nb_w')
            ->from($this->vw_grPliPos.' tbpos')
            ->join(' ('.$sql_sub_s.') tbs', 'tbs.id_gr_poss = tbpos.id_groupe_pli_possible', 'LEFT')
            ->where('nb_w > ', 0)
            ->where('nb_w IS NOT NULL ', NULL, FALSE)
            ->order_by('dt_cr', 'ASC')
            ->order_by('nb_w', 'DESC')
            ->order_by('soc')
            ->order_by('tbpos.typologie')
            ->order_by('groupe_paiement')
            ->get()->result();
    }
    
	public function histo($id_pli){
		$sql_sub_action = $this->ged
			->select('tbact.id_action ident')
			->select('tbact.label_action lbl')
			->select('id_user par')
			->select('(split_part(date_action::varchar, \'.\', 1) || \'.0\')::timestamp without time zone moment')
            ->select('coms_trace com')
            ->select("'' motif_ko")
			->select("(null) dt_motif_ko", FALSE)
			->select("'' consigne_ko")
			->select("(null) dt_consigne", FALSE)
			->select('0 flag', FALSE)
			->from($this->tb_trace.' tbtrc')
			->join($this->tb_action.' tbact', 'tbtrc.id_action = tbact.id_action', 'INNER')
            ->where('id_pli', $id_pli)
            ->where('tbact.courrier', 1)
			->get_compiled_select();
		$this->ged->reset_query();
		$sql_sub_histo = $this->ged
			->select('flag_traitement ident')
			->select('(tbflg.flgtt_etape || \' (\' || tbflg.flgtt_etat || (CASE WHEN flag_rewrite_status_client <> 0 THEN \')\' ELSE (\' - \' || tbstats.libelle || \')\') END)) lbl')
			->select('utilisateur par')
            ->select('dt_event moment')
            ->select('comms com')
            ->select("motif_operateur motif_ko")
			->select('dt_motif_ko dt_motif_ko', FALSE)
			->select("consigne_client consigne_ko")
			->select('dt_consigne dt_consigne', FALSE)
			->select('1 flag', FALSE)
			->from($this->tb_histo.' tbh')
            ->join($this->ftb_flgTrt.' tbflg', 'tbh.flag_traitement = tbflg.id_flag_traitement', 'INNER')
            ->join($this->tb_statS.' tbstats', 'tbh.statut_pli = tbstats.id_statut_saisie', 'INNER')
			->join($this->tb_motifConsigne.' tbmotifcons', 'tbmotifcons.link_histo = tbh.link_motif_consigne', 'LEFT')
			->where('tbh.id_pli', $id_pli)
			->get_compiled_select();
		$this->ged->reset_query();
		return  $this->ged
			->from('(('.$sql_sub_action.') UNION ('.$sql_sub_histo.')) tbe')
			->join($this->tb_user.' tbu', 'tbe.par = tbu.id_utilisateur', 'LEFT')
			->order_by('moment', 'DESC')
			->order_by('flag', 'ASC')
			->get()->result();
	}

    public function correction_data_pli(){
        $plis = $this->base_64
            ->from($this->tb_pli.' t_pli')
            ->join($this->ftb_datapli.' t_data', 't_data.id_pli = t_pli.id_pli', 'LEFT')
            ->select('t_pli.id_pli')
            ->select('t_pli.societe')
            ->where('t_data.dt_enregistrement IS NULL', NULL, FALSE)
            ->get()->result();
        $data_plis = array();
        foreach ($plis as $pli) {
            $data_pli = array(
                'id_pli' => $pli->id_pli
                ,'societe' => $pli->societe
            );
            array_push($data_plis, $data_pli);
        }
        if (count($data_plis) > 0) {
            $this->ged->insert_batch($this->tb_data_pli, $data_plis);
            return 'ok=> '.count($data_plis);//var_dump($data_plis);
        }
        return 'ko';
    }
    
    public function motif_rejet(){
        return $this->ged
            ->from($this->tb_mtfRj)
            ->where('flag', 1)
            ->order_by('motif')
            ->get()->result();
    }

    public function erase_img($id_doc, $img_b64, $recto=TRUE){
        $colonne = $recto ? 'n_ima_base64_recto' : 'n_ima_base64_verso';
        $this->base_64
            ->set($colonne, $img_b64)
            ->where('id_document', $id_doc)
            ->update($this->tb_doc);
        return $this->base_64->affected_rows() == 1;
    }

    public function download_all_ci(){
        return $this->ged
            ->from($this->tb_data_pli.' tb_dtp')
            ->join($this->ftb_pli.' tb_pli', 'tb_pli.id_pli = tb_dtp.id_pli', 'INNER')
            ->join($this->ftb_lotNum.' tb_lnm', 'tb_lnm.id_lot_numerisation = tb_pli.id_lot_numerisation', 'INNER')
            ->join($this->tb_lstCirc.' tb_ci', 'tb_ci.id = tb_dtp.nom_circulaire', 'INNER')
            ->where_in('statut_saisie', array(3,11))
            ->where('dt_enregistrement:: date BETWEEN \'2019-06-18\' AND \'2019-06-24\'', NULL, FALSE)
            ->where('circulaire !=', 'FULDECO')
            ->select('tb_dtp.id_pli')
            ->select('nom_orig_fichier_circulaire')
            ->select('fichier_circulaire')
            ->get()->result();
    }
    
    public function get_motif_consigne($id_pli)
    {
        $data = $this->ged
            ->from($this->tb_motifConsigne)
            ->where('id_pli', $id_pli)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }
    
    public function operateur($type=NULL){
        if(is_null($type)){
            $this->ged->where_in('id_type_utilisateur', array(2, 3));
        }else{
            $this->ged->where('id_type_utilisateur', $type);
        }
        return $this->ged
            ->from($this->tb_user)
            ->where('supprime', 0)
            ->where('actif', 1)
            ->order_by('login')
            ->get()->result();
    }

    public function get_assignation_pli($id_pli, $traitement)
    {
        $data = $this->ged
            ->from($this->tb_assignationPli)
            ->where('id_pli', $id_pli)
            ->where('traitement', $traitement)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL;
    }

    public function save_assignation_pli($data){
        $ligne = new stdClass();
        $ligne->id_pli = $data['id_pli'];
        $ligne->operateur = $data['typage'];
        $ligne->par = (int)$this->session->id_utilisateur;
        $ligne->traitement = 0;
        $this->assignation_pli($ligne);
        $ligne->operateur = $data['saisie'];
        $ligne->traitement = 3;
        $this->assignation_pli($ligne);
        $ligne->operateur = $data['control'];
        $ligne->traitement = 7;
        $this->assignation_pli($ligne);
    }

    private function assignation_pli($ligne)
    {
        $exist = $this->ged
            ->where('id_pli', $ligne->id_pli)
            ->where('operateur', $ligne->operateur)
            ->where('traitement', $ligne->traitement)
            ->from($this->tb_assignationPli)
            ->count_all_results() > 0;
        if(is_null($ligne->operateur) || !$exist){
            $this->ged
                ->where('id_pli', $ligne->id_pli)
                ->where('traitement', $ligne->traitement)
                ->delete($this->tb_assignationPli);
            if(!is_null($ligne->operateur)){
                $this->ged->insert($this->tb_assignationPli, $ligne);
            }
        }
        $this->ged->insert($this->tb_histoAssignationPli, $ligne);
    }

}
