<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_groups_user extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    
    private $tb_user = TB_user;
    private $tb_tpUser = TB_tpUser;
    private $tb_grUser = TB_grUser;
    private $tb_assignation = TB_assignation;
    private $vw_grPliPos = Vw_grPliPos;

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    
    public function nb_all_group(){
		return $this->ged
            ->where('supprime', 0)
            ->from($this->tb_grUser)
            ->count_all_results();
    }
    
    public function get_groups_user($limit, $offset, $arg_find, $arg_ordo, $sens, $filtre, $nb_only = TRUE){
        $sql_sub_nb_user = $this->ged
				->select('tbgr.id_groupe_utilisateur')
				->select('COUNT(id_utilisateur) nb_user')
                ->from($this->tb_grUser.' tbgr')
                ->join($this->tb_user.' tbur', 'tbgr.id_groupe_utilisateur = tbur.id_groupe_utilisateur', 'LEFT')
                ->where('tbur.supprime', 0)
                ->where('tbgr.supprime', 0)
				->group_by('tbgr.id_groupe_utilisateur')
				->get_compiled_select();
        $this->ged->reset_query();
        
        $sql_sub_nb_cr = $this->ged
				->select('tbgr.id_groupe_utilisateur')
				->select('COUNT(id_view_groupe_pli_possible) nb_cr', FALSE)
				->select('\'[\' || STRING_AGG(typologie || \' | \' || groupe_paiement || \' | \' || soc, \'], [\' order by typologie) || \']\' cr', FALSE)
                ->from($this->tb_grUser.' tbgr')
                ->join($this->tb_assignation.' tbass', 'tbgr.id_groupe_utilisateur = tbass.id_groupe_utilisateur', 'LEFT')
                ->join($this->vw_grPliPos.' vp', 'tbass.id_view_groupe_pli_possible = vp.id_groupe_pli_possible', 'LEFT')
                ->where('tbgr.supprime', 0)
				->group_by('tbgr.id_groupe_utilisateur')
				->get_compiled_select();
		$this->ged->reset_query();

        $this->ged
            ->from($this->tb_grUser.' tb_gr_u')
            ->join('('.$sql_sub_nb_user.') sub_nb_user', 'tb_gr_u.id_groupe_utilisateur = sub_nb_user.id_groupe_utilisateur', 'LEFT')
            ->join('('.$sql_sub_nb_cr.') sub_nb_cr', 'tb_gr_u.id_groupe_utilisateur = sub_nb_cr.id_groupe_utilisateur', 'LEFT')
            ->where('tb_gr_u.supprime', 0)
            ->select('tb_gr_u.id_groupe_utilisateur id')
            ->select('tb_gr_u.nom_groupe_utilisateur nom')
            ->select('tb_gr_u.actif actif')
            ->select('sub_nb_user.nb_user nb_u')
            ->select('sub_nb_cr.cr critere')
            ->select('sub_nb_cr.nb_cr nb_critere');

        if ($arg_find != '') {
            $this->ged
                ->group_start()
                    ->like('lower(tb_gr_u.nom_groupe_utilisateur)', $arg_find)
                    ->or_like('lower(sub_nb_cr.cr)', $arg_find)
                ->group_end();
        }
        if (!$nb_only) {
            $this->ged
                ->order_by($arg_ordo, strtoupper($sens))
                ->limit($limit, $offset);
			return $this->ged->get()->result();
        }
        return $this->ged->count_all_results();
    }

    public function suppr_gr($id_gr){
        if(!is_numeric($id_gr)){
            return FALSE;
        }
        $grp_usr = $this->ged
            ->from($this->tb_grUser)
            ->where('id_groupe_utilisateur', $id_gr)
            ->get()->result();
        if(count($grp_usr) > 0){
            $this->ged->trans_start();
            $this->ged
                ->set('id_groupe_utilisateur', NULL)
                ->where('id_groupe_utilisateur', $id_gr)
                ->update($this->tb_user);
            $this->ged->set('nom_groupe_utilisateur', $grp_usr[0]->nom_groupe_utilisateur.'_deleted_'.date('YmdHis'));
            $this->ged
                ->set('supprime', 1)
                ->where('id_groupe_utilisateur', $id_gr)
                ->update($this->tb_grUser);
            $this->ged->trans_complete();
            return !($this->ged->trans_status() === FALSE);
        }
        return FALSE;
    }

    public function exist_nom_grp_user($nom, $id_grp_user=NULL){
        if(!is_null($id_grp_user)){
            $this->ged->where('id_groupe_utilisateur !=', $id_grp_user);
        }
        $data = $this->ged
            ->from($this->tb_grUser)
            ->where('nom_groupe_utilisateur', $nom)
            ->get()->result();
        return count($data) > 0;
    }

    public function new_grp_user($data){
        $sql = $this->ged
            ->set($data)
            ->get_compiled_insert($this->tb_grUser);
        $sql .= ' RETURNING id_groupe_utilisateur ';
        $data = $this->ged->query($sql)->result();
        return $this->ged->affected_rows() > 0 ? $data[0]->id_groupe_utilisateur : NULL;
    }

    public function modif_grp_user($id, $data){
        if (!is_numeric($id)) {
            return FALSE;
        }
        $this->ged
            ->set($data)
            ->where('id_groupe_utilisateur', $id)
            ->update($this->tb_grUser);
        return $this->ged->affected_rows() > 0;
    }

    public function assignation_new($id_grp_user, $data){
        $this->ged->trans_start();
        $this->ged->where('id_groupe_utilisateur', $id_grp_user)->delete($this->tb_assignation);
        if(count($data) > 0){
            $this->ged->insert_batch($this->tb_assignation, $data);
        }
        $this->ged->trans_complete();
        if($this->ged->trans_status() === FALSE){
            log_message('error', 'admin>model_groups_user>assignation_new:assignation échouée pour le groupe_utilisateur#'.$id_grp_user);
        }
    }

    public function modifier($id, $data){
        $this->ged->set($data)->where('id_groupe_utilisateur', $id)->update($this->tb_grUser);
        return $this->ged->affected_rows() > 0;
    }

    public function gr_pli_pos($order_by=NULL){
        if($order_by == 'id_groupe_paiement'){
            $this->ged
                ->select('id_groupe_paiement focused', FALSE)
                ->select('groupe_paiement grp_opt', FALSE)
                ->select('typologie opt1', FALSE)
                ->select('soc opt2', FALSE)
                ->order_by('id_groupe_paiement')
                ->order_by('typologie')
                ->order_by('soc');
        }elseif ($order_by == 'soc') {
            $this->ged
                ->select('id_soc focused', FALSE)
                ->select('soc grp_opt', FALSE)
                ->select('typologie opt1', FALSE)
                ->select('groupe_paiement opt2', FALSE)
                ->order_by('soc')
                ->order_by('typologie')
                ->order_by('id_groupe_paiement');
        }else{
            $this->ged
                ->select('id_typologie focused', FALSE)
                ->select('typologie grp_opt', FALSE)
                ->select('groupe_paiement opt1', FALSE)
                ->select('soc opt2', FALSE)
                ->order_by('typologie')
                ->order_by('id_groupe_paiement')
                ->order_by('soc');
        }
        return $this->ged
            ->select('*')
            ->from($this->vw_grPliPos)
            ->get()->result();
    }

    public function nb_grp_pli_poss(){
        return $this->ged->count_all($this->vw_grPliPos);
    }

    public function grp_user_assignation_pos($id_grp_user){
        if(!is_numeric($id_grp_user)){
            return array();
        }
        return $this->ged
            ->from($this->tb_grUser.' tb_gu')
            ->join($this->tb_assignation.' tb_a', 'tb_a.id_groupe_utilisateur = tb_gu.id_groupe_utilisateur', 'LEFT')
            ->where('tb_gu.id_groupe_utilisateur', $id_grp_user)
            ->get()->result();
    }

    public function member_grp($id_gr){
        if(!is_numeric($id_gr)){
            return array();
        }
        return $this->ged
            ->select('id_utilisateur')
            ->from($this->tb_grUser.' tb_gru')
            ->join($this->tb_user.' tb_u', 'tb_gru.id_groupe_utilisateur = tb_u.id_groupe_utilisateur', 'INNER')
            ->where('tb_gru.id_groupe_utilisateur', $id_gr)
            ->where('tb_u.supprime', 0)
            ->where('id_type_utilisateur', 2)
            ->order_by('login')
            ->get()->result();
    }

    public function operateur(){
        return $this->ged
            ->select('id_utilisateur')
            ->select('login')
            ->from($this->tb_user)
            ->where('supprime', 0)
            ->where('id_type_utilisateur', 2)
            ->order_by('login')
            ->get()->result();
    }

    public function set_members($id, $members){
        if(!is_numeric($id)){
            return FALSE;
        }
        $this->ged->trans_start();
        $this->ged
            ->set('id_groupe_utilisateur', NULL)
            ->where('id_groupe_utilisateur', $id)
            ->update($this->tb_user);
        if(!is_null($members)){
            $this->ged
                ->set('id_groupe_utilisateur', $id)
                ->where_in('id_utilisateur', $members)
                ->update($this->tb_user);
        }
        $this->ged->trans_complete();
        if($this->ged->trans_status() === FALSE){
            log_message('error', 'admin>model_groups_user>set_members: setting membre échouée pour le groupe_utilisateur#'.$id);
            return FALSE;
        }
        return TRUE;
    }

}
