<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cality extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;

    private $ftb_pli = FTB_pli;
    private $tb_trace = TB_trace;
    private $tb_data_pli = TB_data_pli;
    private $tb_categEr = TB_categEr;
    private $ftb_decoup = FTB_decoupage;
    private $tb_ctrlPliEr = TB_ctrlPliEr;
    private $vw_erreurCtrl = VW_erreurCtrl;
    // private $output_ctrl = array(21, 22, 23, 24, 25);
    private $output_ctrl = array(21,22,23,24,25,102,103,124,125);
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function nb_echantillon($dt, $taux=NULL){
        if(!empty($dt[0])){
            $this->ged->where('date_action::DATE >= \''.$dt[0].'\'', NULL, FALSE);
        }
        if(!empty($dt[1])){
            $this->ged->where('date_action::DATE <= \''.$dt[1].'\'', NULL, FALSE);
        }
        if(!is_null($taux)){
            $this->ged->where('tbdec.taux', $taux);
        }
        $sql = $this->ged
            ->distinct()
            ->select('tbpli.id_pli')
            ->from($this->ftb_pli.' tbpli')
            ->where('flag_echantillon', 1)
            ->join($this->tb_trace.' tbtrace', 'tbtrace.id_pli = tbpli.id_pli', 'INNER')
            ->join($this->ftb_decoup.' tbdec', 'tbdec.id_decoupage = tbpli.id_decoupage', 'INNER')
            ->where_in('id_action', $this->output_ctrl)
            ->get_compiled_select();
        $this->ged->reset_query();
        return $this->ged->from('('.$sql.') sub')->count_all_results();
    }

    public function echantillon_avec_faute($dt, $taux=NULL){
        if(!empty($dt[0])){
            $this->ged->where('date_action::DATE >= \''.$dt[0].'\'', NULL, FALSE);
        }
        if(!empty($dt[1])){
            $this->ged->where('date_action::DATE <= \''.$dt[1].'\'', NULL, FALSE);
        }
        if(!is_null($taux)){
            $this->ged->where('tbdec.taux', $taux);
        }
        $sql = $this->ged
            ->distinct()
            ->select('tbpli.id_pli')
            ->from($this->ftb_pli.' tbpli')
            ->where('flag_echantillon', 1)
            ->join($this->tb_trace.' tbtrace', 'tbtrace.id_pli = tbpli.id_pli', 'INNER')
            // ->join($this->tb_data_pli.' tbdata', 'tbdata.id_pli = tbpli.id_pli', 'INNER')
            // ->join($this->tb_categEr.' tb_er', 'tbdata.id_erreur = tb_er.id_erreur', 'INNER')
            ->join($this->ftb_decoup.' tbdec', 'tbdec.id_decoupage = tbpli.id_decoupage', 'INNER')
            ->join($this->tb_ctrlPliEr.' tb_pli_er', 'tb_pli_er.id_pli = tbpli.id_pli', 'INNER')
            ->where_in('id_action', $this->output_ctrl)
            ->get_compiled_select();
        $this->ged->reset_query();
        return $this->ged->from('('.$sql.') sub')->count_all_results();
    }

    public function fautes($dt, $taux=NULL){
        if(!empty($dt[0])){
            $this->ged->where('date_action::DATE >= \''.$dt[0].'\'', NULL, FALSE);
        }
        if(!empty($dt[1])){
            $this->ged->where('date_action::DATE <= \''.$dt[1].'\'', NULL, FALSE);
        }
        if(!is_null($taux)){
            $this->ged->where('tbdec.taux', $taux);
        }
        $sql = $this->ged
            ->distinct()
            ->select('tbpli.id_pli id_pli')
            ->from($this->ftb_pli.' tbpli')
            ->where('flag_echantillon', 1)
            ->join($this->tb_trace.' tbtrace', 'tbtrace.id_pli = tbpli.id_pli', 'INNER')
            // ->join($this->tb_data_pli.' tbdata', 'tbdata.id_pli = tbpli.id_pli', 'INNER')
            // ->join($this->tb_categEr.' tb_er', 'tbdata.id_erreur = tb_er.id_erreur', 'INNER')
            ->join($this->ftb_decoup.' tbdec', 'tbdec.id_decoupage = tbpli.id_decoupage', 'INNER')
            ->join($this->tb_ctrlPliEr.' tb_pli_er', 'tb_pli_er.id_pli = tbpli.id_pli', 'INNER')
            ->where_in('id_action', $this->output_ctrl)
            ->get_compiled_select();
        $this->ged->reset_query();
        return $this->ged
            ->select('tb_pli_er.id_categorie_erreur id_err')
            ->select('tb_er.erreur err')
            ->select('COUNT(tb_pli_er.id_pli) nb')
            ->from('('.$sql.') sub')
            // ->join($this->tb_data_pli.' tbdata', 'tbdata.id_pli = sub.id_pli', 'INNER')
            ->join($this->tb_ctrlPliEr.' tb_pli_er', 'tb_pli_er.id_pli = sub.id_pli', 'INNER')
            ->join($this->tb_categEr.' tb_er', 'tb_pli_er.id_categorie_erreur = tb_er.id_erreur', 'INNER')
            ->group_by('tb_pli_er.id_categorie_erreur')
            ->group_by('tb_er.erreur')
            ->order_by('nb', 'DESC')
            ->order_by('err', 'ASC')
            ->get()->result();
    }

    public function erreur_echantillon($dt, $taux=NULL){
        if(!empty($dt[0])){
            $this->ged->where('dt_ctrl::DATE >= \''.$dt[0].'\'', NULL, FALSE);
        }
        if(!empty($dt[1])){
            $this->ged->where('dt_ctrl::DATE <= \''.$dt[1].'\'', NULL, FALSE);
        }
        if(!is_null($taux)){
            $this->ged->where('taux', $taux);
        }
        return $this->ged
            ->from($this->vw_erreurCtrl)
            ->order_by('erreur')
            ->get()->result();
    }

}
