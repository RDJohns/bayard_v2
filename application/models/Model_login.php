<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    private $push;
    
    private $tb_user = TB_user;
    private $tb_tpUser = TB_tpUser;
    private $tb_tpOpAcces = TB_tpOpAcces;
    private $tb_opAcces = TB_opAcces;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
    }

    public function connexion($login, $pswd) {
        $data = $this->ged
                ->from($this->tb_user)
                ->where('login', $login)
                //->where('pass', $pswd)
                ->where('actif', 1)
                ->where('supprime', 0)
                ->limit(1)
                ->get()->result();
        if(count($data) == 1){
            $this->CI->load->library('encrypt');
            if($pswd == $this->CI->encrypt->decode($data[0]->pass, Kryptxt)){
                return $data[0];
            }
        }
        return NULL;
    }

    public function my_acces_push($id_user){
        $data = $this->push
            ->from($this->tb_opAcces.' opa')
            ->join($this->tb_tpOpAcces.' tp', 'opa.id_type_acces = tp.id', 'INNER')
            ->where('actif', 1)
            ->where('id_user', $id_user)
            ->get()->result();
        return count($data) > 0 ? $data[0] : NULL; 
    }
    
    public function valide_user(){
        return $this->ged
            ->from($this->tb_user)
            ->where('id_utilisateur', $this->CI->session->id_utilisateur)
            ->where('actif', 1)
            ->where('supprime', 0)
            ->count_all_results() > 0;
    }

}
