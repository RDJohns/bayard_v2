<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_reliquat extends CI_Model
{

    private $CI;

    private $ged;
    private $base_64;
    private $base_reb_temp;

    private $tb_pli = TB_pli;
    private $tb_cheque = TB_cheque;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_dataPli = TB_data_pli;
    private $tb_document = TB_document;
    private $tb_user = TB_user;
    private $tb_statut = TB_statS;
    private $tb_etat = TB_etatMatchage;
    private $tb_etatChq = TB_etatChq;
    private $vw_pli = Vw_pli;
    private $vw_lot = Vw_lotNum;
    private $vw_last = Vw_lastHisto;
    private $vw_flagtt = Vw_flgTrt;
    private $f_pli = FTB_pli;
    private $sort_colons = array('date_courrier', 'id_pli', 'pli', 'lot_scan', 'flgtt_etape', 'flgtt_etat', 'libelle', 'cmc7', 'montant::float', 'lbl_etat_chq', 'commande');

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function get_statut_chq(){
        return $this->ged->select('*')
                        ->from($this->tb_etatChq)
                        ->where('id_etat_chq != ',4)
                        ->order_by('id_etat_chq','asc')
                        ->get()
                        ->result();
    }

    public function set_statut($statut){
        if(empty($statut)){
            $arr_statut = array();
            $stat = $this->get_statut_chq();
            foreach ($stat as $st){
                array_push($arr_statut, $st->id_etat_chq);
            }
            return $arr_statut;
        }
        else{
            return $statut;
        }
    }

    public function _order_by(){
        $col = $this->input->post('order[0][column]');
        $dir = $this->input->post('order[0][dir]');
        $order = '';
        if(!is_null($col)){
            $order .= 'ORDER BY '.$this->sort_colons[$col].' '.$dir;
        }
        return $order;
    }

    public function get_list_reliquat($date,$societe,$length,$start,$nreb_date_deb, $nreb_date_fn,$statut){
        $statut = $this->set_statut($statut);
        $query =  $this->ged->select('date_courrier,'.$this->vw_pli.'.id_pli,'.$this->vw_pli.'.pli,lot_scan,flgtt_etape,flgtt_etat,CASE WHEN flgtt_etat = \'Traité\' OR flgtt_etat = \'Clôturé\' THEN '.$this->tb_statut.'.libelle ELSE \'\' END as libelle,initcap(traitement) as traitement,cmc7,montant,commande,lbl_etat_chq')
                        ->from($this->vw_pli)
                        ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
                        ->join($this->tb_etatChq, $this->tb_etatChq.'.id_etat_chq = '.$this->tb_cheque.'.id_etat_chq','left')
                        ->join($this->vw_lot, $this->vw_lot.'.id_lot_numerisation = '.$this->vw_pli.'.id_lot_numerisation')
                        ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
                        ->join($this->tb_statut, $this->tb_dataPli.'.statut_saisie = '.$this->tb_statut.'.id_statut_saisie')
                        ->join($this->vw_last, $this->vw_last.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
                        ->join($this->vw_flagtt, $this->vw_flagtt.'.id_flag_traitement = '.$this->vw_last.'.flag_traitement', 'left')
                        ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
                        ->where_not_in($this->tb_cheque.'.id_etat_chq', array(4))
                        //->where($this->vw_pli.'.flag_traitement != ', 26)
                        //->where_not_in($this->vw_pli.'.flag_traitement', array(16,21, 23, 25))
                        //->where_in($this->tb_dataPli.'.statut_saisie', array(1,7,9,10,11))
                        //->where('date_courrier::date < \''.$date.'\'',null,false)
                        ->where('date_courrier >=',''.$nreb_date_deb.'')
                        ->where('date_courrier <=',''.$nreb_date_fn.'')
                        ->where($this->tb_dataPli.'.societe', $societe)
                        ->where_in($this->tb_cheque.'.id_etat_chq',$statut)
                        ///->order_by('date_courrier', 'asc')
                        //->limit($length)
                        ->get_compiled_select();
        $query .= ' '.$this->_order_by();
        $query .= ' LIMIT '.$length.' OFFSET '.$start;
        return $this->ged->query($query)->result();
    }

    public function get_nb_reliquat($date, $societe,$nreb_date_deb, $nreb_date_fn,$statut){
        $statut = $this->set_statut($statut);
        return $this->ged->select('COUNT(*) AS nb_reliquat')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->vw_lot, $this->vw_lot.'.id_lot_numerisation = '.$this->vw_pli.'.id_lot_numerisation')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_dataPli.'.statut_saisie = '.$this->tb_statut.'.id_statut_saisie')
            ->where_not_in($this->tb_cheque.'.id_etat_chq', array(4))
            //->where($this->vw_pli.'.flag_traitement != ', 26)
            //->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,23,25))
            //->where_in($this->tb_dataPli.'.statut_saisie', array(1,7,9,10,11))
           // ->where('date_courrier::date < \''.$date.'\'',null,false)
            ->where('date_courrier >=',''.$nreb_date_deb.'')
            ->where('date_courrier <=',''.$nreb_date_fn.'')
            ->where($this->tb_dataPli.'.societe', $societe)
            ->where_in($this->tb_cheque.'.id_etat_chq',$statut)
            ->get()
            ->result();
    }

    public function get_list_nosaisie($date,$societe,$length,$start,$nsaisi_date_deb,$nsaisi_date_fn){
        $date_aujourdui = date("Y-m-d");
        $query =  $this->ged->select('date_courrier,'.$this->vw_pli.'.id_pli,'.$this->vw_pli.'.pli,lot_scan,flgtt_etape,flgtt_etat,CASE WHEN flgtt_etat = \'Traité\' OR flgtt_etat = \'Clôturé\' THEN '.$this->tb_statut.'.libelle ELSE \'\' END as libelle,initcap(traitement) as traitement,cmc7,montant,commande,lbl_etat_chq')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_etatChq, $this->tb_etatChq.'.id_etat_chq = '.$this->tb_cheque.'.id_etat_chq','left')
            ->join($this->vw_lot, $this->vw_lot.'.id_lot_numerisation = '.$this->vw_pli.'.id_lot_numerisation')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_dataPli.'.statut_saisie = '.$this->tb_statut.'.id_statut_saisie')
            ->join($this->vw_last, $this->vw_last.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            ->join($this->vw_flagtt, $this->vw_flagtt.'.id_flag_traitement = '.$this->vw_last.'.flag_traitement', 'left')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            ->where($this->vw_pli.'.flag_traitement < ', 5)
           // ->where('date_courrier::date = \''.$date.'\'',null,false)
            ->where('date_courrier >=',''.$nsaisi_date_deb.'')
            ->where('date_courrier <=',''.$nsaisi_date_fn.'')
            ->where('"date_courrier"::date + interval \'3 day\' < \''.$date_aujourdui.'\'::date')//AND (now() > "date_courrier"::date + interval '3 day')
            ->where($this->tb_dataPli.'.societe', $societe)
            ->order_by('date_courrier', 'asc')
            ->limit($length)
            ->get_compiled_select();
        $query = $query.' OFFSET '.$start;
        return $this->ged->query($query)->result();
    }

    public function get_nb_nosaisie($date, $societe,$nsaisi_date_deb,$nsaisi_date_fn){
        $date_aujourd = date("Y-m-d");
        return $this->ged->select('COUNT(*) AS nb_reliquat')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->vw_lot, $this->vw_lot.'.id_lot_numerisation = '.$this->vw_pli.'.id_lot_numerisation')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_dataPli.'.statut_saisie = '.$this->tb_statut.'.id_statut_saisie')
            ->where($this->vw_pli.'.flag_traitement < ', 5)
          //  ->where('date_courrier::date = \''.$date.'\'',null,false)
            ->where('date_courrier >=',''.$nsaisi_date_deb.'')
            ->where('date_courrier <=',''.$nsaisi_date_fn.'')
            ->where('"date_courrier"::date + interval \'3 day\' < \''.$date_aujourd.'\'::date')//AND (now() > "date_courrier"::date + interval '3 day')
            ->where($this->tb_dataPli.'.societe', $societe)
            ->get()
            ->result();
    }


    public function get_list_reliquat_a_exporter($societe,$nreb_date_deb,$nreb_date_fn,$statut){
        $statut = $this->set_statut($statut);
        $query =  $this->ged->select('date_courrier,'.$this->vw_pli.'.id_pli,'.$this->vw_pli.'.pli,lot_scan,'.$this->tb_statut.'.libelle,initcap(traitement) as traitement,cmc7,montant,commande,lbl_etat_chq')
                        ->from($this->vw_pli)
                        ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli')
                        ->join($this->tb_etatChq, $this->tb_etatChq.'.id_etat_chq = '.$this->tb_cheque.'.id_etat_chq','left')
                        ->join($this->vw_lot, $this->vw_lot.'.id_lot_numerisation = '.$this->vw_pli.'.id_lot_numerisation')
                        ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
                        ->join($this->tb_statut, $this->tb_dataPli.'.statut_saisie = '.$this->tb_statut.'.id_statut_saisie')
                        ->join($this->vw_last, $this->vw_last.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
                        ->join($this->vw_flagtt, $this->vw_flagtt.'.id_flag_traitement = '.$this->vw_last.'.flag_traitement', 'left')
                        ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
                        ->where_not_in($this->tb_cheque.'.id_etat_chq', array(4))
                        //->where($this->vw_pli.'.flag_traitement != ', 26)
                        //->where_not_in($this->vw_pli.'.flag_traitement', array(16,21,23,25))
                        ->where_not_in($this->tb_dataPli.'.statut_saisie', array(3,4))
                        //->where('date_courrier::date < \''.$date.'\'',null,false)
                        ->where('date_courrier >=',''.$nreb_date_deb.'')
                        ->where('date_courrier <=',''.$nreb_date_fn.'')
                        ->where($this->tb_dataPli.'.societe', $societe)
                        ->where_in($this->tb_cheque.'.id_etat_chq', $statut)
                        ->order_by('date_courrier', 'asc')
                        ->get_compiled_select();
        return $this->ged->query($query)->result();
    }

    public function get_list_nosaisie_a_exporter($societe, $nsaisi_date_deb, $nsaisi_date_fn){

        $date_aujourdui = date("Y-m-d");
        $query =  $this->ged->select('date_courrier,'.$this->vw_pli.'.id_pli,'.$this->vw_pli.'.pli,lot_scan,'.$this->tb_statut.'.libelle,initcap(traitement) as traitement,cmc7,montant,commande,lbl_etat_chq')
            ->from($this->vw_pli)
            ->join($this->tb_cheque, $this->tb_cheque.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            ->join($this->tb_etatChq, $this->tb_etatChq.'.id_etat_chq = '.$this->tb_cheque.'.id_etat_chq','left')
            ->join($this->vw_lot, $this->vw_lot.'.id_lot_numerisation = '.$this->vw_pli.'.id_lot_numerisation')
            ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->vw_pli.'.id_pli')
            ->join($this->tb_statut, $this->tb_dataPli.'.statut_saisie = '.$this->tb_statut.'.id_statut_saisie')
            ->join($this->vw_last, $this->vw_last.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            ->join($this->vw_flagtt, $this->vw_flagtt.'.id_flag_traitement = '.$this->vw_last.'.flag_traitement', 'left')
            ->join($this->f_pli, $this->f_pli.'.id_pli = '.$this->vw_pli.'.id_pli', 'left')
            ->where($this->vw_pli.'.flag_traitement < ', 5)
           // ->where('date_courrier::date = \''.$date.'\'',null,false)
            ->where('date_courrier >=',''.$nsaisi_date_deb.'')
            ->where('date_courrier <=',''.$nsaisi_date_fn.'')
            ->where('"date_courrier"::date + interval \'3 day\' < \''.$date_aujourdui.'\'::date')//AND (now() > "date_courrier"::date + interval '3 day')
            ->where($this->tb_dataPli.'.societe', $societe)
            ->order_by('date_courrier', 'asc')
            ->get_compiled_select();
        return $this->ged->query($query)->result();

    }

}