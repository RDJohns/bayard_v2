<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Suivi15 extends CI_Model
{

	private $suiviGedBayardVvt 		= "suivi_ged_bayard_vvt";
	private $suiviBayardTraitement  = "suivi_bayard_traitement";
	private $selectData 			= "date_traitement, heure, societe, nature nature_traitement, nb_pli_type, duree_typage,nb_pli_saisie, duree_saisie";
	private $heureMin               = array(1=>"00:00",2=>"00:15+01",3=>"00:30+01",3=>"00:45+01");
	private	$heureMax               = array(1=>"00:15",2=>"00:30",3=>"00:45",3=>"00:59+15");
	// private heureDlancment          = 7,22
	//SELECT *from public.suivi_ged_bayard_vvt('2020-01-01 06:00:00','2020-01-01 07:00:00+15' );
	private $CI;
	private $ged;
	
    public function __construct()
    {
        parent::__construct();
        $this->CI  =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
	}

    public function insertBayardVivetic()
	{ 
		$arrayLimit 	  = $this->dernierLigne();
		$natureTraitement = "1";
		
		$heureInt	      = date("H");
		$dateMin          = date("Y-m-d")." ".$heureInt.":".$arrayLimit["min"];
		$dateMax          = date("Y-m-d")." ".$heureInt.":".$arrayLimit["max"];
		
		$temps            = $arrayLimit["temps"];
		$heure            = $arrayLimit["max"];
		
		$dateTraitement   = date("Y-m-d");
		$limitHeure		  = $arrayLimit["sup_heure"];
		$query 			  = $this->ged->query("SELECT ".$this->selectData." from public.suivi_ged_bayard_vvt('".$limitHeure."');");
		$select			  = $query->result_array();
		$bufferSelect     = $select;
		
		$arrayNone 		  = array(
			array("date_traitement"=>$dateTraitement,
						   "heure"=>date("H:i:s"),
						   "societe"=>1,
						   "nature_traitement"=>$natureTraitement,
						   "nb_pli_type"=>0,
						   "duree_typage"=>0,
						   "nb_pli_saisie"=>0,
						   "duree_saisie"=>0,
						   "temps"=>$temps
						   ),
			array("date_traitement"=>$dateTraitement,
						   "heure"=>date("H:i:s"),
						   "societe"=>2,
						   "nature_traitement"=>$natureTraitement, 
						   "nb_pli_type"=>0,
						   "duree_typage"=>0,
						   "nb_pli_saisie"=>0,
						   "duree_saisie"=>0,
						   "temps"=>$temps
						   ));
		$select[0]["temps"] = $select[1]["temps"] = $temps;				   
		
		echo $sql = "SELECT *from public.suivi_ged_bayard_vvt('".$limitHeure."');";
		
		
		if(count($bufferSelect) > 0)
		{
			$this->ged->insert_batch($this->suiviBayardTraitement,$select);
		}
		else
		{	
			$this->ged->insert_batch($this->suiviBayardTraitement,$arrayNone);
			
		}
	}
	
	
	public function dernierLigne()
	{
		$select      = $this->ged->select('*')->order_by('now', 'DESC')->limit(1)
					    ->get($this->suiviBayardTraitement)->result_array();
		
		$heureClause = $select[0]["date_traitement"]." ".$select[0]["heure"];
		$arrayLimit  = array("min"=>"00:00","max"=>"00:15","temps"=>1,"sup_heure"=>$heureClause);
		
		if(count($select) > 0 && intval($select[0]["temps"]) < 4 && intval($select[0]["temps"]) > 0 )
		{
			$temps = intval($select[0]["temps"])+1;
			
			$arrayLimit = array("min"=>$this->heureMin[intval($select[0]["temps"])],"max"=>$this->heureMax[intval($select[0]["temps"])],"temps"=>$temps,"sup_heure"=>$heureClause);
			return $arrayLimit;
		}
		return $arrayLimit;
	}
	
	
	
}