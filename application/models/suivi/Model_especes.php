<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_especes extends CI_Model
{
    private $tb_flgTrt = TB_flgTrt;
    private $tb_statS = TB_statS;

    public function __construct()
    {
        parent::__construct();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }
    
    //recuperer les id pli payement especes
    function getIdsPli()
    {
        $sql = "
            select paiement_espece.id_pli
            from paiement_espece where montant <> '0'
        ";
        $query = $this->ged->query($sql);
        return $this->getImplodeData($query);
    }
     
    //implode an array 
    public function getImplodeData($data)
    {
        $resultats = array();
        if($data) {
            $cpt = 0;
            foreach ($data->result() as $res)
            {
                $resultats [$cpt] = $res->id_pli;                
                $cpt++;
            }
        }
        $implode = implode(',',$resultats);
        return $implode;
    }

    //get especes 
    function getPaieEspeces($where_pli, $where_f_pli, $where, $lenght = null, $start = null,$date_recep1,$date_recep2)
    {
        $where_limit = "";
         if($lenght != -1) {
             if($lenght != null and $start != null) {
 
                 $where_limit = "
                     LIMIT ".$lenght."
                     OFFSET ".$start."
                 ";
             };
         } else {
             $where_limit = "
                 LIMIT null
                 OFFSET null
             ";
        } 
        if($where_f_pli != '') {
             $where_f_pli = "where ".$where_f_pli;
        }
            
        //var_dump($where_f_pli);die;
         $sql = "
            select 
                paiement_espece.id_pli, 
                montant,date_courrier,
                dt_event,soc.societe,
                flag_traitement,
                lot_scan,
                commande,
                libelle,statut,id_statut_saisie,id_flag_traitement
                from paiement_espece 
            left join
            (
            select 
                f_pli.id_pli,
                f_pli.societe,
                date_courrier::date as date_courrier, 
                dt_event::date as dt_event,
                histo_pli.flag_traitement,
                histo_pli.id,
                lot_scan,
                commande,
                libelle,
                f_flag_traitement.traitement as statut,id_statut_saisie,id_flag_traitement
            from f_pli
            left join f_lot_numerisation  on f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation 
            left JOIN data_pli on f_pli.id_pli = data_pli.id_pli
		    left JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
		    left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement 
            left join f_max_histo_pli_saisie('".$date_recep1."' , '".$date_recep2."', '".$where_pli."') as h on h.id_pli = f_pli.id_pli 
            inner join histo_pli on histo_pli.id = h.id 
            ".$where_f_pli." 
            order by date_courrier::date 
            )f_data on f_data.id_pli = paiement_espece.id_pli
            left join
            (
            select id as soc_id,societe from societe 
            )soc on soc.soc_id = f_data.societe
            where montant <> '0'
         ".$where."  order by id_pli, dt_event ".$where_limit;
       // echo $sql;die;
        $query = $this->ged->query($sql);
        return $query->result();
 
    }

    //count result 
    public function countResult($where_pli, $where_f_pli, $where,$date_recep1,$date_recep2)
    {
        if($where_f_pli != '') {
            $where_f_pli = "where ".$where_f_pli;
        }
        $sql = "select 
                count(*)
                from paiement_espece 
            left join
            (
                select 
                f_pli.id_pli,
                f_pli.societe,
                date_courrier::date as date_courrier, 
                dt_event::date as dt_event,
                histo_pli.flag_traitement,
                histo_pli.id,
                lot_scan,
                commande,
                libelle,
                f_flag_traitement.traitement as statut,id_statut_saisie,id_flag_traitement
            from f_pli
            left join f_lot_numerisation  on f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation 
            left JOIN data_pli on f_pli.id_pli = data_pli.id_pli
		    left JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
		    left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement 
            left join f_max_histo_pli_saisie('".$date_recep1."' , '".$date_recep2."', '".$where_pli."') as h on h.id_pli = f_pli.id_pli 
            inner join histo_pli on histo_pli.id = h.id 
            ".$where_f_pli." 
            order by date_courrier::date 
            )f_data on f_data.id_pli = paiement_espece.id_pli
            left join
            (
            select id as soc_id,societe from societe 
            )soc on soc.soc_id = f_data.societe
            where montant <> '0'
         ".$where;
        
        $query = $this->ged->query($sql);
        return $query->result();
    }
    
    //sum model 
    function sumEspeces($where_pli, $where_f_pli, $where,$date_recep1,$date_recep2)
    {
        if($where_f_pli != '') {
            $where_f_pli = "where ".$where_f_pli;
        }
        $sql = "
                select 
                sum(REPLACE (montant, ',', '.')::float)
                from paiement_espece 
            left join
                (
                    select 
                    f_pli.id_pli,
                    f_pli.societe,
                    date_courrier::date as date_courrier, 
                    dt_event::date as dt_event,
                    histo_pli.flag_traitement,
                    histo_pli.id,
                    lot_scan,
                    commande,
                    libelle,
                    f_flag_traitement.traitement as statut,id_statut_saisie,id_flag_traitement
                from f_pli
                left join f_lot_numerisation  on f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation 
                left JOIN data_pli on f_pli.id_pli = data_pli.id_pli
                left JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
                left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement 
                left join f_max_histo_pli_saisie('".$date_recep1."' , '".$date_recep2."', '".$where_pli."') as h on h.id_pli = f_pli.id_pli 
                inner join histo_pli on histo_pli.id = h.id 
                ".$where_f_pli." 
                order by date_courrier::date 
                )f_data on f_data.id_pli = paiement_espece.id_pli
                left join
                (
                select id as soc_id,societe from societe 
                )soc on soc.soc_id = f_data.societe
                where montant <> '0'
             ".$where;
            //echo $sql;die;
            $query = $this->ged->query($sql);
            return $query->result();
    }

    public function flag_traitement(){
        return $this->base_64
            ->from($this->tb_flgTrt)
            ->where('filter_display', 1)
            ->order_by('traitement')
            ->get()->result();
    }

    public function status(){
        return $this->ged
            ->from($this->tb_statS)
            ->where('actif', 1)
            ->order_by('libelle')
            ->get()->result();
    }
}
