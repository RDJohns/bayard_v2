<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_indicateur extends CI_Model{

    private $CI;

    private $ged;
    private $base_64;
	
	protected $select_ind       		= array('date_courrier','date_deb','date_fin','pli_recu','pli_typage_fini','pli_saisie_finie','pli_controle_fini','pli_termine');	
    protected $column_order_ind  		= array('date_courrier','pli_recu','pli_typage_fini','pli_saisie_finie','pli_controle_fini','pli_termine');
    protected $column_search_ind		= array('date_courrier::text','pli_recu::text','pli_typage_fini::text','pli_saisie_finie::text','pli_controle_fini::text','pli_termine::text');
    protected $order_ind         		= array('date_courrier'=> 'asc','date_deb'=> 'asc','date_fin'=> 'asc');
	
	protected $select_detail_indicateur 	    = array('id_pli','nom_societe','typologie','lot_scan','date_courrier','date_typage','date_saisie','date_controle','date_termine','date_reb','typage_par','saisie_par','controle_par','retraite_par');	
    protected $column_order_detail_indicateur   = array('id_pli','nom_societe','typologie','lot_scan','date_courrier','date_typage','date_saisie','date_controle','date_termine','date_reb');
    protected $column_search_detail_indicateur	= array('id_pli','nom_societe','typologie','lot_scan','date_courrier','date_typage','date_saisie','date_controle','date_termine','date_reb');
    protected $order_detail_indicateur          = array('id_pli'=> 'asc','typologie'=> 'asc','lot_scan'=> 'asc');
	
	protected $select_ind_op       		= array('date_courrier','date_deb','date_fin','pli_recu','login','pli_typage_fini','pli_saisie_finie','pli_controle_fini','pli_termine');	
	protected $column_order_ind_op  	= array('date_courrier','pli_recu','login','pli_typage_fini','pli_saisie_finie','pli_controle_fini','pli_termine');
	protected $column_search_ind_op		= array('date_courrier::text','pli_recu::text','login::text','pli_typage_fini::text','pli_saisie_finie::text','pli_controle_fini::text','pli_termine::text');
	protected $order_ind_op         	= array('date_courrier'=> 'asc','date_deb'=> 'asc','date_fin'=> 'asc','login'=>'asc' );
	
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }
	
	/*public function get_statut_traitement(){
        return $res_type_doc = $this->base_64->select('*')
            ->from('flag_traitement')
            ->where('actif_extract = ', '1')
            ->order_by('comment asc')
            ->get()
            ->result();
    }
	public function get_statut_saisie(){
        return $res_type_doc = $this->ged->select('*')
            ->from('statut_saisie')
            ->where('actif = ', '1')
            ->order_by('libelle asc')
            ->get()
            ->result();
    }*/
	
	 public function get_statut_traitement(){
		
		$sql = "SELECT id_flag_traitement,traitement
						FROM flag_traitement 
						WHERE 1=1 /*actif_extract = 1 */
				        ORDER BY id_flag_traitement asc";		
		return $this->base_64->query($sql)
							->result_array();
    }
	
	 public function get_statut_saisie(){
		
		$sql = "SELECT id_statut_saisie,libelle
						FROM statut_saisie 
						ORDER BY id_statut_saisie asc";		
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_mode_paiement(){
		
		$sql = "SELECT id_mode_paiement,mode_paiement
						FROM mode_paiement 
						ORDER BY id_mode_paiement asc";		
		return $this->ged->query($sql)
							->result_array();
    }
	public function get_operateur (){
		
		$sql = "SELECT id_utilisateur, login, pass, utilisateurs.id_type_utilisateur,  supprime, 
				   id_groupe_utilisateur, matr_gpao
			  FROM utilisateurs
			  INNER join type_utilisateur on utilisateurs.id_type_utilisateur = type_utilisateur.id_type_utilisateur
				WHERE utilisateurs.id_type_utilisateur in (2,5)
				and utilisateurs.actif = 1 
				order by login asc
				";
		return $this->ged->query($sql)
							->result_array();
	}

    public function get_typologie (){

        $sql = "SELECT id, typologie
			  FROM public.typologie
			  WHERE actif = 1
			  order by typologie asc";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_datatables_indicateur($clauseWhere,$granularite,$where_date,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatable_indicateur_sql($clauseWhere,$granularite,$where_date,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->base_64->limit($length, $start);
        $query = $this->base_64->get();
        return $query->result();
    }

    public function count_filtered_indicateur($clauseWhere,$granularite,$where_date,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_indicateur_sql($clauseWhere,$granularite,$where_date,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->base_64->get();
        return $query->num_rows();
		
    }

    public function count_all_indicateur($clauseWhere,$where_date,$granularite)
    { 
		
		if($granularite == 'j'){
        $this->base_64->select($this->select_ind)
                ->from("(
					SELECT 
					date_courrier, 
					date_courrier as date_deb, 
					date_courrier as date_fin, 
					sum(pli_recu) as pli_recu, 
					sum(pli_typage_fini) as pli_typage_fini,
					sum(pli_saisie_finie) as pli_saisie_finie, 
					sum(pli_controle_fini) as pli_controle_fini, 
					sum(pli_termine) as pli_termine 
				FROM 
				(
					SELECT date_courrier::date as date_courrier ,pli.societe,
					COUNT(distinct pli.id_pli) as pli_recu,
					CASE WHEN (pli.flag_traitement >= 2) AND  pli.typage_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_typage_fini,
					CASE WHEN (pli.flag_traitement >= 5) AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_saisie_finie,
					CASE WHEN (pli.flag_traitement = 9) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_controle_fini,
					CASE WHEN ((pli.flag_traitement  in(9,14)  and pli.statut_saisie = 1)
					or pli.statut_saisie in (8,10,24)) and (controle_par is not null or retraite_par is not null) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_termine
					FROM  pli
					INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
					INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
					/*LEFT JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli*/
					INNER JOIN f_societe ON f_societe.id = pli.societe	
					WHERE 1= 1 $clauseWhere
					GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,pli.statut_saisie,flag_saisie,pli.typage_par,pli.saisie_par,controle_par,retraite_par
					
				) as res
				GROUP BY date_courrier) as tab
                    ");
		}
		if($granularite == 's'){
        $this->base_64->select($this->select_ind)
                ->from("(
					SELECT 
						'S'||week as date_courrier,date_deb, date_fin,
						sum(pli_recu) as pli_recu, 
						sum(pli_typage_fini) as pli_typage_fini,
						sum(pli_saisie_finie) as pli_saisie_finie, 
						sum(pli_controle_fini) as pli_controle_fini, 
						sum(pli_termine) as pli_termine
						FROM
						(
							SELECT min (daty) as date_deb, max(daty) as date_fin,week, 1 as flag
							FROM
							(
								select daty,week from liste_weeks($where_date)
							) as tab
							group by week
						) data_dates
						LEFT JOIN(
							SELECT date_courrier::date as date_courrier ,pli.societe,
							COUNT(distinct pli.id_pli) as pli_recu,
							CASE WHEN (pli.flag_traitement >= 2) AND  pli.typage_par IS NOT NULL  THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_typage_fini,
							CASE WHEN (pli.flag_traitement >= 5) AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_saisie_finie,
							CASE WHEN (pli.flag_traitement = 9) AND (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_controle_fini,
							CASE WHEN(( pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
							or pli.statut_saisie in (8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_termine
							FROM  pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
							INNER JOIN f_societe ON f_societe.id = pli.societe	
							WHERE 1= 1 $clauseWhere
							GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,pli.statut_saisie,flag_saisie,pli.typage_par,pli.saisie_par,controle_par,retraite_par
						) data_pli on  data_pli.date_courrier between date_deb and date_fin
						GROUP BY date_deb, date_fin,week
						ORDER BY week asc) as tab
                    ");
		}
		if($granularite == 'm'){
        $this->base_64->select($this->select_ind)
                ->from("(
					SELECT 
						substr(daty::text,1,7) as date_courrier,
						daty as date_deb,
						( daty + interval '1 month' - interval '1 day' )::date as date_fin,
						sum(pli_recu) as pli_recu, 
						sum(pli_typage_fini) as pli_typage_fini,
						sum(pli_saisie_finie) as pli_saisie_finie, 
						sum(pli_controle_fini) as pli_controle_fini, 
						sum(pli_termine) as pli_termine
						FROM
						(
							select liste_months as daty from liste_months($where_date)
						
						) data_dates
						LEFT JOIN(
							SELECT date_courrier::date as date_courrier ,pli.societe,
							COUNT(distinct pli.id_pli) as pli_recu,
							CASE WHEN (pli.flag_traitement >= 2) AND  pli.typage_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_typage_fini,
							CASE WHEN (pli.flag_traitement >= 5) AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_saisie_finie,
							CASE WHEN (pli.flag_traitement = 9) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_controle_fini,
							CASE WHEN ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1) 
							or pli.statut_saisie in (8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_termine
							FROM  pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
							INNER JOIN f_societe ON f_societe.id = pli.societe	
							WHERE 1= 1  $clauseWhere
							GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,pli.statut_saisie,flag_saisie,pli.typage_par,pli.saisie_par,controle_par,retraite_par
						) data_pli on data_pli.date_courrier between daty and ( daty + interval '1 month' - interval '1 day' )::date
						GROUP BY daty
						) as tab
                    ");
		}
        
		return $this->base_64->count_all_results();
		
    }

    public function get_datatable_indicateur_sql($clauseWhere,$granularite,$where_date,$post_order,$search ,$post_order_col,$post_order_dir){
		
		
		if($granularite == 'j'){
        $this->base_64->select($this->select_ind)
                ->from("(
					SELECT 
					date_courrier, 
					date_courrier as date_deb, 
					date_courrier as date_fin, 
					sum(pli_recu) as pli_recu, 
					sum(pli_typage_fini) as pli_typage_fini,
					sum(pli_saisie_finie) as pli_saisie_finie, 
					sum(pli_controle_fini) as pli_controle_fini, 
					sum(pli_termine) as pli_termine 
				FROM 
				(
					SELECT date_courrier::date as date_courrier ,pli.societe,
					COUNT(distinct pli.id_pli) as pli_recu,
					CASE WHEN (pli.flag_traitement >= 2) AND  pli.typage_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_typage_fini,
					CASE WHEN (pli.flag_traitement >= 5) AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_saisie_finie,
					CASE WHEN (pli.flag_traitement = 9) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_controle_fini,
					CASE WHEN ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
						or pli.statut_saisie in (8,10,24)) and (controle_par is not null or retraite_par is not null) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_termine
					FROM  pli
					INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
					INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
					/*LEFT JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli*/
					INNER JOIN f_societe ON f_societe.id = pli.societe	
					WHERE 1= 1 $clauseWhere
					GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,pli.statut_saisie,flag_saisie,pli.typage_par,pli.saisie_par,controle_par,retraite_par
					
				) as res
				GROUP BY date_courrier) as tab
                    ");
		}
		if($granularite == 's'){
        $this->base_64->select($this->select_ind)
                ->from("(
					SELECT 
						'S'||week as date_courrier,date_deb, date_fin,
						sum(pli_recu) as pli_recu, 
						sum(pli_typage_fini) as pli_typage_fini,
						sum(pli_saisie_finie) as pli_saisie_finie, 
						sum(pli_controle_fini) as pli_controle_fini, 
						sum(pli_termine) as pli_termine
						FROM
						(
							SELECT min (daty) as date_deb, max(daty) as date_fin,week, 1 as flag
							FROM
							(
								select daty,week from liste_weeks($where_date)
							) as tab
							group by week
						) data_dates
						LEFT JOIN(
							SELECT date_courrier::date as date_courrier ,pli.societe,
							COUNT(distinct pli.id_pli) as pli_recu,
							CASE WHEN (pli.flag_traitement >= 2) AND  pli.typage_par IS NOT NULL  THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_typage_fini,
							CASE WHEN (pli.flag_traitement >= 5) AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_saisie_finie,
							CASE WHEN (pli.flag_traitement = 9) AND (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_controle_fini,
							CASE WHEN(( pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
							or pli.statut_saisie in (8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_termine
							FROM  pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
							INNER JOIN f_societe ON f_societe.id = pli.societe	
							WHERE 1= 1 $clauseWhere
							GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,pli.statut_saisie,flag_saisie,pli.typage_par,pli.saisie_par,controle_par,retraite_par
						) data_pli on  data_pli.date_courrier between date_deb and date_fin
						GROUP BY date_deb, date_fin,week
						ORDER BY week asc) as tab
                    ");
		}
		if($granularite == 'm'){
        $this->base_64->select($this->select_ind)
                ->from("(
					SELECT 
						substr(daty::text,1,7) as date_courrier,
						daty as date_deb,
						( daty + interval '1 month' - interval '1 day' )::date as date_fin,
						sum(pli_recu) as pli_recu, 
						sum(pli_typage_fini) as pli_typage_fini,
						sum(pli_saisie_finie) as pli_saisie_finie, 
						sum(pli_controle_fini) as pli_controle_fini, 
						sum(pli_termine) as pli_termine
						FROM
						(
							select liste_months as daty from liste_months($where_date)
						
						) data_dates
						LEFT JOIN(
							SELECT date_courrier::date as date_courrier ,pli.societe,
							COUNT(distinct pli.id_pli) as pli_recu,
							CASE WHEN (pli.flag_traitement >= 2) AND  pli.typage_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_typage_fini,
							CASE WHEN (pli.flag_traitement >= 5) AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_saisie_finie,
							CASE WHEN (pli.flag_traitement = 9) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_controle_fini,
							CASE WHEN ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
							or pli.statut_saisie in(8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_termine
							FROM  pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
							INNER JOIN f_societe ON f_societe.id = pli.societe	
							WHERE 1= 1  $clauseWhere
							GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,pli.statut_saisie,flag_saisie,pli.typage_par,pli.saisie_par,controle_par,retraite_par
						) data_pli on data_pli.date_courrier between daty and ( daty + interval '1 month' - interval '1 day' )::date
						GROUP BY daty
						) as tab
                    ");
		}
        
        $i = 0;
        foreach ($this->column_search_ind as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->base_64->group_start();
                    $this->base_64->ilike($item, $search);
                }
                else
                {
                    $this->base_64->or_ilike($item, $search);
                }

                if(count($this->column_search_ind) - 1 == $i)
                    $this->base_64->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->base_64->order_by($this->column_order_ind[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_ind))
        {
            $order = $this->order_ind;
            $this->base_64->order_by(key($order), $order[key($order)]);
        }
    }
	
	public function get_datatables_detail_indicateur ($where_date_courrier,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
		//get_datatables_indicateur_saisie
        $this->get_datatable_detail_indicateur_sql($where_date_courrier,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->base_64->limit($length, $start);
        $query = $this->base_64->get();
        return $query->result();
    }

    public function count_filtered_detail_indicateur($where_date_courrier,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_detail_indicateur_sql($where_date_courrier,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->base_64->get();
        return $query->num_rows();
		
    }

    public function count_all_detail_indicateur($where_date_courrier)
    { 
	  $this->base_64->select($this->select_detail_indicateur,false)
                ->from("(SELECT pli.id_pli::text, 
							f_societe.nom_societe ,
							f_typologie.typologie,
							lot_numerisation.lot_scan,
							substring(lot_numerisation.date_courrier::text from 0 for 11) as date_courrier ,
							pli.societe,
							substring(date_typage::text from 0 for 11) as date_typage ,
							substring(date_saisie::text from 0 for 11) as date_saisie ,
							substring(date_controle::text from 0 for 11) as date_controle ,
							substring(date_termine::text from 0 for 11) as date_termine ,
							substring(date_reb::text from 0 for 11) as date_reb	,
							f1.login as typage_par, 
							f2.login as saisie_par, 
							f3.login as controle_par, 
							f4.login as retraite_par				
						FROM  pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
						INNER JOIN f_societe ON f_societe.id = pli.societe	
						LEFT JOIN f_typologie ON f_typologie.id = pli.typologie
						LEFT JOIN f_utilisateurs f1 ON f1.id_utilisateur = pli.typage_par
						LEFT JOIN f_utilisateurs f2 ON f2.id_utilisateur = pli.saisie_par
						LEFT JOIN f_utilisateurs f3 ON f3.id_utilisateur = pli.controle_par
						LEFT JOIN f_utilisateurs f4 ON f4.id_utilisateur = pli.retraite_par
						WHERE 1= 1  $where_date_courrier
						and (
							((pli.flag_traitement >= 2) AND  typage_par IS NOT NULL)
							OR ((pli.flag_traitement >= 5) AND flag_saisie = 1 AND saisie_par IS NOT NULL)
							or ((pli.flag_traitement = 9) and (controle_par IS NOT NULL or retraite_par IS NOT NULL))
							or (((pli.flag_traitement  in(9, 14) and pli.statut_saisie = 1)
								or pli.statut_saisie in (8, 10, 24) )
								and (controle_par is not null or retraite_par is not null))
						)
					) as tab");
					
        return $this->base_64->count_all_results();
		
    }

    public function get_datatable_detail_indicateur_sql($where_date_courrier,$post_order,$search ,$post_order_col,$post_order_dir){

         $this->base_64->select($this->select_detail_indicateur,false)
                 ->from("
						(SELECT pli.id_pli::text, 
							f_societe.nom_societe ,
							f_typologie.typologie,
							lot_numerisation.lot_scan,
							substring(lot_numerisation.date_courrier::text from 0 for 11) as date_courrier ,
							pli.societe,
							substring(date_typage::text from 0 for 11) as date_typage ,
							substring(date_saisie::text from 0 for 11) as date_saisie ,
							substring(date_controle::text from 0 for 11) as date_controle ,
							substring(date_termine::text from 0 for 11) as date_termine ,
							substring(date_reb::text from 0 for 11) as date_reb	,
							f1.login as typage_par, 
							f2.login as saisie_par, 
							f3.login as controle_par, 
							f4.login as retraite_par				
						FROM  pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
						INNER JOIN f_societe ON f_societe.id = pli.societe	
						LEFT JOIN f_typologie ON f_typologie.id = pli.typologie
						LEFT JOIN f_utilisateurs f1 ON f1.id_utilisateur = pli.typage_par
						LEFT JOIN f_utilisateurs f2 ON f2.id_utilisateur = pli.saisie_par
						LEFT JOIN f_utilisateurs f3 ON f3.id_utilisateur = pli.controle_par
						LEFT JOIN f_utilisateurs f4 ON f4.id_utilisateur = pli.retraite_par
						WHERE 1= 1  $where_date_courrier
						and (
							((pli.flag_traitement >= 2) AND  typage_par IS NOT NULL)
							OR ((pli.flag_traitement >= 5) AND flag_saisie = 1 AND saisie_par IS NOT NULL)
							or ((pli.flag_traitement = 9) and (controle_par IS NOT NULL or retraite_par IS NOT NULL))
							or (((pli.flag_traitement  in(9, 14) and pli.statut_saisie = 1)
								or pli.statut_saisie in (8, 10, 24) )
								and (controle_par is not null or retraite_par is not null))
						)
			) as tab
				");

        $i = 0;
        foreach ($this->column_search_detail_indicateur as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->base_64->group_start();
                    $this->base_64->ilike($item, $search);
                }
                else
                {
                    $this->base_64->or_ilike($item, $search);
                }

                if(count($this->column_search_detail_indicateur) - 1 == $i)
                    $this->base_64->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->base_64->order_by($this->column_order_detail_indicateur[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_detail_indicateur))
        {
            $order = $this->order_detail_indicateur;
            $this->base_64->order_by(key($order), $order[key($order)]);
        }
    }
	
	
	public function get_indicateur_prod($clauseWhere,$granularite,$where_date){
			
		if($granularite == 'j')			
		{	$sql = "SELECT 
					date_courrier, 
					sum(pli_recu) as pli_recu, 
					sum(pli_typage_fini) as pli_typage_fini,
					sum(pli_saisie_finie) as pli_saisie_finie, 
					sum(pli_controle_fini) as pli_controle_fini, 
					sum(pli_termine) as pli_termine
				FROM 
				(
					SELECT date_courrier::date as date_courrier ,pli.societe,
					COUNT(distinct pli.id_pli) as pli_recu,
					CASE WHEN (pli.flag_traitement >= 2) AND  typage_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_typage_fini,
					CASE WHEN flag_saisie=1 AND saisie_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_saisie_finie,
					CASE WHEN (pli.flag_traitement = 9) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_controle_fini,
					CASE WHEN ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
					or pli.statut_saisie in(8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_termine
					FROM  pli
					INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
					INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
					/*LEFT JOIN f_mouvement ON f_mouvement.id_pli = pli.id_pli*/
					INNER JOIN f_societe ON f_societe.id = pli.societe	
					WHERE 1= 1 $clauseWhere
					GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,pli.statut_saisie,id_lot_saisie,typage_par,saisie_par,controle_par,retraite_par,flag_saisie
					
				) as res
				GROUP BY date_courrier
				ORDER BY date_courrier ASC
				";
		}
		if($granularite == 's')
		{
			$sql = "SELECT 
						'S'||week as date_courrier,
						date_deb, date_fin,
						sum(pli_recu) as pli_recu, 
						sum(pli_typage_fini) as pli_typage_fini,
						sum(pli_saisie_finie) as pli_saisie_finie, 
						sum(pli_controle_fini) as pli_controle_fini, 
						sum(pli_termine) as pli_termine
						FROM
						(
							SELECT min (daty) as date_deb, max(daty) as date_fin,week, 1 as flag
							FROM
							(
								select daty,week from liste_weeks($where_date)
							) as tab
							group by week
						) data_dates
						LEFT JOIN(
							SELECT date_courrier::date as date_courrier ,pli.societe,
							COUNT(distinct pli.id_pli) as pli_recu,
							CASE WHEN (pli.flag_traitement >= 2)  AND  typage_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_typage_fini,
							CASE WHEN flag_saisie=1 AND saisie_par IS NOT NULL  THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_saisie_finie,
							CASE WHEN (pli.flag_traitement = 9) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_controle_fini,
							CASE WHEN ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
							or pli.statut_saisie in (8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_termine
							FROM  pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
							INNER JOIN f_societe ON f_societe.id = pli.societe	
							WHERE 1= 1 $clauseWhere
							GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,pli.statut_saisie,id_lot_saisie,typage_par,saisie_par,controle_par,retraite_par,flag_saisie
						) data_pli on  data_pli.date_courrier between date_deb and date_fin
						GROUP BY date_deb, date_fin,week
						ORDER BY week asc";
			
		}
		if($granularite == 'm')
		{
			$sql="SELECT 
						substr(daty::text,1,7) as date_courrier, 
						daty as date_deb, 
						( daty + interval '1 month' - interval '1 day' )::date as date_fin, 
						sum(pli_recu) as pli_recu, 
						sum(pli_typage_fini) as pli_typage_fini,
						sum(pli_saisie_finie) as pli_saisie_finie, 
						sum(pli_controle_fini) as pli_controle_fini, 
						sum(pli_termine) as pli_termine
						FROM
						(
							select liste_months as daty from liste_months($where_date)
						
						) data_dates
						LEFT JOIN(
							SELECT date_courrier::date as date_courrier ,pli.societe,
							COUNT(distinct pli.id_pli) as pli_recu,
							CASE WHEN (pli.flag_traitement >= 2)  AND  typage_par IS NOT NULL THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_typage_fini,
							CASE WHEN flag_saisie=1 AND saisie_par IS NOT NULL THEN  COUNT(distinct pli.id_pli) ELSE 0 END as pli_saisie_finie,
							CASE WHEN (pli.flag_traitement = 9) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_controle_fini,
							CASE WHEN ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
							or pli.statut_saisie in (8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL) THEN COUNT(distinct pli.id_pli) ELSE 0 END as pli_termine
							FROM  pli
							INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
							INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
							INNER JOIN f_societe ON f_societe.id = pli.societe	
							WHERE 1= 1  $clauseWhere
							GROUP BY pli.societe,nom_societe, date_courrier, pli.flag_traitement,pli.statut_saisie,id_lot_saisie,typage_par,saisie_par,controle_par,retraite_par,flag_saisie
						) data_pli on data_pli.date_courrier between daty and ( daty + interval '1 month' - interval '1 day' )::date
						GROUP BY daty
						order BY daty ASC
			";
		}
		
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";
				*/

		return $this->base_64->query($sql)
							->result_array();
    }
	public function get_indicateur_detail($clauseWhere){

         $sql = "	SELECT pli.id_pli::text, 
							f_societe.nom_societe ,
							f_typologie.typologie,
							lot_numerisation.lot_scan,
							substring(lot_numerisation.date_courrier::text from 0 for 11) as date_courrier ,
							pli.societe,
							substring(date_typage::text from 0 for 11) as date_typage ,
							substring(date_saisie::text from 0 for 11) as date_saisie ,
							substring(date_controle::text from 0 for 11) as date_controle ,
							substring(date_termine::text from 0 for 11) as date_termine ,
							substring(date_reb::text from 0 for 11) as date_reb	,
							f1.login as typage_par, 
							f2.login as saisie_par, 
							f3.login as controle_par, 
							f4.login as retraite_par				
						FROM  pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
						INNER JOIN f_societe ON f_societe.id = pli.societe	
						LEFT JOIN f_typologie ON f_typologie.id = pli.typologie
						LEFT JOIN f_utilisateurs f1 ON f1.id_utilisateur = pli.typage_par
						LEFT JOIN f_utilisateurs f2 ON f2.id_utilisateur = pli.saisie_par
						LEFT JOIN f_utilisateurs f3 ON f3.id_utilisateur = pli.controle_par
						LEFT JOIN f_utilisateurs f4 ON f4.id_utilisateur = pli.retraite_par
						WHERE 1= 1  $clauseWhere
						and (
							((pli.flag_traitement >= 2) AND  typage_par IS NOT NULL)
							OR ((pli.flag_traitement >= 5) AND flag_saisie = 1 AND saisie_par IS NOT NULL)
							or ((pli.flag_traitement = 9) and (controle_par IS NOT NULL or retraite_par IS NOT NULL))
							or (((pli.flag_traitement  in(9, 14) and pli.statut_saisie = 1)
								or pli.statut_saisie in (8, 10, 24) )
								and (controle_par is not null or retraite_par is not null))
						)
						ORDER BY pli.id_pli ASC
						
				";
		return $this->base_64->query($sql)
							->result_array();
	  }
	  
    
	public function get_date_fin($date_ffin, $date_fin ){

	 $sql = "select case when extract(months FROM ('$date_ffin'::date)) =  extract(months FROM ('$date_fin'::date)) 
			Then '$date_ffin' else '$date_fin' end as date_fin_fin
			";
	return $this->base_64->query($sql)
						->result_array();
   }
   
   /* */
    public function get_datatables_indicateur_op($clauseWhere,$granularite,$where_date,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatable_indicateur_op_sql($clauseWhere,$granularite,$where_date,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->base_64->limit($length, $start);
        $query = $this->base_64->get();
        return $query->result();
    }

    public function count_filtered_indicateur_op($clauseWhere,$granularite,$where_date,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_indicateur_op_sql($clauseWhere,$granularite,$where_date,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->base_64->get();
        return $query->num_rows();
		
    }

    public function count_all_indicateur_op($clauseWhere,$where_date,$granularite)
    { 
		
		if($granularite == 'j'){
        $this->base_64->select($this->select_ind_op)
                ->from("(
					SELECT 
						data_param.date_courrier as date_courrier ,
						data_param.date_courrier as date_deb, 
						data_param.date_courrier as date_fin, 
						data_param.id_utilisateur,
						data_param.login,
						coalesce(pli_recu,0) pli_recu,
						coalesce(pli_typage_fini,0) pli_typage_fini,
						coalesce(pli_saisie_finie,0) pli_saisie_finie,
						coalesce(pli_controle_fini,0) pli_controle_fini,
						coalesce(pli_termine,0) pli_termine					
						
					FROM 
					(	SELECT date_courrier,id_utilisateur,login
						FROM 
						(
							SELECT id_utilisateur , login,1 flag 
							FROM f_utilisateurs
							WHERE actif = 1 AND supprime = 0
							ORDER BY login asc
						)data_user LEFT JOIN
						(

								SELECT substring(date_courrier::text,1,10) AS date_courrier,1 AS flag
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_societe ON f_societe.id = pli.societe	
								WHERE 1= 1 $clauseWhere
								GROUP BY substring(date_courrier::text,1,10)
							
						) AS data_pli ON data_pli.flag = data_user.flag
					) AS data_param LEFT JOIN 
					(
						SELECT date_courrier, typage_par, 
						SUM(pli_typage_fini) AS pli_typage_fini	
						FROM 
							(
								SELECT pli.typage_par AS typage_par,substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_typage_fini
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								WHERE 1= 1 $clauseWhere 
								AND (pli.flag_traitement >= 2)  AND  pli.typage_par IS NOT NULL
								GROUP BY  pli.typage_par,date_courrier
							) AS data_typage
						GROUP BY date_courrier, typage_par 
					)AS data_typage 
					ON data_typage.date_courrier = data_param.date_courrier
					AND  data_typage.typage_par = data_param.id_utilisateur

					LEFT JOIN 
					(
						SELECT date_courrier, saisie_par, 
						SUM(pli_saisie_finie) AS pli_saisie_finie	
						FROM 
							(
								SELECT pli.saisie_par AS saisie_par,substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_saisie_finie
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
								WHERE 1= 1 $clauseWhere
								AND  flag_saisie = 1 AND pli.saisie_par IS NOT NULL
								GROUP BY  pli.saisie_par,date_courrier
							) AS data_typage
						GROUP BY date_courrier, saisie_par
					)AS data_saisie 
					ON data_saisie.date_courrier = data_param.date_courrier
					AND  data_saisie.saisie_par = data_param.id_utilisateur

					LEFT JOIN 
					(
						SELECT date_courrier, controle_par, 
						SUM(pli_controle_fini) AS pli_controle_fini	
						FROM 
							(
								SELECT 
									CASE WHEN pli.controle_par IS NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par 
									WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
									WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par
									end AS controle_par,
									substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_controle_fini
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
								WHERE 1= 1 $clauseWhere
								AND (pli.flag_traitement in (9,14) AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL ) )
								GROUP BY  pli.controle_par,pli.retraite_par, date_courrier
							) AS data_typage
						GROUP BY date_courrier, controle_par 
					)AS data_controle
					ON data_controle.date_courrier = data_param.date_courrier
					AND  data_controle.controle_par = data_param.id_utilisateur
					LEFT JOIN 
					(
						SELECT date_courrier, termine_par, 
						SUM(pli_termine) AS pli_termine	
						FROM 
							(
								SELECT 
									CASE WHEN
										  pli.typage_par IS NOT NULL AND pli.saisie_par IS NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.typage_par
									WHEN  pli.saisie_par IS NOT NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.saisie_par
									WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
									WHEN  pli.retraite_par IS NOT NULL THEN pli.retraite_par
									end AS termine_par,
									substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_termine
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
								WHERE 1= 1 $clauseWhere
								AND ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1) 
								or pli.statut_saisie in (8,10,24))
								AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL )
								GROUP BY  pli.typage_par ,pli.saisie_par, pli.controle_par,pli.retraite_par,date_courrier
							) AS data_typage
						GROUP BY date_courrier, termine_par 
					)AS data_termine
					ON data_termine.date_courrier = data_param.date_courrier
					AND  data_termine.termine_par = data_param.id_utilisateur
					LEFT JOIN 
					(

						SELECT count (pli.id_pli) as pli_recu, substring(date_courrier::text,1,10) as date_courrier
						FROM  pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN f_societe ON f_societe.id = pli.societe	
						WHERE 1= 1 $clauseWhere
						GROUP BY substring(date_courrier::text, 1, 10)
							

					) as data_recu
					ON data_recu.date_courrier = data_param.date_courrier
					where pli_typage_fini > 0 or pli_saisie_finie > 0 or  pli_controle_fini > 0 or  pli_termine > 0 
				) as tab 
                    ");
		}
		if($granularite == 's'){
        $this->base_64->select($this->select_ind_op)
                ->from("(SELECT
							date_courrier,
							date_deb, 
							date_fin,
							id_utilisateur,
							login, 
							pli_recu,
							pli_typage_fini,
							pli_saisie_finie,
							pli_controle_fini,
							pli_termine
						FROM
						(SELECT
							'S'||week as date_courrier,
							date_deb, 
							date_fin,
							data_pli.id_utilisateur,
							data_pli.login, 
							sum(pli_recu) pli_recu,
							sum(pli_typage_fini) pli_typage_fini,
							sum(pli_saisie_finie) pli_saisie_finie,
							sum(pli_controle_fini) pli_controle_fini,
							sum(pli_termine) pli_termine
						FROM
						(
							SELECT min (daty) as date_deb, max(daty) as date_fin,week
							FROM
							(
								select daty,week from liste_weeks($where_date)
							) as tab
							group by week
						) data_dates
						LEFT JOIN
						(
								SELECT 
									data_param.date_courrier,data_param.id_utilisateur,data_param.login, 
									pli_typage_fini,
									pli_saisie_finie,
									pli_controle_fini,
									pli_termine, 
									pli_recu
								FROM 
								(	SELECT date_courrier,id_utilisateur,login
									FROM 
									(
										SELECT id_utilisateur , login,1 flag 
										FROM f_utilisateurs
										WHERE actif = 1 AND supprime = 0
										ORDER BY login asc
									)data_user LEFT JOIN
									(

											SELECT substring(date_courrier::text,1,10) AS date_courrier,1 AS flag
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_societe ON f_societe.id = pli.societe	
											WHERE 1= 1 $clauseWhere
											GROUP BY substring(date_courrier::text,1,10)
										
									) AS data_pli ON data_pli.flag = data_user.flag
								) AS data_param LEFT JOIN 
								(
									SELECT date_courrier, typage_par, 
									SUM(pli_typage_fini) AS pli_typage_fini	
									FROM 
										(
											SELECT pli.typage_par AS typage_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_typage_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND (pli.flag_traitement >= 2)  AND  pli.typage_par IS NOT NULL
											GROUP BY  pli.typage_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, typage_par 
								)AS data_typage 
								ON data_typage.date_courrier = data_param.date_courrier
								AND  data_typage.typage_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, saisie_par, 
									SUM(pli_saisie_finie) AS pli_saisie_finie	
									FROM 
										(
											SELECT pli.saisie_par AS saisie_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_saisie_finie
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
											WHERE 1= 1 $clauseWhere
											AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL
											GROUP BY  pli.saisie_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, saisie_par 
								)AS data_saisie 
								ON data_saisie.date_courrier = data_param.date_courrier
								AND  data_saisie.saisie_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, controle_par, 
									SUM(pli_controle_fini) AS pli_controle_fini	
									FROM 
										(
											SELECT 
												CASE WHEN pli.controle_par IS NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par 
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS controle_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_controle_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1  $clauseWhere
											AND (pli.flag_traitement = 9 AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL ) )
											GROUP BY  pli.controle_par,pli.retraite_par, date_courrier
										) AS data_typage
									GROUP BY date_courrier, controle_par 
								)AS data_controle
								ON data_controle.date_courrier = data_param.date_courrier
								AND  data_controle.controle_par = data_param.id_utilisateur
								LEFT JOIN 
								(
									SELECT date_courrier, termine_par, 
									SUM(pli_termine) AS pli_termine	
									FROM 
										(
											SELECT 
												CASE WHEN
													  pli.typage_par IS NOT NULL AND pli.saisie_par IS NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.typage_par
												WHEN  pli.saisie_par IS NOT NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.saisie_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS termine_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_termine
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
											or pli.statut_saisie in (8,10,24))
											AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL )
											GROUP BY  pli.typage_par ,pli.saisie_par, pli.controle_par,pli.retraite_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, termine_par 
								)AS data_termine
								ON data_termine.date_courrier = data_param.date_courrier
								AND  data_termine.termine_par = data_param.id_utilisateur
								LEFT JOIN 
								(

									SELECT count (pli.id_pli) as pli_recu, substring(date_courrier::text,1,10) as date_courrier
									FROM  pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									INNER JOIN f_societe ON f_societe.id = pli.societe	
									WHERE 1= 1 $clauseWhere
									GROUP BY substring(date_courrier::text,1,10)
										

								) as data_recu
								ON data_recu.date_courrier = data_param.date_courrier
								
					) as data_pli on  data_pli.date_courrier::date between date_deb and date_fin
								
				GROUP BY data_dates.week,date_deb, date_fin,data_pli.id_utilisateur,data_pli.login	
				)as res  
				WHERE pli_typage_fini > 0 or pli_saisie_finie > 0 or  pli_controle_fini > 0 or  pli_termine > 0				
				) as tab
                    ");
		}
		if($granularite == 'm'){
        $this->base_64->select($this->select_ind_op)
                ->from("(SELECT
							date_courrier,
							date_deb,
							date_fin,
							id_utilisateur,
							login, 
							pli_recu,
							pli_typage_fini,
							pli_saisie_finie,
							pli_controle_fini,
							pli_termine
						FROM(SELECT
							substr(daty::text,1,7) as date_courrier,
							daty as date_deb,
							( daty + interval '1 month' - interval '1 day' )::date as date_fin,
							data_pli.id_utilisateur,
							data_pli.login, 
							sum(pli_recu) pli_recu,
							sum(pli_typage_fini) pli_typage_fini,
							sum(pli_saisie_finie) pli_saisie_finie,
							sum(pli_controle_fini) pli_controle_fini,
							sum(pli_termine) pli_termine
						FROM
						(
							select liste_months as daty from liste_months($where_date)
						) data_dates
						LEFT JOIN
						(
								SELECT 
									data_param.date_courrier,data_param.id_utilisateur,data_param.login, 
									pli_typage_fini,
									pli_saisie_finie,
									pli_controle_fini,
									pli_termine, 
									pli_recu
								FROM 
								(	SELECT date_courrier,id_utilisateur,login
									FROM 
									(
										SELECT id_utilisateur , login,1 flag 
										FROM f_utilisateurs
										WHERE actif = 1 AND supprime = 0
										ORDER BY login asc
									)data_user LEFT JOIN
									(

											SELECT substring(date_courrier::text,1,10) AS date_courrier,1 AS flag
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_societe ON f_societe.id = pli.societe	
											WHERE 1= 1 $clauseWhere
											GROUP BY substring(date_courrier::text, 1, 10)
										
									) AS data_pli ON data_pli.flag = data_user.flag
								) AS data_param LEFT JOIN 
								(
									SELECT date_courrier, typage_par, 
									SUM(pli_typage_fini) AS pli_typage_fini	
									FROM 
										(
											SELECT pli.typage_par AS typage_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_typage_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1  $clauseWhere
											AND (pli.flag_traitement >= 2)  AND  pli.typage_par IS NOT NULL
											GROUP BY  pli.typage_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, typage_par 
								)AS data_typage 
								ON data_typage.date_courrier = data_param.date_courrier
								AND  data_typage.typage_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, saisie_par, 
									SUM(pli_saisie_finie) AS pli_saisie_finie	
									FROM 
										(
											SELECT pli.saisie_par AS saisie_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_saisie_finie
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
											WHERE 1= 1 $clauseWhere 
											AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL
											GROUP BY  pli.saisie_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, saisie_par 
								)AS data_saisie 
								ON data_saisie.date_courrier = data_param.date_courrier
								AND  data_saisie.saisie_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, controle_par, 
									SUM(pli_controle_fini) AS pli_controle_fini	
									FROM 
										(
											SELECT 
												CASE WHEN pli.controle_par IS NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par 
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS controle_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_controle_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND (pli.flag_traitement = 9 AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL ) )
											GROUP BY  pli.controle_par,pli.retraite_par, date_courrier
										) AS data_typage
									GROUP BY date_courrier, controle_par 
								)AS data_controle
								ON data_controle.date_courrier = data_param.date_courrier
								AND  data_controle.controle_par = data_param.id_utilisateur
								LEFT JOIN 
								(
									SELECT date_courrier, termine_par, 
									SUM(pli_termine) AS pli_termine	
									FROM 
										(
											SELECT 
												CASE WHEN
													  pli.typage_par IS NOT NULL AND pli.saisie_par IS NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.typage_par
												WHEN  pli.saisie_par IS NOT NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.saisie_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS termine_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_termine
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
											or pli.statut_saisie in (8,10,24))
											AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL )
											GROUP BY  pli.typage_par ,pli.saisie_par, pli.controle_par,pli.retraite_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, termine_par 
								)AS data_termine
								ON data_termine.date_courrier = data_param.date_courrier
								AND  data_termine.termine_par = data_param.id_utilisateur
								LEFT JOIN 
								(

									SELECT count (pli.id_pli) as pli_recu, substring(date_courrier::text,1,10) as date_courrier
									FROM  pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									INNER JOIN f_societe ON f_societe.id = pli.societe	
									WHERE 1= 1 $clauseWhere
									GROUP BY substring(date_courrier::text, 1, 10)
										

								) as data_recu
								ON data_recu.date_courrier = data_param.date_courrier
								
						) as data_pli on  data_pli.date_courrier::date between daty and ( daty + interval '1 month' - interval '1 day' )::date
										
						GROUP BY data_dates.daty,date_deb,date_fin, data_pli.id_utilisateur,data_pli.login
					 )as res
					 WHERE pli_typage_fini > 0 or pli_saisie_finie > 0 or  pli_controle_fini > 0 or  pli_termine > 0	
					) as tab
                    ");
		}
		
		return $this->base_64->count_all_results();
		
    }

    public function get_datatable_indicateur_op_sql($clauseWhere,$granularite,$where_date,$post_order,$search ,$post_order_col,$post_order_dir){
		
		
		if($granularite == 'j'){
        $this->base_64->select($this->select_ind_op)
                ->from("(
					SELECT 
						data_param.date_courrier as date_courrier ,
						data_param.date_courrier as date_deb, 
						data_param.date_courrier as date_fin, 
						data_param.id_utilisateur,
						data_param.login,
						coalesce(pli_recu,0) pli_recu,
						coalesce(pli_typage_fini,0) pli_typage_fini,
						coalesce(pli_saisie_finie,0) pli_saisie_finie,
						coalesce(pli_controle_fini,0) pli_controle_fini,
						coalesce(pli_termine,0) pli_termine					
						
					FROM 
					(	SELECT date_courrier,id_utilisateur,login
						FROM 
						(
							SELECT id_utilisateur , login,1 flag 
							FROM f_utilisateurs
							WHERE actif = 1 AND supprime = 0
							ORDER BY login asc
						)data_user LEFT JOIN
						(

								SELECT substring(date_courrier::text,1,10) AS date_courrier,1 AS flag
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_societe ON f_societe.id = pli.societe	
								WHERE 1= 1 $clauseWhere
								GROUP BY substring(date_courrier::text,1,10)
							
						) AS data_pli ON data_pli.flag = data_user.flag
					) AS data_param LEFT JOIN 
					(
						SELECT date_courrier, typage_par, 
						SUM(pli_typage_fini) AS pli_typage_fini	
						FROM 
							(
								SELECT pli.typage_par AS typage_par,substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_typage_fini
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								WHERE 1= 1 $clauseWhere 
								AND (pli.flag_traitement >= 2)  AND  pli.typage_par IS NOT NULL
								GROUP BY  pli.typage_par,date_courrier
							) AS data_typage
						GROUP BY date_courrier, typage_par 
					)AS data_typage 
					ON data_typage.date_courrier = data_param.date_courrier
					AND  data_typage.typage_par = data_param.id_utilisateur

					LEFT JOIN 
					(
						SELECT date_courrier, saisie_par, 
						SUM(pli_saisie_finie) AS pli_saisie_finie	
						FROM 
							(
								SELECT pli.saisie_par AS saisie_par,substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_saisie_finie
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
								WHERE 1= 1 $clauseWhere
								AND  flag_saisie = 1 AND pli.saisie_par IS NOT NULL
								GROUP BY  pli.saisie_par,date_courrier
							) AS data_typage
						GROUP BY date_courrier, saisie_par
					)AS data_saisie 
					ON data_saisie.date_courrier = data_param.date_courrier
					AND  data_saisie.saisie_par = data_param.id_utilisateur

					LEFT JOIN 
					(
						SELECT date_courrier, controle_par, 
						SUM(pli_controle_fini) AS pli_controle_fini	
						FROM 
							(
								SELECT 
									CASE WHEN pli.controle_par IS NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par 
									WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
									WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par
									end AS controle_par,
									substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_controle_fini
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
								WHERE 1= 1 $clauseWhere
								AND (pli.flag_traitement in (9,14) AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL ) )
								GROUP BY  pli.controle_par,pli.retraite_par, date_courrier
							) AS data_typage
						GROUP BY date_courrier, controle_par 
					)AS data_controle
					ON data_controle.date_courrier = data_param.date_courrier
					AND  data_controle.controle_par = data_param.id_utilisateur
					LEFT JOIN 
					(
						SELECT date_courrier, termine_par, 
						SUM(pli_termine) AS pli_termine	
						FROM 
							(
								SELECT 
									CASE WHEN
										  pli.typage_par IS NOT NULL AND pli.saisie_par IS NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.typage_par
									WHEN  pli.saisie_par IS NOT NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.saisie_par
									WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
									WHEN  pli.retraite_par IS NOT NULL THEN pli.retraite_par
									end AS termine_par,
									substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_termine
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
								WHERE 1= 1 $clauseWhere
								AND ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
								or pli.statut_saisie in (8,10,24))
								AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL )
								GROUP BY  pli.typage_par ,pli.saisie_par, pli.controle_par,pli.retraite_par,date_courrier
							) AS data_typage
						GROUP BY date_courrier, termine_par 
					)AS data_termine
					ON data_termine.date_courrier = data_param.date_courrier
					AND  data_termine.termine_par = data_param.id_utilisateur
					LEFT JOIN 
					(

						SELECT count (pli.id_pli) as pli_recu, substring(date_courrier::text,1,10) as date_courrier
						FROM  pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN f_societe ON f_societe.id = pli.societe	
						WHERE 1= 1 $clauseWhere
						GROUP BY substring(date_courrier::text, 1, 10)
							

					) as data_recu
					ON data_recu.date_courrier = data_param.date_courrier
					where pli_typage_fini > 0 or pli_saisie_finie > 0 or  pli_controle_fini > 0 or  pli_termine > 0 
				) as tab 
                    ");
		}
		if($granularite == 's'){
        $this->base_64->select($this->select_ind_op)
                ->from("(SELECT
							date_courrier,
							date_deb, 
							date_fin,
							id_utilisateur,
							login, 
							pli_recu,
							pli_typage_fini,
							pli_saisie_finie,
							pli_controle_fini,
							pli_termine
						FROM
						(SELECT
							'S'||week as date_courrier,
							date_deb, 
							date_fin,
							data_pli.id_utilisateur,
							data_pli.login, 
							sum(pli_recu) pli_recu,
							sum(pli_typage_fini) pli_typage_fini,
							sum(pli_saisie_finie) pli_saisie_finie,
							sum(pli_controle_fini) pli_controle_fini,
							sum(pli_termine) pli_termine
						FROM
						(
							SELECT min (daty) as date_deb, max(daty) as date_fin,week
							FROM
							(
								select daty,week from liste_weeks($where_date)
							) as tab
							group by week
						) data_dates
						LEFT JOIN
						(
								SELECT 
									data_param.date_courrier,data_param.id_utilisateur,data_param.login, 
									pli_typage_fini,
									pli_saisie_finie,
									pli_controle_fini,
									pli_termine, 
									pli_recu
								FROM 
								(	SELECT date_courrier,id_utilisateur,login
									FROM 
									(
										SELECT id_utilisateur , login,1 flag 
										FROM f_utilisateurs
										WHERE actif = 1 AND supprime = 0
										ORDER BY login asc
									)data_user LEFT JOIN
									(

											SELECT substring(date_courrier::text,1,10) AS date_courrier,1 AS flag
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_societe ON f_societe.id = pli.societe	
											WHERE 1= 1 $clauseWhere
											GROUP BY substring(date_courrier::text,1,10)
										
									) AS data_pli ON data_pli.flag = data_user.flag
								) AS data_param LEFT JOIN 
								(
									SELECT date_courrier, typage_par, 
									SUM(pli_typage_fini) AS pli_typage_fini	
									FROM 
										(
											SELECT pli.typage_par AS typage_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_typage_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND (pli.flag_traitement >= 2)  AND  pli.typage_par IS NOT NULL
											GROUP BY  pli.typage_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, typage_par 
								)AS data_typage 
								ON data_typage.date_courrier = data_param.date_courrier
								AND  data_typage.typage_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, saisie_par, 
									SUM(pli_saisie_finie) AS pli_saisie_finie	
									FROM 
										(
											SELECT pli.saisie_par AS saisie_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_saisie_finie
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
											WHERE 1= 1 $clauseWhere
											AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL
											GROUP BY  pli.saisie_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, saisie_par 
								)AS data_saisie 
								ON data_saisie.date_courrier = data_param.date_courrier
								AND  data_saisie.saisie_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, controle_par, 
									SUM(pli_controle_fini) AS pli_controle_fini	
									FROM 
										(
											SELECT 
												CASE WHEN pli.controle_par IS NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par 
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS controle_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_controle_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1  $clauseWhere
											AND (pli.flag_traitement = 9 AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL ) )
											GROUP BY  pli.controle_par,pli.retraite_par, date_courrier
										) AS data_typage
									GROUP BY date_courrier, controle_par 
								)AS data_controle
								ON data_controle.date_courrier = data_param.date_courrier
								AND  data_controle.controle_par = data_param.id_utilisateur
								LEFT JOIN 
								(
									SELECT date_courrier, termine_par, 
									SUM(pli_termine) AS pli_termine	
									FROM 
										(
											SELECT 
												CASE WHEN
													  pli.typage_par IS NOT NULL AND pli.saisie_par IS NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.typage_par
												WHEN  pli.saisie_par IS NOT NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.saisie_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS termine_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_termine
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
											or pli.statut_saisie in (8,10,24))
											AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL )
											GROUP BY  pli.typage_par ,pli.saisie_par, pli.controle_par,pli.retraite_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, termine_par 
								)AS data_termine
								ON data_termine.date_courrier = data_param.date_courrier
								AND  data_termine.termine_par = data_param.id_utilisateur
								LEFT JOIN 
								(

									SELECT count (pli.id_pli) as pli_recu, substring(date_courrier::text,1,10) as date_courrier
									FROM  pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									INNER JOIN f_societe ON f_societe.id = pli.societe	
									WHERE 1= 1 $clauseWhere
									GROUP BY substring(date_courrier::text,1,10)
										

								) as data_recu
								ON data_recu.date_courrier = data_param.date_courrier
								
					) as data_pli on  data_pli.date_courrier::date between date_deb and date_fin
								
				GROUP BY data_dates.week,date_deb, date_fin,data_pli.id_utilisateur,data_pli.login	
				)as res  
				WHERE pli_typage_fini > 0 or pli_saisie_finie > 0 or  pli_controle_fini > 0 or  pli_termine > 0				
				) as tab
                    ");
		}
		if($granularite == 'm'){
        $this->base_64->select($this->select_ind_op)
                ->from("(SELECT
							date_courrier,
							date_deb,
							date_fin,
							id_utilisateur,
							login, 
							pli_recu,
							pli_typage_fini,
							pli_saisie_finie,
							pli_controle_fini,
							pli_termine
						FROM(SELECT
							substr(daty::text,1,7) as date_courrier,
							daty as date_deb,
							( daty + interval '1 month' - interval '1 day' )::date as date_fin,
							data_pli.id_utilisateur,
							data_pli.login, 
							sum(pli_recu) pli_recu,
							sum(pli_typage_fini) pli_typage_fini,
							sum(pli_saisie_finie) pli_saisie_finie,
							sum(pli_controle_fini) pli_controle_fini,
							sum(pli_termine) pli_termine
						FROM
						(
							select liste_months as daty from liste_months($where_date)
						) data_dates
						LEFT JOIN
						(
								SELECT 
									data_param.date_courrier,data_param.id_utilisateur,data_param.login, 
									pli_typage_fini,
									pli_saisie_finie,
									pli_controle_fini,
									pli_termine, 
									pli_recu
								FROM 
								(	SELECT date_courrier,id_utilisateur,login
									FROM 
									(
										SELECT id_utilisateur , login,1 flag 
										FROM f_utilisateurs
										WHERE actif = 1 AND supprime = 0
										ORDER BY login asc
									)data_user LEFT JOIN
									(

											SELECT substring(date_courrier::text,1,10) AS date_courrier,1 AS flag
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_societe ON f_societe.id = pli.societe	
											WHERE 1= 1 $clauseWhere
											GROUP BY substring(date_courrier::text, 1, 10)
										
									) AS data_pli ON data_pli.flag = data_user.flag
								) AS data_param LEFT JOIN 
								(
									SELECT date_courrier, typage_par, 
									SUM(pli_typage_fini) AS pli_typage_fini	
									FROM 
										(
											SELECT pli.typage_par AS typage_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_typage_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1  $clauseWhere
											AND (pli.flag_traitement >= 2)  AND  pli.typage_par IS NOT NULL
											GROUP BY  pli.typage_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, typage_par 
								)AS data_typage 
								ON data_typage.date_courrier = data_param.date_courrier
								AND  data_typage.typage_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, saisie_par, 
									SUM(pli_saisie_finie) AS pli_saisie_finie	
									FROM 
										(
											SELECT pli.saisie_par AS saisie_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_saisie_finie
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
											WHERE 1= 1 $clauseWhere 
											AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL
											GROUP BY  pli.saisie_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, saisie_par 
								)AS data_saisie 
								ON data_saisie.date_courrier = data_param.date_courrier
								AND  data_saisie.saisie_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, controle_par, 
									SUM(pli_controle_fini) AS pli_controle_fini	
									FROM 
										(
											SELECT 
												CASE WHEN pli.controle_par IS NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par 
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS controle_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_controle_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND (pli.flag_traitement = 9 AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL ) )
											GROUP BY  pli.controle_par,pli.retraite_par, date_courrier
										) AS data_typage
									GROUP BY date_courrier, controle_par 
								)AS data_controle
								ON data_controle.date_courrier = data_param.date_courrier
								AND  data_controle.controle_par = data_param.id_utilisateur
								LEFT JOIN 
								(
									SELECT date_courrier, termine_par, 
									SUM(pli_termine) AS pli_termine	
									FROM 
										(
											SELECT 
												CASE WHEN
													  pli.typage_par IS NOT NULL AND pli.saisie_par IS NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.typage_par
												WHEN  pli.saisie_par IS NOT NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.saisie_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS termine_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_termine
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
											or pli.statut_saisie in (8,10,24))
											AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL )
											GROUP BY  pli.typage_par ,pli.saisie_par, pli.controle_par,pli.retraite_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, termine_par 
								)AS data_termine
								ON data_termine.date_courrier = data_param.date_courrier
								AND  data_termine.termine_par = data_param.id_utilisateur
								LEFT JOIN 
								(

									SELECT count (pli.id_pli) as pli_recu, substring(date_courrier::text,1,10) as date_courrier
									FROM  pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									INNER JOIN f_societe ON f_societe.id = pli.societe	
									WHERE 1= 1 $clauseWhere
									GROUP BY substring(date_courrier::text, 1, 10)
										

								) as data_recu
								ON data_recu.date_courrier = data_param.date_courrier
								
						) as data_pli on  data_pli.date_courrier::date between daty and ( daty + interval '1 month' - interval '1 day' )::date
										
						GROUP BY data_dates.daty,date_deb,date_fin, data_pli.id_utilisateur,data_pli.login
					 )as res
					 WHERE pli_typage_fini > 0 or pli_saisie_finie > 0 or  pli_controle_fini > 0 or  pli_termine > 0	
					) as tab
                    ");
		}
		
		$i = 0;
        foreach ($this->column_search_ind_op as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->base_64->group_start();
                    $this->base_64->ilike($item, $search);
                }
                else
                {
                    $this->base_64->or_ilike($item, $search);
                }

                if(count($this->column_search_ind_op) - 1 == $i)
                    $this->base_64->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->base_64->order_by($this->column_order_ind_op[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_ind_op))
        {
            $order = $this->order_ind_op;
            $this->base_64->order_by(key($order), $order[key($order)]);
        }
    }
	
	public function get_indicateur_prod_op($clauseWhere,$granularite,$where_date){
			
		if($granularite == 'j'){
        $sql = "SELECT 
						data_param.date_courrier as date_courrier ,
						data_param.date_courrier as date_deb, 
						data_param.date_courrier as date_fin, 
						data_param.id_utilisateur,
						data_param.login,
						pli_recu,
						pli_typage_fini,
						pli_saisie_finie,
						pli_controle_fini,
						pli_termine
					FROM 
					(SELECT date_courrier,id_utilisateur,login
						FROM 
						(
							SELECT id_utilisateur , login,1 flag 
							FROM f_utilisateurs
							WHERE actif = 1 AND supprime = 0
							ORDER BY login asc
						)data_user LEFT JOIN
						(

								SELECT substring(date_courrier::text,1,10) AS date_courrier,1 AS flag
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_societe ON f_societe.id = pli.societe	
								WHERE 1= 1 $clauseWhere
								GROUP BY substring(date_courrier::text,1,10)
							
						) AS data_pli ON data_pli.flag = data_user.flag
					) AS data_param LEFT JOIN 
					(
						SELECT date_courrier, typage_par, 
						SUM(pli_typage_fini) AS pli_typage_fini	
						FROM 
							(
								SELECT pli.typage_par AS typage_par,substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_typage_fini
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								WHERE 1= 1 $clauseWhere 
								AND (pli.flag_traitement >= 2)  AND  pli.typage_par IS NOT NULL
								GROUP BY  pli.typage_par,date_courrier
							) AS data_typage
						GROUP BY date_courrier, typage_par 
					)AS data_typage 
					ON data_typage.date_courrier = data_param.date_courrier
					AND  data_typage.typage_par = data_param.id_utilisateur

					LEFT JOIN 
					(
						SELECT date_courrier, saisie_par, 
						SUM(pli_saisie_finie) AS pli_saisie_finie	
						FROM 
							(
								SELECT pli.saisie_par AS saisie_par,substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_saisie_finie
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
								WHERE 1= 1 $clauseWhere
								AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL
								GROUP BY  pli.saisie_par,date_courrier
							) AS data_typage
						GROUP BY date_courrier, saisie_par 
					)AS data_saisie 
					ON data_saisie.date_courrier = data_param.date_courrier
					AND  data_saisie.saisie_par = data_param.id_utilisateur

					LEFT JOIN 
					(
						SELECT date_courrier, controle_par, 
						SUM(pli_controle_fini) AS pli_controle_fini	
						FROM 
							(
								SELECT 
									CASE WHEN pli.controle_par IS NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par 
									WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
									WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par
									end AS controle_par,
									substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_controle_fini
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								WHERE 1= 1 $clauseWhere
								AND (pli.flag_traitement = 9 AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL ) )
								GROUP BY  pli.controle_par,pli.retraite_par, date_courrier
							) AS data_typage
						GROUP BY date_courrier, controle_par 
					)AS data_controle
					ON data_controle.date_courrier = data_param.date_courrier
					AND  data_controle.controle_par = data_param.id_utilisateur
					LEFT JOIN 
					(
						SELECT date_courrier, termine_par, 
						SUM(pli_termine) AS pli_termine	
						FROM 
							(
								SELECT 
									CASE WHEN
										  pli.typage_par IS NOT NULL AND pli.saisie_par IS NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.typage_par
									WHEN  pli.saisie_par IS NOT NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.saisie_par
									WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
									WHEN  pli.retraite_par IS NOT NULL THEN pli.retraite_par
									end AS termine_par,
									substring(date_courrier::text,1,10) AS date_courrier,
									COUNT(DISTINCT  pli.id_pli) AS pli_termine
								FROM  pli
								INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
								WHERE 1= 1 $clauseWhere
								AND ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
									or pli.statut_saisie in (8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL)
								GROUP BY  pli.typage_par ,pli.saisie_par, pli.controle_par,pli.retraite_par,date_courrier
							) AS data_typage
						GROUP BY date_courrier, termine_par 
					)AS data_termine
					ON data_termine.date_courrier = data_param.date_courrier
					AND  data_termine.termine_par = data_param.id_utilisateur
					LEFT JOIN 
					(

						SELECT count (pli.id_pli) as pli_recu, substring(date_courrier::text,1,10) as date_courrier
						FROM  pli
						INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
						INNER JOIN f_societe ON f_societe.id = pli.societe	
						WHERE 1= 1 $clauseWhere
						GROUP BY substring(date_courrier::text, 1, 10)
							

					) as data_recu
					ON data_recu.date_courrier = data_param.date_courrier
					where pli_typage_fini > 0 or pli_saisie_finie > 0 or  pli_controle_fini > 0 or  pli_termine > 0 
				ORDER BY date_courrier,login asc
                 ";
		}
		if($granularite == 's'){
        $sql = "SELECT
							'S'||week as date_courrier,
							date_deb, 
							date_fin,
							data_pli.id_utilisateur,
							data_pli.login, 
							sum(pli_recu) pli_recu,
							sum(pli_typage_fini) pli_typage_fini,
							sum(pli_saisie_finie) pli_saisie_finie,
							sum(pli_controle_fini) pli_controle_fini,
							sum(pli_termine) pli_termine
						FROM
						(
							SELECT min (daty) as date_deb, max(daty) as date_fin,week
							FROM
							(
								select daty,week from liste_weeks($where_date)
							) as tab
							group by week
						) data_dates
						LEFT JOIN
						(
								SELECT 
									data_param.date_courrier,data_param.id_utilisateur,data_param.login, 
									pli_typage_fini,
									pli_saisie_finie,
									pli_controle_fini,
									pli_termine, 
									pli_recu
								FROM 
								(	SELECT date_courrier,id_utilisateur,login
									FROM 
									(
										SELECT id_utilisateur , login,1 flag 
										FROM f_utilisateurs
										WHERE actif = 1 AND supprime = 0
										ORDER BY login asc
									)data_user LEFT JOIN
									(

											SELECT substring(date_courrier::text,1,10) AS date_courrier,1 AS flag
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_societe ON f_societe.id = pli.societe	
											WHERE 1= 1 $clauseWhere
											GROUP BY substring(date_courrier::text,1,10)
										
									) AS data_pli ON data_pli.flag = data_user.flag
								) AS data_param LEFT JOIN 
								(
									SELECT date_courrier, typage_par, 
									SUM(pli_typage_fini) AS pli_typage_fini	
									FROM 
										(
											SELECT pli.typage_par AS typage_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_typage_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND (pli.flag_traitement >= 2)  AND  pli.typage_par IS NOT NULL
											GROUP BY  pli.typage_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, typage_par 
								)AS data_typage 
								ON data_typage.date_courrier = data_param.date_courrier
								AND  data_typage.typage_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, saisie_par, 
									SUM(pli_saisie_finie) AS pli_saisie_finie	
									FROM 
										(
											SELECT pli.saisie_par AS saisie_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_saisie_finie
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
											WHERE 1= 1 $clauseWhere
											AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL
											GROUP BY  pli.saisie_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, saisie_par 
								)AS data_saisie 
								ON data_saisie.date_courrier = data_param.date_courrier
								AND  data_saisie.saisie_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, controle_par, 
									SUM(pli_controle_fini) AS pli_controle_fini	
									FROM 
										(
											SELECT 
												CASE WHEN pli.controle_par IS NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par 
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS controle_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_controle_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1  $clauseWhere
											AND (pli.flag_traitement = 9 AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL ) )
											GROUP BY  pli.controle_par,pli.retraite_par, date_courrier
										) AS data_typage
									GROUP BY date_courrier, controle_par 
								)AS data_controle
								ON data_controle.date_courrier = data_param.date_courrier
								AND  data_controle.controle_par = data_param.id_utilisateur
								LEFT JOIN 
								(
									SELECT date_courrier, termine_par, 
									SUM(pli_termine) AS pli_termine	
									FROM 
										(
											SELECT 
												CASE WHEN
													  pli.typage_par IS NOT NULL AND pli.saisie_par IS NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.typage_par
												WHEN  pli.saisie_par IS NOT NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.saisie_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS termine_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_termine
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
											or pli.statut_saisie in (8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL)
											GROUP BY  pli.typage_par ,pli.saisie_par, pli.controle_par,pli.retraite_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, termine_par 
								)AS data_termine
								ON data_termine.date_courrier = data_param.date_courrier
								AND  data_termine.termine_par = data_param.id_utilisateur
								LEFT JOIN 
								(

									SELECT count (pli.id_pli) as pli_recu, substring(date_courrier::text,1,10) as date_courrier
									FROM  pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									INNER JOIN f_societe ON f_societe.id = pli.societe	
									WHERE 1= 1 $clauseWhere
									GROUP BY substring(date_courrier::text,1,10)
										

								) as data_recu
								ON data_recu.date_courrier = data_param.date_courrier
								
					) as data_pli on  data_pli.date_courrier::date between date_deb and date_fin
								
				GROUP BY data_dates.week,date_deb, date_fin,data_pli.id_utilisateur,data_pli.login	
				)as res  
				WHERE pli_typage_fini > 0 or pli_saisie_finie > 0 or  pli_controle_fini > 0 or  pli_termine > 0				
				";
		}
		if($granularite == 'm'){
        $sql = "SELECT
							date_courrier,
							date_deb,
							date_fin,
							id_utilisateur,
							login, 
							pli_recu,
							pli_typage_fini,
							pli_saisie_finie,
							pli_controle_fini,
							pli_termine
						FROM(SELECT
							substr(daty::text,1,7) as date_courrier,
							daty as date_deb,
							( daty + interval '1 month' - interval '1 day' )::date as date_fin,
							data_pli.id_utilisateur,
							data_pli.login, 
							sum(pli_recu) pli_recu,
							sum(pli_typage_fini) pli_typage_fini,
							sum(pli_saisie_finie) pli_saisie_finie,
							sum(pli_controle_fini) pli_controle_fini,
							sum(pli_termine) pli_termine
						FROM
						(
							select liste_months as daty from liste_months($where_date)
						) data_dates
						LEFT JOIN
						(
								SELECT 
									data_param.date_courrier,data_param.id_utilisateur,data_param.login, 
									pli_typage_fini,
									pli_saisie_finie,
									pli_controle_fini,
									pli_termine, 
									pli_recu
								FROM 
								(	SELECT date_courrier,id_utilisateur,login
									FROM 
									(
										SELECT id_utilisateur , login,1 flag 
										FROM f_utilisateurs
										WHERE actif = 1 AND supprime = 0
										ORDER BY login asc
									)data_user LEFT JOIN
									(

											SELECT substring(date_courrier::text,1,10) AS date_courrier,1 AS flag
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_societe ON f_societe.id = pli.societe	
											WHERE 1= 1 $clauseWhere
											GROUP BY substring(date_courrier::text, 1, 10)
										
									) AS data_pli ON data_pli.flag = data_user.flag
								) AS data_param LEFT JOIN 
								(
									SELECT date_courrier, typage_par, 
									SUM(pli_typage_fini) AS pli_typage_fini	
									FROM 
										(
											SELECT pli.typage_par AS typage_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_typage_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1  $clauseWhere
											AND (pli.flag_traitement >= 2)  AND  pli.typage_par IS NOT NULL
											GROUP BY  pli.typage_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, typage_par 
								)AS data_typage 
								ON data_typage.date_courrier = data_param.date_courrier
								AND  data_typage.typage_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, saisie_par, 
									SUM(pli_saisie_finie) AS pli_saisie_finie	
									FROM 
										(
											SELECT pli.saisie_par AS saisie_par,substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_saisie_finie
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
											WHERE 1= 1 $clauseWhere 
											AND flag_saisie = 1 AND pli.saisie_par IS NOT NULL
											GROUP BY  pli.saisie_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, saisie_par 
								)AS data_saisie 
								ON data_saisie.date_courrier = data_param.date_courrier
								AND  data_saisie.saisie_par = data_param.id_utilisateur

								LEFT JOIN 
								(
									SELECT date_courrier, controle_par, 
									SUM(pli_controle_fini) AS pli_controle_fini	
									FROM 
										(
											SELECT 
												CASE WHEN pli.controle_par IS NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par 
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS controle_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_controle_fini
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND (pli.flag_traitement = 9 AND (pli.controle_par IS NOT NULL  OR pli.retraite_par IS NOT NULL ) )
											GROUP BY  pli.controle_par,pli.retraite_par, date_courrier
										) AS data_typage
									GROUP BY date_courrier, controle_par 
								)AS data_controle
								ON data_controle.date_courrier = data_param.date_courrier
								AND  data_controle.controle_par = data_param.id_utilisateur
								LEFT JOIN 
								(
									SELECT date_courrier, termine_par, 
									SUM(pli_termine) AS pli_termine	
									FROM 
										(
											SELECT 
												CASE WHEN
													  pli.typage_par IS NOT NULL AND pli.saisie_par IS NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.typage_par
												WHEN  pli.saisie_par IS NOT NULL AND pli.controle_par IS NULL AND pli.retraite_par IS NULL THEN pli.saisie_par
												WHEN  pli.controle_par IS NOT NULL AND pli.retraite_par IS NULL THEN pli.controle_par
												WHEN  pli.retraite_par IS NOT NULL THEN pli.retraite_par
												end AS termine_par,
												substring(date_courrier::text,1,10) AS date_courrier,
												COUNT(DISTINCT  pli.id_pli) AS pli_termine
											FROM  pli
											INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
											WHERE 1= 1 $clauseWhere
											AND ((pli.flag_traitement  in(9,14) and pli.statut_saisie = 1)
											or pli.statut_saisie in (8,10,24)) and (controle_par IS NOT NULL or retraite_par IS NOT NULL)
											GROUP BY  pli.typage_par ,pli.saisie_par, pli.controle_par,pli.retraite_par,date_courrier
										) AS data_typage
									GROUP BY date_courrier, termine_par 
								)AS data_termine
								ON data_termine.date_courrier = data_param.date_courrier
								AND  data_termine.termine_par = data_param.id_utilisateur
								LEFT JOIN 
								(

									SELECT count (pli.id_pli) as pli_recu, substring(date_courrier::text,1,10) as date_courrier
									FROM  pli
									INNER JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
									INNER JOIN f_societe ON f_societe.id = pli.societe	
									WHERE 1= 1 $clauseWhere
									GROUP BY substring(date_courrier::text, 1, 10)
										

								) as data_recu
								ON data_recu.date_courrier = data_param.date_courrier
								
						) as data_pli on  data_pli.date_courrier::date between daty and ( daty + interval '1 month' - interval '1 day' )::date
										
						GROUP BY data_dates.daty,date_deb,date_fin, data_pli.id_utilisateur,data_pli.login
					 )as res
					 WHERE pli_typage_fini > 0 or pli_saisie_finie > 0 or  pli_controle_fini > 0 or  pli_termine > 0	
				 ";
				
		}
		
		 return $this->base_64->query($sql)
							->result_array();
		
    }
	
   /* */
   
}
