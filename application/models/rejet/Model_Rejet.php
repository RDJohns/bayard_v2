<?php
/**
 * Created by PhpStorm.
 * User: 9420
 * Date: 12/03/2019
 * Time: 16:26
 */

class Model_Rejet extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;
    private $motifRejet     = "motif_rejet";

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function motifRejet()
    {
        return $this->ged
            ->select('id,motif')
            ->from($this->motifRejet)
            ->where('flag',1)
            ->order_by("motif asc")
            ->get()->result();
    }
}