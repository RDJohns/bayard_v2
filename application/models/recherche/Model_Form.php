<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Model_Form extends CI_Model
    {
        var $table         = 'view_liste_pli_circulaire';
        var $raSelect      = "ra_select";
        var $raColonne     = "view_ra_liste_colonne";

        private $CI;
        private $ged;
        private $base_64;
        private $ged_push;
        
        private $filtre = "view_ra_filtre";
        function __construct()
        {
            parent::__construct();
            $this->CI       = &get_instance();
            $this->ged      = $this->load->database('ged', TRUE);
            $this->base_64  = $this->load->database('base_64', TRUE);
            $this->ged_push = $this->load->database('ged_push', TRUE);
        }

        public function getFiltre()
        {
            return $this->ged->select('*')->from($this->filtre)->get()->result(); 
        }

        public function idChamp($idFiltre)
        {
            return $this->ged->select('*')->from($this->filtre)->where("id",(int)$idFiltre)->get()->result(); 
        }

        public function selectChamp($idFiltre)
        {
            return $this->ged->select('*')->from($this->raSelect)->where("ra_filtre_id",(int)$idFiltre)->order_by("tri","asc")->get()->result(); 
        }

        public function raColonne($arrayID)
        {
           $sqlRa = "
            SELECT distinct ra_colonne_id, colonne,affichage,colonne_db,ordre
            FROM public.view_ra_liste_colonne where rubrique_id in(SELECT  rubrique_id
            FROM public.ra_filtre where id in(".implode(",", $arrayID).")) order by ordre asc,colonne asc";

            $query = $this->ged->query($sqlRa);
             return $query->result();
        }

        
    }