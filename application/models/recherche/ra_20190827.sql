-- View: public.view_ra_courrier

-- DROP VIEW public.view_ra_courrier;

CREATE OR REPLACE VIEW public.view_ra_courrier AS 
 SELECT tab_final.id_pli,
    tab_final.pli,
    tab_final.societe,
    tab_final.nom_societe,
    tab_final.typologie_id,
    tab_final.typologie,
    tab_final.id_lot_numerisation,
    tab_final.id_lot_saisie,
    tab_final.lot_saisie_bis,
    tab_final.date_creation,
    tab_final.date_reception,
    tab_final.dt_event AS date_traitement,
    tab_final.commande,
    tab_final.lot_scan,
    tab_final.mode_paiement,
    tab_final.flag_traitement,
    tab_final.statut,
    tab_final.libelle,
    tab_final.nom_abonne,
    tab_final.numero_abonne,
    tab_final.numero_payeur,
    tab_final.nom_payeur,
    tab_final.avec_chq,
    tab_final.cmc7,
    tab_final.montant,
    tab_final.nom_client,
    tab_final.anomalie,
    tab_final.nom_deleg,
    tab_final.motif_ko,
    tab_final.nom_orig_fichier_circulaire,
    tab_final.nom_circulaire_id,
    tab_final.nom_circulaire,
    tab_final.motif_ko_scan_id,
    tab_final.motif_ko_scan,
    tab_final.numero_image_verso,
    tab_final.id_doc,
    tab_final.nb_desarchive,
    tab_final.id_doc_desarchive,
    tab_final.image_desarchive,
    tab_final.paiement_chq,
    tab_final.paiement_chq_kd,
    tab_final.paiement_espece,
    tab_final.paiement_prelevmnt_bc,
    tab_final.sans_paiement,
    tab_final.nb_mouvement,
    tab_final.titre,
    tab_final.code,
    tab_final.type_chq_kdo,
    tab_final.fin_colonne
   FROM ( SELECT tab.id_pli,
            tab.pli,
            tab.societe,
            tab.nom_societe,
            tab.typologie_id,
            tab.typologie,
            tab.id_lot_numerisation,
            tab.id_lot_saisie,
            tab.lot_saisie_bis,
            tab.date_creation,
            tab.date_reception,
            tab.dt_event,
            tab.commande,
            tab.lot_scan,
            unique_field(string_agg(tab.mode_paiement::text, ' ; '::text)) AS mode_paiement,
            tab.flag_traitement,
                CASE
                    WHEN tab.statut = tab.libelle::text THEN 'Cloturé'::text
                    ELSE tab.statut
                END AS statut,
            tab.libelle,
            unique_field(string_agg(tab.nom_abonne, ';'::text)) AS nom_abonne,
            unique_field(string_agg(tab.numero_abonne, ';'::text)) AS numero_abonne,
            unique_field(string_agg(tab.numero_payeur, ';'::text)) AS numero_payeur,
            unique_field(string_agg(tab.nom_payeur, ';'::text)) AS nom_payeur,
            tab.avec_chq,
            unique_field(string_agg(tab.cmc7, ';'::text)) AS cmc7,
            unique_field(string_agg(tab.montant, ';'::text)) AS montant,
            unique_field(string_agg(tab.nom_client, ';'::text)) AS nom_client,
            unique_field(string_agg(tab.anomalie::text, ' ; '::text)) AS anomalie,
            tab.nom_deleg,
            tab.motif_ko,
            tab.nom_orig_fichier_circulaire,
            tab.nom_circulaire_id,
            tab.nom_circulaire,
            tab.motif_ko_scan_id,
            tab.motif_ko_scan,
            unique_field(string_agg(tab.image_recto::text, ';'::text)) AS numero_image_verso,
            unique_field(string_agg(tab.id_doc::text, ';'::text)) AS id_doc,
            tab.nb_desarchive,
            tab.id_doc_desarchive,
            tab.image_desarchive,
                CASE
                    WHEN sum(tab.paiement_chq) > 0 THEN 1::bigint
                    ELSE sum(tab.paiement_chq)
                END AS paiement_chq,
                CASE
                    WHEN sum(tab.paiement_chq_kd) > 0 THEN 1::bigint
                    ELSE sum(tab.paiement_chq_kd)
                END AS paiement_chq_kd,
                CASE
                    WHEN sum(tab.paiement_espece) > 0 THEN 1::bigint
                    ELSE sum(tab.paiement_espece)
                END AS paiement_espece,
                CASE
                    WHEN sum(tab.paiement_prelevmnt_bc) > 0 THEN 1::bigint
                    ELSE sum(tab.paiement_prelevmnt_bc)
                END AS paiement_prelevmnt_bc,
                CASE
                    WHEN sum(tab.sans_paiement) > 0 THEN 1::bigint
                    ELSE sum(tab.sans_paiement)
                END AS sans_paiement,
            tab.nb_mouvement,
            tab.titre,
            tab.code,
            tab.type_chq_kdo,
            1 AS fin_colonne
           FROM ( SELECT DISTINCT f_pli.id_pli,
                    f_pli.pli,
                    f_pli.societe,
                    societe.nom_societe,
                    f_lot_numerisation.lot_scan,
                    f_pli.typologie AS typologie_id,
                        CASE
                            WHEN f_pli.typologie IS NULL OR f_pli.typologie = 0 THEN 'Non typé'::character varying
                            ELSE typologie.typologie
                        END AS typologie,
                    f_pli.id_lot_numerisation,
                    f_pli.id_lot_saisie,
                    f_pli.lot_saisie_bis,
                    histo_pli.flag_traitement,
                    f_pli.date_creation,
                    f_lot_numerisation.date_courrier::date AS date_reception,
                    histo_pli.dt_event,
                    f_lot_numerisation.commande,
                    paiement.id_mode_paiement,
                    mode_paiement.mode_paiement,
                        CASE
                            WHEN histo_pli.flag_traitement = 0 OR histo_pli.flag_traitement IS NULL THEN 'Non traité'::text
                            ELSE
                            CASE
                                WHEN histo_pli.flag_traitement >= 1 AND histo_pli.flag_traitement <= 8 OR histo_pli.flag_traitement >= 10 AND histo_pli.flag_traitement <= 13 OR (histo_pli.flag_traitement = ANY (ARRAY[9, 14, 15, 18, 20])) AND view_pli_avec_cheque_new.id_pli IS NOT NULL OR (histo_pli.flag_traitement = ANY (ARRAY[17, 19])) OR (histo_pli.flag_traitement = ANY (ARRAY[23, 22])) THEN 'En cours '::text
                                ELSE
                                CASE
                                    WHEN histo_pli.flag_traitement = 22 THEN 'Matchage terminé'::text
                                    ELSE
                                    CASE
WHEN histo_pli.flag_traitement = 24 THEN 'Fermé'::text
ELSE
CASE
 WHEN histo_pli.flag_traitement = 16 THEN 'KO scan'::text
 ELSE
 CASE
  WHEN histo_pli.flag_traitement = 17 THEN 'KO saisie'::text
  ELSE
  CASE
   WHEN histo_pli.flag_traitement = 21 THEN 'Hors périmètre'::text
   ELSE
   CASE
    WHEN histo_pli.flag_traitement = 18 OR histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND data_pli.statut_saisie = 4 THEN 'KS'::text
    ELSE
    CASE
     WHEN histo_pli.flag_traitement = 19 OR histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND data_pli.statut_saisie = 5 THEN 'KE'::text
     ELSE
     CASE
      WHEN histo_pli.flag_traitement = 20 OR histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND (data_pli.statut_saisie = ANY (ARRAY[2, 6])) THEN 'KD'::text
      ELSE
      CASE
       WHEN histo_pli.flag_traitement = 25 OR histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND data_pli.statut_saisie = 3 THEN 'CI'::text
       ELSE
       CASE
        WHEN (histo_pli.flag_traitement = ANY (ARRAY[9, 14, 15])) AND view_pli_avec_cheque_new.id_pli IS NULL OR histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND data_pli.statut_saisie = 1 THEN 'OK'::text
        ELSE
        CASE
         WHEN (histo_pli.flag_traitement = ANY (ARRAY[16, 21])) OR histo_pli.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL AND (data_pli.statut_saisie = ANY (ARRAY[2, 3, 4, 5, 6])) THEN 'KO'::text
         ELSE ''::text
        END
       END
      END
     END
    END
   END
  END
 END
END
                                    END
                                END
                            END
                        END AS statut,
                    statut_saisie.libelle,
                    mouvement.nom_abonne,
                    mouvement.numero_abonne,
                    mouvement.numero_payeur,
                    mouvement.nom_payeur,
                        CASE
                            WHEN paiement_cheque.id_pli IS NOT NULL THEN 1
                            ELSE 0
                        END AS avec_chq,
                    paiement_cheque.cmc7,
                    paiement_cheque.montant,
                    paiement_cheque.nom_client,
                    paiement_cheque.anomalie_cheque_id,
                    anomalie_cheque.anomalie,
                    data_pli.nom_deleg,
                        CASE
                            WHEN data_pli.autre_motif_ko IS NULL OR data_pli.autre_motif_ko::text = ''::text THEN motif_ko.libelle_motif::text
                            ELSE concat(motif_ko.libelle_motif, ' > ', data_pli.autre_motif_ko)
                        END AS motif_ko,
                    data_pli.nom_orig_fichier_circulaire,
                    data_pli.nom_circulaire AS nom_circulaire_id,
                    liste_circulaires.circulaire AS nom_circulaire,
                    motif_ko_scan.motif_rejet_id AS motif_ko_scan_id,
                        CASE
                            WHEN motif_ko_scan.description IS NULL OR motif_ko_scan.description = ''::text THEN motif_rejet.motif::text
                            ELSE concat(motif_rejet.motif, ' > ', motif_ko_scan.description)
                        END AS motif_ko_scan,
                    f_document_si.n_ima_recto AS image_recto,
                    f_document_si.id_document AS id_doc,
                    view_document_desarchive.nb_desarchive,
                    view_document_desarchive.id_doc_desarchive,
                    view_document_desarchive.image_desarchive,
                        CASE
                            WHEN paiement.id_mode_paiement = 1 THEN 1
                            ELSE 0
                        END AS paiement_chq,
                        CASE
                            WHEN paiement.id_mode_paiement = 6 THEN 1
                            ELSE 0
                        END AS paiement_chq_kd,
                        CASE
                            WHEN paiement.id_mode_paiement = 5 THEN 1
                            ELSE 0
                        END AS paiement_espece,
                        CASE
                            WHEN paiement.id_mode_paiement = 2 THEN 1
                            ELSE 0
                        END AS paiement_prelevmnt_bc,
                        CASE
                            WHEN paiement.id_mode_paiement = 10 THEN 1
                            ELSE 0
                        END AS sans_paiement,
                    view_nb_mouvement_pli.nb_mvt AS nb_mouvement,
                    titres.titre,
                    titres.code,
                    view_type_chq_kdo.type_chq_kdo,
                    1 AS fin_colonne
                   FROM f_pli
                     JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
                     LEFT JOIN view_histo_pli_oid histo_pli ON histo_pli.id_pli = f_pli.id_pli
                     LEFT JOIN f_lot_numerisation f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
                     LEFT JOIN paiement ON paiement.id_pli = f_pli.id_pli
                     LEFT JOIN mode_paiement ON mode_paiement.id_mode_paiement = paiement.id_mode_paiement
                     LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli
                     LEFT JOIN view_ra_mouvement mouvement ON mouvement.id_pli = f_pli.id_pli
                     LEFT JOIN view_ra_paiement_cheque paiement_cheque ON paiement_cheque.id_pli = f_pli.id_pli
                     LEFT JOIN anomalie_cheque ON anomalie_cheque.id = paiement_cheque.anomalie_cheque_id
                     LEFT JOIN motif_ko ON motif_ko.id_motif = data_pli.motif_ko
                     LEFT JOIN statut_saisie ON statut_saisie.id_statut_saisie = data_pli.statut_saisie
                     LEFT JOIN liste_circulaires ON liste_circulaires.id = data_pli.nom_circulaire
                     LEFT JOIN view_nb_mouvement_pli ON view_nb_mouvement_pli.id_pli = f_pli.id_pli
                     LEFT JOIN f_document_si ON f_document_si.id_pli = f_pli.id_pli
                     LEFT JOIN view_document_desarchive ON view_document_desarchive.id_pli = f_pli.id_pli
                     LEFT JOIN view_type_chq_kdo ON view_type_chq_kdo.id_pli = f_pli.id_pli
                     LEFT JOIN motif_ko_scan ON motif_ko_scan.pli_id = f_pli.id_pli
                     LEFT JOIN motif_rejet ON motif_rejet.id = motif_ko_scan.motif_rejet_id
                     LEFT JOIN typologie ON typologie.id = f_pli.typologie
                     LEFT JOIN societe ON societe.id = f_pli.societe
                     LEFT JOIN titres ON titres.id = data_pli.titre
                  ORDER BY f_pli.id_pli) tab
          GROUP BY tab.id_pli, tab.pli, tab.societe, tab.nom_societe, tab.typologie_id, tab.typologie, tab.id_lot_numerisation, tab.id_lot_saisie, tab.lot_saisie_bis, tab.date_creation, tab.date_reception, tab.dt_event, tab.commande, tab.lot_scan, tab.flag_traitement, tab.statut, tab.libelle, tab.avec_chq, tab.nom_deleg, tab.motif_ko, tab.nom_orig_fichier_circulaire, tab.nom_circulaire_id, tab.nom_circulaire, tab.motif_ko_scan_id, tab.motif_ko_scan, tab.nb_desarchive, tab.id_doc_desarchive, tab.image_desarchive, tab.nb_mouvement, tab.titre, tab.code, tab.type_chq_kdo, tab.fin_colonne) tab_final;

ALTER TABLE public.view_ra_courrier
  OWNER TO si;


  /******************/
  CREATE OR REPLACE VIEW public.view_ra_mouvement AS
 SELECT mouvement.id_pli,
    mouvement.titre,
    string_agg(DISTINCT mouvement.nom_abonne::text, ', '::text) AS nom_abonne,
    string_agg(DISTINCT mouvement.numero_abonne::text, ', '::text) AS numero_abonne,
    string_agg(DISTINCT mouvement.numero_payeur::text, ', '::text) AS numero_payeur,
    string_agg(DISTINCT mouvement.nom_payeur::text, ', '::text) AS nom_payeur
   FROM mouvement
  GROUP BY mouvement.id_pli, mouvement.titre;

ALTER TABLE public.view_ra_mouvement
  OWNER TO si;
/********************/
CREATE OR REPLACE VIEW public.view_ra_paiement_cheque AS
 SELECT paiement_cheque.id_pli,
    paiement_cheque.anomalie_cheque_id,
    string_agg(DISTINCT paiement_cheque.cmc7::text, ','::text) AS cmc7,
    string_agg(DISTINCT paiement_cheque.montant::text, ','::text) AS montant,
    string_agg(DISTINCT paiement_cheque.nom_client::text, ','::text) AS nom_client
   FROM paiement_cheque
  GROUP BY paiement_cheque.id_pli, paiement_cheque.anomalie_cheque_id;

ALTER TABLE public.view_ra_paiement_cheque
  OWNER TO si;
/****************/
CREATE OR REPLACE VIEW public.view_ra_filtre AS
 SELECT ra_filtre.id,
    ra_filtre.rubrique_id,
    ra_filtre.colonne,
    ra_filtre.libelle AS libelle_enfant,
    ra_rubrique.libelle,
    ra_filtre.element_type,
    ra_filtre.type_donnes,
    ra_filtre.where_colonne,
    ra_rubrique.source
   FROM ra_filtre
     JOIN ra_rubrique ON ra_rubrique.id = ra_filtre.rubrique_id
  ORDER BY ra_filtre.rubrique_id, ra_filtre.id;

ALTER TABLE public.view_ra_filtre
  OWNER TO si;

/*************/
-- View: public.view_ra_paiement

-- DROP VIEW public.view_ra_paiement;

CREATE OR REPLACE VIEW public.view_ra_paiement AS
 SELECT final.id_pli,
    final.paiement
   FROM ( SELECT tab.id_pli,
            concat(tab.cheque, tab.cheque_kdo, tab.espece,
                CASE
                    WHEN concat(tab.cheque, tab.cheque_kdo, tab.espece) = ''::text OR concat(tab.cheque, tab.cheque_kdo, tab.espece) IS NULL THEN tab.autre_paiement
                    ELSE
                    CASE
                        WHEN tab.autre_paiement <> ''::text THEN concat(', ', tab.autre_paiement)
                        ELSE NULL::text
                    END
                END) AS paiement
           FROM ( SELECT paiement.id_pli,
                        CASE
                            WHEN COALESCE(paiement_cheque.nb_chq, 0::bigint) <= 0 OR COALESCE(paiement_cheque.nb_chq, 0::bigint) IS NULL THEN NULL::text
                            ELSE concat('  CHEQUE(S) : ', COALESCE(paiement_cheque.nb_chq, 0::bigint))
                        END AS cheque,
                        CASE
                            WHEN COALESCE(paiement_cheque_cadeau.nb_chq_kdo, 0::bigint) <= 0 OR COALESCE(paiement_espece.nb_espece, 0::bigint) IS NULL THEN NULL::text
                            ELSE concat(' ,  CHEQUE(S) CADEAU(X) : ', COALESCE(paiement_cheque_cadeau.nb_chq_kdo, 0::bigint))
                        END AS cheque_kdo,
                        CASE
                            WHEN COALESCE(paiement_espece.nb_espece, 0::bigint) <= 0 OR COALESCE(paiement_espece.nb_espece, 0::bigint) IS NULL THEN NULL::text
                            ELSE concat(',  ESPECE : ', COALESCE(paiement_espece.nb_espece, 0::bigint))
                        END AS espece,
                        CASE
                            WHEN paiement_autre.autre_paiement IS NULL THEN ''::text
                            ELSE concat(' ', paiement_autre.autre_paiement)
                        END AS autre_paiement
                   FROM ( SELECT paiement_1.id_pli,
                            count(paiement_1.id_pli) AS nb_paiement
                           FROM paiement paiement_1
                          GROUP BY paiement_1.id_pli) paiement
                     LEFT JOIN ( SELECT paiement_cheque_1.id_pli,
                            count(paiement_cheque_1.id_pli) AS nb_chq
                           FROM paiement_cheque paiement_cheque_1
                          WHERE paiement_cheque_1.montant::double precision > 0::double precision
                          GROUP BY paiement_cheque_1.id_pli) paiement_cheque ON paiement_cheque.id_pli = paiement.id_pli
                     LEFT JOIN ( SELECT paiement_espece_1.id_pli,
                            count(paiement_espece_1.id_pli) AS nb_espece
                           FROM paiement_espece paiement_espece_1
                          WHERE paiement_espece_1.montant::double precision > 0::double precision
                          GROUP BY paiement_espece_1.id_pli) paiement_espece ON paiement_espece.id_pli = paiement.id_pli
                     LEFT JOIN ( SELECT paiement_cheque_cadeau_1.id_pli,
                            count(paiement_cheque_cadeau_1.id_pli) AS nb_chq_kdo
                           FROM paiement_cheque_cadeau paiement_cheque_cadeau_1
                          WHERE paiement_cheque_cadeau_1.montant::double precision > 0::double precision
                          GROUP BY paiement_cheque_cadeau_1.id_pli) paiement_cheque_cadeau ON paiement_cheque_cadeau.id_pli = paiement.id_pli
                     LEFT JOIN ( SELECT paiement_autre_1.id_pli,
                            string_agg(mode_paiement.mode_paiement::text, ', '::text) AS autre_paiement
                           FROM paiement paiement_autre_1
                             LEFT JOIN mode_paiement ON mode_paiement.id_mode_paiement = paiement_autre_1.id_mode_paiement
                          WHERE paiement_autre_1.id_mode_paiement <> ALL (ARRAY[1, 5, 6])
                          GROUP BY paiement_autre_1.id_pli) paiement_autre ON paiement_autre.id_pli = paiement.id_pli) tab) final;

ALTER TABLE public.view_ra_paiement
  OWNER TO si;
