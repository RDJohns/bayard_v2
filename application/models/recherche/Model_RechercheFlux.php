<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Model_RechercheFlux extends CI_Model
    {
        
        private $CI;
        private $ged;
        private $base_64;
        private $ged_push;
        private $listeColonne;
        private $where;
        private $columnOrder;
        private $columnSearch;
        private $sql;
        private $table = "view_ra_flux";

        function __construct()
        {
            parent::__construct();
            $this->CI       = &get_instance();
            $this->ged      = $this->load->database('ged', TRUE);
            $this->base_64  = $this->load->database('base_64', TRUE);
            $this->ged_push = $this->load->database('ged_push', TRUE);
        }

        public function valeurUtile($where_,$listeColonne_)
        {
            $this->where            = $where_;
            $this->listeColonne     = $listeColonne_;
           
        }

        private function _get_query($filtre,$listeColonne)
        {
           

            $this->columnOrder      = $this->session->userdata('columnOrder');
            $this->columnSearch     = $this->session->userdata('columnOrder');
           
            
            $sqlTest = $this->createSql($filtre,$listeColonne);

           $this->ged_push->select("*")->from("(".$sqlTest.") as tab_");

           $i = 0;

           foreach ($this->columnSearch as $emp)
           {
               if(isset($_POST['search']['value']) && !empty($_POST['search']['value'])){
                   $_POST['search']['value'] = $_POST['search']['value'];
               } else
                   $_POST['search']['value'] = '';
               if($_POST['search']['value'])
               {
                   if($i===0) // first loop
                   {
                      $this->ged_push->group_start();
                      $this->ged_push->like($emp, $_POST['search']['value']);
                   }
                   else
                   {
                      $this->ged_push->or_like($emp, $_POST['search']['value']);
                   }
   
                   if(count($this->columnSearch) - 1 == $i) //last loop
                      $this->ged_push->group_end();
               }
               $i++;
           }
   
           if(isset($_POST['order'])) // here order processing
           {
              $this->ged_push->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
           }
           else if(isset($this->order))
           {
               $order = $this->order;
              $this->ged_push->order_by(key($order), $order[key($order)]);
           }
        }

        public function getListe($filtre,$listeColonne)
        {
            $this->_get_query($filtre,"id_flux as id_click,".$listeColonne);
            
            
            if((int)$_POST['length'] < 0)
            {
                 $_POST['length'] = (int)$_POST['recordTotal_'];
            }

            if(isset($_POST['length']) && $_POST['length'] < 1)
            {
                $_POST['length']= '10';
            }
            else
                $_POST['length']= $_POST['length'];

            if(isset($_POST['start']) && $_POST['start'] > 1)
            {
                $_POST['start']= $_POST['start'];
            }

           

           
            
            $this->ged_push->limit($_POST['length'], $_POST['start']);
            $query = $this->ged_push->get();

            return $query->result();
        }
        
        
        public function recordsTotal($filtre,$listeColonne)
        {
            
            $sqlTest = $this->createSql($filtre,$listeColonne);
            $this->ged_push->select("*")->from("(".$sqlTest.") as tab_");
            return $this->ged_push->count_all_results();
        }
        public function createSql($filtre,$listeColonne)
        {
            $strSql  = "1 = 1";
            
            foreach ($filtre as $item) 
            {
                $filtre = trim($item["filter"]); 

                if(is_array($item["value"]))
                {
                    $strValue = "'";
                    $strValue .= implode("','", $item["value"])."'";
                }
                elseif($filtre != "date")
                {
                    $strValue = "'".trim($item["value"])."'";
                }
            
                
                switch($filtre)
                {
                    case 'date':
                    list($date1,$date2) = explode("-",$item["value"]);
                    $str = " and ".$item["colonne"]."::date between '".trim($date1)."' and '".trim($date2)."' ";
                    break;

                    case 'in':
                       $str = " and ".trim($item["colonne"])."::text in(".$strValue.") ";
                    break;

                    case 'not_in':
                        $str = " and ".trim($item["colonne"])."::text not in(".$strValue.") ";
                        break;
                    
                    case 'ilike':
                        $str = " and ".trim($item["colonne"])."::text ilike '%".str_replace("'","",$strValue)."%' ";
                        break;   
                }
                
                $strSql .=$str;
            }

            return $sqlTest = "SELECT  ".$listeColonne." FROM public.view_ra_flux where ".$strSql;
        }
        public function recordsFiltered($filtre,$listeColonne)
        {
            $this->_get_query($filtre,"id_flux as id_click,".$listeColonne);
            $query = $this->ged_push->get();
            return $query->num_rows();
        }

        public function exportExcel($filtre,$listeColonne)
        {
            $sql = $this->createSql($filtre,$listeColonne);
            $query = $this->ged_push->query($sql);
            
            return $query->result();    
            
        }

       
        

    }