<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Model_Recherche extends CI_Model
    {
        /*92006*/
        private $CI;
        private $ged;
        private $base_64;
        private $ged_push;
        private $listeColonne;
        private $where;
        private $columnOrder;
        private $columnSearch;
        private $sql;
        private $table = "view_ra_courrier";
        private $raFct = "ra_where_to_fonction";
        private $whereException = array("nom_abonne","numero_abonne","numero_payeur","nom_payeur","cmc7","montant","nom_image","paiement","id_pli");

        function __construct()
        {
            parent::__construct();
            $this->CI       = &get_instance();
            $this->ged      = $this->load->database('ged', TRUE); 
            $this->base_64  = $this->load->database('base_64', TRUE);
            $this->ged_push = $this->load->database('ged_push', TRUE);


        }

        public function valeurUtile($where_,$listeColonne_)
        {
            $this->where            = $where_;
            $this->listeColonne     = $listeColonne_;
           
        }

        private function _get_query($filtre,$listeColonne)
        {
           $sqlRa = $this->createSql($filtre,$listeColonne);
           return $sqlRa;
           
           
        }

        public function getListe($filtre,$listeColonne)
        {
            $this->columnOrder      = $this->session->userdata('columnOrder');
            $this->columnSearch     = $this->session->userdata('columnOrder');

            $sqlRa= $this->_get_query($filtre,"id_pli as id_click,".$listeColonne);
            
            if((int)$_POST['length'] < 0)
            {
                 $_POST['length'] = (int)$_POST['recordTotal_'];
            }

            if(isset($_POST['length']) && $_POST['length'] < 1)
            {
                $_POST['length']= '10';
            }
            else
                $_POST['length']= $_POST['length'];

            if(isset($_POST['start']) && $_POST['start'] > 1)
            {
                $_POST['start']= $_POST['start'];
            }
            
            

            $sqlFianal = $sqlRa." order by id_click LIMIT ".$_POST['length']." OFFSET ".$_POST['start'];

            $query = $this->ged->query($sqlFianal);
           return $query->result();
            
        }
        
        
        public function recordsTotal($filtre,$listeColonne)
        {
            
            $sqlRa= $this->_get_query($filtre,"id_pli as id_click,".$listeColonne);
            $query = $this->ged->query($sqlRa);
            return $query->num_rows();
        }
        public function createSql($filtre,$listeColonne)
        {
            $strSql   = "1 = 1";
            $strValue = "";
            $str      = "";
            $strIdSociete  = "";
            $aWhere   = array();
            $strIdPli    = "";
            foreach ($filtre as $item) 
            {
                $filtre = trim($item["filter"]); 

                if(is_array($item["value"]))
                {
                    $strValue = "'";
                    $strValue .= implode("','", $item["value"])."'";
                }
                elseif($filtre != "date")
                {
                    $strValue = "'".trim($item["value"])."'";
                }
              
                /*if(trim($item["colonne"]) != 'cmc7' && trim($item["colonne"]) != 'montant' && trim($item["colonne"]) != 'nom_image' && trim($item["colonne"]) != 'paiement' )*/
                if(!in_array(trim($item["colonne"]),$this->whereException))
                {
                    if(trim($item["colonne"] == "societe"))
                    {
                        
                        $item["colonne"] = "f_pli.societe";
                    }

                    switch($filtre)
                    {
                        case 'date':
                        list($date1,$date2) = explode("-",$item["value"]);
                        $str = " and ".$item["colonne"]."::date between '".trim($date1)."' and '".trim($date2)."' ";
                        break;

                        case 'in':
                            $str = " and ".trim($item["colonne"])."::text in(".$strValue.") ";
                        break;

                        case 'not_in':
                            $str = " and ".trim($item["colonne"])."::text not in(".$strValue.") ";
                            break;
                        
                        case 'ilike':
                             $str = " and ".trim($item["colonne"])."::text ilike '%".str_replace("'","",$strValue)."%' ";
                            break;   
                    }
                    if(trim($item["colonne"] == "f_pli.societe"))
                    {
                        
                         $strIdSociete = $str;
                         $str = "";
                    }
                }
                else
                {
                    $colonne = trim($item["colonne"]);
                    
                    switch($colonne)
                    {
                        case 'cmc7':
                            $iDpli =  $this->getCmc7($item["value"],$filtre);
                            $strIdPli  .= $this->getIDpli($iDpli);
                            break;

                        case 'montant':
                            $iDpli =  $this->getMontant($item["value"],$filtre);
                            $strIdPli  .= $this->getIDpli($iDpli);
                            break;

                        case 'paiement':
                            $iDpli =  $this->getPaiement($filtre, $strValue);
                            $strIdPli  .= $this->getIDpli($iDpli);
                            break;
                        
                        case 'nom_image':
                            $iDpli =  $this->getDocumentIdPli($strValue,$filtre,$colonne);  
                            $strIdPli  .= $this->getIDpli($iDpli);
                            break;
                        case "id_pli";
                            $explodeIDpli = explode(",", str_replace("'","",$strValue));
                            $mapID = array_map('intval', $explodeIDpli); 
                            $strTmp = implode(",", $mapID);
                            $strIdPli  .= " and f_pli.id_pli in(".$strTmp.") ";
                            break;

                        default:
                            $iDpli = $this->getIDpliDansMouvement($strValue,$filtre,$colonne);
                            $strIdPli  .= $this->getIDpli($iDpli);


                    }
                    
                   
                }
                
                 $strSql .= $str;
                log_message_push('error', "recherche avancee courrier : ".$str);
                
            } /*fin boucle */
            /** */
            $aWhere["idPli"] = $strIdPli;
            $aWhere["generale"] = $strSql;
            $aWhere["societe"] = $strIdSociete;

            $aLeftJoin   = $this->trimArray($listeColonne);
            $oLeftJoin   = $this->createViewRaCourrierByColonne($aLeftJoin);

            $tabFinal    = "";
            $tab         = "";
            $dansTab     = "";
            $sLeftJoin   = "";
            $groupBy     = "";
            
            if($oLeftJoin)
            {
                $iTmpLeft = 0 ;
                foreach($oLeftJoin as $itemLeft)
                {
                    if(trim($itemLeft->tab_final) != "")
                    {
                        $tabFinal .= ", ".$itemLeft->tab_final;
                    }

                    if(trim($itemLeft->tab) != "")
                    {
                        $tab .= ", ".$itemLeft->tab;
                    }

                    if(trim($itemLeft->dans_tab) != "")
                    {
                        $dansTab .= ", ".$itemLeft->dans_tab;
                    }

                    if(trim($itemLeft->left_join) != "")
                    {
                        $sLeftJoin .= " ".$itemLeft->left_join;
                    }

                    if(trim($itemLeft->group_by) != "")
                    {
                        $groupBy .= ",  ".$itemLeft->group_by;
                    }
                }
            }

            $sqlView = $this->createView($tabFinal,$tab,$dansTab,$sLeftJoin,$groupBy,$aWhere);

           //  echo "<pre>".print_r("SELECT ".$listeColonne." FROM (".$sqlView.") AS php_view")."</pre>"; 
            
            /*
            ! ASIANA TEST OE MODE DEBUG DE RA MODE DEBUG DE LE VIEW "view_ra_courrier" NO ALAINA 
            */
           //echo "<pre>".print_r($sqlView)."</pre>";
            /*return $sqlRa = "SELECT  ".$listeColonne." FROM public.view_ra_courrier where ".$strSql;*/
             return "SELECT ".$listeColonne." FROM (".$sqlView.") AS php_view";

        }

        /**
         * ! SI ON MET A JOUR VIEW view_ra_courrier ON MET A JOUR AUSSI "createView"  OU L'INVERSE  (view_ra_courrier <=>createView())
         * TODO A ne pas oublier de faire le test sur les differents types de left_join
         * 
         ** ASIANA TEST OE MODE DEBUG DE RA MODE DEBUG DE LE VIEW "view_ra_courrier" NO ALAINA
         */
        public function createView($tabFinal,$tab,$dansTab,$sLeftJoin,$groupBy,$where)
        {
            /*
            $aWhere["idPli"] = $strIdPli;
            $aWhere["generale"] = $strSql;
             */
            $listeIDpli = "";
            $generale   = "";
            $societe    = "";

            if (array_key_exists("idPli",$where))
            {
                $listeIDpli = " ".$where["idPli"]." ";
            }

            if (array_key_exists("generale",$where))
            {
                $generale = " ".$where["generale"]." ";
            }

            if (array_key_exists("societe",$where))
            {
                $societe = " ".$where["societe"]." ";
            }

            $sql = "
            SELECT tab_final.id_pli,
            tab_final.pli,
            tab_final.societe,
            tab_final.nom_societe,
            tab_final.typologie_id,
            tab_final.typologie,
            tab_final.id_lot_numerisation,
            tab_final.id_lot_saisie,
            tab_final.lot_saisie_bis,
            tab_final.date_creation,
            tab_final.date_reception,
            tab_final.dt_event AS date_traitement,
            tab_final.commande,
            tab_final.lot_scan,
            
            tab_final.flag_traitement,
            
            tab_final.nom_deleg,
            
            tab_final.nb_mouvement,
            tab_final.titre,
            tab_final.code,
            
            tab_final.fin_colonne,
            concat(tab_final.code, ' - ', tab_final.titre) AS code_titre,
            to_char(tab_final.date_creation, 'DD/MM/YYYY'::text) AS courrier_date,
            to_char(tab_final.dt_event::timestamp with time zone, 'DD/MM/YYYY'::text) AS dt_traitement,
            tab_final.flag_batch,
            tab_final.saisie_batch,
            tab_final.etape,
            tab_final.etat_,
            tab_final.statut_,
            tab_final.date_ko,
            to_char(tab_final.date_ko::timestamp with time zone, 'DD/MM/YYYY'::text) AS ko_date
            ".$tabFinal."  
           FROM ( SELECT tab.id_pli,
                    tab.pli,
                    tab.societe,
                    tab.nom_societe,
                    tab.typologie_id,
                    tab.typologie,
                    tab.id_lot_numerisation,
                    tab.id_lot_saisie,
                    tab.lot_saisie_bis,
                    tab.date_creation,
                    tab.date_reception,
                    tab.dt_event,
                    tab.commande,
                    tab.lot_scan,
                    tab.flag_traitement,
                    
                    tab.nom_deleg,
                   
                    
                    tab.nb_mouvement,
                    tab.titre,
                    tab.code,
                    
                    
                    tab.flag_batch,
                    tab.saisie_batch,
                    tab.etape,
                    tab.etat_,
                    tab.statut_,
                    tab.date_dernier_statut_ko::date AS date_ko,
                    1 AS fin_colonne
                    ".$tab."  
                   FROM ( SELECT DISTINCT f_pli.id_pli,
                            f_pli.pli,
                            f_pli.societe,
                            societe.nom_societe,
                            f_lot_numerisation.lot_scan,
                            f_pli.typologie AS typologie_id,
                            CASE WHEN f_pli.typologie IS NULL OR f_pli.typologie = 0 THEN 'Pas de typologie'::character varying ELSE typologie.typologie END AS typologie,
                            f_pli.id_lot_numerisation,
                            f_pli.id_lot_saisie,
                            f_pli.lot_saisie_bis,
                            data_pli.flag_traitement,
                            f_pli.date_creation,
                            f_lot_numerisation.date_courrier::date AS date_reception,
                            f_pli.date_saisie::date AS dt_event,
                            f_lot_numerisation.commande,
                            
                            0 AS libelle,
                           
                            
                           
                            data_pli.nom_deleg,
                           data_pli.mvt_nbr AS nb_mouvement,
                            titres.titre,
                            titres.code,
                            
                         
                            COALESCE(data_pli.flag_batch) AS flag_batch,
                            CASE WHEN COALESCE(data_pli.flag_batch::integer, 0) = 0 THEN 'Non'::text ELSE CASE WHEN COALESCE(data_pli.flag_batch::integer, 0) = 1 THEN 'Oui'::text ELSE ''::text END END AS saisie_batch,
                                CASE
                                    WHEN COALESCE(data_pli.flag_traitement::integer, 0) = ANY (ARRAY[0, 1, 2]) THEN 'Typage'::text
                                    ELSE
                                    CASE
                                        WHEN COALESCE(data_pli.flag_traitement::integer, 0) = ANY (ARRAY[3, 4, 5, 6]) THEN 'Saisie'::text
                                        ELSE
                                        CASE
                                            WHEN COALESCE(data_pli.flag_traitement::integer, 0) = ANY (ARRAY[7, 8, 11, 13]) THEN 'Contrôle'::text
                                            ELSE
                                            CASE WHEN (COALESCE(data_pli.flag_traitement::integer, 0) = ANY (ARRAY[9, 14])) OR data_pli.statut_saisie = 8 THEN 'Terminé'::text ELSE NULL::text
                                            END
                                        END
                                    END
                                END AS etape,
                                CASE
                                    WHEN COALESCE(data_pli.flag_traitement::integer, 0) = 0 AND data_pli.statut_saisie = 1 OR (COALESCE(data_pli.flag_traitement::integer, 0) = ANY (ARRAY[3, 0])) AND data_pli.statut_saisie = 1 OR COALESCE(data_pli.flag_traitement::integer, 0) = 7 AND data_pli.statut_saisie = 1 OR COALESCE(data_pli.flag_traitement::integer, 0) in( 11,3) THEN 'A traiter'::text
                                    ELSE
                                    CASE
                                        WHEN COALESCE(data_pli.flag_traitement::integer, 0) = 1 AND data_pli.statut_saisie = 1 OR (COALESCE(data_pli.flag_traitement::integer, 0) = ANY (ARRAY[4, 8, 13])) THEN 'En cours'::text
                                        ELSE
                                        CASE
                                            WHEN COALESCE(data_pli.flag_traitement::integer, 0) = ANY (ARRAY[2, 5, 6,9]) THEN 'Traité'::text
                                            ELSE
                                            CASE WHEN COALESCE(data_pli.flag_traitement::integer, 0) = 14 AND data_pli.statut_saisie = 1 THEN 'Clôturé'::text ELSE ''::text
                                            END
                                        END
                                    END
                                END AS etat_,
                                CASE
                            WHEN f_flag_traitement.flag_rewrite_status_client <> 0 and data_pli.statut_saisie in(1) THEN ''::character varying
                            ELSE statut_saisie.libelle
                            END AS statut_,
                            data_pli.date_dernier_statut_ko,
                            1 AS fin_colonne
                            ".$dansTab."  
                           FROM f_pli
                             JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
                             LEFT JOIN f_lot_numerisation f_lot_numerisation ON f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
                             
                            
                             LEFT JOIN statut_saisie ON statut_saisie.id_statut_saisie = data_pli.statut_saisie
                               ".$sLeftJoin."  

                            
                             
                             LEFT JOIN typologie ON typologie.id = f_pli.typologie
                             LEFT JOIN societe ON societe.id = f_pli.societe
                             LEFT JOIN titres ON titres.id = data_pli.titre
                             LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = data_pli.flag_traitement
                              where 1= 1 ".$listeIDpli." ".$societe."
                          ORDER BY f_pli.id_pli) tab
                  GROUP BY tab.id_pli, tab.pli, tab.societe, tab.nom_societe, tab.typologie_id, tab.typologie, tab.id_lot_numerisation, tab.id_lot_saisie, tab.lot_saisie_bis, 
                  tab.date_creation, tab.date_reception, tab.dt_event, tab.commande, tab.lot_scan, tab.flag_traitement, tab.libelle,  tab.nom_deleg,  
                 tab.nb_mouvement, tab.titre, tab.code,  tab.flag_batch, tab.saisie_batch, tab.etape, tab.etat_, 
                  tab.statut_, tab.fin_colonne, tab.date_dernier_statut_ko ".$groupBy.") tab_final  where ".$generale;
            
            return $sql;
        }
        public function createViewRaCourrierByColonne($arrayLeft)
        {
                $this->ged->select('distinct l.*',FALSE);
                $this->ged->from('ra_colonne c');
                $this->ged->join('ra_left_join l','l.id = c.flag_left');
                $this->ged->where('l.actif',1);
                $this->ged->where_in('c.colonne_db',$arrayLeft);

                $q = $this->ged->get();

                if($q->num_rows() != 0)
                {
                    return $q->result();
                }
                return false;
        }
        
        
        

        public function getCmc7($cmc7,$filtre)
        {
            switch($filtre)
                {
                    
                    case 'in':
                        $str = " select id_pli from view_all_cmc7 where cmc7 in('".$cmc7."')";
                    break;

                    case 'not_in':
                     $str = " select id_pli from view_all_cmc7 where cmc7 not in('".$cmc7."')";
                        break;
                    
                    case 'ilike':
                    $str = " select id_pli from view_all_cmc7 where cmc7 ilike '%".$cmc7."%'";
                        break;   
                }
                $query = $this->ged->query($str); 
                return $query->result();  
        }

        public function getPaiement($filtre,$strValue)
        {
            switch($filtre)
                {
                    
                    case 'in':
                        $str = " select id_pli from paiement where id_mode_paiement  in(".$strValue.") ";
                    break;

                    case 'not_in':
                      $str = " select id_pli from paiement where id_mode_paiement not in(".$strValue.") ";
                        break;
                    
                   
                }
                $query = $this->ged->query($str); 
                return $query->result();  
        }

        public function getMontant($strValue,$filtre)
        {
            switch($filtre)
            {
                
                case 'in':
                    $str = " select distinct id_pli from view_all_montant where montant  in(".str_replace("'","",$strValue).") ";
                break;

                case 'not_in':
                 $str = " select distinct id_pli from view_all_montant where montant not in(".str_replace("'","",$strValue).") ";
                    break;
                default:
                     $str = " select distinct id_pli from view_all_montant where montant ".$filtre." ".str_replace("'","",$strValue)." ";
            }

            $query = $this->ged->query($str); 
            return $query->result();  
        }
        
        public function exportExcel($filtre,$listeColonne)
        {

            $sql = $this->createSql($filtre,$listeColonne);
            $query = $this->ged->query($sql,FALSE);

            log_message_push('error', "recherche avancee courrier : export excel");

            return $query->result();
        }

        public function getDocumentIdPli($strValue,$filtre,$colonne)
        {
            /*SELECT id_document, id_pli, n_ima_recto, n_ima_verso, desarchive  FROM public.f_document_ra
            where n_ima_recto = '20190521 0372 0001 (26).jpg' or n_ima_verso = '20190521 0372 0001 (26).jpg' limit 500
            20190826 0372 0001 (20).jpg
            */
            
            $sql = "";

            
           
            if($filtre == 'in')
            {
               $sql = "SELECT  distinct  id_pli FROM public.f_document_ra
                            where n_ima_recto in (".$strValue.") or n_ima_verso in (".$strValue.") ";
            }
            elseif ($filtre == 'ilike')
            {
                $sql = "SELECT  distinct  id_pli FROM public.f_document_ra
                            where n_ima_recto ilike '%".str_replace("'","",$strValue)."%' 
                      or n_ima_verso ilike '%".str_replace("'","",$strValue)."%'";
            }
            else
            {

               $sql = "SELECT  distinct  id_pli FROM public.f_document_ra
               where n_ima_recto not in (".$strValue.") or n_ima_verso not in (".$strValue.") ";
            }

            $query = $this->ged->query($sql);
            return $query->result();
            
        }
        public function getIDpli($objIDpli)
        {
            $str_ = "0";
            if($objIDpli)
            {
                $strID = "0";
                foreach($objIDpli as $itemIDpli)
                {
                    $strID .=",".$itemIDpli->id_pli;
                }
                $str_ = " and f_pli.id_pli in(".$strID.")";
            }
            return $str_;
        }

        public function getIDpliDansMouvement($strValue,$filtre,$colonne)
        {
            $colonne_ = trim($colonne).'::text';
            $sql = "";
            switch($filtre)
            {
                case 'in':
                    $sql = "SELECT distinct id_pli FROM public.mouvement where ".$colonne_." in (".$strValue.")";
                    break;

                case 'not_in':
                    $sql = "SELECT distinct id_pli FROM public.mouvement where ".$colonne_." not in (".$strValue.")";
                    break;

                default:
                    $sql = "SELECT distinct id_pli FROM public.mouvement where ".$colonne_." in (".$strValue.")";


            }

            $query = $this->ged->query($sql);
            return $query->result();
        }


        public function trimArray($inpuString)
        {
            $out = array();
            $in  = explode(",",$inpuString);
            for($i = 0 ; $i < count($in);$i++)
                {
                    if(trim($in[$i]) != "")
                    {
                        $out []=trim($in[$i]); 
                    }
                }
            return $out;
        }

    
        
    }