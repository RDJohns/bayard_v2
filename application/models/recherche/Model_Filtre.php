<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Model_Filtre extends CI_Model
    {
        var $table           = 'view_liste_pli_circulaire';
        var $ra              = 'view_ra_courrier';
        var $mvt             = 'view_titre_pli';
        var $flux            = 'view_ra_flux';
        var $tbFlux          = "flux";
        var $abonnement      = "view_abonnement";

        private $CI;
        private $ged;
        private $base_64;
        private $ged_push;
        
        private $filtre = "view_ra_filtre";
        function __construct()
        {
            parent::__construct();
            $this->CI       = &get_instance();
            $this->ged      = $this->load->database('ged', TRUE);
            $this->base_64  = $this->load->database('base_64', TRUE);
            $this->ged_push = $this->load->database('ged_push', TRUE);
        }

        public function getFiltre()
        {
            return $this->ged->select('*')->from($this->filtre)->get()->result(); 
        }

       public function infoGle($idClic)
       {
            return $this->ged
            ->select('*')
            ->from($this->ra)
            ->where('id_pli',(int)$idClic)
            ->get()->result();
       }
       public function infoMouvement($idClic)
       {
            return $this->ged
            ->select('*')
            ->from($this->mvt)
            ->where('id_pli',(int)$idClic)
            ->get()->result();
       }

       public function getPaiement($idClic)
       {
           $sql = "SELECT string_agg(modes_paiements,',') liste_paie
           FROM public.view_mode_paiement where id_pli =".(int)$idClic;
        
            $query = $this->ged->query($sql);
            return $query->result();
       }
      
       public function getPaiementChq($idClic)
       {
           $sql = "SELECT paiement_cheque.cmc7, paiement_cheque.rlmc,paiement_cheque.montant, anomalie_cheque.anomalie
                    FROM public.paiement_cheque 
                        left join anomalie_cheque on anomalie_cheque.id = paiement_cheque.anomalie_cheque_id where paiement_cheque.id_pli = ".(int)$idClic;

               $query = $this->ged->query($sql);
               return $query->result();           
       }

       public function getChequekdo($idClic)
       {
           $sql = "SELECT montant, type_cheque_cadeau,id_doc
                    FROM public.paiement_cheque_cadeau
                    left join cheque_cadeau on cheque_cadeau.id = paiement_cheque_cadeau.type_chq_kdo
                        where id_pli =".(int)$idClic;

               $query = $this->ged->query($sql);
               return $query->result();           
       }

       public function infoGleFlux($idClic)
       {
            return $this->ged_push
            ->select('*')
            ->from($this->flux)
            ->where('id_flux',(int)$idClic)
            ->get()->result();
       }
       public function getFile($fluxID) 
        {
            return $this->ged_push 
            ->select('*')
            ->from($this->tbFlux)
            ->where('id_flux',(int)$fluxID)
            ->get()->result();
        }

        public function getAbonnement($idClic)
       {
            return $this->ged_push
            ->select('*')
            ->from($this->abonnement)
            ->where('id_flux',(int)$idClic)
            ->get()->result();
       }

       
    }