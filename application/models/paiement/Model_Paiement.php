<?php
/**
 * Created by PhpStorm.
 * User: 9420
 * Date: 12/03/2019
 * Time: 14:34
 */

class Model_Paiement extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;

    private $tbModePaiement = "mode_paiement";

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function modePaiementListe()
    {
        return $this->ged
            ->select('id_mode_paiement,mode_paiement,par_defaut')
            ->from($this->tbModePaiement)
            ->where('actif',1)
            ->order_by("mode_paiement asc")
            ->get()->result();
    }

}