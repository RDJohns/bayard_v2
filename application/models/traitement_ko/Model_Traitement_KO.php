<?php

class Model_Traitement_KO extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;
    private $ged_push;

   private $statutSaisie        = "saisie_statut";
   private $listePliKo          = "ged_traitement_ko";
   private $paveKO             = "pave_traitement_ko_courrier";
   private $paveKO_MS             = "pave_traitement_ko_mail_sftp";
   private $tbPli               = "pli";
   private $tbDataPli           = "data_pli";
   private $gedSaisieEtat       = "etat_saisie_ged";
   private $flux                = "flux";
   private $fluxComment         = "comments_pli" ;
   private $FfluxComment         = "flux_comment" ;
   private $typologie         = "typologie_ttmt_ko" ;

   private $tb_user = TB_user;
   
    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);

        $this->ged_push = $this->load->database('ged_push', TRUE);
    }

    public function getEtatPli()
    {
        return $this->ged
        ->select('*')
        ->from($this->statutSaisie)
        ->get()->result();
    }

    public function listePliKo($aListeStatutSaisie,$listeEtatPli,$filtreEtape,$societe,$source,$daty,$typologie)
    {
       $this->ged->select('*');
      
        list($daty1, $daty2) = explode("-",$daty);
       
        $this->ged->where("date_courrier BETWEEN '". date('Y-m-d', strtotime($daty1))."' and '". date('Y-m-d', strtotime($daty2))."'",'',false);

        if(count($listeEtatPli)> 0 && is_array($listeEtatPli))
       {
         $this->ged->where_in("statut_saisie",$listeEtatPli);
       } 

       if(count($societe)> 0 && is_array($societe))
       {
         $this->ged->where_in("societe",$societe);
       }

       if(count($filtreEtape)> 0 && is_array($filtreEtape))
       {
         $this->ged->where_in("etat_ttmt",$filtreEtape);
       }

       $this->ged->where_in("statut_saisie",$aListeStatutSaisie); 
       $this->ged->where("source_traitement",$source);
       
       if(count($typologie)> 0 && is_array($typologie))
       {
         $this->ged->where_in("typologie_id",$typologie);
       }

       $this->ged->order_by('id_ged', 'ASC');

        $query = $this->ged->get($this->listePliKo);
        return $query->result();
    }
    public function getMotifConsigne($idPli)
    {
        $this->ged->select('*,id as id_motif');
        $this->ged->where_in("id_pli",$idPli);
        $query = $this->ged->get("motif_consigne_dernier");
        return $query->result();
    }

    public function getMotifConsigneFlux($idFlux)
    {
        $this->ged->select('motif_operateur_flux as motif_operateur,id_motif_consigne_flux as id_motif');
        $this->ged->where_in("id_flux",$idFlux);
        $query = $this->ged->get("motif_consigne_flux_dernier");
        return $query->result();
    }

    public function setEtapli($aPli,$aDataPli, $listePli,$pliNotIn,$idMotif,$motifConsigne=null)
    {
        $this->ged->trans_begin();
        $this->base_64->trans_begin();

        $this->base_64->where_in('id_pli', $listePli);
        $this->base_64->update($this->tbPli, $aPli);

        $this->ged->where_in('id_pli', $listePli);
        $this->ged->where_not_in('id_pli', $pliNotIn);
        $this->ged->update($this->tbDataPli, $aDataPli);

        if($motifConsigne != null && is_array($motifConsigne))
        {
            $this->ged->where_in('id_pli', $listePli);
            $this->ged->where('id', $idMotif);
            $this->ged->where_not_in('id_pli', $pliNotIn);
            $this->ged->update("motif_consigne", $motifConsigne);
        }
        

        if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE)
        {
            $this->base_64->trans_rollback();
            $this->ged->trans_rollback();
            return array("update"=>0);
        }
        else 
        {
            $this->base_64->trans_commit();
            $this->ged->trans_commit();
            
            /* $this->CI->histo->plis($listePli); */
            return array("update"=>1);
        }
    }
    public function setEtatFlux($listeFlux,$data,$pliNotIn,$idMotif,$motifConsigne=null)
    {
        $this->ged_push->trans_begin();
        $this->ged_push->where_in('id_flux', $listeFlux);
        if($pliNotIn)
        {
            $this->ged_push->where_not_in('id_flux', $pliNotIn);
        }
        
        $this->ged_push->update($this->flux, $data);

        if($motifConsigne != null && is_array($motifConsigne))
        {
            $this->ged_push->where_in('id_flux', $listeFlux);
            $this->ged_push->where('id_motif_consigne_flux', $idMotif);
            $motifConsigne["dt_consigne"] = date('Y-m-d H:i:s');
            if($pliNotIn)
            {
                $this->ged_push->where_not_in('id_flux', $pliNotIn);
            }
            $this->ged_push->update("motif_consigne_flux", $motifConsigne);
        }

        if($this->ged_push->trans_status() === FALSE )
        {
            $this->ged_push->trans_rollback();
            return array("update"=>0);
        }
        else 
        {
            $this->ged_push->trans_commit();
            /* $this->CI->histo->flux_tab($listeFlux); */
            return array("update"=>1);
        }



    }

    public function etatSaisieGedBySource($source)
    {
        return $this->ged_push
            ->select('*')
            ->from($this->gedSaisieEtat)
            ->where("source",$source)
            ->get()->result();
    }

    public function verificationAvantValidation($listePi,$source,$intEtape ,$statut)
    {
       $this->ged->select('id_ged');
       $this->ged->where_in("id_ged",$listePi);
       $this->ged->where("statut_saisie",$statut);
       $this->ged->where("etat_ttmt",$intEtape);
       $this->ged->where("source_traitement",$source);

       $query = $this->ged->get($this->listePliKo);
       $array = $query->result_array();
       $arr   = array_map (function($value){
        return $value['id_ged'];
        } , $array);

       return $arr;
    }

    // TODO comments FLUX
    public function comments($id_pli, $new_comment=NULL)
	{
		if(!is_numeric($id_pli)){
			return array();
		}
		if(is_array($new_comment) && count($new_comment) > 0){
            $this->ged_push->trans_begin();
            $this->ged_push->insert($this->fluxComment, $new_comment);
            if ($this->ged_push->trans_status() === FALSE)
            {
                $this->ged_push->trans_rollback();
            }
            else
            {
                $this->ged_push->trans_commit();
                $this->CI->histo->action(127, '', $id_pli);
            }
        }
		return $this->ged
			->from($this->FfluxComment.' tbcom')
			->join($this->tb_user.' tbuser', 'tbcom.commented_by = tbuser.id_utilisateur', 'INNER')
			->where('id_pli_comment', $id_pli)
			->order_by('dt_comment', 'DESC')
			->get()->result();
    }
    //paveCourrier              ($daty,$listeEtatPli,$societe,$filtreEtape);
    public function paveCourrier($daty,$dataStatutSaisie,$dataSociete,$filtreEtape,$listeEtatPli,$typologie)
    {
        list($daty1, $daty2) = explode("-",$daty);

      //  $this->ged->select('SUM(pli_ko_scan) pli_ko_scan, SUM(pli_ko_definitif)pli_ko_definitif, SUM(pli_ko_iconnu)pli_ko_iconnu, SUM(pli_ko_src)pli_ko_src, SUM(pli_ko_ko_en_attente)pli_ko_ko_en_attente, SUM(pli_ko_circulaire)pli_ko_circulaire, SUM(pli_ci_editee)pli_ci_editee, SUM(pli_ko_ci_envoyee)pli_ko_ci_envoyee, SUM(pli_ok_ci)pli_ok_ci, SUM(pli_ko_en_cours_by)pli_ko_en_cours_by',FALSE);
        

      
      /* $query = $this->ged->get($this->paveKO);*/
      $where = " and date_courrier::date BETWEEN '".date('Y-m-d', strtotime($daty1))."' and '". date('Y-m-d', strtotime($daty2))."' ";
      if(count($dataSociete)> 0 && is_array($dataSociete))
       {
          $strSociete = implode(",",$dataSociete);
          $where .= "and societe in(".$strSociete.")";
       }
       
       if(count($listeEtatPli)> 0 && is_array($listeEtatPli))
       {
          $strlistePl = implode(",",$listeEtatPli);
          $where .= " and ged_traitement_ko.statut_saisie in(".$strlistePl.") ";
       }
       if(count($dataStatutSaisie)> 0 && is_array($dataStatutSaisie))
       {
          $strlistePl = implode(",",$dataStatutSaisie);
          $where .= " and ged_traitement_ko.statut_saisie in(".$strlistePl.") ";
       }  

       if(count($typologie)> 0 && is_array($typologie))
       {
         

         $typo = implode(",",$typologie);
         $where .= " and ged_traitement_ko.typologie_id in(".$typo.") ";
       }

       if(count($filtreEtape)> 0 && is_array($filtreEtape))
       {
         

         $etaP = implode(",",$filtreEtape);
         $where .= " and ged_traitement_ko.etat_ttmt in(".$etaP.") ";
       }

        $sql = "select 
       SUM(pli_ko_scan) pli_ko_scan, SUM(pli_ko_definitif)pli_ko_definitif, SUM(pli_ko_iconnu)pli_ko_iconnu, SUM(pli_ko_src)pli_ko_src, SUM(pli_ko_ko_en_attente)pli_ko_ko_en_attente, SUM(pli_ko_circulaire)pli_ko_circulaire, SUM(pli_ci_editee)pli_ci_editee, SUM(pli_ko_ci_envoyee)pli_ko_ci_envoyee, SUM(pli_ok_ci)pli_ok_ci, SUM(pli_ko_en_cours_by)pli_ko_en_cours_by
       from(
       SELECT
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 2 THEN 1
                   ELSE 0
               END AS pli_ko_scan,
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 3 THEN 1
                   ELSE 0
               END AS pli_ko_definitif,
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 4 THEN 1
                   ELSE 0
               END AS pli_ko_iconnu,
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 5 THEN 1
                   ELSE 0
               END AS pli_ko_src,
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 6 THEN 1
                   ELSE 0
               END AS pli_ko_ko_en_attente,
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 7 THEN 1
                   ELSE 0
               END AS pli_ko_circulaire,
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 9 THEN 1
                   ELSE 0
               END AS pli_ci_editee,
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 10 THEN 1
                   ELSE 0
               END AS pli_ko_ci_envoyee,
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 11 THEN 1
                   ELSE 0
               END AS pli_ok_ci,
               CASE
                   WHEN COALESCE(ged_traitement_ko.statut_saisie, 0) = 12 THEN 1
                   ELSE 0
               END AS pli_ko_en_cours_by,
           ged_traitement_ko.date_courrier,
           ged_traitement_ko.societe,
           COALESCE(ged_traitement_ko.statut_saisie, 0) AS statut_saisie,
           ged_traitement_ko.etat_ttmt,
           ged_traitement_ko.typologie_id
          FROM ged_traitement_ko
         WHERE  ged_traitement_ko.source = 'Courrier'::text ".$where.") as t";
      


      if(count($dataSociete)> 0 && is_array($dataSociete))
       {
         $this->ged->where_in("societe",$dataSociete);
       }

       if(count($listeEtatPli)> 0 && is_array($listeEtatPli))
       {
         $this->ged->where_in("statut_saisie",$listeEtatPli);
       } 

       if(count($dataStatutSaisie)> 0 && is_array($dataStatutSaisie))
       {
         $this->ged->where_in("statut_saisie",$dataStatutSaisie);
       }

       if(count($typologie)> 0 && is_array($typologie))
       {
         $this->ged->where_in("typologie_id",$typologie);
       }

       if(count($filtreEtape)> 0 && is_array($filtreEtape))
       {
         $this->ged->where_in("etat_ttmt",$filtreEtape);
       }
         $query = $this->ged->query($sql);
         return $query->result();
       
    }

    public function paveMailSftp($daty,$dataStatutSaisie,$dataSociete,$source,$filtreEtape,$listeEtatPli,$typologie)
    {
        list($daty1, $daty2) = explode("-",$daty);

        $this->ged->select('SUM(ko_mail_anomalie) ko_mail_anomalie,SUM(ko_anomalie_pj) ko_anomalie_pj,SUM(hors_perim) hors_perim,SUM(ko_anomalie_fichier) ko_anomalie_fichier,SUM(rejeter) rejeter,SUM(ko_transfert_src) ko_transfert_src,SUM(ko_definitif) ko_definitif,SUM(ko_inconnu) ko_inconnu,SUM(ko_en_attente) ko_en_attente,SUM(ko_en_cours_bayard) ko_en_cours_bayard',FALSE);
        $this->ged->where("date_courrier BETWEEN '". date('Y-m-d', strtotime($daty1))."' and '". date('Y-m-d', strtotime($daty2))."'",'',false);

       if(count($dataSociete)> 0 && is_array($dataSociete))
       {
         $this->ged->where_in("societe",$dataSociete);
       }

       if(count($filtreEtape)> 0 && is_array($filtreEtape))
       {
         $this->ged->where_in("etat_ttmt",$filtreEtape);
       }

       if(count($listeEtatPli)> 0 && is_array($listeEtatPli))
       {
         $this->ged->where_in("statut_saisie",$listeEtatPli);
       }
        
       if(count($typologie)> 0 && is_array($typologie))
       {
         $this->ged->where_in("typologie_id",$typologie);
       }
        
       
       $this->ged->where("source_traitement",$source);

       $query = $this->ged->get($this->paveKO_MS);
       return $query->result();
    }

    public function listeTypologieBySource($source)
    {
        $this->ged_push->select('*');
        $this->ged_push->where("etat_ttmt",$source);
        $query = $this->ged_push->get($this->typologie);
        return $query->result();
    } 
    public function marquerLu($data)
    {
      $this->ged->insert("consigne_lue",$data);     
    }

}