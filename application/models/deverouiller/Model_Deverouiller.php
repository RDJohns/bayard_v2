<?php

class Model_Deverouiller extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;

    var $columnOrder       = array("id_pli","traitement","typage_par","saisie_par","controle_par",null);

    var $columnSearch      = array("id_pli","traitement","typage_par","saisie_par","controle_par");
    var $order             = array("id" => 'desc');
    var $listePli          = 'view_pli_a_deverouiller';
    var $pli               = 'pli';

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->base_64 = $this->load->database('ged', TRUE);
       // $this->base_64 = $this->load->database('base_64', TRUE);
    }

    private function _filtre_query()
    {
        if((int)strlen(trim($_POST['dtIdPli'])) > 0)
        {
            $this->base_64->where("id_pli",$_POST['dtIdPli']);
        }
        if((int)strlen(trim($_POST['dtTypage'])) > 0)
        {
            $this->base_64->where("typage_par",$_POST['dtTypage']);
        }
        if((int)strlen(trim($_POST['dtSaisie'])) > 0)
        {
            $this->base_64->where("saisie_par",$_POST['dtSaisie']);
        }
        if((int)strlen(trim($_POST['dtCtrl'])) > 0)
        {
            $this->base_64->where("controle_par",$_POST['dtCtrl']);
        }
    }
    private function _get_query()
    {
        $this->base_64->from($this->listePli);
        $this->_filtre_query();
        $i = 0;
        foreach ($this->columnSearch as $emp)
        {
            if(isset($_POST['search']['value']) && !empty($_POST['search']['value'])){
                $_POST['search']['value'] = $_POST['search']['value'];
            } else
                $_POST['search']['value'] = '';
            if($_POST['search']['value'])
            {
                if($i===0) // first loop
                {
                    $this->base_64->group_start();
                    $this->base_64->like($emp, $_POST['search']['value']);
                }
                else
                {
                    $this->base_64->or_like($emp, $_POST['search']['value']);
                }

                if(count($this->columnSearch) - 1 == $i) //last loop
                    $this->base_64->group_end();
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->base_64->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->base_64->order_by(key($order), $order[key($order)]);
        }
    }
    public function getListePli()
    {
        $this->_get_query();
        if(isset($_POST['length']) && $_POST['length'] < 1)
        {
            $_POST['length']= '10';
        }
        else
            $_POST['length']= $_POST['length'];

        if(isset($_POST['start']) && $_POST['start'] > 1)
        {
            $_POST['start']= $_POST['start'];
        }
        $this->base_64->limit($_POST['length'], $_POST['start']);
        $query = $this->base_64->get();

        return $query->result();
    }

  public function recordsTotal()
  {
      $this->_filtre_query();
      $this->base_64->from($this->listePli);
      return $this->base_64->count_all_results();
  }

  public function recordsFiltered()
  {
      $this->_get_query();
      $query = $this->base_64->get();
      return $query->num_rows();
  }

  public function deverouillerPli($idPli,$array)
  {
      $this->base_64->trans_start();
      $this->base_64->update($this->pli, $array, "id_pli =".(int)$idPli);

      if ($this->base_64->trans_status() === FALSE)
      {
          $this->base_64->trans_rollback();
          return 0;
      }
      else
      {
          $this->base_64->trans_commit();
          return 1;
      }
  }

}