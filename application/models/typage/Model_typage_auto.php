<?php

class Model_typage_auto extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;

    private $tbTypologie            = "typologie";
    private $tbAnomalieCheque       = "anomalie_cheque";
    private $tbTitre                = "titres";
    private $societe                = "societe";
    private $tbPli                  = "pli";
    private $viewPliNonType         = "view_pli_non_type";
    private $document               = "document";
    private $motifKoScan            = "motif_ko_scan";
    private $paiement               = "paiement";
    private $mouvement              = "mouvement";
    private $paiementCheque         = "paiement_cheque";
    private $dataPli                = "data_pli";
    private $histoPli               = "histo_pli";
    private $preDataCheque          = "view_pre_data_cheque";
    private $provenance             = "provenance";
    private $predata                = "predata";
    private $viewPli                = "view_pli";
    private $f_compoPli             = "f_compo_pli_auto";
    public $existeDataPli           ="OK";

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);

        //$this->load->library('portier');
        //$this->portier->must_op();
    }

    public function get_pli_to_flag(){
        // récuperation des plis respectant les conditions pour entrer en typage auto
        $sql = "SELECT id_pli, type_document,typologie_auto FROM 
                (SELECT pli.id_pli, COUNT(id_document) as nb_document, string_agg(id_type_document::character varying, '-' order by id_type_document)::character varying as type_document FROM ".$this->tbPli." pli 
                INNER JOIN ".$this->document." ON document.id_pli = pli.id_pli  
                WHERE document.id_type_document IN (1,2,3,4,7,8,9,10) 
                AND flag_traitement = 0   
                GROUP BY pli.id_pli) as compte_pli 
                INNER JOIN ".$this->f_compoPli." compo ON compo.id_compo_pli = compte_pli.type_document 
                ORDER BY id_pli ASC  
              ";

        return $this->base_64->query($sql)->result();
    }

    public function get_pli_to_manuel(){
        // récuperation des plis partant en typage manuel
        $sql = "SELECT id_pli, type_document,typologie_auto FROM 
                (SELECT pli.id_pli, COUNT(id_document) as nb_document, string_agg(id_type_document::character varying, '-' order by id_type_document)::character varying as type_document FROM ".$this->tbPli." pli 
                INNER JOIN ".$this->document." ON document.id_pli = pli.id_pli  
                WHERE /*document.id_type_document IN (1,2,3,4,7,8,9,10) 
                AND*/ flag_traitement = 0   
                GROUP BY pli.id_pli) as compte_pli 
                LEFT JOIN ".$this->f_compoPli." compo ON compo.id_compo_pli = compte_pli.type_document 
                WHERE typologie_auto IS NULL 
                ORDER BY id_pli ASC  
              ";

        return $this->base_64->query($sql)->result();
    }

    public function update_flag_batch(){
        $this->base_64->trans_begin();

        // plis en typage auto
        $plis = $this->get_pli_to_flag();
        if($plis){
            foreach($plis as $pli){
                $data_upd = array(
                    'flag_batch' => 0,
                    'flag_typage_auto' => 1,
                    'typologie' => $pli->typologie_auto,
                );
                $this->ged->where('id_pli',$pli->id_pli)
                            ->update($this->dataPli, $data_upd);

                $data_upd_pli = array(
                    'typologie' => $pli->typologie_auto
                );
                $this->base_64->where('id_pli',$pli->id_pli)
                            ->update($this->tbPli, $data_upd_pli);
            }

        }

        // plis en typage manuel
        $plis_man = $this->get_pli_to_manuel();
        if($plis_man){
            foreach($plis_man as $pli){
                $data_upd = array(
                    'flag_batch' => 0 
                );
                $this->ged->where('id_pli',$pli->id_pli)
                            ->update($this->dataPli, $data_upd);

            }

        }

        if($this->base_64->trans_status() === FALSE){
            $this->base_64->trans_rollback();
            return 'Mise à jour échoué';
        }
        else{
            $this->base_64->trans_commit();
            return '';
        }

    }

    public function get_pli_for_typage(){
        $plis = $this->ged->select($this->viewPli.'.id_pli')
                            ->from($this->viewPli)
                            ->join($this->dataPli, $this->dataPli.'.id_pli = '.$this->viewPli.'.id_pli', 'left')
                            ->where($this->viewPli.'.flag_traitement', 0)
                            ->where('flag_batch', 1)
                            ->get()
                            ->result();

        if($plis){
            foreach($plis as $pli){
                $this->typage_per_pli($pli->id_pli);
                
                // insertion mode de paiement = chèque 
                $data_paiement = array(
                    'id_pli' => $pli->id_pli,
                    'id_mode_paiement' => 1 
                );
                $this->ged->insert($this->paiement, $data_paiement);

                /** mise à jour flag_traitement = 2 */
                // data_pli 
                $data_pli_upd = array(
                    'flag_traitement' => 2,
                    'statut_saisie' => 1
                );
                $this->ged->where('id_pli', $pli->id_pli)
                            ->update($this->dataPli, $data_pli_upd);
                
                // pli 
                $pli_upd = array(
                    'flag_traitement' => 2,
                    'date_typage' => date('Y-m-d H:i:s'),
                    'statut_saisie' => 1 
                );
                $this->base_64->where('id_pli', $pli->id_pli)
                            ->update($this->tbPli, $pli_upd);
                
                /** fin mise à jour flag_traitement */

                // historisation du pli 
                $this->CI->histo->pli($pli->id_pli);
            }
        }
    }

    public function typage_per_pli($id_pli){
        $docs = $this->base_64->select($this->predata.'.*')
                                ->from($this->document)
                                ->join($this->predata, $this->predata.'.id_document = '.$this->document.'.id_document','left')
                                ->where('id_pli', $id_pli)
                                ->get()
                                ->result();

        if($docs){
            foreach($docs as $doc){
                if($doc->id_type_document == 2){
                    // traitement du chèque
                    $data = array('cmc7' => $doc->cmc7,
                                    'montant' => $doc->montant,
                                    'id_pli' => $id_pli);
                    
                    $this->clear_pli_data($id_pli, $this->paiementCheque);

                    $this->ged->insert($this->paiementCheque, $data);
                }

                if(($doc->id_type_document != 2) && ($doc->id_type_document != 1)){
                    // traitement des mouvements 
                    $mouvement = $this->check_mouvement($doc);
                    if($mouvement === true){
                        $data = array('id_pli' => $id_pli,
                                    'numero_abonne' => $doc->abonne_destinataire,
                                    'numero_payeur' => $doc->abonne_payeur,
                                    'code_promo' => $doc->code_promo);
                        
                        $this->ged->insert($this->mouvement, $data);
                    }
                }
            }
        }
        echo 'traitement du pli :'.$id_pli.' effectué<br>';
    }

    public function check_mouvement($doc){
        if(($doc->code_promo != '') || 
        ($doc->abonne_payeur != '') ||
        ($doc->abonne_destinataire != '')) return true;
        else return false;
    }

    public function clear_pli_data($id_pli,$table){
        //effacer les données du pli dans les tables
        $this->ged->where('id_pli', $id_pli)
                    ->delete($table);
    }

}

