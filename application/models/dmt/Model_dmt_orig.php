<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dmt extends CI_Model{

    private $CI;

    private $ged;
    private $base_64;
	
	protected $select_ano        		= array('login_typage','id_pli','nom_societe','dt_event','nb_mvt','typologie','mode_paiement','lot_scan','libelle','date_typage','debut_fin_typage','duree_typage','date_deb_min','date_fin_max');
	
    protected $column_order_ano  		= array('login_typage','id_pli','nom_societe','dt_event','nb_mvt','typologie','mode_paiement','lot_scan','libelle','date_typage','debut_fin_typage','duree_typage');
    protected $column_search_ano 		= array('dt_event::text','id_pli::text','mode_paiement::text','nb_mvt::text','typologie::text','libelle::text','date_typage::text','lot_scan::text','nom_societe::text','duree_typage::text','login_typage::text');
    protected $order_ano         		= array('login_typage'=> 'asc','id_pli'=> 'asc','dt_event'=> 'asc');
	
	protected $select_saisie_dmt  		= array('login_saisie','id_pli','nom_societe','dt_event','nb_mvt','typologie','mode_paiement','lot_scan','libelle','date_saisie','debut_fin_saisie','duree_saisie','date_deb_min','date_fin_max');
	
    protected $column_order_saisie_dmt  = array('login_saisie','id_pli','nom_societe','dt_event','nb_mvt','typologie','mode_paiement','lot_scan','libelle','date_saisie','debut_fin_saisie','duree_saisie');
    protected $column_search_saisie_dmt = array('dt_event::text','id_pli::text','mode_paiement::text','nb_mvt::text','typologie::text','libelle::text','date_saisie::text','lot_scan::text','nom_societe::text','duree_saisie::text','login_saisie::text');
    protected $order_saisie_dmt         = array('login_saisie'=> 'asc','id_pli'=> 'asc','dt_event'=> 'asc');
	
	protected $select_ctrl_dmt  		= array('login_controle','id_pli','nom_societe','dt_event','nb_mvt','typologie','mode_paiement','lot_scan','libelle','date_controle','debut_fin_controle','duree_controle','date_deb_min','date_fin_max');
	
    protected $column_order_ctrl_dmt  	= array('login_controle','id_pli','nom_societe','dt_event','nb_mvt','typologie','mode_paiement','lot_scan','libelle','date_controle','debut_fin_controle','duree_controle');
    protected $column_search_ctrl_dmt 	= array('dt_event::text','id_pli::text','mode_paiement::text','nb_mvt::text','typologie::text','libelle::text','date_controle::text','lot_scan::text','nom_societe::text','duree_controle::text','login_controle::text');
    protected $order_ctrl_dmt         	= array('login_controle'=> 'asc','id_pli'=> 'asc','dt_event'=> 'asc');
	
							
	protected $select_global        		= array('dt_event','id_pli','nom_societe','nb_mvt','typologie','mode_paiement','lot_scan','libelle','duree_typage','duree_saisie','duree_controle');
	
    protected $column_order_global  		= array('dt_event','id_pli','nom_societe','nb_mvt','typologie','mode_paiement','lot_scan','libelle','duree_typage','duree_saisie','duree_controle');
    protected $column_search_global 		= array('dt_event::text','id_pli::text','mode_paiement::text','nb_mvt::text','typologie::text','libelle::text','duree_typage::text','duree_saisie::text','duree_controle::text');
    protected $order_global         		= array('dt_event'=> 'asc','id_pli'=> 'asc','nom_societe'=> 'asc');
	
	
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }
	
	/*public function get_statut_traitement(){
        return $res_type_doc = $this->base_64->select('*')
            ->from('flag_traitement')
            ->where('actif_extract = ', '1')
            ->order_by('comment asc')
            ->get()
            ->result();
    }
	public function get_statut_saisie(){
        return $res_type_doc = $this->ged->select('*')
            ->from('statut_saisie')
            ->where('actif = ', '1')
            ->order_by('libelle asc')
            ->get()
            ->result();
    }*/
	
	 public function get_statut_traitement(){
		
		$sql = "SELECT id_flag_traitement,traitement
						FROM flag_traitement 
						WHERE  actif_extract = 1 
				        ORDER BY id_flag_traitement asc";		
		return $this->base_64->query($sql)
							->result_array();
    }
	
	 public function get_statut_saisie(){
		
		$sql = "SELECT id_statut_saisie,libelle
						FROM statut_saisie 
						ORDER BY id_statut_saisie asc";		
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_mode_paiement(){
		
		$sql = "SELECT id_mode_paiement,mode_paiement
						FROM mode_paiement 
						ORDER BY id_mode_paiement asc";		
		return $this->ged->query($sql)
							->result_array();
    }
	public function get_operateur (){
		
		$sql = "SELECT id_utilisateur, login, pass, utilisateurs.id_type_utilisateur,  supprime, 
				   id_groupe_utilisateur, matr_gpao
			  FROM utilisateurs
			  INNER join type_utilisateur on utilisateurs.id_type_utilisateur = type_utilisateur.id_type_utilisateur
				WHERE utilisateurs.id_type_utilisateur in (2,5)
				and utilisateurs.actif = 1 
				order by login asc
				";
		return $this->ged->query($sql)
							->result_array();
	}

    public function get_typologie (){

        $sql = "SELECT id, typologie
			  FROM public.typologie
			  WHERE actif = 1
			  order by typologie asc";
        return $this->ged->query($sql)
            ->result_array();
    }
    public function get_dmt_pli_typage($where_date_courrier_ttt,$where_other,$where_typage,$where_data,$join){
		
			$sql = "
				SELECT 						dt_event,id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,date_typage,debut_fin_typage,duree_typage,etape_typage,id_action_typage,debut_action_typage,fin_action_typage,login_typage,id_user_typage,date_deb_min,date_fin_max
					FROM 
					(
						SELECT
							data_plis.dt_event,
							data_plis.id_pli,	
							mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,
							string_agg(distinct view_dmt_typage.date_ttt::text,',') as date_typage, 
							string_agg( distinct concat((substring (view_dmt_typage.debut::text from 1 for 19)),' - ',(substring (view_dmt_typage.fin::text from 1 for 19))) ,',</br>') as debut_fin_typage,
							min (substring (view_dmt_typage.debut::text from 1 for 19)) as date_deb_min,
							max (substring (view_dmt_typage.fin::text from 1 for 19)) as date_fin_max,
							round(sum(view_dmt_typage.duree_en_h)::numeric,3) as duree_typage,
							view_dmt_typage.etape as etape_typage,
							string_agg(distinct view_dmt_typage.id_action::text,',') as id_action_typage, 
							string_agg(distinct view_dmt_typage.debut_action::text,',') as debut_action_typage,
							string_agg(distinct view_dmt_typage.fin_action::text,',') as fin_action_typage, 
							view_dmt_typage.login_trace as login_typage,
							view_dmt_typage.id_utilisateur as id_user_typage
						FROM
						(
							SELECT
								dt_event,res1.id_pli,
								string_agg(distinct mode_paiement,' + ') as mode_paiement,
								nb_mvt,
								typologie,
								libelle,type_ko,lot_scan,societe,nom_societe
							FROM ( 
								SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								$join
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
								 
							) as res1
							GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
							ORDER BY id_pli ASC
						) as data_plis 
						INNER JOIN view_dmt_typage on data_plis.id_pli = view_dmt_typage.id_pli 
						WHERE 1=1 $where_typage						 
						GROUP BY dt_event,data_plis.id_pli,mode_paiement,nb_mvt,typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,etape_typage,login_trace,id_user_typage
						) as tab
					ORDER BY id_user_typage,id_pli,societe,dt_event ASC
					
				";
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";*/
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_dmt_pli_saisie($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join){
		
			$sql = "
				SELECT 						dt_event,id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,date_saisie,debut_fin_saisie,duree_saisie,etape_saisie,id_action_saisie,debut_action_saisie,fin_action_saisie,login_saisie,id_user_saisie,date_deb_min,date_fin_max
					FROM 
					(
						SELECT
							data_plis.dt_event,
							data_plis.id_pli,	
							mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,
							string_agg(distinct view_dmt_saisie.date_ttt::text,',') as date_saisie, 
							string_agg( distinct concat((substring (view_dmt_saisie.debut::text from 1 for 19)),' - ',(substring (view_dmt_saisie.fin::text from 1 for 19))) ,',</br>') as debut_fin_saisie,
							min (substring (view_dmt_saisie.debut::text from 1 for 19)) as date_deb_min,
							max (substring (view_dmt_saisie.fin::text from 1 for 19)) as date_fin_max,
							round(sum(view_dmt_saisie.duree_en_h)::numeric,3) as duree_saisie,
							view_dmt_saisie.etape as etape_saisie,
							string_agg(distinct view_dmt_saisie.id_action::text,',') as id_action_saisie, 
							string_agg(distinct view_dmt_saisie.debut_action::text,',') as debut_action_saisie,
							string_agg(distinct view_dmt_saisie.fin_action::text,',') as fin_action_saisie, 
							view_dmt_saisie.login_trace as login_saisie,
							view_dmt_saisie.id_utilisateur as id_user_saisie
						FROM
						(
							SELECT
								dt_event,res1.id_pli,
								string_agg(distinct mode_paiement,' + ') as mode_paiement,
								nb_mvt,
								typologie,
								libelle,type_ko,lot_scan,societe,nom_societe
							FROM ( 
								SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								$join
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
							) as res1
							GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
							ORDER BY id_pli ASC
						) as data_plis 
						INNER JOIN view_dmt_saisie on data_plis.id_pli = view_dmt_saisie.id_pli 
						WHERE 1=1 $where_saisie						 
						GROUP BY dt_event,data_plis.id_pli,mode_paiement,nb_mvt,typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,etape_saisie,login_trace,id_user_saisie
						) as tab
					ORDER BY id_user_saisie,id_pli,societe,dt_event ASC
					
				";
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";*/
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_dmt_pli_controle($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join){
		
			$sql = "
				SELECT 						dt_event,id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,date_controle,debut_fin_controle,duree_controle,etape_controle,id_action_controle,debut_action_controle,fin_action_controle,login_controle,id_user_controle,date_deb_min,date_fin_max
					FROM 
					(
						SELECT
							data_plis.dt_event,
							data_plis.id_pli,	
							mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,
							string_agg(distinct view_dmt_controle.date_ttt::text,',') as date_controle, 
							string_agg( distinct concat((substring (view_dmt_controle.debut::text from 1 for 19)),' - ',(substring (view_dmt_controle.fin::text from 1 for 19))) ,',</br>') as debut_fin_controle,
							min (substring (view_dmt_controle.debut::text from 1 for 19)) as date_deb_min,
							max (substring (view_dmt_controle.fin::text from 1 for 19)) as date_fin_max,
							round(sum(view_dmt_controle.duree_en_h)::numeric,3) as duree_controle,
							view_dmt_controle.etape as etape_controle,
							string_agg(distinct view_dmt_controle.id_action::text,',') as id_action_controle, 
							string_agg(distinct view_dmt_controle.debut_action::text,',') as debut_action_controle,
							string_agg(distinct view_dmt_controle.fin_action::text,',') as fin_action_controle, 
							view_dmt_controle.login_trace as login_controle,
							view_dmt_controle.id_utilisateur as id_user_controle
						FROM
						(
							SELECT
								dt_event,res1.id_pli,
								string_agg(distinct mode_paiement,' + ') as mode_paiement,
								nb_mvt,
								typologie,
								libelle,type_ko,lot_scan,societe,nom_societe
							FROM ( 
								SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								$join
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
							) as res1
							GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
							ORDER BY id_pli ASC
						) as data_plis 
						INNER JOIN view_dmt_controle on data_plis.id_pli = view_dmt_controle.id_pli 
						WHERE 1=1 $where_controle						 
						GROUP BY dt_event,data_plis.id_pli,mode_paiement,nb_mvt,typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,etape_controle,login_trace,id_user_controle
						) as tab
					ORDER BY id_user_controle,id_pli,societe,dt_event ASC
					
				";
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";
				*/

		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_datatables_dmt($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatable_dmt_sql($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->ged->limit($length, $start);
        $query = $this->ged->get();
        return $query->result();
    }

    public function count_filtered_dmt($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_dmt_sql($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->ged->get();
        return $query->num_rows();
		
    }

    public function count_all_dmt($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join)
    { 
		
		
        $this->ged->select($this->select_ano)
                ->from("(
					SELECT 						dt_event,id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,date_typage,debut_fin_typage,duree_typage,etape_typage,id_action_typage,debut_action_typage,fin_action_typage,login_typage,id_user_typage,date_deb_min,date_fin_max
					FROM 
					(
						SELECT
							data_plis.dt_event,
							data_plis.id_pli,	
							mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,
							string_agg(distinct view_dmt_typage.date_ttt::text,',') as date_typage, 
							string_agg( distinct concat((substring (view_dmt_typage.debut::text from 1 for 19)),' - ',(substring (view_dmt_typage.fin::text from 1 for 19))) ,',</br>') as debut_fin_typage,
							min (substring (view_dmt_typage.debut::text from 1 for 19)) as date_deb_min,
							max (substring (view_dmt_typage.fin::text from 1 for 19)) as date_fin_max,
							round(sum(view_dmt_typage.duree_en_h)::numeric,3) as duree_typage,
							view_dmt_typage.etape as etape_typage,
							string_agg(distinct view_dmt_typage.id_action::text,',') as id_action_typage, 
							string_agg(distinct view_dmt_typage.debut_action::text,',') as debut_action_typage,
							string_agg(distinct view_dmt_typage.fin_action::text,',') as fin_action_typage, 
							view_dmt_typage.login_trace as login_typage,
							view_dmt_typage.id_utilisateur as id_user_typage
						FROM
						(
							SELECT
								dt_event,res1.id_pli,
								string_agg(distinct mode_paiement,' + ') as mode_paiement,
								nb_mvt,
								typologie,
								libelle,type_ko,lot_scan,societe,nom_societe
							FROM ( 
								SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
								 
							) as res1
							GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
							ORDER BY id_pli ASC
						) as data_plis 
						INNER JOIN view_dmt_typage on data_plis.id_pli = view_dmt_typage.id_pli 
						$where_typage
						 
						GROUP BY dt_event,data_plis.id_pli,mode_paiement,nb_mvt,typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,etape_typage,login_trace,id_user_typage
						) as tab ) as result
                    ");
					
        return $this->ged->count_all_results();
		
    }

    public function get_datatable_dmt_sql($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join,$post_order,$search ,$post_order_col,$post_order_dir){
		/*
		SELECT distinct 
			view_pli_stat.id_pli,
			view_pli_stat.flag_traitement,
			view_pli_stat.id_mode_paiement,
			mode_paiement.mode_paiement,
			view_pli_stat.societe,
			societe.nom_societe,
			view_pli_stat.dt_event,
			pli_ok,pli_ko,pli_ci,pli_ko_kd,pli_ko_ke,pli_ko_ks,pli_hors_perimetre,pli_ferme, pli_ko_scan,
			typologie,
			statut_saisie.libelle,
			case when pli_ko_scan = 1 then 'KO scan' else 
			case when pli_ferme = 1 then 'fermé' else 
			case when pli_hors_perimetre = 1 then 'Hors périmètre' else null
			end end end type_ko,
			nb_mvt,
			lot_scan
		
		FROM view_pli_stat 
		LEFT JOIN data_pli on view_pli_stat.id_pli = data_pli.id_pli 
		LEFT JOIN paiement on paiement.id_pli = view_pli_stat.id_pli  
		LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
		LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
		LEFT JOIN motif_ko on motif_ko.id_motif = data_pli.motif_ko
		LEFT JOIN motif_ko_scan on data_pli.id_pli = motif_ko_scan.pli_id
		LEFT JOIN titres on titres.id = data_pli.titre
		LEFT JOIN societe on societe.id = titres.societe_id
		LEFT JOIN view_nb_mouvement_pli on view_pli_stat.id_pli = view_nb_mouvement_pli.id_pli
		LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
		$join
		WHERE  pli_cloture = 1 $where_date_courrier_ttt $where_other
		$where_data	

		*/
         $this->ged->select($this->select_ano)
                 ->from("(SELECT dt_event,id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,date_typage,debut_fin_typage,duree_typage,etape_typage,id_action_typage,debut_action_typage,fin_action_typage,login_typage,id_user_typage,date_deb_min,date_fin_max
					FROM 
					(
						SELECT
							data_plis.dt_event,
							data_plis.id_pli,	
							mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,
							string_agg(distinct view_dmt_typage.date_ttt::text,',') as date_typage, 
							string_agg( distinct concat((substring (view_dmt_typage.debut::text from 1 for 19)),' - ',(substring (view_dmt_typage.fin::text from 1 for 19))) ,',</br>') as debut_fin_typage,
							min (substring (view_dmt_typage.debut::text from 1 for 19)) as date_deb_min,
							max (substring (view_dmt_typage.fin::text from 1 for 19)) as date_fin_max,
							round(sum(view_dmt_typage.duree_en_h)::numeric,3) as duree_typage,
							view_dmt_typage.etape as etape_typage,
							string_agg(distinct view_dmt_typage.id_action::text,',') as id_action_typage, 
							string_agg(distinct view_dmt_typage.debut_action::text,',') as debut_action_typage,
							string_agg(distinct view_dmt_typage.fin_action::text,',') as fin_action_typage, 
							view_dmt_typage.login_trace as login_typage,
							view_dmt_typage.id_utilisateur as id_user_typage
						FROM
						(
							SELECT
								dt_event,res1.id_pli,
								string_agg(distinct mode_paiement,' + ') as mode_paiement,
								nb_mvt,
								typologie,
								libelle,type_ko,lot_scan,societe,nom_societe
							FROM ( 
								
								SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
								 
							) as res1
							GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
							ORDER BY id_pli ASC
						) as data_plis 
						  INNER JOIN view_dmt_typage on data_plis.id_pli = view_dmt_typage.id_pli 
						  $where_typage
						GROUP BY dt_event,data_plis.id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,etape_typage,login_trace,id_user_typage
						ORDER BY login_trace ASC
						) as tab ) as result
						");

        $i = 0;
        foreach ($this->column_search_ano as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->ilike($item, $search);
                }
                else
                {
                    $this->ged->or_ilike($item, $search);
                }

                if(count($this->column_search_ano) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->ged->order_by($this->column_order_ano[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_ano))
        {
            $order = $this->order_ano;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
    }
	
	public function get_datatables_dmt_saisie($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatable_dmt_saisie_sql($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->ged->limit($length, $start);
        $query = $this->ged->get();
        return $query->result();
    }

    public function count_filtered_dmt_saisie($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_dmt_saisie_sql($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->ged->get();
        return $query->num_rows();
		
    }

    public function count_all_dmt_saisie($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join)
    { 
	  $this->ged->select($this->select_saisie_dmt)
                ->from("(SELECT 						dt_event,id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,date_saisie,debut_fin_saisie,duree_saisie,etape_saisie,id_action_saisie,debut_action_saisie,fin_action_saisie,login_saisie,id_user_saisie,date_deb_min,date_fin_max
					FROM 
					(
						SELECT
							data_plis.dt_event,
							data_plis.id_pli,	
							mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,
							string_agg(distinct view_dmt_saisie.date_ttt::text,',') as date_saisie, 
							string_agg( distinct concat((substring (view_dmt_saisie.debut::text from 1 for 19)),' - ',(substring (view_dmt_saisie.fin::text from 1 for 19))) ,',</br>') as debut_fin_saisie,
							min (substring (view_dmt_saisie.debut::text from 1 for 19)) as date_deb_min,
							max (substring (view_dmt_saisie.fin::text from 1 for 19)) as date_fin_max,
							round(sum(view_dmt_saisie.duree_en_h)::numeric,3) as duree_saisie,
							view_dmt_saisie.etape as etape_saisie,
							string_agg(distinct view_dmt_saisie.id_action::text,',') as id_action_saisie, 
							string_agg(distinct view_dmt_saisie.debut_action,',') as debut_action_saisie,
							string_agg(distinct view_dmt_saisie.fin_action::text,',') as fin_action_saisie, 
							string_agg(distinct view_dmt_saisie.login_trace::text,',') as login_saisie,
							view_dmt_saisie.id_utilisateur as id_user_saisie
						FROM
						(
							SELECT
								dt_event,res1.id_pli,
								string_agg(distinct mode_paiement,' + ') as mode_paiement,
								nb_mvt,
								typologie,
								libelle,type_ko,lot_scan,societe,nom_societe
							FROM ( 
								SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)		
							) as res1
							GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
							ORDER BY id_pli ASC
						) as data_plis 
						INNER JOIN view_dmt_saisie on data_plis.id_pli = view_dmt_saisie.id_pli
						$where_saisie
						GROUP BY dt_event,data_plis.id_pli,mode_paiement,nb_mvt,typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,etape_saisie,login_trace,id_user_saisie
						) as tab ) as result
                         ");
					
        return $this->ged->count_all_results();
		
    }

    public function get_datatable_dmt_saisie_sql($where_date_courrier_ttt,$where_other,$where_saisie,$where_data,$join,$post_order,$search ,$post_order_col,$post_order_dir){

         $this->ged->select($this->select_saisie_dmt)
                 ->from("(SELECT 						dt_event,id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,date_saisie,debut_fin_saisie,duree_saisie,etape_saisie,id_action_saisie,debut_action_saisie,fin_action_saisie,login_saisie,id_user_saisie,date_deb_min,date_fin_max
					FROM 
					(
						SELECT
							data_plis.dt_event,
							data_plis.id_pli,	
							mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,
							string_agg(distinct view_dmt_saisie.date_ttt::text,',') as date_saisie, 
							string_agg( distinct concat((substring (view_dmt_saisie.debut::text from 1 for 19)),' - ',(substring (view_dmt_saisie.fin::text from 1 for 19))) ,',</br>') as debut_fin_saisie,
							min (substring (view_dmt_saisie.debut::text from 1 for 19)) as date_deb_min,
							max (substring (view_dmt_saisie.fin::text from 1 for 19)) as date_fin_max,
							round(sum(view_dmt_saisie.duree_en_h)::numeric,3) as duree_saisie,
							view_dmt_saisie.etape as etape_saisie,
							string_agg(distinct view_dmt_saisie.id_action::text,',') as id_action_saisie, 
							string_agg(distinct view_dmt_saisie.debut_action,',') as debut_action_saisie,
							string_agg(distinct view_dmt_saisie.fin_action::text,',') as fin_action_saisie, 
							string_agg(distinct view_dmt_saisie.login_trace::text,',') as login_saisie,
							view_dmt_saisie.id_utilisateur as id_user_saisie
						FROM
						(
							SELECT
								dt_event,res1.id_pli,
								string_agg(distinct mode_paiement,' + ') as mode_paiement,
								nb_mvt,
								typologie,
								libelle,type_ko,lot_scan,societe,nom_societe
							FROM ( 
								SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)					
							) as res1
							GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
							ORDER BY id_pli ASC
						) as data_plis 
						INNER JOIN view_dmt_saisie on data_plis.id_pli = view_dmt_saisie.id_pli
						$where_saisie
						GROUP BY dt_event,data_plis.id_pli,mode_paiement,nb_mvt,typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,etape_saisie,login_trace,id_user_saisie
						ORDER BY login_trace ASC
						) as tab ) as result
						");

        $i = 0;
        foreach ($this->column_search_saisie_dmt as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->ilike($item, $search);
                }
                else
                {
                    $this->ged->or_ilike($item, $search);
                }

                if(count($this->column_search_saisie_dmt) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->ged->order_by($this->column_order_saisie_dmt[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_saisie_dmt))
        {
            $order = $this->order_saisie_dmt;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
    }
	
	public function get_datatables_dmt_ctrl($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatable_dmt_ctrl_sql($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->ged->limit($length, $start);
        $query = $this->ged->get();
        return $query->result();
    }

    public function count_filtered_dmt_ctrl($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_dmt_ctrl_sql($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->ged->get();
        return $query->num_rows();
		
    }

    public function count_all_dmt_ctrl($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join)
    { 
		
		
        $this->ged->select($this->select_ctrl_dmt)
                ->from("(
					SELECT 						dt_event,id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,date_controle,debut_fin_controle,duree_controle,etape_controle,id_action_controle,debut_action_controle,fin_action_controle,login_controle,id_user_controle,date_deb_min,date_fin_max
					FROM 
					(
						SELECT
							data_plis.dt_event,
							data_plis.id_pli,	
							mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,
							string_agg(distinct view_dmt_controle.date_ttt::text,',') as date_controle, 
							string_agg( distinct concat((substring (view_dmt_controle.debut::text from 1 for 19)),' - ',(substring (view_dmt_controle.fin::text from 1 for 19))) ,',</br>') as debut_fin_controle,
							min (substring (view_dmt_controle.debut::text from 1 for 19)) as date_deb_min,
							max (substring (view_dmt_controle.fin::text from 1 for 19)) as date_fin_max,
							round(sum(view_dmt_controle.duree_en_h)::numeric,3) as duree_controle,
							view_dmt_controle.etape as etape_controle,
							string_agg(distinct view_dmt_controle.id_action::text,',') as id_action_controle, 
							string_agg(distinct view_dmt_controle.debut_action,',') as debut_action_controle,
							string_agg(distinct view_dmt_controle.fin_action::text,',') as fin_action_controle, 
							string_agg(distinct view_dmt_controle.login_trace::text,',') as login_controle,
							view_dmt_controle.id_utilisateur as id_user_controle
						FROM
						(
							SELECT
								dt_event,res1.id_pli,
								string_agg(distinct mode_paiement,' + ') as mode_paiement,
								nb_mvt,
								typologie,
								libelle,type_ko,lot_scan,societe,nom_societe
							FROM ( 
								SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
							) as res1
							GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
							ORDER BY id_pli ASC
						) as data_plis 
						INNER JOIN view_dmt_controle on data_plis.id_pli = view_dmt_controle.id_pli 
						$where_controle
						 
						GROUP BY dt_event,data_plis.id_pli,mode_paiement,nb_mvt,typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,etape_controle,login_trace,id_user_controle
						) as tab ) as result
                         ");
					
        return $this->ged->count_all_results();
		
    }

    public function get_datatable_dmt_ctrl_sql($where_date_courrier_ttt,$where_other,$where_controle,$where_data,$join,$post_order,$search ,$post_order_col,$post_order_dir){

         $this->ged->select($this->select_ctrl_dmt)
                 ->from("(SELECT 						dt_event,id_pli,mode_paiement,nb_mvt,typologie,libelle,type_ko,lot_scan,societe,nom_societe,date_controle,debut_fin_controle,duree_controle,etape_controle,id_action_controle,debut_action_controle,fin_action_controle,login_controle,id_user_controle,date_deb_min,date_fin_max
					FROM 
					(
						SELECT
							data_plis.dt_event,
							data_plis.id_pli,	
							mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,
							string_agg(distinct view_dmt_controle.date_ttt::text,',') as date_controle, 
							string_agg( distinct concat((substring (view_dmt_controle.debut::text from 1 for 19)),' - ',(substring (view_dmt_controle.fin::text from 1 for 19))) ,',</br>') as debut_fin_controle,
							min (substring (view_dmt_controle.debut::text from 1 for 19)) as date_deb_min,
							max (substring (view_dmt_controle.fin::text from 1 for 19)) as date_fin_max,
							round(sum(view_dmt_controle.duree_en_h)::numeric,3) as duree_controle,
							view_dmt_controle.etape as etape_controle,
							string_agg(distinct view_dmt_controle.id_action::text,',') as id_action_controle, 
							string_agg(distinct view_dmt_controle.debut_action,',') as debut_action_controle,
							string_agg(distinct view_dmt_controle.fin_action::text,',') as fin_action_controle, 
							string_agg(distinct view_dmt_controle.login_trace::text,',') as login_controle,
							view_dmt_controle.id_utilisateur as id_user_controle
						FROM
						(
							SELECT
								dt_event,res1.id_pli,
								string_agg(distinct mode_paiement,' + ') as mode_paiement,
								nb_mvt,
								typologie,
								libelle,type_ko,lot_scan,societe,nom_societe
							FROM ( 
								SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)						
							) as res1
							GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
							ORDER BY id_pli ASC
						) as data_plis 
						  INNER JOIN view_dmt_controle on data_plis.id_pli = view_dmt_controle.id_pli 
						  $where_controle
						GROUP BY dt_event,data_plis.id_pli,mode_paiement,nb_mvt,typologie,
							libelle,type_ko,lot_scan,societe,nom_societe,etape_controle,login_trace,id_user_controle
						ORDER BY login_trace ASC
						) as tab ) as result
						");

        $i = 0;
        foreach ($this->column_search_ctrl_dmt as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->ilike($item, $search);
                }
                else
                {
                    $this->ged->or_ilike($item, $search);
                }

                if(count($this->column_search_ctrl_dmt) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->ged->order_by($this->column_order_ctrl_dmt[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_ctrl_dmt))
        {
            $order = $this->order_ctrl_dmt;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
    }
	
	public function get_dmt_global($where_date_courrier_ttt,$where_other,$where_traitement,$where_data,$join,$table_traitement){
			$sql = "
					SELECT distinct 
						view_pli_stat.id_pli,
						round(sum($table_traitement.duree_en_h)::numeric,3) as duree,
						$table_traitement.login_trace as login,
						$table_traitement.id_utilisateur as id_user		
					FROM view_pli_stat 
					INNER JOIN $table_traitement on view_pli_stat.id_pli = $table_traitement.id_pli 
					WHERE  pli_cloture = 1 $where_date_courrier_ttt $where_other
					$where_data					
					/*and view_pli_stat.societe = '1'*/				
					GROUP BY view_pli_stat.id_pli,login,$table_traitement.id_utilisateur							
					ORDER BY view_pli_stat.id_pli,$table_traitement.id_utilisateur ASC
				";
				return $this->ged->query($sql)
							->result_array();
									
		}
	public function get_dmt_globals($where_date_ttt,$where_other,$join_global,$table_traitement){
			$sql = "
				SELECT DISTINCT view_pli_stat.id_pli,
                round(sum($table_traitement.duree_en_h)::numeric, 3) AS duree,
                $table_traitement.login_trace AS login,
                $table_traitement.id_utilisateur AS id_user
					FROM view_pli_stat
					LEFT JOIN typologie ON typologie.id = view_pli_stat.id_typologie
					INNER JOIN societe ON societe.id = view_pli_stat.societe
					INNER JOIN $table_traitement ON view_pli_stat.id_pli = $table_traitement.id_pli
					$join_global
					WHERE pli_cloture = 1
					  $where_date_ttt $where_other
					GROUP BY view_pli_stat.id_pli,
							 login,
							 $table_traitement.id_utilisateur
					ORDER BY view_pli_stat.id_pli,
							 $table_traitement.id_utilisateur ASC
				";
				return $this->ged->query($sql)
							->result_array();
				
		}
	public function get_dmt_global_global($where_date_courrier_ttt,$where_other,$where_data,$join,$where_ttt_typage,$where_ttt_saisie,$where_ttt_controle){
			$sql = "SELECT
						id_pli,login,id_user,SUM(duree)as duree
					FROM
					(
						SELECT
							id_pli,login,id_user,duree,action_ttt
						FROM

						(
							SELECT distinct 
								view_pli_stat.id_pli,	
								view_dmt_typage.login_trace as login,
								view_dmt_typage.id_utilisateur as id_user,
								round(sum(view_dmt_typage.duree_en_h)::numeric,3) as duree,
								'1-typage' as action_ttt
							FROM view_pli_stat 
							LEFT JOIN view_dmt_typage on view_pli_stat.id_pli = view_dmt_typage.id_pli
							WHERE  pli_cloture = 1 and view_dmt_typage.login_trace is not null $where_date_courrier_ttt $where_other $where_ttt_typage
							and view_dmt_typage.duree_en_h>0
							GROUP BY view_pli_stat.id_pli,login ,view_dmt_typage.id_utilisateur				
							ORDER BY view_pli_stat.id_pli,login ASC
						) as data_typage
						union
						(
							SELECT distinct 
								view_pli_stat.id_pli,	
								view_dmt_saisie.login_trace as login,
								view_dmt_saisie.id_utilisateur as id_user,
								round(sum(view_dmt_saisie.duree_en_h)::numeric,3) as duree,
								'2-saisie' as action_ttt
							FROM view_pli_stat 
							LEFT JOIN view_dmt_saisie on view_pli_stat.id_pli = view_dmt_saisie.id_pli 
							WHERE  pli_cloture = 1 and view_dmt_saisie.login_trace is not null $where_date_courrier_ttt $where_other $where_ttt_saisie
							and view_dmt_saisie.duree_en_h>0
							GROUP BY view_pli_stat.id_pli,login ,view_dmt_saisie.id_utilisateur				
							ORDER BY view_pli_stat.id_pli,login ASC
						) /*as data_saisie*/
						union
						(
							SELECT distinct 
								view_pli_stat.id_pli,	
								view_dmt_controle.login_trace as login,
								view_dmt_controle.id_utilisateur as id_user,
								round(sum(view_dmt_controle.duree_en_h)::numeric,3) as duree,
								'3-controle' as action_ttt
							FROM view_pli_stat 
							LEFT JOIN view_dmt_controle on view_pli_stat.id_pli = view_dmt_controle.id_pli 
							WHERE  pli_cloture = 1 and view_dmt_controle.login_trace is not null $where_date_courrier_ttt $where_other $where_ttt_controle
							and view_dmt_controle.duree_en_h>0
							GROUP BY view_pli_stat.id_pli,login ,view_dmt_controle.id_utilisateur		
							ORDER BY view_pli_stat.id_pli,login ASC
						) /*as data_controle*/
					) as data
					where login is not null
					group by id_pli,login ,id_user
					order by id_pli,login asc

				";
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";
				*/
				return $this->ged->query($sql)
							->result_array();
				
						
		}
	
	public function get_datatables_dmt_global($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$where_data_pli,$wheredata_pli,$where_date,$join,$length,$start,$post_order,$search,$post_order_col,$post_order_dir){
        $this->get_datatable_dmt_global_sql($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$where_data_pli,$wheredata_pli,$where_date,$join,$post_order,$search,$post_order_col,$post_order_dir );
        if($length != -1)
         $this->ged->limit($length, $start);
        $query = $this->ged->get();
        return $query->result();
    }

    public function count_filtered_dmt_global($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$where_data_pli,$wheredata_pli,$where_date,$join,$post_order,$search,$post_order_col,$post_order_dir )
    {
        $this->get_datatable_dmt_global_sql($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$where_data_pli,$wheredata_pli,$where_date,$join,$post_order,$search,$post_order_col,$post_order_dir);
		
        $query = $this->ged->get();
        return $query->num_rows();
		
    }

    public function count_all_dmt_global($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$where_data_pli,$wheredata_pli,$where_date,$join)
    { 
		
		
        $this->ged->select($this->select_global)
                ->from("(
				SELECT
						data_plis.dt_event,
						data_plis.id_pli,	
						mode_paiement,
						nb_mvt,
						typologie,
						libelle,type_ko,lot_scan,societe,nom_societe,
						duree_typage, duree_saisie, duree_controle
					FROM
					(
						SELECT
							dt_event,res1.id_pli,
							string_agg(distinct mode_paiement,' + ') as mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe
						FROM ( 
							SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								WHERE
								(((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
								$where_date_courrier_ttt 
								$where_other
								$where_data
								 
														
						) as res1
						GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
						ORDER BY id_pli ASC
					) as data_plis 
					LEFT JOIN
					(	SELECT datas.id_pli,duree_typage, duree_saisie,duree_controle
						  FROM
							(
							SELECT DISTINCT f.id_pli
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							INNER JOIN f_histo_pli_by_date_event(".$where_date.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli AND f.id = histo_pli.id
							INNER JOIN view_statut_pli_cloture ON view_statut_pli_cloture.id_pli = f.id_pli
							INNER join trace on trace.id_pli = f_pli.id_pli
							WHERE pli_cloture = 1
							".$where_data_pli."
							GROUP BY f.id_pli
							ORDER BY f.id_pli ASC
							 ) AS datas
						  LEFT JOIN
							(SELECT id_pli,
								 SUM(duree_typage) AS duree_typage
							 FROM
							   (
							SELECT DISTINCT f.id_pli,
							view_dmt_typage.login_trace AS login,
							view_dmt_typage.id_utilisateur AS id_user,
							round(sum(view_dmt_typage.duree_en_h)::numeric, 3) AS duree_typage
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN f_histo_pli_by_last_date_event(".$where_date.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli AND f.id = histo_pli.id
							LEFT JOIN view_statut_pli_cloture ON view_statut_pli_cloture.id_pli = f.id_pli
							INNER JOIN view_dmt_typage ON f_pli.id_pli = view_dmt_typage.id_pli

							WHERE pli_cloture = 1 
							AND view_dmt_typage.duree_en_h > 0
							AND view_dmt_typage.login_trace IS NOT NULL
							".$wheredata_pli." ".$where_typage." 
							GROUP BY f.id_pli,login,id_user
								 )AS r
							 GROUP BY id_pli
							 ORDER BY id_pli ASC
						   )
							AS data_typage ON datas.id_pli = data_typage.id_pli
						  LEFT JOIN
							(SELECT id_pli,
								 SUM(duree_saisie) AS duree_saisie
							 FROM
							   (
							SELECT DISTINCT f.id_pli,
							view_dmt_saisie.login_trace AS login,
							view_dmt_saisie.id_utilisateur AS id_user,
							round(sum(view_dmt_saisie.duree_en_h)::numeric, 3) AS duree_saisie
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN f_histo_pli_by_last_date_event(".$where_date.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli AND f.id = histo_pli.id
							LEFT JOIN view_statut_pli_cloture ON view_statut_pli_cloture.id_pli = f.id_pli
							INNER JOIN view_dmt_saisie ON f_pli.id_pli = view_dmt_saisie.id_pli

							WHERE pli_cloture = 1
							AND view_dmt_saisie.duree_en_h > 0
							AND view_dmt_saisie.login_trace IS NOT NULL
							".$wheredata_pli." ".$where_saisie." 
							GROUP BY f.id_pli,login,id_user
								 )AS r
							 GROUP BY id_pli
							 ORDER BY id_pli ASC
						   )
							AS data_saisie ON datas.id_pli = data_saisie.id_pli
						  LEFT JOIN
							(SELECT id_pli,
								 SUM(duree_controle) AS duree_controle
							 FROM
							   (
							SELECT DISTINCT f.id_pli,
							view_dmt_controle.login_trace AS login,
							view_dmt_controle.id_utilisateur AS id_user,
							round(sum(view_dmt_controle.duree_en_h)::numeric, 3) AS duree_controle
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN f_histo_pli_by_last_date_event(".$where_date.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli AND f.id = histo_pli.id
							LEFT JOIN view_statut_pli_cloture ON view_statut_pli_cloture.id_pli = f.id_pli
							INNER JOIN view_dmt_controle ON f_pli.id_pli = view_dmt_controle.id_pli

							WHERE pli_cloture = 1
							AND view_dmt_controle.duree_en_h > 0
							AND view_dmt_controle.login_trace IS NOT NULL
							".$wheredata_pli." ".$where_controle." 
							GROUP BY f.id_pli,login,id_user
								 )AS r
							 GROUP BY id_pli
							 ORDER BY id_pli ASC
						   )
							AS data_controle ON datas.id_pli = data_controle.id_pli
					) as data_dmt on data_plis.id_pli = data_dmt.id_pli 
				) as result
			");
					
        return $this->ged->count_all_results();
		
    }

    
	public function get_datatable_dmt_global_sql($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$where_data_pli,$wheredata_pli,$where_date,$join,$post_order,$search ,$post_order_col,$post_order_dir){
		
		$this->ged->select($this->select_global)
                 ->from("(
					 SELECT
						data_plis.dt_event,
						data_plis.id_pli,	
						mode_paiement,
						nb_mvt,
						typologie,
						libelle,type_ko,lot_scan,societe,nom_societe,
						duree_typage, duree_saisie, duree_controle
					FROM
					(
						SELECT
							dt_event,res1.id_pli,
							string_agg(distinct mode_paiement,' + ') as mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe
						FROM ( 
							SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								WHERE 
								(((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
								$where_date_courrier_ttt 
								$where_other
								$where_data														
						) as res1
						GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
						ORDER BY id_pli ASC
					) as data_plis 
					LEFT JOIN
					(	SELECT datas.id_pli,duree_typage, duree_saisie,duree_controle
						  FROM
							(
							SELECT DISTINCT f.id_pli
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							INNER JOIN f_histo_pli_by_date_event(".$where_date.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli AND f.id = histo_pli.id
							INNER JOIN view_statut_pli_cloture ON view_statut_pli_cloture.id_pli = f.id_pli
							INNER join trace on trace.id_pli = f_pli.id_pli
							WHERE pli_cloture = 1
							".$where_data_pli."
							GROUP BY f.id_pli
							ORDER BY f.id_pli ASC
							 ) AS datas
						  LEFT JOIN
							(SELECT id_pli,
								 SUM(duree_typage) AS duree_typage
							 FROM
							   (
							SELECT DISTINCT f.id_pli,
							view_dmt_typage.login_trace AS login,
							view_dmt_typage.id_utilisateur AS id_user,
							round(sum(view_dmt_typage.duree_en_h)::numeric, 3) AS duree_typage
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN f_histo_pli_by_last_date_event(".$where_date.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli AND f.id = histo_pli.id
							LEFT JOIN view_statut_pli_cloture ON view_statut_pli_cloture.id_pli = f.id_pli
							INNER JOIN view_dmt_typage ON f_pli.id_pli = view_dmt_typage.id_pli

							WHERE pli_cloture = 1
							AND view_dmt_typage.login_trace IS NOT NULL
							AND view_dmt_typage.duree_en_h > 0
							".$wheredata_pli." ".$where_typage." 
							GROUP BY f.id_pli,login,id_user
								 )AS r
							 GROUP BY id_pli
							 ORDER BY id_pli ASC
						   )
							AS data_typage ON datas.id_pli = data_typage.id_pli
						  LEFT JOIN
							(SELECT id_pli,
								 SUM(duree_saisie) AS duree_saisie
							 FROM
							   (
							SELECT DISTINCT f.id_pli,
							view_dmt_saisie.login_trace AS login,
							view_dmt_saisie.id_utilisateur AS id_user,
							round(sum(view_dmt_saisie.duree_en_h)::numeric, 3) AS duree_saisie
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN f_histo_pli_by_last_date_event(".$where_date.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli AND f.id = histo_pli.id
							LEFT JOIN view_statut_pli_cloture ON view_statut_pli_cloture.id_pli = f.id_pli
							INNER JOIN view_dmt_saisie ON f_pli.id_pli = view_dmt_saisie.id_pli

							WHERE pli_cloture = 1
							AND view_dmt_saisie.duree_en_h > 0
							AND view_dmt_saisie.login_trace IS NOT NULL
							".$wheredata_pli." ".$where_saisie." 
							GROUP BY f.id_pli,login,id_user
								 )AS r
							 GROUP BY id_pli
							 ORDER BY id_pli ASC
						   )
							AS data_saisie ON datas.id_pli = data_saisie.id_pli
						  LEFT JOIN
							(SELECT id_pli,
								 SUM(duree_controle) AS duree_controle
							 FROM
							   (
							SELECT DISTINCT f.id_pli,
							view_dmt_controle.login_trace AS login,
							view_dmt_controle.id_utilisateur AS id_user,
							round(sum(view_dmt_controle.duree_en_h)::numeric, 3) AS duree_controle
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN f_histo_pli_by_last_date_event(".$where_date.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli AND f.id = histo_pli.id
							LEFT JOIN view_statut_pli_cloture ON view_statut_pli_cloture.id_pli = f.id_pli
							INNER JOIN view_dmt_controle ON f_pli.id_pli = view_dmt_controle.id_pli

							WHERE pli_cloture = 1
							AND view_dmt_controle.duree_en_h > 0
							AND view_dmt_controle.login_trace IS NOT NULL
							".$wheredata_pli." ".$where_controle." 
							GROUP BY f.id_pli,login,id_user
								 )AS r
							 GROUP BY id_pli
							 ORDER BY id_pli ASC
						   )
							AS data_controle ON datas.id_pli = data_controle.id_pli
					) as data_dmt on data_plis.id_pli = data_dmt.id_pli 
					) as result
				");
        /* $this->ged->select($this->select_global)
                 ->from("(
				 
					SELECT
						data_plis.dt_event,
						data_plis.id_pli,	
						mode_paiement,
						nb_mvt,
						typologie,
						libelle,type_ko,lot_scan,societe,nom_societe,
						duree_typage, duree_saisie, duree_controle
					FROM
					(
						SELECT
							dt_event,res1.id_pli,
							string_agg(distinct mode_paiement,' + ') as mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe
						FROM ( 
							SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								$join
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
														
						) as res1
						GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
						ORDER BY id_pli ASC
					) as data_plis 
					LEFT JOIN
				(				SELECT
						datas.id_pli, duree_typage, duree_saisie, duree_controle
					FROM
						(
						SELECT distinct 
								view_pli_stat.id_pli
							FROM view_pli_stat
							WHERE  pli_cloture = 1 $where_data_pli
							GROUP BY view_pli_stat.id_pli				
							ORDER BY view_pli_stat.id_pli
							
						) as datas
						left join 
						(
							SELECT	
								id_pli,
								SUM(duree_typage) as duree_typage									
							FROM
							(
								SELECT distinct 
								view_pli_stat.id_pli,
								view_dmt_typage.login_trace as login,
								view_dmt_typage.id_utilisateur as id_user,
								round(sum(view_dmt_typage.duree_en_h)::numeric,3) as duree_typage
								FROM view_pli_stat 
								LEFT JOIN view_dmt_typage on view_pli_stat.id_pli = view_dmt_typage.id_pli
								WHERE  pli_cloture = 1 and view_dmt_typage.login_trace is not null 
								$where_data_pli
								$where_typage
								GROUP BY view_pli_stat.id_pli,login ,id_user	
							)as r
							GROUP BY id_pli
							
						) as data_typage on datas.id_pli = data_typage.id_pli 
						left join 
						(
							
							SELECT	
								id_pli,
								SUM(duree) as duree_saisie									
							FROM
								(SELECT distinct 
								view_pli_stat.id_pli,	
								view_dmt_saisie.login_trace as login,
								view_dmt_saisie.id_utilisateur as id_user,
								round(sum(view_dmt_saisie.duree_en_h)::numeric,3) as duree
								FROM view_pli_stat 
								INNER JOIN view_dmt_saisie on view_pli_stat.id_pli = view_dmt_saisie.id_pli 
								WHERE  pli_cloture = 1 and view_dmt_saisie.login_trace is not null  
								$where_data_pli						
								$where_saisie
								GROUP BY view_pli_stat.id_pli,login ,id_user				
								) as r
								GROUP BY id_pli
								

						) as data_saisie on datas.id_pli = data_saisie.id_pli
						left join 
						(
							SELECT	
								id_pli,
								SUM(duree) as duree_controle									
							FROM
								(SELECT distinct 
								view_pli_stat.id_pli,	
								view_dmt_controle.login_trace as login,
								view_dmt_controle.id_utilisateur as id_user,
								round(sum(view_dmt_controle.duree_en_h)::numeric,3) as duree
								FROM view_pli_stat 
								INNER JOIN view_dmt_controle on view_pli_stat.id_pli = view_dmt_controle.id_pli
								WHERE  pli_cloture = 1 and view_dmt_controle.login_trace is not null  
								$where_data_pli							
								$where_controle
								GROUP BY view_pli_stat.id_pli,login ,id_user				
								) as r
								GROUP BY id_pli								
							
							
						) as data_controle on datas.id_pli = data_controle.id_pli
						ORDER BY id_pli ASC
					 ) as data_dmt on data_plis.id_pli = data_dmt.id_pli 
					 ) as result
						");
		*/
        $i = 0;
        foreach ($this->column_search_global as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->ilike($item, $search);
                }
                else
                {
                    $this->ged->or_ilike($item, $search);
                }

                if(count($this->column_search_global) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->ged->order_by($this->column_order_global[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_global))
        {
            $order = $this->order_global;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
    }
	
	public function get_dmt($where_date_courrier_ttt,$where_other,$where_typage,$where_saisie,$where_controle,$where_data,$join){
		
			$sql = "
				SELECT
						data_plis.dt_event,
						data_plis.id_pli,	
						mode_paiement,
						nb_mvt,
						typologie,
						libelle,type_ko,lot_scan,societe,nom_societe,
						duree_typage, duree_saisie, duree_controle
					FROM
					(
						SELECT
							dt_event,res1.id_pli,
							string_agg(distinct mode_paiement,' + ') as mode_paiement,
							nb_mvt,
							typologie,
							libelle,type_ko,lot_scan,societe,nom_societe
						FROM ( 
							SELECT distinct 
									f_pli.id_pli,
									view_histo_pli_oid.flag_traitement,
									mode_paiement.mode_paiement,
									data_pli.societe,
									societe.nom_societe,
									view_histo_pli_oid.dt_event,
									typologie.typologie,
									statut_saisie.libelle,
									case when histo_pli.flag_traitement = 16 then 'KO scan' else 
									case when histo_pli.flag_traitement = 24  then 'fermé' else 
									case when histo_pli.flag_traitement = 21 then 'Hors périmètre' else null
									end end end type_ko,
									nb_mvt,
									f_lot_numerisation.lot_scan,
									trace.id_user						
								FROM f_pli 
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN paiement on paiement.id_pli = f_pli.id_pli  
								LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN titres on titres.id = data_pli.titre
								LEFT JOIN societe on societe.id = titres.societe_id
								LEFT JOIN histo_pli on f_pli.id_pli = histo_pli.id_pli
								LEFT JOIN view_nb_mouvement_pli on f_pli.id_pli = view_nb_mouvement_pli.id_pli
								LEFT JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.oid = histo_pli.oid
								LEFT JOIN view_pli_avec_cheque_new ON view_pli_avec_cheque_new.id_pli = f_pli.id_pli					 
								LEFT join trace on trace.id_pli = f_pli.id_pli
								$join
								WHERE  1=1 $where_date_courrier_ttt $where_other
								$where_data
								 and (((view_histo_pli_oid.flag_traitement = ANY (ARRAY[9, 14, 15, 20, 18])) 
									AND view_pli_avec_cheque_new.id_pli IS NULL) 
									OR (view_histo_pli_oid.flag_traitement = 26 AND view_pli_avec_cheque_new.id_pli IS NOT NULL) 
									OR view_histo_pli_oid.flag_traitement = 25)
														
						) as res1
						GROUP BY dt_event,libelle,type_ko,nb_mvt,lot_scan,res1.id_pli,typologie,societe,nom_societe
						ORDER BY id_pli ASC
					) as data_plis 
					LEFT JOIN
				(				SELECT
						datas.id_pli, duree_typage, duree_saisie, duree_controle
					FROM
						(
						SELECT distinct 
								view_pli_stat.id_pli
							FROM view_pli_stat
							WHERE  pli_cloture = 1 $where_date_courrier_ttt $where_other
							GROUP BY view_pli_stat.id_pli				
							ORDER BY view_pli_stat.id_pli
							
						) as datas
						left join 
						(
							SELECT	
								id_pli,
								SUM(duree_typage) as duree_typage									
							FROM
							(
								SELECT distinct 
								view_pli_stat.id_pli,
								view_dmt_typage.login_trace as login,
								view_dmt_typage.id_utilisateur as id_user,
								round(sum(view_dmt_typage.duree_en_h)::numeric,3) as duree_typage
								FROM view_pli_stat 
								LEFT JOIN view_dmt_typage on view_pli_stat.id_pli = view_dmt_typage.id_pli
								WHERE  pli_cloture = 1 and view_dmt_typage.login_trace is not null 
								$where_date_courrier_ttt $where_other 
								$where_typage
								GROUP BY view_pli_stat.id_pli,login ,id_user	
							)as r
							GROUP BY id_pli
							
						) as data_typage on datas.id_pli = data_typage.id_pli 
						left join 
						(
							
							SELECT	
								id_pli,
								SUM(duree) as duree_saisie									
							FROM
								(SELECT distinct 
								view_pli_stat.id_pli,	
								view_dmt_saisie.login_trace as login,
								view_dmt_saisie.id_utilisateur as id_user,
								round(sum(view_dmt_saisie.duree_en_h)::numeric,3) as duree
								FROM view_pli_stat 
								INNER JOIN view_dmt_saisie on view_pli_stat.id_pli = view_dmt_saisie.id_pli 
								WHERE  pli_cloture = 1 and view_dmt_saisie.login_trace is not null  
								$where_date_courrier_ttt $where_other								
								$where_saisie
								GROUP BY view_pli_stat.id_pli,login ,id_user				
								) as r
								GROUP BY id_pli
								

						) as data_saisie on datas.id_pli = data_saisie.id_pli
						left join 
						(
							SELECT	
								id_pli,
								SUM(duree) as duree_controle									
							FROM
								(SELECT distinct 
								view_pli_stat.id_pli,	
								view_dmt_controle.login_trace as login,
								view_dmt_controle.id_utilisateur as id_user,
								round(sum(view_dmt_controle.duree_en_h)::numeric,3) as duree
								FROM view_pli_stat 
								INNER JOIN view_dmt_controle on view_pli_stat.id_pli = view_dmt_controle.id_pli
								WHERE  pli_cloture = 1 and view_dmt_controle.login_trace is not null  
								$where_date_courrier_ttt $where_other								
								$where_controle
								GROUP BY view_pli_stat.id_pli,login ,id_user				
								) as r
								GROUP BY id_pli								
							
							
						) as data_controle on datas.id_pli = data_controle.id_pli
						ORDER BY id_pli ASC
					 ) as data_dmt on data_plis.id_pli = data_dmt.id_pli 
					ORDER by data_plis.dt_event,data_plis.id_pli,mode_paiement ASC
				";
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";
				*/

		return $this->ged->query($sql)
							->result_array();
    }
	
}
