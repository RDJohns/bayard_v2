<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_manip_db extends CI_Model{

    private $CI;

	private $ged;
    private $base_64;
    private $push;

    private $tb_pli = TB_pli;
    private $tb_cheque = TB_cheque;
    private $tb_data_pli = TB_data_pli;
    private $tb_paiement = TB_paiement;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_paieChqKd = TB_paieChqKd;
    private $tb_paieEsp = TB_paieEsp;
    private $tb_motifConsigne = TB_motifConsigne;
    private $tb_paieChqAnom = TB_paieChqAnom;
    private $tb_saisiCtrlField = TB_saisiCtrlField;
    private $tb_histo = TB_histo;
    private $tb_trace = TB_trace;
    private $tb_lotSaisie = TB_lotSaisie;
    private $tb_groupePli = TB_groupe_pli;
    private $tb_fichierKe = TB_fichierKe;
    private $tb_dataKe = TB_dataKe;
    private $tb_cba = TB_cba;
    private $tb_chequeAnoCorrige = TB_chequeAnoCorrige;
    private $tb_regroupement = TB_regroupement;
    private $tb_rejetCbaFl = TB_rejetCbaFl;
    private $tb_rejetCbaLn = TB_rejetCbaLn;
    private $tb_decoup = TB_decoupage;
    private $tb_lotNum = TB_lotNumerisation;
    private $tb_doc = TB_document;
    private $tb_act = TB_action;
    private $actions_cr = array();
    private $actions_fx = array();
    
    private $tb_flux = TB_flux;
    private $tb_histoFx = TB_histoFx;
    private $tb_abonmnt = TB_abonmnt;
    private $tb_anomaFx = TB_anomalieFlux;
    private $tb_motifConsigneFx = TB_motifConsigneFx;
    private $tb_lotSaisieFlux = TB_lotSaisieFlux;

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
        $this->actions_cr = $this->id_actions(1, 0);
        $this->actions_fx = $this->id_actions(0, 1);
    }

    private function id_actions($cr=0, $fx=0){
        $data = $this->ged
            ->select('id_action')
            ->where('courrier', $cr)
            ->where('flux', $fx)
            ->from($this->tb_act)
            ->get()->result();
        $rep = array();
        foreach ($data as $key => $value) {
            array_push($rep, $value->id_action);
        }
        return $rep;
    }

    public function reinit_courrier_all(){
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        try {
            $this->base_64
                ->set('flag_traitement', 0)
                ->set('statut_saisie', 1)
                ->set('recommande', 0)
                ->set('typologie', 0)
                ->set('id_groupe_pli', NULL)
                ->set('typage_par', NULL)
                ->set('saisie_par', NULL)
                ->set('traite_par', 0)
                ->set('id_lot_saisie', NULL)
                ->set('id_decoupage', NULL)
                ->set('flag_echantillon', 0)
                ->set('lot_saisie_bis', 0)
                ->set('controle_par', NULL)
                ->set('retraite_par', NULL)
                ->set('sorti_saisie', NULL)
                ->set('in_decoup', 0)
                ->set('date_tri', NULL)
                ->set('date_tlmc', NULL)
                ->set('date_typage', NULL)
                ->set('date_saisie', NULL)
                ->set('date_controle', NULL)
                ->set('date_termine', NULL)
                ->set('date_reb', NULL)
                ->set('flag_suppr_def_db', 0)
                ->set('dt_suppr_def_db', NULL)
                ->update($this->tb_pli);
            $this->ged
                ->set('statut_saisie', 1)
                ->set('motif_rejet', NULL)
                ->set('description_rejet', NULL)
                ->set('motif_ko', NULL)
                ->set('autre_motif_ko', '')
                ->set('nom_circulaire', NULL)
                ->set('fichier_circulaire', '')
                ->set('message_indechiffrable', NULL)
                ->set('dmd_kdo_fidelite_prim_suppl', NULL)
                ->set('dmd_envoi_kdo_adrss_diff', NULL)
                ->set('type_coupon', '')
                ->set('id_erreur', NULL)
                ->set('comm_erreur', '')
                ->set('nom_orig_fichier_circulaire', '')
                ->set('flag_solde_cloture', 0)
                ->set('nom_deleg', '')
                ->set('flag_ci', 0)
                ->set('ke_prio', 0)
                ->set('mvt_nbr', 0)
                ->set('avec_chq', 0)
                ->set('flag_traitement', 0)
                ->set('flag_batch', 0)
                ->set('code_ecole_gci', '')
                ->set('flag_rejet_batch', 0)
                ->set('date_envoi_ci', NULL)
                ->set('motif_ci', NULL)
                ->set('flag_saisie', 0)
                ->set('dt_batch', NULL)
                ->set('flag_retour_batch', 0)
                ->set('flag_typage_auto', 0)
                ->set('pli_nouveau', 1)
                ->set('par_ttmt_ko', 0)
                ->update($this->tb_data_pli);
            $this->ged->empty_table($this->tb_paiement);
            $this->ged->empty_table($this->tb_paieEsp);
            $this->ged->empty_table($this->tb_mvmnt);
            $this->ged->empty_table($this->tb_cheque);
            $this->ged->empty_table($this->tb_paieChqKd);
            $this->ged->empty_table($this->tb_motifConsigne);
            $this->ged->empty_table($this->tb_paieChqAnom);
            $this->ged->empty_table($this->tb_saisiCtrlField);
            $this->ged->empty_table($this->tb_histo);
            $this->ged->where_in('id_action', $this->actions_cr)->delete($this->tb_trace);
            $this->base_64->empty_table($this->tb_decoup);
            $this->ged->empty_table($this->tb_lotSaisie);
            $this->ged->empty_table($this->tb_groupePli);
            $this->ged->empty_table($this->tb_regroupement);
            $this->ged->empty_table($this->tb_fichierKe);
            $this->ged->empty_table($this->tb_chequeAnoCorrige);
            $this->ged->empty_table($this->tb_dataKe);
            $this->ged->empty_table($this->tb_cba);
            $this->ged->empty_table($this->tb_rejetCbaFl);
            $this->ged->empty_table($this->tb_rejetCbaLn);
        } catch (Exception $exc) {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> model_manip_db> reinit_courrier_all> reinit all courrier failed: '.$exc->getMessage());
            return FALSE;
        }
        if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> model_manip_db> reinit_courrier_all> reinit all courrier failed');
            return FALSE;
        }else {
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            log_message('error', 'models> model_manip_db> reinit_courrier_all> reinit all courrier succes');
            return TRUE;
        }
    }

    public function reinit_flux_all(){
        $this->push->trans_begin();
        $this->ged->trans_begin();
        try {
            $this->push
            ->set(array(
                'flag_anomalie_pj' => 0
                ,'flag_hors_perimetre' => 0
                ,'flag_traitement_niveau' => 0
                ,'flag_important' => 0
                ,'flag_retraitement' => 0
                ,'statut_pli' => 0
                ,'typologie' => 0
                ,'nb_abonnement' => NULL
                ,'type_par' => NULL
                ,'id_motif_blocage' => NULL
                ,'description_motif' => ''
                ,'saisie_par' => NULL
                ,'nb_retraitement' => 0
                ,'id_lot_saisie_flux' => NULL
                ,'lot_saisie_bis' => 0
                ,'date_lot_saisie' => NULL
                ,'motif_escalade' => ''
                ,'motif_transfert' => ''
                ,'dt_quick_save'=> NULL
                ,'mail_reception_typage' => 0
                ,'dt_traitement' => NULL
                ,'sftp_envoi_mail_traitement' => 0
                ,'decoupe_par' => 0
                ,'demande_decoupe_par' => NULL
                ,'motif_ks' => ''
                ,'id_flux_parent' => NULL
                ,'etat_pli_id' => 1
                ,'flag_suppr_def_db_fx' => 0
                ,'dt_suppr_def_db_fx' => NULL
            ))->update($this->tb_flux);
            $this->push->empty_table($this->tb_histoFx);
            $this->ged->where_in('id_action', $this->actions_fx)->delete($this->tb_trace);
            $this->push->empty_table($this->tb_abonmnt);
            $this->push->empty_table($this->tb_anomaFx);
            $this->push->empty_table($this->tb_motifConsigneFx);
            $this->push->empty_table($this->tb_lotSaisieFlux);
        } catch (Exception $exc) {
            $this->push->trans_rollback();
            $this->ged->trans_rollback();
            log_message('error', 'models> model_manip_db> reinit_flux_all> reinit all flux failed: '.$exc->getMessage());
            return FALSE;
        }
        if($this->push->trans_status() === FALSE || $this->ged->trans_status() === FALSE){
            $this->push->trans_rollback();
            $this->ged->trans_rollback();
            log_message('error', 'models> model_manip_db> reinit_flux_all> reinit all flux failed');
            return FALSE;
        }else {
            $this->push->trans_commit();
            $this->ged->trans_commit();
            log_message('error', 'models> model_manip_db> reinit_flux_all> reinit all flux succes');
            return TRUE;
        }
    }

    public function delete_courrier_all()
    {
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        try {
            $this->base_64->empty_table($this->tb_pli);
            $this->base_64->empty_table($this->tb_decoup);
            $this->base_64->empty_table($this->tb_lotNum);
            $this->base_64->empty_table($this->tb_doc);
            $this->ged->empty_table($this->tb_data_pli);
            $this->ged->empty_table($this->tb_paiement);
            $this->ged->empty_table($this->tb_paieEsp);
            $this->ged->empty_table($this->tb_mvmnt);
            $this->ged->empty_table($this->tb_cheque);
            $this->ged->empty_table($this->tb_paieChqKd);
            $this->ged->empty_table($this->tb_motifConsigne);
            $this->ged->empty_table($this->tb_paieChqAnom);
            $this->ged->empty_table($this->tb_saisiCtrlField);
            $this->ged->empty_table($this->tb_histo);
            $this->ged->where_in('id_action', $this->actions_cr)->delete($this->tb_trace);
            $this->ged->empty_table($this->tb_lotSaisie);
            $this->ged->empty_table($this->tb_groupePli);
            $this->ged->empty_table($this->tb_fichierKe);
            $this->ged->empty_table($this->tb_chequeAnoCorrige);
            $this->ged->empty_table($this->tb_dataKe);
            $this->ged->empty_table($this->tb_cba);
            $this->ged->empty_table($this->tb_regroupement);
            $this->ged->empty_table($this->tb_rejetCbaFl);
            $this->ged->empty_table($this->tb_rejetCbaLn);
        } catch (Exception $exc) {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> model_manip_db> delete_courrier_all> suppression all courrier failed: '.$exc->getMessage());
            return FALSE;
        }
        if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> model_manip_db> delete_courrier_all> suppression all courrier failed');
            return FALSE;
        }else {
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            log_message('error', 'models> model_manip_db> delete_courrier_all> suppression all courrier succes');
            return TRUE;
        }
    }
    
    public function delete_flux_all(){
        $this->push->trans_begin();
        $this->ged->trans_begin();
        try {
            $this->push->empty_table($this->tb_flux);
            $this->push->empty_table($this->tb_histoFx);
            $this->ged->where_in('id_action', $this->actions_fx)->delete($this->tb_trace);
            $this->push->empty_table($this->tb_abonmnt);
            $this->push->empty_table($this->tb_anomaFx);
            $this->push->empty_table($this->tb_motifConsigneFx);
            $this->push->empty_table($this->tb_lotSaisieFlux);
        } catch (Exception $exc) {
            $this->push->trans_rollback();
            $this->ged->trans_rollback();
            log_message('error', 'models> model_manip_db> delete_flux_all> suppression all flux failed: '.$exc->getMessage());
            return FALSE;
        }
        if($this->push->trans_status() === FALSE || $this->ged->trans_status() === FALSE){
            $this->push->trans_rollback();
            $this->ged->trans_rollback();
            log_message('error', 'models> model_manip_db> delete_flux_all> suppression all flux failed');
            return FALSE;
        }else {
            $this->push->trans_commit();
            $this->ged->trans_commit();
            log_message('error', 'models> model_manip_db> delete_flux_all> suppression all flux succes');
            return TRUE;
        }
    }

    
    public function delete_courrier($tab_id_pli=array())
    {
        if(!is_array($tab_id_pli) || count($tab_id_pli) < 1){
            log_message('error', 'models> model_manip_db> delete_courrier> (param invalide)suppression courrier failed');
            return FALSE;
        }
        $this->ged->trans_begin();
        $this->base_64->trans_begin();
        try {
            $this->base_64->where_in('id_pli', $tab_id_pli)->delete($this->tb_pli);
            $this->base_64->where_in('id_pli', $tab_id_pli)->delete($this->tb_doc);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_data_pli);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_paiement);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_paieEsp);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_mvmnt);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_cheque);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_paieChqKd);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_motifConsigne);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_paieChqAnom);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_saisiCtrlField);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_histo);
            $this->ged->where_in('id_pli', $tab_id_pli)->where_in('id_action', $this->actions_cr)->delete($this->tb_trace);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_fichierKe);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_chequeAnoCorrige);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_dataKe);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_cba);
            $this->ged->where_in('id_pli', $tab_id_pli)->delete($this->tb_rejetCbaLn);
        } catch (Exception $exc) {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> model_manip_db> delete_courrier> suppression courrier failed: '.$exc->getMessage());
            return FALSE;
        }
        if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE){
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();
            log_message('error', 'models> model_manip_db> delete_courrier> suppression courrier failed');
            return FALSE;
        }else {
            $this->ged->trans_commit();
            $this->base_64->trans_commit();
            log_message('error', 'models> model_manip_db> delete_courrier> suppression courrier succes');
            return TRUE;
        }
    }
    
    public function delete_flux($tab_id_flux=array()){
        if(!is_array($tab_id_flux) || count($tab_id_flux) < 1){
            log_message('error', 'models> model_manip_db> delete_flux> (param invalide)suppression flux failed');
            return FALSE;
        }
        $this->push->trans_begin();
        $this->ged->trans_begin();
        try {
            $this->push->where_in('id_flux', $tab_id_flux)->delete($this->tb_flux);
            $this->push->where_in('id_flux', $tab_id_flux)->delete($this->tb_histoFx);
            $this->ged->where_in('id_pli', $tab_id_flux)->where_in('id_action', $this->actions_fx)->delete($this->tb_trace);
            $this->push->where_in('id_flux', $tab_id_flux)->delete($this->tb_abonmnt);
            $this->push->where_in('flux_id', $tab_id_flux)->delete($this->tb_anomaFx);
            $this->push->where_in('id_flux', $tab_id_flux)->delete($this->tb_motifConsigneFx);
        } catch (Exception $exc) {
            $this->push->trans_rollback();
            $this->ged->trans_rollback();
            log_message('error', 'models> model_manip_db> delete_flux> suppression flux failed: '.$exc->getMessage());
            return FALSE;
        }
        if($this->push->trans_status() === FALSE || $this->ged->trans_status() === FALSE){
            $this->push->trans_rollback();
            $this->ged->trans_rollback();
            log_message('error', 'models> model_manip_db> delete_flux> suppression flux failed');
            return FALSE;
        }else {
            $this->push->trans_commit();
            $this->ged->trans_commit();
            log_message('error', 'models> model_manip_db> delete_flux> suppression flux succes');
            return TRUE;
        }
    }

}