<?php

class Model_koTypage extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;

    private $TBdataPli        = "data_pli";
    private $TBpli            = "pli";
    
    

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function mettrePliKO($pli,$dataPli,$idPli,$motifConsigne=null) 
    {
        /*
        2694526
        data_pli
        pli
        histo_pli
        trace
        
        
         */

        $this->ged->trans_begin();
        $this->base_64->trans_begin();

        $this->ged->where('id_pli', $idPli);
        $this->ged->update($this->TBdataPli, $dataPli);

        $this->base_64->where('id_pli', $idPli);
        $this->base_64->update($this->TBpli, $pli);

        $sqlDelete          = "DELETE FROM public.motif_ko_scan where pli_id=".$idPli.";";
        $link_motif_consigne = $idPli.date('YmdHis');
        $this->ged->query($sqlDelete);

        if($motifConsigne != null && is_array($motifConsigne))
        {
            $motifConsigne["link_histo"] =  $link_motif_consigne;
            $this->ged->insert('motif_consigne',$motifConsigne);
        }
        

        if ($this->base_64->trans_status() === FALSE || $this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            $this->base_64->trans_rollback();

            return 0;
        }
        else
        {
            $this->base_64->trans_commit();
            $this->ged->trans_commit();
            $this->CI->histo->pli((int)$idPli,"Typage",$link_motif_consigne);

            $statutSaisie =  $dataPli["statut_saisie"];
            switch($statutSaisie)
            {
                case 3:
                    $this->CI->histo->action(105, '', (int)$idPli);
                break;

                case 2: 
                    $this->CI->histo->action(104, '', (int)$idPli);
                break;

                case 4: 
                    $this->CI->histo->action(106, '', (int)$idPli);
                break;

                case 6: 
                    $this->CI->histo->action(107, '', (int)$idPli);
                break;

            }
            return 1;
        }
    }

   

}


