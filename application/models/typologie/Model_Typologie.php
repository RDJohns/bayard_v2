<?php

class Model_Typologie extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;

    private $tbTypologie            = "typologie";
    private $tbAnomalieCheque       = "anomalie_cheque";
    private $listeAnomalieChq       = "liste_anomalie_cheque";
    private $tbTitre                = "titres";
    private $societe                = "societe";
    private $TbPli                  = "pli";
    private $viewPliNonType         = "view_pli_non_type";
    private $document               = "document";
    private $motifKoScan            = "motif_ko_scan";
    private $paiement               = "paiement";
    private $mouvement              = "mouvement";
    private $paiementCheque         = "paiement_cheque";
    private $dataPli                = "data_pli";
    private $histoPli               = "histo_pli";
    private $preDataCheque          = "view_pre_data_cheque";
    private $provenance             = "provenance";
    public  $existeDataPli           ="OK";
    private $paiementChqAnomalie    = "paiement_chq_anomalie";
    private $etatChqParIdPli        = "etat_cheque_par_id_pli";

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE); 

        $this->load->library('portier');
        $this->portier->must_op();
    }

   public function litesTypologies($idSco)
    {
        $sql = "SELECT  *FROM public.view_typologie_avec_provenance where id_soc = ".$idSco." order by typologie";
        $query =  $this->ged->query($sql);
        return $query->result();
    }

    public function listeAnomalieCheque()
    {
        return $this->ged
            ->select("id,priorite,anomalie")
            ->from($this->listeAnomalieChq)
            /*->where('flag',1)*/
            
            ->get()->result();
    }
    public function listeTitre($idSoiciete)
    {
        return $this->ged
            ->select('*')
            ->from($this->tbTitre)
            ->where('actif',1)
            ->where('societe_id',(int)$idSoiciete)
            ->order_by("titre_i asc")
            ->order_by("titre asc")
            ->get()->result();
    }

    public function societe()
    {
        return $this->ged
            ->select('*')
            ->from($this->societe)
            ->where('actif',1)
            ->order_by("societe asc")
            ->get()->result();
    }

    public function existeDansDataPli($idPli)
    {
        $row  =  $this->ged 
                        ->select('*')
                        ->from($this->dataPli)
                        ->where('id_pli',$idPli)->get()->result();
        
        $tmpIDpli = 0;
                  
        if($row)
        {
            $tmpIDpli = (int)$row[0]->id_pli;
        }
        
        $this->existeDataPli = "OK";
        if($tmpIDpli < 1  )
        {
            
            $sqlDataPli = "DELETE FROM public.data_pli WHERE id_pli = ".$idPli.";";
            $sqlDataPli .= "INSERT INTO public.data_pli(id_pli, societe) SELECT  id_pli,societe FROM public.f_pli where f_pli.id_pli = ".$idPli." limit 1";
            
            log_message('error', $this->session->userdata('infouser').' Mtypologie>existeDansDataPli erreur #'.(int)$idPli);
            
            $query      = $this->ged->query($sqlDataPli);

            $arrayRes = $this->checkDB('id_pli',(int)$idPli,"data_pli",$this->ged);
            
            $idPliTemp = (int)$arrayRes[0]->id_pli;
            
            if($idPliTemp != $idPli)
            {
                log_message('error', $this->session->userdata('infouser').' Mtypologie>existeDansDataPli KO erreur #'.(int)$idPli);
               $this->existeDataPli = "KO";
            }
            else
            {
                $this->existeDataPli = "OK";
                log_message('error', $this->session->userdata('infouser').' Mtypologie>existeDansDataPli OK erreur #'.(int)$idPli);
            }
            
        }
       
    }
    /**
     *
     */
    public function getPli($societe = 10)
    {
        $arrayRet = array();
       // $this->session->set_userdata('choix_societe', 'some_value');

       

        $encours  = 1;$nbRowPli = 0;$updateID = 0;$idPli = 0;
        /*ec: 116184 */
        $encours  =  $this->base_64 /*misy pli anazy ao*/
                        ->select('*')
                        ->from($this->TbPli)
                        ->where('flag_traitement',1)
                        ->where('typage_par',(int)$this->session->userdata('id_utilisateur'))
                        ->where('recommande >=',0,false)
                        ->get()->result();


        if($encours)
        {
            $idPli          = $encours[0]->id_pli;
        }
        
        $encoursPli     = (int)$idPli;

        $arrayRet["retour"] = "ko";
        $arrayRet["id_pli"] = 0;

        $choixSociete = (int)$societe;
        
        $clauseWhere  = "";

        if($societe != 10 )
        {
            $this->session->set_userdata('choix_societe',0);
        }
        else
        {
            $choixSociete = intval($this->session->userdata('choix_societe'));
        }

        if($choixSociete > 0 && $choixSociete < 3 )
        {
               
                $clauseWhere = "  and f_data_pli.societe =".(int)$choixSociete."  ";
                $this->session->set_userdata('choix_societe',(int)$choixSociete);
                $this->annulerTypage($idPli);
                $encoursPli = 0;
        }

        if((int)$encoursPli > 0)
        {
            $arrayRet["retour"] = "encours_existe";
            $arrayRet["id_pli"] = 0;
        }
        else
        {
            

           /* $sql            = "UPDATE public.pli 
                                    SET  flag_traitement= 1, 
                                    typage_par= ".intval($this->session->userdata('id_utilisateur'))."
                                    WHERE id_pli = ( SELECT DISTINCT id_pli
                                    FROM
                                    (
                                        select id_pli from(SELECT pli.id_pli
                                    FROM PLI
                                    INNER JOIN f_data_pli ON f_data_pli.id_pli = pli.id_pli
                                    JOIN lot_numerisation ON lot_numerisation.id_lot_numerisation = pli.id_lot_numerisation
                                    WHERE pli.flag_traitement = 0
                                        AND f_data_pli.flag_traitement = 0
                                        AND pli.recommande <> '-1'::integer
                                        AND f_data_pli.flag_batch = 0
                                        ".$clauseWhere." 
                                    ORDER BY pli.recommande DESC,lot_numerisation.date_courrier ASC limit 1 ) test_rdm
                                     limit 1 
                                    ) as limit_res)  and  coalesce(flag_traitement,0) = 0 ";*/
                                    
            $clauseWhere  = "";
            $this->session->set_userdata('choix_societe',0);
            if($choixSociete > 0 && $choixSociete < 3 )
            {
                    $clauseWhere = "  and societe =".(int)$choixSociete."  ";
                    $this->session->set_userdata('choix_societe',(int)$choixSociete);
                    $this->annulerTypage($idPli);
                    $encoursPli = 0;
            }

            $sql            = "UPDATE public.pli 
                                    SET  flag_traitement= 1, 
                                    typage_par=".(int)$this->session->userdata('id_utilisateur')." 
                                    WHERE id_pli = (
                                      SELECT id_pli
                                      FROM (SELECT DISTINCT pli.id_pli,
                                      pli.date_courrier,pli.recommande
                              FROM public.view_pli_nouveau pli 
                              INNER JOIN document ON document.id_pli = pli.id_pli
                              WHERE pli.recommande <> '-1'::integer and flag_traitement = 0  ".$clauseWhere."
                              ORDER BY pli.recommande desc,pli.date_courrier asc limit 1) AS res)";

                                    //  echo "<pre>".$sql."</pre>"; 
            $clauseWhere = "";                        

            $query          = $this->base_64->query($sql);

            $checkUpdate    = $this->getPliID();
                                              
            $nbRowPli       = count($checkUpdate);

            if($nbRowPli > 0)
            {
                $updateID = $checkUpdate[0]->id_pli;
               /* $this->existeDansDataPli($updateID);
                if($this->existeDataPli == "KO")
                {
                    $arrayRet["retour"] = "encours_existe";
                    $arrayRet["id_pli"] = 0;
                    return $arrayRet;
                }*/

            }


            if (intval($updateID) > 0)
            {
                
                $sqlUpdateDataPli = "UPDATE public.data_pli SET flag_traitement= 1, par_ttmt_ko = 0, pli_nouveau = 0, statut_saisie = 1 WHERE id_pli =".$checkUpdate[0]->id_pli;
                
                $this->ged->query($sqlUpdateDataPli);
                
                $arrayRet["retour"] = "ok";
                $arrayRet["id_pli"] = $updateID;
                $this->CI->histo->pli((int)$checkUpdate[0]->id_pli);
                $this->CI->histo->action(3, '', (int)$checkUpdate[0]->id_pli);
            }
            else
            {
                $arrayRet["retour"] = "pas_pli";
                $arrayRet["id_pli"] = 0;
            }
        }

        return $arrayRet;
    }
    public function recuperPliUtilisateur($pliID)
    {
        return $this->base_64
            ->select('*')
            ->from($this->viewPliNonType)
            ->where('id_pli',(int)$pliID)
            ->order_by("id_document asc")
            ->get()->result();
    }

    public function getPliID()
    {
        return $this->base_64
            ->select('*')
            ->from($this->TbPli)
            ->where('typage_par',(int)$this->session->userdata('id_utilisateur'))
            ->where('flag_traitement',1)
            ->where('recommande >=',0,false)
            ->get()->result();
    }
    public function saisieEncours()
    {
        return $this->base_64
            ->select('*')
            ->from($this->TbPli)
            ->where('flag_traitement',4)
            ->where('saisie_par',(int)$this->session->userdata('id_utilisateur'))
            ->get()->result();
    }
    public function getDocID($id)
    {
        log_message('error', $this->session->userdata('infouser').' getDocID #'.(int)$id);
        return $this->base_64
            ->select('*')
            ->from($this->document)
            ->where('id_document',(int)$id)
            ->get()->result();
    }

    public function annulerTypage($idPli)
    {
        $arrayRet           = array();
        $arrayRet["update"] = "0";
        $nbAnnuler          = 0;
        
        $this->ged->trans_begin();
        $this->base_64->trans_begin();

        $pli                = array();
        $dataPli            = array("statut_saisie"=>1,"flag_traitement"=> 0);
        
        $sql = "UPDATE public.pli 
                      SET  flag_traitement= 0, typologie = 0,typage_par = null, statut_saisie = 1 
                      WHERE typage_par = ".(int)$this->session->userdata('id_utilisateur')." AND flag_traitement = 1  AND recommande >= 0 ";

        /***
         * https://kb.objectrocket.com/postgresql/combine-tables-in-postgres-with-update-to-join-columns-1145
        */
        $sqlDataPli =  " 
        UPDATE data_pli
        SET flag_traitement = 0,typologie = 0, statut_saisie = 1
        FROM f_pli
            WHERE data_pli.id_pli = f_pli.id_pli 
            and f_pli.recommande >= 0
            and f_pli.typage_par =  ".intval($this->session->userdata('id_utilisateur'))."
            and f_pli.flag_traitement = 1";

        $this->ged->query($sqlDataPli);
        $this->base_64->query($sql);
      

        if($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE)
        {
            $this->base_64->trans_rollback();
            $this->ged->trans_rollback();

            log_message('error', $this->session->userdata('infouser').' Mtypologie>annulerTypage erreur #'.(int)$idPli);
            return array("update"=>0);
        }
        else 
        {
            $this->base_64->trans_commit();
            $this->ged->trans_commit();
            
            $this->CI->histo->pli((int)$idPli);
            $this->CI->histo->action(39, "", (int)$idPli);
            return array("update"=>1);
        }
       

       
    }

    public function koScan($arrayPost, $motifConsigne,$adataPli = null)
    {
        $arrayRet           = array();
        $arrayRet["update"] = "0";
        $updateID           = 0;
        $nbCheckKoScan      = 0;
        $idPli              = $arrayPost["pli_id"];
        $nbRejet            = 0;
        $link_motif_consigne = "";

        $sqlDelete          = "DELETE FROM public.motif_ko_scan where pli_id=".$idPli.";";
        $this->ged->query($sqlDelete);

        $nbRejet            =  $this->ged->select('*')->from($this->motifKoScan)->where('pli_id',(int)$idPli)->get()->num_rows();
        if($nbRejet > 0)
        {
            log_message('error', $this->session->userdata('infouser').' Mtypologie>koScan erreur lors de la suppresion #'.(int)$idPli);
            return $arrayRet;
        }
        else
        {   
            $this->ged->insert($this->motifKoScan,$arrayPost);
            $id     = $this->ged->insert_id();
            if($id <= 0)
            {
                log_message('error', $this->session->userdata('infouser').' Mtypologie>koScan erreur lors de l insertion #'.(int)$idPli);
                return $arrayRet;
            }
           
        }

        $datyNow = date('Y-m-d H:i:s');
        $sql = "UPDATE public.pli 
                      SET  flag_traitement= 2,statut_saisie = 2
                      WHERE typage_par = ".(int)$this->session->userdata('id_utilisateur')." AND flag_traitement = 1 AND recommande >= 0";

        $this->base_64->query($sql);

        $checkKoScan    = $this->checkKoScan($idPli);
        $nbCheckKoScan  = count($checkKoScan);

        if($nbCheckKoScan > 0 )
        {
            $link_motif_consigne = $idPli.date('YmdHis');
            $motifConsigne["link_histo"] =  $link_motif_consigne;

            $updateID   = (int)$checkKoScan[0]->id_pli;
            $sqlUpdate = " update data_pli SET  pli_nouveau = 0, par_ttmt_ko = 0, flag_traitement= 2,statut_saisie = 2, date_dernier_statut_ko ='".$datyNow."' WHERE id_pli =".$idPli ;
            $this->ged->query($sqlUpdate);
            $this->ged->insert("motif_consigne",$motifConsigne);
        }
        if ($updateID > 0)
        {
            
            $arrayRet["update"] = "1";

           /* $sqlU = "UPDATE public.data_pli 
            SET  statut_saisie = 2,flag_traitement = 2
            WHERE id_pli = ".(int)$idPli;

            $this->ged->query($sqlU);*/

            $this->ged->where('id_pli', $idPli);
            $this->ged->update($this->dataPli, $adataPli);

            $this->CI->histo->pli($idPli,"Typage",$link_motif_consigne);
            $this->CI->histo->action(104, '', $idPli);

            $this->ged->delete($this->paiement, array('id_pli' => (int)$idPli));
            $this->ged->delete($this->mouvement, array('id_pli' => (int)$idPli));
            $this->ged->delete($this->paiementCheque, array('id_pli' => (int)$idPli));
            
        }
        else
        {
            return $arrayRet;
        }

        return $arrayRet;
    }
    public function checkKoScan($idPli)
    {
        return $this->base_64
            ->select('*')
            ->from($this->TbPli)
            ->where('id_pli',(int)$idPli)
            ->where('flag_traitement',2)
            ->where('statut_saisie',2)
            ->get()->result();
    }

    public function hpTypage($idPli)
    {
        $arrayRet           = array();
        $arrayRet["update"] = "0";
        $nbAnnuler          = 0;

        $sql = "UPDATE public.pli 
                      SET  flag_traitement= 21, typologie = 0,typage_par = null 
                      WHERE typage_par = ".(int)$this->session->userdata('id_utilisateur')." AND flag_traitement = 1 AND recommande >= 0 ";
        $this->base_64->query($sql);

        $nbAnnuler  =  $this->base_64->select('*')->from($this->TbPli)->where('id_pli',(int)$idPli)->get()->num_rows();

        if($nbAnnuler <= 0)
        {
            log_message('error', $this->session->userdata('infouser').' Mtypologie>hpTypage erreur hors perimetre #'.(int)$idPli);
            return $arrayRet;
        }
        
        $this->ged->delete($this->paiement, array('id_pli' => (int)$idPli));
        $this->ged->delete($this->mouvement, array('id_pli' => (int)$idPli));
        $this->ged->delete($this->paiementCheque, array('id_pli' => (int)$idPli));
       
        $sqlU = "UPDATE public.data_pli SET  statut_saisie = 2 WHERE id_pli = ".(int)$idPli;

        $this->ged->query($sqlU);

        $arrayRet["update"] = "1";
        $this->CI->histo->pli((int)$idPli);
        $this->CI->histo->action(41, '', (int)$idPli);
        return $arrayRet;
    }

    public function paiement($data)
    {
        $this->ged->delete($this->paiement, array('id_pli' => (int)$data[0]["id_pli"]));
        $this->ged->insert_batch($this->paiement, $data);
        $arrayRes = $this->checkDB('id_pli',(int)$data[0]["id_pli"],"paiement",$this->ged);
        return (int)$arrayRes[0]->id_pli;
    }


    public function mouvement($data)
    {
        $this->ged->delete($this->mouvement, array('id_pli' => (int)$data[0]["id_pli"]));
        $this->ged->insert_batch($this->mouvement, $data);
        $arrayRes = $this->checkDB('id_pli',(int)$data[0]["id_pli"],"mouvement",$this->ged);
        return (int)$arrayRes[0]->id_pli;
    }

    public function cheque($data,$idPli)
    {
        $this->ged->delete($this->paiementCheque, array('id_pli' => (int)$idPli));
        $this->ged->insert_batch($this->paiementCheque, $data);
        $arrayResCheque = $this->checkDB('id_pli',(int)$idPli,$this->paiementCheque,$this->ged);
        return $arrayResCheque[0]->id;
    }
    
    public function getDescriptionAnomalieChq($idAnomalie)
    {
         return $this->ged
            ->select('STRING_AGG( anomalie,\'; \') as anomalie',FALSE)
            ->from($this->tbAnomalieCheque)
            ->where_in('id',$idAnomalie)
            ->get()->result();
    }
    public function dataPli($idPli, $dataPli,$pli,$listeAnomalieChq = null)
    {
      
        $link_motif_consigne = "";

        $arrayCheck = $this->checkDB('id_pli',(int)$idPli,$this->TbPli,$this->base_64);
        if((int)$arrayCheck[0]->typage_par == (int)$this->session->userdata('id_utilisateur') && (int)$arrayCheck[0]->flag_traitement == 1)  /*en-cours-ny*/
        {
            if(intval($pli["statut_saisie"]) > 1)
            {
                $link_motif_consigne = $idPli.date('YmdHis');
                $motifChqOperateur   = "Typage : anomalie chèque";
                if($listeAnomalieChq)
                {
                    $mofifAnoChq     = $this->getDescriptionAnomalieChq($listeAnomalieChq);
                    if($mofifAnoChq)
                    {
                        $motifChqOperateur = $mofifAnoChq[0]->anomalie;
                    }
                }
                $motifConsigne["motif_operateur"]   = $motifChqOperateur;
                $motifConsigne["id_pli"]            = $idPli;
                $motifConsigne["id_flg_trtmnt"]     = 2 ;
                $motifConsigne["id_stt_ss"]         =  intval($pli["statut_saisie"]);
                $motifConsigne["link_histo"]        =  $link_motif_consigne;
                $sqlDelete                          = " DELETE FROM public.motif_ko_scan where pli_id=".$idPli.";";
                $this->ged->query($sqlDelete);
                $this->ged->insert("motif_consigne",$motifConsigne);                
            }
            
            $this->base_64->trans_begin();
            $this->ged->trans_begin();

            $this->base_64->where('id_pli', $idPli);
            $this->base_64->update($this->TbPli, $pli);

            $this->ged->where('id_pli', $idPli);
            $this->ged->update($this->dataPli, $dataPli);

            if ($this->ged->trans_status() === FALSE || $this->base_64->trans_status() === FALSE)
            {

                $this->ged->trans_rollback();
                $this->base_64->trans_rollback();
                return 0;
            }
            else
            {
                $this->ged->trans_commit();
                $this->base_64->trans_commit();
            }
            
            $this->CI->histo->pli((int)$idPli,"Typage",$link_motif_consigne);
            $this->CI->histo->action(5, '', (int)$idPli);
            return 1;
        }
        else
        {
            return 2;
        }

    }

    public function checkDB($id,$idValue,$table,$database)
    {
        return $database
            ->select('*')
            ->from($table)
            ->where($id,(int)$idValue)
            ->get()->result();
    }
    public function histoPli($data)
    {
        $this->ged->insert($this->histoPli,$data);
    }

    public function preData($iPli)
    {
        return $this->base_64
            ->select('*')
            ->from($this->preDataCheque)
            ->where('id_pli',(int)$iPli)
            ->get()->result_array();
    }

    public function listeProvenance($idSoiciete)
    {
        return $this->ged
            ->select('*')
            ->from($this->provenance)
            ->where('actif',1)
            ->where('societe_id',(int)$idSoiciete)
            ->order_by("provenance asc")
            ->get()->result();
    }

    public function checkProvenance($typologie,$idSco)
    {
        $sql = "SELECT  *FROM public.view_typologie_avec_provenance where id_soc = ".(int)$idSco." and id_typologie = ".(int)$typologie." order by typologie";
        $query =  $this->ged->query($sql);
        return $query->result();
    }


    public function standBy($idPli)
    {
        return $this->base_64
            ->select('*')
            ->from($this->TbPli)
            ->where('id_pli',(int)$idPli)
            ->where('recommande',-1)
            ->get()->result();
    }
    /*public function insererPaiementChqAnomalie($data)
    {
        $this->ged->delete($this->paiementChqAnomalie, array('id_pli' => intval($data[0]["id_pli"])));
        $this->ged->insert_batch($this->paiementChqAnomalie, $data);
    }*/

    public function anomalieCheque($data,$idPli)
    {
        $this->ged->delete($this->paiementChqAnomalie, array('id_pli' => intval($idPli)));
        $this->ged->insert_batch($this->paiementChqAnomalie, $data);
        $arrayRes = $this->checkDB('id_pli',intval($idPli),$this->paiementChqAnomalie,$this->ged);

        return intval($arrayRes[0]->id_pli);
    }
    
    public function insererEtatCheque($idPli)
    {
        $listeEtatChq = $this->ged->select('*')->from($this->etatChqParIdPli)
                             ->where('id_pli',(int)$idPli)->get()->result_array();
        
        $sql = "";
        for($i = 0 ; $i < count($listeEtatChq); $i++)
        {
            $sql .= " UPDATE public.paiement_cheque SET id_etat_chq =".intval($listeEtatChq[$i]['id_etat_chq'])." WHERE id_pli = ".intval($listeEtatChq[$i]['id_pli'])."  and id_doc = ".intval($listeEtatChq[$i]['id_doc'])."; ";
        }

        $query =  $this->ged->query($sql); 
    }

    
   public function preDataNomPayeur($idPli,$champ)
    {
        //predata.code_promo
        $sql ="
        select *from(
            SELECT pli.id_pli, 
            ".$champ." as champ 
            FROM public.pli
            left join document on document.id_pli = pli.id_pli
            left join predata on predata.id_document = document.id_document
            where pli.id_pli = ".intval($idPli)." ) as pre_d where champ is not null and champ !=''";

        $query =  $this->base_64->query($sql);
        return $query->result(); 
    }


    public function getEtatPliByAnomalieChq($listeAnomalieId)
    {
        $strID = 0;
        
        if(count($listeAnomalieId) > 0)
        {
            $strID = implode(",", $listeAnomalieId); 
        }

        $sql ="
        SELECT etat_pli
        FROM public.etat_pli_par_anomalie_chq
        WHERE priorite IN
            (SELECT max(priorite) prio
            FROM public.etat_pli_par_anomalie_chq
            WHERE id IN(".$strID."))";

        $query =  $this->ged->query($sql);
        return $query->result(); 
    }

}



