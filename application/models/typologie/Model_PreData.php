<?php

class Model_PreData extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;


    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }
    public function numeroCodepromo($idPli)
    {
        $sql = "
        SELECT 
            pli.id_pli,document.id_document,
            predata.abonne_destinataire,
            predata.abonne_payeur,
            predata.code_promo 
            FROM public.pli 
                inner join document   on document.id_pli = pli.id_pli
                inner join predata on predata.id_document = document.id_document
            
            where 
                (
                predata.abonne_destinataire is not null and predata.abonne_destinataire != ''
                or(predata.abonne_payeur is not null and predata.abonne_payeur != '')
                or(predata.code_promo is not null and predata.code_promo != '')
                ) 
                and pli.id_pli = ".intval($idPli)."
                /*order by pli.id_pli*/";
        /*$sqlTest = $sql ." union all ".$sql;*/
        $query =  $this->base_64->query($sql);
        return $query->result();
    }
    /*
    public function getNomPayeur($numero)
    {
        $sql = "SELECT adresses_cba_id, ctm_nbr, concat(atn_1st,' ' ,atn_end) nom   FROM public.f_nom_dans_cba where ctm_nbr = '".$numero."'";
        $query =  $this->base_64->query($sql);
        return $query->result();
    }*/

     // public function numeroCodepromo($idPli)
    // {
        // $sql = "
        // SELECT 
            // pli.id_pli,document.id_document,
            // predata.abonne_destinataire,
            // predata.abonne_payeur,
            // predata.code_promo 
            // FROM public.pli 
                // inner join document   on document.id_pli = pli.id_pli
                // inner join predata on predata.id_document = document.id_document
            
            // where 
                // (
                // predata.abonne_destinataire is not null and predata.abonne_destinataire != ''
                // or(predata.abonne_payeur is not null and predata.abonne_payeur != '')
                // or(predata.code_promo is not null and predata.code_promo != '')
                // ) 
                // and pli.id_pli = ".intval($idPli)."
                // /*order by pli.id_pli*/";
        // $sqlTest = $sql ." union all ".$sql;
        // $query =  $this->base_64->query($sqlTest);
        // return $query->result();
    // }
    
    public function getNomPayeur($numero)
    {
        $sql = "SELECT adresses_cba_id, ctm_nbr, concat(atn_1st,' ' ,atn_end) nom   FROM public.f_nom_dans_cba where ctm_nbr = '".$numero."'";
        $query =  $this->base_64->query($sql);
        return $query->result();
    }
    


}

