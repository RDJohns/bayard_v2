<?php


class Model_Reception_Flux extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;
    private $push;
    private $viewTyplogie = "view_typlogie";

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
    }
    public function pliParTypologie($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlTypologie = "
            select 
              id,
              toute.typologie, 
              case when nb_pli_par_typo is null then 0 else nb_pli_par_typo end as nb_pli_par_typo, 
              case when nb_mvt_par_typo is null then 0 else nb_mvt_par_typo end as nb_mvt_par_typo, 
              case when pourcent_pli is null then 0 else pourcent_pli end as pourcent_pli, 
              case when pourcent_mvt is null then 0 else pourcent_mvt end as pourcent_mvt, 
              case when nb_mvt_total is null then 0 else nb_mvt_total end as nb_mvt_total, 
              case when pli_total is null then 0 else pli_total end as pli_total
    
              from (select typologie.id, typologie.typologie from typologie union select 0, 'Non typé')  toute
              left join (
              SELECT *from public.f_pli_par_typologie('".$date1."','".$date2."',".$societe.")) as par_typo on par_typo.id_typologie = toute.id order by typologie";

            $query = $this->ged->query($sqlTypologie);
        }
        else{
            $sqlTypologie = "
            select 
              id,
              toute.typologie, 
              case when nb_flux_par_typo is null then 0 else nb_flux_par_typo end as nb_flux_par_typo, 
              case when nb_abnmt_par_typo is null then 0 else nb_abnmt_par_typo end as nb_abnmt_par_typo, 
              case when pourcent_flux is null then 0 else pourcent_flux end as pourcent_flux, 
              case when pourcent_abnmt is null then 0 else pourcent_abnmt end as pourcent_abnmt, 
              case when nb_abnmt_total is null then 0 else nb_abnmt_total end as nb_abnmt_total, 
              case when flux_total is null then 0 else flux_total end as flux_total
    
              from (select typologie.id_typologie as id, typologie.typologie from typologie union select 0, 'Non typé')  toute
              left join (
              SELECT *from public.f_flux_par_typologie('".$date1."','".$date2."',".$societe.",".$source.")) as par_typo on par_typo.id_typologie = toute.id order by typologie";

            $query = $this->push->query($sqlTypologie);
        }
        return $query->result();
    }

    public function pliEncours($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlEncours = "SELECT *from public.f_pli_en_cours('".$date1."','".$date2."',".$societe.");";
            $query = $this->ged->query($sqlEncours);
        }
        else{
            $sqlEncours = "SELECT *from public.f_flux_en_cours('".$date1."','".$date2."',".$societe.",".$source.");";
            $query = $this->push->query($sqlEncours);
        }
        return $query->result();
    }
    public function pliCloture($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlEncours = "
            select 
            id,
            toute.typologie, 
            case when nb_pli_par_typo is null then 0 else nb_pli_par_typo end as nb_pli_par_typo, 
            case when nb_mvt_par_typo is null then 0 else nb_mvt_par_typo end as nb_mvt_par_typo, 
            case when pourcent_pli is null then 0 else pourcent_pli end as pourcent_pli, 
            case when pourcent_mvt is null then 0 else pourcent_mvt end as pourcent_mvt, 
            case when nb_mvt_total is null then 0 else nb_mvt_total end as nb_mvt_total, 
            case when pli_total is null then 0 else pli_total end as pli_total
            
            from (select typologie.id, typologie.typologie from typologie union select 0, 'Non typé')  toute
            left join (
            SELECT *from public.f_pli_cloture('".$date1."','".$date2."',".$societe.")) as par_typo on par_typo.id_typologie = toute.id order by typologie
            ";

            $query = $this->ged->query($sqlEncours);
        }
        else{
            $sqlEncours = "
            select 
            id,
            toute.typologie, 
            case when nb_flux_par_typo is null then 0 else nb_flux_par_typo end as nb_flux_par_typo, 
            case when nb_abnmt_par_typo is null then 0 else nb_abnmt_par_typo end as nb_abnmt_par_typo, 
            case when pourcent_flux is null then 0 else pourcent_flux end as pourcent_flux, 
            case when pourcent_abnmt is null then 0 else pourcent_abnmt end as pourcent_abnmt, 
            case when nb_abnmt_total is null then 0 else nb_abnmt_total end as nb_abnmt_total, 
            case when flux_total is null then 0 else flux_total end as flux_total
            
            from (select typologie.id_typologie as id, typologie.typologie from typologie union select 0, 'Non typé')  toute
            left join (
            SELECT *from public.f_flux_cloture('".$date1."','".$date2."',".$societe.",".$source.")) as par_typo on par_typo.id_typologie = toute.id order by typologie
            ";

            $query = $this->push->query($sqlEncours);
        }
        return $query->result();
    }

    public function typologieJournalier($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "SELECT *from public.f_par_typologie_detail('".$date1."','".$date2."',$societe) order by date_courrier";
            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT *from public.f_par_typologie_detail('".$date1."','".$date2."',".$societe.",".$source.") order by date_reception";
            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();


    }

    public function toutesTypologie()
    {
        $sql    = "SELECT id, typologie FROM public.view_typlogie union SELECT 0, 'Non définie';";
        $typo   = array();
        $query  = $this->base_64->query($sql);
        $res    = $query->result();
        foreach($res as $item)
        {
            $typo[$item->id] = $item->typologie;
        }

        return $typo;
    }

    public function nonTraiteEncours($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlTraiteEncours = "
          select 
            total,
            pli_en_cours,
            pli_non_traite,
    
            case when total = 0 or pli_en_cours = 0 then 0 else round(((pli_en_cours*100.00)/(total)),2) end as pourcent_en_cours,
            case when total = 0 or pli_non_traite = 0 then 0 else round(((pli_non_traite*100.00)/(total)),2) end as pourcent_non_traite
            from (
            SELECT societe, nom_societe,sum(pli_en_cours) as pli_en_cours, sum(pli_non_traite) as pli_non_traite
              FROM public.view_pli_stat where societe::integer = ".$societe." and date_courrier::date between '".$date1."' and '".$date2."'
            GROUP BY societe, nom_societe
            ) non_traite
            left join(
            SELECT count(*) total
              FROM public.view_pli_stat where date_courrier::date between '".$date1."' and '".$date2."' and societe::integer = ".$societe.") somme on 1= 1";

            $query = $this->ged->query($sqlTraiteEncours);
        }
        else{
            $sqlTraiteEncours = "
          select 
            total,
            flux_en_cours,
            flux_non_traite,
    
            case when total = 0 or flux_en_cours = 0 then 0 else round(((flux_en_cours*100.00)/(total)),2) end as pourcent_en_cours,
            case when total = 0 or flux_non_traite = 0 then 0 else round(((flux_non_traite*100.00)/(total)),2) end as pourcent_non_traite
            from (
            SELECT societe, nom_societe,sum(flux_en_cours) as flux_en_cours, sum(flux_non_traite) as flux_non_traite
              FROM public.view_stat_flux where societe::integer = ".$societe." and date_reception::date between '".$date1."' and '".$date2."' and id_source::integer = ".$source." 
            GROUP BY societe, nom_societe
            ) non_traite
            left join(
            SELECT count(*) total
              FROM public.view_stat_flux where date_reception::date between '".$date1."' and '".$date2."' and societe::integer = ".$societe." and id_source = ".$source.") somme on 1= 1";

            $query = $this->push->query($sqlTraiteEncours);
        }
        return $query->result();
    }

    public function globaleReception($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sql = "SELECT pli_total,mouvement_total,nb_pli_non_traite,pli_cloture,mvt_cloture,nb_pli_en_cours,mouvement_en_cours,nb_pli_anomalie from public.f_reception_globale('".$date1."','".$date2."','".$societe."');
            ";
            $query = $this->ged->query($sql);
        }
        else{
            $sql = "SELECT flux_total,abonnement_total,nb_flux_non_traite,flux_cloture,abnmt_cloture,nb_flux_en_cours,abonnement_en_cours,nb_flux_anomalie,abnmt_anomalie from public.f_reception_globale('".$date1."','".$date2."','".$societe."','".$source."');
            ";
            $query = $this->push->query($sql);
        }
        return $query->result_array();
    }

    public function fluxAnomalie($date1,$date2,$societe,$source){
        $sql_ano = "SELECT 
                    SUM(flux_anomalie) flux_anomalie,
                    SUM(flux_ko_mail) flux_ko_mail,
                    SUM(flux_ko_pj) flux_ko_pj,
                    SUM(flux_ko_fichier) flux_ko_fichier,
                    SUM(flux_ko_src) flux_ko_src,
                    SUM(flux_ko_def) flux_ko_definitif,
                    SUM(flux_ko_inc) flux_ko_inconnu,
                    SUM(flux_ko_att) flux_ko_attente,
                    SUM(flux_ko_rej) flux_ko_rejete,
                    SUM(flux_ko_hp) flux_ko_hp,
                    SUM(flux_ko_en_cours) flux_ko_en_cours,
                    SUM(ttl_anomalie) total_anomalie,
                    SUM(ttl_ko_mail) total_ko_mail,
                    SUM(ttl_ko_pj) total_ko_pj,
                    SUM(ttl_ko_fichier) total_ko_fichier,
                    SUM(ttl_ko_src) total_ko_src,
                    SUM(ttl_ko_def) total_ko_definitif,
                    SUM(ttl_ko_inc) total_ko_inconnu,
                    SUM(ttl_ko_att) total_ko_attente,
                    SUM(ttl_ko_rej) total_ko_rejete,
                    SUM(ttl_ko_hp) total_ko_hp,
                    SUM(ttl_ko_en_cours) total_ko_en_cours 
	
                    FROM 
                    flux 
                    LEFT JOIN view_anomalie_actuel vaa ON vaa.id_flux = flux.id_flux
                    LEFT JOIN view_anomalie_cumul vac ON vac.id_flux = flux.id_flux  
                    where 
                    flux.societe = ".$societe." and flux.id_source = ".$source." AND date_reception::date  BETWEEN '".$date1."' and '".$date2."' ";

        return $this->push->query($sql_ano)->result();
    }

    public function fluxAnomalieJour($date1,$date2,$societe,$source){
        $sql_ano = "SELECT 
                    date_reception::date date_reception,
                    SUM(flux_anomalie) flux_anomalie,
                    SUM(flux_ko_mail) flux_ko_mail,
                    SUM(flux_ko_pj) flux_ko_pj,
                    SUM(flux_ko_fichier) flux_ko_fichier,
                    SUM(flux_ko_src) flux_ko_src,
                    SUM(flux_ko_def) flux_ko_definitif,
                    SUM(flux_ko_inc) flux_ko_inconnu,
                    SUM(flux_ko_att) flux_ko_attente,
                    SUM(flux_ko_rej) flux_ko_rejete,
                    SUM(flux_ko_hp) flux_ko_hp,
                    SUM(ttl_anomalie) total_anomalie,
                    SUM(ttl_ko_mail) total_ko_mail,
                    SUM(ttl_ko_pj) total_ko_pj,
                    SUM(ttl_ko_fichier) total_ko_fichier,
                    SUM(ttl_ko_src) total_ko_src,
                    SUM(ttl_ko_def) total_ko_definitif,
                    SUM(ttl_ko_inc) total_ko_inconnu,
                    SUM(ttl_ko_att) total_ko_attente,
                    SUM(ttl_ko_rej) total_ko_rejete,
                    SUM(ttl_ko_hp) total_ko_hp
	
                    FROM 
                    flux 
                    LEFT JOIN view_anomalie_actuel vaa ON vaa.id_flux = flux.id_flux
                    LEFT JOIN view_anomalie_cumul vac ON vac.id_flux = flux.id_flux  
                    where 
                    flux.societe = ".$societe." and flux.id_source = ".$source." AND date_reception::date  BETWEEN '".$date1."' and '".$date2."' 
                    GROUP BY date_reception::date ";

        return $this->push->query($sql_ano)->result();
    }

    public function fluxAnomalieSemaine($date1,$date2,$societe,$source){
        $sql_ano = "SELECT 
                    date_part('week',date_reception)::text semaine,
                    SUM(flux_anomalie) flux_anomalie,
                    SUM(flux_ko_mail) flux_ko_mail,
                    SUM(flux_ko_pj) flux_ko_pj,
                    SUM(flux_ko_fichier) flux_ko_fichier,
                    SUM(flux_ko_src) flux_ko_src,
                    SUM(flux_ko_def) flux_ko_definitif,
                    SUM(flux_ko_inc) flux_ko_inconnu,
                    SUM(flux_ko_att) flux_ko_attente,
                    SUM(flux_ko_rej) flux_ko_rejete,
                    SUM(flux_ko_hp) flux_ko_hp,
                    SUM(ttl_anomalie) total_anomalie,
                    SUM(ttl_ko_mail) total_ko_mail,
                    SUM(ttl_ko_pj) total_ko_pj,
                    SUM(ttl_ko_fichier) total_ko_fichier,
                    SUM(ttl_ko_src) total_ko_src,
                    SUM(ttl_ko_def) total_ko_definitif,
                    SUM(ttl_ko_inc) total_ko_inconnu,
                    SUM(ttl_ko_att) total_ko_attente,
                    SUM(ttl_ko_rej) total_ko_rejete,
                    SUM(ttl_ko_hp) total_ko_hp
	
                    FROM 
                    flux 
                    LEFT JOIN view_anomalie_actuel vaa ON vaa.id_flux = flux.id_flux
                    LEFT JOIN view_anomalie_cumul vac ON vac.id_flux = flux.id_flux  
                    where 
                    flux.societe = ".$societe." and flux.id_source = ".$source." AND date_reception::date  BETWEEN '".$date1."' and '".$date2."' 
                    GROUP BY semaine ";

        return $this->push->query($sql_ano)->result();
    }

    public function typologieSemaine($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "SELECT *from public.f_par_typologie_semaine('".$date1."','".$date2."',$societe) order by semaine";
            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT *from public.f_par_typologie_semaine('".$date1."','".$date2."',".$societe.",".$source.") order by semaine";
            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();


    }

    public function cloJournalier($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "SELECT *from public.f_pli_cloture_detail('".$date1."','".$date2."',$societe) order by date_courrier";
            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT *from public.f_flux_cloture_detail('".$date1."','".$date2."',".$societe.",".$source.") order by date_reception";
            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();


    }

    public function cloTypologieSemaine($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "SELECT *from public.f_pli_cloture_semaine('".$date1."','".$date2."',$societe) order by semaine";
            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT *from public.f_flux_cloture_semaine('".$date1."','".$date2."',".$societe.",".$source.") order by semaine";
            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();
    }

    public function detailEncours($date1,$date2,$societe,$source)
    {
        if($source == 3){
        $sqlFinal = "SELECT date_courrier,
        sum(pli_en_cours_typage) as en_cours_typage,
        sum(en_attente_saisie) as en_attente_saisie,
        sum(pli_en_cours_saisie) as en_cours_saisie,
        sum(en_attente_controle) as pli_en_cours_controle,
        sum(en_attente_controle) as en_attente_controle,
        sum(pli_en_attente_consigne) as en_attente_consigne,
        sum(chq_controle_ok) as chq_controle_ok,
        sum(chq_correction_ok) as chq_correction_ok,
        sum(chq_validation_ok) as chq_validation_ok
        FROM public.view_pli_en_cours 
          where date_courrier between '".$date1."' and '".$date2."' and 
          societe = ".$societe." group by date_courrier";


        $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT date_reception,
        sum(flux_en_cours_typage) as en_cours_typage,
        sum(flux_type) as flux_type,
        sum(en_cours_saisie_nv1) as en_cours_saisie_nv1,
        sum(flux_escalade) as flux_escalade,
        sum(en_cours_saisie_nv2) as en_cours_saisie_nv2,
        sum(flux_trans_cons) as flux_trans_cons,
        sum(flux_recep_cons) as flux_recep_cons,
        sum(flux_en_cours_retraite) as flux_en_cours_retraite
        FROM public.view_flux_en_cours 
          where date_reception between '".$date1."' and '".$date2."' and 
          societe = ".$societe." and id_source = ".$source." group by date_reception";


            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();
    }
    public function parSemaineEncours($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "SELECT semaine,
              sum(pli_en_cours_typage) as en_cours_typage,
              sum(en_attente_saisie) as en_attente_saisie,
              sum(pli_en_cours_saisie) as en_cours_saisie,
              sum(en_attente_controle) as pli_en_cours_controle,
              sum(en_attente_controle) as en_attente_controle,
              sum(pli_en_attente_consigne) as en_attente_consigne,
              sum(chq_controle_ok) as chq_controle_ok,
              sum(chq_correction_ok) as chq_correction_ok,
              sum(chq_validation_ok) as chq_validation_ok
            FROM public.view_pli_en_cours 
            where date_courrier between '".$date1."' and '".$date2."' and 
              societe = ".$societe." group by semaine";


            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT semaine,
              sum(flux_en_cours_typage) as en_cours_typage,
        sum(flux_type) as flux_type,
        sum(en_cours_saisie_nv1) as en_cours_saisie_nv1,
        sum(flux_escalade) as flux_escalade,
        sum(en_cours_saisie_nv2) as en_cours_saisie_nv2,
        sum(flux_trans_cons) as flux_trans_cons,
        sum(flux_recep_cons) as flux_recep_cons,
        sum(flux_en_cours_retraite) as flux_en_cours_retraite
        FROM public.view_flux_en_cours 
          where date_reception between '".$date1."' and '".$date2."' and 
          societe = ".$societe." and id_source = ".$source." group by semaine";


            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();
    }

    public function nonTraiteJour($date1,$date2,$societe,$source)
    {
        if ($source == 3){
            $sqlFinal = "select 
                date_courrier,
                pli_en_cours,
                pli_non_traite
                from (
                SELECT date_courrier::date,societe, nom_societe,sum(pli_en_cours) as pli_en_cours, sum(pli_non_traite) as pli_non_traite
                  FROM public.view_pli_stat where societe::integer = ".$societe." and date_courrier::date between '".$date1."' and '".$date2."'
                GROUP BY date_courrier::date,societe, nom_societe
                ) non_traite order by date_courrier
               ";


            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "select 
                date_reception,
                flux_en_cours,
                flux_non_traite
                from (
                SELECT date_reception::date,societe, nom_societe,sum(flux_en_cours) as flux_en_cours, sum(flux_non_traite) as flux_non_traite
                  FROM public.view_stat_flux where societe::integer = ".$societe." and date_reception::date between '".$date1."' and '".$date2."' and id_source = ".$source." 
                GROUP BY date_reception::date,societe, nom_societe
                ) non_traite order by date_reception 
               ";


            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();
    }
    public function nonTraiteSemaine($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "select  
                semaine,
                pli_en_cours,
                pli_non_traite
                from (
                SELECT date_part('week'::text, view_pli_stat.date_courrier) semaine, nom_societe,sum(pli_en_cours) as pli_en_cours, sum(pli_non_traite) as pli_non_traite
                  FROM public.view_pli_stat where societe::integer = ".$societe." and date_courrier::date between '".$date1."' and '".$date2."'
                GROUP BY date_part('week'::text, view_pli_stat.date_courrier),societe, nom_societe
                ) non_traite order by semaine";


            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "select  
                semaine,
                flux_en_cours,
                flux_non_traite
                from (
                SELECT date_part('week'::text, view_stat_flux.date_reception) semaine, nom_societe,sum(flux_en_cours) as flux_en_cours, sum(flux_non_traite) as flux_non_traite
                  FROM public.view_stat_flux where societe::integer = ".$societe." and date_reception::date between '".$date1."' and '".$date2."' and id_source = ".$source." 
                GROUP BY date_part('week'::text, view_stat_flux.date_reception),societe, nom_societe
                ) non_traite order by semaine";


            $queryJournalier = $this->push->query($sqlFinal);
        }

        return $queryJournalier->result();
    }
    public function syntheseSemaine($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "SELECT *from public.f_synthese_semaine ('".$date1."','".$date2."',$societe)";
            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT *from public.f_synthese_semaine ('".$date1."','".$date2."',".$societe.",".$source.")";
            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();
    }
    public function syntheseJournaliere($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "SELECT *from public.f_synthese_journaliere ('".$date1."','".$date2."',$societe)";
            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT *from public.f_synthese_journaliere ('".$date1."','".$date2."',".$societe.",".$source.")";
            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();
    }

    public function verifDonnees($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "SELECT count(*)nb
          FROM public.view_pli_stat where date_courrier::date between '".$date1."' and '".$date2."' and societe::integer =".$societe;
            $query = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT count(*)nb
          FROM public.view_stat_flux where date_reception::date between '".$date1."' and '".$date2."' and societe::integer =".$societe." and id_source =".$source;
            $query = $this->push->query($sqlFinal);
        }
        return $query->result_array();
    }

    public function encoursParTypologie($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlFinal = "SELECT *from public.f_pli_en_cours_par_typologie('".$date1."','".$date2."',$societe)";
            $queryJournalier = $this->ged->query($sqlFinal);
        }
        else{
            $sqlFinal = "SELECT *from public.f_flux_en_cours_par_typologie('".$date1."','".$date2."',".$societe.",".$source.")";
            $queryJournalier = $this->push->query($sqlFinal);
        }
        return $queryJournalier->result();
    }



}