<?php

class Model_Liste_plis_flux extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;
    private $push;

    var $columnOrder       = array("date_courrier","date_numerisation","dt_event","lot_scan","pli","typologie","statut","nb_mouvement","id_pli","id_lot_saisie","modes_paiements");
    var $columnOrderFlux   = array("date_reception","date_traitement","typologie","statut","etat","nb_abonnement","id_flux");
    var $columnSearch      = array("date_courrier","date_numerisation","lot_scan","pli","dt_event","typologie","nb_mouvement","statut","id_pli","id_lot_saisie","modes_paiements");
    var $columnSearchFlux  = array("date_reception","date_traitement","typologie","statut","etat","nb_abonnement","id_flux");
    var $order             = array("date_courrier" => 'desc');
    var $listePli          = 'view_pli_a_deverouiller';


    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->push = $this->load->database('ged_push', TRUE);
    }


    public function recordsTotal($date1,$date2,$societe,$source)
    {
        if($source == 3){
            $sqlPli = "(select *from f_liste_pli('".$date1."','".$date2."',$societe) as tab)";
            $this->ged->select($this->columnOrder)->from("(select *from f_liste_pli('".$date1."','".$date2."',$societe) ) as tab");
            return $this->ged->count_all_results();
        }
        else{
            $sqlPli = "(select *from f_liste_flux('".$date1."','".$date2."',".$societe.",".$source.") as tab)";
            $this->push->select($this->columnOrderFlux)->from("(select *from f_liste_flux('".$date1."','".$date2."',".$societe.",".$source.") ) as tab");
            return $this->push->count_all_results();
        }
    }

    public function recordsFiltered($date1,$date2,$societe,$source)
    {
        $this->_get_query($date1,$date2,$societe,$source);
        if($source == 3){
            $query = $this->ged->get();
        }
        else{
            $query = $this->push->get();
        }
        return $query->num_rows();
    }
    public function exportListePli($date1,$date2,$societe)
    {
        /*$this->ged->select($this->columnOrder)->from("(select *from f_liste_pli('".$date1."','".$date2."',$societe) ) as tab");
        $query = $this->ged->get();

        return $query->result();*/

        $sqlFinal = "SELECT *from public.f_liste_pli('".$date1."','".$date2."',$societe) order by date_courrier";

        /* $sqlFinal = "SELECT *from public.f_liste_pli('2010-01-10','2020-01-01',2) order by id_pli";*/
        $queryJournalier = $this->ged->query($sqlFinal);
        return $queryJournalier->result();
    }

    private function _get_query($date1,$date2,$societe,$source)
    {
        if($source == 3) {
            if ((int)$societe == 1) {
                $sqlPli = "(select *from view_liste_pli_bayard where  WHERE date_courrier BETWEEN '" . $date1 . "' " . $date2 . "') as tab)";
            } else {
                $sqlPli = "(select *from view_liste_pli_milan where  WHERE date_courrier BETWEEN '" . $date1 . "' " . $date2 . "') as tab)";
            }

            $this->ged->select($this->columnOrder)->from("(select *from f_liste_pli('" . $date1 . "','" . $date2 . "',$societe) ) as tab");

            $i = 0;

            foreach ($this->columnSearch as $emp) {
                if (isset($_POST['search']['value']) && !empty($_POST['search']['value'])) {
                    $_POST['search']['value'] = $_POST['search']['value'];
                } else
                    $_POST['search']['value'] = '';
                if ($_POST['search']['value']) {
                    if ($i === 0) // first loop
                    {
                        $this->ged->group_start();
                        $this->ged->like($emp, $_POST['search']['value']);
                    } else {
                        $this->ged->or_like($emp, $_POST['search']['value']);
                    }

                    if (count($this->columnSearch) - 1 == $i) //last loop
                        $this->ged->group_end();
                }
                $i++;
            }

            if (isset($_POST['order'])) // here order processing
            {
                $this->ged->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } else if (isset($this->order)) {
                $order = $this->order;
                $this->ged->order_by(key($order), $order[key($order)]);
            }
        }
        else{

            $this->push->select($this->columnOrderFlux)->from("(select *from f_liste_flux('" . $date1 . "','" . $date2 . "',".$societe.",".$source.") ) as tab");

            $i = 0;

            foreach ($this->columnSearchFlux as $emp) {
                if (isset($_POST['search']['value']) && !empty($_POST['search']['value'])) {
                    $_POST['search']['value'] = $_POST['search']['value'];
                } else
                    $_POST['search']['value'] = '';
                if ($_POST['search']['value']) {
                    if ($i === 0) // first loop
                    {
                        $this->push->group_start();
                        $this->push->like($emp, $_POST['search']['value']);
                    } else {
                        $this->push->or_like($emp, $_POST['search']['value']);
                    }

                    if (count($this->columnSearchFlux) - 1 == $i) //last loop
                        $this->push->group_end();
                }
                $i++;
            }

            if (isset($_POST['order'])) // here order processing
            {
                $this->push->order_by($this->columnOrderFlux[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } else if (isset($this->order)) {
                $order = $this->order;
                $this->push->order_by(key($order), $order[key($order)]);
            }
        }
    }

    public function getListePli($date1,$date2,$societe,$source)
    {
        $this->_get_query($date1,$date2,$societe,$source);
        if(isset($_POST['length']) && $_POST['length'] < 1)
        {
            $_POST['length']= '10';
        }
        else
            $_POST['length']= $_POST['length'];

        if(isset($_POST['start']) && $_POST['start'] > 1)
        {
            $_POST['start']= $_POST['start'];
        }
        if($source == 3){
            $this->ged->limit($_POST['length'], $_POST['start']);
            $query = $this->ged->get();
        }
        else{
            $this->push->limit($_POST['length'], $_POST['start']);
            $query = $this->push->get();
        }

        return $query->result();
    }






}