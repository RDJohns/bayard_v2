<?php

class Model_Pilotage_Flux extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;
    private $ged_push;
    
    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        $this->ged_push = $this->load->database('ged_push', TRUE);
    }


    public function setValueCourrier()
    {
        $sql = "
        select 
            sum(nb_pli) pli_total,
            sum(nb_mouvement) nb_mouvement,
            sum(stock_typage) stock_typage,
            sum(en_cours_typage) en_cours_typage,
            sum(typage_ok) typage_ok,
            sum(ko_scan) ko_scan,
            sum(hors_peri) hors_peri,
            sum(stock_saisie) stock_saisie,
            sum(en_cours_saisie) en_cours_saisie,
            sum(saisie_ok) saisie_ok,
            sum(saisie_ko)saisie_ko,
            sum(saisie_ci)saisie_ci,
            sum(stock_ctrl)stock_ctrl,
            sum(kd)kd,
            sum(ke)ke,
            sum(ks)ks,
            sum(ctrl_ok)ctrl_ok,
            sum(ctrl_ci)ctrl_ci,
            sum(flag_reb) reb,
            'sep'::text sep,
            sum(mvt_typage_ok) mvt_typage_ok,
            sum(mvt_stock_saisie) mvt_stock_saisie,
            sum(mvt_en_cours_saisie) mvt_en_cours_saisie,
            sum(mvt_saisie_ok) mvt_saisie_ok,
            sum(mvt_saisie_ko)mvt_saisie_ko,
            sum(mvt_saisie_ci) mvt_saisie_ci,
            sum(mvt_stock_ctrl)mvt_stock_ctrl,
            sum(mvt_kd)mvt_kd,
            sum(mvt_ke)mvt_ke,
            sum(mvt_ks)mvt_ks,
            sum(mvt_ctrl_ok)mvt_ctrl_ok,
            sum(mvt_ctrl_ci) mvt_ctrl_ci
            
            from (
            SELECT
                1 nb_pli,
                CASE WHEN view_nb_mouvement_pli.nb_mvt IS NULL THEN 0::bigint  ELSE view_nb_mouvement_pli.nb_mvt END AS nb_mouvement,
                case when view_histo_pli_oid.flag_traitement  = 0 or view_histo_pli_oid.flag_traitement is null then 1 else 0 end as stock_typage,
                case when view_histo_pli_oid.flag_traitement  = 1 then 1 else 0 end as en_cours_typage,

                case when view_histo_pli_oid.flag_traitement  = 2 then 1 else 0 end as typage_ok,
                case when view_histo_pli_oid.flag_traitement  = 2 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt  else 0 end as mvt_typage_ok,

                case when view_histo_pli_oid.flag_traitement  = 16 then 1 else 0 end as ko_scan,
                case when view_histo_pli_oid.flag_traitement  = 21 then 1 else 0 end as hors_peri,

                case when view_histo_pli_oid.flag_traitement  = 3 then 1 else 0 end as stock_saisie,
                case when view_histo_pli_oid.flag_traitement  = 3 and view_nb_mouvement_pli.nb_mvt is not null  then  view_nb_mouvement_pli.nb_mvt else 0 end as mvt_stock_saisie,
                
                case when view_histo_pli_oid.flag_traitement  = 4 then 1 else 0 end as en_cours_saisie,
                case when view_histo_pli_oid.flag_traitement  = 4 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_en_cours_saisie,

                case when view_histo_pli_oid.flag_traitement  = 5 and f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 1 then 1 else 0 end as saisie_ok,
                case when view_histo_pli_oid.flag_traitement  = 5 and f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 1 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_saisie_ok,

                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 2 and view_histo_pli_oid.flag_traitement = 17 then 1 else 0 end as saisie_ko,
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 2 and view_histo_pli_oid.flag_traitement = 17 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_saisie_ko,

                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 3 and view_histo_pli_oid.flag_traitement = 23 then 1 else 0 end as saisie_ci,
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 3 and view_histo_pli_oid.flag_traitement = 23 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_saisie_ci,

                case when  view_histo_pli_oid.flag_traitement in(6,7) then 1 else 0 end as stock_ctrl,
                case when  view_histo_pli_oid.flag_traitement in(6,7) and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_stock_ctrl,
                
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 6 and view_histo_pli_oid.flag_traitement = 20 then 1 else 0 end as kd,
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 6 and view_histo_pli_oid.flag_traitement = 20 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_kd,
                
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 4 and view_histo_pli_oid.flag_traitement = 18 then 1 else 0 end as ks,
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 4 and view_histo_pli_oid.flag_traitement = 18 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_ks,

            case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 5 and view_histo_pli_oid.flag_traitement = 19 then 1 else 0 end as ke,
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 5 and view_histo_pli_oid.flag_traitement = 19 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_ke,
                
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 1 and view_histo_pli_oid.flag_traitement = 9 then 1 else 0 end as ctrl_ok,
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 1 and view_histo_pli_oid.flag_traitement = 9 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_ctrl_ok,
                
                
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 3 and view_histo_pli_oid.flag_traitement = 25 then 1 else 0 end as ctrl_ci,
                case when  f_pli.id_lot_saisie is not null and data_pli.statut_saisie = 3 and view_histo_pli_oid.flag_traitement = 25 and view_nb_mouvement_pli.nb_mvt is not null then view_nb_mouvement_pli.nb_mvt else 0 end as mvt_ctrl_ci,
                
                case when view_pli_en_reb.flag_reb is null then 0 else view_pli_en_reb.flag_reb  end as flag_reb
                FROM f_pli
                LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
                LEFT JOIN view_histo_pli_oid ON view_histo_pli_oid.id_pli = f_pli.id_pli
                LEFT JOIN view_nb_mouvement_pli ON view_nb_mouvement_pli.id_pli = f_pli.id_pli
                LEFT JOIN view_pli_en_reb on view_pli_en_reb.id_pli = f_pli.id_pli
                LEFT JOIN data_pli ON data_pli.id_pli = view_histo_pli_oid.id_pli
                LEFT JOIN societe ON societe.id = f_pli.societe
                LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = view_histo_pli_oid.flag_traitement
                where  date_courrier::date between '".$this->session->userdata('dateDebut')."' and '".$this->session->userdata('datefin')."'  ) as courrier
            ";
        $query = $this->ged->query($sql);
        return $query->result();
    }

    public function setValueFlux($source)
    {
        $sql = "
        select 
            count(id_flux) flux_recus, 
	        sum(nb_abonnement)nb_abonnement,
            sum(stock_typage)stock_typage,
            sum(flux_en_cours_typage)flux_en_cours_typage,
        
            sum(typage_ok)typage_ok,
            sum(abonnement_typage_ok)abonnement_typage_ok,
        
            sum(flux_hors_peri)flux_hors_peri,
            sum(flux_mail_anomalie)flux_mail_anomalie,
            sum(flux_anomalie_pj)flux_anomalie_pj,
            sum(flux_anomalie_fichier)flux_anomalie_fichier,

            sum(stock_saisie)stock_saisie,
            sum(abonnement_stock_saisie)abonnement_stock_saisie,
            
            sum(en_cours_saisie)en_cours_saisie,
            sum(abonnement_en_cours_saisie)abonnement_en_cours_saisie,

            sum(saisie_ok)saisie_ok,
            sum(abonnement_saisie_ok)abonnement_saisie_ok,
        
            sum(escalade)escalade,
            sum(abonnement_escalade)abonnement_escalade,
            sum(rejete)rejete
            from (
                select 
                    flux.id_flux,
		    flux.nb_abonnement,	
                    CASE WHEN statut_pli = 0 OR statut_pli IS NULL THEN 1 ELSE 0  END AS stock_typage,
                    CASE WHEN statut_pli = 1 THEN 1 ELSE 0 END AS flux_en_cours_typage,
                
                    CASE WHEN statut_pli = 2 THEN 1 ELSE 0 END AS typage_ok,
                    CASE WHEN statut_pli = 2 and flux.nb_abonnement is not null THEN flux.nb_abonnement ELSE 0 END AS abonnement_typage_ok,
                
                    CASE WHEN statut_pli = 13 THEN 1 ELSE 0 END AS flux_hors_peri, 
                    CASE WHEN statut_pli = 11 THEN 1 ELSE 0 END AS flux_mail_anomalie,
                    CASE WHEN statut_pli = 12 THEN 1 ELSE 0 END AS flux_anomalie_pj,
                    CASE WHEN statut_pli = 14 THEN 1 ELSE 0 END AS flux_anomalie_fichier,

                    CASE WHEN statut_pli = 2 THEN 1 ELSE 0 END AS stock_saisie,
		   CASE WHEN statut_pli = 2 and flux.nb_abonnement is not null THEN flux.nb_abonnement ELSE 0 END AS abonnement_stock_saisie,
		    	
                    CASE WHEN statut_pli  in(3,5) THEN 1 ELSE 0 END AS en_cours_saisie,
                    CASE WHEN statut_pli  in(3,5) and flux.nb_abonnement is not null THEN flux.nb_abonnement ELSE 0 END AS abonnement_en_cours_saisie,

		            CASE WHEN statut_pli  = 7 THEN 1 ELSE 0 END AS saisie_ok,
                    CASE WHEN statut_pli = 7  and flux.nb_abonnement is not null THEN flux.nb_abonnement ELSE 0 END AS abonnement_saisie_ok,
                
                    CASE WHEN statut_pli = 4  THEN 1 ELSE 0 END AS escalade,
                    CASE WHEN statut_pli = 4  and flux.nb_abonnement is not null THEN flux.nb_abonnement ELSE 0 END AS abonnement_escalade,

                     CASE WHEN statut_pli = 15  THEN 1 ELSE 0 END AS rejete
                
                    FROM flux
                       
                        where flux.id_source = ".(int)$source." and flux.date_reception::date between '".$this->session->userdata('dateDebut')."' and '".$this->session->userdata('datefin')."') as flux";

        $query = $this->ged_push->query($sql);
        return $query->result();
    }
}