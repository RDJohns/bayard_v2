<?php


class Model_Reception extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;
    private $viewTyplogie = "view_typlogie";

    public function __construct() 
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }
    public function pliParTypologie($date1,$date2,$societe)
    {
        /*$sqlTypologie = "
        select 
          id,
          toute.typologie, 
          case when nb_pli_par_typo is null then 0 else nb_pli_par_typo end as nb_pli_par_typo, 
          case when nb_mvt_par_typo is null then 0 else nb_mvt_par_typo end as nb_mvt_par_typo, 
          case when pourcent_pli is null then 0 else pourcent_pli end as pourcent_pli, 
          case when pourcent_mvt is null then 0 else pourcent_mvt end as pourcent_mvt, 
          case when nb_mvt_total is null then 0 else nb_mvt_total end as nb_mvt_total, 
          case when pli_total is null then 0 else pli_total end as pli_total

          from (select typologie.id, typologie.typologie from typologie union select 0, 'Non typé')  toute
          left join (
          SELECT *from public.f_pli_par_typologie('".$date1."','".$date2."',".$societe.")) as par_typo on par_typo.id_typologie = toute.id order by typologie";

        $query = $this->ged->query($sqlTypologie);
        return $query->result();*/
        /*$sqlTypologie = "
            select 
                id,
                typologie,
                nb_pli_par_typo,
                nb_mvt_par_typo,
                ROUND((nb_pli_par_typo*100.00)/(tot_nb_pli_par_typo),2)pourcent_pli ,
                ROUND((nb_mvt_par_typo*100.00)/(tot_nb_mvt_par_typo),2)pourcent_mvt ,
                tot_nb_mvt_par_typo nb_mvt_total,
                tot_nb_pli_par_typo pli_total
                from 
                (
                select
                  id,
                  toute.typologie, 
                  case when nb_pli_par_typo is null then 0 else nb_pli_par_typo end as nb_pli_par_typo, 
                  case when nb_mvt_par_typo is null then 0 else nb_mvt_par_typo end as nb_mvt_par_typo
                  from (select typologie.id, typologie.typologie from typologie union select 0, 'Non typé')  toute
                left join (
                SELECT  count(f_pli.id_pli) nb_pli_par_typo,sum(nb_mvt) nb_mvt_par_typo,typologie
                  FROM public.f_pli 
                  left join f_lot_numerisation on f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
                  left join view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = f_pli.id_pli
                 where  date_courrier  between  '".$date1."'  and  '".$date2."' and f_pli.societe = ".$societe."
                 group by typologie ) as par_typo on toute.id = par_typo.typologie  order by typologie) par_typopologie
                left join (
                SELECT  count(f_pli.id_pli) tot_nb_pli_par_typo,sum(nb_mvt) tot_nb_mvt_par_typo
                  FROM public.f_pli 
                  left join f_lot_numerisation on f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
                  left join view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = f_pli.id_pli
                 where  date_courrier  between  '".$date1."'  and  '".$date2."' and f_pli.societe = ".$societe." )par_typologie_total on 1 = 1 ";*/
                 
                 
		$sqlTypologie = "
		SELECT typo.id,
				typo.typologie,
				coalesce(fini.nb_pli_par_typo,0) nb_pli_par_typo,
				coalesce(fini.nb_mvt_par_typo,0) nb_mvt_par_typo,
				coalesce(fini.pourcent_pli,0) pourcent_pli,
				coalesce(fini.pourcent_mvt,0) pourcent_mvt,
				coalesce(fini.nb_mvt_total,0) nb_mvt_total,
				coalesce(fini.pli_total,0) pli_total
			FROM r_typologie typo
				LEFT JOIN ( SELECT par_typologie.id_typologie,
						par_typologie.nb_pli_par_typo,
						par_typologie.nb_mvt_par_typo,
						case when total.pli_total is null or total.pli_total = 0 then 0 else round(par_typologie.nb_pli_par_typo::numeric * 100.00 / coalesce(total.pli_total::numeric,1), 2) end AS pourcent_pli,
						case when par_typologie.nb_mvt_par_typo is null or par_typologie.nb_mvt_par_typo = 0 then 0 else round(par_typologie.nb_mvt_par_typo::numeric * 100.00 / coalesce(total.mouvement_total::numeric,1), 2) end AS pourcent_mvt,
						total.mouvement_total AS nb_mvt_total,
						total.pli_total
					FROM ( SELECT par_typo.id_typologie,
								sum(par_typo.nb_mouvement) AS nb_mvt_par_typo,
								sum(par_typo.pli) AS nb_pli_par_typo
							FROM ( SELECT 1 AS pli,
										COALESCE(data_pli.mvt_nbr, 0) AS nb_mouvement,
										COALESCE(f_pli.typologie, 0) AS id_typologie
									FROM f_pli
										JOIN societe ON societe.id = f_pli.societe
										LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
										JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
										LEFT JOIN typologie ON typologie.id = f_pli.typologie
										LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
										where f_pli.societe = ".$societe." and date_courrier::date  BETWEEN '".$date1."' and '".$date2."' ) par_typo
							GROUP BY par_typo.id_typologie
							ORDER BY par_typo.id_typologie) par_typologie
						LEFT JOIN ( SELECT count(*) AS pli_total,
								sum(data_pli.mvt_nbr) AS mouvement_total
							FROM f_pli
								JOIN societe ON societe.id = f_pli.societe
								LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
								JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
								LEFT JOIN typologie ON typologie.id = f_pli.typologie
								LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								where f_pli.societe = ".$societe." and date_courrier::date  BETWEEN '".$date1."' and '".$date2."' 
								) total ON 1 = 1) fini ON fini.id_typologie = typo.id;

";         
                 
        $query = $this->ged->query($sqlTypologie);
        return $query->result();
        
    }

    public function pliEncours($date1,$date2,$societe)
    {
        $sqlEncours = "SELECT *from public.f_pli_en_cours('".$date1."','".$date2."',".$societe.");";
        $query = $this->ged->query($sqlEncours);
        return $query->result();
    }
    
    public function pliEncours2($date1,$date2,$societe) /*independant de view_pli_stat*/
    {
        /*$sqlEncours = "where f_pli.societe = ".$societe." and date_courrier::date  BETWEEN '".$date1."' and '".$date2."' ) en_cours;";*/
		$sqlEncours ="
				select
	 coalesce(SUM(en_cours_typage),0)::text en_cours_typage,
	 coalesce(SUM(en_attente_saisie),0)::text en_att_saisie,
	 coalesce(SUM(en_cours_saisie),0)::text en_cours_saisie,
	 coalesce(SUM(en_attente_ctrl),0)::text en_att_ctrl,
	 coalesce(SUM(en_cours_ctrl),0)::text en_cours_ctrl,
	 coalesce(SUM(en_attente_retraitement),0)::text en_attente_retraitement,
	 coalesce(SUM(en_cours_retraitement),0)::text en_cours_retraitement,
	 coalesce(SUM(mvt_en_cours_typage),0)::text nb_mvt_en_cours_typage,
	 coalesce(SUM(mvt_en_attente_saisie),0)::text mvt_en_att_saisie,
	 coalesce(SUM(mvt_en_cours_saisie),0)::text mvt_en_cours_saisie,
	 coalesce(SUM(mvt_en_attente_ctrl),0)::text mvt_en_att_ctrl,
	 coalesce(SUM(mvt_en_cours_ctrl),0)::text mvt_en_cours_ctrl,
	 coalesce(SUM(mvt_en_attente_retraitement),0)::text mvt_en_attente_retraitement,
	 coalesce(SUM(mvt_en_cours_retraitement),0)::text mvt_en_cours_retraitement,
	 coalesce(SUM(en_cours),0)::text pli_en_cours,
	 coalesce(SUM(mvt_en_cours),0)::text mvt_pli_en_cours
	 from(
		select 
		f_pli.id_pli,
		case when data_pli.flag_traitement = 1 then 1 else 0 end as en_cours_typage,
		case when (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1 )
		or (data_pli.flag_traitement in(3) and  data_pli.statut_saisie = 1 ) then 1 else 0 end as en_attente_saisie,
		case when (data_pli.flag_traitement = 4 and data_pli.statut_saisie = 1 ) then 1 else 0 end as en_cours_saisie,
		case when (data_pli.flag_traitement = 7  and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1) then 1 else 0 end as en_attente_ctrl,
		case when data_pli.flag_traitement = 8 then 1 else 0 end as en_cours_ctrl,
		case when (data_pli.flag_traitement = 11  and  data_pli.statut_saisie = 1 ) then 1 else 0 end as en_attente_retraitement,
		case when (data_pli.flag_traitement = 13  and  data_pli.statut_saisie = 1 ) then 1 else 0 end as en_cours_retraitement,

		case when data_pli.flag_traitement = 1 then data_pli.mvt_nbr else 0  end as mvt_en_cours_typage,
		case when (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1 )
		or (data_pli.flag_traitement in(3)  and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr else 0  end as mvt_en_attente_saisie,
		case when data_pli.flag_traitement = 4 then data_pli.mvt_nbr else 0  end as mvt_en_cours_saisie,
		case when (data_pli.flag_traitement = 7  and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1) then data_pli.mvt_nbr else 0  end as mvt_en_attente_ctrl,
		case when data_pli.flag_traitement = 8 then data_pli.mvt_nbr else 0  end as mvt_en_cours_ctrl,
		case when (data_pli.flag_traitement = 11 and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr  else 0  end as mvt_en_attente_retraitement,
		case when (data_pli.flag_traitement = 13  and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr else 0  end as mvt_en_cours_retraitement,

		CASE WHEN (data_pli.flag_traitement  in(1,3,4,7,8,11,13)   and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1)
		THEN 1 ELSE 0 END AS en_cours,
		CASE WHEN (data_pli.flag_traitement  in(1,3,4,7,8,11,13)  and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1)
		THEN data_pli.mvt_nbr ELSE 0 END AS mvt_en_cours
	FROM f_pli
		 JOIN societe ON societe.id = f_pli.societe
		 LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
		 JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
		 LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
		 where f_pli.societe = ".$societe." and date_courrier::date  BETWEEN '".$date1."' and '".$date2."'  
		 ) en_cours";
                     
		//echo "<pre>".print_r($sqlEncours)."</pre>";
        $query = $this->ged->query($sqlEncours);
        return $query->result();
    }
    
    public function pliAvecAnomalie($date1,$date2,$societe)
	{
		$sqlAnomalie  = "
			select 
				SUM(pli_anomalie) pli_anomalie,
				SUM(pli_ko_scan) pli_ko_scan,
				SUM(pli_ko_definitif) pli_ko_definitif,
				SUM(pli_ko_inconnu) pli_ko_inconnu,
				SUM(pli_ko_src) pli_ko_src,
				SUM(pli_en_attente) pli_en_attente,
				SUM(pli_ko_circulaire) pli_ko_circulaire,
				SUM(pli_ko_ci_editee) pli_ko_ci_editee,
				SUM(pli_ko_en_cours) pli_ko_en_cours,
				SUM(pli_ok_ci) pli_ok_ci
				

				from(
				select 
				f_pli.id_pli,
				CASE WHEN data_pli.statut_saisie  in(2,3,4,5,6,7,9,12,11) THEN 1 ELSE 0 END AS pli_anomalie,
				CASE WHEN data_pli.statut_saisie  in(2) THEN 1 ELSE 0 END AS pli_ko_scan,
				CASE WHEN data_pli.statut_saisie  in(3) THEN 1 ELSE 0 END AS pli_ko_definitif,
				CASE WHEN data_pli.statut_saisie  in(4) THEN 1 ELSE 0 END AS pli_ko_inconnu,
				CASE WHEN data_pli.statut_saisie  in(5) THEN 1 ELSE 0 END AS pli_ko_src,
				CASE WHEN data_pli.statut_saisie  in(6) THEN 1 ELSE 0 END AS pli_en_attente,
				CASE WHEN data_pli.statut_saisie  in(7) THEN 1 ELSE 0 END AS pli_ko_circulaire,
				CASE WHEN data_pli.statut_saisie  in(9) THEN 1 ELSE 0 END AS pli_ko_ci_editee,
				CASE WHEN data_pli.statut_saisie  in(12) THEN 1 ELSE 0 END AS pli_ko_en_cours,
				CASE WHEN data_pli.statut_saisie  in(11) THEN 1 ELSE 0 END AS pli_ok_ci
				
				FROM f_pli
					 JOIN societe ON societe.id = f_pli.societe
					 LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
					 JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
					 LEFT JOIN typologie ON typologie.id = f_pli.typologie
					 LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
					 where 
					 data_pli.flag_traitement >= 2 and f_pli.societe = ".$societe." and date_courrier::date  BETWEEN '".$date1."' and '".$date2."'  
					 ) ko_pli

				union all 
				select 
					SUM(pli_anomalie) pli_anomalie,
					SUM(pli_ko_scan) pli_ko_scan,
					SUM(pli_ko_definitif) pli_ko_definitif,
					SUM(pli_ko_inconnu) pli_ko_inconnu,
					SUM(pli_ko_src) pli_ko_src,
					SUM(pli_en_attente) pli_en_attente,
					SUM(pli_ko_circulaire) pli_ko_circulaire,
					SUM(pli_ko_ci_editee) pli_ko_ci_editee,
					SUM(pli_ko_en_cours) pli_ko_en_cours,
					SUM(pli_ok_ci) pli_ok_ci
						from(
							select 
							id_pli,
							CASE WHEN statut_pli  in(2,3,4,5,6,7,9,12,11) THEN 1 ELSE 0 END AS pli_anomalie,
							CASE WHEN statut_pli  in(2) THEN 1 ELSE 0 END AS pli_ko_scan,
							CASE WHEN statut_pli  in(3) THEN 1 ELSE 0 END AS pli_ko_definitif,
							CASE WHEN statut_pli  in(4) THEN 1 ELSE 0 END AS pli_ko_inconnu,
							CASE WHEN statut_pli  in(5) THEN 1 ELSE 0 END AS pli_ko_src,
							CASE WHEN statut_pli  in(6) THEN 1 ELSE 0 END AS pli_en_attente,
							CASE WHEN statut_pli  in(7) THEN 1 ELSE 0 END AS pli_ko_circulaire,
							CASE WHEN statut_pli  in(9) THEN 1 ELSE 0 END AS pli_ko_ci_editee,
							CASE WHEN statut_pli  in(12) THEN 1 ELSE 0 END AS pli_ko_en_cours,
							CASE WHEN statut_pli  in(11) THEN 1 ELSE 0 END AS pli_ok_ci,
							dt_courrier::date
							FROM histo_pli where statut_pli in(2,3,4,5,6,7,9,12,11) 
							and societe_h = ".$societe." and dt_courrier::date  BETWEEN '".$date1."' and '".$date2."'
							group by statut_pli,dt_courrier::date,id_pli
							order by id_pli ) ko_pli
				";
		 
		$query = $this->ged->query($sqlAnomalie);
        return $query->result();
		
	}
    public function pliCloture($date1,$date2,$societe)
    {
        $sqlEncours = "
        select 
        id,
        toute.typologie, 
        case when nb_pli_par_typo is null then 0 else nb_pli_par_typo end as nb_pli_par_typo, 
        case when nb_mvt_par_typo is null then 0 else nb_mvt_par_typo end as nb_mvt_par_typo, 
        case when pourcent_pli is null then 0 else pourcent_pli end as pourcent_pli, 
        case when pourcent_mvt is null then 0 else pourcent_mvt end as pourcent_mvt, 
        case when nb_mvt_total is null then 0 else nb_mvt_total end as nb_mvt_total, 
        case when pli_total is null then 0 else pli_total end as pli_total
        
        from (select typologie.id, typologie.typologie from typologie union select 0, 'Non typé')  toute
        left join (
        SELECT *from public.pli_cloture('".$date1."','".$date2."',".$societe.")) as par_typo on par_typo.id_typologie = toute.id order by typologie
        ";

        $query = $this->ged->query($sqlEncours);
        return $query->result();
    }

    public function typologieJournalier($date1,$date2,$societe)
    {
      
	  $sqlFinal = "SELECT *from public.f_par_typologie_detail('".$date1."','".$date2."',$societe) order by date_courrier";
	   
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    

    }

    public function toutesTypologie()
    {
        $sql    = "SELECT id, typologie FROM public.view_typlogie union SELECT 0, 'Non définie';";
        $typo   = array();
        $query  = $this->base_64->query($sql);
        $res    = $query->result();
        foreach($res as $item) 
        {
          $typo[$item->id] = $item->typologie;
        }

        return $typo;
    }

    public function nonTraiteEncours($date1,$date2,$societe)
    {
      $sqlTraiteEncours = "
      select 
        total,
        pli_en_cours,
        pli_non_traite,

        case when total = 0 or pli_en_cours = 0 then 0 else round(((pli_en_cours*100.00)/(total)),2) end as pourcent_en_cours,
        case when total = 0 or pli_non_traite = 0 then 0 else round(((pli_non_traite*100.00)/(total)),2) end as pourcent_non_traite
        from (
        SELECT societe, nom_societe,sum(pli_en_cours) as pli_en_cours, sum(pli_non_traite) as pli_non_traite
          FROM public.reception_globale where societe::integer = ".$societe." and date_courrier::date between '".$date1."' and '".$date2."'
        GROUP BY societe, nom_societe
        ) non_traite
        left join(
        SELECT count(*) total
          FROM public.reception_globale where date_courrier::date between '".$date1."' and '".$date2."' and societe::integer = ".$societe.") somme on 1= 1";

          $query = $this->ged->query($sqlTraiteEncours);
          return $query->result();
    }
    public function globaleReception($date1,$date2,$societe)
    {
        $sql = "SELECT nb_pli_ci_editee , mvt_ci_editee,mvt_anomalie,pli_total,mouvement_total,nb_pli_non_traite,pli_cloture,mvt_cloture,nb_pli_en_cours,mouvement_en_cours,nb_pli_anomalie,pli_ok_ci,mvt_ok_ci from public.reception_globale('".$date1."','".$date2."','".$societe."');
        ";
        $query = $this->ged->query($sql);
        return $query->result_array();
    }
    public function typologieSemaine($date1,$date2,$societe)
    {
      
       $sqlFinal = "SELECT *from public.f_par_typologie_semaine('".$date1."','".$date2."',$societe) order by semaine";
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    

    }
    public function cloJournalier($date1,$date2,$societe)
    {
      
       $sqlFinal = "SELECT *from public.pli_cloture_detail('".$date1."','".$date2."',$societe) order by date_courrier";
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    

    }

    public function cloTypologieSemaine($date1,$date2,$societe)
    {
      
       $sqlFinal = "SELECT *from public.pli_cloture_semaine('".$date1."','".$date2."',$societe) order by semaine";
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    }
	
	public function avecAnomalieSemaine($date1,$date2,$societe)
	{
		$sqlAvecAnomalieSemaine = "
			
			select 
				
				'S'||date_part('week'::text, date_courrier) AS courrier_date,
				SUM(pli_anomalie) pli_anomalie,
				SUM(pli_ko_scan) pli_ko_scan,
				SUM(pli_ko_definitif) pli_ko_definitif,
				SUM(pli_ko_inconnu) pli_ko_inconnu,
				SUM(pli_ko_src) pli_ko_src,
				SUM(pli_en_attente) pli_en_attente,
				SUM(pli_ko_circulaire) pli_ko_circulaire,
				SUM(pli_ko_ci_editee) pli_ko_ci_editee,
				SUM(pli_ci_envoyee) pli_ci_envoyee,
				SUM(pli_ok_ci) pli_ok_ci,
				SUM(pli_ko_en_cours) pli_ko_en_cours

				from(
				select 
				date_courrier,
				f_pli.id_pli,
				CASE WHEN data_pli.statut_saisie  in(2,3,4,5,6,7,9,10,11,12) THEN 1 ELSE 0 END AS pli_anomalie,
				CASE WHEN data_pli.statut_saisie  in(2) THEN 1 ELSE 0 END AS pli_ko_scan,
				CASE WHEN data_pli.statut_saisie  in(3) THEN 1 ELSE 0 END AS pli_ko_definitif,
				CASE WHEN data_pli.statut_saisie  in(4) THEN 1 ELSE 0 END AS pli_ko_inconnu,
				CASE WHEN data_pli.statut_saisie  in(5) THEN 1 ELSE 0 END AS pli_ko_src,
				CASE WHEN data_pli.statut_saisie  in(6) THEN 1 ELSE 0 END AS pli_en_attente,
				CASE WHEN data_pli.statut_saisie  in(7) THEN 1 ELSE 0 END AS pli_ko_circulaire,
				CASE WHEN data_pli.statut_saisie  in(9) THEN 1 ELSE 0 END AS pli_ko_ci_editee,
				CASE WHEN data_pli.statut_saisie  in(10) THEN 1 ELSE 0 END AS pli_ci_envoyee,
				CASE WHEN data_pli.statut_saisie  in(11) THEN 1 ELSE 0 END AS pli_ok_ci,
				CASE WHEN data_pli.statut_saisie  in(12) THEN 1 ELSE 0 END AS pli_ko_en_cours
				FROM f_pli
					 JOIN societe ON societe.id = f_pli.societe
					 LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
					 JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
					 LEFT JOIN typologie ON typologie.id = f_pli.typologie
					 LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
					 where f_pli.societe = ".$societe." and date_courrier::date  BETWEEN '".$date1."' and '".$date2."' 
					  and f_pli.flag_traitement >= 2 /*and  and f_pli.flag_traitement not in(9,14)*/
					 ) ko_pli GROUP BY date_part('week'::text, date_courrier) order by date_part('week'::text, date_courrier)
				
			";
					 
	  $queryJour = $this->ged->query($sqlAvecAnomalieSemaine);
       return $queryJour->result();
    }
	
	public function avecAnomalieJour($date1,$date2,$societe)
	{
		$sqlAvecAnomalieJour = "
			select 
				date_courrier::date,
				to_char(date_courrier, 'DD/MM/YYYY'::text) AS courrier_date,
				SUM(pli_anomalie) pli_anomalie,
				SUM(pli_ko_scan) pli_ko_scan,
				SUM(pli_ko_definitif) pli_ko_definitif,
				SUM(pli_ko_inconnu) pli_ko_inconnu,
				SUM(pli_ko_src) pli_ko_src,
				SUM(pli_en_attente) pli_en_attente,
				SUM(pli_ko_circulaire) pli_ko_circulaire,
				SUM(pli_ko_ci_editee) pli_ko_ci_editee,
				SUM(pli_ci_envoyee) pli_ci_envoyee,
				SUM(pli_ok_ci) pli_ok_ci,
				SUM(pli_ko_en_cours) pli_ko_en_cours

				from(
				select 
				date_courrier,
				f_pli.id_pli,
				CASE WHEN data_pli.statut_saisie  in(2,3,4,5,6,7,9,10,11,12) THEN 1 ELSE 0 END AS pli_anomalie,
				CASE WHEN data_pli.statut_saisie  in(2) THEN 1 ELSE 0 END AS pli_ko_scan,
				CASE WHEN data_pli.statut_saisie  in(3) THEN 1 ELSE 0 END AS pli_ko_definitif,
				CASE WHEN data_pli.statut_saisie  in(4) THEN 1 ELSE 0 END AS pli_ko_inconnu,
				CASE WHEN data_pli.statut_saisie  in(5) THEN 1 ELSE 0 END AS pli_ko_src,
				CASE WHEN data_pli.statut_saisie  in(6) THEN 1 ELSE 0 END AS pli_en_attente,
				CASE WHEN data_pli.statut_saisie  in(7) THEN 1 ELSE 0 END AS pli_ko_circulaire,
				CASE WHEN data_pli.statut_saisie  in(9) THEN 1 ELSE 0 END AS pli_ko_ci_editee,
				CASE WHEN data_pli.statut_saisie  in(10) THEN 1 ELSE 0 END AS pli_ci_envoyee,
				CASE WHEN data_pli.statut_saisie  in(11) THEN 1 ELSE 0 END AS pli_ok_ci,
				CASE WHEN data_pli.statut_saisie  in(12) THEN 1 ELSE 0 END AS pli_ko_en_cours 
				FROM f_pli
					 JOIN societe ON societe.id = f_pli.societe
					 LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
					 JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
					 LEFT JOIN typologie ON typologie.id = f_pli.typologie
					 LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
					where f_pli.societe = ".$societe." and date_courrier::date  BETWEEN '".$date1."' and '".$date2."' 
					 and f_pli.flag_traitement >= 2 /*and  f_pli.flag_traitement not in(9,14)*/
					 ) ko_pli GROUP BY date_courrier::date,to_char(date_courrier, 'DD/MM/YYYY'::text)  order by date_courrier::date";
					 
	  $queryJour = $this->ged->query($sqlAvecAnomalieJour);
       return $queryJour->result();
    }
	
    public function detailEncours($date1,$date2,$societe)
    {
      /*$sqlFinal = "SELECT date_courrier,
        sum(pli_en_cours_typage) as en_cours_typage,
        sum(en_attente_saisie) as en_attente_saisie,
        sum(pli_en_cours_saisie) as en_cours_saisie,
        sum(en_attente_controle) as pli_en_cours_controle,
        sum(en_attente_controle) as en_attente_controle,
        sum(pli_en_attente_consigne) as en_attente_consigne,
        sum(chq_controle_ok) as chq_controle_ok,
        sum(chq_correction_ok) as chq_correction_ok,
        sum(chq_validation_ok) as chq_validation_ok
        FROM public.view_pli_en_cours 
          where date_courrier between '".$date1."' and '".$date2."' and 
          societe = ".$societe." group by date_courrier";*/
	    
		$sqlFinal = "
		select
			date_courrier,
			to_char(date_courrier, 'DD/MM/YYYY'::text) AS courrier_date,
			coalesce(SUM(en_cours_typage),0)::text en_cours_typage,
			 coalesce(SUM(en_attente_saisie),0)::text en_att_saisie,
			 coalesce(SUM(en_cours_saisie),0)::text en_cours_saisie,
			 coalesce(SUM(en_attente_ctrl),0)::text en_att_ctrl,
			 coalesce(SUM(en_cours_ctrl),0)::text en_cours_ctrl,
			 coalesce(SUM(en_attente_retraitement),0)::text en_attente_retraitement,
			 coalesce(SUM(en_cours_retraitement),0)::text en_cours_retraitement,
			 coalesce(SUM(mvt_en_cours_typage),0)::text nb_mvt_en_cours_typage,
			 coalesce(SUM(mvt_en_attente_saisie),0)::text mvt_en_att_saisie,
			 coalesce(SUM(mvt_en_cours_saisie),0)::text mvt_en_cours_saisie,
			 coalesce(SUM(mvt_en_attente_ctrl),0)::text mvt_en_att_ctrl,
			 coalesce(SUM(mvt_en_cours_ctrl),0)::text mvt_en_cours_ctrl,
			 coalesce(SUM(mvt_en_attente_retraitement),0)::text mvt_en_attente_retraitement,
			 coalesce(SUM(mvt_en_cours_retraitement),0)::text mvt_en_cours_retraitement,
			 coalesce(SUM(en_cours),0)::text pli_en_cours,
			 coalesce(SUM(mvt_en_cours),0)::text mvt_pli_en_cours
			 from(
		select 
				
				date_courrier::date,
				f_pli.id_pli,
		case when data_pli.flag_traitement = 1 then 1 else 0 end as en_cours_typage,
		case when (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1 )
		or (data_pli.flag_traitement in(3) and  data_pli.statut_saisie = 1 ) then 1 else 0 end as en_attente_saisie,
		case when (data_pli.flag_traitement = 4 and data_pli.statut_saisie = 1 ) then 1 else 0 end as en_cours_saisie,
		case when (data_pli.flag_traitement = 7  and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1) then 1 else 0 end as en_attente_ctrl,
		case when data_pli.flag_traitement = 8 then 1 else 0 end as en_cours_ctrl,
		case when (data_pli.flag_traitement = 11  and  data_pli.statut_saisie = 1 ) then 1 else 0 end as en_attente_retraitement,
		case when (data_pli.flag_traitement = 13  and  data_pli.statut_saisie = 1 ) then 1 else 0 end as en_cours_retraitement,

		case when data_pli.flag_traitement = 1 then data_pli.mvt_nbr else 0  end as mvt_en_cours_typage,
		case when (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1 )
		or (data_pli.flag_traitement in(3)  and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr else 0  end as mvt_en_attente_saisie,
		case when data_pli.flag_traitement = 4 then data_pli.mvt_nbr else 0  end as mvt_en_cours_saisie,
		case when (data_pli.flag_traitement = 7  and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1) then data_pli.mvt_nbr else 0  end as mvt_en_attente_ctrl,
		case when data_pli.flag_traitement = 8 then data_pli.mvt_nbr else 0  end as mvt_en_cours_ctrl,
		case when (data_pli.flag_traitement = 11 and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr  else 0  end as mvt_en_attente_retraitement,
		case when (data_pli.flag_traitement = 13  and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr else 0  end as mvt_en_cours_retraitement,

		CASE WHEN (data_pli.flag_traitement  in(1,3,4,7,8,11,13)   and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1)
		THEN 1 ELSE 0 END AS en_cours,
		CASE WHEN (data_pli.flag_traitement  in(1,3,4,7,8,11,13)  and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1)
		THEN data_pli.mvt_nbr ELSE 0 END AS mvt_en_cours
			FROM f_pli
				 JOIN societe ON societe.id = f_pli.societe
				 LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
				 JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
				 LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
				 where f_pli.societe = ".$societe." and date_courrier::date  BETWEEN '".$date1."' and '".$date2."' 
				  ) en_cours
				  GROUP BY date_courrier";
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    }
    public function parSemaineEncours($date1,$date2,$societe)
    {
      $sqlFinal = " 
		select
	
			date_part('week'::text, date_courrier) as semaine,
			coalesce(SUM(en_cours_typage),0)::text en_cours_typage,
			 coalesce(SUM(en_attente_saisie),0)::text en_att_saisie,
			 coalesce(SUM(en_cours_saisie),0)::text en_cours_saisie,
			 coalesce(SUM(en_attente_ctrl),0)::text en_att_ctrl,
			 coalesce(SUM(en_cours_ctrl),0)::text en_cours_ctrl,
			 coalesce(SUM(en_attente_retraitement),0)::text en_attente_retraitement,
			 coalesce(SUM(en_cours_retraitement),0)::text en_cours_retraitement,
			 coalesce(SUM(mvt_en_cours_typage),0)::text nb_mvt_en_cours_typage,
			 coalesce(SUM(mvt_en_attente_saisie),0)::text mvt_en_att_saisie,
			 coalesce(SUM(mvt_en_cours_saisie),0)::text mvt_en_cours_saisie,
			 coalesce(SUM(mvt_en_attente_ctrl),0)::text mvt_en_att_ctrl,
			 coalesce(SUM(mvt_en_cours_ctrl),0)::text mvt_en_cours_ctrl,
			 coalesce(SUM(mvt_en_attente_retraitement),0)::text mvt_en_attente_retraitement,
			 coalesce(SUM(mvt_en_cours_retraitement),0)::text mvt_en_cours_retraitement,
			 coalesce(SUM(en_cours),0)::text pli_en_cours,
			 coalesce(SUM(mvt_en_cours),0)::text mvt_pli_en_cours
			 from(
		select 
				f_pli.id_pli,
				date_courrier::date,
				
		case when data_pli.flag_traitement = 1 then 1 else 0 end as en_cours_typage,
		case when (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1 )
		or (data_pli.flag_traitement in(3) and  data_pli.statut_saisie = 1 ) then 1 else 0 end as en_attente_saisie,
		case when (data_pli.flag_traitement = 4 and data_pli.statut_saisie = 1 ) then 1 else 0 end as en_cours_saisie,
		case when (data_pli.flag_traitement = 7  and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1) then 1 else 0 end as en_attente_ctrl,
		case when data_pli.flag_traitement = 8 then 1 else 0 end as en_cours_ctrl,
		case when (data_pli.flag_traitement = 11  and  data_pli.statut_saisie = 1 ) then 1 else 0 end as en_attente_retraitement,
		case when (data_pli.flag_traitement = 13  and  data_pli.statut_saisie = 1 ) then 1 else 0 end as en_cours_retraitement,

		case when data_pli.flag_traitement = 1 then data_pli.mvt_nbr else 0  end as mvt_en_cours_typage,
		case when (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1 )
		or (data_pli.flag_traitement in(3)  and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr else 0  end as mvt_en_attente_saisie,
		case when data_pli.flag_traitement = 4 then data_pli.mvt_nbr else 0  end as mvt_en_cours_saisie,
		case when (data_pli.flag_traitement = 7  and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1) then data_pli.mvt_nbr else 0  end as mvt_en_attente_ctrl,
		case when data_pli.flag_traitement = 8 then data_pli.mvt_nbr else 0  end as mvt_en_cours_ctrl,
		case when (data_pli.flag_traitement = 11 and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr  else 0  end as mvt_en_attente_retraitement,
		case when (data_pli.flag_traitement = 13  and  data_pli.statut_saisie = 1 ) then data_pli.mvt_nbr else 0  end as mvt_en_cours_retraitement,

		CASE WHEN (data_pli.flag_traitement  in(1,3,4,7,8,11,13)   and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1)
		THEN 1 ELSE 0 END AS en_cours,
		CASE WHEN (data_pli.flag_traitement  in(1,3,4,7,8,11,13)  and  data_pli.statut_saisie = 1 ) 
		or (data_pli.flag_traitement = 2 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 5 and data_pli.statut_saisie = 1)
		or (data_pli.flag_traitement = 6 and data_pli.statut_saisie = 1)
		THEN data_pli.mvt_nbr ELSE 0 END AS mvt_en_cours
			FROM f_pli
				 JOIN societe ON societe.id = f_pli.societe
				 LEFT JOIN f_flag_traitement ON f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
				 JOIN data_pli ON data_pli.id_pli = f_pli.id_pli
				 LEFT JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
				 where f_pli.societe = ".$societe." and date_courrier::date  BETWEEN '".$date1."' and '".$date2."' 
				 ) en_cours
				  GROUP BY date_part('week'::text, date_courrier)
			  ";

          
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    }

    public function nonTraiteJour($date1,$date2,$societe)
    {
       $sqlFinal = "select 
            to_char(date_courrier, 'DD/MM/YYYY'::text) AS  date_courrier,
            pli_en_cours,
            pli_non_traite
            from (
            SELECT date_courrier::date,societe, nom_societe,sum(pli_en_cours) as pli_en_cours, sum(pli_non_traite) as pli_non_traite
              FROM public.reception_globale where societe::integer = ".$societe." and date_courrier::date between '".$date1."' and '".$date2."'
            GROUP BY date_courrier::date,societe, nom_societe
            ) non_traite order by date_courrier
           ";

          
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    }
    public function nonTraiteSemaine($date1,$date2,$societe)
    {
      $sqlFinal = "select  
            semaine,
            pli_en_cours,
            pli_non_traite
            from (
            SELECT date_part('week'::text, reception_globale.date_courrier) semaine, nom_societe,sum(pli_en_cours) as pli_en_cours, sum(pli_non_traite) as pli_non_traite
              FROM public.reception_globale where societe::integer = ".$societe." and date_courrier::date between '".$date1."' and '".$date2."'
            GROUP BY date_part('week'::text, reception_globale.date_courrier),societe, nom_societe
            ) non_traite order by semaine";

          // echo "<pre>".print_r($sqlFinal)."</pre>";
       $queryJournalier = $this->ged->query($sqlFinal);

       return $queryJournalier->result();
    }
    public function syntheseSemaine($date1,$date2,$societe)
    {
      
       $sqlFinal = "SELECT *from public.f_synthese_semaine ('".$date1."','".$date2."',$societe)";
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    }
    public function syntheseJournaliere($date1,$date2,$societe)
    {
      
       $sqlFinal = "SELECT *from public.f_synthese_journaliere ('".$date1."','".$date2."',$societe)";
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    }

    public function verifDonnees($date1,$date2,$societe)
    {
      /*$sqlFinal = "SELECT count(*)nb
      FROM public.view_pli_stat where date_courrier::date between '".$date1."' and '".$date2."' and societe::integer =".$societe;*/
      
      $sqlFinal = " SELECT  
                    count(f_pli.id_pli) nb 
                    FROM public.f_pli 
                      left join f_lot_numerisation on f_lot_numerisation.id_lot_numerisation = f_pli.id_lot_numerisation
                       where date_courrier::date between '".$date1."' and '".$date2."' and societe::integer =".$societe;
      $query = $this->ged->query($sqlFinal);
      return $query->result_array();
    }

    public function encoursParTypologie($date1,$date2,$societe)
    {
      
       $sqlFinal = "SELECT *from public.f_pli_en_cours_par_typologie('".$date1."','".$date2."',$societe)";
       $queryJournalier = $this->ged->query($sqlFinal);
       return $queryJournalier->result();
    }
    
    public function pliFermer($date1,$date2,$societe)
    {
      $sql = "select 
            total,pli_ferme,
            case when total = 0 or pli_ferme = 0 then 0 else round(((pli_ferme*100.00)/(total)),2) end as pourcent_pli_ferme
            from (
            SELECT societe, nom_societe, sum(pli_ferme) as pli_ferme
              FROM public.view_pli_stat where societe::integer = ".$societe." and date_courrier::date  between '".$date1."' and '".$date2."' 
            GROUP BY societe, nom_societe
            ) non_traite
            left join(
            SELECT count(*) total
              FROM public.view_pli_stat where date_courrier::date between '".$date1."' and '".$date2."'  and societe::integer = ".$societe." ) somme on 1 = 1";

        $query = $this->ged->query($sql);
        return $query->result_array();

    }
	
	public function pliPasseEnKO($date1,$date2,$societe,$granularite = 'j')
	{
		
		if($granularite == 's')
		{
			$strGranularite = " date_part('week'::text, dt_courrier::date)  ";
		}
		else
		{
			$strGranularite = " to_char(dt_courrier, 'DD/MM/YYYY'::text) ";
		}

		$sql =" 
			select 
			".$strGranularite." AS courrier_date,
			SUM(pli_anomalie) pli_anomalie,
			SUM(pli_ko_scan) pli_ko_scan,
			SUM(pli_ko_definitif) pli_ko_definitif,
			SUM(pli_ko_inconnu) pli_ko_inconnu,
			SUM(pli_ko_src) pli_ko_src,
			SUM(pli_en_attente) pli_en_attente,
			SUM(pli_ko_circulaire) pli_ko_circulaire,
			SUM(pli_ci_envoyee) pli_ci_envoyee,
			SUM(pli_ko_ci_editee) pli_ko_ci_editee,
			SUM(pli_ok_ci) pli_ok_ci,
			SUM(pli_ko_en_cours) pli_ko_en_cours 
				from(
					select 
					id_pli,
					CASE WHEN statut_pli  in(2,3,4,5,6,7,9,10,11,12) THEN 1 ELSE 0 END AS pli_anomalie,
					CASE WHEN statut_pli  in(2) THEN 1 ELSE 0 END AS pli_ko_scan,
					CASE WHEN statut_pli  in(3) THEN 1 ELSE 0 END AS pli_ko_definitif,
					CASE WHEN statut_pli  in(4) THEN 1 ELSE 0 END AS pli_ko_inconnu,
					CASE WHEN statut_pli  in(5) THEN 1 ELSE 0 END AS pli_ko_src,
					CASE WHEN statut_pli  in(6) THEN 1 ELSE 0 END AS pli_en_attente,
					CASE WHEN statut_pli  in(7) THEN 1 ELSE 0 END AS pli_ko_circulaire,
					CASE WHEN statut_pli  in(9) THEN 1 ELSE 0 END AS pli_ko_ci_editee,
					CASE WHEN statut_pli  in(11) THEN 1 ELSE 0 END AS pli_ok_ci,
					CASE WHEN statut_pli  in(12) THEN 1 ELSE 0 END AS pli_ko_en_cours,
					dt_courrier::date,
					CASE WHEN statut_pli  in(10) THEN 1 ELSE 0 END AS pli_ci_envoyee
					 
					FROM histo_pli where statut_pli in(2,3,4,5,6,7,9,10) 
					and societe_h = ".$societe." and dt_courrier::date  BETWEEN '".$date1."' and '".$date2."'
					group by statut_pli,dt_courrier::date,id_pli
					order by id_pli ) ko_pli
					GROUP BY ".$strGranularite." order by ".$strGranularite;
		
					$query = $this->ged->query($sql);
		return $query->result();
	}

}