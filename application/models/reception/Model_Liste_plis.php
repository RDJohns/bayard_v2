<?php

class Model_Liste_plis extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;

    var $columnOrder       = array("date_courrier","date_numerisation","dt_event","lot_scan","pli","typologie","etape","statut","nb_mouvement","id_lot_saisie","modes_paiements","cmc7s","montants","rlmcs","nom_abonne","numero_abonne","id_pli","etat");

    var $columnSearch      = array("date_courrier","date_numerisation","lot_scan","pli","dt_event","typologie","nb_mouvement","etape","statut","id_lot_saisie","modes_paiements","cmc7s","montants","rlmcs","nom_abonne","numero_abonne","id_pli");
    var $order             = array("date_courrier" => 'desc');
    var $listePli          = 'view_pli_a_deverouiller';
    

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }


    public function recordsTotal($date1,$date2,$societe)
    {
        $sqlPli = "(select *from f_liste_pli_new('".$date1."','".$date2."',$societe) as tab)";
        $this->ged->select($this->columnOrder)->from("(select *from f_liste_pli_new('".$date1."','".$date2."',$societe) ) as tab");
        return $this->ged->count_all_results(); 
    }

  public function recordsFiltered($date1,$date2,$societe)
  {
      $this->_get_query($date1,$date2,$societe);
      $query = $this->ged->get();
      return $query->num_rows();
  }
  public function exportListePli($date1,$date2,$societe)
  {
  
    $sqlFinal = "SELECT *from public.f_liste_pli_new('".$date1."','".$date2."',$societe) order by date_courrier";

    $queryJournalier = $this->ged->query($sqlFinal);
    return $queryJournalier->result();
  }

  private function _get_query($date1,$date2,$societe)
    {
       
       
        
       $this->ged->select($this->columnOrder)->from("(select * from f_liste_pli_new('".$date1."','".$date2."',$societe) ) as tab");
       
       $i = 0;

        foreach ($this->columnSearch as $emp)
        {
            if(isset($_POST['search']['value']) && !empty($_POST['search']['value'])){
                $_POST['search']['value'] = $_POST['search']['value'];
            } else
                $_POST['search']['value'] = '';
            if($_POST['search']['value'])
            {
                if($i===0) // first loop
                {
                   $this->ged->group_start();
                   $this->ged->like($emp, $_POST['search']['value']);
                }
                else
                {
                   $this->ged->or_like($emp, $_POST['search']['value']);
                }

                if(count($this->columnSearch) - 1 == $i) //last loop
                   $this->ged->group_end();
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
           $this->ged->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
           $this->ged->order_by(key($order), $order[key($order)]);
        }
    }

    public function getListePli($date1,$date2,$societe)
    {
        $this->_get_query($date1,$date2,$societe);
        if(isset($_POST['length']) && $_POST['length'] < 1)
        {
            $_POST['length']= '10';
        }
        else
            $_POST['length']= $_POST['length'];

        if(isset($_POST['start']) && $_POST['start'] > 1)
        {
            $_POST['start']= $_POST['start'];
        }
        $this->ged->limit($_POST['length'], $_POST['start']);
        $query = $this->ged->get();

        return $query->result();
    }


    
    
  

}