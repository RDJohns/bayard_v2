<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_batch extends CI_Model{
    private $CI;

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    function getCba($length, $start, $clause_where)
    {
        $where_limit = "";
        if($length != -1) {
            if($length != null and $start != null) {

                $where_limit = "
                    LIMIT ".$length."
                    OFFSET ".$start."
                ";
            };
        } else {
            $where_limit = "
                LIMIT null
                OFFSET null
            ";
        } 
        $sql = "
        select 
            societe.nom_societe as societe,
            cba.id_pli,
            dnr_nbr as num_payeur,
            datn_end as nom_payeur,
            ctm_nbr as num_dest,
            atn_end as nom_dest,
            itm_num as code_produit,
            pmo_cde as code_promo,
            date_courrier::date,
            pli,
            code_erreur as code_er,
            nom as nom_fichier,
            btch_dte::date as date_env
        from commande_cba as cba
        left join societe on societe.id = cba.societe
        left join f_pli on f_pli.id_pli = cba.id_pli
        left join f_lot_numerisation fgn_lot on fgn_lot.id_lot_numerisation = f_pli.id_lot_numerisation
        left join rejet_cba_lignes rcl on rcl.id_pli = cba.id_pli::text
        left join rejet_cba_files   rcf on rcf.id = rcl.id_file
        where 1=1 $clause_where 
        $where_limit
        ";

        $query = $this->ged->query($sql);
        $data = $query->result();
        return $data;
        //var_dump($data);die;
    }

    function count($clause_where, $length, $start)
    {
        $where_limit = "";
        if($length != -1) {
            if($length != null and $start != null) {

                $where_limit = "
                    LIMIT ".$length."
                    OFFSET ".$start."
                ";
            };
        } else {
            $where_limit = "
                LIMIT null
                OFFSET null
            ";
        } 
        $sql = "
        select 
        count (*) as nb
        from commande_cba as cba
        left join societe on societe.id = cba.societe
        left join f_pli on f_pli.id_pli = cba.id_pli
        left join f_lot_numerisation fgn_lot on fgn_lot.id_lot_numerisation = f_pli.id_lot_numerisation
        left join rejet_cba_lignes rcl on rcl.id_pli = cba.id_pli::text
        left join rejet_cba_files   rcf on rcf.id = rcl.id_file
        where 1=1 $clause_where
        ";

        $query = $this->ged->query($sql);
        $data = $query->result();
        return $data;
        //var_dump($data);die;
    }

    //getDetailBatch
    function getDetailBatch($clause_where)
    {
        $sql = "
        select
            case when SUM(pli_total)   is null then 0 else SUM(pli_total)  end AS pli_recu,
            case when SUM(pli_saisie)  is null then 0 else SUM(pli_saisie) end AS pli_saisie,
            case when SUM(pli_batch) is null then 0 else SUM(pli_batch) end AS pli_batch,
            case when SUM(saisie_directe)   is null then 0 else SUM(saisie_directe)  end AS saisie_directe,
            case when SUM(saisie_batch)  is null then 0 else SUM(saisie_batch) end AS saisie_batch,
            case when SUM(pas_retour) is null then 0 else SUM(pas_retour) end AS pas_retour_batch,
            case when SUM(ok_batch)   is null then 0 else SUM(ok_batch)  end AS ok_batch,
            case when SUM(ano_batch)  is null then 0 else SUM(ano_batch) end AS ano_batch,
            case when SUM(pas_retour) is null then 0 else SUM(pas_retour) end AS pas_retour_batch
                from
                    (
                        SELECT 
                        count (data_id_pli) as pli_total,
                        sum(flag_saisie) as pli_saisie,
                        case when flag_saisie = 1 and flag_batch=0 then count (data_id_pli) else 0 end as saisie_directe,
                        case when flag_saisie = 1 and flag_batch=1 then count (distinct data_id_pli) else 0 end as saisie_batch,
                        count(DISTINCT res.cba_id_pli)  AS pli_batch,
                        case when flag_retour_batch = 1 and flag_rejet_batch = 1 THEN Count(distinct cba_id_pli)ELSE  0 END as ano_batch,
                        CASE WHEN flag_retour_batch = 1 and flag_rejet_batch = 0 THEN Count(distinct cba_id_pli)ELSE 0 END AS ok_batch,
                        CASE WHEN flag_retour_batch = 0 and flag_batch = 1 and in_cba = 1 THEN Count(distinct cba_id_pli) ELSE 0 end as pas_retour
                    from
                    (
                        SELECT 
                            societe.nom_societe as societe,
                            commande_cba.id_pli cba_id_pli,
                            data_pli.id_pli as data_id_pli,
                            dnr_nbr as num_payeur,
                            datn_end as nom_payeur,
                            ctm_nbr as num_dest,
                            atn_end as nom_dest,
                            itm_num as code_produit,
                            pmo_cde as code_promo,
                            date_courrier::date,
                            pli,
                            code_erreur as code_er,
                            nom as nom_fichier,
                            btch_dte::date as date_env,
                            flag_retour,
                            data_pli.flag_retour_batch, 
                            data_pli.flag_rejet_batch,
                            flag_saisie,
                            flag_batch,
                            in_cba 
                        FROM   data_pli AS data_pli
                                
                                left join f_pli
                                    ON f_pli.id_pli = data_pli.id_pli
                                left join commande_cba 
                                    ON data_pli.id_pli = commande_cba.id_pli
                            left join societe
                                    ON societe.id = commande_cba.societe
                                left join f_lot_numerisation fgn_lot
                                    ON fgn_lot.id_lot_numerisation =
                                    f_pli.id_lot_numerisation
                                left join rejet_cba_lignes rcl
                                    ON rcl.id_pli = commande_cba.id_pli :: text
                                left join rejet_cba_files rcf
                                    ON rcf.id = rcl.id_file
                        where 1 = 1 $clause_where
                    )as res 
                    group by flag_retour_batch,flag_rejet_batch,flag_batch,in_cba,flag_saisie
                ) as global
        
        ";
        $query = $this->ged->query($sql);
        $data = $query->result();
        return $data;
    }

     //function get infos 
    function getInfos($colonne, $alias){
        $sql = "select $colonne as $alias from commande_cba where $colonne <> ''";
        $query = $this->ged->query($sql);
        $i = 0;
        $array = array();
        $data = $query->Result('array');
        if($data) {
            for ($i = 0; $i<sizeof($data); $i++)
            {
                $array[$i] = $data[$i][trim($alias)];
            }
        }
        return $array;
    }
    function getInfosFichier($colonne, $alias){
        $sql = "select $colonne as $alias from rejet_cba_files where $colonne <> ''";
        $query = $this->ged->query($sql);
        $i = 0;
        $array = array();
        $data = $query->Result('array');
        if($data) {
            for ($i = 0; $i<sizeof($data); $i++)
            {
                $array[$i] = $data[$i][trim($alias)];
            }
        }
        return $array;
    }
}