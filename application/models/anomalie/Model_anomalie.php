<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_anomalie extends CI_Model{

    private $CI;

    private $ged;
	private $base_64;
	private $tb_statS = TB_statS;
	
	protected $select_ano        = array('date_courrier','dt_event','date_numerisation', 'nom_societe','mode_paiement','statut','libelle',/*'type_ko'*/'nb_mvt','lot_scan','pli','id_pli','id_lot_saisie','typologie','flgtt_etape','flgtt_etat','flgtt_etat_disp','etat');
    protected $column_order_ano  = array('date_courrier','dt_event','date_numerisation','nom_societe','mode_paiement','statut','libelle','type_ko','nb_mvt','lot_scan','pli','id_pli','id_lot_saisie');
    protected $column_search_ano = array('date_courrier','dt_event','date_numerisation','nom_societe','mode_paiement','statut','libelle','type_ko','nb_mvt','lot_scan','pli','id_pli','id_lot_saisie');
    protected $order_ano         = array('nom_societe'=> 'asc','date_courrier'=> 'asc','date_numerisation'=> 'asc','dt_event'=> 'asc','lot_scan'=> 'asc','pli'=> 'asc');
	
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }
	
	/*public function get_statut_traitement(){
        return $res_type_doc = $this->base_64->select('*')
            ->from('flag_traitement')
            ->where('actif_extract = ', '1')
            ->order_by('comment asc')
            ->get()
            ->result();
    }
	public function get_statut_saisie(){
        return $res_type_doc = $this->ged->select('*')
            ->from('statut_saisie')
            ->where('actif = ', '1')
            ->order_by('libelle asc')
            ->get()
            ->result();
    }*/
	
	 public function get_statut_traitement(){
		
		$sql = "SELECT *
						FROM flag_traitement 
						WHERE  1=1 /*actif_extract = 1 */
				        ORDER BY ordre_affichage asc";		
		return $this->base_64->query($sql)
							->result_array();
    }
	
	 public function get_statut_saisie(){
		
		$sql = "SELECT id_statut_saisie,libelle
						FROM statut_saisie 
						ORDER BY id_statut_saisie asc";		
		return $this->ged->query($sql)
							->result_array();
    }
	
	public function get_mode_paiement(){
		
		$sql = "SELECT id_mode_paiement,mode_paiement
						FROM mode_paiement 
						ORDER BY id_mode_paiement asc";		
		return $this->ged->query($sql)
							->result_array();
    }
	
    public function get_anomalie_pli($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$extract){
		
			if($extract == 1) //par date de courrier et date de traitement
			{
				$sql="SELECT
					date_courrier,dt_event,date_numerisation,nom_societe,mode_paiement,statut,libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
					from (	
					SELECT
						date_courrier,dt_event,date_numerisation,nom_societe,
						string_agg(distinct mode_paiement,' + ') as mode_paiement,
						case when statut = libelle then 'Cloturé' else statut end as statut,
						libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,
						string_agg(distinct id_mp::text,', ') as id_mp,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						FROM ( 
							SELECT 
								f_pli.id_pli, 
								f.flag_traitement,
								substring(f.dt_event::text from 0 for 11) as dt_event,
								f_flag_traitement.traitement as statut,
								f_pli.typage_par,
								f_lot_numerisation.lot_scan, 
								substring(f_lot_numerisation.date_courrier::text from 0 for 11) as date_courrier,
								substring(f_lot_numerisation.date_numerisation::text from 0 for 11) as date_numerisation,
								f_pli.pli, 
								societe.societe as id_societe,
								societe.nom_societe,				
								data_pli.statut_saisie,
								statut_saisie.libelle,				
								societe.nom_societe as titre_societe_nom,
								f_pli.id_lot_saisie,
								view_pli_avec_cheque_new.id_mode_paiement,
								mode_paiement.mode_paiement,
								mode_paiement.id_mode_paiement as id_mp,
								nb_mvt,
								typologie.typologie,
								f_flag_traitement.flgtt_etape,
								f_flag_traitement.flgtt_etat,
								CASE WHEN (flgtt_flag_attente = 1 AND par_ttmt_ko = 1) THEN 'A traiter' ELSE flgtt_etat END as  flgtt_etat_disp,
								CASE WHEN flag_rewrite_status_client <> 0 THEN ' ' ELSE statut_saisie.libelle END as etat		
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN f_histo_pli_by_last_date_event(".$where_f_ttt.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN f_histo_pli_by_date_courrier(".$where_f_courrier.") f_courrier ON f_pli.id_pli = f_courrier.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli and f.id = histo_pli.id
							INNER JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
							LEFT JOIN paiement on paiement.id_pli = f.id_pli  
							/*LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1*/
							LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement			
							LEFT JOIN societe on societe.id = data_pli.societe
							left join typologie on typologie.id = 	data_pli.typologie				
							/*LEFT JOIN view_statut_pli ON view_statut_pli.id_pli = f.id_pli*/
							LEFT JOIN view_nb_mouvement_pli ON view_nb_mouvement_pli.id_pli = f.id_pli
							LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = f_pli.id_pli
							left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement 
							WHERE 
							1=1  
							".$where_date_courrier_ttt."
							".$where_other."
							
						) as res1
						
					group by res1.id_pli,date_courrier,dt_event,date_numerisation,nom_societe,statut,nb_mvt,lot_scan,pli,res1.id_pli,id_lot_saisie,res1.statut_saisie,libelle,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
				)as res2
				WHERE 1= 1 ".$where_mode_paie."
				order by id_pli asc
				";
			}
			if($extract == 2) //recherche par date de courrier
			{
				$sql="SELECT
					date_courrier,dt_event,date_numerisation,nom_societe,mode_paiement,statut,libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
					from (
					SELECT
						date_courrier,dt_event,date_numerisation,nom_societe,
						string_agg(distinct mode_paiement,' + ') as mode_paiement,
						case when statut = libelle then 'Cloturé' else statut end as statut,
						libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,
						string_agg(distinct id_mp::text,', ') as id_mp,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						FROM ( 
							SELECT 
								f_pli.id_pli::text, 
								f.flag_traitement,
								substring(f.dt_event::text from 0 for 11) as dt_event,
								f_flag_traitement.traitement as statut,
								f_pli.typage_par,
								f_lot_numerisation.lot_scan, 
								substring(f_lot_numerisation.date_courrier::text from 0 for 11) as date_courrier,
								substring(f_lot_numerisation.date_numerisation::text from 0 for 11) as date_numerisation,
								f_pli.pli, 
								societe.societe as id_societe,
								societe.nom_societe,				
								data_pli.statut_saisie,
								statut_saisie.libelle,				
								societe.nom_societe as titre_societe_nom,
								f_pli.id_lot_saisie,
								view_pli_avec_cheque_new.id_mode_paiement,
								mode_paiement.mode_paiement,
								mode_paiement.id_mode_paiement as id_mp,
								nb_mvt,
								typologie.typologie,
								f_flag_traitement.flgtt_etape,
								f_flag_traitement.flgtt_etat,
								CASE WHEN (flgtt_flag_attente = 1 AND par_ttmt_ko = 1) THEN 'A traiter' ELSE flgtt_etat END as  flgtt_etat_disp,
								CASE WHEN flag_rewrite_status_client <> 0 THEN ' ' ELSE statut_saisie.libelle END as etat									
							
							FROM f_histo_pli_by_date_courrier(".$where_f_courrier.") f_courrier  
								left JOIN f_pli  ON f_pli.id_pli = f_courrier.id_pli 
								left JOIN view_histo_pli_oid f ON f .id_pli  = f_pli.id_pli
								left JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation			
								LEFT JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN societe on societe.id = data_pli.societe
								left join typologie on typologie.id = 	data_pli.typologie	
								INNER JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN paiement on paiement.id_pli = f.id_pli  
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement				
								LEFT JOIN view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = data_pli.id_pli
								LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli  
								left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement 
							  
							WHERE 
								1=1  
								".$where_date_courrier."
								".$where_other."
								
							) as res1
							
						group by res1.id_pli,date_courrier,dt_event,date_numerisation,nom_societe,statut,nb_mvt,lot_scan,pli,res1.id_pli,id_lot_saisie,res1.statut_saisie,libelle,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
					)as res2
					WHERE 1= 1 ".$where_mode_paie."
					order by id_pli asc
				";
			}
			if($extract == 3) //recherche par date de traitement
			{
				$sql="SELECT
					date_courrier,dt_event,date_numerisation,nom_societe,mode_paiement,statut,libelle,type_ko,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat	
					from (
					SELECT
						date_courrier,dt_event,date_numerisation,nom_societe,
						string_agg(distinct mode_paiement,' + ') as mode_paiement,
						case when statut = libelle then 'Cloturé' else statut end as statut,
						libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,
						string_agg(distinct id_mp::text,', ') as id_mp,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						FROM ( 
								SELECT 
									f_pli.id_pli::text, 
									f.flag_traitement,
									substring(f.dt_event::text from 0 for 11) as dt_event,
									f_flag_traitement.traitement as statut,
									f_pli.typage_par,
									f_lot_numerisation.lot_scan, 
									substring(f_lot_numerisation.date_courrier::text from 0 for 11) as date_courrier,
									substring(f_lot_numerisation.date_numerisation::text from 0 for 11) as date_numerisation,
									f_pli.pli, 
									societe.societe as id_societe,
									societe.nom_societe,				
									data_pli.statut_saisie,
									statut_saisie.libelle,				
									societe.nom_societe as titre_societe_nom,
									f_pli.id_lot_saisie,
									view_pli_avec_cheque_new.id_mode_paiement,
									mode_paiement.mode_paiement,
									mode_paiement.id_mode_paiement as id_mp,
									typologie.typologie,
									f_flag_traitement.flgtt_etape,
									f_flag_traitement.flgtt_etat,
									CASE WHEN (flgtt_flag_attente = 1 AND par_ttmt_ko = 1) THEN 'A traiter' ELSE flgtt_etat END as  flgtt_etat_disp,
									CASE WHEN flag_rewrite_status_client <> 0 THEN ' ' ELSE statut_saisie.libelle END as etat,	
									nb_mvt
								FROM f_pli 		
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								LEFT JOIN f_histo_pli_by_last_date_event(".$where_f_ttt.") f ON f.id_pli  = f_pli.id_pli
								INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli and f.id = histo_pli.id
									
										
								INNER JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN paiement on paiement.id_pli = f.id_pli  
								/*LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1*/
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement			
								LEFT JOIN societe on societe.id = data_pli.societe	
								left join typologie on typologie.id = 	data_pli.typologie			
								/*LEFT JOIN view_statut_pli ON view_statut_pli.id_pli = f.id_pli*/
								LEFT JOIN view_nb_mouvement_pli ON view_nb_mouvement_pli.id_pli = f.id_pli
								LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = f_pli.id_pli
								left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement 
								WHERE 
								1=1  
								".$where_date_courrier_ttt."
								".$where_other."
								
							) as res1
							
						group by res1.id_pli,date_courrier,dt_event,date_numerisation,nom_societe,statut,nb_mvt,lot_scan,pli,res1.id_pli,id_lot_saisie,res1.statut_saisie,libelle,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						)as res2
					WHERE 1= 1 ".$where_mode_paie."
					order by id_pli asc

				";
			}
				
				/*echo "<pre>";
				print_r($sql);
				echo "</pre>";exit;
				*/
		return $this->ged->query($sql)
							->result_array();
    }
	
	

	public function get_datatables_anomalie($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$length,$start,$post_order,$search,$post_order_col,$post_order_dir,$extract){
        $this->get_datatable_ano_sql($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$post_order,$search,$post_order_col,$post_order_dir,$extract );
        if($length != -1)
         $this->ged->limit($length, $start);
        $query = $this->ged->get();
        return $query->result();
    }

    public function count_filtered_anomalie($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$post_order,$search,$post_order_col,$post_order_dir,$extract)
    {
        $this->get_datatable_ano_sql($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$post_order,$search,$post_order_col,$post_order_dir,$extract);
		
        $query = $this->ged->get();
        return $query->num_rows();
		
    }

    public function count_all_anomalie($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$extract)
    { 
		if($extract == 1) //par date de courrier et date de traitement
			{
				$this->ged->select($this->select_ano)
                ->from("(
					SELECT
						date_courrier,dt_event,date_numerisation,nom_societe,
						string_agg(distinct mode_paiement,' + ') as mode_paiement,
						statut,
						libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,
						string_agg(distinct id_mp::text,', ') as id_mp,typologie	
						FROM ( 
							SELECT 
								f_pli.id_pli::text, 
								f.flag_traitement,
								substring(f.dt_event::text from 0 for 11) as dt_event,
								f_flag_traitement.traitement as statut,
								f_pli.typage_par,
								f_lot_numerisation.lot_scan, 
								substring(f_lot_numerisation.date_courrier::text from 0 for 11) as date_courrier,
								substring(f_lot_numerisation.date_numerisation::text from 0 for 11) as date_numerisation,
								f_pli.pli, 
								societe.societe as id_societe,
								societe.nom_societe,				
								data_pli.statut_saisie,
								statut_saisie.libelle,				
								societe.nom_societe as titre_societe_nom,
								f_pli.id_lot_saisie,
								view_pli_avec_cheque_new.id_mode_paiement,
								mode_paiement.mode_paiement,
								mode_paiement.id_mode_paiement as id_mp,
								nb_mvt,
								typologie.typologie	
								
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN f_histo_pli_by_last_date_event(".$where_f_ttt.") f ON f.id_pli  = f_pli.id_pli
							LEFT JOIN f_histo_pli_by_date_courrier(".$where_f_courrier.") f_courrier ON f_pli.id_pli = f_courrier.id_pli
							LEFT JOIN histo_pli on f.id_pli = histo_pli.id_pli and f.id = histo_pli.id
							INNER JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
							LEFT JOIN paiement on paiement.id_pli = f.id_pli  
							/*LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1*/
							LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement			
							LEFT JOIN societe on societe.id = data_pli.societe		
							left join typologie on typologie.id = 	data_pli.typologie	
							/*LEFT JOIN view_statut_pli ON view_statut_pli.id_pli = f.id_pli*/
							LEFT JOIN view_nb_mouvement_pli ON view_nb_mouvement_pli.id_pli = f.id_pli
							LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = f_pli.id_pli
							left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement  
							WHERE 
							1=1  
							".$where_date_courrier_ttt."
							".$where_other."
							
						) as res1						
						group by res1.id_pli,date_courrier,dt_event,date_numerisation,nom_societe,statut,nb_mvt,lot_scan,pli,res1.id_pli,id_lot_saisie,res1.statut_saisie,libelle,typologie
						)as res2 
						WHERE 1= 1 ".$where_mode_paie."
					 ");
			}
		if($extract == 2) //recherche par date de courrier
			{
				$this->ged->select($this->select_ano)
                ->from("(
						SELECT
						date_courrier,dt_event,date_numerisation,nom_societe,
						string_agg(distinct mode_paiement,' + ') as mode_paiement,
						statut,
						libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,
						string_agg(distinct id_mp::text,', ') as id_mp,typologie	
						FROM ( 
							SELECT 
								f_pli.id_pli::text, 
								f.flag_traitement,
								substring(f.dt_event::text from 0 for 11) as dt_event,
								f_flag_traitement.traitement as statut,
								f_pli.typage_par,
								f_lot_numerisation.lot_scan, 
								substring(f_lot_numerisation.date_courrier::text from 0 for 11) as date_courrier,
								substring(f_lot_numerisation.date_numerisation::text from 0 for 11) as date_numerisation,
								f_pli.pli, 
								societe.societe as id_societe,
								societe.nom_societe,				
								data_pli.statut_saisie,
								statut_saisie.libelle,				
								societe.nom_societe as titre_societe_nom,
								f_pli.id_lot_saisie,
								view_pli_avec_cheque_new.id_mode_paiement,
								mode_paiement.mode_paiement,
								mode_paiement.id_mode_paiement as id_mp,
								nb_mvt,
								typologie.typologie								
							
							FROM f_histo_pli_by_date_courrier(".$where_f_courrier.") f_courrier  
								left JOIN f_pli  ON f_pli.id_pli = f_courrier.id_pli 
								left JOIN view_histo_pli_oid f ON f .id_pli  = f_pli.id_pli
								left JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation			
								LEFT JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN societe on societe.id = data_pli.societe
								left join typologie on typologie.id = 	data_pli.typologie		
								left JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN paiement on paiement.id_pli = f.id_pli  
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement				
								LEFT JOIN view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = data_pli.id_pli
								LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli
								left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement  
							  
							WHERE 
								1=1  
								".$where_date_courrier."
								".$where_other."
								
							) as res1
							
						group by res1.id_pli,date_courrier,dt_event,date_numerisation,nom_societe,statut,nb_mvt,lot_scan,pli,res1.id_pli,id_lot_saisie,res1.statut_saisie,libelle,typologie
						)as res2 
							WHERE 1= 1 ".$where_mode_paie."	
                         ");
			}
		if($extract == 3) //par date de traitement
			{
				$this->ged->select($this->select_ano)
                ->from("(
					SELECT
						date_courrier,dt_event,date_numerisation,nom_societe,
						string_agg(distinct mode_paiement,' + ') as mode_paiement,
						statut,
						libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,
						string_agg(distinct id_mp::text,', ') as id_mp,typologie
						FROM ( 
								SELECT 
									f_pli.id_pli::text, 
									f.flag_traitement,
									substring(f.dt_event::text from 0 for 11) as dt_event,
									f_flag_traitement.traitement as statut,
									f_pli.typage_par,
									f_lot_numerisation.lot_scan, 
									substring(f_lot_numerisation.date_courrier::text from 0 for 11) as date_courrier,
									substring(f_lot_numerisation.date_numerisation::text from 0 for 11) as date_numerisation,
									f_pli.pli, 
									societe.societe as id_societe,
									societe.nom_societe,				
									data_pli.statut_saisie,
									statut_saisie.libelle,				
									societe.nom_societe as titre_societe_nom,
									f_pli.id_lot_saisie,
									view_pli_avec_cheque_new.id_mode_paiement,
									mode_paiement.mode_paiement,
									mode_paiement.id_mode_paiement as id_mp,
									nb_mvt::text,
									typologie.typologie
									
								FROM f_pli 		
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								LEFT JOIN f_histo_pli_by_last_date_event(".$where_f_ttt.") f ON f.id_pli  = f_pli.id_pli
								left JOIN histo_pli on f.id_pli = histo_pli.id_pli and f.id = histo_pli.id
									
										
								INNER JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN paiement on paiement.id_pli = f.id_pli  
								/*LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1*/
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement			
								LEFT JOIN societe on societe.id = data_pli.societe
								left join typologie on typologie.id = 	data_pli.typologie				
								/*LEFT JOIN view_statut_pli ON view_statut_pli.id_pli = f.id_pli*/
								LEFT JOIN view_nb_mouvement_pli ON view_nb_mouvement_pli.id_pli = f.id_pli
								LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = f_pli.id_pli
								left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
								WHERE 
								1=1  
								".$where_date_courrier_ttt."
								".$where_other."
								
							) as res1							
							group by res1.id_pli,date_courrier,dt_event,date_numerisation,nom_societe,statut,nb_mvt,lot_scan,pli,res1.id_pli,id_lot_saisie,res1.statut_saisie,libelle,typologie
							)as res2 
						WHERE 1= 1 ".$where_mode_paie."
                    ");
						
			}
        
					
        return $this->ged->count_all_results();
		
    }

    public function get_datatable_ano_sql($where_f_courrier,$where_f_ttt,$where_date_courrier,$where_date_courrier_ttt,$where_other,$where_mode_paie,$post_order,$search ,$post_order_col,$post_order_dir,$extract){
		//var_dump($extract);die;
         if($extract == 1) //par date de courrier et date de traitement
			{
				$this->ged->select($this->select_ano)
                ->from("(
					SELECT
						date_courrier,dt_event,date_numerisation,nom_societe,
						string_agg(distinct mode_paiement,' + ') as mode_paiement,
						statut,
						libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,
						string_agg(distinct id_mp::text,', ') as id_mp,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						FROM ( 
							SELECT 
								f_pli.id_pli::text, 
								f.flag_traitement,
								substring(f.dt_event::text from 0 for 11) as dt_event,
								f_flag_traitement.traitement as statut,
								f_flag_traitement.flgtt_etape,
								f_flag_traitement.flgtt_etat,
								CASE WHEN (flgtt_flag_attente = 1 AND par_ttmt_ko = 1) THEN 'A traiter' ELSE flgtt_etat END as  flgtt_etat_disp,
								f_pli.typage_par,
								CASE WHEN flag_rewrite_status_client <> 0 THEN ' ' ELSE statut_saisie.libelle END as etat,
								f_lot_numerisation.lot_scan, 
								substring(f_lot_numerisation.date_courrier::text from 0 for 11) as date_courrier,
								substring(f_lot_numerisation.date_numerisation::text from 0 for 11) as date_numerisation,
								f_pli.pli, 
								societe.societe as id_societe,
								societe.nom_societe,				
								data_pli.statut_saisie,
								statut_saisie.libelle,				
								societe.nom_societe as titre_societe_nom,
								f_pli.id_lot_saisie,
								view_pli_avec_cheque_new.id_mode_paiement,
								mode_paiement.mode_paiement,
								mode_paiement.id_mode_paiement as id_mp,
								nb_mvt,
								f_pli.societe as societe,
								typologie.typologie
								
								
							FROM f_pli 		
							INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
							INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
							LEFT JOIN f_histo_pli_by_last_date_event(".$where_f_ttt.") f ON f.id_pli  = f_pli.id_pli
							INNER JOIN f_histo_pli_by_date_courrier(".$where_f_courrier.") f_courrier ON f_pli.id_pli = f_courrier.id_pli
							INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli and f.id = histo_pli.id
							INNER JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
							LEFT JOIN paiement on paiement.id_pli = f.id_pli  
							/*LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1*/
							LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement			
							LEFT JOIN societe on societe.id = data_pli.societe	
							left join typologie on typologie.id = 	data_pli.typologie	
							/*LEFT JOIN view_statut_pli ON view_statut_pli.id_pli = f.id_pli*/
							LEFT JOIN view_nb_mouvement_pli ON view_nb_mouvement_pli.id_pli = data_pli.id_pli
							LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = f_pli.id_pli
							left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement 
							WHERE 
							1=1  
							".$where_date_courrier_ttt."
							".$where_other."
							
						) as res1						
						group by res1.id_pli,date_courrier,dt_event,date_numerisation,nom_societe,statut,nb_mvt,lot_scan,pli,res1.id_pli,id_lot_saisie,res1.statut_saisie,libelle,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						)as res2 
						WHERE 1= 1 ".$where_mode_paie."
				");
			}
		if($extract == 2) //recherche par date de courrier
			{
				$this->ged->select($this->select_ano)
                ->from("(
						SELECT
						date_courrier,dt_event,date_numerisation,nom_societe,
						string_agg(distinct mode_paiement,' + ') as mode_paiement,
						statut,
						libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,
						string_agg(distinct id_mp::text,', ') as id_mp,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						FROM ( 
							SELECT 
								f_pli.id_pli::text, 
								f.flag_traitement,
								substring(f.dt_event::text from 0 for 11) as dt_event,
								f_flag_traitement.traitement as statut,
								f_flag_traitement.flgtt_etape,
								f_flag_traitement.flgtt_etat,
								CASE WHEN (flgtt_flag_attente = 1 AND par_ttmt_ko = 1) THEN 'A traiter' ELSE flgtt_etat END as  flgtt_etat_disp,
								CASE WHEN flag_rewrite_status_client <> 0 THEN ' ' ELSE statut_saisie.libelle END as etat,
								f_pli.typage_par,
								f_lot_numerisation.lot_scan, 
								substring(f_lot_numerisation.date_courrier::text from 0 for 11) as date_courrier,
								substring(f_lot_numerisation.date_numerisation::text from 0 for 11) as date_numerisation,
								f_pli.pli, 
								societe.societe as id_societe,
								societe.nom_societe,				
								data_pli.statut_saisie,
								statut_saisie.libelle,				
								societe.nom_societe as titre_societe_nom,
								f_pli.id_lot_saisie,
								view_pli_avec_cheque_new.id_mode_paiement,
								mode_paiement.mode_paiement,
								mode_paiement.id_mode_paiement as id_mp,
								nb_mvt,								
								f_pli.societe,
								typologie.typologie
							FROM f_histo_pli_by_date_courrier(".$where_f_courrier.") f_courrier  
								left  JOIN f_pli  ON f_pli.id_pli = f_courrier.id_pli 
								left JOIN view_histo_pli_oid f ON f .id_pli  = f_pli.id_pli
								left JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation			
								LEFT JOIN data_pli on f_pli.id_pli = data_pli.id_pli 
								LEFT JOIN societe on societe.id = data_pli.societe
								left join typologie on typologie.id = 	data_pli.typologie	
								left JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN paiement on paiement.id_pli = f.id_pli  
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement
								LEFT JOIN view_nb_mouvement_pli on view_nb_mouvement_pli.id_pli = data_pli.id_pli
								LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = data_pli.id_pli
								left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
								
  
							  
							WHERE 
								1=1  
								".$where_date_courrier."
								".$where_other."
								
							) as res1
							
						group by res1.id_pli,date_courrier,dt_event,date_numerisation,nom_societe,statut,nb_mvt,lot_scan,pli,res1.id_pli,id_lot_saisie,res1.statut_saisie,libelle,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						)as res2 
							WHERE 1= 1 ".$where_mode_paie."	
                ");
			}
		if($extract == 3) //par date de traitement
			{
				$this->ged->select($this->select_ano)
                ->from("(
					SELECT
						date_courrier,dt_event,date_numerisation,nom_societe,
						string_agg(distinct mode_paiement,' + ') as mode_paiement,
						statut,
						libelle,nb_mvt,lot_scan,pli,id_pli,id_lot_saisie,
						string_agg(distinct id_mp::text,', ') as id_mp,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
						FROM ( 
								SELECT 
									f_pli.id_pli::text, 
									f.flag_traitement,
									substring(f.dt_event::text from 0 for 11) as dt_event,
									f_flag_traitement.traitement as statut,
									f_flag_traitement.flgtt_etape,
									f_flag_traitement.flgtt_etat,
									CASE WHEN (flgtt_flag_attente = 1 AND par_ttmt_ko = 1) THEN 'A traiter' ELSE flgtt_etat END as  flgtt_etat_disp,
									CASE WHEN flag_rewrite_status_client <> 0 THEN ' ' ELSE statut_saisie.libelle END as etat,
									f_pli.typage_par,
									f_lot_numerisation.lot_scan, 
									substring(f_lot_numerisation.date_courrier::text from 0 for 11) as date_courrier,
									substring(f_lot_numerisation.date_numerisation::text from 0 for 11) as date_numerisation,
									f_pli.pli, 
									societe.societe as id_societe,
									societe.nom_societe,				
									data_pli.statut_saisie,
									statut_saisie.libelle,				
									societe.nom_societe as titre_societe_nom,
									f_pli.id_lot_saisie,
									view_pli_avec_cheque_new.id_mode_paiement,
									mode_paiement.mode_paiement,
									mode_paiement.id_mode_paiement as id_mp,
									nb_mvt,
									f_pli.societe as societe
									typologie.typologie
								FROM f_pli 		
								INNER JOIN data_pli on f_pli.id_pli = data_pli.id_pli
								INNER JOIN f_lot_numerisation ON f_pli.id_lot_numerisation = f_lot_numerisation.id_lot_numerisation
								LEFT JOIN f_histo_pli_by_last_date_event(".$where_f_ttt.") f ON f.id_pli  = f_pli.id_pli
								INNER JOIN histo_pli on f.id_pli = histo_pli.id_pli and f.id = histo_pli.id
									
										
								INNER JOIN statut_saisie on statut_saisie.id_statut_saisie = data_pli.statut_saisie
								LEFT JOIN paiement on paiement.id_pli = f.id_pli  
								/*LEFT JOIN paiement_cheque on paiement_cheque.id_pli = paiement.id_pli and paiement.id_mode_paiement = 1*/
								LEFT JOIN mode_paiement on mode_paiement.id_mode_paiement = paiement.id_mode_paiement			
								LEFT JOIN societe on societe.id = data_pli.societe	
								left join typologie on typologie.id = 	data_pli.typologie			
								/*LEFT JOIN view_statut_pli ON view_statut_pli.id_pli = f.id_pli*/
								LEFT JOIN view_nb_mouvement_pli ON view_nb_mouvement_pli.id_pli = data_pli.id_pli
								LEFT JOIN view_pli_avec_cheque_new on view_pli_avec_cheque_new.id_pli = f_pli.id_pli
								left join f_flag_traitement on f_flag_traitement.id_flag_traitement = f_pli.flag_traitement
								
								

								WHERE 
								1=1  
								".$where_date_courrier_ttt."
								".$where_other."
								
							) as res1							
							group by res1.id_pli,date_courrier,dt_event,date_numerisation,nom_societe,statut,nb_mvt,lot_scan,pli,res1.id_pli,id_lot_saisie,res1.statut_saisie,libelle,typologie,flgtt_etape,flgtt_etat,flgtt_etat_disp,etat
							)as res2 
						WHERE 1= 1 ".$where_mode_paie."
                ");
						
			}
        
				
	

        $i = 0;
        foreach ($this->column_search_ano as $item) 
        {
            if($search)
            {
                if($i===0)
                {
                    $this->ged->group_start();
                    $this->ged->like($item, $search);
                }
                else
                {
                    $this->ged->or_like($item, $search);
                }

                if(count($this->column_search_ano) - 1 == $i)
                    $this->ged->group_end();
            }
            $i++;
        }

        if(isset($post_order)) 
        {
            $this->ged->order_by($this->column_order_ano[$post_order_col], $post_order_dir);
        }
        elseif (isset($this->order_ano))
        {
            $order = $this->order_ano;
            $this->ged->order_by(key($order), $order[key($order)]);
        }
	}
	
	//get statut saisie
	public function statut_saisie(){
		return $this->ged
			->from($this->tb_statS)
			->order_by('id_statut_saisie')
			->where('actif', 1)
			//->where('saisie', 1)
			->get()->result();
	}
	
	
}
