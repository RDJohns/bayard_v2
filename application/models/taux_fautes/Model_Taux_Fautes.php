<?php

class Model_Taux_Fautes extends CI_Model
{
    private $CI;
    private $ged;
    private $base_64;
    
    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }
    public function fautesParTypologie($date1,$date2,$societe)
    {
        $sql = "
        
            SELECT 
                champ, 
                change,
                controled total_controled, 
                case when controled = 0 then 0 else round(100.00*change/controled,2) end as taux
                  from(
                    select
                    typologie champ,
                    sum(change) change,sum(controled) controled
                    from(

                    select typo.typologie,
                    coalesce(id_pli,0) id_pli,coalesce(change,0) change,coalesce(controled,0) controled
                    from r_typologie typo left join (
                    SELECT 
                            c.id_pli,change,controled,c.societe,d.typologie
                            FROM public.saisie_ctrl_field c
                            left join data_pli d on  c.id_pli = d.id_pli
                            
                            where 
                            controled = 1 and c.societe = ".intval($societe)." 
                            and dt_saisie::date between '".$date1."' and '".$date2."' ) par_typo_ on par_typo_.typologie = typo.id
                    ) typologie group by typologie order by typologie ) par_typologie ";
        
        $query = $this->ged->query($sql);
        return $query->result();
    }
    public function fautesParChamp($date1,$date2,$societe)
    {
        $sql = "
            select 
                champ,
                change, 
                round((change*1.00/controled)*100,2) taux,
                controled total_controled
            from(
            SELECT 
                regexp_replace(upper(name_field), '[^[:alnum:]]+', ' ', 'g') champ,sum(change) change,sum(controled) controled
                FROM public.saisie_ctrl_field
                where 
                controled = 1 
                    and societe = ".intval($societe)."
                    and dt_saisie::date between '".$date1."' and '".$date2."' 
                group by name_field 
                order by name_field
                ) par_champ";

        $query = $this->ged->query($sql);
        return $query->result();
        
    }
    public function fautesParOperateurGoblale($date1,$date2,$societe)
    {
        $sql= "
        select  
            saisie_par,round(change*100.00/controled,2)||'%' taux,
            change,controled,coalesce(login,'Inconnu') login
            from(
            SELECT sum(change) change,count(controled)controled, saisie_par
            FROM public.saisie_ctrl_field 
            where    
                controled = 1 
                and societe = ".intval($societe)." 
                and dt_saisie::date between '".$date1."' and '".$date2."'
            group by saisie_par order by saisie_par
            ) par_user 
            left join utilisateurs u on u.id_utilisateur = par_user.saisie_par 
            order by coalesce(login,'Inconnu') ";
        $query = $this->ged->query($sql);
        return $query->result();
    }

    public function fautesParOperateurDetaillee($date1,$date2,$societe,$granularite)
    {
        $sql= "
        select 	   dt_saisie,
                coalesce(login,'Inconnu') login,controled,	
                saisie_par,round(change*100.00/controled,2) taux,
                change
                from(
                SELECT sum(change) change,count(controled)controled, saisie_par,".$granularite." dt_saisie
                FROM public.saisie_ctrl_field 
                where    
                    controled = 1 
                    and societe = ".intval($societe)." 
                    and dt_saisie::date between '".$date1."' and '".$date2."'
                group by saisie_par,".$granularite." order by ".$granularite.",saisie_par
                ) par_user 
                left join utilisateurs u on u.id_utilisateur = par_user.saisie_par 
                order by dt_saisie ";
        $query = $this->ged->query($sql);
        return $query->result();
    }

    public function parTypologieDetaillee($date1,$date2,$societe,$granularite)
    {
        $sql = "
        select 
            round(change*100.00/controled,2) taux, 
            dt_saisie,
            typologie
                from(
        
                SELECT 
                    sum(c.controled)controled,sum(c.change)change,d.typologie,".$granularite." dt_saisie
                    FROM public.saisie_ctrl_field c
                    left join data_pli d on  c.id_pli = d.id_pli
                    
                    where 
                    controled = 1 and c.societe = ".intval($societe)." 
                    and dt_saisie::date between '".$date1."' and '".$date2."' 
                    GROUP BY d.typologie,".$granularite."  order by ".$granularite." ,d.typologie) par_typologie";
        
        $query = $this->ged->query($sql);
        return $query->result();
    }
    public function getTypologie()
    {
        $sql =" select * from v_typologie";
        $query = $this->ged->query($sql);
        return $query->result();
    }


    public function parChampDetaillee($date1,$date2,$societe,$granularite)
    {
        
        $sql = "
        select 
            name_field,
            regexp_replace(upper(name_field), '[^[:alnum:]]+', ' ', 'g') champ,
            controled, 
            round((change*1.00/controled)*100,2) taux,
            dt_saisie
        from(
        SELECT 
            name_field ,sum(change) change,sum(controled) controled,".$granularite." dt_saisie
            FROM public.saisie_ctrl_field
            where 
            controled = 1 
                and societe = ".intval($societe)."
                and dt_saisie::date between '".$date1."' and '".$date2."' 
            group by ".$granularite.",name_field 
            order by name_field
            ) par_champ";
        
        $query = $this->ged->query($sql);
        return $query->result();
    }
    public function verification($date1,$date2,$societe)
    {
        $sql = "
        SELECT  count(*) nb 
        FROM public.saisie_ctrl_field where controled = 1 and dt_saisie::date between '".$date1."' and '".$date2."' and societe = ".intval($societe);
        $query = $this->ged->query($sql);
        return $query->result_array();
    }

}