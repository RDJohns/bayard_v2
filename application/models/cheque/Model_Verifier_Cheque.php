<?php
class Model_Verifier_Cheque extends CI_Model
{

    private $CI;

    private $ged;
    
    private $paiementCchqAnoCorrige = "paiement_chq_ano_corrige";
    
    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
    
    }

    public function verificationCheque($idDoc)
    {
        $array =  $this->ged->select('*')->from($this->paiementCchqAnoCorrige)->where("id_doc",$idDoc)
                  ->order_by('id_anomalie_chq', 'asc')->get()->result_array();

        
        $arr   = array_map (function($value){
                    return $value['id_anomalie_chq'];
                } , $array);
     
        return $arr;
    }

   
   
    

}