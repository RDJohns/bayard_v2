<?php
class Model_cheque extends CI_Model
{

    private $CI;

    private $ged;
    private $base_64;
    private $base_reb_temp;

    private $tb_pli = TB_pli;
    private $f_pli = FTB_pli;
    private $tb_cheque = TB_cheque;
    private $tb_mvmnt = TB_mvmnt;
    private $tb_dataPli = TB_data_pli;
    private $tb_document = TB_document;
    private $tb_user = TB_user;
    private $tb_statut = TB_statS;
    private $tb_etat = TB_etatMatchage;
    private $tb_mTempBy = TB_matchTempBy;
    private $tb_mTempMi = TB_matchTempMi;
    private $tb_chqAnom = TB_paieChqAnom;
    private $tb_anoCorrige = TB_chequeAnoCorrige;
    private $tb_societe = TB_soc;
    private $vw_pli = Vw_pli;
    private $vw_flagttt = Vw_flgTrt;
    private $tb_chqEtat = 'etat_cheque';
    private $tb_chq = 'cheque_saisie';
    private $tb_corr = 'type_correction';
    private $tb_mBy = TB_mstRebBy;
    private $tb_mMi = TB_mstRebMi;
    private $tb_typeKO = TB_koMatchage;
    private $vw_lot = 'view_lot';
    private $vw_doc = Vw_doc;
    private $vw_mstReb = VW_mstReb;
    private $colons = array('pc.id_pli','nom_societe','pc.cmc7','pc.montant');
    private $colons_reb = array('pc.id_pli','nom_societe','lot_scan','mv.numero_payeur','mv.nom_payeur','cmc7','montant','lbl_etat_chq');
    private $colons_adv = array('date_creation','nom_societe','num_pmt','montant_paye','num_payeur','nom_payeur');
    private $sort_colons_reb = array('date_courrier','pc.id_pli','pli','nom_societe','lot_scan','flgtt_etape','flgtt_etat','statut','numero_payeur','nom_payeur','cmc7','montant::float','date_init','lbl_etat_chq');

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
        //$this->base_reb_temp = $this->load->database('base_reb_temp', TRUE);
    }

    public function get_societe()
    {
        return $this->ged->select('*')
            ->from($this->tb_societe)
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function get_etat(){
        return $this->ged->select('*')
            ->from($this->tb_chqEtat)
            ->where_in('id_etat_chq', array(1,3))
            ->order_by('id_etat_chq', 'asc')
            ->get()
            ->result();
    }

    public function get_etat_traitement(){
        return $this->ged->select('*')
            ->from($this->tb_chqEtat)
            ->where_in('id_etat_chq', array(0,1,2,3,4))
            ->order_by('id_etat_chq', 'asc')
            ->get()
            ->result();
    }

    public function get_etat_in_table(){
        return $this->ged->select('*')
            ->from($this->tb_chqEtat)
            ->where_in('id_etat_chq', array(1,2,3))
            ->order_by('id_etat_chq', 'asc')
            ->get()
            ->result();
    }

    public function query_chq(){
        return $this->ged->select('pc.id,cs.id as id_saisie,pc.id_pli,nom_societe,lot_scan,pc.cmc7,pc.montant,pc.id_etat_chq as etat,lbl_etat_chq,pc.id_doc')
            ->from($this->tb_cheque.' as pc')
            ->join($this->tb_chq.' cs','cs.cmc7 = pc.cmc7 AND cs.id_pli = pc.id_pli', 'left')
            ->join($this->vw_pli.' vp ','vp.id_pli = pc.id_pli','left')
            ->join($this->vw_lot.' vl ','vl.id_lot_numerisation = vp.id_lot_numerisation', 'left')
            ->join($this->tb_dataPli.' dp ', 'dp.id_pli = pc.id_pli', 'left')
            ->join($this->tb_societe,$this->tb_societe.'.id = dp.societe', 'left')
            ->join($this->tb_chqEtat,$this->tb_chqEtat.'.id_etat_chq = pc.id_etat_chq')
            ->where(' 1 = 1 ',null,false)
            ->get_compiled_select();
    }

    public function search_query($societe,$etat,$cmc){

        $search = "";
        $search .= $this->_search('dp.societe',$societe);
        $search .= $this->_search('pc.cmc7',$cmc);
        $search .= $this->_search('pc.id_etat_chq',$etat);
        return $search;
    }

    public function search_datatable(){
        $text   = trim($this->input->post("search[value]"));
        $search = "";
        $clause = array();
        if($text != ''){
            $search .= " AND ( ";
            foreach ($this->colons as $col){
                $rec = $this->_or_search($col,$text);
                array_push($clause,$rec);
            }
            $search .= (empty($clause))?"":implode("OR",$clause);
            $search .= " ) ";
        }
        return $search;
    }

    public function _search($col,$txt){
        $where = '';
        if($txt != ''){
            $where .= " AND ".$col."::text ilike '%".$txt."%'";
        }
        return $where;
    }

    public function _or_search($col,$txt){
        $where = '';
        if($txt != '') {
            $where .= " " . $col . "::text ilike '%" . $txt . "%' ";
        }
        return $where;
    }

    public function _order_by(){
        $col = $this->input->post('order[0][column]');
        $dir = $this->input->post('order[0][dir]');

        $order = '';
        if(!is_null($col)){
            $order .= " ORDER BY ".$this->sort_colons_reb[$col]." ".$dir;
        }

        return $order;
    }

    public function _limit_offset(){
        $limit = "";
        $length = $this->input->post('length');
        $start  = $this->input->post('start');
        if($length) $limit .= " LIMIT ".$length;
        if($start) $limit .= " OFFSET ".$start;
        return $limit;
    }

    public function get_query($societe,$etat,$cmc){
        $query = $this->query_chq();
        $query .= $this->search_query($societe,$etat,$cmc);
        $query .= $this->search_datatable();
        return $query;
    }

    public function get_chq($societe,$etat,$cmc){
        $sql = $this->get_query($societe,$etat,$cmc);
        $sql .= " AND pc.id_etat_chq IN (1,3) ";
        $sql .= $this->_limit_offset();
        return $this->ged->query($sql)->result();
    }

    public function get_nb_chq($societe,$etat,$cmc){
        $sql = $this->get_query($societe,$etat,$cmc);
        $sql .= " AND pc.id_etat_chq IN (1,3) ";
        return $this->ged->query($sql)->num_rows();
    }

    public function get_tt_chq($societe,$etat,$cmc){
        $sql = $this->get_query($societe,$etat,$cmc);
        $sql .= $this->_limit_offset();
        return $this->ged->query($sql)->result();
    }

    public function get_nb_tt_chq($societe,$etat,$cmc){
        $sql = $this->get_query($societe,$etat,$cmc);
        return $this->ged->query($sql)->num_rows();
    }

    public function fill_datatable_cheque($societe,$etat,$cmc){
        $dtable     = array();
        $draw       = $this->input->post('draw');
        $result     = $this->get_chq($societe,$etat,$cmc);
        $nb_result  = $this->get_nb_chq($societe,$etat,$cmc);
        $data = array();
        if($result){
            $i = 0;
            $liste_etat = $this->get_etat_in_table();
            foreach ($result as $res){
                $data[$i]["id_pli"]         = $res->id_pli;
                $data[$i]["nom_societe"]    = $res->nom_societe;
                $data[$i]["lot_scan"]       = $res->lot_scan;
                $data[$i]["cmc7"]           = $res->cmc7;
                $data[$i]["montant"]        = $res->montant;
                $select_etat = "<select class='form-control' id='chq-".$res->id."' onchange='change_chq(".$res->id.",".$res->etat.",".$res->id_pli.",".$res->id_doc.",".$res->id_saisie.")'>";
                foreach ($liste_etat as $ls){
                    $selected       = ($ls->id_etat_chq == $res->etat)?'selected':'';
                    $select_etat   .= "<option value='".$ls->id_etat_chq."' ".$selected.">".$ls->lbl_etat_chq."</option>";
                }
                $select_etat .= "</select>";
                $data[$i]["etat"]           = $select_etat;
                $i++;
            }
        }
        $dtable["draw"]             = $draw;
        $dtable["recordsTotal"]     = (count($result) > 0)?count($result):0;
        $dtable["recordsFiltered"]  = ($nb_result > 0)?$nb_result:0;
        $dtable["data"]             = (!empty($result))?$data:'';
        return $dtable;
    }

    public function update_etat_chq($id,$etat,$id_pli,$id_doc,$id_saisie){
        $this->ged->trans_begin();

        if((int) $etat == 2){ // si changement en attente remise, déplacer les anomalies en anomalies corrigées
            $anomalie = $this->ged->select('oid,id_anomalie_chq')
                        ->from($this->tb_chqAnom)
                        ->where('id_pli', $id_pli)
                        ->where('id_doc', $id_doc)
                        ->where('id_anomalie_chq != ', 0)
                        ->get()
                        ->result();

            if($anomalie){
                foreach ($anomalie as $ano){
                    $data = array(
                        'id_doc' => $id_doc,
                        'id_pli' => $id_pli,
                        'id_anomalie_chq' => $ano->id_anomalie_chq
                    );

                    $this->ged->insert($this->tb_anoCorrige, $data);
                }

                $this->ged->where('id_pli',$id_pli)
                            ->where('id_doc',$id_doc)
                            ->delete($this->tb_chqAnom);
            }
        }

        if($id_saisie){
            $this->ged->where('id',$id_saisie)
                        ->where('id_etat_chq != ',4)
                ->update($this->tb_chq, array('id_etat_chq' => $etat));
        }

        $chq = $this->ged->select('cmc7,lbl_etat_chq')
            ->from($this->tb_cheque)
            ->join($this->tb_chqEtat, $this->tb_chqEtat.'.id_etat_chq = '.$this->tb_cheque.'.id_etat_chq', 'left')
            ->where('id',$id)
            ->get()
            ->result(); // récupérer l'ancien statut du chèque avant modification

        $this->ged->where('id',$id)
            ->update($this->tb_cheque, array('id_etat_chq' => $etat));

        $this->CI->histo->action(131, 'Id_doc #'.$id_doc.', cmc7 :'.$chq[0]->cmc7.', ancien statut :'.$chq[0]->lbl_etat_chq, $id_pli); // historisation de l'action

        if ($this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            return false;
        }
        else
        {
            $this->ged->trans_commit();
            return true;
        }
    }

    public function fill_traitement_chq($societe,$etat,$cmc){
        $dtable     = array();
        $draw       = $this->input->post('draw');
        $result     = $this->get_tt_chq($societe,$etat,$cmc);
        $nb_result  = $this->get_nb_tt_chq($societe,$etat,$cmc);
        $data = array();
        if($result){
            $i = 0;
            $liste_etat = $this->get_etat();
            foreach ($result as $res){
                $data[$i]["id_pli"]         = $res->id_pli;
                $data[$i]["nom_societe"]    = $res->nom_societe;
                $data[$i]["lot_scan"]       = $res->lot_scan;
                $data[$i]["cmc7"]           = $res->cmc7;
                $data[$i]["montant"]        = ($res->etat != 4)?"<a onclick='change_montant(".$res->id.",".$res->id_saisie.")' id='montant-".$res->id."'>".u_montant($res->montant)."</a>":u_montant($res->montant);
                $data[$i]["etat"]           = $res->lbl_etat_chq;
                $i++;
            }
        }
        $dtable["draw"]             = $draw;
        $dtable["recordsTotal"]     = (count($result) > 0)?count($result):0;
        $dtable["recordsFiltered"]  = ($nb_result > 0)?$nb_result:0;
        $dtable["data"]             = (!empty($result))?$data:'';
        return $dtable;
    }

    public function update_montant_chq($id,$id_saisie,$montant){
        if($id_saisie){
            $this->ged->where('id',$id)
                        ->where('id_etat_chq != ',4)
                        ->update($this->tb_chq, array('montant' => $montant));
        }

        return $this->ged->where('id',$id)
                            ->where('id_etat_chq != ',4)
                            ->update($this->tb_cheque, array('montant' => $montant));
    }

    //**** KO REB ****//

    public function query_chq_ko_reb(){
        return $this->ged->select('date_courrier,pc.id,pc.id_pli,pli,nom_societe,lot_scan,flgtt_etape,flgtt_etat,
        CASE WHEN flgtt_etat = \'Traité\' OR flgtt_etat = \'Clôturé\' OR flgtt_etat = \'Contrôlé\' THEN '.$this->tb_statut.'.libelle ELSE \'\' END as statut,mv.numero_payeur,mv.nom_payeur,cmc7,montant,pc.id_etat_chq as etat,lbl_etat_chq,date_init,
        CASE WHEN ko_matchage = 1 THEN \'\' ELSE libelle_ko END AS libelle_ko')
            ->from($this->tb_chq.' as pc')
            ->join($this->vw_pli.' vp ','vp.id_pli = pc.id_pli','left')
            ->join($this->vw_lot.' vl ','vl.id_lot_numerisation = vp.id_lot_numerisation', 'left')
            ->join($this->tb_dataPli.' dp ', 'dp.id_pli = pc.id_pli', 'left')
            ->join($this->vw_flagttt.' flag_ttt ', 'flag_ttt.id_flag_traitement = dp.flag_traitement ', 'left')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = pc.id_pli', 'left')
            ->join($this->tb_societe,$this->tb_societe.'.id = dp.societe', 'left')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = dp.statut_saisie ','left')
            ->join($this->tb_chqEtat,$this->tb_chqEtat.'.id_etat_chq = pc.id_etat_chq')
            ->join($this->tb_typeKO, $this->tb_typeKO.'.id_ko = pc.ko_matchage', 'left')
            ->join($this->vw_mstReb, $this->vw_mstReb.'.num_pmt = substring(pc.cmc7,1,7)', 'inner')
            ->group_start()
                ->where('pc.ko_matchage != ',0)
                ->or_where('(pc.date_init IS NOT NULL AND pc.id_etat_chq = 2 AND pc.date_saisie < NOW()::date)',null,false)
            ->group_end()
            ->where('pc.id_etat_chq != ',4)
            ->where('pc.flag_matche',0)
            ->where($this->vw_mstReb.'.flag_matche', 0)
            ->get_compiled_select();
    }

    public function get_query_corresp($societe,$etat,$cmc){
        $query = $this->query_chq_ko_reb();
        $query .= $this->search_query($societe,$etat,$cmc);
        $query .= $this->search_datatable_reb();
        return $query;
    }

    public function _group_by_ko_reb(){
        $group = ' GROUP BY date_courrier, pc.id, pc.id_pli, pli, nom_societe, lot_scan, flgtt_etape, flgtt_etat, statut, mv.numero_payeur, mv.nom_payeur, cmc7, montant, etat, lbl_etat_chq, date_init, libelle_ko ';
        return $group;
    }

    public function get_chq_corresp($societe,$etat,$cmc){
        $sql = $this->get_query_corresp($societe,$etat,$cmc);
        $sql .= $this->_group_by_ko_reb();
        $sql .= $this->_order_by();
        $sql .= $this->_limit_offset();
        return $this->ged->query($sql)->result();
    }

    public function get_nb_chq_corresp($societe,$etat,$cmc){
        $sql = $this->get_query_corresp($societe,$etat,$cmc);
        $sql .= $this->_group_by_ko_reb();
        return $this->ged->query($sql)->num_rows();
    }

    public function search_datatable_reb(){
        $text   = trim($this->input->post("search[value]"));
        $search = "";
        $clause = array();
        if($text != ''){
            $search .= " AND ( ";
            foreach ($this->colons_reb as $col){
                $rec = $this->_or_search($col,$text);
                array_push($clause,$rec);
            }
            $search .= (empty($clause))?"":implode("OR",$clause);
            $search .= " ) ";
        }
        return $search;
    }

    public function fill_correspondance_reb($societe,$etat,$cmc){
        $dtable     = array();
        $draw       = $this->input->post('draw');
        $result     = $this->get_chq_corresp($societe,$etat,$cmc);
        $nb_result  = $this->get_nb_chq_corresp($societe,$etat,$cmc);
        $data = array();

        if($result){
            $i = 0;
            $liste_etat = $this->get_etat();
            foreach ($result as $res){
                $data[$i]["date_courrier"]  = date('d/m/Y', strtotime($res->date_courrier));
                $data[$i]["id_pli"]         = $res->id_pli;
                $data[$i]["pli"]            = $res->pli;
                $data[$i]["nom_societe"]    = '<a class="modif-ko-reb" data-toggle="tooltip" data-placement="top" title="Modifier société" onclick="show_payeur('.$res->id_pli.','.$res->id.')">'.$res->nom_societe.'</a>';
                $data[$i]["lot_scan"]       = $res->lot_scan;
                $data[$i]["etape"]          = $res->flgtt_etape;
                $data[$i]["etat"]           = $res->flgtt_etat;
                $data[$i]["statut"]         = $res->statut;
                $data[$i]["numero_payeur"]  = '<a class="modif-ko-reb" data-toggle="tooltip" data-placement="top" title="Modifier numéro payeur" onclick="show_payeur('.$res->id_pli.','.$res->id.')">'.$res->numero_payeur.'</a>';
                $data[$i]["nom_payeur"]     = $res->nom_payeur;
                $data[$i]["cmc7"]           = $res->cmc7;
                $data[$i]["montant"]        = '<a class="modif-ko-reb" data-toggle="tooltip" data-placement="top" title="Modifier montant" onclick="show_payeur('.$res->id_pli.','.$res->id.')">'.u_montant($res->montant).'</a>';
                $data[$i]["date_init"]      = date('d/m/Y H:i:s', strtotime($res->date_init));;
                $data[$i]["etat_chq"]       = $res->lbl_etat_chq;
                $data[$i]["lib_ko"]         = $res->libelle_ko;
                $i++;
            }
        }
        $dtable["draw"]             = $draw;
        $dtable["recordsTotal"]     = (count($result) > 0)?count($result):0;
        $dtable["recordsFiltered"]  = ($nb_result > 0)?$nb_result:0;
        $dtable["data"]             = (!empty($result))?$data:'';
        return $dtable;
    }

    //***** FIn KO REB ****//

    //***** KO VIPS ****//

    public function query_chq_ko_vips(){
        return $this->ged->select('date_courrier,cs.id,cs.id_pli,pli,nom_societe,lot_scan,flgtt_etape,flgtt_etat,
        CASE WHEN flgtt_etat = \'Traité\' OR flgtt_etat = \'Clôturé\' OR flgtt_etat = \'Contrôlé\' THEN '.$this->tb_statut.'.libelle ELSE \'\' END as statut,numero_payeur,nom_payeur,cs.cmc7,pc.montant,cs.id_etat_chq as etat,lbl_etat_chq,date_init')
            ->from($this->tb_chq.' as cs')
            ->join($this->tb_cheque.' pc ', 'pc.cmc7 = cs.cmc7 AND pc.id_pli = cs.id_pli', 'inner')
            ->join($this->vw_pli.' vp ','vp.id_pli = pc.id_pli','left')
            ->join($this->vw_lot.' vl ','vl.id_lot_numerisation = vp.id_lot_numerisation', 'left')
            ->join($this->vw_doc, $this->vw_doc.'.id_document = pc.id_doc ', 'left')
            ->join($this->tb_dataPli.' dp ', 'dp.id_pli = pc.id_pli', 'left')
            ->join($this->vw_flagttt.' flag_ttt ', 'flag_ttt.id_flag_traitement = dp.flag_traitement ', 'left')
            ->join('(SELECT id_pli, string_agg(numero_payeur,\';\')::text as numero_payeur,
                string_agg(nom_payeur,\';\')::text as nom_payeur, string_agg(numero_abonne,\';\')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv', 'mv.id_pli = pc.id_pli', 'left')
            ->join($this->tb_societe,$this->tb_societe.'.id = dp.societe', 'left')
            ->join($this->tb_statut, $this->tb_statut.'.id_statut_saisie = dp.statut_saisie ','left')
            ->join($this->tb_chqEtat,$this->tb_chqEtat.'.id_etat_chq = cs.id_etat_chq')
            ->where($this->vw_doc.'.ko_vips',1)
            ->get_compiled_select();
    }

    public function get_query_vips($societe,$etat,$cmc){
        $query = $this->query_chq_ko_vips();
        $query .= $this->search_query($societe,$etat,$cmc);
        $query .= $this->search_datatable_reb();
        return $query;
    }

    public function get_chq_vips($societe,$etat,$cmc){
        $sql = $this->get_query_vips($societe,$etat,$cmc);
        $sql .= $this->_order_by();
        $sql .= $this->_limit_offset();
        return $this->ged->query($sql)->result();
    }

    public function get_nb_chq_vips($societe,$etat,$cmc){
        $sql = $this->get_query_corresp($societe,$etat,$cmc);
        return $this->ged->query($sql)->num_rows();
    }

    public function fill_vips_reb($societe,$etat,$cmc){
        $dtable     = array();
        $draw       = $this->input->post('draw');
        $result     = $this->get_chq_vips($societe,$etat,$cmc);
        $nb_result  = $this->get_nb_chq_vips($societe,$etat,$cmc);
        $data = array();

        if($result){
            $i = 0;
            foreach ($result as $res){
                $data[$i]["date_courrier"]  = date('d/m/Y', strtotime($res->date_courrier));
                $data[$i]["id_pli"]         = $res->id_pli;
                $data[$i]["pli"]            = $res->pli;
                $data[$i]["nom_societe"]    = $res->nom_societe;
                $data[$i]["lot_scan"]       = $res->lot_scan;
                $data[$i]["etape"]          = $res->flgtt_etape;
                $data[$i]["etat"]           = $res->flgtt_etat;
                $data[$i]["statut"]         = $res->statut;
                $data[$i]["numero_payeur"]  = $res->numero_payeur;
                $data[$i]["nom_payeur"]     = $res->nom_payeur;
                $data[$i]["cmc7"]           = $res->cmc7;
                $data[$i]["montant"]        = u_montant($res->montant);
                $data[$i]["date_init"]      = date('d/m/Y', strtotime($res->date_init));
                $data[$i]["etat_chq"]       = $res->lbl_etat_chq;
                $i++;
            }
        }
        $dtable["draw"]             = $draw;
        $dtable["recordsTotal"]     = (count($result) > 0)?count($result):0;
        $dtable["recordsFiltered"]  = ($nb_result > 0)?$nb_result:0;
        $dtable["data"]             = (!empty($result))?$data:'';
        return $dtable;
    }

    //*** Extraction ***//

    public function fill_extract_ko_reb($societe,$etat,$cmc){
        $sql = "SELECT date_courrier, pc.id, pc.id_pli, pli, nom_societe, lot_scan, flgtt_etape, flgtt_etat, 
                CASE WHEN flgtt_etat = 'Traité' OR flgtt_etat = 'Contrôlé' OR flgtt_etat = 'Clôturé' THEN statut_saisie.libelle ELSE '' END as statut, mv.numero_payeur, mv.nom_payeur, cmc7, montant, pc.id_etat_chq as etat, lbl_etat_chq 
                FROM cheque_saisie as pc 
                LEFT JOIN view_pli vp ON vp.id_pli = pc.id_pli 
                LEFT JOIN view_lot vl ON vl.id_lot_numerisation = vp.id_lot_numerisation 
                LEFT JOIN data_pli dp ON dp.id_pli = pc.id_pli 
                LEFT JOIN view_flag_traitement flag_ttt ON flag_ttt.id_flag_traitement = dp.flag_traitement 
                LEFT JOIN (
                SELECT id_pli, string_agg(numero_payeur,';')::text as numero_payeur, string_agg(nom_payeur,';')::text as nom_payeur, string_agg(numero_abonne,';')::text as numero_abonne 
                FROM mouvement GROUP BY id_pli) mv ON mv.id_pli = pc.id_pli 
                LEFT JOIN societe ON societe.id = dp.societe 
                LEFT JOIN statut_saisie ON statut_saisie.id_statut_saisie = dp.statut_saisie 
                LEFT JOIN etat_cheque ON etat_cheque.id_etat_chq = pc.id_etat_chq 
                INNER JOIN mst_reb ON mst_reb.num_pmt = substring(cmc7,1,7) 
                WHERE (
                  pc.ko_matchage != 0
                  OR (pc.date_init IS NOT NULL AND pc.id_etat_chq = 2 AND pc.date_saisie::date < NOW()::date)
                )
                AND pc.id_etat_chq != 4 
                AND pc.flag_matche = 0 
                AND mst_reb.flag_matche = 0 ";

        $sql .= $this->search_query($societe,$etat,$cmc);
        $sql .= " GROUP BY date_courrier, pc.id, pc.id_pli, pli, nom_societe, lot_scan, flgtt_etape, flgtt_etat, statut, mv.numero_payeur, mv.nom_payeur, cmc7, montant, etat, lbl_etat_chq, date_init ";

        return $this->ged->query($sql)->result();
    }

    public function fill_extract_ko_vips($societe,$etat,$cmc){
        $sql = "SELECT date_courrier, pc.id, pc.id_pli, pli, nom_societe, lot_scan, flgtt_etape, flgtt_etat, 
                CASE WHEN flgtt_etat = 'Traité' OR flgtt_etat = 'Clôturé' OR flgtt_etat = 'Contrôlé' THEN statut_saisie.libelle ELSE '' END as statut, numero_payeur, nom_payeur, cmc7, montant, pc.id_etat_chq as etat, lbl_etat_chq 
                FROM paiement_cheque as pc 
                LEFT JOIN view_pli vp ON vp.id_pli = pc.id_pli 
                LEFT JOIN view_lot vl ON vl.id_lot_numerisation = vp.id_lot_numerisation 
                LEFT JOIN view_document ON view_document.id_document = pc.id_doc 
                LEFT JOIN data_pli dp ON dp.id_pli = pc.id_pli 
                LEFT JOIN view_flag_traitement flag_ttt ON flag_ttt.id_flag_traitement = dp.flag_traitement 
                LEFT JOIN (
                    SELECT id_pli, string_agg(numero_payeur,';')::text as numero_payeur, string_agg(nom_payeur,';')::text as nom_payeur, string_agg(numero_abonne,';')::text as numero_abonne FROM mouvement GROUP BY id_pli
                ) mv ON mv.id_pli = pc.id_pli 
                LEFT JOIN societe ON societe.id = dp.societe 
                LEFT JOIN statut_saisie ON statut_saisie.id_statut_saisie = dp.statut_saisie 
                LEFT JOIN etat_cheque ON etat_cheque.id_etat_chq = pc.id_etat_chq 
                WHERE view_document.ko_vips = 0  ";

        $sql .= $this->search_query($societe,$etat,$cmc);

        return $this->ged->query($sql)->result();
    }

    public function search_ko_adv(){
        $text   = trim($this->input->post("search[value]"));
        $search = "";
        $clause = array();
        if($text != ''){
            $search .= " AND ( ";
            foreach ($this->colons_adv as $col){
                $rec = $this->_or_search($col,$text);
                array_push($clause,$rec);
            }
            $search .= (empty($clause))?"":implode("OR",$clause);
            $search .= " ) ";
        }
        return $search;
    }

    public function get_ko_adv($societe){
        $limit          = '';
        $where          = '';
        $where_search   = '';
        $order          = '';
        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $col            = $this->input->post('order[0][column]');
        $dir            = $this->input->post('order[0][dir]');
        if($length) $limit .= " LIMIT ".$length;
        if($start) $limit .= " OFFSET ".$start;
        if(!is_null($col)){
            $order .= " ORDER BY ".$this->colons_adv[$col]." ".$dir;
        }
        if($societe) $where .= ' AND id_societe = '.$societe.' ';
        $where_search = $this->search_ko_adv();
        $sql = "SELECT id_mst,date_creation, nom_societe, id_societe,num_pmt, montant_paye, num_payeur,nom_payeur 
                FROM 
                    (
                        SELECT id as id_mst,date_creation, id_societe,num_pmt, montant_paye, num_payeur,nom_payeur FROM mst_reb_advantage_bayard WHERE flag_matche = 0 AND flag_correction IS NULL 
                        union 
                        SELECT id as id_mst,date_creation, id_societe,num_pmt, montant_paye, num_payeur,nom_payeur FROM mst_reb_advantage_milan WHERE flag_matche = 0 AND flag_correction IS NULL 
                    ) tab 
                LEFT JOIN societe ON societe.id = id_societe 
                WHERE 1 = 1 ".$where." ".$where_search." ".$order." ".$limit;

        return $this->ged->query($sql)->result();
    }

    public function get_nb_ko_adv($societe){
        $where = '';
        if($societe) $where .= ' AND id_societe = '.$societe.' ';

        $sql = "SELECT id_mst,date_creation, nom_societe, id_societe,num_pmt, montant_paye, num_payeur,nom_payeur 
                FROM 
                    (
                        SELECT id as id_mst,date_creation, id_societe,num_pmt, montant_paye, num_payeur,nom_payeur FROM mst_reb_advantage_bayard WHERE flag_matche = 0 AND flag_correction IS NULL 
                        union 
                        SELECT id as id_mst,date_creation, id_societe,num_pmt, montant_paye, num_payeur,nom_payeur FROM mst_reb_advantage_milan WHERE flag_matche = 0 AND flag_correction IS NULL 
                    ) tab 
                LEFT JOIN societe ON societe.id = id_societe 
                WHERE 1 = 1 ".$where;

        return $this->ged->query($sql)->num_rows();
    }

    public function fill_ko_adv($societe){
        $draw       = $this->input->post('draw');
        $result     = $this->get_ko_adv($societe);
        $nb_result  = $this->get_nb_ko_adv($societe);
        $data = array();

        if($result){
            $i = 0;
            foreach ($result as $res){
                $data[$i]["date_creation"]  = date('d/m/Y', strtotime($res->date_creation));
                $data[$i]["nom_societe"]    = $res->nom_societe;
                $data[$i]["num_pmt"]        = $res->num_pmt;
                $data[$i]["montant"]        = u_montant($res->montant_paye);
                $data[$i]["num_payeur"]     = $res->num_payeur;
                $data[$i]["nom_payeur"]     = $res->nom_payeur;
                $liste_etat = $this->get_type_correction();
                $select_etat = "<select class='form-control' id='chq-corr-".$res->id_mst."-".$res->id_societe."' onchange='change_chq_corr(".$res->id_mst.",".$res->id_societe.")'>";
                $select_etat .= "<option value=''>-- Choix --</option>";
                foreach ($liste_etat as $ls){
                    $select_etat   .= "<option value='".$ls->id."' >".$ls->libelle_correction."</option>";
                }
                $select_etat .= "</select>";
                $data[$i]["statut"]         = $select_etat;

                $i++;
            }
        }
        $dtable["draw"]             = $draw;
        $dtable["recordsTotal"]     = (count($result) > 0)?count($result):0;
        $dtable["recordsFiltered"]  = ($nb_result > 0)?$nb_result:0;
        $dtable["data"]             = (!empty($result))?$data:'';
        return $dtable;
    }

    public function get_type_correction(){
        return $this->ged->select('*')
            ->from($this->tb_corr)
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function update_statut_corr($id,$id_soc,$etat){
        $table_mst = ((int)$id_soc == 1)?$this->tb_mBy:$this->tb_mMi;

        return $this->ged->where('id', $id)
                    ->update($table_mst,array('flag_correction' => $etat));
    }

    public function get_payeur_info($id_pli,$id){
        return $this->ged->select($this->tb_mvmnt.'.id as id,numero_payeur,nom_payeur,societe,cmc7,montant')
                        ->from($this->tb_mvmnt)
                        ->join($this->tb_dataPli, $this->tb_dataPli.'.id_pli = '.$this->tb_mvmnt.'.id_pli', 'left')
                        ->join($this->tb_chq, $this->tb_chq.'.id_pli = '.$this->tb_mvmnt.'.id_pli', 'left')
                        ->where($this->tb_mvmnt.'.id_pli', $id_pli)
                        ->where($this->tb_chq.'.id', $id)
                        ->get()
                        ->result();
    }

    public function update_payeur($id_pli,$list_payeur,$societe,$id_chq,$montant){
        $this->ged->trans_begin();

        foreach ($list_payeur as $key => $value){
            $old_payeur = $this->ged->select('numero_payeur')
                            ->from($this->tb_mvmnt)
                            ->where('id',$key)
                            ->where('id_pli', $id_pli)
                            ->get()
                            ->result();

            if(strcmp($old_payeur[0]->numero_payeur,trim($value)) !== 0){ // si la nouvelle valeur ne correspond pas à l'ancienne on enregistre les changements et historisation action
                $this->ged->where('id',$key)
                    ->where('id_pli', $id_pli)
                    ->update($this->tb_mvmnt, array('numero_payeur' => trim($value)));

                $this->CI->histo->action(128, 'ancienne valeur numéro payeur: '.$old_payeur[0]->numero_payeur, $id_pli);
            }

        }

        // vérification société
        $old_soc = $this->ged->select($this->tb_dataPli.'.societe,nom_societe')
                            ->from($this->tb_dataPli)
                            ->join($this->tb_societe,$this->tb_societe.'.id = '.$this->tb_dataPli.'.societe', 'left')
                            ->where('id_pli', $id_pli)
                            ->get()
                            ->result();

        if($old_soc[0]->societe != (int) $societe){ // si la nouvelle societe ne correspond pas à l'ancienne on enregistre le changement et l'action
            $this->ged->where('id_pli',$id_pli)
                        ->update($this->tb_dataPli, array('societe' => $societe));

            $this->base_64->where('id_pli',$id_pli)
                ->update($this->tb_pli, array('societe' => $societe));

            $this->ged->where('id_pli',$id_pli)
                ->update($this->tb_chq, array('match_societe' => $societe));

            $this->CI->histo->action(129, 'ancienne société: '.$old_soc[0]->nom_societe, $id_pli);

        }

        // fin vérification société

        // vérification montant
        $old_mont = $this->ged->select('cmc7,montant')
            ->from($this->tb_chq)
            ->where('id', $id_chq)
            ->where('id_pli', $id_pli)
            ->get()
            ->result();

        if($old_mont[0]->montant != u_montant(trim($montant))){ // si le nouveau montant ne correspond pas à l'ancien on enregistre le changement et l'action
            $this->ged->where('id_pli',$id_pli)
                ->where('id',$id_chq)
                ->update($this->tb_chq, array('montant' => u_montant(trim($montant))));

            $this->CI->histo->action(130, 'cmc7 : '.$old_mont[0]->cmc7.' => ancien montant: '.$old_mont[0]->montant, $id_pli);

        }

        // fin vérification montant

        if ($this->ged->trans_status() === FALSE)
        {
            $this->ged->trans_rollback();
            return false;
        }
        else
        {
            $this->ged->trans_commit();
            return true;
        }

    }
}