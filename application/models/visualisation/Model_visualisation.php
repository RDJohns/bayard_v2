<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_visualisation extends CI_Model{

    private $CI;

    private $ged;
    private $base_64;

    private $vw_dataFromDoc = VW_dataFromDoc;

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged', TRUE);
        $this->base_64 = $this->load->database('base_64', TRUE);
    }

    public function get_pli($doc){

        if(!$doc){
            return false;
        }

        $res = $this->base_64->distinct('pli.*')
                    ->from('pli')
                    ->join('document', 'document.id_pli = pli.id_pli')
                    ->join('type_pli', 'type_pli.id_type_pli = pli.typologie')
                    ->where_in('document.id_document', $doc)
                    ->get()
                    ->result();

        return $res;

    }

    public function get_doc($numcmd, $numclt, $nom_clt, $prenom_clt, $num_tel, $code_postal, $statut, $date_deb, $date_fin, $length, $start, $search){
        $where_like = "";

        $resultats = array();

        if($numcmd != ''){
            $where_like .= " AND ( num_commande ilike '%".$numcmd."%' ) ";
        }

        if($numclt != ''){
            $where_like .= " AND ( num_client ilike '%".$numclt."%' ) ";
        }

        if($nom_clt != ''){
            $where_like .= " AND ( nom_client ilike '%".$nom_clt."%' ) ";
        }

        if($prenom_clt != ''){
            $where_like .= " AND ( prenom_client ilike '%".$prenom_clt."%' ) ";
        }

        if($num_tel != ''){
            $where_like .= " AND ( num_tel_client_pli ilike '%".$num_tel."%' ) ";
        }

        if($code_postal != ''){
            $where_like .= " AND ( code_postal ilike '%".$code_postal."%' ) ";
        }

        if($statut != ''){
            switch ($statut){
                case '0' :  {
                    $where_like .= " AND (( histo.flag_traitement IS NULL ) OR ( histo.flag_traitement = 0 AND vp.typage_par IS NULL )) ";
                } break;
                case '1' : {
                    $where_like .= " AND (((histo.flag_traitement = 0 OR histo.flag_traitement IS NULL) AND vp.typage_par IS NOT NULL) OR (histo.flag_traitement = 1 AND vp.traite_par IS NOT NULL)) ";
                } break;
                case '2' : {
                    $where_like .= " AND (( flag_traitement = ".$statut." AND flag_avec_bdc = 0 ) OR ( flag_traitement = ".$statut." AND flag_avec_bdc = 1 AND vp.flag_validation = 3 )) ";
                } break;
                default : {
                    $where_like .= " AND ( flag_traitement = ".$statut." ) ";
                } break;
            }
        }

        if($date_deb != '' && $date_fin != ''){
            $where_date = " AND ( date_courrier::date BETWEEN '".$date_deb."' AND '".$date_fin."' ) ";
        }
        else{
            $where_date = " ";
        }

        if($search){
            $where_search = ' AND ( 
            vp.pli ilike \'%'.$search.'%\' 
            OR lot_scan ilike \'%'.$search.'%\' 
            OR lib_type ilike \'%'.$search.'%\' 
            OR num_commande ilike \'%'.$search.'%\' 
            OR num_client ilike \'%'.$search.'%\' 
            OR nom_client ilike \'%'.$search.'%\' 
            OR prenom_client ilike \'%'.$search.'%\' 
            OR num_tel_client_pli ilike \'%'.$search.'%\' 
            OR code_postal ilike \'%'.$search.'%\' ) ';
        }
        else{
            $where_search = '';
        }

        $where_like_data = '';

        if($length){
            $where_like_data .= ' LIMIT '.$length;
        }

        if($start){
            $where_like_data .= ' OFFSET '.$start;
        }

        $nb_results = $this->count_nb_records_total($where_date, $where_like, $where_search);

        $list_result = $this->ged->query("
            SELECT vd.id_document, 
            lib_type as type_document,
            histo.flag_traitement as flag_traitement, 
            CASE
                        WHEN histo.flag_traitement IS NULL OR histo.flag_traitement = 0 AND vp.typage_par IS NULL THEN 'Non Traité'::text
                        WHEN (histo.flag_traitement = 0 OR histo.flag_traitement IS NULL) AND vp.typage_par IS NOT NULL THEN 'En cours de typage'::text
                        WHEN histo.flag_traitement = 1 AND vp.traite_par IS NULL THEN 'Typé'::text
                        WHEN histo.flag_traitement = 1 AND vp.traite_par IS NOT NULL THEN 'En cours de saisie'::text
                        WHEN (histo.flag_traitement = 2 and flag_avec_bdc = 0) OR (flag_avec_bdc = 1 and histo.flag_traitement = 2 and vp.flag_validation = 3) THEN 'Clôturé'::text
                        WHEN histo.flag_traitement = 3 THEN 'KO inconnu'::text
                        WHEN histo.flag_traitement = 4 THEN 'KO scan'::text 
                        WHEN histo.flag_traitement = 5 THEN 'KO Réclamation'::text 
                        WHEN histo.flag_traitement = 6 THEN 'KO Litige'::text
                        WHEN histo.flag_traitement = 8 THEN 'KO définitif'::text
                        WHEN histo.flag_traitement = 9 THEN 'KO call'::text
                        WHEN histo.flag_traitement = 11 THEN 'KO Circulaire'::text
                        WHEN histo.flag_traitement = 12 THEN 'KO En Attente'::text
                        ELSE ''::text
            END AS traitement,
            vp.pli, 
            lot_scan, 
            date_courrier::date, 
            num_client, 
            num_commande, 
            nom_client,
            prenom_client,
            num_tel_client_pli, 
            code_postal, 
            cmc7, 
            montant_chq
               FROM view_document vd 
               LEFT JOIN view_pli vp ON vp.id_pli = vd.id_pli 
               LEFT JOIN view_lot vl ON vl.id_pli = vp.id_pli and vl.id_lot_numerisation = vp.id_lot_numerisation 
               LEFT JOIN get_max_oid_pli_par_pli() max ON max.id_pli = vp.id_pli  
               LEFT JOIN histo_pli histo ON max.oid = histo.oid 
               LEFT JOIN (
                SELECT ident_doc.id_doc, num_client, num_commande, nom_client, prenom_client, code_postal, cmc7, montant_chq 
                FROM 
                (
                    SELECT distinct id_doc FROM data_document
                ) as ident_doc 
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as num_client FROM data_document WHERE id_champ = 1
                ) as num_client ON num_client.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as num_commande FROM data_document WHERE id_champ = 2
                ) as num_commande ON num_commande.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as nom_client FROM data_document WHERE id_champ = 3
                ) as nom_client ON nom_client.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as prenom_client FROM data_document WHERE id_champ = 23
                ) as prenom_client ON prenom_client.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as code_postal FROM data_document WHERE id_champ = 5
                ) as code_postal ON code_postal.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as cmc7 FROM data_document WHERE id_champ = 6
                ) as cmc7 ON cmc7.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as montant_chq FROM data_document WHERE id_champ = 10
                ) as montant_chq ON montant_chq.id_doc = ident_doc.id_doc 
               ) as donnees ON donnees.id_doc = vd.id_document
               WHERE 1=1 ".$where_date." ". $where_like ." ".$where_search." ORDER BY date_courrier, vp.pli, lot_scan, type_document 
               ".$where_like_data." 
                
            ")
            ->result();
   
        $resultats['nb_total'] = $nb_results;
        $resultats['list_result'] = $list_result;

        return $resultats;

    }

    public function count_nb_records_total($param1, $param2, $param3){
        $nb_doc = $this->ged
                    ->query(
                        "
            SELECT count(vd.id_document) as nb_doc
               FROM view_document vd 
               LEFT JOIN view_pli vp ON vp.id_pli = vd.id_pli 
               LEFT JOIN view_lot vl ON vl.id_pli = vp.id_pli and vl.id_lot_numerisation = vp.id_lot_numerisation
               LEFT JOIN get_max_oid_pli_par_pli() max ON max.id_pli = vp.id_pli  
               LEFT JOIN histo_pli histo ON max.oid = histo.oid 
               LEFT JOIN (
                SELECT ident_doc.id_doc, num_client, num_commande, nom_client, prenom_client, code_postal, cmc7, montant_chq 
                FROM 
                (
                    SELECT distinct id_doc FROM data_document
                ) as ident_doc 
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as num_client FROM data_document WHERE id_champ = 1
                ) as num_client ON num_client.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as num_commande FROM data_document WHERE id_champ = 2
                ) as num_commande ON num_commande.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as nom_client FROM data_document WHERE id_champ = 3
                ) as nom_client ON nom_client.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as prenom_client FROM data_document WHERE id_champ = 23
                ) as prenom_client ON prenom_client.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as code_postal FROM data_document WHERE id_champ = 5
                ) as code_postal ON code_postal.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as cmc7 FROM data_document WHERE id_champ = 6
                ) as cmc7 ON cmc7.id_doc = ident_doc.id_doc
                LEFT JOIN 
                (
                    SELECT id_doc, valeur as montant_chq FROM data_document WHERE id_champ = 10
                ) as montant_chq ON montant_chq.id_doc = ident_doc.id_doc 
               ) as donnees ON donnees.id_doc = vd.id_document
               WHERE 1=1 ".$param1." ". $param2 ." ". $param3 ." "
                    )->result();
        return $nb_doc;
    }

    public function get_one_pli($id){

        $res_pli = $this->base_64->select('*')
            ->from('pli')
            ->join('document', 'document.id_pli = pli.id_pli')
            ->join('type_pli', 'type_pli.id_type_pli = pli.typologie')
            ->join('type_document', 'type_document.id_type_document = document.type_document')
            ->where('pli.id_pli', $id)
            ->order_by('document.id_document', 'asc')
            ->get()
            ->result();

        return $res_pli;

    }

    //utiliser dans vue production
    public function get_one_doc($id){

        $res_doc = $this->base_64->select('*')
            ->from('document')
            ->join('type_document', 'type_document.id_type_document = document.type_document')
            ->where('id_document', $id)
            ->get()
            ->result();

        return $res_doc;

    }

    public function get_info_doc($id){

        $res_type_doc = $this->base_64->select('id_document, id_pli, document.type_document, td.type_document as lib_type, n_ima_base64_recto, n_ima_base64_verso')
            ->from('document')
            ->join('type_document td', 'td.id_type_document = document.type_document', 'left')
            ->where('id_document', $id)
            ->get()
            ->result();

        $res_champ = $this->ged->select('*')
            ->from('champs')
            ->join('groupes_champ gc', 'gc.id_champ = champs.id_champ')
            ->join('data_document dd', 'dd.id_champ = gc.id_champ', 'left')
            ->where('gc.id_type_document', $res_type_doc[0]->type_document)
            ->where('dd.id_doc', $id)
            ->order_by('ordre_affichage', 'asc')
            ->get()
            ->result();

        /*$res_doc = $this->ged->select('*')
            ->from('data_document')
            ->where('id_doc', $id)
            ->order_by('id_champ', 'ASC')
            ->get()
            ->result();*/

        //$tab_info = array();

        /*foreach ($res_doc as $value){
            $tab_info[$value->id_champ] = $value->valeur;
        }*/

        $resultat = array(
            'champs' => $res_champ,
            'doc' => $res_type_doc
        );

        return $resultat;

    }

    public function get_statut_traitement($where = null){
        return $res_type_doc = $this->base_64->select('*')
            ->from('flag_traitement')
            /*->where('traitement ilike ', 'KO%')*/
            ->get()
            ->result();
    }

    //methodes pour récupérer la visualisation des productions
    //en utilisant le fonction whereLike
    public function get_production($idpli = null, $plis=null, $lotScan = null, $ttrpar = null ,$statutPlis = null, $typePlis = null, $typePar = null, $datedeb = null, $datefin = null, $dateTtr = null, $dateTtrFin = null)
    {
        //détailler les clauses where pour chaque type de champs
        $where_like = "";
        $wherein = "";
        if($idpli != "") {
            $where_like .=$this->whereLike($where_like,"view_pli.id_pli", $idpli);
            $wherein = " where view_pli =".$idpli;
        }
        if($plis != "") {
            $where_like .=$this->whereLike($where_like,"view_pli.pli", $plis);
        }
        if($lotScan != "") {
            $where_like .=$this->whereLike($where_like,"view_lot.lot_scan", $lotScan);
        }
        if($statutPlis != "") {
            $where_like .=$this->whereLike($where_like,"histo_pli.flag_traitement", $statutPlis);
        }
        if($typePlis != "") {
            $where_like .=$this->whereLike($where_like,"view_pli.typologie", $typePlis);
        }
        if($typePar != "") {
            $where_like .=$this->whereLike($where_like,"user_typage.login", $typePar);
        }
        if($ttrpar != "") {
            $where_like .=$this->whereLike($where_like,"user_ttt.login", $ttrpar);
        }

        if($dateTtr != "" && $dateTtrFin != "") {
            if($where_like == ''){
                $where_like .= " (date_traitement::date BETWEEN '".$dateTtr."' AND '".$dateTtrFin."')";
            } else {
                $where_like .= " AND ( date_traitement::date BETWEEN '".$dateTtr."' AND '".$dateTtrFin."')";
            }
        }

        if($datedeb != '' && $datefin != ''){
            if($where_like == ''){
                $where_like .= " (date_courrier::date BETWEEN '".$datedeb."' AND '".$datefin."' ) ";
            }
            else{
                $where_like .= " AND ( date_courrier::date BETWEEN '".$datedeb."' AND '".$datefin."' ) ";
            }
        }

        //requetes de seleection des plis selon les clauses where
        $sql = "SELECT        
                        max_oid.oid as oid1,
                        histo_pli.oid as oid2,
                        view_pli.id_pli,
                        histo_pli.source,
                        view_pli.pli,
                        view_pli.typologie,
                        code_type_pli,
                        histo_pli.flag_traitement as id_flag_traitement,
                        histo_pli.dt_event as date_traitement,
                        user_ttt.id_utilisateur as traite_par,
                        user_ttt.login as ttr_par,
                        user_typage.id_utilisateur as typage_par,
                        user_typage.login as type_par,
                        histo_pli.flag_traitement as flag_ttt,
                        histo_pli.ctrl_flag_validation,
                        flag_avec_bdc,
                        flag_validation,
						team_pli,
						valideur,
						controleur,		
                        view_pli.typage_ok,
                        CASE
                                WHEN  (histo_pli.flag_traitement IS NULL OR histo_pli.flag_traitement = 0) AND (view_pli.typage_par IS NULL) THEN 'Non Traité'::text
                                WHEN (histo_pli.flag_traitement = 0 or histo_pli.flag_traitement is null) and user_typage.id_utilisateur is not null then 'En cours de typage'
                                WHEN (histo_pli.flag_traitement = 0 and user_ttt.id_utilisateur is null and view_pli.typage_ok = 1 or(histo_pli.flag_traitement = 1 and user_ttt.id_utilisateur is null )) then 'Typé' 
                                WHEN (histo_pli.flag_traitement = 1 and user_ttt.id_utilisateur is not null ) then 'En cours de saisie' 
                                WHEN (team_pli = 2 and flag_traitement = 2) or (flag_avec_bdc = 1 and histo_pli.flag_traitement = 2 and flag_validation = 3 and team_pli = 1) or (flag_avec_bdc = 0 and histo_pli.flag_traitement = 2 and team_pli = 1) THEN 'Clôturé'
                                WHEN (flag_avec_bdc = 1 and histo_pli.flag_traitement = 2 and team_pli = 1 and valideur is null and controleur is null) then 'Saisie terminé'
								WHEN (flag_avec_bdc = 1 and histo_pli.flag_traitement = 2 and  flag_validation < 3 and team_pli = 1 and (valideur is not null or controleur is not null )) then 'En cours de controle'
                                WHEN histo_pli.flag_traitement = 3 THEN 'KO inconnu'::text
                                WHEN histo_pli.flag_traitement = 4 THEN 'KO scan'::text
                                WHEN histo_pli.flag_traitement = 8 THEN 'KO définitif'::text
                                WHEN histo_pli.flag_traitement = 9 THEN 'KO call'::text
                                WHEN histo_pli.flag_traitement = 11 THEN 'KO Circulaire'::text
                                WHEN histo_pli.flag_traitement = 12 THEN 'KO En Attente'::text
                                ELSE ''::text
                        END as traitement,
                        view_lot.id_lot_numerisation,
                        view_lot.lot_scan,
                        date_courrier::date date_courrier,
                        string_agg(view_document.id_document::text,',') as id_document,
                        string_agg(view_document.type_document::text,',') as type_document,
                        string_agg(view_document.lib_type::text,',') as lib_type
                        
                    
                        
                FROM   view_pli
                inner join view_lot on view_pli.id_pli =  view_lot.id_pli and view_lot.id_lot_numerisation = view_pli.id_lot_numerisation
                left join histo_pli on histo_pli.id_pli = view_pli.id_pli
                
                left join utilisateurs  as user_ttt on user_ttt.id_utilisateur = histo_pli.utilisateur and histo_pli.flag_traitement = 2
                left join utilisateurs  as user_typage on user_typage.id_utilisateur = histo_pli.utilisateur /*and histo_pli.flag_traitement = 0*/
                left join view_document on view_document.id_pli = view_pli.id_pli
                left join get_max_oid_pli_par_pli() as max_oid on max_oid.id_pli = histo_pli.id_pli
                
                WHERE ".$where_like." 
                GROUP BY view_pli.id_pli,
                        view_pli.pli,
                        view_pli.typologie,
                        code_type_pli,
                        histo_pli.flag_traitement,
                        histo_pli.dt_event,
                        user_ttt.id_utilisateur,
                        ttr_par,
                        user_typage.id_utilisateur,
                        type_par,
                        flag_ttt,
                        flag_avec_bdc,
                        flag_validation,
						team_pli,
						valideur,
						controleur,						
                        view_lot.id_lot_numerisation,
                        view_lot.lot_scan,
                        date_courrier,
                        histo_pli.oid,
                        max_oid.oid,
                        histo_pli.ctrl_flag_validation,
                        histo_pli.source,
                        view_pli.typage_par,
                        view_pli.typage_ok
                order by view_lot.lot_scan,view_pli.pli,histo_pli.oid,id_document asc
        ";
        //print_r($sql);die;
        return $this->ged->query($sql)->result();
    }

    //get statut  de document(flag traitement).
    public function getFlagDoc()
    {
        return $res_flag = $this->base_64->select('*')
            ->from('type_document')
            ->where('actif = 1')
            ->get()
            ->result();
    }

    //get statut  de document(flag traitement).
    public function getTypePlis()
    {
        return $res_flag = $this->base_64->select('id_type_pli,code_type_pli')
            ->from('type_pli')
            ->where('actif = 1')
            ->get()
            ->result();
    }

    //fonction pour retourner les clauses where
    public function whereLike($where_like,$col, $value){
        if($where_like == ''){
            $where_like = " (".$col."= '".$value."') ";
        }
        else{
            $where_like = " AND (".$col."=  '".$value."' )";
        }

        return $where_like;
    }

    //get info users
    public function getUsers()
    {
        $sql = "select login from utilisateurs";
        return $this->ged->query($sql)->result();
    }
}
