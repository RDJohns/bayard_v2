<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_visu_push extends CI_Model
{
    private $CI;
    
    private $tb_dataKe = TB_dataKeFx;
	private $tb_fichierKe = TB_fichierKeFx;
	private $tb_flux = TB_flux;
    
    //private $tb_source = TB_Source;
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->ged = $this->load->database('ged_push', TRUE);
    }

     //recuperation des flux selon les criteres de recherche
    public function getFlux($where, $lenght, $start, $orderBy = "")
    {
         $where_limit = "";
         $da = new DateTime();
        // var_dump($da->format('Y-m-d'));die;
         if($lenght != -1) {
             if($lenght != null and $start != null) {
 
                 $where_limit = "
                     LIMIT ".$lenght."
                     OFFSET ".$start."
                 ";
             };
         } else {
             $where_limit = "
                 LIMIT null
                 OFFSET null
             ";
         } 
        //  if ($where == '') {
        //      $where = "and date_reception::date = '".$da->format('Y-m-d')."'";
        //  }
            $sql = "select 
                        flux.id_flux,
                        flux.sous_dossier,
                        objet_flux,
                        from_flux,
                        fromname_flux,
                        nom_fichier,
                        source.id_source,
                        source.source, 
                        societe.societe,
                        typologie.typologie,
                        statut_pli.id_statut,
                        statut_pli.statut,
                        statut_pli.etape_lib,
                        statut_pli.etat_lib,
                        statut_pli.statut_lib,
                        motif,
                        date_reception::date,
                        dern_traitement::date,
                        entite,
                        nb_abonnement nbr_abo,
                        filename_origin,
                        string_agg(distinct abon.numero_payeur,',') numero_payeur,
                        string_agg( distinct abon.numero_abonne,',') numero_abonne,
                        string_agg( distinct abon.nom_abonne,',') nom_abonne,
                        string_agg(distinct abon.nom_payeur,',') nom_payeur,
                        etat_pli.id_etat_pli,
                        etat_pli.libelle_etat_pli as etat
                    from flux 
                        left join source on flux.id_source = source.id_source
                        left join f_societe societe on flux.societe = societe.id
                        left join typologie on typologie.id_typologie = flux.typologie
                        left join statut_pli on statut_pli.id_statut = flux.statut_pli
                        left join  etat_pli ON etat_pli.id_etat_pli = flux.etat_pli_id
                        left join motif_blocage on motif_blocage.id_motif = flux.id_motif_blocage
                        left join (
                            select max(date_traitement) dern_traitement,id_flux from traitement group by id_flux
                        ) as ttr on ttr.id_flux = flux.id_flux
                        left join
                        (
                            select count(id_flux) as nbr_abo ,id_flux as flux_abo,nom_abonne,numero_abonne,nom_payeur,numero_payeur from abonnement group by id_flux,nom_abonne,numero_abonne,nom_payeur,numero_payeur
                        ) abon  on abon.flux_abo =  flux.id_flux
                    where 1 = 1 
                    ".$where." group by flux.id_flux,source.id_source,societe.societe,typologie.typologie,statut_pli.id_statut,motif_blocage.motif,ttr.dern_traitement,etat_pli.id_etat_pli,etat_pli.libelle_etat_pli $orderBy ".$where_limit;
         $query = $this->ged->query($sql);
         return $query->result();
     }

    //compter les flux
    public function getTotal($where, $lenght, $start)
    {
     
            $where_limit = "
                LIMIT null
                OFFSET null
            ";
            $da = new DateTime();
            if ($where == '') {
                $where = "and date_reception::date = '".$da->format('Y-m-d')."'";
            }
       
        $sql = "            
        select 
        count(*)
         from flux 
         left join source on flux.id_source = source.id_source
         left join f_societe societe on flux.societe = societe.id
         left join typologie on typologie.id_typologie = flux.typologie
         left join statut_pli on statut_pli.id_statut = flux.statut_pli
         left join motif_blocage on motif_blocage.id_motif = flux.id_motif_blocage
         left join f_ged_user op_typage on op_typage.id_utilisateur = flux.type_par
         left join f_ged_user op_saisi on  op_saisi.id_utilisateur = flux.saisie_par
         left join (
             select max(date_traitement) dern_traitement, id_flux from traitement group by id_flux
         ) as ttr on ttr.id_flux = flux.id_flux
                
            where 1 = 1
        ".$where." ".$where_limit;
        //var_dump($sql);die;
        $query = $this->ged->query($sql);
        return $query->result();
    }

    //get Abonnement
    public function getAbo($id)
    {
        $sql = "
        SELECT 
            id, id_flux, ttr.titre as titre_abo, nom_abonne, numero_abonne, nom_payeur, numero_payeur, 
            code_postal_abonne, code_postal_payeur, typologie_abonnement,typo.typologie as typologie_abo, 
            stat_abo.id_statut, stat_abo.statut as stat_abo, motif_ko, tarif
        FROM abonnement
        left join (
            select id_titre,titre from titre
        )ttr on ttr.id_titre = abonnement.titre
        left join 
        (
            select id_typologie,typologie from typologie
        
        ) typo on abonnement.typologie_abonnement::integer = typo.id_typologie
        left join 
        (
            select id_statut,statut from statut_abonnement
        ) stat_abo on stat_abo.id_statut = abonnement.id_statut
        
        where id_flux = ".$id;
        $query = $this->ged->query($sql);
        return $query->result();
    }

    //count abonnement
    public function count_abo($id)
    {
        $sql = "select count(*) from abonnement where id_flux = ".$id;
        $query = $this->ged->query($sql);
        return $query->result();

    }

    //pieces jointes
    public function getPieceJointe($id)
    {
        $sql = "
        SELECT id_doc_joint, id_flux, nom_fichier, extension_fichier, date_injection::date, 
        nom_fichier_origine, ajoute
        FROM public.pieces_jointes 
        where id_flux = ".$id;
        $query = $this->ged->query($sql);
        return $query->result();
    }

    //anomalies
    public function getAnomalie($id){
        $sql = "select motif_operateur_flux as anomalie,consigne_client_flux as consigne from motif_consigne_flux where id_flux = ".$id;
        $query =  $this->ged->query($sql);
        $data =  $query->result();
        // var_dump($sql);die;
        return $data;
      
    }
    
    //get info for consigne ke
	public function info_ke($id_flux){
		$data_ke_raw = $this->ged
			->from($this->tb_dataKe)
			->where('id_flux', $id_flux)
			->limit(1)
			->get()->result();
		$info_ke = new stdClass();
		$info_ke->data_ke = count($data_ke_raw) > 0 ? $data_ke_raw[0] : NULL;
		$info_ke->fichier_ke = $this->ged
			->select('*')
			->select('oid')
			->from($this->tb_fichierKe)
			->where('id_flux', $id_flux)
			->order_by('nom_orig')
			->get()->result();
		return $info_ke;
    }
    
	public function save_data_consigne_ke($data){
		try {
			$this->ged->trans_begin();
			if(count($data->deleted_oids) > 0){
				$this->ged->where_in('oid', $data->deleted_oids)->delete($this->tb_fichierKe);
			}
			if(count($data->fichier_ke) > 0){
				$this->ged->insert_batch($this->tb_fichierKe, $data->fichier_ke);
			}
			$this->ged->where('id_flux', $data->id_flux)->delete($this->tb_dataKe);
			$this->ged->insert($this->tb_dataKe, $data->data_flux_ke);
            $this->ged->where('id_flux', $data->id_flux)->set('statut_pli', 9)->update($this->tb_flux);
            $this->CI->histo->action(72, 'Edition consigne dépuis visualisation', $data->id_flux);
            $this->CI->histo->flux($data->id_flux, 'Edition consigne dépuis visualisation');
			if($this->ged->trans_status() === FALSE){
				$this->ged->trans_rollback();
				throw new Exception("Transaction failed");
			}else {
				$this->ged->trans_commit();
			}
		} catch (Exception $th) {
			$this->ged->trans_rollback();
			throw $th;
		}
	}

	public function fichier_ke($oid){
		$fichiers = $this->ged
			->from($this->tb_fichierKe)
			->where('oid', $oid)
			->get()->result();
		return count($fichiers) > 0 ? $fichiers[0] : NULL;
    }
    
    //function pour recuperer statut
    public function getEtat()
    {
        $sql = "
        select id_etat_pli, libelle_etat_pli as etat from etat_pli order by id_etat_pli
          
        ";
        $query = $this->ged->query($sql);

        return $query->result();

    }
    
}
