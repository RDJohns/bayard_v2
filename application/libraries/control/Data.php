<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Data{

    private $CI;

    public function __construct() {
        $this->CI =& get_instance();
		$this->CI->load->model('control/model_pli');
		$this->CI->load->helper('function1');
    }

    public function get_data($pli){
		$data_pli = $this->CI->model_pli->data_pli($pli->id_pli);
		$id_docs = $this->CI->model_pli->id_documents($pli->id_pli);
        if(is_null($data_pli)){
			echo('Erreur: Les donn&eacute;es du pli sont introuvables!');
			return NULL;
        }
        $view_mvmnt = '';
		$data_mvmnt = $this->CI->model_pli->mvmnt_of_pli($pli->id_pli);
		$data_titre = $this->CI->model_pli->titre($data_pli->societe, TRUE);
		foreach ($data_mvmnt as $mvmnt) {
			$view_mvmnt .= $this->get_mvmnt_view(
				array(
					'id_soc' => $data_pli->societe
					,'data_mvmnt' => $mvmnt
					,'data_titre' => $data_titre
					,'data_pli' => $data_pli
				)
			);
		}
		if(count($data_mvmnt) == 0){
			$view_mvmnt = $this->get_mvmnt_view(array('id_soc'=>$data_pli->societe,'data_titre' => $data_titre,'data_pli' => $data_pli));
        }
        $view_cheque = '';
		$data_cheque = $this->CI->model_pli->cheques($pli->id_pli);
		$anom_chqs = $this->CI->model_pli->anom_cheque();
		foreach ($data_cheque as $cheque) {
			$view_cheque .= $this->get_cheque_view(array(
				'id_pli' => $pli->id_pli
				, 'data_chq' => $cheque
				, 'id_docs' => $id_docs
				, 'anom_chqs' => $anom_chqs
			));
        }
        $view_cheque_kd = '';
		$data_cheque_kd = $this->CI->model_pli->cheques_kd($pli->id_pli);
		$type_chqs_kd = $this->CI->model_pli->type_chq_kd();
		foreach ($data_cheque_kd as $cheque_kd) {
			$view_cheque_kd .= $this->get_cheque_kd_view(array(
				'id_pli'=>$pli->id_pli
				, 'chq_kd'=>$cheque_kd
				, 'id_docs' => $id_docs
				, 'type_chqs_kd' => $type_chqs_kd
			));
		}
		$motif_consignes = $this->CI->model_pli->get_all_motif_consigne($pli->id_pli);
		$nb_motif_consign = count($motif_consignes);
        $data_view_pli = array(
			'pli' => $pli
			,'data_pli' => $data_pli
			,'status_in' => $data_pli->statut_saisie// == 2 ? 6 : $data_pli->statut_saisie
			,'docs' => $id_docs
			,'socs' => $this->CI->model_pli->societe()
			,'titres' => $this->CI->model_pli->titre($data_pli->societe)
			,'typos' => $this->CI->model_pli->typologie($data_pli->societe)
			,'mode_paiements' => $this->CI->model_pli->mode_paiement()
			,'view_mvmnts' => $view_mvmnt
			,'ko_motifs' => $this->CI->model_pli->motif_ko()
			,'circulaires' => $this->CI->model_pli->liste_circulaires()
			,'paiements' => $this->CI->model_pli->my_paiement($pli->id_pli)
			,'montant_esp' => $this->CI->model_pli->paie_esp($pli->id_pli)
            ,'view_cheques' => $view_cheque
            ,'view_cheques_kd' => $view_cheque_kd
            ,'with_f_circ' => !empty($data_pli->fichier_circulaire)
			//,'http_circulaire' => base_url(upFileCirc).$data_pli->fichier_circulaire.'?'.date('U')
			,'nom_f_circulaire' => $data_pli->nom_orig_fichier_circulaire
			//,'nom_f_circulaire' => compose_nom_ci($pli->lot_scan, $pli->id_pli, $pli->pli, $data_pli->nom_orig_fichier_circulaire)
			//,'f_circulaire' => site_url('control/ajax/download_circulaire/').$data_pli->fichier_circulaire.'/'.$data_pli->nom_orig_fichier_circulaire
			,'f_circulaire' => site_url('control/ajax/download_circulaire/').$pli->id_pli
			,'categ_erreurs' => $this->CI->model_pli->categorie_erreur()
			,'motif_consigne' => $nb_motif_consign > 0 ? $motif_consignes[($nb_motif_consign-1)] : NULL
			,'motif_consignes' => $motif_consignes
			,'nb_motif_consign' => $nb_motif_consign
		);
		$this->CI->load->model('saisie/model_cba');
		$data_view_pli['mode_saisie_batch'] = $data_pli->flag_batch == 1 ? ($this->CI->model_cba->no_sent($pli->id_pli)? TRUE : FALSE) : FALSE;
		return $data_view_pli;
    }
    
	public function get_mvmnt_view($args=array()){
		$data_mvmnt = array(
			'titres' => isset($args['data_titre']) ? $args['data_titre'] : $this->CI->model_pli->titre((isset($args['id_soc']) ? $args['id_soc'] : 1), TRUE)
			,'ttr' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->titre : 1
			,'code_promo' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->code_promo : ''
			,'num_abonne' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->numero_abonne : ''
			,'nom_abonne' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->nom_abonne : ''
			//,'cp_abonne' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->code_postal_abonne : ''
			,'num_payeur' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->numero_payeur : ''
			,'nom_payeur' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->nom_payeur : ''
			//,'cp_payeur' => isset($args['data_mvmnt']) ? $args['data_mvmnt']->code_postal_payeur : ''
			,'identifiant' => isset($args['data_mvmnt']) && strlen($args['data_mvmnt']->ui_id) > 8 ? $args['data_mvmnt']->ui_id : NULL
		);
		$this->CI->load->model('saisie/model_cba');
		$data_mvmnt['is_batch'] = isset($args['data_pli']) && $args['data_pli']->flag_batch == 0 ? FALSE : TRUE;
		$data_mvmnt['code_promos'] = array();
		if(isset($args['data_pli']) && $args['data_pli']->flag_batch == 1) {
			$data_mvmnt['code_promos'] = $this->CI->model_cba->get_promo($data_mvmnt['code_promo']);
		}
		$data_mvmnt['code_promos_art'] = array();
		if(!is_null($data_mvmnt['identifiant'])){
			$batch = $this->CI->model_pli->get_commande_cba_mvmnt($data_mvmnt['identifiant']);
			if(!is_null($batch)){
				$data_mvmnt['batch'] = $batch;
				$data_mvmnt['code_promos_art'] = $this->CI->model_cba->get_promo_art($data_mvmnt['code_promo'], $batch->pmo_chc);
			}
		}
		return $this->CI->load->view('control/mouvement', $data_mvmnt, TRUE);
    }
    
	public function get_cheque_view($args=array()){
		$args = is_array($args) ? $args : array();
		if(!isset($args['id_pli']) || !is_numeric($args['id_pli'])){
			log_message('error', 'libraries> control> Data> get_cheque_view => id_pli non numeric');
			echo '';
			return '';
		}
		$id_pli = $args['id_pli'];
		$data_cheque = array(
			'anomalies' => isset($args['anom_chqs']) ? $args['anom_chqs'] : $this->CI->model_pli->anom_cheque()
			,'docs' =>  isset($args['id_docs']) ? $args['id_docs'] : $this->CI->model_pli->id_documents($id_pli)
			,'paiement_anomali' => array()
			,'id_etat_chq' => 2
			,'etat_modif' => 0
			,'cmc7' => isset($args['data_chq']) ? $args['data_chq']->cmc7 : ''
			,'rlmc' => isset($args['data_chq']) ? $args['data_chq']->rlmc : ''
			,'montant' => isset($args['data_chq']) ? $args['data_chq']->montant : ''
			,'nom_client' => isset($args['data_chq']) ? $args['data_chq']->nom_client : ''
			,'etranger' => isset($args['data_chq']) ? $args['data_chq']->etranger : 0
			,'identifiant' => isset($args['data_chq']) && strlen($args['data_chq']->ui_id) > 8 ? $args['data_chq']->ui_id : NULL
			,'id_doc' => isset($args['data_chq']) ? $args['data_chq']->id_doc : ''
			,'id_doc_lock' => FALSE
			,'tab_id_anom_corrige' => array()
		);
		if(is_numeric($data_cheque['id_doc'])){
			$id_doc = $data_cheque['id_doc'];
			$data_cheque['paiement_anomali'] = $this->CI->model_pli->paiement_anomali($id_doc);
			$data_cheque['id_etat_chq'] = $args['data_chq']->id_etat_chq;
			$data_cheque['etat_modif'] = $args['data_chq']->etat_modif;
			$data_cheque['id_doc_lock'] = TRUE;
			$data_cheque['anom_corrige'] = $this->CI->model_pli->paiement_chq_ano_corrige($id_doc);
			foreach ($data_cheque['anom_corrige'] as $ano_c) {
				array_push($data_cheque['tab_id_anom_corrige'], $ano_c->id_anomalie_chq);
			}
		}
		return $this->CI->load->view('control/cheque', $data_cheque, TRUE);
    }
    
	public function get_cheque_kd_view($args=array()){
		$args = is_array($args) ? $args : array();
		if(!isset($args['id_pli']) || !is_numeric($args['id_pli'])){
			log_message('error', 'libraries> control> Data> get_cheque_kd_view => id_pli non numeric');
			echo '';
			return '';
		}
		$id_pli = $args['id_pli'];
		$data_cheque = array(
			'choix_type_chq_kds' =>  isset($args['type_chqs_kd']) ? $args['type_chqs_kd'] : $this->CI->model_pli->type_chq_kd()
			,'docs' =>  isset($args['id_docs']) ? $args['id_docs'] : $this->CI->model_pli->id_documents($id_pli)
			,'montant' => isset($args['chq_kd']) ? $args['chq_kd']->montant : ''
			,'type_chq_kd' => isset($args['chq_kd']) ? $args['chq_kd']->type_chq_kdo : ''
			,'code_barre' => isset($args['chq_kd']) ? $args['chq_kd']->code_barre : ''
			,'id_doc' => isset($args['chq_kd']) ? $args['chq_kd']->id_doc : ''
			,'identifiant' => isset($args['chq_kd']) && strlen($args['chq_kd']->ui_id) > 8 ? $args['chq_kd']->ui_id : NULL
		);
		return $this->CI->load->view('control/cheque_cadeau', $data_cheque, TRUE);
    }
    
	public function view_doc($id_doc=NULL){
		if(is_numeric($id_doc)){
			$data_doc = $this->CI->model_pli->document($id_doc);
			if (count($data_doc) > 0) {
				$empty_b64 = base64_encode(file_get_contents(base_url('assets/images/empty_papper.jpg')));
				$data_view_doc = array(
					'doc' => $data_doc[0]
					,'doc_b64_recto' => trim($data_doc[0]->n_ima_base64_recto) == '' ? $empty_b64 : trim($data_doc[0]->n_ima_base64_recto)
					,'doc_b64_verso' => trim($data_doc[0]->n_ima_base64_verso) == '' ? $empty_b64 : trim($data_doc[0]->n_ima_base64_verso)
				);
				return $this->CI->load->view('control/document', $data_view_doc, TRUE);
			}
		}
		return 'Document #'.$id_doc.' introuvable!';
    }
    
	public function option_titres($id_soc=NULL, $titre_i=NULL){
		$options = '';
		$titre_i = $titre_i == '1' ? TRUE : FALSE;
		$data_titre = $this->CI->model_pli->titre($id_soc, $titre_i);
		foreach ($data_titre as $n => $titre) {
			$selected = $n == 0 ? 'selected' : '';
			$options .= '<option value="'.$titre->id.'" title="'.$titre->code.'" data-subtext=" - '.$titre->code.'" '.$selected.' >'.$titre->titre.'</option>';
		}
		return $options;
    }
    
	public function option_typologie($id_soc=1){
		$options = '';
		$data_typo = $this->CI->model_pli->typologie($id_soc);
		foreach ($data_typo as $n => $typo) {
			$selected = $n == 0 ? 'selected' : '';
			$options .= '<option value="'.$typo->id.'" data-subtext=" - '.$typo->sous_lot.'" '.$selected.' >'.$typo->typologie.'</option>';
		}
		echo $options;
	}

	/*public function test_ori_image($id_doc){//test rectifie orientation image
		if(!is_numeric($id_doc)){
			return 'id_document non valide!';
		}
		$chemin_recto = './depot/recto.jpg';
		$chemin_verso = './depot/verso.jpg';
		$data_doc = $this->CI->model_pli->document($id_doc);
		if(count($data_doc) < 1){
			return 'document introuvable';
		}
		$image_recto = base64_decode(trim($data_doc[0]->n_ima_base64_recto));
		$image_verso = base64_decode(trim($data_doc[0]->n_ima_base64_verso));
		$this->CI->load->helper('file');
		write_file($chemin_recto, $image_recto);
		write_file($chemin_verso, $image_verso);
		return array(
			'recto_orig' => $image_recto
			,'recto_fixed' => $this->fixOrientation($chemin_recto)
			,'verso_orig' => $image_verso
			,'verso_fixed' => $this->fixOrientation($chemin_verso)
		);
	}

	private function fixOrientation($filename)
	{
		$image = imagecreatefromjpeg($filename);
        $exif = exif_read_data($filename);

        if (!empty($exif['Orientation'])) {
            switch ($exif['Orientation']) {
                case 3:
                    $image = imagerotate($image, 180, 0);
                    break;

                case 6:
                    $image = imagerotate($image, -90, 0);
                    break;

                case 8:
                    $image = imagerotate($image, 90, 0);
                    break;
            }
		}
		unlink($filename);
		imagejpeg($image, $filename);
		$data_image = read_file($filename);
		unlink($filename);
		return $data_image;
	}*/
    
}
