<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Data_save{
    
    private $CI;

    public $op;
    public $id_pli = '';
    public $mvmnts = array();
    public $mvmnt_payeurs = array();
    public $mvmnt_abonnes = array();
    public $mvmnt_promos = array();
    public $paiements = array();
    public $cheques = array();
    public $cheques_anomalies = array();
    public $histo_cheque = array();
    public $cheques_cadeaux = array();
    public $statut = '';
    public $data_pli = array();
    public $pli = array();
    public $img_dezip = array();
    public $paie_esp = array();
    public $saisie_batch;
    public $ok_data_batch;
    public $error_data_batch;
    public $etat_pli_by_chq;
    public $controle_pli_erreur;
    public $mvmnt_qtt;
    public $motif_ko_by_chq;
    
    public $output = '';

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->model('control/model_pli');
        $this->CI->load->helper('function1');
        $this->op = $this->CI->session->id_utilisateur;
    }

    public function initialize(){
        $this->id_pli = $this->CI->input->post_get('id_pli');
        $pli = $this->CI->model_pli->pli($this->id_pli);
        if(is_null($pli)){
            $this->output; 'Pli introuvable!';
            log_message('error', 'Enregistrement control> erreur id_pli non numeric op#'.$this->CI->session->id_utilisateur.', pli#'.$this->id_pli);
            return FALSE;
        }
        $chq_only = TRUE;
        $paiements = json_decode($this->CI->input->post_get('paiements'));
        foreach ($paiements as $paiement) {
            array_push($this->paiements, array(
                'id_pli' => $this->id_pli
                ,'id_mode_paiement' => $paiement
            ));
            if($paiement != 1){
                $chq_only = FALSE;
            }
        }
        $this->CI->load->model('saisie/model_cba');
        $this->saisie_batch = is_numeric($this->CI->input->post_get('saisie_batch')) && $this->CI->input->post_get('saisie_batch') == 1 ? TRUE : FALSE;
        // $this->saisie_batch = $this->saisie_batch ? ($this->CI->model_cba->no_sent($this->id_pli)? TRUE : FALSE) : FALSE;
        $this->mvmnt_payeurs = json_decode($this->CI->input->post_get('mvmnt_payeurs'));
        $this->mvmnt_abonnes = json_decode($this->CI->input->post_get('mvmnt_abonnes'));
        $this->mvmnt_promos = json_decode($this->CI->input->post_get('mvmnt_promos'));
        $this->ok_data_batch = TRUE;
        $this->error_data_batch = '';
        $this->mvmnt_qtt = array();
        $mvmnts = json_decode($this->CI->input->post_get('mvmnts'));
        foreach ($mvmnts as $key_mvmnt => $mvmnt) {
            $c_mvmnt = array(
                'id_pli' => $this->id_pli
                ,'titre' => $mvmnt->titre
                ,'code_promo' => $mvmnt->code_promo
                ,'numero_abonne' => $mvmnt->num_a
                ,'nom_abonne' => $mvmnt->nom_a
                //,'code_postal_abonne' => $mvmnt->cp_a
                ,'numero_payeur' => $mvmnt->num_p
                ,'nom_payeur' => $mvmnt->nom_p
                ,'ui_id' => $mvmnt->ui_id
                //,'code_postal_payeur' => $mvmnt->cp_p
            );
            if($this->saisie_batch){
                $ui_id = $mvmnt->ui_id;
                $this->mvmnt_qtt[$ui_id] = $mvmnt->qtt;
                $payeur = $this->mvmnt_payeurs->$ui_id;
                $c_mvmnt['numero_payeur'] = $payeur->ctm_nbr;
				$c_mvmnt['nom_payeur'] = $payeur->atn_end;
                $abonne = $this->mvmnt_abonnes->$ui_id;
                $c_mvmnt['numero_abonne'] = $abonne->ctm_nbr;
				$c_mvmnt['nom_abonne'] = $abonne->atn_end;
				$promo = $this->mvmnt_promos->$ui_id;
				$verif_data_batch = $this->CI->model_cba->verif_data_batch($payeur, $abonne, $promo, $chq_only);
				if(!$verif_data_batch['is_ok']){
					$this->error_data_batch .= $this->ok_data_batch ? '' : '; ';
					$this->ok_data_batch = FALSE;
					$this->error_data_batch .= 'mouvement#'.($key_mvmnt+1).': '.$verif_data_batch['msg'];
				}
            }
            array_push($this->mvmnts, $c_mvmnt);
        }
        $paie_esp = $this->CI->input->post_get('montant_esp');
        $this->paie_esp = array(
            'id_pli' => $this->id_pli
            ,'montant' => u_montant($paie_esp)
        );
        $cheques = json_decode($this->CI->input->post_get('cheques'));
        $ref_anom_chq_etat = $this->CI->model_pli->anom_cheque_etat();
        $ref_etat_chq = $this->CI->model_pli->cheque_etat_tab();
		$ref_anom_chq_etat = $this->CI->model_pli->anom_cheque_tab();
		$etat_pli_by_chq_temp = new stdClass();
		$etat_pli_by_chq_temp->etat_pli = 1;
        $etat_pli_by_chq_temp->prio_anom = 0;
        $this->motif_ko_by_chq = '';
        foreach ($cheques as $cheque) {
            $id_etat_chq = $cheque->id_etat_chq;
            $paie_anomalie = $cheque->anomalies;
            $id_etat_chq_temp = $ref_etat_chq[2];
            if(is_array($paie_anomalie)){
                foreach ($paie_anomalie as $key_p_anom => $p_anom) {
                    array_push($this->cheques_anomalies, array(
                        'id_doc' => $cheque->id_doc
                        ,'id_pli' => $this->id_pli
                        ,'id_anomalie_chq' => $p_anom
                        ,'ui_id_pca' => $cheque->ui_id
                    ));
                    if($id_etat_chq_temp->priorite > $ref_anom_chq_etat[$p_anom]->prio_stat){
						$id_etat_chq_temp = $ref_etat_chq[$ref_anom_chq_etat[$p_anom]->id_etat_chq];
					}
					if($etat_pli_by_chq_temp->prio_anom < $ref_anom_chq_etat[$p_anom]->prio_anom /*&& $id_etat_chq != 4*/){
						$etat_pli_by_chq_temp = $ref_anom_chq_etat[$p_anom];
                    }
                    if(!in_array($ref_anom_chq_etat[$p_anom]->etat_pli, array(1, 11))) {
						$this->motif_ko_by_chq .= $this->motif_ko_by_chq == '' ? 'anomalie chèque: ' : ', ';
						$this->motif_ko_by_chq .= $ref_anom_chq_etat[$p_anom]->label_anom;
					}
                }
            }
            if(/*is_numeric($id_etat_chq) &&*/ $id_etat_chq != 4 /*&& $cheque->etat_modif == 0*/){
                $id_etat_chq = $id_etat_chq_temp->id_etat_chq;
            }
            array_push($this->cheques, array(
                'id_pli' => $this->id_pli
                ,'montant' => u_montant($cheque->montant)
                ,'cmc7' => $cheque->cmc7
                ,'rlmc' => $cheque->rlmc
                ,'id_etat_chq' => is_numeric($id_etat_chq) ? $id_etat_chq : NULL
                ,'etat_modif' => $cheque->etat_modif
                ,'id_doc' => $cheque->id_doc
                ,'nom_client' => $cheque->nom_client
                ,'etranger' => $cheque->etranger == 1 ? 1 : 0
                ,'ui_id' => $cheque->ui_id
            ));
            array_push($this->histo_cheque, array(
				'id_doc' => $cheque->id_doc
				,'cmc7' => $cheque->cmc7
				,'anomalies' => is_array($paie_anomalie) ? join(',', $paie_anomalie) : ''
				,'etat' => $id_etat_chq
				,'etape' => 'control'
				,'commentaire' => 'control|retraitement'
                ,'id_pli' => $this->id_pli
                ,'ui_id_hc' => $cheque->ui_id
			));
        }
        $this->etat_pli_by_chq = $etat_pli_by_chq_temp->etat_pli;
        $cheques_cadeaux = json_decode($this->CI->input->post_get('cheques_cadeaux'));
        $cheques_cadeaux = is_array($cheques_cadeaux) ? $cheques_cadeaux : array();
        foreach ($cheques_cadeaux as $cheque_kd) {
            array_push($this->cheques_cadeaux, array(
                'id_pli' => $this->id_pli
                ,'montant' => u_montant($cheque_kd->montant)
                ,'type_chq_kdo' => $cheque_kd->type
                ,'code_barre' => $cheque_kd->code_barre
                ,'id_doc' => $cheque_kd->id_doc
                ,'ui_id' => $cheque_kd->ui_id
            ));
        }
        $this->statut = $this->CI->input->post_get('statut');
        if(!is_numeric($this->statut)){
            $this->output; 'Argument[statut] non conforme!';
            log_message('error', 'Enregistrement control> erreur statut pli non numeric op#'.$this->CI->session->id_utilisateur.', pli#'.$this->id_pli);
            return FALSE;
        }
        $controle_erreurs = json_decode($this->CI->input->post_get('erreur'));
        $this->controle_pli_erreur = array();
        if($pli->flag_echantillon == 1 && is_array($controle_erreurs)) {
            foreach ($controle_erreurs as $key => $controle_erreur) {
                array_push($this->controle_pli_erreur, array(
                    'id_pli' => $this->id_pli
                    ,'id_categorie_erreur' => $controle_erreur
                    ,'id_decoupage' => $pli->id_decoupage
                ));
            }
        }
        $this->data_pli = array(
            'societe' => $this->CI->input->post_get('id_soc')
            ,'infos_datamatrix' => $this->CI->input->post_get('datamatrix')
            ,'titre' => $this->CI->input->post_get('titre')
            ,'code_promotion' => $this->CI->input->post_get('code_promo')
            ,'statut_saisie' => $this->statut
            ,'motif_ko' => $this->CI->input->post_get('motif_ko')
            ,'autre_motif_ko' => trim($this->CI->input->post_get('autre_motif_ko'))
            ,'nom_circulaire' => $this->CI->input->post_get('nom_circ') == '' ? NULL : $this->CI->input->post_get('nom_circ')
            ,'message_indechiffrable' => $this->CI->input->post_get('msg_ind')
            ,'dmd_kdo_fidelite_prim_suppl' => $this->CI->input->post_get('dmd_kdo')
            ,'dmd_envoi_kdo_adrss_diff' => $this->CI->input->post_get('dmd_env_kdo')
            //,'type_coupon' => $this->CI->input->post_get('type_coupon')
            //,'id_erreur' => ($this->CI->input->post_get('erreur') == '' || $this->CI->input->post_get('erreur') == '-1') ? NULL : $this->CI->input->post_get('erreur')
            ,'comm_erreur' => $this->CI->input->post_get('comm_erreur')
            ,'nom_deleg' => $this->CI->input->post_get('nom_deleg')
            ,'mvt_nbr' => count($this->mvmnts)
            ,'avec_chq' => count($this->cheques) > 0 ? 1 : 0
            ,'typologie' => $this->CI->input->post_get('typo')
            ,'code_ecole_gci' => $this->CI->input->post_get('code_ecole_gci')
            ,'par_ttmt_ko' => 0
        );
        if(!preg_match("#\d+#", $this->CI->input->post_get('dmd_env_kdo'))){
            $this->output; 'Données non-conformes';
            log_message('error', 'libraries>control>Data_save data non-conforme op#'.$this->CI->session->id_utilisateur.', pli#'.$this->id_pli);
            return FALSE;
        }
        if($this->CI->input->post_get('modif_f_circ') == 1){
            $temp_file_circulaire = $this->CI->input->post_get('fichier_circ');
            $fichier_circulaire = '';
            if($temp_file_circulaire != ''){
                $ext = $this->CI->input->post_get('ext_fichier_circ');
                $temp_file_circulaire = './'.upTmpFileCirc.$temp_file_circulaire;
                $new_name = $this->id_pli.$ext;
                if(rename($temp_file_circulaire, './'.upFileCirc.$new_name)){
                    $fichier_circulaire = $new_name;
					if(file_exists($temp_file_circulaire)){
						unlink($temp_file_circulaire);
					}
                }else {
                    $this->output; 'Impossible d\'enregistrer le fichier de circulaire!';
                    log_message('error', 'Enregistrement control> erreur copy fichier circulaire <'.$temp_file_circulaire.'> to <'.upFileCirc.$new_name.'>, op#'.$this->CI->session->id_utilisateur.', pli#'.$this->id_pli);
                    return FALSE;
                }
            }
            $this->data_pli['fichier_circulaire'] = $fichier_circulaire;
            $this->data_pli['nom_orig_fichier_circulaire'] = $this->CI->input->post_get('nom_fichier_circ');
        }
        $this->pli = array(
            'typologie' => $this->CI->input->post_get('typo')
            ,'idefix' => $this->CI->input->post_get('idefix')
            ,'societe' => $this->data_pli['societe']
            ,'statut_saisie' => $this->statut
        );
        $this->img_dezip = json_decode($this->CI->input->post_get('img_dezip'));
        return TRUE;
    }

}
