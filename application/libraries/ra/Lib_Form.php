<?php 

class Lib_Form {
    private $CI;
    
    public function __construct() 
    {
       $this->CI =& get_instance();
       $this->CI->load->model('recherche/Model_Form','mf');
    }

    public function getFiltreRa()
    {
       
       // var_dump($_POST);
        $notIn = $this->CI->input->post("desactiver"); 
        $filtre = $this->CI->mf->getFiltre();
        
        /*
             1 - courrier; 2-flux
             $this->session->userdata('TypeTtmt');
        */

        $checkedCourrier = "";
        $checkedFlux = "";
        $typeSource = 0; /*mail ou sftp*/
        if( $this->CI->session->userdata('TypeTtmt') == 1)
        {
            $checkedCourrier = " checked='checked' ";
            $typeSource = 1;
        }
        elseif($this->CI->session->userdata('TypeTtmt') == 2)
        {
            $checkedFlux = " checked='checked' ";
            $typeSource = 2;
        }
        $radio = " <input type='radio' onclick='getSourceTtmt(1);'; ".$checkedCourrier." value='1' name='gender' id='courrier' class='with-gap radio-col-indigo'><label for='courrier'>Courrier</label>
                    <input type='radio' onclick='getSourceTtmt(2);';  ".$checkedFlux." value='2'  name='gender' id='flux' class='with-gap radio-col-indigo'><label for='flux' class='m-l-20'>Mail et SFTP</label>
                    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;"; 

        $strFiltre  = $radio."<label>Ajouter un filtre : &nbsp;&nbsp;</label><select data-live-search='true' style='height:10px;' id='ra-filtre-enfant' class='c-ra-filtre'>";
        $rubriqueID = 0;
        $strFiltre .= '<option value="0"></option>';    
        foreach($filtre as $item)
        {
            
            if($typeSource == (int)$item->source)
            {
                $bdRubriqueID = (int)$item->rubrique_id;
                $disabled  = "";
                if((int)$item->id > 0 && in_array((int)$item->id,$notIn))
                {
                    $disabled = " disabled ";
                }
                if($rubriqueID == 0 ||  $bdRubriqueID != $rubriqueID)
                {
                    if($rubriqueID > 0 )
                    {
                        $strFiltre .= '</optgroup>';
                    }
                    $strFiltre .= '<optgroup value="'.$item->rubrique_id.'" label="<h5>'.$item->libelle.'</h5>">';
                    $strFiltre .= '<option '. $disabled.' value="'.$item->id.'">'.$item->libelle_enfant.'</option>';
                }
                else
                {
                    $strFiltre .= '<option '. $disabled.' value="'.$item->id.'">'.$item->libelle_enfant.'</option>';
                }
                $rubriqueID = $bdRubriqueID;
            }
           
        }
    
        $strFiltre .= '</optgroup></select>';
        echo $strFiltre;
    }

    public function ajouterChamp()
    {
        $idFiltre = $this->CI->input->post("id");
        $champ = $this->CI->mf->idChamp($idFiltre);

       $strChamp = "";

       if($champ)    
       {
            $objChamp = $champ[0];
            $elmType = $objChamp->element_type;
            $colonne = $objChamp->colonne;
            $whereColonne = $objChamp->where_colonne;
            $strChamp .= '<div  class="col-md-4"><div class="form-line" style="text-align:right;"><label id="label_'.$objChamp->where_colonne.'" >'.$objChamp->libelle_enfant.'</label></div></div>'; /*label */

            if($objChamp->type_donnes  != 'date')
            {
                $strChamp .= '<div class="col-md-3">';/*debut CHOIX operateur*/
                $strChamp .='   <div class="form-group">';
                $strChamp .='       <select  class="form-control operateur_'.$objChamp->where_colonne.'"  id="op_'.$objChamp->where_colonne.'">';
                $strChamp .='           <option value="in" selected>égal(e)</option>';
                $strChamp .='           <option value="not_in">différent(e)</option>';
                
                if($objChamp->element_type != 'select' && $objChamp->where_colonne != 'montant')
                {
                    $strChamp .='           <option value="ilike">similaire à</option>'; 
                }
                elseif($objChamp->where_colonne == 'montant')
                {
                    $strChamp .='           <option value=">=">supérieur ou égal à</option>'; 
                    $strChamp .='           <option value="<=">inférieur ou égal à</option>'; 
                }
                
                $strChamp .='       </select>';
                $strChamp .='   </div>';
                $strChamp .= '</div>'; /*fin CHOIX operateur*/    
            }
           

            switch($elmType)
            {
                case 'input':
                    
                    if($objChamp->type_donnes  != 'date')
                    {
                        $strChamp .= '<div class="col-md-4">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="'.$objChamp->where_colonne.'" class="form-control recherche-avancee '.$objChamp->where_colonne.'" placeholder ="Saisir ici : '.$objChamp->libelle_enfant.'"/>
                                            </div>
                                        </div>
                                    </div>';
                    }
                    else
                    {
                  
                        $strChamp .= '<div class="col-md-5">
                                    <div class="form-group">
                                        <div class="form-line"  >
                                            <input  type="text"  id="'.$objChamp->where_colonne.'" class="form-control recherche-avancee '.$objChamp->where_colonne.'" name="daterange" value="2018/07/01 - 2018/07/01"  readonly/>
                                        </div>
                                    </div>

                            </div>';        

                                
                    }
                   
                    break;

                case 'select':

                $champSelect =  $this->CI->mf->selectChamp($idFiltre);
                
                if($champSelect)
                {
                    $multiple = "";
                    if($objChamp->type_donnes  != 'boolean')
                    {
                        $multiple = " multiple ";    
                    }
                    $strChamp .= '<div class="col-md-4">';/*debut CHOIX operateur*/
                    $strChamp .='   <div class="form-group">';

                    $strChamp .='       <select  id="'.$objChamp->where_colonne.'" class="form-control recherche-avancee '.$objChamp->where_colonne.'" data-live-search="true" '.$multiple.'>';

                    $iTmpSelect = 0;
                    
                    foreach($champSelect as $itemSelect)
                    {
                        $selected = "";

                        if($iTmpSelect == 0) 
                            $selected = " selected ";
                        $strChamp .='<option value="'.$itemSelect->id_select.'" '.$selected.'>'.$itemSelect->libelle.'</option>';   

                        $iTmpSelect++;
                    }
                    
                    $strChamp .='       </select>';
                    $strChamp .='   </div>';
                    $strChamp .= '</div>'; /*fin CHOIX operateur*/    
                }
                break;
               case 'textearea':
                    $strChamp .= '<div class="col-md-4">';/*debut CHOIX operateur*/
                    $strChamp .='   <div class="form-group">';
                    $placeHolder ="";
                    if(trim($objChamp->where_colonne) == "id_pli")
                    {
                        $placeHolder = "Merci de mettre ici l'ID pli. \nExemple : 1234, 56789, 101112";
                    }
                    elseif(trim($objChamp->where_colonne) == "cmc7")
                    {
                        $placeHolder = "Merci de mettre ici le cmc7. \nExample : 'cmc7', 'cmc7', 'cmc7'";
                    }
                    elseif(trim($objChamp->where_colonne) == "id_doc")
                    {
                        $placeHolder = "Merci de mettre ici le l'ID doc. \nExample : ' 1234, 56789, 101112";
                    }
                    $strChamp .='   <textarea rows="4"  id="'.$objChamp->where_colonne.'" class="no-resize form-control recherche-avancee '.$objChamp->where_colonne.'" placeholder="'.$placeHolder.'"></textarea>';

                    $strChamp .='   </div>';
                    $strChamp .= '</div>'; /*fin CHOIX operateur*/   
               break;

            }

            $strChamp .= '<div class="col-md-1" style="float: left;top: 5px;position: relative;left: -23px;">
                             <span class="badge bg-pink" style="cursor:pointer;" onclick="supprimerChamp(\'close_'.$whereColonne.'\','.$idFiltre.')"; >-</span>
                        </div>';       
            $strChamp = '<div class="row close_'.$whereColonne.'">'.$strChamp.'</div>';
       }
       
      echo $strChamp;
     

    }

    public function raColonne()
    {
        $idListe = $this->CI->input->post("id");
         
        // <option value="CA">California</option>

        $colonneMultiple = $this->CI->mf->raColonne($idListe);
        $strMultipleColonne = "";
        if($colonneMultiple)
        {
            $strMultipleColonne = '<select id="ra-colonne-liste" class="ms" multiple="multiple">';

            foreach($colonneMultiple as $item)
            {
                $selected = " ";

                if((int)$item->affichage)
                {
                    $selected = " selected ";
                }

                $strMultipleColonne .= '<option value="'.$item->colonne_db.'" '.$selected.'>'.$item->colonne.'</option>'; 
            }

            $strMultipleColonne .= '</select><br/>';  
            $strMultipleColonne .= '<div class="col-xs-6 col-sm-6 btn-recherche" style="padding-left: 0px !important;">
                                        <button type="button" class="btn bg-indigo waves-effect" onclick="rechercher(0);">Rechercher</button> 
                                        <button type="button" class="btn btn-success waves-effect" onclick="rechercher(1);">Excel</button>
                                    </div>';
        }
        echo $strMultipleColonne;
        
    }
}