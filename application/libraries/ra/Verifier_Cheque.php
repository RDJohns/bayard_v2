<?php 
    class Verifier_Cheque
    {
        private $CI;

        public function __construct() 
        {
            $this->CI =& get_instance();
        }

        public function VerifierChequeID($currentDoc)
        {
            $this->CI->load->model('cheque/Model_Verifier_Cheque','mCheque');

            $listeAnomaileCorrige = array();
            $listeAnomaileCorrige = $this->CI->mCheque->verificationCheque($currentDoc);
            
            $retourAnoCorrige     = array();
            if($listeAnomaileCorrige)
            {
                $str = '<span class="badge bg-red ">L\'anomalie de ce chèque est déjà corrigé par CDN </span>';
                $retourAnoCorrige = array("str"=>$str,"listeID"=>$listeAnomaileCorrige);
            }
            else 
            {
                $retourAnoCorrige = array("str"=>"","listeID"=> array());
            }
            /*$str = '<span class="badge bg-red anomalie-corrige-cdn">L\'anomalie de ce chèque est déjà corrigé par CDN </span>';
                $retourAnoCorrige = array("str"=>$str,"listeID"=>1);*/
            return $retourAnoCorrige;
        }
    }
?>