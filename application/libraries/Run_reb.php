<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Run_reb{

    private $CI;
    private $chemin;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->helper('path');
        $this->chemin = set_realpath('.');
        $this->run();
    }

    private function run()
    {
        log_message_reb('error', 'run scripts reb');

        //regroupement
        exec('php '.$this->chemin.'index.php cron new_cron_reb');
    }

}