<?php 

class Lib_Sftp {
    private $CI;
    
    public function __construct() 
    {
       $this->CI =& get_instance();
	   $this->CI->load->model('push/decoupage/Model_LIBSftp','MLIBSftp');
    }
	
	
	public function downloadFluxPubliposte($idFlux)
	{
		$this->CI->load->helper('download');
        $this->CI->load->helper('file');

		$flux 		= $this->CI->MLIBSftp->getFlux($idFlux);

		if($flux)
		{
			$data 		= $flux[0];
			$filename   = FCPATH.$data->emplacement;
			if (file_exists($filename))
			   {
					force_download($data->filename_origin, read_file($filename));
					return '';
				}
		}
		else
        {
            force_download('pas_de_fichier.txt', 'fichier introuvable pour ID flux :'.(int)$idFlux);
            return '';
        }
        return '';
	}

	public  function moveFlux()
    {
        $this->CI->MLIBSftp->moveFlux();
	}

	public function downloadFichierPubliposte($idFlux,$idDecoupage,$type)
	{
		
	
		$this->CI->load->helper('download');
		$this->CI->load->helper('file');
		
		if((int)$type == 0) 
		{
			$flux 		= $this->CI->MLIBSftp->fichierPubliposter(array("id_flux_parent"=>(int)$idFlux));
			var_dump($flux);
			
			if($flux)
			{
				$data = $flux[0];
				$filename = $data->flux_publiposte;
				$fichierOrigin = $data->fichier;
				
				$emplacement = './SFTP_MAIL/sftp/';
                $tab_url = explode('/'.'sftp/', $data->emplacement);
                $emplacement .= $tab_url[1].'/'.$data->nom_fichier;
				if(file_exists($emplacement))
				{
					  force_download($data->filename_origin, read_file($emplacement));
                	return '';	
				}
				force_download('pas_de_fichier.txt', 'fichier introuvable pour ID flux :'.(int)$idFlux);
				
              
			}
			else
			{
				force_download('pas_de_fichier.txt', 'fichier introuvable pour ID flux :'.(int)$idFlux);
			}
		}
		elseif($type == 1) 
		{
			$flux 		= $this->CI->MLIBSftp->fichierPubliposter(array("id_decoupe"=>(int)$idDecoupage));
			if($flux)
			{
				$obj = $flux[0];
				
				$path = (trim($obj->type_publiposte) == "REJET KO") ? "DECOUPAGE/rejet_ko/" : "DECOUPAGE/publiposter/";
				$filename   = FCPATH.$path.$obj->flux_publiposte;
				if (file_exists($filename))
			    {
					force_download($obj->fichier, read_file($filename));
					return '';
				}
				force_download('pas_de_fichier.txt', 'fichier introuvable pour ID flux :'.(int)$idFlux." ID decoupe : ".(int)$idDecoupage);
				return '';
				
			}
			else
			{
				force_download('pas_de_fichier.txt', 'fichier introuvable pour ID flux :'.(int)$idFlux." ID decoupe : ".(int)$idDecoupage);
			}
		}
		else
		{
			force_download('pas_de_fichier.txt', 'fichier introuvable pour ID flux :'.(int)$idFlux);
		}
	}
	
	public function downloadFluxParent($id)
	{
		
	}
	
    
}

/**test git */
/**test git */