<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu{
    /*
    1) Utilisation dans les views 
    -------------------------------------------------------------------
    -------------------------------------------------------------------
    ---             <?php                                           ---
    ---                 $CI =&get_instance();                       ---
    ---                 $actif =isset($menu_acif)?$menu_acif:null;  ---
    ---                 $CI->menu->menu($actif);                    ---
    ---             ?>                                              ---
    -------------------------------------------------------------------
    -------------------------------------------------------------------

    2) Ajouter un élément dans le menu: ajouter un array dans le array $menu
    -------------------------------------------------------------------
    ---------------------------- EXEMPLE ------------------------------
    -------------------------------------------------------------------

        array('titre'=>'Regroupements des plis <br/>par date-courrier',
                        'detail'=>'Regroupements des plis par date-courrier',
                        'lien'=>'javascript:show_info_grp_pli_by_older();',
                        'icon'=>'fa fa-exclamation',
                        'color'=>'green',
                        'condition'=> $type_utilisateur_2,
                        'url_js'=>true,
                        'id'=>'menu_regroupement_pli_par_date'
                    )
        
        
        1) titre : titre du menu
        2) detail : detail de l'interface
        3) lien : URL de l'interface
        4) icon : icon de l'interface dans le menu
        5) color : couleur del'item du menu
        6) condition : condition d'affichage selons le niveau d'autorisation de l'user

        ---------------- 7 et 8 pour le menu spécial ------------------

        7) url_js : true si le lien est un fonction Javascrypt ET un élément dans le menu
        8) id : id de li pour l'affichage spécifique d'un seul élément 

        7 et 8 ) ajouter : $('#id_li').show(); ou/et $('.visible_menu').show(); dans le js du fichier courant

    -------------------------------------------------------------------
    -------------------------------------------------------------------
    */
    private $menu ;
    public function __construct() {
        $this->CI =& get_instance();
        // Déclaration des conditions
        $type_utilisateur_4 = $this->CI->session->id_type_utilisateur == 4;//old valeur !=8
        $traitement_ko      = $this->CI->session->avec_traitement_ko == 1;
        $type_utilisateur_8 = $this->CI->session->id_type_utilisateur == 8;
        $type_utilisateur_2 = $this->CI->session->id_type_utilisateur == 2;
        $type_utilisateur_5 = $this->CI->session->id_type_utilisateur == 5;
        $type_utilisateur_7 = $this->CI->session->id_type_utilisateur == 7;
        $type_utilisateur_1 = $this->CI->session->id_type_utilisateur == 1;
        $type_utilisateur_3 = $this->CI->session->id_type_utilisateur == 3;

        $type_utilisateur_100 = $this->CI->session->id_type_utilisateur == 100;

        $operateur2 = false;
        $operateur1 = false;

        if($type_utilisateur_5){
            $this->CI->load->model('model_login');
            $data_op_push = $this->CI->model_login->my_acces_push($this->CI->session->id_utilisateur);
            if(($data_op_push->id_type_acces)==2){
                $operateur1 = true;
            }else if (($data_op_push->id_type_acces)==3){
                $operateur2 = true;
            }
        }
        // Initialisation de l'array menu
        $this->menu = array(
            array('titre'=>'Visualisation des courriers',
                'detail'=>'Visu détaillée des courriers',
                'lien'=>'visual/visual',
                'icon'=>'fa fa-eye',
                'color'=>'cyan',
                'condition'=> $type_utilisateur_4 || $type_utilisateur_8 || $type_utilisateur_7 || $type_utilisateur_100
            )    
            ,array('titre'=>'Stat. GED',
                'detail'=>'Interface de stat GED Mada et GED CE',
                'lien'=>'statistics/Stat',
                'icon'=>'fa fa-arrow-circle-right',
                'color'=>'brown',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Visualisation des MAIL/SFTP',
                'detail'=>'Visu détaillée des MAIL/SFTP',
                'lien'=>'visualisation/visu_push',
                'icon'=>'fa fa-sign-in',
                'color'=>'deep-purple',
                'condition'=> $type_utilisateur_4 || $type_utilisateur_100
            )
            ,array('titre'=>'Visualisation batch',
                'detail'=>'Visualisation batch',
                'lien'=>'anomalie/batch',
                'icon'=>'fa fa-bug',
                'color'=>'red',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Réception',
                'detail'=>'Suivi de la réception des plis/flux',
                'lien'=>'reception/pli_flux',
                'icon'=>'fa fa-envelope',
                'color'=>'light-green',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Suivi mensuel',
                'detail'=>'Réception mensuelle',
                'lien'=>'statistics/statistics/stat_mensuel',
                'icon'=>'fa fa-signal',
                'color'=>'teal',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Suivi des soldes',
                'detail'=>'Soldes par date de courrier',
                'lien'=>'statistics/statistics/suivi_solde',
                'icon'=>'fa fa-battery-half',
                'color'=>'pink-cstm',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Traitement',
                'detail'=>'Suivi des traitements',
                'lien'=>'statistics/statistics_flux/stat_traitement',
                'icon'=>'fa fa-tasks',
                'color'=>'light-blue',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Extraction',
                'detail'=>'Extraction des plis',
                'lien'=>'anomalie/anomalie/visu_anomalie',
                'icon'=>'fa fa-file-text',
                'color'=>'cyan-t',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Visualisation taux de fautes',
                'detail'=>'Visualisation taux de fautes',
                'lien'=>'visualisation/fautes',
                'icon'=>'fa fa-table',
                'color'=>'pink',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Suivi des espèces',
                'detail'=>'Espèces',
                'lien'=>'suivi/especes',
                'icon'=>'fa fa-money',
                'color'=>'amber',
                'condition'=> $type_utilisateur_4
            )
            /*,array('titre'=>'Suivi des provenances',
                'detail'=>'Provenance des enveloppes',
                'lien'=>'provenance/pli',
                'icon'=>'fa fa-flag',
                'color'=>'green',
                'condition'=> $type_utilisateur_4
            )*/
            ,array('titre'=>'Suivi des ch&egrave;ques non REB',
                'detail'=>'Ch&egrave;ques non remis en banque',
                'lien'=>'suivi/reliquat',
                'icon'=>'fa fa-list',
                'color'=>'lime',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Suivi des ch&egrave;ques non saisis',
                'detail'=>'Ch&egrave;ques non saisis à J+3',
                'lien'=>'suivi/nosaisie',
                'icon'=>'fa fa-clipboard',
                'color'=>'blue-grey',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Suivi des DMT',
                'detail'=>'Suivi des DMT',
                'lien'=>'dmt/dmt/visu_dmt',
                'icon'=>'fa fa-hourglass-1',
                'color'=>'grenat',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Visualisation par 3 typologie',
                'detail'=>'Visualisation par 3 typologie',
                'lien'=>'statistics/statistics/visu_typo',
                'icon'=>'fa fa-eye',
                'color'=>'teal',
                'condition'=> $type_utilisateur_4
            )
            ,array('titre'=>'Recherche avancée Pli et Flux',
                'detail'=>'Recherche avancée Pli et Flux',
                'lien'=>'recherche/avancee',
                'icon'=>'fa fa-search-plus',
                'color'=>'pink',
                'condition'=> $type_utilisateur_4 || $type_utilisateur_100
            )
            ,array('titre'=>'Traitement des KO / Circulaire',
                'detail'=>'Traitement des KO / Circulaire',
                'lien'=>'traitement/KO',
                'icon'=>'fa fa-arrow-circle-right',
                'color'=>'orange',
                'condition'=> ($type_utilisateur_4 && $traitement_ko) || $type_utilisateur_8 || ($type_utilisateur_1 && $traitement_ko) 
            )
            ,array('titre'=>'Saisie',
                'detail'=>'Aller dans l\'interface de saisie de pli',
                'lien'=>'saisie/saisie',
                'icon'=>'fa fa-print',
                'color'=>'light-green',
                'condition'=> $type_utilisateur_2
            )
            ,array('titre'=>'Typage pli',
                'detail'=>'Aller dans l\'interface de typage de pli',
                'lien'=>'typage/typage',
                'icon'=>'fa fa-laptop',
                'color'=>'light-blue',
                'condition'=> $type_utilisateur_2
            )
            ,array('titre'=>'Typage flux',
                'detail'=>'Aller dans l\'interface de typage de flux',
                'lien'=>'push/typage/Pli',
                'icon'=>'fa fa-laptop',
                'color'=>'light-blue',
                'condition'=> $type_utilisateur_5 && $operateur1
            )
            ,array('titre'=>'Saisie flux',
                'detail'=>'Aller dans l\'interface de saisie de flux',
                'lien'=>'push/saisie/saisie_push',
                'icon'=>'fa fa-print',
                'color'=>'teal',
                'condition'=> $type_utilisateur_5 && $operateur1
            )
            ,array('titre'=>'Indicateurs de production',
                'detail'=>'Indicateurs de prod',
                'lien'=>'indicateur/indicateur/visu_indicateur',
                'icon'=>'fa fa-industry',
                'color'=>'light-blue',
                'condition'=> $type_utilisateur_4
            )
        /*
        ---------------------------------------------------------------------------
        ---------                                                       -----------
        ---------                   Administrateur                      -----------
        ---------                                                       -----------
        ---------------------------------------------------------------------------
        */
            ,array('titre'=>'Administration des utilisateurs',
                'detail'=>'Administration des utilisateurs',
                'lien'=>'admin/users',
                'icon'=>'fa fa-user',
                'color'=>'cyan-t',
                'condition'=> $type_utilisateur_1
            )
            ,array('titre'=>'Groupe utilisateur',
                'detail'=>'Groupe utilisateur',
                'lien'=>'admin/groups_user',
                'icon'=>'fa fa-users',
                'color'=>'cyan',
                'condition'=> $type_utilisateur_1
            )
            ,array('titre'=>'Matchage',
                'detail'=>'Matchage',
                'lien'=>'matchage/NewMatchage',
                'icon'=>'fa fa-copy',
                'color'=>'deep-purple',
                'condition'=> $type_utilisateur_1
            )
            ,array('titre'=>'Matchage ch&egrave;ques cadeaux',
                'detail'=>'Matchage ch&egrave;ques cadeaux',
                'lien'=>'matchage/matchage_ckd',
                'icon'=>'fa fa-gift',
                'color'=>'pink',
                'condition'=> $type_utilisateur_1
            )
            ,array('titre'=>'Support',
                'detail'=>'Support',
                'lien'=>'admin/support',
                'icon'=>'fa fa-cogs',
                'color'=>'light-green',
                'condition'=> $type_utilisateur_1
            )
            ,array('titre'=>'Support-push',
                'detail'=>'Support-push',
                'lien'=>'admin/support_push',
                'icon'=>'fa fa-upload',
                'color'=>'teal',
                'condition'=> $type_utilisateur_1
            )
            /*,array('titre'=>'Priorisation des KE',
                'detail'=>'Priorisation des KE',
                'lien'=>'admin/gestion_ke',
                'icon'=>'fa fa-tasks',
                'color'=>'amber',
                'condition'=> $type_utilisateur_1
            )*/
            ,array('titre'=>'Qualit&eacute;',
                'detail'=>'Qualit&eacute;',
                'lien'=>'admin/qualite',
                'icon'=>'fa fa-eyedropper',
                'color'=>'lime',
                'condition'=> $type_utilisateur_1
            )
            ,array('titre'=>'Regroupements des plis',
                'detail'=>'Regroupements des plis',
                'lien'=>'javascript:show_info_grp_pli();',
                'icon'=>'fa fa-inbox',
                'color'=>'lime',
                'condition'=> $type_utilisateur_1,
                'url_js'=>true
            )
            ,array('titre'=>'Regroupements des plis <br> par date-courrier',
                'detail'=>'Regroupements des plis par date-courrier',
                'lien'=>'javascript:show_info_grp_pli_by_older();',
                'icon'=>'fa fa-inbox',
                'color'=>'blue-grey',
                'condition'=> $type_utilisateur_1,
                'url_js'=>true
            )
            ,array('titre'=>'DMT',
                'detail'=>'DMT',
                'lien'=>'admin/dmt',
                'icon'=>'fa fa-tachometer',
                'color'=>'purple',
                'condition'=> $type_utilisateur_1
            )
            ,array('titre'=>'Etat des ch&egrave;ques',
                'detail'=>'Modification état des chèques',
                'lien'=>'cheque/etat_cheque',
                'icon'=>'fa fa-money',
                'color'=>'light-green',
                'condition'=> $type_utilisateur_8
            )
            ,array('titre'=>'Traitement des ch&egrave;ques',
                'detail'=>'Modification état des chèques',
                'lien'=>'cheque/traitement_cheque',
                'icon'=>'fa fa-money',
                'color'=>'light-green',
                'condition'=> $type_utilisateur_1
            )
            ,array('titre'=>'Liste des ch&egrave;ques KO REB',
                'detail'=>'Liste des ch&egrave;ques non envoy&eacute;s en REB',
                'lien'=>'cheque/correspondance_cheque',
                'icon'=>'fa fa-exclamation-triangle',
                'color'=>'deep-orange',
                'condition'=> $type_utilisateur_1 || $type_utilisateur_8
            )
            ,array('titre'=>'Liste des ch&egrave;ques KO VIPS',
                'detail'=>'Liste des ch&egrave;ques KO VIPS',
                'lien'=>'cheque/vips_cheque',
                'icon'=>'fa fa-exclamation-triangle',
                'color'=>'blue-grey',
                'condition'=> $type_utilisateur_1 || $type_utilisateur_8
            )
            ,array('titre'=>'Raccourcis',
                'detail'=>'Liste des raccourcis',
                'lien'=>'javascript:raccourci();',
                'icon'=>'fa fa-exclamation-circle',
                'color'=>'blue',
                'condition'=>  $type_utilisateur_2 || $type_utilisateur_3,
                'url_js'=>true
            )
            ,array('titre'=>'Mes lots de saisie',
                'detail'=>'Lots de saisie',
                'lien'=>'javascript:show_info_lot();',
                'icon'=>'fa fa-exclamation',
                'color'=>'green',
                'condition'=> $type_utilisateur_2,
                'url_js'=>true,
                'id'=>'menu_lot_de_saisie'
            )
            ,array('titre'=>'Regroupements des plis',
                'detail'=>'Regroupements des plis',
                'lien'=>'javascript:show_info_grp_pli();',
                'icon'=>'fa fa-exclamation',
                'color'=>'green',
                'condition'=> $type_utilisateur_2,
                'url_js'=>true,
                'id'=>'menu_regroupement_pli'
            )
            ,array('titre'=>'Regroupements des plis <br/>par date-courrier',
                'detail'=>'',//Regroupements des plis par date-courrier',
                'lien'=>'javascript:show_info_grp_pli_by_older();',
                'icon'=>'fa fa-exclamation',
                'color'=>'green',
                'condition'=> $type_utilisateur_2,
                'url_js'=>true,
                'id'=>'menu_regroupement_pli_par_date'
            )
            ,array('titre'=>'Regroupements des flux <br/> par date de r&eacute;ception',
                'detail'=>'',//Regroupements des flux par date de r&eacute;ception',
                'lien'=>'javascript:afficher_modale_menu(\'mdl_display_group_flux_by_older\');',
                'icon'=>'fa fa-exclamation',
                'color'=>'green',
                'condition'=> $type_utilisateur_5,
                'url_js'=>true,
                'id'=>'menu_regroupement_flux_par_date'
            )  
            ,array('titre'=>'Mes lots de saisie',
                'detail'=>'Lots de saisie',
                'lien'=>'javascript:afficher_modale_menu(\'mdl_display_lot_saisie\');',
                'icon'=>'fa fa-exclamation',
                'color'=>'green',
                'condition'=> $type_utilisateur_5,
                'url_js'=>true,
                'id'=>'menu_mdl_display_lot_saisie'
            )
            ,array('titre'=>'Retraitement',
                'detail'=>'Retraitement de pli',
                'lien'=>'control/retraitement',
                'icon'=>'fa fa-tasks',
                'color'=>'blue',
                'condition'=> $type_utilisateur_3
            )
            ,array('titre'=>'Suivi Circulaire',
                'detail'=>'Suivi Circulaire',
                'lien'=>'circulaire/pli',
                'icon'=>'fa fa-paperclip',
                'color'=>'light-green',
                'condition'=> $type_utilisateur_3
            ) 
            ,array('titre'=>'virements citrix sur la GED',
                'detail'=>'Nouveau saisie virements citrix',
                'lien'=>'citrix/Saisie_citrix',
                'icon'=>'fa fa-credit-card-alt',
                'color'=>'brown',
                'condition'=> $type_utilisateur_3
            )
            ,array('titre'=>'Donn&eacute;es du jour',
                'detail'=>'Donn&eacute;es du jour',
                'lien'=>'javascript:show_info_ctrl();',
                'icon'=>'fa fa-exclamation-circle',
                'color'=>'grey',
                'condition'=> $type_utilisateur_3,
                'url_js'=>true
            )
            ,array('titre'=>'Visualisation saisie citrix',
                'detail'=>'Visualisation de suivi de virement citrix',
                'lien'=>'citrix/visu_citrix',
                'icon'=>'fa fa-eye',
                'color'=>'deep-purple',
                'condition'=> $type_utilisateur_3
            ) 
            ,array('titre'=>'Contrôle des plis OK',
                'detail'=>'Contrôle des plis OK',
                'lien'=>'control/control',
                'icon'=>'fa fa-line-chart',
                'color'=>'indigo',
                'condition'=> $type_utilisateur_3
            )  
            ,array('titre'=>'Retraitement',
                'detail'=>'Traitement de flux - Retraitement',
                'lien'=>'push/saisie/retraitement_push',
                'icon'=>'fa fa-tasks',
                'color'=>'indigo',
                'condition'=> $type_utilisateur_5 && $operateur2
            ) 
            ,array('titre'=>'Saisie niveau 2 ',
                'detail'=>'Traitement de flux - saisie niveau 2',
                'lien'=>'push/saisie/saisie_push2',
                'icon'=>'fa fa-print',
                'color'=>'orange',
                'condition'=> $type_utilisateur_5 && $operateur2
            )    
        );
        // order by ASC pour admin et DESC pour les autres utlisateurs
        $this->prepareData();
        // ajouter au debut de la liste
        // ---------------------------------------------------------
        // Pour type_utilisateur_4
        $this->ajouterAuDebut('Visualisation des courriers');
        // Pour type_utilisateur_3
        $this->ajouterAuDebut('Contrôle des plis OK');
        // ajouter à la fin de la liste
        $this->ajouterALaFin('Raccourcis');
       
    }
    

    public function menu($display_none=null){
       $data ='<div class="collapse navbar-collapse" >
                <ul class="nav navbar-nav navbar-right" >
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="color:white;">
                            <i class="material-icons">view_headline</i>
                        </a>
                        <ul class="dropdown-menu">
                        <li class="header" style="text-align: left !important;margin-left: 12px;">
                        Utilisateur : '.(trim($this->CI->session->login== ""? "non défini ":$this->CI->session->login)).'
                        </li>
                            <li class="body">
                                <ul class="menu" id="menu-menu-id">';
                                     foreach ($this->menu as $cle=>$val){
                                        $raccourci ="";
                                        $afficher = null;
                                        $display = true;
                                        //Vérifier les conditions
                                        if(!empty($val['condition'])){
                                            if($val['condition']==1 || $val['condition']==true){
                                                $afficher = true;
                                            }else{
                                                $afficher = false;
                                            }
                                        }
                                        //Ne pas afficher le menu actif ou autre
                                        if(!empty($display_none)){
                                            if(is_array($display_none)){
                                                if (in_array(trim($val['titre']), $display_none)) {
                                                    $display = false;
                                                }else{
                                                    $display = true;
                                                }
                                            }else{
                                                $display = trim($display_none)==trim($val['titre']) ? false : true;
                                            }
                                        }

                                        // Vérification de menu actif
                                        $disable = "";
                                        if(stristr(current_url(), $val['lien']) == TRUE) {
                                            $disable = 'style="display:none;"';//caché dans la liste le menu actif
                                        }
                                        if($afficher && $display){
                                            $url = "";
                                            if(isset($val['url_js'])){
                                                $url = ($val['lien']);
                                            }else{
                                                $url = site_url($val['lien']);
                                            }

                                            // Menu spécial pour une page  
                                            if(isset($val['url_js'])){
                                                $id = isset($val['id'])?$val['id']:null;
                                                $data.= $this->AjouterMenuSpecilPourUnePage($val['titre'],$val['detail'],$url,$val['icon'],$val['color'],$val['url_js'],$id);
                                            } 
                                            else{// tous les autres menu
                                                $data.=  '<li '.$disable.'>
                                                    <a href="'.$url.'">
                                                        <div class="icon-circle bg-'.$val['color'].'">
                                                            <i class="'.$val['icon'].'"></i>
                                                        </div>
                                                        <div class="menu-info">
                                                            <h4><b>'.$val['titre'].'</b></h4>
                                                            <p>
                                                            '.$val['detail'].'
                                                            </p>
                                                        </div>
                                                    </a>
                                                </li>';
                                            }
                                            
                                        }
                                     }
                                    $data .= $raccourci.'<li>
                                        <a href="javascript:deconnexion();">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">input</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Deconnexion</b></h4>
                                                <p>Aller dans la page de login</p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>';
        echo $data;
    }

    private function prepareData(){
        if($this->CI->session->id_type_utilisateur == 1){
            function function_special_inexist_pour_menu($a, $b) {
                
                return $a['titre'] > $b['titre'] ? 1 : -1;
            };
        }else{
            function function_special_inexist_pour_menu($a, $b) {
                
                return $a['titre'] > $b['titre'] ? -1 : 1;
            };
        }
        usort($this->menu , 'function_special_inexist_pour_menu');
    }

    private function AjouterMenuSpecilPourUnePage($titre,$detail,$lien,$icon,$color,$url_js,$id=null){
        $res = "";
        if(isset($url_js)){
            // ajouter : $('#id_li').show(); ou/et $('.visible_menu').show(); dans le js du fichier courant
            $id_ = "";
            $class = "";
            if(!empty($id)){
                $id_ = 'id = "'.$id.'"';
            }else{
                $class = "visible_menu";
            }
            $res =  '<li class="'.$class.'" style="display:none;" '.$id_.'>
                <a href="'.$lien.'">
                    <div class="icon-circle bg-'.$color.'">
                        <i class="'.$icon.'"></i>
                    </div>
                    <div class="menu-info">
                        <h4><b>'.$titre.'</b></h4>
                        <p>
                        '.$detail.'
                        </p>
                    </div>
                </a>
            </li>';
        }
        return $res;
    }

    private function getParTitre($titre){
        return $this->menu[$this->getIndice($titre)];
    }

    private function getIndice($titre){
        $res= 0;
        if(isset($titre)){
            foreach($this->menu as $cle=>$val){
                if($val['titre']==$titre){
                    $res = $cle;
                }     
            }
        }
        return $res;
    }

    private function ajouterAuDebut($titre){
        $data = $this->getParTitre($titre);
        unset($this->menu[$this->getIndice($titre)]);
        $this->menu = array_merge($this->menu);
        array_unshift($this->menu,$data);
    }

    private function ajouterALaFin($titre){
        $data = $this->getParTitre($titre);
        unset($this->menu[$this->getIndice($titre)]);
        $this->menu = array_merge($this->menu);
        array_push($this->menu,$data);
    }

}
