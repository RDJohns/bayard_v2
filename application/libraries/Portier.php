<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Portier{

	public $valide = FALSE;

	private $CI;

	public function __construct() {
		//redirect(site_url('chantier'));
		$this->valide = FALSE;
		$this->CI =& get_instance();
		$this->CI->load->model('model_login');
		if( isset($this->CI->session->id_utilisateur) && is_numeric($this->CI->session->id_utilisateur) ){
			$this->valide = $this->CI->model_login->valide_user();
		}
	}

	
	public function auto_inspecter_v($brut=FALSE){
		if( !$this->valide ){
			log_message('error', 'Perte de session!'.(isset($_SERVER['REMOTE_ADDR']) ? ': '.$_SERVER['REMOTE_ADDR'] : ''));
			$this->CI->session->sess_destroy();
			$this->valide = FALSE;
			if($brut){
				echo 'Perte de session, merci de vous reconnecter: '.site_url();
			}else{
				show_error('Merci de vous reconnecter: '.site_url(), 200, 'Perte de session!');
			}
			exit();
			//redirect(base_url());
		}
	}
	
	public function auto_inspecter(){
		if( !$this->valide ){
			log_message('error', 'Perte de session!'.(isset($_SERVER['REMOTE_ADDR']) ? ': '.$_SERVER['REMOTE_ADDR'] : ''));
			redirect(base_url());
		}
	}
	
	public function must_admin(){
		if($this->valide){
			if($this->CI->session->id_type_utilisateur != 1){
				log_message('error', 'no acces admin for '.$this->CI->session->id_utilisateur);
				redirect(base_url());
			}
		}else{
			$this->auto_inspecter();
		}
	}
	
	public function must_op(){
		if($this->valide){
			if($this->CI->session->id_type_utilisateur != 2){
				log_message('error', 'no acces operator for '.$this->CI->session->id_utilisateur);
				redirect(base_url());
			}
		}else{
			$this->auto_inspecter();
		}
	}
	
	public function must_calit(){
		if($this->valide){
			if($this->CI->session->id_type_utilisateur != 3){
				log_message('error', 'no acces control for '.$this->CI->session->id_utilisateur);
				redirect(base_url());
			}
		}else{
			$this->auto_inspecter();
		}
	}
	
	public function must_observ(){
		if($this->valide){
			if($this->CI->session->id_type_utilisateur != 4){
				log_message('error', 'no acces observateur for '.$this->CI->session->id_utilisateur);
				redirect(base_url());
			}
		}else{
			$this->auto_inspecter();
		}
	}

    public function must_cdn(){
        if($this->valide){
            if($this->CI->session->id_type_utilisateur != 8){
                log_message('error', 'no acces cdn for '.$this->CI->session->id_utilisateur);
                redirect(base_url());
            }
        }else{
            $this->auto_inspecter();
        }
    }
	
	public function must_op_push($acces=NULL, $ajax=FALSE){
		if($this->valide){
			if($this->CI->session->id_type_utilisateur != 5 || (!is_null($acces) && $this->CI->session->acces_op_push != $acces)){
				log_message('error', 'no acces push traitement '.$acces.' for '.$this->CI->session->id_utilisateur);
				if ($ajax) {
					echo 'Connexion refus&eacute;e, merci de vous reconnecter: '.site_url();
				} else {
					redirect(base_url());
				}
				
			}
		}else{
			if($ajax){
				$this->auto_inspecter_v();
			}else{
				$this->auto_inspecter();
			}
		}
	}

    public function must_admin_cdn(){
        if($this->valide){
            if(!in_array($this->CI->session->id_type_utilisateur,array(1,8))){
                log_message('error', 'no acces cdn/admin for '.$this->CI->session->id_utilisateur);
                redirect(base_url());
            }
        }else{
            $this->auto_inspecter();
        }
    }

	public function deconnexion(){
		if( $this->valide ){
			log_message('info', 'Logout de '.$this->CI->session->id_utilisateur);
			$this->CI->histo->action(2);
		}
		$this->CI->session->sess_destroy();
		$this->valide = FALSE;
	}

}
