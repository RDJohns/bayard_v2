<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Filtres{

    private $CI;

    public function __construct() {
        $this->CI =& get_instance();
    }

    public function get_filtres_view(){
        $this->CI->load->model('push/saisie/model_flux');
        $data_filtres = array(
            'societies' => $this->CI->model_flux->societies()
            ,'sources' => $this->CI->model_flux->sources()
            ,'typologies' => $this->CI->model_flux->typologies(NULL, TRUE)
            ,'current_society' => $this->CI->session->userdata('current_society')
            ,'current_source' => $this->CI->session->userdata('current_source')
            ,'current_typologie' => $this->CI->session->userdata('current_typologie')
        );
        return $this->CI->load->view('push/saisie/filtres', $data_filtres, TRUE);
    }

    public function get_value(){
        $values = array(
            'current_society' => $this->CI->input->post_get('id_soc')
            ,'current_source' => $this->CI->input->post_get('id_src')
            ,'current_typologie' => $this->CI->input->post_get('id_typo')
        );
        $this->CI->session->current_society = is_null($values['current_society']) ? $this->CI->session->userdata('current_society') : $values['current_society'];
        $this->CI->session->current_source = is_null($values['current_source']) ? $this->CI->session->userdata('current_source') : $values['current_source'];
        $this->CI->session->current_typologie = is_null($values['current_typologie']) ? $this->CI->session->userdata('current_typologie') : $values['current_typologie'];
        return $values;
    }

    public function have_session()
    {
        if(is_numeric($this->CI->session->current_society) && is_numeric($this->CI->session->current_source) && is_numeric($this->CI->session->current_typologie)){
            return TRUE;
        }
        return FALSE;
    }

}
