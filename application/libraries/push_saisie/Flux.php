<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Flux{

    private $CI;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->helper('function1');
        $this->CI->load->model('push/saisie/model_flux');
    }

    public function get_view($flux, $statuts, $motif_escalade=TRUE, $motif_trasfert=TRUE, $motif_ks=FALSE, $default_statut=7, $info_ke=NULL){
        $typo_option = array(
            'mail' => $flux->id_src == 1 ? TRUE : NULL
            ,'sftp' => $flux->id_src == 2 ? TRUE : NULL
            ,'bayard' => $flux->soc == 1 ? TRUE : NULL
            ,'milan' => $flux->soc == 2 ? TRUE : NULL
        );
        $data_view = array(
            'flux' => $flux
            ,'socs' => $this->CI->model_flux->societies()
            ,'typos' => $this->CI->model_flux->typologies($typo_option, TRUE)
            ,'statuts' => $statuts
            ,'default_statut' => $default_statut
            ,'with_motif_escalade' => $motif_escalade
            ,'with_motif_transfert' => $motif_trasfert
            ,'with_motif_ks' => $motif_ks
            ,'pjs' => $this->CI->model_flux->my_pjs($flux->id_flux)
            ,'view_abonnements' => ''
            ,'data_ke' => NULL
            ,'nb_files' => NULL
            ,'motif_consigne_flux' => $this->CI->model_flux->motif_consigne($flux->id_flux)
        );
        if(!is_null($info_ke)){
            $data_view['data_ke'] = $info_ke->data_ke;
		    $data_view['nb_files'] = $info_ke->nb_fichier_ke;
        }
        $abonnements = $this->CI->model_flux->my_abonnements($flux->id_flux);
        $this->CI->load->library('push_saisie/abonnement');
        foreach ($abonnements as $key => $abonnement) {
            $data_view['view_abonnements'] .= $this->CI->abonnement->get_view($abonnement, $flux->id_src, $flux->soc);
        }
        if(count($abonnements) < 1){
            for ($i=0; $i < $flux->nb_abonnement; $i++) { 
                $data_view['view_abonnements'] .= $this->CI->abonnement->get_view(NULL, $flux->id_src, $flux->soc, $flux->id_typo);
            }
            if($flux->nb_abonnement == '' || $flux->nb_abonnement == 0){
                $data_view['view_abonnements'] .= $this->CI->abonnement->get_view(NULL, $flux->id_src, $flux->soc, $flux->id_typo);
            }
        }
        return  $this->CI->load->view('push/saisie/flux', $data_view, TRUE);
    }

    public function collecte_data(){
        $id_flux = $this->CI->input->post_get('id_flux');
        if(is_numeric($id_flux)){
            $data = new stdClass();
            $data->id_flux = $id_flux;
            $data->statut_pli = $this->CI->input->post_get('statut_pli');
            $data->typologie = $this->CI->input->post_get('typologie');
            $data->nb_abonnement = $this->CI->input->post_get('nb_abonnement');
            $data->entite = $this->CI->input->post_get('entite');
            $data->societe = $this->CI->input->post_get('societe');
            $data->id_source = $this->CI->input->post_get('id_source');
            $data->motif_escalade = $this->CI->input->post_get('motif_escalade');
            $data->motif_transfert = $this->CI->input->post_get('motif_transfert');
            $data->motif_ks = $this->CI->input->post_get('motif_ks');
            $abonnements = json_decode($this->CI->input->post_get('abonnements'));
            $abonnements = is_array($abonnements) ? $abonnements : array();
            $data->abonnements = array();
            foreach ($abonnements as $abonnement) {
                array_push($data->abonnements, array(
                    'id_flux' => $abonnement->id_flux
                    ,'titre' => $abonnement->titre
                    ,'nom_abonne' => $abonnement->nom_abonne
                    ,'numero_abonne' => $abonnement->numero_abonne
                    ,'nom_payeur' => $abonnement->nom_payeur
                    ,'numero_payeur' => $abonnement->numero_payeur
                    ,'tarif' => u_montant($abonnement->tarif)
                    ,'typologie_abonnement' => $abonnement->typologie_abonnement
                    ,'id_statut' => $abonnement->id_statut
                    ,'motif_ko' => $abonnement->motif_ko
                ));
            }
            $pjs = json_decode($this->CI->input->post_get('pjs'));
            $pjs = is_array($pjs) ? $pjs : array();
            $data->pjs = array();
            foreach ($pjs as $pj) {
                array_push($data->pjs, array(
                    'id_flux' => $pj->id_flux
                    ,'nom_fichier' => $pj->nom_fichier
                    ,'extension_fichier' => $pj->extension_fichier
                    ,'date_injection' => $pj->date_injection == '' ? date('Y-m-d G:i:s') : $pj->date_injection
                    ,'nom_fichier_origine' => $pj->nom_fichier
                    ,'ajoute' => 1//$pj->ajoute
                ));
            }
            return $data;
        }
        return NULL; 
    }

    public function quick_save(){
        $collected_data = $this->collecte_data();
        if(!is_null($collected_data)){
            $data = new stdClass();
            $data->id_flux = $collected_data->id_flux;
            $data->data_pjs = $collected_data->pjs;
            $data->data_abonnement = $collected_data->abonnements;
            $data->data_flux = array(
                'typologie' => $collected_data->typologie
                ,'nb_abonnement' => $collected_data->nb_abonnement
                ,'entite' => $collected_data->entite
                ,'societe' => $collected_data->societe
                ,'motif_escalade' => $collected_data->motif_escalade
                ,'motif_transfert' => $collected_data->motif_transfert
                ,'motif_ks' => $collected_data->motif_ks
            );
            if($this->CI->model_flux->flux_for_me($data->id_flux)){
                if($this->CI->model_flux->quick_save($data)){
                    $this->CI->histo->action(56, '', $data->id_flux);
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    public function escalade(){
        $collected_data = $this->collecte_data();
        if(!is_null($collected_data)){
            $data = new stdClass();
            $data->id_flux = $collected_data->id_flux;
            $data->data_pjs = $collected_data->pjs;
            $data->data_abonnement = $collected_data->abonnements;
            $data->data_flux = array(
                'typologie' => $collected_data->typologie
                ,'nb_abonnement' => $collected_data->nb_abonnement
                ,'entite' => $collected_data->entite
                ,'societe' => $collected_data->societe
                ,'statut_pli' => '4'
                ,'etat_pli_id' => '1'
                ,'flag_traitement_niveau' => '1'
                ,'motif_escalade' => $collected_data->motif_escalade
                ,'motif_transfert' => ''
                ,'motif_ks' => ''
            );
            if($this->CI->model_flux->flux_for_me($data->id_flux)){
                if($this->CI->model_flux->simple_save($data)){
                    $this->CI->histo->action(58, '', $data->id_flux);
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    public function saisie_ko(){
        $collected_data = $this->collecte_data();
        $fx = $this->CI->model_flux->flux_line($collected_data->id_flux);
        if(!is_null($collected_data)){
            if(is_null($fx)){
                log_message('error', 'libraries> push_saisie> flux> saisie_ko: Flux introuvable #'.$collected_data->id_flux);
                return FALSE;
            }
            $data = new stdClass();
            $data->id_flux = $collected_data->id_flux;
            $data->data_pjs = $collected_data->pjs;
            $data->data_abonnement = $collected_data->abonnements;
            $data->link_traitement_motif_consigne = $collected_data->id_flux.date('YmdHis');
            $data->data_flux = array(
                'typologie' => $collected_data->typologie
                ,'nb_abonnement' => $collected_data->nb_abonnement
                ,'entite' => $collected_data->entite
                ,'societe' => $collected_data->societe
                ,'statut_pli' => '7'
                ,'etat_pli_id' => $collected_data->statut_pli
                ,'flag_traitement_niveau' => '2'
                ,'motif_escalade' => $collected_data->motif_escalade
                ,'motif_transfert' => $collected_data->motif_transfert
                ,'motif_ks' => ''
                ,'date_dernier_statut_ko' => date('Y-m-d H:i:s')
            );
            $data->motif_consigne_flux = array(
                'id_flux' => $collected_data->id_flux
                ,'motif_operateur_flux' => $collected_data->motif_transfert
                ,'consigne_client_flux' => ''
                ,'id_etat_pli' => $collected_data->statut_pli
                ,'link_traitement' => $data->link_traitement_motif_consigne
            );
            if($fx->statut_pli == 10){
                $data->data_flux['nb_retraitement'] = is_numeric($fx->nb_retraitement) ? $fx->nb_retraitement + 1 : '1';
                $data->data_flux['flag_retraitement'] = '1';
            }
            if($this->CI->model_flux->flux_for_me($data->id_flux)){
                if($this->CI->model_flux->simple_save($data)){
                    switch ($collected_data->statut_pli) {
                        case 21:
                            $this->CI->histo->action(117, '', $data->id_flux);
                            break;
                        case 22:
                            $this->CI->histo->action(118, '', $data->id_flux);
                            break;
                        case 23:
                            $this->CI->histo->action(119, '', $data->id_flux);
                            break;
                        default:
                    }
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    public function ks(){
        $collected_data = $this->collecte_data();
        $fx = $this->CI->model_flux->flux_line($collected_data->id_flux);
        if(!is_null($collected_data)){
            if(is_null($fx)){
                log_message('error', 'libraries> push_saisie> flux> ks: Flux introuvable #'.$collected_data->id_flux);
                return FALSE;
            }
            $data = new stdClass();
            $data->id_flux = $collected_data->id_flux;
            $data->data_pjs = $collected_data->pjs;
            $data->data_abonnement = $collected_data->abonnements;
            $data->data_flux = array(
                'typologie' => $collected_data->typologie
                ,'nb_abonnement' => $collected_data->nb_abonnement
                ,'entite' => $collected_data->entite
                ,'societe' => $collected_data->societe
                ,'statut_pli' => '7'
                ,'etat_pli_id' => '19'
                ,'flag_traitement_niveau' => '2'
                ,'motif_escalade' => $collected_data->motif_escalade
                ,'motif_transfert' => $collected_data->motif_transfert
                ,'motif_ks' => $collected_data->motif_ks
            );
            $data->motif_consigne_flux = array(
                'id_flux' => $collected_data->id_flux
                ,'motif_operateur_flux' => $collected_data->motif_ks
                ,'consigne_client_flux' => ''
            );
            if($fx->statut_pli == 10){
                $data->data_flux['nb_retraitement'] = is_numeric($fx->nb_retraitement) ? $fx->nb_retraitement + 1 : '1';
                $data->data_flux['flag_retraitement'] = '1';
            }
            if($this->CI->model_flux->flux_for_me($data->id_flux)){
                if($this->CI->model_flux->simple_save($data)){
                    $this->CI->histo->action(90, '', $data->id_flux);
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    public function hp(){
        $id_flux = $this->CI->input->post_get('id_flux');
        if(is_numeric($id_flux)){
            $fx = $this->CI->model_flux->flux_line($id_flux);
            if(is_null($fx)){
                log_message('error', 'libraries> push_saisie> flux> hp: Flux introuvable #'.$id_flux);
                return FALSE;
            }
            $data = new stdClass();
            $data->id_flux = $id_flux;
            $data->data_flux = array(
                'statut_pli' => '13'
                ,'flag_hors_perimetre' => '1'
            );
            if($fx->statut_pli == 3){
                $data->data_flux['flag_traitement_niveau'] = '1';
            }elseif ($fx->statut_pli == 5) {
                $data->data_flux['flag_traitement_niveau'] = '2';
            }elseif ($fx->statut_pli == 10) {
                $data->data_flux['flag_retraitement'] = '1';
            }
            $data->data_anomalie = array(
                'flux_id' => $id_flux
                ,'statut_pli_id' => '13'
                ,'utilisateur' => $this->CI->session->id_utilisateur
                ,'anomalie' => $this->CI->input->post_get('motif')
            );
            if($this->CI->model_flux->flux_for_me($data->id_flux)){
                if($this->CI->model_flux->anomalie_save($data)){
                    $this->CI->histo->action(59, '', $data->id_flux);
                    return TRUE;
                }
            }
            return FALSE;
        }
        log_message('error', 'libraries> push_saisie> flux> hp: Flux introuvable #'.$id_flux);
        return FALSE;
    }

    public function anomalie_pj(){
        $id_flux = $this->CI->input->post_get('id_flux');
        if(is_numeric($id_flux)){
            $data = new stdClass();
            $data->id_flux = $id_flux;
            $data->data_flux = array(
                'statut_pli' => '12'
                ,'flag_anomalie_pj' => '1'
                ,'flag_traitement_niveau' => '1'
            );
            $data->data_anomalie = array(
                'flux_id' => $id_flux
                ,'statut_pli_id' => '12'
                ,'utilisateur' => $this->CI->session->id_utilisateur
                ,'anomalie' => $this->CI->input->post_get('motif')
            );
            if($this->CI->model_flux->flux_for_me($data->id_flux)){
                if($this->CI->model_flux->anomalie_save($data)){
                    $this->CI->histo->action(60, 'Anomalie PJ', $data->id_flux);
                    return TRUE;
                }
            }
            return FALSE;
        }
        log_message('error', 'libraries> push_saisie> flux> anomalie_pj: Flux introuvable #'.$id_flux);
        return FALSE;
    }

    public function anomalie_mail(){
        $id_flux = $this->CI->input->post_get('id_flux');
        if(is_numeric($id_flux)){
            $data = new stdClass();
            $data->id_flux = $id_flux;
            $data->data_flux = array(
                'statut_pli' => '11'
                ,'flag_traitement_niveau' => '1'
            );
            $data->data_anomalie = array(
                'flux_id' => $id_flux
                ,'statut_pli_id' => '11'
                ,'utilisateur' => $this->CI->session->id_utilisateur
                ,'anomalie' => $this->CI->input->post_get('motif')
            );
            if($this->CI->model_flux->flux_for_me($data->id_flux)){
                if($this->CI->model_flux->anomalie_save($data)){
                    $this->CI->histo->action(60, 'Anomalie mail', $data->id_flux);
                    return TRUE;
                }
            }
            return FALSE;
        }
        log_message('error', 'libraries> push_saisie> flux> anomalie_mail: Flux introuvable #'.$id_flux);
        return FALSE;
    }

    public function anomalie_fichier(){
        $id_flux = $this->CI->input->post_get('id_flux');
        if(is_numeric($id_flux)){
            $data = new stdClass();
            $data->id_flux = $id_flux;
            $data->data_flux = array(
                'statut_pli' => '14'
                ,'flag_traitement_niveau' => '1'
            );
            $data->data_anomalie = array(
                'flux_id' => $id_flux
                ,'statut_pli_id' => '14'
                ,'utilisateur' => $this->CI->session->id_utilisateur
                ,'anomalie' => $this->CI->input->post_get('motif')
            );
            if($this->CI->model_flux->flux_for_me($data->id_flux)){
                if($this->CI->model_flux->anomalie_save($data)){
                    $this->CI->histo->action(60, 'Anomalie fichier', $data->id_flux);
                    return TRUE;
                }
            }
            return FALSE;
        }
        log_message('error', 'libraries> push_saisie> flux> anomalie_mail: Flux introuvable #'.$id_flux);
        return FALSE;
    }

    public function data_traite(){
        $collected_data = $this->collecte_data();
        if(!is_null($collected_data)){
            $collected_data->data_flux = array(
                'typologie' => $collected_data->typologie
                ,'nb_abonnement' => $collected_data->nb_abonnement
                ,'entite' => $collected_data->entite
                ,'societe' => $collected_data->societe
                ,'motif_escalade' => $collected_data->motif_escalade
                ,'motif_transfert' => $collected_data->motif_transfert
                ,'motif_ks' => $collected_data->motif_ks
                ,'saisie_par' => $this->CI->session->id_utilisateur
                ,'statut_pli' => '7'
                ,'etat_pli_id' => '1'
                ,'date_lot_saisie' => date('Y-m-d G:i:s')
            );
            $fx = $this->CI->model_flux->flux_line($collected_data->id_flux);
            if(is_null($fx)){
                log_message('error', 'libraries> push_saisie> flux> data_traite: Flux introuvable #'.$collected_data->id_flux);
                return NULL;
            }
            if($fx->statut_pli == 10){
                $collected_data->data_flux['nb_retraitement'] = is_numeric($fx->nb_retraitement) ? $fx->nb_retraitement + 1 : '1';
                $collected_data->data_flux['flag_retraitement'] = '1';
            }
            return $collected_data;
        }else{
            return NULL;
        }
    }

}
