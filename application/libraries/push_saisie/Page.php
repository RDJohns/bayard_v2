<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Page{

	private $CI;

    private $data;
    private $links;

	public function __construct() {
        $this->CI =& get_instance();
        $this->initialise();
    }

	public function initialise(){
        $this->data = array();
        $this->data['additional_css'] = '';
        $this->data['additional_js'] = '';
        $this->data['id_utilisateur'] = $this->CI->session->id_utilisateur;
        $this->data['type_utilisateur'] = $this->CI->session->id_type_utilisateur;
        $this->data['type_operateur'] = $this->CI->session->acces_op_push;
		$this->data['page_titre'] = 'GED-BAYARD - Traitement de flux';
        $this->data['login_user'] = $this->CI->session->login;
        $this->links = array();
    }

    public function add_links($titre, $lien, $icon='turned_in', $color='blue'){
        $link = array(
            'titre' => $titre
            ,'lien' => $lien
            ,'icon' => $icon
            ,'color' => $color
        );
        array_push($this->links, $link);
    }

	public function set_titre($titre){
        $this->data['page_titre'] .= ' - '.$titre;
    }

	public function set_my_url($my_url){
        $this->data['my_url'] = $my_url;
    }

	public function add_js($js){
        $this->data['additional_js'] .= '<script src="'.base_url('assets/js/')."$js.js?".'12"></script>';
    }

    public function add_css($css){
        $this->data['additional_css'] .= '<link rel="stylesheet" href="'.base_url('assets/css/')."$css.css?".'2">';
    }

	public function difine_var($name, $val){
		$this->data[$name] = $val;
	}

	public function afficher($contenu = NULL){
        $this->data['links'] = $this->links;;
        if(!is_null($contenu)){
            $this->data['contenu'] = $contenu;
        }
        $this->CI->load->view('push/saisie/page', $this->data);
    }

}
