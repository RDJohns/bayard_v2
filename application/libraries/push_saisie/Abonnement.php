<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Abonnement{

    private $CI;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->helper('function1');
        $this->CI->load->model('push/saisie/model_flux');
    }

    public function get_view($abonnement=NULL, $source=NULL, $societe=NULL, $typo_init=NULL){
        $typo_option = array(
            'mail' => $source == 1 ? TRUE : NULL
            ,'sftp' => $source == 2 ? TRUE : NULL
            ,'bayard' => $societe == 1 ? TRUE : NULL
            ,'milan' => $societe == 2 ? TRUE : NULL
        );
        if(is_null($abonnement)){
            $abonnement = new stdClass();
            $abonnement->numero_abonne = '';
            $abonnement->nom_abonne = '';
            $abonnement->numero_payeur = '';
            $abonnement->nom_payeur = '';
            $abonnement->titre = '';
            $abonnement->tarif = '0';
            $abonnement->typologie_abonnement = is_numeric($typo_init) ? $typo_init : '';
            $abonnement->id_statut = '1';
            $abonnement->motif_ko = '';
        }
        $data_abonnement = array(
            'typos' => $this->CI->model_flux->typologies($typo_option)
            ,'titres' => $this->CI->model_flux->titres($societe)
            ,'statuts' => $this->CI->model_flux->statut_abonnement()
            ,'abonnement' => $abonnement
        );
        return $this->CI->load->view('push/saisie/abonnement', $data_abonnement, TRUE);
    }

    public function get_options_titres($societe=NULL){
        $titres = $this->CI->model_flux->titres($societe);
        $options = '';
        foreach ($titres as $key => $titre) {
            $options .= '<option value="'.$titre->id_titre.'" '.($key == 0 ? 'selected' : '').' title="'.$titre->code.'" data-subtext=" - '.$titre->code.'">'.$titre->titre.'</option>';
        }
        return $options;
    }

    public function get_options_typos($source=NULL, $societe=NULL){
        $typo_option = array(
            'mail' => $source == 1 ? TRUE : NULL
            ,'sftp' => $source == 2 ? TRUE : NULL
            ,'bayard' => $societe == 1 ? TRUE : NULL
            ,'milan' => $societe == 2 ? TRUE : NULL
        );
        $typos = $this->CI->model_flux->typologies($typo_option);
        $options = '';
        foreach ($typos as $key => $typo) {
            $options .= '<option value="'.$typo->id_typologie.'" '.($key == 0 ? 'selected' : '').' >'.$typo->typologie.'</option>';
        }
        return $options;
    }
    
}
