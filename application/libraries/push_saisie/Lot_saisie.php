<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Lot_saisie{

    private $CI;
    private $force_new;
    public $last_lot_saisie;
    public $is_new;
    private $no_lot_saisie;
    private $label_advantage;
    private $num_incremented;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->library('push_saisie/filtres');
    }

    public function initialise($force_new=FALSE){
        $this->force_new = $force_new;
        $this->CI->load->model('push/saisie/model_flux');
        $this->last_lot_saisie = $this->CI->model_flux->last_lot_saisie($this->CI->session->id_utilisateur);
        $this->is_new = FALSE;
        $this->no_lot_saisie = FALSE;
        if(is_null($this->last_lot_saisie)){//no lot saisie today
            $this->is_new = TRUE;
            $this->last_lot_saisie = $this->CI->model_flux->last_lot_saisie($this->CI->session->id_utilisateur, FALSE);
            $this->no_lot_saisie = is_null($this->last_lot_saisie);
            if(!$this->CI->filtres->have_session() && !is_null($this->last_lot_saisie)){//no session et avec lot saisie
                $this->CI->session->current_society = $this->last_lot_saisie->id_soc;
                $this->CI->session->current_source = $this->last_lot_saisie->id_source;
                $this->CI->session->current_typologie = $this->last_lot_saisie->id_typologie;
            }
        }else{//avec lot saisie today
            if(!$this->CI->filtres->have_session() || !$this->force_new) {//no session ou !force_new
                $this->CI->session->current_society = $this->last_lot_saisie->id_soc;
                $this->CI->session->current_source = $this->last_lot_saisie->id_source;
                $this->CI->session->current_typologie = $this->last_lot_saisie->id_typologie;
            }
        }
        if ($this->is_new || $this->force_new) {
            $this->creer_lot_saisie();
        }else{
            $this->num_incremented = $this->last_lot_saisie->num;
            $this->label_advantage = $this->last_lot_saisie->lot_advantage;
            $this->CI->session->current_label_advantage = $this->label_advantage;
            $this->CI->session->current_num_lot_saisie = $this->num_incremented;
            $this->CI->session->current_login_lot_saisie = $this->last_lot_saisie->login;
            $this->CI->session->current_id_lot_saisie = $this->last_lot_saisie->id;
        }
    }

    private function creer_lot_saisie(){
        if($this->no_lot_saisie){
            $this->num_incremented = 1;
        }else {
            $this->num_incremented = $this->last_lot_saisie->num + 1;
        }
        $this->label_advantage = $this->num_incremented.'_'.$this->CI->session->login;
        $this->CI->session->current_label_advantage = $this->label_advantage;
        $this->CI->session->current_num_lot_saisie = $this->num_incremented;
        $this->CI->session->current_login_lot_saisie = $this->CI->session->login;
        $this->CI->session->current_id_lot_saisie = '0';
    }

    public function compatible($lot_saisie_data_save, $id_soc, $id_source, $id_typo){
        return ($lot_saisie_data_save->id_soc == $id_soc 
            && $lot_saisie_data_save->id_source == $id_source 
            && $lot_saisie_data_save->id_typologie == $id_typo);
    }

    public function data_save(){
        $this->CI->load->model('push/saisie/model_flux');
        $data = $this->CI->model_flux->last_lot_saisie($this->CI->session->id_utilisateur);
        if($this->CI->session->userdata('current_id_lot_saisie') == 0){
            $data = new stdClass();
            $data->id_user = $this->CI->session->id_utilisateur;
            $data->id_soc = $this->CI->session->current_society;
            $data->id_source = $this->CI->session->current_source;
            $data->id_typologie = $this->CI->session->current_typologie;
            $data->num = $this->CI->session->current_num_lot_saisie;
            $data->login = $this->CI->session->current_login_lot_saisie;
        }elseif (!is_numeric($this->CI->session->userdata('current_id_lot_saisie'))) {
            $data = NULL;
        }
        return $data;
    }

    public function get_init_view($lbl_bouton='Traiter', $link_init='', $message=''){
        $data_view = array(
            'message' => $message
            ,'link_init' => $link_init
            ,'lbl_bouton' => $lbl_bouton
            ,'with_return' => FALSE
            ,'id_lot_saisie' => $this->CI->session->userdata('current_id_lot_saisie')
        );
        $data_view['label_advantage'] = $this->CI->session->current_label_advantage;
        $this->CI->load->library('push_saisie/filtres');
        $data_view['filtres'] = $this->CI->filtres->get_filtres_view();
        if($this->is_new){
            return $this->CI->load->view('push/saisie/initial', $data_view, TRUE);
        }else{
            if ($this->force_new) {
                $data_view['with_return'] = TRUE;
                return $this->CI->load->view('push/saisie/initial', $data_view, TRUE);
            } else {
                $data_view['id_soc'] = $this->CI->session->current_society;
                $data_view['id_source'] = $this->CI->session->current_source;
                $data_view['id_typologie'] = $this->CI->session->current_typologie;
                $data_view['soc'] = $this->last_lot_saisie->nom_societe;
                $data_view['src'] = $this->last_lot_saisie->source;
                $data_view['typo'] = $this->last_lot_saisie->typologie;
                $data_view['nb_pli_in_this_lot_saisie'] = $this->CI->model_flux->nb_flux_for_lot_saisie($this->last_lot_saisie->id);
                $data_view['direct_load_pli'] = $this->CI->input->post_get('direct') == 1 ? TRUE : FALSE;
                return $this->CI->load->view('push/saisie/continue', $data_view, TRUE);
            }
            
        }
    }

}
