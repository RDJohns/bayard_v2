<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Data_cba{

    private $CI;

    private $data_saved;
    private $data_pli;
    private $data_cheque;
    private $data_mvmnt_lines;
    private $data_to_cba;

    public $can_to_batch;
    public $error_msg;
    private $num_payeur;
    private $payeur;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model('saisie/model_cba');
        $this->num_payeur = NULL;
        $this->payeur = array();
    }

    public function set_data($data_saved)
    {
        $this->data_saved = $data_saved;
        if($this->can_send_to_batch() && $this->CI->model_cba->no_sent($this->data_saved->id_pli)){
            $this->collect_data_to_cba();
            $this->data_saved->data_pli['in_cba'] = 1;
            $this->data_saved->data_to_cba = $this->data_to_cba;
            $this->data_saved->saisie_batch = TRUE;
        }else{
            // $this->remove_cba();
        }
    }

    public function to_cba()
    {
        $this->data_pli = $this->CI->model_cba->data_pli($this->data_saved->id_pli);
        // $this->collect_data_to_cba();
        if($this->data_pli->in_cba == 1){//in CBA
            $this->CI->model_cba->update_cba($this->data_to_cba, $this->data_saved->id_pli);
        }else{//not yet in cba
            $this->CI->model_cba->update_cba($this->data_to_cba, $this->data_saved->id_pli);
            // $this->CI->model_cba->save_cba($this->data_to_cba, $this->data_saved->id_pli);
        }
    }

    public function remove_from_cba()
    {
        $this->remove_cba();
    }

    private function collect_data_to_cba()
    {
        $this->data_to_cba = array();
        foreach ($this->data_mvmnt_lines as $key_mvmnt => $data_mvmnt) {
            array_push($this->data_to_cba, array(
                'id_prest' => $this->data_saved->id_pli
                ,'dnr_nbr' => $data_mvmnt['payeur']->ctm_nbr//N° client payeur
                ,'dctm_ttl' => $data_mvmnt['payeur']->ctm_ttl//Civilité payeur
                ,'datn_end' => $data_mvmnt['payeur']->atn_end//Nom payeur
                ,'datn_1st' => $data_mvmnt['payeur']->atn_1st//Prénom payeur
                ,'dcmp_nme' => $data_mvmnt['payeur']->cmp_nme//Raison sociale payeur
                ,'ddpt_nme' => $data_mvmnt['payeur']->dpt_nme//Complément Raison sociale payeur
                ,'dstr_1st' => $data_mvmnt['payeur']->str_1st//Volet1 adresse payeur
                ,'dstr_2nd' => $data_mvmnt['payeur']->str_2nd//Volet2 adresse payeur
                ,'dstr_3rd' => $data_mvmnt['payeur']->str_3rd//Volet3 adresse payeur
                ,'dzip_cde' => $data_mvmnt['payeur']->zip_cde//Code Postal payeur
                ,'dctm_ste' => $data_mvmnt['payeur']->ctm_ste//Code Etat payeur
                ,'dctm_cty' => $data_mvmnt['payeur']->ctm_cty//Ville payeur
                ,'dcun_typ' => $data_mvmnt['payeur']->cun_typ//Code Pays payeur
                ,'dpho_nbr' => $data_mvmnt['payeur']->pho_nbr//N° Téléphone Domicile payeur
                ,'dpho_nbr2' => $data_mvmnt['payeur']->pho_nbr2//N° Téléphone Bureau payeur
                ,'dpho_nbr3' => $data_mvmnt['payeur']->pho_nbr3//N° Téléphone Portable payeur
                ,'dbir_dte' => $data_mvmnt['payeur']->bir_dte//Date de Naissance AAAAMMJJ payeur
                ,'dsex_cde' => $data_mvmnt['payeur']->sex_cde//Code Sexe payeur
                ,'dadr_email' => $data_mvmnt['payeur']->adr_emal//Adresse Email payeur
                ,'dpmo_adr' => $data_mvmnt['payeur']->pmo_adr//Top prospection Adresse payeur
                ,'dpmo_pho' => $data_mvmnt['payeur']->pmo_phn//Top prospection Téléphone (les 3) payeur
                ,'dpmo_fax' => $data_mvmnt['payeur']->pmo_fax//Top prospection Fax payeur
                ,'dpmo_eml' => $data_mvmnt['payeur']->pmo_eml//Top prospection Email payeur
                ,'dpromo' => $data_mvmnt['payeur']->promo//Top prospection payeur
                ,'dpmo_sms' => $data_mvmnt['payeur']->pmo_sms//Top prospection SMS payeur
                ,'ctm_nbr' => $data_mvmnt['abonne']->ctm_nbr//N° client abonne
                ,'ctm_ttl' => $data_mvmnt['abonne']->ctm_ttl//Civilité abonne
                ,'atn_end' => $data_mvmnt['abonne']->atn_end//Nom abonne
                ,'atn_1st' => $data_mvmnt['abonne']->atn_1st//Prénom abonne
                ,'cmp_nme' => $data_mvmnt['abonne']->cmp_nme//Raison sociale abonne
                ,'dpt_nme' => $data_mvmnt['abonne']->dpt_nme//Complément Raison sociale abonne
                ,'str_1st' => $data_mvmnt['abonne']->str_1st//Volet1 adresse abonne
                ,'str_2nd' => $data_mvmnt['abonne']->str_2nd//Volet2 adresse abonne
                ,'str_3rd' => $data_mvmnt['abonne']->str_3rd//Volet3 adresse abonne
                ,'zip_cde' => $data_mvmnt['abonne']->zip_cde//Code Postal abonne
                ,'ctm_ste' => $data_mvmnt['abonne']->ctm_ste//Code Etat abonne
                ,'ctm_cty' => $data_mvmnt['abonne']->ctm_cty//Ville abonne
                ,'cun_typ' => $data_mvmnt['abonne']->cun_typ//Code Pays abonne
                ,'pho_nbr' => $data_mvmnt['abonne']->pho_nbr//N° Téléphone Domicile abonne
                ,'pho_nbr2' => $data_mvmnt['abonne']->pho_nbr2//N° Téléphone Bureau abonne
                ,'pho_nbr3' => $data_mvmnt['abonne']->pho_nbr3//N° Téléphone Portable abonne
                ,'bir_dte' => $data_mvmnt['abonne']->bir_dte//Date de Naissance AAAAMMJJ abonne
                ,'sex_cde' => $data_mvmnt['abonne']->sex_cde//Code Sexe abonne
                ,'adr_email' => $data_mvmnt['abonne']->adr_emal//Adresse Email abonne
                ,'pmo_adr' => $data_mvmnt['abonne']->pmo_adr//Top prospection Adresse abonne
                ,'pmo_pho' => $data_mvmnt['abonne']->pmo_phn//Top prospection Téléphone (les 3) abonne
                ,'pmo_fax' => $data_mvmnt['abonne']->pmo_fax//Top prospection Fax abonne
                ,'pmo_eml' => $data_mvmnt['abonne']->pmo_eml//Top prospection Email abonne
                ,'promo' => $data_mvmnt['abonne']->promo//Top prospection abonne
                ,'pmo_sms' => $data_mvmnt['abonne']->pmo_sms//Top prospection SMS abonne
                ,'due_dte' => ''
                ,'po_num' => ''
                ,'slm_nbr' => ''
                ,'ref_ctm' => ''
                ,'itm_num' => $data_mvmnt['promo']->code_produit
                ,'pmo_cde' => $data_mvmnt['promo']->code_promo
                ,'pmo_chc' => $data_mvmnt['promo']->code_choix
                ,'bil_org' => $this->data_saved->pli['societe'] == 1 ? '0' : 'M'
                ,'qty_ord' => $data_mvmnt['qtt']
                ,'disc_per' => ''
                ,'siret' => ''
                ,'srv_cde' => ''
                ,'doc_pth' => ''//$this->CI->model_cba->name_doc($this->data_cheque['id_doc'])
                ,'amt_pd' => $key_mvmnt == 0 ? $this->data_cheque['montant'] : ''
                ,'chk_num' => $key_mvmnt == 0 ? substr($this->data_cheque['cmc7'], 0, 7) : ''
                ,'card_num' => ''
                ,'exp_dte' => ''
                ,'sec_cde' => ''
                ,'bank_idn' => ''
                ,'acct_nbr' => ''
                ,'csn_dte' => ''
                ,'id_pli' => $this->data_saved->id_pli
                ,'mvmnt_ui_id' => $data_mvmnt['mvmnt']['ui_id']
                ,'societe' => $this->data_saved->pli['societe']
            ));
        }
    }

    private function remove_cba()
    {
        $this->CI->model_cba->remove_cba($this->data_saved->id_pli);
    }

    private function can_send_to_batch()
    {
        $this->can_to_batch = FALSE;
        $this->error_msg = '';
        $this->data_cheque = NULL;
        $this->data_mvmnt_lines = array();
        // if (in_array($this->data_saved->pli['flag_traitement'], array(6, 9))) {//in status OK
        if (in_array($this->data_saved->statut, array(1))) {//in status OK
            if(count($this->data_saved->paiements) == 1 && count($this->data_saved->cheques) == 1){//have only one paiment and is by Cheque
                $this->data_cheque = $this->data_saved->cheques[0];
                if($this->data_cheque['etranger'] == 1){//the cheque isn't fr
                    $this->can_to_batch = FALSE;
                    $this->error_msg = '(BATCH) Le cheque est étranger';
                    return FALSE;
                }
                $with_num_payeur = FALSE;
                $sommes = 0;
                $valide_sommes = TRUE;
                foreach ($this->data_saved->mvmnts as $key_mvmnt => $mvmnt) {
                    $uid = $mvmnt['ui_id'];
                    $payeur = $this->data_saved->mvmnt_payeurs->$uid;
                    $abonne = $this->data_saved->mvmnt_abonnes->$uid;
                    $promo =  $this->data_saved->mvmnt_promos->$uid;
                    $qtt = $this->data_saved->mvmnt_qtt[$uid];
                    $data_mvmnt = array(
                        'mvmnt' => $mvmnt
                        ,'payeur' => $this->restitut_adress($payeur)
                        ,'abonne' => $this->restitut_adress($abonne)
                        ,'promo' => $promo
                        ,'qtt' => $qtt
                    );//echo var_dump($promo);die();exit();
                    $data_promo = $this->CI->model_cba->get_promo_choix_prod($promo);
                    if(is_numeric($data_promo->tarif_ttc)){
                        $sommes += $data_promo->tarif_ttc * $qtt;
                    }else{
                        $valide_sommes = FALSE;
                    }
                    array_push($this->payeur, $data_mvmnt['payeur']);
                    $num_payeur = $data_mvmnt['payeur']->ctm_nbr;
                    if(is_numeric($num_payeur)){
                        $with_num_payeur = TRUE;
                        if(is_null($this->num_payeur)){
                            $this->num_payeur = $num_payeur;
                        }else{
                            if($this->num_payeur != $num_payeur){
                                $this->can_to_batch = FALSE;
                                $this->error_msg = '(BATCH) Numéro payeur multiple';
                                return FALSE;
                            }
                        }
                    }
                    if(TRUE) {
                        array_push($this->data_mvmnt_lines, $data_mvmnt);//mvmnt with his payeur, abonne and promo
                    }
                }
                /*if(count($this->data_mvmnt_lines) < 1){//have valide mvmnt
                    return FALSE;
                }*/
                if($valide_sommes && is_numeric($this->data_cheque['montant'])){
                    if($sommes > $this->data_cheque['montant']){
                        $this->can_to_batch = FALSE;
                        $this->error_msg = '(BATCH) Montant chèque insuffisant pour un total de '.$sommes.'€';
                        return FALSE;
                    }
                }
                $id_pr = NULL;
                foreach ($this->payeur as $key_p => $value_p) {
                    if(is_null($id_pr)) {
                        $id_pr = $with_num_payeur ? $value_p->ctm_nbr : $value_p->atn_end;
                    }else{
                        $autre_pr = $with_num_payeur ? $value_p->ctm_nbr : $value_p->atn_end;
                        if($id_pr != $autre_pr){
                            $this->can_to_batch = FALSE;
                            $this->error_msg = '(BATCH) Payeur multiple';
                            return FALSE;
                        }
                    }
                }
                if(count($this->data_mvmnt_lines) < count($this->data_saved->mvmnts)){//all his mvmnts are valide
                    $this->can_to_batch = FALSE;
                    $this->error_msg = '(BATCH) Mouvement invalide';
                    return FALSE;
                }
                $this->can_to_batch = TRUE;
                return TRUE;
            }else{
                $this->can_to_batch = FALSE;
                $this->error_msg = '(BATCH) Mode de paiement incompatible';
                return FALSE;
            }
        }else{
            $this->can_to_batch = FALSE;
            $this->error_msg = '(BATCH) Pli KO ou CI (ou chèque en anomalie), passez en mode saisie directe Advantage';
            return FALSE;
        }
    }

    private function restitut_adress($adress)
    {
        if(trim($adress->ctm_nbr) != ''){
            $client = $this->CI->model_cba->get_data_client($adress->ctm_nbr);
            if(!is_null($client)){
                $adress->ctm_nbr = $client->ctm_nbr;
                $adress->str_1st = '';//$client->str_1st;
                $adress->str_2nd = '';//$client->str_2nd;
                $adress->str_3rd = '';//$client->str_3rd;
                $adress->zip_cde = '';//$client->zip_cde;
                $adress->ctm_cty = '';//$client->ctm_cty;
                $adress->ctm_ste = '';//$client->ctm_ste;
                $adress->cun_typ = '';//$client->cun_typ;
                $adress->ctm_ttl = trim($adress->ctm_ttl) == trim($client->ctm_ttl) ? '' : $adress->ctm_ttl;
                $adress->atn_1st = trim($adress->atn_1st) == trim($client->atn_1st) ? '' : $adress->atn_1st;
                $adress->atn_end = trim($adress->atn_end) == trim($client->atn_end) ? '' : $adress->atn_end;
                $adress->cmp_nme = trim($adress->cmp_nme) == trim($client->cmp_nme) ? '' : $adress->cmp_nme;
                $adress->dpt_nme = trim($adress->dpt_nme) == trim($client->dpt_nme) ? '' : $adress->dpt_nme;
                $adress->pho_nbr = trim($adress->pho_nbr) == trim($client->pho_nbr) ? '' : $adress->pho_nbr;
                $adress->pho_nbr2 = trim($adress->pho_nbr2) == trim($client->pho_nbr2) ? '' : $adress->pho_nbr2;
                $adress->pho_nbr3 = trim($adress->pho_nbr3) == trim($client->pho_nbr3) ? '' : $adress->pho_nbr3;
                $adress->bir_dte = trim($adress->bir_dte) == trim($client->bir_dte) ? '' : $adress->bir_dte;
                $adress->sex_cde = trim($adress->sex_cde) == trim($client->sex_cde) ? '' : $adress->sex_cde;
                $adress->adr_emal = trim($adress->adr_emal) == trim($client->adr_emal) ? '' : $adress->adr_emal;
                $adress->pmo_adr = trim($adress->pmo_adr) == trim($client->pmo_adr) ? '' : $adress->pmo_adr;
                $adress->pmo_phn = trim($adress->pmo_phn) == trim($client->pmo_phn) ? '' : $adress->pmo_phn;
                $adress->pmo_fax = trim($adress->pmo_fax) == trim($client->pmo_fax) ? '' : $adress->pmo_fax;
                $adress->pmo_eml = trim($adress->pmo_eml) == trim($client->pmo_eml) ? '' : $adress->pmo_eml;
                $adress->promo = trim($adress->promo) == trim($client->promo) ? '' : $adress->promo;
                //$adress->pmo_sms = trim($adress->pmo_sms) == trim($client->pmo_sms) ? '' : $adress->pmo_sms;
                /*if(trim($adress->atn_1st) != '' || trim($adress->atn_end) != '' || trim($adress->bir_dte) != '') {
                }*/
            }
        }
        return $adress;
    }

}
