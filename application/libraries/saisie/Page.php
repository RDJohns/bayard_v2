<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Page{

	private $CI;

    private $data;
    private $links;

	public function __construct() {
        $this->CI =& get_instance();
        $this->initialise();
    }

	public function initialise(){
        $this->data = array();
        $this->data['additional_css'] = '';
        $this->data['additional_js'] = '';
        $this->data['id_utilisateur'] = $this->CI->session->id_utilisateur;
        $this->data['type_utilisateur'] = $this->CI->session->id_type_utilisateur;
		$this->data['page_titre'] = 'GED-BAYARD';
        $this->data['login_user'] = $this->CI->session->login;
        $this->links = array();
    }

    public function add_links($titre, $lien){
        $link = array(
            'titre' => $titre
            ,'lien' => $lien
        );
        array_push($this->links, $link);
    }

	public function set_titre($titre){
        $this->data['page_titre'] .= ' - '.$titre;
    }

	public function add_js($js){
        $this->data['additional_js'] .= '<script src="'.base_url('assets/js/')."$js.js?".'95"></script>';
    }

    public function add_css($css){
        $this->data['additional_css'] .= '<link rel="stylesheet" href="'.base_url('assets/css/')."$css.css?".date('s').'">';
    }

	public function difine_var($name, $val){
		$this->data[$name] = $val;
	}

	public function afficher($contenu = NULL){
        $this->data['links'] = $this->links;;
        if(!is_null($contenu)){
            $this->data['contenu'] = $contenu;
        }
        $this->CI->load->view('saisie/page', $this->data);
    }

}
