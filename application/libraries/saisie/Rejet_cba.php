<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Rejet_cba{

    private $CI;
    private $rep = './depot/rejet_batch/';
    private $depot_temp;
    private $depot;

    public function __construct()
    {
        $this->depot_temp = $this->rep.'temp/';
        $this->depot = $this->rep.'depot/';
        $this->CI =& get_instance();
        $this->CI->load->helper('file');
        $this->CI->load->model('saisie/model_cba');
        $this->CI->load->model('saisie/model_pli_saisie');
        $this->get_files_rejet();
    }

    private function get_files_rejet()
    {
        $all = get_filenames($this->depot_temp, FALSE);
        foreach ($all as $key => $one) {
            if(preg_match("#index\.html$#", $one)){
                continue;
            }
            // echo $one;
            if(!$this->CI->model_cba->is_traited_file($one)) {
                $this->traite_one_file($one);
            }
        }
    }

    private function traite_one_file($fl)
    {
        $with_ligne = FALSE;
        $id_file = date('U').mt_rand().mt_rand();
        $data_to_save = array(
            'rejet_cba_files' => array(
                'nom' => $fl
                ,'dt_nom' => $this->extract_dt_fl($fl)
                ,'id' => $id_file
            )
            ,'rejet_cba_lignes' => array()
            ,'id_plis' => array()
            ,'id_plis_ok' => array()
            ,'id_plis_ko' => array()
        );
        if (($contenu = fopen($this->depot_temp.$fl, "r")) !== FALSE) {
            while (($ligne = fgetcsv($contenu, 1000, "|")) !== FALSE) {
                $nb_col = count($ligne);
                $id_pli = $this->trim_data($ligne[0]);
                $flag = $this->trim_data($ligne[1]);
                if($nb_col == 3 && is_numeric($id_pli) && is_numeric($flag)){
                    array_push($data_to_save['rejet_cba_lignes'], array(
                        'id_pli' => $id_pli
                        ,'flag_retour' => $flag
                        ,'code_erreur' => $this->trim_data($ligne[2])
                        ,'id_file' => $id_file
                    ));
                    array_push($data_to_save['id_plis'], $id_pli);
                    if ($flag == 1) {
                        array_push($data_to_save['id_plis_ok'], $id_pli);
                    }else {
                        array_push($data_to_save['id_plis_ko'], $id_pli);
                    }
                    $with_ligne = TRUE;
                }else{
                    log_message('error', 'rejet batch: libraries>saisie>rejet_batch>traite_one_file: ligne non conforme>fichier: '.$fl.'>'.implode('|',$ligne));
                }
            }
            fclose($contenu);
            if(!$with_ligne){
                log_message('error', 'rejet batch: libraries>saisie>rejet_batch>traite_one_file: aucune ligne>fichier: '.$fl);
            }//var_dump($data_to_save);die();
            if($this->CI->model_cba->appli_rejet_batch($data_to_save)){
                if(copy($this->depot_temp.$fl, $this->depot.$fl)) {
                    if(!unlink($this->depot_temp.$fl)) {
                        log_message('error', 'rejet batch: libraries>saisie>rejet_batch>traite_one_file: erreur suppression>fichier: '.$fl);
                    }
                }else{
                    log_message('error', 'rejet batch: libraries>saisie>rejet_batch>traite_one_file: erreur copy>fichier: '.$fl);
                }
            }
        }else{
            log_message('error', 'rejet batch: libraries>saisie>rejet_batch>traite_one_file: fichier illisible>fichier: '.$fl);
        }
    }

    private function trim_data($src)
    {
        $src = str_replace('"', '', $src);
        $src = str_replace("'", '', $src);
        $src = str_replace(" ", '', $src);
        return trim($src);
    }

    private function extract_dt_fl($fl)
    {
        $tab = explode('_', $fl);
        if(isset($tab[1])){
            $str_dt = $tab[1];
            $dt = DateTime::createFromFormat("Ymdhis", $str_dt);
            if($dt !== FALSE){
                return $dt->format('Y-m-d h:i:s');
            }
        }
        return NULL;
    }

}
