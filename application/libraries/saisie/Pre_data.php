<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pre_data{

    private $CI;
    private $src;
    public $chqs;
    public $chqs_kd;
    public $mvmnts;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->chqs = array();
        $this->chqs_kd = array();
        $this->mvmnts = array();
    }

    public function set_predata($id_pli)
    {
        $this->src = $this->CI->model_pli_saisie->get_predata($id_pli);
        foreach ($this->src as $key => $doc) {
            $data = new stdClass();
            $data->src = $doc;
            if($doc->id_type_document == 2 || is_numeric(trim($doc->cmc7))){
                $data->is_with_cmc7 = is_numeric(trim($doc->cmc7)) ? TRUE : FALSE;
                $data->is_with_nom_client = strlen(trim($doc->n_client)) > 0 ? TRUE : FALSE;
                array_push($this->chqs, $data);
            }elseif (in_array($doc->id_type_document, array(3,4,7,12)) || is_numeric(trim($doc->abonne_payeur)) || is_numeric(trim($doc->abonne_destinataire)) || strlen(trim($doc->code_promo)) > 0 || strlen(trim($doc->code_titre)) > 0) {
                $data->is_with_num_payeur = is_numeric(trim($doc->abonne_payeur)) ? TRUE : FALSE;
                $data->is_with_nom_payeur = strlen(trim($doc->nom)) > 0 ? TRUE : FALSE;
                $data->is_with_num_ab = is_numeric(trim($doc->abonne_destinataire)) ? TRUE : FALSE;
                $data->is_with_code_promo = strlen(trim($doc->code_promo)) > 0 ? TRUE : FALSE;
                if(strlen(trim($doc->code_titre)) > 0){
                    $data->id_titre = $this->CI->model_pli_saisie->get_id_titre(trim($doc->code_titre));
                    $data->is_with_id_titre = is_numeric($data->id_titre) ? TRUE : FALSE;
                }
                array_push($this->mvmnts, $data);
            }
        }
    }

}
