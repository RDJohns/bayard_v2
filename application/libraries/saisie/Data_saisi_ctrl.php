<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Data_saisi_ctrl{

    private $CI;

    private $data_fields;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->data_fields = json_decode($this->CI->input->post_get('field_saisie_ctrl'));
    }

    public function saisie_values($data_saved){
        $data_saisie_ctrl = array();
        $dt_saisie = date('Y-m-d H:i:s');
        foreach ($this->data_fields as $key => $field) {
            array_push($data_saisie_ctrl, array(
                'id_pli' => $data_saved->id_pli
                ,'id_field' => $field->id_field
                ,'name_field' => $field->name_field
                ,'saisie_value' => trim($field->valeur)
                ,'change' => 0
                ,'controled' => 0
                ,'saisie_par' => $this->CI->session->id_utilisateur
                ,'dt_saisie' => $dt_saisie
                ,'societe' => $data_saved->data_pli['societe']
            ));
        }
        $data_saved->data_saisie_ctrl = $data_saisie_ctrl;
    }

    public function ctrl_values($data_saved){
        $this->CI->load->model('saisie/model_pli_saisie');
        $data_saisie_ctrl = array();
        $pli = $this->CI->model_pli_saisie->pli($data_saved->id_pli);
        $old_values = $this->CI->model_pli_saisie->get_old_values_ctrl_saisie($data_saved->id_pli);
        $field_found = array();
        foreach ($this->data_fields as $key => $field) {
            $old_value = isset($old_values[$field->id_field]) ? $old_values[$field->id_field]->saisie_value : ''; 
            $name_field = isset($old_values[$field->id_field]) ? $old_values[$field->id_field]->name_field : $field->name_field; 
            array_push($data_saisie_ctrl, array(
                'id_pli' => $data_saved->id_pli
                ,'id_field' => $field->id_field
                ,'name_field' => $name_field
                ,'saisie_value' => $old_value
                ,'control_value' => trim($field->valeur)
                ,'change' => $old_value == trim($field->valeur) ? 0 : 1
                ,'controled' =>  1
                ,'saisie_par' => $pli->saisie_par
                ,'dt_saisie' => $pli->date_saisie
                ,'societe' => $data_saved->data_pli['societe']
            ));
            array_push($field_found, $field->id_field);
        }
        foreach ($old_values as $key => $value) {
            if(!in_array($key, $field_found)){
                array_push($data_saisie_ctrl, array(
                    'id_pli' => $data_saved->id_pli
                    ,'id_field' => $value->id_field
                    ,'name_field' => $value->name_field
                    ,'saisie_value' => $value->saisie_value
                    ,'control_value' => NULL
                    ,'change' => 1
                    ,'controled' =>  1
                    ,'saisie_par' => $pli->saisie_par
                    ,'dt_saisie' => $pli->date_saisie
                    ,'societe' => $data_saved->data_pli['societe']
                ));
            }
        }
        $data_saved->data_saisie_ctrl = $data_saisie_ctrl;
    }
}
