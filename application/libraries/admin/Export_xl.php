<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/php_excel/PHPExcel.php';
require_once APPPATH.'libraries/php_excel/PHPExcel/Writer/Excel2007.php';

class Export_xl{

    private $CI;

    private $excel;
    private $current_page;
    private $i_col;
    private $i_ln;
    private $current_ln;
    private $current_cl;
    private $style_titre;
    private $style_txt;
    private $style_nb;
    private $width_col;

    public function __construct() {
        $this->CI =& get_instance();
        $this->excel = new PHPExcel();
        $this->current_page = -1;
        $this->i_col = 'A';
        $this->i_ln = 1;
        $this->init_style();
    }

    public function add_page($titre=NULL){
        $this->current_page++;
        if($this->current_page > 0){
            $this->excel->createSheet();
        }
        $this->current_ln = $this->i_ln;
        $this->current_cl = $this->i_col;
        $titre = is_null($titre) ? 'Page '.($this->current_page + 1) : $titre;
        $this->excel->setActiveSheetIndex($this->current_page);
        $sheet = $this->excel->getActiveSheet();
        $sheet->setTitle($titre);
    }

    public function write_colomn_title($tab_title, $tab_width){
        $sheet = $this->excel->getActiveSheet();
        $col = $this->i_col;
        $ln = $this->current_ln;
        foreach ($tab_title as $key => $title) {
            $width = is_numeric($tab_width[$key]) ? $tab_width[$key] : 10;
            $sheet->getColumnDimension($col)->setWidth($width);
            $sheet->getStyle($col.$ln)->applyFromArray($this->style_titre);
            $sheet->setCellValue($col.$ln, $title);
            $col++;
        }
        $this->current_cl = $this->i_col;
    }

    public function add_void_line($nb=1){
        for ($i=0; $i < $nb; $i++) { 
            $this->current_ln++;
        }
        $this->current_cl = $this->i_col;
    }
    
    public function write_txt($cel){
        $sheet = $this->excel->getActiveSheet();
        $coord = $this->current_cl.$this->current_ln;
        $sheet->getStyle($coord)->applyFromArray($this->style_txt);
        $sheet->setCellValue($coord, $cel);
        $this->current_cl++;
    }

    public function write_date($cel){
        $sheet = $this->excel->getActiveSheet();
        $coord = $this->current_cl.$this->current_ln;
        $sheet->getStyle($coord)->applyFromArray($this->style_txt);
        $sheet->setCellValue($coord, '"'.$cel.'"');
        $this->current_cl++;
    }
    public function write_nb($cel){
        $sheet = $this->excel->getActiveSheet();
        $coord = $this->current_cl.$this->current_ln;
        $sheet->getStyle($coord)->applyFromArray($this->style_nb);
        $sheet->setCellValue($coord, $cel);
        $this->current_cl++;
    }
    
    public function create_file($file_name, $token){
        $file_name = $file_name.'.xlsx';
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $url =  './'.urlFileTemp.$token.'_token_'.$file_name;
        $objWriter->save($url);
        return $file_name;
    }
    
    private function init_style(){
        $this->style_titre = array(
            'font' => array(
                'bold'  => TRUE
                ,'size'  => 11
            )
            ,'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                ,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ,'wrap' => TRUE
            )
        );
        $this->style_txt = array(
            'font' => array(
                'bold'  => FALSE
                ,'size'  => 10
            )
            ,'alignment' => array(
                'wrap' => TRUE
            )
        );
        $this->style_nb = array(
            'font' => array(
                'bold'  => FALSE
                ,'size'  => 10
            )
            ,'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );
    }

}
