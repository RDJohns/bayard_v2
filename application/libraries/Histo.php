<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Histo{
    
    private $CI;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->model('model_histo');
    }

    public function pli($id_pli, $comms='', $link_motif_consigne=''){ 
        if(is_numeric($id_pli)){
            $pli = $this->CI->model_histo->pli($id_pli);
            if(!is_null($pli)){
                $user = isset($this->CI->session->id_utilisateur) ? $this->CI->session->id_utilisateur : NULL;
                $last = $this->CI->model_histo->last_histo($id_pli);
                if(is_null($last)){
                    $this->CI->model_histo->save_histo($user, $id_pli, $comms, $pli, $link_motif_consigne);
                }else{
                    if($pli->flag_traitement != $last->flag_traitement || $pli->statut_saisie != $last->statut_pli){
                        $this->CI->model_histo->save_histo($user, $id_pli, $comms, $pli, $link_motif_consigne);
                    }
                }
            }
        }
    }

    public function plis($tab_id_pli, $comms=''){
        if(is_array($tab_id_pli)){
            foreach ($tab_id_pli as $id_pli) {
                $this->pli($id_pli, $comms);
            }
        }
    }

    public function action($id_action, $comm='', $id_pli=NULL){
        $user = isset($this->CI->session->id_utilisateur) ? $this->CI->session->id_utilisateur : NULL;
        $this->CI->model_histo->save_act($user, $id_action, $comm, $id_pli);
    }

    public function action_plis($id_action, $comm='', $tab_id_pli=NULL){
        if(is_array($tab_id_pli) && count($tab_id_pli) > 0){
            foreach ($tab_id_pli as $id_pli) {
                $this->action($id_action, $comm, $id_pli);
            }
        }
    }
    
    public function flux($id_flux, $comms='', $link_motif_consigne_flux=''){
        if(is_numeric($id_flux)){
            $flux = $this->CI->model_histo->flux($id_flux);
            if(!is_null($flux)){
                $user = isset($this->CI->session->id_utilisateur) ? $this->CI->session->id_utilisateur : NULL;
                $last = $this->CI->model_histo->last_histo_fx($id_flux);
                if(is_null($last)){
                    $this->CI->model_histo->save_histo_fx($user, $id_flux, $comms, $flux, $link_motif_consigne_flux);
                }else{
                    if($flux->statut_pli != $last->statut_pli || $flux->statut_pli != $last->etat_flux_h){
                        $this->CI->model_histo->save_histo_fx($user, $id_flux, $comms, $flux, $link_motif_consigne_flux);
                    }
                }
            }
        }
    }
    
    public function flux_tab($tab_id_flux, $comms=''){
        if(is_array($tab_id_flux)){
            foreach ($tab_id_flux as $id_flux) {
                $this->flux($id_flux, $comms);
            }
        }
    }

}
