<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Run_script{

    private $CI;
    private $chemin;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->helper('path');
        $this->chemin = set_realpath('.');
        $this->run();
    }

    private function run()
    {
        log_message('debug', 'run scripts');
        
        //control population decoupage
        /*log_message('debug', 'run controle population decoupage');
        $this->CI->load->model('control/model_pli');
        $this->CI->model_pli->verif_control();*/
        
        //regroupement
        exec('php '.$this->chemin.'index.php regroupement regroupement');
    }

}